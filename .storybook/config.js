const req = require.context('../src', true, /.stories.tsx$/);

import { configure } from '@storybook/react';

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
