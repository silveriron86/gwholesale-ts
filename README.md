# Grub frontend

## Requirements
- [latest lts nodejs](https://nodejs.org/en/)
- [latest yarn](https://yarnpkg.com/en)

## Start
- yarn
- yarn start
- localhost:8081

## Tech stacks
- React
- Redux
- [redux-observable](https://github.com/redux-observable/redux-observable)

  https://www.youtube.com/watch?v=AslncyG8whg

- [RxJS](https://rxjs-dev.firebaseapp.com/)
- [redux-epics-decorator](https://github.com/Brooooooklyn/redux-epics-decorator)

  For better code organization, testing, and type safe.

- [emotion](https://emotion.sh/)

## Some notes about naming in styles

```ts
// React.CSSProperties for props#style of lib component
export const highLightStyle: React.CSSProperties = {
  backgroundColor: 'transparent',
}

// emotionjs#css for props#style of DOM
export const autoCompleteDropdownCss = css({
  backgroundColor: lightGreen,
})

// emotionjs#styled for a DOM node
export const SearchBtnText = styled.span({
  marginLeft: 9,
})
```
