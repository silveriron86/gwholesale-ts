### Wholesaleware Build Instruction (Backend Old)

 Request setting.xml from your colleague 
 ```
 cp setting.xml ~/.m2/settings.xml
 mvn install:install-file -Dfile=./lib/java-bitpay-client-1.0-SNAPSHOT.jar -DgroupId=com.bitpay -DartifactId=bitpay-java-api -Dversion=1.0 -Dpackaging=jar -DgeneratePom=true
 ```

 Setup Build Environment
 ```
 // 1, You need the latest version of maven and "JAVA 8"
 // Follow this link on how to install Java 8
 // https://mkyong.com/java/how-to-install-java-on-mac-osx/

 // 2, You need to install a third party jar outside of repository using mvn under food directory

 mvn install:install-file -Dfile=./lib/java-bitpay-client-1.0-SNAPSHOT.jar -DgroupId=com.bitpay -DartifactId=bitpay-java-api -Dversion=1.0 -Dpackaging=jar -DgeneratePom=true
 
 ```

 Build / Compile / Upload to S3
 (For staging change version to follow by SNAPSHOT) 
 Modify {food}/pom.xml
 †
 ```
 // <version>1.1.23.71-SNAPSHOT</version>
 // Then enter the following command to clean, build, package and deploy to S3 repository

 mvn clean install deploy
 ```

 Deployment
 1. Login to Staging/Production Environment

 ```
 ssh -i ~/.ssh/wholesaleware.pem ec2-user@54.177.216.185
 ```

 2. Upload War files from S3 repository into webserver (staging/production)

 ```
 sudo wget https://s3-us-west-1.amazonaws.com/grubmaven/snapshot/com/grubmarket/grubmarket/1.1.23.73.2-SNAPSHOT/grubmarket-1.1.23.73.2-20171024.083928-1.war
 ```

 3. Stop Tomcat 7 server

 sudo service tomcat7 stop

 4. Remove old artifact and copy new one over. (Backup as needed )

 ``` 
 sudo rm -r /var/lib/tomcat7/webapps/*
 sudo cp grubmarket-1.1.23.73.2-20171030.225213-2.war /var/lib/tomcat7/webapps/ROOT.war
 
 ```

 5. Start Tomcat Server again

 ```
 sudo service tomcat7 start
 ```


### Wholesaleware (Frontend)


## Staging

  Setup Build Environment
  ```
  // 1, You need the latest version lts nodejs
  https://nodejs.org/en/
  ```

  ``` Run the following sh command
  1, Build and Upload
  rm -r dist
  rm dist.zip
  npm run build
  zip -r dist.zip dist
  scp -i ~/.ssh/gf.pem dist.zip ec2-user@54.193.111.179:/home/ec2-user/dist.zip

  2, Deploy
  sudo unzip dist.zip
  sudo rm -r /var/lib/tomcat7/webapps/gwholesaler
  sudo cp -r dist /var/lib/tomcat7/webapps/gwholesaler
  ```