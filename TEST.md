# Test environment setup
## use terminal install nvm to manage node versions
https://github.com/nvm-sh/nvm#installing-and-updating

## use nvm

#### install node version 14.19.1

    nvm install 14.19.1

#### set node 14.19.1 to default version

    nvm use 14.19.1
    nvm alias default 14.19.1

## start project

use terminal enter the project directory

    yarn install

    yarn start

The browser open this address to start the test case

http://localhost:8081/#/home/landing

# Pull request testing flow

 1. when a developer send PR to project，Tao  will assigns test tasks
 2. testers  find the correct git branch according by the trello ticket info or pull request info
 3. switch to the git branch && start project
 4. If you find a bug, please add to trello ticket
 5. after you finish all test ，plz go to the pull request page in the github project and click the merge button（If you have any questions, please contact the developer of the current task, or xiaoF）

# Regression testing flow

1. complete regression testing in the test environment （test branch）
2. check the regression test task list in trello
111
