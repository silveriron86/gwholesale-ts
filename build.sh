#!/usr/bin/env bash  

rm -r dist
rm dist.zip
npm run build
zip -r dist.zip dist
scp -i ~/.ssh/wholesaleware.pem dist.zip ec2-user@54.153.24.54:/home/ec2-user/dist.zip
