#!/usr/bin/env bash  

rm -r dist
rm dist.zip
npm run build
zip -r dist.zip dist
scp -i ~/.ssh/wholesaleware.pem dist.zip ec2-user@13.52.243.65:/home/ec2-user/dist.zip
