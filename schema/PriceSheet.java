package com.grubmarket.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.grubmarket.model.QueryName.QueryNameHolder;

@NamedQueries({
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_BY_OWNER_ID_NAME,
	query = "SELECT NEW MAP (p.priceSheetId as priceSheetId," +
		"p.createdDate as createdDate," + "p.updatedDate as updatedDate," + 
		"p.name as name)" +
		"FROM PriceSheet p WHERE p.user.userId = :userId")
})

@Entity
@Table(name = "price_sheet")
public class PriceSheet implements Serializable {

	private static final long serialVersionUID = 1814827188334646860L;
	private int priceSheetId;
	private Date createdDate;
	private Date updatedDate;
	private String name;
	private User user;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getPriceSheetId() {
		return priceSheetId;
	}

	public void setPriceSheetId(int priceSheetId) {
		this.priceSheetId = priceSheetId;
	}
	
	@Column(name = "created_at", updatable = false, insertable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_at", updatable = false, insertable = false)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
