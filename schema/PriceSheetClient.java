package com.grubmarket.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.grubmarket.model.QueryName.QueryNameHolder;

@NamedQueries({
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_BY_ASSIGNED_CUSTOMER_ID_NAME,
			query = "SELECT NEW MAP (p.priceSheet.priceSheetId as priceSheetId," +
					"p.priceSheet.createdDate as createdDate," + "p.priceSheet.updatedDate as updatedDate," + 
					"p.linkToken as linkToken," + "p.linkExpireDate as linkExpireDate," +
					"p.priceSheetClientId as priceSheetClientId," + "p.priceSheet.name as name)" +
				"FROM PriceSheetClient p WHERE p.wholesaleCustomerClient.customerId = :customerId"),
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_BY_ASSIGNED_USER_ID_NAME,
	query = "SELECT NEW MAP (p.priceSheet.priceSheetId as priceSheetId," +
			"p.priceSheet.createdDate as createdDate," + "p.priceSheet.updatedDate as updatedDate," +
			"p.wholesaleCustomerClient.seller.firstName as firstName," + "p.wholesaleCustomerClient.seller.lastName as lastName," + "p.wholesaleCustomerClient.seller.company as company," +
			"p.wholesaleCustomerClient.seller.emailAddress as emailAddress," + "p.wholesaleCustomerClient.seller.phone as phone," +
			"p.wholesaleCustomerClient.customerId as customerId," +
			"p.linkToken as linkToken," + "p.linkExpireDate as linkExpireDate," +
			"p.priceSheetClientId as priceSheetClientId," + "p.priceSheet.name as name)" +
		"FROM PriceSheetClient p WHERE p.wholesaleCustomerClient.user.userId = :userId"),
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_USER_BY_PRICE_SHEET_ID_NAME,
			query = "SELECT NEW MAP (p.wholesaleCustomerClient.customerId as customerId," +
					"p.wholesaleCustomerClient.fullName as fullName," + "p.wholesaleCustomerClient.emailAddress as emailAddress," + 
					"p.linkToken as linkToken," + "p.linkExpireDate as linkExpireDate," +
					"p.priceSheetClientId as priceSheetClientId)" +
					"FROM PriceSheetClient p WHERE p.priceSheet.priceSheetId = :priceSheetId")
})

@Entity
@Table(name = "price_sheet_client")
public class PriceSheetClient implements Serializable{

	private static final long serialVersionUID = 4729754661931207498L;
	private int priceSheetClientId;
	private PriceSheet priceSheet;
	private Date createdDate;
	private Date updatedDate;
	private WholesaleCustomerClient wholesaleCustomerClient;
	private String linkToken;
	private Date linkExpireDate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getPriceSheetClientId() {
		return priceSheetClientId;
	}

	public void setPriceSheetClientId(int priceSheetClientId) {
		this.priceSheetClientId = priceSheetClientId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "price_sheet_id")
	public PriceSheet getPriceSheet() {
		return priceSheet;
	}

	public void setPriceSheet(PriceSheet priceSheet) {
		this.priceSheet = priceSheet;
	}
	
	@Column(name = "created_at", updatable = false, insertable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_at", updatable = false, insertable = false)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "wholesale_customer_client_id")
	public WholesaleCustomerClient getWholesaleCustomerClient() {
		return wholesaleCustomerClient;
	}

	public void setWholesaleCustomerClient(WholesaleCustomerClient wholesaleCustomerClient) {
		this.wholesaleCustomerClient = wholesaleCustomerClient;
	}

	@Column(name = "link_token")
	public String getLinkToken() {
		return linkToken;
	}

	public void setLinkToken(String linkToken) {
		this.linkToken = linkToken;
	}

	@Column(name = "link_expire_at")
	public Date getLinkExpireDate() {
		return linkExpireDate;
	}

	public void setLinkExpireDate(Date linkExpireDate) {
		this.linkExpireDate = linkExpireDate;
	}
	
}
