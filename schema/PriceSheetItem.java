package com.grubmarket.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.grubmarket.model.QueryName.QueryNameHolder;

@NamedQueries({
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_ITEM_BY_PRICE_SHEET_ID_API_NAME,
		query = "SELECT NEW MAP (p.priceSheetItemId as priceSheetItemId," +
			"p.createdDate as createdDate," + "p.updatedDate as updatedDate," + 
			"p.freight as freight," + "p.markup as markup," + "p.salePrice as salePrice," +
			"p.wholesaleItem.wholesaleItemId as itemId," + "p.wholesaleItem.provider as provider," +  
			"p.wholesaleItem.wholesaleCategory.name as category," + "p.wholesaleItem.isListed as isListed," +
			"p.wholesaleItem.size as size," + "p.wholesaleItem.packing as packing," + "p.wholesaleItem.SKU as SKU," + 
			"p.wholesaleItem.origin as origin," + "p.wholesaleItem.label as label," +
			"p.wholesaleItem.weight as weight," + "p.wholesaleItem.cost as cost," +
			"p.wholesaleItem.quantity as availableQuantity," + "p.wholesaleItem.grade as grade," +
			"p.wholesaleItem.wholesaleCategory as wholesaleCategory," +
			"p.wholesaleItem.isOrganic as organic," + "p.wholesaleItem.stockEnabled as stockEnabled," + "p.wholesaleItem.variety as variety)" +
			"FROM PriceSheetItem p WHERE p.priceSheet.priceSheetId = :priceSheetId"),
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_ITEM_BY_PRICE_SHEET_ID_NAME,
		query = "FROM PriceSheetItem p WHERE p.priceSheet.priceSheetId = :priceSheetId"),
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_ITEM_BY_PRICE_SHEET_AND_ITEM_NAME,
		query = "FROM PriceSheetItem AS p " +
//				"LEFT JOIN FETCH p.item AS item" +
//				"LEFT JOIN FETCH p.priceSheet AS priceSheet" +
				" WHERE p.priceSheet.priceSheetId = :priceSheetId AND p.wholesaleItem.wholesaleItemId = :wholesaleItemId"),
	@NamedQuery(name = QueryNameHolder.FIND_PRICE_SHEET_ITEMS_BY_ITEM_ID_NAME,
	query = "SELECT NEW MAP (p.priceSheetItemId as priceSheetItemId," +
		"p.freight as freight," + "p.markup as markup," + "p.salePrice as salePrice," +
		"p.wholesaleItem.cost as cost," + "p.priceSheet.name as priceSheetName)" +
		"FROM PriceSheetItem p WHERE p.wholesaleItem.wholesaleItemId = :wholesaleItemId AND p.priceSheet.user.userId = :userId")
})

@Entity
@Table(name = "price_sheet_item")
public class PriceSheetItem implements Serializable {

	private static final long serialVersionUID = 1629764168885237405L;
	private int priceSheetItemId;
	private PriceSheet priceSheet;
	private Double freight;
	private Double markup;
	private Double salePrice;
	private WholesaleItem wholesaleItem;
	private Date createdDate;
	private Date updatedDate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getPriceSheetItemId() {
		return priceSheetItemId;
	}

	public void setPriceSheetItemId(int priceSheetItemId) {
		this.priceSheetItemId = priceSheetItemId;
	}
	
	@Column(name = "freight")
	public Double getFreight() {
		return freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}

	@Column(name = "markup")
	public Double getMarkup() {
		return markup;
	}

	public void setMarkup(Double markup) {
		this.markup = markup;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "price_sheet_id")
	public PriceSheet getPriceSheet() {
		return priceSheet;
	}

	public void setPriceSheet(PriceSheet priceSheet) {
		this.priceSheet = priceSheet;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "wholesale_item_id")
	public WholesaleItem getWholesaleItem() {
		return wholesaleItem;
	}

	public void setWholesaleItem(WholesaleItem wholesaleItem) {
		this.wholesaleItem = wholesaleItem;
	}
	
	@Column(name = "created_at", updatable = false, insertable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_at", updatable = false, insertable = false)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "sale_price")
	public Double getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(Double salePrice) {
		this.salePrice = salePrice;
	}
	
	
	public static Comparator<PriceSheetItem> NAME_COMPARATOR = new Comparator<PriceSheetItem>() {
		@Override
		public int compare(final PriceSheetItem o1, final PriceSheetItem o2) {
			String fullName1 = o1.getWholesaleItem().getIsOrganic()+o1.getWholesaleItem().getWholesaleCategory().getName()+o1.getWholesaleItem().getVariety();
			String fullName2 = o2.getWholesaleItem().getIsOrganic()+o2.getWholesaleItem().getWholesaleCategory().getName()+o2.getWholesaleItem().getVariety();
			return fullName1.compareTo(fullName2);
		}
	};
}
