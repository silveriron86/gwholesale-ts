# GWholesale Schema

## User

User (SSO Login, only use credntial for this app)
WholesaleSession
WholesaleCustomerClient (Actual Customer Information)
WholesaleUserSetting (User Setting)

## Item
WholesaleItem
WholesaleCategory
WholesaleSection

## Pricesheet

PriceSheet
PriceSheetItem (A sheet that contains list of items to display to a customer)
PriceSheetClient

## Order

WholesaleOrder
WholesaleOrderItem
