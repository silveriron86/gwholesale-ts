package com.grubmarket.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name = "wholesale_category")
public class WholesaleCategory implements Serializable {

	private static final long serialVersionUID = -4087965060753289661L;
	private int wholesaleCategoryId;
	private String name;
	private String warehouse;
	private WholesaleSection wholesaleSection;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getWholesaleCategoryId() {
		return wholesaleCategoryId;
	}
	
	public void setWholesaleCategoryId(int wholesaleCategoryId) {
		this.wholesaleCategoryId = wholesaleCategoryId;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "wholesale_section_id")
	public WholesaleSection getWholesaleSection() {
		return wholesaleSection;
	}

	public void setWholesaleSection(WholesaleSection wholesaleSection) {
		this.wholesaleSection = wholesaleSection;
	}

	@Column(name = "warehouse")
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
}
