package com.grubmarket.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.grubmarket.enums.AccountStatus;
import com.grubmarket.model.QueryName.QueryNameHolder;

//FIND_WHOLESALE_CUSTOMER_BY_SELLER_ID
@NamedQueries({
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_CUSTOMER_BY_SELLER_ID_NAME,
		query = 
			"FROM WholesaleCustomerClient w WHERE w.seller.userId = :sellerId"),
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_CUSTOMER_BY_ID_NAME,
		query = 
			"FROM WholesaleCustomerClient w WHERE w.customerId = :customerId")
})

@Entity
@Table(name = "wholesale_customer_client")
public class WholesaleCustomerClient implements Serializable {

	private static final long serialVersionUID = 4921419197125514548L;
	
	private int customerId;
	private User user;
	private User seller;
	private String emailAddress;
	private String fullName;
	private String phone;
	private String company;
	private Address address;
	private AccountStatus status;
	private String qboId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "seller_id")
	public User getSeller() {
		return seller;
	}

	public void setSeller(User seller) {
		this.seller = seller;
	}

	@Column(name= "email_address")
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Column(name= "full_name")
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	@Column(name= "phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "address_id")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	@Column(name= "company")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}


	@Column(name= "status")
	@Enumerated(EnumType.STRING)
	public AccountStatus getStatus() {
		return status;
	}
	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	@Column(name= "qbo_id")
	public String getQboId() {
		return qboId;
	}

	public void setQboId(String qboId) {
		this.qboId = qboId;
	}
	
}