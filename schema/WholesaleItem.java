package com.grubmarket.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.grubmarket.model.QueryName.QueryNameHolder;

@Entity()
@BatchSize(size = 25)
@Table(name = "wholesale_item")

@NamedQueries({
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_ITEMS_BY_WAREHOUSE_NAME,
		query = "SELECT NEW MAP (i.wholesaleItemId as itemId," +
			"i.provider as provider," +  "i.variety as variety," + "i.isListed as status," +
			"i.size as size," + "i.packing as packing," + "i.origin as origin," + 
			"i.label as label," + "i.SKU as SKU," + "i.stockEnabled as stockEnabled," +  
			"i.weight as weight," + "i.cost as cost," + "i.isOrganic as organic," +
			"i.quantity as quantity," +  "i.grade as grade," + "i.wholesaleCategory.name as category," +
			"i.wholesaleCategory as wholesaleCategory," +
			"i.qboId as qboId" +
			") FROM WholesaleItem AS i WHERE i.warehouse = :warehouse"),
})

public class WholesaleItem implements Serializable {

	private static final long serialVersionUID = 1436268558622497625L;
	
	private int wholesaleItemId;
	
	private String weight;
	private Double cost;
	private Integer quantity;
	private String provider;
	private String variety;
	private String size;
	private String origin;
	private String packing;
	private String grade;
	private String label;
	private String warehouse;
	private String SKU;
	private WholesaleCategory wholesaleCategory;
	private String qboId;
	
	private boolean stockEnabled;
	private boolean isOrganic;
	private boolean isListed;
	private Date createdDate;
	private Date updatedDate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "wholesale_item_id")
	public int getWholesaleItemId() {
		return wholesaleItemId;
	}
	
	public void setWholesaleItemId(int wholesaleItemId) {
		this.wholesaleItemId = wholesaleItemId;
	}
	
	@Column(name = "weight")
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	@Column(name = "cost")
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	@Column(name = "quantity")
	public Integer getQuantity() {
		return quantity;
	}
	
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name = "provider")
	public String getProvider() {
		return provider;
	}
	
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	@Column(name = "variety")
	public String getVariety() {
		return variety;
	}
	
	public void setVariety(String variety) {
		this.variety = variety;
	}
	
	@Column(name = "size")
	public String getSize() {
		return size;
	}
	
	public void setSize(String size) {
		this.size = size;
	}
	
	@Column(name = "origin")
	public String getOrigin() {
		return origin;
	}
	
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	@Column(name = "packing")
	public String getPacking() {
		return packing;
	}
	
	public void setPacking(String packing) {
		this.packing = packing;
	}
	
	@Column(name = "grade")
	public String getGrade() {
		return grade;
	}
	
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	@Column(name = "label")
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Column(name = "warehouse")
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 1)
	@JoinColumn(name = "wholesale_category_id")
	public WholesaleCategory getWholesaleCategory() {
		return wholesaleCategory;
	}
	
	public void setWholesaleCategory(WholesaleCategory wholesaleCategory) {
		this.wholesaleCategory = wholesaleCategory;
	}
	
	@Column(name = "is_listed")
	public boolean getIsListed() {
		return isListed;
	}
	
	public void setIsListed(boolean isListed) {
		this.isListed = isListed;
	}
	
	@Column(name = "created_at", updatable = false, insertable = false)
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "updated_at", updatable = false, insertable = false)
	public Date getUpdatedDate() {
		return updatedDate;
	}
	
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public void addQuantity(Integer addition)
	{
		if (addition == null) return;
		if (quantity == null) quantity = 0;
		setQuantity(quantity + addition);
	}

	public void deductQuantity(Integer deduction)
	{
		if (deduction == null) return;
		if (quantity == null) quantity = 0;
		this.setQuantity(quantity - deduction);
	}

	@Column(name = "is_organic")
	public boolean getIsOrganic() {
		return isOrganic;
	}

	public void setIsOrganic(boolean organic) {
		this.isOrganic = organic;
	}

	@Column(name = "SKU")
	public String getSKU() {
		return SKU;
	}

	public void setSKU(String sKU) {
		SKU = sKU;
	}

	@Column(name = "stock_enabled")
	public boolean isStockEnabled() {
		return stockEnabled;
	}

	public void setStockEnabled(boolean stockEnabled) {
		this.stockEnabled = stockEnabled;
	}

	@Column(name = "qbo_id")
	public String getQboId() {
		return qboId;
	}

	public void setQboId(String qboId) {
		this.qboId = qboId;
	}

}
