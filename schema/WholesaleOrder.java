package com.grubmarket.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.grubmarket.enums.WholesaleOrderStatusEnum;
import com.grubmarket.model.QueryName.QueryNameHolder;

@NamedQueries({
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_ORDER_BY_CUSTOMER_ID_NAME,
		query = "SELECT NEW MAP (w.wholesaleOrderId as wholesaleOrderId," +
			"w.totalCost as totalCost," + "w.totalPrice as totalPrice," + "w.qboId as qboId," +
			"w.deliveryDate as deliveryDate," + "w.createdDate as createdDate," + "w.wholesaleCustomerClient.fullName as fullName," +
			"w.updatedDate as updatedDate," + "w.wholesaleOrderStatus as status," + "w.wholesaleCustomerClient.customerId as wholesaleCustomerClientId)" +
			"FROM WholesaleOrder w WHERE w.wholesaleCustomerClient.customerId = :customerId"),
	@NamedQuery(name = QueryNameHolder.FIND_WAREHOUSE_WHOLESALE_ORDER_BY_DELIVERY_DATE_NAME,
	query = "SELECT NEW MAP (w.wholesaleOrderId as wholesaleOrderId," +
		"w.totalCost as totalCost," + "w.totalPrice as totalPrice," + "w.qboId as qboId," +
		"w.deliveryDate as deliveryDate," + "w.createdDate as createdDate," + "w.wholesaleCustomerClient.fullName as fullName," +
		"w.updatedDate as updatedDate," + "w.wholesaleOrderStatus as status," + "w.wholesaleCustomerClient.customerId as wholesaleCustomerClientId)" +
		"FROM WholesaleOrder w WHERE w.wholesaleCustomerClient.seller.company = :warehouse AND w.deliveryDate = :deliveryDate"),
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_ORDER_BY_USER_ID_NAME,
	query = "SELECT NEW MAP (w.wholesaleOrderId as wholesaleOrderId," +
		"w.totalPrice as totalPrice," + "w.wholesaleCustomerClient.seller.company as company," +
		"w.wholesaleCustomerClient.seller.firstName as firstName," + "w.wholesaleCustomerClient.seller.lastName as lastName," +
		"w.deliveryDate as deliveryDate," + "w.createdDate as createdDate," + "w.qboId as qboId," +
		"w.updatedDate as updatedDate," + "w.wholesaleOrderStatus as status," + "w.wholesaleCustomerClient.customerId as wholesaleCustomerClientId)" +
		"FROM WholesaleOrder w WHERE w.wholesaleCustomerClient.user.userId = :userId")
})

@Entity
@Table(name = "wholesale_order")
public class WholesaleOrder implements Serializable {

	private static final long serialVersionUID = 6929302062756997358L;
	
	private int wholesaleOrderId;
	private Double totalCost;
	private Double totalPrice;
	private WholesaleOrderStatusEnum wholesaleOrderStatus;
	private Date deliveryDate;
	private Date createdDate;
	private Date updatedDate;
	private User user;
	private String priceSheetId;
	private WholesaleCustomerClient wholesaleCustomerClient;
	private String qboId;
	private String linkToken;
	private Date linkExpireDate;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getWholesaleOrderId() {
		return wholesaleOrderId;
	}
	
	public void setWholesaleOrderId(int wholesaleOrderId) {
		this.wholesaleOrderId = wholesaleOrderId;
	}

	@Column(name = "total_cost")
	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	@Column(name = "total_price")
	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Column(name = "status", columnDefinition = "enum")
	@Enumerated(EnumType.STRING)
	public WholesaleOrderStatusEnum getWholesaleOrderStatus() {
		return wholesaleOrderStatus;
	}

	public void setWholesaleOrderStatus(WholesaleOrderStatusEnum wholesaleOrderStatus) {
		this.wholesaleOrderStatus = wholesaleOrderStatus;
	}

	@Column(name = "delivery_date")
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@Column(name = "created_at", updatable = false, insertable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_at", updatable = false, insertable = false)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "wholesale_customer_client_id")
	public WholesaleCustomerClient getWholesaleCustomerClient() {
		return wholesaleCustomerClient;
	}

	public void setWholesaleCustomerClient(WholesaleCustomerClient wholesaleCustomerClient) {
		this.wholesaleCustomerClient = wholesaleCustomerClient;
	}
	
	@Column(name= "qbo_id")
	public String getQboId() {
		return qboId;
	}

	public void setQboId(String qboId) {
		this.qboId = qboId;
	}
	
	@Column(name = "link_token")
	public String getLinkToken() {
		return linkToken;
	}

	public void setLinkToken(String linkToken) {
		this.linkToken = linkToken;
	}

	@Column(name = "link_expire_at")
	public Date getLinkExpireDate() {
		return linkExpireDate;
	}

	public void setLinkExpireDate(Date linkExpireDate) {
		this.linkExpireDate = linkExpireDate;
	}

	@Column(name = "pricesheet_id")
	public String getPriceSheetId() {
		return priceSheetId;
	}

	public void setPriceSheetId(String priceSheetId) {
		this.priceSheetId = priceSheetId;
	}
	
}
