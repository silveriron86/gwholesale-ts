package com.grubmarket.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.grubmarket.enums.WholesaleOrderStatusEnum;
import com.grubmarket.model.QueryName.QueryNameHolder;

@NamedQueries({
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_ORDER_ITEM_BY_ORDER_ID_NAME,
		query = "SELECT NEW MAP (w.wholesaleOrderItemId as wholesaleOrderItemId," +
			"w.cost as cost," + "w.price as price," + "w.quantity as quantity," +
			"w.deliveryDate as deliveryDate," + "w.createdDate as createdDate," +
			"w.markup as markup," + "w.freight as freight," +
			"w.updatedDate as updatedDate," + "w.wholesaleOrderItemStatus as status," +
			"w.wholesaleItem.wholesaleItemId as itemId," + "w.wholesaleItem.provider as provider," +  
			"w.wholesaleItem.wholesaleCategory.name as category," + "w.wholesaleItem.isListed as isListed," +
			"w.wholesaleItem.size as size," + "w.wholesaleItem.packing as packing," + 
			"w.wholesaleItem.origin as origin," + "w.wholesaleItem.label as label," +
			"w.wholesaleItem.weight as weight," + "w.wholesaleItem.isOrganic as organic," +
			"w.wholesaleItem.quantity as availableQuantity," + "w.wholesaleItem.grade as grade," + 
			"w.wholesaleItem.SKU as SKU," + "w.wholesaleItem.stockEnabled as stockEnabled," + "w.wholesaleItem.variety as variety)" +
			"FROM WholesaleOrderItem w WHERE w.order.wholesaleOrderId = :orderId"),
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_ORDER_ITEM_BY_ID_NAME,
	query = 
		"FROM WholesaleOrderItem w WHERE w.wholesaleOrderItemId = :wholesaleOrderItemId"),
	@NamedQuery(name = QueryNameHolder.FIND_WHOLESALE_ORDER_ITEM_BY_WHOLESALE_ITEM_ID_NAME,
	query = "SELECT NEW MAP (w.wholesaleOrderItemId as wholesaleOrderItemId," +
		"w.cost as cost," + "w.price as price," + "w.quantity as quantity," +
		"w.markup as markup," + "w.freight as freight," +
		"w.deliveryDate as deliveryDate," + "w.createdDate as createdDate," +
		"w.updatedDate as updatedDate," + "w.wholesaleOrderItemStatus as status," +
		"w.wholesaleItem.wholesaleItemId as itemId," + "w.wholesaleItem.provider as provider," +  
		"w.wholesaleItem.wholesaleCategory.name as category," + "w.wholesaleItem.isListed as isListed," +
		"w.wholesaleItem.size as size," + "w.wholesaleItem.packing as packing," + 
		"w.wholesaleItem.origin as origin," + "w.wholesaleItem.label as label," +
		"w.wholesaleItem.weight as weight," + "w.wholesaleItem.SKU as SKU," +
		"w.wholesaleItem.quantity as availableQuantity," + "w.wholesaleItem.grade as grade," + 
		"w.wholesaleItem.stockEnabled as stockEnabled," + "w.wholesaleItem.variety as variety)" +
		"FROM WholesaleOrderItem w WHERE w.wholesaleItem.wholesaleItemId = :wholesaleItemId")
})

@Entity
@Table(name = "wholesale_order_item")
public class WholesaleOrderItem implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5492312944065690094L;
	private int wholesaleOrderItemId;
	private Double cost;
	private Double price;
	private Double markup;
	private Double freight;
	private WholesaleOrderStatusEnum wholesaleOrderItemStatus;
	private Date deliveryDate;
	private Date createdDate;
	private Date updatedDate;
	private Integer quantity;

	private WholesaleOrder order;
	private WholesaleItem wholesaleItem;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getWholesaleOrderItemId() {
		return wholesaleOrderItemId;
	}
	
	public void setWholesaleOrderItemId(int wholesaleOrderItemId) {
		this.wholesaleOrderItemId = wholesaleOrderItemId;
	}

	@Column(name = "cost")
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "status", columnDefinition = "enum")
	@Enumerated(EnumType.STRING)
	public WholesaleOrderStatusEnum getWholesaleOrderItemStatus() {
		return wholesaleOrderItemStatus;
	}

	public void setWholesaleOrderItemStatus(WholesaleOrderStatusEnum wholesaleOrderItemStatus) {
		this.wholesaleOrderItemStatus = wholesaleOrderItemStatus;
	}

	@Column(name = "delivery_date")
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@Column(name = "created_at", updatable = false, insertable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_at", updatable = false, insertable = false)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "wholesale_order_id")
	public WholesaleOrder getOrder() {
		return order;
	}

	public void setOrder(WholesaleOrder wholesaleOrder) {
		this.order = wholesaleOrder;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinColumn(name = "wholesale_item_id")
	public WholesaleItem getWholesaleItem() {
		return wholesaleItem;
	}

	public void setWholesaleItem(WholesaleItem wholesaleItem) {
		this.wholesaleItem = wholesaleItem;
	}

	@Column(name = "markup")
	public Double getMarkup() {
		return markup;
	}

	public void setMarkup(Double markup) {
		this.markup = markup;
	}

	@Column(name = "freight")
	public Double getFreight() {
		return freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}

	public static Comparator<WholesaleOrderItem> NAME_COMPARATOR = new Comparator<WholesaleOrderItem>() {
		@Override
		public int compare(final WholesaleOrderItem o1, final WholesaleOrderItem o2) {
			String fullName1 = o1.getWholesaleItem().getIsOrganic()+o1.getWholesaleItem().getWholesaleCategory().getName()+o1.getWholesaleItem().getVariety();
			String fullName2 = o2.getWholesaleItem().getIsOrganic()+o2.getWholesaleItem().getWholesaleCategory().getName()+o2.getWholesaleItem().getVariety();
			return fullName1.compareTo(fullName2);
		}
	};
	/*
	      }
	   };*/
}
