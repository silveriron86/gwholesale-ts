package com.grubmarket.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wholesale_section")
public class WholesaleSection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1386788302585460198L;
	private int wholesaleSectionId;
	private String name;
	private String warehouse;
	private String qboId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getWholesaleSectionId() {
		return wholesaleSectionId;
	}
	
	public void setWholesaleSectionId(int wholesaleSectionId) {
		this.wholesaleSectionId = wholesaleSectionId;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "warehouse")
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	@Column(name = "qbo_id")
	public String getQboId() {
		return qboId;
	}

	public void setQboId(String qboId) {
		this.qboId = qboId;
	}
}