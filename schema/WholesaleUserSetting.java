package com.grubmarket.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wholesale_user_setting")
public class WholesaleUserSetting implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3694652683461825584L;
	private int userId;
	private String qboRealmId;
	private String qboAccessToken;
	private String qboRefreshToken;
	private String qboEmail;
	private Date lastUpdate;
	
	private String companyName;
	private String logoURL;
	private String colorTheme;
	

	@Id
	@Column(name = "user_id")
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Column(name = "qbo_realm_id")
	public String getQboRealmId() {
		return qboRealmId;
	}
	public void setQboRealmId(String qboRealmId) {
		this.qboRealmId = qboRealmId;
	}

	@Column(name = "qbo_access_token")
	public String getQboAccessToken() {
		return qboAccessToken;
	}
	public void setQboAccessToken(String qboAccessToken) {
		this.qboAccessToken = qboAccessToken;
	}
	
	@Column(name = "qbo_refresh_token")
	public String getQboRefreshToken() {
		return qboRefreshToken;
	}
	public void setQboRefreshToken(String qboRefreshToken) {
		this.qboRefreshToken = qboRefreshToken;
	}

	@Column(name = "qbo_email")
	public String getQboEmail() {
		return qboEmail;
	}
	public void setQboEmail(String qboEmail) {
		this.qboEmail = qboEmail;
	}

	@Column(name = "updated_at", updatable = false, insertable = false)
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Column(name = "company_name")
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	@Column(name = "logo_url")
	public String getLogoURL() {
		return logoURL;
	}
	public void setLogoURL(String logoURL) {
		this.logoURL = logoURL;
	}
	
	@Column(name = "color_theme")
	public String getColorTheme() {
		return colorTheme;
	}
	public void setColorTheme(String colorTheme) {
		this.colorTheme = colorTheme;
	}
}