import 'reflect-metadata'
import 'normalize.css'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { ThemeProvider } from 'emotion-theming'

import { configureStore } from './store'
import { Routes } from './routes'
if (process.env.NODE_ENV !== 'production') {
  require('antd/dist/antd.less')
}

const { store, history } = configureStore()

const root = document.querySelector('#root')!

const render = (Component: any) => {
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <ThemeProvider theme={{}}>
          <Component />
        </ThemeProvider>
      </ConnectedRouter>
    </Provider>,
    root,
  )
}

render(Routes)

if (__DEV__ && module.hot) {
  module.hot.accept('./routes.tsx', () => {
    render(require('./routes.tsx').Routes)
  })
}
