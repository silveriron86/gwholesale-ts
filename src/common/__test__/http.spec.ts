import Sinon from 'sinon'
import { Subscription } from 'rxjs'
import { getInstance } from 'redux-epics-decorator'

import { Http } from '../http'

describe('Http specs:', () => {
  let subscription: Subscription
  let http: Http

  beforeEach(() => {
    subscription = new Subscription()
    http = getInstance(Http)
    http.prefix = ''
  })

  afterEach(() => {
    subscription.unsubscribe()
  })
  ;['get', 'post', 'put', 'delete'].forEach((method) => {
    it(`Http#${method} should pass the same method to fetch`, () => {
      const spy = Sinon.spy(window, 'fetch')
      const mockUrl = '/api/test'
      const sub = http[method](mockUrl).subscribe()
      subscription.add(sub)
      const [args] = spy.args
      expect(args[1]!.method).toBe(method.toUpperCase())
      spy.restore()
    })

    it(`Http#${method} with query should dispatch correct url`, () => {
      const spy = Sinon.spy(window, 'fetch')
      const mockUrl = '/api/test'
      const sub = http[method](mockUrl, {
        query: {
          foo: 'bar',
        },
      }).subscribe()
      subscription.add(sub)
      const [[requestUrl]] = spy.args
      expect(requestUrl).toBe('/api/test?foo=bar')
      spy.restore()
    })

    it(`Http#${method} should be able to canceled`, () => {
      const mockUrl = '/api/test'
      const spy = Sinon.stub(AbortController.prototype, 'abort')
      const sub = http[method](mockUrl).subscribe()

      sub.unsubscribe()
      expect(spy.callCount).toBe(1)
      spy.restore()
    })
  })
})
