export interface Theme {
  main: string
  dark: string
  light: string
  lighter: string
  primary: string
  theme: string
}

export const white = '#ffffff'
export const white6 = 'rgba(255, 255, 255, 0.6)'
export const white4 = 'rgba(255, 255, 255, 0.4)'
export const black = '#000000'
export const blue = '#2880B9'
// same as antd
export const error = '#f5222d'

export const darkGreen = '#1C6E31'
export const brightGreen = '#26AC5F'
export const brightGreen2 = '#2BC46C'
export const filterGreen = '#EDF1EE'
export const backgroundGreen = '#FAFAFA'
export const leafGreen = '#529E63'
export const leafLightGreen = '#e9f3eb'
export const leafGreenOpacity = 'rgba(82, 158, 99, 0.1)'
export const mutedGreen = '#86D2A7'
export const forestGreen = '#0D4B1C'
export const lightGreen = '#EDF1EE'
export const lightGreen2 = '#BEE6CF'
export const lightGreen3 = '#EEF5EF'
export const btnGreen = '#BEE6CF'

export const lightGrey = '#C1C3C1'
export const lightGrey2 = '#C1C3C1'
export const lightGrey3 = '#C1C3C1'
export const lightGrey4 = '#C1C3C1'
export const lightGrey5 = '#C1C3C1'
export const mediumGrey = '#555F61'
export const mediumGrey2 = '#A2A2A2'
export const grey = '#C1C3C1'
export const brownGrey = '#645D5C'
export const darkGrey = '#373D3F'
//export const darkGrey2 = '#22282A'
export const medLightGrey = '#E7E7E7'
export const topLightGrey = '#F3F3F3'
export const disabledGrey = '#C4C4C4'

export const gray01 = '#22282A'
export const gray02 = '#4A5355'
export const gray03 = '#82898A'
export const gray04 = '#D8DBDB'
export const gray05 = '#F7F7F7'

export const yellow = '#F6EC91'
export const mainYellow = '#623E15'
export const darkYellow = '#9C6A08'
export const lightYellow = '#D19700'
export const lighterYellow = '#f9f2df'
export const primaryYellow = '#F2BD04'
export const themeYellow = '#EBD721'

export const red = '#FF715E'
export const mainRed = '#4B0D0D'
export const darkRed = '#6E211C'
export const lightRed = '#A64040'
export const lighterRed = '#f4e7e7'
export const primaryRed = '#D84242'
export const themeRed = '#B91818'

export const mainOrange = '#7A2C00'
export const darkOrange = '#9A4A00'
export const lightOrange = '#C86000'
export const lighterOrange = '#f8ebdf'
export const primaryOrange = '#E97000'
export const themeOrange = '#EF8D1A'

export const mainBlue = '#154662'
export const darkBlue = '#1A61A3'
export const lightBlue = '#0689C2'
export const lighterBlue = '#e0f0f7'
export const primaryBlue = '#00A5EC'
export const themeBlue = '#1C94C7'

export const mainPurple = '#371562'
export const darkPurple = '#492693'
export const lightPurple = '#712CB6'
export const lighterPurple = '#ede5f6'
export const primaryPurple = '#8443D6'
export const themePurple = '#81078B'

export const mainNeutral = '#3E3E3E'
export const darkNeutral = '#525252'
export const lightNeutral = '#818181'
export const lighterNeutral = '#efefef'
export const primaryNeutral = '#000000'
export const themeNeutral = '#A2A2A2'

export const badgeColor = '#B50000'

export const transparent = 'transparent'

export const cancelRed  = '#C01700'

export const themes: { [key: string]: Theme } = {
  red: {
    main: mainRed,
    dark: darkRed,
    light: lightRed,
    lighter: lighterRed,
    primary: primaryRed,
    theme: themeRed,
  },
  orange: {
    main: mainOrange,
    dark: darkOrange,
    light: lightOrange,
    lighter: lighterOrange,
    primary: primaryOrange,
    theme: themeOrange,
  },
  yellow: {
    main: mainYellow,
    dark: darkYellow,
    light: lightYellow,
    lighter: lighterYellow,
    primary: primaryYellow,
    theme: themeYellow,
  },
  green: {
    main: forestGreen,
    dark: darkGreen,
    light: leafGreen,
    lighter: leafLightGreen,
    primary: brightGreen,
    theme: darkGreen,
  },
  blue: {
    main: mainBlue,
    dark: darkBlue,
    light: lightBlue,
    lighter: lighterBlue,
    primary: primaryBlue,
    theme: themeBlue,
  },
  purple: {
    main: mainPurple,
    dark: darkPurple,
    light: lightPurple,
    lighter: lighterPurple,
    primary: primaryPurple,
    theme: themePurple,
  },
  neutral: {
    main: mainNeutral,
    dark: darkNeutral,
    light: lightNeutral,
    lighter: lighterNeutral,
    primary: primaryNeutral,
    theme: themeNeutral,
  },
}
