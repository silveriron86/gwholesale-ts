/**
 * @deprecated
 */
export const TITLES = [
  'sku',
  'category',
  'name',
  'size',
  'weight',
  'grade',
  'packing',
  'origin',
  'quantity',
  'cost',
  'margin',
  'freight',
  'price',
]

export const FINANCIAL_TERMS = [
  'Due on receipt',
  '1% 10 Net 30',
  '2% 10 Net 30',
  'Net 7',
  'Net 8',
  'Net 10',
  'Net 14',
  'Net 15',
  'Net 21',
  'Net 30',
  'Net 45',
  'Net 60',
  'PACA Prompt 10 Days',
]

export const TAX_STATUSES = [
  {
    label: 'Not taxable',
    value: false
  },
  {
    label: 'Taxable',
    value: true
  }
]

export const DATE_FORMAT = 'MM/DD/YYYY'

export const NULL_PLACEHOLDER = '-'

// todo: update cache key name
export const CACHED_AUTH_TOKEN = 'session'
export const CACHED_USER_ID = 'id'
export const CACHED_COMPANY = 'company'
export const CACHED_ACCOUNT_TYPE = 'accountType'
export const CACHED_FULLNAME = 'fullName'
export const CACHED_ACCESSTOKEN = 'accessToken'
export const CACHED_IS_AGREED = 'isAgreed'
export const CACHED_QBO_LINKED = 'qboLinked'
export const CACHED_NS_LINKED = 'nsLinked'
