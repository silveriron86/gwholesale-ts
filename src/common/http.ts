import 'whatwg-fetch'
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch'

import { Observable, Observer } from 'rxjs'
import { notification } from 'antd'

import { CACHED_ACCESSTOKEN } from './const'
import { Injectable } from 'redux-epics-decorator'
import { isPlainObject } from 'lodash'
import { stringify } from 'qs'

interface RequestOption extends RequestInit {
  query?: Object
}

type RequestMethods = 'GET' | 'POST' | 'DELETE' | 'PUT'

@Injectable()
export class Http {
  get = this.create('GET')

  put = this.create('PUT')

  post = this.create('POST')

  delete = this.create('DELETE')

  get defaultHeaders() {
    return new Headers({
      'Content-Type': 'application/json',
      // auto read cache auth token for each request
      Authorization: `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN)}`,
    })
  }

  prefix = process.env.WSW2_HOST + '/api/v1/wholesale'

  private request<T>(url: string, options: RequestInit): Observable<T> {
    const headers = new Headers(options.headers)
    if (this.defaultHeaders && !options.headers) {
      for (const [headerName, value] of this.defaultHeaders.entries()) {
        headers.set(headerName, value)
      }
    }

    return Observable.create((observer: Observer<T>) => {
      const controller = new AbortController()
      fetch(url, {
        method: 'GET',
        // credentials: 'include',
        ...options,
        headers,
        body: typeof options.body === 'string' ? options.body : JSON.stringify(options.body),
        signal: controller.signal,
      })
        .then((response) => response.text())
        .then((responseString) => {
          if (
            responseString != null &&
            (responseString.indexOf('Session not found') > -1 ||
              responseString.indexOf('Invalid permission') > -1 ||
              responseString.indexOf('Session Expired') > -1)
          ) {
            throw new Error('login error')
          } else if (responseString.indexOf('invalid_token') > -1) {
            throw new Error('invalid_token')
          }
          try {
            return JSON.parse(responseString)
          } catch {
            return responseString
          }
        })
        .then((result: T) => {
          observer.next(result)
          observer.complete()
        })
        .catch((error) => {
          if (error.message === 'invalid_token') {
            localStorage.clear()
            if (window.location.href.includes('/login')) {
              notification.error({
                message: 'ERROR',
                description: 'Incorrect email or password',
              })
              return
            }
            window.location.href = '/#/login'
            window.location.reload()
            return
          }
          this.logHttpError(error, url, options.body)
          observer.error(error)
        })
      if (url.indexOf('?noCancel') < 0) {
        return () => controller.abort()
      }
    })
  }

  private create(method: RequestMethods) {
    return <T>(path: string, options: RequestOption = {}): Observable<T> => {
      const { query, ...restOptions } = options
      let url = query ? `${this.prefix}${path}?${stringify(query)}` : this.prefix + path
      if (path == '/oauth/token') {
        url = process.env.GATEWAY_HOST + "/wsw-auth" + path
      } else if (path == '/api/user/password' || path.includes('/api/auth/reset-password')) {
        url = process.env.GATEWAY_HOST + "/wsw-hr" + path
      }else if(path.indexOf('/container') == 0 || path.indexOf('/tms-flow-item') == 0
        || path.indexOf('/logistic-order') == 0){
        path = query ? `${path}?${stringify(query)}`: path
        url = process.env.GATEWAY_HOST + "/wsw-wsw2/api" + path
      }
      return this.request<T>(url, { ...restOptions, method })
    }
  }

  private logHttpError(error: any, requestURL: string | undefined, body: any) {
    if (error.response && error.response.status) {
      const { status } = error.response
      const query = Object.create(null)
      if (requestURL) {
        query.requestUrl = requestURL
      }
      if (isPlainObject(body)) {
        query.body = body
      }
      if (status === 401) {
        console.info(error, query)
      } else if (status < 500) {
        console.error(error, query)
      } else if (status >= 500) {
        console.error(error, query)
      }
    }
  }
}
