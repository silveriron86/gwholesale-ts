export * from './color'
export * from './const'
export * from './http'

import Storage from './storage'

export const local = new Storage({ storage: localStorage })
export const session = new Storage({ storage: sessionStorage })
