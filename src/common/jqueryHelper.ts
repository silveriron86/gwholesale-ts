import jQuery from 'jquery'
import { first } from 'rxjs/operators'

let whichAddressButton: string | null = null

export const documentBody = jQuery(document.body)

export const documentBodyKeyupEvent = (targetClass: string) => {
  return jQuery(document.body).unbind('keyup').bind('keyup', function (e: any) {
    if (e.keyCode == 27) {
      jQuery('.ant-popover').addClass('ant-popover-hidden')
      if (whichAddressButton !== null) {
        jQuery(`.${whichAddressButton}`).trigger('focus')
        jQuery(`.${whichAddressButton}`).trigger('click')
      }
    }
  })
}

export const datePickerEscKeyupEvent = (callback: Function) => {
  return jQuery(document.body).unbind('keydown').bind('keydown', callback)
}

export const buttonWithPopover = (className: string) => {
  jQuery(`.${className}`).unbind('keyup').bind('keyup', (e: any) => {
    if (e.keyCode == 9) {
      if (jQuery(e.target).hasClass('ant-btn')) {
        jQuery(e.target).addClass('focused-btn');
      } else {
        jQuery(e.target).removeClass('focused-btn');
      }
    }
  });
}

export const onLoadFocusOnFirst = (className: string) => {

  jQuery(`.${className}`).ready(function () {
    setTimeout(function () {
      const childInputs = jQuery(`.${className}`).find('.ant-input, .ant-input-number-input, .ant-select.ant-select-enabled:not(.ant-select-auto-complete), .ant-btn, .ant-radio-input')
      if (childInputs.length > 0) {
        let firstEl = childInputs[0]
        jQuery(firstEl).trigger('focus')

        childInputs.map(function (index: number, el: any) {
          jQuery(el).keydown(function (e: any) {
            if (e.keyCode == 9) {
              if (e.shiftKey) {
                if (index > 0) {
                  if (jQuery(childInputs[index - 1]).prop('disabled')) {
                    return
                  }
                  if (jQuery(childInputs[index - 1]).hasClass('ant-select')) {
                    jQuery(childInputs[index - 1]).children()[0].focus()
                  } else {
                    jQuery(childInputs[index - 1]).focus()
                  }
                }
              } else {
                if (index < childInputs.length) {
                  if (jQuery(childInputs[index + 1]).prop('disabled')) {
                    return
                  }
                  if (jQuery(childInputs[index + 1]).hasClass('ant-select')) {
                    jQuery(childInputs[index + 1]).children()[0].focus()
                  } else {
                    jQuery(childInputs[index + 1]).focus()
                  }
                }
              }

              e.preventDefault()
            }
          })
        })
      }
    }, 100)
  })
}

export const onPopoverOpenWithButtons = (className: string) => {
  jQuery(`.${className}`).on('click', function () {
    setTimeout(function () {
      whichAddressButton = className
      let elements = jQuery('.ant-popover:not(.ant-popover-hidden)').find('.ant-btn, .ant-input')
      let firstEl = elements[0]

      jQuery(firstEl).trigger('focus')
    }, 500)

  })
}

export const onPopoverOpenWithSecondButton = (className: string) => {
  jQuery(`.${className}`).on('click', function () {
    setTimeout(function () {
      whichAddressButton = className
      let elements = jQuery('.ant-popover:not(.ant-popover-hidden)').find('.ant-btn, .ant-input')
      if (elements.length != 2) return
      let secondEl = elements[1]

      jQuery(secondEl).trigger('focus')
    }, 500)

  })
}

export const Level2WithTable = (className: string) => {
  jQuery(`.${className}`).ready(function () {
    setTimeout(function () {
      let tables = jQuery(`.${className}`).find('.ant-table')
      let isEditable = false;
      tables.map((index: number, el: any) => {
        let tabAbleCols = jQuery(el).find('.tab-able');
        if (tabAbleCols.length > 0) {
          isEditable = true
        }
      })

      if (isEditable) {
        handleEditableTable(className)
      } else {
        handleNonEditableTable(className)
      }

    }, 100)
  })
}

const handleNonEditableTable = (className: string) => {
  let childInputs = jQuery(`.${className}`).find('.ant-input, .ant-input-number-input, .ant-select.ant-select-enabled:not(.ant-select-auto-complete), .ant-btn, .ant-radio-input, .ant-pagination li')

  let paginators = jQuery(`.${className}`).find('.ant-pagination li')
  if (paginators.length == 3) {
    childInputs = childInputs.filter((index: number, el: any) => { return jQuery(el).prop('tagName') != 'LI' })
  }

  if (childInputs.length > 0) {
    let firstEl = childInputs[0]
    jQuery(firstEl).trigger('focus')

    childInputs.map(function (index: number, el: any) {
      jQuery(el).keydown(function (e: any) {
        if (e.keyCode == 9) {
          if (e.shiftKey) {

            if (index > 0) {
              if (jQuery(childInputs[index - 1]).prop('disabled') || jQuery(childInputs[index - 1]).hasClass('ant-pagination-disabled')) {
                return
              }

              if (jQuery(childInputs[index - 1]).hasClass('ant-select')) {
                jQuery(childInputs[index - 1]).children()[0].focus()
              } else {
                jQuery(childInputs[index - 1]).focus()
              }
            }
          } else {
            if (index < childInputs.length) {
              if (jQuery(childInputs[index + 1]).prop('disabled') || jQuery(childInputs[index + 1]).hasClass('ant-pagination-disabled')) {
                return
              }
              if (jQuery(childInputs[index + 1]).hasClass('ant-select')) {
                jQuery(childInputs[index + 1]).children()[0].focus()
              } else {
                jQuery(childInputs[index + 1]).focus()
              }
            }
          }

          e.preventDefault()
        }
      })
    })
  }
}

const handleEditableTable = (className: string) => {

}

export const Level3WithTable = (className: string) => {
  jQuery(`.${className}`).ready(function () {
    setTimeout(function () {
      let tables = jQuery(`.${className}`).find('.ant-table')
      let isEditable = false;
      tables.map((index: number, el: any) => {
        let tabAbleCols = jQuery(el).find('.tab-able');
        if (tabAbleCols.length > 0) {
          isEditable = true
        }
      })

      if (isEditable) {
        handleEditableTable(className)
      } else {
        handleNonEditableTableWithNonFocusable(className)
      }

    }, 100)
  })
}



let FOCUSED_INDEX = 0;
const handleNonEditableTableWithNonFocusable = (className: string) => {
  let childInputs = jQuery(`.${className}`).find('.ant-input, .ant-select.ant-select-enabled:not(.ant-select-auto-complete), .ant-btn, .ant-pagination li')

  let paginators = jQuery(`.${className}`).find('.ant-pagination li')
  if (paginators.length == 3) {
    childInputs = childInputs.filter((index: number, el: any) => { return jQuery(el).prop('tagName') != 'LI' })
  }

  if (childInputs.length > 0 && FOCUSED_INDEX < childInputs.length) {
    makeFocusEl(childInputs[FOCUSED_INDEX])
  }

  jQuery(document.body).unbind('keyup').bind('keyup', function (e: any) {
    if (e.keyCode == 13) {
      if (FOCUSED_INDEX == 1 && jQuery(childInputs[FOCUSED_INDEX]).hasClass('focused-calendar')) {
        jQuery(childInputs[FOCUSED_INDEX]).trigger('click')
      }
    }
    if (e.keyCode == 9) {
      if (FOCUSED_INDEX > 0) {
        jQuery(childInputs[1]).removeClass('focused-calendar')
      }
      if (e.shiftKey) {
        FOCUSED_INDEX > 0 ? FOCUSED_INDEX-- : FOCUSED_INDEX = childInputs.length - 1
      } else {
        FOCUSED_INDEX < childInputs.length - 1 ? FOCUSED_INDEX++ : FOCUSED_INDEX = 0
      }

      if (className == 'level-3-with-table') {
        makeFocusEl(childInputs[FOCUSED_INDEX])
      }
    }
  })
}

const makeFocusEl = (el: any) => {
  if (jQuery(el).hasClass('ant-calendar-picker-input')) {
    jQuery(el).addClass('focused-calendar')
    jQuery('.ant-btn').trigger('blur')  //exception for not updating of add itme button focused status when shift tab

    if (jQuery('.no-init-scroll').length === 0) {
      jQuery('html, body').animate({ scrollTop: 0 }, "quick")
    }
  } else {
    jQuery(el).trigger('focus')
  }
}

export const navigateSearchItems = (el: any, items: any[], callback: Function) => {
  if (el && items.length > 0) {
    let curIndex = 0
    jQuery(document.body).unbind('keydown').bind('keydown', function (e: any) {
      if (e.keyCode == 40) {
        if (curIndex < items.length - 1)
          curIndex++;
        else
          curIndex = 0

        callback(el, items, curIndex, 'move')
      } else if (e.keyCode == 38) {
        if (curIndex > 0)
          curIndex--;
        else
          curIndex = items.length - 1

        callback(el, items, curIndex, 'move')
      } else if (e.keyCode == 13) {
        callback(el, items, curIndex, 'select')
      }
    })
  } else {
    jQuery(document.body).unbind('keydown')
  }
}
