import { bignumber as bignumberMathjs, BigNumber, create, all } from 'mathjs'

// 参数为'' undefined时原始的bignumber会报错。 此处转换成0
export const bignumber = (x: number | string | undefined) => bignumberMathjs(x || 0)

export const Big = create(all, {
  number: 'BigNumber',
})

type FormatParams = {
  comma?: boolean // 逗号分隔符
  fixed?: number // 保留几位小数. 前端展示，默认保留2位
  $?: boolean // $前缀
  showPlus?: boolean // 展示加号
}

// https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
export function numberWithCommas(x: any) {
  const parts = x.toString().split('.')
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return parts.join('.')
}

export function formatBn(big: BigNumber | number | string | undefined, params?: FormatParams): string {
  if (typeof big === 'number' || typeof big === 'string' || typeof big === 'undefined') {
    big = bignumber(big)
  }
  const { comma = true, fixed = 2, $ = false, showPlus = false } = params || {}
  let res = big.abs().toFixed(fixed)
  if (comma) {
    res = numberWithCommas(res)
  }
  if ($) {
    res = '$' + res
  }
  if (big.lessThan(0)) {
    res = '-' + res
  } else if (showPlus) {
    res = '+' + res
  }
  return res
}

// // 转成bignumber
// let big = bignumber(0.1111)
// // 计算
// big = big.add(0.2222)  // add sub mul div 加减乘除
// // 转字符串
// const str = big.toString()
// // 转字符串: 保留2位小数
// const fixedStr = big.toFixed(2)
// // 转普通数字: 数字大于2^53时，转出来的数字会不准确。
// const num = big.toNumber()
// // 转 Decimal/bignumber:  保留2位小数
// const anotherNum = big.toDecimalPlaces(2)
// const num2 = anotherNum.toNumber()

// 复杂计算用BigNumber的evaluate
// let a = Big.evaluate('0.1+0.2')
// a = bignumber(a) // evaluate返回的不一定是Bignumber，可能是数字等，可以用bignumber转成Bignumber
