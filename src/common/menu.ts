import { CACHED_NS_LINKED, CACHED_QBO_LINKED } from './const'

/**
 * Each menu item should be formed below data structure
 *
 * label: string ( required )
 * default?: string (optional but required when children is undefined)
 * children? Array (optional)
 *    label: string (required)
 *    default?: string (required in chidren array)
 *    sub?: Array (optional)
 * sub?: Array (optional)
 * path?: string (optional but required when both of children and sub are undefined)
 *
 */

const customerSub = [
  {
    name: 'Orders',
    path: '/customer/:customerId/orders',
  },
  {
    name: 'Products',
    path: '/customer/:customerId/products',
  },
  {
    name: 'Price Sheets',
    path: '/customer/:customerId/price-sheets',
  },
  {
    name: 'Account Details',
    path: '/customer/:customerId/account',
  },
  {
    name: 'Contacts',
    path: '/customer/:customerId/contacts',
  },
  {
    name: 'Addresses',
    path: '/customer/:customerId/addresses',
  },
  // {
  //   name: 'Settings',
  //   path: '/customer/:customerId/settings',
  // },
]

// if (localStorage.getItem(CACHED_QBO_LINKED) != 'null') {
customerSub.push({
  name: 'Financial Details',
  path: '/customer/:customerId/financial-details',
})
// }
customerSub.push({
  name: 'Documents',
  path: '/customer/:customerId/documents',
})
customerSub.push({
  name: 'Chat',
  path: '/customer/:customerId/chat',
})

const salesOrderSub = [
  {
    name: 'Cart',
    path: '/order/:orderId/sales-cart',
  },
  {
    name: 'Checkout',
    path: '/order/:orderId/sales-checkout',
  },
  {
    name: 'Documents',
    path: '/order/:orderId/sales-documents',
  },
  // {
  //   name: 'Settings',
  //   path: '/order/:orderId/sales-settings',
  // },
  {
    name: 'Margins',
    path: '/order/:orderId/sales-margins',
  },
  // {
  //   name: 'Audit Log',
  //   path: '/order/:orderId/audit-log',
  // },
]
if (localStorage.getItem(CACHED_QBO_LINKED) != 'null') {
  salesOrderSub.push({
    name: 'Credit Memo',
    path: '/order/:orderId/credit-memo',
  })
}
const purchaseOrderSub = [
  {
    name: 'Purchase Order',
    path: '/order/:orderId/purchase-cart',
  },
  {
    name: 'Order Details',
    path: '/order/:orderId/purchase-details',
  },
  {
    name: 'Receiving',
    path: '/order/:orderId/purchase-receiving',
  },
  // {
  //   name: 'Related Orders',
  //   path: '/order/:orderId/purchase-related',
  // },
  {
    name: 'Documents',
    path: '/order/:orderId/purchase-documents',
  },
]
if (localStorage.getItem(CACHED_QBO_LINKED) != 'null') {
  // purchaseOrderSub.push({
  //   name: 'Credit Memo',
  //   path: '/order/:orderId/purchase-credit-memo',
  // })
}

const getCashSales = () => {
  if (localStorage.getItem('isEnableCashSales') === 'true')
    return {
      label: 'Cash Sales',
      default: '/cash-sales',
      icon: 'menu_sales_orders',
    }
  return null
}

const MENU = {
  admin: [
    // {
    //   label: 'Dashboard',
    //   default: '/dashboard-1',
    //   sub: [
    //     {
    //       name: 'Dashboard - 1',
    //       path: '/dashboard-1',
    //     },
    //     {
    //       name: 'Dashboard - 2',
    //       path: '/dashboard-2',
    //     },
    //   ],
    // },
    {
      label: 'Selling & Shipping',
      children: [
        {
          label: 'Customers',
          default: '/customers',
          icon: 'menu_customers',
          sub: customerSub,
        },
        {
          label: 'Sales Orders',
          default: '/sales-orders',
          icon: 'menu_sales_orders',
          sub: salesOrderSub,
        },
        {
          label: 'Sales Receipt',
          path: '/POS',
          default: '/POS',
          icon: 'menu_pricesheets',
        },
        {
          label: 'Price Sheets',
          path: '/prices',
          default: '/prices',
          icon: 'menu_customers',
        },
        // {
        //   label: 'Sales Report',
        //   default: '/sales-order-reports',
        // }
        {
          label: 'Fulfillment',
          default: '/delivery/deliveries',
          icon: 'menu_deliveries',
        },
        {
          label: 'Routes',
          default: '/delivery/routes',
          icon: 'menu_routes',
        },
        {
          label: 'Drivers',
          default: '/settings',
          icon: 'menu_drivers',
        },
        // {
        //   label: 'Banking Journal',
        //   default: '/delivery/driver-reports',
        //   icon: 'menu_banking'
        // },
      ].filter((menu) => !!menu),
    },
    {
      label: 'Inventory',
      children: [
        {
          label: 'Products',
          path: '/inventory',
          default: '/inventory',
          icon: 'menu_products',
        },
        {
          label: 'Restock',
          path: '/restock',
          default: '/restock',
          icon: 'menu_customers',
        },
        {
          label: 'Label',
          path: '/label',
          default: '/label',
          icon: 'menu_pricesheets',
        },
        {
          label: 'Warehouse Sync',
          path: '/location',
          default: '/location',
          icon: 'menu_sync',
        },
        {
          label: 'Pallet History',
          path: '/pallet-history',
          default: '/pallet-history',
          icon: 'menu_pallet_history',
        },
        {
          label: 'Work Orders',
          path: '/work-orders',
          default: '/work-orders',
          icon: 'menu_work_orders',
          sub: [
            {
              name: 'Overview',
              path: '/manufacturing/:orderId/wo-overview',
            },
            {
              name: 'Audit Log',
              path: '/manufacturing/:orderId/audit-log',
            },
            {
              name: 'Templates',
              path: '/manufacturing/:orderId/wo-templates',
            },
          ],
        },
        {
          label: 'Processing List',
          path: '/manufacturing-pl',
          default: '/manufacturing-pl',
          icon: 'menu_processing_list',
        },
      ],
    },
    {
      label: 'Purchasing',
      children: [
        {
          label: 'Vendors',
          default: '/vendors',
          icon: 'menu_products',
          sub: [
            {
              name: 'Orders',
              path: '/vendor/:vendorId/orders',
            },
            {
              name: 'Account Details',
              path: '/vendor/:vendorId/account',
            },
            {
              name: 'Product List',
              path: '/vendor/:vendorId/products',
            },
            {
              name: 'Contacts',
              path: '/vendor/:vendorId/contacts',
            },
            {
              name: 'Addresses',
              path: '/vendor/:vendorId/addresses',
            },
            {
              name: 'Financial Details',
              path: '/vendor/:vendorId/financial-details',
            },
            {
              name: 'Documents',
              path: '/vendor/:vendorId/documents',
            },
          ],
        },
        {
          label: 'Purchase Orders',
          default: '/purchase-orders',
          icon: 'menu_pricesheets',
          sub: [],
        },
        {
          label: 'Requisition Catalog',
          default: '/requisition',
          icon: 'menu_sync',
        },
        {
          label: '',
        },
        {
          label: 'Reports',
          default: '/reports',
          path: '/reports',
          icon: 'menu_reports',
        },
        {
          label: 'Statistics',
          default: '/statistics',
          path: '/statistics',
          icon: 'menu_reports',
        },
      ],
    },
  ],
  customer: [
    {
      label: 'Order History',
      default: '/sales-orders',
      sub: [
        {
          name: 'Order History',
          path: '/sales-orders',
        },
      ],
    },
    {
      label: 'Price Sheets',
      default: '/pricesheets',
      sub: [
        {
          name: 'Price Sheets',
          path: '/pricesheets',
        },
      ],
    },
  ],
  seller: [
    {
      label: 'Selling & Shipping',
      children: [
        {
          label: 'Sales Orders',
          default: '/sales-orders',
          icon: 'menu_sales_orders',
          sub: salesOrderSub,
        },
        {
          label: 'Fulfillment',
          default: '/delivery/deliveries',
          icon: 'menu_deliveries',
        },
      ],
    },
    {
      label: 'Inventory',
      children: [
        {
          label: 'Products',
          path: '/inventory',
          default: '/inventory',
          icon: 'menu_products',
        },
        {
          label: 'Work Orders',
          path: '/work-orders',
          default: '/work-orders',
          icon: 'menu_work_orders',
          sub: [
            {
              name: 'Overview',
              path: '/manufacturing/:orderId/wo-overview',
            },
            {
              name: 'Audit Log',
              path: '/manufacturing/:orderId/audit-log',
            },
            {
              name: 'Templates',
              path: '/manufacturing/:orderId/wo-templates',
            },
          ],
        },
      ],
    },
    {
      label: 'Purchasing',
      children: [
        {
          label: 'Purchase Orders',
          default: '/purchase-orders',
          icon: 'menu_pricesheets',
          sub: [],
        },
        {
          label: '',
        },
        {
          label: 'Reports',
          default: '/reports',
          path: '/reports',
          icon: 'menu_reports',
        },
      ],
    },
  ],
  buyer: [
    {
      label: 'Inventory',
      children: [
        {
          label: 'Products',
          path: '/inventory',
          default: '/inventory',
          icon: 'menu_products',
        },
      ],
    },
    {
      label: 'Purchasing',
      children: [
        {
          label: 'Vendors',
          default: '/vendors',
          icon: 'menu_products',
          sub: [
            {
              name: 'Orders',
              path: '/vendor/:vendorId/orders',
            },
            {
              name: 'Account Details',
              path: '/vendor/:vendorId/account',
            },
            {
              name: 'Contacts',
              path: '/vendor/:vendorId/contacts',
            },
            {
              name: 'Addresses',
              path: '/vendor/:vendorId/addresses',
            },
            {
              name: 'Financial Details',
              path: '/vendor/:vendorId/financial-details',
            },
            {
              name: 'Documents',
              path: '/vendor/:vendorId/documents',
            },
          ],
        },
        {
          label: 'Purchase Orders',
          default: '/purchase-orders',
          icon: 'menu_pricesheets',
          sub: [],
        },
      ],
    },
  ],
  warehouse: [
    {
      label: 'Selling & Shipping',
      children: [
        {
          label: 'Sales Orders',
          default: '/sales-orders',
          icon: 'menu_sales_orders',
          sub: salesOrderSub,
        },
        {
          label: 'Fulfillment',
          default: '/delivery/deliveries',
          icon: 'menu_deliveries',
        },
      ],
    },
    {
      label: 'Inventory',
      children: [
        {
          label: 'Products',
          path: '/inventory',
          default: '/inventory',
          icon: 'menu_products',
        },
        {
          label: 'Warehouse Sync',
          path: '/location',
          default: '/location',
          icon: 'menu_sync',
        },
        {
          label: 'Pallet History',
          path: '/pallet-history',
          default: '/pallet-history',
          icon: 'menu_pallet_history',
        },
      ],
    },
    {
      label: 'Purchasing',
      children: [
        {
          label: 'Purchase Orders',
          default: '/purchase-orders',
          icon: 'menu_pricesheets',
          sub: [],
        },
      ],
    },
  ],
  seller_restricted: [
    {
      label: 'Selling & Shipping',
      children: [
        {
          label: 'Sales Orders',
          default: '/sales-orders',
          icon: 'menu_sales_orders',
          sub: salesOrderSub,
        },
        {
          label: 'Fulfillment',
          default: '/delivery/deliveries',
          icon: 'menu_deliveries',
        },
      ],
    },
    {
      label: 'Inventory',
      children: [
        {
          label: 'Work Orders',
          path: '/work-orders',
          default: '/work-orders',
          icon: 'menu_work_orders',
          sub: [
            {
              name: 'Overview',
              path: '/manufacturing/:orderId/wo-overview',
            },
            {
              name: 'Audit Log',
              path: '/manufacturing/:orderId/audit-log',
            },
            {
              name: 'Templates',
              path: '/manufacturing/:orderId/wo-templates',
            },
          ],
        },
      ],
    },
    {
      label: 'Purchasing',
      children: [
        {
          label: 'Reports',
          default: '/reports',
          path: '/reports',
          icon: 'menu_reports',
        },
      ],
    },
  ],
}

export default MENU
