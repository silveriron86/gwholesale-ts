import { responseHandler } from './utils'

type Fn = (...args: any[]) => any

export function promisify(fn: Fn) {
  return function(...params: any[]) {
    return new Promise(function(resolve, reject) {
      fn(...params).subscribe({
        next(res: any) {
          try {
            responseHandler(res)
            if (res.statusCodeValue !== 200) {
              const err = {
                ...res,
                message: res?.body?.message || 'Request failed, please try again later',
              }
              throw err
            }
            resolve(res)
          } catch (error) {
            reject(error)
          }
        },
        error(err: unknown) {
          reject(err)
        },
      })
    })
  }
}
