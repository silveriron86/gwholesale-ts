import { mediumGrey2 } from './color'

export const font = {
  title: 24,
}

export const gap = 10

export const borderColor = mediumGrey2
