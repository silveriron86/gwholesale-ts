// @ts-ignore
import hexRgb from 'hex-rgb'
// import { push } from 'connected-react-router'
import { message, notification, Modal } from 'antd'
import { Http, CACHED_ACCESSTOKEN } from '~/common'
import { Base64 } from 'js-base64'
import moment from 'moment'
import jQuery from 'jquery'
import { ALLOCATION_TYPE, OrderItem, UserRole } from '~/schema'

import _ from 'lodash'

export function formatPrintOriginAddress(address: string) {
  if (!address) {
    return []
  }
  const index = address.indexOf(',')
  return [address.substring(0, index), address.substring(index + 1, address.length)]
}

export function validateEmail(email: string) {
  if (!email) return false
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

export function hasSpecialCharacter(str: string) {
  return /[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str)
}

export function checkError(error: any) {
  console.log('check error: ' + error.message)
  //hack
  if (error == 'CONFLICT') {
    throw error
  }
  if (error.message === 'login error' || error.message === 'Session Expired') {
    localStorage.clear()
    window.location.href = '/'
    window.location.reload()

    return true
    // return push('/login')
  } else {
    console.error('unhandled check error: ', error)
    return {
      type: 'error_message',
      payload: error,
      error: true,
    }
  }
}

//If frontend is deployed, The user's browser will not refresh automatically
//Use the apiVersion to determine whether the front-end page needs to be refreshed
export function refreshPage(data: any) {
  let apiVersion = window.localStorage.getItem('GRUBMARKET_API_VERSION')
  if (apiVersion != data.body.apiVersion) {
    window.localStorage.setItem('GRUBMARKET_API_VERSION', data.body.apiVersion)
    window.location.reload()
  }
}

export function responseHandler(data: any, alert: boolean = false) {
  if (!data) return
  if (data.body != null) {
    refreshPage(data)
  }

  if (alert) {
    sendMessage(data)
  }
  // If the responds data back
  // first check http status code
  // data.statusCodeValue
  // if !=200
  // throw new Error(data.statusCodeValue)
  // send message
  if (data.statusCodeValue != 200) {
    if (data.statusCodeValue == 409) {
      throw data.statusCode
    }

    //special handler for update purchase order item when poi already sold
    if (data.statusCodeValue == 304) {
      let secondsToGo = 5
      const modal = Modal.warning({
        width: 600,
        title: 'Warning',
        keyboard: false,
        content: `Your data has outdated. beacuse purchase order item have been either committed or sold in a sales order(s).We need to refresh to get the latest data. This behavior will be executed automatically after ${secondsToGo} seconds, or you can click the OK button yourself.`,
        onOk() {
          window.location.reload()
        },
        onCancel() {
          clearInterval(timer)
          clearTimeout(timeout)
          modal.destroy()
        },
      })
      const timer = setInterval(() => {
        secondsToGo -= 1
        modal.update({
          content: `Your data has outdated. beacuse purchase order item have been either committed or sold in a sales order(s).We need to refresh to get the latest data. This behavior will be executed automatically after ${secondsToGo} seconds, or you can click the OK button yourself.`,
        })
      }, 1000)
      const timeout = setTimeout(() => {
        clearInterval(timer)
        modal.destroy()
        window.location.reload()
      }, secondsToGo * 1000)
      return
    }
    //special handler for remove purchase order item
    if (data?.body?.message == 'Item cannot be removed') {
      notification.error({
        message: data.body.message,
        description: `Units of this item have been either committed or sold in a sales order(s). This item can no longer be removed from this purchase order.`,
        onClose: () => {},
      })
      return
      //special handler for cancel purchase order
    } else if (data?.body?.message == 'Purchase order cannot be canceled') {
      notification.error({
        message: data.body.message,
        description: `Items from this purchase order have been either committed or sold in a sales order(s). This purchase order can no longer be canceled. You may still remove individual items that have not been committed or sold. `,
        onClose: () => {},
      })
      return
    } else if (data?.body?.message == 'Purchase order cannot be back') {
      notification.error({
        message: data.body.message,
        description: `Items from this purchase order have been either committed or sold in a sales order(s). This purchase order can no back to confirmed and edit order `,
        onClose: () => {},
      })
      return
    }
    notification.error({
      message: 'ERROR',
      description: data?.body?.message || data?.statusCode,
      onClose: () => {},
    })
    // We need to prompt a 400 error to the user. This will block the front end
    // comment by crazyleega
    // if (data.statusCodeValue == 400)
    //   throw new Error(data.body.message)
  }
  return data
}

function sendEmailToTao(data: any) {
  return new Http().post<any>(`/inventory/sendQBOEmail`, {
    body: JSON.stringify(data),
  })
}

export function sendMessage(data: any) {
  // if (data.statusCodeValue == 200) {
  //   message.success(data.body.message)
  // } else {
  //   message.error(data.body.message)
  // }
  if (data.statusCodeValue == 200) {
    notification.success({
      message: 'SUCCESS',
      description: data.body.message,
      onClose: () => {},
      duration: 5,
    })
  } else {
    // notification.error({
    //   message: 'ERROR',
    //   description: data.body.message,
    //   onClose: () => {},
    // })
  }

  if (data.body != null && data.body.nsResponse != null) {
    if (data.body.nsResponse.indexOf('error') != -1) {
      notification.error({
        message: 'NS ERROR',
        description: 'NetSuite: ' + data.body.nsResponse,
        onClose: () => {},
      })
    }
  }
  if (data.body != null && data.body.qboResponse != null) {
    if (data.body.qboResponse.indexOf('Authentication Failed') != -1) {
      notification.error({
        message: 'ERROR',
        description: 'QuickBook: ' + data.body.qboResponse,
        onClose: () => {},
      })
    } else if (data.body.qboResponse.indexOf('Failed') != -1) {
      notification.error({
        message: 'ERROR',
        description: 'QuickBook: ' + data.body.qboResponse,
        onClose: () => {},
      })
    }
  }
  if (data.body != null && data.body.relatedBillError != null) {
    notification.error({
      message: 'ERROR',
      description: data.body.relatedBillError,
      onClose: () => {},
    })
  }
}

export function checkMessage(data: any, expected: string, expected2?: string) {
  if (!data.message) throw new Error('Unexpected Error')
  if (expected && expected2) {
    if (data.message && data.message !== expected && data.message !== expected2) throw data.message
  } else if (data.message && data.message !== expected) throw data.message
  return data
}

export const hex2rgba = (hex: string, alpha: number) => {
  const rgb = hexRgb(hex).join(',')
  return `rgba(${rgb},${alpha})`
}

export function tuple<T extends Array<keyof any>>(...args: T) {
  return args
}

export function formatNumber(num: number, len: number): string {
  if (!num && num !== 0) return formatNumber(0, len)

  return (typeof num === 'string' ? parseFloat(num) : num)
    .toFixed(len)
    .toString()
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

export function padString(num: number) {
  const str = num.toString()
  const pad = '0000'
  return pad.substring(0, pad.length - str.length) + str
}

export function generealPadString(num: number, padlen: number, padchar: string = '') {
  var pad_char = padchar ? padchar : '0'
  var pad = new Array(1 + padlen).join(pad_char)
  return (pad + num).slice(-pad.length)
}

export function formatAddress(item: any, multiLines: boolean = false) {
  if (!item) {
    return ''
  }
  const br = multiLines === true ? '\n' : ', '
  return `${item.department ? item.department + br : ''}${item.street1 ? item.street1.trim() + br : ''}${item.city}, ${
    item.state
  } ${item.zipcode}`
}

export function copyStyles(sourceDoc: any, targetDoc: any) {
  Array.from(sourceDoc.styleSheets).forEach((styleSheet: any) => {
    if (styleSheet.cssRules) {
      // true for inline styles
      const newStyleEl = sourceDoc.createElement('style')

      Array.from(styleSheet.cssRules).forEach((cssRule: any) => {
        newStyleEl.appendChild(sourceDoc.createTextNode(cssRule.cssText))
      })

      targetDoc.head.appendChild(newStyleEl)
    } else if (styleSheet.href) {
      // true for stylesheets loaded from a URL
      const newLinkEl = sourceDoc.createElement('link')

      newLinkEl.rel = 'stylesheet'
      newLinkEl.href = styleSheet.href
      targetDoc.head.appendChild(newLinkEl)
    }
  })
}

export function printWindow(contentId: string, callBack?: () => React.ReactInstance, layout: string = 'portrait') {
  var mywindow = window.open('', 'PRINT', 'height=1080')
  if (!mywindow) return
  mywindow!.document.write('<html><head>')
  mywindow!.document.write('</head><body >')
  mywindow!.document.write(document.getElementById(contentId)!.innerHTML)
  mywindow!.document.write('</body></html>')

  mywindow!.document.close() // necessary for IE >= 10
  copyStyles(document, mywindow!.document)
  var css = ''
  if (
    contentId == 'printPickSheetModal' ||
    contentId == 'printBillModal' ||
    contentId == 'printManifestModal' ||
    contentId == 'PrintInvoiceModal' ||
    contentId == 'PrintPreviewDiv'
  ) {
    css = `@media print{@page { size: ${layout}; margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-botto {position: fixed; bottom: 0; width: 90%}}`
  } else if (contentId.indexOf('PrintLabelsModal') === 0 || contentId.indexOf('PrintPalletLabelsModal') === 0) {
    css =
      '@media print{@page { size: 4in 3in; margin: 16px } hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  } else if (contentId.indexOf('Print34LabelsModal') === 0) {
    css =
      '@media print{@page { size: 4in 3in; margin: 16px } hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  } else if (contentId.indexOf('PrintLabels25X1') === 0) {
    css =
      '@media print{@page { size: 2.5in 1in; margin: 0px} hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  } else if (contentId == 'PrintSalesOrderPalletLabel') {
    css = '@media print{@page { size: Letter landscape; margin: 16px } .print-block {display: block !important;} }'
  } else if (contentId === 'PrintOrderSummaryModal') {
    css = `@media print{@page { margin: 14mm 4mm }}`
  } else if (contentId === 'SelectFreshPrintInvoiceModal') {
    css = `@media print {@page { margin: 0.19in .04in 0in .4in; }}`
  } else if (contentId.includes('PrintInvoiceModal')) {
    //Default for custom invoice templates - example: 'MyCustomTemplatePrintInvoiceModal'
    css = `@media print{@page { size: ${layout}; margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-bottom {position: fixed; bottom: 0; width: 90%}}`
  } else if (contentId.indexOf('PrintLabels6X4') === 0) {
    css = '@media print{@page { size: 6in 4in; margin: 0px } hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  }

  if (contentId === 'QuickPrintModal') {
    css = `@media print{@page { margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-botto {position: fixed; bottom: 0; width: 90%}}`
  }

  if (css != '') {
    var style = document.createElement('style')

    style.type = 'text/css'
    style.media = 'print'

    if (style.styleSheet) {
      style.styleSheet.cssText = css
    } else {
      style.appendChild(mywindow!.document.createTextNode(css))
    }

    mywindow!.document.head.appendChild(style)
  }
  if (callBack != null) callBack()
  mywindow!.focus() // necessary for IE >= 10*/

  mywindow!.print()
  mywindow!.onafterprint = function() {
    mywindow!.close()
  }
}

export function printWindowAsync(contentId: string, callBack?: () => React.ReactInstance, layout: string = 'portrait') {
  var mywindow = window.open('', 'PRINT', 'height=1080')
  if (!mywindow) return
  mywindow!.document.write('<html><head>')
  mywindow!.document.write('</head><body >')
  mywindow!.document.write(document.getElementById(contentId)!.innerHTML)
  mywindow!.document.write('</body></html>')

  mywindow!.document.close() // necessary for IE >= 10
  copyStyles(document, mywindow!.document)
  var css = ''
  if (
    contentId == 'printPickSheetModal' ||
    contentId == 'printBillModal' ||
    contentId == 'printManifestModal' ||
    contentId == 'PrintInvoiceModal' ||
    contentId == 'PrintPreviewDiv'
  ) {
    css = `@media print{@page { size: ${layout}; margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-botto {position: fixed; bottom: 0; width: 90%}}`
  } else if (contentId.indexOf('PrintLabelsModal') === 0) {
    css =
      '@media print{@page { size: 4in 3in; margin: 16px } hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  } else if (contentId.indexOf('PrintLabels4X3') === 0) {
    css =
      '@media print{@page { size: 2.5in 1.15in;} hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  } else if (contentId == 'PrintSalesOrderPalletLabel') {
    css = '@media print{@page { size: Letter landscape; margin: 16px } .print-block {display: block !important;} }'
  } else if (contentId === 'PrintOrderSummaryModal') {
    css = `@media print{@page { margin: 14mm 4mm }}`
  } else if (contentId === 'SelectFreshPrintInvoiceModal') {
    css = `@media print {@page { margin: 0.19in .04in 0in .4in;}}`
  } else if (contentId.includes('PrintInvoiceModal')) {
    //Default for custom invoice templates - example: 'MyCustomTemplatePrintInvoiceModal'
    css = `@media print{@page { size: ${layout}; margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-bottom {position: fixed; bottom: 0; width: 90%}}`
  }


  if (contentId === 'QuickPrintModal') {
    css = `@media print{@page { margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-botto {position: fixed; bottom: 0; width: 90%}}`
  }

  if (css != '') {
    var style = document.createElement('style')

    style.type = 'text/css'
    style.media = 'print'

    if (style.styleSheet) {
      style.styleSheet.cssText = css
    } else {
      style.appendChild(mywindow!.document.createTextNode(css))
    }

    mywindow!.document.head.appendChild(style)
  }
  if (callBack != null) callBack()
  mywindow!.focus() // necessary for IE >= 10*/

  setTimeout(() => {
    mywindow!.print()
    mywindow!.onafterprint = function() {
      mywindow!.close()
    }
  }, 1000)
}

export function printQuickPrintAsync(contentId: string, timer: number, callBack?: () => React.ReactInstance, layout: string = 'portrait') {
  var mywindow = window.open('', 'PRINT', 'height=1080')
  if (!mywindow) return
  mywindow!.document.write('<html><head>')
  mywindow!.document.write('</head><body >')
  mywindow!.document.write(document.getElementById(contentId)!.innerHTML)
  mywindow!.document.write('</body></html>')

  mywindow!.document.close() // necessary for IE >= 10
  copyStyles(document, mywindow!.document)
  var css = ''
  if (
    contentId == 'printPickSheetModal' ||
    contentId == 'printBillModal' ||
    contentId == 'printManifestModal' ||
    contentId == 'PrintInvoiceModal' ||
    contentId == 'PrintPreviewDiv'
  ) {
    css = `@media print{@page { size: ${layout}; margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-botto {position: fixed; bottom: 0; width: 90%}}`
  } else if (contentId.indexOf('PrintLabelsModal') === 0) {
    css =
      '@media print{@page { size: 4in 3in; margin: 16px } hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  } else if (contentId.indexOf('PrintLabels4X3') === 0) {
    css =
      '@media print{@page { size: 2.5in 1.15in;} hr {display: none} .print-full {width: 100% !important} .print-block {display: block !important;} }'
  } else if (contentId == 'PrintSalesOrderPalletLabel') {
    css = '@media print{@page { size: Letter landscape; margin: 16px } .print-block {display: block !important;} }'
  } else if (contentId === 'PrintOrderSummaryModal') {
    css = `@media print{@page { margin: 14mm 4mm }}`
  } else if (contentId === 'SelectFreshPrintInvoiceModal') {
    css = `@media print {@page { margin: 0.19in .04in 0in .4in;}}`
  } else if (contentId.includes('PrintInvoiceModal')) {
    //Default for custom invoice templates - example: 'MyCustomTemplatePrintInvoiceModal'
    css = `@media print{@page { size: ${layout}; margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-bottom {position: fixed; bottom: 0; width: 90%}}`
  }

  if (contentId === 'QuickPrintModal') {
    css = `@media print{@page { margin: 14mm 4mm } .print-modal .ant-table {overflow-x: 'visible'} .floating-to-botto {position: fixed; bottom: 0; width: 90%}}`
  }

  if (css != '') {
    var style = document.createElement('style')

    style.type = 'text/css'
    style.media = 'print'

    if (style.styleSheet) {
      style.styleSheet.cssText = css
    } else {
      style.appendChild(mywindow!.document.createTextNode(css))
    }

    mywindow!.document.head.appendChild(style)
  }
  if (callBack != null) callBack()
  mywindow!.focus() // necessary for IE >= 10*/

  setTimeout(() => {
    mywindow!.print()
    mywindow!.onafterprint = function() {
      mywindow!.close()
    }
  }, timer*1000)
}

export function getRoutePath() {
  const url = location.href
  const cases = [
    {
      prefix: '/customer/',
      id: 'customerId',
    },
    {
      prefix: '/vendor/',
      id: 'vendorId',
    },
    {
      prefix: '/order/',
      id: 'orderId',
    },
    {
      prefix: '/product/',
      id: 'id',
    },
    {
      prefix: '/pricesheet/',
      id: 'id',
    },
    {
      prefix: '/price/',
      id: 'id',
    },
    {
      prefix: '/manufacturing/',
      id: 'orderId',
    },
  ]

  let ret = { path: url, id: '', param: '' }

  cases.forEach((item: any) => {
    let path = url
    const start = path.indexOf(item.prefix)
    let id = ''
    const param = `:${item.id}`
    if (start >= 0) {
      const end = path.indexOf('/', start + item.prefix.length)
      if (end >= 0) {
        id = path.substring(start + item.prefix.length, end)
        path = path.replace(`/${id}/`, `/${param}/`)
      } else {
        id = path.substring(start + item.prefix.length)
        path = path.replace(`/${id}`, `/${param}`)
      }

      ret = { path: path, id: id, param: param }
    }
  })

  return ret
}

export const isArrayEqual = (x: Array<any>, y: Array<any>) => {
  return _(x)
    .differenceWith(y, _.isEqual)
    .isEmpty()
}

export const b64EncodeUnicode = (str: any) => {
  return btoa(
    encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1: String) {
      return String.fromCharCode(`0x${p1}`)
    }),
  )
}

export const getFileUrl = (url: string, isPublic: boolean) => {
  if (url == null) return ''
  if (url.indexOf('http') > -1) {
    return url
  } else {
    if (isPublic) {
      return `${process.env['AWS_PUBLIC']}/${url}`
    } else {
      return `${process.env['AWS_PRIVATE']}/${Base64.encode(localStorage.getItem(CACHED_ACCESSTOKEN) as string)}/${url}`
    }
  }
}

export const formatDate = (value: string | number, isSimple:boolean = false) => {
  return moment.utc(value).format(isSimple ? 'M/D/YY' : 'MM/DD/YYYY')
}

export const combineUomForInventory = (value: number, ratio: number, inventoryUom: string, subUom: string) => {
  let decimalPlace = value % 1
  let deviation = 0.05 //If the error is within 0.05, directly round up
  // console.log(decimalPlace)
  if (decimalPlace > 0 && 1 - decimalPlace > deviation) {
    let rate = 1 / ratio
    let count = decimalPlace / rate
    if (1 - (count % 1) < deviation) {
      count = count.toFixed(0)
    }
    let integerNum = value | 0
    return `${formatNumber(integerNum, 0)} ${inventoryUom} ${count | 0} ${subUom}`
  } else {
    value = value.toFixed(0)
    return `${formatNumber(value, 0)} ${inventoryUom} 0 ${subUom}`
  }
}

export const combineUom = (value: number, currentUom: string, wholesaleItem: any, productUom: any) => {
  if (wholesaleItem.constantRatio && value > 0) {
    if (productUom != null) {
      if (currentUom == wholesaleItem.baseUOM) {
        value = value / wholesaleItem.ratioUOM
        return combineUomForInventory(value, productUom.ratio, wholesaleItem.inventoryUOM, productUom.name)
      } else if (currentUom == wholesaleItem.inventoryUOM) {
        return combineUomForInventory(value, productUom.ratio, wholesaleItem.inventoryUOM, productUom.name)
      } else if (productUom != null && currentUom == productUom.name) {
        value = value / productUom.ratio
        return combineUomForInventory(value, productUom.ratio, wholesaleItem.inventoryUOM, productUom.name)
      } else {
        return `${formatNumber(value, 0)} ${wholesaleItem.inventoryUOM}`
      }
    } else {
      return `${formatNumber(value, 0)} ${wholesaleItem.inventoryUOM}`
    }
  } else {
    return `${value} ${wholesaleItem.inventoryUOM}`
  }
}

export const getLogicAndGroup = (logic: string, group?: string) => {
  let defaultLogic = 'FOLLOW_DEFAULT_SALES',
    defaultGroup = 'A'
  switch (logic) {
    case 'GROUP_A': {
      defaultLogic = 'FOLLOW_GROUP'
      defaultGroup = 'A'
      break
    }
    case 'GROUP_B': {
      defaultLogic = 'FOLLOW_GROUP'
      defaultGroup = 'B'
      break
    }
    case 'GROUP_C': {
      defaultLogic = 'FOLLOW_GROUP'
      defaultGroup = 'C'
      break
    }
    case 'GROUP_D': {
      defaultLogic = 'FOLLOW_GROUP'
      defaultGroup = 'D'
      break
    }
    case 'GROUP_E': {
      defaultLogic = 'FOLLOW_GROUP'
      defaultGroup = 'E'
      break
    }
    default: {
      defaultLogic = logic
      defaultGroup = group ? group : 'A'
    }
  }
  return { defaultLogic, defaultGroup }
}

export const formateGroupLogic = (logic: string, group?: string) => {
  if (logic == 'FOLLOW_GROUP') {
    switch (group) {
      case 'A': {
        logic = 'GROUP_A'
        break
      }
      case 'B': {
        logic = 'GROUP_B'
        break
      }
      case 'C': {
        logic = 'GROUP_C'
        break
      }
      case 'D': {
        logic = 'GROUP_D'
        break
      }
      case 'E': {
        logic = 'GROUP_E'
        break
      }
    }
  }

  return logic
}

export const queryParams = (data: any) => {
  var array = []
  for (var key in data) {
    if (typeof data[key] !== 'undefined' && data[key] !== null) {
      array.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    }
  }
  return array.join('&')
}

export const basePriceToRatioPrice = (
  currentUom: string,
  price: number,
  item: any,
  digit?: number,
  useFactor?: boolean,
) => {
  if (currentUom == item.inventoryUOM) {
    price = price
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          if (useFactor) {
            price = (price / uom.ratio) * (1 + uom.priceFactor / 100)
          } else {
            price = price / uom.ratio
          }
          break
        }
      }
    }
  }
  price = typeof price === 'string' ? parseFloat(price) : price
  if (digit) {
    return mathRoundFun(price, digit)
  }
  return mathRoundFun(price, 2)
}

export const multiplyUomPriceFactor = (currentUom: string, price: number, item: any, digit?: number) => {
  if (currentUom == item.inventoryUOM) {
    price = price
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          price = price * (1 + uom.priceFactor / 100)
          break
        }
      }
    }
  }
  price = typeof price === 'string' ? parseFloat(price) : price
  if (digit) {
    return mathRoundFun(price, digit)
  }
  return mathRoundFun(price, 2)
}

export const ratioPriceToBasePrice = (currentUom: string, price: number, item: any, digit?: number) => {
  if (currentUom == item.inventoryUOM) {
    price = price
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          price = price * uom.ratio
          break
        }
      }
    }
  }
  price = typeof price === 'string' ? parseFloat(price) : price
  if (digit) {
    return mathRoundFun(price, digit)
  }
  return mathRoundFun(price, 2)
}

export const baseQtyToRatioQty = (currentUom: string, qty: number, item: any, digit?: number) => {
  if (currentUom == item.inventoryUOM) {
    qty = qty
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          qty = qty * uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const ratioQtyToBaseQty = (currentUom: string, qty: number, item: any, digit?: number) => {
  if (currentUom == item.inventoryUOM) {
    qty = qty
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          qty = qty / uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const inventoryQtyToRatioQty = (currentUom: string, qty: number, item: any, digit?: number) => {
  if (currentUom == item.inventoryUOM) {
    qty = qty
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          qty = qty * uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const ratioQtyToInventoryQty = (currentUom: string, qty: number, item: any, digit?: number) => {
  if (currentUom == item.inventoryUOM) {
    qty = qty
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          qty = qty / uom.ratio
          break
        }
      }
    }
  }
  qty = typeof qty === 'string' ? parseFloat(qty) : qty
  if (digit) {
    return mathRoundFun(qty, digit)
  }
  return mathRoundFun(qty, 2)
}

export const inventoryPriceToRatioPrice = (currentUom: string, price: number, item: any, digit?: number) => {
  if (currentUom == item.inventoryUOM) {
    price = price
  } else {
    if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
      for (const uom of item.wholesaleProductUomList) {
        if (currentUom == uom.name) {
          price = price / uom.ratio
          break
        }
      }
    }
  }
  price = typeof price === 'string' ? parseFloat(price) : price
  if (digit) {
    return mathRoundFun(price, digit)
  }
  return mathRoundFun(price, 2)
}

export const mathRoundFun = (value: number, decimal: number) => {
  return Math.round(value * Math.pow(10, decimal)) / Math.pow(10, decimal)
}

export const round = (value: number, decimal = 2): string => {
  if (typeof value !== 'number') {
    return value
  }
  return value.toLocaleString('en-US', {
    maximumFractionDigits: decimal,
    minimumFractionDigits: decimal,
  })
}

export const formatOrderItem = (item: any) => {
  return {
    wholesaleItemId: item.itemId,
    wholesaleOrderItemId: item.wholesaleOrderItemId,
    quantity: item.quantity ? item.quantity : 0,
    picked: item.picked,
    cost: item.cost,
    price: item.price,
    margin: item.margin,
    freight: item.freight,
    status: item.status,
    UOM: item.UOM,
    lotId: item.lotId,
    pricingUOM: item.pricingUOM,
  }
}

export const getFulfillmentDate = (sellerSetting: any) => {
  let fulfillmentDate = moment().format('MM/DD/YYYY')
  if (sellerSetting && sellerSetting.company) {
    if (sellerSetting.company.defaultLastUsedDateEnabled === true) {
      const lastUsedFulfillmentDate = localStorage.getItem('LAST_USED_TARGET_FULFILLMENT_DATE')
      if (lastUsedFulfillmentDate) {
        return lastUsedFulfillmentDate
      }
    }

    if (sellerSetting.company.targetFulfillmentDate == 'Tomorrow') {
      fulfillmentDate = moment()
        .add(1, 'days')
        .format('MM/DD/YYYY')
    } else if (
      sellerSetting.company.targetFulfillmentDate == 'Today with Cut-off Time' &&
      sellerSetting.company.targetFulfillmentDate !== null
    ) {
      const isPassed = moment().isAfter(moment(sellerSetting.company.cutOffTime, 'hh:mm:a'))
      console.log('cut off time is ', sellerSetting.company.cutOffTime, ' - is passed ? ', isPassed)
      if (isPassed) {
        fulfillmentDate = moment()
          .add(1, 'days')
          .format('MM/DD/YYYY')
      } else {
        fulfillmentDate = moment().format('MM/DD/YYYY')
      }
    } else {
      fulfillmentDate = moment().format('MM/DD/YYYY')
    }

    if (sellerSetting.company.defaultLastUsedDateEnabled === true) {
      localStorage.setItem('LAST_USED_TARGET_FULFILLMENT_DATE', fulfillmentDate)
    }
  }
  // console.log('check fulfillment date ', fulfillmentDate)
  return fulfillmentDate
}

export const calPoTotalCost = (orderItems: OrderItem[],orderCart?: boolean) => {
  let totalUnit = 0
  let subTotal = 0
  let unitUOM: string
  if (orderItems && orderItems.length > 0) {
    for (let i = 0; i < orderItems.length; i++) {
      let ratioQuantity = 0,
        item
      if (orderItems[i].wholesaleItem) {
        item = orderItems[i].wholesaleItem
        unitUOM = orderItems[i].overrideUOM ? orderItems[i].overrideUOM : item.inventoryUOM
      } else {
        item = orderItems[i]
        unitUOM = orderItems[i].overrideUOM ? orderItems[i].overrideUOM : orderItems[i].inventoryUOM
      }

      if(item.status == 'RECEIVED'){
        if (!judgeConstantRatio(item)) {
          subTotal += Math.round(item.orderWeight * item.cost * 100) / 100
        } else {
          subTotal += Math.round(item.receivedQty * item.cost * 100) / 100
        }
      }

      // if (unitUOM == item.inventoryUOM) {
      //   ratioQuantity = orderItems[i].receivedQty
      // } else {
      ratioQuantity = inventoryQtyToRatioQty(unitUOM, orderItems[i].receivedQty || 0, item)
      // }
      totalUnit += ratioQuantity
    }
  }

  return {
    totalUnit,
    subTotal,
  }
}

export const getOrderPrefix = (setting: any, type: string) => {
  if (setting && setting.company) {
    if (type == 'sales') {
      return setting.company.salesPrefix ? setting.company.salesPrefix : ''
    } else {
      return setting.company.purchasePrefix ? setting.company.purchasePrefix : ''
    }
  } else {
    return ''
  }
}

export const isCustomOrderNumberEnabled = (setting: any) => {
  if (setting && setting.company) {
    return setting.company.visibleCustomOrderNo
  } else {
    return false
  }
}

export const formatOrderStatus = (status: string, sellerSetting: any) => {
  if (!status) return ''
  if (status === 'CANCEL') {
    return 'CANCELED'
  } else if (status === 'SHIPPED' && sellerSetting && sellerSetting.company && sellerSetting.company.customShippedLabel) {
    return sellerSetting.company.customShippedLabel
  } else if (status.toUpperCase() === 'PICKING' && sellerSetting && sellerSetting.company && sellerSetting.company.isDisablePickingStep) {
    return 'NEW'
  } else {
    return status
  }
}

export const formatItemDescription = (
  itemName: string,
  sku: string,
  sellerSetting: any,
  customerProductCode: string = '',
) => {
  let customCodes = ''
  if (customerProductCode && customerProductCode.trim()) {
    let productCodes = customerProductCode.split(',')
    productCodes = productCodes.map((el) => el.trim())
    customCodes = productCodes.join('-') + '-'
  }
  if (!sellerSetting || !sellerSetting.company) return customCodes + itemName
  if (sellerSetting.company.itemDescriptionFormat === 'B') {
    return (
      customCodes +
      (typeof sku !== 'undefined' && sku !== 'undefined' && sku !== '' && sku !== null
        ? `${sku}-${itemName}`
        : itemName)
    )
  }
  return customCodes + itemName
}

export const initHighlightEvent = () => {
  setTimeout(() => {
    jQuery('.highlight-input input')
      .unbind()
      .bind('focus', (e: any) => {
        jQuery(e.target).select()
      })
  }, 100)
}

export const calculateTotalOrder = (
  orderItemByProduct: any[],
  oneOffItems: any[],
  catchWeightValues: any[],
  digit?: number,
) => {
  const printDeliveryItems = Object.values(orderItemByProduct).filter((el) => {
    if (el.quantity > 0) {
      // Order Qty > 0
      return true
    }

    let pickedQty = 0
    if (!judgeConstantRatio(el)) {
      const recordWeights = catchWeightValues[el.wholesaleOrderItemId]
      pickedQty = recordWeights ? recordWeights.length : 0
    } else {
      pickedQty = el.picked
    }

    if (pickedQty > 0) {
      // Picked Qty > 0
      return true
    }
  })

  const items = printDeliveryItems.concat(oneOffItems)
  let _totalQuantity = 0
  let _totalAmount = 0
  let _totalWeight = 0

  for (let i = 0; i < items.length; i++) {
    let amount = 0
    let constantRatio = judgeConstantRatio(items[i])
    _totalQuantity += _.toNumber(items[i].picked)

    let pricingUOM = items[i].pricingUOM ? items[i].pricingUOM : items[i].UOM
    let unitUOM = items[i].UOM
    let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(items[i].price), items[i], digit || 2)

    if (items[i].oldCatchWeight) {
      _totalWeight += _.toNumber(items[i].catchWeightQty)
      amount = numberMultipy(_.toNumber(items[i].price), _.toNumber(items[i].catchWeightQty))
    } else {
      if (!constantRatio) {
        _totalWeight += inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i])
        amount = numberMultipy(
          ratioPrice,
          inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i], 12),
        )
      } else {
        const quantity = inventoryQtyToRatioQty(
          pricingUOM,
          ratioQtyToInventoryQty(unitUOM, _.toNumber(items[i].picked), items[i], 12),
          items[i],
          2,
        )
        _totalWeight += quantity
        amount = numberMultipy(ratioPrice, quantity)
      }
    }
    amount = Math.round(amount * 100) / 100
    _totalAmount += amount
  }

  return _totalAmount === 0 ? '0.00' : formatNumber(_totalAmount, 2)
}

export const calculateTotalOrderTax = (orderItemByProduct: any[], oneOffItems: any[], catchWeightValues: any[]) => {
  const printDeliveryItems = Object.values(orderItemByProduct).filter((el) => {
    if (el.quantity > 0) {
      // Order Qty > 0
      return true
    }

    let pickedQty = 0
    if (!judgeConstantRatio(el)) {
      const recordWeights = catchWeightValues[el.wholesaleOrderItemId]
      pickedQty = recordWeights ? recordWeights.length : 0
    } else {
      pickedQty = el.picked
    }

    if (pickedQty > 0) {
      // Picked Qty > 0
      return true
    }
  })

  const items = printDeliveryItems.concat(oneOffItems)
  let _totalTax = 0

  for (let i = 0; i < items.length; i++) {
    let amount = 0
    let tax = 0
    let taxRate = _.toNumber(items[i].taxRate ? items[i].taxRate : 0)
    let taxRateRounded = mathRoundFun(taxRate / 100, 6)
    let taxEnabled = items[i].taxEnabled
    let constantRatio = judgeConstantRatio(items[i])

    if (!taxEnabled) {
      continue
    }

    let pricingUOM = items[i].pricingUOM ? items[i].pricingUOM : items[i].UOM
    let unitUOM = items[i].UOM
    let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(items[i].price), items[i], 12)

    if (items[i].oldCatchWeight) {
      amount = numberMultipy(_.toNumber(items[i].price), _.toNumber(items[i].catchWeightQty))
    } else {
      if (!constantRatio) {
        amount = numberMultipy(
          ratioPrice,
          inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i], 12),
        )
      } else {
        const quantity = inventoryQtyToRatioQty(
          pricingUOM,
          ratioQtyToInventoryQty(unitUOM, _.toNumber(items[i].picked), items[i], 12),
          items[i],
          12,
        )
        amount = numberMultipy(ratioPrice, quantity)
      }
    }
    tax = amount * taxRateRounded
    _totalTax += tax
  }

  return _totalTax === 0 ? '0.00' : formatNumber(_totalTax, 2)
}

export const notify = (type: string, message: string, desc: string) => {
  notification[type]({
    message: message,
    description: desc,
    duration: type === 'success' ? 5 : 4.5,
  })
}

export const formatPriceSheetItem = (priceSheetItems: any) => {
  //Because of the addition of flexible uom, the backend return needs to be changed.
  //So avoid more changes to the html, so the data remains the same as before when assembling
  return priceSheetItems.map((priceSheetItem: any) => {
    let item = priceSheetItem.wholesaleItem
    if (item != null) {
      priceSheetItem['itemId'] = item.wholesaleItemId
      priceSheetItem['provider'] = item.provider
      priceSheetItem['category'] = item.wholesaleCategory.name
      priceSheetItem['isListed'] = item.isListed
      priceSheetItem['size'] = item.size
      priceSheetItem['packing'] = item.packing
      priceSheetItem['SKU'] = item.sku
      priceSheetItem['origin'] = item.origin
      priceSheetItem['label'] = item.label
      priceSheetItem['weight'] = item.weight
      priceSheetItem['cost'] = item.cost
      priceSheetItem['availableQuantity'] = item.quantity
      priceSheetItem['grade'] = item.grade
      priceSheetItem['wholesaleCategory'] = item.wholesaleCategory
      priceSheetItem['baseUOM'] = item.baseUOM
      priceSheetItem['inventoryUOM'] = item.inventoryUOM
      priceSheetItem['wholesaleProductUomList'] = item.wholesaleProductUomList
      priceSheetItem['constantRatio'] = item.constantRatio
      priceSheetItem['ratioUOM'] = item.ratioUOM
      priceSheetItem['defaultPrice'] = item.price
      priceSheetItem['marginA'] = item.marginA
      priceSheetItem['descA'] = item.descA
      priceSheetItem['marginB'] = item.marginB
      priceSheetItem['descB'] = item.descB
      priceSheetItem['marginC'] = item.marginC
      priceSheetItem['descC'] = item.descC
      priceSheetItem['marginD'] = item.marginD
      priceSheetItem['descD'] = item.descD
      priceSheetItem['marginE'] = item.marginE
      priceSheetItem['descE'] = item.descE
      priceSheetItem['organic'] = item.isOrganic
      priceSheetItem['stockEnabled'] = item.stockEnabled
      priceSheetItem['variety'] = item.variety
      priceSheetItem['lastSoldChecked'] = item.lastSoldChecked
      priceSheetItem['groupChecked'] = item.groupChecked
      priceSheetItem['suppliers'] = item.suppliers
      priceSheetItem['priceGroupType'] = item.priceGroupType
      priceSheetItem['upc'] = item.upc
      delete priceSheetItem.wholesaleItem
    }
    return priceSheetItem
  })
}

export const numberMultipy = (arg1: number, arg2: number) => {
  var m = 0,
    s1 = arg1 != null ? arg1.toString() : '0',
    s2 = arg2 != null ? arg2.toString() : '0'
  try {
    m += s1.split('.')[1].length
  } catch (e) {}
  try {
    m += s2.split('.')[1].length
  } catch (e) {}
  return (Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) / Math.pow(10, m)
}

export const judgeConstantRatio = (orderItem: any) => {
  let unitUOM = orderItem?.overrideUOM || orderItem?.UOM
  let unitProductUOM = orderItem?.wholesaleProductUomList?.find((uom: any) => {
    return uom.name == unitUOM
  })

  let pricingUOM = orderItem?.pricingUOM || orderItem?.UOM
  let pricingProductUOM = orderItem?.wholesaleProductUomList?.find((uom: any) => {
    return uom.name == pricingUOM
  })

  const isUnitFixed = unitUOM === orderItem.inventoryUOM || unitProductUOM?.constantRatio
  const isPricingFixed = pricingUOM === orderItem.inventoryUOM || pricingProductUOM?.constantRatio
  if ((!isUnitFixed || !isPricingFixed) && unitUOM !== pricingUOM) {
    return false
  }
  return true
}

export const getString = (v: any) => {
  return v === null || v === 'null' ? '' : v
}

export const isSeller = (accountType: string) => {
  if (
    _.indexOf(
      [
        UserRole.SALES,
        UserRole.BUYER,
        UserRole.ADMIN,
        UserRole.SUPERADMIN,
        UserRole.WAREHOUSE,
        UserRole.SELLER_RESTRICTED,
      ],
      accountType,
    ) > -1
  ) {
    return true
  } else {
    return false
  }
}

export const formatTimeDifference = (refreshTime: Date) => {
  if (!refreshTime) return ''
  var today = new Date()
  var diff = today.getTime() - refreshTime.getTime() < 0 ? 0 : today.getTime() - refreshTime.getTime()
  var days = Math.floor(diff / 1000 / 60 / 60 / 24)
  diff -= days * 1000 * 60 * 60 * 24
  var hours = Math.floor(diff / 1000 / 60 / 60)
  diff -= hours * 1000 * 60 * 60
  var minutes = Math.floor(diff / 1000 / 60)
  diff -= minutes * 1000 * 60
  var seconds = Math.floor(diff / 1000)

  // If using time pickers with 24 hours format, add the below line get exact hours
  if (hours < 0) hours = hours + 24
  if (hours == 0 && minutes == 0 && seconds == 0) {
    seconds = 1 //display as 1 second when refreshTime has a value but all h/m/s are 0.
  }
  return `${days > 0 ? days + (days == 1 ? ' day, ' : ' days, ') : ''}
          ${hours > 0 ? hours + (hours == 1 ? ' hour, ' : ' hours, ') : ''}
          ${minutes > 0 ? minutes + (minutes == 1 ? ' minute' : ' minutes') : ''}
          ${seconds > 0 && minutes == 0 && hours == 0 ? seconds + (seconds == 1 ? ' second' : ' seconds') : ''}`
}

export const getDisMatchDuplicateOrderItems = (orderItems: any, from: number) => {
  return orderItems.filter((orderItem: any) => {
    // from 1: customer detail page,  2: order detail page
    if (from == 1) {
      let item = orderItem.wholesaleItem
      if (item != null) {
        if (!item.active) {
          return true
        }
        // uom is backend entity name, UOM is custom name
        else if (orderItem.uom != item.inventoryUOM) {
          let productUOMList = item.wholesaleProductUomList
          let unUseUOM = productUOMList.filter((uom: any) => {
            if (uom.deletedAt == null && uom.name == orderItem.UOM) {
              return true
            }
          })
          return unUseUOM.length > 0 ? false : true
        } else if (orderItem.pricingUOM != item.inventoryUOM) {
          let productUOMList = item.wholesaleProductUomList
          let existInFlexibleUOM = productUOMList.filter((uom: any) => {
            if (uom.deletedAt == null && uom.name == orderItem.pricingUOM) {
              return true
            }
          })
          return existInFlexibleUOM.length > 0 ? false : true
        }
      }
    } else if (from == 2) {
      const UOM = orderItem.overrideUOM ? orderItem.overrideUOM : orderItem.UOM
      if (!orderItem.active) {
        return true
      } else if (UOM != orderItem.inventoryUOM) {
        let productUOMList = orderItem.wholesaleProductUomList
        let existInFlexibleUOM = productUOMList.filter((uom: any) => {
          if (uom.deletedAt == null && uom.name == UOM) {
            return true
          }
        })
        return existInFlexibleUOM.length > 0 ? false : true
      } else if (orderItem.pricingUOM != orderItem.inventoryUOM) {
        let productUOMList = orderItem.wholesaleProductUomList
        let existInFlexibleUOM = productUOMList.filter((uom: any) => {
          if (uom.deletedAt == null && uom.name == orderItem.pricingUOM) {
            return true
          }
        })
        return existInFlexibleUOM.length > 0 ? false : true
      }
    }
  })
}

export const needShowDuplicateConfirmModal = (orderItems: any, from: number) => {
  let disMatchOrderItems = getDisMatchDuplicateOrderItems(orderItems, from)
  return disMatchOrderItems.length > 0 ? true : false
}

export const updateDisMatchDuplicateOrderItemsForGroup = (orderItems: any, from: number) => {
  let disMatchOrderItems = getDisMatchDuplicateOrderItems(orderItems, from)
  return disMatchOrderItems.map((orderItem: any) => {
    if (from == 1) {
      let item = orderItem.wholesaleItem
      if (item != null) {
        if (!item.active) {
          orderItem.duplicateType = 1
        } else if (orderItem.UOM != item.inventoryUOM) {
          let productUOMList = item.wholesaleProductUomList
          let existInFlexibleUOM = productUOMList.filter((uom: any) => {
            if (uom.deletedAt == null && orderItem.UOM == uom.name) {
              return true
            }
          })
          existInFlexibleUOM.length == 0 ? (orderItem.duplicateType = 2) : ''
        } else if (orderItem.pricingUOM != item.inventoryUOM) {
          let productUOMList = item.wholesaleProductUomList
          let existInFlexibleUOM = productUOMList.map((uom: any) => {
            if (uom.deletedAt == null && orderItem.pricingUOM == uom.name) {
              return true
            }
          })
          existInFlexibleUOM.length == 0 ? (orderItem.duplicateType = 3) : ''
        }
      }
    } else if (from == 2) {
      const UOM = orderItem.overrideUOM ? orderItem.overrideUOM : orderItem.UOM
      if (!orderItem.active) {
        orderItem.duplicateType = 1
      } else if (UOM != orderItem.inventoryUOM) {
        let productUOMList = orderItem.wholesaleProductUomList
        let existInFlexibleUOM = productUOMList.filter((uom: any) => {
          if (uom.deletedAt == null && UOM == uom.name) {
            return true
          }
        })
        existInFlexibleUOM.length == 0 ? (orderItem.duplicateType = 2) : ''
      } else if (orderItem.pricingUOM != orderItem.inventoryUOM) {
        let productUOMList = orderItem.wholesaleProductUomList
        let existInFlexibleUOM = productUOMList.map((uom: any) => {
          if (uom.deletedAt == null && orderItem.pricingUOM == uom.name) {
            return true
          }
        })
        existInFlexibleUOM.length == 0 ? (orderItem.duplicateType = 3) : ''
      }
    }
    return orderItem
  })
}

/**
 * function debounce
 * @param func
 * @param delay
 */
export const debounce = (func: Function, delay: number = 600) => {
  let timer: any
  return function(this: ThisType<any>) {
    if (timer != null) {
      clearTimeout(timer)
    }
    const arg = arguments
    timer = setTimeout(() => {
      func.apply(this, Array.from(arg))
    }, delay)
  }
}

export const handlerNoLotNumber = (items: any, flag?: number) => {
  let { availableToSell, onHandQty, commitedQty, noLotCommittedQty, noLotOnHandQty, noLotReturnQty } = items
  availableToSell = availableToSell === null ? 0 : availableToSell
  onHandQty = onHandQty === null ? 0 : onHandQty
  commitedQty = commitedQty === null ? 0 : commitedQty
  noLotCommittedQty = noLotCommittedQty === null ? 0 : noLotCommittedQty
  noLotOnHandQty = noLotOnHandQty === null ? 0 : noLotOnHandQty
  // undefined means notLotReturnQty too
  noLotReturnQty = noLotReturnQty === null || noLotReturnQty == undefined ? 0 : noLotReturnQty

  if (flag == 1) {
    return mathRoundFun(availableToSell - noLotCommittedQty - noLotOnHandQty + noLotReturnQty, 2)
  } else if (flag == 2) {
    return mathRoundFun(onHandQty - noLotOnHandQty + noLotReturnQty, 2)
  } else if (flag == 3) {
    return mathRoundFun(commitedQty + noLotCommittedQty, 2)
  }
}

export const getPriceGroupPriceString = (priceGroupType: number, price: number, desc: string) => {
  let groupPriceString = ''
  if (desc != '' && desc != null) {
    groupPriceString =
      priceGroupType == 1 ? `${desc}` : priceGroupType == 2 ? `${desc};${price}% margin` : `${desc};$${price} markup`
    return ` (${groupPriceString})`
  } else {
    groupPriceString = priceGroupType == 1 ? `` : priceGroupType == 2 ? `${price}% margin` : `$${price} markup`
    return groupPriceString ? ` (${groupPriceString})` : ''
  }
}

export const onSortString = (key: string, a: any, b: any) => {
  const stringA = a ? (a[key] ? a[key] : '') : ''
  const stringB = b ? (b[key] ? b[key] : '') : ''
  return stringA.localeCompare(stringB)
}

export const getFromAndToByDateType = (dateType: number) => {
  let from = null
  let to = null
  switch (dateType) {
    case 1:
      from = null
      to = null
      break
    case 2:
      from = moment().format('MM/DD/YYYY')
      to = moment().format('MM/DD/YYYY')
      break
    case 3:
      from = moment()
        .add(1, 'days')
        .format('MM/DD/YYYY')
      to = moment()
        .add(1, 'days')
        .format('MM/DD/YYYY')
      break
    case 4:
      from = moment()
        .subtract(1, 'days')
        .format('MM/DD/YYYY')
      to = moment()
        .subtract(1, 'days')
        .format('MM/DD/YYYY')
      break
    case 5:
      from = moment()
        .subtract(7, 'days')
        .format('MM/DD/YYYY')
      to = moment().format('MM/DD/YYYY')
      break
    default:
      from = null
      to = null
      break
  }

  return { from, to }
}

export const getCostPre = (
  item: any,
  allocationMethod: number,
  totalChargesPrice: number,
  totalReceivedQty: number,
  totalPalletUnits: number,
) => {
  if (allocationMethod === ALLOCATION_TYPE.BILLABLE) {
    if (!item.receivedQty && !item.qtyConfirmed) return 0
    return _.floor(_.divide(totalChargesPrice, totalReceivedQty), 12)
  } else if (allocationMethod === ALLOCATION_TYPE.PALLETS) {
    if (!item.palletUnits) return 0
    if (!getUnits(item, allocationMethod)) return 0
    return _.floor(
      _.divide(
        _.multiply(_.divide(totalChargesPrice, totalPalletUnits), item.palletQty),
        getUnits(item, allocationMethod),
      ),
      12,
    )
  } else if (allocationMethod === ALLOCATION_TYPE.MANUALLY) {
    if (!item.totalAllocateCharge || (!item.receivedQty && !item.qtyConfirmed)) return 0
    return _.floor(
      _.divide(item.totalAllocateCharge, item.status === 'CONFIRMED' ? item.qtyConfirmed : item.receivedQty),
      12,
    )
  }
  return 0
}

export const getUnits = (r: any, allocationMethod: ALLOCATION_TYPE) => {
  const { status } = r

  if (status === 'PLACED') {
    // 0 if item is in Placed Status
    return 0
  } else if (status === 'CONFIRMED') {
    // Confirmed Qty if item is in Confirmed status
    return r.qtyConfirmed
  } else if (status === 'RECEIVED') {
    // Received Qty if item is in Received status
    return r.receivedQty
  }
  if (allocationMethod === ALLOCATION_TYPE.BILLABLE || allocationMethod === ALLOCATION_TYPE.MANUALLY) {
    return r.receivedQty
  } else if (allocationMethod === ALLOCATION_TYPE.PALLETS) {
    return r.palletUnits
  }

  return 0
}

export const getDefaultUnit = (type: string, sellerSetting: any) => {
  let unit = ''
  switch (type) {
    case 'WEIGHT':
      unit = sellerSetting && sellerSetting.company.logisticUnit == 'IMPERIAL' ? 'LB' : 'KG'
      break
    case 'VOLUME':
      unit = sellerSetting && sellerSetting.company.logisticUnit == 'IMPERIAL' ? 'Cubic Ft.' : 'Cubic M.'
      break
    default:
      break
  }
  return unit
}

export const getAllowedEndpoint = (module: string, accountType: string, defaultPath: string) => {
  if (module == 'product') {
    return [UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(accountType) != -1
      ? 'details'
      : defaultPath
  }
  return defaultPath
}

export const addVendorProductCode = (list: any[], productList: any[], key = 'itemId') => {
  return (list || []).map((item) => {
    const itemId = item[key]
    const res = (productList || []).find((prod) => prod.wholesaleItemId == itemId)
    const { vendorProductCode, customField1 } = res || {}
    // return vendorProductCode ? { ...item, vendorProductCode } : item
    return { ...item, vendorProductCode, customField1 }
  })
}

export const calculatePOPrintableSubtotal = (orderItems: any[]) => {
  let subTotal = 0
  orderItems.forEach((element: any) => {
    if (!judgeConstantRatio(element)) {
      subTotal += element.cost * element.orderWeight
    } else {
      subTotal += element.cost * element.quantity
    }
  })
  return Math.round(subTotal * 100) / 100
}

export const warehouseSyncParseAlgorithm = (value: string) => {
  if (!value) return ''
  if (value.length >= 30 && value.startsWith('WSW', 4)) {
    return value.substring(28)
  } else if (value.length < 30 && value.startsWith('WSW', 4)) {
    return value.substring(18)
  } else if (value.length < 18 && value.startsWith('/B/')) {
    return value.slice(3)
  } else if (value.includes('.') && !value.includes('/')) {
    return value
  } else if (value.length > 18 && !value.includes('WSW')) {
    return value
  }
  return value
}

export const getAvailableToSell = (item: any) => {
  let lotAvailableQty = item.lotAvailableQty === null ? 0 : item.lotAvailableQty
  return lotAvailableQty <= 0 ? 0 : lotAvailableQty
}

export const calcOneOffTotal = (data: any[] | undefined) => {
  let oneOffTotal = 0
  if (data && data.length) {
    data.forEach((el) => {
      oneOffTotal += el.price * el.picked
    })
  }
  return oneOffTotal
}

export const sorter = (a: string | null, b: string | null) => {
  if (a === b) {
    return 0
  }
  // push blank at last
  if (!a && !b) {
    return 0
  }
  if (!a) {
    return 1
  }
  // push blank at last
  if (!b) {
    return -1
  }
  return a > b ? 1 : -1
}

export const isFloat = (value: number) => {
  if (!value) return false
  return Math.floor(value) === value ? false : true
}

export const displayStatus = (status: string, sellerSetting: any) => {
  if (status === 'SHIPPED' && sellerSetting?.company?.customShippedLabel) {
    return sellerSetting.company.customShippedLabel
  }
  return status;
}

export const includes = (parent: string, child: string) => {
  const lowCaseParent = parent?.toLowerCase()?.trim() || ''
  const lowCaseChild = child?.toLowerCase()?.trim()
  if(!lowCaseChild){
    return true
  }
  return lowCaseParent.includes(lowCaseChild)
}

export const precisionAdd = (arg1: number, arg2: number) => {
  let r1, r2, m;
  try {
    r1 = arg1.toString().split(".")[1].length
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split(".")[1].length
  } catch (e) {
    r2 = 0
  }
  m = Math.pow(10, Math.max(r1, r2))
  return ((arg1 * m + arg2 * m) / m)
}

export const precisionSubtract = (arg1: number, arg2: number) => {
  let r1, r2, m, n;
  try {
    r1 = arg1.toString().split(".")[1].length
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split(".")[1].length
  } catch (e) {
    r2 = 0
  }
  m = Math.pow(10, Math.max(r1, r2));
  n = (r1 >= r2) ? r1 : r2;
  return ((arg1 * m - arg2 * m) / m)
}

export const precisionMultiply = (arg1: number, arg2: number) => {
  let m = 0,
    s1 = arg1.toString(),
    s2 = arg2.toString();
  try {
    m += s1.split(".")[1].length
  } catch (e) { }
  try {
    m += s2.split(".")[1].length
  } catch (e) { }
  return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
}

export const precisionDivide = (arg1: number, arg2: number) => {
  let t1 = 0,
    t2 = 0,
    r1, r2;
  try {
    t1 = arg1.toString().split('.')[1].length;
  } catch (e) { }
  try {
    t2 = arg2.toString().split('.')[1].length;
  } catch (e) { }
  r1 = Number(arg1.toString().replace('.', ''));
  r2 = Number(arg2.toString().replace('.', ''));
  return (r1 / r2) * Math.pow(10, t2 - t1);
}


export const priceToPrice = (fromPrice: number, from: string, to: string, item: any, digit = 2) => {
  fromPrice = typeof fromPrice === 'string' ? parseFloat(fromPrice) : fromPrice

  const findRatio = (uom: string) => {
    if (uom == item.inventoryUOM) {
      return 1
    }
    for (const i of item.wholesaleProductUomList || []) {
      if (i.name == uom) {
        return i.ratio
      }
    }
    return 1
  }

  const fromRatio = findRatio(from)
  const toRatio = findRatio(to)
  const ratio = toRatio / (fromRatio || 1)
  const price = fromPrice / ratio
  return mathRoundFun(price, digit)
}

export const quantityToQuantity = (fromQuantity: number, fromUOM: string, toUOM: string, item: any, digit = 2) => {
  fromQuantity = +fromQuantity // 转数字

  const findRatio = (uom: string) => {
    if (uom == item.inventoryUOM) {
      return 1
    }
    for (const i of item.wholesaleProductUomList || []) {
      if (i.name == uom) {
        return i.ratio
      }
    }
    return 1
  }
  const fromRatio = findRatio(fromUOM)
  const toRatio = findRatio(toUOM)
  const ratio = toRatio / (fromRatio || 1)
  const toQuantity = fromQuantity * ratio
  return mathRoundFun(toQuantity, digit)
}

// https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
export function numberWithCommas(x: any) {
  const parts = x.toString().split('.')
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return parts.join('.')
}
