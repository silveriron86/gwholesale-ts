/** @jsx jsx */
import { jsx } from '@emotion/core'
import React, { useState } from 'react'
import useForm from 'rc-form-hooks'
import { Button, Icon, Input } from 'antd'

import Tag from '../Tag'
import { Container, inputStyle, titleStyle, tagStyle, getCaretIconCss, showHideStyle } from './style'

interface FormOptions {
  category: string
}

const CategoryHeader: React.SFC<{
  value: string
  editMode?: boolean
  onChange?: (value: string) => void
  collapsed: boolean
  onCollapseChange: () => void
  bordered?: boolean
  hideCollapse?: boolean
}> = (props) => {
  const [editing, setEditing] = useState(false)
  const [value, setValue] = useState(props.value)

  const { getFieldDecorator, validateFields, resetFields } = useForm<FormOptions>()
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    validateFields()
      .then(({ category }) => {
        setValue(category)
        setEditing(false)
        if (typeof props.onChange === 'function') {
          props.onChange(category)
        }
      })
      .catch(console.error)
  }

  const handleEditableClick = () => {
    setEditing(true)
  }

  const handleCancel = () => {
    setEditing(false)
    resetFields(['category'])
  }

  if(value === 'All') {
    return null;
  }

  return (
    <Container bordered={props.bordered}>
      {editing ? (
        <form onSubmit={handleSubmit} css={titleStyle}>
          {getFieldDecorator('category', {
            initialValue: value,
            rules: [
              {
                required: true,
                message: 'Please input a category name',
              },
            ],
          })(<Input allowClear type="text" css={inputStyle} placeholder="Please input something" />)}
          <Button type="primary" htmlType="submit" style={{ marginRight: 5 }}>
            Save
          </Button>
          <Button onClick={handleCancel}>Cancel</Button>
        </form>
      ) : (
        <React.Fragment>
          {/* {props.editMode ? (
            <Tag css={tagStyle} onClick={handleEditableClick}>
              <Icon type="edit" />
              &nbsp; EDIT CATEGORY
            </Tag>
          ) : null} */}
          <div css={titleStyle} onClick={props.onCollapseChange}>
            {value}
            {/* <Icon type="caret-down" css={getCaretIconCss(!props.collapsed)} /> */}
          </div>
          {!props.hideCollapse && (
            <Tag css={showHideStyle} onClick={props.onCollapseChange}>
              <Icon type="caret-down" css={getCaretIconCss(!props.collapsed)} />
              &nbsp; {props.collapsed ? 'Expand' : 'Collapse'}
            </Tag>
          )}
        </React.Fragment>
      )}
    </Container>
  )
}

export default CategoryHeader
