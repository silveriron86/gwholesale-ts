import styled from '@emotion/styled'
import { css } from '@emotion/core'

import { font, gap } from '~/common/styleVars'

export const inputStyle = css({
  width: 200,
  marginRight: gap * 1.5,
})

export const tagStyle = css({
  marginLeft: gap * 1.5,
})

export const showHideStyle = css({
  position: 'absolute',
  right: gap * 1.5,
  cursor: 'pointer'
})

export const Container: React.SFC<{
  bordered?: boolean
  width?: number
}> = styled.div((props) => ({
  border: `1px solid ${props.theme.light}`,
  borderRightWidth: props.bordered ? 1 : 0,
  borderLeftWidth: props.bordered ? 1 : 0,
  height: 54,
  width: props.width,
  display: 'flex',
  alignItems: 'center',
  position: 'relative',
  background: props.theme.lighter
}))

export const titleStyle = css({
  fontSize: font.title,
  fontWeight: 'bold',
  position: 'absolute',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  top: '50%',
  cursor: 'pointer'
})

export const getCaretIconCss = (collapsed: boolean) => css({
  transform: collapsed ? 'rotate(180deg)' : 'rotate(0deg)',
  transition: 'transform 300ms linear'
})
