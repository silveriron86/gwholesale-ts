import React from 'react'
import { Menu } from 'antd'
import { MenuItem, MenuItemText } from './style'
import { ClassNames } from '@emotion/core'
import { Theme, CACHED_ACCOUNT_TYPE } from '~/common'
import MENU from '~/common/menu'
import { Link } from 'react-router-dom'
import { Company, UserRole } from '~/schema'
import { Icon } from '~/components/icon/'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { SettingDispatchProps, SettingModule, SettingStateProps } from '~/modules/setting'

const { SubMenu } = Menu

export interface MenuListProps {
  theme: Theme
  selected: string
  onSelect: Function
  companyInfo: Company | null
}

class MenuList extends React.PureComponent<MenuListProps & SettingStateProps & SettingDispatchProps> {
  noSubMenuKeys: Array<string> = []
  state: any

  constructor(props: MenuListProps) {
    super(props)
    const { selected } = props
    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE) && localStorage.getItem(CACHED_ACCOUNT_TYPE) != 'null' ? localStorage.getItem(CACHED_ACCOUNT_TYPE) : UserRole.ADMIN
    let menus = accountType === UserRole.SUPERADMIN ? MENU[UserRole.ADMIN] : (accountType != null ? MENU[accountType] : [])
    let openKeys = [];
    menus.forEach((element) => {
      if (element.children) {
        openKeys.push(`menu-${element.label}`);
      } else {
        this.noSubMenuKeys.push(`menu-${element.label}`)
      }
    })

    this.state = {
      openKeys,
      selectedKeys: [this.props.selected],
    }
  }

  onOpenChange = (openKeys: Array<string>) => {
    this.setState({ openKeys })
  }

  onSelect = (param: any) => {
    const { openKeys } = this.state
    this.setState(
      {
        selectedKeys: param.key,
        openKeys: this.noSubMenuKeys.indexOf(param.key) >= 0 ? [] : openKeys,
      },
      () => {
        if (!window.event.ctrlKey) {
          this.props.onSelect(param.key)
        }
      },
    )
  }

  _getMenuItemStyle = (hasChildren: boolean, key: string) => {
    const { selectedKeys } = this.state
    const selected = (selectedKeys as Array<string>).indexOf(key) >= 0
    return {
      display: 'flex',
      alignItems: 'center',
      marginBottom: hasChildren ? 20 : 0,
      backgroundColor: selected ? this.props.theme.dark : 'transparent',
      borderLeft: `4px solid ${selected ? this.props.theme.light : 'transparent'}`,
      color: selected ? 'white' : 'rgba(255, 255, 255, 0.6)',
      height: 32,
    }
  }

  _getMenuItemClass = (key: string) => {
    const { selectedKeys } = this.state
    return (selectedKeys as Array<string>).indexOf(key) >= 0 ? 'selected' : ''
  }

  render() {
    const rows: Array<any> = []
    // const menus = localStorage.getItem(CACHED_ACCOUNT_TYPE) === UserRole.CUSTOMER ? MENU.customer : MENU.seller
    const accountType =
      localStorage.getItem(CACHED_ACCOUNT_TYPE) && localStorage.getItem(CACHED_ACCOUNT_TYPE) != 'null'
        ? localStorage.getItem(CACHED_ACCOUNT_TYPE)
        : UserRole.ADMIN
    let menus =
      accountType === UserRole.SUPERADMIN ? MENU[UserRole.ADMIN] : accountType != null ? MENU[accountType] : []
    const { companyInfo } = this.props
    const { openKeys } = this.state
    menus.forEach((element) => {
      if (element.children) {
        const cols: Array<any> = []
        element.children.forEach((child) => {
          if (companyInfo && companyInfo.hideMenuLabel === true && child.label === 'Label') {
            return
          }
          if (companyInfo && companyInfo.hideMenuRequisitionCat === true && child.label === 'Requisition Catalog') {
            return
          }
          const key = `menu-${element.label}-${child.label}`
          let childItem: any = null
          if (child.default) {
            childItem = (
              <Link to={`${child.default}`} style={{ marginLeft: -21 }}>
                <MenuItemText style={{ display: 'flex', alignItems: 'center' }}>
                  <Icon
                    type={child.icon}
                    viewBox={void 0}
                    style={{ width: 24, height: 24, marginBottom: 1, marginRight: 9, display: 'block' }}
                  />
                  {child.label}
                </MenuItemText>
              </Link>
            )
          } else {
            childItem = <MenuItemText>{child.label}</MenuItemText>
          }
          if (child.label === '') {
            cols.push(
              <div style={{ borderBottom: '1px solid rgba(255, 255, 255, 0.1)', marginTop: 24, marginBottom: 24 }} />,
            )
          } else {
            cols.push(
              <Menu.Item className={this._getMenuItemClass(key)} key={key} style={this._getMenuItemStyle(false, key)}>
                {childItem}
              </Menu.Item>,
            )
          }
        })

        rows.push(
          <SubMenu
            key={`menu-${element.label}`}
            style={MenuItem}
            title={
              <MenuItemText
                style={{
                  height: 30,
                  marginLeft: 18,
                  fontSize: 12,
                  fontWeight: 'bold',
                  fontFamily: 'Museo Sans Rounded',
                  color: 'rgba(255, 255, 255, 0.6)',
                }}
              >
                <Icon
                  type="menu_arrow"
                  viewBox={void 0}
                  style={{
                    width: 10,
                    height: 6,
                    position: 'absolute',
                    left: 24,
                    top: 18,
                    transform: openKeys.indexOf(`menu-${element.label}`) >= 0 ? 'none' : 'rotate(180deg)',
                  }}
                />
                {element.label.toUpperCase()}
              </MenuItemText>
            }
          >
            {cols}
          </SubMenu>,
        )
      } else {
        let menuItemText: any = null
        if ((!element.sub || element.sub.length === 0) && element.path) {
          menuItemText = (
            <Link to={`${element.path}`}>
              <MenuItemText>{element.label}</MenuItemText>
            </Link>
          )
        } else if (element.sub && element.default) {
          menuItemText = (
            <Link to={`${element.default}`}>
              <MenuItemText>{element.label}</MenuItemText>
            </Link>
          )
        } else {
          menuItemText = <MenuItemText>{element.label}</MenuItemText>
        }

        rows.push(
          <Menu.Item key={`menu-${element.label}`} style={this._getMenuItemStyle(true, `menu-${element.label}`)}>
            {menuItemText}
          </Menu.Item>,
        )
      }
    })

    return (
      <ClassNames>
        {({ css, cx }) => (
          <Menu
            style={{ borderRight: 0, backgroundColor: this.props.theme.main }}
            openKeys={this.state.openKeys}
            selectedKeys={this.state.selectedKeys}
            onOpenChange={this.onOpenChange}
            onSelect={this.onSelect}
            subMenuCloseDelay={0.2}
            mode="inline"
            theme="dark"
          >
            {rows}
          </Menu>
        )}
      </ClassNames>
    )
  }
}
export default MenuList
