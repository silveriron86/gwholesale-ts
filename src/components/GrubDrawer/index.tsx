import React, { useEffect, useState } from 'react'
import { Icon, Avatar, Button, Dropdown, Menu } from 'antd'
import Drawer, { RcDrawerProps } from 'rc-drawer'
import { withTheme } from 'emotion-theming'
import { ClassNames } from '@emotion/core'
import { isMobile, isBrowser } from 'react-device-detect'
import { Icon as IconSvg } from '~/components'
import { CACHED_ACCOUNT_TYPE, Theme } from '~/common'
import { myAccountMenuDropDownStyle } from '~/components/header/header.style'
import jQuery from 'jquery'

import {
  HandlerText,
  DrawerWrapper,
  drawerWrapperStyle,
  HandlerContainer,
  HandlerFilterContainer,
  DrawerHeader,
  DrawerTitle,
  DrawerBody,
  DrawerFilterBody,
  drawerStyle,
  UserInfoText,
} from './style'
import {
  HandlerContainer as MobileHandler,
  drawerWrapperStyle as MobileDrawerWrapperStyle,
} from './../MobileDrawer/style'
import 'rc-drawer/assets/index.css'
import MenuList from './MenuList'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { HomePageDispatchProps, HomePageModule, HomePageStateProps } from '~/modules/homepage'
import { AuthUser, UserRole } from '~/schema'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import { Link } from 'react-router-dom'

const Handler: React.SFC<{
  theme: Theme
  open: boolean
  type: string
  onClick: () => void
  onMouseOver: () => void
  onMouseLeave: () => void
  filter: boolean
}> = (props) => (
  <>
    {!isMobile || props.type == 'filter' ? (
      <>
        {props.filter ? (
          <HandlerFilterContainer style={{ width: 48 }} onClick={props.onClick}>
            {props.open ? (
              <Icon type="close" style={{ fontSize: 20 }} />
            ) : (
              <IconSvg
                style={{ color: props.theme.main }}
                type="filter-icon"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              />
            )}
            <HandlerText>{props.open ? 'CLOSE FILTER' : 'FILTER'}</HandlerText>
          </HandlerFilterContainer>
        ) : (
          <HandlerContainer onClick={props.onClick} onMouseOver={props.onMouseOver} onMouseLeave={props.onMouseLeave}>
            {props.open ? (
              <Icon type="close" style={{ fontSize: 20 }} />
            ) : (
              <IconSvg
                style={{ color: props.theme.main }}
                type="filter-icon"
                viewBox="0 0 24 24"
                width="24"
                height="24"
              />
            )}
            <HandlerText>{props.open ? 'CLOSE MENU' : 'MAIN MENU'}</HandlerText>
          </HandlerContainer>
        )}
      </>
    ) : (
      <>
        <MobileHandler onClick={props.onClick} onMouseOver={props.onMouseOver} onMouseLeave={props.onMouseLeave}>
          {props.open ? (
            <Icon type="close" style={{ fontSize: 20 }} />
          ) : (
            <IconSvg
              style={{ color: props.theme.main }}
              type="filter-icon"
              viewBox="0 0 24 24"
              width="24"
              height="24"
            />
          )}
        </MobileHandler>
      </>
    )}
  </>
)

const GrubDrawer: React.SFC<RcDrawerProps &
  HomePageDispatchProps &
  HomePageStateProps & {
    theme: Theme
    selected: string
    onSelect: Function
    currentUser?: AuthUser
    placement: string
    subDrawer: boolean
    width: number
    opened?: boolean
  }> = (props) => {
    const [open, setOpen] = useState(false)
    const [type, setType] = useState(props.subDrawer ? 'filter' : 'menu')
    const [clickable, setClickable] = useState(false)

    useEffect(() => {
      if (props.currentUser?.accountType !== UserRole.CUSTOMER) {
        props.getCompanyInMenuList()
      }
    }, [])

    useEffect(() => {
      if (props.opened) {
        setTimeout(() => {
          jQuery('.drawer-handle').trigger('click')
        }, 1000)
      }
    }, [props.opened])

    const toggleDrawer = () => {
      // if (clickable) {
      setOpen(!open)
      // }
    }
    const afterVisibleChange = (visible) => {
      // if (clickable) {
      setOpen(visible)
      // }
    }

    const onSelect = (key: any) => {
      props.onSelect(key)
      toggleDrawer()
    }

    const onMouseOver = () => {
      setClickable(true)
    }

    const onMouseLeave = () => {
      setClickable(false)
    }

    const renderMyAccountMenu = () => {
      let sorts = ['Edit Profile', 'Edit Categories', 'Edit Company Setting']
      const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)
      if ([UserRole.BUYER, UserRole.SALES, UserRole.CUSTOMER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(accountType) > -1) {
        sorts = ['Edit Profile']
      }
      return (
        <Menu style={myAccountMenuDropDownStyle}>
          {sorts.map((sort) => (
            <Menu.Item key={sort}>
              <Link to={'/myaccount/' + sort.substring(sort.lastIndexOf(' ') + 1).toLowerCase()}>{sort}</Link>
            </Menu.Item>
          ))}
        </Menu>
      )
    }

    const handleLogout = () => {
      localStorage.clear()
      window.location.reload()
    }

    const { children, title, selected, ...restProps } = props

    const hasSelfHeader = true

    const drawerContent = props.subDrawer ? (
      <div>
        <DrawerHeader>
          <DrawerTitle>
            <Icon style={{ marginRight: 5, color: props.theme.main }} type="menu-fold" />
            Filters
          </DrawerTitle>
        </DrawerHeader>
        <DrawerFilterBody>{children}</DrawerFilterBody>
      </div>
    ) : (
      <DrawerWrapper>
        {hasSelfHeader && (
          <div style={{ padding: '16px 0 14px 16px', borderBottom: '1px solid rgba(255, 255, 255, 0.1)' }}>
            <IconSvg type="logo4" viewBox={void 0} color={'white'} style={{ width: 224, height: 37, display: 'block' }} />
          </div>
        )}
        <DrawerBody style={{ paddingRight: 0, paddingTop: 14 }}>
          <MenuList theme={props.theme} selected={selected} onSelect={onSelect} companyInfo={props.companyInfo} />
          {children}
        </DrawerBody>
        <Flex className="footer-section">
          <Avatar icon="user" size={50} />
          <div className="info">
            <UserInfoText className="user-name">
              {props.currentUser && props.currentUser.fullName != 'undefined'
                ? props.currentUser.fullName
                : 'Unknown User'}
            </UserInfoText>
            <Flex>
              <Dropdown trigger={['click']} overlay={renderMyAccountMenu()} overlayStyle={{ zIndex: 9999 }}>
                <Button type="link">
                  <IconSvg type="menu_settings" viewBox={void 0} style={{ width: 16, height: 16 }} />
                  Settings
                </Button>
              </Dropdown>
              <span className="between-dot">.</span>
              <Button type="link" onClick={handleLogout}>
                Log out
              </Button>
            </Flex>
          </div>
          {/* <UserInfoText style={{ fontSize: 13, marginTop: 5 }}>
          {props.currentUser ? props.currentUser.accountType : ''}
        </UserInfoText> */}
        </Flex>
      </DrawerWrapper>
    )

    return (
      <ClassNames>
        {({ css, cx }) => (
          <Drawer
            wrapperClassName={`${cx(css(isBrowser ? drawerWrapperStyle : MobileDrawerWrapperStyle))} ${hasSelfHeader ? 'self-header' : ''
              }`}
            className={cx(css(drawerStyle))}
            level={null}
            closable
            maskStyle={{ background: 'transparent' }}
            onChange={afterVisibleChange}
            handler={
              <div className="drawer-handle">
                <Handler
                  filter={props.subDrawer}
                  theme={props.theme}
                  open={open}
                  onClick={toggleDrawer}
                  type={type}
                  onMouseOver={onMouseOver}
                  onMouseLeave={onMouseLeave}
                />
              </div>
            }
            {...restProps}
          >
            {drawerContent}
          </Drawer>
        )}
      </ClassNames>
    )
  }

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.homepage,
    currentUser: state.currentUser,
  }
}

export default withTheme(connect(HomePageModule)(mapStateToProps)(GrubDrawer))
