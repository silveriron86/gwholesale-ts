import styled from '@emotion/styled'
import css from '@emotion/css'

import { gap, borderColor } from '~/common/styleVars'

const handlerWidth = 48

export const drawerStyle = css({
  '&': {
    marginTop: 59,
    height: 'calc(100vh - 59px)',

    '& .drawer-mask': {
      height: '100vh !important',
      pointerEvents: 'none',
    },

    '&.drawer-open .drawer-mask': {
      pointerEvents: 'auto',
    },

    '& .drawer-content-wrapper': {
      height: 'calc(100vh - 59px) !important'
    }
  },
})

export const drawerWrapperStyle = css({
  '& .drawer-content': {
    backgroundColor: 'transparent',
    '&::-webkit-scrollbar-track': {
      borderRadius: 0,
      // '-webkit-box-shadow': 'inset 0 0 6px rgba(0, 0, 0, 0.3)',
      backgroundColor: 'rgba(0, 0, 0, 0.3)'
    },
    '&::-webkit-scrollbar': {
      width: 12,
      backgroundColor: '#F5F5F5'
    },
    '&::-webkit-scrollbar-thumb': {
      borderRadius: 0,
      // '-webkit-box-shadow': 'inset 0 0 6px rgba(0, 0, 0, 0.3)',
      backgroundColor: 'rgba(0, 0, 0, 0.3)'
    }
  },
  '& .drawer-handle': {
    top: 0,
    width: handlerWidth,
    boxShadow: 'none',
    height: '100%',
    borderColor: 'transparent',
    display: 'block',
    backgroundColor: 'transparent',
    cursor: 'default'
  },
  '& .drawer-left .drawer-handle': {
    right: -1 * handlerWidth,
  },
})

export const HandlerContainer = styled.div((props) => ({
  width: '100%',
  height: '100%',
  backgroundColor: props.theme.main,
  clipPath: 'url(#drawerClip)',
  color: 'white',
  cursor: 'pointer'
}))

export const HandlerFilterContainer = styled.div((props) => ({
  width: '100%',
  height: '100%',
  backgroundColor: props.theme.main,
  color: 'white',
}))

export const HandlerText = styled.div({
  marginTop: gap * 1.5,
  fontSize: `13px !important`,
  writingMode: 'vertical-lr',
  width: handlerWidth,
  lineHeight: `${handlerWidth}px`,
})

export const DrawerWrapper = styled.div((props) => ({
  backgroundColor: props.theme.main,
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  '.footer-section': {
    padding: '0 20px',
    alignItems: 'center',
    height: 100,
    borderTop: '1px solid rgba(255, 255, 255, 0.1)',
    background: '#0b4018',
    width: 247,
    position: 'fixed',
    bottom: 0,
    '.info': {
      marginLeft: 10,
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      paddingTop: 29
    },
    '.user-name': {
      marginTop: 0,
      fontSize: 15,
      fontFamily: 'Museo Sans Rounded',
      fontWeight: 'bold',
      lineHeight: '21px',
      marginBottom: 1,
    },
    '.ant-btn': {
      border: '0 !important',
      padding: 0,
      height: 22,
      lineHeight: '22px',
      svg: {
        marginRight: 5,
        marginTop: -3,
      },
      span: {
        fontFamily: 'Museo Sans Rounded',
        fontSize: 14,
        color: 'white'
      }
    },
    '.between-dot': {
      margin: '-3px 5px 0',
      color: 'white'
    }
  }
}))

export const DrawerHeader = styled.div({
  padding: '16px 0 24px',
  color: 'white',
  borderBottom: `1px solid ${borderColor}`,
  marginLeft: '26px',
  width: 190,
})

export const DrawerBody = styled.div((props) => ({
  flex: 1,
  padding: '60px 24px 110px 0px',
  fontSize: 14,
  lineHeight: 1.5,
  wordWrap: 'break-word',
  backgroundColor: props.theme.main
})

export const DrawerFilterBody = styled.div({
  padding: '15px 24px 24px 30px',
  fontSize: 14,
  lineHeight: 1.5,
  wordWrap: 'break-word',
})

export const DrawerTitle = styled.div({
  color: '#373D3F',
  fontFamily: 'Museo Sans Rounded',
  lineHeight: '28px',
  fontSize: 26,
  fontWeight: 700,
  letterSpacing: '0.05em',
})

export const UserInfoText = styled.div({
  fontFamily: 'Arial',
  fontSize: 15,
  fontWeight: 'bold',
  lineHeight: '108%',
  letterSpacing: '0.05em',
  color: '#FFFFFF',
  marginTop: 12,
})

export const MenuItem = {
  marginBottom: 16,
}

export const MenuItemText = styled.span((props) => ({
  color: 'white',
  fontFamily: 'Arial',
  fontSize: 16,
  lineHeight: '108%',
  letterSpacing: '0.05em',
}))
