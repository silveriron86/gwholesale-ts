/**@jsx jsx */
import React, { useState, useEffect } from 'react'
import { jsx, ClassNames } from '@emotion/core'
import { AutoComplete, Input } from 'antd'
import lodash from 'lodash'
import Highlighter from 'react-highlight-words'

import { Icon } from '~/components'
import { autoCompleteDropdownCss, grubSuggestCss, highLightStyle, SearchAllText } from './style'
import { SelectValue } from 'antd/lib/select'
import { Theme, lightGreen } from '~/common'
import jQuery from 'jquery'
import { navigateSearchItems } from '~/common/jqueryHelper'
import { Flex } from '~/modules/customers/customers.style'

interface CoreSearchResult {
  title: string
}

// 暂时先维持两层结构 后续有需求再增加
export interface SearchResult extends CoreSearchResult {
  title: string
  children: CoreSearchResult[]
}

const SearchButton: React.SFC<
  {
    onClick: React.MouseEventHandler<HTMLDivElement>
  } & { theme: Theme }
> = (props) => (
  <div onClick={props.onClick}>
    <Icon viewBox="0 0 16 16" width="16" height="16" type="search" color={props.theme.light} />
    {/* <SearchBtnText>SEARCH</SearchBtnText> */}
  </div>
)

export interface Props {
  onSearch: (keyword: string, selected?: boolean, searchType?: string) => void
  onSuggest: (keyword: string) => SearchResult[]
  theme: Theme
  placeholder?: string
  width: number
  defaultValue?: string
  customWidth?: boolean
  maxDropdownWidth?: number
  isProduct?: boolean
}

function GrubSuggest(props: Props) {
  // state for search keywords
  const [query, setQuery] = useState<string>(props.defaultValue)
  // state for dataSource in suggestion menu
  const [dataSource, setDataSource] = useState<SearchResult[]>([])
  // state to keep suggestion menu open or not
  const [openMenu, setMenuOpen] = useState<boolean>(false)
  // search for '---' in products or lots
  const [searchType, setSearchType] = useState<string>('products')

  useEffect(() => {
    if (openMenu) {
      setTimeout(() => {
        const dropdown = jQuery('.grub-suggest-dropdown').children()[0]
        const ul = jQuery(dropdown).children()[0]
        const items = jQuery('.grub-suggest-dropdown').find('li:not(.ant-select-dropdown-menu-item-group)')
        resetEventWithNewItems(items)
        if (ul) {
          jQuery(ul).animate({ scrollTop: 0 }, 10)
          if (items.length > 0) {
            jQuery(items[0]).css({ 'background-color': '#C1C3C1' })
          }
        }
      }, 50)
    }
  }, [openMenu])

  useEffect(() => {
    jQuery('.products-searcher .ant-input').unbind().bind('keydown', (event: any) => {
      const keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13') {
        setTimeout(() => {
          const search = jQuery('.products-searcher .ant-input').val();
          clearSuggestion()
          if (props.isProduct === true) {
            props.onSearch(search || '', undefined, searchType)
          } else {
            props.onSearch(search || '')
          }
        }, 300)
      }
    })
  }, [searchType])

  function handleKeyEvent(parent: any, items: any[], itemIndex: number, actionType: string, e: any) {
    if (openMenu) {
      if (actionType == 'move') {
        const height = jQuery(parent).height()
        const count = items.length
        // setTimeout(() => {
        jQuery(parent).animate({ scrollTop: Math.floor(itemIndex / count * height) }, 10)
        // }, 30)

        items.map((index: number, el: any) => {
          jQuery(el).css('background-color', '')
        })
        jQuery(items[itemIndex]).css({ 'background-color': '#C1C3C1' })
      } else {
        // let suggestion = jQuery(items[itemIndex]).text()
        // if (itemIndex == 0) {
        //   suggestion = suggestion.substr(10).trim()
        // }
        // console.log('here', e);
        // props.onSearch(suggestion.toString())
        // clearSuggestion()
        // setQuery(suggestion.toString())
        // setTimeout(() => {
        //   jQuery('.grub-suggest-dropdown-wrapper input').val(suggestion.toString())
        // })
      }
    }
  }

  // handle dataSource and menuOpen
  function setSuggestMenu(data: any[]) {
    setDataSource(data)
    setMenuOpen(data.length > 0)
  }

  // clear suggestion
  function clearSuggestion() {
    setSuggestMenu([])
  }

  // callback when you input something and get result for suggest menu
  const handleSearch = (keyword: string) => {
    // note down search keyword to state (consider mount it to this?)
    setQuery(keyword)
    // hide suggestion menu when search keywords less than 2
    if (!keyword || keyword.length < 2) {
      resetEventWithNewItems([])
      return clearSuggestion()
    }
    // search by keyword
    const next = keyword ? props.onSuggest(keyword) : []

    //whenever search value is changed, reselect first element
    setTimeout(() => {
      const items = jQuery('.grub-suggest-dropdown').find('li:not(.ant-select-dropdown-menu-item-group)')
      resetEventWithNewItems(items)
      if (items.length > 0) {
        jQuery(items[0]).css({ 'background-color': '#C1C3C1' })
      }
    }, 10)

    // Fix: Search typeahead dropdown for Customer and Vendor list is glitchy
    setTimeout(() => {
      jQuery('.grub-suggest-dropdown li.search-item').off('mouseenter mouseleave')
      jQuery('.grub-suggest-dropdown li.search-item').hover(
        function (e: any) {
          e.preventDefault();
          e.stopPropagation();
        }, function (e: any) {
          e.preventDefault();
          e.stopPropagation();
        }
      );
    }, 100)
    return setSuggestMenu(next)
  }

  const resetEventWithNewItems = (items: any[]) => {
    const dropdown = jQuery('.grub-suggest-dropdown').children()[0]
    const ul = jQuery(dropdown).children()[0]
    navigateSearchItems(ul, items, handleKeyEvent)
  }

  // callback when you select one from suggest menu
  const handleSelect = (suggestion: SelectValue, option: Object) => {
    // trigger `props.onSearch` callback
    props.onSearch(suggestion.toString(), option.props.className === 'search-item')
    // cleanup for search result
    clearSuggestion()
    // set query (make any sense)
    setQuery(suggestion.toString())
  }

  // remove suggestions and search directly
  const triggerDirectSearch = () => {
    clearSuggestion()
    if (props.isProduct === true) {
      props.onSearch(query || '', undefined, searchType)
    } else {
      props.onSearch(query || '')
    }
  }

  // callback when you click search btn
  const handleSearchBtnClick = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    e.stopPropagation()
    triggerDirectSearch()
  }

  // callback when you click `Enter` in input
  const handlePressEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (!openMenu) {
      e.preventDefault()
      e.stopPropagation()
      triggerDirectSearch()
    }
  }

  // trigger suggestion it when focus
  const handleFocus = () => {
    handleSearch(query || '')
  }

  // remove suggestion but keep keywords when blur
  const handleBlur = () => {
    clearSuggestion()
  }

  const handleClear = (e: any) => {
    e.stopPropagation()
    setQuery('')
    clearSuggestion()
    props.onSearch('', undefined, searchType)
  }

  const searchIn = (inType: string) => {
    setSearchType(inType)
    props.onSearch(query, undefined, inType)
  }

  let options = !dataSource.length
    ? undefined // amazing type for ts`?`
    : dataSource
      .filter((n) => n.children.length)
      .map((group) => (
        <AutoComplete.OptGroup key={group.title} label={group.title}>
          {group.children.map((opt) => (
            <AutoComplete.Option key={opt.title} value={opt.itemId ? `itemId-${opt.itemId}` : opt.title} className='search-item'>
              <Highlighter
                highlightStyle={highLightStyle}
                searchWords={[query as string]}
                autoEscape={true}
                textToHighlight={opt.title}
              />
            </AutoComplete.Option>
          ))}
        </AutoComplete.OptGroup>
      ))

  const searchForQuery: any = query ? (
    props.isProduct === true ? (
      <AutoComplete.Option key={query} className='product-search-for'>
        <SearchAllText className={`for-product ${searchType === 'products' ? 'active' : ''}`} onClick={() => searchIn('products')}>
          <Icon viewBox="0 0 16 16" width="16" height="16" type="search" />
          <span>Search for &ldquo;{query}&rdquo; in products</span>
        </SearchAllText>
        <SearchAllText className={`for-product ${searchType === 'lots' ? 'active' : ''}`} onClick={() => searchIn('lots')}>
          <Icon viewBox="0 0 16 16" width="16" height="16" type="search" />
          <span>Search for &ldquo;{query}&rdquo; in lots</span>
        </SearchAllText>
      </AutoComplete.Option>
    ) : (
      <AutoComplete.Option key={query}>
        <SearchAllText>Search for {query}</SearchAllText>
      </AutoComplete.Option>
    )
  ) : []
  if (options && query) {
    options.splice(0, 0, searchForQuery)
  }

  return (
    <ClassNames>
      {({ css, cx }) => (
        <AutoComplete
          defaultValue={query}
          allowClear={false}
          dropdownMatchSelectWidth={!props.customWidth ? true : false}
          className={`${cx(css(grubSuggestCss))} grub-suggest-dropdown-wrapper`}
          dropdownClassName={`${cx(css(autoCompleteDropdownCss))} grub-suggest-dropdown`}
          size="large"
          style={{ width: `${props.width ? props.width : 300}px`, border: '0', fontWeight: 100 }}
          open={openMenu}
          dataSource={options}
          onSelect={handleSelect}
          onFocus={handleFocus}
          onBlur={handleBlur}
          onSearch={handleSearch}
          placeholder={props.placeholder ? props.placeholder : "Search..."}
          optionLabelProp="value"
          dropdownMenuStyle={{ width: props.maxDropdownWidth ? props.maxDropdownWidth : 'none' }}
          value={query}
        >
          <Input
            className="grub-suggest-input"
            onPressEnter={handlePressEnter}
            suffix={
              <Flex>
                {query &&
                  <div className="clear-icon" onClick={handleClear}>
                    <Icon viewBox="0 0 16 16" width="16" height="16" type="close-circle" />
                  </div>
                }
                <SearchButton onClick={handleSearchBtnClick} theme={props.theme} />
              </Flex>
            }
          />
        </AutoComplete>
      )}
    </ClassNames>
  )
}

export default GrubSuggest
