import { css } from '@emotion/core'
import styled from '@emotion/styled'

import { brightGreen, medLightGrey, lightGreen, lightGrey } from '~/common'

export const grubSuggestCss = css({
  '&': {
    '.clear-icon': {
      'path': {
        fill: lightGrey
      },
      marginRight: 8,
      display: 'block'
    },
    '.grub-suggest-input:hover': {
      '.clear-icon': {
        display: 'block'
      },
    }
  },
})

export const autoCompleteDropdownCss = css({
  '&': {
    backgroundColor: 'lightGreen',
  },
  '& .ant-select-dropdown-menu-item-group': {
    // display: 'flex',
  },
  '& .ant-select-dropdown-menu-item-group-title': {
    color: brightGreen,
    fontSize: 14,
    // width: 100,
  },
  '& .ant-select-dropdown-menu-item-group-list': {
    // flex: 1,
  },
  '& .ant-select-dropdown-menu-item-group:not(:last-child)': {
    borderBottom: `1px solid ${medLightGrey}`,
  },
  '& .ant-select-dropdown-menu-item': {
    backgroundColor: lightGreen,
  },
})

export const highLightStyle: React.CSSProperties = {
  backgroundColor: 'transparent',
  fontWeight: 'bold',
}

export const SearchBtnText = styled.span({
  marginLeft: 9,
  fontSize: 14,
  lineHeight: 15,
  letterSpacing: '0.05em',
})

export const SearchAllText = styled.div(() => ({
  textAlign: 'center',
  color: brightGreen,
  '&.for-product': {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    padding: '5px 10px',
    backgroundColor: '#edf1ee',
    span: {
      paddingTop: 2,
      display: 'block',
      marginLeft: 6,
      textAlign: 'left',
    },
    '&.active': {
      backgroundColor: '#cdcdcd',
    },
    '&:hover': {
      backgroundColor: '#a3ada4',
    }
  }
}))
