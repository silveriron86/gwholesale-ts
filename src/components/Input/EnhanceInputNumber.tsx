import React, { ComponentProps, FC, useRef } from 'react'
import styled from '@emotion/styled'
import { Input } from 'antd'
import { formatBn } from '~/common/mathjs'

const StyledInput = styled(Input)<{ $warn?: boolean }>`
  &&& {
    background-color: ${(p) => p.$warn && '#feeae9'};
    border: ${(p) => p.$warn && '1px solid #eb5757'};
    box-shadow: ${(p) => p.$warn && '0px 1px 2px rgba(0, 0, 0, 0.25)'};
  }
`

type InputProps = ComponentProps<typeof Input>
export type ValueType = string | number | undefined

type PropsType = Omit<InputProps, 'onChange' | 'value'> & {
  value: ValueType
  onChange?: (value: string) => void
  id?: string
  place2?: boolean
  warn?: boolean
  max?: number
  formatter?: (value: ValueType, focused: boolean) => string
  parser?: (value: string) => string
}

const InputNumber: FC<PropsType> = (props) => {
  const { value, onChange, id, place2, warn, max, formatter, parser, onFocus, onBlur, ...rest } = props
  const focusRef = useRef(false)

  const _onChange: InputProps['onChange'] = (e) => {
    let { value } = e.target
    value = parser ? parser(value) : value

    if (!/^\d*\.?\d*$/.test(value)) {
      return
    }
    if (max && Number(value) > max) {
      return
    }
    // 最多小数点后两位
    if (place2 && /\.\d{3,}$/.test(value)) {
      return
    }
    if (/^0+[1-9]/.test(value)) {
      value = value.replace(/^0+/, '')
    }
    if (/^0{2,}/.test(value)) {
      value = value.replace(/^0{2,}/, '0')
    }
    onChange && onChange(value)
  }

  const _onFocus = (e: any) => {
    focusRef.current = true
    onFocus && onFocus(e)
  }

  const _onBlur = (e: any) => {
    focusRef.current = false
    onBlur && onBlur(e)
  }

  return (
    <StyledInput
      {...rest}
      value={formatter ? formatter(value, focusRef.current) : value}
      id={id}
      onFocus={_onFocus}
      onBlur={_onBlur}
      onChange={_onChange}
      $warn={warn}
    />
  )
}

export const format$ = (value: ValueType, focused: boolean) => {
  if (focused) {
    if (value === undefined) {
      return '$'
    }
    return '$' + value
  }
  return formatBn(value, { $: true })
}

export const parse$ = (val: string) => val.replace('$', '')

export const formatPercent = (value: ValueType, focused: boolean) => {
  if (focused) {
    if (value === undefined) {
      return ''
    }
    return value + ''
  }
  if (value === undefined || value === '') {
    return '%'
  }
  return formatBn(value) + '%'
}

export const parsePercent = (val: string) => val.replace('%', '')

export default InputNumber
