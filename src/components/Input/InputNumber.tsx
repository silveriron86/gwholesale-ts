import React, { ComponentProps, FC } from 'react'

import { Input } from 'antd'

type InputProps = ComponentProps<typeof Input>

type PropsType = InputProps & {
  onChange?: (value: string) => void
  id?: string
  place2?: boolean
}

const InputNumber: FC<PropsType> = (props) => {
  const { onChange, id, place2, ...rest } = props

  const _onChange: InputProps['onChange'] = (e) => {
    let { value } = e.target

    if (!/^\d*\.?\d*$/.test(value)) {
      return
    }
    // 最多小数点后两位
    if (place2 && /\.\d{3,}$/.test(value)) {
      return
    }
    if (/^0+[1-9]/.test(value)) {
      value = value.replace(/^0+/, '')
    }
    if (/^0{2,}/.test(value)) {
      value = value.replace(/^0{2,}/, '0')
    }
    onChange && onChange(value)
  }

  return <Input {...rest} id={id} onChange={_onChange} />
}

export default InputNumber
