import React from 'react'
import { Icon } from 'antd'
import Drawer, { RcDrawerProps } from 'rc-drawer'
import { ClassNames } from '@emotion/core'

import { drawerWrapperStyle, HandlerContainer, DrawerBody, drawerStyle } from './style'
import { Icon as IconSvg } from '~/components/icon'
import 'rc-drawer/assets/index.css'

const Handler: React.SFC<{
  open: boolean
  onClick: () => void
}> = (props) => (
  <HandlerContainer className="home-menu" onClick={props.onClick}>
    <IconSvg type={props.open ? "close-menu" : "menu"} viewBox={void 0} width="31" height="31" />
  </HandlerContainer>
)

const MobileDrawer: React.SFC<RcDrawerProps & {
  open: boolean
  onClick: () => void
}> = (props) => {
  const { children, title, open, onClick, ...restProps } = props
  return (
    <ClassNames>
      {({ css, cx }) => (
        <Drawer
          open={open}
          wrapperClassName={cx(css(drawerWrapperStyle))}
          className={cx(css(drawerStyle))}
          placement="right"
          closable
          level={null}
          width={'100%'}
          height={375}
          mask={false}
          showMask={false}
          handler={
            <div className="drawer-handle" style={{
              width: 31,
              height: 31,
              left: open ? 'calc(100% - 50px)' : -50,
              top: open ? 21 : 24,
              position: 'absolute',
              zIndex: 1
            }}>
              <Handler open={open} onClick={onClick} />
            </div>
          }
          {...restProps}
        >
          <DrawerBody>{children}</DrawerBody>
        </Drawer>
      )}
    </ClassNames>
  )
}

export default MobileDrawer
