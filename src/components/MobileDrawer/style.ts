import styled from '@emotion/styled'
import css from '@emotion/css'

import { gap } from '~/common/styleVars'
import { forestGreen } from '~/common'

const handlerWidth = 69

export const drawerStyle = css({
  display: 'none',
  '@media only screen and (max-width: 768px)': {
    display: 'block',
  },
})

export const drawerWrapperStyle = css({
  '& .drawer-handle': {
    top: 0,
    width: handlerWidth,
    boxShadow: 'none',
    height: '100%',
    borderColor: 'transparent',
    display: 'block',
    backgroundColor: 'transparent',
  },
  '& .drawer-left .drawer-handle': {
    right: -1 * handlerWidth,
  },
})

export const HandlerContainer = styled.div((props) => ({
  width: '100%',
  height: 'calc(100% - 59px)',
  backgroundColor: props.theme.main,
  clipPath: 'url(#mobileDrawerClip)',
  color: 'white',
  i: {
    marginTop: '12px',
    marginLeft: '-2px',
  },
  '&.home-menu': {
    clipPath: 'unset',
    backgroundColor: 'transparent',
    svg: {
      display: 'block'
    },
    '@media only screen and (max-width: 768px)': {
      width: 31,
      height: 31,
      borderRadius: 5,
      backgroundColor: '#1d6f31'
    }
  }
}))

export const HandlerText = styled.div({
  marginTop: gap * 1.5,
  writingMode: 'vertical-lr',
  width: handlerWidth,
  lineHeight: `${handlerWidth}px`,
})

export const DrawerBody = styled.div({
  padding: '15px 24px 24px 30px',
  fontSize: 14,
  lineHeight: 1.5,
  wordWrap: 'break-word',
  backgroundColor: forestGreen,
  color: 'white',
  height: '100%',
})
