import { Layout } from 'antd'
import styled from '@emotion/styled'

export const PageLayoutWrapper = styled(Layout)({
  height: 'calc(100% - 59px)',
  minWidth: 1250,
  '&.mobile': {
    // minWidth: '100%',
    width: '100%',
  },
  '&.full-page': {
    minWidth: 'unset'
  }
})

export const SidebarStyle: React.CSSProperties = {
  overflow: 'auto',
  height: 'calc(100vh - 59px)',
  position: 'fixed',
  left: 0,
  background: '#F3F3F3',
  maxWidth: '260px',
  zIndex: 10,
}

export const SubMenuHandlerContainer = styled('div')({
  position: 'fixed',
  width: 50,
  height: 50,
  background: '#f0f2f5',
  cursor: 'pointer',
  left: 0,
  bottom: 0,
  zIndex: 10000,
  fontSize: '2em',
  color: 'black',
  padding: 10,
  borderRadius: 25,
  boxShadow: '1px -1px 3px 1px',
})
