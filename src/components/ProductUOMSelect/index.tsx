import React from 'react'
import { ThemeSelect } from '~/modules/customers/customers.style'
import { Icon, Select } from 'antd'
import { WholesaleProductUom } from '~/schema'

type ProductUOMSelectProps = {
  onChange: Function
  inventoryUOM: string
  wholesaleProductUomList: WholesaleProductUom[]
  [x: string]: any
}

function ProductUOMSelect(props: ProductUOMSelectProps) {
  const { onChange, inventoryUOM, wholesaleProductUomList, ...rest } = props

  const _onChange = (val: string) => {
    if (val === inventoryUOM) {
      onChange(val, 1)
      return
    }
    const obj = wholesaleProductUomList?.find((item) => {
      return item.name === val && !item.deletedAt
    })
    onChange(val, obj?.ratio)
  }

  return (
    <ThemeSelect {...rest} onChange={_onChange} suffixIcon={<Icon type="caret-down" />}>
      {inventoryUOM && (
        <Select.Option value={inventoryUOM} title={inventoryUOM}>
          {inventoryUOM}
        </Select.Option>
      )}

      {wholesaleProductUomList?.length > 0 &&
        wholesaleProductUomList.map((uom: WholesaleProductUom) => {
          if (!uom.deletedAt) {
            return (
              <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                {uom.name}
              </Select.Option>
            )
          }
          return null
        })}
    </ThemeSelect>
  )
}

export default ProductUOMSelect
