/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Menu } from 'antd'
import { withTheme } from 'emotion-theming'
import { MenuItemTextWrapper, MenuWrapper, MenuItemText, sideMenuStyle } from './sidemenu.style'
import { MenuItem } from '../PageLayout/page.model'
import { SelectParam } from 'antd/lib/menu'
import { HeaderStates } from './../header'
import { Link } from 'react-router-dom'
import { getRoutePath } from '~/common/utils'
import { history } from './../../store/history'
import { Icon } from '../icon'

type MenuProps = HeaderStates & {
  items: MenuItem[]
}

class SideMenuComponent extends React.PureComponent<MenuProps> {
  render() {
    let selectedIndex = ''
    const route = getRoutePath()
    const {items} = this.props;
    if(typeof items === 'undefined') {
      return null;
    }

    items.forEach((item, index) => {
      if (route.path.indexOf(item.path) >= 0) {
        selectedIndex = `${index + 1}`
      }
    })

    return (
      <MenuWrapper>
        <Menu mode="inline" selectedKeys={[selectedIndex]} style={sideMenuStyle} onSelect={this.onSelect}>
          {this.renderMenuItems(this.props.items)}
        </Menu>
      </MenuWrapper>
    )
  }

  renderMenuItems(data: MenuItem[]) {
    const route = getRoutePath()
    const items: any = []
    data.forEach((el, index) => {
      
      const active = el.path.replace(route.param, route.id) == history.location.pathname
     
      items.push(
        <Menu.Item key={`${index + 1}`}>
          <Link to={`${route.id ? el.path.replace(route.param, route.id) : el.path}`}>
            <MenuItemTextWrapper>
              <MenuItemText active={active}>{el.name}</MenuItemText>
              {active === true && <Icon type="arrow-right-product" viewBox='0 0 8 8' height='16' style={{ marginTop: 12 }} />}
            </MenuItemTextWrapper>
          </Link>
        </Menu.Item>,
      )
    })
    return items
  }

  onSelect = (param: SelectParam) => {
    this.setState({ activedItemIndex: param.key })
  }
}

export default withTheme(SideMenuComponent)
