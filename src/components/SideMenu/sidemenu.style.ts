import styled from '@emotion/styled'
import { white, brightGreen, darkGrey } from '~/common'

export const MenuWrapper = styled('div')((props) => ({
  letterSpacing: '0.05em',
  paddingLeft: '0px',
  '& > ul': {
    '& > li': {
      '&::after': {
        borderRight: `3px solid ${props.theme.dark} !important`,
      },
    },
  },
}))

export const buttonStyle: React.CSSProperties = {
  borderRadius: '20px',
  height: '35px',
  fontSize: '13px',
  backgroundColor: brightGreen,
  color: white,
  padding: '0 20px',
}

export const sideMenuStyle = {
  backgroundColor: '#F3F3F3',
  padding: '24px 0px',
}

export const MenuItemTextWrapper = styled('div')((props: any)=>({
  display: 'flex',
  justifyContent: 'space-between',
  marginLeft: 66,
  '& svg path': {
    fill: props.theme.dark
  }
}))

interface ActiveProps {
  active: boolean
}

export const MenuItemText = styled('div')<ActiveProps>((props) => ({
  color: props.active ? props.theme.dark : darkGrey,
  fontSize: '14px',
  fontWeight: 'bold',
  '& + i': {
    color: props.active ? props.theme.dark : darkGrey,
  },
}))
