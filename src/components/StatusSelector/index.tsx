import React from 'react'
import { Dropdown, Menu } from 'antd'
import { ClickParam } from 'antd/lib/menu'

const statusList = ['PLACED', 'DELIVERED', 'RECEIVED']

const statusSelector: React.SFC<{
  onClick: (status: string) => void;
  children: React.ReactNode;
}> = (props) => {
  const onClick = ({ key }: ClickParam) => { props.onClick(key) }
  const statusOverlay = (
    <Menu onClick={onClick}>
      {statusList.map((s) => (
        <Menu.Item key={s}>{s}</Menu.Item>
      ))}
    </Menu>
  )

  return (
    <Dropdown overlay={statusOverlay} trigger={['click']}>
      {props.children}
    </Dropdown>
  )
}

export default React.memo(statusSelector)
