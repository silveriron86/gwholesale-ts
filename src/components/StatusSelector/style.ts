import styled from '@emotion/styled'

import { leafGreen, gray01 } from '~/common'
import { hex2rgba } from '~/common/utils'

export const TagContainer = styled.span`
  background-color: ${hex2rgba(leafGreen, 0.4)};
  border-radius: 10px;
  padding: 4px 12px;
  color: ${gray01};
`
