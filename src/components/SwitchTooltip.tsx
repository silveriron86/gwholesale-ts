import React from 'react'
import { Switch, Tooltip } from 'antd'
import styled from '@emotion/styled'

const ThemeSwitch: any = styled(Switch)((props) => ({
  '&.ant-switch-checked': {
    backgroundColor: props.theme.theme,
  },
}))

type Props = {
  showTip: boolean
  tooltipProps: React.ComponentProps<typeof Tooltip>
  switchProps: React.ComponentProps<typeof Switch>
}

export default function SwitchTooltip(props: Props) {
  const { showTip, tooltipProps, switchProps } = props
  const switchComp = <ThemeSwitch {...switchProps} />

  if (!showTip) {
    return switchComp
  }

  return (
    <Tooltip {...tooltipProps}>
      <div style={{ padding: '8px' }}>{switchComp}</div>
    </Tooltip>
  )
}
