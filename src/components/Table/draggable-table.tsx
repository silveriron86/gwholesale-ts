import * as React from 'react'
import { Table } from 'antd'
import { DndProvider, DragSource, DropTarget, DropTargetMonitor } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import update from 'immutability-helper'
import { ExpandableTable } from '~/modules/manufacturing/processing-list/processing-list.style'
import { cloneDeep } from 'lodash'
interface DragSortingTableProps {
  dataSource: any[]
  columns: any[]
  loading: boolean
  pagination: any
  expandIcon: any
  expandIconColumnIndex: number
  expandIconAsCell: boolean
  defaultExpandAllRows: boolean
  onRow: any
  expandedRowRender: any
  draggable: boolean
  stationSettingId: number
  totalOrders: any[]
  reassignStationSettingId: Function
  updateWorkOrderDisplayNumber: Function
}

let dragingIndex = -1
let draggingStationId = -1
let draggingOrderId = -1

class BodyRow extends React.PureComponent {
  render() {
    const { isOver, connectDragSource, connectDropTarget, moveRow, ...restProps } = this.props
    const style = { ...restProps.style }

    let { className } = restProps
    if (isOver && draggingOrderId != -1 && typeof draggingOrderId != 'undefined') {
      if (draggingStationId == restProps.stationSettingId) {
        if (restProps.index > dragingIndex) {
          className += ' drop-over-downward'
        }
        if (restProps.index < dragingIndex) {
          className += ' drop-over-upward'
        }
      } else {
        className += ' drop-over-upward'
      }
    }

    return connectDragSource(connectDropTarget(<tr {...restProps} className={className} style={style} />))
  }
}

const rowSource = {
  beginDrag(props: any) {
    dragingIndex = props.index
    draggingStationId = props.stationSettingId
    draggingOrderId = props.orderId
    return {
      index: props.index,
      stationSettingId: props.stationSettingId,
      orderId: props.orderId,
    }
  },
}

const rowTarget = {
  drop(props: any, monitor: DropTargetMonitor) {
    const dragIndex = monitor.getItem().index
    const hoverIndex = props.index
    const hoverInfo = {
      index: props.index,
      stationSettingId: props.stationSettingId,
      orderId: props.orderId,
    }
    // Don't replace items with themselves
    if (dragIndex === hoverIndex && draggingStationId == hoverInfo.stationSettingId) {
      return
    }

    // Don't allow to move for empty row
    if (!monitor.getItem().orderId) {
      return
    }

    // Time to actually perform the action
    props.moveRow(monitor.getItem(), hoverInfo)

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex
  },
}

const DragableBodyRow = DropTarget('row', rowTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
}))(
  DragSource('row', rowSource, (connect) => ({
    connectDragSource: connect.dragSource(),
  }))(BodyRow),
)

class DragSortingTable extends React.Component<DragSortingTableProps> {
  state = {
    data: this.props.dataSource,
  }

  componentWillReceiveProps(nextProps: DragSortingTableProps) {
    if (this.props.dataSource && nextProps.dataSource && this.props.dataSource.length != nextProps.dataSource.length) {
      this.setState({ data: nextProps.dataSource })
    }
  }

  components = {
    body: {
      row: DragableBodyRow,
    },
  }

  moveRow = (dragInfo: any, hoverInfo: any) => {
    if (!dragInfo || !hoverInfo) return
    if (dragInfo.stationSettingId == hoverInfo.stationSettingId) {
      const { data } = this.state
      const dragRow = data[dragInfo.index]

      this.setState(
        update(this.state, {
          data: {
            $splice: [
              [dragInfo.index, 1],
              [hoverInfo.index, 0, dragRow],
            ],
          },
        }),
        () => {
          const displayOrderData = this.state.data.map((el: Order, index: number) => {
            return { wholesaleOrderId: el.wholesaleOrderId, displayOrder: index + 1 }
          })
          this.props.updateWorkOrderDisplayNumber(displayOrderData)
        },
      )
    } else {
      this.props.reassignStationSettingId(dragInfo, hoverInfo)
    }
  }

  render() {
    const {
      draggable,
      pagination,
      loading,
      columns,
      onRow,
      stationSettingId,
      expandIcon,
      expandIconColumnIndex,
      expandIconAsCell,
      defaultExpandAllRows,
      expandedRowRender,
    } = this.props
    const { data } = this.state
    const expandData = cloneDeep(data)
    expandData.push({})
    return draggable ? (
      <DndProvider backend={HTML5Backend}>
        <ExpandableTable
          columns={columns}
          expandIcon={expandIcon}
          expandIconColumnIndex={expandIconColumnIndex}
          expandIconAsCell={expandIconAsCell}
          defaultExpandAllRows={defaultExpandAllRows}
          dataSource={expandData}
          components={this.components}
          pagination={pagination}
          loading={loading}
          onRow={(record, index) => ({
            ...onRow(record, index),
            index,
            stationSettingId,
            orderId: record.wholesaleOrderId,
            moveRow: this.moveRow,
          })}
          expandedRowRender={expandedRowRender}
        />
      </DndProvider>
    ) : (
      <Table columns={columns} dataSource={this.state.data} components={this.components} pagination={pagination} />
    )
  }
}

export default DragSortingTable
