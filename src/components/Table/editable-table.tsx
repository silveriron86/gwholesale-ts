import React from 'react'
import { EditableFormRow, EditableCell } from '~/components/Table'
import { TableEventListeners } from 'antd/lib/table'
import { ThemeTable, ThemeModal, ThemeIconButton, ThemeIcon } from '~/modules/customers/customers.style'
import { ResizeableTitle } from '../muti-sort-table'

interface EditableTableProps {
  columns: Array<any>
  dataSource: Array<any>
  handleSave: Function
  handleSelectProduct?: Function
  rowKey?: string
  showHeader?: boolean
  pagination?: any
  allProps?: any
  className?: string
  isExpandable?: boolean
  expandedRowRender?: any
  footer?: any
  render?: any
  onRow: (record: any, index: number) => TableEventListeners
}

export default class EditableTable extends React.PureComponent<EditableTableProps> {
  state = {
    editDlgVisible: false, //UOM
  }

  handleCancelUOM = () => {
    this.setState({
      editDlgVisible: false,
    })
  }

  onEditSelect = () => {
    this.setState({
      editDlgVisible: true,
    })
  }

  retCol = (col: any) => {
    const { handleSave, handleSelectProduct, allProps } = this.props
    if (!col.editable) {
      return col
    }
    return {
      ...col,
      onCell: (record: any) => ({
        record,
        editable: col.editable,
        edit_width: col.edit_width,
        type: col.type,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave: handleSave,
        handleselectproduct: handleSelectProduct,
        allProps,
        onEditUOM: this.onEditSelect,
      }),
    }
  }

  render() {
    const {
      dataSource,
      rowKey,
      showHeader,
      pagination,
      className,
      expandedRowRender,
      isExpandable,
      footer,
      onRow,
      tableLoading,
      render
    } = this.props

    const components = {
      header: {
        cell: ResizeableTitle,
      },
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    }

    const columns = this.props.columns.map((colGroup: any) => {
      if (colGroup.children) {
        colGroup.children = colGroup.children.map((col: any) => {
          return this.retCol(col)
        })
      } else {
        return this.retCol(colGroup)
      }

      return {
        ...colGroup,
      }
    })

    const expandButton = (props: any) => (
      <ThemeIconButton
        className="bold-blink"
        shape="round"
        onClick={props.onExpand}
        type="link"
        style={{ marginRight: 3 }}
      >
        <ThemeIcon type={props.expanded === true ? 'up' : 'down'} />
      </ThemeIconButton>
    )
    return (
      <>
        <ThemeTable
          showHeader={showHeader}
          pagination={typeof pagination !== 'undefined' ? pagination : { hideOnSinglePage: true, pageSize: 12 }}
          components={components}
          columns={columns}
          dataSource={dataSource}
          rowKey={rowKey ? rowKey : 'id'}
          className={className ? className : ''}
          onRow={onRow}
          bordered
          expandIcon={isExpandable ? expandButton : null}
          expandIconAsCell={false}
          expandedRowRender={isExpandable ? expandedRowRender : null}
          footer={footer ? footer : null}
          loading={tableLoading}
          render={render ? render : null}
        />
        {typeof this.props.allProps !== 'undefined' && (
          <ThemeModal
            title={`Edit Value List "Unit of Measurement"`}
            visible={this.state.editDlgVisible}
            onCancel={this.handleCancelUOM}
            cancelText="Close"
            okButtonProps={{ style: { display: 'none' } }}
          >
            <this.props.allProps.TypeEditWrapper
              isModal={true}
              field="unitOfMeasure"
              title="Unit of Measurement"
              buttonTitle="Add New UOM"
              {...this.props.allProps}
            />
          </ThemeModal>
        )}
      </>
    )
  }
}
