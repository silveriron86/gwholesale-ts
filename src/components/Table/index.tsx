import { Input, Form, Select, Divider } from 'antd'
import React from 'react'
import jQuery from 'jquery'
// import SearchTable from './search-table'
import { ThemeModal, ThemeTextArea, ThemeSelect } from '~/modules/customers/customers.style'
import ProductModal from '~/modules/customers/sales/cart/modals/product-modal'

const { Option } = Select
const EditableContext = React.createContext({})

const EditableRow = ({ form, index, ...props }: any) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
)

interface EditableCellProps {
  record: any
  handleSave: Function
  handleselectproduct: Function
  children: any
  dataIndex: number | string
  title: string
  index: number
  editable: boolean
  edit_width?: number | string
  type?: string
}

export const EditableFormRow = Form.create()(EditableRow)

export class EditableCell extends React.Component<EditableCellProps> {
  state = {
    editing: false,
    visibleSelect: false,
    visibleSearch: false,
    editDlgVisible: false, //UOM
    selectItems: ['Spoiled', 'Poor Quality', 'Did Not Order'],
  }

  isClickedEdit = false
  input: any
  form: any
  selectNode: any

  componentDidUpdate() {
    jQuery('.ant-table-tbody .ant-btn.tab-able')
      .unbind()
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9) {
          const formItem = jQuery(e.currentTarget)
          const wrapper = formItem.parents('.ant-table-tbody')
          let tdIndex = wrapper.find('td').index(formItem.parents('td'))
          this.processTab(wrapper, tdIndex, e)
        }
      })
  }

  setTabFocus() {
    setTimeout(() => {
      let wrapper = jQuery('td[type="modal"]').parents('tbody')
      let els = wrapper.find('.tab-able')
      let savedTabIndex = localStorage.getItem('TABLE-TAB-INDEX')
      if (savedTabIndex) {
        let index = parseInt(savedTabIndex, 10)
        if (els.length > index) jQuery(els[index]).focus()
      }
    }, 50)
  }

  processTab(wrapper, tdIndex, e) {
    setTimeout(() => {
      let els = wrapper.find('.tab-able')
      let tables = jQuery('.ant-table-wrapper')
      // els = els.filter((index: number, el: any) => {
      //   const td = jQuery(el).parents('td');
      //   return td.attr('type') !== 'modal'
      // })

      if (els.length === 1 && tables.length == 1) {
        return
      }

      let found = 0
      els.map((index: number, el: any) => {
        const td = jQuery(el).parents('td')
        if (wrapper.find('td').index(td) === tdIndex) {
          found = index
        }
      })
      let newIndex = found
      if (e.shiftKey) {
        newIndex--
        // newIndex = newIndex < 0 ? els.length - 1 : newIndex
      } else {
        newIndex++
        // newIndex = newIndex >= els.length ? 0 : newIndex
      }

      // if (newIndex < 0) {
      //   if (jQuery('.header-last-tab input').length) {
      //     //when last tab is search input = purchase order
      //     jQuery('.header-last-tab input').focus()
      //   } else if (tables.length == 1 || els.length > 0 && jQuery(els[0]).parents('.sales-order-table').length) {
      //     // slaes order
      //     jQuery('.header-last-tab').focus()
      //   } else if (tables.length > 1 && els.length > 0 && jQuery(els[0]).parents('.one-off-table').length) {
      //     // slaes order
      //     els = jQuery('.sales-order-table .tab-able')
      //     console.log('move to first table', els)
      //     if (els.length > 0) {
      //       this.activateEl(els, els.length - 1)
      //     } else {
      //       jQuery('.header-last-tab').focus()
      //     }
      //   }
      // } else if (newIndex >= els.length) {
      //   if (tables.length == 1 || els.length > 0 && jQuery(els[0]).parents('.one-off-table').length) {
      //     if (jQuery('.ant-input').length > 0) {
      //       jQuery('.ant-input')
      //         .first()
      //         .focus()
      //       jQuery('html, body').animate(
      //         {
      //           scrollTop: 0,
      //         },
      //         100,
      //       )
      //     }
      //   } else if (tables.length > 1 && els.length > 0 && jQuery(els[0]).parents('.sales-order-table').length) {
      //     els = jQuery('.one-off-table .tab-able')
      //     console.log('move to second table', els)
      //     if (els.length > 0) {
      //       this.activateEl(els, 0)
      //     }
      //   }
      // } else {
      this.activateEl(els, newIndex)
      // }
    }, 50)
  }

  activateEl = (els: any[], index: number) => {
    if (
      jQuery(els[index]).hasClass('ant-btn') ||
      jQuery(els[index])
        .parent()
        .attr('type') === 'modal'
    ) {
      // if modal or button, only focus
      localStorage.setItem('TABLE-TAB-INDEX', index.toString())
      jQuery(els[index]).focus()
    } else {
      jQuery(els[index]).trigger('click')
      jQuery(els[index])
        .find('input')
        .focus()
    }
  }

  toggleEdit = (type: any, event: any) => {
    if (window.localStorage.getItem('CLICKED-INDEX') == 'no_value') {
      let wrapper = jQuery(event.target).parents('tbody')
      const editableEls = wrapper.find('.tab-able')
      let clickedIndex = editableEls.index(event.target)
      console.log(event.target)
      if (!jQuery(event.target).hasClass('tab-able')) {
        const parentTd = jQuery(event.target).parents('td')
        if (parentTd.length) {
          const tabable = jQuery(parentTd[0]).find('.tab-able')
          console.log(tabable)
          if (tabable.length) {
            clickedIndex = editableEls.index(tabable[0])
          }
        }
      }
      console.log('clicked index = ', clickedIndex)
      if (clickedIndex > -1) {
        console.log('clicked index', clickedIndex)
        window.localStorage.setItem('CLICKED-INDEX', `${clickedIndex}`)
      }
    }

    if (this.isClickedEdit) {
      this.isClickedEdit = false
      return
    }

    const editing = !this.state.editing
    this.setState({ editing }, () => {
      // Picked cannot happen when there is no lot
      if (editing && (this.props.dataIndex !== 'picked' || this.props.record.lotId)) {
        if (type === 'modal') {
          if (
            this.props.dataIndex === 'variety' &&
            this.props.record.status !== 'PLACED' &&
            this.props.record.status !== 'NEW'
          ) {
            // Don't allow delete or update item id after picking is started
            this.setState({
              editing: false,
              visibleSearch: false,
            })
          } else {
            this.setState({
              visibleSearch: true,
            })
          }
        } else {
          let isAllowed = this.checkAllowedEdit()
          if (isAllowed === false) {
            this.setState({
              editing: false,
            })
          } else if (this.input) {
            this.input.focus()
            this.input.select()
          } else if (this.selectNode) {
            this.selectNode.focus()
          }

          jQuery('.ant-table-tbody .ant-form-item')
            .unbind()
            .bind('keydown', (e: any) => {
              if (e.keyCode === 9 || e.keyCode == 13) {
                const formItem = jQuery('.ant-table-tbody .ant-form-item')
                if (formItem.find('.ant-select').length) {
                  // For select box, to avoid enter key
                  if (e.keyCode === 13) {
                    return
                  }
                }
                const wrapper = formItem.parents('.ant-table-tbody')

                let tdIndex = wrapper.find('td').index(formItem.parents('td'))
                this.processTab(wrapper, tdIndex, e)
              }
            })
        }
      }
    })
  }

  checkAllowedEdit = () => {
    if (
      this.props.record.isLocked &&
      this.props.dataIndex !== 'return_expired_quantity' &&
      this.props.dataIndex !== 'return_quantity' &&
      this.props.dataIndex !== 'return_reason' &&
      this.props.dataIndex !== 'return_net_weight'
    ) {
      return false
    }
    let isAllowed = true
    //special handle
    if (
      (this.props.record.status != null &&
        this.props.record.status !== 'PLACED' &&
        this.props.record.status !== 'NEW' &&
        this.props.record.status !== 'CONFIRMED' && // for purchase order item. when status is confirmed quantity should be editable
        this.props.record.status !== 'RECEIVED' &&
        this.props.record.itemId) ||
      (this.props.record.orderStatus != null && this.props.record.orderStatus !== 'REQUEST')
    ) {
      if (this.props.dataIndex === 'price' || this.props.dataIndex === 'quantity' || this.props.dataIndex === 'UOM') {
        isAllowed = false
      }
    }

    if (
      this.props.record.status == 'PICKING' ||
      this.props.record.status == 'SHIPPED' ||
      this.props.record.status == 'CANCEL'
    ) {
      if (this.props.dataIndex === 'picked') {
        isAllowed = false
      }
    }

    //for credit memo
    if (this.props.record.status != 'SHIPPED' && this.props.record.status != 'RECEIVED') {
      if (
        this.props.dataIndex === 'return_expired_quantity' ||
        this.props.dataIndex === 'return_quantity' ||
        this.props.dataIndex === 'return_reason' ||
        this.props.dataIndex === 'return_net_weight'
      ) {
        isAllowed = false
      }
    }
    /*
    if (this.props.record.status == 'RECEIVED') {
      if (
        this.props.dataIndex === 'modifiers' ||
        this.props.dataIndex === 'carrier' ||
        this.props.dataIndex === 'qtyConfirmed' ||
        this.props.dataIndex === 'cost' ||
        this.props.dataIndex === 'freight' ||
        this.props.dataIndex === 'margin' ||
        this.props.dataIndex === 'salesPrice' ||
        this.props.dataIndex === 'extraOrigin'
      ) {
        isAllowed = false
      }
    }
    */

    if (this.props.dataIndex === 'cost' && this.props.record.pas === true) {
      isAllowed = true
    }

    if (this.props.record.status == 'RECEIVED') {
      if (this.props.dataIndex === 'palletQty') {
        isAllowed = false
      }
    }

    //for work order
    if (this.props.record.orderStatus != 'RECEIVED' && this.props.record.orderStatus != 'WIP') {
      if (this.props.dataIndex === 'unitsActual' || this.props.dataIndex === 'weightActual') {
        isAllowed = false
      }
    }

    if (this.props.record.orderStatus != 'REQUEST' && this.props.record.orderStatus != 'NEW') {
      if (this.props.dataIndex === 'unitsPlanned') {
        isAllowed = false
      }
    }

    return isAllowed
  }

  addSelectItem = () => {
    this.setState({
      visibleSelect: true,
    })
  }

  handleSelectCancel = () => {
    this.setState({
      visibleSelect: false,
    })
  }

  handleSelectOk = () => {
    this.setState({
      visibleSelect: false,
    })
  }

  handleSearchOk = () => {
    this.setState({
      editing: false,
      visibleSearch: false,
    })

    // Tab Order
    this.setTabFocus()
  }

  save = (e: { currentTarget: { id: string | number } }) => {
    const { record, handleSave } = this.props
    this.form.validateFields((error: { [x: string]: any }, values: any) => {
      if (error && error[e.currentTarget.id]) {
        return
      }

      let data = { ...record, ...values }
      if (e.currentTarget.id === 'unitWeight') {
        // for catch weight update, need to save the row index
        const tr = jQuery('.quantity-detail-table .ant-table-body .ant-table-row input[type="text"]').parents(
          '.ant-table-row',
        )
        const rowIndex = jQuery(tr[0]).prevAll().length
        data = { ...record, ...values, ...{ rowIndex } }
      }
      this.toggleEdit(null, e)
      handleSave(data, e.currentTarget.id)
    })
  }

  saveSelect = (v: any) => {
    const values = {
      return_reason: v,
    }
    const { record, handleSave } = this.props
    handleSave({ ...record, ...values })
  }

  saveUOM = (v: any) => {
    const values = {
      UOM: v,
    }
    const { record, handleSave } = this.props
    handleSave({ ...record, ...values })
  }

  savePurchaseModifiers = (val: any) => {
    const values = {
      modifiers: val,
    }
    const { record, handleSave } = this.props
    handleSave({ ...record, ...values })
  }

  savePurchaseOrigin = (val: any) => {
    const values = {
      extraOrigin: val,
    }
    const { record, handleSave } = this.props
    handleSave({ ...record, ...values })
  }

  renderModifiersList = (record: any) => {
    if (!record.suppliers) return []
    console.log(record)
    const currentSuppliers = record.suppliers.split(', ').map((el: string) => el.trim())
    let result: any[] = []
    currentSuppliers.forEach((el: any, index: number) => {
      result.push(
        <Select.Option key={index} value={el} className="select-option">
          {el}
        </Select.Option>,
      )
    })
    return result
  }

  handleselectproduct = (record: any, item: any) => {
    this.props.handleselectproduct(record, item)
    this.handleSearchOk()
  }

  toggleCompanyName = () => {
    this.setState({
      editing: !this.state.editing,
    })
  }

  renderCell = (form: any) => {
    this.form = form
    const { children, dataIndex, record, title, edit_width, type } = this.props
    const { editing, selectItems, visibleSelect, visibleSearch } = this.state

    if (dataIndex === 'companyName') {
      return editing ? (
        <div className="editable-cell-value-wrap" onBlur={this.toggleCompanyName}>
          {children}
        </div>
      ) : (
        <div className="editable-cell-value-wrap" onClick={this.toggleCompanyName}>
          {record.companyName}
        </div>
      )
    }

    let isEditableUOM =
      dataIndex !== 'UOM' ||
      type === 'one-off' ||
      (dataIndex === 'UOM' &&
        record.baseUOM != record.inventoryUOM &&
        (this.props.record.status == 'PLACED' ||
          this.props.record.status == 'NEW' ||
          this.props.record.orderStatus == 'REQUEST') &&
        record.constantRatio) ||
      (!record.constantRatio && record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0)
    if (editing) {
      if (!isEditableUOM) {
        return <span>{record.UOM}</span>
      }
    }

    let isTabAbleWithCondition = true
    if (type === 'modal') {
      if (
        this.props.dataIndex === 'variety' &&
        this.props.record.status !== 'PLACED' &&
        this.props.record.status !== 'NEW'
      ) {
        isTabAbleWithCondition = false
      }
    } else {
      isTabAbleWithCondition = this.checkAllowedEdit()
    }

    let showEmptyString = false
    if (type === 'number') {
      if (
        this.props.dataIndex === 'qtyConfirmed' &&
        (this.props.record.status === 'NEW' || this.props.record.status === 'PLACED')
      ) {
        showEmptyString = true
      }
      if (this.props.dataIndex === 'receivedQty' && this.props.record.status === 'CONFIRMED') {
        // showEmptyString = true
      }
    }
    // Picked cannot happen when there is no lot
    return editing && (dataIndex !== 'picked' || record.lotId) ? (
      <Form.Item style={{ margin: 0, width: '100%' }}>
        {form.getFieldDecorator(dataIndex, {
          // rules: [
          //   {
          //     required: true,
          //     message: `${title} is required.`,
          //   },
          // ],
          getValueFromEvent: (event: any) => {
            if (!event.target) {
              return event
            }
            if (type == 'number') {
              return event.target.value.replace(/^\D*(\d*(?:\.\d{0,2})?).*$/g, '$1')
            } else {
              return event.target.value
            }
          },
          initialValue: showEmptyString ? '' : record[dataIndex],
        })(
          dataIndex === 'UOM' ? (
            <ThemeSelect
              initialValue={record[dataIndex]}
              ref={(node: any) => (this.selectNode = node)}
              onBlur={this.toggleEdit.bind(this, null)}
              onChange={this.saveUOM}
              showSearch
              optionFilterProp="children"
              onSearch={() => { }}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              dropdownRender={(menu: any) => (
                <div>
                  {menu}
                  {/* <EditA onMouseDown={e => e.preventDefault()} onClick={() => this.props.onEditUOM()}>Edit...</EditA> */}
                </div>
              )}
              style={{ width: '100%' }}
              dropdownStyle={{ minWidth: 110 }}
            >
              {type === 'one-off' &&
                this.props.allProps.companyProductTypes.unitOfMeasure &&
                this.props.allProps.companyProductTypes.unitOfMeasure.length > 0
                ? this.props.allProps.companyProductTypes.unitOfMeasure.map(
                  (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                    return (
                      <Select.Option key={index} value={item.name} className="select-option">
                        {item.name}
                      </Select.Option>
                    )
                  },
                )
                : null}
              {type !== 'one-off' && record.baseUOM != null && record.baseUOM != '' && record.constantRatio ? (
                <Select.Option key={record.baseUOM} value={record.baseUOM} className="select-option">
                  {record.baseUOM}
                </Select.Option>
              ) : (
                ''
              )}
              {type !== 'one-off' && record.inventoryUOM != null && record.inventoryUOM != '' ? (
                <Select.Option key={record.inventoryUOM} value={record.inventoryUOM} className="select-option">
                  {record.inventoryUOM}
                </Select.Option>
              ) : (
                ''
              )}
              {type !== 'one-off' && record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                ? record.wholesaleProductUomList.map((uom) => (
                  <Select.Option key={uom.name} value={uom.name} className="select-option">
                    {uom.name}
                  </Select.Option>
                ))
                : ''}
            </ThemeSelect>
          ) : type === 'select' ? (
            <>
              <ThemeSelect
                style={{ width: edit_width ? edit_width : '100%' }}
                ref={(node: any) => (this.selectNode = node)}
                onBlur={this.toggleEdit.bind(this, null)}
                onChange={this.saveSelect}
                initialValue={record[dataIndex]}
                // tslint:disable-next-line:jsx-no-lambda
                dropdownRender={(menu: React.ReactNode) => (
                  <div>
                    {menu}
                    <Divider style={{ margin: '4px 0' }} />
                    <div
                      style={{ padding: '4px 8px', cursor: 'pointer' }}
                      onMouseDown={(e) => {
                        e.preventDefault()
                        this.isClickedEdit = true
                      }}
                      onClick={this.addSelectItem}
                    >
                      Edit...
                    </div>
                  </div>
                )}
              >
                {selectItems.map((item) => (
                  <Option key={item}>{item}</Option>
                ))}
              </ThemeSelect>

              <ThemeModal
                title='Edit Value List "CreditMemo | Return Reasons"'
                visible={visibleSelect}
                onOk={this.handleSelectOk}
                onCancel={this.handleSelectCancel}
              >
                <ThemeTextArea rows={4} value={selectItems.join('\n') + '\n'} />
              </ThemeModal>
            </>
          ) : type === 'modal' ? (
            <>
              {children}
              {title === 'PRODUCT NAME' ? (
                <ThemeModal
                  closable={false}
                  keyboard={true}
                  okText="Close [esc]"
                  okButtonProps={{ shape: 'round' }}
                  cancelButtonProps={{ style: { display: 'none' } }}
                  width={1000}
                  visible={visibleSearch}
                  onOk={this.handleSearchOk}
                  onCancel={this.handleSearchOk}
                >
                  <ProductModal
                    hasTabs={true}
                    isAdd={false}
                    selected={record.variety.toLowerCase()}
                    id={record.itemId}
                    onSelect={this.handleselectproduct.bind(this, record)}
                  />
                </ThemeModal>
              ) : null}
            </>
          ) : dataIndex === 'modifiers' ? (
            <ThemeSelect
              initialValue={record[dataIndex]}
              ref={(node: any) => (this.selectNode = node)}
              onBlur={this.toggleEdit.bind(this, null)}
              onChange={this.savePurchaseModifiers}
              dropdownRender={(menu: any) => <div>{menu}</div>}
              style={{ width: '100%' }}
              dropdownStyle={{ minWidth: 110 }}
              showSearch
              filterOption={(input: any, option: any) => {
                if (option.props.children) {
                  return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                } else {
                  return true
                }
              }}
            >
              {this.renderModifiersList(record)}
            </ThemeSelect>
          ) : (
            <Input
              style={{ width: edit_width ? edit_width : '100%', height: 40 }}
              ref={(node) => (this.input = node)}
              onPressEnter={this.save}
              onBlur={this.save}
            />
          ),
        )}
      </Form.Item>
    ) : (
      <div
        className={`editable-cell-value-wrap ${(dataIndex !== 'picked' || record.lotId) && type !== 'modal' && isTabAbleWithCondition && isEditableUOM
            ? 'tab-able'
            : ''
          }`}
        onClick={this.toggleEdit.bind(this, type)}
      >
        {children}
      </div>
    )
  }

  render() {
    const { editable, dataIndex, title, record, index, handleSave, children, ...restProps } = this.props
    return (
      <td {...restProps}>
        {editable ? <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer> : children}
      </td>
    )
  }
}
