import React from 'react'
import { Flex, ThemeInput } from '~/modules/customers/customers.style'
import { Icon } from 'antd'

interface SearchInputProps {
  placeholder: string
  defaultValue?: string
  handleSearch: Function
  className?: string
  withApiCall?: boolean
}
class SearchInput extends React.PureComponent<SearchInputProps> {
  state: any
  timeoutHandler?: NodeJS.Timeout
  constructor(props: SearchInputProps) {
    super(props)
    this.state = {
      filterName: props.defaultValue ? props.defaultValue : '',
    }
  }

  handleSearch = (e: { target: { value: any } }) => {

    const v = e.target.value
    this.setState({
      filterName: v,
    })
    if (typeof this.props.withApiCall != 'undefined' && this.props.withApiCall === false) {
      this.props.handleSearch(v.toLowerCase(), v)
    } else {
      if (this.timeoutHandler) {
        clearTimeout(this.timeoutHandler)
      }

      this.timeoutHandler = setTimeout(() => {
        this.props.handleSearch(v.toLowerCase())
      }, 200)
    }
  }
  onClearSearch = (evt: any) => {
    evt.stopPropagation()
    this.setState({
      filterName: ''
    }, () => {
      this.props.handleSearch('')
    })
  }
  componentWillReceiveProps(nextProps: SearchInputProps) {
    const { defaultValue } = nextProps
    //if product modal close need reset input value
    //when you not set default value, then need add typeOf judgment to work for not set defualtvalue
    //but cann't reset input value in close modal
    if (typeof (defaultValue) != "undefined" && !defaultValue) {
      this.setState({
        filterName: '',
      })
    }
  }

  render() {
    const { placeholder, className } = this.props
    return (
      <ThemeInput
        type="text"
        suffix={
          <Flex>
            <Icon className={`clear-icon ${this.state.filterName ? 'show' : 'hide'}`} onClick={this.onClearSearch} viewBox="0 0 16 16" width="16" height="16" type="close-circle" />
            <Icon type="search" />
          </Flex>
        }
        value={this.state.filterName}
        placeholder={placeholder}
        onChange={this.handleSearch}
        className={className ? className : ''}
      />
    )
  }
}

export default SearchInput
