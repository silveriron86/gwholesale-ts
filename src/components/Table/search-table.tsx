import React from 'react'
import { Table } from 'antd'
import { cloneDeep } from 'lodash'
import SearchInput from './search-input'
import { Incoming, FlexCenter } from '~/modules/customers/customers.style'

interface SearchTableProps {}

const testData = [
  {
    id: 1,
    date: '1/9/2019',
    lot: '123-456-789',
    sku: '0028',
    product_name: 'TEST 1',
  },
  {
    id: 2,
    date: '1/10/2019',
    lot: '222-456-789',
    sku: '0029',
    product_name: 'TEST 2',
  },
]

export class SearchTable extends React.PureComponent<SearchTableProps> {
  state = {
    initItems: cloneDeep(testData),
    searchStr: '',
  }

  onSearch = (text: string) => {
    this.setState({
      searchStr: text,
    })
  }

  render() {
    const { initItems, searchStr } = this.state
    const listItems = cloneDeep(initItems)

    const filteredItems = listItems.filter((row) => {
      return (
        row.date.indexOf(searchStr) >= 0 ||
        row.lot.indexOf(searchStr) >= 0 ||
        row.sku.indexOf(searchStr) >= 0 ||
        row.product_name.toLowerCase().indexOf(searchStr) >= 0
      )
    })

    const columns = [
      {
        title: 'Date',
        dataIndex: 'date',
        render: (text: string, _record: any, index: number) => {
          if (index % 2 === 1) {
            return (
              <FlexCenter>
                {text}
                <Incoming>Incoming</Incoming>
              </FlexCenter>
            )
          }
        },
      },
      {
        title: 'Lot #',
        dataIndex: 'lot',
      },
      {
        title: 'SKU',
        dataIndex: 'sku',
      },
      {
        title: 'Product Name',
        dataIndex: 'product_name',
      },
    ]

    return (
      <>
        <SearchInput handleSearch={this.onSearch} placeholder="Search by PRODUCT NAME, SKU, LOT#" />
        <Table
          pagination={{ hideOnSinglePage: true, pageSize: 12 }}
          columns={columns}
          dataSource={filteredItems}
          key="id"
        />
      </>
    )
  }
}

export default SearchTable
