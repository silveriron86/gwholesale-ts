/** @jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'

import { TagContainer } from './tag.style'

const tag: React.SFC<{
  onClick?: () => void
}> = (props) => {
  const { children, ...restProps } = props
  return <TagContainer {...restProps}>{children}</TagContainer>
}

export default tag
