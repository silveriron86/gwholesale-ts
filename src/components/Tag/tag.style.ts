import styled from '@emotion/styled'

import { hex2rgba } from '~/common/utils'

export const TagContainer = styled.span((props) => ({
  display: 'inline-block',
  backgroundColor: hex2rgba(props.theme.light, 0.4),
  borderRadius: 24,
  height: 24,
  lineHeight: '24px',
  paddingLeft: 12,
  paddingRight: 12,
  color: props.theme.primary,
}))
