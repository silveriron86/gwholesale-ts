import styled from '@emotion/styled'
import moment from 'moment'
import React from 'react'

import { brightGreen, DATE_FORMAT, NULL_PLACEHOLDER } from '~/common'

export const PrimaryText = styled.span`
  color: ${brightGreen};
`

export const BoldText = styled.span`
  font-weight: bold;
`

export const AmountText: React.SFC<{}> = (props) => {
  return <React.Fragment>{props.children}</React.Fragment>
}

export const DateText: React.SFC<{
  children: string
}> = (props) => {
  const $time = moment(props.children)
  const timeStr = $time.isValid() ? $time.format(DATE_FORMAT) : NULL_PLACEHOLDER

  return <React.Fragment>{timeStr}</React.Fragment>
}
