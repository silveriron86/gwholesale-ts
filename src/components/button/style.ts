import styled from "@emotion/styled";
import {Button} from "antd";

export const StatusButton = styled(Button)<{ color?: string }>`
  & > span {
    color: ${props => props.color || ""};
  }
`;
