/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { DatePicker, Form } from 'antd'
import { FormComponentProps } from 'antd/es/form'
import moment from 'moment'

import { Icon } from '~/components'

import { DateEditorWrapper, DateTitle, DatePickerStyle } from './elements.style'

export type DatePickerProps = FormComponentProps & {
  onPickerChange: Function
}

export class DatePickerComponent extends React.PureComponent<DatePickerProps> {
  getState = () => {
    return new Promise((resolve) => {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          resolve(values)
        }
        resolve()
      })
    })
  }

  onDateChange = (_dateMoment: any) => {
    this.props.onPickerChange(_dateMoment)
  }

  render() {
    const {
      form: { getFieldDecorator },
      children,
    } = this.props

    return (
      <DateEditorWrapper>
        <Form.Item>
          {getFieldDecorator('deliveryDate', {
            initialValue: moment(),
            rules: [{ type: 'object', required: true, message: 'Please select delivery date!' }],
          })(
            <DatePicker
              onChange={this.onDateChange}
              style={DatePickerStyle}
              placeholder="MM/DD/YYYY"
              format="MM/DD/YYYY"
              allowClear={false}
              suffixIcon={<Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
            />,
          )}
        </Form.Item>
        <DateTitle>{children}</DateTitle>
      </DateEditorWrapper>
    )
  }
}

export const DatePickerElement = Form.create<DatePickerProps>()(DatePickerComponent)
