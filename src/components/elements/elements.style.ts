import styled from '@emotion/styled'

import { mediumGrey, white, white6, Theme, brightGreen, lightGrey, disabledGrey, topLightGrey } from '~/common'
import { ExtraItem } from '~/components/elements'

export const DateEditorWrapper = styled('div')((props) => ({
  float: 'left',
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
  '& .ant-row': {
    marginBottom: 15,
  },
  '& .ant-calendar-picker': {
    '& .ant-calendar-picker-input': {
      border: 'none',
      padding: '4px 0',
      borderBottom: 'solid 1px black',
      background: 'none',
      borderRadius: 0,
      fontWeight: 'bold',
      '&:focus': {
        boxShadow: 'none',
      },
    },
    '& .ant-calendar-picker-icon': {
      width: '20px',
      height: '21px',
      marginTop: '-10px',
      color: props.theme.primary,
    },
  },
}))

export const DateTitle = styled('span')({
  color: mediumGrey,
  textAlign: 'left',
  fontFamily: 'Museo Sans Rounded',
})

export const DatePickerStyle: React.CSSProperties = {
  width: '200px',
}

export const InputWrapper = styled('div')((props) => ({
  display: 'flex',
  flexDirection: 'column-reverse',
  float: 'left',
  '& .ant-input': {
    border: 'none',
    padding: '4px 0',
    borderBottom: 'solid 1px black',
    background: 'none',
    borderRadius: 0,
    fontWeight: 'bold',
    '&:focus': {
      boxShadow: 'none',
    },
  },
}))

export const PayloadCardWrapper = styled('div')({
  padding: '0 15px 20px',
  width: 380,
})

const PAYLOAD_HEIGHT = 350

export const PayloadContainer = styled('div')((props) => ({
  height: `${PAYLOAD_HEIGHT}px`,
  borderRadius: '4px',
  border: '1px solid #E5E5E5',
  background: white,
  position: 'relative',
  overflow: 'auto'
}))

export const PayloadHeader = styled('div')((props) => ({
  backgroundColor: props.theme.primary,
  padding: '12px 16px',
  textAlign: 'left',
  borderTopLeftRadius: 4,
  borderTopRightRadius: 4,
}))

export const PayloadHeaderText = styled('div')({
  color: white6,
  fontSize: 18,
  fontWeight: 'bold'
})

export const PayloadBody = styled('div')({

})

export const PayloadIcon = styled('div')((props: any) => ({
  '& svg path': {
    fill: props.theme.primary
  }
}))

interface ViewTypeProps {
  viewType: boolean
}

export const ViewSelector = styled('div')({
  display: 'flex',
  // paddingLeft: 20,
})

export const ViewSwitcher = styled(ExtraItem)<ViewTypeProps>((props) => ({
  cursor: 'pointer',
  '& span': {
    color: props.viewType ? props.theme.primary : disabledGrey,
  },
  '& svg path': {
    fill: props.viewType ? props.theme.primary : disabledGrey,
  },
}))

export const Label = styled('span')<ViewTypeProps>((props) => ({
  marginLeft: 2,
  marginTop: 1,
  fontWeight: 'bold',
  fontSize: '12px !important'
}))

export const DividerWrapper = styled('div')<ViewTypeProps>((props) => ({
  '& .ant-divider': {
    margin: '0 5px',
    height: 16,
    backgroundColor: props.viewType ? topLightGrey : lightGrey,
  },
}))
