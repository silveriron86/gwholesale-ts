/**
 * customize all of the action elements such as button, dropdown etc...
 */

import styled from '@emotion/styled'
import { white, lightGrey, darkGrey } from '~/common'
import { Button, Row } from 'antd'

interface CustomButtonProps {
  disabled: boolean
  theme: any
}
export const CustomButton: any = styled(Button)<CustomButtonProps>((props) => ({
  borderRadius: 20,
  height: 35,
  fontSize: '13px',
  backgroundColor: props.disabled ? `${props.theme.light} !important` : props.theme.light,
  color: props.disabled ? 'white !important' : white,
  opacity: props.disabled ? 0.3 : 1,
  padding: '0 20px',
  outline: 'none',
  boxShadow: 'none',
  marginLeft: 13,
  '&:hover': {
    borderColor: props.theme.light,
    color: props.theme.light,
  },
  '&:focus': {
    backgroundColor: props.theme.light,
    color: 'white',
    // borderColor: props.theme.primary,
  },
  '& svg': {
    marginRight: 10
  }
  '& svg path': {
    fill: white
  }
}))

/**
 * customize all of the static elements such as span, p, label etc...
 */
export const ListGrayBoldSpan = styled.span`
  color: ${lightGrey};
  font-weight: bold;
  line-height: 108%;
  font-size: 14px;
`
export const ListGrayNormalSpan = styled.span`
  color: ${darkGrey};
  font-size: 14px;
  font-weight: bold;
`

export const DetailWrapperStyle = styled(Row)((props) => ({
  backgroundColor: props.theme.lighter,
  borderRadius: '6px',
}))

export const ExtraItem = styled('div')((props) => ({
  display: 'flex',
  alignItems: 'center',
  cursor: 'pointer',

  '& + &': {
    marginLeft: '20px',
  },
  '& > i': {
    fontSize: '18px',
    marginRight: '6px',
    color: props.theme.primary,
  },
  '& > span': {
    color: props.theme.primary,
    fontSize: '14px',
    letterSpacing: '0.05em',
  },
}))
