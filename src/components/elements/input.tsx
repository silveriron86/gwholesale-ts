/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Form, Input } from 'antd'
import { FormComponentProps } from 'antd/es/form'

import { DateTitle, InputWrapper } from './elements.style'

export class InputElement extends React.PureComponent<FormComponentProps> {
  getState = () => {
    return new Promise((resolve) => {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          resolve(values)
        }
        resolve()
      })
    })
  }

  render() {
    const {
      form: { getFieldDecorator },
      children,
    } = this.props

    return (
      <InputWrapper>
        <Form.Item>
          {getFieldDecorator('fullName', {
            rules: [{ required: true, message: 'Full Name is required!' }],
          })(<Input type="number" placeholder="Amount" style={{ width: '200px' }} />)}
        </Form.Item>
        <DateTitle>{children}</DateTitle>
      </InputWrapper>
    )
  }
}

export const CustomInput = Form.create<FormComponentProps>()(InputElement)
