/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Table, Popconfirm } from 'antd'

import { Icon as IconSvg } from '~/components/icon/'
import { PayloadContainer, PayloadCardWrapper, PayloadHeader, PayloadHeaderText, PayloadBody, PayloadIcon } from './elements.style'

export interface PayloadProps {
  data: any[],
  delete: Function
}

export class Payload extends React.PureComponent<PayloadProps> {
  columns: ({ title: string; dataIndex: string; key?: undefined; render?: undefined } | { title: string; dataIndex: string; key: string; render: (text: any, record: any) => JSX.Element })[]
  constructor(props: PayloadProps) {
    super(props)
    
    this.columns = [
      {
        title: '#',
        dataIndex: 'id',
      },
      {
        title: 'DRIVER NAME',
        dataIndex: 'driver',
      },
      {
        title: 'ROUTES',
        dataIndex: 'routeName',
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        render: (text, record) =>
          <Popconfirm title="Sure to delete?" onConfirm={() => this.props.delete(record.id)}>
            <a>
              <PayloadIcon><IconSvg
                type="close"
                viewBox="0 0 20 28"
                width={15}
                height={14}
              /></PayloadIcon>
            </a>
          </Popconfirm>
      }
    ]
  }

  handleDelete = (id: number) => {
    
  }
  render() {
    const { children, data } = this.props
    const numberOfPayloads = data.length
    
    return (
      <PayloadCardWrapper>
        <PayloadContainer style={{ position: 'relative' }}>
          <PayloadHeader>
            <PayloadHeaderText style={{ textAlign: 'center' }}>{ children }</PayloadHeaderText>
            <PayloadHeaderText style={{ position: 'absolute', right: 10, top: 14 }}>{ numberOfPayloads == 0 ? '' : numberOfPayloads }</PayloadHeaderText>
          </PayloadHeader>
          <PayloadBody>
            <Table dataSource={data} columns={this.columns} pagination={false} rowKey="id" size="small" />
          </PayloadBody>
        </PayloadContainer>
      </PayloadCardWrapper>
    )
  }
}