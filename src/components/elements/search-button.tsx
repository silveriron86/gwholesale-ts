import React from 'react'
import {Icon as IconSvg} from './../icon'

export const searchButton = (
    <React.Fragment>
      <IconSvg viewBox="0 0 16 16" width="16" height="16" type="search" />
      <span style={{ marginLeft: '9px', fontSize: '14px', lineHeight: '15px', letterSpacing: '0.05em' }}>SEARCH</span>
    </React.Fragment>
  )