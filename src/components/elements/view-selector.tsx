import * as React from 'react'
import { notification, Divider } from 'antd'
import { withTheme } from 'emotion-theming'
import { Icon } from '~/components'
import { ViewSelector, ViewSwitcher, Label, DividerWrapper } from './elements.style'

export interface ViewSelectorProps {
  onClick: Function
  viewType: string
}

export class ViewSelectorComponent extends React.PureComponent<ViewSelectorProps> {
  componentDidMount() { }

  componentWillReceiveProps(nextProps: ViewSelectorProps) { }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Go to Pricesheet below to place a new order',
      onClick: () => {
        //console.log('Notification Clicked!')
      },
    })
  }

  onSwitchingView = (param: string) => {
    this.props.onClick(param)
  }

  render() {
    const { viewType } = this.props
    let isTableView = true
    let viewBox = '0 0 20 16'
    if (viewType === 'list') {
      isTableView = false
    }
    return (
      <div style={{ position: 'absolute', left: 60, top: 20}}>
        <ViewSelector>
          <ViewSwitcher viewType={viewType === 'table'} onClick={this.onSwitchingView.bind(this, 'table')}>
            <Icon type="table" viewBox={viewBox} width={20} height={16} />
            <Label viewType={isTableView}>TABLE VIEW</Label>
          </ViewSwitcher>
          <DividerWrapper viewType={isTableView}>
            <Divider type="vertical" />
          </DividerWrapper>
          <ViewSwitcher viewType={viewType !== 'table'} onClick={this.onSwitchingView.bind(this, 'list')}>
            <Icon type="list" viewBox={viewBox} width={20} height={16} />
            <Label viewType={isTableView}>LIST VIEW</Label>
          </ViewSwitcher>
        </ViewSelector>
      </div>
    )
  }
}

export const ViewSwitcherEl = withTheme(ViewSelectorComponent)
