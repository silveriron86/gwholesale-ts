import React from 'react'
import { ErrorContainer, ErrorBody } from './style'
import { Icon } from 'antd'

class Error404 extends React.PureComponent {
  render() {
    return(
      <ErrorContainer>
        <ErrorBody className="mainbox-404">
          <div className="err-404">4</div>
          <Icon type='question-circle' className='far-404'/>
          
          <div className="err2-404">4</div>
          <div className="msg-404">Page Not Found.</div>
        </ErrorBody>
      </ErrorContainer>
    )
  }
}

export default Error404