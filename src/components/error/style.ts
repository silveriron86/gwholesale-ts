import styled from "@emotion/styled";

export const ErrorContainer = styled('div')((props: any)=>({
    width: '100vw',
    height: '100vh',
    backgroundColor: props.theme.lighter,
    paddingTop: '20%',
}))

export const ErrorBody = styled('div')((props: any)=>({
    margin: 'auto',
    height: 600,
    width: 600,
    position: 'relative',

    '& .err-404': {
        color: props.theme.primary,
        fontFamily: '"Nunito Sans", sans-serif',
        fontSize: '11rem',
        position: 'absolute',
        left: '15%',
        top: '10%'
    },
  
    '& .far-404': {
        position: 'absolute',
        fontSize: '8.5rem',
        left: '42%',
        color: props.theme.primary,
    },
  
    '& .err2-404': {
        color: props.theme.primary,
        fontFamily: '"Nunito Sans", sans-serif',
        fontSize: '11rem',
        position: 'absolute',
        left: '75%',
        top: '10%',
    },
  
    '& .msg-404': {
        textAlign: 'center',
        fontFamily: '"Nunito Sans", sans-serif',
        fontSize: '1.6rem',
        position: 'absolute',
        left: '16%',
        top: '30%',
        width: '75%'
    },
  
    '& .msg-404 a': {
        textDecoration: 'none',
        color: 'white'
    },
  
    '& .msg-404  a:hover': {
        textDecoration: 'underline'
    }
}))
  