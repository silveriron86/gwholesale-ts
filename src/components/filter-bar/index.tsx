import React, { FC, useState, memo, useEffect } from 'react'
import { StyleButton, StyleModal, ModalFooter, ModalBody, StyleCheckbox, StyleTag } from './style'
import { Icon, Button } from 'antd'
import { AvailableStatus } from '~/schema'
import { produce } from 'immer'

interface FilterProps {
  title: string
  open: boolean
  filterOptions: FilterOptions
  filterData: FilterData
  setFiltersData: (data: FilterData) => void
  setShowModal: (is: boolean) => void
  resetFilter: () => void
}

type Menu = 'status' | 'customer' | 'driver'

export interface FilterOptions {
  [key: string]: { title: string; subTitle: string; tagEnd: string; filters: Filter[] }
}

export interface Filter {
  label: string
  value: string | number
}

type AvailableStatusKeys = keyof typeof AvailableStatus
export interface FilterData {
  status: AvailableStatusKeys[]
  customer: number[]
  driver: number[]
}

export const FilterButtons: FC<FilterProps> = (props) => {
  const { title, open, filterOptions, filterData, setFiltersData, setShowModal, resetFilter } = props

  const menus: string[] = Object.keys(filterOptions)
  const [activeMenu, setActiveMenu] = useState<Menu>(menus[0] as Menu)
  const [data, setData] = useState<FilterData>(filterData)

  useEffect(() => {
    setData(filterData)
  }, [filterData])

  const openModal = () => {
    setShowModal(true)
  }

  const closeModal = () => {
    setShowModal(false)
  }

  const onChangeFiles = () => {
    setFiltersData(data)
    setShowModal(false)
  }

  const footer = (
    <ModalFooter>
      <div>
        <Button className="apply" onClick={onChangeFiles}>
          Apply
        </Button>
        <Button type="link" className="cancel" onClick={closeModal}>
          Cancel
        </Button>
      </div>
      <Button className="clear" type="link" onClick={resetFilter}>
        Clear all filters
      </Button>
    </ModalFooter>
  )

  const handleCheckChange = (value: FilterData[keyof FilterData]) => {
    setData(
      produce<FilterData>(data, (draftState) => {
        draftState[activeMenu] = value
      }),
    )
  }

  return (
    <>
      <StyleButton onClick={openModal}>
        Add a Filter <Icon type="filter" />
      </StyleButton>
      <Button type="link" icon="close" onClick={resetFilter} style={{ color: '#4A5355', marginLeft: '7px' }}>
        Clear all filters
      </Button>
      <StyleModal title={title} width="540px" visible={open} footer={footer} onCancel={closeModal}>
        <ModalBody>
          <ul>
            {menus.map((m: string) => (
              <li key={m} className={activeMenu === m ? 'active' : ''} onClick={() => setActiveMenu(m)}>
                {filterOptions[m].title}
              </li>
            ))}
          </ul>
          <section>
            <h4>{filterOptions[activeMenu].subTitle}</h4>
            <StyleCheckbox
              value={data[activeMenu]}
              options={filterOptions[activeMenu].filters}
              onChange={handleCheckChange}
            />
          </section>
        </ModalBody>
      </StyleModal>
    </>
  )
}

interface ITag {
  label: string
  key: string
  value: string | number
}
interface FilterTagsProps {
  tags: ITag[]
  onClose: (data: { key: string; value: string | number }) => void
}

export const FilterTags: FC<FilterTagsProps> = memo((props) => {
  const { tags, onClose } = props

  return (
    <>
      {tags.map((tag) => (
        <StyleTag
          key={`${tag.label}-${tag.value}`}
          closable
          onClose={() => {
            onClose({ key: tag.key, value: tag.value })
          }}
        >
          {tag.label}
        </StyleTag>
      ))}
    </>
  )
})
