import styled from '@emotion/styled'
import { Button, Modal, Checkbox, Tag } from 'antd'

export const StyleButton: any = styled(Button)((props) => ({
  color: props.theme.dark,
  fontWeight: 600,
  fontSize: '15px',
  boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
  borderRadius: '3px',
  border: '1px solid #D8DBDB',
  height: '42px',

  '&:hover, &:active, &:focus': {
    color: props.theme.primary,
  },
}))

export const StyleModal: any = styled(Modal)((props) => ({
  '& .ant-modal-body': {
    padding: 0,
  },

  '& .ant-modal-footer': {
    backgroundColor: '#F7F7F7',
  },
}))

export const ModalFooter: any = styled.div((props) => ({
  display: 'flex',
  justifyContent: 'space-between',

  '& .apply': {
    color: 'white',
    height: '40px',
    fontWeight: 600,
    fontSize: '15px',
    backgroundColor: props.theme.dark,
    border: `1px solid ${props.theme.dark}`,
    boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
    borderRadius: '3px',
  },

  '& .cancel': {
    color: props.theme.dark,
    fontWeight: 600,
    fontSize: '15px',
  },

  '& .clear': {
    color: '#4A5355',
    fontWeight: 600,
    fontSize: '15px',
    lineHeight: '140%',
  },
}))

export const ModalBody: any = styled.div((props) => ({
  display: 'flex',
  paddingBottom: '9px',

  '& ul': {
    width: '179px',
    height: '425px',
    borderRight: '1px solid #D8DBDB',
    listStyle: 'none',
    padding: 0,
    margin: 0,
  },

  '& ul li': {
    height: '36px',
    padding: '11px 0 11px 25px',
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: '100%',
    cursor: 'pointer',
    color: '#4A5355',
  },

  '& ul li.active': {
    backgroundColor: '#F2F9F3',
    color: props.theme.dark,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    '&::after': {
      content: '""',
      display: 'inline-block',
      width: '6px',
      height: '6px',
      borderRadius: '100%',
      backgroundColor: props.theme.dark,
      marginRight: '30px',
    },
  },

  '& section': {
    paddingLeft: '32px',
    paddingTop: '16px',
  },

  '& section h4': {
    color: '#82898A',
    fontSize: '15px',
    lineHeight: '140%',
  },
}))

export const StyleCheckbox: any = styled(Checkbox.Group)((props) => ({
  '& .ant-checkbox-group-item': {
    display: 'block',
    marginTop: '22px',
  },

  '& + .ant-checkbox-wrapper': {
    marginLeft: 0,
  },
}))

export const StyleTag: any = styled(Tag)((props) => ({
  borderRadius: '32px',
  color: props.theme.dark,
  boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
  fontWeight: 600,
  fontSize: '14px',
  lineHeight: '140%',
  padding: '4px 14px',
}))
