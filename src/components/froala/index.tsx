import 'froala-editor/js/froala_editor.pkgd.min.js';
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import 'font-awesome/css/font-awesome.css';
import 'froala-editor/js/plugins.pkgd.min.js';

import { Col, Row } from 'antd'

import { CACHED_ACCESSTOKEN } from '~/common/const'
import FroalaEditor from 'react-froala-wysiwyg';
/**@jsx jsx */
import React from 'react';
import { getFileUrl } from '~/common/utils'
import { jsx } from '@emotion/core'
import { withTheme } from 'emotion-theming'

export interface FroalaStates {
    onHandleModelChange: Function
    content: string
}

class FroalaComponent extends React.PureComponent<FroalaStates>  {
    state = {
        config: {
            // toolbarButtons: ["bold", "italic", "fontSize", "underline", "color", "emoticons", "|", 'paragraphFormat', "align", "formatOL", "formatUL", 'insertLink', 'insertImage', 'quote'],
            // quickInsertButtons: [],
            // pluginsEnabled: ['image', 'lists', 'link', 'fontSize', 'colors', 'charCounter', 'paragraphFormat', 'align', 'quote', 'emoticons'],   //  启动的plugins
            placeholderText: 'Edit Your Content Here!',
            charCounterCount: false,
            heightMin: 500,
            // Set the file upload URL.
            imageUploadURL: process.env.WSW2_HOST + '/api/v1/wholesale/file/froala-upload',
            fileUploadURL: process.env.WSW2_HOST + '/api/v1/wholesale/file/froala-upload',
            videoUploadURL: process.env.WSW2_HOST + '/api/v1/wholesale/file/froala-upload',
            // Additional upload params.
            // imageUploadParams: { id: 'my_editor' },

            // Set request type.
            imageUploadMethod: 'POST',

            // Set max file size to 20MB.
            imageMaxSize: 20 * 1024 * 1024,
            requestHeaders: {
                'Authorization': `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN) != null ? localStorage.getItem(CACHED_ACCESSTOKEN) : ""}`
            },
            events: {
                'image.uploaded': function (response: any) {
                    const data = JSON.parse(response)
                    let imageUrl = data.link
                    imageUrl = getFileUrl(imageUrl, true)
                    const editor = this;

                    // Insert image.
                    editor.image.insert(imageUrl, false, {}, editor.image.get(), { link: imageUrl });

                    return false;
                },
                'file.uploaded': function (response: any) {
                    const data = JSON.parse(response)
                    let fileUrl = data.link
                    fileUrl = getFileUrl(fileUrl, true)
                    const filename = fileUrl.substring(fileUrl.lastIndexOf('/') + 1, fileUrl.length);
                    const editor = this;

                    // Insert file.
                    editor.file.insert(fileUrl, filename, { link: fileUrl });

                    return false;
                },
                'video.uploaded': function (response: any) {
                    console.log(response);
                    const data = JSON.parse(response)
                    let videoUrl = data.link
                    videoUrl = getFileUrl(videoUrl, true)
                    const editor = this;

                    // Insert video.
                    var videoTag = `<video style="width:600px;" controls src="${videoUrl}" ></video>`
                    editor.video.insert(videoTag);
                    return false;
                },
            }
        }
    }


    render() {
        return (
            <div>
                <Row>
                    <FroalaEditor
                        tag='textarea'
                        config={this.state.config}
                        model={this.props.content}
                        onModelChange={this.props.onHandleModelChange}
                    />
                </Row>
            </div >
        )
    }
}
export const Froala = withTheme(FroalaComponent)
