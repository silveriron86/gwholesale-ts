import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { white, white6, darkGreen, filterGreen, brightGreen } from '~/common'

export const HeaderContainer = styled('div')((props) => ({
  position: 'fixed',
  top: 0,
  left: 0,
  width: '100%',
  backgroundColor: props.theme.dark,
  height: '59px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  zIndex: 1000,
}))

export const MobileDrawerMenu = styled('div')(() => ({
  // height: '450px',
  // display: 'table',
  paddingTop: '50px',
  // paddingBottom: '50px',
  width: '100%',
}))

export const MobileDrawerLine = styled('div')(() => ({
  // display: 'table-row',
  // height: '50%',
  width: '100%'
}))

export const MobileDrawerLink = styled('div')(() => ({
  // display: 'table-cell',
  // verticalAlign: 'middle',
  marginTop: 40,
  'div': {
    '& > a': {
      // marginTop: '42px',
      color: 'white',
      fontSize: '24px',
      lineHeight: '31.2px',
      fontWeight: 700,
      fontFamily: 'Museo Sans Rounded',
      margin: '0 auto 35px',
      display: 'block',
      width: 100,
      textAlign: 'center'
    }
  },
}))

export const MobileDrawerLogin = styled('div')(() => ({
  // height: '50px',
  // borderTop: '1px solid #1C6E31',
  // paddingTop: '24px',
  'button': {
    display: 'block',
    width: '100%',
    marginBottom: 10,
    borderRadius: 5,
    // color: 'white',
    // fontSize: '14px',
    // lineHeight: '15px',
    // letterSpacing: '0.05em',
    a: {
      fontSize: '12px !important',
      lineHeight: '15.6px !important',
    },
    '&.login': {
      backgroundColor: '#1C6E31',
      a: {
        color: 'white'
      }
    }
  },
}))

export const HeaderHomeContainer = styled('div')(() => ({
  // position: 'fixed',
  position: 'absolute',
  top: 40,
  left: 0,
  width: '100%',
  height: '79px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  zIndex: 10,
  i: {
    display: 'none',
  },
  '&.landing': {
    top: 0,
    paddingTop: 30,
    // position: 'relative'
    '&.terms': {
      backgroundColor: '#1d6f31',
      paddingBottom: 30,
      position: 'relative'
    },
  },
  '@media only screen and (max-width: 768px)': {
    height: 'auto',
    minHeight: '50px',
    display: 'block',
    position: 'relative',
    i: {
      display: 'inline',
    },
    img: {
      display: 'block',
      width: '204px',
      height: '33px',
      marginTop: '-26px',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    button: {
      display: 'none',
    },
  },
  '@media only screen and (max-width: 480px)': {
    '&.landing': {
      a:
      {
        marginLeft: '18px !important',
        marginTop: 22,
        display: 'block'
      }
    }
  }
}))

export const logoStyle: React.CSSProperties = {
  marginLeft: '15px',
  width: '115px',
  height: '35px',
}

export const LogoImage = styled('img')({
  objectFit: 'contain',
  maxHeight: 35,
  marginLeft: 15,
})

export const Tabs = styled('div')({
  display: 'flex',
  width: '60%',
})

export const LoginNavBar = styled('div')({
  position: 'fixed',
  height: '40px',
  display: 'flex',
  justifyContent: 'flex-end',
  alignItems: 'center',
  width: '100%',
  top: 0,
  left: 0,
  zIndex: 100,
  backgroundColor: filterGreen,
  '&.landing': {
    position: 'relative',
    backgroundColor: 'transparent',
    height: 'auto',
    width: 'auto'
  },
  '@media only screen and (max-width: 768px)': {
    display: 'none',
  },
})

export const LoginWrap = styled('div')({
  display: 'block',
  fontSize: '16px',
  lineHeight: '20.8px',
  marginRight: '91px',
  fontWeight: 700,
  padding: '15px 20px',
  borderRadius: 5,
  cursor: 'pointer',
  '&:hover': {
    boxShadow: '0px 10px 15px 0px #00000014',
    overflow: 'visible',
  },
  '&:active, &.selected': {
    background: 'linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), linear-gradient(0deg, #0D4B1C, #0D4B1C)',
  },
  a: {
    color: '#1C6E31',
    fontFamily: 'Museo Sans Rounded',
  },
  '&.landing': {
    marginRight: 0,
    backgroundColor: '#0D4B1C',
    a: {
      color: 'white',
    }
  },
  '&.pricing': {
    marginBottom: 15,
    marginRight: 0,
    backgroundColor: '#1C6E31',
    a: {
      color: 'white'
    }
  }
})

export const TabsHome = styled('div')({
  fontWeight: 700,
  zIndex: 100,
  marginRight: '52px',
  display: 'flex',
  justifyContent: 'flex-end',
  alignItems: 'center',
  width: '100%',

  '@media only screen and (max-width: 768px)': {
    display: 'none',
  },
  '&.landing': {
    marginRight: 115
  }
})

export const navLinkCss = css({
  width: '20%',
  cursor: 'pointer',
  textDecoration: 'none',
  color: white,
  fontSize: '14px',
  textAlign: 'center',
  position: 'relative',
  '&:active': {
    color: white6,
  },
  '&:focus': {
    textDecoration: 'none',
  },
  '&:hover': {
    color: white,
  },
})

export const navLinkHomeCss = css({
  width: '12%',
  cursor: 'pointer',
  whiteSpace: 'nowrap',
  textDecoration: 'none',
  color: '#1C6E31',
  fontSize: '16px',
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 'bold',
  textAlign: 'center',
  position: 'relative',
  lineHeight: '10.8px',
  letterSpacing: '0.05em',
  '&:active': {
    color: brightGreen,
  },
  '&:focus': {
    textDecoration: 'none',
  },
  '&:hover': {
    color: brightGreen,
  },
  '@media(min-width: 768px)': {
    minWidth: '90px',
  },
  '&.landing': {
    color: 'white !important',
    fontWeight: 600,
    width: 'auto',
    marginRight: 45,
    height: 50,
    lineHeight: '50px',
    position: 'relative',
    '.bb': {
      display: 'none'
    },
    '&:hover': {
      color: 'white !important',
      fontWeight: 700,
      '.bb': {
        display: 'none',
      }
    },
    '&:active, &.selected': {
      color: 'white !important',
      fontWeight: 700,
      '.bb': {
        bottom: 0,
        display: 'block',
        backgroundColor: 'white',
        height: 4,
        borderRadius: 5,
        width: '100%',
        position: 'absolute',
      }
    }
  }
})

export const navLinkHomeActiveCss = css`
  & {
    color: ${darkGreen} !important;
  }
  &:hover {
    color: ${darkGreen} !important;
  }
  &::before {
    width: 4px;
    height: 4px;
    content: ' ';
    display: block;
    border-radius: 50%;
    background-color: ${darkGreen};
    position: absolute;
    top: 50%;
    margin-top: -2px;
    left: 10px;
  }
`

export const navLinkActiveCss = css`
  & {
    color: ${white6} !important;
  }
  &:hover {
    color: ${white6} !important;
  }
  &::before {
    width: 4px;
    height: 4px;
    content: ' ';
    display: block;
    border-radius: 50%;
    background-color: ${white6};
    position: absolute;
    top: 50%;
    margin-top: -2px;
    left: 10px;
  }
`

export const myAccountMenuButtonStyle: React.CSSProperties = {
  height: '35px',
  fontSize: '14px',
  padding: '0',
  borderStyle: 'hidden',
}

export const myAccountMenuDropDownStyle: React.CSSProperties = {
  borderRadius: '0',
}

export const MyAccount = styled('div')({
  color: white,
  marginRight: '52px',
})

export const MyAccountText = styled('span')({
  color: white,
  marginLeft: '10px'
})

export const arrowDownStyle: React.CSSProperties = {
  width: '18px',
  height: '13px',
  marginLeft: '5px',
  color: white,
}

export const homeButtonStyle = css({
  border: '1px solid #26AC5F',
  boxSizing: 'border-box',
  // borderRadius: '20px',
  height: '45px',
  paddingLeft: 16,
  paddingRight: 16,
  a: {
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: '25.2px',
    color: 'white'
  }
  '@media(max-width: 768px)': {
    width: '121px',
    fontSize: '14px',
  },
  '&.landing': {
    border: 'none',
    cursor: 'pointer',
    a: {
      fontWeight: 700,
      fontSize: 16,
      lineHeight: '20.8px',
      color: '#1C6E31'
    },
    '&:hover': {
      boxShadow: '0px 10px 15px 0px #00000014',
    },
    '&:active, &.selected': {
      backgroundColor: '#F2F9F3',
      boxShadow: '0px 10px 15px 0px #00000014',
    }
  }
})

export const accountDividerStyle = {
  color: white,
  margin: '0 25px',
  height: '25px',
}

export const accountInfoStyle = {
  display: 'inline-block',
  background: 'none',
  cursor: 'pointer'
}

export const ModuleName = styled('div')((props: any) => ({
  borderLeft: '1px solid white',
  height: 30,
  display: 'flex',
  alignItems: 'center',
  color: 'white',
  paddingLeft: 89,
  marginLeft: 0,
  fontSize: '20px',
  fontWeight: 400,
  fontFamily: 'Museo Sans Rounded',
  lineHeight: '23.4px',
  marginTop: 1,
  'svg': {
    width: 20,
    height: 14,
    marginRight: 2
  }
}))
