/** @jsx jsx */
import { jsx } from '@emotion/core'
import React from 'react'
import { Link, NavLink, RouteProps } from 'react-router-dom'
import { withTheme } from 'emotion-theming'

import { Button, Modal, Icon as IconButton } from 'antd'
import MobileDrawer from '~/components/MobileDrawer'

import { brightGreen, Theme } from '~/common'
import { getRoutePath, isSeller } from '~/common/utils'

import {
  HeaderContainer,
  HeaderHomeContainer,
  TabsHome,
  navLinkHomeCss,
  MobileDrawerMenu,
  MobileDrawerLink,
  MobileDrawerLine,
  MobileDrawerLogin,
  LoginNavBar,
  LoginWrap,
  homeButtonStyle,
  ModuleName,
} from './header.style'
import HomeLogo from './Logo.png'
import HomeLogoWhite from './Logo_white.png'
import { HomePageDispatchProps, HomePageStateProps, HomePageModule } from '~/modules/homepage/homepage.module'
import RequestDemoForm, { RequestDemoFields } from '~/modules/homepage/components/request-demo.component'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { Icon as IconSvg } from '~/components/icon/'
import { AuthUser, UserRole } from '~/schema'
import { ThemeIcon } from '~/modules/customers/customers.style'

export interface HeaderStates {
  hide?: boolean
  logo?: string
  theme?: Theme
  location?: any
  tabConfig?: any[]
  currentUser?: AuthUser
}

export type HeaderProps = HomePageDispatchProps & HomePageStateProps & RouteProps & HeaderStates

class HeaderComponent extends React.PureComponent<HeaderProps> {
  state = {
    isExpanded: false,
  }

  handleToggle() {
    this.setState({
      isExpanded: !this.state.isExpanded,
    })
  }

  isActive(pathname: string, _: any, location: any) {
    return location.pathname === pathname
  }

  render() {
    const { hide, location, tabConfig, currentUser, isMobile } = this.props
    const currentRoute = getRoutePath()
    const isCustomer = currentUser && currentUser.accountType === UserRole.CUSTOMER ? true : false
    let moduleName = ''

    const curPath = currentRoute.path
    let matched = tabConfig
      ? tabConfig.filter((route) => {
        return curPath.indexOf(route.path) >= 0
      })
      : []
    if (matched.length) {
      moduleName = matched[0].headerName
    }

    if (curPath.indexOf('/sales-order/') > 0) {
      return null
    }

    if (!moduleName && curPath.indexOf('/myaccount/') > 0) {
      moduleName = 'My Account'
    }
    if (!moduleName && /pricing\/.+\/default-pricing/.test(curPath)){
      moduleName = 'Back'
    }
    if (/pricesheet\/.+\/print/.test(curPath)){
      moduleName = 'Back'
    }
    if (!tabConfig) {
      return (
        <div style={{ height: '99px' }}>
          {this.renderHomeTabs()}
          {this.renderRequestDemoModal()}
        </div>
      )
    }
    // temporary way to avoid fixed header issue
    if (location !== null && (location.pathname.includes('home') || location.pathname === '/' || location.pathname === '/product' || location.pathname === '/pricing')) {
      if (location.pathname === '/' || location.pathname === '/home/landing' || location.pathname === '/home/terms' || location.pathname === '/product' || location.pathname === '/pricing') {
        return null
      }
      return (
        <div style={{ height: isMobile ? 20 : 99 }}>
          <LoginNavBar style={{ height: 40 }}>
            <LoginWrap>
              <NavLink to="/login" isActive={this.isActive.bind(this, '/login')}>
                Log In <IconButton style={{ marginLeft: '10px' }} type="arrow-right" />
              </NavLink>
            </LoginWrap>
          </LoginNavBar>
          {this.renderHomeTabs()}
          {this.renderRequestDemoModal()}
        </div>
      )
    }

    const BackIcon = <ThemeIcon type="arrow-left" viewBox="0 0 20 20" width={13} height={11} style={{ color: 'white' }} />
    const backStyle = { paddingLeft: 70, position: 'absolute', left: 0, top: 23.5, zIndex: 1000 }
    return (
      <div style={{ height: 59 }}>
        <HeaderContainer style={{ backgroundColor: location.pathname.includes('/login') ? 'white' : '' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {moduleName === 'Customer Accounts' && (
              <a href="#/customers" style={backStyle}>
                {BackIcon}
              </a>
            )}
            {moduleName === 'Vendor Accounts' && (
              <a href="#/vendors" style={backStyle}>
                {BackIcon}
              </a>
            )}
            {moduleName === 'Product' && (
              <a href="#/inventory" style={backStyle}>
                {BackIcon}
              </a>
            )}
            {moduleName === 'New Order' && (
              <a href="#/pricesheets" style={backStyle}>
                {BackIcon}
              </a>
            )}
            {moduleName === 'Back' && (
              <a href="javascript:history.back(-1)" style={backStyle}>
                {BackIcon}
              </a>
            )}

            <ModuleName style={{ paddingLeft: moduleName == 'Work Orders' ? 60 : 89 }}>
              {moduleName === 'Work Orders' && (
                <a
                  href="#/work-orders"
                  style={{ color: 'white', display: curPath.indexOf('/work-orders') > -1 ? 'none' : 'block' }}
                >
                  {BackIcon}
                </a>
              )}
              {moduleName === 'Sales Orders' && isCustomer ? 'Order History' : moduleName}
            </ModuleName>
          </div>
        </HeaderContainer>
      </div>
    )
  }

  onRequestDemo = (payload: RequestDemoFields) => {
    this.props.sendRequestDemo(payload)
  }

  onCloseRequest = () => {
    this.props.closeModal()
  }

  private renderRequestDemoModal() {
    const closeIcon = <IconSvg type="close-green" viewBox={void 0} width={24} height={24} />
    return (
      <Modal
        closeIcon={closeIcon}
        width={600}
        footer={null}
        visible={this.props.showModal}
        onCancel={this.onCloseRequest}
      >
        <RequestDemoForm onSubmit={this.onRequestDemo} onCancel={this.onCloseRequest} />
      </Modal>
    )
  }

  private renderMobileMenu() {
    const { isExpanded } = this.state
    const { openModal } = this.props
    const handleToggle = this.handleToggle.bind(this)
    return (
      <MobileDrawer open={isExpanded} onClick={handleToggle}>
        <MobileDrawerMenu>
          <MobileDrawerLine>
            <MobileDrawerLink>
              <img src={HomeLogoWhite} style={{ position: 'absolute', top: 24, marginLeft: -10 }} />
              <div>
                <NavLink onClick={handleToggle} to="/product" isActive={this.isActive.bind(this, '/product')}>
                  Product
                </NavLink>
              </div>
              <div>
                <NavLink onClick={handleToggle} to="/pricing" isActive={this.isActive.bind(this, '/pricing')}>
                  Packages
                </NavLink>
              </div>
            </MobileDrawerLink>
          </MobileDrawerLine>
        </MobileDrawerMenu>
        <MobileDrawerLogin>
          {/* <div>
            <NavLink onClick={handleToggle} to="/login" isActive={this.isActive.bind(this, '/login')}>
              LOG IN <IconButton style={{ marginLeft: '10px' }} type="arrow-right" />
            </NavLink>
          </div> */}
          <Button css={homeButtonStyle} className="landing login">
          <NavLink to="/login" isActive={this.isActive.bind(this, '/login')}>
              Login
            </NavLink>
          </Button>
          <Button
            // onClick={openModal}
            css={homeButtonStyle}
            className="landing"
          >
            <a href="https://share.hsforms.com/1zGjYvlFmT5e475T3I4GH5Acfaea">Request a Demo</a>
          </Button>
        </MobileDrawerLogin>
      </MobileDrawer>
    )
  }

  private renderHomeTabs() {
    const { openModal, tabConfig, location } = this.props
    // const {isExpanded} = this.state
    const className = tabConfig ? '' : `landing ${location.pathname === '/home/terms' ? 'terms' : ''}`
    console.log('location.pathname = ', location)
    return (
      <HeaderHomeContainer className={`header-home ${className}`}>
        {this.renderMobileMenu()}

        <Link to="/" style={{ marginLeft: tabConfig ? 24 : 100 }}>
          <img src={tabConfig ? HomeLogo : HomeLogoWhite} />
        </Link>
        <TabsHome className={className}>
          <NavLink
            className={`${location.hash === '#/product' ? 'selected' : ''} ${className}`}
            to="/product"
            isActive={this.isActive.bind(this, '/product')}
            css={navLinkHomeCss}
          >
            Product
            <span className="bb"></span>
          </NavLink>
          <NavLink
            className={`${location.hash === '#/pricing' ? 'selected' : ''} ${className}`}
            to="/pricing"
            isActive={this.isActive.bind(this, '/pricing')}
            css={navLinkHomeCss}
          >
            Packages
            <span className="bb"></span>
          </NavLink>
          {!tabConfig && (
            <>
              <LoginNavBar style={{ height: 40 }} className="landing">
                <LoginWrap className={className}>
                  <NavLink to="/login" isActive={this.isActive.bind(this, '/login')}>
                    Order Online <IconButton style={{ marginLeft: '10px' }} type="arrow-right" />
                  </NavLink>
                </LoginWrap>
              </LoginNavBar>

              <LoginNavBar style={{ height: 40, marginLeft: 35 }} className="landing">
                <LoginWrap className={className}>
                  <NavLink to="/login" isActive={this.isActive.bind(this, '/login')}>
                    Log In as Wholesaler/Broker <IconButton style={{ marginLeft: '10px' }} type="arrow-right" />
                  </NavLink>
                </LoginWrap>
              </LoginNavBar>
            </>
          )}
          {/* <Button
            css={homeButtonStyle}
            style={{
              color: brightGreen,
              paddingLeft: 27,
              paddingRight: 27,
            }}
          >
            <a href="http://go.wholesaleware.com">Learn more</a>
          </Button> */}
          <Button
            // onClick={openModal}
            css={homeButtonStyle}
            className={className}
            style={{
              // color: '#fff',
              // background: brightGreen,
              width: 'auto',
              marginLeft: 35,
            }}
          >
            <a href="https://share.hsforms.com/1zGjYvlFmT5e475T3I4GH5Acfaea">Request a Demo</a>
          </Button>
        </TabsHome>
      </HeaderHomeContainer>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.homepage

export const Header = withTheme(connect(HomePageModule)(mapStateToProps)(HeaderComponent))
