import { css } from '@emotion/core'

export const icon = css({
  verticalAlign: 'middle',
  fill: 'currentColor',
  overflow: 'hidden',
})
