/** @jsx jsx */

import React from 'react'
import { jsx } from '@emotion/core'

import { icon } from './icon.style'
import { icons } from './svg-icons'

export type IconProps = React.SVGProps<SVGElement> & {
  type: string
}

export class Icon extends React.PureComponent<IconProps> {
  render() {
    const { type, ...restProps } = this.props

    if (!icons[type]) {
      throw new TypeError(`Invalid icon type: ${type}`)
    }
    const IconSVG = icons[type].default

    return <IconSVG viewBox="0 0 12 12" width="12" height="12" {...restProps} css={icon} />
  }
}
