const req = (require as any).context('./resources', true, /\.svg$/i)

export const icons = {}

req.keys().forEach((key: string) => {
  const pureKey = key.replace(/\.\/(.*?)\.svg$/, '$1')
  icons[pureKey] = req(key)
})
