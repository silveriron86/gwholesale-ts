import React from 'react'
import { Tooltip } from 'antd'
import { mathRoundFun } from '~/common/utils'

interface InventoryProps {
  item: any
  flag: number
}

class InventoryAsterisk extends React.PureComponent<InventoryProps> {
  constructor(props: InventoryProps) {
    super(props)
  }

  render() {
    const {
      availableToSell,
      onHandQty,
      commitedQty,
      noLotCommittedQty,
      noLotOnHandQty,
      noLotReturnQty,
    } = this.props.item
    const { flag } = this.props
    let dom = <></>
    if (flag == 1) {
      if (noLotCommittedQty > 0 || noLotOnHandQty > 0 || noLotReturnQty > 0) {
        dom = (
          <>
            <div>&nbsp;&nbsp;{availableToSell === null ? 0 : mathRoundFun(availableToSell, 2)} Available</div>
            <div>-{noLotOnHandQty === null ? 0 : mathRoundFun(noLotOnHandQty, 2)} Shipped w/o Lots</div>
            <div>-{noLotCommittedQty === null ? 0 : mathRoundFun(noLotCommittedQty, 2)} Committed w/o Lots</div>
            <div>+{noLotReturnQty === null ? 0 : mathRoundFun(noLotReturnQty, 2)} Return w/o Lots</div>
          </>
        )
        return (
          <>
            <Tooltip title={dom} placement="bottom">
              *
            </Tooltip>
          </>
        )
      }
    } else if (flag == 2) {
      if (noLotOnHandQty > 0 || noLotReturnQty > 0) {
        dom = (
          <>
            <div>&nbsp;&nbsp;{onHandQty === null ? 0 : mathRoundFun(onHandQty, 2)} On Hand</div>
            <div>-{noLotOnHandQty === null ? 0 : mathRoundFun(noLotOnHandQty, 2)} Shipped w/o Lots</div>
            <div>+{noLotReturnQty === null ? 0 : mathRoundFun(noLotReturnQty, 2)} Return w/o Lots</div>
          </>
        )
        return (
          <>
            <Tooltip title={dom} placement="bottom">
              *
            </Tooltip>
          </>
        )
      }
    } else if (flag == 3) {
      if (noLotCommittedQty > 0) {
        dom = (
          <>
            <div>&nbsp;&nbsp;{commitedQty === null ? 0 : mathRoundFun(commitedQty, 2)} Committed</div>
            <div>+{mathRoundFun(noLotCommittedQty, 2)} Committed w/o Lots</div>
          </>
        )
        return (
          <>
            <Tooltip title={dom} placement="bottom">
              *
            </Tooltip>
          </>
        )
      }
    }
    return <></>
  }
}
export default InventoryAsterisk
