/**@jsx jsx */
import { Table } from 'antd'
import React from 'react'
import { jsx } from '@emotion/core'
import { Resizable, ResizeCallbackData } from 'react-resizable'
import { TableProps, ColumnProps } from 'antd/es/table'

import { SortButton } from './sort-button.component'
import { makeSortFn } from './make-sort-fn'
import { tableStyleOverride } from './style'

export type MutiSortTableProps<T> = TableProps<T>

interface SortMeta {
  sortFn: (a: any, b?: any) => number
  sortOrder: 'ascend' | 'descend'
  dataIndex: string
}

export const ResizeableTitle: React.SFC<TitleProps> = (props) => {
  const { onResize, width, ...restProps } = props;

  if (!width) {
    return <th {...restProps} />;
  }

  return (
    <Resizable width={width} height={0} onResize={onResize} draggableOpts={{ enableUserSelectHack: false }}>
      <th {...restProps} />
    </Resizable>
  );
}

export interface TitleProps extends React.DetailedHTMLProps<React.ThHTMLAttributes<HTMLTableHeaderCellElement>, HTMLTableHeaderCellElement> {
  width: number
  onResize: (e: React.SyntheticEvent, data: ResizeCallbackData) => any
}

export class MutiSortTable<T> extends React.PureComponent<TableProps<T>, {
  sortIndex: SortMeta[]
  columns: ColumnProps<T>[]
}> {
  state = {
    sortIndex: [] as SortMeta[],
    columns: [] as ColumnProps<T>[]
  }

  components = {
    header: {
      cell: ResizeableTitle
    }
  }

  componentDidMount() {
    if (this.props.columns) {
      this.setState({ columns: this.getColumns(this.props) })
    }
  }

  componentDidUpdate(prevProps: TableProps<T>) {
    if (this.props.columns !== prevProps.columns) {
      this.setState({ columns: this.getColumns(this.props) })
    }
  }

  handleResize = (index: number) => (_e: React.SyntheticEvent, { size }: ResizeCallbackData) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      return { columns: nextColumns };
    });
  };

  render() {
    const { columns, loading, dataSource, bordered = true, ...restProps } = this.props
    if (!columns || !dataSource) {
      return <Table {...this.props} />
    }
    const sortedData = [...dataSource]
    sortedData.sort(makeSortFn(this.state.sortIndex.map((s) => (a, b) => {
      if (s.sortOrder === 'descend') {
        return s.sortFn(a, b) * (-1)
      }
      return s.sortFn(a, b)
    })))

    const tableColumns = this.state.columns.map((col, index) => ({
      ...col,
      onHeaderCell: (column: { width?: any; }) => ({
        width: column.width,
        onResize: this.handleResize(index),
      }),
    }))

    return (
      <Table
        components={this.components}
        bordered={bordered}
        loading={loading}
        columns={tableColumns}
        dataSource={sortedData}
        css={tableStyleOverride}
        {...restProps}
      />
    )
  }

  private getColumns(props: TableProps<T>) {
    const context = this
    const _columns = props.columns!.map((column, _index) => ({
      ...column,
      get title() {
        const actived = context.state.sortIndex.find((meta) => meta.dataIndex === column.dataIndex)
        const sortButton = column.sorter ? (
          <SortButton
            actived={!!actived}
            onClick={context.onClickSort(column)}
            sortOrder={actived ? actived.sortOrder : 'ascend'}
          />
        ) : null
        return (
          <React.Fragment>
            {column.title}
            {sortButton}
          </React.Fragment>
        )
      },
    }))
    _columns.forEach((column) => {
      delete column.sorter
    })
    return _columns
  }

  private onClickSort<T>(props: ColumnProps<T>) {
    return () => {
      const sortIndex = [...this.state.sortIndex]
      const position = sortIndex.findIndex((v) => v.dataIndex === props.dataIndex)
      if (position > -1) {
        const metaSort = sortIndex[position]
        if (metaSort.sortOrder === 'ascend')
          sortIndex.splice(position, 1, {
            sortFn: props.sorter as SortMeta['sortFn'],
            dataIndex: props.dataIndex!,
            sortOrder: 'descend',
          })
        else sortIndex.splice(position, 1)
      } else {
        sortIndex.push({ sortFn: props.sorter as SortMeta['sortFn'], dataIndex: props.dataIndex!, sortOrder: 'ascend' })
      }

      this.setState({
        sortIndex
      }, () => {
        this.setState({
          columns: this.getColumns(this.props)
        })
      })
    }
  }
}
