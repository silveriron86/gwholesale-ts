import { firstBy } from 'thenby'

export interface SortMeta<T> {
  dataIndex?: string
  sorted: boolean
  sorter: (a: T, b?: T) => number
}

export function makeSortFn<T>(sortFnList: ((a: T, b?: T) => number)[]) {
  if (!sortFnList.length) {
    return () => 1
  }
  const [first] = sortFnList
  return sortFnList.reduce((acc, cur) => acc.thenBy(cur), firstBy(first))
}
