import React from 'react'
import { withTheme } from 'emotion-theming'

import { grey, Theme } from '~/common'
import { SortButtonWrapper } from './sort-button.style'

export interface SortButtonProps extends React.HTMLAttributes<HTMLDivElement> {
  actived: boolean
  theme: Theme
  sortOrder: 'ascend' | 'descend'
}

class SortButtonComponent extends React.PureComponent<SortButtonProps> {
  render() {
    const { actived, theme, sortOrder, ...restProps } = this.props
    const color = actived ? theme.primary : grey
    return (
      <SortButtonWrapper {...restProps}>
        { sortOrder == 'descend' ? (
          <svg width="11" height="10" viewBox="0 0 11 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.4445 4.22185L5.22266 8.4437L1.00081 4.22185" stroke={color} strokeWidth="2" />
          </svg>
        ) : (
          <svg width="11" height="10" viewBox="0 0 11 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.001,7.222L5.223,3l4.222,4.22" stroke={color} strokeWidth="2" />
          </svg>
        ) }
      </SortButtonWrapper>
    )
  }
}

export const SortButton = withTheme(SortButtonComponent)
