import styled from '@emotion/styled'

export const SortButtonWrapper = styled('div')({
  display: 'flex',
  cursor: 'pointer',
  width: '15px',
  justifyContent: 'flex-end',
  '& > svg': {
    marginTop: '2px',
    marginLeft: '6px',
  },
})
