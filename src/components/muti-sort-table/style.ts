import { css } from '@emotion/core'
import { white, gray05, gray02} from '~/common'

export const tableStyleOverride = css({
  '& .ant-table-content': {
    '& .ant-table-body': {
      '& > table:not(.address)': {
        // borderTop: 'none',
        '& .ant-table-thead': {
          // boxShadow: '0px 12px 14px -6px rgba(0,0,0,0.15)',
          '& > tr > th': {
            //backgroundColor: white,
            backgroundColor: gray05,
            // border: 'none',
            color: gray02
          },
          '.ant-table-column-title': {
            color: `${gray02} !important`
          },
        },
      },
    },
  },
})
