/** @jsx jsx */
import React, { useState, useEffect, useRef } from 'react'
import { jsx } from '@emotion/core'
import { Radio, Select, Row, Col, Form, Tooltip, notification } from 'antd'
import InputNumber from '../Input/InputNumber'
import { FullInputNumber, InputLabel, ThemeSelect } from '~/modules/customers/customers.style'
import { AuthUser, UserRole } from '~/schema'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { withTheme } from 'emotion-theming'
import { HomePageModule } from '~/modules/homepage'
import { Icon as IconLocal } from '~/components'
import { formateGroupLogic } from '~/common/utils'
import { CACHED_NS_LINKED } from '~/common'
import styled from '@emotion/styled'
import { CustomerService } from '~/modules/customers/customers.service'
import { useParams } from 'react-router'
import { responseHandler } from '~/common/utils'
import { Link } from 'react-router-dom'

const { Option } = Select
export const CUSTOM_MARGIN = 'CUSTOM_MARGIN'
export const MATCH_A_DIFFERENT_CUSTOMER = 'MATCH_A_DIFFERENT_CUSTOMER'
type Props = {
  setPricingType?: Function
  onSelectedLogicChange?: Function
  changeSalesPrice?: Function
  price?: number | null
  defaultGroup: string
  defaultLogic: string
  isDefault?: boolean
  logicTitle: string
  groupTitle: string
  cost?: number
  currentUser: AuthUser
  showCustomMargin?: boolean
  showCustomerOption?: boolean
  defaultMargin?: number
  onMarginChange?: any
  onCustomerChange?: any
  sharedClientId?: any
  isCustomerDefaultPrice?: boolean
}

const PricingOptions: React.SFC<Props> = (props) => {
  const {
    setPricingType,
    onSelectedLogicChange,
    defaultGroup,
    defaultLogic,
    logicTitle,
    showCustomMargin,
    showCustomerOption,
    defaultMargin,
    onMarginChange,
    onCustomerChange,
    isCustomerDefaultPrice,
    sharedClientId,
  } = props
  const [logic, setLogic] = useState<string>('')
  const [margin, setMargin] = useState<string | number>('0.0')
  const { customerId } = useParams<{ customerId: string }>()
  const [customerList, setCustomerList] = useState<any[]>([])
  const selectedCustomer = customerList.find((item) => item.clientId == sharedClientId)

  useEffect(() => {
    setLogic(formateGroupLogic(defaultLogic, defaultGroup))
  }, [defaultLogic])

  useEffect(() => {
    if (customerId) {
      CustomerService.instance.getDefaultPriceCustomerList(customerId).subscribe({
        next(data: any) {
          if (!data) {
            notification.error({
              message: 'Fail to get customer list',
            })
            return
          }
          responseHandler(data)
          setCustomerList(data?.body?.data || [])
        },
        error(error) {
          notification.error({
            message: 'Fail to get customer list',
          })
        },
        complete() {},
      })
    }
  }, [customerId])

  useEffect(() => {
    const input = document.getElementById('custom-margin-input')
    const active = document.activeElement
    if (active && input === active) {
      setMargin(defaultMargin)
    } else {
      setMargin(Number(defaultMargin).toFixed(1))
    }
  }, [defaultMargin])

  const _onChange = (val) => {
    if (/\.\d{2,}/.test(val)) {
      return
    }
    setMargin(val)
    onMarginChange && onMarginChange(val)
  }

  const _onBlur = (e) => {
    const { value } = e.target
    if (!value) {
      return
    }
    const val = Number(value).toFixed(1)
    setMargin(val)
    onMarginChange && onMarginChange(val)
  }

  function onLogicChange(value: any) {
    setLogic(value)

    if (setPricingType) {
      if (value != 'GROUP') {
        setPricingType(value)
      } else {
        setPricingType('')
      }
    }
    if (onSelectedLogicChange) {
      onSelectedLogicChange(value)
    }
  }

  return (
    <StyleDiv>
      <div style={{ width: isCustomerDefaultPrice ? '100%' : 350 }}>
        <InputLabel style={{ fontWeight: 'bold' }}>
          {logicTitle}
          <Tooltip
            placement="top"
            title="The selected customer pricing strategy will apply for all products sold to this customer. If the selected pricing strategy is not enabled for a given product. the default price will be use. Use the Override Pricing table below to select a different pricing strategy for any individual product."
          >
            <IconLocal type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
          </Tooltip>
        </InputLabel>

        <div>
          <ThemeSelect style={{ width: 250 }} onChange={onLogicChange} value={logic}>
            <Option value="FOLLOW_DEFAULT_SALES">Default Price</Option>
            <Option value="FOLLOW_LAST_SOLD">Last Sold Price</Option>
            {showCustomMargin && <Option value={CUSTOM_MARGIN}>Margin</Option>}

            {/* <Option value='AVERAGE_SOLD'>Follow Average Sold Price</Option> */}
            {/* {
          type != 'product' && <Option value='PRODUCT_DEFAULT_LOGIC'>Follow Product Default Logic</Option>
          } */}

            {/* {isDefault == true &&
          <Option value='LOT_SUGGEST_SALE_PRICE'>Follow Lot Suggest Sales Price</Option>
          } */}
            <Option value="GROUP_A">Group A</Option>
            <Option value="GROUP_B">Group B</Option>
            <Option value="GROUP_C">Group C</Option>
            <Option value="GROUP_D">Group D</Option>
            {localStorage.getItem(CACHED_NS_LINKED) == 'null' && <Option value="GROUP_E">Group E</Option>}
            {showCustomerOption && <Option value={MATCH_A_DIFFERENT_CUSTOMER}>Match a different customer</Option>}
          </ThemeSelect>
          {logic == CUSTOM_MARGIN && (
            <InputNumber
              id="custom-margin-input"
              value={margin}
              style={{ width: 110 }}
              onChange={_onChange}
              onBlur={_onBlur}
              suffix="%"
            />
          )}
          {logic == MATCH_A_DIFFERENT_CUSTOMER && (
            <React.Fragment>
              <Select
                placeholder="Type customer name..."
                optionFilterProp="children"
                showSearch
                value={sharedClientId || undefined}
                onChange={onCustomerChange}
                style={{ width: 300, marginLeft: 15 }}
              >
                {customerList.map((cs) => (
                  <Option value={cs.clientId}>{cs.companyName}</Option>
                ))}
              </Select>
              {selectedCustomer && (
                <Link
                  style={{ marginLeft: 15 }}
                  to={`/pricing/${selectedCustomer.priceSheetId}/default-pricing/${selectedCustomer.clientId}`}
                >
                  View customer pricing
                </Link>
              )}
            </React.Fragment>
          )}
        </div>
      </div>
    </StyleDiv>
  )
}

const mapStateToProps = (state: GlobalState) => {
  return {
    currentUser: state.currentUser,
  }
}

const WithThemePricingOptions = withTheme(connect(HomePageModule)(mapStateToProps)(PricingOptions))

export default WithThemePricingOptions

const StyleDiv = styled.div`
  text-align: left;
  margin-bottom: 20;
  display: flex;
  .ant-input {
    border: 1px solid #d9d9d9 !important;
  }
`
