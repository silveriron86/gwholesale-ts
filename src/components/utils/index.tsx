/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { MessageContainer } from './message.style';
import { Alert } from 'antd';

export enum MessageType {
  ERROR = 'error',
  INFO = 'info',
  WARNING = 'warning',
  SUCCESS = 'success',
}

export interface MessageProps {
  message: string
  description?: string
  type: MessageType | null
}

export class MessageHeader extends React.PureComponent<MessageProps> {

  render() {
    const { message, type, description } = this.props
    if (!type) return null
    return (
      <MessageContainer>
        <Alert message={message} description={description} type={type} showIcon/>
      </MessageContainer>
    )
  }
}
