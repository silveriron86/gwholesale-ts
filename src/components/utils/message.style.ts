import styled from '@emotion/styled'

export const MessageContainer = styled('div')({
  width: '100%',
  alignItems: 'center',
  justifyContent: 'space-between',
})
