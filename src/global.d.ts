type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

declare interface Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : Function
  __REDUX_DEVTOOLS_EXTENSION__: any
}

declare const __DEV__: string
declare const __COMMIT_SHA__: string
declare const __PKG_VERSION__: string
declare const __AUTHORIZATION__: string

declare interface NodeModule {
  hot: any
}

declare module '*.svg' {
  export const ReactComponent: import('react').ComponentClass<import('react').SVGProps<SVGSVGElement>>
  export default ReactComponent
}

declare module '*.png' {
  const url: string
  export default url
}

declare module '*.jpg' {
  const url: string
  export default url
}

declare module '*.woff' {
  const url: string
  export default url
}

declare module 'rc-drawer' {
  import { DrawerProps } from 'antd/lib/drawer'

  interface RcDrawerPropsTmp extends DrawerProps {
    level?: 'all' | null | string
    wrapperClassName?: string
    open?: boolean
  }

  export type RcDrawerProps = Omit<RcDrawerPropsTmp, 'visible'>

  const RcDrawer: React.FunctionComponent<RcDrawerProps>
  export default RcDrawer
}

declare module 'react-barcode'
