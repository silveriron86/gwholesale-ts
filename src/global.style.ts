import { css } from '@emotion/core'
import { injectGlobal } from 'emotion'

// import museoSansRounded from '~/static/fonts/museo-sans-rounded/normal.woff'
import museoSansRounded700 from '~/static/fonts/MuseoSansRounded-700-webfont.woff'
import museoSansRounded300 from '~/static/fonts/MuseoSansRounded-300-webfont.woff'
import PatuaOne from '~/static/fonts/PatuaOne-Regular.ttf'

import { mediumGrey, mediumGrey2, black, darkGrey, gray01, gray05, gray02 } from './common'

injectGlobal({
  '@font-face': {
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 'normal',
    fontStyle: 'normal',
    src: `local("Museo Sans Rounded"), url("${museoSansRounded300}")`,
  },
})

injectGlobal({
  '@font-face': {
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 'bold',
    fontStyle: 'normal',
    src: `local("Museo Sans Rounded"), url("${museoSansRounded700}")`,
  },
})

injectGlobal({
  '@font-face': {
    fontFamily: 'Patua One',
    fontStyle: 'normal',
    src: `local("Patua One"), url("${PatuaOne}")`,
  },
})

export const globalStyle = css({
  '#root': {
    fontFamily: "Museo Sans Rounded",
    lineHeight: '15px',
    letterSpacing: '0.05em',
  },
  'body': {
    fontFamily: "Museo Sans Rounded",
  },

  '@media print': {
    '@page': {
      marginTop: '20px',
      marginBottom: '20px',
    },
    'body, main': {
      border: '0',
      float: 'none',
      margin: '0',
      overflow: 'visible',
      padding: '0',
      position: 'static',
      width: '100%',
    },
    'p,h1,div': {
      fontFamily: 'Roboto,"Segoe UI",Tahoma,sans-serif',
    },
  },
  '#customer-so-table-preview': {
    '& .ant-table-content': {
      minWidth: 'initial !important',
      overflow: 'initial !important',
    },
  },
  '.ant-table-bordered .ant-table-thead > tr > th, .ant-table-bordered .ant-table-tbody > tr > td': {
    borderRight: '0px',
  },
  '.ant-table-bordered .ant-table-thead > tr > th:last-child': {
    borderRight: '1px solid #EDF1EE',
  },
  '.has-border .ant-table-bordered .ant-table-thead > tr > th, .has-border .ant-table-bordered .ant-table-tbody > tr > td': {
    borderRight: '1px solid #EDF1EE',
  },
  '.ant-input-search-button': {
    paddingRight: '3px',
    fontSize: '14px',
    boxShadow: 'none',
  },
  '.ant-input-search ::placeholder': {
    fontFamily: '"Museo Sans Rounded"',
    fontSize: '14px',
    // lineHeight: '15px',
    letterSpacing: '0.05em',
    // color: mediumGrey,
    color: '#82898A'
  },
  '.ant-table-wrapper': {
    '.ant-table-content': {
      '.ant-table-body': {
        '.ant-table-thead': {
          '& > tr > th': {
            paddingBottom: '15px',
          },
          '.ant-table-column-title': {
            display: 'flex !important',
            alignItems: 'left',
            justifyContent: 'left',
            fontSize: '12px',
            fontWeight: 700,
            wordBreak: 'keep-all',
            color: `${gray02} !important`
          },
          '& .react-resizable': {
            position: 'relative',
          },
          '& .react-resizable-handle': {
            position: 'absolute',
            width: 10,
            height: '100%',
            bottom: 0,
            right: -5,
            cursor: 'col-resize',
          },
        },
        '.ant-table-tbody > th > td': {
          lineHeight: '17px',
          paddingTop: '19px',
          paddingBottom: '14px',
          paddingLeft: '9px',
          paddingRight: '9px',
        },
        '.ant-table-tbody > tr > td': {
          // color: black,
          color: gray01,
          // background: 'rgb(250, 250, 250)',
          background: 'rgb(255, 255, 255)'
        },
        'tbody > .ant-table-row > td': {
          // color: black,
          color: gray01
        },
      },
    },
  },
  '.ant-modal-wrap': {
    fontFamily: '"Museo Sans Rounded"',
    // fontFamily: 'Roboto, "Segoe UI", Tahoma,sans-serif',
    fontWeight: 'bold',
    lineHeight: '15px',
    letterSpacing: '0.05em',
  },
  '.ant-modal-wrap .ant-col': {
    fontWeight: 'bold',
  },

  '.ant-form-item-label': {
    paddingBottom: '3px !important',
    label: {
      color: mediumGrey,
    },
  },
  '.ant-alert': {
    display: 'flex',
    justifyContent: 'center',
    '.ant-alert-icon': {
      paddingTop: '2px',
      paddingRight: '5px',
      top: '',
      left: '',
      position: 'unset',
    },
    '.ant-alert-message': {
      fontWeight: 700,
      lineHeight: '17px',
    },
    '.ant-alert-description': {
      fontWeight: 300,
      lineHeight: '17px',
    },
  },
  '.new-item-drawer': {
    marginTop: '59px',
    fontFamily: '"Museo Sans Rounded"',
    lineHeight: '15px',
    letterSpacing: '0.05em',
    '.ant-drawer-content-wrapper': {
      '.ant-drawer-content': {
        '.ant-drawer-header': {
          borderBottom: `1px solid ${mediumGrey2}`,
          '.ant-drawer-title': {
            color: darkGrey,
            fontFamily: '"Museo Sans Rounded"',
            lineHeight: '28px',
            fontSize: '26px',
            fontWeight: 700,
            letterSpacing: '0.05em',
          },
        },
      },
    },
  },

  '.filter-drawer': {
    marginTop: '59px',
    '.ant-drawer-content-wrapper': {
      '.ant-drawer-content': {
        '.ant-drawer-header': {
          borderBottom: `1px solid ${mediumGrey2}`,
          '.ant-drawer-title': {
            color: darkGrey,
            fontFamily: '"Museo Sans Rounded"',
            lineHeight: '28px',
            fontSize: '26px',
            fontWeight: 700,
            letterSpacing: '0.05em',
          },
        },
        '.ant-drawer-body': {
          paddingTop: '15px',
          paddingLeft: '33px',
        },
      },
    },
  },
  '.left': {
    float: 'left',
  },
  '.right': {
    float: 'right',
  },
  '.clearfix': {
    clear: 'both',
  },
  '.first-col': {
    paddingLeft: '40px !important',
    paddingRight: '30px !important',
    textAlign: 'center',
  },
  '.ant-modal-centered.mobile': {
    '.ant-modal': {
      width: '98% !important',
      '.ant-modal-body': {
        padding: '19px 20px 13.5px',
        '.avatar-wrapper': {
          marginLeft: -20,
          marginBottom: 25,
        },
        '.right-side': {
          '.label': {
            marginTop: 20,
          },
        },
        '.label': {
          fontSize: 12,
          lineHeight: '14.7px',
        },
        '.green-value': {
          fontSize: 12,
          lineHeight: '14.7px',
        },
        '.value': {
          fontSize: 16,
          lineHeight: '18px',
          color: mediumGrey,
          marginTop: 9,
          marginLeft: 15,
        },
        '.specifications': {
          padding: '13.5px 12.5px',
          '.ant-checkbox-wrapper': {
            fontSize: 12,
            marginTop: -2,
          },
        },
        h1: {
          fontSize: 16,
        },
        h3: {
          fontSize: 18,
          marginBottom: 0,
        },
        '.details': {
          padding: '22px 25px',
        },
      },
    },
  },
  '.drawer-content-wrapper': {
    '.ant-menu-item': {
      a: {
        span: {
          color: 'rgba(255, 255, 255, 0.6)',
          fontSize: 15,
          lineHeight: '140%',
          fontFamily: 'Museo Sans Rounded',
        },
      },
      svg: {
        fill: 'rgba(255, 255, 255, 0.6)',
      },
      '&.selected': {
        a: {
          span: {
            color: 'white',
            fontWeight: 'bold',
          },
        },
        svg: {
          fill: 'white',
        },
      },
    },
    '.ant-menu-submenu-title': {
      height: '30px !important',
    },
    '.ant-menu-submenu-arrow': {
      display: 'none',
    },
  },
  '.self-header': {
    '.drawer-left': {
      marginTop: '0 !important',
      height: '100vh !important',
      '.drawer-content-wrapper': {
        height: '100vh !important',
      },
    },
  },
  '.anticon-warning': {
    fontSize: '15px !important',
    marginLeft: '3px !important',
    svg: {
      color: 'rgb(189, 39, 32)',
    },
  },
  '.enter-po-autocomplete, .full-select': {
    width: 'fit-content !important',
  },
  '.date-filter-select': {
    minWidth: '100%',
    '.ant-select-selection-selected-value': {
      maxWidth: 'fit-content !important',
      paddingRight: 10,
      color: gray01,
    },
  },
  '.pas': {
    '.ant-checkbox-disabled .ant-checkbox-inner': {
      backgroundColor: '#FEFEFE !important',
    },
  },
  '.read-only .floating-menu-body': {
    display: 'none !important',
  },
  '.no-stepper .ant-input-number-handler-wrap': {
    display: 'none',
  },
  '.no-stepper input': {
    textAlign: 'right',
  },
  '.product-specification-form': {
    '.ant-form-item': {
      marginBottom: 0,
    },
  },

  '.ml10': {
    marginLeft: 10,
  },
  '.ml20': {
    marginLeft: 20,
  },
  '.mt3': {
    marginTop: 3,
  },
  '.mt5': {
    marginTop: 5,
  },
  '.mt8': {
    marginTop: 8,
  },
  '.mt10': {
    marginTop: 10,
  },
  '.mt20': {
    marginTop: 20,
  },
  '.mt30': {
    marginTop: 30,
  },

  '.mb20': {
    marginBottom: 20,
  },
  '.product-specification-span': {
    marginTop: 20,
    textAlign: 'left',
  },

  '.product-uom-table': {
    'td:nth-child(1),td:nth-child(2),td:nth-child(8)': {
      textAlign: 'left',
    },
  },
  '.product-specification-row': {
    minHeight: '40px',
    lineHeight: '40px',
  },
  '.text-left': {
    textAlign: 'left',
  },
  '.text-right': {
    textAlign: 'right',
  },
  '.text-center': {
    textAlign: 'center',
  },
  '.no-shadow .ant-table-thead': {
    boxShadow: 'none !important',
  },
  '.no-shadow .ant-table-bordered .ant-table-tbody > tr > td:last-child': {
    borderRight: '1px solid #e8e8e8',
  },
  '.overFont': {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  'td.no-pad-td': {
    padding: '0 !important',
    '&.pl-90': {
      paddingLeft: '81px !important',
    },
  },
  '.report-dp .ant-calendar-picker-input.ant-input': {
    paddingLeft: 6,
  },
  '.print-modal .ant-modal-close': {
    marginTop: 4,
  },

  '.ant-spin-nested-loading.full-width': {
    width: '100%',
    minWidth: '100%',
  },
  '.th-center': {
    textAlign: 'center',
  },
  '.fresh-green tbody tr td:nth-of-type(2)': {
    width: '20% !important',
  },
  '.fresh-green tbody tr:last-of-type td:last-of-type': {
    borderLeft: 'none',
  },
  '.request-demo-modal .ant-modal-close-x': {
    width: 24,
    height: 24,
    lineHeight: '24px',
    marginTop: 25,
    marginRight: 54,
  },
  '@media (max-width: 480px)': {
    '.request-demo-modal .ant-modal-close-x': {
      marginRight: 24,
    },
  },
  '.sales-cart-table': {
    '.ant-table-wrapper': {
      overflowX: 'auto',
      '.ant-table': {
        '.ant-table-content, .ant-table-body': {
          width: 'fit-content',
          minWidth: 'calc(100% - 2px)',
        },
      },
    },
  },
  '.print-modal': {
    '.ant-table': {
      overflowX: 'auto',
    },
  },
  div: {
    '&::-webkit-scrollbar': {
      width: 'auto',
      height: 12,
    },
    '&::-webkit-scrollbar-track': {
      // backgroundColor: '#dddddd',
      // borderRadius: 20,
      '-webkit-box-shadow': 'auto',
      '-webkit-border-radius': 'auto',
      'border-radius': 'auto',
      background: 'auto',
    },
    '&::-webkit-scrollbar-thumb': {
      // '-webkit-box-shadow': 'inset 0 0 6px rgba(0, 0, 0, 0.3)',
      backgroundColor: '#c0c0c0',
      '&:hover': {
        backgroundColor: '#7c7c7c',
      },
    },
  },
  '.ant-dropdown-menu-item.ant-dropdown-menu-item-disabled': {
    path: {
      stroke: 'rgba(0,0,0,0.25)'
    }
  },
  'svg.cancel-icon': {
    fill: 'lightgray !important',
    path: {
      stroke: 'lightgray !important'
    }
  },
  '.relative': {
    position: 'relative',
  },
  '.purchase-order-overview-table': {
    '.floating-menu-body': {
      display: 'none',
    },
    'tbody': {
      'tr:hover': {
        '.floating-menu-body': {
          display: 'block',
        },
      }
    }
  },
  'span.common-label': {
    fontWeight: 500,
    fontFamily: 'Arial'
  },
  '.hide-checkbox .ant-checkbox-wrapper': {
    display: 'none',
  },
  '.order-detail-spinner': {
    '.ant-spin-blur': {
      opacity: 1,
      '&::after': {
        opacity: 0.5,
        marginTop: 60,
      }
    }
  },
  '.ant-notification.ant-notification-topRight': {
    top: '74px !important'
  },
  '.back-reports': {
    position: 'absolute',
    marginTop: -136,
    marginLeft: -10,
    zIndex: 9999,
    'svg': {
      fill: 'white'
    }
  },
  '.multi-selector-32': {
    minWidth: 230,
    height: 32,
    '.ant-select-selection--multiple': {
      height: 32,
    },
    '.ant-select-selection__choice__content': {
      color: gray01,
    }
  },
  '.bold': {
    fontWeight: 'bold',
  },
  '.product-search-for': {
    padding: 0,
  },
  '.hide-row': {
    display: 'none'
  },
  '.ant-form-item': {
    '&.no-error': {
      paddingBottom: 0,
      marginBottom: 0,
      '.ant-form-explain': {
        display: 'none'
      }
    }
  },
  '.qbo-warning': {
    color: 'transparent',
    marginLeft: 10
  },
  '.order-status-filter .ant-select-selection__placeholder': {
    color: '#22282A'
  },
  '@media (max-width: 651px)': {
    '.css-11isuga-icon': {
      marginTop: '0 !important',
      zoom: 0.5,
      marginBottom: '20px !important'
    },
  },
})
