import { Random } from 'mockjs'

export default () => {
  const ret = Array.from({ length: Random.natural(5, 15) }, () => ({
    quote: Random.date(),
    po: Random.natural(1, 100),
    vendor: Random.name(),
    unitesOrder: Random.natural(1, 100),
    unitesConfirmed: Random.natural(1, 100),
    unitesReceived: Random.natural(1, 100),
    lot: Random.natural(1, 100),
    uom: Random.natural(1, 100),
    totalCost: Random.float(10, 100, 0, 2),
    receivedDate: Random.date(),
  }))

  return ret
}
