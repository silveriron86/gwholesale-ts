import { Random } from 'mockjs'
import { from } from 'rxjs'

export default () => {
  const ret = Array.from({ length: Random.natural(5, 15) }, () => ({
    id: Random.natural(100, 1000),
    totalCost: Random.float(10, 100, 0, 2),
    totalPrice: Random.float(100, 1000, 0, 2),
    status: ['PLACED', 'DELIVERED', 'RECEIVED'][Random.natural(0, 2)],
    deliveryDate: Random.date(),
    updatedDate: Random.date(),
    quantity: Random.natural(1, 5),
    priceSheetId: Random.natural(100, 1000),
    customer: {
      id: Random.natural(100, 1000),
      name: Random.name(),
    },
    qboId: Random.natural(100, 1000),
  }))

  return from(Promise.resolve(ret))
}
