import { random } from 'lodash'

import { SaleSection, SaleCategory } from '~/schema'



const categories = ['FRUIT', 'GRAINS', 'OTHER']

export function randomSaleCategory(): SaleCategory {
  const r = Math.random()
  let index = 0
  if (r < 0.3) {
    index = 1
  } else if (r > 0.8) {
    index = 2
  }
  return {
    wholesaleSection: randomSaleSection(),
    wholesaleCategoryId: `${5000000 + random(0, 100, false)}`,
    warehouse: 'random warehouse',
    name: categories[index],
  }
}

function randomSaleSection(): SaleSection {
  return {
    wholesaleSectionId: "Section1",
    name: `random section`,
    warehouse: 'random warehouse',
    qboId: `${100000 + random()}`,
  }
}
