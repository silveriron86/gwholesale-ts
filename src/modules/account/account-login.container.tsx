import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'

import { GlobalState } from '~/store/reducer'
import { AccountModule, AccountDispatchProps, AccountStateProps } from './account.module'

import { brightGreen, Theme } from '~/common'
import { Icon } from '~/components/icon'
import { Flex, LoginContainer, LoginHeader, loginLogoStyle } from './account-new.style';

import { CustomizedLoginForm } from './components/login-form.component'
import { MessageHeader } from '~/components';
import Form, { FormComponentProps } from 'antd/lib/form';
import { Modal, Button, Input } from 'antd';
import { DialogFooter } from '../pricesheet/components/pricesheet-detail-header.style';
import { withTheme } from 'emotion-theming';

export type AccountLoginProps = AccountDispatchProps & AccountStateProps & RouteComponentProps<{ success: string }> & { theme: Theme }


interface FormFields {
  email: string
}

interface ForgotPasswordFormProps {
  visible: boolean
  onOk: (name: string) => void
  onCancel: () => void
  theme: Theme
}

class ForgotPasswordForm extends React.PureComponent<FormComponentProps<FormFields> & ForgotPasswordFormProps> {
  render() {
    const {
      form: { getFieldDecorator },
    } = this.props
    return (
      <Modal
        visible={this.props.visible}
        onCancel={this.handleCancel}
        footer={[
          <DialogFooter key="plus">
            <Button
              type="primary"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              Request
            </Button>
            <Button onClick={this.handleCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <label>Forgot Password</label>
        <br />
        <Form.Item>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'E-mail address is required!' }],
          })(<Input placeholder="Please enter e-mail address" style={{ width: '80%' }} />)}
        </Form.Item>
      </Modal>
    )
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onOk } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        onOk(values.email)
      }
    })
  }

  handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onCancel } = this.props
    onCancel()
    form.resetFields()
  }
}

const ForgotPasswordFormModal = Form.create<FormComponentProps<FormFields> & ForgotPasswordFormProps>()(ForgotPasswordForm)



export class AccountLogin extends React.PureComponent<AccountLoginProps> {

  state = {
    email: "",
    password: "",
    showModal: false,
  }

  componentDidMount() {
    this.props.logout()
    if (this.props.match.params.success != null)
      this.props.showSuccess("Password Reset Success! Please log in with new password.")
  }


  onShowModal = () => {
    this.setState({
      showModal: true,
    })
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
    })
  }

  onSendForgotPassword = (email: string) => {
    console.log(email)
    this.props.forgotPassword(email)
    this.onCloseModal()
  }

  render() {
    return (
      <>
      <MessageHeader message={this.props.message} type={this.props.type} description={this.props.description} />

      <ForgotPasswordFormModal
        theme={this.props.theme}
        visible={this.state.showModal}
        onOk={this.onSendForgotPassword}
        onCancel={this.onCloseModal}
      />
      <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column'}}>
        <Icon type="logo2" viewBox={void 0} color={brightGreen} style={loginLogoStyle} />
        <LoginContainer>
          <Flex className="flex-div" style={{flexDirection: 'column', marginLeft: '75px'}}>
            <LoginHeader>Sign In</LoginHeader>
            <CustomizedLoginForm {...this.state} loginBytoken={this.props.loginBytoken} onShowModal={this.onShowModal} />
          </Flex>
        </LoginContainer>
      </div>
      </>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.account

export const AccountLoginContainer = withTheme(connect(AccountModule)(mapStateToProps)(AccountLogin))
