import React, { useEffect } from 'react'
import { Redirect } from 'react-router'
import { useEffectModule } from 'redux-epics-decorator'

import { AccountModule } from './account.module'

const AccountLogout: React.SFC<{}> = _props => {
  const [, action] = useEffectModule(
    AccountModule,
    () => ({}),
  )

  useEffect(() => {
    action.logout()
  }, [])

  return <Redirect to="login" />
}

export default AccountLogout
