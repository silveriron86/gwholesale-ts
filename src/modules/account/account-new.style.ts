import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { white, black, mediumGrey, brightGreen, grey, filterGreen } from '~/common'

export const Container = styled('div')({
  backgroundColor: white,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const Flex = styled('div')({
  display: 'flex',
  '&.v-center': {
    verticalAlign: 'middle'
  }
})

export const BackButton = styled('div')({
  color: brightGreen,
  fontSize: '12px',
  marginRight: '30px',
  minWidth: '190px',
  padding: '55px 0px 0px 121px',

  '& > span': {
    cursor: 'pointer',
    textTransform: 'uppercase',
    marginLeft: '8px',
    fontWeight: 700,
    letterSpacing: '0em',
  },
})

export const HeaderCrumb = styled('div')({
  color: mediumGrey,
  padding: '55px 0px 0px 0px',
  // padding: '0px 38px 0px 220px',
  fontSize: '12px',
  fontWeight: 700,
  textTransform: 'uppercase',
})

export const HeaderTitle = styled('div')({
  maxWidth: '600px',
  fontWeight: 'bold',
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '51px',
})

export const CustomerInputWrapper = styled('div')({
  display: 'flex',

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    fontWeight: 700,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const CustomerInput = styled('div')({})

export const CustomerInputTitle = styled('div')({
  color: grey,
  fontSize: '14px',
  lineHeight: '33px',
})

export const ButtonWrapper = styled('div')({
  display: 'flex',
  paddingTop: '51px',
})

export const buttonStyles = css({
  width: '167px',
  borderRadius: '20px',
  backgroundColor: brightGreen,
  borderColor: brightGreen,
})

export const SideButtonTitle = styled('div')({
  fontWeight: 700,
  paddingTop: '57px',
  color: mediumGrey,
  fontSize: '14px',
  marginBottom: '26px',
})

export const sideButtonStyles = css({
  width: '300px',
  borderRadius: '20px',
  backgroundColor: brightGreen,
  borderColor: brightGreen,
  marginBottom: '19px',
})

export const LoginContainer = styled('div')({
  backgroundColor: filterGreen,
  width: '652px',
  height: '417px',
  display: 'flex',
  '@media (max-width: 651px)': {
    width: '100%',
    height: 'auto',
    padding: 25,
    '.flex-div': {
      marginLeft: '0 !important',
      input: {
        width: '100% !important'
      }
    },
    '.login-form': {
      '.ant-form-item-children': {
        width: '100%',
        display: 'block',
        'a.forgot-pwd': {
          position: 'absolute',
          marginTop: - 50,
          display: 'block',
          right: 0
        }
      }
    }
  },
})

export const LoginHeader = styled('div')({
  backgroundColor: filterGreen,
  color: mediumGrey,
  marginTop: '46px',
  fontSize: '28px',
  fontWeight: 700,
  lineHeight: '30px',
  '@media (max-width: 651px)': {
    marginTop: 25
  }
})

export const loginLogoStyle: React.CSSProperties = {
  marginTop: '127px',
  marginBottom: '32px',
  width: '82px',
  height: '82px',
}
