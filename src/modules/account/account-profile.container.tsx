/**@jsx jsx */
import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteProps } from 'react-router'
import { jsx } from '@emotion/core'
import { Icon, Button, notification, Radio, Row, Col } from 'antd'
import _ from 'lodash';
import { MessageType } from '~/components'
import { GlobalState } from '~/store/reducer'
import { AccountModule, AccountDispatchProps, AccountStateProps } from './account.module'

import { white, brightGreen, CACHED_QBO_LINKED, CACHED_NS_LINKED, CACHED_FULLNAME, CACHED_ACCOUNT_TYPE } from '~/common'
import {
  Flex,
  Container,
  BackButton,
  HeaderCrumb,
  HeaderTitle,
  SideButtonTitle,
  sideButtonStyles,
} from './account-new.style'

import { ImportCustomerModal, ImportItemModal, ImportVendorModal, ImportNSCustomerModal, ImportNSVendorModal, ImportNSItemModal, ImportNSOrderSalesModal, ImportNSOrderPurchaseModal } from './components/import-table.component'
import { AccountProfileForm } from './components/account-profile-form.component'
import { cloneDeep } from 'lodash'
import { SaleItem, TempCustomer, QBOAccount, NSAccount, NSOrderImport, UserRole } from '~/schema'
import PageLayout from '~/components/PageLayout'
import { GeneralSettingWrapper } from '../setting/theme.style';

export type AccountProfileProps = AccountDispatchProps &
  AccountStateProps &
  RouteProps & {
    isCompanySetting?: boolean
    userSetting: any
  }

interface AccountState {
  showModalConnectQB: boolean
  showModalConnectNS: boolean
  showModalImportItem: boolean
  showModalImportCustomer: boolean
  showModalImportVendor: boolean
  showModalImportNSItem: boolean
  showModalImportNSCustomer: boolean
  showModalImportNSVendor: boolean
  showModalImportNSSalesOrder: boolean
  showModalImportNSPurchaseOrder: boolean
  search: string
  message: string
  type: MessageType | null
  selectedVendors: QBOAccount[]
  selectedRows: QBOAccount[]
  selectedNSVendors: NSAccount[]
  selectedNSCustomers: NSAccount[]
  selectedItems: SaleItem[]
  selectedOrders: NSOrderImport[]
}

export class AccountProfile extends React.PureComponent<AccountProfileProps, AccountState> {

  qboRealmId = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
  nsRealmId = localStorage.getItem(CACHED_NS_LINKED) != 'null'

  state = {
    showModalConnectQB: false,
    showModalConnectNS: false,
    showModalImportItem: false,
    showModalImportCustomer: false,
    showModalImportVendor: false,
    showModalImportNSItem: false,
    showModalImportNSCustomer: false,
    showModalImportNSVendor: false,
    showModalImportNSSalesOrder: false,
    showModalImportNSPurchaseOrder: false,
    search: '',
    editValues: {},
    message: '',
    type: null,
    selectedRows: [],
    selectedItems: [],
    selectedVendors: [],
    selectedNSVendors: [],
    selectedNSCustomers: [],
    selectedOrders: [],
    integrationMethod: !this.qboRealmId && !this.nsRealmId ? 0 : this.qboRealmId ? 1 : 2
  }

  componentDidMount() {
    this.props.resetLoading()
    this.props.getUser()
  }

  componentWillUnmount() {
    this.props.dispose$()
  }

  componentWillReceiveProps(nextProps: AccountProfileProps) {
    if (nextProps.message !== this.state.message) {
      this.setState({
        message: nextProps.message,
        type: nextProps.type,
      })
      if (nextProps.type != null) {
        notification[nextProps.type!]({
          message: nextProps.type!.toLocaleUpperCase(),
          description: nextProps.message,
          onClose: this.onCloseNotification,
          duration: nextProps.type === 'success' ? 5: 4.5
        })
      }
      this.setState({
        showModalImportCustomer: nextProps.showModalImportCustomer,
        showModalImportVendor: nextProps.showModalImportVendor,
        showModalImportItem: nextProps.showModalImportItem,
        showModalImportNSCustomer: nextProps.showModalNSImportCustomer,
        showModalImportNSVendor: nextProps.showModalNSImportVendor,
        showModalImportNSItem: nextProps.showModalNSImportItem,
        showModalImportNSSalesOrder: nextProps.showModalImportNSSalesOrder,
        showModalImportNSPurchaseOrder: nextProps.showModalImportNSPurchaseOrder,
      })
    }
  }

  onCloseNotification = () => {
    this.props.resetLoading()
  }

  saveForm = (values: any) => {
    console.log(values)
    const { email, firstName, lastName, password } = values
    localStorage.setItem(CACHED_FULLNAME, `${firstName} ${lastName}`)
    this.props.updateUser({
      email,
      firstName,
      lastName,
    })

    if (password) {
      this.props.updatePassword({
        token: values.password,
      })
    }
  }

  renderQboButtons() {

    // const nsRealmId = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    // const buttons = qboRealmId ?
    //   (
    //     <div>
    //       {this.renderImportCustomerModal()}
    //       {this.renderImportVendorModal()}
    //       {this.renderImportItemModal()}
    //     </div>
    //   ) : (<span></span>)
    return (
      <div>
        <Flex>{this.renderImportQuickbookButton()}</Flex>
        <div>
          {this.renderImportCustomerModal()}
          {this.renderImportVendorModal()}
          {this.renderImportItemModal()}
        </div>
      </div>
    )
  }

  renderNsButtons() {
    const { user } = this.props
    return (
      <div>
        <Flex>{this.renderImportNetsuiteButton()}</Flex>
        <div>
          {this.renderImportNSCustomerModal()}
          {this.renderImportNSVendorModal()}
          {this.renderImportNSItemModal()}
          {this.renderImportNSSalesOrder()}
          {this.renderImportNSPurchaseOrder()}
        </div>
      </div>
    )
  }

  integrationOptionChange = (e: any) => {
    this.setState({ integrationMethod: e.target.value })
  }

  render() {
    if (!this.props.user) return null
    const { isCompanySetting } = this.props
    return isCompanySetting === true ? (
      <div>
        <div>
          <Radio.Group onChange={this.integrationOptionChange} value={this.state.integrationMethod} style={{ display: 'flex', flexDirection: 'column' }}>
            <Radio value={0} style={{ padding: '30px 0' }}>No integration enabled</Radio>
            <Radio value={1} style={{ paddingBottom: 30 }}>Enable QuickBooks Connector</Radio>
            {this.state.integrationMethod == 1 && this.renderQboButtons()}
            <Radio value={2} style={{ padding: this.state.integrationMethod == 2 ? '0 0 30px' : '50px 0 30px' }}>Enable NetSuite Connector</Radio>
            {this.state.integrationMethod == 2 && this.renderNsButtons()}
          </Radio.Group>
        </div>
      </div>
    ) : (
      <PageLayout noSubMenu={true}>
        <Container>
          {/*
            <MessageHeader message={this.props.message} type={this.props.type} description={this.props.description} /> */}
          <Flex>
            <BackButton>
              <div style={{ padding: '0px 10px', cursor: 'pointer' }} onClick={this.onClickBack}>
                <Icon type="arrow-left" style={{ marginRight: 12 }} />
                <span style={{ fontWeight: 700 }}>BACK</span>
              </div>
            </BackButton>
            <div>
              <Flex>
                <HeaderCrumb>Account</HeaderCrumb>
              </Flex>
              <HeaderTitle>My Profile</HeaderTitle>
              <Flex>
                <AccountProfileForm
                  {...this.props}
                  saveForm={this.saveForm}
                  user={this.props.user}
                  getUser={this.props.getUser}
                />
                {/* {this.renderImportQuickbookButton()} */}
              </Flex>
              {/* {this.renderImportCustomerModal()}
                {this.renderImportVendorModal()}
                {this.renderImportItemModal()} */}
            </div>
          </Flex>
        </Container>
      </PageLayout>
    )
  }

  private onClickConnectQB = () => {
    const { user } = this.props
    if (!user) return
    if (__DEV__) {
      window.location.href = `http://localhost:8080/grubmarket/connectToQuickbooks?userId=` + user.id + `&redirectLink=` + encodeURI("http://localhost:8081/myaccount/profile")
      /*
      window.location.href =
        `http://staging.grubmarket.com/connectToQuickbooks?userId=` +
        user.id +
        `&redirectLink=` +
        encodeURI('http://staging.grubmarket.com/gwholesaler/v2/') +
        encodeURIComponent('#/myaccount/profile')
        */
    } else {
      window.location.href =
        //`http://wholesaleware.grubmarket.com/connectToQuickbooks?userId=` +
        `https://aut2.wholesaleware.com/connectToQuickbooks?userId=` +
        user.id +
        `&redirectLink=` +
        //encodeURI('http://wholesaleware.grubmarket.com/gwholesaler/v2/') +
        encodeURI('https://wholesaleware.com/') +
        encodeURIComponent('#/myaccount/setting')
    }
  }


  private onClickConnectNS = () => {
    const { user } = this.props
    if (!user) return
    if (__DEV__) {
      window.location.href = `http://localhost:8080/grubmarket/netsuiteConnect?userId=` + user.id + `&redirectLink=` + encodeURI("http://localhost:8081/myaccount/profile")
      /*window.location.href =
        `http://staging.grubmarket.com/netsuiteConnect?userId=` +
        user.id +
        `&redirectLink=` +
        encodeURI('http://staging.grubmarket.com/gwholesaler/v2/') +
        encodeURIComponent('#/myaccount/profile')
        */
    } else {
      window.location.href =
        //`http://wholesaleware.grubmarket.com/connectToQuickbooks?userId=` +
        `https://aut2.wholesaleware.com/netsuiteConnect?userId=` +
        user.id +
        `&redirectLink=` +
        //encodeURI('http://wholesaleware.grubmarket.com/gwholesaler/v2/') +
        encodeURI('https://wholesaleware.com/') +
        encodeURIComponent('#/myaccount/setting')
    }
  }

  private onClickImportNSItemButton = () => {
    this.props.resetLoading()
    this.props.getNSItems()
    this.setState({
      showModalImportNSItem: true,
      selectedItems: [],
    })
  }

  private onClickImportNSCustomerButton = () => {
    this.props.resetLoading()
    this.props.getNSCustomers()
    this.setState({
      showModalImportNSCustomer: true,
      selectedNSCustomers: [],
    })
  }

  private onClickImportNSVendorButton = () => {
    this.props.resetLoading()
    this.props.getNSVendors()
    this.setState({
      showModalImportNSVendor: true,
      selectedNSVendors: [],
    })
  }


  private onClickImportNSSalesOrderButton = () => {
    this.props.resetLoading()
    this.props.getNSSalesOrder()
    this.setState({
      showModalImportNSSalesOrder: true,
      selectedOrders: [],
    })
  }

  private onClickImportNSPurchaseOrderButton = () => {
    this.props.resetLoading()
    this.props.getNSPurchaseOrder()
    this.setState({
      showModalImportNSPurchaseOrder: true,
      selectedOrders: [],
    })
  }

  private onClickNSUpdateItemsDefaultUOMButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateItemsDefaultUOM()
  }

  private onClickNSUpdateItemsUomLastSaleButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateItemsUomLastSale()
  }

  private onClickNSUpdateClientsPaymentButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateClientsPaymentFromNS()
  }

  private onClickUpdateItemsToNSButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateItemsToNS()
  }

  private onClickNSUpdateClientsBusinessTypeButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateClientsBusinessTypeFromNS()
  }

  private onClickNSUpdateItemBasePriceButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateItemsBasePrice()
  }

  private onClickNSUpdateItemsFromNSButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateItemsFromNS()
  }

  private onClickNSUpdateCustomersFromNSButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateCustomersFromNS()
  }

  private onClickNSUpdateCustomerAddressFromNSButton = () => {
    this.props.resetLoading()
    this.props.nsUpdateCustomerAddressFromNS()
  }

  private onClickImportNSUOMButton = () => {
    this.props.resetLoading()
    this.props.nsImportUOM()
  }

  private onClickImportNSExtraItemButton = () => {
    this.props.resetLoading()
    this.props.nsImportExtraItem()
  }

  private onClickImportNSPaymentTermButton = () => {
    this.props.resetLoading()
    this.props.nsImportTerm()
  }

  private onClickImportNSFreightButton = () => {
    this.props.resetLoading()
    this.props.nsImportFreight()
  }

  private onClickImportNSVendorTypeButton = () => {
    this.props.resetLoading()
    this.props.nsImportVendorType()
  }

  private onClickImportNSCustomerTypeButton = () => {
    this.props.resetLoading()
    this.props.nsImportCustomerType()
  }

  private onClickImportNSCategoryButton = () => {
    this.props.resetLoading()
    this.props.nsImportCategory()
  }

  private onClickImportQBItemButton = () => {
    this.props.resetLoading()
    this.props.getQBItems()
    this.setState({
      showModalImportItem: true,
      selectedItems: [],
    })
  }

  private onClickImportQBCustomerButton = () => {
    this.props.resetLoading()
    this.props.getQBCustomers()
    this.setState({
      showModalImportCustomer: true,
      selectedRows: [],
    })
  }

  private onClickImportQBVendorButton = () => {
    this.props.resetLoading()
    this.props.getQBVendors()
    this.setState({
      showModalImportVendor: true,
      selectedVendors: [],
    })
  }

  private onClickImportQBTermButton = () => {
    this.props.resetLoading()
    this.props.qbImportTerm()
  }

  private onClickBack = () => {
    this.props.goBack()
  }

  private renderImportQuickbookButton() {
    const { integrationMethod } = this.state
    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE) && localStorage.getItem(CACHED_ACCOUNT_TYPE) != 'null' ? localStorage.getItem(CACHED_ACCOUNT_TYPE) : UserRole.ADMIN
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Button type="primary" size="large" css={sideButtonStyles} onClick={this.onClickConnectQB} disabled={integrationMethod != 1}>
          Connect to Quickbooks (Admin only)
        </Button>
        <div>
          <Button
            type="primary"
            size="large"
            css={sideButtonStyles}
            style={{ backgroundColor: white, color: brightGreen }}
            onClick={this.onClickImportQBItemButton}
            disabled={integrationMethod != 1}
          >
            Import Item From QBO
          </Button>
          <Button
            type="primary"
            size="large"
            css={sideButtonStyles}
            style={{ backgroundColor: white, color: brightGreen }}
            onClick={this.onClickImportQBCustomerButton}
            disabled={integrationMethod != 1}
          >
            Import Customer From QBO
          </Button>
          <Button
            type="primary"
            size="large"
            css={sideButtonStyles}
            style={{ backgroundColor: white, color: brightGreen }}
            onClick={this.onClickImportQBVendorButton}
            disabled={integrationMethod != 1}
          >
            Import Vendor From QBO
          </Button>
          <Button
            type="primary"
            size="large"
            css={sideButtonStyles}
            style={{ backgroundColor: white, color: brightGreen }}
            onClick={this.onClickImportQBTermButton}
            disabled={integrationMethod != 1}
          >
            Import Terms From QBO
          </Button>
        </div>
      </div>
    )
  }

  private renderImportNetsuiteButton() {
    const { userSetting } = this.props
    console.log(userSetting)
    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE) && localStorage.getItem(CACHED_ACCOUNT_TYPE) != 'null' ? localStorage.getItem(CACHED_ACCOUNT_TYPE) : UserRole.ADMIN
    const { integrationMethod } = this.state
    return (
      <GeneralSettingWrapper hidden={accountType !== UserRole.SUPERADMIN}>
          <Row gutter={24}>
            <Col span={12} offset={0}>
              <Button type="primary" size="large" css={sideButtonStyles} onClick={this.onClickConnectNS} disabled={integrationMethod != 2}>
                Connect to Netsuite (Admin only)
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSItemButton}
                disabled={integrationMethod != 2}
              >
                Import Item From NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSCustomerButton}
                disabled={integrationMethod != 2}
              >
                Import Customer From NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSVendorButton}
                disabled={integrationMethod != 2}
              >
                Import Vendor From NS
              </Button>
              {/* <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSExtraItemButton}
                disabled={integrationMethod != 2}
              >
                Import Extra Charge Items From NS
              </Button> */}
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSCategoryButton}
                disabled={integrationMethod != 2}
              >
                Import Category From NS
              </Button>
              <Flex className='v-center'>
                <Button
                  type="primary"
                  size="large"
                  css={sideButtonStyles}
                  style={{ backgroundColor: white, color: brightGreen }}
                  onClick={this.onClickImportNSUOMButton}
                  disabled={integrationMethod != 2}
                >
                  Import UOM From NS
                </Button>
                <div style={{ padding: 8, marginLeft: 12 }}>
                  NetSuite UOM configuration type: {userSetting && userSetting.nsCustomUom === true ? 'Custom' : 'Native'}
                </div>
              </Flex>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSPaymentTermButton}
                disabled={integrationMethod != 2}
              >
                Import Payment Term From NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSFreightButton}
                disabled={integrationMethod != 2}
              >
                Import Freight From NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSCustomerTypeButton}
                disabled={integrationMethod != 2}
              >
                Import Customer Business Type From NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSVendorTypeButton}
                disabled={integrationMethod != 2}
              >
                Import Vendor Business Type From NS
              </Button>
            </Col>
            <Col span={12} offset={0}>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSSalesOrderButton}
                disabled={integrationMethod != 2}
              >
                Import Sales Orders From NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickImportNSPurchaseOrderButton}
                disabled={integrationMethod != 2}
              >
                Import Purchase Orders From NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateItemsFromNSButton}
                disabled={integrationMethod != 2}
              >
                Update Items from NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateItemBasePriceButton}
                disabled={integrationMethod != 2}
              >
                Update Items Price from NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateItemsDefaultUOMButton}
                disabled={integrationMethod != 2}
              >
                Update Items Default UOMs from NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateCustomersFromNSButton}
                disabled={integrationMethod != 2}
              >
                Update Customers from NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateCustomerAddressFromNSButton}
                disabled={integrationMethod != 2}
              >
                Update Customers Address from NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateClientsPaymentButton}
                disabled={integrationMethod != 2}
              >
                Update Clients Payment Term from NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateClientsBusinessTypeButton}
                disabled={integrationMethod != 2}
              >
                Update Clients Business type from NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickUpdateItemsToNSButton}
                disabled={integrationMethod != 2}
                hidden={true}
              >
                Update Items to NS
              </Button>
              <Button
                type="primary"
                size="large"
                css={sideButtonStyles}
                style={{ backgroundColor: white, color: brightGreen }}
                onClick={this.onClickNSUpdateItemsUomLastSaleButton}
                disabled={integrationMethod != 2}
                hidden={true}
              >
                Update Items Sales UOM
              </Button>
            </Col>
          </Row>
      </GeneralSettingWrapper>
    )
  }

  private renderImportCustomerModal() {
    const { showModalImportCustomer } = this.state
    const { importCustomers, loadingCustomers } = this.props

    const onClickImportQBCustomer = () => {
      this.props.resetLoading()
      this.props.createCustomers(this.state.selectedRows)
    }
    const onRowSelection = (selectedRows: QBOAccount[]) => {
      this.setState({
        selectedRows: selectedRows,
      })
    }
    const onCloseImportQBCustomer = () => {
      this.setState({
        showModalImportCustomer: false,
      })
    }

    return (
      <ImportCustomerModal
        visible={showModalImportCustomer}
        customers={importCustomers}
        loading={loadingCustomers}
        onImport={onClickImportQBCustomer}
        onSelectionChange={onRowSelection}
        onCancel={onCloseImportQBCustomer}
      />
    )
  }

  private renderImportNSCustomerModal() {
    const { showModalImportNSCustomer } = this.state
    const { importNSCustomers, loadingCustomers } = this.props

    const onClickImportNSCustomer = () => {
      this.props.resetLoading()
      this.props.createCustomers(this.state.selectedNSCustomers)
    }
    const onRowSelection = (selectedRows: NSAccount[]) => {
      this.setState({
        selectedNSCustomers: selectedRows,
      })
    }
    const onCloseImportNSCustomer = () => {
      this.setState({
        showModalImportNSCustomer: false,
      })
    }

    return (
      <ImportNSCustomerModal
        visible={showModalImportNSCustomer}
        customers={importNSCustomers}
        loading={loadingCustomers}
        onImport={onClickImportNSCustomer}
        onSelectionChange={onRowSelection}
        onCancel={onCloseImportNSCustomer}
      />
    )
  }

  private renderImportVendorModal() {
    const { showModalImportVendor } = this.state
    const { importVendors, loadingVendors } = this.props

    const onClickImportQBVendor = () => {
      this.props.resetLoading()
      this.props.createVendors(this.state.selectedVendors)
    }
    const onRowSelection = (selectedRows: QBOAccount[]) => {
      this.setState({
        selectedVendors: selectedRows,
      })
    }
    const onCloseImportQBVendor = () => {
      this.setState({
        showModalImportVendor: false,
      })
    }

    return (
      <ImportVendorModal
        visible={showModalImportVendor}
        vendors={importVendors}
        loading={loadingVendors}
        onImport={onClickImportQBVendor}
        onSelectionChange={onRowSelection}
        onCancel={onCloseImportQBVendor}
      />
    )
  }

  private renderImportNSVendorModal() {
    const { showModalImportNSVendor } = this.state
    const { importNSVendors, loadingVendors } = this.props

    const onClickImportNSVendor = () => {
      this.props.resetLoading()
      this.props.createVendors(this.state.selectedNSVendors)
    }
    const onRowSelection = (selectedRows: NSAccount[]) => {
      this.setState({
        selectedNSVendors: selectedRows,
      })
    }
    const onCloseImportNSVendor = () => {
      this.setState({
        showModalImportNSVendor: false,
      })
    }

    return (
      <ImportNSVendorModal
        visible={showModalImportNSVendor}
        vendors={importNSVendors}
        loading={loadingVendors}
        onImport={onClickImportNSVendor}
        onSelectionChange={onRowSelection}
        onCancel={onCloseImportNSVendor}
      />
    )
  }


  private renderImportNSSalesOrder() {
    const { showModalImportNSSalesOrder } = this.state
    const { importNSOrder, loadingNSOrder } = this.props

    const onClickImportNSSalesOrder = () => {
      this.props.resetLoading()
      this.props.importNSSalesOrders(this.state.selectedOrders)
    }
    const onRowSelection = (selectedRows: NSOrderImport[]) => {
      this.setState({
        selectedOrders: selectedRows,
      })
    }
    const onCloseImportNSSalesOrder = () => {
      this.setState({
        showModalImportNSSalesOrder: false,
      })
    }

    return (
      <ImportNSOrderSalesModal
        visible={showModalImportNSSalesOrder}
        orders={importNSOrder}
        loading={loadingNSOrder}
        onImport={onClickImportNSSalesOrder}
        onSelectionChange={onRowSelection}
        onCancel={onCloseImportNSSalesOrder}
      />
    )
  }


  private renderImportNSPurchaseOrder() {
    const { showModalImportNSPurchaseOrder } = this.state
    const { importNSOrder, loadingNSOrder } = this.props

    const onClickImportNSPurchaseOrder = () => {
      this.props.resetLoading()
      this.props.importNSPurchaseOrders(this.state.selectedOrders)
    }
    const onRowSelection = (selectedRows: NSOrderImport[]) => {
      this.setState({
        selectedOrders: selectedRows,
      })
    }
    const onCloseImportNSPurchaseOrder = () => {
      this.setState({
        showModalImportNSPurchaseOrder: false,
      })
    }

    return (
      <ImportNSOrderPurchaseModal
        visible={showModalImportNSPurchaseOrder}
        orders={importNSOrder}
        loading={loadingNSOrder}
        onImport={onClickImportNSPurchaseOrder}
        onSelectionChange={onRowSelection}
        onCancel={onCloseImportNSPurchaseOrder}
      />
    )
  }

  private renderImportItemModal() {
    const { importSaleItems, loadingItems } = this.props
    const { showModalImportItem, search } = this.state

    let listItems: SaleItem[] = _.uniqBy(cloneDeep(importSaleItems), 'qboId');
    if (search) {
      const searchStr = search.toLowerCase();
      listItems = listItems.filter((item) => {
        return (item.variety.toLowerCase().indexOf(searchStr)) >= 0 || (item.SKU && item.SKU.toLowerCase().indexOf(searchStr) >= 0)
      })
    }

    const onClickImportQBItem = () => {
      this.props.resetLoading()
      this.props.createItems(this.state.selectedItems)
    }
    const onRowSelection = (selectedItems: SaleItem[]) => {
      this.setState({
        selectedItems: selectedItems,
      })
    }
    const onCloseImportQBItem = () => {
      this.setState({
        showModalImportItem: false,
      })
    }
    const onSearch = (value: string) => {
      this.setState({
        search: value
      })
    }

    return (
      <ImportItemModal
        visible={showModalImportItem}
        items={listItems}
        loading={loadingItems}
        onImport={onClickImportQBItem}
        onSelectionChange={onRowSelection}
        onSearch={onSearch}
        onCancel={onCloseImportQBItem}
      />
    )
  }

  private renderImportNSItemModal() {
    const { importSaleItems, loadingItems } = this.props
    const { showModalImportNSItem, search } = this.state

    let listItems: SaleItem[] = _.uniqBy(cloneDeep(importSaleItems), 'nsId');
    console.log(importSaleItems, listItems);
    if (search) {
      const searchStr = search.toLowerCase();
      listItems = listItems.filter((item) => {
        return (item.variety.toLowerCase().indexOf(searchStr)) >= 0 || (item.SKU && item.SKU.toLowerCase().indexOf(searchStr) >= 0)
      })
    }

    const onClickImportNSItem = () => {
      this.props.resetLoading()
      this.props.createItems(this.state.selectedItems)
    }
    const onRowSelection = (selectedItems: SaleItem[]) => {
      this.setState({
        selectedItems: selectedItems,
      })
    }
    const onCloseImportNSItem = () => {
      this.setState({
        showModalImportNSItem: false,
      })
    }
    const onSearch = (value: string) => {
      this.setState({
        search: value,
      })
    }

    return (
      <ImportNSItemModal
        visible={showModalImportNSItem}
        items={listItems}
        loading={loadingItems}
        onImport={onClickImportNSItem}
        onSelectionChange={onRowSelection}
        onSearch={onSearch}
        onCancel={onCloseImportNSItem}
      />
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.account

export const AccountProfileContainer = connect(AccountModule)(mapStateToProps)(AccountProfile)
