import {
  Module,
  EffectModule,
  ModuleDispatchProps,
  Effect,
  StateObservable,
  DefineAction,
  Reducer,
} from 'redux-epics-decorator'

import { Observable, of, from } from 'rxjs'
import { map, switchMap, takeUntil, catchError, endWith, tap, mapTo, mergeMap, delay } from 'rxjs/operators'
import { push, goBack } from 'connected-react-router'
import { Action } from 'redux-actions'
import { GlobalState } from '~/store/reducer'
import {
  CACHED_AUTH_TOKEN,
  CACHED_USER_ID,
  CACHED_ACCOUNT_TYPE,
  CACHED_COMPANY,
  CACHED_FULLNAME,
  CACHED_IS_AGREED,
  CACHED_ACCESSTOKEN,
  CACHED_QBO_LINKED,
  CACHED_NS_LINKED,
} from '~/common'
import { AccountService } from './account.service'
import { CustomerService } from '../customers/customers.service'
import { InventoryService } from '../inventory/inventory.service'
import {
  AccountUser,
  TempCustomer,
  SaleItem,
  // CustomerStatus,
  SaleSection,
  SaleCategory,
  AuthUser,
  CustomerStatus,
  CustomerType,
  QBOAccount,
  Address,
  UserRole,
  NSAccount,
  NSOrderImport,
  // CustomerType,
  // Address,
} from '~/schema'
import { MessageType } from '~/components'
import { checkError, checkMessage, validateEmail, responseHandler } from '~/common/utils'

import { USER_LOGIN, USER_LOGOUT_SUCCESS } from '~/root.module'
import { notification } from 'antd'
import { VendorService } from '../vendors/vendors.service'

export interface AccountStateProps {
  user?: AccountUser
  importCustomers: QBOAccount[]
  importVendors: QBOAccount[]
  importNSCustomers: NSAccount[]
  importNSVendors: NSAccount[]
  importSaleItems: SaleItem[] | []
  importNSOrder: NSOrderImport[]
  loadingVendors: boolean
  loadingCustomers: boolean
  loadingItems: boolean
  loadingUOM: boolean
  loadingCategory: boolean
  loadingNSOrder: boolean
  message: string
  description: string
  type: MessageType | null
  showModalImportCustomer: boolean
  showModalImportVendor: boolean
  showModalImportItem: boolean
  showModalImportNSCustomer: boolean
  showModalImportNSVendor: boolean
  showModalImportNSItem: boolean
  showModalImportNSSalesOrder: boolean
  showModalImportNSPurchaseOrder: boolean
}

interface LoginPayload {
  email: string
  password: string
}

@Module('account')
export class AccountModule extends EffectModule<AccountStateProps> {
  defaultState = {
    importCustomers: [],
    importVendors: [],
    importNSCustomers: [],
    importNSVendors: [],
    importNSOrder: [],
    importSaleItems: [],
    loadingCustomers: true,
    loadingVendors: true,
    loadingItems: true,
    loadingUOM: true,
    loadingCategory: true,
    message: '',
    description: '',
    type: null,
    showModalImportCustomer: false,
    showModalImportVendor: false,
    showModalImportItem: false,
    showModalImportNSCustomer: false,
    showModalImportNSVendor: false,
    showModalImportNSItem: false,
    showModalImportNSSalesOrder: false,
    showModalImportNSPurchaseOrder: false,
  }

  @DefineAction() dispose$!: Observable<void>
  constructor(
    private readonly account: AccountService,
    private readonly customer: CustomerService,
    private readonly inventory: InventoryService,
    private readonly vendor: VendorService,
  ) {
    super()
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect()
  goto(action$: Observable<string>) {
    return action$.pipe(
      map((path) => {
        if (path === 'INITIAL_SCREEN') {
          localStorage.setItem('VISITED_FROM_LOGIN', 'TRUE')
          const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)
          if (accountType == 'buyer') {
            path = '/purchase-orders'
          } else {
            path = '/sales-orders'
          }
          // path = localStorage.getItem(CACHED_ACCOUNT_TYPE) === UserRole.CUSTOMER ? '/sales-orders' : '/customers'

        }
        return push(path)
      }),
    )
  }

  @Effect({
    done: (state: AccountStateProps, action: Action<any>) => {
      console.log(action.payload)
      return { ...state, user: action.payload }
    },
  })
  login(action$: Observable<LoginPayload>) {
    return action$.pipe(
      switchMap((data) => {
        return this.account.loginUser(data).pipe(
          switchMap((data) => of(this.checkLogin(data.body))),
          map(this.createAction('done')),
          /*
            endWith(this.markAsGlobal(createAction('FETCH_GLOBAL_DATA')({}))),
            endWith(this.createActionFrom(this.settingModule.getTheme)()),
            endWith(this.createActionFrom(this.settingModule.getLogo)()),
            endWith(this.createActionFrom(this.goto)("/customers")),
             */
          endWith(this.markAsGlobal(USER_LOGIN())),
          endWith(this.createActionFrom(this.goto)('INITIAL_SCREEN')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }
  @Effect({
    done: (state: AccountStateProps, action: Action<any>) => {
      console.log(action.payload)
      if (action.payload.accountType === 'customer') {
        localStorage.setItem('isAgreed', 'true')
      }
      return { ...state, user: action.payload }
    },
  })
  loginBytoken(action$: Observable<LoginPayload>) {
    return action$.pipe(
      switchMap((data) => {
        return this.account.getAccessToken(data).pipe(
          switchMap((tokenData) => {
            if (tokenData.access_token) {
              localStorage.setItem(CACHED_ACCESSTOKEN, `${tokenData.access_token}`)
              localStorage.removeItem('LAST_USED_TARGET_FULFILLMENT_DATE')
            }
            return this.account.getUserInfo(tokenData.access_token).pipe(
              switchMap((data) => of(this.checkLogin(data.body))),
              map(this.createAction('done')),
              endWith(this.markAsGlobal(USER_LOGIN())),
              endWith(this.createActionFrom(this.goto)('INITIAL_SCREEN')),
              catchError((error) => of(checkError(error))),
            )
          }),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Reducer()
  showSuccess(state: AccountStateProps, action: Action<string>) {
    return {
      ...state,
      message: action.payload,
      type: MessageType.SUCCESS,
    }
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return { ...state, user: null }
    },
  })
  logout(action$: Observable<void>) {
    return action$.pipe(
      map(this.createAction('done')),
      tap(() => {
        localStorage.clear()
      }),
      mapTo(this.markAsGlobal(USER_LOGOUT_SUCCESS())),
    )
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return {
        ...state,
        loadingItems: true,
        loadingCustomers: true,
        loadingVendors: true,
        loadingUOM: true,
        loadingCategory: true,
        message: '',
        description: '',
        type: null,
        showModalImportItem: false,
        showModalImportVendor: false,
        showModalImportCustomer: false,
        showModalImportNSItem: false,
        showModalImportNSVendor: false,
        showModalImportNSCustomer: false,
      }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return {
        ...state,
        // message: 'Update User Successful', type: MessageType.SUCCESS
      }
    },
    error_message: (state: AccountStateProps, { payload }: Action<AccountUser>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateUser(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.account.updateUser(state$.value.currentUser.userId, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.markAsGlobal(USER_LOGIN())),
          endWith(this.createActionFrom(this.getUser)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return {
        ...state,
      }
    },
    error_message: (state: AccountStateProps, { payload }: Action<AccountUser>) => {
      return {
        ...state,
      }
    },
  })
  updatePassword(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.account.updatePassword(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      if (payload.status === 404) {
        notification.error({
          message: 'You account/password is incorrect'
        })
      }
      return { ...state, message: 'Reset password e-mail sent', type: MessageType.SUCCESS }
    },
  })
  forgotPassword(action$: Observable<string>) {
    return action$.pipe(
      switchMap((data: string) =>
        this.account.forgotPassword(data).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return { ...state, message: 'Password Set with Success', type: MessageType.SUCCESS }
    },
    error_message: (state: AccountStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  resetPassword(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.account.resetPassword(data).pipe(
          switchMap((data) => of(checkMessage(data, 'RESET_PASSWORD_COMPLETE'))),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.goto)('/login/success')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }


  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateItemsDefaultUOM(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateItemsDefaultUOM().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateItemsBasePrice(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateItemsBasePrice().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }


  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateItemsFromNS(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateItemsFromNS().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }


  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateCustomersFromNS(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateCustomersFromNS().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsImportUOM(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportUOM().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsImportExtraItem(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportExtraItem().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsImportTerm(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportTerm().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  qbImportTerm(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportTerm().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsImportFreight(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportFreight().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateItemsUomLastSale(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateItemsUomLastSale().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateItemsToNS(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateItemsToNS().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateCustomerAddressFromNS(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateCustomerAddressFromNS().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateClientsBusinessTypeFromNS(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateClientsBusinessTypeFromNS().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsUpdateClientsPaymentFromNS(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsUpdateClientsPaymentFromNS().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsImportCustomerType(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportCustomerType().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsImportVendorType(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportVendorType().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, loadingUOM: true }
    },
    error_message: (state: AccountStateProps, action: Action<any>) => {
      return {
        ...state,
        error: action.payload,
        loadingUOM: true,
      }
    },
  })
  nsImportCategory(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.nsImportCategory().pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<AccountUser>) => {
      return { ...state, user: payload }
    },
    error_message: (state: AccountStateProps, action: Action<AccountUser[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
      }
    },
  })
  getUser(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.account.getUser(state$.value.currentUser.userId).pipe(
          switchMap((data) => of(this.formatAccount(data.body.data))),
          map(this.createAction('done'), takeUntil(this.dispose$)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importSaleItems: payload, loadingItems: false }
    },
  })
  getQBItems(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory
          .getAllItems(state$.value.currentUser ? state$.value.currentUser.company : 'GRUBMARKET', null)
          .pipe(
            switchMap((data1) =>
              this.inventory.getImportItems().pipe(
                switchMap((data2) => of(this.formatInventory(data1.body.data, data2))),
                takeUntil(this.dispose$),
                map(this.createAction('done')),
                catchError((error) => of(checkError(error))),
              ),
            ),
          ),
      ),
    )
  }


  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importSaleItems: payload, loadingItems: false }
    },
  })
  getNSItems(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory
          .getAllItems(state$.value.currentUser ? state$.value.currentUser.company : 'GRUBMARKET', null, -1)
          .pipe(
            switchMap((data1) =>
              this.inventory.getImportNSItems().pipe(
                switchMap((data2) => of(this.formatNSInventory(data1.body.data, data2))),
                takeUntil(this.dispose$),
                map(this.createAction('done')),
                catchError((error) => of(checkError(error))),
              ),
            ),
          ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importNSOrder: payload, loadingNSOrder: false }
    },
  })
  getNSSalesOrder(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getImportNSSalesOrder().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importNSOrder: payload, loadingNSOrder: false }
    },
  })
  getNSPurchaseOrder(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getImportNSPurchaseOrder().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, action: Action<any>) => {
      //window.location.reload()
      return {
        ...state,
        loading: false,
      }
    },
  })
  importNSSalesOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orders: any) => from(orders)),
      mergeMap((order: any) => {
        return this.account.nsImportSalesOrder(order.orderId)
      }, 2),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: AccountStateProps, action: Action<any>) => {
      //window.location.reload()
      return {
        ...state,
        loading: false,
      }
    },
  })
  importNSPurchaseOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orders: any) => from(orders)),
      mergeMap((order: any) => {
        return this.account.nsImportPurchaseOrder(order.orderId)
      }, 2),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importNSCustomers: payload, loadingCustomers: false }
    },
  })
  getNSCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser.userId).pipe(
          switchMap((data1) =>
            this.customer.getImportNSCustomers().pipe(
              switchMap((data2) => of(this.compareNSImportCustomer(data1.body.data, data2))),
              takeUntil(this.dispose$),
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importCustomers: payload, loadingCustomers: false }
    },
  })
  getQBCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser.userId).pipe(
          switchMap((data1) =>
            this.customer.getImportCustomers().pipe(
              switchMap((data2) => of(this.compareImportCustomer(data1.body.data, data2))),
              takeUntil(this.dispose$),
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importNSVendors: payload, loadingVendors: false }
    },
  })
  getNSVendors(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getAllVendors().pipe(
          switchMap((data1: any) =>
            this.vendor.getImportNSVendors().pipe(
              switchMap((data2) => of(this.compareNSImportCustomer(data1.body.data, data2))),
              takeUntil(this.dispose$),
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps, { payload }: Action<any>) => {
      return { ...state, importVendors: payload, loadingVendors: false }
    },
    error_message: (state: AccountStateProps, action: Action<AccountUser[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
      }
    },
  })
  getQBVendors(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getAllVendors().pipe(
          switchMap((data1: any) =>
            this.vendor.getImportVendors().pipe(
              switchMap((data2) => of(this.compareImportCustomer(data1.body.data, data2))),
              takeUntil(this.dispose$),
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return {
        ...state,
        showModalImportItem: false,
        showModalImportNSItem: false,
        loadingItems: false,
        message: 'Items Imported Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: AccountStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingItems: false,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  createItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((items: any) => from(items)),
      mergeMap((item: any) => {
        var newItem = {
          variety: item.variety,
          qboId: item.qboId,
          nsId: item.nsId,
          nsCategoryId: item.nsCategoryId,
          baseUOM: item.baseUOM,
          inventoryUOM: item.inventoryUOM,
          sku: item.SKU,
          sell: true,
          purchase: true,
          active: item.status,
        }
        return this.inventory.saveItem(newItem).pipe(delay(3000))
      }, 4),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return {
        ...state,
        showModalImportCustomer: false,
        showModalImportNSCustomer: false,
        loadingCustomers: false,
        message: 'Customers Imported Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: AccountStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingCustomers: false,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  createCustomers(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customers: any) => from(customers)),
      mergeMap((customer: any) => {
        var newCustomer = { company: customer.company, mainContact: customer, qboId: customer.qboId, nsId: customer.nsId }
        return this.customer.createCustomer(state$.value.currentUser.userId, newCustomer).pipe(delay(3000))
      }, 4),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: AccountStateProps) => {
      return {
        ...state,
        showModalImportVendor: false,
        showModalImportNSVendor: false,
        loadingVendors: false,
        message: 'Vendors Imported Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: AccountStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingVendors: false,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  createVendors(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((vendors: any) => from(vendors)),
      mergeMap((vendor: any) => {
        var newVendor = { company: vendor.company, mainContact: vendor, qboId: vendor.qboId, nsId: vendor.nsId }
        return this.vendor.createVendor(newVendor).pipe(delay(3000))
      }, 4),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  formatNSInventory(data1: SaleItem[], data2: any) {
    const itemList = {}
    const importItemList = []
    for (const item of data1) {
      if (item.nsId != null)
        itemList[item.nsId] = item
    }
    for (const item of data2) {
      const section: SaleSection = {
        wholesaleSectionId: '',
        name: '',
        warehouse: '',
        qboId: '',
        isDefault: false,
      }
      const category: SaleCategory = {
        wholesaleCategoryId: '',
        name: '',
        warehouse: '',
        wholesaleSection: section,
        isDefault: false,
      }
      const tempItem: SaleItem = {
        wholesaleItemId: '',
        itemId: '',
        weight: 0,
        cost: 0,
        quantity: 0,
        provider: '',
        variety: '',
        size: '',
        origin: '',
        packing: '',
        grade: '',
        label: '',
        warehouse: '',
        SKU: '',
        wholesaleCategory: category,
        nsId: item.id,
        nsCategoryId: 0,
        stock: 0,
        stockEnabled: false,
        organic: false,
        isListed: item.active,
        createdDate: '',
        updatedDate: '',
        baseUOM: '',
        inventoryUOM: '',
        status: true,
      }
      tempItem.variety = item.fullname
      if (item.upccode != null) {
        tempItem.SKU = item.upccode
      }
      if (item.isinactive != null) {
        if (item.isinactive == 'F')
          tempItem.status = true
        else if (item.isinactive == 'T')
          tempItem.status = false
      }
      if (item.baseUOM)
        tempItem.baseUOM = item.baseUOM
      if (item.inventoryUOM)
        tempItem.inventoryUOM = item.inventoryUOM
      if (item.class != null)
        tempItem.nsCategoryId = item.class
      if (itemList[item.id] == null) {
        importItemList.push(tempItem)
      }
    }
    return importItemList.map((data) => ({
      itemId: '',
      weight: '',
      cost: 0,
      quantity: 0,
      provider: '',
      variety: data.variety,
      size: '',
      origin: '',
      packing: '',
      grade: '',
      label: '',
      warehouse: '',
      SKU: data.SKU,
      nsId: data.nsId,
      nsCategoryId: data.nsCategoryId,
      baseUOM: data.baseUOM,
      inventoryUOM: data.inventoryUOM,
      status: data.status,
      stock: 0,
      stockEnabled: false,
      isOrganic: false,
      isListed: data.isListed,
    }))
  }

  formatInventory(data1: SaleItem[], data2: any) {
    const itemList = {}
    const importItemList = []
    for (const item of data1) {
      if (item.qboId != null) itemList[item.qboId] = item
    }
    for (const item of data2.itemList) {
      const section: SaleSection = {
        wholesaleSectionId: '',
        name: '',
        warehouse: '',
        qboId: '',
      }
      const category: SaleCategory = {
        wholesaleCategoryId: '',
        name: '',
        warehouse: '',
        wholesaleSection: section,
      }
      const tempItem: SaleItem = {
        wholesaleItemId: '',
        itemId: '',
        weight: 0,
        cost: 0,
        quantity: 0,
        provider: '',
        variety: '',
        size: '',
        origin: '',
        packing: '',
        grade: '',
        label: '',
        warehouse: '',
        SKU: item.Sku,
        wholesaleCategory: category,
        qboId: item.Id,
        stock: 0,
        stockEnabled: false,
        organic: false,
        isListed: item.Active,
        createdDate: '',
        updatedDate: '',
        status: true,
      }
      if (item.Name != null && item.Name.length >= 0) {
        tempItem.variety = item.Name
      } else {
        tempItem.variety = item.FullyQualifiedName
      }
      // if (item.purchaseCost != null) { item.unitPrice
      //   tempItem.cost = item.purchaseCost
      // }
      if (item.Sku != null) {
        tempItem.SKU = item.Sku
      }

      if (itemList[item.Id] == null) {
        importItemList.push(tempItem)
      }
    }
    return importItemList.map((data) => ({
      itemId: '',
      weight: '',
      cost: 0,
      quantity: 0,
      provider: '',
      variety: data.variety,
      size: '',
      origin: '',
      packing: '',
      grade: '',
      label: '',
      warehouse: '',
      SKU: data.SKU,
      qboId: data.qboId,
      status: true,
      stock: 0,
      stockEnabled: false,
      isOrganic: false,
      isListed: data.isListed,
    }))
  }

  compareNSImportCustomer(data1: NSAccount[], data2: any): NSAccount[] {
    console.log('hello NS')
    const customerList = {}
    const customerList2 = {}
    const importCustomerList = []
    for (const customer of data1) {
      if (customer.nsId != null) customerList[customer.nsId] = customer
    }
    console.log(customerList)
    let dataList = data2
    for (const customer of dataList) {
      const status = customer.status ? CustomerStatus.ACTIVE : CustomerStatus.INACTIVE

      if (customerList2[customer.entity] === 1) {
        continue
      }
      customerList2[customer.entity] = 1
      const tempCustomer: NSAccount = {
        type: CustomerType.INDIVIDUAL,
        company: customer.companyname,
        name: customer.companyname,
        phone: '',
        email: '',
        address: {} as Address,
        shippingAddress: {} as Address,
        billingAddress: {} as Address,
        status: CustomerStatus.ACTIVE,
        nsId: customer.entity,
        mobilePhone: '',
        fax: '',
      }
      if (customer.isinactive != null) {
        if (customer.isinactive == 'F')
          tempCustomer.status = CustomerStatus.ACTIVE
        else if (customer.isinactive == 'T')
          tempCustomer.status = CustomerStatus.INACTIVE
      }
      if (customer.email != null)
        tempCustomer.email = customer.email
      if (customer.phone != null)
        tempCustomer.phone = customer.phone
      if (customer.mobile != null)
        tempCustomer.mobilePhone = customer.mobilePhone
      if (customer.fax != null) tempCustomer.fax = customer.fax
      if (customer.defaultbillingaddress != null) {
        tempCustomer.billingAddress.street1 = customer.addr1
        if (customer.addr2 != null)
          tempCustomer.billingAddress.street1 += ' ' + customer.addr2
        if (customer.addr3 != null)
          tempCustomer.billingAddress.street1 += ' ' + customer.addr3
        tempCustomer.billingAddress.city = customer.city ? customer.city : ''
        tempCustomer.billingAddress.state = customer.state ? customer.state : ''
        tempCustomer.billingAddress.zipcode = customer.zip ? customer.zip : '0'
        tempCustomer.billingAddress.country = customer.country ? customer.country : ''
        tempCustomer.billingAddress.nsId = customer.bill_id ? customer.bill_id : ''
      }
      if (customer.defaultshippingaddress != null) {
        tempCustomer.shippingAddress.street1 = customer.addr1_0
        if (customer.addr2_0 != null)
          tempCustomer.shippingAddress.street1 += ' ' + customer.addr2_0
        if (customer.addr3_0 != null)
          tempCustomer.shippingAddress.street1 += ' ' + customer.addr3_0
        tempCustomer.shippingAddress.city = customer.city_0 ? customer.city_0 : ''
        tempCustomer.shippingAddress.state = customer.state_0 ? customer.state_0 : ''
        tempCustomer.shippingAddress.zipcode = customer.zip_0 ? customer.zip_0 : '0'
        tempCustomer.shippingAddress.country = customer.country_0 ? customer.country_0 : ''
        tempCustomer.shippingAddress.nsId = customer.ship_id ? customer.ship_id : ''
      }
      if (customer.firstname != null && customer.lastname)
        tempCustomer.name = customer.firstname + " " + customer.lastname

      console.log(customerList)
      if (customerList[customer.entity] == null) {
        importCustomerList.push(tempCustomer)
      }
    }

    return importCustomerList.map((data) => ({
      type: data.type,
      company: data.company,
      name: data.name,
      phone: data.phone,
      email: data.email,
      address: data.address,
      shippingAddress: data.shippingAddress,
      billingAddress: data.billingAddress,
      status: data.status,
      nsId: data.nsId,
      mobilePhone: data.mobilePhone,
      fax: data.fax,
    }))
  }

  compareImportCustomer(data1: QBOAccount[], data2: any): QBOAccount[] {
    console.log('hello')
    const customerList = {}
    const importCustomerList = []
    for (const customer of data1) {
      if (customer.qboId != null) customerList[customer.qboId] = customer
    }
    let dataList = []
    if (data2.customerList != null) dataList = data2.customerList
    if (data2.vendorList != null) dataList = data2.vendorList
    for (const customer of dataList) {
      const status = customer.status ? CustomerStatus.ACTIVE : CustomerStatus.INACTIVE
      const tempCustomer: QBOAccount = {
        type: CustomerType.INDIVIDUAL,
        company: customer.CompanyName,
        name: '',
        phone: '',
        email: '',
        address: {} as Address,
        shippingAddress: {} as Address,
        billingAddress: {} as Address,
        status: CustomerStatus.ACTIVE,
        qboId: customer.Id,
        mobilePhone: '',
        fax: '',
      }
      if (customer.CompanyName == null || customer.PrintOnCheckName.length === 0)
        tempCustomer.company = customer.PrintOnCheckName
      if (customer.PrimaryEmailAddr != null && customer.PrimaryEmailAddr.Address != null)
        tempCustomer.email = customer.PrimaryEmailAddr.Address
      if (customer.PrimaryPhone != null && customer.PrimaryPhone.FreeFormNumber != null)
        tempCustomer.phone = customer.PrimaryPhone.FreeFormNumber
      if (customer.Mobile != null && customer.Mobile.FreeFormNumber != null)
        tempCustomer.mobilePhone = customer.Mobile.FreeFormNumber
      if (customer.Fax != null && customer.Fax.FreeFormNumber != null) tempCustomer.fax = customer.Fax.FreeFormNumber
      if (customer.BillAddr != null) {
        tempCustomer.billingAddress.street1 = customer.BillAddr.Line1
        if (customer.BillAddr.Line2 != null && customer.BillAddr.Line2.length > 0)
          tempCustomer.billingAddress.street1 += ' ' + customer.BillAddr.Line2
        if (customer.BillAddr.Line3 != null && customer.BillAddr.Line3.length > 0)
          tempCustomer.billingAddress.street1 += ' ' + customer.BillAddr.Line3
        if (customer.BillAddr.Line4 != null && customer.BillAddr.Line4.length > 0)
          tempCustomer.billingAddress.street1 += ' ' + customer.BillAddr.Line4
        if (customer.BillAddr.Line5 != null && customer.BillAddr.Line5.length > 0)
          tempCustomer.billingAddress.street1 += ' ' + customer.BillAddr.Line5
        tempCustomer.billingAddress.city = customer.BillAddr.City
        tempCustomer.billingAddress.state = customer.BillAddr.CountrySubDivisionCode
        tempCustomer.billingAddress.zipcode = customer.BillAddr.PostalCode
        tempCustomer.billingAddress.country = customer.BillAddr.Country
      }
      if (customer.ShipAddr != null) {
        tempCustomer.shippingAddress.street1 = customer.ShipAddr.Line1
        if (customer.ShipAddr.Line2 != null && customer.ShipAddr.Line2.length > 0)
          tempCustomer.shippingAddress.street1 += ' ' + customer.ShipAddr.Line2
        if (customer.ShipAddr.Line3 != null && customer.ShipAddr.Line3.length > 0)
          tempCustomer.shippingAddress.street1 += ' ' + customer.ShipAddr.Line3
        if (customer.ShipAddr.Line4 != null && customer.ShipAddr.Line4.length > 0)
          tempCustomer.shippingAddress.street1 += ' ' + customer.ShipAddr.Line4
        if (customer.ShipAddr.Line5 != null && customer.ShipAddr.Line5.length > 0)
          tempCustomer.shippingAddress.street1 += ' ' + customer.ShipAddr.Line5
        tempCustomer.shippingAddress.city = customer.ShipAddr.City
        tempCustomer.shippingAddress.state = customer.ShipAddr.CountrySubDivisionCode
        tempCustomer.shippingAddress.zipcode = customer.ShipAddr.PostalCode
        tempCustomer.shippingAddress.country = customer.ShipAddr.Country
      }

      if (customer.Name == null || customer.DisplayName.length === 0) {
        tempCustomer.name = customer.DisplayName
      }
      console.log(customerList)
      if (customerList[customer.Id] == null) {
        importCustomerList.push(tempCustomer)
      }
    }

    return importCustomerList.map((data) => ({
      type: data.type,
      company: data.company,
      name: data.name,
      phone: data.phone,
      email: data.email,
      address: data.address,
      shippingAddress: data.shippingAddress,
      billingAddress: data.billingAddress,
      status: data.status,
      qboId: data.qboId,
      mobilePhone: data.mobilePhone,
      fax: data.fax,
    }))
  }

  checkLogin(bodyData: any) {
    let data = { ...bodyData.data }
    let accessToken = localStorage.getItem(CACHED_ACCESSTOKEN)
    if (!data.userId || !accessToken) {
      localStorage.clear()
      notification.error({
        message: data.message || 'Server error, please try again later',
      })
      throw new Error('Login failed')
    }
    // localStorage.setItem(CACHED_AUTH_TOKEN, data.tokenId)
    localStorage.setItem(CACHED_USER_ID, `${data.userId}`)
    localStorage.setItem(CACHED_ACCOUNT_TYPE, data.accountType)
    localStorage.setItem(CACHED_COMPANY, data.company)
    localStorage.setItem(CACHED_FULLNAME, data.fullName)
    localStorage.setItem(CACHED_IS_AGREED, data.isAgreed)
    localStorage.setItem(CACHED_QBO_LINKED, data.qboRealmId)
    localStorage.setItem(CACHED_NS_LINKED, data.nsRealmId)
    localStorage.setItem('GRUBMARKET_API_VERSION', bodyData.apiVersion)
    return data
  }

  /**
     * @deprecated
    formatAccount2(data: AuthUser): AccountUser {
      return {
        id: data.userId,
        emailAddress: data.emailAddress,
        firstName: data.firstName,
        lastName: data.lastName,
        phone: data.phone,
        status: data.status,
        addressId: data.addressId,
        company: data.company,
        password: '',
      }
    }
    */

  /**
     * @deprecated

     */
  formatAccount(data: any): AccountUser {
    return {
      id: data.userId,
      emailAddress: data.emailAddress,
      firstName: data.firstName,
      lastName: data.lastName,
      phone: data.phone,
      status: data.status,
      addressId: data.addressId,
      company: data.company,
      password: '',
      imagePath: data.imagePath,
      role: data.role
    }
  }
}

export type AccountDispatchProps = ModuleDispatchProps<AccountModule>
