import { Http } from '~/common'
import { Injectable } from 'redux-epics-decorator'
import { AccountUser, AuthUser } from '~/schema'
import { b64EncodeUnicode } from '~/common/utils'
import { CACHED_ACCESSTOKEN } from '~/common/const'


@Injectable()
export class AccountService {
  constructor(private readonly http: Http) { }

  getUser(userId: string) {
    if (userId == 'null') {
      userId = '1'
    }
    return this.http.get<AccountUser>(`/account/user/${userId}`)
  }

  nsUpdateItemsDefaultUOM() {
    return this.http.post<any>(`/netsuite/updateItemsDefaultUOM`)
  }

  nsUpdateItemsBasePrice() {
    return this.http.post<any>(`/netsuite/updateItemsBasePrice`)
  }

  nsUpdateItemsFromNS() {
    return this.http.post<any>(`/netsuite/updateItemsFromNS`)
  }

  nsUpdateCustomersFromNS() {
    return this.http.post<any>(`/netsuite/updateCustomersFromNS`)
  }

  nsUpdateItemsToNS() {
    return this.http.post<any>(`/netsuite/updateItemsToNs`)
  }

  nsUpdateClientsPaymentFromNS() {
    return this.http.post<any>(`/netsuite/updateClientsPaymentFromNS`)
  }

  nsUpdateCustomerAddressFromNS() {
    return this.http.post<any>(`/netsuite/updateCustomerAddressFromNS`)
  }

  nsUpdateClientsBusinessTypeFromNS() {
    return this.http.post<any>(`/netsuite/updateClientsBusinessTypeFromNS`)
  }

  nsImportUOM() {
    return this.http.post<any>(`/netsuite/syncNsUOM`)
  }

  nsImportExtraItem() {
    return this.http.post<any>(`/netsuite/syncNsExtraItem`)
  }

  nsImportTerm() {
    return this.http.post<any>(`/netsuite/syncNsTerm`)
  }

  qbImportTerm() {
    return this.http.post<any>(`/quickbook/importTerms`)
  }

  nsImportFreight() {
    return this.http.post<any>(`/netsuite/syncNsFreight`)
  }

  nsImportCategory() {
    return this.http.post<any>(`/netsuite/syncNsCategory`)
  }

  nsImportCustomerType() {
    return this.http.post<any>(`/netsuite/syncNsCustomerBusinessType`)
  }

  nsImportVendorType() {
    return this.http.post<any>(`/netsuite/syncNsVendorBusinessType`)
  }

  nsUpdateItemsUomLastSale() {
    return this.http.post<any>(`/netsuite/updateItemsUomLastSale`)
  }

  nsImportSalesOrder(orderId: string) {
    return this.http.post<any>(`/netsuite/importOrder/invoice/${orderId}`)
  }

  nsImportPurchaseOrder(orderId: string) {
    return this.http.post<any>(`/netsuite/importOrder/vendorbill/${orderId}`)
  }

  updateUser(userId: string, data: any) {
    // return this.http.post(`updateMainAccountUser`, {
    return this.http.post<any>(`/account/user/updateUser/${userId}`, {
      body: JSON.stringify(data),
    })
  }

  updatePassword(data: any) {
    let header = new Headers()
    // @ts-ignore
    header.append('Authorization', `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN) != null ? localStorage.getItem(CACHED_ACCESSTOKEN) : ""}`)
    header.append('Content-Type', 'application/x-www-form-urlencoded')
    return this.http.post<any>(`/api/user/password`, {
      body: `token=${encodeURIComponent(data.token)}`,
      headers: header,
    })
  }

  loginUser(data: any) {
    return this.http.post<any>(`/session/login/`, {
      body: JSON.stringify(data),
    })
  }

  getAccessToken(data: any) {
    let header = new Headers()
    let requestData = {
      username: data.email,
      password: data.password,
      grant_type: 'password',
      scope: 'all',
    }
    const formBody = []
    for (const property in requestData) {
      const encodedKey = encodeURIComponent(property)
      const encodedValue = encodeURIComponent(requestData[property])
      formBody.push(encodedKey + '=' + encodedValue)
    }
    // @ts-ignore
    // header.append('Authorization', "Basic b3d0Om9hdXRoMg==")
    const credentials = `${process.env['CLIENT_ID']}:${process.env['CLIENT_SECRET']}`
    header.append('Authorization', `Basic ${b64EncodeUnicode(credentials)}`)
    header.append('Content-Type', 'application/x-www-form-urlencoded')

    return this.http.post<any>(`/oauth/token`, {
      body: formBody.join('&'),
      headers: header,
    })
  }

  forgotPassword(email: string) {
    const header = new Headers()
    header.append('Content-Type', 'application/x-www-form-urlencoded')

    return this.http.post<any>(`/api/auth/reset-password?account=${email}`, {
      headers: header,
    })
  }

  getUserInfo(data: any) {
    let header = new Headers()
    header.append('Authorization', `Bearer ${data}`)
    return this.http.post<any>(`/session/userInfo`, {
      headers: header,
    })
  }

  resetPassword(data: any) {
    return this.http.post<any>(`/session/resetPassword`, {
      body: JSON.stringify(data),
    })
  }
}
