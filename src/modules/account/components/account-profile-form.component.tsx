/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'

import { Form, Input, Button, notification, Upload, Icon, Avatar } from 'antd'
import { FormComponentProps, FormCreateOption } from 'antd/lib/form'
const FormItem = Form.Item

import { inputTitleStyles } from './login-form.style'
import { buttonStyles } from '~/modules/orders/order-new.style'

import { Container, ButtonWrapper, NonFlexContainer } from './account-profile-form.style'

import { validateEmail,getFileUrl,isSeller } from '~/common/utils'
import { AccountUser } from '~/schema'
import { CACHED_ACCESSTOKEN } from '~/common'
import { ThemeOutlineButton } from '~/modules/customers/customers.style'

interface FormProps extends FormComponentProps {
  user?: AccountUser | null
  getUser: Function
  saveForm: (value: any) => void
}

interface FormFields {
  email: string
  firstName: string
  lastName: string
  password: string
  password2: string
}

class ProfileForm extends React.Component<FormProps & FormComponentProps<FormFields>> {
  state = {
    editValues: {},
    confirmDirty: false,
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.saveForm(values)
      }
    })
  }

  handleEmail = (_rule: any, value: string, callback: any) => {
    if (value && !validateEmail(value)) {
      callback('Invalid Email')
    }
    callback()
  }

  onChangeEditInput = (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value

    if (channel.indexOf('email') > -1) {
      // this.setState({
      //   emailRule: validateEmail(value) ? "success" : "error",
      //   emailError: validateEmail(value) ? " " : "Invalid Email"
      // })
    } else if (channel.indexOf('password') > -1) {
    } else if (channel.indexOf('password2') > -1) {
    }

    this.setState({
      editValues: {
        ...this.state.editValues,
        [channel]: value,
      },
    })
  }

  validateToNextPassword = (_rule: any, value: any, callback: () => void) => {
    const form = this.props.form
    if (value && this.state.confirmDirty) {
      form.validateFields(['password2'], { force: true })
    }
    callback()
  }

  compareToFirstPassword = (_rule: any, value: any, callback: { (arg0: string): void; (): void }) => {
    const form = this.props.form
    if (value && value !== form.getFieldValue('password')) {
      callback('Passwords does not match!')
    } else {
      callback()
    }
  }

  handlePwdBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const value = e.target.value
    // 根据 password1 的值来决定 password2 的校验规则
    // 如果 password1 存在 则 password2 为 required
    this.setState(
      {
        confirmDirty: Boolean(value),
      },
      () => {
        this.props.form.validateFields(['password2'], { force: true })
      },
    )
  }

  handleConfirmBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const value = e.target.value
    this.setState({
      confirmDirty: this.state.confirmDirty || Boolean(value),
    })
  }

  setUploadProps = () =>{
    return {
      name: 'file',
      action:process.env.WSW2_HOST + "/api/v1/wholesale/file/account/avatar",
      showUploadList: false,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN)}`,
      },
      onChange(info: any) {
        if (info.file.status === 'uploading') {

        }
        if (info.file.status === 'done') {
          notification['success']({
            message: 'Success',
            description: `Avatar uploaded successfully`,
            duration: 5
          })
        } else if (info.file.status === 'error') {
          notification['error']({
            message: 'Error',
            description: `Avatar file upload failed`,
          })
        }
      },
    }
  }

  render() {
    const { user, getUser } = this.props
    const { getFieldDecorator } = this.props.form
    let isSellerRole = user ? isSeller(user.role) : true
    return (
      <NonFlexContainer>
        <div >
          <div>
            {user && user.imagePath ?
            <Avatar
              size={128}
              src={getFileUrl(user.imagePath,!isSellerRole)}
              style={{marginBottom: 20}}
            /> :
            <Avatar
              size={128}
              icon="user"
              style={{marginBottom: 20}}
            />
            }
          </div>
          <Upload {this.setUploadProps()}>
            <ThemeOutlineButton shape="round" className='sales-document-action-btn document-upload-btn' onKeyDown={(e:any)=>{
              if(e.keyCode == 13) {
                e.stopPropagation()
              }}}>
              <Icon type="upload" />
              Upload
            </ThemeOutlineButton>
          </Upload>
        </div>
        <div>
          <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <div style={{ display: 'flex', flexDirection: 'row' }}>
                <FormItem css={inputTitleStyles} style={{ paddingTop: '57px', marginBottom: '36px' }} label="First Name">
                  {getFieldDecorator('firstName', {
                    rules: [{ required: true, message: 'First Name is required!' }],
                  })(<Input placeholder="FIRST NAME" style={{ width: '242px' }} />)}
                </FormItem>
                <FormItem css={inputTitleStyles} style={{ paddingTop: '57px', paddingLeft: '20px' }} label="Last Name">
                  {getFieldDecorator('lastName', {
                    rules: [{ required: true, message: 'Last Name is required!' }],
                  })(<Input placeholder="LAST NAME" style={{ width: '242px' }} />)}
                </FormItem>
              </div>
              <FormItem css={inputTitleStyles} style={{ marginBottom: '34px' }} label="Email">
                <Input placeholder="EMAIL" style={{ width: '503px' }} value={user ? user.emailAddress : ''} readOnly/>
              </FormItem>
              <FormItem css={inputTitleStyles} style={{ marginBottom: '34px' }} label="Password">
                {getFieldDecorator('password', {
                  rules: [{ validator: this.validateToNextPassword }],
                })(
                  <Input
                    type="password"
                    placeholder="**********"
                    onBlur={this.handlePwdBlur}
                    style={{ width: '503px' }}
                  />,
                )}
              </FormItem>
              <FormItem css={inputTitleStyles} style={{ marginBottom: '34px' }} label="Confirm Password">
                {getFieldDecorator('password2', {
                  rules: [
                    { required: this.state.confirmDirty, message: 'Password confirm is required!' },
                    { validator: this.compareToFirstPassword },
                  ],
                })(
                  <Input
                    type="password"
                    placeholder="**********"
                    onBlur={this.handleConfirmBlur}
                    style={{ width: '503px' }}
                  />,
                )}
              </FormItem>
              <FormItem>
                <ButtonWrapper>
                  <Button type="primary" htmlType="submit" size="large" css={buttonStyles}>
                    Update Account
                  </Button>
                </ButtonWrapper>
              </FormItem>
            </div>
          </Form>
        </div>
      </NonFlexContainer>
    )
  }
}

const mapPropsToFields = (props: FormProps) => {
  const { user } = props
  if (!user) {
    return
  }

  return {
    firstName: Form.createFormField({
      value: user.firstName,
    }),
    lastName: Form.createFormField({
      value: user.lastName,
    }),
    email: Form.createFormField({
      value: user.emailAddress,
    }),
  }
}

const FormOptions: FormCreateOption<FormProps> = { mapPropsToFields }

export const AccountProfileForm = Form.create(FormOptions)(ProfileForm)
