import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { mediumGrey, grey, brightGreen } from '~/common'

export const Container = styled('div')({
  display: 'flex',

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    fontWeight: 700,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const NonFlexContainer = styled('div')({
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    fontWeight: 700,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const ButtonWrapper = styled('div')({
  display: 'flex',
  paddingTop: '51px',
})

export const inputTitleStyles = css({
  color: mediumGrey,
  fontWeight: 700,
  fontSize: '14px',
  lineHeight: '24px',
  textTransform: 'uppercase',
  paddingBottom: '0px !important',

  '& .ant-form-item-required': {
    color: grey,
    fontSize: '14px',
    lineHeight: '33px',
  },
})

export const buttonStyle = css({
  borderRadius: '200px',
  width: '130px',
  height: '40px',
  border: `1px solid ${brightGreen}`,
  backgroundColor: brightGreen,
  marginTop: '19px',
  marginRight: '25px',
  fontWeight: 700,
})
