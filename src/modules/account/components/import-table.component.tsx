/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { ColumnProps } from 'antd/es/table'

import { QBOAccount, NSAccount, NSOrderImport } from '~/schema'
import { SaleItem } from '~/schema'
import {
  tableCss,
  EmptyTr,
  HoverInputCss,
  DialogStyle,
  DialogHeader,
  DialogFooter,
  importButtonStyle,
} from './import-table.style'
import { Input, Modal, Button } from 'antd'
import { ThemeButton, ThemeTable } from '~/modules/customers/customers.style'
import { searchButton } from '~/components/elements/search-button'

export interface CustomerTableProps {
  customers: QBOAccount[]
  loading: boolean
  onSelectionChange: (changes: any) => void
}

export interface VendorTableProps {
  vendors: QBOAccount[]
  loading: boolean
  onSelectionChange: (changes: any) => void
}

export interface ItemTableProps {
  saleItems: SaleItem[]
  loading: boolean
  onSelectionChange: (changes: any) => void
}


export interface CustomerNSTableProps {
  customers: NSAccount[]
  loading: boolean
  onSelectionChange: (changes: any) => void
}

export interface VendorNSTableProps {
  vendors: NSAccount[]
  loading: boolean
  onSelectionChange: (changes: any) => void
}

export interface OrderNSTableProps {
  orders: NSOrderImport[]
  loading: boolean
  onSelectionChange: (changes: any) => void
}

const itemColumns: ColumnProps<SaleItem>[] = [
  {
    title: 'QBO ID',
    dataIndex: 'qboId',
    key: 'qboId',
    width: 142,
    sorter: (a, b) => a.qboId.localeCompare(b.qboId),
  },
  {
    title: 'SKU',
    dataIndex: 'SKU',
    key: 'SKU',
    width: 142,
    sorter: (a, b) => a.SKU.localeCompare(b.SKU),
  },
  {
    title: 'Name',
    dataIndex: 'variety',
    key: 'variety',
    width: 467,
    sorter: (a, b) => a.variety.localeCompare(b.variety),
  },
]


const itemNSColumns: ColumnProps<SaleItem>[] = [
  {
    title: 'NS ID',
    dataIndex: 'nsId',
    key: 'nsId',
    width: 142,
    sorter: (a, b) => a.nsId.localeCompare(b.nsId),
  },
  {
    title: 'SKU',
    dataIndex: 'SKU',
    key: 'SKU',
    width: 142,
    sorter: (a, b) => a.SKU.localeCompare(b.SKU),
  },
  {
    title: 'Name',
    dataIndex: 'variety',
    key: 'variety',
    width: 467,
    sorter: (a, b) => a.variety.localeCompare(b.variety),
  },
  {
    title: 'Pricing UOM',
    dataIndex: 'baseUOM',
    key: 'baseUOM',
    width: 467,
    sorter: (a, b) => a.baseUOM.localeCompare(b.baseUOM),
  },
  {
    title: 'Inventory UOM',
    dataIndex: 'inventoryUOM',
    key: 'inventoryUOM',
    width: 467,
    sorter: (a, b) => a.inventoryUOM.localeCompare(b.inventoryUOM),
  },
]

export class CustomerTable extends React.PureComponent<CustomerTableProps> {
  private rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      this.props.onSelectionChange(selectedRows)
    },
  }
  render() {
    const customers = this.props.customers
    const loading = this.props.loading
    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }
    const style: DialogStyle = {
      checkbox: true,
    }


    const customerColumns: ColumnProps<QBOAccount>[] = [
      {
        title: 'CUSTOMER',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name),
        width: 312,
      },
      {
        title: 'COMPANY',
        dataIndex: 'company',
        key: 'company',
        sorter: (a, b) => a.company.localeCompare(b.company),
        width: 312,
      },
      {
        title: 'PHONE NUMBER',
        dataIndex: 'phone',
        key: 'phone',
        width: 200,
      },
      {
        title: 'EMAIL',
        dataIndex: 'email',
        key: 'email',
        width: 280,
        render: (text: any,record:any,index:number) => (
          <div css={HoverInputCss}>
            <Input defaultValue={text} onBlur = {
              (event:any) => {
                this.props.customers[index].email = event.target.value
              }
            }/>
          </div>
        ),
      },
    ]
    return (
      <ThemeTable
        loading={loading}
        rowSelection={this.rowSelection}
        pagination={false}
        components={components}
        scroll={{ y: 625, x: true }}
        columns={customerColumns}
        dataSource={customers}
        rowKey="qboId"
        css={tableCss(style)}
      />
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export class CustomerNSTable extends React.PureComponent<CustomerNSTableProps> {
  private rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      this.props.onSelectionChange(selectedRows)
    },
  }
  render() {
    const customers = this.props.customers
    const loading = this.props.loading
    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }
    const style: DialogStyle = {
      checkbox: true,
    }


    const customerColumns: ColumnProps<NSAccount>[] = [
      {
        title: 'CUSTOMER',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name),
        width: 312,
      },
      {
        title: 'COMPANY',
        dataIndex: 'company',
        key: 'company',
        sorter: (a, b) => a.company.localeCompare(b.company),
        width: 312,
      },
      {
        title: 'PHONE NUMBER',
        dataIndex: 'phone',
        key: 'phone',
        width: 200,
      },
      {
        title: 'EMAIL',
        dataIndex: 'email',
        key: 'email',
        width: 280,
        render: (text: any,record:any,index:number) => (
          <div css={HoverInputCss}>
            <Input defaultValue={text} onBlur = {
              (event:any) => {
                this.props.customers[index].email = event.target.value
              }
            }/>
          </div>
        ),
      },
    ]
    return (
      <ThemeTable
        loading={loading}
        rowSelection={this.rowSelection}
        pagination={false}
        components={components}
        scroll={{ y: 625, x: true }}
        columns={customerColumns}
        dataSource={customers}
        rowKey="nsId"
        css={tableCss(style)}
      />
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export class VendorTable extends React.PureComponent<VendorTableProps> {
  private rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      this.props.onSelectionChange(selectedRows)
    },
  }
  render() {
    const vendors = this.props.vendors
    const loading = this.props.loading
    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }
    const style: DialogStyle = {
      checkbox: true,
    }


  const vendorColumns: ColumnProps<QBOAccount>[] = [
    {
      title: 'VENDOR',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => a.name.localeCompare(b.name),
      width: 312,
    },
    {
      title: 'COMPANY',
      dataIndex: 'company',
      key: 'company',
      sorter: (a, b) => a.company.localeCompare(b.company),
      width: 312,
    },
    {
      title: 'PHONE NUMBER',
      dataIndex: 'phone',
      key: 'phone',
      width: 200,
    },
    {
      title: 'EMAIL',
      dataIndex: 'email',
      key: 'email',
      width: 280,
      render: (text: any,record:any,index:number) => (
        <Input defaultValue={text} onBlur = {
          (event:any) => {
            this.props.vendors[index].email = event.target.value
          }
        }/>
      ),
    },
  ]
    return (
      <ThemeTable
        loading={loading}
        rowSelection={this.rowSelection}
        pagination={false}
        components={components}
        scroll={{ y: 625, x: true }}
        columns={vendorColumns}
        dataSource={vendors}
        rowKey="qboId"
        css={tableCss(style)}
      />
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export class VendorNSTable extends React.PureComponent<VendorNSTableProps> {
  private rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      this.props.onSelectionChange(selectedRows)
    },
  }
  render() {
    const vendors = this.props.vendors
    const loading = this.props.loading
    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }
    const style: DialogStyle = {
      checkbox: true,
    }


  const vendorColumns: ColumnProps<NSAccount>[] = [
    {
      title: 'VENDOR',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => a.name.localeCompare(b.name),
      width: 312,
    },
    {
      title: 'COMPANY',
      dataIndex: 'company',
      key: 'company',
      sorter: (a, b) => a.company.localeCompare(b.company),
      width: 312,
    },
    {
      title: 'PHONE NUMBER',
      dataIndex: 'phone',
      key: 'phone',
      width: 200,
    },
    {
      title: 'EMAIL',
      dataIndex: 'email',
      key: 'email',
      width: 280,
      render: (text: any,record:any,index:number) => (
        <Input defaultValue={text} onBlur = {
          (event:any) => {
            this.props.vendors[index].email = event.target.value
          }
        }/>
      ),
    },
  ]
    return (
      <ThemeTable
        loading={loading}
        rowSelection={this.rowSelection}
        pagination={false}
        components={components}
        scroll={{ y: 625, x: true }}
        columns={vendorColumns}
        dataSource={vendors}
        rowKey="nsId"
        css={tableCss(style)}
      />
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export class OrderNSTable extends React.PureComponent<OrderNSTableProps> {
  private rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      this.props.onSelectionChange(selectedRows)
    },
  }
  render() {
    const orders = this.props.orders
    const loading = this.props.loading
    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }
    const style: DialogStyle = {
      checkbox: true,
    }


  const vendorColumns: ColumnProps<NSOrderImport>[] = [
    {
      title: 'NS ORDER ID',
      dataIndex: 'orderId',
      key: 'orderId',
      width: 312,
      sorter: (a, b) => {
        return a.orderId.localeCompare(b.orderId)
      },
    },
    {
      title: 'COMPANY',
      dataIndex: 'client',
      key: 'client',
      width: 312,
      sorter: (a, b) => {
        return a.client.localeCompare(b.client)
      },
    }
  ]
    return (
      <ThemeTable
        loading={loading}
        rowSelection={this.rowSelection}
        pagination={false}
        components={components}
        scroll={{ y: 625, x: true }}
        columns={vendorColumns}
        dataSource={orders}
        rowKey="nsId"
        css={tableCss(style)}
      />
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export interface ImportCustomerModalProps {
  visible: boolean
  customers: QBOAccount[]
  loading: boolean
  onImport: () => void
  onSelectionChange: (customers: QBOAccount[]) => void
  onCancel: () => void
}

export class ImportCustomerModal extends React.PureComponent<ImportCustomerModalProps> {
  render() {
    const { visible, customers, loading, onImport, onSelectionChange, onCancel } = this.props
    return (
      <Modal width={1166} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>Import Customers</DialogHeader>
        <CustomerTable customers={customers} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}

export interface ImportNSCustomerModalProps {
  visible: boolean
  customers: NSAccount[]
  loading: boolean
  onImport: () => void
  onSelectionChange: (customers: NSAccount[]) => void
  onCancel: () => void
}

export class ImportNSCustomerModal extends React.PureComponent<ImportNSCustomerModalProps> {
  render() {
    const { visible, customers, loading, onImport, onSelectionChange, onCancel } = this.props
    return (
      <Modal width={1166} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>Import Customers</DialogHeader>
        <CustomerNSTable customers={customers} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}

export interface ImportVendorModalProps {
  visible: boolean
  vendors: QBOAccount[]
  loading: boolean
  onImport: () => void
  onSelectionChange: (customers: QBOAccount[]) => void
  onCancel: () => void
}

export class ImportVendorModal extends React.PureComponent<ImportVendorModalProps> {
  render() {
    const { visible, vendors, loading, onImport, onSelectionChange, onCancel } = this.props
    return (
      <Modal width={1166} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>Import Vendors</DialogHeader>
        <VendorTable vendors={vendors} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}

export interface ImportNSVendorModalProps {
  visible: boolean
  vendors: NSAccount[]
  loading: boolean
  onImport: () => void
  onSelectionChange: (customers: NSAccount[]) => void
  onCancel: () => void
}

export class ImportNSVendorModal extends React.PureComponent<ImportNSVendorModalProps> {
  render() {
    const { visible, vendors, loading, onImport, onSelectionChange, onCancel } = this.props
    return (
      <Modal width={1166} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>Import Vendors</DialogHeader>
        <VendorNSTable vendors={vendors} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}

export interface ImportNSOrderModalProps {
  visible: boolean
  orders: NSOrderImport[]
  loading: boolean
  onImport: () => void
  onSelectionChange: (orders: NSOrderImport[]) => void
  onCancel: () => void
}

export class ImportNSOrderSalesModal extends React.PureComponent<ImportNSOrderModalProps> {
  render() {
    const { visible, orders, loading, onImport, onSelectionChange, onCancel } = this.props
    return (
      <Modal width={1166} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>Import NS Sales Order</DialogHeader>
        <OrderNSTable orders={orders} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}

export class ImportNSOrderPurchaseModal extends React.PureComponent<ImportNSOrderModalProps> {
  render() {
    const { visible, orders, loading, onImport, onSelectionChange, onCancel } = this.props
    return (
      <Modal width={1166} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>Import NS Purchase Order</DialogHeader>
        <OrderNSTable orders={orders} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}

export class ItemNSTable extends React.PureComponent<ItemTableProps> {
  private rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      this.props.onSelectionChange(selectedRows)
    },
  }

  render() {
    const {saleItems} = this.props
    const loading = this.props.loading
    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }
    const style: DialogStyle = {
      checkbox: true,
    }
    return (
      <ThemeTable
        loading={loading}
        rowSelection={this.rowSelection}
        pagination={false}
        components={components}
        scroll={{ y: 400 }}
        columns={itemNSColumns}
        rowKey="qboId"
        dataSource={saleItems}
        css={tableCss(style)}
      />
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export class ItemTable extends React.PureComponent<ItemTableProps> {
  private rowSelection = {
    onChange: (selectedRowKeys: any, selectedRows: any) => {
      this.props.onSelectionChange(selectedRows)
    },
  }

  render() {
    const {saleItems, loading} = this.props
    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }
    const style: DialogStyle = {
      checkbox: true,
    }

    return (
      <ThemeTable
        loading={loading}
        rowSelection={this.rowSelection}
        pagination={false}
        components={components}
        scroll={{ y: 400 }}
        columns={itemColumns}
        rowKey="qboId"
        dataSource={saleItems}
        css={tableCss(style)}
      />
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export interface ImportItemModalProps {
  visible: boolean
  items: SaleItem[]
  loading: boolean
  onImport: () => void
  onSelectionChange: (items: SaleItem[]) => void
  onSearch: (value: string) => void
  onCancel: () => void
}

export class ImportNSItemModal extends React.PureComponent<ImportItemModalProps> {
  render() {
    const { visible, items, loading, onImport, onSelectionChange, onSearch, onCancel } = this.props

    return (
      <Modal width={794} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>
          Import Items
          <Input.Search
            placeholder="Search item name or SKU"
            size="large"
            style={{ width: '406px', border: '0', marginLeft: '33px' }}
            enterButton={searchButton}
            onSearch={onSearch}
          />
        </DialogHeader>
        <ItemNSTable saleItems={items} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}

export class ImportItemModal extends React.PureComponent<ImportItemModalProps> {
  render() {
    const { visible, items, loading, onImport, onSelectionChange, onSearch, onCancel } = this.props

    return (
      <Modal width={794} footer={null} bodyStyle={{ padding: '0' }} visible={visible} onCancel={onCancel}>
        <DialogHeader>
          Import Items
          <Input.Search
            placeholder="Search item name or SKU"
            size="large"
            style={{ width: '406px', border: '0', marginLeft: '33px' }}
            enterButton={searchButton}
            onSearch={onSearch}
          />
        </DialogHeader>
        <ItemTable saleItems={items} loading={loading} onSelectionChange={onSelectionChange} />
        <DialogFooter>
          <ThemeButton size="large" type="primary" style={importButtonStyle} onClick={onImport}>
            Import
          </ThemeButton>
          <Button onClick={onCancel}>CANCEL</Button>
        </DialogFooter>
      </Modal>
    )
  }
}
