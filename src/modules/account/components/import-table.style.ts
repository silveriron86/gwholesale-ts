import styled from '@emotion/styled'
import { css } from '@emotion/core'

import { white, black, brownGrey, brightGreen, transparent, mediumGrey } from '~/common'

export interface DialogStyle {
  checkbox?: boolean
  tableHeaderLargePadding?: boolean
}

export const TableTr = styled('tr')({
  height: '50px',
})

export const EmptyTr = styled('tr')({
  height: '0px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})

export const HoverInputCss = () =>
  css({
    '& .ant-input:hover': {
      border: '1px solid #d9d9d9',
    },
    '& .ant-input': {
      border: '0px',
    },
  })

export const tableCss = (props: DialogStyle) =>
  css({
    '& .ant-table-content': {
      '& .ant-table-header': {
        // y-scroll
        marginBottom: '0px',
        paddingBottom: '12px',
        background: white,
        '& > table': {
          '& .ant-table-thead': {
            // boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
            '& > tr > th:first-of-type': {
              padding: '1px 0px 15px 22px',
            },
            '& > tr > th:nth-of-type(2)': {
              padding: props.checkbox ? '0px 0px 15px 0px' : '',
            },
            '& > tr > th': {
              backgroundColor: white,
              color: black,
              border: 'none',
              padding: props.tableHeaderLargePadding ? '0px 0px 15px 27px' : '0px 0px 15px 17px',
              fontWeight: 700,
              fontSize: '12px',
              lineHeight: '14px',
              '& > div': {
                display: 'flex',
              },
            },
          },
        },
      },
    },
    '& .ant-table-body': {
      '& > table': {
        '& > tbody': {
          '& .ant-table-row': {
            borderBottom: '1px solid #d9d9d9',
            '& td:first-of-type': {
              borderRight: '0px',
            },
            '& td:nth-of-type(2)': {
              padding: '7px 7px 8px 0px',
            },
            '& td': {
              color: black,
              fontSize: '14px',
              lineHeight: '17px',
              fontWeight: 300,
              padding: '7px 7px 8px 22px',
              borderRight: '1px solid #EDF1EE',
            },
          },
        },
      },
    },
  })

export const DialogHeader = styled('div')({
  display: 'flex',
  alignItems: 'flex-end',
  marginLeft: '43px',
  marginBottom: '21px',
  padding: '24px',
  fontSize: '28px',
  color: mediumGrey,
  lineHeight: '30px',
  letterSpacing: '0.05em',

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    paddingLeft: '4px',
    paddingBottom: '0px',
    fontSize: '14px',
    lineHeight: '15px',
    letterSpacing: '0.05em',
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: brightGreen,
    padding: 0,
    '&::after': {
      display: 'none',
    },
  },
})

export const DialogFooter = styled('div')({
  width: '100%',
  boxSizing: 'border-box',
  boxShadow: '0px 7px 7px 13px rgba(0, 0, 0, 0.15)',
  padding: '14px',
  paddingRight: '30px',
  display: 'flex',
  justifyContent: 'flex-end',
  alignItems: 'center',
  marginRight: '14px',
  marginTop: '11px',

  '& > button:nth-of-type(2)': {
    color: brownGrey,
    marginLeft: '23px',
    cursor: 'pointer',
    border: '0px',
    '& > span': {
      textDecoration: 'underline',
      fontSize: '14px',
      lineHeight: '15px',
      fontWeight: 700,
    },
  },
})

export const importButtonStyle: React.CSSProperties = {
  borderRadius: '200px',
  width: '130px',
  height: '40px',
}
