/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Form, Input, Checkbox, Button, Modal } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import { inputTitleStyles, buttonStyle } from './login-form.style'
import { isChrome } from 'react-device-detect'

const { warning } = Modal
const FormItem = Form.Item

interface LoginProps extends FormComponentProps {
  email: string
  password: string
  loginBytoken: (payload: any) => void
  onShowModal: () => void
}

class LoginForm extends React.Component<LoginProps> {
  constructor(props: LoginProps) {
    super(props)
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const { onShowModal } = this.props
    // const { handleSubmit } = this.props;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical" className="login-form">
        <FormItem
          css={inputTitleStyles}
          style={{ paddingTop: '32px', marginBottom: '21px' }}
          label="Email"
          colon={false}
        >
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Email is required!' }],
          })(<Input style={{ width: '504px' }} />)}
        </FormItem>
        <FormItem css={inputTitleStyles} style={{ marginBottom: '11px' }} label="Password">
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Password is required!' }],
          })(<Input type="password" style={{ width: '504px' }} />)}
        </FormItem>
        <FormItem>
          <Button css={buttonStyle} type="primary" htmlType="submit">
            Sign In
          </Button>
          {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(<Checkbox style={{ visibility: 'hidden', marginRight: '100px' }}>Remember me</Checkbox>)}
          <a onClick={onShowModal} className="forgot-pwd">Forgot password</a>
        </FormItem>
        <FormItem>
          Want to use ? <a href="mailto:cs@grubmarket.com">Contact us now!</a>
        </FormItem>
      </Form>
    )
  }

  private handleSubmit = (e: any) => {
    const { form, loginBytoken } = this.props
    e.preventDefault()
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (!isChrome) {
            warning({
            title: 'Warning',
            content: 'We suggest to use google chrome instead of safari',
            onOk() {
              loginBytoken({ email: values.email, password: values.password })
            }
          });
        } else {
          loginBytoken({ email: values.email, password: values.password })
        }
      }
    })
  }
}

export const CustomizedLoginForm = Form.create<LoginProps>()(LoginForm)
