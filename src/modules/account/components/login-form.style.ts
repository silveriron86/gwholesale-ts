import styled from '@emotion/styled'
import { css } from '@emotion/core'

import { mediumGrey, brightGreen, grey } from '~/common'

export const Flex = styled('div')({
  display: 'flex',
})

export const inputTitleStyles = css({
  color: mediumGrey,
  fontWeight: 700,
  fontSize: '14px',
  lineHeight: '24px',
  textTransform: 'uppercase',
  paddingBottom: '0px !important',

  '& .ant-form-item-required': {
    color: grey,
    fontSize: '14px',
    lineHeight: '33px',
  }
})

export const buttonStyle = css({
  borderRadius: '200px',
  width: '130px',
  height: '40px',
  border: `1px solid ${brightGreen}`,
  backgroundColor: brightGreen,
  marginTop: '19px',
  marginRight: '25px',
  fontWeight: 700,
})
