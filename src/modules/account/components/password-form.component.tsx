/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Form, Input, Button } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import { inputTitleStyles, buttonStyle } from './login-form.style'

const FormItem = Form.Item

interface PasswordProps extends FormComponentProps {
  resetPassword: (payload: any) => void
  token: string
}

class PasswordForm extends React.Component<PasswordProps & FormComponentProps> {
  state = {
    confirmDirty: false,
  }


  constructor(props: PasswordProps & FormComponentProps) {
    super(props)
  }


  validateToNextPassword = (_rule: any, value: any, callback: () => void) => {
    const form = this.props.form
    if (value && this.state.confirmDirty) {
      form.validateFields(['password2'], { force: true })
    }
    callback()
  }

  compareToFirstPassword = (_rule: any, value: any, callback: { (arg0: string): void; (): void }) => {
    const form = this.props.form
    if (value && value !== form.getFieldValue('password1')) {
      callback('Passwords does not match!')
    } else {
      callback()
    }
  }

  handlePwdBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const value = e.target.value
    // 根据 password1 的值来决定 password2 的校验规则
    // 如果 password1 存在 则 password2 为 required
    this.setState(
      {
        confirmDirty: Boolean(value),
      },
      () => {
        this.props.form.validateFields(['password2'], { force: true })
      },
    )
  }

  handleConfirmBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    const value = e.target.value
    this.setState({
      confirmDirty: this.state.confirmDirty || Boolean(value),
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    // const { handleSubmit } = this.props;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
        <FormItem css={inputTitleStyles} style={{ marginBottom: '34px' }} label="Password">
          {getFieldDecorator('password1', {
            rules: [{ validator: this.validateToNextPassword }],
          })(
            <Input
              type="password"
              placeholder="**********"
              onBlur={this.handlePwdBlur}
              style={{ width: '503px' }}
            />,
          )}
        </FormItem>
        <FormItem css={inputTitleStyles} style={{ marginBottom: '34px' }} label="Confirm Password">
          {getFieldDecorator('password2', {
            rules: [
              { required: this.state.confirmDirty, message: 'Password confirm is required!' },
              { validator: this.compareToFirstPassword },
            ],
          })(
            <Input
              type="password"
              placeholder="**********"
              onBlur={this.handleConfirmBlur}
              style={{ width: '503px' }}
            />,
          )}
        </FormItem>
        <FormItem>
          <Button css={buttonStyle} type="primary" htmlType="submit">
            Save
          </Button>
        </FormItem>
      </Form>
    )
  }

  private handleSubmit = (e: any) => {
    const { form, resetPassword, token } = this.props
    e.preventDefault()
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        resetPassword({ password: values.password1, token: token })
      }
    })
  }
}

export const CustomizedPasswordForm = Form.create<PasswordProps>()(PasswordForm)
