import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'

import { GlobalState } from '~/store/reducer'
import { AccountModule, AccountDispatchProps, AccountStateProps } from './account.module'

import { brightGreen, Theme } from '~/common'
import { Icon } from '~/components/icon'
import { Flex, LoginContainer, LoginHeader, loginLogoStyle } from './account-new.style';

import { CustomizedPasswordForm } from './components/password-form.component'
import { MessageHeader } from '~/components';
import { withTheme } from 'emotion-theming';

export type AccountPasswordProps = AccountDispatchProps & AccountStateProps & RouteComponentProps<{ token: string }> & { theme: Theme }


export class AccountPassword extends  React.PureComponent<AccountPasswordProps> {

  componentDidMount() {
  }

  render() {
    const token = this.props.match.params.token
    console.log("token "+token)

    return (
      <>
      <MessageHeader message={this.props.message} type={this.props.type} description={this.props.description} />

      <div style={{display: 'flex', alignItems: 'center', flexDirection: 'column'}}>
        <Icon type="logo2" viewBox={void 0} color={brightGreen} style={loginLogoStyle} />
        <LoginContainer>
          <Flex style={{flexDirection: 'column', marginLeft: '75px'}}>
            <LoginHeader>Set new Password</LoginHeader>
            <CustomizedPasswordForm  token={token} resetPassword={this.props.resetPassword} />
          </Flex>
        </LoginContainer>
      </div>
      </>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.account

export const AccountPasswordContainer = withTheme(connect(AccountModule)(mapStateToProps)(AccountPassword))
