import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { RouteComponentProps, RouteProps } from 'react-router'

import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'

import { OrderTable, OrderHeader, presets } from './components'
import { AuthUser, UserRole } from '~/schema'
import { ResizeCallbackData } from 'react-resizable'
import PageLayout from '~/components/PageLayout'
import moment from 'moment'
import { Modal, Icon } from 'antd'
import { CACHED_NS_LINKED } from '~/common'
import { OrderQBOSyncModal } from './components/order-sync-qbo-modal.container'
import { PrintPickSheet } from './sales/cart/print'
import { fullButton } from './sales/_style'
import { ThemeButton, ThemeSpin } from '../customers/customers.style'
import { printWindow, getFulfillmentDate, isSeller } from '~/common/utils'
import _ from 'lodash'
import PrintInvoice from '~/modules/cashSales/sales/cart/print/print-invoice'

export type OrdersProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteProps &
  RouteComponentProps & {
    currentUser: AuthUser
    onResize?: (e: React.SyntheticEvent, data: ResizeCallbackData) => any
  }

export class OrdersComponent extends React.PureComponent<OrdersProps> {
  dateFormat = 'MM/DD/YYYY'
  state: any

  constructor(props: OrdersProps) {
    super(props)
    const sessionSearch = localStorage.getItem(`CS_ORDERS_SEARCH`)
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search)
    const search = urlParams.get('search')
    const startDate = urlParams.get('datestart')
    const endDate = urlParams.get('dateend')
    const page = urlParams.get('page')
    const preset = urlParams.get('preset')

    let fromDate = moment()
    let toDate = moment()
    if (preset === '8') {
      if (startDate) {
        fromDate = moment(startDate, 'MMDDYY')
      }
      if (endDate) {
        toDate = moment(endDate, 'MMDDYY')
      }
    }

    this.state = {
      searchStr: search ? search : '',
      curPreset: preset !== '' && preset !== null ? parseInt(preset, 10) : startDate || endDate ? 8 : 2,
      fromDate,
      toDate,
      fromDateString: fromDate.format(this.dateFormat),
      toDateString: toDate.format(this.dateFormat),
      showModal: false,
      page: page ? parseInt(page, 10) : 0,
      pageSize: 20,
      orderByFilter: '',
      direction: '',
      printPickSheetReviewShow: false,
      PrintBillOfLadingReviewShow: false,
      printPickSheetRef: null,
      PrintBillOfLadingRef: null,
      finishLoadingPrintLogo: this.props.logo === 'default' ? true : false,
    }
  }

  componentDidMount() {
    this.props.resetOrder()
    const { curPreset, fromDate, toDate } = this.state

    if (curPreset === 8) {
      this.onPresetChange(curPreset, { from: fromDate, to: toDate })
    } else {
      const selected = presets.find((p) => p.key === curPreset)
      this.onPresetChange(curPreset, selected)
    }
    this.props.getPrintSetting()
    this.props.getSellerSetting()
    this.props.getCompanyProductAllTypes()

    const { currentUser } = this.props
    // if (isSeller(currentUser.accountType)) {
    //   this.props.getCustomers()
    // }
  }

  getAllOrders() {
    const searchObj = {
      ...this.state,
      fromDate: this.state.fromDate.format('MM/DD/YYYY'),
      toDate: this.state.toDate.format('MM/DD/YYYY'),
    }

    this.updateURL()

    const { currentUser } = this.props
    this.props.resetLoading()
    if (isSeller(currentUser.accountType)) {
      const query = {
        page: searchObj.page,
        pageSize: searchObj.pageSize,
        from: searchObj.fromDate,
        to: searchObj.toDate,
        search: searchObj.searchStr,
        orderBy: searchObj.orderByFilter,
        direction: searchObj.direction,
      }
      this.props.getWholesaleCashSaleOrders({ page: 0, ..._.pickBy(query, _.identity) })
    } else {
      this.props.getOrdersForCustomerUserId(searchObj)
    }
  }

  updateURL = () => {
    const url = new URL(window.location.href)
    const queryParams = url.searchParams
    const { searchStr, fromDate, toDate, page, curPreset } = this.state
    if (searchStr) {
      queryParams.set('search', searchStr)
    }
    queryParams.set('preset', curPreset.toString())
    if (curPreset === 8) {
      if (fromDate) {
        queryParams.set('datestart', moment(fromDate).format('MMDDYY'))
      }
      if (toDate) {
        queryParams.set('dateend', moment(toDate).format('MMDDYY'))
      }
    }
    if (page > 0) {
      queryParams.set('page', page.toString())
    }
    url.search = queryParams.toString()

    localStorage.setItem(`CS_ORDERS_SEARCH`, url.search)

    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString())
  }

  componentWillReceiveProps(nextProps: OrdersProps) {
    if (nextProps.newOrderId > 0) {
      localStorage.setItem('NEW_CASH_SALE', 'true')
      this.props.history.push(`/cash-sales/${nextProps.newOrderId}/`)
      this.props.resetNewOrderId()
    }
    if (JSON.stringify(this.props.sellerSetting) !== JSON.stringify(nextProps.sellerSetting)) {
    }
    if (nextProps.loading === false && this.state.showModal === true) this.onCloseModal()

    if (this.props.loading !== nextProps.loading && nextProps.loading === false) {
      const { page, pageSize } = this.state
      const { cashSaleOrderTotal } = nextProps
      if (cashSaleOrderTotal <= page * pageSize) {
        this.setState(
          {
            page: 0,
          },
          () => {
            this.updateURL()
          },
        )
      }
    }
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  onDateChange = (dates: [any, any], dateStrings: [string, string]) => {
    const state = { ...this.state }
    state.fromDate = dates[0]
    state.toDate = dates[1]
    // state['page'] = 0
    this.setState(state, () => {
      this.getAllOrders()
    })
  }

  onSync = (orderIds: string[]) => {
    if (this.props.loading === true) return
    this.props.resetLoading()

    const isNS = localStorage.getItem(CACHED_NS_LINKED) !== 'null'
    if (isNS) {
      this.props.setBatchMax(orderIds.length)
      this.props.syncNSOrders(orderIds)
    } else this.props.syncQBOOrders(orderIds)
  }

  onOpenModal = () => {
    this.setState({
      showModal: true,
    })
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
    })
  }

  OnTableChange = (pagination: any, sorter: any) => {
    const state = { ...this.state }
    if (pagination !== null) {
      state.page = pagination.current - 1
    }
    if (sorter && Array.isArray(sorter)) {
      const filterStr = sorter
        .map((el: any) => {
          return el.dataIndex
        })
        .join(',')
      const directionStr = sorter
        .map((el: any) => {
          return el.sortOrder === 'ascend' ? 'ASC' : 'DESC'
        })
        .join(',')
      state.orderByFilter = filterStr ? filterStr : ''
      state.direction = directionStr ? directionStr : ''
    }

    this.setState(state, () => {
      this.getAllOrders()
    })
  }

  onCreate = (clientNumber: string) => {
    // const { routes } = this.props

    const quotedDate = moment(new Date()).format('MM/DD/YYYY')
    const { sellerSetting } = this.props
    const fulfillmentDate = getFulfillmentDate(sellerSetting)
    const data = {
      wholesaleClientId: clientNumber,
      orderDate: quotedDate,
      quotedDate: quotedDate,
      // deliveryDate: quotedDate, // default for Scheduled Delivery Date to be the same as what you are setting for Quoted Delivery Date
      deliveryDate: fulfillmentDate,
      itemList: [],
      cashSale: true,
    }
    // console.log(data)
    this.props.createEmptyOrder(data)
  }

  onSearch = (value: string) => {
    const state = { ...this.state }
    state.searchStr = value
    state.page = 0
    this.setState(state, () => {
      this.getAllOrders()
    })
  }

  onPrintPicks = () => {
    this.setState({
      printPickSheetReviewShow: true,
    })

    this._loadPrintData()
  }

  _loadPrintData = () => {
    const { searchStr, fromDate, toDate } = this.state
    this.props.startPrintOrders()
    this.props.getPrintCashSales({
      fromDate: fromDate.format('MM/DD/YYYY'),
      toDate: toDate.format('MM/DD/YYYY'),
      searchStr,
    })
  }

  onPickDelivery = () => {
    this.setState({
      PrintBillOfLadingReviewShow: true,
    })

    this._loadPrintData()
  }

  closePrintModal = (flag: number) => {
    if (flag === 1) {
      this.setState({
        printPickSheetReviewShow: false,
      })
    } else {
      this.setState({
        PrintBillOfLadingReviewShow: false,
      })
    }
  }

  changePrintLogoStatus = () => {
    this.setState({
      finishLoadingPrintLogo: true,
    })
  }

  onPresetChange = (curPreset: number, selected: any) => {
    this.setState(
      {
        curPreset,
      },
      () => {
        const dateFormat = 'MM/DD/YYYY'
        console.log('**** selcted = ', selected)
        if (selected && selected.from && selected.to) {
          this.onDateChange(
            [selected.from, selected.to],
            [selected.from.format(dateFormat), selected.to.format(dateFormat)],
          )
        }
      },
    )
  }

  render() {
    const {
      searchStr,
      curPreset,
      fromDate,
      toDate,
      page,
      pageSize,
      printPickSheetReviewShow,
      PrintBillOfLadingReviewShow,
    } = this.state
    const {
      cashSaleOrders,
      cashSaleOrderTotal,
      itemList,
      loadingSaleItems,
      printCashSales,
      printSetting,
      loadingPrintOrders,
      companyProductTypes,
      cashSaleAllTotal
    } = this.props
    const lastOrders = cashSaleOrders

    const printContent: any = (type: number) => {
      let content
      if (type === 1) {
        content = this.state.printPickSheetRef
      } else {
        content = this.state.PrintBillOfLadingRef
      }
      return content
    }

    const company = this.props.sellerSetting ? this.props.sellerSetting.company : null

    return (
      <PageLayout noSubMenu={true} currentTopMenu="menu-Selling & Shipping-Cash Sales">
        <OrderHeader
          search={searchStr}
          curPreset={curPreset}
          dates={[fromDate, toDate]}
          onPresetChange={this.onPresetChange}
          onDateChange={this.onDateChange}
          currentUser={this.props.currentUser}
          onCreate={this.onCreate}
          clients={[]}
          onSearch={this.onSearch}
          onShowModal={this.onOpenModal}
          loadingSaleItems={loadingSaleItems}
          saleItems={itemList}
          total={cashSaleOrderTotal}
          cashSaleAllTotal={cashSaleAllTotal}
          finalizePurchaseOrder={this.props.finalizePurchaseOrder}
          onPrintPicks={this.onPrintPicks}
          onPickDelivery={this.onPickDelivery}
          printSetting={this.props.printSetting}
          companyProductTypes={this.props.companyProductTypes}
          settingCompanyName={this.props.settingCompanyName}
          sellerSetting={this.props.sellerSetting}
          history={this.props.history}
        />
        <Modal
          width={1080}
          footer={null}
          visible={printPickSheetReviewShow}
          onCancel={() => this.closePrintModal(1)}
          wrapClassName="print-modal"
        >
          <ThemeSpin spinning={loadingPrintOrders}>
            <div id={'printPickSheetModal'} style={{ minHeight: 500 }}>
              <div
                ref={(el) => {
                  this.setState({ printPickSheetRef: el })
                }}
              >
                {printCashSales.map((order, index) => {
                  return (
                    <div key={`print-picksheet-${index}`}>
                      <PrintPickSheet
                        orderItems={order.printOrderItem}
                        currentOrder={order}
                        companyName={this.props.companyName}
                        catchWeightValues={[]}
                        logo={this.props.logo}
                        multiple={true}
                        changePrintLogoStatus={this.changePrintLogoStatus}
                      />
                      <div style={{ pageBreakAfter: 'always' }} />
                    </div>
                  )
                })}
              </div>
            </div>
            <ThemeButton
              shape="round"
              style={fullButton}
              className="print-pick-sheet"
              onClick={() => printWindow('printPickSheetModal', printContent.bind(this, 1))}
            >
              <Icon type="printer" theme="filled" />
              Print Pick Sheets
            </ThemeButton>
          </ThemeSpin>
        </Modal>

        <Modal
          width={1200}
          footer={null}
          visible={PrintBillOfLadingReviewShow}
          onCancel={() => this.closePrintModal(2)}
          wrapClassName="print-modal"
        >
          <ThemeSpin spinning={loadingPrintOrders}>
            <div id={'PrintBillOfLadingModal'} style={{ minHeight: 500 }}>
              <div
                ref={(el) => {
                  this.setState({ PrintBillOfLadingRef: el })
                }}
              >
                {printCashSales.map((order, index) => {
                  return (
                    <div key={`print-picksheet-${index}`}>
                      <PrintInvoice
                        getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                        catchWeightValues={[]}
                        orderItems={order.printOrderItem}
                        currentOrder={order}
                        multiple={true}
                        companyName={this.props.companyName}
                        company={company}
                        logo={this.props.logo}
                        printSetting={printSetting}
                        type={'invoice'}
                        changePrintLogoStatus={this.changePrintLogoStatus}
                        sellerSetting={this.props.sellerSetting}
                        fulfillmentOptionType={order.fulfillmentType}
                        paymentTerms={[
                          ..._.get(companyProductTypes, 'paymentTerms', []),
                          ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                        ]}
                      />
                      <div style={{ pageBreakAfter: 'always' }} />
                    </div>
                  )
                })}
              </div>
            </div>

            <ThemeButton
              shape="round"
              style={fullButton}
              className="print-bill-lading"
              onClick={() => printWindow('PrintBillOfLadingModal', printContent.bind(this, 2))}
            >
              <Icon type="printer" theme="filled" />
              Print Invoices
            </ThemeButton>
          </ThemeSpin>
        </Modal>

        <OrderQBOSyncModal
          visible={this.state.showModal}
          onCancel={this.onCloseModal}
          onOk={this.onSync}
          orders={lastOrders}
          loading={this.props.loading}
          sellerSetting={this.props.sellerSetting}
        />
        <OrderTable
          history={this.props.history}
          orders={lastOrders}
          tableMini={false}
          currentUser={this.props.currentUser}
          changePage={this.OnTableChange}
          currentPage={page + 1}
          pageSize={pageSize}
          total={cashSaleOrderTotal}
          loading={this.props.loading}
          curSortKey={this.state.orderByFilter}
          sellerSetting={this.props.sellerSetting}
        />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export const CashSales = connect(OrdersModule)(mapStateToProps)(OrdersComponent)
