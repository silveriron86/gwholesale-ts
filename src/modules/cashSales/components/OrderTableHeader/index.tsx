/**@jsx jsx */
/** @jsxFrag Preact.Fragment */
import React from 'react'
import { jsx } from '@emotion/core'
import { Select, DatePicker, Button, Form, Spin, Row, Col } from 'antd'
import { FormComponentProps } from 'antd/es/form'
import moment from 'moment'
import { NewOrderFormModal } from '~/modules/orders/components'

import {
  Header,
  BackButton,
  backButtonCss,
  OrderWrapper,
  OrderTitle,
  OrderId,
  OrderEditor,
  CustomerSelector,
  CustomerSelectorTitle,
  selectStyles,
  DateTitle,
  DateEditorWrapper,
  ItemWrapper,
  ItemTitle,
  ItemCount,
  PriceWrapper,
  PriceTitle,
  PriceTotal,
  ButtonWrapper,
  buttonStyles,
  overrideFormCss,
  CompanyInfoWrapper,
  LogoWrapper
} from './styles'
import { Icon } from '~/components'
import { OrderLoading, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Moment } from 'moment'
import { formatNumber } from '~/common/utils'
import { ThemeButton, ThemeIcon } from '~/modules/customers/customers.style'
import { defaultLogoStyle } from '~/modules/setting/theme.style'


export interface CustomerFormFields {
  deliveryDate: Moment
  clientId: string
}

interface OrderTableHeaderProps {
  customers: any
  loadingIndex: string
  loadIndex: OrdersDispatchProps['loadIndex']
  itemCount: number
  totalPrice: number
  onCreateOrder: (payload: CustomerFormFields) => void
  orderMeta?: {
    title: string
    id: string
    selector: string
    clientCompany: string,
    logoUrl: string
  }
  selectedCustomer?: any
  onClickBack: () => ReduxActions.Action<void>
  isRemovedCats: boolean
  onUpdateCatsView: Function
  isTokenOrder: boolean
}

export class OrderTableHeader extends React.PureComponent<
  FormComponentProps<CustomerFormFields> & OrderTableHeaderProps
> {
  state = {
    createShow: false
  }
  getState = (): Promise<CustomerFormFields | null> => {
    const { selectedCustomer } = this.props
    return new Promise((resolve) => {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          values.clientId = selectedCustomer
          resolve(values)
        }
        resolve()
      })
    })
  }

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    // this.props.loadIndex(OrderLoading.OrderPlacing)
    // const payload = await this.getState()
    // if (payload) {
    //   this.props.onCreateOrder(payload)
    // } else {
    //   this.props.loadIndex('')
    // }
    const { selectedCustomer, isTokenOrder } = this.props
    if (isTokenOrder) {
      if (selectedCustomer) {
        this.onClickCreate(selectedCustomer);
      }
    } else {
      if (selectedCustomer) {
        this.onClickCreate(selectedCustomer);
      } else {
        this.setState({
          createShow: true
        })
      }
    }
  }

  onCloseNewOrder = () => {
    this.setState({
      createShow: false
    })
  }

  onClickCreate = (selectedCustomer: any) => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.loadIndex(OrderLoading.OrderPlacing)
        values.clientId = selectedCustomer
        this.props.onCreateOrder(values)
        this.setState({
          createShow: false
        })
      }
    })
  }
  render() {
    const {
      customers,
      loadingIndex,
      orderMeta = {} as any,
      form: { getFieldDecorator },
    } = this.props
    const logoImage =
      !orderMeta.logoUrl || orderMeta.logoUrl === 'default' ? (
        <LogoWrapper>
          <Icon type="logo" viewBox={void 0} style={defaultLogoStyle} />
        </LogoWrapper>
      ) : (
        <img src={orderMeta.logoUrl} />
      )
    return (
      <>
        <NewOrderFormModal
          visible={this.state.createShow}
          onOk={this.onClickCreate}
          onCancel={this.onCloseNewOrder}
          clients={customers}
          okButtonName="CREATE ORDER"
          isNewOrderFormModal={true}
        />
        <Form onSubmit={this.handleSubmit} css={overrideFormCss}>
          <div>
            <Row type="flex" justify="space-between" style={{ padding: 20 }}>
              <BackButton onClick={this.props.onClickBack} style={{ float: 'left' }}>
                <Icon viewBox="-3 -3 16 16" width="24" height="24" type="arrow-left" css={backButtonCss} />
                BACK
              </BackButton>
              <CompanyInfoWrapper style={{ marginRight: 20 }}>
                {logoImage}
                <span>{orderMeta.selector}</span>
              </CompanyInfoWrapper>
            </Row>
            <Row>
              <Col offset={4} span={16}>
                <Row type='flex' justify='space-between' style={{ marginBottom: 20 }}>
                  <OrderWrapper>
                    <OrderId>{orderMeta.clientCompany || '#'}</OrderId>
                  </OrderWrapper>
                  <DateEditorWrapper>
                    <Form.Item>
                      {getFieldDecorator('deliveryDate', {
                        initialValue: moment(),
                        rules: [{ type: 'object', required: true, message: 'Please select delivery date!' }],
                      })(
                        <DatePicker
                          style={{ width: 200 }}
                          placeholder="MM/DD/YYYY"
                          format="MM/DD/YYYY"
                          allowClear={false}
                          suffixIcon={<Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                        />,
                      )}
                    </Form.Item>
                    <DateTitle>DELIVERY DATE</DateTitle>
                  </DateEditorWrapper>
                  <ItemWrapper>
                    <ItemCount>{this.props.itemCount}</ItemCount>
                    <ItemTitle>ITEM(S)</ItemTitle>
                  </ItemWrapper>
                  <PriceWrapper>
                    <PriceTotal>$ {formatNumber(this.props.totalPrice, 2)}</PriceTotal>
                    <PriceTitle>TOTAL PRICE</PriceTitle>
                  </PriceWrapper>
                  <ButtonWrapper style={{ paddingBottom: 10 }}>
                    <ThemeButton htmlType="submit" type="primary" size="large" css={buttonStyles} disabled={loadingIndex.length > 0}>
                      {loadingIndex === OrderLoading.OrderPlacing ? (
                        <span>
                          <Spin /> Placing Order
                        </span>
                      ) : (
                        <span>Place Order</span>
                      )}
                    </ThemeButton>
                    <ThemeButton
                      size="large"
                      type="primary"
                      css={buttonStyles}
                      style={{ position: 'absolute', right: -200 }}
                      onClick={this.props.onUpdateCatsView}
                    >
                      {this.props.isRemovedCats === true ? 'Category View' : 'Remove Categories'}
                    </ThemeButton>
                  </ButtonWrapper>
                </Row>
              </Col>
              <Col span={4}>
              </Col>
            </Row>
          </div>
        </Form>
      </>
    )
  }
}

const EnhancedOrderTableHeader = Form.create<FormComponentProps<CustomerFormFields> & OrderTableHeaderProps>()(
  OrderTableHeader,
)

export default EnhancedOrderTableHeader
