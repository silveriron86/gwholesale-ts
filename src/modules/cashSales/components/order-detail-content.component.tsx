/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { ColumnProps } from 'antd/es/table'
import { OrderItem, OrderDetail } from '~/schema'
import { tableCss } from './order-detail.style'
import { MutiSortTable } from '~/components'

export interface OrderDetailContentProps {
  order: OrderDetail
  items: OrderItem[]
  hasPadding?: boolean
  editorMode?: boolean
}

export class OrderDetailContent extends React.PureComponent<OrderDetailContentProps> {
  private mutiSortTable = React.createRef<any>()

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a ? a[key] ? a[key] : '' : ''
    const stringB = b ? b[key] ? b[key] : '' : ''
    return stringA.localeCompare(stringB)
  }

  render() {
    const columns: ColumnProps<OrderItem>[] = [
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'SKU')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('SKU', b, a)
          return this.onSortString('SKU', a, b)
        },
      },
      {
        title: 'CATEGORY',
        dataIndex: 'category',
        key: 'category',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'category',
          )
          if (b.category) {
            if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('name', b.category, a.category)
            return this.onSortString('name', a.category, b.category)
          } else {
            return false;
          }
        },
      },
      {
        title: 'NAME',
        dataIndex: 'variety',
        key: 'variety',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'variety',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('variety', b, a)
          return this.onSortString('variety', a, b)
        },
        render(name: string, record: any) {
          if (!record.isInventory) {
            return <b>{record.itemName}</b>
          } else {
            return <b>{name}</b>
          }
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'size')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('size', b, a)
          return this.onSortString('size', a, b)
        },
      },
      {
        title: 'WEIGHT',
        dataIndex: 'weight',
        key: 'weight',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'weight')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('weight', b, a)
          return this.onSortString('weight', a, b)
        },
      },
      {
        title: 'GRADE',
        dataIndex: 'grade',
        key: 'grade',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'grade')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('grade', b, a)
          return this.onSortString('grade', a, b)
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'packing',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('packing', b, a)
          return this.onSortString('packing', a, b)
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'origin',
        key: 'origin',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'origin')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('origin', b, a)
          return this.onSortString('origin', a, b)
        },
      },
      {
        title: 'PROVIDER',
        dataIndex: 'provider',
        key: 'provider',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'provider',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('provider', b, a)
          return this.onSortString('provider', a, b)
        },
        render: (provider: string) => {
          return provider ? provider : ''
        }
      },
      {
        title: 'QUANTITY',
        dataIndex: 'quantity',
        key: 'quantity',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'quantity',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return b.quantity - a.quantity
          return a.quantity - b.quantity
        },
        render(text) {
          return <b>{text}</b>
        },
      },
      //Cost and Margin don't need to show in this table
      // {
      //   title: 'COST',
      //   dataIndex: 'cost',
      //   key: 'cost',
      //   sorter: (a, b) => {
      //     const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'cost')
      //     if (sortIndex && sortIndex.sortOrder === 'descend') return b.cost - a.cost
      //     return a.cost - b.cost
      //   },
      //   render(rowPrice) {
      //     return `${rowPrice ? '$' + rowPrice : ''}`
      //   },
      // },
      // {
      //   title: 'MARGIN',
      //   dataIndex: 'margin',
      //   key: 'margin',
      //   sorter: (a, b) => {
      //     const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'margin')
      //     if (sortIndex && sortIndex.sortOrder === 'descend') return b.margin - a.margin
      //     return a.margin - b.margin
      //   },
      //   render(rowPrice) {
      //     return `${rowPrice ? rowPrice + '%' : ''}`
      //   },
      // },
      {
        title: 'FREIGHT',
        dataIndex: 'freight',
        key: 'freight',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'freight',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return b.freight - a.freight
          return a.freight - b.freight
        },
        render(rowPrice) {
          return `${rowPrice ? '$' + rowPrice : ''}`
        },
      },
      {
        title: 'SALE PRICE',
        dataIndex: 'price',
        key: 'price',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'price')
          if (sortIndex && sortIndex.sortOrder === 'descend') return b.price - a.price
          return a.price - b.price
        },
        render(rowPrice) {
          return <b>{rowPrice ? '$' + rowPrice.toFixed(2) : ''}</b>
        },
      },
    ]

    if (!this.props.items) {
      return 'loading'
    }

    /*
    const components = {
      body: { row: this.row, table: 'table' },
    }
    */
    return (
      <MutiSortTable
        ref={this.mutiSortTable}
        css={tableCss(!!this.props.hasPadding)}
        pagination={false}
        //components={components}
        columns={columns}
        dataSource={this.props.items}
        bordered={true}
        rowKey="itemId"
      />
    )
  }

  /*
  private row = (props: any) => {
    const tr = <TableTr>{props.children}</TableTr>
    const { record } = props.children[1].props
    if (record.addNewItem) {
      return (
        <React.Fragment>
          <AddNewItemTr isEditing={this.state.isEditing} key="editor">
            <AddNewItem>
              {this.renderEditorRadios()}
              <AddItemsWrapper onClick={this.onEditOrder} isEditing={this.state.isEditing}>
                <PlusButton>+</PlusButton>
                Add Item(s)
              </AddItemsWrapper>
            </AddNewItem>
          </AddNewItemTr>
          {tr}
        </React.Fragment>
      )
    }
    if (record.isAnchor) {
      return (
        <React.Fragment>
          <CategoryTr>
            <Category>{record.wholesaleCategory.wholesaleSection.name}</Category>
          </CategoryTr>
          {tr}
        </React.Fragment>
      )
    }
    return tr
  }

  private renderEditorRadios() {
    if (!this.state.isEditing) {
      return null
    }
    return (
      <React.Fragment>
        <RadioWrapper>
          <RadioGroup defaultValue="Pricesheet">
            <Radio value="Pricesheet">Price Sheet</Radio>
            <Radio value="Inventory">Inventory</Radio>
          </RadioGroup>
        </RadioWrapper>
        <AutoComplete css={autocompleteCss} placeholder="Type the name of an item" />
        <Icon type="search" css={searchIconStyle} viewBox="0 0 18 18" width="16" height="16" />
      </React.Fragment>
    )
  }

  private onEditOrder = () => {
    this.setState({
      isEditing: true,
    })
  }
  */
}
