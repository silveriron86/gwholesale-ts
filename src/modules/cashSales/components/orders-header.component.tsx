/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Button, Menu, DatePicker, Modal, Select, message, Icon, Input, Dropdown, Tooltip } from 'antd'
import { Icon as IconSvg } from '~/components'
import { withTheme } from 'emotion-theming'
import {
  HeaderContainer,
  HeaderTitle,
  HeaderOptions,
  DateEditorWrapper,
  DateTitle,
  DialogFooter,
} from './orders-header.style'
import { Theme, CACHED_QBO_LINKED, CACHED_NS_LINKED, gray01 } from '~/common'
import { AuthUser } from '~/schema'
import Form, { FormComponentProps } from 'antd/lib/form'
import AutoComplete from 'antd/es/auto-complete'
import { ThemeButton, ThemeOutlineButton, ThemeSelect } from '~/modules/customers/customers.style'
import jQuery from 'jquery'
import { searchButton } from '~/components/elements/search-button'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import moment from 'moment'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, formatNumber, getFulfillmentDate, responseHandler } from '~/common/utils'
import { of } from 'rxjs'

export interface OrderHeaderProps {
  theme: Theme
  dates: [any, any]
  onShowModal: () => void
  onDateChange: (moment: [any, any], dateString: [string, string]) => void
  onSearch: (criteria: string) => void
  currentUser: AuthUser
  onCreate: (client: string) => void
  clients: any[]
  fromPriceSheet?: boolean
  total: any
  onPrintPicks: Function
  onPickDelivery: Function
  printSetting?: any
  companyProductTypes?: any
  settingCompanyName?: string
  sellerSetting?: any
  history?: any
  search?: string
}

interface OrdersState {
  createShow: boolean
  type: string
  visibleEnterModal?: boolean
  curPreset: any
  onPresetChange: Function
  searchStr: string
}

export interface NewOrderFormProps {
  visible: boolean
  onOk: (name: string) => void
  onCancel: () => void
  theme?: Theme
  clients: any[]
  okButtonName: string
  isNewOrderFormModal: boolean
  defaultClientId?: number | null
}

interface NewOrderFormFields { }

export const presets = [
  {
    key: 0,
    label: 'Past 7 Days',
    from: moment().subtract(7, 'days'),
    to: moment(),
  },
  {
    key: 1,
    label: 'Past 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment(),
  },
  {
    key: 9,
    label: 'Yesterday',
    from: moment().subtract(1, 'days'),
    to: moment().subtract(1, 'days'),
  },
  {
    key: 2,
    label: 'Today',
    from: moment(),
    to: moment(),
  },
  {
    key: 3,
    label: 'Tomorrow',
    from: moment().add(1, 'days'),
    to: moment().add(1, 'days'),
  },
  {
    key: 4,
    label: 'Today and Tomorrow',
    from: moment(),
    to: moment().add(1, 'days'),
  },
  {
    key: 5,
    label: 'Next 7 Days',
    from: moment(),
    to: moment().add(7, 'days'),
  },
  {
    key: 6,
    label: 'Next 30 Days',
    from: moment(),
    to: moment().add(30, 'days'),
  },
  {
    key: 7,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment().add(30, 'days'),
  },
  {
    key: 8,
    label: 'Custom Date Range',
  },
]
export class NewOrderForm extends React.PureComponent<FormComponentProps<NewOrderFormFields> & NewOrderFormProps> {
  state = {
    onSelected: -1,
    displayClients: this.props.clients,
  }
  componentDidUpdate(prevProps: any) {
    if (!this.props.visible) {
      this.props.form.resetFields()
    } else {
      setTimeout(() => {
        const inputCtrl = jQuery('.client-select-auto-complete .ant-select-search__field')
        inputCtrl.trigger('focus')
        // inputCtrl.trigger('click')
        this.focusedItemToTop()
      }, 500)
    }
  }

  focusedItemToTop = () => {
    const dropdown = jQuery('.customer-select-dropdown ul')
    const parent = jQuery(dropdown).parents('.ant-select-dropdown')
    if (!jQuery(parent).hasClass('ant-select-dropdown-hidden')) {
      setTimeout(() => {
        const selectedItem = jQuery('.customer-select-dropdown ul .ant-select-dropdown-menu-item-selected')
        const liEls = jQuery('.customer-select-dropdown ul li')
        const index = jQuery(liEls).index(selectedItem)

        jQuery(dropdown).scrollTop(index * 32) //32 is li height
      }, 100)
    }
  }

  renderCompanyName = (client) => {
    return (
      <Select.Option key={client.clientId} text={client.clientId}>
        {client.clientCompanyName}
      </Select.Option>
    )
  }

  handleFilterOption = (input, option) => {
    return input.length >= 2 && option.props.children.toUpperCase().indexOf(input.toUpperCase()) !== -1
  }

  onSelect = (value) => {
    this.setState(
      {
        onSelected: value,
      },
      () => {
        setTimeout(() => {
          jQuery('.ant-modal-footer .ant-btn-primary').focus()
        }, 500)
      },
    )
  }

  filterDisplayClients = (searchStr: string, defaultClientId: any) => {
    const { clients } = this.props
    // const clients = clients.sort((a, b) => a.clientCompanyName.localeCompare(b.clientCompanyName))
    if (defaultClientId) {
      const defaultClientIndex = clients.findIndex((el) => el.clientId == defaultClientId)
      let startIndex = defaultClientIndex > 20 ? defaultClientIndex - 20 : 0
      let endIndex = defaultClientIndex + 40 > clients.length ? clients.length : startIndex + 40
      const result = clients.slice(startIndex, endIndex)
      this.setState({ displayClients: result })
    } else {
      if (searchStr.length > 1) {
        const filtered = clients.filter(
          (el) => el.clientCompanyName.toLowerCase().indexOf(searchStr.toLowerCase()) > -1,
        )
        this.setState({ displayClients: filtered })
      } else {
        this.setState({ displayClients: [] })
      }
    }
  }

  render() {
    const {
      form: { getFieldDecorator },
      fromPriceSheet,
      clients,
      defaultClientId,
    } = this.props
    const { displayClients } = this.state
    const type = 'Customer'
    let defaultClient = null
    if (this.props.defaultClientId) {
      defaultClient = clients.find((el) => {
        return el.clientId === defaultClientId
      })
    }
    return (
      <Modal
        visible={this.props.visible}
        onCancel={this.handleCancel}
        footer={[
          <DialogFooter key="plus">
            <ThemeButton type="primary" icon="plus" onClick={this.handleOk}>
              {this.props.okButtonName}
            </ThemeButton>
            <Button onClick={this.handleCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <label>{type} Account</label>
        <br />
        <Form.Item>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: `Select a ${type.toLowerCase()} account` }],
            initialValue:
              fromPriceSheet === true || !defaultClientId
                ? defaultClient
                  ? defaultClient.clientCompanyName
                  : ''
                : defaultClientId,
          })(
            fromPriceSheet === true || !defaultClientId ? (
              <AutoComplete
                className="client-select-auto-complete"
                size="large"
                autoFocus={true}
                style={{ width: '100%' }}
                dataSource={displayClients && displayClients.map(this.renderCompanyName)}
                filterOption={this.handleFilterOption}
                onSelect={this.onSelect}
                onSearch={(v) => this.filterDisplayClients(v, null)}
                onFocus={() => this.filterDisplayClients('', defaultClientId)}
                placeholder={`Select a ${type} name`}
              />
            ) : (
              <ThemeSelect
                className="client-select-auto-complete"
                defaultOpen={true}
                autoFocus={true}
                defaultValue={defaultClientId}
                // value={defaultClientId}
                showSearch
                style={{ width: '100%' }}
                optionFilterProp="children"
                onChange={this.onSelect}
                onSearch={(v) => this.filterDisplayClients(v, null)}
                onFocus={() => this.filterDisplayClients('', defaultClientId)}
                placeholder={`Select a ${type} name`}
                filterOption={(input: string, option: any) => {
                  return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }}
                dropdownClassName="customer-select-dropdown"
              >
                {displayClients
                  ? displayClients.map((el) => {
                    return (
                      <Select.Option key={el.clientId} value={el.clientId}>
                        {el.clientCompanyName}
                      </Select.Option>
                    )
                  })
                  : ''}
              </ThemeSelect>
            ),
          )}
        </Form.Item>
        {!this.props.isNewOrderFormModal && this.props.children}
      </Modal>
    )
  }
  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onOk } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        if ((this.state.onSelected! = -1 && values.name == this.state.onSelected)) {
          onOk(values.name)
        } else {
          message.error('Please select a client name at list')
        }
      }
    })
  }

  handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onCancel } = this.props
    onCancel()
    form.resetFields()
  }
}

export const NewOrderFormModal = Form.create<FormComponentProps<NewOrderFormFields> & NewOrderFormProps>()(NewOrderForm)

class OrderHeaderComponent extends React.PureComponent<OrderHeaderProps, OrdersState> {
  state = {
    createShow: false,
    type: '',
    visibleEnterModal: false,
    searchStr: this.props.search
  }
  componentDidMount() {
    // Tab Order
    setTimeout(() => {
      if (jQuery('.tab-able-el').length > 0) {
        jQuery('.tab-able-el input')
          .first()
          .focus()
      }
    }, 1000)

    jQuery('.tab-able-el')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9) {
          let index = jQuery('.tab-able-el').index(e.currentTarget)
          if (e.shiftKey) {
            if (index === 1) {
              e.preventDefault()
              jQuery('.tab-able-el input')
                .first()
                .focus()
            }
          } else {
            if (index === 2) {
              e.preventDefault()
              if (jQuery('.tab-view-link').length > 0) {
                jQuery('.tab-view-link')
                  .first()
                  .focus()
              }
            }
          }
        }
      })
    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.ctrlKey === true /* && e.shiftKey === true*/ && e.keyCode === 78) {
          if (window.location.href.indexOf('/sales-orders') > 0) {
            this.setState({
              createShow: true,
            })
          } else {
            this.setState({
              visibleEnterModal: true,
            })
          }
          return
        }

        if (jQuery(e.target).hasClass('tab-view-link')) {
          e.preventDefault()
          let cards = jQuery('.tab-view-link')
          if (e.keyCode === 9) {
            let tabIndex = jQuery('.tab-view-link').index(e.target)
            if (e.shiftKey) {
              tabIndex--
            } else {
              tabIndex++
            }

            if (tabIndex < 0) {
              let lastBtn = jQuery('.tab-able-el')[2]
              jQuery(lastBtn).focus()
            } else if (tabIndex >= cards.length) {
              jQuery('.tab-able-el input')
                .first()
                .focus()
              jQuery('html, body').animate(
                {
                  scrollTop: 0,
                },
                100,
              )
            } else {
              jQuery(cards[tabIndex]).focus()
            }
          } else if (e.keyCode === 13) {
            window.location.href = e.target.href
          }
        }
      })
  }

  renderQboSyncButton = () => {
    const qboRealmId = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    if (qboRealmId)
      return (
        <ThemeButton className="icon" size="large" type="primary" onClick={this.props.onShowModal}>
          <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
          QBO Sync
        </ThemeButton>
      )
    return <span />
  }

  renderNsSyncButton = () => {
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    if (nsRealmId)
      return (
        <ThemeButton className="icon" size="large" type="primary" onClick={this.props.onShowModal}>
          <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
          NS Sync
        </ThemeButton>
      )
    return <span />
  }

  onToggleEnterModal = () => {
    this.setState({
      visibleEnterModal: !this.state.visibleEnterModal,
    })

    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.altKey === true && e.shiftKey === true && e.keyCode === 65) {
          this.setState({
            visibleEnterModal: true,
          })
        }
      })
  }

  onPresetChange = (val: number) => {
    const selected = presets.find((p) => p.key === val)
    console.log(val, selected)
    this.props.onPresetChange(val, selected)
    //  else if (val == 6) {
    //   const { dates } = this.props
    //   this.props.onDateChange([dates[0], dates[1]], [dates[0].format(dateFormat), dates[1].format(dateFormat)])
    // }
  }

  onSearchTextChange = (evt: any) => {
    this.setState({ searchStr: evt.target.value })
  }

  onSearch = (val: string) => {
    this.setState({ searchStr: val }, () => {
      this.props.onSearch(val)
    })
  }

  render() {
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED) !== 'null'
    const { total, printSetting, clients, cashSaleAllTotal } = this.props
    const userPrintSetting = JSON.parse(printSetting)
    const { searchStr } = this.state
    // const newOrderLink = location.href.indexOf('sales-') >= 0 ? '/newSaleOrder' : 'newPurchaseOrder'
    this.setState({ type: 'sales' })

    const warningTooltip =
      total >= 30 ? (
        <Tooltip
          title="Due to the large number of records, this feature may not work as expected"
          placement="bottomLeft"
        >
          <Icon type="warning" />
        </Tooltip>
      ) : null

    return (
      <HeaderContainer>
        <div style={{ position: 'absolute', top: 10, zIndex: 1000, right: 25 }}>
          <Flex>
            <ThemeOutlineButton
              size="large"
              type="primary"
              style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }}
            >
              Actions
            </ThemeOutlineButton>
            <Dropdown
              overlay={
                <Menu onClick={this.onActions}>
                  <Menu.Item key="1">Sync to {nsRealmId ? 'NS' : 'QBO'} </Menu.Item>
                  <Menu.Item key="2">Print Pick Sheets {warningTooltip}</Menu.Item>
                  <Menu.Item key="3" style={{ textTransform: 'capitalize' }}>
                    Print{' '}
                    {userPrintSetting && userPrintSetting.invoice_title ? userPrintSetting.invoice_title : 'Invoice'}s{' '}
                    {warningTooltip}
                  </Menu.Item>
                </Menu>
              }
              trigger="click"
              placement="bottomRight"
            >
              <ThemeOutlineButton
                size="large"
                type="primary"
                style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
              >
                <Icon type="down" />
              </ThemeOutlineButton>
            </Dropdown>

            <ThemeButton
              className="icon enter-purchase"
              size="large"
              type="primary"
              onClick={this.handleCreateNewCashSale}
            >
              <Icon type="plus-circle" style={{ fontSize: 20 }} />
              New Cash Sale
            </ThemeButton>
          </Flex>
        </div>
        <HeaderTitle>Cash Sales</HeaderTitle>
        <HeaderOptions style={{ alignItems: 'end' }}>
          <div style={{ width: '30%' }}>
            <DateTitle style={{ visibility: 'hidden' }}>SEARCH SALES ORDERS</DateTitle>
            <Input.Search
              placeholder={`Search order ID, sales name, status, shipping address`}
              size="large"
              defaultValue={searchStr}
              style={{ border: '0' }}
              enterButton={searchButton}
              onSearch={this.props.onSearch}
              onChange={this.onSearchTextChange}
              value={searchStr}
              allowClear
            />
          </div>
          <div style={{ display: 'flex' }}>
            {this.renderDateEditor()}
            <div style={{ marginTop: 34, marginLeft: 15, whiteSpace: 'nowrap', color: gray01 }}>
              {total} record{total > 1 ? 's' : ''}
            </div>
            <div style={{ marginTop: 34, marginLeft: 15, whiteSpace: 'nowrap', color: gray01 }}>
              Total ${formatNumber(cashSaleAllTotal, 2)}
            </div>
          </div>
          <div style={{ width: '40%' }} />
          <NewOrderFormModal
            theme={this.props.theme}
            visible={this.state.createShow}
            onOk={this.onClickCreate}
            onCancel={this.onCloseNewOrder}
            clients={clients}
            okButtonName="CREATE"
            isNewOrderFormModal={true}
          />
        </HeaderOptions>
      </HeaderContainer>
    )
  }

  private renderDateEditor() {
    const calendarIcon = <IconSvg type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    const { dates, curPreset } = this.props
    const { RangePicker } = DatePicker

    return (
      <DateEditorWrapper className="orders-delivery-date" style={{ minWidth: 230 }}>
        {curPreset == 8 && (
          <RangePicker
            // placeholder={dateFormat}
            defaultValue={[dates[0], dates[1]]}
            format={dateFormat}
            suffixIcon={calendarIcon}
            onChange={this.props.onDateChange}
            className="tab-able-el"
            allowClear={false}
          />
        )}
        <Select
          defaultValue={3}
          value={curPreset}
          className="date-filter-select"
          onChange={this.onPresetChange}
          dropdownClassName="full-select"
        >
          {presets.map((el: any) => {
            return (
              <Select.Option value={el.key} key={el.key}>
                {el.label}
              </Select.Option>
            )
          })}
        </Select>
        <DateTitle style={{ textAlign: 'left' }}>Target Fulfillment Date</DateTitle>
      </DateEditorWrapper>
    )
  }

  private handleCreateNewCashSale = () => {
    const { sellerSetting, currentUser, history } = this.props
    const quotedDate = moment(new Date()).format('MM/DD/YYYY')
    const fulfillmentDate = getFulfillmentDate(sellerSetting)

    OrderService.instance.getCashCustomerClient({ userId: currentUser.userId }).subscribe({
      next(res: any) {
        const data = {
          wholesaleClientId: String(res.body.data.clientId),
          orderDate: quotedDate,
          quotedDate: quotedDate,
          deliveryDate: fulfillmentDate,
          itemList: [],
          cashSale: true,
        }
        OrderService.instance.createOrder(data, currentUser.userId).subscribe({
          next(res: any) {
            of(responseHandler(res, false).body.data)
            localStorage.setItem('NEW_CASH_SALE', 'true')
            history.push(`/cash-sales/${res.body.data.wholesaleOrderId}/`)
          },
          error(err) {
            checkError(err)
          },
        })
      },
    })
  }

  private onCloseNewOrder = () => {
    this.setState({
      createShow: false,
    })
  }

  private onClickCreate = (clientName: string) => {
    const { type } = this.state
    if (clientName) {
      this.props.onCreate(clientName)
    }
    this.setState({
      createShow: false,
    })
  }

  private onActions = (item: any) => {
    const { key } = item
    if (key === '1') {
      this.props.onShowModal()
    } else if (key === '2') {
      this.props.onPrintPicks()
    } else {
      this.props.onPickDelivery()
    }
  }
}

export const OrderHeader = withTheme(OrderHeaderComponent)
