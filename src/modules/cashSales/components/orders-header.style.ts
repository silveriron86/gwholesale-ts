import styled from '@emotion/styled'
import { css } from '@emotion/core'
import {white, black, brownGrey, transparent, brightGreen, grey, gray01} from '~/common'
import { isBrowser, deviceType } from 'react-device-detect'

export const HeaderContainer = styled('div')({
  width: '100%',
  backgroundColor: white,
  padding: `50px ${isBrowser ? 81 : 20}px 30px ${isBrowser ? 131 : 20}px`,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  color: black,
  fontSize: '36px',
  fontWeight: 700,
  marginBottom: '39px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const HeaderOptions = styled('div')((props) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: props.theme.primary,
    '&::after': {
      display: 'none',
    },
  },
  '.clear-icon': {
    display: 'none'
  },
  '& .ant-input-search:hover .clear-icon.show': {
    display: 'block'
  }
}))

export const DateTitle = styled('span')({
  color: gray01,
  marginBottom: '10px',
})

export const DateEditorWrapper = styled('div')((props) => ({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '1%',
  '& .ant-calendar-picker': {
    '& .ant-calendar-picker-input': {
      border: 'none',
      padding: '4px 0',
      borderBottom: 'solid 1px black',
      borderRadius: 0,
      '&:focus': {
        boxShadow: 'none',
      },
    },
    '& .ant-calendar-picker-icon': {
      width: '20px',
      height: '21px',
      marginTop: '-10px',
      color: props.theme.primary,
    },
  },
}))

export const searchIconCss = css({
  marginRight: '10px',
})

export const SortByText = styled('span')({
  color: `${brownGrey} !important`,
})

export const SortByIcon: React.CSSProperties = {
  color: `${brownGrey} !important`,
  width: '18px',
  height: '13px',
}

export const ghostButtonStyle = css({
  borderRadius: '5px',
  width: '130px',
  height: '35px',
  border: `1px solid ${brownGrey}`,
  backgroundColor: transparent,
  color: `${brownGrey} !important`,
})

export const buttonStyle = css({
  borderRadius: `${isBrowser ? 200 : 100}px`,
  padding: `0 ${isBrowser ? 0 : 10}px`,
  height: `40px`,
  border: `1px solid ${brownGrey}`,
  margin: `3px ${isBrowser ? 30 : 5}px`,
  '&.ant-btn-primary': {
    width: `${deviceType == 'mobile' ? 100 : 45}%`,
    maxWidth: 180,
    minWidth: 'fit-content',
  },
  '&.ant-dropdown-trigger': {
    borderRadius: '5px',
    borderColor: brownGrey,
  },
  '& > a': {
    '&:visited': {
      color: white,
    },
  },
})

export const DialogFooter = styled('div')({
  paddingRight: '38px',
  '& > button:nth-of-type(1)': {
    height: '40px',
    fontSize: '16px',
    width: '200px',
    borderRadius: '200px',
  },

  '& > button:nth-of-type(2)': {
    color: brownGrey,
    marginLeft: '23px',
    cursor: 'pointer',
    border: '0px',
    '& > span': {
      textDecoration: 'underline',
      fontSize: '14px',
      lineHeight: '15px',
      fontWeight: 700,
    },
  },
})

export const EnterPurchaseWrapper = styled('div')((props: any) => ({
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 'normal',
  p: {
    color: 'black',
  },
  '.vendor': {
    label: {
      display: 'block'
    },
    '.ant-select': {
      minWidth: 400,
      width: 'fit-content',
      display: 'block',
      marginBottom: 20,
    },
    '.reference-no': {
      padding: '0 4px',
      height: 40
    }
  },
  '.hoverable': {
    color: props.theme.dark,
    cursor: 'pointer'
  },
  '.hoverable:hover': {
    textDecoration: 'underline',
  },
  '.custom-ant-select-focused': {
    border: `1px solid ${props.theme.primary}`,
    borderRadius: 5
  }
}))
