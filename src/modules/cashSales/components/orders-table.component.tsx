/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Link } from 'react-router-dom'
import { Icon, Table, Tooltip } from 'antd'
import { ColumnProps } from 'antd/es/table'
import { format } from 'date-fns'
import moment from 'moment'
import { Order, AuthUser, UserRole } from '~/schema'
import { tableCss } from './order-detail.style'
import { QBOImage, EmptyTr, NSImage } from './orders-table.style'
import qboImage from './qbo.png'
import nsImage from './netsuite.svg'
import { MutiSortTable } from '~/components'
import { blue, CACHED_QBO_LINKED, CACHED_NS_LINKED, CACHED_ACCOUNT_TYPE } from '~/common'
import { formatDate, formatNumber, getOrderPrefix, formatOrderStatus, isSeller } from '~/common/utils'
import { ThemeTable, ThemeLink, TextLink } from '~/modules/customers/customers.style'
import { History } from 'history'
import { MultiBackSortTable } from '~/components/multi-back-sortable'

export interface OrderTableProps {
  orders: Order[]
  tableMini: boolean
  currentUser?: AuthUser
  history: History
  currentPage: any
  pageSize: any
  total: any
  changePage: Function
  loading: boolean
  curSortKey: string
  sellerSetting?: any
}

const onSortString = (key: string, a: any, b: any) => {
  const stringA: string = a[key] ? a[key] : ''
  const stringB: string = b[key] ? b[key] : ''
  return stringA.localeCompare(stringB)
}


export class OrderTable extends React.PureComponent<OrderTableProps> {
  handleTableChange = (page: any, filters: any, sorter: any) => {
    this.props.changePage(page, sorter)
  };

  getTextLink = (order: any, content: any, isIdColumn: boolean = false) => {
    const { sellerSetting } = this.props
    let prefix = ''
    if (sellerSetting && isIdColumn) {
      prefix = getOrderPrefix(sellerSetting, 'sales')
    }
    return (
      <TextLink className="text" to={`${'/cash-sales/' + order.id}`}>
        {prefix}{content}
      </TextLink>
    )
  }


  // tslint:disable-next-line:member-ordering
  columns: ColumnProps<Order>[] = [
    {
      title: 'ID #',
      dataIndex: 'id',
      key: 'id',
      width: 200,
      className: 'no-pad-td pl-90',
      render: (d, record) => this.getTextLink(record, d, true)
    },
    {
      title: this.props.currentUser.company === 'Fresh Green Inc' ? 'RECEIPT NO.' : 'CUST. PO',
      dataIndex: 'reference',
      width: 100,
      key: 'reference',
      className: 'no-pad-td',
      render: (v, order) => this.getTextLink(order, v)
    },
    {
      title: 'ORDER DATE',
      dataIndex: 'orderDate',
      key: 'orderDate',
      width: 80,
      // sorter: (a, b) => a.createdDate - b.createdDate,
      sorter: true,
      // render: (val) => formatDate(val)
      className: 'no-pad-td',
      render: (orderDate: number, record: any) => this.getTextLink(record, moment(orderDate).format('MM/DD/YYYY'))
    },
    {
      title: 'FULFILLMENT DATE',
      dataIndex: 'deliveryDate',
      key: 'deliveryDate',
      width: 80,
      // sorter: (a, b) => a.deliveryDate - b.deliveryDate,
      sorter: true,
      className: 'no-pad-td',
      render: (val, record) => this.getTextLink(record, moment(val).format('MM/DD/YYYY'))
    },
    {
      title: 'ORDER TOTAL',
      dataIndex: 'totalCost',
      key: 'totalCost',
      width: 100,
      className: 'no-pad-td',
      render: (text, record: any) => {
        return this.getTextLink(record, <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}</span>)
      },
      sorter: (a, b) => a.totalCost - b.totalCost,
      // sorter: true,
    },
    {
      title: 'STATUS',
      dataIndex: 'status',
      key: 'wholesaleOrderStatus',
      width: 100,
      // sorter: (a, b) => onSortString('status', a, b),
      sorter: true,
      className: 'no-pad-td',
      render: (status, record: any) => this.getTextLink(record, formatOrderStatus(status, this.props.sellerSetting))
    }]

  // tslint:disable-next-line:member-ordering
  customerColumns: ColumnProps<Order>[] = [
    {
      title: 'ID #',
      dataIndex: 'id',
      key: 'id',
      className: 'no-pad-td pl-90',
      render: (d, record: any) => this.getTextLink(record, d, true),
    },
    {
      title: 'ORDER DATE',
      dataIndex: 'orderDate',
      key: 'orderDate',
      sorter: (a: any, b: any) => a.orderDate - b.orderDate,
      className: 'no-pad-td',
      render: (orderDate, record: any) => this.getTextLink(record, format(orderDate, 'MM/D/YY')),
    },
    {
      title: 'DELIVERY DATE',
      dataIndex: 'deliveryDate',
      key: 'deliveryDate',
      // sorter: (a, b) =>
      //   if (a.deliveryDate && b.deliveryDate) {
      //     return new Date(a.deliveryDate).getTime() - new Date(b.deliveryDate).getTime()
      //   }
      // },
      sorter: true,
      className: 'no-pad-td',
      render: (deliveryDate, record: any) => this.getTextLink(record, moment.utc(deliveryDate).format('MM/D/YY')),
    },
    {
      title: 'TOTAL SALE PRICE',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      className: 'no-pad-td',
      render: (text, record: any) => {
        return this.getTextLink(record, <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}</span>)
      },
      // sorter: (a, b) => a.totalPrice - b.totalPrice,
      sorter: true,
    },
    {
      title: 'SELLER',
      dataIndex: 'firstName',
      key: 'firstName',
      // sorter: (a, b) => a.firstName.localeCompare(b.firstName),
      sorter: true,
      className: 'no-pad-td',
      render: (_, order, __) => {
        let content = null
        if (order.firstName) {
          content = (
            <span>
              {order.firstName} {order.lastName}
              <br />
              {order.company.name}
            </span>
          )
        } else {
          content = (
            <span>
              {order.company.name}
            </span>
          )
        }
        return this.getTextLink(order, content)
      }
    },
    {
      key: 'view',
      render: (order) => (
        <Link to={`/order/${order.linkToken}`}>
          VIEW <Icon type="arrow-right" />
        </Link>
      ),
    },
  ]

  // tslint:disable-next-line:member-ordering
  miniColumns: ColumnProps<Order>[] = [
    {
      title: 'ID #',
      dataIndex: 'id',
      key: 'id',
      className: 'no-pad-td pl-90',
      render: (d, order) => this.getTextLink(order, d, true),
    },
    {
      title: 'ORDER DATE',
      dataIndex: 'orderDate',
      key: 'orderDate',
      // sorter: (a, b) => a.updatedDate - b.updatedDate,
      sorter: true,
      className: 'no-pad-td',
      render: (orderDate, order) => this.getTextLink(order, moment(orderDate).format('MM/DD/YYYY')),
    },
    {
      title: 'TOTAL SALE PRICE',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      className: 'no-pad-td',
      render: (text, order) => {
        return this.getTextLink(order, <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}</span>)
      },
      // sorter: (a, b) => a.totalPrice - b.totalPrice,
      sorter: true,
    },
    {
      title: 'CUSTOMER',
      dataIndex: 'customer',
      key: 'customer',
      render: (customer) =>
        customer && (
          <Link to={`/customers/${customer.id}`}>
            <span style={{ textDecoration: 'underline', color: blue }}>{customer.name}</span>
          </Link>
        ),
      // sorter: (a, b) => a.customer.id - b.customer.id,
      sorter: true,
    },
  ]

  addEndingColumns = () => {
    let hasEndingCols = false
    this.columns.forEach(element => {
      if (element.dataIndex === 'company') {
        hasEndingCols = true
        return
      }
    });
    if (hasEndingCols) {
      return
    }
    if (localStorage.getItem(CACHED_QBO_LINKED) !== 'null')
      this.columns.push(
        {
          title: 'QBO',
          dataIndex: 'qboId',
          key: 'qboId',
          width: 50,
          sorter: true,
          className: 'no-pad-td',
          render: (qboId: string, record: any) => {
            if (typeof qboId !== 'undefined' && qboId !== null) {
              const act = (record.company.type === 'CUSTOMER') ? 'invoice' : 'bill';
              return <a href={`https://qbo.intuit.com/app/${act}?txnId=${qboId}`} target="_blank"><QBOImage src={qboImage} /></a>
            } else {
              return this.getTextLink(record, '')
            }
          },
        }
      )

    if (localStorage.getItem(CACHED_NS_LINKED) !== 'null')
      this.columns.push(
        {
          title: 'NS',
          dataIndex: 'nsId',
          key: 'nsId',
          width: 50,
          sorter: true,
          render: (text, record) => {
            const nsRealmId = localStorage.getItem(CACHED_NS_LINKED)
            if (text) {
              const act = 'cashsale.nl';
              return <a href={`https://${nsRealmId}.app.netsuite.com/app/accounting/transactions/${act}?id=${text}`} target="_blank"><NSImage src={nsImage} /></a>
            } else {
              return ''
            }
          },
        }
      )

    this.columns.push(
      {
        title: 'COMPANY',
        dataIndex: 'company',
        key: 'wholesaleClient.clientCompany.companyName',
        width: 300,
        render: (company) =>
          company && (
            <Link to={
              `${company.type === 'CUSTOMER' ? "/customer/" + company.id + "/orders" : "/vendor/" + company.id + "/orders"}`
            }>
              <span style={{ textDecoration: 'underline', color: blue }}>{company.name}</span>
            </Link>
          ),
        // sorter: (a, b) => a.company.name.localeCompare(b.company.name),
        sorter: true,
      })
    this.columns.push(
      {
        key: 'view',
        width: 100,
        render: (order) => (
          <ThemeLink className='tab-view-link'
            // tslint:disable-next-line:jsx-no-lambda
            to={
              `${'/cash-sales/' + order.id}`
            }>
            VIEW <Icon type="arrow-right" />
          </ThemeLink>
        ),
      })
  }

  render() {
    const { currentPage, pageSize, total } = this.props;
    const orders = this.props.orders
    const tableMini = this.props.tableMini
    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)
    this.addEndingColumns()

    if (this.columns[5].dataIndex === 'pas') {
      this.columns.splice(5, 1);
    }

    this.columns.splice(5, 0, {
      title: (<Tooltip title="Priced after sale">PAS</Tooltip>),
      dataIndex: 'pas',
      key: '',
      width: 80,
      sorter: true,
      className: 'no-pad-td',
      render: (id: boolean, record: any) => {
        const pasItems = record.orderItems && Array.isArray(record.orderItems) ? record.orderItems.filter((el: any) => { return el.pas === true }) : []
        return this.getTextLink(record, `${pasItems.length} ${pasItems.length <= 1 ? 'item' : 'items'}`)
      }
    });


    if (accountType === UserRole.WAREHOUSE) {
      const found = this.columns.findIndex(col => {
        return col.dataIndex === 'totalCost'
      });
      if (found >= 0) {
        this.columns.splice(found, 1);
      }
    }

    const orderColumns =
      this.props.currentUser != null &&
        (isSeller(this.props.currentUser.accountType))
        ? this.columns
        : this.customerColumns

    const onRow = (order: any, index: number) => {
      return {
        onClick: (_event: any) => {
          if (order.company.type && order.id && _event.target.tagName !== 'A' && _event.target.tagName !== 'SPAN' && _event.target.tagName !== 'IMG'&& _event.target.tagName !== 'path'&&_event.target.tagName !== 'svg') {
            _event.preventDefault()
            const url = `${'/cash-sales/' + order.id}`;
            this.props.history.push(url)
          }
        }
      }
    }

    if (tableMini) {
      if (orders.length > 7) {
        return (
          <MultiBackSortTable
            scroll={{ y: 627 }}
            pagination={false}
            // components={components}
            columns={this.miniColumns}
            dataSource={orders.splice(0, 7)}
            rowKey="id"
            css={tableCss(false)}
            loading={this.props.loading}
            onChange={this.handleTableChange}
            onSort={this.handleTableChange}
          />
        )
      } else {
        return (
          <MultiBackSortTable
            scroll={{ y: 627 }}
            pagination={false}
            // components={components}
            columns={this.miniColumns}
            dataSource={orders}
            rowKey="id"
            css={tableCss(false)}
            loading={this.props.loading}
            onChange={this.handleTableChange}
            onSort={this.handleTableChange}
          />
        )
      }
    } else {
      return (
        <MultiBackSortTable
          pagination={{
            total: total,
            pageSize: pageSize,
            current: currentPage,
            defaultCurrent: 0
          }}
          // components={components}
          columns={orderColumns}
          dataSource={orders}
          rowKey="id"
          css={tableCss(false)}
          onRow={onRow}
          loading={this.props.loading}
          onChange={this.handleTableChange}
          onSort={this.handleTableChange}
        />
      )
    }
  }

  private wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}
