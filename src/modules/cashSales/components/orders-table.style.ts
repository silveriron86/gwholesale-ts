import styled from '@emotion/styled'
import { black } from '~/common'
import nsImage from '~/modules/orders/components/netsuite.svg'

export const Price = styled('span')({
  fontFamily: '"Museo Sans Rounded"',
  color: black,
  fontSize: '18px',
})

export const NSImage: any = styled(nsImage)((props) => ({
  width: '24px',
  height: '24px',
}))

export const QBOImage: any = styled('img')((props) => ({
  width: '24px',
  height: '24px',
  backgroundColor: props.theme.primary,
  borderRadius: 12,
}))

export const EmptyTr = styled('tr')({
  height: '15px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})
