import React, { useEffect, useState, memo } from 'react'
import { OrderItem } from '~/schema'
import { DragItem, Flex } from '../../styles'
import { Icon as IconSVG } from '~/components'
import { ThemeCheckbox } from '~/modules/customers/customers.style'
import { formatNumber } from '~/common/utils'

const styles = {
  // display: 'inline-block',
  transform: 'rotate(-7deg)',
  WebkitTransform: 'rotate(-7deg)',
  transformOrigin: 'left',
  cursor: 'grabbing'
}

export interface BoxDragPreviewProps {
  record: OrderItem
  selectedRowIndex: number
}

export interface BoxDragPreviewState {
  tickTock: any
}

export const BoxDragPreview: React.FC<BoxDragPreviewProps> = memo(
  ({ record, selectedRowIndex }) => {
    const [tickTock, setTickTock] = useState(false)

    useEffect(
      function subscribeToIntervalTick() {
        const interval = setInterval(() => setTickTock(!tickTock), 500)
        return () => clearInterval(interval)
      },
      [tickTock],
    )

    return (
      <div style={styles}>
        <Flex>
          <DragItem className='product-name'>
            <Flex>
              <div><IconSVG type='draggable' viewBox='0 0 20 24' width={16} height={20} /></div>
              <ThemeCheckbox checked={selectedRowIndex != -1} style={{ margin: '0 8px' }} />
              <span>{record.variety}</span>
            </Flex>
          </DragItem>
          <DragItem className='lotId'>
            {record.lotId}
          </DragItem>
          <DragItem className='uom'>
            {formatNumber(record.quantity, 2)}/{record.inventoryUOM}
          </DragItem>
          <DragItem className='quoted-price'>
            {formatNumber(record.price, 2)}/{record.baseUOM}
          </DragItem>
        </Flex>
      </div>
    )
  },
)
