import * as React from 'react'
import { Icon, Select, DatePicker } from 'antd'
import { FlexDiv, Item, PaddingContainer } from '../../styles'
import { BlinkBorderText, DetailLabel } from '~/modules/customers/sales/_style'
import { ShippingPopoverModal } from '~/modules/vendors/purchase-orders/_components/shipping-popover'
import { datePickerEscKeyupEvent } from '~/common/jqueryHelper'
import { ThemeIcon, ThemeSelect } from '~/modules/customers/customers.style'
import moment from 'moment'
import { formatAddress } from '~/common/utils'

const calendarIcon = <ThemeIcon type="calendar" />

type OrderInfoProps = {}
// 这个文件没有用到  2021-11-01
export class HeaderOrderDetail extends React.PureComponent<OrderInfoProps> {
  state: any
  quotedDatePicker:
    | string
    | ((instance: ClassicComponent<DatePickerProps, any> | null) => void)
    | RefObject<ClassicComponent<DatePickerProps, any>>
    | null
    | undefined
  constructor(props: OrderInfoProps) {
    super(props)
    const { order } = props
    this.state = {
      isCollapsed: true,
      orderDatePicker: false,
      quotedDatePicker: false,
      scheduledDatePicker: false,
      shippingModalVisible: false,
      purchaser: order.purchaser ? order.purchaser.userId : null,
    }
  }

  componentDidMount() {
    const handleEscClick = (event: any) => {
      if (event.keyCode == 27) {
        this.setState({
          orderDatePicker: false,
          quotedDatePicker: false,
          scheduledDatePicker: false,
        })
      }
    }

    datePickerEscKeyupEvent(handleEscClick)
  }

  handleCollapse = () => {
    this.setState({ isCollapsed: !this.state.isCollapsed })
  }

  handleShippingVisibleChange = () => {
    this.setState({
      shippingModalVisible: !this.state.shippingModalVisible,
      accountNames: [],
    })
  }

  onDatePickerOpenChange = (type: number, open: boolean) => {
    switch (type) {
      case 1:
        this.setState({ orderDatePicker: open })
        break
      case 2:
        this.setState({ quotedDatePicker: open })
        break
      case 3:
        this.setState({ scheduledDatePicker: open })
        break
    }
  }

  onDateChange = (type, _dateMoment, _dataString) => {
    const { currentOrder } = this.props
    if (currentOrder && _dataString != null) {
      let data = {}
      data['wholesaleOrderId'] = currentOrder.wholesaleOrderId
      if (type == 1) data['orderDate'] = _dataString
      if (type == 2) data['quotedDate'] = _dataString
      if (type == 3) data['deliveryDate'] = _dataString
      this.props.updateOrderInfo({ ...data, cashSale: true })
    }
  }

  render() {
    const { order, sellerSetting, currentCompanyUsers } = this.props
    const { isCollapsed, purchaser } = this.state

    const orderDate = order.orderDate ? moment.utc(order.orderDate) : moment()
    const quotedDate = order.quotedDate ? moment.utc(order.quotedDate) : moment()
    const deliveryDate = order.deliveryDate ? moment.utc(order.deliveryDate) : moment()

    const sellerAddress =
      sellerSetting && sellerSetting.userSetting && sellerSetting.userSetting.company
        ? sellerSetting.userSetting.company.address
        : null

    const address =
      order.shippingAddress && order.shippingAddress.address && order.shippingAddress.address
        ? order.shippingAddress.address
        : sellerAddress
    const destinationName = address ? formatAddress(address) : ''

    return (
      <PaddingContainer style={{ borderBottom: 0 }}>
        <FlexDiv style={{ padding: '4px 0 8px 50px' }}>
          <Item className="left icon-item">
            <Icon type={isCollapsed ? 'caret-up' : 'caret-down'} onClick={this.handleCollapse} />
            {isCollapsed ? 'Show' : 'Hide'} Order Details
          </Item>
        </FlexDiv>
        {!isCollapsed && (
          <>
            <FlexDiv style={{ paddingBottom: 10, paddingLeft: 50 }}>
              <div style={{ width: 248, marginRight: 16 }}>
                <DetailLabel className="text-left">PURCHASER</DetailLabel>
                {currentCompanyUsers && (
                  <ThemeSelect
                    defaultValue={purchaser}
                    // onChange={this.handleChangeValue.bind(this, 'purchaser')}
                    style={{ width: '100%' }}
                  >
                    {currentCompanyUsers.map(
                      (
                        item: { userId: string | number | undefined; firstName: string; lastName: string },
                        index: string | number | undefined,
                      ) => {
                        return (
                          <Select.Option key={index} value={item.userId}>
                            {item.firstName + '  ' + item.lastName}
                          </Select.Option>
                        )
                      },
                    )}
                  </ThemeSelect>
                )}
              </div>
              <div style={{ width: 360, marginRight: 30 }}>
                <DetailLabel className="text-left">DELIVERY ADDRESS</DetailLabel>
                <ShippingPopoverModal
                  order={order}
                  visible={this.state.shippingModalVisible}
                  onVisibleChange={this.handleShippingVisibleChange}
                >
                  <BlinkBorderText value={destinationName} className="black-bottom bold-blink" />
                </ShippingPopoverModal>
              </div>
              <div className="inputDisabled" style={{ marginRight: 30 }}>
                <DetailLabel className="text-left">ORDER DATE</DetailLabel>
                <DatePicker
                  // disabledDate={this.disabledDate}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={calendarIcon}
                  value={orderDate}
                  onChange={this.onDateChange.bind(this, 1)}
                  className="no-border black-bottom bold-blink"
                  style={{ width: 180, display: 'block' }}
                  // disabled
                  open={this.state.orderDatePicker}
                  onOpenChange={this.onDatePickerOpenChange.bind(this, 1)}
                  allowClear={false}
                />
              </div>
              <div style={{ marginRight: 30 }}>
                <DetailLabel className="text-left">QUOTED DELIVERY DATE</DetailLabel>
                <DatePicker
                  // disabledDate={this.disabledDate}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={calendarIcon}
                  value={quotedDate}
                  onChange={this.onDateChange.bind(this, 2)}
                  className="no-border black-bottom bold-blink"
                  style={{ width: 180, display: 'block' }}
                  open={this.state.quotedDatePicker}
                  onOpenChange={this.onDatePickerOpenChange.bind(this, 2)}
                  ref={this.quotedDatePicker}
                  allowClear={false}
                />
              </div>
              <div>
                <DetailLabel className="text-left">SCHEDULED DELIVERY DATE</DetailLabel>
                <DatePicker
                  // disabledDate={this.disabledDate}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={calendarIcon}
                  value={deliveryDate}
                  onChange={this.onDateChange.bind(this, 3)}
                  className="no-border black-bottom bold-blink"
                  style={{ width: 180, display: 'block' }}
                  open={this.state.scheduledDatePicker}
                  onOpenChange={this.onDatePickerOpenChange.bind(this, 3)}
                  allowClear={false}
                />
              </div>
              {/* <Col md={3}>
                <div style={{ width: '90%' }}>
                  <DetailLabel>ORDER #</DetailLabel>
                  <BlinkBorderText
                    value={order ? order.wholesaleOrderId : ''}
                    readOnly
                    className="black-bottom bold-blink"
                  />
                </div>
              </Col> */}
            </FlexDiv>
          </>
        )}
      </PaddingContainer>
    )
  }
}

export default HeaderOrderDetail
