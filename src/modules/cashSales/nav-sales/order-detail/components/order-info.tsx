import { Icon, Radio, Select, Tooltip } from 'antd'
import TextArea from 'antd/lib/input/TextArea'
import { cloneDeep } from 'lodash'
import * as React from 'react'
import { formatAddress, formatDate } from '~/common/utils'
import { flexStyle, ThemeRadio, ThemeSelect, ThemeTextArea } from '~/modules/customers/customers.style'
import { OrderDetail, MainAddress } from '~/schema'
import {
  FlexDiv,
  Item,
  OrderInfoItem,
  PaddingContainer,
  ValueLabel,
  InputLabel,
  ItemMarginTop8,
  ItemMarginTop25,
  ItemHMargin4,
  ItemHMargin40
} from './../../styles'
import jQuery from 'jquery'

type OrderInfoProps = {
  loadingCurrentOrder: boolean
  loadingAddress: boolean
  currentOrder: OrderDetail
  addresses: MainAddress[]
  updateOrderInfo: Function
}
export class OrderInfo extends React.PureComponent<OrderInfoProps> {
  state: any
  orderHeaderInputId = localStorage.getItem("CASH-SALE-HEADER-INPUT-ID")
  constructor(props: OrderInfoProps) {
    super(props)
    const { currentOrder } = this.props
    this.state = {
      currentOrderId: 0,
      isCollapsed: this.orderHeaderInputId == '2' || this.orderHeaderInputId == '3' ? false : true,
      billingAddresses: [],
      shippingAddresses: [],
      // deliveryInstruction: currentOrder ? currentOrder.deliveryInstruction : '',
      pickerNote: currentOrder ? currentOrder.pickerNote : '',
      customerNote: currentOrder ? currentOrder.customerNote : '',
    }
  }

  componentDidMount() {
    this.focusWhenComponentMounted()
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingAddress === true && nextProps.loadingAddress === false) {
      this.initHeader(nextProps.addresses, nextProps.currentOrder)
    }
  }

  focusWhenComponentMounted = () => {
    const orderHeaderInputId = localStorage.getItem("CASH-SALE-HEADER-INPUT-ID")
    if (orderHeaderInputId) {
      const headerInputFields = jQuery('.header-input-field input, input.header-input-field, textarea.header-input-field')
      const index = parseInt(orderHeaderInputId)
      if (index == 2 || index == 3) {
        jQuery.each(headerInputFields, (i, el) => {
          if (jQuery(el).data("id") == index) {
            jQuery(el).trigger('focus')
            return
          }
        })
      }
    }
  }

  handleFocusIn = () => {
    jQuery('.header-input-field input, input.header-input-field, textarea.header-input-field').unbind().bind('focus', function (e) {
      const id = jQuery(e.target).data('id')
      localStorage.setItem("CASH-SALE-HEADER-INPUT-ID", `${id}`)
    })
  }


  onRadioChange = (e: any) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      pickup: e.target.value,
      cashSale: true
    }
    this.props.updateOrderInfo(data)
  }

  handleChange = (type: string, e: any) => {
    const state = { ...this.state }
    const { value } = e.target
    state[type] = value
    this.setState(state)
  }

  handleBlur = (type: string, e: any) => {
    let data: any = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId
    }
    if (type == 'pickerNote' || type == 'customerNote') {
      data = { ...data, showNotif: false }
      data[type] = this.state[type]
      this.props.updateOrderInfo({ ...data, cashSale: true })
    }
  }


  onAddressChange = (type: string, wholesaleAddressId: number) => {
    let data: any = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
    }
    if (type == 'billing' && wholesaleAddressId > 0) {
      data.billingAddressId = wholesaleAddressId
    } else if (type == 'shipping' && wholesaleAddressId > 0) {
      data.shippingAddressId = wholesaleAddressId
    }
    this.props.updateOrderInfo({ ...data, cashSale: true })
  }

  initHeader = (addresses: any[], currentOrder: OrderDetail) => {
    const _billingAddresses: MainAddress[] = []
    const _shippingAddresses: MainAddress[] = []

    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        _shippingAddresses.push(address)
      } else if (address.addressType === 'BILLING') {
        _billingAddresses.push(address)
      }
    })

    this.setState({
      currentOrderId: currentOrder ? currentOrder.wholesaleOrderId : 0,
      billingAddresses: cloneDeep(_billingAddresses),
      shippingAddresses: cloneDeep(_shippingAddresses),
      // deliveryInstruction: currentOrder ? currentOrder.deliveryInstruction : '',
      pickerNote: currentOrder ? currentOrder.pickerNote : '',
      customerNote: currentOrder ? currentOrder.customerNote : '',
    })
  }

  handleCollapse = () => {
    this.setState({ isCollapsed: !this.state.isCollapsed }, () => {
      this.handleFocusIn()
    })
  }

  render() {
    const { currentOrder } = this.props
    const { isCollapsed, billingAddresses, shippingAddresses, pickerNote, customerNote } = this.state
    const disabled = !currentOrder || currentOrder.wholesaleOrderStatus === 'CANCEL'

    return (
      <PaddingContainer style={{ borderBottom: 0 }}>
        <FlexDiv style={{ padding: '4px 0 8px' }}>
          <Item className='left icon-item'>
            <Icon type={isCollapsed ? 'caret-up' : 'caret-down'} onClick={this.handleCollapse} />
            {isCollapsed ? "Show" : "Hide"} Order Details
          </Item>
        </FlexDiv>
        {!isCollapsed &&
          <>
            <FlexDiv>
              <Item className='left' style={{ width: 130 }}>
                <InputLabel>Created Date</InputLabel>
                <ValueLabel className="small">{currentOrder && formatDate(currentOrder.createdDate)}</ValueLabel>
              </Item>
              <Item className='left' style={{ width: 100 }}>
                <InputLabel>Sales Rep.</InputLabel>
                <ValueLabel className="small">{currentOrder && currentOrder.seller ? currentOrder.seller.firstName + ' ' + currentOrder.seller.lastName : 'N/A'}</ValueLabel>
              </Item>
              {/* <Item style={ItemHMargin40}>
            <InputLabel>Net Terms</InputLabel>
            <ValueLabel>Net 7</ValueLabel>
          </Item> */}
              <OrderInfoItem style={ItemHMargin40}>
                <InputLabel>Billing Address</InputLabel>
                <ThemeSelect
                  onChange={this.onAddressChange.bind(this, 'billing')}
                  style={{ width: '100%' }}
                  disabled={disabled}
                  value={currentOrder && billingAddresses.length && currentOrder.billingAddress ? currentOrder.billingAddress.wholesaleAddressId : ''}>
                  {billingAddresses.map((el: MainAddress, index: number) => {
                    if (el.address) {
                      return <Select.Option key={index} value={el.wholesaleAddressId}>{formatAddress(el.address)}</Select.Option>
                    }
                  })
                  }
                </ThemeSelect>
              </OrderInfoItem>
              <OrderInfoItem style={ItemHMargin40}>
                <InputLabel>Shipping Address</InputLabel>
                <ThemeSelect
                  onChange={this.onAddressChange.bind(this, 'shipping')}
                  style={{ width: '100%' }}
                  value={currentOrder && shippingAddresses.length && currentOrder.shippingAddress ? currentOrder.shippingAddress.wholesaleAddressId : ''}
                  disabled={disabled}
                >
                  {shippingAddresses.map((el: MainAddress, index: number) => {
                    if (el.address) {
                      return <Select.Option key={index} value={el.wholesaleAddressId}>{formatAddress(el.address)}</Select.Option>
                    }
                  })
                  }
                </ThemeSelect>
                <div style={ItemMarginTop8}>
                  <Radio.Group onChange={this.onRadioChange} value={currentOrder ? currentOrder.pickup : false} disabled={disabled}>
                    <ThemeRadio value={false}>DELIVERY</ThemeRadio>
                    <ThemeRadio value={true}>PICKUP</ThemeRadio>
                  </Radio.Group>
                </div>
              </OrderInfoItem>
            </FlexDiv>
            <FlexDiv style={ItemMarginTop8}>
              <OrderInfoItem className='left'>
                <InputLabel style={flexStyle}>Internal notes<Tooltip placement="top" title="This note will be displayed on the Pick Sheet. It will not be displayed on the Delivery List."><Icon style={ItemHMargin4} type='info-circle' /></Tooltip></InputLabel>
                <ThemeTextArea
                  data-id={2}
                  className='note-text header-input-field'
                  rows={4}
                  value={pickerNote}
                  onChange={this.handleChange.bind(this, 'pickerNote')}
                  onBlur={this.handleBlur.bind(this, 'pickerNote')}
                  disabled={disabled} />
                {/* <ThemeSelect defaultValue={1} style={{ width: '100%' }}>
              <Select.Option value={1}>Double-check packages</Select.Option>
            </ThemeSelect>
            <div style={ItemMarginTop8}>
              "Please be sure to double-check packages before loading them onto the truck. Lorem Ipusum..."
            </div> */}
              </OrderInfoItem>
              <OrderInfoItem>
                <InputLabel style={flexStyle}>Note for Customer<Tooltip placement="top" title="This note will be displayed on the Delivery List. It will not be displayed on the Pick Sheet."><Icon style={ItemHMargin4} type='info-circle' /></Tooltip></InputLabel>
                <ThemeTextArea
                  data-id={3}
                  className='note-text header-input-field'
                  rows={4}
                  value={customerNote}
                  onChange={this.handleChange.bind(this, 'customerNote')}
                  onBlur={this.handleBlur.bind(this, 'customerNote')}
                  disabled={disabled} />
              </OrderInfoItem>
              {/* <OrderInfoItem>
                <InputLabel style={flexStyle}>Delivery Instructions<Tooltip placement="top" title="This note will be displayed on the Delivery List. It will not be displayed on the Pick Sheet."><Icon style={ItemHMargin4} type='info-circle' /></Tooltip></InputLabel>
                <ThemeTextArea
                  className='note-text'
                  rows={4}
                  value={deliveryInstruction}
                  disabled={currentOrder && currentOrder.wholesaleOrderStatus == 'CANCEL'}
                  onBlur={this.handleBlur.bind(this, 'deliveryInstruction')}
                  onChange={this.handleChange.bind(this, 'deliveryInstruction')}
                />
              </OrderInfoItem> */}
            </FlexDiv>
          </>
        }
        {/* <AlignLeftDiv style={ItemMarginTop8} className='dark-theme-color bold'>
          <Icon type='plus' style={ItemHMargin4} />
          Add Another Note
        </AlignLeftDiv> */}
      </PaddingContainer>
    )
  }
}

export default OrderInfo
