import * as React from 'react'
import { AutoComplete, DatePicker, Icon, Menu, Dropdown, Select, Tooltip } from 'antd'
import moment from 'moment'
import {
  ThemeIcon,
  ThemeButton,
  ThemeOutlineButton,
  ThemeInput,
  ThemeCheckbox,
  ThemeSelect,
} from '~/modules/customers/customers.style'
import {
  BalanceLabel,
  Flex,
  FlexDiv,
  Item,
  ItemHMargin4,
  OrderInfoItem,
  OrderStatusLabel,
  PaddingContainer,
  ValueLabel,
  InputLabel,
  VerticalPadding12,
  HeaderActionMenu,
  CustomerName,
  WarningText,
} from './../../styles'
import { OrderItem, OrderDetail, UserRole } from '~/schema'
import { Moment } from 'moment'
import {
  calculateTotalOrder,
  formatNumber,
  getFulfillmentDate,
  getOrderPrefix,
  formatOrderStatus,
  responseHandler,
  checkError,
  formatTimeDifference,
} from '~/common/utils'
import { Icon as IconSvg } from '~/components/icon/'
import { Link } from 'react-router-dom'
import { CACHED_ACCOUNT_TYPE, CACHED_QBO_LINKED, CACHED_USER_ID, CACHED_COMPANY, gray01 } from '~/common'
import { CACHED_NS_LINKED } from '~/common'
import { history } from '~/store/history'
import { NewOrderFormModal } from '~/modules/cashSales/components'
import { connect } from 'react-redux'
import { OrderService } from '~/modules/orders/order.service'
import { of } from 'rxjs'
import _ from 'lodash'

type HeaderProps = {
  loading: boolean
  loadingCurrentOrder: boolean
  orderItems: OrderItem[]
  oneOffItems: OrderItem[]
  currentOrder: OrderDetail
  resetLoading: Function
  syncQBOOrder: Function
  syncNSOrder: Function
  duplicateOrder: Function
  updateOrderInfo: Function
  onDoHeaderActions: Function
  printSetting: any
  sellerSetting: any
  customers: any[]
  cashCustomer: any[]
  getCashCustomerClient: Function
  getReferenceList: Function
  cashReferences: string[]
  simplifyCustomers: []
  settingCompanyName: string
}

export class OrderSummary extends React.PureComponent<HeaderProps> {
  state = {
    orderDate: undefined,
    deliveryDate: undefined,
    reference: '',
    showCustomerModal: false,
    isReferenceDuplicate: false,
  }

  componentDidMount() {
    if (!this.props.cashCustomer.length) {
      this.props.getCashCustomerClient(this.props.currentUser.userId)
    }
    this.props.getReferenceList()
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      if (nextProps.currentOrder && !this.state.deliveryDate) {
        this.setState({
          orderDate: nextProps.currentOrder.orderDate,
          deliveryDate: nextProps.currentOrder.deliveryDate,
          reference: nextProps.currentOrder.reference,
        })
      }
    }
    if (nextProps.currentOrder) {
      this.setState({
        orderStatus: nextProps.currentOrder.wholesaleOrderStatus,
        financialTerms: nextProps.currentOrder.financialTerms,
      })
    }

    if (nextProps.currentOrder) {
      if ( !this.props.currentOrder || (JSON.stringify(this.props.currentOrder) !== JSON.stringify(nextProps.currentOrder)) || (JSON.stringify(this.props.cashReferences) !== JSON.stringify(nextProps.cashReferences)) ) {
          this.onReferenceChange({
            target: {
              value: nextProps.currentOrder.reference
            }
          }, nextProps)
      }
    }
  }

  onOrderDateChange = (date: Moment | null, dateString: string) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      orderDate: dateString,
      showNotif: false,
      cashSale: true
    }

    this.props.updateOrderInfo(data)
    this.setState({ orderDate: dateString })
  }

  onFulfillmentDateChange = (date: Moment | null, dateString: string) => {
    const today = moment().format('MM/DD/YYYY')
    let data: any = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      deliveryDate: dateString,
      cashSale: true,
      showNotif: false,
    }

    if(moment(today) > moment(dateString)) {
      data = {...data, orderDate: dateString}
    }

    if (this.props.sellerSetting.company.defaultLastUsedDateEnabled === true) {
      localStorage.setItem('LAST_USED_TARGET_FULFILLMENT_DATE', dateString)
    }

    this.props.updateOrderInfo(data)
    this.setState({ deliveryDate: dateString, orderDate: moment(today) > moment(dateString) ? dateString : this.state.orderDate })
  }

  onReferenceChange = (e: any, props: any) => {
    const val = e.target.value
    const { cashReferences, currentOrder } = props
    const references = cashReferences
      .filter((el: any) => el.wholesaleOrderId != currentOrder.wholesaleOrderId)
      .map((el: any) => {
        return el.reference
      })
    this.setState({
      reference: val,
      isReferenceDuplicate: references.indexOf(val) > -1 ? true : false,
    })
  }

  onSaveReference = () => {
    const { cashReferences, currentOrder } = this.props
    const { reference } = this.state
    const references = cashReferences
      .filter((el: any) => el.wholesaleOrderId != currentOrder.wholesaleOrderId)
      .map((el: any) => {
        return el.reference
      })
    this.setState(
      {
        isReferenceDuplicate: references.indexOf(reference) > -1 ? true : false,
      },
      () => {
        const data = {
          wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
          reference,
          cashSale: true,
          showNotif: false,
        }
        this.props.updateOrderInfo(data)
      },
    )
  }

  onCashSaleChange = (e: any) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      cashSale: true,
    }
    this.props.updateOrderInfo(data)
  }

  handleSyncQBOOrder = () => {
    const { currentOrder } = this.props
    if (currentOrder) {
      this.props.resetLoading()
      this.props.syncQBOOrder(currentOrder.wholesaleOrderId)
    }
  }

  handleSyncNSOrder = () => {
    const { currentOrder } = this.props
    if (currentOrder) {
      this.props.resetLoading()
      this.props.syncNSOrder(currentOrder.wholesaleOrderId)
    }
  }

  handleDuplicateOrder = () => {
    const { orderItems, currentOrder, oneOffItems, sellerSetting } = this.props

    // var delivery: any = new Date(currentOrder!.deliveryDate)
    // delivery = moment(delivery).add(2, 'days').format('MM/DD/YYYY')
    // var delivery = moment().format('MM/DD/YYYY')
    let fulfillmentDate = getFulfillmentDate(sellerSetting)

    // let newOrderItems = orderItems.concat(oneOffItems)
    // let tempOrderItems = newOrderItems.map((obj) => {
    //   return {
    //     wholesaleItemId: obj.itemId,
    //     note: obj.note,
    //     UOM: obj.UOM,
    //     price: obj.price,
    //     cost: obj.cost,
    //     itemName: obj.itemName,
    //     displayOrder: obj.displayOrder,
    //     pricingUOM: obj.pricingUOM,
    //     ratio: obj.ratio,
    //     pricingLogic: obj.pricingLogic,
    //     pricingGroup: obj.pricingGroup,
    //   }
    // })
    const data = {
      deliveryDate: fulfillmentDate,
      // orderDate: delivery,
      // wholesaleCustomerClientId: currentOrder!.wholesaleClient.clientId,
      // itemList: tempOrderItems,
      wholesaleOrderId: currentOrder!.wholesaleOrderId,
      // cashSale: true,
    }
    this.props.duplicateOrder(data)
  }

  onSelectAction = ({ key }) => {
    const { currentOrder } = this.props
    if (key == 1) {
      this.handleDuplicateOrder()
    } else if (key == 2) {
      // lock order
      this.props.onDoHeaderActions(currentOrder.isLocked ? 'unlock' : 'lock')
    } else if (key == 3) {
      // cancel order
      this.props.onDoHeaderActions('cancel')
    }
  }

  renderQboSyncButton = () => {
    const {
      loading,
      currentOrder: { wholesaleOrderStatus, qboId },
      orderItemByProduct,
      oneOffItems,
      catchWeightValues,
      lastQboUpdate,
    } = this.props
    const totalOrder = calculateTotalOrder(orderItemByProduct, oneOffItems, catchWeightValues)
    const qboRealmId = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    const userId = localStorage.getItem(CACHED_USER_ID)
    const companyId = localStorage.getItem(CACHED_COMPANY)
    const disabled = wholesaleOrderStatus === 'CANCEL' || this.props.updating || totalOrder === '0.00'
    if (qboRealmId) {
      const lastUpdated = new Date(lastQboUpdate)
      const style = { borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }
      return (
        <Flex>
          <Tooltip
            placement={'top'}
            title={totalOrder === '0.00' ? 'You cannot sync an order with $0 total.' : lastQboUpdate ? `Synced to Quickbooks ${formatTimeDifference(lastUpdated)} ago` : ''}
          >
            <ThemeOutlineButton
              disabled={disabled}
              size="large"
              onClick={() => this.handleSyncQBOOrder()}
              loading={loading}
              style={qboId ? style : {}}
              hidden={((companyId == 'Growers Produce' && !(userId == '105629' || userId == '116137' || userId == '110151' || userId == '97960' || userId == '110154' || userId == '110148' || userId == '110149') ||
                (companyId == 'PayLess Logistics' && !(userId == '105120' || userId == '105165' || userId == '105159' || userId == '105160'))))}
            >
              <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
              {qboId ? 'Re-' : ''}Sync to QBO
            </ThemeOutlineButton>
          </Tooltip>
          {qboId && (
            <Dropdown
              overlay={
                qboId.includes("{") ?
                  (
                    this.renderQboContainerInvoices(qboId)
                  ) : (
                    <HeaderActionMenu>
                      <Menu.Item key="1">
                        <a href={`https://qbo.intuit.com/app/invoice?txnId=${qboId}`} target="_blank">
                          Open In Quickbooks
                        </a>
                      </Menu.Item>
                    </HeaderActionMenu>
                  )}
              trigger={['click']}
              placement="bottomRight"
            >
              <ThemeOutlineButton
                size="large"
                type="primary"
                style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
              >
                <Icon type="down" />
              </ThemeOutlineButton>
            </Dropdown>
          )}
        </Flex>
      )
    }
    return <span />
  }

  renderQboContainerInvoices = (qboId: string) => {
    const qboIds = JSON.parse(qboId)
    var i = 1;
    var rows = [];
    for (const k in qboIds) {
      // note: we are adding a key prop here to allow react to uniquely identify each
      // element in this array. see: https://reactjs.org/docs/lists-and-keys.html
      rows.push(
        <Menu.Item key={i}>
          <a href={`https://qbo.intuit.com/app/invoice?txnId=${qboIds[k]}`} target="_blank">
            Open QBO Invoice {qboIds[k]}
          </a>
        </Menu.Item>
      );
      i++
    }
    return (
      <HeaderActionMenu>
        {rows}
      </HeaderActionMenu>
    )
  }

  renderNsSyncButton = () => {
    const {
      loading,
      currentOrder: { wholesaleOrderStatus },
    } = this.props
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    if (nsRealmId)
      return (
        <ThemeOutlineButton
          disabled={wholesaleOrderStatus === 'CANCEL'}
          size="large"
          onClick={() => this.handleSyncNSOrder()}
          loading={loading}
        >
          <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
          Sync to NS
        </ThemeOutlineButton>
      )
    return <span />
  }

  onChangeCustomer = (selectedCustomer: any, actionType: string) => {
    this.setState({ showCustomerModal: false }, () => {
      if (selectedCustomer && actionType == 'ok') {
        const data = {
          wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
          wholesaleClientId: selectedCustomer,
          cashSale: true,
        }
        this.props.updateOrderInfo(data)
      }
    })
  }

  handleChangeFinancialTerms = (financialTerms: string) => {
    const { currentOrder, updateOrderInfo } = this.props
    updateOrderInfo({
      wholesaleOrderId: currentOrder.wholesaleOrderId,
      financialTerms,
    })
  }

  handleCreateNewCashSale = () => {
    const { sellerSetting, currentUser, history } = this.props
    const quotedDate = moment(new Date()).format('MM/DD/YYYY')
    const fulfillmentDate = getFulfillmentDate(sellerSetting)

    OrderService.instance.getCashCustomerClient({ userId: currentUser.userId }).subscribe({
      next(res: any) {
        const data = {
          wholesaleClientId: String(res.body.data.clientId),
          orderDate: quotedDate,
          quotedDate: quotedDate,
          deliveryDate: fulfillmentDate,
          itemList: [],
          cashSale: true,
        }
        OrderService.instance.createOrder(data, currentUser.userId).subscribe({
          next(res: any) {
            of(responseHandler(res, false).body.data)
            localStorage.setItem('NEW_CASH_SALE', 'true')
            history.push(`/cash-sales/${res.body.data.wholesaleOrderId}/`)
          },
          error(err) {
            checkError(err)
          },
        })
      },
    })
  }

  showInput = (order: any) => {
    const wholesaleOrderStatus = order ? order.wholesaleOrderStatus : ''
    if (wholesaleOrderStatus == 'SHIPPED') {
      return true
    } else {
      return false
    }
  }

  render() {
    const {
      currentOrder,
      loading,
      printSetting,
      sellerSetting,
      simplifyCustomers,
      cashCustomer,
      companyProductTypes,
    } = this.props
    const { orderDate, deliveryDate, reference, isReferenceDuplicate, financialTerms } = this.state
    const paymentTerms = [
      ..._.get(companyProductTypes, 'paymentTerms', []),
      ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
    ]
    const orderTotal = currentOrder
      ? currentOrder.totalPrice + (currentOrder.shippingCost ? currentOrder.shippingCost : 0)
      : 0
    const userPrintSetting = JSON.parse(printSetting)
    const orderDisabled =
      !currentOrder ||
      (currentOrder &&
        (currentOrder.wholesaleOrderStatus == 'CANCEL' || currentOrder.wholesaleOrderStatus == 'SHIPPED'))

    let shippingAddress = ''
    if (currentOrder && currentOrder.wholesaleClient && currentOrder.wholesaleClient.mainShippingAddress) {
      const { street1, city, state } = currentOrder.wholesaleClient.mainShippingAddress.address
      shippingAddress = `${street1}, ${city}, ${state}`
    }
    const redirectToCustomer = currentOrder
      ? window.location.origin + window.location.pathname + `#/customer/${currentOrder.wholesaleClient.clientId}/orders`
      : ''

    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)
    const disabled= !currentOrder || currentOrder.wholesaleOrderStatus === 'CANCEL'

    return (
      <>
        <Flex
          className="v-center"
          style={{
            ...VerticalPadding12,
            justifyContent: 'space-between',
            position: 'fixed',
            marginLeft: -40,
            width: '100%',
            height: 60,
            backgroundColor: this.props.theme.theme,
            zIndex: 1000,
          }}
        >
          <FlexDiv>
            <Item className="left icon-item">
              <a href={`#/cash-sales`} style={{ paddingLeft: 82, color: 'white' }}>
                <ThemeIcon type="arrow-left" viewBox="0 0 20 20" width={13} height={11} style={{ color: 'white' }} />
                <ValueLabel>Cash Sales</ValueLabel>
              </a>
            </Item>
          </FlexDiv>
          <FlexDiv>
            <Item className="header-btns">
              {currentOrder && currentOrder.parentOrder == null ? (
                <span>
                  {this.renderQboSyncButton()}
                  {this.renderNsSyncButton()}
                </span>
              ) : (
                <span />
              )}
            </Item>
            {userPrintSetting &&
              (typeof userPrintSetting.pick_enabled === 'undefined' || userPrintSetting.pick_enabled) && (
                <Item className="header-btns right">
                  <ThemeOutlineButton
                    size="large"
                    onClick={() => this.props.onDoHeaderActions('view_pick')}
                    disabled={disabled}
                  >
                    View{' '}
                    {userPrintSetting &&
                    typeof userPrintSetting.pick_title !== 'undefined' &&
                    userPrintSetting.pick_title
                      ? userPrintSetting.pick_title
                      : 'Pick Sheet'}
                  </ThemeOutlineButton>
                </Item>
              )}
            {userPrintSetting &&
              (typeof userPrintSetting.bill_enabled === 'undefined' || userPrintSetting.bill_enabled) && (
                <Item className="header-btns right">
                  <ThemeOutlineButton
                    size="large"
                    style={{ textTransform: 'capitalize' }}
                    onClick={() => this.props.onDoHeaderActions('view_bill')}
                    disabled={disabled}
                  >
                    View{' '}
                    {userPrintSetting && userPrintSetting.bill_title ? userPrintSetting.bill_title : 'Bill of Lading'}
                  </ThemeOutlineButton>
                </Item>
              )}
            {userPrintSetting &&
              (typeof userPrintSetting.invoice_enabled === 'undefined' || userPrintSetting.invoice_enabled) && (
                <Item className="header-btns right">
                  <ThemeOutlineButton
                    size="large"
                    style={{ textTransform: 'capitalize' }}
                    onClick={() => this.props.onDoHeaderActions('view_invoice')}
                    disabled={disabled}
                  >
                    View{' '}
                    {userPrintSetting && userPrintSetting.invoice_title ? userPrintSetting.invoice_title : 'Invoice'}
                  </ThemeOutlineButton>
                </Item>
              )}
            <Item className="header-btns right">
              <ThemeButton
                className="icon enter-purchase"
                size="large"
                type="primary"
                onClick={this.handleCreateNewCashSale}
                style={{marginLeft: 0}}
              >
                <Icon type="plus-circle" style={{ fontSize: 20 }} />
                New Cash Sale
              </ThemeButton>
            </Item>
            {/* <Item className="right">
              <ThemeOutlineButton shape="round" onClick={() => this.handleDuplicateOrder()}>
                Duplicate Order
              </ThemeOutlineButton>
            </Item> */}
            {currentOrder !== null && (
              <Dropdown
                overlay={
                  <HeaderActionMenu onClick={this.onSelectAction}>
                    <Menu.Item key="1">
                      <IconSvg
                        type="duplicate"
                        viewBox={void 0}
                        style={{ width: 24, height: 24, marginRight: 4, fill: 'transparent' }}
                      />
                      Duplicate Order
                    </Menu.Item>
                    {/* <Menu.Item key="2">
                      <IconSvg
                        type={currentOrder.isLocked ? 'unlock' : 'lock'}
                        viewBox="0 0 440 440"
                        style={{ width: 20, height: 20, fill: this.props.theme.theme, marginRight: 6, marginLeft: 2 }}
                      />
                      {currentOrder.isLocked ? 'UnLock' : 'Lock'} Order
                    </Menu.Item> */}
                    <Menu.Item
                      key="3"
                      disabled={disabled || currentOrder.isLocked}
                    >
                      <Icon
                        type="close"
                        style={{ fontSize: 18, color: this.props.theme.theme, marginLeft: 3, marginRight: 7 }}
                      />
                      Cancel Order
                    </Menu.Item>
                  </HeaderActionMenu>
                }
                placement="bottomLeft"
                trigger={['click']}
              >
                <IconSvg type="more" viewBox={void 0} style={{ width: 24, height: 24, margin: '9px 24px 5px 12px' }} />
              </Dropdown>
            )}
          </FlexDiv>
        </Flex>
        <PaddingContainer style={{ paddingTop: 76, paddingBottom: 24 }}>
          <FlexDiv className="space-between">
            <FlexDiv>
              <OrderInfoItem className="left">
                {/* <InputLabel style={{ position: 'relative' }}>Account Name</InputLabel> */}
                <CustomerName style={{ paddingTop: 8 }}>
                  <ValueLabel
                    className="link-text"
                    // onClick={() => history.push(`/customer/${currentOrder.wholesaleClient.clientId}/orders`)}
                  >
                    <a href={redirectToCustomer}>
                      {currentOrder && currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
                        ? currentOrder.wholesaleClient.clientCompany.companyName
                        : ''}
                    </a>
                  </ValueLabel>
                  <IconSvg
                    type="edit"
                    viewBox="0 0 18 18"
                    width={16}
                    height={16}
                    className={orderDisabled ? 'disabled' : 'enabled'}
                    onClick={() => {
                      this.setState({ showCustomerModal: true })
                    }}
                  />
                  {this.state.showCustomerModal && (
                    <NewOrderFormModal
                      visible={this.state.showCustomerModal}
                      onOk={(e) => this.onChangeCustomer(e, 'ok')}
                      onCancel={(e) => this.onChangeCustomer(e, 'close')}
                      clients={simplifyCustomers}
                      okButtonName="Change Customer"
                      isNewOrderFormModal={false}
                      defaultClientId={currentOrder ? currentOrder.wholesaleClient.clientId : 'No Client'}
                    />
                  )}
                </CustomerName>
                {shippingAddress && <ValueLabel className="small black">{shippingAddress}</ValueLabel>}
                {currentOrder && currentOrder.wholesaleClient && accountType !== UserRole.WAREHOUSE && (
                  <ValueLabel className="small black">
                    Customer balance: ${formatNumber(currentOrder.wholesaleClient.openBalance, 2)}{' '}
                    {currentOrder.wholesaleClient.overdue > 0 && (
                      <span>
                        (<span className="red">${formatNumber(currentOrder.wholesaleClient.overdue, 2)}</span> overdue)
                      </span>
                    )}
                  </ValueLabel>
                )}
              </OrderInfoItem>
              <Item>
                <InputLabel>Order Date</InputLabel>
                <DatePicker  style={{width: '142px'}}
                  data-id={0}
                  className="header-input-field"
                  allowClear={false}
                  onChange={this.onOrderDateChange}
                  value={orderDate ? moment(orderDate) : undefined}
                  placeholder="MM/DD/YYYY"
                  format="MM/DD/YYYY"
                  disabled={disabled}
                  suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                />
              </Item>
              <Item>
                <InputLabel>Target Fulfillment Date</InputLabel>
                <DatePicker
                  data-id={0}
                  className="header-input-field"
                  allowClear={false}
                  onChange={this.onFulfillmentDateChange}
                  value={deliveryDate ? moment(deliveryDate) : undefined}
                  placeholder="MM/DD/YYYY"
                  format="MM/DD/YYYY"
                  disabled={disabled}
                  suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                />
              </Item>
              <Item>
                <InputLabel>
                  {this.props.settingCompanyName === 'Fresh Green Inc' ? 'Receipt No.' : 'Customer PO No.'}
                </InputLabel>
                <ThemeInput
                  data-id={1}
                  value={reference}
                  onChange={(e: any) => this.onReferenceChange(e, this.props)}
                  onBlur={this.onSaveReference}
                  disabled={this.showInput(currentOrder) || disabled}
                  className='sales-cart-input header-input-field normal receipt-no-input'
                />
                <WarningText className={isReferenceDuplicate ? 'show' : 'hide'}>
                  {this.props.settingCompanyName === 'Fresh Green Inc'
                    ? 'Duplicate Receipt No.'
                    : 'Duplicate Customer PO No.'}
                </WarningText>
              </Item>
              {/* <Item style={{ alignSelf: 'flex-start', marginTop: 37 }}>
                <ThemeCheckbox
                  checked={currentOrder && currentOrder.cashSale ? true : false}
                  onChange={this.onCashSaleChange}
                  disabled={orderDisabled}
                  className="sales-cart-input"
                >
                  Cash Sale
                </ThemeCheckbox>
              </Item> */}
            </FlexDiv>
            <FlexDiv style={{color: gray01}}>
              <Item className="left padding32-item">
                <InputLabel>Order No.</InputLabel>
                <ValueLabel>
                  {getOrderPrefix(sellerSetting, 'sales')}
                  {currentOrder ? currentOrder.wholesaleOrderId : ''}
                </ValueLabel>
              </Item>
              <Item className="padding32-item">
                <InputLabel>Order Status</InputLabel>
                <OrderStatusLabel>
                  {formatOrderStatus(currentOrder ? currentOrder.wholesaleOrderStatus : '', sellerSetting)}
                </OrderStatusLabel>
              </Item>
              <Item className="padding32-item">
                <InputLabel>Order Total</InputLabel>
                {/* <ValueLabel>${orderTotal.toFixed(2)}</ValueLabel> */}
                <ValueLabel>${calculateTotalOrder(this.props.orderItems, this.props.oneOffItems, this.props.catchWeightValues)}</ValueLabel>
              </Item>
              <Item className="right padding32-item">
                <InputLabel>Terms</InputLabel>
                <ThemeSelect
                  value={financialTerms}
                  style={{ minWidth: 120 }}
                  onChange={(v) => this.handleChangeFinancialTerms(v)}
                  disabled={disabled}
                >
                  {paymentTerms.map((v) => (
                    <Select.Option value={v.name}>{v.name}</Select.Option>
                  ))}
                </ThemeSelect>
                {_.get(
                  paymentTerms.find((v) => v.name === financialTerms),
                  'type',
                  '',
                ) === 'FINANCIAL_TERM_FIXED_DAYS' && (
                  <p style={{ color: '#a2a2a2', marginTop: 10, marginBottom: 0 }}>
                    Due &nbsp;
                    {moment(deliveryDate)
                      .add(
                        _.get(
                          paymentTerms.find((v) => v.name === financialTerms),
                          'dueDays',
                          0,
                        ),
                        'days',
                      )
                      .format('MM/DD/YY')}
                    {/* {console.log(moment(deliveryDate).add(5, 'days'))} */}
                  </p>
                )}
              </Item>
            </FlexDiv>
          </FlexDiv>
        </PaddingContainer>
      </>
    )
  }
}

export default OrderSummary
