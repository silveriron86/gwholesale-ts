import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { InventoryProductVerticalForm } from '~/modules/inventory/components'
import { InventoryDispatchProps, InventoryLoading, InventoryModule, InventoryStateProps } from '~/modules/inventory'
import { RouteProps } from 'react-router'

export type AddProductModalProps = InventoryDispatchProps &
  InventoryStateProps &
  RouteProps & {
    theme: Theme
    visible: boolean
    onToggle: Function
    reloadItems: Function
    orderId: number
    default: string
  }

class AddProductModal extends React.PureComponent<AddProductModalProps> {
  componentDidMount() {
    this.props.getAllCompanyTypes()
    this.props.getVendors()
  }

  componentWillReceiveProps(nextProps: AddProductModalProps) {
    this.setDefaultProductName(nextProps.default)
  }

  setDefaultProductName = (productName: string) => {
    const { form } = this.formRef.props
    form.setFieldsValue({
      name: productName
    })
  }

  handleOk = () => {
    const { orderId } = this.props
    const { form } = this.formRef.props
    form.validateFields((err, values) => {
      if (err) {
        return
      }
      values = {
        ...values,
        sell: true,
        purchase: true,
        active: true,
        isOrganic: false,
      }
      if (!values.sku || (values.sku && values.sku.trim() == '')) {
        values.sku = values.name
      }
      if (!values.quantity || isNaN(values.quantity)) {
        values.quantity = 0
      }
      if (values.ratioUOM === 'N/A' && values.constantRatio === false) {
        values.ratioUOM = 1
      }
      this.props.loadIndex(InventoryLoading.INVENTORY_ITEM_SAVE)
      this.props.createItem({
        orderId,
        data: values
      })
      form.resetFields()
      this.props.onToggle(true)

      setTimeout(() => {
        this.props.reloadItems(this.props.orderId);
      }, 1000)
    })
  }

  handleCancel = () => {
    this.props.onToggle(false)
  }

  saveFormRef = (formRef: any) => {
    this.formRef = formRef
  }

  render() {
    const { companyProductTypes, visible, vendors } = this.props
    return (
      <InventoryProductVerticalForm
        wrappedComponentRef={this.saveFormRef}
        companyProductTypes={companyProductTypes}
        vendors={vendors}
        handleOk={this.handleOk}
        handleCancel={this.handleCancel}
        visible={visible}
        getAllCompanyTypes={this.props.getAllCompanyTypes}
      />
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.inventory
export default withTheme(connect(InventoryModule)(mapStateToProps)(AddProductModal))
