import * as React from 'react'
import { InputLabel, ThemeInput } from '~/modules/customers/customers.style'
import { OrderItem } from '~/schema'
import { Description, DialogBodyDiv, Item } from '../../styles'
import jQuery from 'jquery'

type EditItemNameProps = {
  record: OrderItem
  open: boolean
  setCurrentRecord: Function
}

class EditItemName extends React.PureComponent<EditItemNameProps> {
  state = {
    editedItemName: ''
  }

  componentDidMount() {
    if (this.props.record) {
      this.setState({
        editedItemName: this.props.record.editedItemName ? this.props.record.editedItemName : this.props.record.variety
      }, () => {
        this.props.setCurrentRecord({ ...this.props.record, editedItemName: this.state.editedItemName })
      })
    }

    setTimeout(() => {
      jQuery('.edit-item-name-input').trigger('focus')
    }, 50)

  }

  componentWillReceiveProps(nextProps: EditItemNameProps) {
    if (!this.props.record && nextProps.record || !this.props.open && nextProps.open) {
      this.setState({
        editedItemName: nextProps.record.editedItemName
      })
    }
  }

  onTextChange = (evt: any) => {
    const { record, setCurrentRecord } = this.props
    this.setState({ editedItemName: evt.target.value })
    setCurrentRecord({ ...record, editedItemName: evt.target.value })
  }

  render() {
    const { editedItemName } = this.state
    const { record } = this.props
    return (
      <div className={`item-note-modal-body ${this.props.open ? 'open' : 'hidden'}`}>
        <DialogBodyDiv>
          <Item style={{ margin: 0 }}>Edit item display name</Item>
          <Description style={{ color: 'black', fontWeight: 500 }}>Edit the item name that is displayed on the pick sheet, invoice and bill of lading (customer will see this.)</Description>
          <InputLabel style={{ fontWeight: 500 }}>Item display name</InputLabel>
          <ThemeInput
            defaultValue={editedItemName}
            className='edit-item-name-input'
            style={{ width: '100%' }}
            value={editedItemName ? editedItemName : ''}
            onChange={this.onTextChange}
          />
        </DialogBodyDiv>
      </div>
    )
  }
}

export default EditItemName