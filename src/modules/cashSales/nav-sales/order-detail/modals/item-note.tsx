import * as React from 'react'
import { InputLabel, ThemeTextArea } from '~/modules/customers/customers.style'
import { OrderItem } from '~/schema'
import { Description, DialogBodyDiv, DialogSubContainer, FlexDiv, Item, ValueLabel } from '../../styles'

type AddItemNoteModalProps = {
  record: OrderItem
  open: boolean
  setCurrentRecord: Function
}

class AddItemNoteModal extends React.PureComponent<AddItemNoteModalProps> {
  state = {
    record: this.props.record,
    note: ''
  }

  componentDidMount() {
    if (this.props.record) {
      this.setState({
        record: this.props.record,
        note: this.props.record.note
      })
    }

  }

  componentWillReceiveProps(nextProps: AddItemNoteModalProps) {
    if (!this.props.record && nextProps.record || !this.props.open && nextProps.open) {
      this.setState({
        record: nextProps.record,
        note: nextProps.record.note
      })
    }
  }

  onTextChange = (evt: any) => {
    const { record, setCurrentRecord } = this.props
    this.setState({ note: evt.target.value })
    setCurrentRecord({ ...record, note: evt.target.value })
  }

  render() {
    const { record, note } = this.state
    return (
      <div className={`item-note-modal-body ${this.props.open ? 'open' : 'hidden'}`}>
        <DialogSubContainer className='bordered'>
          <FlexDiv>
            <Item>
              <InputLabel>Item</InputLabel>
              <ValueLabel className='medium'>{record ? record.variety : ''}</ValueLabel>
            </Item>
            <Item>
              <InputLabel>Lot</InputLabel>
              <ValueLabel className='medium'>{record ? record.lotId : ''}</ValueLabel>
            </Item>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <Description>This internal note will be printed on the pick sheet.</Description>
          <InputLabel>Internal Note</InputLabel>
          <ThemeTextArea
            rows={4}
            style={{ width: '100%' }}
            placeholder={'Note...'}
            value={note && note != 'null' ? note : ''}
            onChange={this.onTextChange}
          />
        </DialogBodyDiv>
      </div>
    )
  }
}

export default AddItemNoteModal