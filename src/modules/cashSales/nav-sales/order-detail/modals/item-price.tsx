import { Radio, Tooltip, Button } from 'antd'
import { withTheme } from 'emotion-theming'
import moment from 'moment'
import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { InputLabel, ThemeInput, ThemeTextArea, ThemeSpin, ThemeButton } from '~/modules/customers/customers.style'
import { ProductProps, ProductDispatchProps, ProductModule } from '~/modules/product'
import { OrderDetail, OrderItem } from '~/schema'
import { GlobalState } from '~/store/reducer'
import {
  Description,
  DialogBodyDiv,
  DialogSubContainer,
  Flex,
  FlexDiv,
  Item,
  Unit,
  ValueLabel,
  VerticalPadding12,
  VerticalPadding8,
  SelectWrapper,
  CostWrapper,
} from '../../styles'
import UomSelect from '../components/uom-select'
import {
  basePriceToRatioPrice,
  ratioPriceToBasePrice,
  getLogicAndGroup,
  formateGroupLogic,
  formatNumber,
  mathRoundFun,
  getPriceGroupPriceString,
  multiplyUomPriceFactor,
} from '~/common/utils'
import { Icon } from '~/components'

type ItemPriceProps = ProductProps &
  ProductDispatchProps & {
    changeOrderItemPrice: Function
    changeModalStatus: Function
    record: OrderItem
    order: OrderDetail
  }

class ItemPrice extends React.PureComponent<ItemPriceProps> {
  state = {
    pricingLogic: this.props.record
      ? formateGroupLogic(this.props.record.pricingLogic, this.props.record.pricingGroup)
      : '',
    customPrice:
      this.props.record && this.props.record.pricingLogic == 'CUSTOM_PRICE'
        ? basePriceToRatioPrice(
            this.props.record.pricingUOM ? this.props.record.pricingUOM : this.props.record.inventoryUOM,
            this.props.record.price,
            this.props.record,
          )
        : '',
    selectedPrice: this.props.record ? this.props.record.price : 0,
    items: this.props.items,
    orderItem: this.props.record ? this.props.record : {},
  }

  componentDidMount() {
    this.getInitData(this.props.record.wholesaleOrderItemId)
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.record != null && this.props.record != null) {
      if (
        nextProps.record.wholesaleOrderItemId != this.props.record.wholesaleOrderItemId ||
        nextProps.record.lotId != this.props.record.lotId
      ) {
        this.getInitData(nextProps.record.wholesaleOrderItemId)
      }
      if (JSON.stringify(nextProps.record) != JSON.stringify(this.props.record)) {
        this.setState({
          orderItem: nextProps.record,
          pricingLogic: nextProps.record
            ? formateGroupLogic(nextProps.record.pricingLogic, nextProps.record.pricingGroup)
            : '',
          customPrice:
            nextProps.record && nextProps.record.pricingLogic == 'CUSTOM_PRICE'
              ? basePriceToRatioPrice(
                  nextProps.record.pricingUOM ? nextProps.record.pricingUOM : nextProps.record.inventoryUOM,
                  nextProps.record.price,
                  nextProps.record,
                )
              : '',
        })
      }
    }
  }

  getInitData = (wholesaleOrderItemId: number) => {
    const { order } = this.props
    this.props.resetProductPricingLoading()
    this.props.getProductPricingData({
      wholesaleOrderItemId: wholesaleOrderItemId ? wholesaleOrderItemId : this.props.record.wholesaleOrderItemId,
      clientId: order.wholesaleClient.clientId,
    })
  }

  onChange = (e: any) => {
    const value = e.target.value
    this.setState(
      {
        pricingLogic: value,
      },
      () => {
        if (value == 'CUSTOM_PRICE' && this.input != null) {
          this.input.focus()
        }
      },
    )
  }

  calculateSalePrice = (margin: number) => {
    const { items } = this.state
    if (margin == 0) {
      return items['cost']
    } else if (margin > 0 && items['cost'] > 0) {
      return Math.round((items['cost'] / (1 - margin / 100)) * 100) / 100
    } else {
      return 0
    }
  }

  onCustomPriceChange = (evt: any) => {
    this.setState({ customPrice: evt.target.value })
  }

  onChangeUom = (orderItemId: number, value: any) => {
    let price = this.props.record.price
    if (this.props.record.pricingUOM != value) {
      //change uom need use lot cost caculate new price ,  requirement from G
      price = basePriceToRatioPrice(value, this.props.record.price, this.props.record, 12, true)
    } else {
      price = basePriceToRatioPrice(this.props.record.pricingUOM, this.props.record.price, this.props.record)
    }
    this.setState({
      orderItem: {
        ...this.state.orderItem,
        pricingUOM: value,
      },
      customPrice: price,
    })
  }

  updateOrderItemPrice = () => {
    const { orderItem, pricingLogic, customPrice } = this.state
    let logic = getLogicAndGroup(pricingLogic)
    let data = {
      pricingLogic: logic.defaultLogic,
      pricingGroup: logic.defaultGroup,
      pricingUOM: orderItem.pricingUOM ? orderItem.pricingUOM : orderItem.inventoryUOM,
      price: customPrice,
      wholesaleOrderItemId: orderItem.wholesaleOrderItemId,
    }
    // console.log(data)
    this.props.changeOrderItemPrice(data)
    this.props.changeModalStatus('openItemPriceModal')
  }

  onCancelModal = () => {
    this.props.changeModalStatus('openItemPriceModal')
  }

  getPriceForSalesOrder = (field: string, isInit?: boolean) => {
    const productPricing = this.props.productPricing
    const { priceGroupType, costMethod } = productPricing
    const { orderItem } = this.state

    const pricingUOM = orderItem.pricingUOM ? orderItem.pricingUOM : orderItem.inventoryUOM
    const defaultPricingUOM = productPricing.defaultSellingPricingUOM
      ? productPricing.defaultSellingPricingUOM
      : orderItem.inventoryUOM
    let cost = orderItem.cost
    if (priceGroupType == 1) {
      if (defaultPricingUOM != pricingUOM) {
        return formatNumber(
          basePriceToRatioPrice(
            pricingUOM,
            ratioPriceToBasePrice(defaultPricingUOM, productPricing[field], productPricing, 12),
            productPricing,
            2,
            isInit ? false : true,
          ),
          2,
        )
      } else {
        return formatNumber(productPricing[field], 2)
      }
    } else if (priceGroupType == 2) {
      cost = costMethod == 1 ? orderItem.cost : productPricing.cost
      if (isInit) {
        return formatNumber(productPricing[field], 2)
      } else {
        return formatNumber(
          basePriceToRatioPrice(
            pricingUOM,
            mathRoundFun(cost / (1 - productPricing[field] / 100), 12),
            productPricing,
            2,
            true,
          ),
          2,
        )
      }
    } else {
      cost = costMethod == 1 ? orderItem.cost : productPricing.cost
      if (isInit) {
        return formatNumber(productPricing[field], 2)
      } else {
        return formatNumber(
          multiplyUomPriceFactor(
            pricingUOM,
            basePriceToRatioPrice(pricingUOM, mathRoundFun(cost, 12), productPricing, 2, false) + productPricing[field],
            productPricing,
            2,
          ),
          2,
        )
      }
    }
  }

  render() {
    const radioStyle = {
      width: '70%',
    }

    const { pricingLogic, customPrice, orderItem } = this.state
    const { productPricing, loadingProductPrice } = this.props

    const lastSoldDate =
      productPricing && productPricing.lastSoldDate ? moment.utc(productPricing.lastSoldDate).format('MM/DD/YY') : ''

    const pricingUOM = orderItem.pricingUOM ? orderItem.pricingUOM : orderItem.inventoryUOM
    return (
      <>
        <ThemeSpin tip="Loading..." spinning={loadingProductPrice}>
          <DialogBodyDiv className="item-price-modal">
            <Description>Select a product pricing strategy to apply, or set a custom price.</Description>
            <div>
              Unit of Measurement for Pricing Display
              <Tooltip
                placement="top"
                title="Choose how you would like to view pricing. The system will automatically calculate prices based on the ratios configured for the product. The pricing for catchweight items can only be displayed with the configured pricing unit of measure."
              >
                <Icon type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
              </Tooltip>
            </div>
            <SelectWrapper>
              <UomSelect handlerChangeUom={this.onChangeUom} orderItem={orderItem} from={'pricingModal'} type={1} />
            </SelectWrapper>
            <CostWrapper>
              Item Cost:&nbsp;
              {productPricing.lotCost ? (
                <span>
                  {productPricing
                    ? `$${formatNumber(basePriceToRatioPrice(pricingUOM, productPricing.lotCost, productPricing), 2)}`
                    : ''}
                  {orderItem ? `/${pricingUOM}` : ''}&nbsp;(
                  {`Lot ${orderItem.lotId}`})
                </span>
              ) : (
                <span>
                  {productPricing
                    ? `$${formatNumber(basePriceToRatioPrice(pricingUOM, productPricing.cost, productPricing, 2), 2)}`
                    : ''}
                  {orderItem ? `/${pricingUOM}` : ''}&nbsp;(
                  {'default cost'})
                </span>
              )}
            </CostWrapper>
            <Radio.Group onChange={this.onChange} value={pricingLogic} style={{ width: '100%' }}>
              <Flex className="v-center" style={VerticalPadding8}>
                <Radio style={radioStyle} value={'FOLLOW_DEFAULT_SALES'}>
                  Default Price
                </Radio>
                <Unit>
                  $
                  {productPricing
                    ? formatNumber(basePriceToRatioPrice(pricingUOM, productPricing.price, productPricing, 12, true), 2)
                    : 0.0}
                  {orderItem ? `/${pricingUOM}` : ''}
                </Unit>
              </Flex>

              {productPricing.lastSoldChecked && lastSoldDate && (
                <Flex className="v-center" style={VerticalPadding8}>
                  <Radio style={radioStyle} value={'FOLLOW_LAST_SOLD'}>
                    Last sold price for this account
                  </Radio>
                  <Unit>
                    {productPricing
                      ? `$${formatNumber(
                          basePriceToRatioPrice(pricingUOM, productPricing.lastSoldPrice, productPricing, 12),
                          2,
                        )}`
                      : ''}
                    {orderItem ? `/${pricingUOM}` : ''}
                    {lastSoldDate ? ` on ${lastSoldDate}` : ''}
                  </Unit>
                </Flex>
              )}

              {productPricing.groupChecked && (
                <>
                  <Flex className="v-center" style={VerticalPadding8}>
                    <Radio style={radioStyle} value={'GROUP_A'}>
                      Group A
                      {`${getPriceGroupPriceString(
                        productPricing.priceGroupType,
                        this.getPriceForSalesOrder('marginA', true),
                        productPricing.descA,
                      )}`}
                    </Radio>
                    <Unit>
                      ${this.getPriceForSalesOrder('marginA')}
                      {orderItem ? `/${pricingUOM}` : ''}
                    </Unit>
                  </Flex>
                  <Flex className="v-center" style={VerticalPadding8}>
                    <Radio style={radioStyle} value={'GROUP_B'}>
                      Group B
                      {`${getPriceGroupPriceString(
                        productPricing.priceGroupType,
                        this.getPriceForSalesOrder('marginB', true),
                        productPricing.descB,
                      )}`}
                    </Radio>
                    <Unit>
                      ${this.getPriceForSalesOrder('marginB')}
                      {orderItem ? `/${pricingUOM}` : ''}
                    </Unit>
                  </Flex>
                  <Flex className="v-center" style={VerticalPadding8}>
                    <Radio style={radioStyle} value={'GROUP_C'}>
                      Group C
                      {`${getPriceGroupPriceString(
                        productPricing.priceGroupType,
                        this.getPriceForSalesOrder('marginC', true),
                        productPricing.descC,
                      )}`}
                    </Radio>
                    <Unit>
                      ${this.getPriceForSalesOrder('marginC')}
                      {orderItem ? `/${pricingUOM}` : ''}
                    </Unit>
                  </Flex>
                  <Flex className="v-center" style={VerticalPadding8}>
                    <Radio style={radioStyle} value={'GROUP_D'}>
                      Group D
                      {`${getPriceGroupPriceString(
                        productPricing.priceGroupType,
                        this.getPriceForSalesOrder('marginD', true),
                        productPricing.descD,
                      )}`}
                    </Radio>
                    <Unit>
                      ${this.getPriceForSalesOrder('marginD')}
                      {orderItem ? `/${pricingUOM}` : ''}
                    </Unit>
                  </Flex>
                  <Flex className="v-center" style={VerticalPadding8}>
                    <Radio style={radioStyle} value={'GROUP_E'}>
                      Group E
                      {`${getPriceGroupPriceString(
                        productPricing.priceGroupType,
                        this.getPriceForSalesOrder('marginE', true),
                        productPricing.descE,
                      )}`}
                    </Radio>
                    <Unit>
                      ${this.getPriceForSalesOrder('marginE')}
                      {orderItem ? `/${pricingUOM}` : ''}
                    </Unit>
                  </Flex>
                </>
              )}
              <Flex className="v-center" style={VerticalPadding8}>
                <Radio style={radioStyle} value={'CUSTOM_PRICE'}>
                  Custom Price
                </Radio>
                {pricingLogic === 'CUSTOM_PRICE' ? (
                  <>
                    $
                    <ThemeInput
                      type="number"
                      style={{ width: 100 }}
                      onChange={this.onCustomPriceChange}
                      value={customPrice}
                      min={0}
                      precision={2}
                      ref={(input) => (this.input = input)}
                    />
                    {orderItem ? `/${pricingUOM}` : ''}
                  </>
                ) : null}
              </Flex>
            </Radio.Group>
            <DialogSubContainer>
              <ThemeButton type="primary" onClick={this.updateOrderItemPrice}>
                Save
              </ThemeButton>
              &nbsp;&nbsp;
              <Button onClick={this.onCancelModal}>CANCEL</Button>
            </DialogSubContainer>
          </DialogBodyDiv>
        </ThemeSpin>
      </>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.product
export default withTheme(connect(ProductModule)(mapStateToProps)(ItemPrice))
