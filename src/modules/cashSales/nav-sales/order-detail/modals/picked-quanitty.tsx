import { Button, Icon, Switch } from 'antd'
import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { InputLabel, ThemeInput, ThemeTextArea } from '~/modules/customers/customers.style'
import { OrdersDispatchProps, OrdersStateProps, OrdersModule } from '~/modules/orders'
import { OrderItem } from '~/schema'
import { GlobalState } from '~/store/reducer'
import { DialogBodyDiv, DialogSubContainer, Flex, FlexDiv, Item, ValueLabel, VerticalPadding8 } from '../../styles'
import jQuery from 'jquery'
import { formatNumber } from '~/common/utils'
import { withRouter } from 'react-router-dom'
import { cloneDeep, cloneDeepWith } from 'lodash'

type PickedQuantityProps = OrdersStateProps &
  OrdersDispatchProps & {
    record: OrderItem
    isWO?: boolean
    updateSaleOrderItemForWO?: Function
    open: boolean
  }

function formatNumberValue(value) {
  value += ''
  const list = value.split('.')
  const prefix = list[0].charAt(0) === '-' ? '-' : ''
  let num = prefix ? list[0].slice(1) : list[0]
  let result = ''
  while (num.length > 3) {
    result = `,${num.slice(-3)}${result}`
    num = num.slice(0, num.length - 3)
  }
  if (num) {
    result = num + result
  }
  return `${prefix}${result}${list[1] ? `.${list[1]}` : ''}`
}

class NumericInput extends React.Component {
  onChange = (e) => {
    const { value } = e.target
    const reg = /^-?\d*(\.\d*)?$/
    if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      this.props.onChange(value)
    }
  }

  // '.' at the end or only '-' in the input box.
  onBlur = () => {
    const { value, onBlur, onChange } = this.props
    let valueTemp = value
    if ((value.length > 0 && value.charAt(value.length - 1) === '.') || value === '-') {
      valueTemp = value.slice(0, -1)
    }
    if (value !== valueTemp) {
      onChange(valueTemp.replace(/0*(\d+)/, '$1'))
    }
    if (onBlur) {
      onBlur()
    }
  }

  render() {
    return <ThemeInput {...this.props} onChange={this.onChange} onBlur={this.onBlur} maxLength={25} />
  }
}

class PickedQuantity extends React.PureComponent<PickedQuantityProps> {
  state = {
    newValue: '',
    items: [],
    loaded: false,
    firstOpen: false,
    ids: [],
    scanMode: localStorage.getItem('SCAN_MODE') === 'true',
  }

  componentDidMount() {
    if (this.props.record) {
      this.props.resetLoading()
      this.getQuantityCatchWeight(this.props.record.wholesaleOrderItemId)
    }
  }

  initEventHandler = () => {
    // Move focus to next field by carriage return
    jQuery('.quantity-item input').unbind().bind('keypress', (e: any) => {
      if(e.keyCode === 13) {
        const currentEl = jQuery(e.currentTarget).parent().parent()
        let index = jQuery('.quantites-wrapper .quantity-item').index(currentEl[0])
        if(index + 1 === jQuery('.quantites-wrapper .quantity-item').length - 1) {
          index = -1;
        }
        jQuery(jQuery('.quantites-wrapper .quantity-item')[index+1]).find('input').focus();
      }
    })
  }

  componentWillReceiveProps(nextProps: PickedQuantityProps) {
    if (!this.props.open && nextProps.open) {
      // new modal
      this.setState({
        newValue: '',
        items: [],
        loaded: false,
        firstOpen: false,
        ids: [],
        scanMode: localStorage.getItem('SCAN_MODE') === 'true'
      })

      if (typeof nextProps.catchWeights[nextProps.record.wholesaleOrderItemId] === 'undefined') {
        this.props.resetLoading()
        this.getQuantityCatchWeight(nextProps.record.wholesaleOrderItemId)
      } else {
        let newItems: any[] = []
        const catchWeights = nextProps.catchWeights[nextProps.record.wholesaleOrderItemId]
        if (catchWeights.length > 0) {
          newItems = JSON.parse(JSON.stringify(catchWeights))
        } else {
          if (nextProps.record && nextProps.record.quantity > 0) {
            for (let i = 0; i < nextProps.record.quantity; i++) {
              newItems.push({
                unitWeight: 0,
              })
            }
          }
        }
        this.setState({ items: newItems })
      }
    }

    if (!nextProps.loading && !this.state.firstOpen) {
      // if (Object.keys(nextProps.catchWeight).length == 0 && nextProps.record.quantity) {
      //   this.props.addCatchWeightByNumber({ orderItemId: nextProps.record.wholesaleOrderItemId, count: nextProps.record.quantity })
      // }
      this.setFocusOnInput()
      this.setState({ firstOpen: true })
    }

    if (this.props.loading !== nextProps.loading && nextProps.loading === false) {
      if (this.state.loaded === false) {
        let ids: any[] = []
        nextProps.catchWeight.forEach((v, i) => {
          ids.push(v.id)
        })

        let newItems: any[] = []
        if (nextProps.catchWeight && nextProps.catchWeight.length > 0) {
          newItems = JSON.parse(JSON.stringify(nextProps.catchWeight))
        } else {
          if (nextProps.record && nextProps.record.quantity > 0) {
            for (let i = 0; i < nextProps.record.quantity; i++) {
              newItems.push({
                unitWeight: 0,
              })
            }
          }
        }
        this.setState({
          items: newItems,
          loaded: true,
          ids,
        })
      }
    }
    if (JSON.stringify(nextProps.catchWeight) !== JSON.stringify(this.props.catchWeight)) {
      if (nextProps.catchWeight) {
        this.setState({ items: nextProps.catchWeight ? JSON.parse(JSON.stringify(nextProps.catchWeight)) : null })
      }
      if (this.props.catchWeight.length == 0 && nextProps.catchWeight.length > 0) {
        this.setFocusOnInput()
      }
      const orderItem: any = { ...nextProps.record, ...{ picked: nextProps.catchWeight.length } }
      if (orderItem.wholesaleOrderItemId) {
        let totalWeight = 0
        const filled = nextProps.catchWeight.length
        if (filled > 0) {
          nextProps.catchWeight.forEach((item: any) => {
            if (item.unitWeight !== '') {
              totalWeight += parseFloat(item.unitWeight)
            }
          })
        }
      }
    }
  }

  setFocusOnInput = () => {
    setTimeout(() => {
      const fields = jQuery('.quantity-item').find('.ant-input, .ant-btn')
      if (fields.length) {
        this.onFocusEventHandler()
        jQuery(fields[0]).trigger('focus')
      }
      this.initEventHandler();
    }, 500)
  }

  onFocusEventHandler = () => {
    jQuery('.quantity-item .ant-input')
      .unbind()
      .bind('focus', (e) => {
        jQuery(e.target).select()
      })
  }

  getQuantityCatchWeight = (orderItemId: string) => {
    this.props.setOrderItemId(parseInt(orderItemId, 10))
    this.props.getQuantityCatchWeight(orderItemId)
  }

  onChangeUnitWeight = (index: number, value: number) => {
    let updatedItems = [...this.state.items]
    updatedItems.forEach((el: any, idx: number) => {
      if (idx === index) {
        el.unitWeight = value
      }
    })
    this.setState({ items: updatedItems })
  }

  updateQuantity = (id: number, evt: any) => {
    const { catchWeight } = this.props
    const found = catchWeight.find((c) => c.id === id)
    const newValue = Number.parseFloat(evt.target.value)
    if (id && !isNaN(evt.target.value) && newValue >= 0 && Number.parseFloat(found.unitWeight) !== newValue) {
      const data = {
        qtyDetailId: id,
        orderItemId: this.props.record.wholesaleOrderItemId,
        unitWeight: newValue,
      }
      this.props.resetLoading()
      this.props.setOrderItemId(this.props.record.wholesaleOrderItemId)
      this.props.updateQuantityCatchWeight(data)
    }
  }

  deleteQuantity = (index: number, evt: any) => {
    const item = this.state.items[index]
    // if (item && item.id) {
    //   this.props.resetLoading()
    //   this.props.deleteQuantityCatchWeight({
    //     orderItemId: this.props.record.wholesaleOrderItemId,
    //     qtyDetailId: item.id,
    //   })
    // }
    const updateItems = [...this.state.items]
    updateItems.splice(index, 1)
    this.setState({ items: updateItems })
  }

  renderQuantitiesField = () => {
    const { items, scanMode } = this.state
    const { record } = this.props
    console.log(record)
    let result: any[] = []
    items.forEach((el: any, index: number) => {
      let warning: boolean = false
      if(scanMode) {
        if(el.unitWeight.length !== 12) {
          warning = true
        } else {
          // digits 2 ~ 6
          let sku = el.unitWeight.substr(1, 5)
          console.log('sku = ', sku)
          if(record.SKU && record.SKU.indexOf(sku) === 0) {
            // 7 to 11, real value
          } else {
            warning = true
          }
        }
      }
      result.push(
        <Item className="quantity-item" key={index}>
          <Flex className="v-center space-between">
            {warning ? (
              <InputLabel className="warning">WRONG SKU!</InputLabel>
            ): (
              <InputLabel>
                {index == 0 && record ? record.UOM : ''} {index + 1}
              </InputLabel>
            )}
            <Icon type="close-circle" className="close-quantity" onClick={this.deleteQuantity.bind(this, index)} />
          </Flex>
          <div>
            {scanMode ? (
              <ThemeInput className={warning ? "warning" : ''} value={el.unitWeight} onChange={(e: any) => this.onChangeUnitWeight(index, e.target.value)}  />
            ): (
              <NumericInput value={el.unitWeight} onChange={this.onChangeUnitWeight.bind(this, index)} />
            )}
          </div>
        </Item>,
      )
    })
    return result
  }

  handleAdd = () => {
    const { items } = this.state
    let updateItems: any[] = [...items]
    // this.props.resetLoading()
    let newItem: any = {
      unitWeight: 0,
    }
    updateItems.push({ ...newItem })
    this.setState({ items: updateItems }, () => {
      this.setFocusOnInput()
    })
    /*
    const orderItemId = this.props.record.wholesaleOrderItemId
    newItem = { ...newItem, orderItemId }
    this.props.addQuantityCatchWeight(newItem)
    */
  }

  handleSave = () => {
    const { items, scanMode } = this.state
    const { record } = this.props

    const itemList = cloneDeepWith(items).map((item: any) => {
      if(scanMode) {
        // digits 7 to 11, insert decimal between digits 9 and 10.
        let val = item.unitWeight.substr(6, 3) + '.' + item.unitWeight.substr(9, 2)
        item.unitWeight = parseFloat(val)
      }
      if (item.unitWeight === '') {
        item.unitWeight = 0
      }
      return item
    })

    this.props.setOrderItemId(record.wholesaleOrderItemId)
    this.props.addQuantityCatchWeightByList({
      orderItemId: record.wholesaleOrderItemId,
      data: { itemList },
    })

    // let totalWeight = 0
    // if (items && items.length > 0) {
    //   items.forEach((item: any) => {
    //     if (item.unitWeight !== '') {
    //       totalWeight += parseFloat(item.unitWeight)
    //     }
    //   })
    // }
    // return {
    //   picked: items.length,
    //   catchWeightQty: totalWeight
    // }
  }

  onChangeScanMode = (scanMode: boolean) => {
    localStorage.setItem('SCAN_MODE', scanMode.toString())
    this.setState({
      scanMode
    }, () => {
      setTimeout(() => {
        this.initEventHandler()
      }, 10)
    })
  }

  render() {
    const { items, scanMode } = this.state
    const { record } = this.props
    let totalWeight = 0
    if (items && items.length > 0) {
      items.forEach((item: any) => {
        if (item.unitWeight !== '') {
          totalWeight += parseFloat(item.unitWeight)
        }
      })
    }
    return (
      <>
        <DialogSubContainer className="bordered">
          <FlexDiv className="space-between">
            <FlexDiv>
              <Item>
                <InputLabel>Item</InputLabel>
                <ValueLabel className="medium">{record ? record.variety : ''}</ValueLabel>
              </Item>
              <Item>
                <InputLabel>Lot</InputLabel>
                <ValueLabel className="medium">{record ? record.lotId : ''}</ValueLabel>
              </Item>
              <Item>
                <InputLabel>Pricing UOM</InputLabel>
                <ValueLabel className="medium">
                  {record ? (record.pricingUOM ? record.pricingUOM : record.inventoryUOM) : ''}
                </ValueLabel>
              </Item>
            </FlexDiv>
            <Item>
              <InputLabel className="theme">Scan Mode</InputLabel>
              <Switch checked={scanMode} onChange={this.onChangeScanMode} />
            </Item>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <InputLabel style={VerticalPadding8}>
            Total Billable Quantity: {formatNumber(totalWeight, 2)} {record ? record.baseUOM : ''}
          </InputLabel>
          <Flex className="quantites-wrapper" style={{ flexWrap: 'wrap', alignItems: 'flex-end', minHeight: 64 }}>
            {this.renderQuantitiesField()}
            <Item className="quantity-item ">
              <Button onClick={this.handleAdd}>Add {record ? record.UOM : ''}</Button>
            </Item>
          </Flex>
        </DialogBodyDiv>
      </>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withRouter(connect(OrdersModule)(mapStateToProps, null, null, { forwardRef: true })(PickedQuantity))
