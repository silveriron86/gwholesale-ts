import * as React from 'react'
import { useState } from 'react'
import { AutoComplete, Select, Tooltip } from 'antd'
import { AddACWrapper } from '../../styles'
import { cloneDeep } from 'lodash'
import { combineUomForInventory, formatNumber, mathRoundFun, handlerNoLotNumber } from '~/common/utils'
import { OrderDetail, OrderItem } from '~/schema'
import { History } from 'history'
import moment from 'moment'
import _ from 'lodash'
import jQuery from 'jquery'
import { ThemeSelect } from '~/modules/customers/customers.style'
import InventoryAsterisk from '~/components/inventory/inventoryAsterisk'
import { useLatest } from 'ahooks'

const { Option } = AutoComplete

interface CartProps {
  currentOrder: OrderDetail
  orderItems: OrderItem[]
  loading: boolean
  toggleModal: Function
  updateSaleOrderItem: Function
  updateSaleOrderItemWithoutRefresh: Function
  startUpdating: Function
  changeSelectedItems: Function
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  history: History
  getOrderItemsById: Function
  updateOrder: Function
  updateOrderItemsDisplayOrder: Function
  onChagneInserItemPosition: Function
  onChangeOrderItemPricingLogic: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  toggleOpenChildModal: Function
  visibleItemModal: boolean
  condensedMode: boolean
  updating: boolean
  copyOrderQty: Function
  current: any
  hideReplaceDropdown: Function
}

export const ReplaceItem: React.SFC<CartProps> = (props: any) => {
  const [productSKUs, setProductSKUs] = useState([])
  const [search, setSearch] = useState('')
  const visibleDropdown = props?.current?.visibleDropdown
  const currentVariety = props?.current?.variety
  const { sellerSetting, simplifyItemsByOrderId, simplifyItems } = props
  const _simplifyItems = sellerSetting?.company?.enableAddItemFromProductList ? simplifyItemsByOrderId : simplifyItems


  React.useEffect(() => {
    if (visibleDropdown) {
      jQuery('.replace-item-dropdown input').trigger('focus')
      checkDropdownIsAdded()
    }
  }, [visibleDropdown])

  const initProductSkus = (searchStr: string, type: string, isLoading: boolean) => {
    if (!isLoading && searchStr.length < 2) {
      setProductSKUs([])
      return
    }

    let options: any[] = []
    if (_simplifyItems && _simplifyItems.length > 0) {
      _simplifyItems.forEach((item, index) => {
        let row = item
        if (typeof item.wholesaleItem !== 'undefined') {
          row = { ...item, ...item.wholesaleItem }
          row.SKU = row.sku
        }

        if (
          isLoading ||
          (!isLoading && row[type] && row[type].toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) ||
          (!isLoading && row.variety && row.variety.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0)
        ) {
          // const quantity = row.availableQty === null ? 0 : row.availableQty
          // let availableToSell = mathRoundFun(quantity, 2) + ' ' + row.inventoryUOM
          // if (row.subUom) {
          //   availableToSell = combineUomForInventory(quantity, row.subRatio, row.inventoryUOM, row.subUom)
          // }

          // const onHandQty = row.onHandQty === null ? 0 : row.onHandQty
          // let handAmount = mathRoundFun(onHandQty, 2) + ' ' + row.inventoryUOM
          // if (row.subUom) {
          //   handAmount = combineUomForInventory(onHandQty, row.subRatio, row.inventoryUOM, row.subUom)
          // }

          const productName = type === 'SKU' ? `${row.variety} ${row[type] ? `(${row[type]})` : ''}` : row[type]
          options.push({
            key: productName,
            value: row.itemId,
            itemInfo: `${row[type] ? row[type] : ''} ${row.variety} ${handlerNoLotNumber(row, 1)} ${row.inventoryUOM}
            ${(<InventoryAsterisk item={row} flag={1}></InventoryAsterisk>)} available (${handlerNoLotNumber(row, 2)} ${
              row.inventoryUOM
            }
            ${(<InventoryAsterisk item={row} flag={2}></InventoryAsterisk>)}&nbsp;on hand)`,
            text: (
              <AddACWrapper className="space-between">
                <div className="sku ellipsis">{row[type] ? row[type] : ''}</div>
                <div className="product-name ellipsis">{row.variety}</div>
                <div className="inventory ellipsis">
                  {handlerNoLotNumber(row, 1)} {row.inventoryUOM}
                  <InventoryAsterisk item={row} flag={1}></InventoryAsterisk> available ({handlerNoLotNumber(row, 2)}{' '}
                  {row.inventoryUOM}
                  <InventoryAsterisk item={row} flag={2}></InventoryAsterisk>&nbsp;on hand)
                </div>
              </AddACWrapper>
            ),
          })
        }
      })
    }
    let result = options.sort((a, b) => a.key.localeCompare(b.key))
    if (isLoading) {
      let currentItemIndex = result.findIndex((el) => el.key.toLowerCase().indexOf(searchStr.toLowerCase()) > -1)
      console.log(searchStr)
      console.log('current start Index', currentItemIndex, result[currentItemIndex])
      let startIndex = currentItemIndex > 20 ? currentItemIndex - 20 : 0
      let endIndex = startIndex + 40 > result.length ? result.length : startIndex + 40
      console.log(startIndex, endIndex)
      result = result.slice(startIndex, endIndex)
      console.log('sliced ', result)
    }
    if (result.length > 50) {
      setProductSKUs(result.slice(0, 50))
    } else {
      setProductSKUs(result)
    }
  }

  const initSkuRef = useLatest(initProductSkus)

  React.useEffect(() => {
    if (_simplifyItems.length) {
      initSkuRef.current(currentVariety, 'SKU', true)
    }
  }, [_simplifyItems, currentVariety, initSkuRef])

  React.useEffect(() => {
    if (search) {
      const triggeredSource = localStorage.getItem('trigger-by-keyboard')
      let defaultOpen = true
      if (triggeredSource === '1') {
        defaultOpen = false
      }
      const open = defaultOpen || search.length > 1

      const dropdowns = jQuery('.ant-select-dropdown.ant-select-dropdown--single')
      let replaceDropdown = null
      let itemsContainer = null
      jQuery.each(dropdowns, function(i: number, el: any) {
        if (jQuery(el).find('.replace-dropdown-items-container').length) {
          replaceDropdown = el
          itemsContainer = jQuery(replaceDropdown).find('.replace-dropdown-items-container')
        }
      })
      if (replaceDropdown && itemsContainer) {
        if (open) {
          jQuery(replaceDropdown).css('visibility', 'visible')
          jQuery(itemsContainer).removeClass('replace-item-hidden')
        } else {
          jQuery(replaceDropdown).css('visibility', 'hidden')
          jQuery(itemsContainer).addClass('replace-item-hidden')
        }
      }
    }
  }, [search])

  const checkDropdownIsAdded = () => {
    jQuery('body').on('DOMNodeInserted', function(e) {
      if (
        jQuery(e.target).find('.ant-select-dropdown.ant-select-dropdown--single') &&
        jQuery(e.target).find('.replace-dropdown-items-container').length
      ) {
        const triggeredSource = localStorage.getItem('trigger-by-keyboard')
        let defaultOpen = true
        if (triggeredSource === '1') {
          defaultOpen = false
        }
        const replaceDropdown = jQuery(e.target).find('.ant-select-dropdown.ant-select-dropdown--single')[0]
        const itemsContainer = jQuery(e.target).find('.replace-dropdown-items-container')
        if (defaultOpen) {
          jQuery(replaceDropdown).css('visibility', 'visible')
          jQuery(itemsContainer).removeClass('replace-item-hidden')
        } else {
          jQuery(replaceDropdown).css('visibility', 'hidden')
          jQuery(itemsContainer).addClass('replace-item-hidden')
        }
      }
    })
  }

  const onSelectSKU = (itemId, option) => {
    if (props.current.itemId != itemId) {
      const found = _simplifyItems.find((item: any) => {
        return item.itemId == itemId
      })
      if (found) {
        setProductSKUs([])

        let newItem = cloneDeep(found)
        if (newItem.wholesaleOrderItemId) {
          delete newItem.variety
          delete newItem.SKU
          delete newItem.provider
          delete newItem.itemId
        } else {
          delete newItem.children
        }
        let itemsList: any[] = []
        itemsList.push(_formatNewItem(newItem))
        _updateOrder(itemsList)
      }
    }
    onBlur()
  }

  const _updateOrder = (itemsList: any[]) => {
    const { currentOrder, insertItemPosition } = props
    if (currentOrder) {
      if (itemsList.length > 0) {
        itemsList.map((item: any) => {
          item.wholesaleOrderId = currentOrder.wholesaleOrderId
        })
      }
      if (itemsList.length == 1 && typeof insertItemPosition != 'undefined' && insertItemPosition > -1) {
        itemsList[0]['displayOrder'] = insertItemPosition
      }

      let currentRecord = _formatDeleteItem(props.current)
      currentRecord.deleted = true
      itemsList.push(currentRecord)

      const formData = {
        deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        userId: currentOrder.user.userId,
        totalPrice: currentOrder.totalPrice,
        wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
        itemList: itemsList,
        status: currentOrder.wholesaleOrderStatus,
      }

      props.resetLoading()
      props.updateOrder(formData)
    }
  }

  const _formatNewItem = (newItem: any) => {
    let quotedPrice = ''
    let UOM = ''
    let lotId = ''
    let margin = 0
    if (typeof newItem.wholesaleOrderItemId === 'undefined') {
      // if coming from product catalogy
      // The quoted price will be cost*default_margin
      // Newitem is wholesaleItem is different with wholesaleOrderItem
      if (typeof newItem.wholesaleItem === 'undefined') {
        const markup = (newItem.defaultMargin ? newItem.defaultMargin : 0) / 100
        // if (newItem.constantRatio == true) {
        //   quotedPrice = formatNumber(newItem.cost ? ((1 + markup) * newItem.cost) / newItem.ratioUOM : 0, 2)
        // } else {
        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost : 0, 2)
        // }
        UOM = newItem.inventoryUOM
      } else {
        const markup = (newItem.margin ? newItem.margin : 0) / 100
        const freight = newItem.freight ? newItem.freight : 0
        if (newItem.uom == newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio == true) {
          quotedPrice = formatNumber(
            newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
            2,
          )
        } else {
          quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
        }
        UOM = newItem.UOM
      }
    } else {
      // if coming for lot
      // The quoted price will be cost of lot*lot_margin if it is greater than 0, if not lot*default_margin
      // quotedPrice = newItem.cost * newItem.lotMargin
      // if(quotedPrice <= 0) {
      //   quotedPrice = newItem.cost * newItem.wholesaleItem.defaultMargin
      // }
      const markup = (newItem.margin ? newItem.margin : 0) / 100
      const freight = newItem.freight ? newItem.freight : 0
      if (newItem.uom == newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio == true) {
        quotedPrice = formatNumber(
          newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
          2,
        )
      } else {
        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
      }
      if (newItem.uom) {
        UOM = newItem.uom
      }
      lotId = newItem.lotId
    }

    return {
      wholesaleItemId: newItem.wholesaleItem ? newItem.wholesaleItem.wholesaleItemId : newItem.itemId,
      wholesaleOrderItemId: null,
      quantity: props.current.quantity,
      cost: newItem.cost,
      price: quotedPrice,
      picked: props.current.picked,
      catchWeightQty: props.current.picked,
      margin: margin,
      freight: 0,
      // status: newItem.wholesaleItem ? 'PLACED' : newItem.status,
      status: 'PLACED',
      UOM: UOM,
      lotId: lotId,
      displayOrder: props.current.displayOrder,
    }
  }

  const _formatDeleteItem = (item: any) => {
    return {
      wholesaleItemId: item.itemId,
      wholesaleOrderItemId: item.wholesaleOrderItemId,
      quantity: item.quantity,
      picked: item.picked,
      cost: item.cost,
      price: item.price,
      margin: item.margin,
      freight: item.freight,
      status: item.status,
      UOM: item.UOM,
      deleted: true,
    }
  }

  const onSearch = (value: string) => {
    initProductSkus(value, 'SKU', false)
    setSearch(value)
  }

  const onBlur = () => {
    props.hideReplaceDropdown(props.current.wholesaleOrderItemId)
  }

  const renderOptions = () => {
    const children = productSKUs.map((el: any) => (
      <Select.Option key={el.key} value={el.value} title={el.value} itemInfo={el.itemInfo}>
        {el.text}
      </Select.Option>
    ))

    return children
  }

  return (
    <ThemeSelect
      defaultActiveFirstOption={false}
      className="replace-item-dropdown"
      defaultOpen={true}
      autoFocus={true}
      value={props.current.itemId}
      showSearch
      style={{ width: '100%', position: 'absolute', top: -15 }}
      placeholder="Replace item..."
      optionFilterProp="children"
      onSelect={onSelectSKU}
      onSearch={onSearch}
      onBlur={onBlur}
      filterOption={(input, option) => {
        return option.props.itemInfo.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }}
      dropdownStyle={{ minWidth: 700 }}
      dropdownRender={(el: any) => {
        return <div className={`replace-dropdown-items-container`}>{el}</div>
      }}
    >
      {renderOptions()}
    </ThemeSelect>
  )
}
