import { Icon, Select } from 'antd'
import * as React from 'react'
import { InputLabel, ThemeCheckbox, ThemeInput, ThemeSelect, ThemeTextArea } from '~/modules/customers/customers.style'
import { DialogBodyDiv, DialogSubContainer, FlexDiv, Item, ItemHMargin4, ValueLabel } from '../../styles'

type AddItemNoteModalProps = {

}

class WorkOrderRequest extends React.PureComponent<AddItemNoteModalProps> {

  render() {
    return (
      <>
        <DialogSubContainer className='bordered'>
          <FlexDiv>
            <Item>
              <InputLabel>Item</InputLabel>
              <ValueLabel>Honey</ValueLabel>
            </Item>
            <Item>
              <InputLabel>Lot</InputLabel>
              <ValueLabel>880-2515</ValueLabel>
            </Item>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <InputLabel>Work Order Subject</InputLabel>
          <ThemeInput
            style={{ width: '100%' }}
            placeholder={'Subject...'}
          />
        </DialogBodyDiv>
        <DialogBodyDiv>
          <InputLabel>Note to Picker</InputLabel>
          <ThemeTextArea
            rows={4}
            style={{ width: '100%' }}
            placeholder={'Note...'}
          />
        </DialogBodyDiv>
        <DialogBodyDiv>
          <InputLabel>Station</InputLabel>
          <ThemeSelect defaultValue={1} style={{ width: '60%' }}>
            <Select.Option value={1}>Hayward.GF</Select.Option>
          </ThemeSelect>
        </DialogBodyDiv>
        <DialogBodyDiv>
          <ThemeCheckbox >This is a special customer request <Icon type='info-circle' style={ItemHMargin4} /></ThemeCheckbox>
        </DialogBodyDiv>
        <DialogBodyDiv>
          Work orders are shared with the people responsible for picking the associated products.
          Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.
        </DialogBodyDiv>
      </>
    )
  }
}

export default WorkOrderRequest
