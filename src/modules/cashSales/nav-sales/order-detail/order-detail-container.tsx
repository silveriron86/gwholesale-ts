import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import jQuery from 'jquery'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import SalesOrderDetailHeader from './order-detail-header'
import SalesOrderItems from './order-items'
import { Theme } from '~/common'
import { History } from 'history'
import { AuthUser } from '~/schema'
import { ThemeSpin } from '~/modules/customers/customers.style'

type SODetailProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    history: History
    theme: Theme
    currentUser: AuthUser
  }

class SalesOrderDetailContainer extends React.PureComponent<SODetailProps> {
  state = {}
  childRef = React.createRef<any>()

  componentDidMount() {
    const orderId = this.props.match.params.orderId
    console.log('here reloading orders')
    this.props.resetLoading()
    this.props.getOrderDetail(orderId)
    this.props.getOrderItemsById(orderId)
    this.props.getSimplifyCustomers()
    this.props.getSellerSetting()
    this.props.getOrderAdjustments(orderId)
    setTimeout(() => {
      this.props.getOneOffItems(orderId)
    }, 50)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      this.props.setAddressLoading()
      this.props.getAddresses()
    }
  }

  onDoHeaderActions = (actionName: string) => {
    console.log('actionName = ', actionName)
    this.childRef.current.onDoHeaderActions(actionName)
  }

  render() {
    const { cancelingOrder, duplicateOrderLoading } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Selling & Shipping-Cash Sales'}>
        <ThemeSpin spinning={cancelingOrder || duplicateOrderLoading}>
          <SalesOrderDetailHeader {...this.props} onDoHeaderActions={this.onDoHeaderActions} />
          <SalesOrderItems {...this.props} ref={this.childRef} />
        </ThemeSpin>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.orders,
    currentUser: state.currentUser
  }
}

const D = withTheme(connect(OrdersModule)(mapStateToProps)(SalesOrderDetailContainer))
export default D

