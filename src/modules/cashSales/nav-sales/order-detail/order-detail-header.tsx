import * as React from 'react'

import OrderInfo from './components/order-info'
import OrderSummary from './components/order-summary'
import { OrderDetailHeaderContainer } from '../styles'
import { MainAddress, OrderDetail } from '~/schema'

type HeaderProps = {
  loading: boolean
  loadingCurrentOrder: boolean
  loadingAddress: boolean
  currentOrder: OrderDetail
  resetLoading: Function
  syncQBOOrder: Function
  syncNSOrder: Function
  duplicateOrder: Function
  addresses: MainAddress[]
  updateOrderInfo: Function
  onDoHeaderActions: Function
}
export class SalesOrderDetailHeader extends React.PureComponent<HeaderProps> {
  render() {
    return (
      <OrderDetailHeaderContainer>
        <OrderSummary {...this.props} />
        <OrderInfo {...this.props} />
      </OrderDetailHeaderContainer>
    )
  }
}

export default SalesOrderDetailHeader
