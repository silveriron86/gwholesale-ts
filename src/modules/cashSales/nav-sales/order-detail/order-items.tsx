import * as React from 'react'
import {
  DialogSubContainer,
  Flex,
  noPaddingFooter,
  TabHeaderBigSpan,
  TableHeaderItem,
  TabsWrapper,
  ThemeColorSpan,
  NoBorderButton,
} from '../styles'
import { Tabs, Icon, Button, Tooltip, Switch } from 'antd'
import { Icon as IconSVG } from '~/components'
import SwitchTooltip from '~/components/SwitchTooltip'

import { SalesCart } from '../../sales/cart'
import { SalesDocuments } from '~/modules/customers/sales/documents'
import { SalesMargins } from '~/modules/customers/sales/margins'
import { CreditMemo } from '~/modules/customers/sales/credit-memo'
import { ThemeButton, ThemeModal, ThemeOutlineButton, ThemeTextButton, ThemeSwitchWrap } from '~/modules/customers/customers.style'
import AddOrderItemModal from './modals/add-item'
import AddProductModal from './modals/add-product'

import jQuery from 'jquery'
import { OrdersDispatchProps, OrdersStateProps } from '~/modules/orders'
import { AuthUser, OrderItem, User, UserRole } from '~/schema'
import { CACHED_ACCOUNT_TYPE } from '~/common'
import { isArray, debounce } from 'lodash'
import moment from 'moment'
import { History } from 'history'
import { ClassNames } from '@emotion/core'
import ConfirmShippingModal from '~/modules/customers/nav-sales/order-detail/modals/confirm-ship'
import { formatOrderItem, judgeConstantRatio, notify } from '~/common/utils'
import Fulfillment from '~/modules/customers/nav-sales/order-detail/tabs/fulfillment'
import SalesOrderPick from '~/modules/customers/nav-sales/order-detail/tabs/pick'

const { TabPane } = Tabs

type Props = {
  history: History
  currentUser: AuthUser
}

export class SalesOrderItems extends React.PureComponent<OrdersDispatchProps & OrdersStateProps & Props> {
  focusedIndex: number = 0
  prevIndex: number = 0
  timer: any = null
  bodyClicked: boolean = false
  oldRowKey: number = 0
  isFirstLoad: boolean = false
  headerInputFields: any = null
  accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)
  state = {
    visibleAddModal: false,
    activeKey: this.accountType != UserRole.WAREHOUSE ? '1' : '8',
    addChargeClicked: false,
    addProductModalVisible: false,
    confirmShipModal: false,
    selectedItems: [],
    insertItemPosition: -1,
    openChildModal: false,
    condensedMode: false,
    defaultProductName: '',
    orderItemsLength: this.props.orderItems.length,
  }
  cartRef = React.createRef<any>()

  toggleAddModal = () => {
    if (this.props.loading) return
    this.setState({
      visibleAddModal: !this.state.visibleAddModal,
      addChargeClicked: false,
    })
  }

  toggleOpenChildModal = (status: boolean) => {
    this.setState({ openChildModal: status })
  }

  setAddChargeClicked = () => {
    this.setState({
      addChargeClicked: true,
    })
  }

  componentDidMount() {
    if(!this.props.sellerSetting){
      this.props.getSellerSetting()
    }
    this.isFirstLoad = true
    setTimeout(() => {
      this.setTabOrder()
      this.setInitialFocus()
      this.selectInputWhenFocused()
    }, 50)
    localStorage.removeItem('trigger-by-keyboard')
    localStorage.removeItem('editing-row-key')
    window.localStorage.setItem('CLICKED-INDEX', `no_value`)
  }

  componentDidUpdate(prevProps: any) {
    if (!prevProps.currentOrder && this.props.currentOrder) {
      setTimeout(() => {
        this.setInitialFocus()
        this.selectInputWhenFocused()
      }, 50)
    }
    const orderHeaderInputId = localStorage.getItem("CASH-SALE-HEADER-INPUT-ID")
    if (orderHeaderInputId && localStorage.getItem('NEW_CASH_SALE') !== 'true') {
      return
    }
    const currentRows = jQuery('.sales-cart-table tbody tr').length
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'

    if (this.isFirstLoad && this.props.settingCompanyName) {
      const editableEls = jQuery('.sales-cart-table').find(
        '.replace-item-handler, .ant-input:not(.ant-select-search__field), .ant-btn, .pas .ant-checkbox-input, .ant-select-enabled .ant-select-selection, .ant-input-number-input:not(:disabled)',
      )
      if (
        !this.props.loadingSaleItems &&
        !this.props.loading &&
        !this.props.loadingAddress &&
        !this.props.loadingCurrentOrder &&
        editableEls.length > 0
      ) {
        //when finished loadings and table has editable elements, we can say page load is finished.
        //at this time, will handle focus to order qty(freshgreen) or additem autocomplete(non-freshgreen)
        this.isFirstLoad = false
        setTimeout(() => {
          if (isFreshGreen) {
            const lastOrderQty = jQuery('.sales-cart-table .order-quantity-input input:not(.ant-checkbox-input)').last()
            this.setFocusEl(lastOrderQty, editableEls, 13)
            this.setFocusedIndex(editableEls, lastOrderQty)
          } else {
            const addItemDropdown = jQuery('.add-item-btn-bottom input')
            this.setFocusEl(addItemDropdown, editableEls, 13)
            this.setFocusedIndex(editableEls, addItemDropdown)
          }
          if (localStorage.getItem('NEW_CASH_SALE') === 'true') {
            setTimeout(() => {
              jQuery('.receipt-no-input').trigger('focus')
              localStorage.removeItem('NEW_CASH_SALE')
            }, 50)
          }
        }, 500)
      }
    }

    if (prevProps.addingCartItem && !this.props.addingCartItem) {
      setTimeout(() => {
        const editableEls = jQuery('.sales-cart-table').find(
          '.replace-item-handler, .ant-input:not(.ant-select-search__field), .ant-btn, .pas .ant-checkbox-input, .ant-select-enabled .ant-select-selection, .ant-input-number-input:not(:disabled)',
        )

        if (editableEls[this.focusedIndex]) {
          const trObj = jQuery(editableEls[this.focusedIndex]).parents('tr')[0]
          if(trObj) {
            this.setFocusEl(editableEls[this.focusedIndex], editableEls, 13)
          }
        }
      }, 10)
    }

    if (prevProps.orderItems.length == this.props.orderItems.length - 1 &&
      prevProps.addingCartItem != this.props.addingCartItem &&
      this.props.addingCartItem === true) {
      localStorage.removeItem('trigger-by-keyboard')
      localStorage.removeItem('editing-row-key')
      setTimeout(() => {
        const editableEls = jQuery('.sales-cart-table').find(
          '.replace-item-handler, .ant-input:not(.ant-select-search__field), .ant-btn, .pas .ant-checkbox-input, .ant-select-enabled .ant-select-selection, .ant-input-number-input:not(:disabled)',
        )
        if (isFreshGreen) {
          const customFocusedSelect = jQuery('.sales-cart-table').find('.custom-ant-select-focused')
          jQuery.each(customFocusedSelect, (index: number, el: any) => {
            jQuery(el).removeClass('custom-ant-select-focused')
          })
          const focusedEls = jQuery('.sales-cart-table').find(
            '.ant-input:focus .ant-btn:focus, .ant-btn:focus, .pas .ant-checkbox-input:focus, .ant-input-number-input:focus',
          )

          jQuery.each(focusedEls, (index: number, el: any) => {
            jQuery(el).trigger('blur')
          })

          const uomParent = jQuery('.uom-select-container')
            .last()
            .parent()
          jQuery(uomParent).addClass('custom-ant-select-focused')
          this.setFocusedIndex(editableEls, uomParent)
        } else {
          const lastQty = jQuery('.sales-cart-table .order-quantity-input input:not(.ant-checkbox-input)').last()
          this.setFocusEl(lastQty, editableEls, 13)
          this.setFocusedIndex(editableEls, lastQty)
        }
        this.selectInputWhenFocused()
      }, 10)
    }

    // if (prevProps.orderItems.length == this.props.orderItems.length && this.props.orderItems.length == 0) {
    //   this.setFocusOnOrderQty()
    // }
    if (
      JSON.stringify(this.props.orderItems) != JSON.stringify(prevProps.orderItems) &&
      ((isFreshGreen && this.props.orderItems.length + 1 === currentRows) ||
        (!isFreshGreen && this.props.orderItems.length === currentRows))
    ) {
      setTimeout(() => {
        // focus handling after replace item
        const editableTableEls = jQuery('.sales-cart-table').find(
          '.replace-item-handler, .ant-input:not(.ant-select-search__field), .ant-btn, .pas .ant-checkbox-input, .ant-select-enabled .ant-select-selection, .ant-input-number-input:not(:disabled)',
        )
        const editingRowIndex = localStorage.getItem('editing-row-key')
        if (this.prevIndex > -1 && editableTableEls[this.prevIndex]) {
          const triggeredSource = localStorage.getItem('trigger-by-keyboard')
          let trObj
          if (triggeredSource === '1') {
            trObj = jQuery(editableTableEls[this.prevIndex]).parents('tr')
          } else if (editingRowIndex && editingRowIndex != 'null' && !isNaN(parseInt(editingRowIndex))) {
            trObj = jQuery('.sales-cart-table tbody tr')[parseInt(editingRowIndex)]
          }
          if (isFreshGreen) {
            if (trObj && jQuery(trObj).data('row-key') != this.oldRowKey) {
              if (triggeredSource === '1') {
                //if this is navigation by keyboard, focus by below method
                this.setFocusEl(editableTableEls[this.focusedIndex], editableTableEls, 13)
              } else {
                // or mouse click
                const uomParent = jQuery(trObj)
                  .find('.uom-select-container')
                  .last()
                  .parent()
                jQuery(uomParent).addClass('custom-ant-select-focused')
                const element = jQuery(uomParent)
                  .find('.ant-select-enabled .ant-select-selection')
                  .last()
                this.focusedIndex = jQuery(editableTableEls).index(element)
              }
            }
          } else {
            if (trObj && jQuery(trObj).data('row-key') != this.oldRowKey) {
              if (triggeredSource === '1') {
                //if this is navigation by keyboard, focus by below method
                this.setFocusEl(editableTableEls[this.focusedIndex], editableTableEls, 13)
              } else {
                // or mouse click
                const orderQty = jQuery(trObj)
                  .find('.sales-cart-table .order-quantity-input input:not(.ant-checkbox-input)')
                  .last()
                this.setFocusEl(orderQty, editableTableEls, 13)
                this.focusedIndex = jQuery(editableTableEls).index(element)
              }
            }
          }
        }
      }, 500)
    }
  }

  // setFocusOnOrderQty = () => {
  //   setTimeout(() => {
  //     jQuery('.order-quantity-input input')
  //       .last()
  //       .focus()
  //   }, 100)
  // }

  handleTabChange = (key: string) => {
    this.setState({ activeKey: key }, () => {
      setTimeout(() => {
        this.setInitialFocus()
        this.selectInputWhenFocused()
      }, 50)
      window.localStorage.setItem('CLICKED-INDEX', `no_value`)
    })
    const { currentOrder } = this.props
    if (key == '5' && currentOrder) {
      this.props.getAllOrderItemAdjustementsByOrderId(currentOrder.wholesaleOrderId)
    }
  }

  setInitialFocus = () => {
    this.focusedIndex = 0
    const _this = this
    this.headerInputFields = jQuery('.header-input-field input, input.header-input-field, textarea.header-input-field')
    jQuery('.header-input-field input, input.header-input-field, textarea.header-input-field').unbind().bind('focus', function (e) {
      _this.headerInputFields = jQuery('.header-input-field input, input.header-input-field, textarea.header-input-field')
      if (_this.focusedIndex > 0) {
        _this.bodyClicked = true
      }
      _this.focusedIndex = 0
      const id = jQuery(e.target).data('id')
      localStorage.setItem("CASH-SALE-HEADER-INPUT-ID", `${id}`)
    })
    const orderHeaderInputId = localStorage.getItem("CASH-SALE-HEADER-INPUT-ID")
    if (orderHeaderInputId) {
      const index = parseInt(orderHeaderInputId)
      if (index == 0 || index == 1) {
        jQuery(this.headerInputFields[index]).trigger('focus')
      }
    }
  }

  selectInputWhenFocused = () => {
    jQuery('.sales-cart-table .ant-input-number-input, .sales-cart-table .ant-input')
      .unbind('focus')
      .bind('focus', (e: any) => {
        jQuery(e.target).trigger('select')
        if (localStorage.getItem("CASH-SALE-HEADER-INPUT-ID")) {
          localStorage.removeItem("CASH-SALE-HEADER-INPUT-ID")
        }
      })
  }

  changeSelectedItems = (items: OrderItem[]) => {
    this.setState({ selectedItems: items })
  }

  triggerAndSelectByKeyCode = (el: any, keyCode: number) => {
    const isFocusedToUOM = jQuery(el).is(':focus')
    if (keyCode != 40 || !isFocusedToUOM) {
      //will not trigger because ant select will be triggered by down keyboard as default when it is focused
      jQuery(el).trigger('click')
    }

    setTimeout(() => {
      const tr = jQuery(el).parents('tr')
      const rowKey = jQuery(tr).data('row-key')
      const lis = jQuery(`.sales-cart-uom-${rowKey} li`)
      const selected = jQuery(`.sales-cart-uom-${rowKey} li.ant-select-dropdown-menu-item-selected`)
      const selectedIndex = jQuery(lis).index(selected)
      let navigateIndex = selectedIndex
      if (keyCode == 39 || keyCode == 40) {
        if (navigateIndex == lis.length - 1) {
          navigateIndex = 0
        } else {
          navigateIndex++
        }
      } else if (keyCode == 37 || keyCode == 38) {
        if (navigateIndex == 0) {
          navigateIndex = lis.length - 1
        } else {
          navigateIndex--
        }
      }
      jQuery.each(lis, (index: number, el: any) => {
        if (index == navigateIndex) {
          jQuery(el).addClass('ant-select-dropdown-menu-item-active')
        } else {
          jQuery(el).removeClass('ant-select-dropdown-menu-item-active')
        }
      })
    }, 10)
  }

  setFocusEl = (el: any, editables: any[] = [], keycode: number = -1) => {
    if (this.cartRef.current && this.cartRef.current.childRef.current.state.sendEmailModalVisible) return
    jQuery('.select-container-parent').removeClass('custom-ant-select-focused')
    jQuery('.ant-checkbox').removeClass('custom-ant-select-focused')
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'
    setTimeout(() => {
      if (jQuery(el).hasClass('ant-select-selection')) {
        if (isFreshGreen && (keycode == 13 || keycode == 9)) {
          const inputs = jQuery('.sales-cart-table input')
          jQuery.each(inputs, function (i: number, map: any) {
            jQuery(map).trigger('blur')
          })
          const parent = jQuery(el)
            .parent()
            .parent()
          if (jQuery(parent).hasClass('select-container-parent') || jQuery(parent).hasClass('ant-checkbox')) {
            jQuery(parent).addClass('custom-ant-select-focused')
          }
          if (jQuery(el).find('.ant-select-search__field').length) {
            jQuery(jQuery(el).find('.ant-select-search__field')[0]).trigger('focus')
          }
        } else if (!isFreshGreen && jQuery(el).find('.ant-select-search__field').length && keycode != 32) {
          jQuery(jQuery(el).find('.ant-select-search__field')[0]).trigger('focus')
        } else if ([37, 38, 39, 40].indexOf(keycode) > -1) {
          this.triggerAndSelectByKeyCode(el, keycode)
        } else if (!isFreshGreen) {
          jQuery(el).trigger('focus')
        }
      } else if (jQuery(el).hasClass('ant-checkbox-input')) {
        if (isFreshGreen && keycode == 13) {
          const parent = jQuery(el).parent()
          jQuery(parent).addClass('custom-ant-select-focused')
          jQuery.each(editables, function (i: number, map: any) {
            jQuery(map).trigger('blur')
          })
          jQuery(parent).trigger('focus')
        } else {
          jQuery(el).focus()
        }
      } else if (jQuery(el).hasClass('replace-item-handler')) {
        localStorage.setItem('trigger-by-keyboard', '1')
        jQuery(el).trigger('click')
      } else if (jQuery(el).hasClass('tab-able')) {
        if (!jQuery(el).hasClass('ant-btn')) {
          setTimeout(() => {
            jQuery(el)[0].click()
          }, 10)
        } else {
          setTimeout(() => {
            jQuery(el).trigger('focus')
          }, 10)
        }
      } else if (
        jQuery(el).hasClass('ant-btn') ||
        jQuery(el).hasClass('ant-input') ||
        jQuery(el).hasClass('ant-input-number-input')
      ) {
        jQuery(el).trigger('focus')
        jQuery(el).trigger('select')
      }
    }, 10)
  }

  setFocusedIndex = (editableTableEls: any[], curEl: any) => {
    const customFocusedSelect = jQuery('.sales-cart-table .custom-ant-select-focused')
    if (jQuery(editableTableEls).index(curEl) >= 0) {
      this.focusedIndex = jQuery(editableTableEls).index(curEl)
      jQuery.each(customFocusedSelect, (index: number, els: any) => {
        jQuery(els).removeClass('custom-ant-select-focused')
      })
    } else if (customFocusedSelect.length) {
      let childOfCustomSelect = jQuery(customFocusedSelect).find('.ant-select-selection')[0]
      if (!childOfCustomSelect) {
        childOfCustomSelect = jQuery(customFocusedSelect).find('.ant-checkbox-input')[0]
      }
      this.focusedIndex = jQuery(editableTableEls).index(childOfCustomSelect)
    }

    if (jQuery(curEl).hasClass('ant-select-search__field')) {
      const parent = jQuery(curEl).parents('.ant-select-selection')
      this.focusedIndex = jQuery(editableTableEls).index(parent)
    }
  }

  setTabOrder = () => {
    const _this: any = this
    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if ((
          _this.state.visibleAddModal ||
          _this.state.openChildModal ||
          _this.props.loading) &&
          (e.keyCode == 9 || e.keyCode == 13)
        ) {
          return
        }

        if (jQuery(e.target).hasClass('header-input-field')) {
          //do not run this method when cursor is on header input fields
          return
        }
        if (
          jQuery('.add-item-btn-bottom input').is(':focus') &&
          !_this.isAutoCompleteOpen() &&
          !e.shiftKey &&
          (e.keyCode == 9 || e.keyCode == 13)
        ) {
          e.preventDefault()
          return
        }
        // let actionFields = jQuery('.page-tab .ant-tabs-tabpane-active .sales-cart-input:not(:disabled)')
        let editableTableEls: any[] = []
        if (_this.state.activeKey == '1') {
          editableTableEls = jQuery('.sales-cart-table').find(
            '.replace-item-handler, .ant-input:not(.ant-select-search__field), .ant-btn, .pas .ant-checkbox-input, .ant-select-enabled .ant-select-selection, .ant-input-number-input:not(:disabled)',
          )
          if (_this.isReplaceItemDropdownOpen() && (e.keyCode == 9 || e.keyCode == 13)) {
            jQuery('.replace-dropdown-items-container .ant-select-dropdown-menu-item-active').trigger('click')
            this.oldRowKey = jQuery(e.target)
              .parents('tr')
              .data('row-key')
            _this.prevIndex = _this.focusedIndex
            const uom = jQuery(e.target)
              .parents('tr')
              .find('.uom-select-container .ant-select-selection')[0]
            _this.focusedIndex++
            this.setFocusEl(uom, editableTableEls, 13)
            return
          }
          this.setFocusedIndex(editableTableEls, e.target)
        } else {
          editableTableEls = jQuery('.page-tab .ant-tabs-tabpane-active .ant-table tbody tr .tab-able')
        }
        if (window.localStorage.getItem('CLICKED-INDEX')) {
          const clickedIndex = window.localStorage.getItem('CLICKED-INDEX')
          if (clickedIndex != '-1' && clickedIndex != 'no_value') {
            _this.focusedIndex = parseInt(clickedIndex, 10)
            window.localStorage.setItem('CLICKED-INDEX', '-1')
          }
        } else if (_this.bodyClicked) {
          _this.bodyClicked = false
          _this.focusedIndex = 0
          return
        }
        const isFreshGreen =
          _this.props.settingCompanyName === 'Fresh Green' || _this.props.settingCompanyName === 'Fresh Green Inc'
        if (e.keyCode == 9 && jQuery(e.target).parents('.add-item-btn-bottom').length && _this.isAutoCompleteOpen()) {
          e.preventDefault()
          jQuery('.enter-po-autocomplete .ant-select-dropdown-menu-item-active').trigger('click')
        } else if (e.keyCode == 9 || (isFreshGreen && e.keyCode == 13)) {
          if (e.shiftKey) {
            _this.focusedIndex--
            if (_this.focusedIndex < 0) {
              _this.focusedIndex = editableTableEls.length - 1
            }
            _this.setFocusEl(editableTableEls[_this.focusedIndex], editableTableEls, e.keyCode)
          } else {
            _this.focusedIndex++
            if (_this.focusedIndex < editableTableEls.length) {
              //   _this.focusedIndex = 0
              //   if (!_this.isAutoCompleteOpen()) {
              //     _this.setFocusEl(editableTableEls[0], editableTableEls, e.keyCode)
              //   }
              // } else {
              _this.setFocusEl(editableTableEls[_this.focusedIndex], editableTableEls, e.keyCode)
            }
          }
        } else if (
          (e.keyCode == 32 ||
            ([37, 38, 39, 40].indexOf(e.keyCode) > -1 &&
              isFreshGreen &&
              _this.isTriggableByArrow(editableTableEls[_this.focusedIndex])
            )
          ) && !_this.isItemNoteModalOpen() && !_this.isReplaceItemDropdownOpen()
        ) {
          _this.setFocusEl(editableTableEls[_this.focusedIndex], editableTableEls, e.keyCode)
        }
      })

    jQuery('body')
      .unbind('click')
      .bind('click', (e: any) => {
        if (jQuery(e.target).hasClass('tab-able')) {
          window.localStorage.setItem('CLICKED-INDEX', `no_value`)
        }
      })
  }

  isAutoCompleteOpen = () => {
    if (
      jQuery('body').find('.enter-po-autocomplete').length &&
      !jQuery('body').find('.enter-po-autocomplete.ant-select-dropdown-hidden').length
    ) {
      return true
    } else {
      return false
    }
  }

  isItemNoteModalOpen = () => {
    if (jQuery('body').find('.item-note-modal-body').length == 1 &&
      jQuery('body').find('.item-note-modal-body.hidden').length == 0
    ) {
      return true
    } else {
      return false
    }
  }

  isReplaceItemDropdownOpen = () => {
    const replaceBody = jQuery('body').find('.replace-dropdown-items-container')
    if (replaceBody.length && !jQuery(replaceBody).hasClass('replace-item-hidden')) {
      return true
    } else {
      return false
    }
  }

  isTriggableByArrow = (el: any) => {
    if (
      jQuery(el).parents('.uom-select-container').length &&
      (jQuery('body').find('.ant-select-dropdown').length == 0 ||
        jQuery('body').find('.ant-select-dropdown').length == jQuery('body').find('.ant-select-dropdown-hidden').length)
    ) {
      return true
    } else {
      return false
    }
  }

  addOffItem = () => {
    const { currentOrder, oneOffItems } = this.props
    if (!currentOrder) {
      return
    }
    let itemList: OrderItem[] = oneOffItems
      ? oneOffItems.filter((obj: any) => {
        if (!obj.isInventory) {
          obj.uom = obj.UOM
          return true
        } else {
          return false
        }
      })
      : []
    let newData = {
      wholesaleOrderItemId: '-' + (itemList.length + 1).toString(),
      constantRatio: true,
      cost: 0,
      picked: 0,
      price: 0,
      quantity: 0,
      status: 'SHIPPED',
      itemName: ' ',
      uom: 'each',
      orderId: currentOrder.wholesaleOrderId,
    }
    if (this.state.visibleAddModal) {
      this.toggleAddModal()
    }
    this.setActiveKey('2')
    this.setAddChargeClicked()
    setTimeout(() => {
      this.props.createOffItem(newData)
    }, 100)
  }

  setActiveKey = (key: string) => {
    this.setState({ activeKey: key })
  }

  onDoHeaderActions = (action: string) => {
    if (action === 'lock' || action === 'unlock') {
      this.cartRef.current.updateOrderLocked(action === 'lock')
    } else if (action === 'cancel') {
      this.props.startCanceling()
      setTimeout(() => {
        this.cartRef.current.handleOrderStatus('CANCEL')
      }, 100)
    } else {
      this.cartRef.current.handleOpenPreviewModal(action)
    }
  }

  onToggleProductModal = (creating: boolean) => {
    const addProductModalVisible = !this.state.addProductModalVisible
    if (creating && !addProductModalVisible) {
      this.toggleAddModal()
    }

    this.setState({
      addProductModalVisible,
    })
  }

  onShowProductModal = (searchText: string) => {
    this.setState(
      {
        defaultProductName: searchText,
      },
      () => {
        this.onToggleProductModal()
      },
    )
  }

  deleteCartItem = () => {
    const { selectedItems } = this.state
    // if (selectedItems.length != 1) return //TODO: need to make api to bulk delete
    if (!selectedItems.length) return
    let itemsList: any[] = []
    selectedItems.forEach((item: OrderItem) => {
      let orderItem: any = formatOrderItem(item)
      orderItem = { ...orderItem, deleted: true }
      itemsList.push(orderItem)
    })
    this.updateOrder(itemsList)
    this.setState({ selectedItems: [] })
  }

  updateOrder = (itemsList: any[]) => {
    const { currentOrder } = this.props
    if (!currentOrder) return
    const formData = {
      deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
      wholesaleOrderId: currentOrder.wholesaleOrderId,
      userId: currentOrder.user.userId,
      totalPrice: currentOrder.totalPrice,
      wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
      itemList: itemsList,
      status: currentOrder.wholesaleOrderStatus,
      isLocked: isArray(itemsList) && itemsList.length > 0 ? (itemsList[0].status == 'SHIPPED' ? true : false) : false,
    }
    this.props.updateOrder(formData)
  }

  shipAndLockOrder = (status: string) => {
    this.props.startUpdating()
    this.props.setIsShippedSwitchLoading()
    let itemsList: any[] = []
    const { orderItems, oneOffItems } = this.props
    const allItems = [...orderItems, ...oneOffItems]
    allItems.forEach((el) => {
      let item = formatOrderItem(el)
      item.status = status
      itemsList.push(item)
    })

    setTimeout(() => this.updateOrder(itemsList), 1000)
  }

  shippingFromModal = (setPicked:boolean) => {
    let nonPickedItems = this.getConfirmModalType()
    if (nonPickedItems.length) {
      const hasNotCatchWeight = this.getConfirmType(nonPickedItems)
      const nonPickedItemIds = nonPickedItems.map((el) => {
        return el.wholesaleOrderItemId
      })
      if (hasNotCatchWeight) {
        let itemsList: any[] = []
        this.props.orderItems.forEach((el) => {
          let item = formatOrderItem(el)
          item.status = 'SHIPPED'
          if (setPicked && nonPickedItemIds.indexOf(el.wholesaleOrderItemId) > -1) {
            item.picked = item.quantity
          }
          itemsList.push(item)
        })
        this.props.startUpdating()
        this.updateOrder(itemsList)
      }
    }
    this.setState({ confirmShipModal: false })
  }

  shippingOrder = (checked: Boolean) => {
    const { currentOrder, orderItems } = this.props
    //migration cann't handle this data,so we don't allow user to unshiped
    const hasOldCatchWeight = orderItems.filter((orderItem: any) => orderItem.oldCatchWeight).length > 0
    if (hasOldCatchWeight) {
      notify('warn', 'Warn', 'This data cannot be operated, please contact the system administrator')
      return
    }

    // this.setState({
    //   switchCheck: checked,
    // })
    if (currentOrder && currentOrder.wholesaleOrderStatus != 'SHIPPED') {
      this.toggleShipConfirmModal()
      // this.shipAndLockOrder('SHIPPED')
    } else {
      this.shipAndLockOrder('PICKING')
    }
  }

  toggleShipConfirmModal = () => {
    if (this.state.confirmShipModal) {
      this.setState({ confirmShipModal: false })
    } else {
      const nonPickedItems = this.getConfirmModalType()
      if (nonPickedItems.length) {
        // const hasNotCatchWeight = this.getConfirmType(nonPickedItems)
        // const storageType = localStorage.getItem('confirm-ship-message')
        // const storageTypeBoolean = storageType ? Number.parseInt(storageType) : 0
        // if (!hasNotCatchWeight || (!storageTypeBoolean && hasNotCatchWeight)) {
        this.setState({ confirmShipModal: true })
        // }
      } else {
        this.shipAndLockOrder('SHIPPED')
      }
    }
  }

  getConfirmModalType = () => {
    const { orderItems } = this.props
    let result: OrderItem[] = []
    if (this.props.sellerSetting && this.props.sellerSetting.company.isDisablePickingStep === true) {
      return []
    }
    orderItems.forEach((el: OrderItem) => {
      if (!el.catchWeightQty && el.status != 'PICKING') {
        if (!judgeConstantRatio(el) && el.quantity === 0) {
          // when catchweight items have order qty = 0 or empty, do not prompt user to enter catchweights
        } else {
          result.push(el)
        }
      }
    })
    if (
      this.props.sellerSetting &&
      this.props.sellerSetting.company.isDisablePickingStep === false &&
      !!result.length
    ) {
      return result
    }
    return []
  }

  getConfirmType = (nonOrderItems: OrderItem[]) => {
    let type = 1
    nonOrderItems.forEach((el) => {
      if (!judgeConstantRatio(el)) {
        type = 0
      }
    })
    return type
  }

  onChagneInserItemPosition = (value) => {
    this.setState({ insertItemPosition: value })
  }

  onToggleCondensedMode = () => {
    this.setState({
      condensedMode: !this.state.condensedMode,
    })
  }

  onClickAddCartItemButton = () => {
    if (!this.state.visibleAddModal) {
      this.toggleAddModal()
      this.setState({ insertItemPosition: -1 })
    }
  }

  render() {
    const {
      currentOrder,
      orderItems,
      oneOffItems,
      isShippedSwitchLoading,
      isShippedSwitchCheck,
      currentUser,
      sellerSetting,
    } = this.props
    const {
      visibleAddModal,
      addProductModalVisible,
      selectedItems,
      confirmShipModal,
      condensedMode,
      defaultProductName,
    } = this.state
    const nonPickedItems = this.getConfirmModalType()
    const type = this.getConfirmType(nonPickedItems)
    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)
    const isShipped = currentOrder && currentOrder.wholesaleOrderStatus === 'SHIPPED'
    const loadingOrderItems = orderItems.filter((orderItem: any) => {
      return orderItem.qtyUpdatedFlag ? true : false
    })

    const isForbidLotError = this.props?.sellerSetting?.company?.isForbidLotError

    const disableShip = isForbidLotError
      ? !isShippedSwitchCheck && orderItems.some(e => {
        if(Number(e.quantity)<=0 && Number(e.picked)<=0){
          return false
        }
        if(!e.lotId) { return true }
        if(e.picked > 0){
          if(e.lotAvailableQty + Number(e.quantity) < e.picked){
            return true
          }
          return false
        }
        return e.lotAvailableQty < 0
      } )
      : false

    return (
      <TabsWrapper className="page-tab">
        <Tabs
          defaultActiveKey={accountType != UserRole.WAREHOUSE ? '1' : '8'}
          onChange={this.handleTabChange}
          activeKey={this.state.activeKey}
        >
          {accountType != UserRole.WAREHOUSE && (
            <TabPane tab={`Cart (${orderItems.length})`} key="1">
              <div className="tab-header">
                <Flex className="v-center">
                  {selectedItems.length ? (
                    <>
                      <TableHeaderItem onClick={this.deleteCartItem} className="fill" style={{ marginRight: 22 }}>
                        <IconSVG
                          type="trash"
                          viewBox="0 0 24 24"
                          width="16"
                          height="16"
                          style={{ margin: '0 4px 2px -2px' }}
                        />
                        <ThemeColorSpan className="dark-color">Remote item{selectedItems.length > 1 ? 's' : ''}</ThemeColorSpan>
                      </TableHeaderItem>
                    </>
                  ) : (
                    <div />
                  )}
                </Flex>
                {currentOrder && currentOrder.wholesaleOrderStatus != 'CANCEL' && (
                  <Flex className="v-center">
                    <div style={{ marginRight: 40 }}>
                      <TableHeaderItem className="sales-cart-ship-toggle">
                        <Flex className="v-center">
                          <ThemeTextButton
                            className="bold"
                            style={{ marginRight: 15 }}
                            onClick={this.onToggleCondensedMode}
                          >
                            {condensedMode ? 'Return to edit mode' : 'Preview order'}
                          </ThemeTextButton>
                          <>
                            <TabHeaderBigSpan>
                              {currentOrder && currentOrder.wholesaleOrderStatus == 'SHIPPED'
                                ? 'Uncomplete to edit order'
                                : `Complete and lock order`}
                            </TabHeaderBigSpan>
                            <SwitchTooltip
                              showTip={disableShip}
                              switchProps={{
                                disabled: disableShip,
                                checked: isShippedSwitchCheck,
                                loading: this.props.updateOrderQuantityLoading.fetching || isShippedSwitchLoading || loadingOrderItems.length > 0,
                                onChange: this.shippingOrder,
                              }}
                              tooltipProps={{
                                title:
                                  'One or more items does not have a lot selected and/or one or more selected lots does not have enough inventory to fulfill the order/picked quantity',
                              }}
                            />
                          </>
                        </Flex>
                      </TableHeaderItem>
                    </div>
                    {currentOrder &&
                      currentOrder.wholesaleOrderStatus != 'SHIPPED' &&
                      currentOrder.wholesaleOrderStatus != 'CANCEL' &&
                      !condensedMode && (
                        <ThemeOutlineButton
                          onClick={this.onClickAddCartItemButton}
                          className="sales-cart-input add-item add-sales-cart-item header-last-tab bold-blink"
                        >
                          <Icon type="plus-circle" />
                          {sellerSetting?.company?.enableAddItemFromProductList?'Add item from full product list':'Add item'}
                        </ThemeOutlineButton>
                      )}
                  </Flex>
                )}
              </div>
              <ThemeModal
                title={`Add Item to Cart`}
                centered
                visible={visibleAddModal}
                onCancel={this.toggleAddModal}
                okButtonProps={{ style: { display: 'none' } }}
                cancelButtonProps={{ style: { display: 'none' } }}
                footer={<Button type='primary' onClick={this.toggleAddModal}>Done</Button>}
                width={'75%'}
                style={{ minWidth: 1000, maxHeight: '95vh' }}
                className="add-cart-item-modal"
              >
                <AddOrderItemModal
                  visible={visibleAddModal}
                  salesType="SELL"
                  onClose={this.toggleAddModal}
                  addOffItem={this.addOffItem}
                  showProductModal={this.onShowProductModal}
                  insertItemPosition={this.state.insertItemPosition}
                />
              </ThemeModal>
              {currentOrder && (
                <AddProductModal
                  default={defaultProductName}
                  reloadItems={this.props.getOrderItemsById}
                  orderId={currentOrder.wholesaleOrderId}
                  visible={addProductModalVisible}
                  onToggle={this.onToggleProductModal}
                />
              )}
              <SalesCart
                visibleAddItemModal={visibleAddModal}
                onlyContent={true}
                toggleModal={this.toggleAddModal}
                changeSelectedItems={this.changeSelectedItems}
                onChagneInserItemPosition={this.onChagneInserItemPosition}
                toggleOpenChildModal={this.toggleOpenChildModal}
                setShippingOrder={this.shippingOrder}
                disableShip={disableShip}
                condensedMode={condensedMode}
                {...this.props}
                ref={this.cartRef}
              />
            </TabPane>
          )}
          {accountType != UserRole.WAREHOUSE && (
            <TabPane tab={`Extra Charges (${oneOffItems?.length ?? 0})`} key="2">
              <div className="tab-header">
                <Flex className="v-center">
                  <h3>Extra Charges {`(${oneOffItems?.length ?? 0})`}</h3>
                </Flex>
              </div>
              <SalesCart
                onlyContent={true}
                extraCharge={true}
                isShipped={isShipped}
                {...this.props}
                addChargeClicked={this.state.addChargeClicked}
              />
            </TabPane>
          )}
          {(currentUser.accountType == UserRole.WAREHOUSE ||
            (sellerSetting && sellerSetting.company && sellerSetting.company.warehousePickEnabled)) && (
              <TabPane
                tab={`Pick${currentUser.accountType == UserRole.WAREHOUSE ? ' (' + orderItems.length + ')' : ''}`}
                key="8"
              >
                <SalesOrderPick disableShip={disableShip} shipOrder={this.shippingOrder} {...this.props} />
              </TabPane>
            )}
          {accountType != UserRole.WAREHOUSE && (
            <TabPane tab="Fulfillment" key="7">
              <Fulfillment {...this.props} isCashSale={true} />
            </TabPane>
          )}
          <TabPane tab="Documents" key="4">
            <SalesDocuments onlyContent={true} {...this.props} />
          </TabPane>
          {accountType != UserRole.SALES && accountType != UserRole.BUYER && (
            <TabPane tab="Profitability" key="5">
              <SalesMargins onlyContent={true} {...this.props} />
            </TabPane>
          )}
          {/* <TabPane
            tab={this.props.adjustments.length === 0 ? 'Credit Memo' : `Credit Memo (${this.props.adjustments.length})`}
            key="6"
          >
            <CreditMemo onlyContent={true} {...this.props} />
          </TabPane> */}
        </Tabs>
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              title={`${type == 0 ? 'Please enter catchweights prior to shipping' : 'Confirm picked quantities'}`}
              visible={confirmShipModal}
              bodyStyle={{ padding: 0 }}
              onCancel={this.toggleShipConfirmModal}
              className={`${cx(css(noPaddingFooter))}`}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={this.shippingFromModal.bind(this,true)}>
                    {type == 1 ? 'Update picked quantities and ship order' : 'OK'}
                  </ThemeButton>
                  {
                    type == 0 ? <Button onClick={this.toggleShipConfirmModal}>Cancel</Button>
                    : <><Button onClick={this.shippingFromModal.bind(this,false)}>Ship without updating quantities</Button>
                    <NoBorderButton onClick={this.toggleShipConfirmModal}>Cancel</NoBorderButton>   </>
                  }

                </DialogSubContainer>
              }
              width={610}
            >
              <ConfirmShippingModal nonPickedItems={nonPickedItems} type={type} />
            </ThemeModal>
          )}
        </ClassNames>
      </TabsWrapper>
    )
  }
}

export default SalesOrderItems
