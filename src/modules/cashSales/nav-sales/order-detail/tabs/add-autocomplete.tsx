import * as React from 'react'
import { useState } from 'react'
import { AutoComplete, Tooltip } from 'antd'
import { AddACWrapper } from '../../styles'
import { cloneDeep } from 'lodash'
import { combineUomForInventory, formatNumber, mathRoundFun, handlerNoLotNumber } from '~/common/utils'
import InventoryAsterisk from '~/components/inventory/inventoryAsterisk'
import { OrderDetail, OrderItem } from '~/schema'
import { History } from 'history'
import moment from 'moment'
import _ from 'lodash'
import jQuery from 'jquery'

const { Option } = AutoComplete

interface CartProps {
  currentOrder: OrderDetail
  orderItems: OrderItem[]
  loading: boolean
  isDisablePickingStep: boolean
  toggleModal: Function
  updateSaleOrderItem: Function
  updateSaleOrderItemWithoutRefresh: Function
  startUpdating: Function
  changeSelectedItems: Function
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  history: History
  getOrderItemsById: Function
  updateOrder: Function
  updateOrderItemsDisplayOrder: Function
  onChagneInserItemPosition: Function
  onChangeOrderItemPricingLogic: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  toggleOpenChildModal: Function
  visibleItemModal: boolean
  condensedMode: boolean
  updating: boolean
  copyOrderQty: Function
}

export const AddAutocomplete: React.SFC<CartProps> = (props: any) => {
  const [productSKUs, setProductSKUs] = useState<any[]>([])
  const [defaultActive, setDefaultActive] = useState(false)

  React.useEffect(() => {
    if (props.simplifyItems && props.simplifyItems.length > 0 && !localStorage.getItem('FIRST_OPENED_CART') && !localStorage.getItem("CASH-SALE-HEADER-INPUT-ID")) {
      localStorage.setItem('FIRST_OPENED_CART', 'TRUE')
      const addItemBtn = jQuery('.footer-add-item .ant-input.ant-select-search__field')
      addItemBtn.trigger('focus')
      addItemBtn.trigger('click')
    }
  }, [props.simplifyItems])

  const onProductSearch = (searchStr: string, type: string) => {
    if (searchStr.length < 2) {
      setProductSKUs([])
      return
    }

    const { simplifyItems } = props
    // tslint:disable-next-line:prefer-const
    let timeoutHandler: any = null
    let options: any[] = []
    let matches1: any[] = []
    let matches2: any[] = []
    let matches3: any[] = []
    if (simplifyItems && simplifyItems.length > 0) {
      simplifyItems.forEach((item, index) => {
        let row = item
        //   if (typeof item.wholesaleItem !== 'undefined') {
        //     row = { ...item, ...item.wholesaleItem }
        //     row.SKU = row.sku
        //   }
        //   if (typeof row.SKU === 'undefined') {
        //     row.SKU = row.sku
        //   }

        if (row[type] && row[type].toLowerCase().indexOf(searchStr.toLowerCase()) === 0) {
          // full match on 2nd filed
          matches2.push(formartOption(row, type))
        } else if (row.variety && row.variety.toLowerCase().indexOf(searchStr.toLowerCase()) === 0) {
          // full match on variety
          matches1.push(formartOption(row, type))
        } else if (
          (row[type] && row[type].toLowerCase().indexOf(searchStr.toLowerCase()) > 0) ||
          (type === 'SKU' && row.variety && row.variety.toLowerCase().indexOf(searchStr.toLowerCase()) > 0)
        ) {
          // others
          matches3.push(formartOption(row, type))
        }
      })
    }
    if (matches1) {
      matches1 = matches1.sort((a, b) => a.key.localeCompare(b.key))
    }
    if (matches2) {
      // matches2 = matches2.sort((a, b) => a[type].localeCompare(b[type]))
    }
    if (matches3) {
      matches3 = matches3.sort((a, b) => a.key.localeCompare(b.key))
    }

    // When searches items, Autocomplete automatically focuses on the last 1st item.
    // Fix: It always should focus on the 1st row regardless of last one.
    setProductSKUs([])
    setDefaultActive(false)
    const allMatches = [...matches2, ...matches1, ...matches3]
    setTimeout(() => {
      if (allMatches.length > 50) {
        setProductSKUs(allMatches.slice(0, 50))
      } else {
        setProductSKUs(allMatches)
      }
      if (timeoutHandler) {
        clearTimeout(timeoutHandler)
      }

      timeoutHandler = setTimeout(() => {
        setDefaultActive(true)
      }, 200)
    }, 50)
  }

  const formartOption = (row: any, type: string, begin: string) => {
    let productName = row[type]
    if (type === 'SKU') {
      const sku = row[type] ? `${row[type]}` : ''
      if (begin === 'sku') {
        productName = `${sku} ${row.variety}`
      } else {
        productName = `${row.variety} ${sku}`
      }
    }

    return {
      key: productName,
      value: row.itemId,
      text: (
        <AddACWrapper className="space-between">
          <div className="sku ellipsis">{row[type] ? row[type] : ''}</div>
          <div className="product-name ellipsis">{row.variety}</div>
          <div className="inventory ellipsis">
            {handlerNoLotNumber(row, 1)} {row.inventoryUOM}
            <InventoryAsterisk item={row} flag={1}></InventoryAsterisk> available ({handlerNoLotNumber(row, 2)}{' '}
            {row.inventoryUOM}
            <InventoryAsterisk item={row} flag={2}></InventoryAsterisk>&nbsp;on hand)
          </div>
        </AddACWrapper>
      ),
    }
  }
  const onSelectSKU = (itemId, option) => {
    const { simplifyItems, quantity, currentOrder, isDisablePickingStep, orderItems } = props
    const found = simplifyItems.find((item: any) => {
      return item.itemId == itemId
    })

    if (found) {
      // setProductSKUs([])

      // let newItem = cloneDeep(found)
      // if (newItem.wholesaleOrderItemId) {
      //   delete newItem.variety
      //   delete newItem.SKU
      //   delete newItem.provider
      //   delete newItem.itemId
      // } else {
      //   delete newItem.children
      // }
      // let itemsList: any[] = []
      // newItem.quantity = quantity ? parseFloat(quantity) : 0
      // itemsList.push(_formatNewItem(newItem))
      // _updateOrder(itemsList, found)

      const _orderItem = {
        ..._formatNewItem(found),
        quantity: quantity ? parseFloat(quantity) : 0,
        variety: found.variety,
        pricingUOM: found.defaultSellingPricingUOM,
        inventoryUOM: found.inventoryUOM,
        UOM: found.defaultSalesUnitUOM,
        SKU: found.SKU,
        price: found.price,
        wholesaleOrderItemId: 0,
        wholesaleProductUomList: found.wholesaleProductUomList,
      }
      props.updateOrderItemsRedux({orderItems: [...orderItems, {..._orderItem}], orderItemByProduct: {}})
      props.setAddingCartItem()
      props.addCashSalesCartOrderItem({
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        wholesaleItemId: found.wholesaleItem ? found.wholesaleItem.wholesaleItemId : found.itemId,
        lotId: found.lotId,
        quantity: quantity ? parseFloat(quantity) : 0,
        picked: isDisablePickingStep ? (quantity ? parseFloat(quantity) : 0) : 0,
        addFromInput: true,
      })
      if (!_.isUndefined(props.callBack)) {
        props.callBack()
      }
    }
  }
/*
  const _updateOrder = (itemsList: any[], found: any) => {
    const { currentOrder, insertItemPosition, orderItems } = props
    if (currentOrder) {
      if (itemsList.length > 0) {
        itemsList.map((item: any) => {
          item.wholesaleOrderId = currentOrder.wholesaleOrderId
        })
      }
      if (itemsList.length == 1 && typeof insertItemPosition != 'undefined' && insertItemPosition > -1) {
        itemsList[0]['displayOrder'] = insertItemPosition
      }
      const formData = {
        deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        userId: currentOrder.user.userId,
        totalPrice: currentOrder.totalPrice,
        wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
        itemList: itemsList,
        status: currentOrder.wholesaleOrderStatus,
      }
      const _orderItem = {
        ...itemsList[0],
        variety: found.variety,
        pricingUOM: found.inventoryUOM,
        inventoryUOM: found.inventoryUOM,
        UOM: found.inventoryUOM,
        price: found.price,
        wholesaleProductUomList: found.wholesaleProductUomList,
      }
      orderItems.push(_orderItem)
      // props.resetLoading()
      props.setAddingCartItem()
      props.addCashSalesCartOrderItem(formData)
      if (!_.isUndefined(props.callBack)) {
        props.callBack()
      }
    }
  }
*/
  const _formatNewItem = (newItem: any) => {
    const { isDisablePickingStep } = props
    let quotedPrice = ''
    let UOM = ''
    let lotId = ''
    let margin = 0
    if (typeof newItem.wholesaleOrderItemId === 'undefined') {
      // if coming from product catalogy
      // The quoted price will be cost*default_margin
      // Newitem is wholesaleItem is different with wholesaleOrderItem
      if (typeof newItem.wholesaleItem === 'undefined') {
        const markup = (newItem.defaultMargin ? newItem.defaultMargin : 0) / 100
        // if (newItem.constantRatio == true) {
        //   quotedPrice = formatNumber(newItem.cost ? ((1 + markup) * newItem.cost) / newItem.ratioUOM : 0, 2)
        // } else {
        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost : 0, 2)
        // }
        UOM = newItem.inventoryUOM
      } else {
        const markup = (newItem.margin ? newItem.margin : 0) / 100
        const freight = newItem.freight ? newItem.freight : 0
        if (newItem.uom == newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio == true) {
          quotedPrice = formatNumber(
            newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
            2,
          )
        } else {
          quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
        }
        UOM = newItem.UOM
      }
    } else {
      // if coming for lot
      // The quoted price will be cost of lot*lot_margin if it is greater than 0, if not lot*default_margin
      // quotedPrice = newItem.cost * newItem.lotMargin
      // if(quotedPrice <= 0) {
      //   quotedPrice = newItem.cost * newItem.wholesaleItem.defaultMargin
      // }
      const markup = (newItem.margin ? newItem.margin : 0) / 100
      const freight = newItem.freight ? newItem.freight : 0
      if (newItem.uom == newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio == true) {
        quotedPrice = formatNumber(
          newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
          2,
        )
      } else {
        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
      }
      if (newItem.uom) {
        UOM = newItem.uom
      }
      lotId = newItem.lotId
    }

    return {
      wholesaleItemId: newItem.wholesaleItem ? newItem.wholesaleItem.wholesaleItemId : newItem.itemId,
      wholesaleOrderItemId: null,
      quantity: newItem.quantity,
      picked: isDisablePickingStep ? newItem.quantity : '',
      cost: newItem.cost,
      price: quotedPrice,
      margin: margin,
      freight: 0,
      // status: newItem.wholesaleItem ? 'PLACED' : newItem.status,
      status: 'PLACED',
      UOM: UOM,
      lotId: lotId,
    }
  }

  return (
    <AutoComplete
      className="add-item-btn-bottom"
      // size="large"
      style={{ width: '100%' }}
      placeholder="Add Item..."
      dataSource={productSKUs}
      onSelect={onSelectSKU}
      onFocus={() => onProductSearch('', 'SKU')}
      onSearch={(v) => onProductSearch(v, 'SKU')}
      dropdownClassName="enter-po-autocomplete"
      optionLabelProp="key"
      defaultActiveFirstOption={defaultActive}
    />
  )
}
