import * as React from 'react'
import { useState, useEffect } from 'react'
import { Menu, Button, Icon } from 'antd'
import { Icon as IconSVG } from '~/components'
import { ThemeButton, ThemeModal, ThemeSpin } from '~/modules/customers/customers.style'
import {
  ItemHMargin4,
  GridTableWrapper,
  CartTableWrapper,
  FloatMenuItem,
  DialogSubContainer,
  noPaddingFooter,
} from '../../styles'
import AddOrderItemModal from '../modals/add-item'
import { cloneDeep } from 'lodash'
import AddItemNoteModal from '../modals/item-note'
import { ClassNames } from '@emotion/core'
import { formatNumber, formatOrderItem } from '~/common/utils'
import { OrderDetail, OrderItem } from '~/schema'
import { DragSortingTable } from '../components/draggable-table'
import ItemPrice from '../modals/item-price'
import PickedQuantity from '../modals/picked-quanitty'
import WorkOrderModal from '~/modules/manufacturing/processing-list/modals/wo-modal'
import { History } from 'history'
import moment from 'moment'
import jQuery from 'jquery'
import _ from 'lodash'
import LotsTable from '../../../sales/cart/tables/lots-table'
import { AddAutocomplete } from './add-autocomplete'
import PriceModal from '~/modules/customers/sales/cart/modals/price-modal'
import EditItemName from './../modals/edit-item-name'

interface CartProps {
  currentOrder: OrderDetail
  orderItems: OrderItem[]
  loading: boolean
  toggleModal: Function
  updateSaleOrderItem: Function
  updateSOItemForUOM: Function
  updateSaleOrderItemWithoutRefresh: Function
  startUpdating: Function
  changeSelectedItems: Function
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  history: History
  getOrderItemsById: Function
  updateOrder: Function
  updateOrderItemsDisplayOrder: Function
  onChagneInserItemPosition: Function
  onChangeOrderItemPricingLogic: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  toggleOpenChildModal: Function
  visibleItemModal: boolean
  condensedMode: boolean
  updating: boolean
  copyOrderQty: Function
  startUpdatingField: Function
  updatingField: boolean
  handlerOrderItemsRedux: Function
  handlerUpdateSOUOMRedux: Function
  addCashSalesCartOrderItem: Function
  removeOrderItemForOrder: Function
}

export const Cart: React.SFC<CartProps> = (props: any) => {
  const {
    currentOrder,
    orderItems,
    toggleModal,
    catchWeightValues,
    history,
    loading,
    visibleItemModal,
    updateSaleOrderItem,
    handlerOrderItemsRedux,
    handlerUpdateSOUOMRedux,
    updateSOItemForUOM,
    updateSaleOrderItemWithoutRefresh,
    startUpdating,
    changeSelectedItems,
    getWeightsByOrderItemIds,
    getOrderItemsById,
    updateOrder,
    updateOrderItemsDisplayOrder,
    updateOrderItemForLot,
    onChagneInserItemPosition,
    toggleOpenChildModal,
    onChangeOrderItemPricingLogic,
    onChangeOrderItemPricingLogicWithoutRefresh,
    condensedMode,
    updating,
    copyOrderQty,
    settingCompanyName,
    startUpdatingField,
    updatingField,
    updatingUOMField,
    sellerSetting,
    removeOrderItemForOrder,
    simplifyItemsByOrderId,
    simplifyItems,
  } = props
  const [dataSource, setDataSource] = useState(orderItems)
  const [openAddItemModal, setOpenAddItemModal] = useState(false)
  const [openItemNoteModal, setOpenItemNoteModal] = useState(false)
  const [openAddWOModal, setOpenAddWOModal] = useState(false)
  const [openItemPriceModal, setOpenItemPriceModal] = useState(false)
  const [openPriceModal, setOpenPriceModal] = useState(false)
  const [openPickedQuantityModal, setOpenPickedQuantityModal] = useState(false)
  const [openLotsModal, setOpenLotsModal] = useState(false)
  const [openEditItemNameModal, setOpenEditItemNameModal] = useState(false)
  const [selectedRows, setSelectedRows] = useState([])
  const [currentRecord, setCurrentRecord] = useState<OrderItem | null>(null)
  const [clickedMenuId, setClickedMenuId] = useState(-1)
  const [orderItemCount, setOrderItemCount] = useState(orderItems ? orderItems.length : 0)
  const pickedQuantityRef = React.useRef(null)
  const [isFreshGreen, setIsFreshGreen] = useState(false)

  useEffect(() => {
    if (settingCompanyName === 'Fresh Green' || settingCompanyName === 'Fresh Green Inc') {
      setIsFreshGreen(true)
    }
  }, [settingCompanyName])

  useEffect(() => {
    if (!updatingUOMField) {
      const priceColumn = jQuery('.sales-cart-table .cart-price .ant-input-number-input:focus')
      if (priceColumn) {
        setTimeout(() => {
          jQuery(priceColumn).trigger('select')
        }, 10)
      }
    }
  }, [updatingUOMField])

  useEffect(() => {
    setDataSource(orderItems)
    setOrderItemCount(orderItems.length)
    if (currentOrder) {
      checkAndUpdateDisplayOrder()
    }

    changeSelectedRows()

    if (currentRecord) {
      const found = orderItems.find((oi) => oi.wholesaleOrderItemId === currentRecord.wholesaleOrderItemId)
      setCurrentRecord(cloneDeep(found))
    }
  }, [orderItems])

  useEffect(() => {
    changeSelectedItems(selectedRows)
  }, [selectedRows.length])

  const changeSelectedRows = () => {
    let refreshedSelectedRows: any[] = []
    const oldSelectedIds = selectedRows.map((el) => {
      return el.wholesaleOrderItemId
    })
    orderItems.forEach((el) => {
      if (oldSelectedIds.indexOf(el.wholesaleOrderItemId) > -1) {
        refreshedSelectedRows.push(el)
      }
    })
    setSelectedRows(refreshedSelectedRows)
  }

  const checkAndUpdateDisplayOrder = () => {
    const dispalyOrders = orderItems.map((el: OrderItem) => {
      return el.displayOrder
    })
    const isDuplicated = checkDuplicates(dispalyOrders)

    if (isDuplicated) {
      let cloneItems = cloneDeep(orderItems)
      cloneItems.forEach((el: OrderItem, index: number) => {
        el.displayOrder = index
      })
      updateOrderItemsDisplayOrder({ itemsList: cloneItems })
    }
  }

  const checkDuplicates = (arr: any[]) => {
    let valuesAlreadySeen = []

    for (let i = 0; i < arr.length; i++) {
      let value = arr[i]
      if (valuesAlreadySeen.indexOf(value) !== -1) {
        return true
      }
      valuesAlreadySeen.push(value)
    }
    return false
  }

  const handleWorkorderCreated = () => {
    if (!currentOrder) return
    getOrderItemsById(currentOrder.wholesaleOrderId)
  }

  const validateOrderStatusIsShipped = () => {
    if (currentOrder && currentOrder.wholesaleOrderStatus == 'SHIPPED') {
      return true
    }
    return false
  }

  const renderFloatMenuItems = (orderItemId: string) => {
    let orderItem = orderItems.find((el: OrderItem) => {
      return el.wholesaleOrderItemId == orderItemId
    })

    return (
      <Menu onClick={menuClick.bind(this, orderItemId)}>
        <Menu.Item key={0}>
          <FloatMenuItem className="fill">
            <IconSVG type="trash" style={ItemHMargin4} viewBox="0 0 24 24" width={20} height={20} />
            Delete Item
          </FloatMenuItem>
        </Menu.Item>
        <Menu.Item key={1}>
          <FloatMenuItem>
            <IconSVG type="add-note" style={ItemHMargin4} viewBox="0 0 20 20" width={20} height={20} />
            Add a Note
          </FloatMenuItem>
        </Menu.Item>
        {!(orderItem && orderItem.workOrderStatus) && (
          <Menu.Item key={2} disabled={orderItem == null ? true : orderItem.UOM != orderItem.inventoryUOM}>
            <FloatMenuItem className="fill">
              <IconSVG type="add-wo" style={ItemHMargin4} viewBox="0 0 20 20" width={20} height={20} />
              Add a Work Order
            </FloatMenuItem>
          </Menu.Item>
        )}
      </Menu>
    )
  }

  const deleteOrderItem = (orderItemId: string) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    let orderItem = orderItems.find((el: OrderItem) => {
      return el.wholesaleOrderItemId == orderItemId
    })
    if (orderItem) {
      // orderItem = formatOrderItem(orderItem)
      // orderItem = { ...orderItem, deleted: true }
      // let itemsList: any[] = []
      // itemsList.push(orderItem)

      // const formData = {
      //   deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
      //   wholesaleOrderId: currentOrder.wholesaleOrderId,
      //   userId: currentOrder.user.userId,
      //   totalPrice: currentOrder.totalPrice,
      //   wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
      //   itemList: itemsList,
      //   status: currentOrder.wholesaleOrderStatus,
      // }
      // updateOrder(formData)
      removeOrderItemForOrder(orderItemId)
    }
  }

  const onToggleSelect = (type: string, e: any) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    if (type === 'All') {
      if (e.target.checked) {
        setSelectedRows(cloneDeep(dataSource))
      } else {
        setSelectedRows([])
      }
    } else if (dataSource.length) {
      const curRow = dataSource.find((el: OrderItem) => {
        return `${el.wholesaleOrderItemId}` == type
      })
      const existIndex = selectedRows.findIndex((el: OrderItem) => {
        return `${el.wholesaleOrderItemId}` == type
      })
      if (e.target.checked) {
        if (existIndex == -1) {
          setSelectedRows([...selectedRows, curRow])
        }
      } else {
        if (existIndex != -1) {
          selectedRows.splice(existIndex, 1)
          setSelectedRows(cloneDeep(selectedRows))
        }
      }
    }
  }

  const onCancelModal = (type: string) => {
    switch (type) {
      case 'openAddItemModal':
        setOpenAddItemModal(false)
        break
      case 'openItemNoteModal':
        setOpenItemNoteModal(false)
        break
      case 'openAddWOModal':
        setOpenAddWOModal(false)
        break
      case 'openItemPriceModal':
        setOpenItemPriceModal(false)
        break
      case 'openPickedQuantityModal':
        setOpenPickedQuantityModal(false)
        break
      case 'priceModal':
        setOpenPriceModal(false)
        break
      case 'lotsModal':
        setOpenLotsModal(false)
        break
      case 'openEditItemNameModal':
        setOpenEditItemNameModal(false)
        break
    }
    setClickedMenuId(-1)
    toggleOpenChildModal(false)
    if (type == 'openItemPriceModal' || type === 'openPickedQuantityModal') {
      return
    }
    setTimeout(() => {
      setCurrentRecord(null)
    }, 1000)
  }

  // const menuClick = ({ key }: ClickParam) => {
  const menuClick = (orderItemId: any, { key }: any) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    setClickedMenuId(orderItemId)
    const record = dataSource.find((el: any) => {
      return orderItemId == el.wholesaleOrderItemId
    })
    if (key == '0') {
      deleteOrderItem(orderItemId)
    } else if (key == '1') {
      if (record) {
        console.log(record)
        setCurrentRecord(record)
        toggleOpenChildModal(true)
        setOpenItemNoteModal(true)
      }
    } else if (key == '2') {
      if (record) {
        setCurrentRecord(record)
      }
      toggleOpenChildModal(true)
      setOpenAddWOModal(true)
    }
  }

  const openModal = (type: string, record: OrderItem) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    toggleOpenChildModal(true)
    switch (type) {
      case 'itemPriceModal':
        getOrderItemsById(currentOrder.wholesaleOrderId)
        console.log('* record = ', record)
        setCurrentRecord(record)
        setOpenItemPriceModal(true)
        break
      case 'pickedQuantityModal':
        setCurrentRecord(record)
        setOpenPickedQuantityModal(true)
        break
      case 'addNoteModal':
        setCurrentRecord(record)
        setOpenItemNoteModal(true)
        break
      case 'addWOModal':
        setClickedMenuId(record.wholesaleOrderItemId)
        setOpenAddWOModal(true)
        break
      case 'priceModal':
        setCurrentRecord(record)
        setOpenPriceModal(true)
        break
      case 'lotsModal':
        setCurrentRecord(record)
        setOpenLotsModal(true)
        break
      case 'openEditItemNameModal':
        setCurrentRecord(record)
        setOpenEditItemNameModal(true)
        break
    }
  }

  // const onChangeItemDetail = (type: string, value: any) => {
  //   if (validateOrderStatusIsShipped()) {
  //     return
  //   }
  //   if (currentRecord) {
  //     currentRecord[type] = value
  //   }
  //   setCurrentRecord(currentRecord)
  // }

  const handleRecordSave = (type: string) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    if (type === 'openPickedQuantityModal') {
      // const warnings = jQuery('.quantity-item .warning').length > 0
      // if (warnings) {
      //   return;
      // }
      startUpdating()
      pickedQuantityRef.current.handleSave()
    } else if (type == 'deleteItemNote' && currentRecord) {
      let row = { ...currentRecord, note: '' }
      onCancelModal('openItemNoteModal')
      handlerOrderItemsRedux(row)
      updateSaleOrderItem(row)
    } else if (currentRecord) {
      handlerOrderItemsRedux(currentRecord)
      updateSaleOrderItem(currentRecord)
    }

    onCancelModal(type)

    if (type !== 'openPickedQuantityModal') {
      setCurrentRecord(null)
    }
  }

  const onClickAddItem = (orderItemId: string) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    const item = orderItems.find((el: OrderItem) => {
      return el.wholesaleOrderItemId == orderItemId
    })
    const index = orderItems.findIndex((el: OrderItem) => {
      return el.wholesaleOrderItemId == orderItemId
    })
    onChagneInserItemPosition(item && item.displayOrder ? item.displayOrder : index)
    // setInsertingRowIndex(index + 1)
    toggleModal(true)
  }

  const onUpdateSaleOrderItem = (row: any, field: string) => {
    console.log('*** field = ', field)
    // startUpdatingField()
    if (field === 'uom' || field === 'picked') {
      // startUpdating()
    }

    if (field === 'quantity' || field === 'pas') {
      // When change quantity/pas/price, no need to refresh.
      // For price change, it uses updateOrderItemPricingLogic
      console.log('* No refresh = ', field)
      //hack for don't set note to null string
      delete row.note
      updateSaleOrderItemWithoutRefresh(row)
    } else {
      let newStatus = ''
      if (field === 'status') {
        if (row.status === 'PICKING' && currentOrder.status !== 'PICKING') {
          newStatus = 'PICKING'
        } else if (row.status === 'PLACED' && currentOrder.status !== 'NEW') {
          let allNew: boolean = true
          dataSource.forEach((item: any, index: number) => {
            if (item.wholesaleOrderItemId !== row.wholesaleOrderItemId) {
              if (item.status !== 'PLACED') {
                allNew = false
              }
            }
          })
          if (allNew) {
            newStatus = 'NEW'
          }
        }
      }

      if (newStatus) {
        updateSaleOrderItemWithoutRefresh(row)
        props.onUpdateOrderStatus('NEW')
      } else {
        if (field === 'uom') {
          // When modifying UOM, we need to call api to get the catchweights of the order item
          console.log('wholesaleOrderItemId === ', row.wholesaleOrderItemId)
          // handlerOrderItemsRedux(row)
          startUpdatingField()
          updateSOItemForUOM(row)
        } else {
          // handlerOrderItemsRedux(row)

          //hack for don't set note to null string
          delete row.note
          updateSaleOrderItem(row)
        }
      }
    }
  }

  const handleSelectLot = _.debounce((record: any) => {
    let row: any = { ...currentRecord }
    if (record) {
      if (record.lotId != row.lotId) {
        row['lotId'] = record.lotId
        const markup = (record.margin ? record.margin : 0) / 100
        const freight = record.freight ? record.freight : 0
        const cost = record.cost ? record.cost : 0
        if (record.uom == record.wholesaleItem.inventoryUOM && record.wholesaleItem.constantRatio == true) {
          row['price'] = formatNumber(cost ? (1 + markup) * (cost / record.wholesaleItem.ratioUOM) + freight : 0, 2)
        } else {
          row['price'] = formatNumber(cost ? (1 + markup) * cost + freight : 0, 2)
        }
        updateOrderItemForLot(row)
      }
    } else {
      row['lotType'] = 1
      row['lotId'] = null
      updateOrderItemForLot(row)
    }

    onCancelModal('lotsModal')
    setCurrentRecord(null)
  }, 200)

  // const changeOrderItemPricingLogic = (data: any) => {
  //   console.log(data)
  //   dataSource.find((el: any) => {
  //     if (el.wholesaleOrderItemId == data.wholesaleOrderItemId) {
  //       el.pricingUOM = data.UOM
  //       el.price = ratioPriceToBasePrice(data.UOM, data.price, el, 2)
  //       el.pricingGroup = data.pricingGroup
  //       el.pricingLogic = data.pricingLogic
  //     }
  //   })
  //   setDataSource(dataSource)
  //   onChangeOrderItemPricingLogic(data)
  // }

  const footer = (
    <div>
      <div className="footer-add-item" key={`add-ac-${dataSource.length}`}>
        {/* <ThemeOutlineButton style={{ border: 'none' }} onClick={toggleModal} className="add-item-btn-bottom">
          <Icon type="plus-circle" />
          Add Item
        </ThemeOutlineButton> */}
        <AddAutocomplete {...props} simplifyItems={sellerSetting?.company?.enableAddItemFromProductList? simplifyItemsByOrderId : simplifyItems} />
      </div>
    </div>
  )
  return (
    <div style={{ paddingRight: 20 }}>
      <GridTableWrapper>
        <CartTableWrapper className="sales-cart-table">
          <DragSortingTable
            dataSource={dataSource}
            rowKey={'wholesaleOrderItemId'}
            onRow={(record: OrderItem, index: number) => ({
              index,
              wholesaleOrderItemId: record.wholesaleOrderItemId,
            })}
            footer={condensedMode || currentOrder.wholesaleOrderStatus === 'CANCEL' ? null : footer}
            acProps={props}
            saleItems={props.saleItems}
            selectedRows={selectedRows}
            catchWeightValues={catchWeightValues}
            renderFloatingMenu={renderFloatMenuItems}
            openModal={openModal}
            openAddModal={toggleModal}
            onToggleSelect={onToggleSelect}
            updateRowItem={onUpdateSaleOrderItem}
            onClickAddItem={onClickAddItem}
            updateOrderItemsDisplayOrder={updateOrderItemsDisplayOrder}
            handlerOrderItemsRedux={handlerOrderItemsRedux}
            handlerUpdateSOUOMRedux={handlerUpdateSOUOMRedux}
            validateOrderStatusIsShipped={validateOrderStatusIsShipped}
            onChangeOrderItemPricingLogicWithoutRefresh={onChangeOrderItemPricingLogicWithoutRefresh}
            draggable={!condensedMode}
            currentOrder={currentOrder}
            updating={updating}
            updatingField={updatingField}
            copyOrderQty={copyOrderQty}
            openAddItemModal={openAddItemModal}
            isFreshGreen={isFreshGreen}
            {...props}
          />
        </CartTableWrapper>
      </GridTableWrapper>
      <ThemeModal
        title={`Add Item to Cart`}
        visible={openAddItemModal}
        onCancel={onCancelModal.bind(this, 'openAddItemModal')}
        okButtonProps={{ style: { display: 'none' } }}
        cancelButtonProps={{ style: { display: 'none' } }}
        width={'75%'}
        style={{ minWidth: 1000 }}
      >
        <AddOrderItemModal salesType="SELL" />
      </ThemeModal>
      <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`Item Notes`}
            visible={openItemNoteModal}
            onCancel={onCancelModal.bind(this, 'openItemNoteModal')}
            okText={'Save'}
            cancelText={'Cancel'}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={
              <DialogSubContainer>
                <ThemeButton type="primary" onClick={handleRecordSave.bind(this, 'openItemNoteModal')}>
                  Save
                </ThemeButton>
                <Button onClick={handleRecordSave.bind(this, 'deleteItemNote')}>Delete Note</Button>
                <Button onClick={onCancelModal.bind(this, 'openItemNoteModal')}>CANCEL</Button>
              </DialogSubContainer>
            }
            width={600}
          >
            <AddItemNoteModal record={currentRecord} setCurrentRecord={setCurrentRecord} open={openItemNoteModal} />
          </ThemeModal>
        )}
      </ClassNames>
      {/* <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`Work Order Request`}
            visible={openAddWOModal}
            onCancel={onCancelModal.bind(this, 'openAddWOModal')}
            okText={'Save'}
            cancelText={'Cancel'}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={
              <DialogSubContainer>
                <ThemeButton type="primary">
                  Save
            </ThemeButton>
                <Button onClick={onCancelModal.bind(this, 'openAddWOModal')}>CANCEL</Button>
              </DialogSubContainer>
            }
            width={600}
          >
            <WorkOrderRequest />
          </ThemeModal>
        )}
      </ClassNames> */}
      <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`Item Price`}
            visible={openItemPriceModal}
            onCancel={onCancelModal.bind(this, 'openItemPriceModal')}
            okText={'Save'}
            cancelText={'Cancel'}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={null}
            // footer={
            //   <DialogSubContainer>
            //     <ThemeButton type="primary" onClick={handleRecordSave.bind(this, 'openItemPriceModal')}>
            //       Save
            //     </ThemeButton>
            //     <Button onClick={onCancelModal.bind(this, 'openItemPriceModal')}>CANCEL</Button>
            //   </DialogSubContainer>
            // }
            width={700}
          >
            <ItemPrice
              changeOrderItemPrice={onChangeOrderItemPricingLogic}
              changeModalStatus={onCancelModal}
              record={currentRecord}
              order={currentOrder}
            />
          </ThemeModal>
        )}
      </ClassNames>
      <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`Picked Quantities (catchweight item)`}
            visible={openPickedQuantityModal}
            onCancel={onCancelModal.bind(this, 'openPickedQuantityModal')}
            okText={'Save'}
            cancelText={'Cancel'}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={
              <DialogSubContainer>
                <ThemeButton type="primary" onClick={handleRecordSave.bind(this, 'openPickedQuantityModal')}>
                  <ThemeSpin spinning={loading} className="button-spinner" size={'small'} />
                  Save
                </ThemeButton>
                <Button onClick={onCancelModal.bind(this, 'openPickedQuantityModal')}>CANCEL</Button>
              </DialogSubContainer>
            }
            width={700}
          >
            <PickedQuantity
              record={currentRecord}
              open={openPickedQuantityModal}
              wrappedComponentRef={pickedQuantityRef}
            />
          </ThemeModal>
        )}
      </ClassNames>
      <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`${currentRecord ? currentRecord.variety : ''}`}
            visible={openEditItemNameModal}
            onCancel={onCancelModal.bind(this, 'openEditItemNameModal')}
            okText={'Save'}
            cancelText={'Cancel'}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={
              <DialogSubContainer>
                <ThemeButton type="primary" onClick={handleRecordSave.bind(this, 'openEditItemNameModal')}>
                  <ThemeSpin spinning={loading} className="button-spinner" size={'small'} />
                  Save
                </ThemeButton>
                <Button onClick={onCancelModal.bind(this, 'openEditItemNameModal')}>CANCEL</Button>
              </DialogSubContainer>
            }
            width={400}
          >
            {openEditItemNameModal && (
              <EditItemName record={currentRecord} open={openEditItemNameModal} setCurrentRecord={setCurrentRecord} />
            )}
          </ThemeModal>
        )}
      </ClassNames>
      {currentOrder !== null && (
        <WorkOrderModal
          orderId={currentOrder.wholesaleOrderId}
          orderItemId={clickedMenuId}
          quantity={currentRecord ? currentRecord.quantity : 0}
          isSpecial={true}
          visible={openAddWOModal}
          handleOk={onCancelModal.bind(this, 'openAddWOModal')}
          onWordOrderCreated={handleWorkorderCreated}
          history={history}
          handleCancel={onCancelModal.bind(this, 'openAddWOModal')}
        />
      )}
      <ThemeModal
        title={'Lots'}
        keyboard={true}
        visible={openLotsModal}
        onOk={onCancelModal.bind(this, 'lotsModal')}
        onCancel={onCancelModal.bind(this, 'lotsModal')}
        okText="Close [esc]"
        okButtonProps={{ shape: 'round' }}
        cancelButtonProps={{ style: { display: 'none' } }}
        width={'75%'}
      >
        <LotsTable
          itemId={currentRecord ? currentRecord.itemId : 0}
          lotId={currentRecord ? currentRecord.lotId : 0}
          onSelect={handleSelectLot}
        />
      </ThemeModal>
      <ThemeModal
        className="price-modal"
        title="PRICING INFORMATION"
        closeIcon={<Icon type="close" style={{ fontSize: '20px', color: 'white' }} />}
        keyboard={true}
        visible={openPriceModal}
        onOk={onCancelModal.bind(this, 'priceModal')}
        onCancel={onCancelModal.bind(this, 'priceModal')}
        okText="Close [esc]"
        okButtonProps={{ shape: 'round' }}
        cancelButtonProps={{ style: { display: 'none' } }}
        width={1330}
        footer={null}
        style={{ height: 868 }}
        centered
      >
        <PriceModal
          itemId={currentRecord ? currentRecord.itemId : 0}
          clientId={currentOrder ? currentOrder.wholesaleClient.clientId.toString() : ''}
        />
      </ThemeModal>
    </div>
  )
}
