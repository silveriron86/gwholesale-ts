import * as React from 'react'
import { Button, Icon, Input, Select, Table } from 'antd'
import { Flex } from '../../styles'
import { formatNumber } from '~/common/utils'

interface ExtraChargeProps {}

export default class ExtraCharge extends React.PureComponent<ExtraChargeProps> {
  render() {
    const dataSource = [
      {
        name: 'Palettes',
        uom: 'Each',
        quantity: 94,
        quoted: 22,
        total: 45.12,
      },
      {
        name: 'Freight charge',
        uom: 'Each',
        quantity: 168,
        quoted: 12.98,
        total: 51.92,
      },
      {
        name: 'Temperature sensor',
        uom: 'Each',
        quantity: 5,
        quoted: 13.45,
        total: 67.25,
      },
    ]

    const columns = [
      {
        title: 'Charge',
        dataIndex: 'name',
      },
      {
        title: 'UOM',
        dataIndex: 'uom',
        render: (value: string, record: any) => {
          return (
            <Select value={value} style={{ width: 75 }}>
              <Select.Option value="Each">Each</Select.Option>
            </Select>
          )
        },
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity',
        render: (value: string, record: any) => {
          return <Input defaultValue={value} style={{ width: 73 }} />
        },
      },
      {
        title: 'Quoted Price',
        dataIndex: 'quoted',
        render: (value: string, record: any) => {
          return (
            <div style={{ color: '#82898A' }}>
              <span>$</span> <Input defaultValue={value} style={{ width: 73 }} /> <span>/ each</span>
            </div>
          )
        },
      },
      {
        title: 'Total Price',
        dataIndex: 'total',
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
    ]

    const rowSelection = {
      getCheckboxProps: (record: any) => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    }

    return (
      <>
        <div className="tab-header">
          <Flex className="v-center">
            <h3>Extra Charges (3)</h3>
            <label>3 charges</label>
          </Flex>
          <div>
            <Button>
              <Icon type="plus-circle" /> Add a Charge
            </Button>
          </div>
        </div>
        <Table rowSelection={rowSelection} dataSource={dataSource} columns={columns} />
      </>
    )
  }
}
