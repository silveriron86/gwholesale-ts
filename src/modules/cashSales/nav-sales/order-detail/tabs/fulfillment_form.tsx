import * as React from 'react'
import { Row, Col, Radio, Button, TimePicker, Select, Form, AutoComplete, Input, Icon, Divider } from 'antd'
import moment from 'moment'
import {
  ThemeInput,
  ThemeRadio,
  ThemeSelect,
  ThemeTextArea,
  ThemeButton,
  ThemeModal,
} from '~/modules/customers/customers.style'
import { Flex } from '~/modules/orders/order-detail.style'
import { Icon as IconSvg } from '~/components/icon'
import { calculateTotalOrder, formatAddress, formatDate, formatNumber } from '~/common/utils'
import { OrderDetail, MainAddress, OrderItem, WholesaleRoute, MainContact, UserRole } from '~/schema'
import { mb0 } from '~/modules/customers/sales/_style'
import { FormComponentProps } from 'antd/lib/form'
import { CACHED_NS_LINKED, FINANCIAL_TERMS, CACHED_ACCOUNT_TYPE } from '~/common'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'

interface FulfillmentFormProps {
  onSave: Function
  onChangeFulfillmentOptionType: Function
  getCompanyProductAllTypes: Function
  currentOrder: OrderDetail
  addresses: MainAddress[]
  currentCompanyUsers: any[]
  orderItems: OrderItem[]
  oneOffItems: any[]
  companyProductTypes: any
  shippingAddresses: any[]
  fulfillmentOptionType: number
  routes: WholesaleRoute[]
  customerContacts: MainContact[]
}

const FormItem = Form.Item
const folderIcon = <IconSvg viewBox="0 0 24 24" width="24" height="24" type="folder" style={{ fill: 'transparent' }} />

class FulfillmentForm extends React.PureComponent<FulfillmentFormProps & FormComponentProps> {
  constructor(props: any) {
    super(props)
  }

  state = {
    type: this.props.currentOrder ? this.props.currentOrder.fulfillmentType : 1,
    originAddresses: [],
    contacts: [],
    deliveryPhones: [],
    visibleFreightTypeModal: false,
    visibleShippingTermsModal: false,
    currentOrder: this.props.currentOrder,
  }

  componentDidMount() {
    if (this.props.currentOrder) {
      if (this.props.shippingAddresses) {
        this.setState({
          originAddresses: this.props.shippingAddresses,
        })
      }
      if (this.props.customerContacts) {
        this.setState({
          contacts: this.props.customerContacts,
          deliveryPhones: this.getDeliveryPhoneNumbers(this.props.customerContacts),
        })
      }
      this.props.onChangeFulfillmentOptionType(this.props.currentOrder.fulfillmentType)
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.shippingAddresses && nextProps.shippingAddresses) {
      if (this.props.shippingAddresses.length != nextProps.shippingAddresses.length) {
        this.setState({
          originAddresses: nextProps.shippingAddresses,
        })
      }
    }
    if (this.props.customerContacts && nextProps.customerContacts) {
      if (this.props.customerContacts.length != nextProps.customerContacts.length) {
        this.setState({
          contacts: nextProps.customerContacts,
          deliveryPhones: this.getDeliveryPhoneNumbers(nextProps.customerContacts),
        })
      }
    }
    if (JSON.stringify(this.props.currentOrder) != JSON.stringify(nextProps.currentOrder)) {
      this.setState({
        currentOrder: nextProps.currentOrder,
        type: nextProps.currentOrder.fulfillmentType,
      })
      this.props.form.resetFields(['defaultRoute'])
    }
  }

  getDeliveryPhoneNumbers = (contacts: MainContact[]) => {
    const { currentOrder } = this.state
    if (!currentOrder) return []
    const customerMainPhone = currentOrder.wholesaleClient.mobilePhone
    const customerAlternativePhone = currentOrder.wholesaleClient.alternativePhone
    const customerContactPhones = contacts.map((el) => {
      return el.mobilePhone
    })
    console.log(contacts)
    console.log(customerAlternativePhone, customerMainPhone, customerContactPhones)
    return [...customerContactPhones, customerMainPhone, customerAlternativePhone]
  }

  onRadioChange = (e: any) => {
    this.setState({
      type: e.target.value,
    })
    this.props.onChangeFulfillmentOptionType(e.target.value)
  }

  getActiveRoutes = () => {
    const { currentOrder, routes } = this.props
    let activeRoutes: any[] = []
    if (currentOrder !== null && routes.length > 0) {
      routes.forEach((route) => {
        const weekDay = moment(currentOrder.deliveryDate)
          .format('dddd')
          .toUpperCase()
        if (currentOrder && route.activeDays.indexOf(weekDay) >= 0) {
          activeRoutes.push({ ...route })
        }
      })
    }
    if (activeRoutes.length > 0) {
      return activeRoutes
    }
    return []
  }

  handleSubmit = (e: any) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values)
      }
      console.log(values)
      this.props.onSave(values)
    })
  }

  handleSearchAddress = (search: string, type: string) => {
    if (type == 'address') {
      const { shippingAddresses } = this.props
      const filtered = shippingAddresses.filter((el) => {
        return (
          formatAddress(el.address)
            .toLowerCase()
            .indexOf(search.toLowerCase()) > -1
        )
      })
      this.setState({ originAddresses: filtered })
    }
    //  else if (type == 'contact') {
    //   const { customerContacts } = this.props
    //   const filtered = customerContacts.filter(el => { return el.name.toLowerCase().indexOf(search.toLowerCase()) > -1 })
    //   this.setState({ contacts: filtered })
    // } else if (type == 'delivery_phone') {
    //   const { customerContacts } = this.props
    //   const filtered = customerContacts.filter(el => { return el.name.toLowerCase().indexOf(search.toLowerCase()) > -1 })
    //   this.setState({ contacts: filtered })
    // }
  }

  openCompanySettingChange = (type: string) => {
    console.log('val', type)
    let state = { ...this.state }
    state[type] = true
    this.setState(state)
  }

  onCompanySettingTypeModal = (type: string) => {
    let state = { ...this.state }
    state[type] = !this.state[type]
    this.setState(state)
    this.props.getCompanyProductAllTypes()
  }

  onDeliveryAddressChange = (value: any) => {
    const { addresses } = this.props
    const activeRoutes = this.getActiveRoutes()
    const shippingAddress = addresses.find((el) => {
      return el.wholesaleAddressId == value
    })
    if (shippingAddress && shippingAddress.wholesaleRouteDetail && shippingAddress.wholesaleRouteDetail.route) {
      const route = activeRoutes.find((el) => {
        return el.id == shippingAddress.wholesaleRouteDetail.route.id
      })
      if (route) {
        this.props.form.setFieldsValue({ defaultRoute: route.id })
      } else {
        this.props.form.setFieldsValue({ defaultRoute: undefined })
      }
    } else {
      this.props.form.setFieldsValue({ defaultRoute: undefined })
    }
  }

  render() {
    const {
      currentOrder,
      type,
      originAddresses,
      contacts,
      deliveryPhones,
      visibleFreightTypeModal,
      visibleShippingTermsModal,
    } = this.state
    const removeDuplidatePhones = deliveryPhones.filter((el, index) => {
      return deliveryPhones.indexOf(el) == index
    })
    const {
      addresses,
      currentCompanyUsers,
      orderItems,
      companyProductTypes,
      shippingAddresses,
      fulfillmentOptionType,
    } = this.props
    const isLocked = currentOrder.wholesaleOrderStatus === 'SHIPPED'
    let defaultAddress = ''
    if (shippingAddresses && shippingAddresses.length > 0) {
      let record = shippingAddresses.find((a: any) => a.isMain === true)
      if (typeof record === 'undefined') {
        record = shippingAddresses[0]
      }
      defaultAddress = `${record.address.department ? record.address.department : ''} ${record.address.street1} ${
        record.address.city
      } ${record.address.state}, ${record.address.zipcode} ${record.address.country}`
    }

    const { getFieldDecorator } = this.props.form
    const activeRoutes = this.getActiveRoutes()

    let deliveryAddresses: MainAddress[] = []
    let billingAddresses: MainAddress[] = []
    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        deliveryAddresses.push(address)
      } else if (address.addressType === 'BILLING') {
        billingAddresses.push(address)
      }
    })

    let totalUnit = 0
    let totalQuantity = 0
    if (orderItems && orderItems.length > 0) {
      orderItems.forEach((item: any) => {
        totalUnit += item.picked
        totalQuantity += item.catchWeightQty
      })
    }

    const deliveryAddressItem = (
      <Flex className="v-center">
        <FormItem style={{ flex: 1 }}>
          {getFieldDecorator('shippingAddressId', {
            rules: [{ required: false }],
            initialValue:
              currentOrder && deliveryAddresses.length && currentOrder.shippingAddress
                ? currentOrder.shippingAddress.wholesaleAddressId
                : '',
          })(
            <ThemeSelect
              style={{ flex: 1 }}
              showSearch
              optionFilterProp="children"
              onChange={this.onDeliveryAddressChange}
              onSearch={() => {}}
              filterOption={(input: any, option: any) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              disabled={isLocked}
            >
              {deliveryAddresses.map((el: MainAddress, index: number) => {
                if (el.address) {
                  return (
                    <Select.Option key={`sa-${index}`} value={el.wholesaleAddressId}>
                      {formatAddress(el.address)}
                    </Select.Option>
                  )
                }
              })}
            </ThemeSelect>,
          )}
        </FormItem>
        <Button disabled={isLocked} href={`#/customer/${currentOrder.wholesaleClient.clientId}/addresses`} type="link">
          {folderIcon}
        </Button>
      </Flex>
    )

    const enabledNS = localStorage.getItem(CACHED_NS_LINKED) != 'null' && localStorage.getItem(CACHED_NS_LINKED) != null
    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)

    const getFreightType = () => {
      // if (!enabledNS) {
      if(1) {
        return (
          <>
            <div className="label mt28">Freight Type</div>
            <FormItem style={{ width: 250 }}>
              {getFieldDecorator('freightType', {
                rules: [{ required: false }],
                initialValue: currentOrder.freightType,
              })(
                <ThemeSelect
                  style={{ position: 'relative' }}
                  showSearch
                  optionFilterProp="children"
                  onSearch={() => {}}
                  filterOption={(input: any, option: any) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  disabled={isLocked}
                  dropdownRender={(menu: any) => (
                    <div>
                      {menu}
                      <Divider style={{ margin: '4px 0' }} />
                      <div
                        style={{ padding: '4px 8px', cursor: 'pointer' }}
                        onMouseDown={(e) => e.preventDefault()}
                        onClick={(e) => this.openCompanySettingChange('visibleFreightTypeModal')}
                      >
                        <Icon type="plus" /> Add New Freight Type...
                      </div>
                    </div>
                  )}
                >
                  {companyProductTypes.freightTypes.sort((a: any, b: any) => a.name.localeCompare(b.name)).map(
                    (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                      return (
                        <Select.Option key={`freight-type-${index}`} value={item.name}>
                          {item.name}
                        </Select.Option>
                      )
                    },
                  )}
                </ThemeSelect>,
              )}
            </FormItem>
          </>
        )
      } else {
        return <></>
      }
    }

    return (
      <div>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
          <Row>
            <Col span={12}>
              <div className="label">Billing Address</div>
              <Flex className="v-center">
                <FormItem style={{ flex: 1 }}>
                  {getFieldDecorator('billingAddressId', {
                    rules: [{ required: false }],
                    initialValue:
                      currentOrder && billingAddresses.length && currentOrder.billingAddress
                        ? currentOrder.billingAddress.wholesaleAddressId
                        : '',
                  })(
                    <ThemeSelect
                      style={{ flex: 1 }}
                      showSearch
                      optionFilterProp="children"
                      onSearch={() => {}}
                      filterOption={(input: any, option: any) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                      disabled={isLocked}
                    >
                      {billingAddresses.map((el: MainAddress, index: number) => {
                        if (el.address) {
                          return (
                            <Select.Option key={`ba-${index}`} value={el.wholesaleAddressId}>
                              {formatAddress(el.address)}
                            </Select.Option>
                          )
                        }
                      })}
                    </ThemeSelect>,
                  )}
                </FormItem>
                <Button
                  disabled={isLocked}
                  href={`#/customer/${currentOrder.wholesaleClient.clientId}/addresses`}
                  type="link"
                >
                  {folderIcon}
                </Button>
              </Flex>
              <div className="label mt28">Number Of Pallets</div>
              <FormItem style={{ flex: 1, width: 250 }}>
                {getFieldDecorator('numberOfPallets', {
                  rules: [{ required: false }],
                  initialValue: currentOrder.numberOfPallets ? currentOrder.numberOfPallets : '',
                })(<ThemeInput type="number" disabled={isLocked} />)}
              </FormItem>
              <hr />

              <div className="label mt28"></div>
              <FormItem>
                {getFieldDecorator('fulfillmentType', {
                  rules: [{ required: false }],
                  initialValue: currentOrder.fulfillmentType ? currentOrder.fulfillmentType : 1,
                })(
                  <Radio.Group onChange={this.onRadioChange} disabled={isLocked}>
                    <ThemeRadio value={1}>Delivery</ThemeRadio>
                    <ThemeRadio value={2}>Will-Call</ThemeRadio>
                    <ThemeRadio value={3}>Logistics</ThemeRadio>
                  </Radio.Group>,
                )}
              </FormItem>
              {/* {type === 1 && (
              <Button type="link" className="left select-delivery">
                <IconSvg viewBox="0 0 16 15" width="16" height="15" type="delivery" style={{ fill: 'transparent' }} />
                Select Delivery Window(s)
              </Button>
            )} */}
              <div className="clearfix" />

              <div style={{ display: fulfillmentOptionType === 1 ? 'block' : 'none' }}>
                <div className="label mt28">Delivery Address</div>
                {deliveryAddressItem}

                <div className="label mt28">Route</div>
                <Flex className="v-center">
                  <FormItem style={{ flex: 1 }}>
                    {getFieldDecorator('defaultRoute', {
                      rules: [{ required: false }],
                      initialValue: currentOrder.overrideRoute
                        ? currentOrder.overrideRoute.id
                        : currentOrder.defaultRoute
                        ? currentOrder.defaultRoute.id
                        : '',
                    })(
                      <ThemeSelect
                        style={{ flex: 1 }}
                        disabled={isLocked}
                        defaultValue={
                          currentOrder.overrideRoute
                            ? currentOrder.overrideRoute.id
                            : currentOrder.defaultRoute
                            ? currentOrder.defaultRoute.id
                            : ''
                        }
                        value={
                          currentOrder.overrideRoute
                            ? currentOrder.overrideRoute.id
                            : currentOrder.defaultRoute
                            ? currentOrder.defaultRoute.id
                            : ''
                        }
                      >
                        <Select.Option value={0}>No Driver/Route Selected</Select.Option>
                        {activeRoutes.map((route, index) => {
                          return (
                            <Select.Option key={`route=${index}`} value={route.id}>
                              {route.routeName}
                            </Select.Option>
                          )
                        })}
                      </ThemeSelect>,
                    )}
                  </FormItem>
                  <Button disabled={isLocked} href={`#/delivery/routes`} type="link">
                    {folderIcon}
                  </Button>
                </Flex>
                <div className="label mt28">Delivery Contact Name</div>
                {/* <ThemeInput
                  placeholder="Name..."
                  style={{ width: 250 }}
                  value={currentOrder.wholesaleClient.mainContact ? currentOrder.wholesaleClient.mainContact.name : ''}
                  disabled
                /> */}
                <FormItem style={{ ...mb0, flex: 1 }}>
                  {getFieldDecorator('deliveryContact', {
                    rules: [{ required: false }],
                    initialValue: currentOrder.deliveryContact
                      ? currentOrder.deliveryContact
                      : currentOrder.wholesaleClient.mainContact
                      ? currentOrder.wholesaleClient.mainContact.name
                      : '',
                  })(
                    <ThemeSelect style={{ width: 250 }} placeholder="Delivery Contact Name" disabled={isLocked}>
                      {contacts.map((el: any) => (
                        <Select.Option key={el.contactId} value={el.name}>
                          {el.name}
                        </Select.Option>
                      ))}
                    </ThemeSelect>,
                  )}
                </FormItem>

                <div className="label mt28">Delivery Phone Number</div>
                <FormItem style={mb0}>
                  {getFieldDecorator('deliveryPhone', {
                    rules: [{ required: false }],
                    initialValue: currentOrder.deliveryPhone
                      ? currentOrder.deliveryPhone
                      : currentOrder.wholesaleClient.mainContact
                      ? currentOrder.wholesaleClient.mainContact.mobilePhone
                      : '',
                  })(
                    <ThemeSelect style={{ width: 250 }} placeholder="Delivery Phone" disabled={isLocked}>
                      {removeDuplidatePhones.map((el: any) => (
                        <Select.Option key={el} value={el}>
                          {el}
                        </Select.Option>
                      ))}
                    </ThemeSelect>,
                  )}
                </FormItem>
              </div>

              <div style={{ display: fulfillmentOptionType === 2 ? 'block' : 'none' }}>
                <div className="label mt28">Pickup Address</div>
                <Flex className="v-center">
                  <FormItem style={{ flex: 1 }}>
                    {getFieldDecorator('pickupAddress', {
                      rules: [{ required: false }],
                      initialValue: currentOrder.pickupAddress ? currentOrder.pickupAddress : defaultAddress,
                    })(<ThemeInput style={{ flex: 1 }} disabled={isLocked} />)}
                  </FormItem>
                  <div style={{ width: 42 }} />
                </Flex>
                <div className="label mt28">Warehouse Pickup Time</div>

                <FormItem>
                  {getFieldDecorator('warehousePickupTime', {
                    rules: [{ required: false }],
                    initialValue:
                      currentOrder.warehousePickupTime === 'null' || currentOrder.warehousePickupTime === null
                        ? null
                        : moment(currentOrder.warehousePickupTime, 'hh:mm A'),
                  })(
                    <TimePicker
                      style={{ width: 250 }}
                      format="hh:mm A"
                      placeholder="HH:MM AM/PM"
                      disabled={isLocked}
                    />,
                  )}
                </FormItem>

                <div className="label mt28">Pickup Contact Name</div>
                <FormItem style={{ flex: 1 }}>
                  {getFieldDecorator('pickupContactName', {
                    rules: [{ required: false }],
                    initialValue: currentOrder.pickupContactName,
                  })(<ThemeInput style={{ width: 250 }} placeholder="Name..." disabled={isLocked} />)}
                </FormItem>

                <div className="label mt28">Pickup Phone Number</div>
                <FormItem style={{ flex: 1 }}>
                  {getFieldDecorator('pickupPhoneNumber', {
                    rules: [{ required: false }],
                    initialValue: currentOrder.pickupPhoneNumber,
                  })(<ThemeInput style={{ width: 250 }} placeholder="Phone number..." disabled={isLocked} />)}
                </FormItem>

                <div className="label mt28">Pickup Reference No.</div>
                <FormItem style={{ flex: 1 }}>
                  {getFieldDecorator('pickupReferenceNo', {
                    rules: [{ required: false }],
                    initialValue: currentOrder.pickupReferenceNo,
                  })(<ThemeInput style={{ width: 250 }} disabled={isLocked} />)}
                </FormItem>
              </div>

              <div style={{ display: fulfillmentOptionType === 3 ? 'block' : 'none' }}>
                <div className="label mt28">Carrier</div>
                <Flex className="v-center">
                  <FormItem style={{ flex: 1 }}>
                    {getFieldDecorator('carrier', {
                      rules: [{ required: false }],
                      initialValue: currentOrder.carrier,
                    })(<ThemeInput disabled={isLocked} />)}
                  </FormItem>
                  <Button
                    disabled={isLocked}
                    href={`#/vendor/${currentOrder.wholesaleClient.clientId}/addresses`}
                    type="link"
                  >
                    {folderIcon}
                  </Button>
                </Flex>
                <div className="label mt28">Origin Address</div>
                <Flex className="v-center">
                  <FormItem style={{ flex: 1 }}>
                    {getFieldDecorator('originAddress', {
                      rules: [{ required: false }],
                      initialValue: currentOrder.originAddress ? currentOrder.originAddress : defaultAddress,
                      // })(<ThemeInput style={{ flex: 1 }} disabled={isLocked} />)}
                    })(
                      <AutoComplete
                        style={{ flex: 1 }}
                        onSearch={(val) => this.handleSearchAddress(val, 'address')}
                        disabled={isLocked}
                        dataSource={originAddresses.map((el: any) => (
                          <AutoComplete.Option key={el.wholesaleAddressId} value={formatAddress(el.address)}>
                            {formatAddress(el.address)}
                          </AutoComplete.Option>
                        ))}
                      />,
                    )}
                    <Icon type="down" className="custom-dropdown-arrow" viewBox="0 0 12 12" width={12} height={12} />
                  </FormItem>
                  <Button disabled={isLocked} href={`#/myaccount/setting`} type="link">
                    {folderIcon}
                  </Button>
                </Flex>
                <div className="label mt28">Delivery Address</div>
                {deliveryAddressItem}

                {getFreightType()}

                <div className="label mt28">Shipping Terms</div>
                <FormItem style={{ width: 250 }}>
                  {getFieldDecorator('shippingTerm', {
                    rules: [{ required: false }],
                    initialValue: currentOrder.shippingTerm,
                  })(
                    <ThemeSelect
                      style={{ flex: 1 }}
                      showSearch
                      optionFilterProp="children"
                      onSearch={() => {}}
                      filterOption={(input: any, option: any) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                      disabled={isLocked || enabledNS}
                      dropdownRender={(menu: any) => (
                        <div>
                          {menu}
                          <Divider style={{ margin: '4px 0' }} />
                          <div
                            style={{ padding: '4px 8px', cursor: 'pointer' }}
                            onMouseDown={(e) => e.preventDefault()}
                            onClick={(e) => this.openCompanySettingChange('visibleShippingTermsModal')}
                          >
                            <Icon type="plus" /> Add New Shipping Terms...
                          </div>
                        </div>
                      )}
                    >
                      {companyProductTypes.shippingTerms.sort((a: any, b: any) => a.name.localeCompare(b.name)).map(
                        (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                          return (
                            <Select.Option key={`shipping-term-${index}`} value={item.name}>
                              {item.name}
                            </Select.Option>
                          )
                        },
                      )}
                    </ThemeSelect>,
                  )}
                </FormItem>
                <div className="label mt28">Carrier Reference No.</div>
                <FormItem>
                  {getFieldDecorator('carrierReferenceNo', {
                    rules: [{ required: false }],
                    initialValue: currentOrder.carrierReferenceNo,
                  })(<ThemeInput style={{ width: 250 }} disabled={isLocked} />)}
                </FormItem>
              </div>

              <div className="label mt28">Fulfillment Instructions</div>
              <FormItem style={mb0}>
                {getFieldDecorator('deliveryInstruction', {
                  rules: [{ required: false }],
                  initialValue:
                    !currentOrder.deliveryInstruction || currentOrder.deliveryInstruction == 'null'
                      ? ''
                      : currentOrder.deliveryInstruction,
                })(
                  <ThemeTextArea
                    placeholder="Fulfillment Instruction..."
                    style={{ width: 250, height: 96 }}
                    disabled={isLocked}
                  />,
                )}
              </FormItem>
            </Col>
            <Col span={12} className="os-wrapper">
              <div className="order-summary">
                <h4>Order Summary</h4>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <div className="os-row">
                    <div className="label">Total Units</div>
                    <div className="value">{totalUnit}</div>
                  </div>
                  <div className="os-row">
                    <div className="label">Total Billable Quantity</div>
                    <div className="value">{formatNumber(totalQuantity, 2)}</div>
                  </div>
                </div>
                {accountType !== UserRole.WAREHOUSE && (
                  <div className="os-row">
                    <div className="label">Order Total</div>
                    <div className="value">{`$${calculateTotalOrder(
                      this.props.orderItemByProduct,
                      this.props.oneOffItems,
                      this.props.catchWeightValues
                    )}`}</div>
                  </div>
                )}
                {/* {!currentOrder.cashSale && (
                  <div className="os-row">
                    <div className="label">Payment Terms</div>
                    <FormItem style={mb0}>
                      {getFieldDecorator('financialTerms', {
                        rules: [{ required: false }],
                        initialValue: currentOrder.financialTerms
                          ? currentOrder.financialTerms
                          : currentOrder.wholesaleClient.paymentTerm
                            ? currentOrder.wholesaleClient.paymentTerm
                            : '',
                      })(
                        <ThemeSelect
                          style={{ width: '100%' }}
                          placeholder="Freight type"
                          // value={freightType}
                          disabled={isLocked || enabledNS}
                        >
                          {FINANCIAL_TERMS.map((item) => (
                            <Select.Option key={`customer-term-${item}`} value={item}>
                              {item}
                            </Select.Option>
                          ))}
                        </ThemeSelect>,
                      )}
                    </FormItem>
                  </div>
                )} */}
                <div className="os-row">
                  <div className="label">Sales Representative</div>
                  <FormItem style={mb0}>
                    {getFieldDecorator('seller', {
                      rules: [{ required: false }],
                      initialValue: currentOrder.seller ? currentOrder.seller.userId : '',
                    })(
                      <ThemeSelect style={{ width: '100%' }} disabled={isLocked}>
                        {currentCompanyUsers &&
                          currentCompanyUsers.filter(v => (v.role !== 'SUPERADMIN' && v.role !== 'WAREHOUSE')).map((item, index) => {
                            return (
                              <Select.Option key={`sr-${index}`} value={item.userId}>
                                {item.firstName + '  ' + item.lastName}
                              </Select.Option>
                            )
                          })}
                      </ThemeSelect>,
                    )}
                  </FormItem>
                </div>
              </div>
            </Col>
          </Row>
          <ThemeButton
            // className="sales-cart-input"
            // onClick={this.onSave}
            htmlType="submit"
            type="primary"
            size="large"
            style={{ marginTop: 20, marginBottom: 30 }}
            disabled={isLocked}
          >
            Save Changes
          </ThemeButton>
        </Form>
        <ThemeModal
          title={`Edit Value List "Freight Types"`}
          visible={visibleFreightTypeModal}
          onCancel={this.onCompanySettingTypeModal.bind(this, 'visibleFreightTypeModal')}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="freightTypes" title="Freight Type" buttonTitle="Add New Freight Type" />
        </ThemeModal>
        <ThemeModal
          title={`Edit Value List "Shipping Terms"`}
          visible={visibleShippingTermsModal}
          onCancel={this.onCompanySettingTypeModal.bind(this, 'visibleShippingTermsModal')}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor
            isModal={true}
            field="shippingTerms"
            title="Shipping Terms"
            buttonTitle="Add New Shipping Terms"
          />
        </ThemeModal>
      </div>
    )
  }
}

export default Form.create<FulfillmentFormProps>()(FulfillmentForm)
