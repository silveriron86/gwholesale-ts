import * as React from 'react'
import { OrderListHeaderContainer } from '../styles'

export class OrderListHeader extends React.PureComponent {
  render() {
    return (
      <OrderListHeaderContainer>
        Order List Header
      </OrderListHeaderContainer>
    )
  }
}

export default OrderListHeader
