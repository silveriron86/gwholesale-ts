import React from 'react'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import OrderListHeader from './header'
import SalesOrders from './sales-orders'


type SOListProps = OrdersDispatchProps & OrdersStateProps

class SalesOrderList extends React.PureComponent<SOListProps> {
  state = {

  }

  render() {
    return (
      <PageLayout noSubMenu={true} currentTopMenu="menu-Selling & Shipping-Cash Sales">
        <OrderListHeader />
        <SalesOrders />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(SalesOrderList))
