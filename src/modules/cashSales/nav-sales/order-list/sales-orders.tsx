import * as React from 'react'
import { ThemeTable } from '~/modules/customers/customers.style'
import { OrderListBodyContainer } from '../styles'

export class SalesOrders extends React.PureComponent {

  columns: any[] = [
    {
      title: 'OrderID',
      dataIndex: 'wholesaleOrderId',
      key: 'wholesaleOrderId'
    },
    {
      title: 'Company',
      dataIndex: 'wholesaleCompany',
      key: 'wholesaleCompany'
    },
    {
      title: 'Date Placed',
      dataIndex: 'createdDate',
      key: 'createdDate'
    },
    {
      title: 'Delivery Date',
      dataIndex: 'deliveryDate',
      key: 'deliveryDate'
    },
    {
      title: 'Order Total',
      dataIndex: 'orderTotal',
      key: 'orderTotal'
    },
    {
      title: 'Status',
      dataIndex: 'wholesaleOrderStatus',
      key: 'wholesaleOrderStatus'
    },
    {
      title: 'QBO',
      dataIndex: 'qboId',
      key: 'qboId'
    },
  ]

  render() {
    return (
      <OrderListBodyContainer>
        <ThemeTable
          columns={this.columns}
          dataSource={[]}
        />

      </OrderListBodyContainer>
    )
  }
}

export default SalesOrders
