import React from 'react'
import { withTheme } from 'emotion-theming'
import { SalesContainer, fullButton, LockText, PrintModalHeader } from '../_style'
import {
  CustomerDetailsWrapper as DetailsWrapper,
  CustomerDetailsTitle as DetailsTitle,
  floatLeft,
  floatRight,
  ThemeOutlineButton,
  ThemeModal,
  ThemeSpin,
  ThemeButton,
  ThemeOutlineCancelButton,
  ThemeSwitch,
} from '~/modules/customers/customers.style'
import { Row, Col, Icon, Modal, Popconfirm, Tooltip } from 'antd'
import { History } from 'history'
import { CustomButton } from '~/components'
import { CartOrderTable } from './tables'
import { OneOffItemTable } from './tables'
import ProductModal from './modals/product-modal'
import { OrderDetail, OrderItem } from '~/schema'
import PrintBillOfLading from '~/modules/cashSales/sales/cart/print/print-bill-lading'
import PrintInvoice from '~/modules/cashSales/sales/cart/print/print-invoice'
import jQuery from 'jquery'
import { red } from '~/common'
import { printWindow, judgeConstantRatio, printWindowAsync } from '~/common/utils'
import { cloneDeep } from 'lodash'
import { Cart } from '../../nav-sales/order-detail/tabs/cart'
import { PrintPickSheet } from './print/print-pick-sheet'

interface CartContainerProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  company: any
  logo: string
  loading: boolean
  onAddItem: Function
  onSwapItem: Function
  onRemoveItem: Function
  onUpdateItem: Function
  onUpdateOrderStatus: Function
  onUpdateAllItemStatus: Function
  onRefreshOrderItems: Function
  onDuplicateOrder: Function
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
  deleteProductType: Function
  updateProductType: Function
  companyProductTypes: any[]
  history: History
  catchWeightValues: any
  getWeightsByOrderItemIds: Function
  addCatchWeightByNumber: Function
  deleteCatchWeightByNumber: Function
  orderPrintVersion: Function
  openAddOffItem: Function
  oneOffItemList: OrderItem[]
  onUpsetOffItem: Function
  onRemoveOffItem: Function
  resetLoading: Function
  updateOrderLocked: Function
  onlyContent?: boolean
  extraCharge?: boolean
  addChargeClicked?: boolean
  toggleModal?: Function
  toggleOpenChildModal?: Function
  onChangeOrderItemPricingLogic: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  setShippingOrder?: Function
  printSetting: string
  sellerSetting: any
}

export class CartContainer extends React.PureComponent<CartContainerProps> {
  state = {
    newModalVisible: false,
    printPickSheetReviewShow: false,
    PrintInvoiceReviewShow: false,
    printBillShow: false,
    printPickSheetRef: null,
    PrintBillOfLadingRef: null,
    PrintInvoiceRef: null,
    searchInput: '',
    addItemClicked: false,
    addOneOffClicked: false,
    isLocked: this.props.currentOrder.isLocked,
    finishLoadingPrintLogo: this.props.logo == 'default' ? true : false,
    pickSheetLoading: false,
  }

  componentDidMount() {
    localStorage.removeItem('FIRST_OPENED_CART')
    let map = { 65: false, 18: false, 91: false, 73: false, 17: false }
    let os = 'WIN'
    if (navigator.appVersion.indexOf('Mac') != -1) {
      os = 'MAC'
    }
    const _this = this
    jQuery('html')
      // .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode in map) {
          map[e.keyCode] = true
        }
        if (map[18] && map[65]) {
          setTimeout(function() {
            // top button
            jQuery('.sales-cart-input.add-sales-cart-item').trigger('focus')
            jQuery('.sales-cart-input.add-sales-cart-item').trigger('click')

            // footer button
            const addItemBtn = jQuery('.footer-add-item .ant-input.ant-select-search__field')
            addItemBtn.val('')
            // addItemBtn.trigger('click')
          }, 30)
        }
        if (os == 'WIN') {
          if (map[17] && map[73]) {
            setTimeout(function() {
              if (_this.state.printPickSheetReviewShow) {
                jQuery('.print-pick-sheet').trigger('click')
              } else if (_this.state.PrintInvoiceReviewShow) {
                jQuery('.print-bill-lading').trigger('click')
              } else if (_this.state.printBillShow) {
                jQuery('.print-bill').trigger('click')
              }
            }, 30)
          }
        } else if (os == 'MAC') {
          if (map[91] && map[73]) {
            setTimeout(function() {
              if (_this.state.printPickSheetReviewShow) {
                jQuery('.print-pick-sheet').trigger('click')
              } else if (_this.state.PrintInvoiceReviewShow) {
                jQuery('.print-bill-lading').trigger('click')
              } else if (_this.state.printBillShow) {
                jQuery('.print-bill').trigger('click')
              }
            }, 30)
          }
        }
      })
      .bind('keyup', (e: any) => {
        for (const keycode in map) {
          map[keycode] = false
        }
      })
  }

  handleSelectOk = () => {
    this.setState({
      newModalVisible: false,
      searchInput: '',
    })
    window.localStorage.setItem('SALES-CART-MODAL-INDEX', '0')
    jQuery(`.modal-order-tab`)
      .find('.ant-tabs-tab')
      .removeClass('tab-focus')
    jQuery(`.modal-order-tab`)
      .find('.ant-table-row')
      .removeClass('focused-row')
  }

  handleAddOrderItem = (item: any) => {
    this.setState({ addItemClicked: true })
    this.props.onAddItem(item)
    this.handleSelectOk()
  }

  openAddModal = () => {
    this.setState({
      newModalVisible: true,
    })
    setTimeout(() => {
      jQuery(`.modal-order-tab`)
        .find('.ant-input')[0]
        .focus()
    }, 0)
  }

  handleUpdateStatus = (status: any) => {
    this.props.onUpdateOrderStatus(status)
  }

  handleDuplicateOrder = () => {
    this.props.onDuplicateOrder()
  }

  onSearch = (text: string) => {
    this.setState({
      searchInput: text,
    })
    window.localStorage.setItem('SALES-CART-MODAL-INDEX', '0')
  }

  openAddOffItem = (name?: string) => {
    this.setState({ addOneOffClicked: true })
    this.props.openAddOffItem(name)
  }

  onChangeSwitch = (checked: Boolean) => {
    this.setState({
      isLocked: checked,
    })
    this.props.updateOrderLocked(checked)
  }

  doPrintBill = (modalName: string, index: number) => {
    const { currentOrder } = this.props
    printWindowAsync(modalName, null)
    if (currentOrder && currentOrder.wholesaleOrderStatus != 'SHIPPED') {
      if (index === 1) {
        // Picksheet
        this.handleUpdateStatus('PICKING')
      } else {
        // BOL, Invoice
        if (!this.props.disableShip) {
          this.props.setShippingOrder()
        }
      }
    }
  }

  printContent: any = (type: number) => {
    let content
    const currentOrder = this.props.currentOrder
    if (type === 1) {
      content = this.state.printPickSheetRef
      if (currentOrder.wholesaleOrderStatus == 'NEW' || currentOrder.wholesaleOrderStatus == 'PLACED') {
        this.handleUpdateStatus('PICKING')
      }
    } else if (type === 2) {
      content = this.state.PrintInvoiceRef
    } else {
      content = this.state.PrintBillOfLadingRef
      // if (currentOrder.wholesaleOrderStatus == 'PICKING') {
      //   this.handleUpdateStatus('SHIPPED')
      // }
    }
    return content
  }

  changePrintLogoStatus = () => {
    this.setState({
      finishLoadingPrintLogo: true,
    })
  }
  render() {
    const { newModalVisible, searchInput, isLocked, finishLoadingPrintLogo } = this.state
    const { loading, currentOrder, onlyContent, extraCharge, catchWeightValues, printSetting } = this.props
    const userPrintSetting = JSON.parse(printSetting)
    const printDeliveryItems = this.props.orderItems.filter((el) => {
      if (el.quantity > 0) {
        // Order Qty > 0
        return true
      }

      let pickedQty = 0
      if (!judgeConstantRatio(el)) {
        const recordWeights = catchWeightValues[el.wholesaleOrderItemId]
        pickedQty = recordWeights ? recordWeights.length : 0
      } else {
        pickedQty = el.picked
      }

      if (pickedQty > 0) {
        // Picked Qty > 0
        return true
      }
    })
    const printAllDeliveryItems = printDeliveryItems.concat(this.props.oneOffItemList)

    const itemsCountTextSuffix =
      this.props.orderItems.length > 0 ? (this.props.orderItems.length == 1 ? 'item' : 'items') : ''

    return (
      <ThemeSpin tip="Loading..." spinning={loading}>
        <SalesContainer className="sales-cart-tabs-area" style={{ padding: onlyContent ? '0' : '0 18px' }}>
          <ThemeModal
            closable={false}
            keyboard={true}
            okText="Close [esc]"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={'75%'}
            visible={newModalVisible}
            onOk={this.handleSelectOk}
            onCancel={this.handleSelectOk}
          >
            <ProductModal
              hasTabs={true}
              isAdd={true}
              selected={searchInput}
              onSelect={this.handleAddOrderItem}
              onSearch={this.onSearch}
              salesType="SELL"
            />
          </ThemeModal>

          <DetailsWrapper style={{ padding: onlyContent ? '0 0 20px' : 20, borderTopWidth: onlyContent ? 0 : 5 }}>
            {onlyContent !== true && (
              <Row>
                <Col lg={4} md={4}>
                  <DetailsTitle style={floatLeft}>
                    Cart{' '}
                    {this.props.orderItems.length > 0
                      ? `(${this.props.orderItems.length} ${itemsCountTextSuffix})`
                      : ''}
                  </DetailsTitle>
                </Col>
                <Col lg={10} md={10}>
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    {currentOrder.wholesaleOrderStatus != 'SHIPPED' && currentOrder.wholesaleOrderStatus != 'CANCEL' ? (
                      <div>
                        <ThemeOutlineButton
                          style={{ ...{ marginTop: -5, marginRight: 10 } }}
                          shape="round"
                          onClick={this.openAddModal}
                          className="sales-cart-input add-item bold-blink"
                        >
                          <Icon type="plus-circle" />
                          Add Item
                        </ThemeOutlineButton>
                      </div>
                    ) : (
                      ''
                    )}
                    <div>
                      <ThemeOutlineButton
                        style={{ ...{ marginTop: -5, marginRight: 10 } }}
                        shape="round"
                        onClick={this.openAddOffItem}
                        className="sales-cart-input bold-blink one-off-item"
                      >
                        <Icon type="plus-circle" />
                        Add One Off Item
                      </ThemeOutlineButton>
                    </div>
                  </div>
                </Col>
                <Col lg={10} md={10}>
                  <ThemeButton
                    style={{ ...floatRight, ...{ marginTop: -5, marginRight: 20 } }}
                    shape="round"
                    onClick={() => this.handleDuplicateOrder()}
                    className="sales-cart-input header-last-tab duplicate-order"
                  >
                    <Icon type="copy" theme="filled" />
                    Duplicate Order
                  </ThemeButton>
                </Col>
              </Row>
            )}
            {extraCharge === true ? (
              <Row>
                <Col lg={24} md={24}>
                  <div style={{ minHeight: 400 }} className="one-off-table">
                    <OneOffItemTable
                      editable={
                        this.props.currentOrder &&
                        currentOrder.wholesaleOrderStatus !== 'CANCEL' &&
                        currentOrder.wholesaleOrderStatus !== 'SHIPPED'
                      }
                      currentOrder={this.props.currentOrder}
                      orderItems={this.props.oneOffItemList}
                      handleSave={this.props.onUpsetOffItem}
                      handleRemove={this.props.onRemoveOffItem}
                      handleUpdateAllItemStatus={this.props.onUpdateAllItemStatus}
                      addItemButtonClicked={this.state.addOneOffClicked || this.props.addChargeClicked}
                      isExtraCharge={extraCharge}
                      openAddOffItem={this.openAddOffItem}
                      companyProductTypes={this.props.companyProductTypes}
                    />
                  </div>
                </Col>
              </Row>
            ) : (
              <Row>
                <Col lg={24} md={24}>
                  <div
                    style={{ minHeight: 400 }}
                    className={`sales-order-table ${currentOrder.wholesaleOrderStatus == 'CANCEL' ? 'read-only' : ''}`}
                  >
                    <Cart
                      {...this.props}
                      toggleModal={this.props.toggleModal}
                      changeSelectedItems={this.props.changeSelectedItems}
                      onChagneInserItemPosition={this.props.onChagneInserItemPosition}
                      toggleOpenChildModal={this.props.toggleOpenChildModal}
                      onChangeOrderItemPricingLogic={this.props.onChangeOrderItemPricingLogic}
                      updateOrderItemForLot={this.props.updateOrderItemForLot}
                      onChangeOrderItemPricingLogicWithoutRefresh={
                        this.props.onChangeOrderItemPricingLogicWithoutRefresh
                      }
                    />
                  </div>
                </Col>
              </Row>
            )}

            {extraCharge !== true && false && (
              <Row style={{ marginTop: 20, paddingBottom: 20 }}>
                <Col lg={20} md={20}>
                  <Row>
                    {/* <Col lg={6} md={24} style={ml0}>
                    <ThemeButton shape="round" style={fullButton} disabled>
                      <Icon type="reload" />
                      Update Picking
                    </ThemeButton>
                  </Col> */}
                    <Col md={8} lg={4} style={{ paddingTop: '7px' }}>
                      <ThemeSwitch checked={isLocked} onChange={this.onChangeSwitch} />
                      <LockText>Lock/Finalize Order</LockText>
                    </Col>
                    <Col md={8} lg={4}>
                      <ThemeButton
                        shape="round"
                        style={fullButton}
                        onClick={this.openPrintModal.bind(this, 1)}
                        disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}
                      >
                        <Icon type="printer" theme="filled" />
                        Preview Pick Sheet
                      </ThemeButton>
                    </Col>
                    <Col md={8} lg={4}>
                      <ThemeButton
                        shape="round"
                        style={fullButton}
                        onClick={this.openPrintModal.bind(this, 2)}
                        disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}
                      >
                        <Icon type="printer" theme="filled" />
                        Preview Delivery List
                      </ThemeButton>
                    </Col>
                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleUpdateStatus('CANCEL')}
                      disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}>

                      <Icon type="close-circle" theme="filled" />
                      Cancel Order
                    </ThemeButton>
                  </Col> */}

                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleUpdateStatus('INVOICED')}>
                      <Icon type="dollar-circle" theme="filled" />
                      Set to Invoiced
                    </ThemeButton>
                  </Col> */}
                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleDuplicateOrder()}>
                      <Icon type="copy" theme="filled" />
                      Duplicate Order
                    </ThemeButton>
                  </Col> */}
                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleSyncQBOOrder()}>
                      <Icon type={this.props.loading ? "loading" : "cloud"} theme="filled" />
                      QBO Sync
                    </ThemeButton>
                  </Col> */}
                  </Row>
                </Col>
                <Col md={4}>
                  <Col lg={24} md={24}>
                    <Popconfirm
                      placement="topRight"
                      title="Are you sure to cancel the order?"
                      onConfirm={() => this.handleUpdateStatus('CANCEL')}
                      okText="Yes"
                      cancelText="No"
                    >
                      <ThemeOutlineCancelButton
                        shape="round"
                        style={fullButton}
                        disabled={currentOrder.wholesaleOrderStatus == 'CANCEL' || isLocked}
                      >
                        <Icon type="close-circle" theme="filled" />
                        Cancel Order
                      </ThemeOutlineCancelButton>
                    </Popconfirm>
                  </Col>
                </Col>
              </Row>
            )}
          </DetailsWrapper>
          <Modal
            width={1200}
            footer={null}
            visible={this.state.PrintInvoiceReviewShow}
            onCancel={this.closePrintModal.bind(this, 2)}
            wrapClassName="print-modal"
          >
            <PrintModalHeader>
              <Tooltip title="Print invoice and set order status to Shipped" placement="bottom">
                <ThemeButton
                  size="large"
                  className="print-bill-lading"
                  onClick={() => this.doPrintBill('PrintInvoiceModal', 2)}
                  disabled={!finishLoadingPrintLogo}
                >
                  <Icon type="printer" theme="filled" />
                  Print{' '}
                  {userPrintSetting && userPrintSetting.invoice_title ? userPrintSetting.invoice_title : 'Invoice'}
                </ThemeButton>
              </Tooltip>
              <div className="clearfix" />
            </PrintModalHeader>
            <div id={'PrintInvoiceModal'}>
              <PrintInvoice
                getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                catchWeightValues={this.props.catchWeightValues}
                orderItems={printAllDeliveryItems}
                currentOrder={this.props.currentOrder}
                companyName={this.props.companyName}
                company={this.props.company}
                logo={this.props.logo}
                printSetting={this.props.printSetting}
                sellerSetting={this.props.sellerSetting}
                changePrintLogoStatus={this.changePrintLogoStatus}
                fulfillmentOptionType={this.props.fulfillmentOptionType}
                type={'invoice'}
                ref={(el) => {
                  this.setState({ PrintInvoiceRef: el })
                }}
              />
            </div>
          </Modal>
          <Modal
            width={1200}
            footer={null}
            visible={this.state.printBillShow}
            onCancel={this.closePrintModal.bind(this, 3)}
            wrapClassName="print-modal"
          >
            <PrintModalHeader>
              <Tooltip title="Print bill of lading and set order status to Shipped" placement="bottom">
                <ThemeButton
                  size="large"
                  className="print-bill"
                  onClick={() => this.doPrintBill('printBillModal', 3)}
                  disabled={!finishLoadingPrintLogo}
                >
                  <Icon type="printer" theme="filled" />
                  Print{' '}
                  {userPrintSetting && userPrintSetting.bill_title ? userPrintSetting.bill_title : 'Bill of Lading'}
                </ThemeButton>
              </Tooltip>
              <div className="clearfix" />
            </PrintModalHeader>
            <div id={'printBillModal'} style={{ marginTop: '30px' }}>
              <PrintBillOfLading
                title={'Bill of Lading'.toUpperCase()}
                getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                catchWeightValues={this.props.catchWeightValues}
                orderItems={printAllDeliveryItems}
                currentOrder={this.props.currentOrder}
                companyName={this.props.companyName}
                company={this.props.company}
                logo={this.props.logo}
                printSetting={this.props.printSetting}
                sellerSetting={this.props.sellerSetting}
                changePrintLogoStatus={this.changePrintLogoStatus}
                type={'bill'}
                fulfillmentOptionType={this.props.fulfillmentOptionType}
                ref={(el) => {
                  this.setState({ PrintBillOfLadingRef: el })
                }}
              />
            </div>
          </Modal>
          {this.state.printPickSheetReviewShow && (
            <Modal
              width={1080}
              footer={null}
              visible={this.state.printPickSheetReviewShow}
              onCancel={this.closePrintModal.bind(this, 1)}
              wrapClassName="print-modal"
            >
              <ThemeSpin spinning={this.state.pickSheetLoading}>
                <PrintModalHeader>
                  <Tooltip title="Print pick sheet and set order status to Picking" placement="bottom">
                    <ThemeButton
                      size="large"
                      className="print-pick-sheet"
                      onClick={() => this.doPrintBill('printPickSheetModal', 1)}
                      disabled={!finishLoadingPrintLogo}
                    >
                      <Icon type="printer" theme="filled" />
                      Print{' '}
                      {userPrintSetting && userPrintSetting.pick_title ? userPrintSetting.pick_title : 'Pick Sheet'}
                    </ThemeButton>
                  </Tooltip>
                  <div className="clearfix" />
                </PrintModalHeader>
                <div id={'printPickSheetModal'} style={{paddingTop: 25}}>
                  <PrintPickSheet
                    orderItems={this.props.orderSortItems}
                    currentOrder={this.props.currentOrder}
                    companyName={this.props.companyName}
                    company={this.props.company}
                    catchWeightValues={this.props.catchWeightValues}
                    logo={this.props.logo}
                    printSetting={this.props.printSetting}
                    sellerSetting={this.props.sellerSetting}
                    changePrintLogoStatus={this.changePrintLogoStatus}
                    ref={(el) => {
                      this.setState({ printPickSheetRef: el })
                    }}
                  />
                </div>
              </ThemeSpin>
            </Modal>
          )}
        </SalesContainer>
      </ThemeSpin>
    )
  }

  openPrintModal = (type: number) => {
    const orderId = this.props.currentOrder.wholesaleOrderId
    // this.props.getOrderItemsById(orderId)
    const time = setInterval(() => {
      if (!this.props.updateOrderQuantityLoading.fetching) {
        // this.props.getOrderItemsById(orderId)
        const printDeliveryItems = this.props.orderItems.filter((el) => el.picked && el.picked != 0)
        const ids = printDeliveryItems.filter((el) => !judgeConstantRatio(el)).map((el) => el.wholesaleOrderItemId)
        this.props.getWeightsByOrderItemIds(ids)
        clearInterval(time)
      }
    }, 200)

    if (type === 1) {
      this.setState({
        printPickSheetReviewShow: true,
        pickSheetLoading: true,
      })
      this.props.orderPrintVersion(orderId)
      // sort items
      const jsonObj = JSON.parse(this.props.printSetting)
      const item_sort = jsonObj.item_sort

      const timer = setInterval(() => {
        if (!this.props.updateOrderQuantityLoading.fetching) {
          this.props.getOrderItemsByIdAndSort({
            id: orderId,
            sort: item_sort,
          })
          this.setState({ pickSheetLoading: false })
          clearInterval(timer)
        }
      }, 200)
      // this.props.onUpdateOrderStatus("PICKING")
    } else if (type === 2) {
      const printDeliveryItems = this.props.orderItems.filter((el) => el.picked && el.picked != 0)
      const ids = printDeliveryItems.filter((el) => !judgeConstantRatio(el)).map((el) => el.wholesaleOrderItemId)
      this.props.getWeightsByOrderItemIds(ids)
      this.setState({
        PrintInvoiceReviewShow: true,
      })
      // this.props.onUpdateOrderStatus("SHIPPED")
    } else if (type === 3) {
      const printDeliveryItems = this.props.orderItems.filter((el) => el.picked && el.picked != 0)
      const ids = printDeliveryItems.filter((el) => !judgeConstantRatio(el)).map((el) => el.wholesaleOrderItemId)
      this.props.getWeightsByOrderItemIds(ids)
      this.setState({
        printBillShow: true,
      })
    }
  }

  closePrintModal = (type: number) => {
    if (type === 1) {
      this.setState({
        printPickSheetReviewShow: false,
      })
    } else if (type === 2) {
      this.setState({
        PrintInvoiceReviewShow: false,
      })
    } else {
      this.setState({
        printBillShow: false,
      })
    }
  }
}

export default withTheme(CartContainer)
