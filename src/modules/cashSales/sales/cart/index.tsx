import React from 'react'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import moment from 'moment'
import { History } from 'history'
import { Theme } from '~/common'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import SalesOverview from '../widgets/sales-overview'
import { CartContainer } from './cart-container'
import { OrderItem } from '~/schema'
import { RouteComponentProps } from 'react-router'
import lodash from 'lodash'
import { formatNumber } from '~/common/utils'

type SalesCartProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    history: History
    theme: Theme
    onlyContent?: boolean
    extraCharge?: boolean
    addChargeClicked?: boolean
    toggleOpenChildModal?: Function
    toggleModal?: Function
    setShippingOrder: Function
    condensedMode?: boolean
  }

export class SalesCart extends React.PureComponent<SalesCartProps> {
  state = {
    cancelNotifyModalVisible: false,
    oneOffItemList: [],
    normalItemList: [],
  }
  childRef = React.createRef<any>()
  componentDidMount() {
    if (!this.props.printSetting) {
      this.props.getPrintSetting()
    }

    // use companyName to check if it's Fresh Green
    this.props.getSellerSetting()

    this.props.getCompanyProductAllTypes()
    this.setOffItemsState(this.props)
    // this.props.getItemForAddItemModal({ type: 'SELL' })
    this.props.getSimplifyItems('SELL')
    const orderId = this.props.match?.params?.orderId
    this.props.getSimplifyItemsByOrderId({orderId, type:'SELL'})
  }

  setOffItemsState = (props: any) => {
    const oneOffItemList = props.oneOffItems.filter((obj: any) => {
      if (!obj.isInventory) {
        obj.uom = obj.UOM
        obj.variety = obj.itemName
        return true
      } else {
        return false
      }
    })
    this.setState({
      oneOffItemList,
    })
  }

  componentWillReceiveProps(nextProps: SalesCartProps) {
    if (nextProps.newOrderId !== -1 && nextProps.newOrderId !== this.props.newOrderId) {
      window.location.href = `#/cash-sales/${nextProps!.currentOrder!.wholesaleOrderId}`
      window.location.reload()
    }

    if (this.props.oneOffItems !== nextProps.oneOffItems) {
      this.setOffItemsState(nextProps)
    }

    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      // this.props.getAddresses()
    }
  }

  _formatNewItem = (newItem: any) => {
    let quotedPrice = ''
    let UOM = ''
    let lotId = ''
    const margin = 0

    if (typeof newItem.wholesaleOrderItemId === 'undefined') {
      // if coming from product catalogy
      // The quoted price will be cost*default_margin
      // Newitem is wholesaleItem is different with wholesaleOrderItem
      if (typeof newItem.wholesaleItem === 'undefined') {
        const markup = (newItem.defaultMargin ? newItem.defaultMargin : 0) / 100

        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost : 0, 2)
        UOM = newItem.inventoryUOM
      } else {
        const markup = (newItem.margin ? newItem.margin : 0) / 100
        const freight = newItem.freight ? newItem.freight : 0
        if (newItem.uom == newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio == true) {
          quotedPrice = formatNumber(
            newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
            2,
          )
        } else {
          quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
        }
        UOM = newItem.UOM
      }
    } else {
      // if coming for lot
      // The quoted price will be cost of lot*lot_margin if it is greater than 0, if not lot*default_margin

      const markup = (newItem.margin ? newItem.margin : 0) / 100
      const freight = newItem.freight ? newItem.freight : 0
      if (newItem.uom === newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio === true) {
        quotedPrice = formatNumber(
          newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
          2,
        )
      } else {
        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
      }
      if (newItem.uom) {
        UOM = newItem.uom
      }
      lotId = newItem.lotId
    }

    return {
      wholesaleItemId: newItem.wholesaleItem ? newItem.wholesaleItem.wholesaleItemId : newItem.itemId,
      wholesaleOrderItemId: null,
      quantity: 0,
      cost: newItem.cost,
      price: quotedPrice,
      margin: margin,
      freight: 0,
      status: 'PLACED',
      UOM: UOM,
      lotId: lotId,
    }
  }

  handleAddItem = lodash.debounce((newItem: any) => {
    // const { orderItems } = this.props
    // tslint:disable-next-line:prefer-const
    let itemsList: any[] = []
    // orderItems.forEach((item) => {
    //   itemsList.push(this._formatItem(item))
    // })onAddItem
    itemsList.push(this._formatNewItem(newItem))
    this._updateOrder(itemsList)
  }, 200)

  _updateOrder = (itemsList: any[]) => {
    const { currentOrder } = this.props
    if (currentOrder) {
      if (itemsList.length > 0) {
        itemsList.map((item: any) => {
          item.wholesaleOrderId = currentOrder.wholesaleOrderId
        })
      }
      const formData = {
        deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        userId: currentOrder.user.userId,
        totalPrice: currentOrder.totalPrice,
        wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
        itemList: itemsList,
        status: currentOrder.wholesaleOrderStatus,
      }
      this.props.updateOrder(formData)
    }
  }

  _formatItem = (item: any) => {
    return {
      wholesaleItemId: item.itemId,
      wholesaleOrderItemId: item.wholesaleOrderItemId,
      quantity: item.quantity,
      picked: item.picked,
      cost: item.cost,
      price: item.price,
      margin: item.margin,
      freight: item.freight,
      status: item.status,
      UOM: item.UOM,
    }
  }

  handleRemoveItem = (id: string) => {
    const { orderItems } = this.props
    // tslint:disable-next-line:prefer-const
    let itemsList: any[] = []
    orderItems.forEach((item) => {
      if (item.wholesaleOrderItemId == id) {
        let orderItem = this._formatItem(item)
        orderItem = { ...orderItem, deleted: true }
        itemsList.push(orderItem)
      }
    })
    this._updateOrder(itemsList)
  }

  handleSwapItem = (id: string, newItem: any) => {
    this.props.resetLoading()
    const itemsList: any[] = []
    itemsList.push(this._formatNewItem(newItem))
    this._updateOrder(itemsList)
  }

  handleUpdateItem = (item: OrderItem) => {
    this.props.updateSaleOrderItem(item)
  }

  handleOrderStatus = (status: string) => {
    const { currentOrder } = this.props
    if (currentOrder) {
      this.props.resetLoading()
      setTimeout(() => {
        this.props.updateOrderInfo({
          wholesaleOrderId: currentOrder.wholesaleOrderId,
          deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
          wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
          status: status,
          cashSale: true,
        })
      }, 100)
    }
  }

  handleUpdateAllItemStatus = (orderItems: OrderItem[]) => {
    // tslint:disable-next-line:prefer-const
    let itemsList: any[] = []
    orderItems.forEach((item) => {
      itemsList.push(this._formatItem(item))
    })
    this._updateOrder(itemsList)
  }

  handleUpdateAllOffItemStatus = (orderItems: OrderItem[]) => {
    // tslint:disable-next-line:prefer-const
    let itemsList: any[] = []
    orderItems.forEach((item) => {
      itemsList.push(this._formatItem(item))
    })
    this._updateOrder(itemsList)
  }

  handleRefreshOrderItems = () => {
    const orderId = this.props.match.params.orderId
    this.props.getOrderItemsById(orderId)
  }

  handleDuplicateOrder = () => {
    const { orderItems, currentOrder, oneOffItems } = this.props

    var delivery: any = new Date(currentOrder!.deliveryDate)
    delivery = moment(delivery)
      .add(2, 'days')
      .format('MM/DD/YYYY')
    let newOrderItems = orderItems.concat(oneOffItems)
    let tempOrderItems = newOrderItems.map((obj) => {
      return {
        wholesaleItemId: obj.itemId,
        note: obj.note,
        UOM: obj.UOM,
        price: obj.price,
        cost: obj.cost,
        itemName: obj.itemName,
      }
    })
    const data = {
      deliveryDate: delivery,
      wholesaleCustomerClientId: currentOrder!.wholesaleClient.clientId,
      itemList: tempOrderItems,
      cashSale: true,
    }
    this.props.duplicateOrder(data)
  }

  openAddOffItem = (name?: string) => {
    const itemList: OrderItem[] = lodash.cloneDeep(this.state.oneOffItemList)
    const newData = {
      wholesaleOrderItemId: '-' + (itemList.length + 1).toString(),
      constantRatio: true,
      cost: 0,
      picked: 0,
      price: 0,
      quantity: 0,
      status: 'SHIPPED',
      itemName: name ?? '',
      uom: 'each',
      orderId: this.props.match.params.orderId,
    }
    itemList.push(newData)
    this.setState({
      offItemList: itemList,
    })

    this.props.createOffItem(newData)
  }

  onUpsetOffItem = (data: any) => {
    // update
    if (parseInt(data['wholesaleOrderItemId']) > 0) {
      this.props.updateOffItem(data)
    } else {
      this.props.createOffItem(data)
    }
  }

  removeOffItem = (data: any) => {
    this.props.removeOffItem({ wholesaleOrderItemId: data })
  }

  updateOrderLocked = (checked: any) => {
    this.props.updateOrderLocked({
      wholesaleOrderId: this.props.match.params.orderId,
      isLocked: checked,
    })
  }

  handleOpenPreviewModal = (action: string) => {
    const actions = ['view_pick', 'view_invoice', 'view_bill']
    console.log(action, action === 'view_pick' ? 1 : 2)
    const index = actions.indexOf(action)
    this.childRef.current.openPrintModal(index + 1)
  }

  onChangeOrderItemPricingLogic = (data: any) => {
    this.props.updateOrderItemPricingLogic(data)
    //TODO refresh catchweight for order item
  }

  onChangeOrderItemPricingLogicWithoutRefresh = async (data: any) => {
    // this.props.handlerUpdateSOUOMRedux(data)
    this.props.updateOrderItemPricingLogicWithoutRefresh(data)
  }

  render() {
    const { currentOrder, orderItems, loading, onlyContent, extraCharge } = this.props
    // const { cancelNotifyModalVisible } = this.state
    if (!currentOrder || currentOrder.wholesaleClient.type !== 'CUSTOMER') {
      if (onlyContent) return null
      return <PageLayout currentTopMenu={'menu-Customers-Sales Orders'} />
    }
    let error = ''
    if (orderItems && orderItems.length > 0) {
      orderItems.forEach((record) => {
        if (record.picked !== null && record.quantity !== record.picked) {
          if (record.quantity < record.picked) {
            error = 'Errors On Order'
          } else {
            error = 'Warning On Order'
          }
        }
      })
    }
    const company = this.props.sellerSetting ? this.props.sellerSetting.company : null
    const cartContainerSection = (
      <CartContainer
        onlyContent={onlyContent}
        extraCharge={extraCharge}
        currentOrder={currentOrder}
        loading={loading}
        company={company}
        onAddItem={this.handleAddItem}
        onSwapItem={this.handleSwapItem}
        onUpdateItem={this.handleUpdateItem}
        onRemoveItem={this.handleRemoveItem}
        onUpdateOrderStatus={this.handleOrderStatus}
        onDuplicateOrder={this.handleDuplicateOrder}
        onUpdateAllItemStatus={this.handleUpdateAllItemStatus}
        onRefreshOrderItems={this.handleRefreshOrderItems}
        onUpsetOffItem={this.onUpsetOffItem}
        onRemoveOffItem={this.removeOffItem}
        updateOrderLocked={this.updateOrderLocked}
        openAddOffItem={this.openAddOffItem}
        oneOffItemList={this.state.oneOffItemList}
        onChangeOrderItemPricingLogic={this.onChangeOrderItemPricingLogic}
        onChangeOrderItemPricingLogicWithoutRefresh={this.onChangeOrderItemPricingLogicWithoutRefresh}
        ref={this.childRef}
        {...this.props}
      />
    )

    if (onlyContent) {
      return (
        <>
          <SalesOverview
            onlyContent={onlyContent}
            orderId={this.props.match.params.orderId}
            errorMsg={error}
            order={currentOrder}
          />
          {cartContainerSection}
        </>
      )
    }

    return (
      <PageLayout currentTopMenu={'menu-Customers-Sales Orders'}>
        {/* <ThemeModal
            title={'Notice'}
            keyboard={true}
            visible={cancelNotifyModalVisible}
            onCancel={this.closeCancelNoticeModal}
            onOk={this.closeCancelNoticeModal}
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={500}
          >
            <p>This order has been cancelled!!!</p>
          </ThemeModal> */}

        {/* <div className={cancelNotifyModalVisible ? 'cancel-order-mask' : ''}>  */}
        {/* </div> */}
        <SalesOverview orderId={this.props.match.params.orderId} errorMsg={error} order={currentOrder} />
        {cartContainerSection}
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
const D = withTheme(connect(OrdersModule)(mapStateToProps)(SalesCart))
export default D
