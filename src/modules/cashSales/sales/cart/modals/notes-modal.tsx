import React from 'react'
import { InputLabel, ThemeTextArea } from '~/modules/customers/customers.style'
import { OrderItem } from '~/schema'

export interface NotesModalProps {
  record: OrderItem,
  setNewNote: Function,
  index: number,
  isLocked?: Boolean
}

class NotesModal extends React.PureComponent<NotesModalProps> {
  state: any
  constructor(props: NotesModalProps) {
    super(props)
    this.state = {
      record: props.record,
    }
  }

  handleChange = (e: { target: { value: any } }) => {
    // tslint:disable-next-line:prefer-const
    this.props.setNewNote(e.target.value, this.props.index);
    // this.setState({record: record})
  }

  render() {
    const { record } = this.state
    const { isLocked } = this.props
    return (
      <div style={{ padding: 15 }}>
        {/* <InputLabel>TEMPORARY NOTES</InputLabel> */}
        <InputLabel>NOTE</InputLabel>
        <ThemeTextArea rows={4} defaultValue={record.note} style={{ width: 500 }} onChange={this.handleChange.bind(this)} disabled={isLocked} />

        {/* <InputLabel style={{ marginTop: 15 }}>PERMANENT NOTES</InputLabel> */}
        {/* <ThemeTextArea rows={4} defaultValue={data.permanent_note} style={{ width: 500 }} /> */}
      </div>
    )
  }
}

export default NotesModal
