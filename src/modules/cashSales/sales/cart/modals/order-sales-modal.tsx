import React from 'react'
import { SalesHeader, ThemeTable } from '~/modules/customers/customers.style'
import { Icon } from 'antd'
import { formatNumber } from '~/common/utils'
import moment from 'moment';
import { OrderItem } from '~/schema';

export interface OrderSalesModalProps {
  sales: OrderItem[]
  purchases: OrderItem[]
}

class OrderSalesModal extends React.PureComponent<OrderSalesModalProps> {
  render() {
    const { sales, purchases } = this.props
    const columns = [
      {
        title: 'Ship Date',
        dataIndex: 'shipDate',
        render: (value: any) => value ? moment(value).format('MM/DD/YYYY') : ''
      },
      {
        title: 'Lot #',
        dataIndex: 'lotId',
      },
      {
        title: 'Cust. #',
        dataIndex: 'clientId',
      },
      {
        title: 'Customer',
        dataIndex: 'customer',
      },
      {
        title: 'Sales Price',
        dataIndex: 'salesPrice',
        render: (value: any) => `$${formatNumber(value, 2)}`
      },
      {
        title: 'Units Comitted',
        dataIndex: 'unitCommited',
      },
      {
        title: 'Units Shipped',
        dataIndex: 'unitShipped',
      },
    ]

    const orderCols = [
      {
        title: 'Date',
        dataIndex: 'date',
        render: (value: any) => value ? moment(value).format('MM/DD/YYYY') : ''
      },
      {
        title: 'PO #',
        dataIndex: 'PO',
      },
      {
        title: 'Vendor',
        dataIndex: 'vendor',
      },
      {
        title: 'Carrier',
        dataIndex: 'carrier',
      },
      {
        title: 'Estimated Total Cost',
        dataIndex: 'estimatedTotalCost',
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Set Price',
        dataIndex: 'setPrice',
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Margin %',
        dataIndex: 'margin',
        render: (value: number) => `${formatNumber(value, 2)}%`,
      },
      {
        title: 'Available to Sell',
        dataIndex: 'availableToSell',
        render: (value: number) => formatNumber(value, 2)
      },
      {
        title: 'Units on hand',
        dataIndex: 'inventoryOnHand',
      },
    ]

    return (
      <>
        <SalesHeader>
          <Icon type="reconciliation" style={{ fontSize: 22 }} /> Purchase Order
        </SalesHeader>
        <ThemeTable
          pagination={{ hideOnSinglePage: true, pageSize: 5 }}
          columns={orderCols}
          dataSource={purchases}
          rowKey="PO"
        />
        <SalesHeader>
          <Icon type="shopping-cart" style={{ fontSize: 22 }} /> Sales
        </SalesHeader>
        <ThemeTable
          pagination={{ hideOnSinglePage: true, pageSize: 5 }}
          columns={columns}
          dataSource={sales}
          // rowKey="shipDate"
        />
      </>
    )
  }
}

export default OrderSalesModal
