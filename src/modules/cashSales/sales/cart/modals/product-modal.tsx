import React from 'react'
import jQuery from 'jquery'
import { connect } from 'redux-epics-decorator'
import {
  PopoverWrapper,
  ThemeTabs,
  ThemeIconButton,
  FlexCenter,
  // Incoming,
  ThemeModal,
  ThemeTable,
} from '~/modules/customers/customers.style'
import { Tabs, Table, Icon, Tooltip } from 'antd'
import moment from 'moment'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import SearchInput from '~/components/Table/search-input'
import { ChatAvatar, avatarImg } from '~/modules/customers/accounts/chat/_styles'
import { formatNumber, getFileUrl, inventoryQtyToRatioQty, mathRoundFun } from '~/common/utils'
import OrderSalesModal from './order-sales-modal'
import { ThumbnailImage } from '~/modules/inventory/components/inventory-table.style'
import PriceModal from './price-modal'
import { cloneDeep, isArray } from 'lodash'

const { TabPane } = Tabs
type ProductModalProps = OrdersDispatchProps &
  OrdersStateProps & {
    isAdd: boolean
    hasTabs?: boolean
    selected: string
    id?: number
    salesType: string
    onSelect: Function
    onSearch: Function
  }

class ProductModal extends React.PureComponent<ProductModalProps> {
  state: any
  constructor(props: ProductModalProps) {
    super(props)
    this.state = {
      tabKey: '2',
      visibleOrderModal: false,
      visiblePriceModal: false,
      itemId: 0,
      currentItem: null,
    }
  }

  onChangeTab = (key: string) => {
    this.setState({
      tabKey: key,
    })
  }

  handleOrderModalOk = () => {
    this.setState({
      visibleOrderModal: false,
    })
  }

  openOrderModal = (record: any) => {
    this.props.getAllSales(record.lotId)
    this.props.getAllPurchases(record.lotId)
    this.setState({
      visibleOrderModal: true,
      currentItem: record,
    })
  }

  openPriceModal = (record: any) => {
    console.log(record.wholesaleItem.wholesaleItemId)
    this.setState({
      itemId: record.wholesaleItem.wholesaleItemId,
      visiblePriceModal: true,
    })
  }

  componentDidMount() {
    this.props.getItemForAddItemModal({
      type: this.props.salesType
    }) //set the salesType SELL or PURCHASE to get the items list in each page
    this.props.getAvailableItemLot(this.props.salesType)
    this.getFrequentSalesOrderItems()
    const _this = this

    function setTab(tabIndex: number) {
      // console.log(tabIndex);
      let cards

      if (_this.props.salesType == 'PURCHASE') {
        cards = jQuery(`.modal-order-tab`).find('.ant-input, .ant-tabs-tab, .ant-table-row')
      } else if (_this.props.salesType == 'SELL') {
        cards = jQuery(`.modal-order-tab`).find('.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-row')
      } else {
        cards = jQuery(`.modal-item-tab`).find('.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-row')
      }
      // console.log(cards)
      if (cards.length === 0) {
        return
      }

      let currentCard = jQuery(cards[tabIndex])
      // console.log(currentCard);

      if (currentCard.hasClass('ant-btn')) {
        currentCard.addClass('focused-btn')
      } else if (currentCard.hasClass('ant-input')) {
        //shift tab to input  reset tab css
        currentCard.focus()
        if (cards.length > 2) {
          jQuery(cards[1]).removeClass('tab-focus')
          jQuery(cards[1]).removeClass('focused-row')
        }
        jQuery(cards[cards.length - 1]).removeClass('focused-row')
      } else if (currentCard.hasClass('ant-tabs-tab')) {
        currentCard.addClass('tab-focus')
        if (tabIndex == 1) {
          jQuery(cards[2]).removeClass('tab-focus')
          // jQuery(cards[0]).blur();
        }
        if (tabIndex == 2) {
          jQuery(cards[1]).removeClass('tab-focus')
          if (cards.length > 3) {
            jQuery(cards[3]).removeClass('focused-row')
          }
        }
      } else if (currentCard.hasClass('ant-table-row')) {
        currentCard.addClass('focused-row')
        jQuery(cards[tabIndex - 1]).removeClass('focused-row')
        if (cards.length > 2) {
          jQuery(cards[2]).removeClass('tab-focus')
        }
        tabIndex + 1 < cards.length ? jQuery(cards[tabIndex + 1]).removeClass('focused-row') : ''
      } else if (currentCard.hasClass('ant-pagination-item')) {
        // jQuery(cards[tabIndex]).addClass('ant-pagination-item-active')
        // if(tabIndex > 8){
        //   jQuery(cards[tabIndex-1]).removeClass('ant-pagination-item-active')
        // }
        if (tabIndex == 7) {
          jQuery(cards[7]).removeClass('focused-row')
        }
      } else {
        jQuery(cards[cards.length - 1]).removeClass('focused-row')
      }
      window.localStorage.setItem('SALES-CART-MODAL-INDEX', tabIndex == cards.length ? '0' : tabIndex.toString())
    }
    // jQuery('.ant-input').unbind('focus').bind('focus', (e: any) => {
    window.localStorage.setItem('SALES-CART-MODAL-INDEX', '0')
    setTab(0)
    // });

    function clickRow(tabIndex: number) {
      let { saleItems, availableItemLots, salesType } = _this.props
      let doms
      if (salesType == 'PURCHASE') {
        doms = jQuery(`.modal-order-tab`).find('.ant-input, .ant-tabs-tab, .ant-table-row')
      } else if (salesType == 'SELL') {
        doms = jQuery(`.modal-order-tab`).find('.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-row')
      } else {
        doms = jQuery(`.modal-item-tab`).find('.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-row')
      }
      let orderItemId = jQuery(doms[tabIndex]).data('row-key')

      let { tabKey } = _this.state
      let orderItem
      if (salesType == 'PURCHASE') {
        orderItem = saleItems.filter((obj) => {
          return obj.itemId == orderItemId
        })
      } else {
        if (tabKey == '1') {
          orderItem = saleItems.filter((obj) => {
            return obj.itemId == orderItemId
          })
        } else {
          orderItem = availableItemLots.filter((obj) => {
            return obj.wholesaleOrderItemId == orderItemId
          })
        }
      }

      // console.log(orderItem);
      if (orderItem && orderItem.length > 0) {
        _this.props.onSelect(orderItem[0])
      }
      window.localStorage.setItem('SALES-CART-MODAL-INDEX', '0')
      jQuery(doms[tabIndex]).removeClass('focused-row')
    }

    function handleKeyDown(e: any) {
      let oTabIndex: string | null = window.localStorage.getItem('SALES-CART-MODAL-INDEX')
      let doms
      if (_this.props.salesType == 'PURCHASE') {
        doms = jQuery(`.modal-order-tab`).find('.ant-input, .ant-tabs-tab, .ant-table-row')
      } else if (_this.props.salesType == 'SELL') {
        doms = jQuery(`.modal-order-tab`).find('.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-row')
      } else {
        doms = jQuery(`.modal-item-tab`).find('.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-row')
      }
      let tabIndex: number = 0
      // console.log(oTabIndex,tabIndex);
      if (oTabIndex) {
        tabIndex = parseInt(oTabIndex, 10)
      }
      if (e.keyCode === 9) {
        // Tab
        if (e.shiftKey) {
          tabIndex--
          tabIndex = tabIndex < 0 ? doms.length - 1 : tabIndex
        } else {
          tabIndex++
          tabIndex = tabIndex == doms.length ? 0 : tabIndex
        }
        // if(tabIndex != 0){
        //search need input something

        // }
        setTab(tabIndex)
        e.preventDefault()
      } else if (e.keyCode === 13) {
        // Enter
        if (_this.props.salesType == 'PURCHASE') {
          // add purchase order item
          if (tabIndex > 0 && tabIndex < 6) {
            // table item
            clickRow(tabIndex)
          }
        } else {
          //add sale order item
          if (tabIndex === 1) {
            _this.setState({
              tabKey: '1',
            })
            jQuery(doms[1]).addClass('tab-focus')
          } else if (tabIndex === 2) {
            _this.setState({
              tabKey: '2',
            })
            jQuery(doms[2]).addClass('tab-focus')
          } else if (tabIndex > 2 && tabIndex < 8) {
            // table item
            clickRow(tabIndex)
          }
        }
        if (tabIndex != 0) {
          e.preventDefault()
        }
      } else {
        jQuery(doms[1]).removeClass('tab-focus')
        jQuery(doms[2]).removeClass('tab-focus')
      }
    }

    //add item
    jQuery('.modal-order-tab')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        handleKeyDown(e)
      })
    //change item prduct name
    jQuery('.modal-item-tab')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        handleKeyDown(e)
      })
  }

  getFrequentSalesOrderItems = () => {
    const { currentOrder } = this.props
    if (currentOrder) {
      this.props.getFrequentSalesOrderItems({ clientId: currentOrder.wholesaleClient.clientId, search: '' })
    }
  }

  handleModalOk = () => {
    this.setState({
      visiblePriceModal: false,
    })
  }

  render() {
    const {
      saleItems,
      hasTabs,
      availableItemLots,
      allSales,
      allPurchases,
      salesType,
      frequentItems,
      currentOrder,
    } = this.props
    const { searchStr, visibleOrderModal, currentItem, tabKey, visiblePriceModal, itemId } = this.state
    const selected = this.props.selected.length >= 2 ? this.props.selected : ''
    let filteredItems: any[] = []
    if(saleItems) {
      let matches1: any[] = []
      let matches2: any[] = []
      let matches3: any[] = []
      cloneDeep(saleItems).forEach((row, index) => {
        if(typeof row.SKU === 'undefined' && row.sku) {
          row.SKU = row.sku
        }
        if (row.SKU && row.SKU.toLowerCase().indexOf(selected) === 0) {
          // full match on 2nd filed
          matches2.push(row);
        } else if (row.variety && row.variety.toLowerCase().indexOf(selected) === 0) {
            // full match on variety
          matches1.push(row);
        } else if (
          (row.wholesaleCategory && row.wholesaleCategory.name.indexOf(selected) > 0) ||
          (row.variety && row.variety.toLowerCase().indexOf(selected) > 0) ||
          (row.SKU && row.SKU.toLowerCase().indexOf(selected) > 0)
        ) {
          // others
          matches3.push(row);
        }
      })

      if(matches1) {
        matches1 = matches1.sort((a, b) => a.variety.localeCompare(b.variety))
      }
      if(matches2) {
        matches2 = matches2.sort((a, b) => a.SKU.localeCompare(b.SKU))
      }
      if(matches3) {
        matches3 = matches3.sort((a, b) => a.variety.localeCompare(b.variety))
      }

      filteredItems = [...matches2, ...matches1, ...matches3]
    }

    let filteredItems2: any[] = []
    if(availableItemLots && availableItemLots.length > 0) {
      let matches1: any[] = []
      let matches2: any[] = []
      let matches3: any[] = []
      cloneDeep(availableItemLots).forEach((row, index) => {
        if(typeof row.SKU === 'undefined' && row.sku) {
          row.SKU = row.sku
        }
        if (row.wholesaleItem.SKU && row.wholesaleItem.SKU.toLowerCase().indexOf(selected) === 0) {
          // full match on 2nd filed
          matches2.push(row);
        } else if (row.wholesaleItem.variety.toLowerCase().indexOf(selected) === 0) {
            // full match on variety
          matches1.push(row);
        } else if (
          row.lotId.indexOf(selected) > 0 ||
            row.wholesaleItem.variety.toLowerCase().indexOf(selected) > 0 ||
            (row.wholesaleItem.SKU && row.wholesaleItem.SKU.toLowerCase().indexOf(selected) > 0)
        ) {
          // others
          matches3.push(row);
        }
      })

      if(matches1) {
        matches1 = matches1.sort((a, b) => a.wholesaleItem.variety.localeCompare(b.wholesaleItem.variety))
      }
      if(matches2) {
        matches2 = matches2.sort((a, b) => a.SKU.localeCompare(b.SKU))
      }
      if(matches3) {
        matches3 = matches3.sort((a, b) => a.wholesaleItem.variety.localeCompare(b.wholesaleItem.variety))
      }

      filteredItems2 = [...matches2, ...matches1, ...matches3]
    }


    const filteredItems3 = frequentItems
      ? frequentItems.filter((row) => {
          return (
            (row.wholesaleCategory && row.wholesaleCategory.name.indexOf(selected) >= 0) ||
            (row.variety && row.variety.toLowerCase().indexOf(selected) >= 0) ||
            (row.SKU && row.SKU.toLowerCase().indexOf(selected) >= 0)
          )
        })
      : []

    const columns2: any[] = [
      {
        title: '',
        dataIndex: 'wholesaleItem.cover',
        align: 'center',
        width: 50,
        render: (src: string) => {
          if (src) {
            return <ThumbnailImage src={getFileUrl(src, false)} />
          } else {
            return ''
          }
        },
      },
      // {
      //   title: 'DATE',
      //   dataIndex: 'createdDate',
      //   render: (value: number) => moment(value).format('MM/DD/YYYY'),
      // },

      {
        title: 'ARRIVAL DATE',
        dataIndex: 'deliveryDate',
        width: 120,
        render: (value: number) => {
          const year = moment.utc(value).get('year')
          // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
          if (year != 1990) {
            return moment.utc(value).format('MM/DD/YYYY')
          }
        },
      },
      // {
      //   title: 'UOM',
      //   dataIndex: 'uom',
      // },
      {
        title: 'LOT',
        dataIndex: 'lotId',
      },
      {
        title: 'SKU',
        dataIndex: 'wholesaleItem.sku',
      },
      {
        title: 'PRODUCT NAME',
        dataIndex: 'wholesaleItem.variety',
      },
      {
        title: 'Provider',
        dataIndex: 'modifiers',
        key: 'modifiers',
      },
      {
        title: 'AVAILABLE TO SELL',
        dataIndex: 'lotAvailableQty',
        key: 'available_to_sell',
        width: 200,
        render: (_text: any, item: any, _index: any) => {
          let lotAvailableQty = item.lotAvailableQty === null ? 0 : formatNumber(item.lotAvailableQty, 2)
          let itemInfo = item.wholesaleItem
          if (lotAvailableQty <= 0) {
            return ''
          }
          if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio == true && itemInfo.ratioUOM == 1) {
            return `${lotAvailableQty} ${item.uom ? ' ' + item.uom : ''}`
          } else if (itemInfo.inventoryUOM != itemInfo.baseUOM && itemInfo.constantRatio != true) {
            return lotAvailableQty + (item.uom ? ' ' + item.uom : '')
          } else if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio != true) {
            return lotAvailableQty + (item.uom ? ' ' + item.uom : '')
          } else {
            let ratioQty = formatNumber(lotAvailableQty * item.wholesaleItem.ratioUOM, 2)
            return (
              <div>
                {lotAvailableQty} {item.wholesaleItem.inventoryUOM ? ' ' + item.wholesaleItem.inventoryUOM : ''}
                <br />
                {ratioQty} {item.wholesaleItem.baseUOM ? ' ' + item.wholesaleItem.baseUOM : ''}
              </div>
            )
          }
        },
      },
      {
        title: 'UNITS ON HAND',
        dataIndex: 'onHandQty',
        key: 'onHandQty',
        width: 200,
        render: (_text: any, item: any, _index: any) => {
          let onHandQty = item.onHandQty === null ? 0 : formatNumber(item.onHandQty, 2)
          let itemInfo = item.wholesaleItem
          if (onHandQty <= 0) {
            return ''
          }
          if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio == true && itemInfo.ratioUOM == 1) {
            return `${onHandQty} ${item.uom ? ' ' + item.uom : ''}`
          } else if (itemInfo.inventoryUOM != itemInfo.baseUOM && itemInfo.constantRatio != true) {
            return onHandQty + (item.uom ? ' ' + item.uom : '')
          } else if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio != true) {
            return onHandQty + (item.uom ? ' ' + item.uom : '')
          } else {
            let ratioQty = formatNumber(onHandQty * item.wholesaleItem.ratioUOM, 2)
            return (
              <div>
                {onHandQty} {item.wholesaleItem.inventoryUOM ? ' ' + item.wholesaleItem.inventoryUOM : ''}
                <br />
                {ratioQty} {item.wholesaleItem.baseUOM ? ' ' + item.wholesaleItem.baseUOM : ''}
              </div>
            )
          }
        },
      },
      {
        title: 'COST',
        dataIndex: 'cost',
        key: 'total_cost',
        width: 100,
        render: (value: number, record: any) => {
          // if (record.wholesaleItem.constantRatio == true) {
          //   if (record.uom == record.wholesaleItem.inventoryUOM) {
          //     return (
          //       <div style={{ textAlign: 'right', paddingRight: 20 }}>
          //         ${formatNumber(value ? value / record.wholesaleItem.ratioUOM : 0, 2)}
          //       </div>
          //     )
          //   } else {
          //     return (
          //       <div style={{ textAlign: 'right', paddingRight: 20 }}>
          //         ${formatNumber(value ? value : 0, 2)}
          //       </div>
          //     )
          //   }

          // } else {
          //   if (record.orderWeight > 0 && value > 0) {
          //     return (
          //       <div style={{ textAlign: 'right', paddingRight: 20 }}>
          //         ${formatNumber(value ? (value * record.receivedQty) / record.orderWeight : 0, 2)}
          //       </div>
          //     )
          //   } else {
          //     return (
          //       <div style={{ textAlign: 'right', paddingRight: 20 }}>
          //         ${formatNumber(value ? value : 0, 2)}
          //       </div>
          //     )
          //   }

          // }
          return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? value : 0, 2)}</div>
        },
      },
      // {
      //   title: 'QUOTED PRICE',
      //   dataIndex: 'cost',
      //   key: 'cost',
      //   align: 'right',
      //   width: 100,
      //   render: (value: number, record: any) => {
      //     const markup = (record.margin ? record.margin : 0) / 100
      //     const freight = record.freight ? record.freight : 0
      //     if (record.uom == record.wholesaleItem.inventoryUOM && record.wholesaleItem.constantRatio == true) {
      //       return (
      //         <div style={{ textAlign: 'right', paddingRight: 20 }}>
      //           ${formatNumber(value ? (1 + markup) * (value / record.wholesaleItem.ratioUOM) + freight : 0, 2)}
      //         </div>
      //       )
      //     } else {
      //       return (
      //         <div style={{ textAlign: 'right', paddingRight: 20 }}>
      //           ${formatNumber(value ? (1 + markup) * value + freight : 0, 2)}
      //         </div>
      //       )
      //     }
      //   },
      // },
      {
        title: 'QUOTED PRICE',
        width: 100,
        key: 'priceBar',
        render: (_text: string, record: any) => (
          <FlexCenter>
            <ThemeIconButton type="link" style={{ borderWidth: 0 }} onClick={() => this.openPriceModal(record)}>
              <Icon type="bar-chart" style={{ fontSize: 20 }} />
            </ThemeIconButton>
          </FlexCenter>
        ),
      },
      {
        title: '',
        key: 'bar',
        render: (_text: string, record: any) => (
          <FlexCenter>
            <ThemeIconButton type="link" style={{ borderWidth: 0 }} onClick={() => this.openOrderModal(record)}>
              <Icon type="bar-chart" style={{ fontSize: 20 }} />
            </ThemeIconButton>
          </FlexCenter>
        ),
      },
    ]

    const columns1 = [
      {
        title: '',
        dataIndex: 'cover',
        align: 'center',
        width: 50,
        render: (src: string) => {
          if (src) {
            return <ThumbnailImage src={getFileUrl(src, false)} />
          } else {
            return ''
          }
        },
      },
      {
        title: 'NEXT ARRIVAL',
        dataIndex: 'nextArrival',
        width: 120,
        render: (value: number) => {
          if (value == undefined) {
            return ''
          }
          const year = moment.utc(value).get('year')
          // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
          if (year != 1990) {
            return moment.utc(value).format('MM/DD/YYYY')
          }
        },
      },
      // {
      //   title: 'UOM',
      //   dataIndex: 'baseUOM',
      //   width: 120,
      // },
      {
        title: 'LAST RECEIVED',
        dataIndex: 'lastReceived',
        width: 120,
        render: (value: number) => {
          if (value == undefined) {
            return ''
          }
          const year = moment.utc(value).get('year')
          // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
          if (year != 1990) {
            return moment.utc(value).format('MM/DD/YYYY')
          }
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        width: 150,
      },
      {
        title: 'PRODUCT NAME',
        dataIndex: 'variety',
        width: 200,
      },
      {
        title: 'AVAILABLE TO SELL',
        dataIndex: 'availableQty',
        key: 'available_to_sell',
        align: 'right',
        width: 250,
        render: (_text: any, item: any, _index: any) => {
          let quantity = item.availableQty === null ? 0 : item.availableQty
          if (quantity <= 0) {
            return ''
          }
          // if (item.inventoryUOM == item.baseUOM && item.constantRatio == true && item.ratioUOM == 1) {
          //   return `(${quantity} ${item.baseUOM ? ' ' + item.baseUOM : ''})`
          // } else if (item.inventoryUOM != item.baseUOM && item.constantRatio != true) {
          //   return quantity + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
          // } else if (item.inventoryUOM == item.baseUOM && item.constantRatio != true) {
          //   return quantity + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
          // } else {
          //   let ratioQty = formatNumber(quantity * item.ratioUOM, 2)
          //   return (
          //     <div>
          //       {quantity} {item.inventoryUOM ? ' ' + item.inventoryUOM : ''}
          //       <br />
          //       {ratioQty} {item.baseUOM ? ' ' + item.baseUOM : ''}
          //     </div>
          //   )
          // }
          if (quantity > 0) {
            return isArray(item.wholesaleProductUomList) &&
              item.wholesaleProductUomList.length > 0 &&
              item.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
              <Tooltip
                title={
                  <>
                    {item.wholesaleProductUomList.map((uom: any) => {
                      if (uom.useForInventory) {
                        return (
                          <div>
                            {inventoryQtyToRatioQty(uom.name, quantity, item, 2)} {uom.name}
                          </div>
                        )
                      }
                    })}
                  </>
                }
              >
                <span style={{ marginRight: '20px' }}>
                  {mathRoundFun(quantity, 2)} {item.inventoryUOM}
                </span>
              </Tooltip>
            ) : (
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(quantity, 2)} {item.inventoryUOM}
              </span>
            )
          } else {
            return ''
          }
        },
      },
      {
        title: 'UNITS ON HAND',
        dataIndex: 'onHandQty',
        key: 'inventory_on_hand',
        width: 250,
        align: 'right',
        render(_text: any, item: any, _index: any) {
          let onHandQty = item.onHandQty === null ? 0 : item.onHandQty
          if (onHandQty <= 0) {
            return ''
          }
          // if (item.inventoryUOM == item.baseUOM && item.constantRatio == true && item.ratioUOM == 1) {
          //   return `(${onHandQty} ${item.baseUOM ? ' ' + item.baseUOM : ''})`
          // } else if (item.inventoryUOM != item.baseUOM && item.constantRatio != true) {
          //   return onHandQty + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
          // } else if (item.inventoryUOM == item.baseUOM && item.constantRatio != true) {
          //   return onHandQty + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
          // } else {
          //   let ratioQty = formatNumber(onHandQty * item.ratioUOM, 2)
          //   return (
          //     <div>
          //       {onHandQty} {item.inventoryUOM ? ' ' + item.inventoryUOM : ''}
          //       <br />
          //       {ratioQty} {item.baseUOM ? ' ' + item.baseUOM : ''}
          //     </div>
          //   )
          // }
          if (onHandQty > 0) {
            return isArray(item.wholesaleProductUomList) &&
              item.wholesaleProductUomList.length > 0 &&
              item.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
              <Tooltip
                title={
                  <>
                    {item.wholesaleProductUomList.map((uom: any) => {
                      if (uom.useForInventory) {
                        return (
                          <div>
                            {inventoryQtyToRatioQty(uom.name, onHandQty, item, 2)} {uom.name}
                          </div>
                        )
                      }
                    })}
                  </>
                }
              >
                <span style={{ marginRight: '20px' }}>
                  {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
                </span>
              </Tooltip>
            ) : (
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
              </span>
            )
          } else {
            return ''
          }
        },
      },
    ]

    const columns3 = [
      {
        title: '',
        dataIndex: 'cover',
        align: 'center',
        width: 50,
        render: (src: string) => {
          if (src) {
            return <ThumbnailImage src={getFileUrl(src, false)} />
          } else {
            return ''
          }
        },
      },
      {
        title: 'UOM',
        dataIndex: 'baseUOM',
        width: 120,
      },

      {
        title: 'SKU',
        dataIndex: 'SKU',
        width: 150,
      },
      {
        title: 'PRODUCT NAME',
        dataIndex: 'variety',
        width: 200,
      },
      {
        title: 'AVAILABLE TO SELL',
        dataIndex: 'availableQty',
        key: 'available_to_sell',
        width: 250,
        render: (_text: any, item: any, _index: any) => {
          let quantity = item.availableQty === null ? 0 : formatNumber(item.availableQty, 2)
          if (quantity <= 0) {
            return ''
          }
          if (item.inventoryUOM == item.baseUOM && item.constantRatio == true && item.ratioUOM == 1) {
            return `(${quantity} ${item.baseUOM ? ' ' + item.baseUOM : ''})`
          } else if (item.inventoryUOM != item.baseUOM && item.constantRatio != true) {
            return quantity + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
          } else {
            let ratioQty = formatNumber(quantity * item.ratioUOM, 2)
            return (
              <div>
                {quantity} {item.inventoryUOM ? ' ' + item.inventoryUOM : ''}
                <br />
                {ratioQty} {item.baseUOM ? ' ' + item.baseUOM : ''}
              </div>
            )
          }
        },
      },
      {
        title: 'UNITS ON HAND',
        dataIndex: 'onHandQty',
        key: 'inventory_on_hand',
        width: 250,
        render(_text: any, item: any, _index: any) {
          let onHandQty = item.onHandQty === null ? 0 : formatNumber(item.onHandQty, 2)
          if (onHandQty <= 0) {
            return ''
          }
          if (item.inventoryUOM == item.baseUOM && item.constantRatio == true && item.ratioUOM == 1) {
            return `(${onHandQty} ${item.baseUOM ? ' ' + item.baseUOM : ''})`
          } else if (item.inventoryUOM != item.baseUOM && item.constantRatio != true) {
            return onHandQty + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
          } else {
            let ratioQty = formatNumber(onHandQty * item.ratioUOM, 2)
            return (
              <div>
                {onHandQty} {item.inventoryUOM ? ' ' + item.inventoryUOM : ''}
                <br />
                {ratioQty} {item.baseUOM ? ' ' + item.baseUOM : ''}
              </div>
            )
          }
        },
      },
      {
        title: 'Delivery Date',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        render: (value: number) => {
          return value ? moment(value).format('MM/DD/YYYY') : ''
        },
      },
      {
        title: 'Price',
        dataIndex: 'orderPrice',
        key: 'orderPrice',
        render: (value: number) => {
          return value ? `$${value.toFixed(2)}` : ''
        },
      },
    ]

    // console.log(filteredItems, filteredItems2)
    return (
      <PopoverWrapper className={salesType ? 'modal-order-tab' : 'modal-item-tab'}>
        <ThemeModal
          className="price-modal"
          title="PRICING INFORMATION"
          closable={false}
          keyboard={true}
          visible={visiblePriceModal}
          onOk={this.handleModalOk}
          onCancel={this.handleModalOk}
          okText="Close [esc]"
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={1330}
          footer={null}
          style={{ height: 868 }}
          centered
        >
          <PriceModal itemId={itemId} clientId={currentOrder ? currentOrder.wholesaleClient.clientId.toString() : ''} />
        </ThemeModal>
        <ThemeModal
          className="order-sales-modal"
          title={currentItem ? currentItem.lotId : ''}
          keyboard={true}
          okText="Go Back [esc]"
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={1200}
          visible={visibleOrderModal}
          onOk={this.handleOrderModalOk}
          onCancel={this.handleOrderModalOk}
        >
          <OrderSalesModal sales={allSales} purchases={allPurchases} />
        </ThemeModal>
        <SearchInput
          handleSearch={this.props.onSearch}
          placeholder="Search by PRODUCT NAME, SKU, LOT#"
          defaultValue={this.props.selected}
        />
        {hasTabs === true ? (
          <ThemeTabs
            defaultActiveKey={tabKey}
            activeKey={tabKey}
            onChange={this.onChangeTab}
            type="card"
            style={{ marginBottom: -30 }}
          >
            <TabPane tab="Products Catalog" key="1">
              <ThemeTable
                pagination={{ hideOnSinglePage: true, pageSize: 5 }}
                columns={columns1}
                dataSource={filteredItems}
                rowKey="itemId"
                onRow={(record, _rowIndex) => {
                  return {
                    onClick: (_event) => {
                      this.props.onSelect(record)
                    },
                  }
                }}
              />
            </TabPane>
            <TabPane tab="Lots Catalog" key="2">
              <ThemeTable
                pagination={{ hideOnSinglePage: true, pageSize: 5 }}
                columns={columns2}
                dataSource={filteredItems2}
                rowKey="wholesaleOrderItemId"
                onRow={(record, _rowIndex) => {
                  return {
                    onClick: (_event) => {
                      _event.preventDefault()
                      if (_event.target.tagName !== 'BUTTON') {
                        this.props.onSelect(record)
                      }
                    },
                  }
                }}
              />
            </TabPane>
            <TabPane tab="Frequent Items" key="3">
              <ThemeTable
                pagination={{ hideOnSinglePage: true, pageSize: 5 }}
                columns={columns3}
                dataSource={filteredItems3}
                rowKey="itemId"
                onRow={(record, _rowIndex) => {
                  return {
                    onClick: (_event) => {
                      this.props.onSelect(record)
                    },
                  }
                }}
              />
            </TabPane>
          </ThemeTabs>
        ) : (
          <ThemeTable
            style={{ marginTop: 20, marginBottom: -30 }}
            pagination={{ hideOnSinglePage: true, pageSize: 5 }}
            columns={columns1}
            dataSource={filteredItems}
            rowKey="itemId"
            // tslint:disable-next-line:jsx-no-lambda
            onRow={(record, _rowIndex) => {
              return {
                onClick: (_event) => {
                  this.props.onSelect(record)
                },
              }
            }}
          />
        )}
      </PopoverWrapper>
    )
  }
}

// export default ProductModal
const mapStateToProps = (state: GlobalState) => state.orders
export default connect(OrdersModule)(mapStateToProps)(ProductModal)
