import React from 'react'
import { connect } from 'redux-epics-decorator'
import { Col, Layout, Row, Popconfirm, Icon } from 'antd'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { transLayout } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { ThemeIcon, QuantityTableWrapper, ThemeSpin } from '~/modules/customers/customers.style'
import { OrderItem } from '~/schema'
import EditableTable from '~/components/Table/editable-table'
import jQuery from 'jquery'
import { Http } from '~/common'

type QuantityModalProps = OrdersDispatchProps &
  OrdersStateProps & {
    orderItem: OrderItem
    isWO?: boolean
    updateSaleOrderItemForWO?: Function
  }

const newRow = {
  id: -1,
  unitWeight: '',
}
class QuantityModal extends React.PureComponent<QuantityModalProps> {
  selectedIndex: number = -1

  state = {
    newValue: '',
    items: [],
    loaded: false,
    pageIndex: 0,
    ids: [],
  }

  componentDidMount() {
    this.props.resetLoading()
    this._getQuantityCatchWeight(this.props.orderItem.wholesaleOrderItemId)

    this.setLastFocused()
    let that = this
    jQuery('.quantity-detail-table')
      .unbind('keydown')
      .bind('keydown', function(e: any) {
        setTimeout(() => {
          that.handleTabKeyDown(e)
        }, 100)
      })
  }

  componentWillReceiveProps(nextProps: QuantityModalProps) {
    if (JSON.stringify(nextProps.orderItem) !== JSON.stringify(this.props.orderItem) && nextProps.orderItem !== null) {
      // new modal
      this.setState({
        newValue: '',
        items: [],
        loaded: false,
        pageIndex: 0,
        ids: [],
      })
      this.props.resetLoading()
      this._getQuantityCatchWeight(nextProps.orderItem.wholesaleOrderItemId)
    }
    if (this.props.loading !== nextProps.loading && nextProps.loading === false) {
      if (this.state.loaded === false) {
        let ids = []
        nextProps.catchWeight.forEach((v, i) => {
          ids.push(v.id)
        })
        this.setState({
          items: nextProps.catchWeight,
          loaded: true,
          ids,
        })
      }
    }
    if (JSON.stringify(nextProps.catchWeight) !== JSON.stringify(this.props.catchWeight)) {
      const orderItem = { ...nextProps.orderItem, ...{ picked: nextProps.catchWeight.length } }
      if (this.props.isWO === true) {
        const filled = nextProps.catchWeight.length
        let totalWeight = 0
        if (filled > 0) {
          nextProps.catchWeight.forEach((item: any) => {
            if (item.unitWeight !== '') {
              totalWeight += parseFloat(item.unitWeight)
            }
          })
        }
        orderItem.weightActual = totalWeight.toFixed(1)
        this.props.updateSaleOrderItemForWO(orderItem)
      } else {
        this.props.updateSaleOrderItem(orderItem)
      }
    }
  }

  componentDidUpdate(prevProps: QuantityModalProps) {
    if (prevProps.catchWeight.length != this.props.catchWeight.length) {
      setTimeout(() => {
        this.setLastFocused()
      }, 100)
    }

    const _this = this
    jQuery('.quantity-detail-table .ant-table-body tr td:nth-child(2)')
      .unbind('click')
      .bind('click', function(e: any) {
        const tr = jQuery(e.currentTarget).parents('.ant-table-row')
        const rowIndex = jQuery(tr[0]).prevAll().length - 1
        _this.selectedIndex = rowIndex
      })
  }

  setLastFocused = () => {
    setTimeout(() => {
      const editableRows = jQuery('.quantity-detail-table table tbody tr')
      this.selectedIndex = editableRows.length - 1
      const last = jQuery(editableRows[this.selectedIndex]).find('.tab-able')[0]
      jQuery(last).trigger('click')
    }, 100)
  }

  handleTabKeyDown = (e: any) => {
    const editableRows = jQuery('.quantity-detail-table table tbody tr')
    const paginations = jQuery('.quantity-detail-table .ant-pagination li:not(.ant-pagination-disabled)')
    const tblRows = editableRows.length
    if (this.selectedIndex < 0) {
      this.selectedIndex = tblRows - 1
    }

    jQuery.merge(editableRows, paginations)

    if (e.keyCode == 9) {
      if (e.shiftKey) {
        if (this.selectedIndex > 0) this.selectedIndex--
        else this.selectedIndex = editableRows.length - 1
      } else {
        if (this.selectedIndex < editableRows.length - 1) this.selectedIndex++
        else this.selectedIndex = 0
      }

      setTimeout(() => {
        if (this.selectedIndex < tblRows)
          jQuery(jQuery(editableRows[this.selectedIndex]).find('.tab-able')[0]).trigger('click')
        else {
          jQuery(editableRows[this.selectedIndex]).trigger('focus')
        }
      }, 100)
    }
  }

  onPageChange = (page: any, pageSize: number) => {
    this.setState(
      {
        pageIndex: page - 1,
      },
      () => {
        setTimeout(() => {
          this.setLastFocused()
        }, 100)
      },
    )
  }

  _getQuantityCatchWeight = (orderItemId: string) => {
    this.props.getQuantityCatchWeight(orderItemId)
  }

  onDeleteRow = (index: number, row: any) => {
    let ids = [...this.state.ids]
    const realIndex = this.state.pageIndex * 10 + index
    const rowId = ids[realIndex]

    this.props.resetLoading()
    if (this.props.isWO === true) {
      this.props.deleteQuantityCatchWeightForWO({
        orderItemId: this.props.orderItem.wholesaleOrderItemId,
        qtyDetailId: rowId,
      })
    } else {
      this.props.deleteQuantityCatchWeight({
        orderItemId: this.props.orderItem.wholesaleOrderItemId,
        qtyDetailId: rowId,
      })
    }

    let items = [...this.state.items]
    items.splice(index, 1)
    ids.splice(realIndex, 1)
    this.setState({
      ids,
    })

    this.setState({
      items,
    })
  }

  onChangeNew = (e: any) => {
    this.setState({
      newValue: e.target.value,
    })
  }

  handleSave = (row: any) => {
    if (!row) {
      return
    }
    if (row.id === -1) {
      // create new
      if (row.unitWeight !== '') {
        this.props.resetLoading()
        const items = JSON.parse(JSON.stringify(this.state.items))
        items.push({
          id: new Date().getTime(),
          unitWeight: row.unitWeight,
        })

        let ids = [...this.state.ids]
        ids.push(-1)
        this.setState({ items, ids })

        const orderItemId = this.props.orderItem.wholesaleOrderItemId
        const http = new Http()
        http
          .post(`/inventory/addQuantityCatchWeight/${orderItemId}?timestamp=${new Date().getTime()}`, {
            body: JSON.stringify({
              unitWeight: row.unitWeight,
            }),
          })
          .subscribe((res) => {
            if (typeof res === 'string') {
              // error
            } else {
              const addedCatchWeight = res.body.data
              const ids: number[] = [...this.state.ids]

              // find first -1
              const newIndex = ids.findIndex((id) => {
                return id === -1
              })
              ids[newIndex] = addedCatchWeight.id
              this.setState({ ids })

              if (this.props.isWO === true) {
                this.props.getQuantityCatchWeight(orderItemId)
              } else {
                this.props.reloadQuantityCatchWeight({
                  orderItemId,
                })
              }
            }
          })
      }
    } else {
      // update
      let ids = [...this.state.ids]
      const realIndex = this.state.pageIndex * 10 + row.rowIndex
      const rowId = ids[realIndex]
      if (typeof rowId === 'undefined') {
        return
      }
      const items = [...this.state.items]
      const currentItem = items[realIndex]
      if (currentItem && currentItem.unitWeight !== row.unitWeight) {
        this.props.resetLoading()
        if (this.props.isWO === true) {
          this.props.updateQuantityCatchWeightForWO({
            qtyDetailId: rowId,
            orderItemId: this.props.orderItem.wholesaleOrderItemId,
            unitWeight: row.unitWeight != '' ? row.unitWeight : 0,
          })
        } else {
          const orderItemId = this.props.orderItem.wholesaleOrderItemId
          const http = new Http()
          http
            .post(`/inventory/updateQuantityCatchWeight/${rowId}?timestamp=${new Date().getTime()}`, {
              body: JSON.stringify({
                orderItemId,
                unitWeight: row.unitWeight != '' ? row.unitWeight : 0,
              }),
            })
            .subscribe((res) => {
              if (typeof res === 'string') {
                // error
              } else {
                this.props.reloadQuantityCatchWeight({
                  orderItemId,
                })
              }
            })
        }
      }

      items[realIndex].unitWeight = row.unitWeight
      this.setState({
        items,
      })
    }
  }

  _getCaseNumber = (index) => {
    return `Case ${this.state.pageIndex * 10 + index + 1}`
  }

  render() {
    const { orderItem, loading, catchWeight } = this.props
    const { items, ids } = this.state
    let totalWeight = 0
    if (items.length > 0) {
      items.forEach((item: any) => {
        if (item.unitWeight !== '') {
          totalWeight += parseFloat(item.unitWeight)
        }
      })
    }

    const catchWeightList = [...items, { ...newRow }]

    if (!orderItem) {
      return null
    }

    const columns = [
      {
        title: 'UNIT #',
        dataIndex: 'id',
        key: 'unit',
        width: 180,
        render: (v: number, item: any, index: number) => {
          if (item.id === -1) return null
          return this._getCaseNumber(index)
        },
      },
      {
        title: 'QUANTITY',
        dataIndex: 'unitWeight',
        editable: true,
        edit_width: 370,
        width: 400,
        render: (v: number, item: any, index: number) => {
          return item.id !== -1 ? (
            v
          ) : (
            <div style={{ width: 475, height: 35, border: '1px solid #d9d9d9', borderRadius: 4 }} />
          )
        },
      },

      {
        title: 'PRICING UOM',
        width: 120,
        render: () => {
          return `${orderItem.baseUOM}`
        },
      },
      {
        title: '',
        key: 'action',
        align: 'center',
        render: (v: number, item: any, index: number) => {
          if (item.id === -1) {
            return null
          }

          return ids[index] === -1 ? (
            <ThemeSpin spinning={true} />
          ) : (
            <Popconfirm
              title="Permanently delete this?"
              okText="Delete"
              onConfirm={this.onDeleteRow.bind(this, index, item)}
            >
              <ThemeIcon type="close" />
            </Popconfirm>
          )
        },
      },
    ]

    return (
      <>
        <Layout style={transLayout}>
          <Row>
            <Col md={8}>PRODUCT</Col>
            <Col md={8} />
            <Col md={8}>SKU</Col>
          </Row>
          <Row>
            <Col md={8}>{orderItem.variety}</Col>
            <Col md={8} />
            <Col md={8}>{orderItem.SKU}</Col>
          </Row>
          <Row style={{ marginTop: 5 }}>
            <Col md={8}>LOT</Col>
            <Col md={8}>PRICING UOM</Col>
            <Col md={8}>TOTAL QUANTITY</Col>
          </Row>
          <Row style={{ minHeight: 50 }}>
            <Col md={8}>{orderItem.lotId}</Col>
            <Col md={8}>{orderItem.baseUOM}</Col>
            <Col md={8}>
              {totalWeight.toFixed(2)} {orderItem.baseUOM}
            </Col>
          </Row>
          <QuantityTableWrapper className="quantity-detail-table">
            <EditableTable
              pagination={{
                hideOnSinglePage: true,
                pageSize: 10,
                defaultCurrent: Math.ceil(catchWeightList.length / 10),
                onChange: this.onPageChange,
              }}
              columns={columns}
              dataSource={catchWeightList}
              handleSave={this.handleSave}
            />
          </QuantityTableWrapper>
        </Layout>
      </>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default connect(OrdersModule)(mapStateToProps)(QuantityModal)
