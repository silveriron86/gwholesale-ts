import styled from '@emotion/styled'
import { Layout, Row } from 'antd'
import { badgeColor, medLightGrey } from '~/common'

export const PrintPickSheetWrapper = styled('div')({
  padding: '40px 30px',
  backgroundColor: 'transparent',
  '.f12': {
    fontSize: '12px !important',
  },
  '.f14': {
    fontSize: '14px !important',
  },
  '.f10': {
    fontSize: '10px !important',
  },
})

export const HeaderTextRightBox = styled('div')({
  textAlign: 'right',
  fontWeight: 'bold',
  '&.normal': {
    fontWeight: 'normal',
  },
})

export const HeaderTextCenterBox = styled('div')({
  textAlign: 'center',
})

export const HeaderTextLeftBox = styled('div')({
  textAlign: 'left',
  fontWeight: 'bold',
  '&.normal': {
    fontWeight: 'normal',
  },
  '&.picksheet': {
    fontWeight: 500,
    fontSize: 12,
  },
})

export const HeaderBox = styled('div')({
  '.ant-col': {
    fontWeight: 'normal',
    color: 'black',
  },
})

export const WeightSpan = styled('div')({
  minWidth: 50,
  padding: '2px 4px 2px 0',
  fontSize: '13px !important',
})

export const WeightWrapper = styled('div')({
  // textAlign: 'center',
  display: 'flex',
  // paddingLeft: 50
})

export const ModalDetailText = styled('p')({
  marginTop: 3,
  padding: '0px 0px 20px',
  fontSize: 12,
  fontWeight: 'normal',
  letterSpacing: 'normal',
  color: 'rgba(0, 0, 0, 0.8)',
  b: {
    fontSize: 14,
  },
})

export const UnderLineWrapper = styled('div')({
  marginLeft: 40,
  marginTop: 30,
  marginBottom: 40,
  display: 'flex',
  justifyContent: 'space-between',
  width: '60%',
})

export const Underline = styled('div')({
  flex: 1,
  marginLeft: 2,
  // minWidth: '65%',
  borderBottom: '1px solid black',
})

export const DeliverySction = styled('div')({
  border: '1px solid black',
  width: '100%',
  padding: 5,
  marginTop: 10,
  color: 'black',
  '& span': {
    fontSize: 10,
    fontWeight: 100,
    letterSpacing: 0,
  },
  '& h4': {
    fontWeight: 'bold',
  },
})

export const PrintPickSheetTable = styled('div')({
  margin: '20px 0',
  '& .ant-table-tbody': {
    fontSize: '12px !important',
    // fontWeight:100,
  },
  '& .ant-table-tbody td': {
    // padding: '0',
    paddingTop: 5,
    paddingBottom: 5,
    verticalAlign: 'middle!important;',
  },
  '.ant-table-expanded-row': {
    td: {
      padding: '0 !important',
      border: 0,
    },
  },
  '& .ant-table-tbody td svg': {
    // marginTop: 1
  },
  '&.pick-sheet': {
    '.ant-table-column-title': {
      fontSize: '12px !important',
      fontWeight: '500 !important',
    },
    '.ant-table-tbody': {
      fontSize: '14px !important',
      fontWeight: '500 !important',
    },
  },
  '.ant-table-thead > tr > th, .ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr > td': {
    backgroundColor: 'white',
  },
  th: {
    '&.th-left': {
      paddingLeft: '16px !important',
    },
  },
  '& .alternate-row': {
    'thead th': {
      backgroundColor: 'rgba(196,196,196,0.2) !important',
      borderBottom: '1px solid #EDF1EE !important',
      '&.name-order': {
        background: 'white !important',
        textAlign: 'left !important',
        paddingLeft: 0,
        paddingTop: 0,
      },
    },
    'tbody tr': {
      td: {
        borderBottom: '1px solid #EDF1EE !important',
      },
      '&.alternative': {
        td: {
          backgroundColor: 'rgba(196, 196, 196, 0.2) !important',
        },
      }
    },
  },
})

export const DeliveryListTable = styled('div')({
  fontWeight: 500,
  '& .ant-table thead th.ant-table-expand-icon-th': {
    display: 'none',
  },
  '& .ant-table tbody td.ant-table-row-expand-icon-cell': {
    display: 'none',
  },
  '& .ant-table-row': {
    fontSize: '13px',
    'td': {
      padding: '5px 0',
    }
  },
  th: {
    paddingLeft: 0,
    paddingRight: 0,
    span: {
      fontWeight: '500 !important',
    },
    '&.th-left': {
      paddingLeft: '16px !important',
    },
  },
  'th, td': {
    background: 'white !important',
    borderLeft: '0 !important',
    borderRight: '0 !important',
  },
  // '& .alternate-row': {
  //   'thead th': {
  //     backgroundColor: 'rgba(196,196,196,0.2) !important',
  //     '&.name-order': {
  //       background: 'white !important',
  //       textAlign: 'left !important',
  //       paddingLeft: 0,
  //       paddingTop: 0
  //     }
  //   },
  //   'tbody tr': {
  //     '&.alternative': {
  //       td: {
  //         backgroundColor: 'rgba(196, 196, 196, 0.2) !important'
  //       }
  //     }
  //   }
  // }
})

export const EllipseText = styled('div')({
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
})

export const FlexWrap = styled('div')({
  display: 'flex',
  flexWrap: 'wrap',
  paddingTop: 5,
  paddingBottom: 5,
  maxWidth: 900,
  '.ant-input': {
    margin: '0 10px',
    background: 'transparent',
    border: 0,
    outline: 'none',
    borderBottom: '1px solid #cdcdcd',
    borderRadius: 0,
    width: 80,
  },
})

export const Inputs = styled('div')({
  width: '100%',
  height: '100%',
  padding: 0,
  borderBottom: '1px solid #EDF1EE',
  '.ant-input': {
    marginLeft: '0 !important',
    '&:nth-child(10n+0)': {
      marginRight: '0 !important',
    },
  },
})

export const LightColoredRow = styled(Row)((props: any) => ({
  background: props.theme.lighter,
  color: props.theme.primary,
}))

export const pl24: React.CSSProperties = {
  paddingLeft: 24,
}
