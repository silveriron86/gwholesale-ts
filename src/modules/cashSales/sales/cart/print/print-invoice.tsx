import React from 'react'
import { OrderDetail, OrderItem } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode'
import {
  HeaderTextRightBox,
  HeaderTextLeftBox,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
  ModalDetailText,
  UnderLineWrapper,
  Underline,
  DeliverySction,
  DeliveryListTable,
  WeightSpan,
  WeightWrapper,
  LightColoredRow,
  pl24,
  DisplayLabel,
  DisplayValue,
  DisplayLeftLabel,
} from '~/modules/customers/sales/cart/print/_styles'
import Moment from 'react-moment'
import {
  formatNumber,
  formatAddress,
  basePriceToRatioPrice,
  mathRoundFun,
  getOrderPrefix,
  inventoryQtyToRatioQty,
  ratioQtyToInventoryQty,
  numberMultipy,
  judgeConstantRatio,
  formatItemDescription,
} from '~/common/utils'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import { Icon } from '~/components'
import { timingFunctions } from 'polished'
import { timeout } from 'rxjs/operators'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import _, { cloneDeep } from 'lodash'
import moment from 'moment'
import { PrintSetting } from '~/modules/setting/components/print-setting'
import { connect } from 'react-redux'

interface PrintBillOfLadingProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  logo: string
  company: any
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  title?: string
  multiple?: boolean
  printSetting: any
  type: string
  changePrintLogoStatus: Function
  fulfillmentOptionType?: number
  sellerSetting?: any
}

export class PrintInvoice extends React.PureComponent<PrintBillOfLadingProps> {
  state = {
    totalQuantity: 0,
    totalAmount: 0,
    totalWeight: 0,
    visibleAlert: false,
    selectedRowKeys: [],
    orderItems: this.props.orderItems,
    pagePrintSetting: null,
  }

  componentDidMount(): void {
    this.getAmount(this.props.orderItems)
    if (this.props.printSetting) {
      this.setPrintSetting(this.props.printSetting, this.props.type)
    }
  }

  componentWillReceiveProps(nextProps: PrintBillOfLadingProps) {
    if (this.props.orderItems !== nextProps.orderItems) {
      this.getAmount(nextProps.orderItems)
      this.setState({ orderItems: nextProps.orderItems })
    }

    if (this.props.printSetting != nextProps.printSetting && nextProps.printSetting != null) {
      this.setPrintSetting(nextProps.printSetting, nextProps.type)
    }
  }

  setPrintSetting = (jsonSetting: string, modalType: string) => {
    const pageSetting = JSON.parse(jsonSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`${modalType}_`) > -1) {
        const newKey = el.substring(`${modalType}_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    this.setState({ pagePrintSetting: data })
  }

  getAmount = (items: any) => {
    let _totalQuantity = 0
    let _totalAmount = 0
    let _totalWeight = 0

    for (let i = 0; i < items.length; i++) {
      let amount = 0
      let constantRatio = judgeConstantRatio(items[i])
      _totalQuantity += _.toNumber(items[i].picked)

      let pricingUOM = items[i].pricingUOM ? items[i].pricingUOM : items[i].UOM
      let unitUOM = items[i].UOM
      let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(items[i].price), items[i], 12)

      if (items[i].oldCatchWeight) {
        _totalWeight += _.toNumber(items[i].catchWeightQty)
        amount = numberMultipy(_.toNumber(items[i].price), _.toNumber(items[i].catchWeightQty))
      } else {
        if (!constantRatio) {
          _totalWeight += inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i])
          amount = numberMultipy(
            ratioPrice,
            inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i], 12),
          )
        } else {
          const quantity = inventoryQtyToRatioQty(
            pricingUOM,
            ratioQtyToInventoryQty(unitUOM, _.toNumber(items[i].picked), items[i], 12),
            items[i],
            12,
          )
          _totalWeight += quantity
          amount = numberMultipy(ratioPrice, quantity)
        }
      }
      items[i]['amount'] = '$' + formatNumber(amount, 2)
      _totalAmount += amount
    }

    this.setState({
      totalQuantity: _totalQuantity,
      totalAmount: _totalAmount,
      totalWeight: _totalWeight,
      orderItems: items,
    })
  }

  getExpandableRowKeys = () => {
    const { orderItems } = this.state
    let result: any[] = []
    orderItems.forEach((el) => {
      if (!el.constantRatio) {
        result.push(el.wholesaleOrderItemId)
      }
    })
    return result
  }

  renderCatchWeightValues = (weights: any[]) => {
    // const { catchWeightValues, multiple } = this.props

    // let weights: any[] = []
    // if (multiple && typeof record.catchWeightValues !== 'undefined') {
    //   weights = record.catchWeightValues
    // } else {
    //   weights = catchWeightValues[record.wholesaleOrderItemId] ? catchWeightValues[record.wholesaleOrderItemId] : []
    // }
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (type == 'invoice' && pagePrintSetting && !pagePrintSetting.catchWeightValues) {
    }
    if (weights.length > 0) {
      let result: any[] = []
      let sub: any[] = []
      weights.forEach((el: any, index: number) => {
        if (index > 0 && (index - 10) % 10 == 0) {
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
          sub = []
        }

        const weightEl = <WeightSpan>{el.unitWeight ? el.unitWeight : ''}</WeightSpan>
        if (index == weights.length - 1) {
          sub.push(weightEl)
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
        } else {
          sub.push(weightEl)
        }
      })
      return result
    }

    return []
  }

  checkWeights = (row: any) => {
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (type == 'invoice' && pagePrintSetting && !pagePrintSetting.catchWeightValues) {
      return false
    }
    return typeof row.weights !== 'undefined' && row.weights.length > 0
  }

  getDisplayingColumns = (columns: any[]) => {
    const { pagePrintSetting } = this.state
    let newColumns: any[] = []
    if (!pagePrintSetting) return columns

    columns.forEach((el) => {
      const key = el.key
      if (pagePrintSetting[key] !== false) {
        newColumns.push(el)
      }
    })
    return newColumns
  }

  onImageLoad = () => {
    this.props.changePrintLogoStatus()
  }

  getNameAndOrderNo = () => {
    const { currentOrder, sellerSetting } = this.props
    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'

    return (
      <HeaderTextRightBox style={{ color: 'black' }}>
        {clientName} | #{getOrderPrefix(sellerSetting, 'sales')}
        {currentOrder.wholesaleOrderId}
      </HeaderTextRightBox>
    )
  }

  render() {
    const {
      currentOrder,
      title,
      type,
      fulfillmentOptionType,
      sellerSetting,
      catchWeightValues,
      multiple,
      printSetting,
      company,
    } = this.props
    const { orderItems, pagePrintSetting } = this.state
    const isFreshGreen = company.companyName === 'Fresh Green' || company.companyName === 'Fresh Green Inc'

    const carrierSetting = pagePrintSetting ? pagePrintSetting.carrier_reference : false
    const origin = pagePrintSetting ? pagePrintSetting.origin : false
    const soldto = pagePrintSetting ? pagePrintSetting.soldto : false
    const shipto = pagePrintSetting ? pagePrintSetting.shipto : false

    let tableClass = ''
    if (printSetting) {
      const setting = JSON.parse(printSetting)
      if (
        (type === 'invoice' && setting.invoice_alternateRow === true) ||
        (type !== 'invoice' && setting.bill_alternateRow === true)
      ) {
        tableClass = 'alternate-row'
      }
    }

    let items = cloneDeep(orderItems)
    orderItems.forEach((record, i) => {
      const index = items.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
      items[index].key = i
      if (
        type == 'bill' ||
        (type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightValues)))
      ) {
        let showCatchWeightModal = false
        let constantRatio = judgeConstantRatio(record)
        if (!constantRatio) {
          showCatchWeightModal = true
        }
        if (showCatchWeightModal) {
          let weights: any[] = []
          if (multiple && typeof record.catchWeightValues !== 'undefined') {
            weights = record.catchWeightValues
          } else {
            weights = catchWeightValues[record.wholesaleOrderItemId]
              ? catchWeightValues[record.wholesaleOrderItemId]
              : []
          }

          if (weights.length > 0) {
            items.splice(index + 1, 0, {
              // ...cloneDeep(record),
              relatedOrderItemId: record.wholesaleOrderItemId,
              weights,
            })
          }
        }
      }
    })

    // const expandedRowKeys = this.getExpandableRowKeys()
    const columns: Array<any> = [
      {
        title: '#',
        dataIndex: 'key',
        key: 'index',
        align: 'center',
        width: 80,
        render: (key: number, row: OrderItem) => {
          return this.checkWeights(row) ? '' : key + 1
        },
      },
      {
        title: 'ORDERED',
        dataIndex: 'quantity',
        key: 'quantity',
        align: 'right',
        width: 80,
        render: (value: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }

          return mathRoundFun(value, 2)
        }
      },
      {
        title: 'SHIPPED',
        dataIndex: 'picked',
        key: 'picked',
        align: 'right',
        // width: 80,
        render: (value: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }
          return mathRoundFun(value, 2)
        }
      },
      {
        title: 'SHIPPED UOM',
        dataIndex: 'UOM',
        key: 'UOM',
        align: 'center',
        width: 80,
      },
      {
        title: 'PRODUCT',
        dataIndex: 'variety',
        key: 'variety',
        align: 'left',
        className: 'th-left',
        render: (variety: string, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return {
              children: this.renderCatchWeightValues(row.weights),
              props: {
                colSpan: 8,
              },
            }
          }

          if (row.itemId) {
            return row.editedItemName ? row.editedItemName : formatItemDescription(variety, row.SKU, sellerSetting)
          }
          const desc = row.chargeDesc ? ` (${row.chargeDesc})` : ''
          return `${variety}${desc}`
        },
      },
      {
        title: 'BRAND',
        dataIndex: 'modifiers',
        key: 'modifiers',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'extraOrigin',
        key: 'extraOrigin',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'BILLABLE QTY',
        dataIndex: 'catchWeightQty',
        key: 'catchWeightQty',
        align: 'right',
        width: 80,
        render: (val: any, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showBillableQty,
            showValue = false
          if (record.picked || record.catchWeightQty) {
            showValue = true
          }

          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM

          let constantRatio = judgeConstantRatio(record)
          if (record.oldCatchWeight) {
            showBillableQty = record.catchWeightQty
          } else {
            if (!constantRatio) {
              showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
            } else {
              showBillableQty = inventoryQtyToRatioQty(
                pricingUOM,
                ratioQtyToInventoryQty(unitUOM, record.picked, record, 12),
                record,
              )
            }
          }

          return showValue ? `${mathRoundFun(showBillableQty, 2)}${pricingUOM != null ? ' ' + pricingUOM : ''}` : ''
        },
      },
      // {
      //   title: 'BILLABLE UOM',
      //   dataIndex: 'UOM',
      //   key: 'UOM',
      //   align: 'center',
      //   render: (price: number, record: any) => {
      //     return record.pricingUOM ? record.pricingUOM : record.baseUOM
      //   },
      // },
      {
        title: 'PRICE',
        dataIndex: 'price',
        align: 'right',
        key: 'price',
        width: 120,
        render: (price: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          var uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          price = basePriceToRatioPrice(uom, price, record)
          return (
            <div style={{ fontWeight: 500 }}>
              ${formatNumber(price, 2)}
              {uom != null ? '/' + uom : ''}
            </div>
          )
        },
      },
      {
        title: 'SUBTOTAL',
        dataIndex: 'quantity',
        key: 'total',
        align: 'right',
        width: 120,
        render: (data: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showValue = false
          let total = 0

          if (record.picked || record.catchWeightQty) {
            showValue = true
          }
          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM
          let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(record.price), record, 12)

          let constantRatio = judgeConstantRatio(record)

          if (!constantRatio) {
            total = numberMultipy(
              ratioPrice,
              inventoryQtyToRatioQty(pricingUOM, _.toNumber(record.catchWeightQty), record, 12),
            )
          } else {
            let quantity = inventoryQtyToRatioQty(
              pricingUOM,
              ratioQtyToInventoryQty(unitUOM, _.toNumber(record.picked), record, 12),
              record,
              12,
            )
            total = numberMultipy(ratioPrice, quantity)
          }
          return <div style={{ fontWeight: 500 }}>{showValue && total ? `$${formatNumber(total, 2)}` : ''}</div>
        },
      },
    ]
    if (
      !pagePrintSetting ||
      (pagePrintSetting && (pagePrintSetting.lot || typeof pagePrintSetting.lot === 'undefined'))
    ) {
      const sizeIndex = columns.findIndex((el) => el.dataIndex == 'extraOrigin')
      if (sizeIndex <= 1) return
      columns.splice(sizeIndex + 1, 0, {
        title: 'LOT',
        dataIndex: 'lotId',
        key: 'lotId',
        width: 120,
        align: 'center',
        render: (value: string, row: any) => {
          return (
            <div>
              {_.get(row, 'lotIds', []).map((lotId) => (
                <span key={lotId} style={{ display: 'block' }}>
                  {lotId}
                </span>
              ))}
            </div>
          )
        },
      })
    }

    const totals = (
      <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24}>
        <Col span={8}>
          <Row gutter={24}>
            <Col span={16}>
              <HeaderTextLeftBox className="normal">TOTAL UNITS</HeaderTextLeftBox>
            </Col>
            <Col span={8}>
              <HeaderTextLeftBox>{this.state.totalQuantity}</HeaderTextLeftBox>
            </Col>
          </Row>
        </Col>
        <Col span={8}>
          {type === 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightQty)) && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextLeftBox className="normal">TOTAL BILLABLE QTY</HeaderTextLeftBox>
              </Col>
              <Col span={8}>
                <HeaderTextLeftBox>{formatNumber(this.state.totalWeight, 0)}</HeaderTextLeftBox>
              </Col>
            </Row>
          )}
        </Col>
        {type === 'invoice' && (
          <Col span={8}>
            <Row gutter={24}>
              <Col span={11}>
                <HeaderTextLeftBox className="normal">TOTAL</HeaderTextLeftBox>
              </Col>
              <Col span={13}>
                <HeaderTextRightBox>${formatNumber(this.state.totalAmount, 2)}</HeaderTextRightBox>
              </Col>
            </Row>
          </Col>
        )}
      </Row>
    )

    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'
    company = company
      ? company
      : currentOrder.wholesaleClient && currentOrder.wholesaleClient.wholesaleCompany
      ? currentOrder.wholesaleClient.wholesaleCompany
      : null

    const soldToCol = soldto ? (
      <Col span={8}>
        <div className="f12">
          <h6
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'bold',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            Sold to:
          </h6>
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {clientName}
          </p>
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
              whiteSpace: 'pre-line',
            }}
          >
            {currentOrder.billingAddress ? formatAddress(currentOrder.billingAddress.address, true).trim() : 'N/A'}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
              ? currentOrder.wholesaleClient.mainBillingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    ) : (
      <></>
    )

    const shipToCol = shipto ? (
      <Col span={8}>
        <div className="f12">
          <h6
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'bold',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            Ship to:{' '}
          </h6>
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {clientName}
          </p>
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
              whiteSpace: 'pre-line',
            }}
          >
            {fulfillmentOptionType !== 2 ? (
              <>
                {currentOrder.shippingAddress
                  ? formatAddress(currentOrder.shippingAddress.address, true).trim()
                  : 'N/A'}
              </>
            ) : (
              <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
            )}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    ) : (
      <></>
    )

    const logisticCols = (
      <>
        {carrierSetting ? (
          <Col span={4}>
            <div className="f12">
              <DisplayLeftLabel>Carrier </DisplayLeftLabel>
              <DisplayValue>{currentOrder.carrier}</DisplayValue>
              <DisplayValue>{currentOrder.carrierReferenceNo}</DisplayValue>
            </div>
          </Col>
        ) : (
          <></>
        )}
        {origin ? (
          <Col span={6}>
            <div className="f12">
              <DisplayLeftLabel>Origin </DisplayLeftLabel>
              <DisplayValue>{currentOrder.originAddress}</DisplayValue>
            </div>
          </Col>
        ) : (
          <></>
        )}
        <Col span={6}>
          <div className="f12">
            <DisplayLeftLabel>Destination </DisplayLeftLabel>
            <DisplayValue>{clientName}</DisplayValue>
            <DisplayValue style={{ whiteSpace: 'pre-wrap' }}>
              {currentOrder.shippingAddress ? formatAddress(currentOrder.shippingAddress.address, true).trim() : 'N/A'}
            </DisplayValue>
            <DisplayValue>
              {currentOrder.wholesaleClient.mainShippingAddress &&
              currentOrder.wholesaleClient.mainShippingAddress.phone
                ? currentOrder.wholesaleClient.mainShippingAddress.phone
                : currentOrder.wholesaleClient.mobilePhone}
            </DisplayValue>
          </div>
        </Col>
      </>
    )

    let phone = ''
    if (company) {
      phone = (company.mainBillingAddress &&
      company.mainBillingAddress.phone &&
      company.mainBillingAddress.phone !== 'null'
        ? company.mainBillingAddress.phone
        : company.phone
        ? company.phone
        : ''
      ).replace('+1-', '')
    }

    let logo = this.props.logo
    if (logo && logo !== 'default') {
      if (!location.origin.includes('www')) {
        logo = logo.replace('www.', '')
      }
    }

    return (
      <PrintPickSheetWrapper>
        <HeaderBox style={{ fontWeight: 'normal' }}>
          {!pagePrintSetting ||
            (pagePrintSetting && pagePrintSetting.barcode && (
              <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} width={2} height={30} />
            ))}
          <Row style={{ textAlign: 'center', marginTop: '25px' }}>
            <h1 style={{ marginBottom: '0.25em' }}>
              {pagePrintSetting && pagePrintSetting.title
                ? pagePrintSetting.title.toUpperCase()
                : type == 'invoice'
                ? 'INVOICE'
                : 'BILL OF LADING'}
            </h1>
          </Row>
          <Row>
            <Col span={14}>
              <CompanyInfoWrapper>
                <div style={{ textAlign: 'left' }}>
                  {(!pagePrintSetting || (pagePrintSetting && pagePrintSetting.logo)) && (
                    <div style={{ marginRight: 10 }}>
                      {logo === 'default' ? (
                        <LogoWrapper style={{ textAlign: 'left' }}>
                          <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                        </LogoWrapper>
                      ) : (
                        <img src={logo} style={{ maxWidth: 150 }} onLoad={this.onImageLoad} />
                      )}
                    </div>
                  )}
                  {company && (
                    <div className="left">
                      {company.companyName !== null && (
                        <HeaderTextLeftBox className="f14">{company.companyName}</HeaderTextLeftBox>
                      )}
                      {(!pagePrintSetting ||
                        (pagePrintSetting &&
                          (pagePrintSetting.submitPaymentWord ||
                            typeof pagePrintSetting.submitPaymentWord === 'undefined'))) && (
                        <HeaderTextLeftBox className="f12">Please submit payment to:</HeaderTextLeftBox>
                      )}
                      {company.email !== null && <div>{company.email}</div>}
                      {company.mainBillingAddress !== null && (
                        <div className="f12">{formatAddress(company.mainBillingAddress.address, false)}</div>
                      )}
                      {(phone || company.fax) && (
                        <div className="f12">
                          Phone: {phone}{company.fax ? ` / Fax: ${company.fax}` : ''}
                        </div>
                      )}
                    </div>
                  )}
                </div>
              </CompanyInfoWrapper>
            </Col>
            <Col span={8} className="f12">
              {!pagePrintSetting ||
                (pagePrintSetting && pagePrintSetting.so && (
                  <Row style={{ display: 'flex' }}>
                    <DisplayLabel>SALES ORDER#: </DisplayLabel>
                    <DisplayValue style={pl24}>
                      {getOrderPrefix(sellerSetting, 'sales')}
                      {currentOrder.wholesaleOrderId}
                    </DisplayValue>
                  </Row>
                ))}
              {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.num)) && (
                <Row style={{ display: 'flex' }}>
                  <DisplayLabel>Invoice #: </DisplayLabel>
                  <DisplayValue style={pl24}>
                    {getOrderPrefix(sellerSetting, 'sales')}
                    {currentOrder.wholesaleOrderId}
                  </DisplayValue>
                </Row>
              )}
              {(!pagePrintSetting ||
                (pagePrintSetting && (pagePrintSetting.po || typeof pagePrintSetting.po === 'undefined'))) &&
                currentOrder.reference && (
                  <Row style={{ display: 'flex' }}>
                    <DisplayLabel>{isFreshGreen ? 'Receipt No.' : 'Customer PO #'}:</DisplayLabel>
                    <DisplayValue style={pl24}>{currentOrder.reference}</DisplayValue>
                  </Row>
                )}
              {/* {typeof title === 'undefined' && ( */}

              {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
                <>
                  <Row style={{ display: 'flex' }}>
                    <DisplayLabel>Pickup Time:</DisplayLabel>
                    <DisplayValue style={pl24}>{currentOrder.warehousePickupTime}</DisplayValue>
                  </Row>
                </>
              ) : (
                <>
                  {!pagePrintSetting ||
                    (pagePrintSetting && pagePrintSetting.window && (
                      <Row style={{ display: 'flex' }}>
                        <DisplayLabel>{fulfillmentOptionType === 2 ? 'Pickup Time' : 'Window'}:</DisplayLabel>
                        <DisplayValue style={pl24}>
                          {currentOrder.shippingAddress && currentOrder.shippingAddress.deliveryWindows
                            ? currentOrder.shippingAddress.deliveryWindows
                            : 'N/A'}
                        </DisplayValue>
                      </Row>
                    ))}
                </>
              )}
              {(!pagePrintSetting ||
                (pagePrintSetting &&
                  (pagePrintSetting.numberOfPallets || typeof pagePrintSetting.numberOfPallets === 'undefined'))) &&
                currentOrder.numberOfPallets && (
                  <Row style={{ display: 'flex' }}>
                    <DisplayLabel>No. of pallets:</DisplayLabel>
                    <DisplayValue style={pl24}>{currentOrder.numberOfPallets}</DisplayValue>
                  </Row>
                )}
              {/* <HeaderTextLeftBox>&nbsp;</HeaderTextLeftBox> */}
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Order Date:</DisplayLabel>
                <DisplayValue style={pl24}>
                  <Moment format="MM/DD/YYYY" date={currentOrder.createdDate} />
                </DisplayValue>
              </Row>
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Target Fulfillment Date:</DisplayLabel>
                <DisplayValue style={pl24}>
                  <Moment format="MM/DD/YYYY" date={currentOrder.deliveryDate} />
                </DisplayValue>
              </Row>
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Sales Representative:</DisplayLabel>
                <DisplayValue style={pl24}>
                  {currentOrder.seller ? `${currentOrder.seller.firstName} ${currentOrder.seller.lastName}` : 'N/A'}
                </DisplayValue>
              </Row>
              {fulfillmentOptionType === 3 && (
                <Row style={{ display: 'flex' }}>
                  <DisplayLabel>Shipping Terms:</DisplayLabel>
                  <DisplayValue style={pl24}>{currentOrder.shippingTerm ? currentOrder.shippingTerm : ''}</DisplayValue>
                </Row>
              )}
              {/* {typeof title === 'undefined' && ( */}
              {!pagePrintSetting ||
                (pagePrintSetting && pagePrintSetting.driver && fulfillmentOptionType != 3 && (
                  <Row style={{ display: 'flex' }}>
                    <DisplayLabel>Delivery Driver:</DisplayLabel>
                    <DisplayValue style={pl24}>
                      {currentOrder.overrideDriver
                        ? `${currentOrder.overrideDriver.firstName} ${currentOrder.overrideDriver.lastName}`
                        : currentOrder.defaultDriver
                        ? `${currentOrder.defaultDriver.firstName} ${currentOrder.defaultDriver.lastName}`
                        : 'N/A'}
                    </DisplayValue>
                  </Row>
                ))}
            </Col>
          </Row>
          <Row style={{ marginTop: 8 }} className="f12">
            {fulfillmentOptionType != 3 ? (
              <>
                {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
                  <>
                    <Col span={8}>
                      <div>
                        <HeaderTextLeftBox>PICKUP ADDRESS</HeaderTextLeftBox>
                        <div>{clientName}</div>
                        <div style={{ whiteSpace: 'pre-wrap' }}>{currentOrder.pickupAddress}</div>
                      </div>
                    </Col>
                    {type == 'bill' ||
                    (type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.soldto)))
                      ? soldToCol
                      : ''}
                  </>
                ) : (
                  <>
                    {type == 'bill' ||
                    (type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.soldto)))
                      ? soldToCol
                      : ''}
                    {type == 'bill' ||
                    (type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.shipto)))
                      ? shipToCol
                      : ''}
                  </>
                )}
              </>
            ) : (
              <>{logisticCols}</>
            )}
          </Row>
          {currentOrder.wholesaleClient && currentOrder.wholesaleClient.mainContact && (
            <Row
              style={{
                marginTop: 20,
                borderBottom: '1px solid #aaa',
                paddingBottom: 5,
                marginBottom: 5,
              }}
              className="f12"
            >
              <Col span={5}>
                <HeaderTextLeftBox>Delivery Contact</HeaderTextLeftBox>
              </Col>
              <Col span={6}>
                {currentOrder.deliveryContact
                  ? currentOrder.deliveryContact
                  : currentOrder.wholesaleClient.mainContact
                  ? currentOrder.wholesaleClient.mainContact.name
                  : ''}
              </Col>
              {fulfillmentOptionType == 1 && (
                <Col span={6}>
                  {currentOrder.deliveryPhone
                    ? currentOrder.deliveryPhone
                    : currentOrder.wholesaleClient.mainContact
                    ? currentOrder.wholesaleClient.mainContact.mobilePhone
                    : ''}
                </Col>
              )}
            </Row>
          )}
          {type == 'invoice' &&
            (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.driver_payment_collection)) && (
              <Row style={{ padding: '10px 0' }} className="f12">
                <Col span={7}>
                  <Flex>
                    <HeaderTextLeftBox className="normal">CASH RECEIVED $</HeaderTextLeftBox>
                    <Underline>{this.state.totalQuantity}</Underline>
                  </Flex>
                </Col>

                <Col span={7} offset={1}>
                  <Flex>
                    <HeaderTextLeftBox className="normal">CHECK RECEIVED $</HeaderTextLeftBox>
                    <Underline>{this.state.totalQuantity}</Underline>
                  </Flex>
                </Col>
                <Col span={7} offset={1}>
                  <Flex>
                    <HeaderTextLeftBox className="normal">CHECK #</HeaderTextLeftBox>
                    <Underline>{this.state.totalQuantity}</Underline>
                  </Flex>
                </Col>
              </Row>
            )}
          {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.account_balance_forward)) && (
            <>
              <LightColoredRow style={{ padding: '4px 8px' }} className="f12">
                <Col span={4}>DATE</Col>
                <Col span={16}>ACCOUNT SUMMARY</Col>
                <Col span={4} style={{ textAlign: 'right' }}>
                  AMOUNT
                </Col>
              </LightColoredRow>
              <Row style={{ padding: '4px 8px' }} className="f12">
                <Col span={4}>01/12/2015</Col>
                <Col span={16}>
                  <div>Balance Forward</div>
                  <div>New charges(details below)</div>
                  <div>Total Amount Due</div>
                </Col>
                <Col span={4} style={{ textAlign: 'right' }}>
                  <div>100</div>
                  <div>100</div>
                  <div>100</div>
                </Col>
              </Row>
            </>
          )}
          {currentOrder.customerNote && (
            <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
              <Col span={3}>
                <HeaderTextLeftBox>Notes to Customer</HeaderTextLeftBox>
              </Col>
              <Col span={6}>
                <div style={{ paddingLeft: 15 }}>{currentOrder.customerNote}</div>
              </Col>
            </Row>
          )}
          {currentOrder.deliveryInstruction && (
            <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
              <Col span={24}>
                <HeaderTextLeftBox>
                  {fulfillmentOptionType === 1 || fulfillmentOptionType === 2 ? 'Fulfillment ' : 'Delivery '}
                  Instructions
                </HeaderTextLeftBox>
                <pre style={{ marginBottom: 0 }}>{currentOrder.deliveryInstruction}</pre>
              </Col>
            </Row>
          )}
        </HeaderBox>
        <PrintPickSheetTable style={{ marginTop: 18 }}>
          {/* {totals} */}
          <DeliveryListTable>
            <Table
              className={tableClass}
              bordered={false}
              style={{ width: '100%' }}
              rowKey="wholesaleOrderItemId"
              rowClassName={(record, index) => {
                let rowOrderItemId = record.wholesaleOrderItemId
                if (this.checkWeights(record)) {
                  rowOrderItemId = record.relatedOrderItemId
                }
                const i = orderItems.findIndex((oi) => oi.wholesaleOrderItemId === rowOrderItemId)
                return i % 2 === 1 ? 'alternative' : ''
              }}
              columns={this.getDisplayingColumns(columns)}
              dataSource={items}
              pagination={false}
            />
          </DeliveryListTable>
          <hr />
          {totals}
        </PrintPickSheetTable>
        <InvoiceCreditMemoSection summary={this.state.totalAmount} tableClass={tableClass} />
        {/* <UnderLineWrapper>
          <div>Warehouse Verification (Signature):</div>
        </UnderLineWrapper> */}
        <Flex>
          <div>Warehouse Verification (Signature):</div>
          <Underline />
        </Flex>
        {/* <ModalDetailText>
          {clientName === 'TBD'
            ? 'The perishable agricultural commodities listed on this invoice are sold subject to the statutory trust authorized by section 5(c) of the Perishable Agricultural Commodities Act, 1930 (7 U.S.C. 499e(c)). The seller of these commodities retains a trust claim over these commodities, all inventories of food or other products derived from these commodities, and any receivables or proceeds from the sale of these commodities until full payment is received.'
            : 'All Claim for damaged or shortage of goods should be made within 24 hrs upon receipt of merchandise/goods. The perishable commodities listed on this invoice are sold subject to the statutory trust authorized by section 5c of the Perishable Agricultural Commodities Act, 1930 (7 U.S. C499E c). Friends Development (USA), Inc. retains a trust claim over these said commodities, all inventories or other products derived from these commodities and any receivables or proceeds from the sales of these commodities until full payment is received (Net 15 Days).'}
          <br />
          <b>溫馨提醒: 請客戶務必當面點清貨品數量後再簽收, 過後發現丟失, 本公司概不負責! 敬請見諒!</b>
        </ModalDetailText> */}
        {pagePrintSetting && pagePrintSetting.customMsg && (
          <ModalDetailText className="f18" style={{ fontWeight: 'bold', marginTop: 10, marginBottom: 0, paddingBottom: 10 }}>
            {pagePrintSetting.customMsg}
          </ModalDetailText>
        )}
        {pagePrintSetting && pagePrintSetting.terms && (
          <ModalDetailText className="f10">{pagePrintSetting.terms}</ModalDetailText>
        )}

        <Flex>
          <div>Customer Print Name:</div>
          <Underline />
          <div>Signature:</div>
          <Underline />
        </Flex>

        {(!pagePrintSetting ||
          (pagePrintSetting && (pagePrintSetting.barcode || pagePrintSetting.printTime || pagePrintSetting.so))) && (
          <div style={{ marginTop: 20 }}>
            <div>
              {!pagePrintSetting || (pagePrintSetting && pagePrintSetting.barcode) ? (
                <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} width={2} height={30} />
              ) : (
                <div>&nbsp;</div>
              )}
            </div>
            <Flex className="v-center between" style={{ fontSize: 16, paddingTop: 5 }}>
              <div>
                {!pagePrintSetting ||
                  (pagePrintSetting && pagePrintSetting.printTime && (
                    <HeaderTextRightBox style={{ letterSpacing: 0, color: 'black' }}>
                      Printed {moment().format('MM/DD/YY hh:mm A')}
                    </HeaderTextRightBox>
                  ))}
              </div>
              <div>{!pagePrintSetting || (pagePrintSetting && pagePrintSetting.so && this.getNameAndOrderNo())}</div>
            </Flex>
          </div>
        )}
      </PrintPickSheetWrapper>
    )
  }
}

const CreditMemoSection = ({ adjustments, summary, printSetting, tableClass }: any) => {
  if (!printSetting.invoice_credit_memo) return null
  const dataSource = adjustments
    .map((v) => ({
      quantity: v.quantity,
      uom: v.uom,
      orderId: v.order.wholesaleOrderId,
      deliveryDate: v.order.deliveryDate,
      returnInInventory: v.returnInInventory,
      variety: v.orderItem.wholesaleItem.variety,
      sku: v.orderItem.wholesaleItem.sku,
      wholesaleProductUomList: v.item.wholesaleProductUomList,
      inventoryUOM: v.item.inventoryUOM,
      price: v.salePrice,
      pricingUOM: v.orderItem.pricingUOM,
    }))
    .filter((ad) => ad.quantity)

  const getBillableQtyWithUOM = (record: any) => {
    let showBillableQty,
      uom,
      subUom = null,
      pricingSubUom = null

    if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
      let tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.uom,
      )
      subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
      tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.pricingUOM,
      )
      pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
    }

    if (record.quantity) {
      uom = record.pricingUOM
      if (record.pricingUOM === record.inventoryUOM) {
        if (record.uom === record.inventoryUOM) {
          showBillableQty = record.quantity ? record.quantity : 0
        } else if (subUom != null && record.uom === subUom.name) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity / subUom.ratio, 4) : 0
        } else {
        }
      } else {
        if (record.uom === record.inventoryUOM && pricingSubUom != null) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity * pricingSubUom.ratio, 4) : 0
        } else if (subUom != null && record.uom === subUom.name && pricingSubUom != null) {
          showBillableQty = record.quantity ? (record.quantity / subUom.ratio) * pricingSubUom.ratio : 0
        }
      }
    } else {
      showBillableQty = record.quantity
      uom = record.pricingUOM
    }
    if (!record.pricingUOM) {
      showBillableQty = record.quantity
    }
    return {
      showBillableQty,
      uom,
    }
  }

  const columns = [
    {
      title: '#',
      key: 'index',
      align: 'center',
      width: 80,
      render: (_key: any, _row: any, index: number) => index + 1,
    },
    {
      title: 'CREDITED',
      align: 'right',
      width: 100,
      render: (r) => `-${r.quantity} ${r.uom || ''}`,
    },
    {
      title: 'ORDER',
      align: 'left',
      width: 120,
      render: (r) => {
        return (
          <div>
            {r.orderId}
            <br />
            {r.deliveryDate}
          </div>
        )
      },
    },
    {
      title: 'RETURN',
      align: 'left',
      render: (r) => (r.returnInInventory ? 'YES' : 'NO'),
    },
    {
      title: 'PRODUCT',
      align: 'left',
      dataIndex: 'variety',
      render: (t) => <div style={{ maxWidth: 200 }}>{t}</div>,
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
    },
    {
      title: 'BILLABLE QTY',
      align: 'right',
      dataIndex: 'catchWeightQty',
      render: (weight: number, record: any) => {
        const { uom, showBillableQty } = getBillableQtyWithUOM(record)
        return (
          <>
            -{mathRoundFun(showBillableQty, 4)} {uom}
          </>
        )
      },
    },
    {
      title: 'PRICE',
      dataIndex: 'price',
      align: 'right',
      width: 120,
      render: (value: number, record: any) => {
        let price = value
        // const pricingUOM = record.returnUom
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        if (_.isNumber(price)) {
          price = basePriceToRatioPrice(pricingUOM, price, record)
        }

        return (
          <>
            {`${price ? '$' + formatNumber(price, 2) : '$0'}`}/{pricingUOM}
          </>
        )
      },
    },
    {
      title: 'SUBTOTAL',
      align: 'right',
      dataIndex: 'totalPrice',
      render: (data: number, record: any) => {
        const extended = Number(record.price ? record.price : 0)
        const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
        const total = extended * quantity

        return <div>{total ? `-$${formatNumber(total, 2)}` : ''}</div>
      },
    },
  ]

  let totalReturn = 0
  let totalQty = 0
  let totalUnit = 0

  if (dataSource.length > 0) {
    dataSource.forEach((record, index) => {
      const extended = Number(record.price ? record.price : 0)
      const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
      let showValue = false
      if (quantity) {
        showValue = true
      }
      totalReturn += showValue ? extended * quantity : 0

      const { showBillableQty } = getBillableQtyWithUOM(record)
      totalQty += showBillableQty

      totalUnit += typeof record.quantity !== 'undefined' ? parseFloat(record.quantity) : 0
    })
  }
  const Totals = () => {
    return (
      <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24} type="flex" justify="space-between">
        <Col span={8}>
          {!!totalUnit && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextLeftBox className="normal">TOTAL CREDITED UNITS</HeaderTextLeftBox>
              </Col>
              <Col span={8}>
                <HeaderTextLeftBox>-{totalUnit}</HeaderTextLeftBox>
              </Col>
            </Row>
          )}
        </Col>
        <Col span={8}>
          {!!totalReturn && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextRightBox className="normal">TOTAL CREDIT</HeaderTextRightBox>
              </Col>
              <Col span={8}>
                <HeaderTextRightBox>-${formatNumber(Math.abs(totalReturn), 2)}</HeaderTextRightBox>
              </Col>
            </Row>
          )}
          <Row gutter={24}>
            <Col span={16}>
              <HeaderTextRightBox className="normal">SUMMARY</HeaderTextRightBox>
            </Col>
            <Col span={8}>
              <HeaderTextRightBox>
                {summary - totalReturn < 0 && '-'}${formatNumber(Math.abs(summary - Math.abs(totalReturn)), 2)}
              </HeaderTextRightBox>
            </Col>
          </Row>
        </Col>
      </Row>
    )
  }

  return dataSource.length ? (
    <div style={{ marginBottom: '20px' }}>
      <PrintPickSheetTable style={{ marginTop: 10 }}>
        {!!totalReturn && (
          <DeliveryListTable>
            <Table
              style={{ width: '100%' }}
              rowKey="wholesaleOrderItemId"
              columns={columns}
              dataSource={dataSource}
              pagination={false}
              className={tableClass}
              bordered={false}
              rowKey="wholesaleOrderItemId"
              rowClassName={(record, index) => {
                return index % 2 === 1 ? 'alternative' : ''
              }}
            />
          </DeliveryListTable>
        )}
      </PrintPickSheetTable>
      <hr />
      <Totals />
    </div>
  ) : (
    <></>
  )
}

const InvoiceCreditMemoSection = connect(({ orders, setting }) => ({
  adjustments: orders.adjustments,
  printSetting: JSON.parse(orders.printSetting),
}))(CreditMemoSection)

export default PrintInvoice
