import React from 'react'
import { OrderDetail, OrderItem } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode'
import {
  HeaderTextLeftBox,
  EllipseText,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
  FlexWrap,
  Inputs,
} from '~/modules/customers/sales/cart/print/_styles'
import Moment from 'react-moment'
import _moment from 'moment'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import { formatAddress, formatItemDescription, getOrderPrefix, judgeConstantRatio, isCustomOrderNumberEnabled } from '~/common/utils'
import { Icon } from '~/components'
import moment from 'moment'
import { Icon as IconSvg } from '~/components'
import { ThemeInput } from '~/modules/customers/customers.style'
import { cloneDeep } from 'lodash'

interface PrintPickSheetProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  logo: string
  isWorkOrder?: boolean
  catchWeightValues: any
  multiple?: boolean
  printSetting?: string
  changePrintLogoStatus: Function
  sellerSetting?: any
  company: any
}

export class PrintPickSheet extends React.PureComponent<PrintPickSheetProps> {
  state = {
    visibleAlert: false,
    selectedRowKeys: [],
    pagePrintSetting: null,
  }

  componentDidMount() {
    if (this.props.printSetting) {
      this.setPrintSetting(this.props.printSetting)
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.printSetting != nextProps.printSetting && nextProps.printSetting != null) {
      this.setPrintSetting(nextProps.printSetting)
    }
  }

  setPrintSetting = (jsonSetting: string) => {
    const pageSetting = JSON.parse(jsonSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`pick_`) > -1) {
        const newKey = el.substring(`pick_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    this.setState({ pagePrintSetting: data })
  }

  getDisplayingColumns = (columns: any[]) => {
    const { pagePrintSetting } = this.state
    let newColumns: any[] = []
    console.log(pagePrintSetting)
    if (!pagePrintSetting) return columns
    columns.forEach((el) => {
      const key = el.key
      if (pagePrintSetting[key] !== false) {
        newColumns.push(el)
      }
    })
    return newColumns
  }

  onImageLoad = () => {
    this.props.changePrintLogoStatus()
  }

  checkWeights = (row: any) => {
    return typeof row.weights !== 'undefined' && row.weights.length > 0
  }

  render() {
    const {
      orderItems,
      currentOrder,
      isWorkOrder,
      catchWeightValues,
      multiple,
      sellerSetting,
      printSetting,
      company,
    } = this.props
    let settings = null
    let tableClass = ''
    if (printSetting) {
      settings = JSON.parse(printSetting)
      if (settings.pick_alternateRow === true) {
        tableClass = 'alternate-row'
      }
    }

    let filteredItems: any[] = []
    if (orderItems) {
      filteredItems = orderItems.filter((el) => {
        return el.quantity > 0
      })
    }

    let items = cloneDeep(filteredItems)
    filteredItems.forEach((record) => {
      let weights: any[] = []
      let showCatchWeightModal = false
      let constantRatio = judgeConstantRatio(record)
      if (!constantRatio) {
        showCatchWeightModal = true
      }
      if (showCatchWeightModal) {
        if (multiple && typeof record.catchWeightValues !== 'undefined') {
          weights = record.catchWeightValues
        } else {
          weights = catchWeightValues[record.wholesaleOrderItemId] ? catchWeightValues[record.wholesaleOrderItemId] : []
        }
        const index = items.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
        if (weights.length > 0) {
          items.splice(index + 1, 0, {
            weights,
          })
        }
      }
    })

    const columns: Array<any> = [
      // {
      //   title: 'TYPE',
      //   dataIndex: isWorkOrder ? 'wholesaleOrderItemStatus' : 'status',
      //   key: 'status',
      //   align: 'center',
      //   width: 100,
      // },
      {
        title: 'UNIT',
        dataIndex: 'quantity',
        key: 'quantity',
        align: 'center',
        width: 70,
        render: (quantity: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }
          return <span style={{ whiteSpace: 'nowrap' }}>{quantity}</span>
        }
      },
      {
        title: 'UOM',
        dataIndex: isWorkOrder ? 'uom' : 'UOM',
        key: 'UOM',
        align: 'center',
        width: 100,
      },
      {
        title: 'PRODUCT DESCRIPTION',
        dataIndex: isWorkOrder ? 'wholesaleItem.variety' : 'variety',
        key: 'variety',
        align: 'left',
        className: 'th-left',
        render: (variety: string, record: any) => {
          if (this.checkWeights(record) && company && company.isDisablePickingStep === false) {
            const { weights } = record
            let el = null
            el = (
              <Inputs>
                <FlexWrap>
                  {weights.map((w: any, i) => {
                    return (
                      <ThemeInput
                        key={`${record.variety}-${i}`}
                        value={w.unitWeight ? w.unitWeight : ''}
                        readOnly
                        style={{ width: 68, textAlign: 'center' }}
                      />
                    )
                  })}
                </FlexWrap>
              </Inputs>
            )
            return {
              children: el,
              props: {
                colSpan: 6,
              },
            }
          }
          return record.editedItemName
            ? record.editedItemName
            : formatItemDescription(variety, record.SKU, sellerSetting)
        },
      },
      {
        title: 'BRAND',
        dataIndex: 'modifiers',
        key: 'modifiers',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'extraOrigin',
        key: 'extraOrigin',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'LOT',
        dataIndex: 'lotId',
        key: 'lotId',
        align: 'center',
        width: 120,
        render: (data: string, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return (
            <div style={{ height: 30 }}>
              {data != null && data.length > 0 && (
                <>
                  {settings && settings.pick_lotType === 'barcode' ? (
                    <Barcode value={data} width={1} height={30} margin={0} displayValue={false} />
                  ) : (
                    data
                  )}
                </>
              )}
            </div>
          )
        },
      },
      {
        title: 'LOCATION',
        dataIndex: isWorkOrder ? 'location' : 'locationNames',
        align: 'center',
        key: 'locationNames',
        render: (locationNames: string[], record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let ret = ''
          if (locationNames && locationNames.length > 0) {
            ret = locationNames.join(', ')
            if (locationNames.length > 5) {
              ret += ', ...'
            }
          }
          return ret
        },
      },
      {
        title: 'UNITS PICKED',
        dataIndex: 'status',
        key: 'status',
        align: 'center',
        width: 60,
        render: (status: string, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return status !== 'PLACED' && status !== 'NEW' ? (record.picked ? record.picked : 0) : ''
        },
      },
      {
        title: '',
        dataIndex: 'isSpecialCut',
        width: 10,
        render: (isSpecialCut: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return isSpecialCut > 0 ? <IconSvg viewBox="0 0 24 24" width="15" height="15" type="star" /> : null
        },
      },
      {
        title: 'NOTE',
        dataIndex: 'note',
        key: 'note',
        align: 'center',
        width: 180,
        render: (note: string, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return <EllipseText>{note}</EllipseText>
        },
      },
    ]

    if (isWorkOrder === true) {
      columns.splice(5, 1)
    }
    const isLogoEnabled = !settings || (settings && (settings.pick_logo || typeof settings.pick_logo === 'undefined'))
    let accountMgr = ''
    if (isWorkOrder === true && currentOrder) {
      accountMgr = currentOrder.user.firstName + ' ' + currentOrder.user.lastName
    } else if (currentOrder.wholesaleClient && currentOrder.wholesaleClient.accountant) {
      accountMgr = currentOrder.wholesaleClient.accountant.firstName + ' ' + currentOrder.wholesaleClient.accountant.lastName
    }

    return (
      <PrintPickSheetWrapper>
        <HeaderBox>
          <Row gutter={24} style={{ marginBottom: 20 }}>
            <Col span={6} style={{ textAlign: 'left', paddingLeft: 18 }}>
              <CompanyInfoWrapper>
                {isLogoEnabled && (
                  <>
                    {this.props.logo === 'default' ? (
                      <LogoWrapper>
                        <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                      </LogoWrapper>
                    ) : (
                      <img src={this.props.logo} onLoad={this.onImageLoad.bind(this)} />
                    )}
                  </>
                )}
                <span style={{ fontSize: 14, textAlign: 'left' }}>{this.props.companyName}</span>
              </CompanyInfoWrapper>
            </Col>
            <Col offset={2} span={8} style={{ textAlign: 'center' }}>
              <h1>{settings && settings.pick_title ? settings.pick_title.toUpperCase() : 'PICK SHEET'}</h1>
            </Col>
            <Col span={7} style={{ textAlign: 'right' }}>
              <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} height={90} />
            </Col>
            <Col span={1} />
          </Row>

          <Row gutter={24}>
            <Col span={6} style={{ paddingLeft: 18 }}>
              <HeaderTextLeftBox className="picksheet">SHIP TO:</HeaderTextLeftBox>
            </Col>
            <Col span={5}>
              <HeaderTextLeftBox className="picksheet">
                {currentOrder.wholesaleClient ? currentOrder.wholesaleClient.clientCompany.companyName : (<span>&nbsp;</span>)}
              </HeaderTextLeftBox>
            </Col>
            <Col span={3} />
            <Col span={5} style={{ padding: 0 }}>
              <HeaderTextLeftBox className="picksheet">{isWorkOrder ? 'WORK ORDER:' : 'ORDER NO.'} </HeaderTextLeftBox>
            </Col>
            <Col span={4}>
              <HeaderTextLeftBox className="picksheet">
                {getOrderPrefix(sellerSetting, 'sales')}
                {isCustomOrderNumberEnabled(sellerSetting) && currentOrder.customOrderNo
                  ? currentOrder.customOrderNo
                  : currentOrder.wholesaleOrderId}
              </HeaderTextLeftBox>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={6} style={{ paddingLeft: 18 }}>
              <HeaderTextLeftBox className="picksheet">SHIPPING ADDRESS:</HeaderTextLeftBox>
            </Col>
            {currentOrder.shippingAddress != null ? (
              <Col span={5}>
                <HeaderTextLeftBox className="picksheet">
                  {currentOrder.shippingAddress && currentOrder.shippingAddress.address
                    ? formatAddress(currentOrder.shippingAddress.address, true)
                    : (<span>&nbsp;</span>)}
                  <br />
                  {currentOrder.wholesaleClient.mainShippingAddress &&
                    currentOrder.wholesaleClient.mainShippingAddress.phone
                    ? currentOrder.wholesaleClient.mainShippingAddress.phone
                    : currentOrder.wholesaleClient.mobilePhone}
                </HeaderTextLeftBox>
              </Col>
            ) : (
              <Col span={5}><HeaderTextLeftBox className="picksheet">N/A</HeaderTextLeftBox></Col>
            )}
            <Col span={3} />
            <Col span={5} style={{ padding: 0 }}>
              <HeaderTextLeftBox className="picksheet">TARGET FULFILLMENT: </HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">DRIVER: </HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">PRINT VERSION: </HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">PRINT TIME: </HeaderTextLeftBox>
              {accountMgr !== '' && (
                <HeaderTextLeftBox className="picksheet">ACCOUNT MGR.: </HeaderTextLeftBox>
              )}
              <HeaderTextLeftBox className="picksheet">SALES REP: </HeaderTextLeftBox>
            </Col>
            <Col span={4} style={{ paddingRight: 0 }}>
              <HeaderTextLeftBox className="picksheet">
                <Moment format="MM/DD/YY" date={moment.utc(currentOrder.deliveryDate)} />
              </HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">
                {currentOrder.defaultDriver
                  ? currentOrder.defaultDriver.firstName + ' ' + currentOrder.defaultDriver.lastName
                  : (<span>&nbsp;</span>)}
              </HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">{currentOrder.printVersion ? currentOrder.printVersion : (<span>&nbsp;</span>)}</HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">
                <Moment format="MM/DD/YY h:mm A" date={_moment().format()} />
              </HeaderTextLeftBox>
              {accountMgr !== '' && (
                <HeaderTextLeftBox className="picksheet">
                  {accountMgr}
                </HeaderTextLeftBox>
              )}
              <HeaderTextLeftBox className="picksheet">
                {currentOrder.seller ? `${currentOrder.seller.firstName} ${currentOrder.seller.lastName}` : ''}
              </HeaderTextLeftBox>
            </Col>
          </Row>
          <Row gutter={24}>
            {currentOrder.pickerNote ? (
              <>
                <Col span={6} style={{ paddingLeft: 18 }}>
                  <HeaderTextLeftBox className="picksheet">NOTES:</HeaderTextLeftBox>
                </Col>
                <Col span={5}>
                  <HeaderTextLeftBox className="picksheet">{currentOrder.pickerNote}</HeaderTextLeftBox>
                </Col>
              </>
            ) : (
              <Col span={11}>&nbsp;</Col>
            )}
          </Row>
        </HeaderBox>
        <PrintPickSheetTable className="pick-sheet">
          <Table
            className={tableClass}
            rowKey="wholesaleOrderItemId"
            columns={this.getDisplayingColumns(columns)}
            dataSource={items}
            pagination={false}
          />
        </PrintPickSheetTable>
      </PrintPickSheetWrapper>
    )
  }
}