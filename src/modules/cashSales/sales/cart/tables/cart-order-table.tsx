import React from 'react'
import { Button, Icon, Popconfirm, Popover, Tooltip, Modal } from 'antd'
import jQuery from 'jquery'
import { PureWrapper as Wrapper } from '~/modules/customers/accounts/addresses/addresses.style'
// import { Order } from '../../../customers.model'
import {
  ml0,
  ThemeTextButton,
  ThemeIconButton,
  ThemeButton,
  ThemeColorDivRound,
  textCenter,
  ThemeCheckbox,
  ThemeIcon,
  ThemeOutlineButton,
  FlexCenter,
  ThemeModal,
  floatRight,
  FlexRow,
} from '~/modules/customers/customers.style'
import { formatNumber } from '~/common/utils'
import { cloneDeep } from 'lodash'
import EditableTable from '~/components/Table/editable-table'
import { noteGreyIcon, noteIcon } from '../../_style'
import NotesModal from '../modals/notes-modal'
import PriceModal from '../modals/price-modal'
import { OrderDetail, OrderItem } from '~/schema'
import LotsTable from './lots-table'
import QuantityModal from '../modals/quantity-modal'
import TypeEditWrapper from '~/modules/settings/tabs/Product/TypeEditWrapper'
import _ from 'lodash'
import WorkOrderModal from '~/modules/manufacturing/processing-list/modals/wo-modal'
import { History } from 'history'
import { Icon as IconSvg } from '~/components'
import {gray01} from "~/common";

interface OrderTableProps {
  editable: boolean
  currentOrder: OrderDetail
  orderItems: OrderItem[]
  handleSave: Function
  handleRemove: Function
  handleUpdateAllItemStatus: Function
  handleSwapItem: Function
  handleRefreshOrderItems: Function
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
  deleteProductType: Function
  updateProductType: Function
  addQuantityCatchWeight: Function
  companyProductTypes: any[]
  history: History
  catchWeightValues: any
  getWeightsByOrderItemIds: Function
  addCatchWeightByNumber: Function
  deleteCatchWeightByNumber: Function
  addItemButtonClicked: boolean
  resetLoading: Function
}

export class CartOrderTable extends React.PureComponent<OrderTableProps> {
  state: any
  constructor(props: OrderTableProps) {
    super(props)
    this.state = {
      dataSource: cloneDeep(props.orderItems),
      visibleNew: false,
      visiblePriceModal: false,
      visibleLotsModal: false,
      visibleWeightModal: false,
      orderItemId: -1,
      selectedItem: null,
      itemId: -1,
      initNotes: _.map(props.orderItems, 'note'),
      modifyNotes: _.map(props.orderItems, 'note'),
      pageSize: 30,
      currentPage: 1,
    }
  }

  componentWillReceiveProps(nextProps: OrderTableProps) {
    if (this.props.orderItems !== nextProps.orderItems) {
      let ids = nextProps.orderItems.map((el) => el.wholesaleOrderItemId)
      nextProps.getWeightsByOrderItemIds(ids)

      this.setState({
        dataSource: cloneDeep(nextProps.orderItems),
        initNotes: _.map(nextProps.orderItems, 'note'),
        modifyNotes: _.map(nextProps.orderItems, 'note'),
      })

      //After add item successfully. Need to set the number of pages to the last
      if (nextProps.orderItems.length - this.props.orderItems.length == 1) {
        this.setState({
          currentPage: Math.ceil(nextProps.orderItems.length / this.state.pageSize),
        })
      }
    }
  }

  componentDidUpdate(prevProps: OrderTableProps) {
    //select first tab-able element when new item is added
    // if (this.props.addItemButtonClicked && prevProps.orderItems.length >= 0 && prevProps.orderItems.length == this.props.orderItems.length - 1) {
    if (prevProps.orderItems.length >= 0 && prevProps.orderItems.length == this.props.orderItems.length - 1) {
      const rows = jQuery('.sales-order-table .ant-table-row')
      const allEls = jQuery('.sales-order-table .ant-table-row .tab-able')
      setTimeout(() => {
        if (rows.length > 0) {
          const editableEls = jQuery(rows[rows.length - 1]).find('.tab-able')
          if (editableEls.length > 0) {
            jQuery(editableEls[0]).trigger('click')
            const index = allEls.index(editableEls[0])
            window.localStorage.setItem('CLICKED-INDEX', `${index}`)
          }
        }
      }, 50)
    }
  }

  handleSave = (row: OrderItem, field?: string) => {
    const { catchWeightValues } = this.props
    const isCatchweightItem: boolean = !row.constantRatio && row.status != 'SHIPPED'
    if ((field === 'picked' || field === 'toggle_status') && typeof catchWeightValues[row.wholesaleOrderItemId] !== 'undefined' && isCatchweightItem) {
      const rows = catchWeightValues[row.wholesaleOrderItemId].length
      let picked = parseInt(row.picked.toString(), 10)
      // const oldOrderItem: OrderItem = this.props.orderItems.find((oi) => {
      //   return oi.wholesaleOrderItemId === row.wholesaleOrderItemId
      // })
      // if (oldOrderItem.picked === row.picked) {
      //   return
      // }
      // console.log(picked, rows)
      if (picked < rows) {
        // warning
        Modal.confirm({
          title: 'Warning',
          content: 'You will be losing all other rows, are you sure?',
          centered: true,
          onOk: () => {
            this._save(row, field)
            // remove the rows from quantity modal
            const diff = rows - picked
            console.log({
              orderItemId: row.wholesaleOrderItemId,
              count: diff,
            })
            // need to call a API to remove the rows as many as diff.
            setTimeout(() => {
              this.props.resetLoading();
              this.props.deleteCatchWeightByNumber({
                orderItemId: row.wholesaleOrderItemId,
                count: diff,
              })
            }, 150)
            localStorage.removeItem('PREV_PICKED')
            localStorage.removeItem('PREV_STATUS')
          },
          onCancel: () => {
            const prevPicked = localStorage.getItem('PREV_PICKED')
            const prevStatus = localStorage.getItem('PREV_STATUS')
            if (prevPicked) {
              localStorage.removeItem('PREV_PICKED')
              localStorage.removeItem('PREV_STATUS')
              const dataSource = [...this.state.dataSource]
              const index = dataSource.findIndex((item) => item.wholesaleOrderItemId === row.wholesaleOrderItemId)
              dataSource[index].picked = prevPicked
              dataSource[index].status = prevStatus
              this.setState({ dataSource: dataSource })
            }
          }
        })
      } else if (picked > rows) {
        this._save(row, field)
        // add rows from quantity modal
        const diff = picked - rows
        // need to call a API to add the rows as many as diff.
        console.log({
          orderItemId: row.wholesaleOrderItemId,
          count: diff,
        })
        setTimeout(() => {
          this.props.resetLoading();
          this.props.addCatchWeightByNumber({
            orderItemId: row.wholesaleOrderItemId,
            count: diff,
          })
        }, 150)
      } else {
        this._save(row, field)
      }
    } else {
      this._save(row, field)
    }
  }

  _save(row: OrderItem, field?: string) {
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })

    this.setState({ dataSource: newData })
    row.quantity = row.quantity ? row.quantity : 0
    row.picked = row.picked ? row.picked : 0
    this.props.handleSave(row)
  }

  onDeleteRow = (id: number) => {
    const dataSource = [...this.state.dataSource]
    this.setState({ dataSource: dataSource.filter((item) => item.wholesaleOrderItemId !== id) })
    this.props.handleRemove(id)
  }

  onToggleValue = (record: OrderItem, type: string) => {
    const dataSource = [...this.state.dataSource]
    const index = dataSource.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
    if (type === 'PICKING' && (record.status === 'PICKING' || record.status === 'PLACED' || record.status === 'NEW')) {
      if (record.status === 'PICKING') {
        localStorage.setItem('PREV_PICKED', record.picked)
        localStorage.setItem('PREV_STATUS', record.status)
        // record.picked = 0
        record.status = 'PLACED'
      } else {
        if (record.constantRatio && record.picked == 0) {
          record.picked = record.quantity
        }
        record.status = 'PICKING'
      }
    } else {
      record.status = record.status === 'SHIPPED' ? 'PICKING' : 'SHIPPED'
    }

    dataSource[index].picked = record.picked
    dataSource[index].status = record.status
    console.log(index, dataSource[index].status)
    this.setState({ dataSource: dataSource }, () => {
      this.handleSave(record, 'toggle_status')

      setTimeout(() => {
        let wrapper = jQuery('.tab-able').parents('tbody')
        let els = wrapper.find('.tab-able')
        console.log(els)
        let savedTabIndex = localStorage.getItem('TABLE-TAB-INDEX')
        if (savedTabIndex) {
          let index = parseInt(savedTabIndex, 10)
          if (els.length > index) jQuery(els[index]).focus()
        }
      }, 2000)
    })
  }

  checkedBox = (type: string) => {
    const dataSource = [...this.state.dataSource]
    let checked = true
    if (dataSource.length > 0) {
      dataSource.forEach((record, index) => {
        if (type === 'PICKING' && (record.status == 'NEW' || record.status == 'PLACED')) checked = false
        else if (type === 'SHIPPED' && record.status !== 'SHIPPED') checked = false
      })
    }
    // console.log(type + " checked = " + checked)
    return checked
  }

  onToggleAll = (type: string, e: any) => {
    const dataSource = [...this.state.dataSource]
    const val = e.target.checked
    if (dataSource.length > 0) {
      dataSource.forEach((record, index) => {
        if (type === 'PICKING' && (record.status === 'PICKING' || record.status === 'PLACED' || record.status === 'NEW')) {
          if (!val) {
            record.picked = record.picked
            record.status = 'PLACED'
          } else {
            if (record.picked > 0) {
              record.picked = record.picked
            } else {
              record.picked = record.quantity
            }

            record.status = 'PICKING'
          }
        } else {
          if (e.target.checked) {
            record.status = 'SHIPPED'
          } else {
            //have credit memo, cann't unshipped
            if (record.returnedQty != 0) {
              record.status = 'SHIPPED'
            } else {
              record.status = 'PICKING'
            }
          }
        }
      })
      this.setState({ dataSource: dataSource })
      this.props.handleUpdateAllItemStatus(dataSource)
    }
  }

  onErrorVisbleChange = (visible: boolean) => {
    // console.log(visible);
  }

  onNotesVisbleChange = (record: any, index: number, visible: boolean) => {
    let { initNotes, modifyNotes } = this.state
    const { currentOrder } = this.props
    if (currentOrder.isLocked) {
      return
    }
    if (visible === false) {
      if (initNotes[index] != modifyNotes[index]) {
        initNotes[index] = modifyNotes[index]
        record.note = modifyNotes[index]
        this.props.handleSave(record)
        this.setState({ initNotes })
      }
    }
  }
  setNewNote(value: string, index: number) {
    let { modifyNotes } = this.state
    modifyNotes[index] = value
    this.setState({ modifyNotes })
  }

  handleSelectProduct = (record: OrderItem, item: any) => {
    // tslint:disable-next-line:no-console
    if (record.itemId !== item.itemId) {
      if (record.status === 'PLACED' || record.status === 'NEW') {
        this.props.handleSwapItem(record.wholesaleOrderItemId, item)
      } else {
        record.itemId = item.itemId
        this.props.handleSave(record)
      }
    }
  }

  openPriceModal = (itemId: number) => {
    this.setState({
      itemId,
      visiblePriceModal: true,
    })
  }

  onToggleWeightModal = (record: OrderItem | null) => {
    this.setState({
      visibleWeightModal: !this.state.visibleWeightModal,
      selectedItem: record ? record : null,
    })
    // this.props.handleRefreshOrderItems()
    setTimeout(() => {
      jQuery('#new-weight-input').focus()
    }, 1000)
  }

  handleModalOk = () => {
    this.setState({
      visiblePriceModal: false,
    })
  }

  handleSelectLot = _.debounce((record: any) => {
    let row = { ...this.state.selectedItem }
    if (record) {
      row['lotId'] = record.lotId
      const markup = (record.margin ? record.margin : 0) / 100
      const freight = record.freight ? record.freight : 0
      const cost = record.cost ? record.cost : 0
      if (record.uom == record.wholesaleItem.inventoryUOM && record.wholesaleItem.constantRatio == true) {
        row['price'] = formatNumber(cost ? (1 + markup) * (cost / record.wholesaleItem.ratioUOM) + freight : 0, 2)
      } else {
        row['price'] = formatNumber(cost ? (1 + markup) * cost + freight : 0, 2)
      }
    } else {
      row['lotId'] = null
    }
    this.handleSave(row)
    this.onToggleLotsModal(null)
  }, 200)

  getCatchWeight = () => {
    this.props.getWeightsByOrderItemIds(this.props.orderItems.filter((el) => el.wholesaleOrderItemId))
  }

  onToggleLotsModal = (record: OrderItem | null) => {
    this.setState({
      visibleLotsModal: !this.state.visibleLotsModal,
      selectedItem: record ? record : null,
    })
  }

  toggleNewModal = (orderItemId: number) => {
    this.setState({
      visibleNew: !this.state.visibleNew,
      orderItemId,
    })
  }

  handleNew = () => {
    this.setState({
      visibleNew: false,
    })
  }

  onWorkOrderCreated = () => {
    this.props.handleRefreshOrderItems()
  }

  _renderStatusBar = (status: string) => {
    let color = 'red'
    switch (status) {
      case 'PLANNING':
        color = 'red'
        break
      case 'SCHEDULED':
        color = 'blue'
        break
      case 'COMPLETED':
        color = 'green'
        break
    }
    return <div style={{ width: 16, height: 16, backgroundColor: color, marginTop: 7 }} />
  }
  changePage = (page: any, pageSize: any) => {
    this.setState({
      currentPage: page,
    })
  }

  render() {
    const { editable, currentOrder, catchWeightValues, history } = this.props
    const {
      dataSource,
      visiblePriceModal,
      visibleLotsModal,
      visibleWeightModal,
      selectedItem,
      itemId,
      visibleNew,
      orderItemId,
      currentPage,
      pageSize,
    } = this.state
    const pickedChecked = this.checkedBox('PICKING')
    const shippedChecked = this.checkedBox('SHIPPED')
    const orderStatus = currentOrder.wholesaleOrderStatus

    let isPlacedOrder = orderStatus === 'PLACED'
    let isAllPicking = orderStatus === 'PICKING'
    let hasShipping = false
    let hasReturn = false
    dataSource.forEach((row: any) => {
      if (row.status !== 'SHIPPED') {
        // It means PICKED and SHIPPED all are checked?
        isPlacedOrder = false
      }
      if (row.status !== 'PICKING') {
        // When all item status is picking.then you can click table header SHIPPED
        isAllPicking = false
      }
      if (row.status == 'SHIPPED') {
        hasShipping = true
      }
      if (row.returnedQty > 0) {
        hasReturn = true
      }
    })

    const isPicking = orderStatus === 'PICKING'
    const isShipping = orderStatus === 'SHIPPED'
    const isCancel = orderStatus === 'CANCEL'
    const isLocked = currentOrder.isLocked

    const columns: Array<any> = [
      {
        title: 'Sales Order',
        children: [
          {
            title: '#',
            dataIndex: 'wholesaleOrderItemId',
            key: 'wholesaleOrderItemId',
            width: 60,
            align: 'center',
            render: (id: number, record: OrderItem, index: number) => {
              return record.picked !== null && record.quantity !== record.picked ? (
                <Tooltip
                  title="Sales Units do not match Picked Units"
                  placement="right"
                  overlayClassName="error-tooltip"
                >
                  <FlexRow>
                    <IconSvg
                      style={{ color: '#660000' }}
                      type="exclamation"
                      viewBox="0 0 12 18"
                      width="12"
                      height="18"
                    />
                    {index + 1}
                  </FlexRow>
                </Tooltip>
              ) : (
                index + 1
              )
            },
          },
          {
            title: 'UNIT',
            dataIndex: 'quantity',
            key: 'unit',
            editable: true,
            edit_width: 50,
            type: 'number',
          },
          {
            title: 'UOM',
            dataIndex: 'UOM',
            key: 'UOM',
            editable: currentOrder.parentOrder == null ? true : false,
            edit_width: 80,
            width: 105,
          },
          {
            title: 'PRODUCT NAME',
            dataIndex: 'variety',
            key: 'variety',
            // dataIndex: 'wholesaleOrderItemId',
            // key: 'wholesaleOrderItemId',
            // editable: true,
            // type: 'modal',
            // render: (variety: string, record: OrderItem) =>
            //   <FlexCenter>
            //     <ThemeTextButton type="text" className="tab-able">{variety}</ThemeTextButton>
            //   </FlexCenter>
            align: 'center',
          },
          {
            title: 'LOT',
            dataIndex: 'lotId',
            key: 'lot',
            // editable: true,
            // type: 'modal',
            render: (lotId: string, record: OrderItem) => (
              <FlexCenter>
                {(record.status == 'PLACED' || record.status == 'NEW') && !isLocked && !record.workOrderStatus ? (
                  <ThemeIconButton
                    className="tab-able"
                    type="link"
                    onClick={this.onToggleLotsModal.bind(this, record)}
                    style={{ width: '100%', textAlign: 'left' }}
                  >
                    <span style={{ color: gray01 }}>{lotId}</span>
                    <Icon type="edit" />
                  </ThemeIconButton>
                ) : (
                  <span style={{ color: gray01 }}>{lotId}</span>
                )}
              </FlexCenter>
            ),
          },
          {
            //     if (lot)
            //     wholesale_order_item.lot.cost * (1+lot.margin) + lot.freight
            //    else
            //   item.default_cost * (1+item.default_margin) + item.freight
            title: 'PRICE',
            dataIndex: 'price',
            key: 'price',
            editable: true,
            edit_width: 80,

            render: (data: number, record: OrderItem) => (
              <div style={{ width: '100%' }}>
                {((editable && record.status == 'PLACED') || record.status == 'NEW') && !isLocked ? (
                  <ThemeIconButton type="link">
                    <Icon type="edit" />
                  </ThemeIconButton>
                ) : (
                  ''
                )}
                $
                {formatNumber(
                  data
                    ? data
                    : 0,
                  2,
                )}
                {/* {!editable && (

            render: (data: number, record: OrderItem) => (
              <>

                {editable && (
                  <ThemeIconButton type="link">
                    <Icon type="edit" />
                  </ThemeIconButton>
                )}
                ${formatNumber(data ? data : record.cost * (record.margin > 0 ? 1 + record.margin / 100 : 1) + (record.freight ? record.freight : 0), 2)}
                {!editable && (
                  <ThemeIconButton type="link">
                    <Icon type="info-circle" />
                  </ThemeIconButton>
                )} */}
                {editable && record.status != 'SHIPPED' && (
                  <ThemeIconButton
                    className={`bar-chart-btn ${record.status == 'PLACED' || record.status == 'NEW' ? '' : 'tab-able'}`}
                    type="link"
                    style={{ marginLeft: 20 }}
                    onClick={() => this.openPriceModal(record.itemId)}
                  >
                    <Icon type="bar-chart" style={{ fontSize: 16 }} />
                  </ThemeIconButton>
                )}
                <div style={floatRight}>
                  {(record.workOrderStatus == null || record.workOrderStatus == '') &&
                    (record.status == 'PLACED' || record.status == 'NEW') && !isLocked ? (
                    <ThemeIconButton
                      className="bar-chart-btn"
                      type="link"
                      style={{ width: '100%', textAlign: 'left' }}
                      onClick={() => this.toggleNewModal(record.wholesaleOrderItemId)}
                    >
                      <Icon type="tool" style={{ fontSize: 20 }} />
                    </ThemeIconButton>
                  ) : (
                    ''
                  )}
                  {record.workOrderStatus ? (
                    <Tooltip title={record.workOrderStatus}>{this._renderStatusBar(record.workOrderStatus)}</Tooltip>
                  ) : (
                    ''
                  )}
                </div>
              </div>
            ),
          },
          {
            title: 'PRICING UOM',
            dataIndex: 'baseUOM',
            key: 'baseUOM',
            width: 100,
            render: (baseUOM: number, record: OrderItem) => {
              // return record.baseUOM != record.inventoryUOM && record.constantRatio && record.ratioUOM != 1 ?
              //   `${record.ratioUOM} ${record.baseUOM}`
              //   : record.baseUOM
              return record.baseUOM
            },
          },
        ],
      },
      {
        title: 'Picking',
        children: [
          {
            title: 'UNITS PICKED',
            dataIndex: 'picked',
            editable: true,
            edit_width: 50,
            key: 'picked',
            type: 'number',
          },
          {
            title: 'QUANTITY',
            dataIndex: 'catchWeightQty',
            key: 'orderWeight',
            // type: 'modal',
            // editable: true,
            render: (weight: number, record: OrderItem) => {
              return (
                <FlexCenter>
                  {!isPicking && isPlacedOrder ? (
                    weight === 0 ? (
                      ''
                    ) : (
                      formatNumber(weight, 2)
                    )
                  ) : !record.constantRatio && (record.status == 'PLACED' || record.status == 'NEW') && !isLocked ? (
                    <>
                      {
                        (record.status !== 'PLACED' && record.status !== 'NEW') ? (
                          weight === 0 ? '' : formatNumber(weight, 2)
                        ) : (
                          <Button
                            className="tab-able"
                            type="link"
                            onClick={this.onToggleWeightModal.bind(this, record)}
                            style={{
                              width: '100%',
                              textAlign: 'center',
                              //color: 'rgba(0, 0, 0, 0.65)',
                              color: gray01,
                              border: '1px dashed lightGrey',
                            }}
                          >
                            {weight === 0 ? '' : formatNumber(weight, 2)}
                          </Button>
                        )
                      }
                    </>
                  ) : weight === 0 ? (
                    ''
                  ) : (
                    formatNumber(weight, 2)
                  )}
                </FlexCenter>
              )
            },
          },
          {
            title: (
              <>
                PICKED
                <ThemeCheckbox
                  disabled={isShipping || isCancel || hasShipping || isLocked}
                  style={{ margin: '-2px 0 0 4px' }}
                  onChange={this.onToggleAll.bind(this, 'PICKING')}
                  checked={pickedChecked}
                />
              </>
            ),
            dataIndex: 'status',
            key: 'is_picked',
            width: 100,
            render: (status: string, record: any, _index: number) => {
              return (
                <FlexCenter>
                  {status !== 'PLACED' && status !== 'NEW' ? (
                    <ThemeButton
                      className={`${isShipping || status == 'SHIPPED' || isCancel ? '' : 'tab-able'}`}
                      disabled={isShipping || status == 'SHIPPED' || isCancel || isLocked}
                      shape="circle"
                      onClick={this.onToggleValue.bind(this, record, 'PICKING')}
                    >
                      <Icon type="bulb" theme="filled" />
                    </ThemeButton>
                  ) : (
                    <ThemeOutlineButton
                      className={`${isShipping || isCancel ? '' : 'tab-able'}`}
                      disabled={isShipping || isCancel || isLocked}
                      shape="circle"
                      onClick={this.onToggleValue.bind(this, record, 'PICKING')}
                    >
                      <ThemeIcon type="bulb" theme="filled" />
                    </ThemeOutlineButton>
                  )}
                </FlexCenter>
              )
            },
          },
        ],
      },
      {
        title: 'Shipping',
        children: [
          {
            title: 'EXTENDED',
            dataIndex: 'extended',
            key: 'extended',
            editable: !isPicking && !isPlacedOrder && !isCancel, //isShipping
            width: 125,
            edit_width: 100,
            render: (data: number, record: any) => {
              const value = Number(record.price ? record.price : 0)
              const prefix = value >= 0 ? '' : '-'
              return (
                <div style={textCenter}>
                  {prefix}${formatNumber(Math.abs(value), 2)}
                </div>
              )
            },
          },
          {
            title: 'ITEM TOTAL',
            dataIndex: 'quantity',
            key: 'item_total',
            render: (data: number, record: any) => {
              const extended = Number(record.price ? record.price : 0) + Number(record.freight ? record.freight : 0)
              let total = 0
              // if (record.catchWeightQty === null || record.catchWeightQty <= 0) {
              //   total = extended * record.picked
              // } else {
              total = extended * record.catchWeightQty
              // }
              const prefix = total >= 0 ? '' : '-'
              return (
                <div style={textCenter}>
                  {prefix}${formatNumber(Math.abs(total), 2)}
                </div>
              )
            },
          },
          {
            title: (
              <>
                SHIPPED
                {
                  hasReturn ?
                    <Tooltip placement="topLeft" title="The order already has a credit memo, cannot be unshipped" arrowPointAtCenter>
                      <ThemeCheckbox
                        disabled={true}
                        style={{ margin: '-2px 0 0 4px' }}
                        onChange={this.onToggleAll.bind(this, 'SHIPPED')}
                        checked={shippedChecked}
                      />
                    </Tooltip>
                    :
                    <ThemeCheckbox
                      disabled={(!isShipping && !isAllPicking) || isCancel || isLocked}
                      style={{ margin: '-2px 0 0 4px' }}
                      onChange={this.onToggleAll.bind(this, 'SHIPPED')}
                      checked={shippedChecked}
                    />
                }

              </>
            ),
            dataIndex: 'status',
            key: 'is_shipped',
            width: 110,
            render: (status: string, record: any, _index: number) => {
              if (record.status === 'PLACED' || record.status === 'NEW') {
                return null
              }
              return (
                <FlexCenter>
                  {status == 'SHIPPED' ? (
                    <ThemeButton
                      // disabled={(!isShipping && isPlacedOrder) || record.returnedQty !=0 || isCancel}
                      // onClick={this.onToggleValue.bind(this, record, 'SHIPPED')}
                      disabled={isLocked}
                      shape="circle"
                      style={{ cursor: 'default' }}
                    >
                      <Icon type="carry-out" theme="filled" />
                    </ThemeButton>
                  ) : (
                    <ThemeOutlineButton
                      // disabled={(!isShipping && isPlacedOrder) || isCancel} onClick={this.onToggleValue.bind(this, record, 'SHIPPED')}
                      disabled={isLocked}
                      shape="circle"
                      style={{ cursor: 'default' }}
                    >
                      <ThemeIcon type="carry-out" theme="filled" />
                    </ThemeOutlineButton>
                  )}
                </FlexCenter>
              )
            },
          },
          {
            title: '',
            key: 'delete',
            render: (_text: any, record: any, index: any) => {
              if (record.status !== 'PLACED' && record.status !== 'NEW') {
                return
              }

              if (record.workOrderStatus) {
                // You cannot delete the sales order item anymore no matter what the status is
                return
              }
              if (isLocked) {
                return
              }

              return (
                <Popconfirm
                  disabled={!isShipping && isPlacedOrder}
                  title="Permanently delete this one related record?"
                  okText="Delete"
                  onConfirm={this.onDeleteRow.bind(this, record.wholesaleOrderItemId)}
                >
                  <FlexCenter style={{ cursor: 'pointer' }}>
                    <ThemeIcon
                      type="close"
                      style={!isShipping && isPlacedOrder ? { color: '#bbb', cursor: 'not-allowed' } : {}}
                    />
                  </FlexCenter>
                </Popconfirm>
              )
            },
          },
        ],
      },
    ]

    if (editable) {
      columns[0].children.push({
        title: 'NOTES',
        dataIndex: 'notes',
        render: (notes: string, record: OrderItem, index: number) => (
          <Popover
            placement="right"
            content={<NotesModal isLocked={isLocked} record={record} setNewNote={this.setNewNote.bind(this)} index={index} />}
            trigger="click"
            onVisibleChange={this.onNotesVisbleChange.bind(this, record, index)}
          >
            <FlexCenter>
              {
                <ThemeIcon
                  type="file-text"
                  style={record.note ? noteIcon : noteGreyIcon}
                // style={record.permanent_note || record.temporary_note ? noteIcon : noteGreyIcon}
                />
              }
            </FlexCenter>
          </Popover>
        ),
      })
    }

    let allProps = { ...this.props }
    allProps['TypeEditWrapper'] = TypeEditWrapper

    return (
      <Wrapper style={ml0}>
        <Wrapper style={ml0}>
          <ThemeModal
            className="price-modal"
            title="PRICING INFORMATION"
            closable={false}
            keyboard={true}
            visible={visiblePriceModal}
            onOk={this.handleModalOk}
            onCancel={this.handleModalOk}
            okText="Close [esc]"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={1330}
            footer={null}
            style={{ height: 868 }}
            centered
          >
            <PriceModal itemId={itemId} clientId={currentOrder ? currentOrder.wholesaleClient.clientId.toString() : ''} />
          </ThemeModal>

          <ThemeModal
            title={'Quantity Details'}
            keyboard={true}
            visible={visibleWeightModal}
            onOk={this.onToggleWeightModal.bind(this, null)}
            onCancel={this.onToggleWeightModal.bind(this, null)}
            okText={'Exit'}
            okButtonProps={{ shape: 'round', style: { display: 'none' } }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={800}
          >
            {visibleWeightModal ? <QuantityModal orderItem={selectedItem} /> : <></>}
          </ThemeModal>

          <ThemeModal
            title={'Lots'}
            keyboard={true}
            visible={visibleLotsModal}
            onOk={this.onToggleLotsModal.bind(this, null)}
            onCancel={this.onToggleLotsModal.bind(this, null)}
            okText="Close [esc]"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={'75%'}
          >
            <LotsTable
              itemId={selectedItem ? selectedItem.itemId : 0}
              lotId={selectedItem ? selectedItem.lotId : 0}
              onSelect={this.handleSelectLot}
            />
          </ThemeModal>
          {currentOrder !== null && (
            <WorkOrderModal
              orderId={currentOrder.wholesaleOrderId}
              orderItemId={orderItemId}
              isSpecial={true}
              visible={visibleNew}
              handleOk={this.handleNew}
              onWordOrderCreated={this.onWorkOrderCreated}
              history={history}
              handleCancel={() => this.toggleNewModal(null)}
            />
          )}

          <EditableTable
            className="nested-head min-height-placeholder sales-cart"
            columns={columns}
            dataSource={dataSource}
            handleSave={this.handleSave}
            handleSelectProduct={this.handleSelectProduct}
            rowKey="wholesaleOrderItemId"
            pagination={{ pageSize: pageSize, current: currentPage, onChange: this.changePage, defaultCurrent: 1 }}
            allProps={allProps}
          // isExpandable={orderStatus === 'SHIPPED'}
          // expandedRowRender={(record: OrderItem) => (
          //   <div style={{ padding: '23px 41px 38px', maxWidth: 1330 }}>
          //     <PriceModal isExpanded={true} itemId={record.itemId} />
          //   </div>
          // )}
          />
        </Wrapper>
      </Wrapper>
    )
  }
}

export default CartOrderTable
