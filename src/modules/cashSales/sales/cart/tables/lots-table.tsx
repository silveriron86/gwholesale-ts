import React from 'react'
import { PureWrapper as Wrapper } from '~/modules/customers/accounts/addresses/addresses.style'
import {
  ThemeTable,
  ml0,
  ThemeModal,
  ThemeIconButton,
  FlexCenter,
  ThemeTextButton,
} from '~/modules/customers/customers.style'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import moment from 'moment'
import {
  formatNumber,
  combineUomForInventory,
  mathRoundFun,
  inventoryQtyToRatioQty,
  formatTimeDifference,
} from '~/common/utils'
import PriceModal from '../modals/price-modal'
import { Icon, Tooltip, Button } from 'antd'
import { isArray } from 'lodash'
import { Flex } from '~/modules/customers/nav-sales/styles'

type LotsTableProps = OrdersDispatchProps &
  OrdersStateProps & {
    itemId: number
    lotId: number
    onSelect: Function
  }
export class LotsTable extends React.PureComponent<LotsTableProps> {
  state = {
    selectedRowKeys: [],
    visiblePriceModal: false,
  }

  onSelectChange = (selectedRowKeys: any) => {
    this.setState({ selectedRowKeys })
  }

  componentDidMount() {
    // console.log("component did mount ", this.props)
    this._getItemLots(this.props.itemId)
  }

  componentWillReceiveProps(nextProps: LotsTableProps) {
    if (this.props.itemId !== nextProps.itemId && nextProps.itemId > 0) {
      this._getItemLots(nextProps.itemId)
    }
  }

  _getItemLots = (itemId: number) => {
    // console.log('lot table is loading', itemId, this.props)
    this.props.getItemLot(itemId)
  }

  handleModalOk = () => {
    this.setState({
      visiblePriceModal: false,
    })
  }

  openPriceModal = () => {
    if (this.props.itemId > 0) {
      this.setState({
        visiblePriceModal: true,
      })
    }
  }

  refreshData = () => {
    this._getItemLots(this.props.itemId)
  }

  render() {
    const { itemLots, itemId, currentOrder, lotDataRefreshedAt } = this.props
    const { visiblePriceModal } = this.state
    // tslint:disable-next-line:no-console
    console.log(itemLots)
    const columns: any[] = [
      // {
      //   title: 'DATE',
      //   dataIndex: 'createdDate',
      //   width: 110,
      //   align: 'center',
      //   render: (value: number) => moment(value).format('MM/DD/YYYY'),
      // },
      {
        title: 'ARRIVAL DATE',
        className: 'th-left',
        dataIndex: 'deliveryDate',
        render: (value: number) => {
          const year = moment.utc(value).get('year')
          // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
          if (year != 1990) {
            return moment.utc(value).format('MM/DD/YYYY')
          }
        },
      },
      {
        title: 'LOT #',
        className: 'th-left',
        dataIndex: 'lotId',
        width: 120,
      },
      // {
      //   title: 'PRODUCT NAME',
      //   dataIndex: 'wholesaleItem.variety',
      // },
      {
        title: 'AVAILABLE TO SELL',
        className: 'th-left',
        dataIndex: 'lotAvailableQty',
        render(_text: any, item: any, _index: any) {
          let lotAvailableQty = item.lotAvailableQty === null ? 0 : item.lotAvailableQty
          // let itemInfo = item.wholesaleItem
          if (lotAvailableQty <= 0) {
            return ''
          }
          // if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio == true && itemInfo.ratioUOM == 1) {
          //   return `${formatNumber(lotAvailableQty, 2)} ${item.uom ? ' ' + item.uom : ''}`
          // } else if (itemInfo.inventoryUOM != itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return formatNumber(lotAvailableQty, 2) + (item.uom ? ' ' + item.uom : '')
          // } else if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return formatNumber(lotAvailableQty, 2) + (item.uom ? ' ' + item.uom : '')
          // } else {
          //   let ratioQty = formatNumber(lotAvailableQty * item.wholesaleItem.ratioUOM, 2)
          //   return (
          //     <div>
          //       {formatNumber(lotAvailableQty, 2)}{' '}
          //       {item.wholesaleItem.inventoryUOM ? ' ' + item.wholesaleItem.inventoryUOM : ''}
          //       <br />
          //       {ratioQty} {item.wholesaleItem.baseUOM ? ' ' + item.wholesaleItem.baseUOM : ''}
          //     </div>
          //   )
          // }

          if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
            if (lotAvailableQty > 0) {
              return isArray(item.wholesaleProductUomList) &&
                item.wholesaleProductUomList.length > 0 &&
                item.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
                <Tooltip
                  title={
                    <>
                      {item.wholesaleProductUomList.map((uom: any) => {
                        if (uom.useForInventory) {
                          return (
                            <div>
                              {inventoryQtyToRatioQty(uom.name, lotAvailableQty, item, 2)} {uom.name}
                            </div>
                          )
                        }
                      })}
                    </>
                  }
                >
                  <span style={{ marginRight: '20px' }}>
                    {mathRoundFun(lotAvailableQty, 2)} {item.inventoryUOM}
                  </span>
                </Tooltip>
              ) : (
                <span style={{ marginRight: '20px' }}>
                  {mathRoundFun(lotAvailableQty, 2)} {item.inventoryUOM}
                </span>
              )
            } else {
              return ''
            }
          } else {
            return mathRoundFun(lotAvailableQty, 2) + ' ' + item.inventoryUOM
          }
        },
      },
      {
        title: 'UNITS ON HAND',
        className: 'th-left',
        dataIndex: 'onHandQty',
        render: (_text: any, item: any, _index: any) => {
          let onHandQty = item.onHandQty === null ? 0 : item.onHandQty
          // let itemInfo = item.wholesaleItem
          if (onHandQty <= 0) {
            return ''
          }
          // if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio == true && itemInfo.ratioUOM == 1) {
          //   return `${onHandQty} ${item.uom ? ' ' + item.uom : ''}`
          // } else if (itemInfo.inventoryUOM != itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return onHandQty + (item.uom ? ' ' + item.uom : '')
          // } else if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return onHandQty + (item.uom ? ' ' + item.uom : '')
          // } else {
          //   let ratioQty = formatNumber(onHandQty * item.wholesaleItem.ratioUOM, 2)
          //   return (
          //     <div>
          //       {onHandQty} {item.wholesaleItem.inventoryUOM ? ' ' + item.wholesaleItem.inventoryUOM : ''}
          //       <br />
          //       {ratioQty} {item.wholesaleItem.baseUOM ? ' ' + item.wholesaleItem.baseUOM : ''}
          //     </div>
          //   )
          // }
          if (item.wholesaleProductUomList != null && item.wholesaleProductUomList.length > 0) {
            if (onHandQty > 0) {
              return isArray(item.wholesaleProductUomList) &&
                item.wholesaleProductUomList.length > 0 &&
                item.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
                <Tooltip
                  title={
                    <>
                      {item.wholesaleProductUomList.map((uom: any) => {
                        if (uom.useForInventory) {
                          return (
                            <div>
                              {inventoryQtyToRatioQty(uom.name, onHandQty, item, 2)} {uom.name}
                            </div>
                          )
                        }
                      })}
                    </>
                  }
                >
                  <span style={{ marginRight: '20px' }}>
                    {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
                  </span>
                </Tooltip>
              ) : (
                <span style={{ marginRight: '20px' }}>
                  {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
                </span>
              )
            } else {
              return ''
            }
          } else {
            return mathRoundFun(onHandQty, 2) + ' ' + item.inventoryUOM
          }
        },
      },
      {
        title: 'COST',
        className: 'th-right th-cost',
        dataIndex: 'cost',
        key: 'totalCost',
        render: (value: number, record: any) => {
          // const freight = record.freight ? record.freight : 0
          // if (record.uom == record.wholesaleItem.inventoryUOM && record.wholesaleItem.constantRatio == true) {
          //   return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? (value + freight) / record.wholesaleItem.ratioUOM : 0, 2)}</div>
          // } else {
          //   return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? value + freight : 0, 2)}</div>
          // }
          let cost = value ? value : 0
          cost += record.perAllocateCost
          return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(cost ? cost : 0, 2)}</div>
        },
      },
      {
        title: 'VENDOR',
        className: 'th-left',
        dataIndex: 'vendor',
        render: (vendor: any, record: any) => {
          if (record.wholesaleClient && record.wholesaleClient.clientCompany) {
            return record.wholesaleClient.clientCompany.companyName
          } else {
            return ''
          }
        },
      },
      {
        title: 'BRAND',
        className: 'th-left th-cost',
        dataIndex: 'modifiers',
        // width: 120,
      },
      {
        title: 'ORIGIN',
        className: 'th-left th-cost',
        dataIndex: 'extraOrigin',
      },
      {
        title: 'NOTES FROM PO',
        className: 'th-left',
        dataIndex: 'note',
        width: 180,
        render: (note: string) => {
          return (
            <Tooltip title={note && note.length > 25 ? note : ''}>
              <div style={{ width: 160, overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>
                {note ? note : ''}
              </div>
            </Tooltip>
          )
        },
      },
      // {
      //   title: 'QUOTED PRICE',
      //   dataIndex: 'cost',
      //   key: 'quotedCost',
      //   width: 120,
      //   render: (value: number, record: any) => {
      //     const markup = (record.margin ? record.margin : 0) / 100
      //     const freight = record.freight ? record.freight : 0
      //     if (record.uom == record.wholesaleItem.inventoryUOM && record.wholesaleItem.constantRatio == true) {
      //       return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? ((1 + markup) * (value + freight) / record.wholesaleItem.ratioUOM) : 0, 2)}</div>
      //     } else {
      //       return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? (1 + markup) * (value + freight) : 0, 2)}</div>
      //     }
      //   }
      // },
      // {
      //   title: 'QUOTED PRICE',
      //   key: 'priceBar',
      //   render: (_text: string, record: any) => (
      //     <FlexCenter>
      //       <ThemeIconButton type="link" style={{ borderWidth: 0 }} onClick={() => this.openPriceModal()}>
      //         <Icon type="bar-chart" style={{ fontSize: 20 }} />
      //       </ThemeIconButton>
      //     </FlexCenter>
      //   ),
      // },
    ]

    return (
      <Wrapper style={{ ...ml0, marginTop: -8, width: '100%' }}>
        {itemLots.length > 0 && (
          <Flex className="space-between v-center" style={{ marginBottom: 16 }}>
            <div style={{ fontSize: 18 }}>{itemLots[0].wholesaleItem.variety}</div>
            <div style={{ textAlign: 'right' }}>
              <Button onClick={this.refreshData}>
                <Icon type="sync" />
                Refresh
              </Button>
              <br />
              <span style={{ fontWeight: 100 }}>
                {lotDataRefreshedAt ? `Refreshed ${formatTimeDifference(lotDataRefreshedAt)} ago` : ''}
              </span>
            </div>
          </Flex>
        )}
        <ThemeModal
          className="price-modal"
          title="PRICING INFORMATION"
          closable={false}
          keyboard={true}
          visible={visiblePriceModal}
          onOk={this.handleModalOk}
          onCancel={this.handleModalOk}
          okText="Close [esc]"
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={1330}
          footer={null}
          style={{ height: 868 }}
          centered
        >
          <PriceModal itemId={itemId} clientId={currentOrder ? currentOrder.wholesaleClient.clientId.toString() : ''} />
        </ThemeModal>
        <ThemeTable
          className="lots-table"
          rowKey="lotId"
          pagination={{ hideOnSinglePage: true, pageSize: 12 }}
          columns={columns}
          dataSource={itemLots}
          footer={() => {
            return (
              <ThemeTextButton onClick={() => this.props.onSelect(null)}>
                Do not pull from an existing lot
              </ThemeTextButton>
            )
          }}
          rowKey="wholesaleOrderItemId"
          // tslint:disable-next-line:jsx-no-lambda
          rowClassName={(record: any, _rowIndex) => {
            return record.lotId === this.props.lotId ? 'selected-row' : ''
          }}
          // tslint:disable-next-line:jsx-no-lambda
          onRow={(record, _rowIndex) => {
            return {
              onClick: (_event) => {
                _event.preventDefault()
                if (_event.target.tagName !== 'BUTTON') {
                  this.props.onSelect(record)
                }
              },
            }
          }}
        />
      </Wrapper>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default connect(OrdersModule)(mapStateToProps)(LotsTable)
