import React from 'react'
import { Icon } from 'antd'
import { PureWrapper as Wrapper, WrapperTitle } from '~/modules/customers/accounts/addresses/addresses.style'
import { Picking } from '../../../customers.model'
import { ThemeButton, ThemeTable, ml0, ThemeCheckbox } from '~/modules/customers/customers.style'
import { cloneDeep } from 'lodash'

interface PickingTableProps {
  items: Array<Picking>
}

export class PickingTable extends React.PureComponent<PickingTableProps> {
  state = {
    initItems: cloneDeep(this.props.items),
    selectedRowKeys: [],
  }

  onSelectChange = (selectedRowKeys: any) => {
    this.setState({ selectedRowKeys })
  }

  render() {
    const { initItems } = this.state
    const listItems: Picking[] = cloneDeep(initItems)

    const columns = [
      {
        title: (
          <>
            PICKED
            <ThemeCheckbox style={{ margin: '-2px 0 0 2px' }} />
          </>
        ),
        key: 'a',
        render: () => (
          <ThemeButton type="primary" shape="circle">
            <Icon type="bulb" theme="filled" />
          </ThemeButton>
        ),
      },
      {
        title: 'PICKED',
        dataIndex: 'picked',
      },
      {
        title: 'QUANTITY',
        dataIndex: 'quantity',
      },
    ]

    // const rowSelection = {
    //   selectedRowKeys,
    //   onChange: this.onSelectChange
    // }

    return (
      <Wrapper style={ml0}>
        <WrapperTitle>Picking</WrapperTitle>
        <ThemeTable
          // rowSelection={rowSelection}
          pagination={{ hideOnSinglePage: true, pageSize: 12 }}
          columns={columns}
          dataSource={listItems}
        />
      </Wrapper>
    )
  }
}

export default PickingTable
