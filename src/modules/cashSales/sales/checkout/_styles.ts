import { medLightGrey, mediumGrey, badgeColor, black } from '~/common'
import styled from '@emotion/styled'

export const editFormWrapper: React.CSSProperties = {
  background: 'transparent',
  borderTop: `1px solid ${medLightGrey}`,
  borderBottom: `1px solid ${medLightGrey}`,
  paddingTop: 10,
  paddingBottom: 0,
  margin: '15px 0 20px',
}

export const Frame = styled('div')({
  border: `1px solid ${medLightGrey}`,
  borderRadius: 4,
  padding: '15px 20px',
})

export const AddressLabel = styled('div')({
  fontSize: 17,
  lineHeight: '20px',
  fontWeight: 'bold',
  color: mediumGrey,
  height: 60,
  margin: '10px 10px 15px',
  display: 'flex',
  alignItems: 'center',
})

export const IconWrapper = styled('span')((props) => ({
  cursor: 'pointer',
  '& path': {
    fill: props.theme.primary,
  },
}))

export const noDriveSet: React.CSSProperties = {
  color: '#ccc',
  fontSize: 13,
  fontWeight: 'normal',
}

export const routeText: React.CSSProperties = {
  fontSize: 17,
}

export const userIcon: React.CSSProperties = {
  color: '#ccc',
  fontSize: 30,
}

export const shippingRadois: React.CSSProperties = {
  margin: '5px 50px',
}

export const DetailsRow: React.CSSProperties = {
  margin: '-10px -20px 5px',
}

interface ActiveProps {
  active?: boolean
}

export const DetailValue = styled('div')<ActiveProps>((props) => ({
  fontSize: '17px',
  fontFamily: 'Arial',
  fontWeight: 'bold',
  color: props.active ? badgeColor : black,
}))
