import * as React from 'react'
import { pd20, ThemeOutlineButton, InputLabel, ThemeRadio, PopoverWrapper, InputRow, ThemeSelect, RowDivider, ThemeButton } from '~/modules/customers/customers.style'
import { Layout, Row, Col, Icon, Radio, Popover, Select } from 'antd'
import { transLayout, Flex } from '~/modules/customers/accounts/components/customers-detail/customers-detail.style'
import { Frame, AddressLabel, noDriveSet, routeText, userIcon, shippingRadois, DetailsRow, IconWrapper } from './_styles'
import { cloneDeep } from 'lodash'
import { formatAddress } from '~/common/utils'
import ChangeAddressModal from '~/modules/customers/sales/widgets/change-address-modal'
import { GlobalState } from '~/store/reducer'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { OrdersDispatchProps, OrdersModule, OrdersStateProps } from '~/modules/orders'
import { MainAddress, WholesaleRoute } from '~/schema'
import { RouteComponentProps } from 'react-router'
import { Theme } from '~/common'
import moment from 'moment'
import { Icon as IconSVG } from '~/components/icon'

const { Option } = Select

type ChangesProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
    shippingAddress: MainAddress
    billingAddress: MainAddress
  }

export class Changes extends React.PureComponent<ChangesProps> {
  state: any
  constructor(props: ChangesProps) {
    super(props)
    this.state = {
      shippingModalVisible: false,
      billingModalVisible: false,
      routeModalVisible: false,
      overrideRoute: null,
      shippingAddress: {},
      billingAddress: {},
      billingAddresses: [],
      shippingAddresses: [],
      columns: [
        {
          title: '#',
          dataIndex: 'index',
          width: 50,
          render: (_text: string, record: MainAddress, index: number) => {
            return index + 1
          },
        },
        {
          title: 'FULL ADDRESS',
          dataIndex: 'address',
          render: (_text: string, record: MainAddress, _index: number) => {
            const { currentOrder } = this.props
            const isStar = currentOrder && currentOrder.shippingAddress && currentOrder.shippingAddress.wholesaleAddressId > 0
              && currentOrder.shippingAddress.wholesaleAddressId == record.wholesaleAddressId
            console.log(record)
            return <div>
              {isStar &&
                <IconWrapper>
                  <IconSVG style={{ marginRight: 4 }} type="star" viewBox="0 0 24 24" width="16" height="16" ></IconSVG>
                </IconWrapper>
              }
              <span>{record.address.street1} {record.address.city} {record.address.state}, {record.address.zipcode}</span>
            </div>
          },
        },
      ]
    }
  }


  handleData = (addresses: any[]) => {

    const _billingAddresses: MainAddress[] = []
    const _shippingAddresses: MainAddress[] = []

    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        _shippingAddresses.push(address)
      } else if (address.addressType === 'BILLING') {
        _billingAddresses.push(address)
      }
    })

    this.setState({
      billingAddresses: cloneDeep(_billingAddresses),
      shippingAddresses: cloneDeep(_shippingAddresses)
    })
  }

  componentWillReceiveProps(nextProps: ChangesProps) {
    this.handleData(nextProps.addresses)
  }



  componentDidMount(): void {
    // this.handleData(this.props.addresses)
    this.props.getAllRoutes()
  }

  handleShippingModalVisibleChange = (visible: boolean) => {
    this.setState({ shippingModalVisible: visible });
  };

  handleBillingModalVisibleChange = (visible: boolean) => {
    this.setState({ billingModalVisible: visible });
  };


  onRadioChange = (e: any) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      pickup: e.target.value,
      cashSale: true
    }
    this.props.updateOrderInfo(data)
  }

  onShippingAddressClickRow = (record: any, e: any) => {

    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      shippingAddressId: record.wholesaleAddressId,
      cashSale: true
    }
    this.props.updateOrderInfo(data)

    this.setState({
      shippingModalVisible: false,
    })

  }

  onBillingAddressClickRow = (record: any, e: any) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      billingAddressId: record.wholesaleAddressId,
      cashSale: true
    }
    this.props.updateOrderInfo(data)

    this.setState({
      billingModalVisible: false,
    })
  }

  handleRouteModalVisibleChange = () => {
    const { routeModalVisible } = this.state
    const { currentOrder } = this.props
    if (!routeModalVisible && currentOrder) {
      const activeRoutes = this._getActiveRoutes()
      if (activeRoutes.length > 0) {
        if (currentOrder.defaultRoute || currentOrder.overrideRoute) {
          const routeId = currentOrder.overrideRoute ? currentOrder.overrideRoute.id : currentOrder.defaultRoute.id
          const found = activeRoutes.find((r) => r.id === routeId)
          this.setState({
            overrideRoute: (typeof found === 'undefined') ? activeRoutes[0].id : routeId
          })
        } else {
          this.setState({
            overrideRoute: activeRoutes[0].id
          })
        }
      }
    }
    this.setState({
      routeModalVisible: !routeModalVisible,
    })
  }

  handleChangeRoute = (value: any) => {
    this.setState({
      overrideRoute: value,
    })
  }

  saveRoute = () => {
    const { overrideRoute } = this.state
    const { currentOrder } = this.props
    if (overrideRoute && currentOrder) {
      this.props.updateOrderInfo({
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
        defaultRoute: overrideRoute,
        cashSale: true
      })
    }
    this.handleRouteModalVisibleChange()
  }

  _getActiveRoutes = () => {
    const { currentOrder, routes } = this.props
    let activeRoutes: WholesaleRoute[] = []
    if (currentOrder !== null && routes.length > 0) {
      routes.forEach(route => {
        const weekDay = moment(currentOrder.deliveryDate).format('dddd').toUpperCase()
        if (currentOrder && route.activeDays.indexOf(weekDay) >= 0) {
          activeRoutes.push({ ...route })
        }
      })
    }
    return activeRoutes
  }

  render() {
    const { routeModalVisible, overrideRoute } = this.state
    const { currentOrder } = this.props
    const activeRoutes = this._getActiveRoutes()

    if (!currentOrder) {
      return null
    }

    return (
      <Layout style={transLayout}>
        <Row style={DetailsRow}>
          <Col lg={8} md={12} style={pd20}>
            <Frame>
              <Flex>
                <InputLabel>SHIPPING</InputLabel>
                <Radio.Group onChange={this.onRadioChange} value={currentOrder.pickup} style={shippingRadois}>
                  <ThemeRadio value={false}>DELIVERY</ThemeRadio>
                  <ThemeRadio value={true}>PICKUP</ThemeRadio>
                </Radio.Group>
              </Flex>
              <AddressLabel>{!currentOrder.pickup && currentOrder.shippingAddress && formatAddress(currentOrder.shippingAddress.address)}</AddressLabel>


              <Popover
                content={<ChangeAddressModal columns={this.state.columns} dataSource={this.state.shippingAddresses} onClick={this.onShippingAddressClickRow.bind(this)} />}
                title="Click & Select Shipping address "
                trigger="click"
                visible={this.state.shippingModalVisible}
                onVisibleChange={this.handleShippingModalVisibleChange}
              >
                <ThemeOutlineButton shape="round" style={{ marginRight: 20 }} disabled={currentOrder.pickup || currentOrder.wholesaleOrderStatus == 'CANCEL'}>
                  Change
                  <Icon type="arrow-right" />
                </ThemeOutlineButton>
              </Popover>

              <ThemeOutlineButton shape="round" disabled>
                Delivery Windows
                <Icon type="arrow-right" />
              </ThemeOutlineButton>
            </Frame>
          </Col>
          <Col lg={8} md={12} style={pd20}>
            <Frame>
              <InputLabel>PAYMENT ADDRESS</InputLabel>
              <AddressLabel>{currentOrder.billingAddress && formatAddress(currentOrder.billingAddress.address)}</AddressLabel>
              <Popover
                content={<ChangeAddressModal columns={this.state.columns} dataSource={this.state.billingAddresses} onClick={this.onBillingAddressClickRow.bind(this)} />}
                title="Click & Select payment address "
                trigger="click"
                visible={this.state.billingModalVisible}
                onVisibleChange={this.handleBillingModalVisibleChange}
              >
                <ThemeOutlineButton shape="round" style={{ marginRight: 20 }} disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}>
                  Change
                  <Icon type="arrow-right" />
                </ThemeOutlineButton>
              </Popover>
            </Frame>
          </Col>
          <Col lg={8} md={12} style={pd20}>
            <Frame>
              <InputLabel>DRIVE / ROUTE</InputLabel>
              <AddressLabel>
                <Icon type="user" style={userIcon} />
                <div style={{ marginLeft: 30 }}>
                  {(currentOrder && (currentOrder.overrideRoute || currentOrder.defaultRoute)) ? (
                    <div style={routeText}>{(currentOrder.overrideRoute ? currentOrder.overrideRoute.routeName : currentOrder.defaultRoute.routeName)}</div>
                  ) : (
                    <div style={noDriveSet}>No Drive Set...</div>
                  )}
                </div>
              </AddressLabel>
              <Popover
                placement="right"
                content={
                  <PopoverWrapper style={{ width: 400 }}>
                    <Layout style={transLayout}>
                      <InputRow>
                        <Col md={7}>
                          <InputLabel>ROUTE</InputLabel>
                        </Col>
                        <Col md={17}>
                          <ThemeSelect style={{ width: '100%' }} value={overrideRoute} onChange={this.handleChangeRoute}>
                            {
                              activeRoutes.length > 0 && activeRoutes.map((route, index) => {
                                return <Option key={`route=${index}`} value={route.id}>{route.routeName}</Option>
                              })
                            }
                          </ThemeSelect>
                        </Col>
                      </InputRow>
                      <RowDivider />
                      <Row>
                        <Col align="right">
                          <ThemeButton onClick={this.saveRoute}>
                            <Icon type="save" theme="filled" />
                            Save
                          </ThemeButton>
                        </Col>
                      </Row>
                    </Layout>
                  </PopoverWrapper>
                }
                trigger="click"
                visible={routeModalVisible}
                onVisibleChange={this.handleRouteModalVisibleChange}
              >
                <ThemeOutlineButton shape="round" disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}>
                  Change
                  <Icon type="arrow-right" />
                </ThemeOutlineButton>
              </Popover>
            </Frame>
          </Col>
        </Row>
      </Layout>
    )
  }
}
const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(Changes))

// export default Changes
