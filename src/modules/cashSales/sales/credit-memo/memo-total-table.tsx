import React from 'react'
import { ThemeButton, ThemeCheckbox, ThemeIcon, ThemeInput } from '~/modules/customers/customers.style'
import { cloneDeep } from 'lodash'
import { Icon as IconSVG } from '~/components'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import SelectShippedProductModal from './memo-select-shipped-product-modal'
import { Popconfirm, Table, Tooltip } from 'antd'
import {
  formatNumber,
  mathRoundFun,
  basePriceToRatioPrice,
  ratioQtyToInventoryQty,
  formatItemDescription,
  getAllowedEndpoint,
} from '~/common/utils'
import { CartTableWrapper, Flex, ItemHMargin4, Unit } from '../../nav-sales/styles'
import UomSelect from '~/modules/customers/nav-sales/order-detail/components/uom-select'
import _ from 'lodash'
import { OrderItem } from '~/schema'
import { CMLabel, CMValue } from '../_style'
import moment from 'moment'
import styled from '@emotion/styled'
import { gray01 } from '~/common'

type MemoTotalTableProps = OrdersDispatchProps &
  OrdersStateProps & {
    orderId: string
    isPurchase?: Boolean
  }

export class MemoTotalTable extends React.PureComponent<MemoTotalTableProps> {
  state = {
    dataSource: cloneDeep(this.props.adjustments),
    showTable: this.props.adjustments.length > 0 ? true : false,
    selectShippedProductModalVisible: false,
    currentUOM: '',
  }

  componentDidMount() {
    const { orderId } = this.props
    this.props.getOrderAdjustments(orderId)
  }

  onChangeUom = (orderItemId: string, value: any) => {
    const newData = [...this.state.dataSource]
    newData.map((orderItem: any) => {
      if (orderItem.wholesaleOrderItemId === orderItemId) {
        orderItem[this.state.currentUOM] = value
        this.handleSave(orderItem)
        return
      }
    })
    this.setState({ dataSource: cloneDeep(newData) })
  }

  updateOrderItem = (orderItemId: number, type: string, pas: boolean) => {
    //
  }

  onChangeItem = (wholesaleOrderItemId: number, type: string, value: string | boolean) => {
    const newData = [...this.state.dataSource]
    newData.map((orderItem: any) => {
      if (orderItem.wholesaleOrderItemId === wholesaleOrderItemId) {
        orderItem[type] = value
        if (value === false) {
          orderItem['returnInventoryQuantity'] = 0
          orderItem['returnInventoryUOM'] = null
        }
        if (type === 'returnInInventory') {
          this.handleSave(orderItem)
        }
        return
      }
    })
    this.setState({ dataSource: newData })
  }

  changeTableStatus = () => {
    this.setState({
      showTable: true,
    })
  }

  handleSave = (row: any) => {
    const { adjustments } = this.props
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
    if (adjustments.length > 0) {
      const filtered = adjustments.filter((item) => item.orderItem.wholesaleOrderItemId === row.wholesaleOrderItemId)
      if (filtered.length > 0) {
        // update
        // tslint:disable-next-line:prefer-const
        let data = this._getAdjustmentObject(row)
        data.adjustmentId = row.adjustmentId
        if (
          data.returnQuantity !== item.returnQuantity ||
          data.UOM !== item.UOM ||
          data.returnReason !== item.returnReason ||
          data.returnInInventory !== item.returnInInventory
        ) {
          this.props.updateOrderAdjustment(data)
        }
        return
      }
    }
    this.props.createOrderAdjustment({ orderId: this.props.orderId, requestData: this._getAdjustmentObject(row) })
  }

  _getAdjustmentObject = (row: any) => {
    const quantity = Math.abs(row.returnQuantity)
    const returnQuantity = Math.abs(row.returnInventoryQuantity)
    // const expiredQuantity = Math.abs(row.return_expired_quantity)
    return {
      adjustmentId: '',
      orderItemId: row.wholesaleOrderItemId,
      quantity: quantity ? quantity : 0,
      // expiredQuantity: expiredQuantity,
      returnInInventory: row.returnInInventory ? true : false,
      reason: row.returnReason ? row.returnReason : '',
      UOM: row.returnUom ? row.returnUom : row.UOM,
      // returnWeight: row.return_net_weight,
      type: 1,
      returnQuantity,
      returnUOM: row.returnInventoryUOM || row.returnUom || row.UOM,
      // cost: row.return_net_weight
    }
  }

  componentWillReceiveProps(nextProps: MemoTotalTableProps) {
    const orderItemIds = nextProps.orderItems.map((v) => v.wholesaleOrderItemId)
    const adjustmentIds = nextProps.adjustments.length
      ? nextProps.adjustments.map((v) => v.orderItem.wholesaleOrderItemId)
      : []

    const extraAdjustments = nextProps.adjustments.filter(
      (ad) => !orderItemIds.includes(ad.orderItem.wholesaleOrderItemId),
    )

    if (nextProps.currentOrder.wholesaleClient.wholesaleCompany.isAutomatically) {
      if (
        nextProps.orderItems.length > 0 &&
        _.intersection(orderItemIds, adjustmentIds).length < nextProps.orderItems.length
      ) {
        const data = [...nextProps.orderItems]
        const adjustments = [...nextProps.adjustments]
        data.map((orderItem: any) => {
          const filtered = adjustments.filter(
            (ad) => ad.orderItem.wholesaleOrderItemId === orderItem.wholesaleOrderItemId,
          )
          if (filtered.length > 0) {
            orderItem.adjustmentId = filtered[0].id
            orderItem.returnQuantity = filtered[0].quantity
            // orderItem.return_expired_quantity = filtered[0].returnExpiredQty
            orderItem.returnReason = filtered[0].reason
            // orderItem.return_net_weight = filtered[0].returnWeight
            // orderItem.margin = filtered[0].margin
            orderItem.returnUom = filtered[0].uom
            orderItem.returnInInventory = filtered[0].returnInInventory ? true : false
            orderItem.returnInventoryQuantity = filtered[0].returnQuantity || filtered[0].quantity
            orderItem.returnInventoryUOM = filtered[0].returnUOM
          } else {
            ; (orderItem.returnUom = orderItem.UOM), (orderItem.returnQuantity = 0)
          }
        })

        this.setState({
          dataSource: cloneDeep([
            ...extraAdjustments.map((v) => ({
              ...v,
              ...v.orderItem.wholesaleItem,
              ...v.orderItem,
              adjustmentId: v.id,
              returnQuantity: v.quantity,
              returnReason: v.reason,
              returnUom: v.uom,
              returnInInventory: v.returnInInventory ? true : false,
              UOM: v.orderItem.uom,
              returnInventoryQuantity: v.returnQuantity || v.quantity,
              returnInventoryUOM: v.returnUOM,
            })),
            ...data,
          ]),
        })
      }

      if (
        nextProps.adjustments !== this.props.adjustments &&
        _.intersection(orderItemIds, adjustmentIds).length >= nextProps.orderItems.length
      ) {
        this.setState({
          dataSource: nextProps.adjustments.map((v) => ({
            ...v,
            ...v.orderItem.wholesaleItem,
            ...v.orderItem,
            adjustmentId: v.id,
            returnQuantity: v.quantity,
            returnReason: v.reason,
            returnUom: v.uom,
            returnInInventory: v.returnInInventory ? true : false,
            UOM: v.orderItem.uom,
            returnInventoryQuantity: v.returnQuantity || v.quantity,
            returnInventoryUOM: v.returnUOM,
          })),
        })
      }
    } else {
      return this.setState({
        dataSource: nextProps.adjustments.map((v) => ({
          ...v,
          ...v.orderItem.wholesaleItem,
          ...v.orderItem,
          adjustmentId: v.id,
          returnQuantity: v.quantity,
          returnReason: v.reason,
          returnUom: v.uom,
          returnInInventory: v.returnInInventory ? true : false,
          UOM: v.orderItem.uom,
          returnInventoryQuantity: v.returnQuantity || v.quantity,
          returnInventoryUOM: v.returnUOM,
        })),
      })
    }
  }

  // getBillableQtyWithUOM = (record: any) => {
  //   let showBillableQty,
  //     uom,
  //     showValue = false,
  //     subUom = null,
  //     pricingSubUom = null

  //   if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
  //     let tempUom = record.wholesaleProductUomList.filter(
  //       (productUom: WholesaleProductUom) => productUom.name === record.returnUom,
  //     )
  //     subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
  //     tempUom = record.wholesaleProductUomList.filter(
  //       (productUom: WholesaleProductUom) => productUom.name === record.returnUom,
  //     )
  //     pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
  //   }

  //   if (record.returnUom) {
  //     uom = record.returnUom
  //     if (record.returnUom === record.inventoryUOM) {
  //       if (record.returnUom === record.inventoryUOM) {
  //         showBillableQty = record.returnQuantity ? record.returnQuantity : 0
  //       } else if (subUom != null && uom === subUom.name) {
  //         console.log(1)
  //         showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity / subUom.ratio, 4) : 0
  //       }
  //     } else {
  //       if (record.returnUom === record.inventoryUOM && pricingSubUom != null) {
  //         showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity * pricingSubUom.ratio, 4) : 0
  //       } else if (subUom != null && uom === subUom.name && pricingSubUom != null) {
  //         showBillableQty = record.returnQuantity ? (record.returnQuantity / subUom.ratio) * pricingSubUom.ratio : 0
  //       }
  //     }
  //   } else {
  //     showBillableQty = record.returnQuantity
  //     uom = record.returnUom || record.inventoryUOM
  //   }
  //   return {
  //     showBillableQty
  //   }
  // }

  getBillableQtyWithUOM = (record: any) => {
    let showBillableQty,
      uom,
      subUom = null,
      pricingSubUom = null

    if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
      let tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.returnUom,
      )
      subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
      tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.pricingUOM,
      )
      pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
    }

    if (record.returnQuantity) {
      uom = record.pricingUOM
      if (record.pricingUOM === record.inventoryUOM) {
        if (record.returnUom === record.inventoryUOM) {
          showBillableQty = record.returnQuantity ? record.returnQuantity : 0
        } else if (subUom != null && record.returnUom === subUom.name) {
          showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity / subUom.ratio, 4) : 0
        } else {
        }
      } else {
        if (record.returnUom === record.inventoryUOM && pricingSubUom != null) {
          showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity * pricingSubUom.ratio, 4) : 0
        } else if (subUom != null && record.returnUom === subUom.name && pricingSubUom != null) {
          showBillableQty = record.returnQuantity ? (record.returnQuantity / subUom.ratio) * pricingSubUom.ratio : 0
        }
      }
    } else {
      showBillableQty = record.returnQuantity
      uom = record.pricingUOM
    }
    if (!record.pricingUOM) {
      showBillableQty = record.returnQuantity
    }
    return {
      showBillableQty,
      uom,
    }
  }

  render() {
    const { dataSource, showTable, selectShippedProductModalVisible } = this.state
    const { isPurchase, catchWeightValues, currentOrder, updating, adjustments, orderId, sellerSetting } = this.props
    // const readOnly = currentOrder.wholesaleOrderStatus !== 'SHIPPED'
    const readOnly = false
    const accountType = localStorage.getItem("accountType") || ''

    const columns: Array<any> = [
      {
        title: '',
      },
      {
        title: 'Item',
        dataIndex: 'variety',
        width: 300,
        render: (variety: string, record: any, index: number) => {
          let visibleWarningTooltip = record.picked !== 0
          if (record.constantRatio === false) {
            const weights = catchWeightValues[record.wholesaleOrderItemId]
            visibleWarningTooltip = weights && weights.length ? true : false
          }
          return (
            <>
              <div className="name-column">
                <Flex style={{ paddingLeft: 8 }}>
                  <a
                    href={`#/product/${record.wholesaleItemId || record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`}
                    className="product-name"
                  >
                    {formatItemDescription(variety, record.sku, sellerSetting)}
                  </a>
                  <div>
                    <Tooltip placement="top" title={'Note added'}>
                      <IconSVG
                        // onClick={openModal.bind(this, 'addNoteModal', record)}
                        type="add-note"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          marginTop: 2,
                          display: record.note ? 'block' : 'none',
                          fill: 'none',
                        }}
                      />
                    </Tooltip>
                  </div>
                  <div>
                    <Tooltip placement="top" title={'Work order added'}>
                      <IconSVG
                        // onClick={openModal.bind(this, 'addWOModal', record)}
                        type="add-wo"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          display: record.workOrderStatus ? 'block' : 'none',
                          cursor: 'normal',
                        }}
                      />
                    </Tooltip>
                  </div>
                </Flex>
              </div>
            </>
          )
        },
      },
      {
        title: 'Original order',
        render: (r: any) => {
          return (
            <span>
              {r.wholesaleOrderItemId} ({moment(r.deliveryDate).format('MM/DD/YYYY')})
            </span>
          )
        },
      },
      {
        title: 'Lot',
        dataIndex: 'lotId',
        width: 150,
        render: (lotId: string, record: any) => {
          return lotId ? lotId : 'No lots available'
        },
      },
      {
        title: 'Total shipped units',
        dataIndex: 'quantity',
        width: 120,
        render: (quantity: number, record: any) => {
          const pricingUOM = record.UOM ? record.UOM : record.inventoryUOM
          // const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          return (
            <Flex className="v-center">
              {quantity ? quantity : ''} {pricingUOM}
            </Flex>
          )
        },
      },
      {
        title: 'Credited units',
        dataIndex: 'returnQuantity',
        width: 200,
        render: (quantity: number, record: OrderItem) => {
          // record.UOM = record.return_uom ? record.return_uom : record.UOM
          return (
            <Flex className="v-center">
              <ThemeInput
                value={quantity ? quantity : 0}
                style={{ marginRight: 4, width: 60, textAlign: 'right' }}
                onChange={(evt: any) =>
                  this.onChangeItem(record.wholesaleOrderItemId, 'returnQuantity', evt.target.value)
                }
                onBlur={() => this.handleSave(record)}
                disabled={readOnly}
              />
              <UomSelect
                disabled={readOnly}
                orderItem={record}
                handlerChangeUom={(orderItemId, value) => {
                  this.setState({ currentUOM: 'returnUom' }, () => {
                    this.onChangeUom(orderItemId, value)
                  })
                }}
                type={1}
                from="creditMemo"
              />
            </Flex>
          )
        },
      },
      {
        title: 'Price',
        dataIndex: 'salePrice',
        width: 150,
        render: (value: number, record: any) => {
          let price = value
          // const pricingUOM = record.returnUom
          const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          if (_.isNumber(price)) {
            price = basePriceToRatioPrice(pricingUOM, price, record)
          }

          return (
            <Flex className="v-center">
              {`${price ? '$' + formatNumber(price, 2) : '$0.00'}`}
              <Unit style={{ flex: 1, minWidth: 60 }}>/ {pricingUOM}</Unit>
            </Flex>
          )
        },
      },
      {
        title: 'Billable Qty',
        dataIndex: 'catchWeightQty',
        // width: 200,
        align: 'left',
        render: (weight: number, record: any) => {
          const { uom, showBillableQty } = this.getBillableQtyWithUOM(record)
          return (
            <Flex className="v-center">
              <div style={{ textAlign: 'right' }}>{mathRoundFun(showBillableQty, 4)}</div>
              <Unit style={{ flex: 'unset' }}>{uom}</Unit>
            </Flex>
          )
        },
      },
      {
        title: <Tooltip title="If checked, Return Qty will be added back into inventory.">Return to Inventory</Tooltip>,
        dataIndex: 'returnInInventory',
        width: 230,
        render: (value: boolean, record: any) => {
          return (
            <Flex className="v-center">
              <ThemeCheckbox
                className="pas"
                checked={value}
                onChange={(e: any) =>
                  this.onChangeItem(record.wholesaleOrderItemId, 'returnInInventory', e.target.checked)
                }
                disabled={readOnly}
              />
              {record.returnInInventory && (
                <>
                  <ThemeInput
                    value={record.returnInventoryQuantity || 0}
                    style={{ marginRight: 4, marginLeft: 4, width: 60, textAlign: 'right' }}
                    onChange={(evt: any) =>
                      this.onChangeItem(record.wholesaleOrderItemId, 'returnInventoryQuantity', evt.target.value)
                    }
                    onBlur={() => this.handleSave(record)}
                    disabled={readOnly}
                  />
                  <UomSelect
                    disabled={readOnly}
                    defaultUom={record.returnInventoryUOM || record.UOM || record.inventoryUOM}
                    orderItem={record}
                    handlerChangeUom={(orderItemId, value) => {
                      this.setState({ currentUOM: 'returnInventoryUOM' }, () => {
                        this.onChangeUom(orderItemId, value)
                      })
                    }}
                    type={1}
                    from="creditMemo"
                  />
                </>
              )}
            </Flex>
          )
        },
      },
      {
        title: 'Total Price',
        dataIndex: 'totalPrice',
        render: (data: number, record: any) => {
          const { showBillableQty, uom } = this.getBillableQtyWithUOM(record)
          const total = showBillableQty * basePriceToRatioPrice(uom, record.price, record)

          return <>{total ? `$${formatNumber(Math.abs(total), 2)}` : ''}</>
        },
      },
      {
        title: 'Return Reason',
        dataIndex: 'returnReason',
        width: 200,
        // sorter: (a: any, b: any) => onSortString('status', a, b),
        render: (value: string, record: any) => {
          return (
            <Flex className="v-center space-between">
              <ThemeInput
                disabled={readOnly}
                style={{ width: '150px' }}
                value={value}
                onChange={(evt: any) =>
                  this.onChangeItem(record.wholesaleOrderItemId, 'returnReason', evt.target.value)
                }
                onBlur={() => this.handleSave(record)}
              />
              <Popconfirm
                title="Permanently delete this one related record?"
                okText="Delete"
                onConfirm={() => this.props.deleteOrderAdjustement(record.adjustmentId)}
              >
                {record.adjustmentId && <ThemeIcon type="delete" className="delete-btn" />}
              </Popconfirm>
            </Flex>
          )
        },
      },
    ]

    let totalReturn = 0
    let totalQty = 0
    let totalUnit = 0
    let totalReInventory = 0

    if (dataSource.length > 0) {
      dataSource.forEach((record, index) => {
        const { showBillableQty, uom } = this.getBillableQtyWithUOM(record)
        const extended = Number(record.price ? record.price : 0)
        const quantity = ratioQtyToInventoryQty(record.returnUom, record.returnQuantity, record)
        let showValue = false
        if (quantity) {
          showValue = true
        }
        totalReturn += showValue ? showBillableQty * basePriceToRatioPrice(uom, extended, record) : 0

        totalQty += Number(showBillableQty)
        totalUnit += typeof record.returnQuantity !== 'undefined' ? parseFloat(record.returnQuantity) : 0
        totalReInventory +=
          typeof record.returnInventoryQuantity !== 'undefined' ? parseFloat(record.returnInventoryQuantity) : 0
      })
    }

    const Totals = () => {
      if (totalReturn == 0) return null
      return (
        <Flex style={{ paddingRight: 30 }}>
          <div>
            <CMLabel>Total credited units</CMLabel>
            <CMValue>{showTable ? totalUnit : 0}</CMValue>
          </div>
          <div style={{ margin: '0 20px' }}>
            <CMLabel>Total billable units</CMLabel>
            <CMValue>{formatNumber(showTable ? Math.abs(totalQty) : 0, 2)}</CMValue>
          </div>
          <div style={{ margin: '0 20px' }}>
            <CMLabel>Total re-inventory units</CMLabel>
            <CMValue>{totalReInventory || 0}</CMValue>
          </div>
          <div>
            <CMLabel>Total credit</CMLabel>
            <CMValue>${formatNumber(showTable ? Math.abs(totalReturn) : 0, 2)}</CMValue>
          </div>
        </Flex>
      )
    }

    return (
      <>
        {!showTable && currentOrder.wholesaleOrderStatus !== 'CANCEL' ? (
          <Flex className="space-between">
            <ThemeButton onClick={this.changeTableStatus}>Create credit memo</ThemeButton>
            {showTable && <Totals />}
          </Flex>
        ) : (
          ''
        )}

        {showTable ? (
          <div style={{ paddingRight: 20 }}>
            {currentOrder && (
              <Flex className="space-between" style={{ marginBottom: 20 }}>
                <div style={{ fontSize: 16,color: gray01 }}>
                  {/*Credit Memo No. <span style={{ color: gray01 }}>#{currentOrder.wholesaleOrderId}C</span>*/}
                  Credit Memo No. <span>#{currentOrder.wholesaleOrderId}C</span>
                </div>
                {<Totals />}
              </Flex>
            )}

            <CartTableWrapper>
              <StyleTable
                pagination={false}
                rowClassName={() => 'row'}
                columns={columns}
                dataSource={dataSource}
                handleSave={this.handleSave}
                rowKey="id"
                scroll={{ x: 1700 }}
              />
              {!readOnly && currentOrder.wholesaleOrderStatus !== 'CANCEL' && (
                <ThemeButton
                  onClick={() => this.setState({ selectShippedProductModalVisible: true })}
                  style={{ marginTop: 24 }}
                >
                  Add Item
                </ThemeButton>
              )}
            </CartTableWrapper>
          </div>
        ) : null}
        {selectShippedProductModalVisible && (
          <SelectShippedProductModal
            orderId={orderId}
            adjustmentList={this.state.dataSource}
            sellerSetting={this.props.sellerSetting}
            onCancel={() => this.setState({ selectShippedProductModalVisible: false })}
            createOrderAdjustment={(row: any) =>
              this.props.createOrderAdjustment({
                orderId: this.props.orderId,
                requestData: this._getAdjustmentObject(row),
              })
            }
          />
        )}
      </>
    )
  }
}

const StyleTable = styled(Table)({
  '.row:hover .delete-btn': {
    display: 'block',
  },

  '.delete-btn': {
    display: 'none',
  },
})

const mapStateToProps = (state: GlobalState) => state.orders
export default connect(OrdersModule)(mapStateToProps)(MemoTotalTable)
