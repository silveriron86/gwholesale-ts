import styled from '@emotion/styled'

export const Marker = styled('div')((props) => ({
  fontSize: 13,
  fontWeight: 'bold',
  color: props.theme.primary,
  border: `1px solid ${props.theme.primary}`,
  borderRadius: 5,
  padding: '13px 10px 10px',
  width: 'fit-content',
  marginTop: -8,
}))

export const MarkerText = styled('div')((props) => ({
  fontSize: 25,
  fontWeight: 'bolder',
  color: props.theme.main,
  // marginLeft: 40,
  float: 'right',
}))
