import React from 'react'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import SalesOverview from '../widgets/sales-overview'
import { MarginsContainer } from './margins-container'
import { OrderItem } from '~/schema'
import { RouteComponentProps } from 'react-router'

type SalesMarginsProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
    onlyContent?: boolean
  }

export class SalesMargins extends React.PureComponent<SalesMarginsProps> {
  handleUpdateItem = (item: OrderItem) => {
    // tslint:disable-next-line:no-console
    console.log('--- update order item ---', item)
    this.props.updateOrderItem(item)
  }

  render() {
    const { currentOrder, orderItems, onlyContent, orderItemsImpacts } = this.props
    if (onlyContent) {
      if (!currentOrder) {
        return null;
      }
      return (
        <>
          <SalesOverview onlyContent={onlyContent} orderId={this.props.match.params.orderId} order={currentOrder} />
          <MarginsContainer
            onlyContent={onlyContent}
            orderItems={orderItems}
            orderItemsImpacts={orderItemsImpacts}
            currentOrder={currentOrder}
            onUpdateItem={this.handleUpdateItem} />
        </>
      )
    }

    if (!currentOrder) {
      return <PageLayout currentTopMenu={'menu-Customers-Sales Orders'} />
    }

    return (
      <PageLayout currentTopMenu={'menu-Customers-Sales Orders'}>
        <SalesOverview orderId={this.props.match.params.orderId} order={currentOrder} />
        <MarginsContainer orderItems={orderItems} onUpdateItem={this.handleUpdateItem} />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(SalesMargins))
