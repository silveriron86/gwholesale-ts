import React from 'react'
import { Button, Col, Icon, Row, Tooltip } from 'antd'
import {
  ml0,
  ThemeIconButton,
  ThemeModal,
  textCenter,
  CustomerDetailsTitle as DetailsTitle,
  floatLeft,
  ThemeIcon,
} from '~/modules/customers/customers.style'
import { basePriceToRatioPrice, formatNumber } from '~/common/utils'
import { cloneDeep } from 'lodash'
import EditableTable from '~/components/Table/editable-table'
import { PureWrapper as Wrapper } from '~/modules/customers/accounts/addresses/addresses.style'
import { OrderItem } from '~/schema'
import PriceModal from '../cart/modals/price-modal'
import LotsTable from '../cart/tables/lots-table'
import { Marker, MarkerText } from '~/modules/customers/sales/margins/_styles'
import { SalesContainer } from '~/modules/customers/sales/_style'
import { title } from '~/modules/inventory/components/RelatedOrderTable/style'

interface MarginsTotalTableProps {
  editable: boolean
  orderItems: OrderItem[]
  handleSave: Function
  onlyContent?: boolean
  orderItemsImpacts: any[]
  currentOrder: OrderDetail
}

export class MarginsTotalTable extends React.PureComponent<MarginsTotalTableProps> {
  state: any
  constructor(props: MarginsTotalTableProps) {
    super(props)
    this.state = {
      dataSource: [],
      totalSales: 0,
      totalGrossMargin: 0,
      totalProfit: 0,
      visibleLotsModal: false,
      selectedItem: null,
    }
  }

  componentDidMount(): void {
    this.getTotalSales(cloneDeep(this.props.orderItems), this.props.orderItemsImpacts)
  }

  componentWillReceiveProps(nextProps: MarginsTotalTableProps) {
    if (nextProps.orderItems.length > 0 || nextProps.orderItemsImpacts.length > 0) {
      this.getTotalSales(nextProps.orderItems, nextProps.orderItemsImpacts)
    }
  }

  handleSave = (row: OrderItem) => {
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.itemId === item.itemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
    this.props.handleSave(row)
  }

  handleSelectProduct = (record: OrderItem, item: any) => {
    // tslint:disable-next-line:no-console
    console.log(record, item)
    record.itemId = item.itemId
    this.props.handleSave(record)
  }

  openPriceModal = () => {
    this.setState({
      visiblePriceModal: true,
    })
  }

  handleModalOk = () => {
    this.setState({
      visiblePriceModal: false,
    })
  }

  handleSelectLot = (record: any) => {
    let row = { ...this.state.selectedItem }
    if (record) {
      row.lotId = record.lotId
    } else {
      row.lotId = null
    }
    row.quantity = row.quantity ? row.quantity : 0
    this.handleSave(row)
    this.onToggleLotsModal(null)
  }

  onToggleLotsModal = (record: OrderItem | null) => {
    this.setState({
      visibleLotsModal: !this.state.visibleLotsModal,
      selectedItem: record ? record : null,
    })
  }

  getTotalSales = (records: any, orderItemsImpacts: any[]) => {
    let _totalSales = 0
    let _totalProfit = 0
    for (let i = 0; i < records.length; i++) {
      if (records[i].cost > 0) {
        let quantity = 0

        // if (records[i].constantRatio) {
        //   if (records[i].status == 'NEW' || records[i].status == 'PLACED') {
        //     quantity = records[i].quantity
        //   } else {
        //     quantity = records[i].picked
        //   }
        // } else {
        quantity = records[i].catchWeightQty
        // }

        const _revenue = quantity * records[i].price
        const _cost = records[i].cost * quantity
        const _profit = _revenue - _cost
        const _markup = (_profit / _cost) * 100
        const _gross_margin = (_profit / _revenue) * 100

        records[i]['revenue'] = '$' + formatNumber(_revenue, 2)
        records[i]['total_cost'] = formatNumber(_cost, 2)
        records[i]['markup'] = formatNumber(isNaN(_markup) ? 0 : _markup, 2) + '%'
        records[i]['gross_margin'] = formatNumber(isNaN(_gross_margin) ? 0 : _gross_margin, 2) + '%'
        records[i]['profit'] = formatNumber(_profit, 2)
        _totalProfit += _profit
        _totalSales += _revenue
      } else {
        records[i]['revenue'] = '0'
        records[i]['total_cost'] = '0'
        records[i]['markup'] = '0'
        records[i]['gross_margin'] = '0'
        records[i]['profit'] = '0'
      }
    }

    let creditData: any[] = []
    for (let i = 0; i < orderItemsImpacts.length; i++) {
      const row = orderItemsImpacts[i]
      if (!row.orderItem) continue;
      const unitCost = basePriceToRatioPrice(row.orderItem.pricingUOM, row.orderItem.cost, row.orderItem, 2)
      const totalRevenue = row.returnInInventory === true ? -(parseFloat(row.orderItem.price) * parseFloat(row.quantity)) : (parseFloat(row.orderItem.price) * parseFloat(row.quantity))
      const totalCost = row.returnInInventory === true ? -(unitCost * parseFloat(row.quantity)) : (unitCost * parseFloat(row.quantity))
      creditData.push({
        picked: row.quantity,
        UOM: row.uom,
        pricingUOM: row.orderItem.pricingUOM ? row.orderItem.pricingUOM : row.orderItem.uom,
        variety: row.returnInInventory === true ? `Credit(returned to invetory)-${row.item.variety}` : row.item.variety,
        lotId: row.orderItem.lotId ? row.orderItem.lotId : '',
        cost: row.orderItem.cost,
        price: row.orderItem.price,
        extended: totalRevenue ? totalRevenue : 0,//revenue
        total_cost: totalCost ? totalCost : 0,
        markup: '',
        gross_margin: '',
        orderItem: row.orderItem,
        isCredit: true
      })
      _totalProfit += totalRevenue - totalCost
      _totalSales += totalRevenue
    }

    this.setState({
      totalGrossMargin: formatNumber((_totalProfit / _totalSales) * 100, 2),
      totalProfit: _totalProfit,
      totalSales: _totalSales,
      dataSource: [...records, ...creditData],
    })
  }

  render() {
    const { editable, onlyContent } = this.props
    const {
      dataSource,
      totalGrossMargin,
      visiblePriceModal,
      visibleLotsModal,
      selectedItem,
      totalSales,
      totalProfit,
    } = this.state
    console.log('datasource', dataSource)
    const columns: Array<any> = [
      {
        title: 'Sales Order',
        children: [
          {
            title: '#',
            dataIndex: 'wholesaleOrderItemId',
            key: 'wholesaleOrderItemId',
            align: 'center',
            render: (id: number, record: OrderItem, index: number) => {
              return index + 1
            },
          },
          {
            title: 'UNIT',
            dataIndex: 'picked',
            key: 'unit',
            className: 'th-left',
            // editable: true,
            // edit_width: 50,
          },
          {
            title: 'UOM',
            dataIndex: 'UOM',
            key: 'UOM',
            className: 'th-left',
            // editable: true,
            // edit_width: 80,
          },
          {
            title: 'PRODUCT NAME',
            dataIndex: 'variety',
            className: 'th-left',
            key: 'variety',
            // editable: true,
            // type: 'modal',
            render: (data: string, record: any) =>
              record.isManufactureItem == null ? (
                <div>{data}</div>
              ) : (
                <div>
                  {data}
                  <Tooltip title="This is a manufacture item, the Cost is based on the average of all previous purchase order item">
                    <ThemeIcon type="question-circle" style={{ marginLeft: '5px' }} />
                  </Tooltip>
                </div>
              ),
          },
          {
            title: 'LOT',
            dataIndex: 'lotId',
            key: 'lot',
            // editable: true,
            // type: 'modal',
            className: 'th-left',
            render: (data: any, record: any) => data,
          },
          {
            title: 'UNIT COST',
            dataIndex: 'cost',
            key: 'cost',
            align: 'right',
            className: 'p9',
            // editable: true,
            // edit_width: 50,
            render: (data: number, record: any) => {
              const orderItem = record.isCredit === true ? record.orderItem : record
              const ratioPrice = basePriceToRatioPrice(record.pricingUOM, data, orderItem, 2)
              return ratioPrice == 0 ? `$0.00` : `$${formatNumber(ratioPrice, 2)}`
            },
          },
          {
            title: 'UNIT PRICE',
            dataIndex: 'price',
            key: 'price',
            align: 'right',
            className: 'p9',
            width: 140,
            render: (data: number, record: any) => {
              const ratioPrice = basePriceToRatioPrice(record.pricingUOM, data, record, 2)
              return ratioPrice == 0 ? `$0.00` : `$${formatNumber(ratioPrice, 2)}`
            },
          },
        ],
      },
      {
        title: 'Margins',
        children: [
          {
            title: 'COST',
            dataIndex: 'total_cost',
            className: 'th-left',
            width: 90,
            render: (value) => {
              return value ? `${value < 0 ? '-' : ''}$${formatNumber(Math.abs(value), 2)}` : `$0.00`
            }
          },
          {
            title: 'REVENUE',
            dataIndex: 'extended',

            width: 100,
            render: (data: number, record: any) => {
              let total = 0
              if (record.isCredit === true) {
                total = data
              } else {
                const extended = record.price + record.freight
                total = extended * record.catchWeightQty

              }
              const prefix = total >= 0 ? '' : '-'
              return (
                <div style={textCenter}>
                  {prefix}${total != 0 ? formatNumber(Math.abs(total), 2) : '0.00'}
                </div>
              )
            },
          },
          {
            title: 'MARKUP %',
            dataIndex: 'markup',
            className: 'th-left',
            width: 110,
          },
          {
            title: 'MARGIN',
            dataIndex: 'gross_margin',
            className: 'th-left',
            width: 130,
          },

        ],
      },
    ]

    return (
      <Wrapper style={ml0}>
        <Row style={{ marginBottom: 15 }}>
          {onlyContent !== true && (
            <Col md={2}>
              <DetailsTitle style={floatLeft}>Margins</DetailsTitle>
            </Col>
          )}
          <Col xl={6}>
            <Marker>
              MARGIN <MarkerText style={{ marginTop: -11, marginLeft: 5 }}>{totalGrossMargin}%</MarkerText>
            </Marker>
          </Col>
          <Col xl={6} style={{ marginLeft: '10px' }}>
            <Marker>
              PROFIT
              <MarkerText style={{ marginTop: -11, marginLeft: 5 }}>${formatNumber(totalProfit, 2)}</MarkerText>
            </Marker>
          </Col>
        </Row>
        <Wrapper style={ml0}>
          <ThemeModal
            title="PRICE"
            keyboard={true}
            visible={visiblePriceModal}
            onOk={this.handleModalOk}
            onCancel={this.handleModalOk}
            okText="Close [esc]"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={800}
          >
            <PriceModal />
          </ThemeModal>

          <ThemeModal
            title={'Lots'}
            keyboard={true}
            visible={visibleLotsModal}
            onOk={this.onToggleLotsModal.bind(this, null)}
            onCancel={this.onToggleLotsModal.bind(this, null)}
            okText="Close [esc]"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={900}
          >
            <LotsTable
              itemId={selectedItem ? selectedItem.itemId : 0}
              lotId={selectedItem ? selectedItem.lotId : 0}
              onSelect={this.handleSelectLot}
            />
          </ThemeModal>
          <EditableTable
            className="nested-head no-shadow"
            columns={columns}
            dataSource={dataSource}
            handleSave={this.handleSave}
            handleSelectProduct={this.handleSelectProduct}
            rowKey="itemId"
          />
        </Wrapper>
      </Wrapper>
    )
  }
}

export default MarginsTotalTable
