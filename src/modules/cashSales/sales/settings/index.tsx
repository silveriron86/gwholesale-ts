import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import { RouteComponentProps } from 'react-router'

type SalesSettingsProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

export class SalesSettings extends React.PureComponent<SalesSettingsProps> {
  render() {
    return <PageLayout currentTopMenu={'menu-Customers-Sales Orders'}/>
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(SalesSettings))
