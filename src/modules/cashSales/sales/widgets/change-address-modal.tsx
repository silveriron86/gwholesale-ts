import * as React from 'react'
import { PopoverWrapper } from '~/modules/customers/customers.style'
import {Table } from 'antd'


export interface ChangeAddressModalProps {
  columns:any
  dataSource:any[]
  onClick:Function
}

class ChangeAddressModal extends React.PureComponent<ChangeAddressModalProps> {


  render() {
    const { columns,dataSource,onClick} = this.props;

    return (
      <PopoverWrapper style={{ width: 400 }}>
        <Table columns={columns}
               dataSource={dataSource}
               pagination={false}
               scroll={{ y: 240 }}
               onRow={record => { return {onClick: event => {onClick(record,event)}}}}/>
      </PopoverWrapper>
    )
  }
}

export default ChangeAddressModal
