import React from 'react'
import {
  pd20,
  InputLabel,
  DetailsRow,
  ThemeInput,
  ThemeTextArea,
  ThemeSelect,
  // ThemeCheckbox,
  ThemeButton,
  ThemeOutlineButton,
} from '../../customers.style'
import { Layout, Row, Col, Select, Icon, Form, Popconfirm, Tooltip } from 'antd'
import { layoutStyle } from '../../components/customers-detail/customers-detail.style'
import { TempCustomer } from '~/schema'
import { FormComponentProps } from 'antd/lib/form'
import {
  onLoadFocusOnFirst,
  onPopoverOpenWithButtons,
  documentBodyKeyupEvent,
  onPopoverOpenWithSecondButton,
} from '~/common/jqueryHelper'
import { CACHED_NS_LINKED } from '~/common'
// import { FINANCIAL_TERMS } from '~/common'
const { Option } = Select

interface FormFields {
  name: string
  type: string
  mobilePhone: string
}

type DetailsFormProps = FormComponentProps<FormFields> & {
  customer: TempCustomer
  onSave: Function
  companyProductTypes: any
  sellerSetting: any
}

export class DetailsFormComp extends React.PureComponent<DetailsFormProps> {
  componentDidMount() {
    onLoadFocusOnFirst('simple-input-wrapper')

    onPopoverOpenWithSecondButton('popover-with-buttons')

    documentBodyKeyupEvent('popover-with-buttons')
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.onSave(values)
      }
    })
  }

  firstUpperCase(text: any) {
    if (!text) return ''
    let lower = text.toLowerCase()
    return lower.charAt(0).toUpperCase() + lower.slice(1)
  }

  render() {
    const {
      customer,
      companyProductTypes,
      currentCompanyUsers,
      sellerSetting,
      form: { getFieldDecorator },
    } = this.props
    const type = location.href.indexOf('vendor') >= 0 ? 'Vendor' : 'Account'
    const businessTypes =
      type == 'Account'
        ? (companyProductTypes != null ? companyProductTypes.customerTypes : [])
        : (companyProductTypes != null ? companyProductTypes.vendorTypes : [])
    const enabledNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
        <Layout style={layoutStyle} className="simple-input-wrapper">
          <Row style={DetailsRow}>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>{type.toUpperCase()} NAME</InputLabel>
              <Form.Item>
                {getFieldDecorator('name', {
                  rules: [{ required: true, message: 'Account Name is required!' }],
                  initialValue: customer.clientCompany ? customer.clientCompany.companyName : '',
                })(<ThemeInput />)}
              </Form.Item>
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>STATUS</InputLabel>
              <Form.Item>
                {getFieldDecorator('status', {
                  initialValue: customer.status,
                })(
                  <ThemeSelect style={{ width: '100%' }}>
                    {['Active', 'Inactive'].map((item) => (
                      <Option key={item}>{item}</Option>
                    ))}
                  </ThemeSelect>,
                )}
              </Form.Item>
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>BUSINESS TYPE</InputLabel>
              <Form.Item>
                {getFieldDecorator('businessType', {
                  initialValue: customer.businessType,
                })(
                  <ThemeSelect style={{ width: '100%' }}>
                    {businessTypes.map((item: { name: {} | null | undefined }, index: string | number | undefined) => (
                      <Option key={`business-type-${index}`} value={item.name}>
                        {item.name}
                      </Option>
                    ))}
                  </ThemeSelect>,
                )}
              </Form.Item>
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>MAIN PHONE</InputLabel>
              <Form.Item>
                {getFieldDecorator('mobilePhone', {
                  // rules: [
                  //   {
                  //     pattern: /(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})/,
                  //     message: 'Invalid international phone number!'
                  //   },
                  //   {
                  //     required: true,
                  //     message: 'Please fill out the main phone!'
                  //   }
                  // ],
                  initialValue: customer.mobilePhone,
                })(<ThemeInput />)}
              </Form.Item>
            </Col>
          </Row>

          <Row style={DetailsRow}>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>ALTERNATIVE PHONE</InputLabel>
              <Form.Item>
                {getFieldDecorator('alternativePhone', {
                  rules: [{ required: false }],
                  initialValue: customer.alternativePhone,
                })(<ThemeInput />)}
              </Form.Item>
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>FAX</InputLabel>
              <Form.Item>
                {getFieldDecorator('fax', {
                  rules: [{ required: false }],
                  initialValue: customer.fax,
                })(<ThemeInput />)}
              </Form.Item>
            </Col>
            {(!sellerSetting || (sellerSetting && sellerSetting.company && sellerSetting.company.companyName != 'Jana Food Services')) &&
              <Col lg={6} md={12} style={pd20}>
                <InputLabel>EMAIL</InputLabel>
                <Form.Item>
                  {getFieldDecorator('email', {
                    rules: [
                      //   {
                      //     type: 'email',
                      //     message: 'Email is not valid!'
                      //   },
                      {
                        required: true,
                        message: 'Email is required!',
                      },
                    ],
                    initialValue: customer.email,
                  })(<ThemeInput />)}
                </Form.Item>
              </Col>
            }
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>WEBSITE</InputLabel>
              <Form.Item>
                {getFieldDecorator('website', {
                  rules: [{ required: false }],
                  initialValue: customer.website,
                })(<ThemeInput />)}
              </Form.Item>
            </Col>
          </Row>

          <Row style={DetailsRow}>
            {type === 'Account' && currentCompanyUsers && currentCompanyUsers.length > 0 && (
              <Col lg={6} md={12} style={pd20}>
                <InputLabel>ACCOUNTING REPRESENTATIVE</InputLabel>
                <Form.Item>
                  {getFieldDecorator('accountant', {
                    rules: [{ required: false }],
                    initialValue: customer.accountant ? customer.accountant.userId : null,
                  })(
                    <ThemeSelect style={{ width: '100%' }}>
                      {currentCompanyUsers.map((item: any, index: number) => {
                          return (
                            <Select.Option key={index} value={item.userId}>
                              {item.firstName + '  ' + item.lastName}
                            </Select.Option>
                          )
                        })}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            )}
            {type === 'Account' && currentCompanyUsers && currentCompanyUsers.length > 0 && (
              <>
                <Col lg={6} md={12} style={pd20}>
                  <InputLabel>
                  SALES REPRESENTATIVE
                    <Tooltip title="If a sales representative is assigned, then any sales order created for this customer will be assigned to the assigned sales representative.
                    If no sales representative is assigned, then any sales order created for this customer will be assigned to the user who created the sales order.">
                        <Icon type="info-circle" className="info-icon" style={{ margin: '0 4px' }} />
                      </Tooltip>
                  </InputLabel>
                  <Form.Item>
                    {getFieldDecorator('seller', {
                      rules: [{ required: false }],
                      initialValue: customer.seller ? customer.seller.userId : 0,
                    })(
                      <ThemeSelect style={{ width: '100%' }}>
                        <Select.Option key={0} value={0}>
                          No assigned sales representative
                        </Select.Option>
                        {currentCompanyUsers.filter(v => (v.role !== 'SUPERADMIN' && v.role !== 'WAREHOUSE')).map((item: any, index: number) => {
                            return (
                              <Select.Option key={index} value={item.userId}>
                                {item.firstName + '  ' + item.lastName}
                              </Select.Option>
                            )
                          })}
                      </ThemeSelect>,
                    )}
                  </Form.Item>
                </Col>
                <Col lg={6} md={12} style={pd20}>
                  <InputLabel>
                    CUSTOMER ABBREVIATION
                    <Tooltip title="Specify a 1-5 letter prefix unique to this customer for use an printable documents like
                    pickup reference number.">
                      <Icon type="info-circle" style={{ marginLeft: 8 }} />
                    </Tooltip>
                  </InputLabel>
                  <Form.Item>
                    {getFieldDecorator('abbreviation', {
                      rules: [{ required: false, pattern: new RegExp(/^[a-zA-Z0-9]{0,5}$/), message: "Up to 5 alphanumeric characters only" }],
                      initialValue: customer.abbreviation ? customer.abbreviation : null,
                    })(<ThemeInput />)}
                  </Form.Item>
                </Col>
                {/* <Col lg={6} md={12} style={pd20}>
                  <InputLabel>PAYMENT TERMS</InputLabel>
                  <Form.Item>
                    {getFieldDecorator('paymentTerm', {
                      rules: [{ required: false }],
                      initialValue: customer.paymentTerm
                    })(
                      <ThemeSelect
                        style={{ width: '100%' }}
                        placeholder="Payment Terms"
                      >
                        {FINANCIAL_TERMS.map((item) => (
                          <Option key={`customer-term-${item}`} value={item}>
                            {item}
                          </Option>
                        ))}
                      </ThemeSelect>
                    )}
                  </Form.Item>
                </Col> */}
              </>
            )}
            {type === 'Vendor' && (
              <Col lg={6} md={12} style={pd20}>
                <InputLabel>ACCOUNTING REPRESENTATIVE</InputLabel>
                <Form.Item>
                  {getFieldDecorator('accountant', {
                    rules: [{ required: false }],
                    initialValue: customer.accountant ? customer.accountant.userId : null,
                  })(
                    <ThemeSelect style={{ width: '100%' }}>
                      {currentCompanyUsers &&
                        currentCompanyUsers.filter(v => (v.role !== 'SUPERADMIN' && v.role !== 'WAREHOUSE')).map((item: any, index: number) => {
                          return (
                            <Select.Option key={index} value={item.userId}>
                              {item.firstName + '  ' + item.lastName}
                            </Select.Option>
                          )
                        })}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            )}
            {type === 'Vendor' && (
              <Col lg={6} md={12} style={pd20}>
                <InputLabel>PURCHASER</InputLabel>
                <Form.Item>
                  {getFieldDecorator('purchaser', {
                    rules: [{ required: false }],
                    initialValue: customer.purchaser ? customer.purchaser.userId : null,
                  })(
                    <ThemeSelect style={{ width: '100%' }}>
                      {currentCompanyUsers &&
                        currentCompanyUsers.filter(v => (v.role !== 'SUPERADMIN' && v.role !== 'WAREHOUSE')).map((item: any, index: number) => {
                          return (
                            <Select.Option key={index} value={item.userId}>
                              {item.firstName + '  ' + item.lastName}
                            </Select.Option>
                          )
                        })}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            )}
          </Row>

          <Row style={DetailsRow}>
            <Col lg={8} md={12} style={pd20}>
              <InputLabel>DBA NAME</InputLabel>
              <Form.Item>
                {getFieldDecorator('dba', {
                  rules: [{ required: false }],
                  initialValue: customer.dba,
                })(
                  <ThemeTextArea
                    rows={4}
                    placeholder={`you can edit DBA name in ${this.firstUpperCase(
                      customer.type,
                    )} Accounts -> Account details`}
                  />,
                )}
              </Form.Item>
            </Col>
            <Col lg={8} md={12} style={pd20}>
              <InputLabel>NOTES</InputLabel>
              <Form.Item>
                {getFieldDecorator('notes', {
                  rules: [{ required: false }],
                  initialValue: customer.notes,
                })(
                  <ThemeTextArea
                    rows={4}
                    placeholder={`you can edit notes in ${this.firstUpperCase(
                      customer.type,
                    )} Accounts -> Account details`}
                  />,
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              {/* <Popconfirm
                title="Are you sure to save?"
                onConfirm={this.handleSubmit}
                okText="Save"
                cancelText="Cancel"
              > */}
              <ThemeOutlineButton
                htmlType="submit"
                shape="round"
                style={{ width: 200, marginBottom: 20 }}
                className="popover-with-buttons"
              >
                <Icon type="save" theme="filled" />
                Save Account Details
              </ThemeOutlineButton>
              {/* </Popconfirm> */}
            </Col>
          </Row>
        </Layout>
      </Form>
    )
  }
}

const DetailsForm = Form.create()(DetailsFormComp)
export default DetailsForm
