import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { CustomersInfoBody, CustomersInfoHeader } from '../../components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import DetailsForm from './details-form'
import { DetailsOverview } from '../../components/details-overview'
import { CustomersModule } from '../../customers.module'
import _ from 'lodash'
import { DetailsTitle, DetailsWrapper } from '../../customers.style'
import { HeaderActions } from '~/modules/customers/accounts/header-actions/header-actions'
import { CustomerBaseComponent } from '~/modules/customers/accounts/base-component/CustomerBaseComponent'

export class AccountDetails extends CustomerBaseComponent {
  state = {
    companyProductTypes: null,
  }

  componentDidMount() {
    const customerId = this.props.match.params.customerId
    this.props.getCustomer(customerId)
    this.props.getCompanyUsers()
    this.props.getCompanyProductAllTypes()
    this.setState({ companyProductTypes: this.props.companyProductTypes })
    if (_.isEmpty(this.props.sellerSetting)) {
      this.props.getSellerSetting()
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.companyProductTypes) != JSON.stringify(nextProps.companyProductTypes)) {
      this.setState({ companyProductTypes: nextProps.companyProductTypes })
    }
  }

  onSave = (data: any) => {
    // tslint:disable-next-line:no-console
    this.props.updateCurrentCustomer(data)
  }

  render() {
    const { customer } = this.props
    const { companyProductTypes } = this.state
    return (
      <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions
          onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
          onCreateBlankOrder={this.onCreateBlankOrder}
          duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}
        />
        <CustomersInfoHeader>
          <DetailsOverview customer={customer} />
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <DetailsWrapper>
            <DetailsTitle>Account Details</DetailsTitle>
            {!_.isEmpty(customer) && (
              <DetailsForm
                customer={customer}
                companyProductTypes={companyProductTypes}
                onSave={this.onSave}
                {...this.props}
              />
            )}
            {/*{!_.isEmpty(customer) && <DetailsForm customer={customer} onSave={this.onSave}  {...this.props}/>}*/}
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.customers
export default withTheme(connect(CustomersModule)(mapState)(AccountDetails))
