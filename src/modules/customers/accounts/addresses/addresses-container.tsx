import * as React from 'react'
import { ThemeButton, DetailsWrapper, DetailsTitle, floatRight, floatLeft, Clear, ThemeSpin } from '../../customers.style'
import { Layout, Col, Row, Icon, Popover } from 'antd'
import { layoutStyle } from '../../components/customers-detail/customers-detail.style'
import { CustomButton } from '~/components'
import { Wrapper } from './addresses.style'
import BillingModal from './modals/billing-modal'
import ShippingModal from './modals/shipping-modal'
import BillingTable from './tables/billing-table'
import ShippingTable from './tables/shipping-table'
import { MainAddress, WholesaleRoute } from '~/schema'
import { onPopoverOpenWithButtons, documentBodyKeyupEvent, Level2WithTable } from '~/common/jqueryHelper'

interface AddressesContainerProps {
  billingAddresses: MainAddress[]
  shippingAddresses: MainAddress[]
  routes: WholesaleRoute[]
  from: string
  onSave: Function
  loadingPaymentAddresses: boolean
  loadingDeliveryAddresses: boolean
  startLoadingPaymentAddress: Function
  startLoadingDeliveryAddress: Function
}

export class AddressesContainer extends React.PureComponent<AddressesContainerProps> {
  state: any
  constructor(props: AddressesContainerProps) {
    super(props)
    this.state = {
      newModalVisible: false,
      updateModalVisible: [],
      newShippingModalVisible: false,
      updateShippingModalVisible: [],
      currentIndex: 0,
    }
  }

  componentDidMount() {
    onPopoverOpenWithButtons('popover-with-buttons-billing')
    documentBodyKeyupEvent('popover-with-buttons-billing')
    onPopoverOpenWithButtons('popover-with-buttons-delivery')
    Level2WithTable('level-2-with-table')
  }

  getAddress = (item: any) => {
    return `${item.street} ${item.city} ${item.state}, ${item.zip_code}`
  }

  getBillingModal = (address: MainAddress | null, index: number) => {
    const { newModalVisible, updateModalVisible } = this.state
    const { billingAddresses } = this.props
    const visible = index === -1 ? newModalVisible : updateModalVisible[index]
    return <BillingModal data={address} save={this.save} visible={visible} isFirst={billingAddresses.length === 0} />
  }

  handleVisibleChange = (item: MainAddress | null, index: number, visible: boolean) => {
    if (item) {
      const visibles: Array<boolean> = [...this.state.updateModalVisible]
      visibles[index] = visible
      this.setState({
        currentIndex: index,
        updateModalVisible: visibles,
      })
    } else {
      this.setState({
        newModalVisible: visible,
      })
    }
  }

  hideModal = (billingAddress: MainAddress | null) => {
    if (billingAddress.wholesaleAddressId) {
      const { currentIndex, updateModalVisible } = this.state
      const visibles: Array<boolean> = [...updateModalVisible]
      visibles[currentIndex] = false
      this.setState({
        updateModalVisible: visibles,
      })
    } else {
      this.setState({
        newModalVisible: false,
      })
    }
  }

  saveAsDefault = (address: MainAddress | null) => {
    address['addressType'] = 'BILLING'
    this.props.onSave(address)
    this.hideModal(address)
    //todo
    // this.props.setMainContact(contact.contactId)
  }

  save = (address: MainAddress | null) => {
    address['addressType'] = 'BILLING'
    this.props.startLoadingPaymentAddress();
    this.props.onSave(address)
    this.hideModal(address)
  }

  handleShippingVisibleChange = (item: MainAddress | null, index: number, visible: boolean) => {
    if (item) {
      const visibles: Array<boolean> = [...this.state.updateShippingModalVisible]
      visibles[index] = visible
      this.setState({
        currentIndex: index,
        updateShippingModalVisible: visibles,
      })
    } else {
      this.setState({
        newShippingModalVisible: visible,
      })
    }
  }

  getShippingModal = (address: MainAddress | null, index: number) => {
    const { newShippingModalVisible, updateShippingModalVisible } = this.state
    const { from, shippingAddresses } = this.props
    const visible = index === -1 ? newShippingModalVisible : updateShippingModalVisible[index]
    return (
      <ShippingModal data={address} visible={visible} routes={this.props.routes} save={this.saveShipping} from={from}  isFirst={shippingAddresses.length === 0} />
    )
  }

  hideShippingModal = (address: MainAddress | null) => {
    if (address.wholesaleAddressId) {
      const { currentIndex, updateShippingModalVisible } = this.state
      const visibles: Array<boolean> = [...updateShippingModalVisible]
      visibles[currentIndex] = false
      this.setState({
        updateShippingModalVisible: visibles,
      })
    } else {
      this.setState({
        newShippingModalVisible: false,
      })
    }
  }

  saveShipping = (address: MainAddress | null) => {
    address['addressType'] = 'SHIPPING'
    this.props.startLoadingDeliveryAddress();
    this.props.onSave(address)
    this.hideShippingModal(address)
  }
  render() {
    const {
      shippingAddresses,
      billingAddresses,
      from,
      loadingPaymentAddresses,
      loadingDeliveryAddresses,
    } = this.props
    const { newModalVisible, updateModalVisible, newShippingModalVisible, updateShippingModalVisible } = this.state
    const isVendor = from === 'purchase'

    return (
      <DetailsWrapper className="level-2-with-table">
        <Layout style={layoutStyle}>
          <Row>
            <Col md={8}>
              <Wrapper>
                <DetailsTitle style={{ padding: '4px 0' }}>Address</DetailsTitle>
                <Popover
                  placement="left"
                  content={this.getBillingModal(null, -1)}
                  trigger="click"
                  visible={newModalVisible}
                  onVisibleChange={this.handleVisibleChange.bind(this, null, 0)}
                >
                  <ThemeButton shape="round" className="popover-with-buttons-billing">
                    <Icon type="plus-circle" theme="filled" />
                    New Payment Address
                  </ThemeButton>
                </Popover>
              </Wrapper>

              <ThemeSpin spinning={loadingPaymentAddresses}>
                <BillingTable
                  listItems={billingAddresses}
                  getModal={this.getBillingModal}
                  updateModalVisible={updateModalVisible}
                  handleVisibleChange={this.handleVisibleChange}
                />
              </ThemeSpin>
            </Col>
            <Col md={16}>
              <Wrapper>
                <DetailsTitle>&nbsp;</DetailsTitle>
                <Popover
                  placement="left"
                  content={this.getShippingModal(null, -1)}
                  trigger="click"
                  visible={newShippingModalVisible}
                  onVisibleChange={this.handleShippingVisibleChange.bind(this, null, 0)}
                >
                  <ThemeButton shape="round" className="popover-with-buttons-delivery">
                    <Icon type="plus-circle" theme="filled" />
                    New {isVendor ? 'Physical' : 'Delivery'} Address
                  </ThemeButton>
                </Popover>
              </Wrapper>
              <ThemeSpin spinning={loadingDeliveryAddresses}>
                <ShippingTable
                  listItems={shippingAddresses}
                  getModal={this.getShippingModal}
                  updateModalVisible={updateShippingModalVisible}
                  handleVisibleChange={this.handleShippingVisibleChange}
                  isVendor={isVendor}
                />
              </ThemeSpin>
            </Col>
          </Row>
        </Layout>
      </DetailsWrapper>
    )
  }
}

export default AddressesContainer
