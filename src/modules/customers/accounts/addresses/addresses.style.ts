import styled from '@emotion/styled'
import { mediumGrey, lightGrey, white } from '~/common/color'

export const Wrapper = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  margin: '10px',
  height: 36,
  '& .ant-btn': {
    height: 36,
  },
})

export const PureWrapper = styled('div')({
  margin: '0 10px',
})

interface disabledProps {
  disabled?: boolean
}
export const WrapperTitle = styled('div')<disabledProps>((props) => ({
  width: '100%',
  backgroundColor: props.disabled ? lightGrey : props.theme.lighter,
  textAlign: 'center',
  fontSize: 18,
  fontWeight: 'bold',
  color: props.disabled ? mediumGrey : props.theme.main,
  padding: 10,
}))

export const WrapperSubTitle = styled('div')((props) => ({
  width: '100%',
  backgroundColor: props.theme.lighter,
  textAlign: 'left',
  fontSize: 12,
  fontWeight: 'bold',
  color: mediumGrey,
  padding: '12px 0',
}))

export const tableDivider: React.CSSProperties = {
  borderLeft: `1px solid ${lightGrey}`,
  minHeight: 450,
  paddingLeft: 10,
  marginLeft: 0,
}

export const OneOffItemTableWrapper = styled('div')({
  '& .extra-charge': {
    'table': {
      border: 'none',
      'thead th': {
        padding: 12
      },
      'tbody td': {
        padding: 12
      }
    },
    'table thead': {
      boxShadow: 'none !important'
    },
    '.ant-table-content .ant-table-footer': {
      border: '1px solid #D8DBDB',
      backgroundColor: white,
      padding: 0,
    },
    'table thead tr:first-of-type th': {
      backgroundColor: '#F7F7F7 !important',
    },
    'table thead th': {
      borderLeft: '1px solid #D8DBDB',
    },
    'table tbody td': {
      borderLeft: '1px solid #D8DBDB'
    },
    'table thead th:last-of-type': {
      borderRight: '1px solid #D8DBDB',
    },
    'table tbody td:last-of-type': {
      borderRight: '1px solid #D8DBDB'
    },
  },
  '& td .editable-cell-value-wrap': {
    display: 'block'
  }
})