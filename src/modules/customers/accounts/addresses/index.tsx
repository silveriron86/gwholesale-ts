import * as React from 'react'
import {withTheme} from 'emotion-theming'
import {CustomersInfoBody, CustomersInfoHeader, Flex} from '../../components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import {DetailsOverview} from '../../components/details-overview'
import AddressesContainer from './addresses-container'
import {GlobalState} from '~/store/reducer'
import {connect} from 'redux-epics-decorator'
import {CustomersModule} from '../../customers.module'
import {MainAddress} from '~/schema'
import {HeaderActions} from "~/modules/customers/accounts/header-actions/header-actions";
import {CustomerBaseComponent} from "~/modules/customers/accounts/base-component/CustomerBaseComponent";

export class Addresses extends CustomerBaseComponent {
  componentDidMount() {
    const customerId = this.props.match.params.customerId
    this.props.getCustomer(customerId)
    this.props.getAddresses(customerId)
    this.props.getAllRoutes()
  }

  onSave = (data: MainAddress) => {
    // tslint:disable-next-line:no-console
    console.log(data)
    if (data.wholesaleAddressId) {
      // update
      this.props.updateAddress(data)
    } else {
      // create
      this.props.createAddress(data)
    }
  }

  render() {
    const {customer, addresses, routes} = this.props
    const billingAddresses: MainAddress[] = []
    const shippingAddresses: MainAddress[] = []
    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        shippingAddresses.push(address)
      } else {
        billingAddresses.push(address)
      }
    })

    return (
      <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
                       onCreateBlankOrder={this.onCreateBlankOrder}
                       duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}/>
        <Flex style={{flexDirection: 'column'}}>
          <CustomersInfoHeader>
            <DetailsOverview customer={customer}/>
          </CustomersInfoHeader>
          <CustomersInfoBody style={{flex: 1}}>
            <AddressesContainer
              {...this.props}
              billingAddresses={billingAddresses}
              shippingAddresses={shippingAddresses}
              routes={routes}
              from="sales"
              onSave={this.onSave}
            />
          </CustomersInfoBody>
        </Flex>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.customers
export default withTheme(connect(CustomersModule)(mapState)(Addresses))
