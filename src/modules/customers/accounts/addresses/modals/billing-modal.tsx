import * as React from 'react'
import {
  InputLabel,
  PopoverWrapper,
  InputRow,
  floatRight,
  ThemeInput,
  ThemeTextArea,
  ThemeOutlineButton,
  ThemeButton,, ThemeCheckbox
} from '../../../customers.style'
import Form, { FormComponentProps } from 'antd/lib/form'
import { Icon, Col, Layout, Row } from 'antd'
import { transLayout } from '../../../components/customers-detail/customers-detail.style'
import { MainAddress } from '~/schema'
import { mb0 } from '~/modules/customers/sales/_style'
import { getString } from '~/common/utils'

const FormItem = Form.Item
interface BillingModalProps extends FormComponentProps {
  data: MainAddress | null
  visible: boolean
  save: (item: MainAddress | null) => void
  isFirst: boolean
}

const initialAddress = {
  address: {
    street1: '',
    street2: '',
    city: '',
    state: '',
    department: '',
    zipcode: '',
  },
  phone: ''
  addressType: 'BILLING',
  deliveryRoute: null,
  isMain: false,
}

class BillingModal extends React.PureComponent<BillingModalProps> {
  state: any
  isMain: boolean
  constructor(props: BillingModalProps) {
    super(props)
    const { data } = props
    this.isMain = false
    this.state = {
      data: { ...initialAddress },
      street: null,
      requiredFields: false,
    }
  }

  _initValues = (props: BillingModalProps) => {
    const data = JSON.parse(JSON.stringify(props.data))
    let street = ''
    if (data) {
      street = data.address ? data.address.street1 : ''
      if (data.address && data.address.street2) {
        street += '\n' + data.address.street2
      }
    }

    const values = data ? data : { ...initialAddress }
    const country = getString(values.address.country)
    this.setState({
      data: values,
      street: street,
      requiredFields: country && country.match( /(United States|U.S.|US)/ )
    })
  }

  componentDidMount() {
    this._initValues(this.props)
  }

  componentWillReceiveProps(nextProps: BillingModalProps) {
    console.log(nextProps)
    if (this.props.visible !== nextProps.visible && nextProps.visible === true) {
      this._initValues(nextProps)
    }
  }

  onDeleteAddress = () => {
    const data = { ...this.state.data }
    data['isDelete'] = true
    delete data.isMain
    this.props.save(data)
  }

  handleSubmit = (e: any) => {
    const { form, data } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let v = { ...values }
        v.addressType = 'BILLING'
        if (data) {
          v.address = { ...v.address, addressId: data.address ? data.address.addressId : '' }
          v.wholesaleAddressId = data.wholesaleAddressId
        }
        if (this.isMain) {
          v.isMain = true
        }
        console.log(v);
        this.props.save(v)
        form.resetFields()
        this.isMain = false
      }
    })
  }

  onChangeCountry = (e: any) => {
    const country = getString(e.target.value)
    this.setState({
      requiredFields: country && country.match( /(United States|U.S.|US)/ )
    })
  }

  render() {
    const { save, isFirst } = this.props
    const { data, requiredFields } = this.state
    const { getFieldDecorator } = this.props.form

    return (
      <PopoverWrapper style={{ width: 400 }}>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
          <Layout style={transLayout}>
            <InputRow>
              <Col md={7}>
                <InputLabel>DEPARTMENT</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.department', {
                    rules: [{ required: false }],
                    initialValue: getString(data.address.department),
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>STREET</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.street1', {
                    rules: [{ required: true, message: 'Street is required!' }],
                    initialValue: getString(data.address.street1),
                  })(<ThemeTextArea rows={4} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>CITY</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.city', {
                    rules: [{ required: true, message: 'City is required!' }],
                    initialValue: getString(data.address.city),
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>STATE</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.state', {
                    rules: [{ required: requiredFields }],
                    initialValue: getString(data.address.state),
                  })(<ThemeInput maxLength={2} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>ZIP CODE</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.zipcode', {
                    rules: [
                      {
                        required: requiredFields,
                        message: 'Zipcode is required!',
                      },
                      {
                        pattern: /\b\d{5}\b/,
                        message: 'Zipcode must be exactly 5 digitals',
                      },
                    ],
                    initialValue: getString(data.address.zipcode),
                  })(<ThemeInput maxLength={5} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>COUNTRY</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.country', {
                    rules: [{ required: true, message: 'Country is required!' }],
                    initialValue: getString(data.address.country),
                  })(<ThemeInput onChange={this.onChangeCountry} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>PHONE NUMBER</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('phone', {
                    // rules: [{ required: true, message: 'City is required!' }],
                    initialValue: getString(data.phone),
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel style={{ paddingTop: 0, lineHeight: '16px' }}>SAVE AS DEFAULT&nbsp;&nbsp;</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('isMain', {
                    initialValue: isFirst ? true : data.isMain,
                    valuePropName: 'checked'
                  })(<ThemeCheckbox disabled={isFirst} />)}
                </FormItem>
              </Col>
            </InputRow>
            <Row>
              <Col md={10}>
                {data.wholesaleAddressId && (
                  <ThemeOutlineButton
                    onClick={this.onDeleteAddress.bind(this, data)}
                    style={floatRight}
                    shape="round"
                  >
                    <Icon type="close" />
                    Delete Address
                  </ThemeOutlineButton>
                )}
              </Col>
              <Col md={14}>
                <ThemeButton
                  shape="round"
                  type="primary"
                  htmlType="submit"
                  style={floatRight}
                >
                  <Icon type="save" />
                  Save Payment Address
                </ThemeButton>
              </Col>
            </Row>
          </Layout>
        </Form>
      </PopoverWrapper>
    )
  }
}

export default Form.create<BillingModalProps>()(BillingModal)
