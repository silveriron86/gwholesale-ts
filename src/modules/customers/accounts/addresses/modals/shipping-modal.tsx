import * as React from 'react'
import {
  InputLabel,
  PopoverWrapper,
  InputRow,
  floatRight,
  RowDivider,
  textCenter,
  ThemeInput,
  ThemeTextArea,
  ThemeOutlineButton,
  ThemeSelect,
  ThemeButton,
  ThemeCheckbox,
} from '../../../customers.style'
import Form, { FormComponentProps } from 'antd/lib/form'
import { CustomButton } from '~/components'
import { Icon, Col, Layout, Row, Select, TimePicker } from 'antd'
import { transLayout } from '../../../components/customers-detail/customers-detail.style'
import { MainAddress, WholesaleRoute } from '~/schema'
import { mb0 } from '~/modules/customers/sales/_style'
import moment from 'moment'
import { getString } from '~/common/utils'

const FormItem = Form.Item
interface ShippingModalProps extends FormComponentProps {
  data: MainAddress | null
  visible: boolean
  routes: WholesaleRoute[]
  save: (item: MainAddress | null) => void
  from: string
  isFirst: boolean
}

const defaultAddress = {
  address: {
    street1: '',
    street2: '',
    city: '',
    state: '',
    department: '',
    zipcode: '',
  },
  phone: '',
  addressType: 'SHIPPING',
  deliveryRoute: null,
  deliveryWindows: null,
  isMain: false,
}

const weekDays: string[] = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']

const deliveryWindowsObj = {
  MON: [null, null],
  TUE: [null, null],
  WED: [null, null],
  THU: [null, null],
  FRI: [null, null],
  SAT: [null, null],
  SUN: [null, null],
}

class ShippingModal extends React.PureComponent<ShippingModalProps> {
  state: any
  isMain: boolean
  constructor(props: ShippingModalProps) {
    super(props)
    this.isMain = false
    this.state = {
      data: { ...defaultAddress },
      street: null,
      deliveryWindows: JSON.parse(JSON.stringify(deliveryWindowsObj)),
      requiredFields: false,
    }
  }

  _initValues = (props: ShippingModalProps) => {
    const data = JSON.parse(JSON.stringify(props.data))
    let street = ''
    if (data) {
      street = data.address.street1
      if (data.address.street2) {
        street += '\n' + data.address.street2
      }
    }

    if (data && data.deliveryRoute) {
      data.deliveryRoute = data.deliveryRoute.id
    } else {
      if (props.routes && props.routes.length > 0) {
        if (data) {
          data.deliveryRoute = props.routes[0].id
        } else {
          defaultAddress.deliveryRoute = props.routes[0].id
        }
      }
    }

    if (data && data.deliveryWindows) {
      this.setDeliveryWindows(data.deliveryWindows)
    } else {
      this.setState({
        deliveryWindows: JSON.parse(JSON.stringify(deliveryWindowsObj)),
      })
    }

    const values = data ? data : { ...defaultAddress };
    const country = getString(values.address.country)
    this.setState({
      data: values,
      street: street,
      editableWindows: false,
      requiredFields: country && country.match( /(United States|U.S.|US)/ )
    })
  }

  componentDidMount() {
    this._initValues(this.props)
  }

  componentWillReceiveProps(nextProps: ShippingModalProps) {
    if (this.props.visible !== nextProps.visible && nextProps.visible === true) {
      this._initValues(nextProps)
    }
  }

  onDeleteAddress = () => {
    const data = { ...this.state.data }
    data.isDelete = true
    delete data.isMain
    this.props.save(data)
  }

  toggleEditWindows = () => {
    this.setState({
      editableWindows: !this.state.editableWindows,
    })
  }

  getDeliveryWindows = () => {
    const deliveryWindows = { ...this.state.deliveryWindows }
    let result = ''
    weekDays.forEach((el) => {
      if (deliveryWindows[el][0] && deliveryWindows[el][1]) {
        result +=
          el +
          moment(deliveryWindows[el][0]).format('HH:mm') +
          '-' +
          moment(deliveryWindows[el][1]).format('HH:mm') +
          ';'
      }
    })

    result = result.slice(0, -1)
    console.log(result)
    return result
  }

  setDeliveryWindows = (arg: string) => {
    let deliveryWindows = { ...this.state.deliveryWindows }
    let arrData = arg.split(';')
    if (arrData.length > 0) {
      arrData.forEach((el) => {
        let prefix = el.substring(0, 3)
        let timeStr = el.substring(3)
        let times = timeStr.split('-')
        console.log(prefix)
        console.log(times)
        if (weekDays.includes(prefix) && times[0].length > 0 && times[1].length > 0) {
          deliveryWindows[prefix][0] = moment(times[0], 'HH:mm')
          deliveryWindows[prefix][1] = moment(times[1], 'HH:mm')

          console.log(deliveryWindows[prefix])
        }
      })
    }
    this.setState({ deliveryWindows })
  }

  handleSubmit = (e: any) => {
    const { form, data } = this.props
    e.preventDefault()

    const deliveryWindows = this.getDeliveryWindows()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let v = { ...values }
        v.addressType = 'SHIPPING'
        if (data) {
          v.address = { ...v.address, addressId: data.address ? data.address.addressId : '' }
          v.wholesaleAddressId = data.wholesaleAddressId
        }
        if (this.isMain) {
          v.isMain = true
        }
        v.deliveryWindows = deliveryWindows
        this.props.save(v)
        form.resetFields()
        this.isMain = false
      }
    })
  }

  onTimeChange = (param: any, time: any) => {
    const day = param.day
    const index = param.index
    const deliveryWindows = { ...this.state.deliveryWindows }
    deliveryWindows[day][index] = time
    this.setState({ deliveryWindows })
    console.log(deliveryWindows)
  }

  onChangeCountry = (e: any) => {
    const country = getString(e.target.value)
    this.setState({
      requiredFields: country && country.match( /(United States|U.S.|US)/ )
    })
  }

  render() {
    const { routes, from, isFirst } = this.props
    const { getFieldDecorator } = this.props.form
    const { data, editableWindows, deliveryWindows, requiredFields } = this.state
    const { Option } = Select
    const isVendor = from === 'purchase'

    return (
      <PopoverWrapper style={{ width: 400 }}>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
          <Layout style={transLayout}>
            <InputRow>
              <Col md={7}>
                <InputLabel>DEPARTMENT</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.department', {
                    rules: [{ required: false }],
                    initialValue: getString(data.address.department),
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>STREET</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.street1', {
                    rules: [{ required: true, message: 'Street is required!' }],
                    initialValue: getString(data.address.street1),
                  })(<ThemeTextArea rows={4} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>CITY</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.city', {
                    rules: [{ required: true, message: 'City is required!' }],
                    initialValue: getString(data.address.city),
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>STATE</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.state', {
                    rules: [{ required: requiredFields }],
                    initialValue: getString(data.address.state),
                  })(<ThemeInput maxLength={2} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>ZIP CODE</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.zipcode', {
                    rules: [
                      {
                        required: requiredFields,
                        message: 'Zipcode is required!',
                      },
                      {
                        len: 5,
                        message: 'Zipcode must be exactly 5 digitals',
                      },
                      {
                        pattern: /^[0-9]+$/,
                        message: 'Zipcode must be exactly 5 digitals',
                      },
                    ],
                    initialValue: getString(data.address.zipcode),
                  })(<ThemeInput maxLength={5} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>COUNTRY</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('address.country', {
                    rules: [{ required: true, message: 'Country is required!' }],
                    initialValue: getString(data.address.country),
                  })(<ThemeInput onChange={this.onChangeCountry} />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>PHONE NUMBER</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('phone', {
                    // rules: [{ required: true, message: 'City is required!' }],
                    initialValue: getString(data.phone),
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <RowDivider />
            {data && from == 'sales' ? (
              <>
                <InputRow>
                  <Col md={7}>
                    <InputLabel>ROUTE</InputLabel>
                  </Col>
                  <Col md={17}>
                    <FormItem style={mb0}>
                      {getFieldDecorator('deliveryRoute', {
                        // rules: [{ required: true, message: 'Route is required!' }],
                        initialValue: data.wholesaleRouteDetail ? data.wholesaleRouteDetail.route.id : '',
                      })(
                        <ThemeSelect style={{ width: '100%' }}>
                          {routes &&
                            routes.length > 0 &&
                            routes.map((route, index) => {
                              return (
                                <Option key={`route=${index}`} value={route.id}>
                                  {route.routeName}
                                </Option>
                              )
                            })}
                        </ThemeSelect>,
                      )}
                    </FormItem>
                  </Col>
                </InputRow>
                <RowDivider />
              </>
            ) : (
                ''
              )}
            {data && !editableWindows && (
              <InputRow>
                <Col md={10}>
                  <InputLabel>DELIVERY WINDOWS</InputLabel>
                </Col>
                <Col md={14}>
                  <ThemeButton shape="round" onClick={this.toggleEditWindows} style={floatRight}>
                    <Icon type="edit" theme="filled" />
                    Edit Delivery Windows
                  </ThemeButton>
                </Col>
              </InputRow>
            )}
            {(editableWindows || !data) && (
              <>
                <InputRow>
                  <Col md={24}>
                    <InputLabel>DELIVERY WINDOWS</InputLabel>
                  </Col>
                </InputRow>
                {weekDays.map((w, i) => {
                  return (
                    <InputRow key={`w-${i}`}>
                      <Col md={2} />
                      <Col md={3}>
                        <InputLabel style={{ marginRight: 15 }}>{w}</InputLabel>
                      </Col>
                      <Col md={7}>
                        <FormItem style={mb0}>
                          {getFieldDecorator(`deliveryWindows[${w}][0]`, {
                            rules: [{ required: deliveryWindows[w][1] !== null, message: 'This is required!' }],
                            initialValue: deliveryWindows[w][0],
                          })(
                            <TimePicker format="HH:mm" onChange={this.onTimeChange.bind(this, { day: w, index: 0 })} />,
                          )}
                        </FormItem>
                      </Col>
                      <Col md={3} style={textCenter}>
                        <InputLabel style={{ marginLeft: 18 }}>TO</InputLabel>
                      </Col>
                      <Col md={7}>
                        <FormItem style={mb0}>
                          {getFieldDecorator(`deliveryWindows[${w}][1]`, {
                            initialValue: deliveryWindows[w][1],
                          })(
                            <TimePicker format="HH:mm" onChange={this.onTimeChange.bind(this, { day: w, index: 1 })} />,
                          )}
                        </FormItem>
                      </Col>
                    </InputRow>
                  )
                })}
              </>
            )}
            <RowDivider />
            <InputRow>
              <Col md={7}>
                <InputLabel style={{ paddingTop: 0, lineHeight: '16px' }}>SAVE AS DEFAULT&nbsp;&nbsp;</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('isMain', {
                    initialValue: isFirst ? true : data.isMain,
                    valuePropName: 'checked',
                  })(<ThemeCheckbox disabled={isFirst} />)}
                </FormItem>
              </Col>
            </InputRow>
            <Row>
              <Col md={10}>
                {data.wholesaleAddressId && (
                  <ThemeOutlineButton onClick={this.onDeleteAddress.bind(this, data)} style={floatRight} shape="round">
                    <Icon type="close" />
                    Delete Address
                  </ThemeOutlineButton>
                )}
              </Col>
              <Col md={14}>
                <ThemeButton shape="round" type="primary" htmlType="submit" style={floatRight}>
                  <Icon type="save" />
                  Save {isVendor ? 'Physical' : 'Delivery'} Address
                </ThemeButton>
              </Col>
            </Row>
          </Layout>
        </Form>
      </PopoverWrapper>
    )
  }
}

export default Form.create<ShippingModalProps>()(ShippingModal)
