import React from 'react'
import { cloneDeep } from 'lodash'
import { ml0, ThemeTable, ThemeOutlineButton } from '~/modules/customers/customers.style'
import { Wrapper, WrapperTitle } from '../addresses.style'
import { Popover, Icon } from 'antd'
import { ColumnProps } from 'antd/es/table'
import { Address, MainAddress } from '~/schema'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'

interface BillingTableProps {
  listItems: MainAddress[]
  getModal: Function
  updateModalVisible: Array<boolean>
  handleVisibleChange: Function
  theme: Theme
}

export class BillingTable extends React.PureComponent<BillingTableProps> {
  render() {
    const { getModal, updateModalVisible, handleVisibleChange, listItems, theme } = this.props
    const items = cloneDeep(listItems)
    const sortedItems =
      items.length > 1
        ? items.sort(function (x: MainAddress, y: MainAddress) {
          return x.isMain ? -1 : y.isMain ? 1 : 0
        })
        : items

    const columns: ColumnProps<any>[] = [
      {
        title: '',
        dataIndex: 'wholesaleAddressId',
        width: 60,
        render: (_text: string, _record: MainAddress, index: number) => {
          return index + 1
        },
      },
      {
        title: 'PAYMENT ADDRESS',
        dataIndex: 'address',
        width: 400,
        render: (address: Address, record: MainAddress, _index: number) => {
          if (!address) return ''
          const fullAddress = `${record.address.department ? record.address.department : ''} ${record.address.street1
            } ${record.address.city} ${record.address.state}, ${record.address.zipcode} ${record.address.country}`
          return (
            <>
              {record.isMain === true && (
                <span style={{ color: theme.primary, marginRight: 5 }}>
                  <Icon type="star" theme="filled" />
                </span>
              )}
              {fullAddress}
            </>
          )
        },
      },
      {
        dataIndex: 'markup',
        render: (_text: string, record: MainAddress, index: number) => (
          <Popover
            placement="left"
            content={getModal(record, index)}
            trigger="click"
            visible={updateModalVisible[index]}
            onVisibleChange={handleVisibleChange.bind(this, record, index)}
          >
            <ThemeOutlineButton shape="round">
              Edit
              <Icon type="edit" theme="filled" />
            </ThemeOutlineButton>
          </Popover>
        ),
      },
    ]

    return (
      <div style={{ margin: '0 2px' }}>
        <WrapperTitle>Payment Address</WrapperTitle>
        <ThemeTable
          pagination={{ hideOnSinglePage: true, pageSize: 12 }}
          columns={columns}
          dataSource={sortedItems}
          rowKey="wholesaleAddressId"
        />
      </div>
    )
  }
}

export default withTheme(BillingTable)
