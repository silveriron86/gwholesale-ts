import React from "react";
import {CustomersDispatchProps, TempCustomersStateProps} from "~/modules/customers";
import {Theme} from "~/common";
import {notification} from "antd";
import {getFulfillmentDate} from "~/common/utils";
import moment from "moment";
import {WholesaleRoute} from "~/schema";
import {RouteComponentProps} from "react-router";

type CustomerBaseComponentProps = CustomersDispatchProps &
  TempCustomersStateProps & {
  theme: Theme
} & RouteComponentProps<{ customerId: string }>

export class CustomerBaseComponent<S = any> extends React.PureComponent<CustomerBaseComponentProps, S> {

  protected onDuplicateMostRecentOrder = () => {
    if (this.id != null) {
      const { sellerSetting } = this.props
      const fulfillmentDate = getFulfillmentDate(sellerSetting)
      this.props.resetMostRecentLoadingStatus()
      this.props.duplicateMostRecentOrder({
        clientId: this.id,
        body: { deliveryDate: fulfillmentDate },
      })
    }
  }

  protected onCreateBlankOrder = () => {
    const {routes, customer, sellerSetting} = this.props
    if (customer && customer.creditLimit > 0 && customer.openBalance > customer.creditLimit) {
      notification.error({
        message: 'ERROR',
        description: 'Customer open balance is greater than credit limit',
        onClose: () => {
        },
      })
      return
    }

    const fulfillmentDate = getFulfillmentDate(sellerSetting)

    let data = {
      wholesaleClientId: this.id,
      deliveryDate: fulfillmentDate,
      orderDate: moment().format('MM/DD/YYYY'),
      itemList: [],
    }

    let activeRoutes: WholesaleRoute[] = []
    if (routes.length > 0) {
      routes.forEach((route) => {
        const weekDay = moment()
          .format('dddd')
          .toUpperCase()
        if (route.activeDays.indexOf(weekDay) >= 0) {
          activeRoutes.push({...route})
        }
      })

      if (activeRoutes.length > 0) {
        data.defaultRoute = activeRoutes[0].id
      }
    }
    this.props.createEmptyOrder(data)
  }

  get id(): string {
    return this.props.match.params.customerId;
  }
}

