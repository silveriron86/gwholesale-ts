import styled from '@emotion/styled'
import { Layout, Pagination } from 'antd'
import { badgeColor, medLightGrey } from '~/common'

export const ChatWrapper = styled(Layout)((props) => ({
  border: `1px solid ${props.theme.primary}`,
  borderRadius: 5,
  padding: 15,
  backgroundColor: 'transparent',
}))

export const UnreadMessages = styled('div')((props) => ({
  position: 'absolute',
  marginLeft: 70,
  borderRadius: 3,
  padding: '5px 20px',
  backgroundColor: badgeColor,
  boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
  width: 'fit-content',
  color: 'white',
}))

export const ReceivedMessage = styled('div')((props) => ({
  border: `1px solid ${props.theme.primary}`,
  backgroundColor: props.theme.lighter,
  borderRadius: 5,
  width: '100%',
  padding: '10px 8px',
  fontWeight: 'bold',
  marginTop: 3,
  fontSize: 12,
  wordBreak: 'break-all'
}))

export const SentMessage = styled('div')((props) => ({
  backgroundColor: medLightGrey,
  borderRadius: 5,
  width: '100%',
  padding: '10px 8px',
  marginTop: 3,
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 'bold',
  fontSize: 12,
  wordBreak: 'break-all',
}))

export const ChatAvatar = styled('div')((props) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  width: 52,
  height: 52,
  marginRight: 8,
  overflow: 'hidden',
}))

export const chatRoom: React.CSSProperties = {
  paddingRight: 20,
  backgroundColor: 'transparent',
  maxHeight: 460,
  overflowY: 'auto',
}

export const avatarImg: React.CSSProperties = {
  height: 30
}

export const infoRow: React.CSSProperties = {
  justifyContent: 'space-between',
  fontSize: 12,
  padding: '0 10px',
}

export const ThemePagination = styled(Pagination)((props) => ({
  '&': {
    '&.ant-pagination': {
      color: props.theme.light,
      paddingRight: '30px',
      '.ant-pagination-item-active': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-item:focus, .ant-pagination-item:hover': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-prev:focus .ant-pagination-item-link, .ant-pagination-next:focus .ant-pagination-item-link, .ant-pagination-prev:hover .ant-pagination-item-link, .ant-pagination-next:hover .ant-pagination-item-link': {
        borderColor: props.theme.light,
        color: props.theme.light
      }
    }
  }
}))

export const LoadMore = styled('a')((props) => ({
  '&': {
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 5,
    color: props.theme.light
  }

}))