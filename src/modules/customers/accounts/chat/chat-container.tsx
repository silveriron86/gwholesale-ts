import * as React from 'react'
import { DetailsWrapper, DetailsTitle } from '../../customers.style'
import { Layout, Row, Col } from 'antd'
import { transLayout } from '../../components/customers-detail/customers-detail.style'
import { ChatWrapper, UnreadMessages } from './_styles'
import ChatRoom from './chat-room'
import { Message } from '../../customers.model'
import LimitedInput from './limited-input'

interface ChatContainerProps {
  messages: Array<Message>
  total: number
  addMessage: Function
  getMore: Function
}

export class ChatContainer extends React.PureComponent<ChatContainerProps> {
  render() {
    return (
      <DetailsWrapper>
        {/* <UnreadMessages>28 UNREAD MESSAGES</UnreadMessages> */}
        <DetailsTitle>Chat</DetailsTitle>
        <Layout style={transLayout}>
          <Row>
            <Col xs={24} sm={24} md={22}>
              <ChatWrapper>
                <Row>
                  <Col xs={24} sm={12} md={14}>
                    <ChatRoom messages={this.props.messages}  getMore={this.props.getMore} total={this.props.total}/>
                  </Col>
                  <Col xs={24} sm={12} md={10}>
                    <LimitedInput limit={200} handleSend={this.props.addMessage} />
                  </Col>
                </Row>
              </ChatWrapper>
            </Col>
          </Row>
        </Layout>
      </DetailsWrapper>
    )
  }
}

export default ChatContainer
