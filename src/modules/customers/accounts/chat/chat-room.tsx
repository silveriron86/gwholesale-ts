import * as React from 'react'
import { Layout } from 'antd'
import ReceivedRow from './received-row'
import SentRow from './sent-row'
import { chatRoom, ThemePagination, LoadMore } from './_styles'
import { Message } from '../../customers.model'


interface ChatRoomProps {
  messages: Array<Message>
  total:number
  getMore:Function
}

export class ChatRoom extends React.PureComponent<ChatRoomProps> {
  render() {
    const { messages,total } = this.props
    const len = messages!= null ? messages.length : 0
    return (
      <Layout style={chatRoom} >
        {
          total > len ? <LoadMore onClick={this.props.getMore}>Click me for more historical message</LoadMore> : ""
        }
        {len == 0 ? <div style={{textAlign: 'center'}}>No Chat Data</div> : messages.map((message, index) => {
          if (message.type === 0) {
            // Received
            return <ReceivedRow item={message} key={`chat-room-${index}`}  />
          } else {
            // Sent
            return <SentRow item={message} key={`chat-room-${index}`}/>
          }
        })}
        <div id="messageAnchor"></div>
        {/* {
          len == 0 ? "" : <ThemePagination defaultCurrent={1} total={total} 
          onChange={this.props.onChangePage} style={{textAlign: 'center'}}/>
        } */}
      </Layout>  
    )
  }
}

export default ChatRoom
