import React from 'react'
import {withTheme} from 'emotion-theming'
import {GlobalState} from '~/store/reducer'
import {connect} from 'redux-epics-decorator'
import {Theme} from '~/common'
import PageLayout from '~/components/PageLayout'
import {CustomersInfoBody, CustomersInfoHeader} from '../../components/customers-detail/customers-detail.style'
import DetailsOverview from '../../components/details-overview'
import ChatContainer from './chat-container'
import {notification} from 'antd'

import {CustomersDispatchProps, CustomersModule, TempCustomersStateProps} from '../../customers.module'
import {HeaderActions} from "~/modules/customers/accounts/header-actions/header-actions";
import {CustomerBaseComponent} from "~/modules/customers/accounts/base-component/CustomerBaseComponent";

type ChatProps = CustomersDispatchProps &
  TempCustomersStateProps & {
  theme: Theme
}

export class Chat extends CustomerBaseComponent {

  state: any

  constructor(props: ChatProps) {
    super(props)
    this.state = {
      clientId: '',
      chatList: [],
      currentChatList: [],
      pageSize: -10,
      currentLength: -10
    }
  }

  componentWillReceiveProps(nextProps: ChatProps) {
    if (this.state.chatList != nextProps.chatList) {
      //has new add, len +1
      if (nextProps.message) {
        this.setState({
          chatList: nextProps.chatList,
          currentChatList: nextProps.chatList.slice(this.state.currentLength + (-1)),
          currentLength: this.state.currentLength + (-1)
        })

        const anchorElement = document.getElementById("messageAnchor");
        if (anchorElement) {
          setTimeout(() => {
            anchorElement.scrollIntoView({
              behavior: 'smooth',
            });
          }, 200)
        }
      } else {
        this.setState({
          chatList: nextProps.chatList,
          currentChatList: nextProps.chatList.slice(this.state.currentLength)
        })
      }

    }

    if (nextProps.message !== this.props.message) {
      if (nextProps.type != null) {
        notification[nextProps.type!]({
          message: nextProps.type!.toLocaleUpperCase(),
          description: nextProps.message,
          onClose: nextProps.resetNotif,
          duration: nextProps.type === 'success' ? 5: 4.5
        })
      }
    }

  }

  componentDidMount() {
    const clientId = this.props.match.params.customerId
    this.setState({
      clientId: clientId
    })
    this.props.getCustomer(clientId)
    this.props.getContacts(clientId)
    this.props.getChatListByClientId(clientId);

  }

  handleAddMessage = (message: string) => {
    this.props.createChat({message, clientId: this.state.clientId});
  }

  getMore = () => {
    let len = this.state.currentChatList.length
    if (len < this.state.chatList.length) {
      let newLen = -len + this.state.pageSize
      this.setState({
        currentChatList: this.state.chatList.slice(newLen),
        currentLength: newLen
      })
    }
  }

  render() {
    const {customer} = this.props
    const total = this.props.chatList.length
    const {currentChatList} = this.state
    return (
      <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
                       onCreateBlankOrder={this.onCreateBlankOrder}
                       duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}/>
        <CustomersInfoHeader>
          <DetailsOverview customer={customer}/>
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <ChatContainer messages={currentChatList} getMore={this.getMore} total={total}
                         addMessage={this.handleAddMessage}/>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.customers
export default withTheme(connect(CustomersModule)(mapState)(Chat))
