import React from 'react'
import { ThemeTextArea, Characters,ThemeButton } from '~/modules/customers/customers.style'
import { notification } from 'antd'
import { onLoadFocusOnFirst } from '~/common/jqueryHelper'

interface LimitedInputProps {
  limit: number
  handleSend: Function
}

export class LimitedInput extends React.PureComponent<LimitedInputProps> {
  state = {
    message: '',
  }

  componentDidMount() {
    onLoadFocusOnFirst('simple-input-wrapper')
  }

  handleChange = (e: { target: { value: any } }) => {
    const { limit } = this.props
    const v = e.target.value
    if (v.length > limit) {
      return
    }

    this.setState({
      message: v,
    })
  }

  handleKeyUp = (e: any) => {
    if (e.key === 'Enter' && e.ctrlKey) {
      this.onSubmit();
    }
  }

  onSubmit = () =>{
    let { message } = this.state
    if (!message) {
      notification['warning']({
        message: "Warning",
        description: "Message can not be empty"
      })
      return
    }
    this.props.handleSend(message)
    this.setState({
      message: '',
    })
  }

  render() {
    const { limit } = this.props
    const { message } = this.state
    return (
      <div className='simple-input-wrapper'>
        <ThemeTextArea
          rows={5}
          value={message}
          placeholder="Send a message..."
          onKeyUp={this.handleKeyUp}
          onChange={this.handleChange}
        />
        <Characters>
          {limit - message.length} <b>CHARACTERS</b>
        </Characters>
        <div style={{textAlign: 'right'}}>
          <ThemeButton onClick={this.onSubmit} >
            Submit
          </ThemeButton>
        </div>      
      </div>
    )
  }
}

export default LimitedInput
