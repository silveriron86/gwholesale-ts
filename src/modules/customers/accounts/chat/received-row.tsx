import * as React from 'react'
import moment from 'moment'
import { Row, Col, Avatar } from 'antd'
import { FlexRow, Flex1 } from '../../customers.style'
import { ReceivedMessage, ChatAvatar, avatarImg, infoRow } from './_styles'
import { Message } from '../../customers.model'
import {Chat} from '../../../../schema'


interface ReceivedRowProps {
  item: Chat
}

export class ReceivedRow extends React.PureComponent<ReceivedRowProps> {
  render() {
    const { item } = this.props
    const createdDate = moment(item.createdDate).format('l LT')
    return (
      <Row style={{ marginTop: 15 }}>
        <Col xs={24} sm={24} md={20}>
          <FlexRow>
            <ChatAvatar>
              <Avatar icon="user" />
            </ChatAvatar>
            <Flex1>
              <FlexRow style={infoRow}>
                <div>
                  <b>{item.user.firstName + " " + item.user.lastName}</b> said
                </div>
                <div>{createdDate}</div>
              </FlexRow>
              <ReceivedMessage>{item.content}</ReceivedMessage>
            </Flex1>
          </FlexRow>
        </Col>
      </Row>
    )
  }
}

export default ReceivedRow
