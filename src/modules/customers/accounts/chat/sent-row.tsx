import * as React from 'react'
import moment from 'moment'
import { Row, Col, Avatar } from 'antd'
import { FlexRow, Flex1 } from '../../customers.style'
import { ChatAvatar, SentMessage, avatarImg, infoRow } from './_styles'
import { Message } from '../../customers.model'
import {Chat} from '../../../../schema'
interface SentRowProps {
  item: Chat
}

export class SentRow extends React.PureComponent<SentRowProps> {
  render() {
    const { item } = this.props
    const createdDate = moment(item.createdDate).format('l LT')
    return (
      <Row style={{ marginTop: 15 }}>
        <Col xs={24} sm={24} md={4} />
        <Col xs={24} sm={24} md={20}>
          <FlexRow>
            <Flex1>
              <FlexRow style={infoRow}>
                <div>{createdDate}</div>
                <div>
                  <b>{item.user.firstName + " " + item.user.lastName}</b> said
                </div>
              </FlexRow>
              <SentMessage>{item.content}</SentMessage>
            </Flex1>
            <ChatAvatar style={{ marginLeft: 8, marginRight: 0 }}>
              {/* <img src={item.sender_avatar} style={avatarImg} /> */}
              <Avatar icon="user" />
            </ChatAvatar>
          </FlexRow>
        </Col>
      </Row>
    )
  }
}

export default SentRow
