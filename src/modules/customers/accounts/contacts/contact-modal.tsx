import * as React from 'react'
import {
  InputLabel,
  PopoverWrapper,
  InputRow,
  ThemeInput,
  ThemeTextArea,
  ThemeOutlineButton,
  ThemeButton,
  ThemeCheckbox,
  Flex,
} from '../../customers.style'
import Form, { FormComponentProps } from 'antd/lib/form'
// import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Icon, Col, Layout, Row, Popconfirm } from 'antd'
import { transLayout } from '../../components/customers-detail/customers-detail.style'
import { MainContact } from '~/schema'
import { mb0 } from '../../sales/_style'

const FormItem = Form.Item

interface ContactModalProps extends FormComponentProps {
  contact: MainContact | null
  saveAsMain: (item: MainContact) => void
  save: (item: MainContact) => void
  del: (item: MainContact) => void
  visible: boolean
}

const defaultContact = {
  wholesaleCompany: null,
  title: '',
  name: '',
  phone: '',
  mobilePhone: '',
  email: '',
  notes: '',
  salutation: '',
  fax: '',
  orderConfirmCCEnabled: false,
}

class ContactModal extends React.PureComponent<ContactModalProps> {
  state: any
  isMain: boolean
  constructor(props: ContactModalProps) {
    super(props)
    this.isMain = false
    this.state = {
      contact: { ...defaultContact },
      firstName: '',
      lastName: '',
    }
  }

  _initValues = (props: ContactModalProps) => {
    const { contact } = props
    let firstName = ''
    let lastName = ''
    if (contact && contact.name) {
      const tmp = contact.name.split(' ')
      firstName = tmp.slice(0, -1).join(' ')
      lastName = tmp.slice(-1).join(' ')
    }
    this.setState({
      contact: contact ? contact : { ...defaultContact },
      firstName: firstName,
      lastName: lastName,
    })
  }

  componentDidMount() {
    this._initValues(this.props)
  }

  componentWillReceiveProps(nextProps: ContactModalProps) {
    if (this.props.visible !== nextProps.visible && nextProps.visible === true) {
      this._initValues(nextProps)
    }
  }

  onChange = (type: string, e: any) => {
    const contact = { ...this.state.contact }
    contact[type] = e.target.value
    this.setState({
      contact,
    })
  }

  onChangeName = (type: string, e: any) => {
    const state = { ...this.state }
    state[type] = e.target.value
    this.setState(state, () => {
      state.contact.name = [this.state.firstName, this.state.lastName].join(' ')
      this.setState({
        contact: state.contact,
      })
    })
  }

  onDeleteContact = () =>{
    this.props.del({ ...this.state.contact })
  }

  handleSubmit = (e: any) => {
    const { form, contact } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let data = {...values}
        data.name = [data.firstName, data.lastName].join(' ');

        delete data.firstName
        delete data.lastName

        if (contact && contact.id) {
          data.id = contact.id;
        }

        if(this.isMain) {
          data.orderConfirmCCEnabled = true
          this.props.saveAsMain(data)
        }else {
          this.props.save(data)
        }
        form.resetFields();
      }
    })
  }

  onClickSubmit = (arg: boolean) => {
    this.isMain = arg
  }

  render() {
    const { contact, firstName, lastName } = this.state
    const { getFieldDecorator } = this.props.form

    return (
      <PopoverWrapper style={{ width: 400 }}>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
          <Layout style={transLayout}>
            <InputRow>
              <Col span={7}>
                <InputLabel>TITLE</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('title', {
                    // rules: [{ required: true, message: 'Title is required!' }],
                    initialValue: contact.title
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>SALUTATION</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('salutation', {
                    // rules: [{ required: true, message: 'Salutation is required!' }],
                    initialValue: contact.salutation
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>FIRST NAME</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('firstName', {
                    // rules: [{ required: true, message: 'First name is required!' }],
                    initialValue: firstName
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>LAST NAME</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('lastName', {
                    // rules: [{ required: true, message: 'Last name is required!' }],
                    initialValue: lastName
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>MAIN PHONE</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('phone', {
                    // rules: [
                    //   {
                    //     required: true,
                    //     message: 'Main phone is required!'
                    //   },
                    //   {
                    //     pattern: /(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})/,
                    //     message: 'Invalid international phone number!'
                    //   }
                    // ],
                    initialValue: contact.phone
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>MOBILE PHONE</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('mobilePhone', {
                    // rules: [
                    //   {
                    //     required: true,
                    //     message: 'Mobile phone is required!'
                    //   },
                    //   {
                    //     pattern: /(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})/,
                    //     message: 'Invalid international phone number!'
                    //   }
                    // ],
                    initialValue: contact.mobilePhone
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>FAX</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('fax', {
                    // rules: [{ required: true, message: 'Fax is required!' }],
                    initialValue: contact.fax
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>EMAIL</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('email', {
                    rules: [
                      {
                        type: 'email',
                        message: 'Email is not valid!'
                      },
                      {
                        required: true,
                        message: 'Email is required!'
                      }
                    ],
                    initialValue: contact.email
                  })(<ThemeInput />)}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col span={7}>
                <InputLabel>NOTES</InputLabel>
              </Col>
              <Col span={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('notes', {
                    rules: [{ required: false }],
                    initialValue: contact.notes
                  })(<ThemeTextArea rows={4} />)}
                </FormItem>
              </Col>
            </InputRow>

            <Flex style={{marginBottom: 5}}>
              <FormItem style={mb0}>
                {getFieldDecorator('orderConfirmCCEnabled', {
                  initialValue: contact.isMain ? true : contact.orderConfirmCCEnabled,
                  valuePropName: 'checked'
                })(<ThemeCheckbox disabled={contact ? contact.isMain : false} />)}
              </FormItem>
              <label style={{marginLeft: 8}} htmlFor="orderConfirmCCEnabled">
                Send eCommerce receipt/confirmation emails
              </label>
            </Flex>
            <Row>
              <Col span={14}>
                {contact.id && (
                  <ThemeButton shape="round" type="primary" htmlType="submit" onClick={this.onClickSubmit.bind(this, true)}>
                    <Icon type="star" theme="filled" />
                    Save As Main Contact
                  </ThemeButton>
                )}
              </Col>
              <Col span={10}>
                <ThemeButton shape="round" type="primary" htmlType="submit" onClick={this.onClickSubmit.bind(this, false)}>
                  <Icon type="save" />
                  Save Contact
                </ThemeButton>
              </Col>
            </Row>
            <Row style={{marginTop:10}}>
              <Col offset={15} span={9}>
                {contact.id && (
                  <Popconfirm
                    placement="left"
                    title={"Are you sure to delete this contact?"}
                    onConfirm={this.onDeleteContact}
                    okText="Yes"
                    cancelText="No"
                  >
                    <ThemeOutlineButton shape="round">
                      <Icon type="close" />
                      Delete Contact
                    </ThemeOutlineButton>
                  </Popconfirm>
                )}
              </Col>
            </Row>
          </Layout>
        </Form>
      </PopoverWrapper>
    )
  }
}

export default Form.create<ContactModalProps>()(ContactModal)
