import * as React from 'react'
import { cloneDeep } from 'lodash'
import { DetailsWrapper, DetailsTitle, floatRight, ThemeTable, ThemeButton } from '../../customers.style'
import { ColumnProps } from 'antd/lib/table'
import { tableCss } from '../../components/sales-orders-table/table.style'
import { Icon, Col, Layout, Row, Popover } from 'antd'
// import { layoutStyle } from '../../components/customers-detail/customers-detail.style'
import ContactModal from './contact-modal'
import { MainContact } from '~/schema'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { Level2WithTable, onPopoverOpenWithButtons, documentBodyKeyupEvent } from '~/common/jqueryHelper'

export interface ContactsContainerProps {
  contacts: MainContact[]
  editItem: (item: MainContact) => void
  search: string
  onSave: () => void
  onSaveAsMain: () => void
  onDelete: () => void
  theme: Theme
  loadingContacts: boolean
}
const emtpyNode = () => null

class ContactsContainer extends React.PureComponent<ContactsContainerProps> {
  state = {
    filteredCategory: [],
    selectedItemIndex: -1,
    expandedRowKeys: [],
    newModalVisible: false,
    updateModalVisible: [],
    currentIndex: 0,
  }

  componentDidMount() {
    onPopoverOpenWithButtons('popover-with-buttons')
    documentBodyKeyupEvent('popover-with-buttons')
    Level2WithTable('level-2-with-table')
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    Level2WithTable('level-2-with-table')
  }

  private mutiSortTable = React.createRef<any>()

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    //this.props.clearRelatedOrders()
    if (this.state.expandedRowKeys.length > 0) {
      const row = []
      for (const value of expandedRowKeys) if (value !== this.state.expandedRowKeys[0]) row.push(value)
      this.setState({
        expandedRowKeys: row,
      })
    } else {
      this.setState({
        expandedRowKeys: expandedRowKeys,
      })
    }
  }

  handleVisibleChange = (item: MainContact | null, index: number, visible: boolean) => {
    if (item) {
      const visibles: Array<boolean> = [...this.state.updateModalVisible]
      visibles[index] = visible
      this.setState({
        currentIndex: index,
        updateModalVisible: visibles,
      })
    } else {
      this.setState({
        newModalVisible: visible,
      })
    }
  }

  hideModal = (contact: MainContact | null) => {
    if (contact && contact.id) {
      const { currentIndex, updateModalVisible } = this.state
      const visibles: Array<boolean> = [...updateModalVisible]
      visibles[currentIndex] = false
      this.setState({
        updateModalVisible: visibles,
      })
    } else {
      this.setState({
        newModalVisible: false,
      })
    }
  }

  saveAsMain = (contact: MainContact) => {
    this.props.onSaveAsMain(contact)
    this.hideModal(contact)
  }

  save = (contact: MainContact) => {
    this.props.onSave(contact)
    this.hideModal(contact)
  }

  del = (contact: MainContact) => {
    this.props.onDelete(contact)
    this.hideModal(contact)
  }

  getContactModal = (contact: MainContact | null, index: number) => {
    const { newModalVisible, updateModalVisible } = this.state
    const visible = index === -1 ? newModalVisible : updateModalVisible[index]
    return (
      <ContactModal
        visible={visible}
        contact={contact}
        saveAsMain={this.saveAsMain}
        save={this.save}
        del={this.del}
      />
    )
  }

  insertAndShirt = (arr: any[], from: number, to: number) => {
    let cutOut = arr.splice(from, 1)[0];
    arr.splice(to, 0, cutOut)
    return arr
  }

  render() {
    const { contacts, theme, search, loadingContacts } = this.props
    const { newModalVisible, updateModalVisible } = this.state
    const _this = this

    let listItems: MainContact[] = cloneDeep(contacts)

    if (search) {
      listItems = listItems.filter((item) => item.title.toUpperCase().includes(search.toUpperCase()))
    }
    const mainContactIndex = listItems.findIndex(el => el.isMain === true)
    if (mainContactIndex > -1) {
      listItems = this.insertAndShirt(listItems, mainContactIndex, 0)
    }

    const columns: ColumnProps<MainContact>[] = [
      {
        title: 'TITLE',
        dataIndex: 'title',
        key: 'title',
        width: 200,
        className: 'th-left pl-0',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'title')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('title', b, a)
          return this.onSortString('title', a, b)
        },
        render: (title: string, record: MainContact, index: number) => {
          return (
            <>
              {record.isMain && (
                <div style={{ color: theme.primary, marginRight: 5, display: 'inline-block' }}>
                  <Icon type="star" theme="filled" />
                </div>
              )}
              {title}
            </>
          )
        },
      },
      {
        title: 'NAME',
        dataIndex: 'name',
        key: 'name',
        width: 180,
        className: 'th-left pl-0',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'salutation',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('name', b, a)
          return this.onSortString('name', a, b)
        },
      },
      {
        title: 'MAIN PHONE',
        dataIndex: 'phone',
        key: 'phone',
        width: 200,
        className: 'th-left pl-0',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'phone')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('phone', b, a)
          return this.onSortString('phone', a, b)
        },
      },
      {
        title: 'MOBILE PHONE',
        dataIndex: 'mobilePhone',
        key: 'mobilePhone',
        width: 200,
        className: 'th-left pl-0',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'mobilePhone',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('mobilePhone', b, a)
          return this.onSortString('mobilePhone', a, b)
        },
      },
      {
        title: 'EMAIL',
        dataIndex: 'email',
        key: 'email',
        width: 250,
        className: 'th-left pl-0',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'email')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('email', b, a)
          return this.onSortString('email', a, b)
        },
      },
      {
        title: 'NOTES',
        dataIndex: 'notes',
        key: 'notes',
        width: 250,
        className: 'th-left pl-0',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'notes')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('notes', b, a)
          return this.onSortString('notes', a, b)
        },
      },
      {
        title: '',
        key: 'action',
        render(text: any, record: MainContact, index: number) {
          return (
            <Popover
              placement="left"
              content={_this.getContactModal(record, index)}
              trigger="click"
              visible={updateModalVisible[index]}
              onVisibleChange={_this.handleVisibleChange.bind(this, record, index)}
            >
              <ThemeButton shape="round" style={floatRight}>
                <Icon type="edit" theme="filled" />
                Edit
              </ThemeButton>
            </Popover>
          )
        },
      },
    ]

    return (
      <DetailsWrapper className='level-2-with-table'>
        <div className='action-container'>
          <Row>
            <Col md={12} style={{ padding: '4px 0' }}>
              <DetailsTitle>Contacts</DetailsTitle>
            </Col>
            <Col md={12}>
              <Popover
                placement="left"
                content={_this.getContactModal(null, -1)}
                trigger="click"
                visible={newModalVisible}
                onVisibleChange={_this.handleVisibleChange.bind(this, null, 0)}
              >
                <ThemeButton shape="round" style={floatRight} className='popover-with-buttons'>
                  <Icon type="plus-circle" theme="filled" />
                  New Contact
                </ThemeButton>
              </Popover>
            </Col>
          </Row>
        </div>
        <Layout>
          <ThemeTable
            loading={loadingContacts}
            ref={this.mutiSortTable}
            css={tableCss(false, true)}
            pagination={{ pageSize: 12 }}
            columns={columns}
            dataSource={listItems}
            rowKey="id"
            expandRowByClick
            expandIconAsCell={false}
            expandIcon={emtpyNode}
            expandedRowKeys={this.state.expandedRowKeys}
            onExpandedRowsChange={this.onExpendedRowsChange}
          />
        </Layout>
      </DetailsWrapper>
    )
  }
}

export default withTheme(ContactsContainer)
