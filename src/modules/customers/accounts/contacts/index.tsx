import * as React from 'react'
import {GlobalState} from '~/store/reducer'
import {connect} from 'redux-epics-decorator'
import {withTheme} from 'emotion-theming'
import {CustomersInfoBody, CustomersInfoHeader} from '../../components/customers-detail/customers-detail.style'

import PageLayout from '~/components/PageLayout'
import {DetailsOverview} from '../../components/details-overview'
import ContactsContainer from './contacts-container'
import {CustomersModule} from '../../customers.module'
import {MainContact} from '~/schema'
import {HeaderActions} from "~/modules/customers/accounts/header-actions/header-actions";
import {CustomerBaseComponent} from "~/modules/customers/accounts/base-component/CustomerBaseComponent";


export class Contacts extends CustomerBaseComponent {
  editItem = (item: MainContact) => {
  }

  componentDidMount() {
    const customerId = this.props.match.params.customerId
    this.props.getCustomer(customerId)
    this.props.getContacts(customerId)
  }

  onSave = (contact: MainContact) => {
    contact.wholesaleClient = {
      clientId: this.props.customer.clientId
    }
    if (contact.id) {
      // update
      this.props.updateContact(contact)
    } else {
      // create
      // contact.wholesaleCompany = customer.wholesaleCompany
      // contact.user = null
      // contact.userRole = null
      this.props.createContact(contact)
    }
  }

  onSaveAsMain = (contact: MainContact) => {
    contact.isMain = true
    this.onSave(contact)
    this.props.setMainContact(contact.id)
  }

  render() {
    const {customer, contacts, loadingContacts} = this.props

    return (
      <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
                       onCreateBlankOrder={this.onCreateBlankOrder}
                       duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}/>
        <CustomersInfoHeader>
          <DetailsOverview customer={customer}/>
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <ContactsContainer
            loadingContacts={loadingContacts}
            contacts={contacts}
            editItem={this.editItem}
            search={''}
            onSave={this.onSave}
            onSaveAsMain={this.onSaveAsMain}
            onDelete={this.props.deleteContact}
          />
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.customers
export default withTheme(connect(CustomersModule)(mapState)(Contacts))
