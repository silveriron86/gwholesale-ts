import * as React from 'react'
import jQuery from 'jquery'
import { ThemeButton, InputLabel, PopoverWrapper, InputRow, ThemeInput, ThemeTextArea, ThemeOutlineButton } from '../../customers.style'
import { FormComponentProps } from 'antd/lib/form'
import { Form, Icon, Col, Row } from 'antd'
import { mb0 } from '../../sales/_style'
import { buttonWithPopover } from '~/common/jqueryHelper'

const FormItem = Form.Item

interface DocumentModalProps extends FormComponentProps {
  document: any | null
  save: (item: any) => void
  visible: boolean
}

const defaultDocument = {
  title: '',
  type: '',
  notes: '',
  id: '',
}

class DocumentModal extends React.PureComponent<DocumentModalProps> {
  state: any
  titleInput: any
  constructor(props: DocumentModalProps) {
    super(props)
    this.state = {
      document: { ...defaultDocument },
    }
  }

  _initValues = (props: DocumentModalProps) => {
    const { document } = props

    this.setState({
      document: document ? document : { ...defaultDocument },
    })
  }

  componentDidMount() {
    this._initValues(this.props)
    if(this.titleInput) {
      this.titleInput.focus()
    }

    buttonWithPopover('button-with-popover-area')
  }

  componentWillReceiveProps(nextProps: DocumentModalProps) {
    if (this.props.visible !== nextProps.visible && nextProps.visible === true) {
      this._initValues(nextProps)
    }
  }

  onChange = (type: string, e: any) => {
    const document = { ...this.state.document }
    document[type] = e.target.value
    this.setState({
      document: document,
    })
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    const { document } = this.state
    e.preventDefault()
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const data = {
          id: document.id,
          title: values.title,
          type: values.type,
          notes: typeof values.notes === 'undefined' ? '' : values.notes,
        }
        this.props.save(data)
        form.resetFields();
      }
    })
  }

  render() {
    const { document } = this.state
    const { getFieldDecorator } = this.props.form
    return (
      <PopoverWrapper style={{ width: 350 }} className='button-with-popover-area'>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
          <InputRow>
            <Col md={4}>
              <InputLabel>TITLE</InputLabel>
            </Col>
            <Col md={20}>
              <FormItem style={mb0}>
                {getFieldDecorator('title', {
                  rules: [{ required: false, message: 'Title is required!' }],
                  initialValue: document.title,
                })(<ThemeInput ref={(input)=>{this.titleInput = input;}}/>)}
              </FormItem>
            </Col>
          </InputRow>
          <InputRow>
            <Col md={4}>
              <InputLabel>TYPE</InputLabel>
            </Col>
            <Col md={20}>
              <FormItem style={mb0}>
                {getFieldDecorator('type', {
                  rules: [{ required: false, message: 'Type is required!' }],
                  initialValue: document.type
                })(<ThemeInput />)}
              </FormItem>
            </Col>
          </InputRow>
          <InputRow>
            <Col md={4}>
              <InputLabel>NOTES</InputLabel>
            </Col>
            <Col md={20}>
              <FormItem
                style={{marginBottom: 0}}
                colon={false}
              >
                {getFieldDecorator('notes', {
                  rules: [{ required: false }],
                  initialValue: document.notes
                })(<ThemeTextArea rows={4} />)}
              </FormItem>
            </Col>
          </InputRow>
          <Row>
            <Col md={12} />
            <Col md={12}>
              <FormItem style={{marginBottom: 0}}>
                <ThemeOutlineButton shape="round" htmlType="submit">
                  <Icon type="save" />
                  Save Document
                </ThemeOutlineButton>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </PopoverWrapper>
    )
  }
}

export default Form.create<DocumentModalProps>()(DocumentModal)
