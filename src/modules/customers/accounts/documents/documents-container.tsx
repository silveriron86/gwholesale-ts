import * as React from 'react'
import jQuery from 'jquery'
import { DetailsWrapper, CustomerDetailsTitle as DetailsTitle, floatRight, ThemeButton, ThemeOutlineButton, flexStyle } from '../../customers.style'
import { Icon, Col, Layout, Row, Popover, DatePicker } from 'antd'
import { layoutStyle } from '../../components/customers-detail/customers-detail.style'
import DocumentsTable from './documents-table'
import { Document } from '~/schema'
import DocumentModal from '~/modules/customers/accounts/documents/document-modal'
import { NewOrderFormModal } from '~/modules/orders/components'
import { documentBodyKeyupEvent, Level2WithTable, onPopoverOpenWithButtons, datePickerEscKeyupEvent } from '~/common/jqueryHelper'
import { button } from '@storybook/addon-knobs'
import SearchInput from '~/components/Table/search-input'
import moment, { Moment } from 'moment'
import GrubSuggest from '~/components/GrubSuggest'
import { Theme, gray01 } from '~/common'
import { includes } from '~/common/utils'
const { RangePicker } = DatePicker

export interface DocumentsContainerProps {
  theme: Theme
  documents: Document[]
  onSave: Function
  getDocuments: Function
  buttonStatus?: boolean
  isSales?: boolean
  pageSize: number
  onlyContent?: boolean
  loading: boolean
}

class DocumentsContainer extends React.PureComponent<DocumentsContainerProps> {
  // focusedButtonIndex: number
  // btns: any[]
  state = {
    newModalVisible: false,
    updateModalVisible: [],
    currentIndex: 0,
    id: null,
    title: '',
    notes: '',
    type: '',
    search: ''
  }

  componentDidMount() {
    onPopoverOpenWithButtons('customer-new-document')
    Level2WithTable('level-2-with-table')

    datePickerEscKeyupEvent(this.handleEscKeydown)
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    if (prevProps && this.props && prevProps.documents.length != this.props.documents.length) {
      Level2WithTable('level-2-with-table')
    }
  }

  handleEscKeydown = (e: any) => {
    if (e.keyCode == 27) {
      if (this.state.newModalVisible) {
        this.setState({
          newModalVisible: false,
        })
        jQuery('.customer-new-document').trigger('focus')
      } else {
        const visibles: Array<boolean> = [...this.state.updateModalVisible]
        let hides: Array<boolean> = []
        visibles.forEach((el, index) => {
          hides.push(false)
        });
        this.setState({
          updateModalVisible: hides
        })
      }

    }
  }

  handleVisibleChange = (item: Document | null, index: number, visible: boolean) => {
    if (item) {
      const visibles: Array<boolean> = [...this.state.updateModalVisible]
      visibles[index] = visible
      this.setState({
        currentIndex: index,
        updateModalVisible: visibles,
        id: item.id,
        title: item.title,
        notes: item.notes,
        type: item.type,
      })
    } else {
      this.setState({
        newModalVisible: visible,
        id: null,
        title: '',
        notes: '',
        type: '',
      })
    }
  }

  hideModal = (document: Document | null) => {
    if (document && document.id) {
      const { currentIndex, updateModalVisible } = this.state
      const visibles: Array<boolean> = [...updateModalVisible]
      visibles[currentIndex] = false
      this.setState({
        updateModalVisible: visibles,
      })
    } else {
      this.setState({
        newModalVisible: false,
      })
    }
  }

  save = (document: any) => {
    const {documents} = this.props
    let i = 1
    while(!document.title){
      const newTitle = 'Untitled '+i
      if(documents.find(item=>item.title===newTitle)){
        ++i;
        continue;
      }
      document.title = newTitle
    }
    this.props.onSave(document)
    this.hideModal(document)
  }

  onDelete = (document: Document | null) => {
    document["delete"] = true;
    this.props.onSave(document)
    // this.hideModal(document)
  }

  onChange = (type: string, e: any) => {
    const state = { ...this.state }
    state[type] = e.target.value
    this.setState(state)
  }

  getDocumentModal = (document: Document | null, index: number) => {
    const { newModalVisible, updateModalVisible } = this.state
    const visible = index === -1 ? newModalVisible : updateModalVisible[index]
    return <DocumentModal document={document} save={this.save} visible={visible} />
  }

  onSearch = (keyword: string) => {
    this.setState(pre=> ({...pre, search: keyword}))
  }

  onSuggest = (str: string) => {
    return []
  }

  render() {
    const { newModalVisible, updateModalVisible, search } = this.state
    const { documents, buttonStatus, isSales, pageSize, onlyContent, loading } = this.props
    const data = documents.filter(item=> includes(item.title,search)||includes(item.type,search)||includes(item.notes,search))

    return (
      <DetailsWrapper className='level-2-with-table' style={{ color: gray01, padding: onlyContent ? '5px 0 20px' : 20, borderTopWidth: onlyContent ? 0 : 5 }}>
        <div className='action-container' style={{ padding: onlyContent ? 0 : isSales === true ? '0 20px' : '0 30px' }}>
          <Row>
            <Col md={24}>
              <Row style={{ paddingBottom: 20 }}>
                <Col md={12} style={{ ...flexStyle, marginTop: 0 }}>
                  {onlyContent !== true && <DetailsTitle style={{ padding: 0, marginRight: 10 }}>Documents</DetailsTitle>}
                  {/* <div style={{ position: 'absolute', marginLeft: onlyContent ? 0 : 135, marginTop: onlyContent ? 25 : 0 }}>
                    <RangePicker
                      placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                      format={'MM/DD/YYYY'}
                      defaultValue={[from, to]}
                      onChange={this.onDateChange}
                      allowClear={false}
                    />
                  </div> */}
                </Col>
                <Col md={12} style={{ ...flexStyle, justifyContent: 'flex-end', marginTop: -8 }}>
                  {/* <SearchInput
                    handleSearch={this.onSearch}
                    placeholder="Search by Keyword..."
                  /> */}
                  <GrubSuggest onSearch={this.onSearch} theme={this.props.theme} onSuggest={this.onSuggest} />
                  <Popover
                    placement="left"
                    content={this.getDocumentModal(null, -1)}
                    trigger="click"
                    visible={newModalVisible}
                    onVisibleChange={this.handleVisibleChange.bind(this, null, 0)}
                  >
                    <ThemeOutlineButton shape="round" style={{ ...floatRight, marginLeft: 10 }} className="customer-new-document sales-cart-input" disabled={buttonStatus != undefined && buttonStatus === true}>
                      <Icon type="plus-circle" theme="filled" />
                      New Document
                    </ThemeOutlineButton>
                  </Popover>
                </Col>
              </Row>
              <DocumentsTable
                onDelete={this.onDelete}
                getDocuments={this.props.getDocuments}
                data={data}
                getModal={this.getDocumentModal}
                updateModalVisible={updateModalVisible}
                handleVisibleChange={this.handleVisibleChange}
                pageSize={pageSize}
                loading={loading}
                save={this.save}
              />
            </Col>
          </Row>
        </div>
      </DetailsWrapper>
    )
  }
}

export default DocumentsContainer
