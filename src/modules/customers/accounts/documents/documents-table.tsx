import React from 'react'
import moment from 'moment'
import { ml0, ThemeTable, ThemeOutlineButton, mh10 } from '~/modules/customers/customers.style'
import { Document } from '~/schema'
import { Upload, Popover, Icon, notification, Layout, Popconfirm } from 'antd'
import { Wrapper } from '../addresses/addresses.style'
import { ColumnProps } from 'antd/es/table'
import { CACHED_ACCESSTOKEN } from '~/common'
import { getFileUrl } from '~/common/utils'

interface DocumentsTableProps {
  getDocuments: Function
  onDelete: Function
  data: Array<Document>
  getModal: Function
  updateModalVisible: Array<boolean>
  handleVisibleChange: Function
  pageSize: number
  loading: boolean
  save: (docu:any) => void
}

export class DocumentsTable extends React.PureComponent<DocumentsTableProps> {

  render() {
    const { data, getModal, updateModalVisible, handleVisibleChange, getDocuments, onDelete, pageSize, loading } = this.props

    const columns: ColumnProps<any>[] = [
      {
        title: 'CREATED DATE',
        dataIndex: 'createdDate',
        width: 180,
        align: 'center',
        render: (value: number) => {
          return moment(value).format('MM/DD/YYYY LT')
        },
      },
      {
        title: 'TITLE',
        dataIndex: 'title',
        width: 150,
        align:'center'
      },
      {
        title: 'TYPE',
        dataIndex: 'type',
        width: 150,
        align:'center'

      },
      {
        title: 'NOTES',
        dataIndex: 'notes',
        width: 350,
        align:'center'
      },
      {
        title: '',
        dataIndex: '',
        render: (_text: string, item: Document, index: number) => (
          <>
            <Popover
              placement="left"
              content={getModal(item, index)}
              trigger="click"
              visible={updateModalVisible[index]}
              onVisibleChange={handleVisibleChange.bind(this, item, index)}
            >
              <ThemeOutlineButton shape="round" style={mh10} className='sales-document-action-btn'>
                <Icon type="edit" />
                Edit
              </ThemeOutlineButton>
            </Popover>
            <div style={{ display: 'inline-block', width: 100 }}>
              <Upload {this.setUploadProps(item, getDocuments)}>
                <ThemeOutlineButton shape="round" className='sales-document-action-btn document-upload-btn' onKeyDown={(e: any) => {
                  if (e.keyCode == 13) {
                    e.stopPropagation()
                  }
                }}>
                  <Icon type="upload" />
                  Upload
                </ThemeOutlineButton>
              </Upload>
            </div>
            <span>
              <ThemeOutlineButton shape="round" style={mh10} disabled={item.url ? false : true} href={getFileUrl(item.url, false)} className='sales-document-action-btn'>
                <Icon type="download" />
                Download
              </ThemeOutlineButton>
            </span>
            <span onClick={this.viewDocument.bind(this, item.url)}>
              <ThemeOutlineButton shape="round" style={mh10} disabled={item.url ? false : true} className='sales-document-action-btn'>
                <Icon type="block" />
                View
              </ThemeOutlineButton>
            </span >

            <Popconfirm
              placement="left"
              title={"Are you sure to delete this document?"}
              onConfirm={onDelete.bind(this, item)}
              okText="Yes"
              cancelText="No"
            >
              <span>
                <ThemeOutlineButton shape="round" style={mh10} disabled={item.url ? false : true} className='sales-document-action-btn'>
                  <Icon type="delete" />
                  Delete
                </ThemeOutlineButton>
              </span >
            </Popconfirm>
          </>
        ),
      },
    ]

    return (
      <Layout style={ml0} className='level-2-with-table'>
        <ThemeTable
          pagination={{
            hideOnSinglePage: true,
            pageSize: pageSize,
            total: data.length,
            defaultCurrent: 1
          }}
          loading={loading}
          columns={columns}
          dataSource={data}
          rowKey="id"
        />
      </Layout>
    )
  }

  private setUploadProps = (item, getDocuments) => {
    const { save } = this.props
    return {
      name: 'file',
      action: process.env.WSW2_HOST + "/api/v1/wholesale/file/document/" + item.id,
      showUploadList: false,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN)}`,
      },
      data: { "item": item },
      onChange(info: any) {
        if (info.file.status !== 'uploading') {
          // console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done' && info.file.response.statusCodeValue == 200) {
          notification['success']({
            message: 'Success',
            description: `${info.file.name} file uploaded successfully`,
            duration: 5
          })
          if(/Untitled /.test(item.title)){
            save({...item, title: info.file.name})
          } else{
            getDocuments(item.wholesaleOrder ? item.wholesaleOrder.wholesaleOrderId : item.wholesaleClient.clientId);
          }
        } else if (info.file.status === 'error' || (info.file.status === 'done' && info.file.response.statusCodeValue != 200)) {
          notification['error']({
            message: 'Error',
            description: `${info.file.name} file upload failed`,
          })
        }
      },
    }
  }

  private viewDocument = (documentUrl: string) => {
    const lastIndex = documentUrl.lastIndexOf('.')
    const extension = documentUrl.substr(lastIndex + 1)
    const imageTypes = ['jpg', 'jpeg', 'png', 'tiff', 'bmp', 'gif']
    let url = getFileUrl(documentUrl, false)
    if (imageTypes.indexOf(extension.toLowerCase()) > -1) {
      let newtab = window.open()
      newtab.document.body.innerHTML = `<div style='width: 100vw; height: 100vh; display:flex;align-items: center; '><img src="${url}" style="margin:auto"/></div>`
    } else {
      window.open(`https://docs.google.com/viewerng/viewer?url=${url}`)
    }
  }

  private downloadDocument = (documentUrl: string) => {
    let url = getFileUrl(documentUrl, false)
    fetch(url).then(res => res.blob()).then(blob => {
      var a = document.createElement('a');
      var url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = documentUrl.substring(documentUrl.lastIndexOf('/') + 1);
      a.click();
      window.URL.revokeObjectURL(url);
    })
  }
}

export default DocumentsTable
