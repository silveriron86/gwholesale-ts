import * as React from 'react'
import {GlobalState} from '~/store/reducer'
import {connect} from 'redux-epics-decorator'
import {withTheme} from 'emotion-theming'
import {CustomersInfoBody, CustomersInfoHeader} from '../../components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import {DetailsOverview} from '../../components/details-overview'
import DocumentsContainer from './documents-container'
import {CustomersModule} from '../../customers.module'
import {Document} from '~/schema'
import moment from 'moment'
import {HeaderActions} from "~/modules/customers/accounts/header-actions/header-actions";
import {CustomerBaseComponent} from "~/modules/customers/accounts/base-component/CustomerBaseComponent";

export class Documents extends CustomerBaseComponent {
  state = {
    from: moment().subtract(30, 'days'),
    to: moment(),
    curPage: 0,
    pageSize: 12,
    search: ''
  }

  componentDidMount() {
    const customerId = this.props.match.params.customerId;
    this.props.getCustomer(customerId)
    this.props.resetDocuments()
    this.getCustomerDocument()
  }

  // componentWillReceiveProps(nextProps: DocumentsProps) {
  //   if (nextProps.message !== this.props.message) {
  //     if (nextProps.type != null) {
  //       notification[nextProps.type!]({
  //         message: nextProps.type!.toLocaleUpperCase(),
  //         description: nextProps.message,
  //         onClose: nextProps.resetNotif,
  //       })
  //     }
  //   }
  // }

  getCustomerDocument = () => {
    const customerId = this.props.match.params.customerId
    const searchObj = {
      // from: this.state.from.format('MM/DD/YYYY'),
      // to: this.state.to.format('MM/DD/YYYY'),
      search: this.state.search,
      // curPage: this.state.curPage,
      // pageSize: this.state.pageSize,
    }
    this.props.setDocumentSearchProps(searchObj)
    this.props.getDocuments({...searchObj, customerId: customerId})
  }

  onSave = (data: Document) => {
    if (data.id) {
      this.props.updateDocument(data)
    } else {
      this.props.createDocument(data)
    }
  }

  onChange = (type: string, data: any) => {
    if (type == 'search') {
      this.setState({search: data}, this.getCustomerDocument)
    }
  }

  render() {
    const {customer, documents, documentsLoading} = this.props
    const {pageSize} = this.state
    return (
      <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
                       onCreateBlankOrder={this.onCreateBlankOrder}
                       duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}/>
        <CustomersInfoHeader>
          <DetailsOverview customer={customer}/>
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <DocumentsContainer
            loading={documentsLoading}
            getDocuments={this.getCustomerDocument}
            documents={documents}
            onSave={this.onSave}
            pageSize={pageSize}
            theme={this.props.theme}/>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.customers
export default withTheme(connect(CustomersModule)(mapState)(Documents))
