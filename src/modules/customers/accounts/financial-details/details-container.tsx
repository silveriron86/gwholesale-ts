import * as React from 'react'
import {
  DetailsWrapper,
  DetailsTitle,
  pd20,
  InputLabel,
  DetailsRow,
  ml0,
  ThemeInput,
  ThemeSelect, ThemeButton
} from '../../customers.style'
import { Layout, Row, Col, Icon, Select, Popconfirm } from 'antd'
import {
  layoutStyle,
  Flex,
  CardTextWrapper,
  DetailWrapperStyle,
  transLayout,
} from '../../components/customers-detail/customers-detail.style'
import { formatNumber } from '~/common/utils'
import OverviewSection from '../../components/overview-section'
import { TempCustomer } from '~/schema'
import moment from 'moment'
import { onLoadFocusOnFirst } from '~/common/jqueryHelper'
import Form, { FormComponentProps } from 'antd/lib/form'
import { CACHED_NS_LINKED, CACHED_QBO_LINKED, FINANCIAL_TERMS, TAX_STATUSES } from '~/common'

interface FormFields {
  creditLimit: string,
  creditRating: string,
  servicesProvided: string,
  creditMonthlyInterestRate: string,
  seller: any
}


type FinancialDetailsContainerProps = FormComponentProps<FormFields> & {
  customer: TempCustomer
  companyProductTypes: any
  onRefresh: () => void
}


export class FinancialDetails extends React.PureComponent<FinancialDetailsContainerProps> {
  state = {
    creditLimit: '',
    creditRating: '',
    servicesProvided: '',
    creditMonthlyInterestRate: ''
  }
  constructor(props: any) {
    super(props)
  }

  componentDidMount() {
    onLoadFocusOnFirst('simple-input-wrapper')
  }

  componentWillReceiveProps(nextProps: any) {
    const { customer } = this.props
    if (customer) {
      this.setState({
        creditLimit: customer.creditLimit,
        creditRating: customer.creditRating,
        servicesProvided: customer.servicesProvided,
        creditMonthlyInterestRate: customer.creditMonthlyInterestRate
      })
    }
  }

  handleChange = (type: string, e: any) => {
    this.handleChangeValue(type, e.target.value)
  }

  handleChangeValue = (type: string, value: any) => {
    const state = { ...this.state }
    state[type] = value
    this.setState(state)
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values)
        this.props.updateCurrentCustomer(values);
      }
    })
  }

  render() {
    const { customer, currentCompanyUsers, form: { getFieldDecorator } } = this.props
    const { onRefresh } = this.props
    const { companyProductTypes } = this.props
    const qboId = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    let paymentTerms = companyProductTypes != null ? (localStorage.getItem(CACHED_QBO_LINKED) != 'null' ? companyProductTypes.QBOpaymentTerms : companyProductTypes.paymentTerms) : []

    return (
      <DetailsWrapper className='simple-input-wrapper'>
        <DetailsTitle>Financial Details</DetailsTitle>
        <Layout style={transLayout}>
          <DetailWrapperStyle type="flex" justify="space-between" style={{ margin: '0 0 20px -10px', padding: 15 }}>
            <Col md={18}>
              <Layout style={{ background: 'transparent' }}>
                <Row type="flex" justify="space-between">
                  <Col xs={24} sm={12} md={8}>
                    <OverviewSection
                      active={false}
                      label="OPEN BALANCE"
                      timestamp={"Updated " + moment(customer.lastQBOUpdate).fromNow()}
                      value={customer.openBalance != null ? `${formatNumber(customer.openBalance, 2)}` : ''}
                      style={ml0}
                    />
                  </Col>
                  <Col xs={24} sm={12} md={8}>
                    <OverviewSection
                      active={true}
                      label="OVERDUE"
                      timestamp={"Updated " + moment(customer.lastQBOUpdate).fromNow()}
                      value={customer.overdue != null ? `${formatNumber(customer.overdue, 2)}` : ''}
                    />
                  </Col>
                  <Col xs={24} sm={12} md={8}>
                    <OverviewSection active={false} label="PAYMENT TERMS" value={customer.paymentTerm} />
                  </Col>
                </Row>
              </Layout>
              <Flex style={{ marginTop: 10 }}>
                <ThemeButton shape="round" onClick={onRefresh} >
                  <Icon type="reload" theme="filled" />
                  Refresh
                </ThemeButton>
                <CardTextWrapper active={false} style={{ margin: '12px 0 0 10px' }}>
                  Updated {moment(customer.lastQBOUpdate).fromNow()}
                </CardTextWrapper>
              </Flex>
            </Col>
          </DetailWrapperStyle>
        </Layout>
        <Layout style={layoutStyle}>
          <Form onSubmit={this.handleSubmit}>
            {(qboId || (!qboId && !nsRealmId)) &&
              <Row style={DetailsRow}>
                <Col lg={6} md={12} style={pd20}>
                  <InputLabel>CREDIT LIMIT</InputLabel>
                  {customer && (
                    <Form.Item>
                      {getFieldDecorator('creditLimit', {
                        initialValue: customer.creditLimit
                      })(
                        <ThemeInput />
                      )}
                    </Form.Item>
                  )}
                </Col>
                <Col lg={6} md={12} style={pd20}>
                  <InputLabel>CREDIT RATING</InputLabel>
                  {customer && (
                    <Form.Item>
                      {getFieldDecorator('creditRating', {
                        initialValue: customer.creditRating
                      })(
                        <ThemeInput />
                      )}
                    </Form.Item>
                  )}
                </Col>
                {/* <Col lg={6} md={12} style={pd20}>
                <InputLabel>SERVICES PROVIDED</InputLabel>
                {customer && (
                  <Form.Item>
                    {getFieldDecorator('servicesProvided', {
                      initialValue: customer.servicesProvided
                    })(
                      <ThemeInput />
                    )}
                  </Form.Item>
                )}
              </Col> */}
                <Col lg={6} md={12} style={pd20}>
                  <InputLabel>CREDIT MONTHLY INTEREST RATE</InputLabel>
                  {customer && (
                    <Form.Item>
                      {getFieldDecorator('creditMonthlyInterestRate', {
                        initialValue: customer.creditMonthlyInterestRate
                      })(
                        <ThemeInput />
                      )}
                    </Form.Item>
                  )}
                </Col>
              </Row>
            }
            <Row style={DetailsRow}>
              <Col lg={6} md={12} style={pd20}>
                <InputLabel>PAYMENT TERMS</InputLabel>
                <Form.Item>
                  {getFieldDecorator('paymentTerm', {
                    rules: [{ required: false }],
                    initialValue: customer.paymentTerm
                  })(
                    <ThemeSelect
                      style={{ width: '100%' }}
                      placeholder="Payment Terms"
                    >
                      {paymentTerms.map((item: any) => (
                        <Option key={`customer-term-${item.name}`} value={item.name}>
                          {item.name}
                        </Option>
                      )}
                    </ThemeSelect>
                  )}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} style={pd20}>
                <InputLabel>TAX STATUS</InputLabel>
                <Form.Item>
                  {getFieldDecorator('taxable', {
                    rules: [{ required: false }],
                    initialValue: typeof customer.taxable !== 'undefined' && customer.taxable === true ? true : false
                  })(
                    <ThemeSelect
                      style={{ width: '100%' }}
                    >
                      {TAX_STATUSES.map((item) => (
                        <Option key={`tax-status-${item}`} value={item.value}>
                          {item.label}
                        </Option>
                      ))}
                    </ThemeSelect>
                  )}
                </Form.Item>
              </Col>
            </Row>
            {/*
            <Row style={DetailsRow}>
              <Col lg={6} md={12} style={pd20}>
                <InputLabel>SALES REPRESENTATIVE</InputLabel>
                {currentCompanyUsers && customer.seller && (
                  <Form.Item>
                    {getFieldDecorator('seller', {
                      initialValue: customer.seller.userId,
                    })(
                      <ThemeSelect style={{ width: '100%' }}>
                        {currentCompanyUsers.map((item, index) => {
                          return (
                            <Select.Option key={index} value={item.userId}>
                              {item.firstName + "  " + item.lastName}
                            </Select.Option>
                          )
                        })}
                      </ThemeSelect>
                    )}
                  </Form.Item>
                )}
              </Col>
            </Row>
            */}
            <Row>
              <Col>
                {/* <Popconfirm
                  title="Are you sure to save?"
                  onConfirm={this.handleSubmit}
                  okText="Save"
                  cancelText="Cancel"
                > */}
                {/* <ThemeButton shape="round" type="primary" style={{ width: 200, marginBottom: 20 }} onClick={this.onSave.bind(this)}> */}
                <ThemeButton shape="round" type="primary" htmlType="submit" style={{ width: 200, marginBottom: 20 }}>
                  <Icon type="save" theme="filled" />
                  Save Financial Details
                </ThemeButton>
                {/* </Popconfirm> */}
              </Col>
            </Row>
          </Form>
        </Layout>
      </DetailsWrapper>
    )
  }
}

const FinancialDetailsContainer = Form.create()(FinancialDetails)
export default FinancialDetailsContainer
