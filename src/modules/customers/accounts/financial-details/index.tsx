import * as React from 'react'
import {withTheme} from 'emotion-theming'
import {Theme} from '~/common'
import {CustomersInfoBody, CustomersInfoHeader} from '../../components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import {DetailsOverview} from '../../components/details-overview'
import FinancialDetailsContainer from './details-container'
import {GlobalState} from '~/store/reducer'
import {connect} from 'redux-epics-decorator'
import {CustomersDispatchProps, CustomersModule, TempCustomersStateProps} from '../../customers.module'
import {HeaderActions} from "~/modules/customers/accounts/header-actions/header-actions";
import {CustomerBaseComponent} from "~/modules/customers/accounts/base-component/CustomerBaseComponent";
import { Theme, CACHED_NS_LINKED, CACHED_QBO_LINKED } from '~/common'

export class FinancialDetails extends CustomerBaseComponent {

  componentDidMount(): void {
    const customerId = this.props.match.params.customerId
    this.props.getCustomer(customerId)
    this.props.getCompanyUsers()
    this.props.getCompanyProductAllTypes()
  }

  private onRefresh = () => {
    if (localStorage.getItem(CACHED_NS_LINKED) != 'null')
      this.props.getLatestNSBalance(this.props.customer.clientId)
    if (localStorage.getItem(CACHED_QBO_LINKED) != 'null')
      this.props.getLatestQBOBalance(this.props.customer.clientId)
  }


  render() {
    const {customer} = this.props
    return (
      <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
                       onCreateBlankOrder={this.onCreateBlankOrder}
                       duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}/>
        <CustomersInfoHeader>
          <DetailsOverview customer={customer}/>
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <FinancialDetailsContainer onRefresh={this.onRefresh} {...this.props}/>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.customers
export default withTheme(connect(CustomersModule)(mapState)(FinancialDetails))
