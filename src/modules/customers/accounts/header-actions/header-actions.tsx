import React from 'react'
import { FlexDiv, Item } from '~/modules/customers/nav-sales/styles'
import { ThemeOutlineButton } from '~/modules/customers/customers.style'
import { Icon } from 'antd'
import { Icon as IconSvg } from '~/components'

interface HeaderActionsProps {
  onDuplicateMostRecentOrder: () => void
  onCreateBlankOrder: () => void
  duplicateMostRecentOrderLoading: boolean
}

export class HeaderActions extends React.PureComponent<HeaderActionsProps> {
  _onCopy = () => {
    const { onDuplicateMostRecentOrder } = this.props
    onDuplicateMostRecentOrder && onDuplicateMostRecentOrder()
  }

  _onCreate = () => {
    const { onCreateBlankOrder } = this.props
    onCreateBlankOrder && onCreateBlankOrder()
  }

  render() {
    return (
      <FlexDiv style={{ position: 'absolute', right: 11, marginTop: -48, zIndex: 1000 }}>
        <Item className="header-btns">
          <ThemeOutlineButton size="large" onClick={this._onCopy} loading={this.props.duplicateMostRecentOrderLoading} >
            <IconSvg
              type="duplicate"
              viewBox={void 0}
              style={{ width: 24, height: 24, marginRight: 4, fill: 'transparent' }}
            />
            Duplicate Most Recent Order
          </ThemeOutlineButton>
        </Item>
        <Item className="header-btns right">
          <ThemeOutlineButton size="large" onClick={this._onCreate}>
            <Icon type="plus" /> Create Blank Order
          </ThemeOutlineButton>
        </Item>
      </FlexDiv>
    )
  }
}
