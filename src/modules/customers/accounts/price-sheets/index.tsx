import React from 'react'
import {Button, Checkbox, Dropdown, Icon, Menu, notification} from 'antd'
import {withTheme} from 'emotion-theming'

import {Icon as IconSvg} from '~/components'

import {CustomerPriceSheet, CustomersModule,} from '../../customers.module'
import {connect} from 'redux-epics-decorator'
import {GlobalState} from '~/store/reducer'

import {
  AddButtonWrap,
  addToButtonStyle,
  HeaderOptionWrap,
  OptionContainer,
  PriceSheetWrap,
  TableHeader,
} from '../../components/customer-detail/customer-detail.style'
import {CustomerStatus} from '~/schema'
import PageLayout from '~/components/PageLayout'
import DetailsOverview from '../../components/details-overview'
import {CustomersInfoBody, CustomersInfoHeader} from '../../components/customers-detail/customers-detail.style'
import {ThemeCheckBox, ThemeIcon} from '../../customers.style'
import {CustomerPricingTable} from './pricing-table'
import {PriceSheetListRow} from '~/modules/pricing/component/pricesheet-list-item'
import {
  ListBody,
  ListHeader,
  PriceSheetWrap as PricingWrap,
  ProductPriceWrapper,
} from '~/modules/pricesheet/components/pricesheets-table.style'
import {history} from "~/store/history";
import {HeaderActions} from "~/modules/customers/accounts/header-actions/header-actions";
import {CheckboxChangeEvent} from "antd/es/checkbox";
import {CustomerBaseComponent} from "~/modules/customers/accounts/base-component/CustomerBaseComponent";
//
// type CustomerPriceSheetsProps = CustomersDispatchProps &
//   TempCustomersStateProps &
//   RouteComponentProps<{ customerId: string }> & {
//   theme: Theme
// }

export class CustomerPriceSheets extends CustomerBaseComponent {

  readonly state = {
    currentOrderIndex: -1,
    showOrder: true,
    editingInfo: false,
    editValues: {},
    message: '',
    type: null,
    sortDirection: 'asc',
    sortKey: 'priceSheetId',
  }

  componentDidMount() {
    const customerId = this.props.match.params.customerId
    this.props.getCustomer(customerId)
    this.props.getPriceSheet(customerId)
    this.props.getSellerPriceSheet()
    // console.log(moment().subtract(30, "days").format("MM/DD/YYYY"));
    // console.log(moment().add(30, "days").format("MM/DD/YYYY"));
    // this.props.getMostRecentOrder(customerId);
  }

  // componentWillReceiveProps(nextProps: CustomerPriceSheetsProps) {
  // if (nextProps.message !== this.state.message) {
  //   this.setState({
  //     message: nextProps.message,
  //     type: nextProps.type,
  //   })
  // if (nextProps.type != null) {
  //   notification[nextProps.type!]({
  //     message: nextProps.type!.toLocaleUpperCase(),
  //     description: nextProps.message,
  //     onClose: nextProps.resetLoading,
  //   })
  // }
  // }
  // }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Go to Pricesheet below to place a new order',
      onClick: () => {
        //console.log('Notification Clicked!')
      },
    })
  }

  onAssignPriceSheet = ({key}: { key: string }) => {
    this.props.assignPriceSheet(+key)
  }

  onUnassignPriceSheet = (priceSheet: CustomerPriceSheet) => () => {
    this.props.unAssignPriceSheet(priceSheet.priceSheetClientId)
  }

  onToggleOrder = () => {
    const {showOrder} = this.state
    this.setState({
      showOrder: !showOrder,
    })
  }

  onClickEditInfo = () => {
    this.setState({
      editingInfo: true,
    })
  }

  onClickActiveIcon = () => {
    const {customer} = this.props
    customer.status === CustomerStatus.ACTIVE
      ? this.setState(
      {
        editValues: {
          status: CustomerStatus.INACTIVE,
        },
      },
      () => this.props.updateCurrentCustomer(this.state.editValues),
      )
      : this.setState(
      {
        editValues: {
          status: CustomerStatus.ACTIVE,
        },
      },
      () => this.props.updateCurrentCustomer(this.state.editValues),
      )
  }

  onClickDeleteIcon = () => {
    this.setState(
      {
        editValues: {
          status: CustomerStatus.DELETE,
        },
      },
      () => this.props.updateCurrentCustomer(this.state.editValues),
    )
  }

  onSwitchDisplayPriceSheetPrice = (e: CheckboxChangeEvent) => {
    this.props.resetLoading();
    const displayPriceSheetPrice = e.target.checked ? 0 : 1;
    this.props.updateCurrentCustomer({displayPriceSheetPrice: displayPriceSheetPrice});
  }

  // onDuplicateMostRecentOrder = () => {
  //   const {customerId} = this.props.match.params;
  //   customerId && this.props.duplicateMostRecentOrder(customerId);
  // };
  //
  // onCreateBlankOrder = () => {
  //   const {routes, match, customer, sellerSetting} = this.props
  //   if (customer && customer.creditLimit > 0 && customer.openBalance > customer.creditLimit) {
  //     notification.error({
  //       message: 'ERROR',
  //       description: 'Customer open balance is greater than credit limit',
  //       onClose: () => {
  //       },
  //     })
  //     return
  //   }
  //
  //   const fulfillmentDate = getFulfillmentDate(sellerSetting)
  //
  //   let data = {
  //     wholesaleClientId: match.params.customerId,
  //     deliveryDate: fulfillmentDate,
  //     orderDate: moment().format('MM/DD/YYYY'),
  //     itemList: [],
  //   }
  //
  //   let activeRoutes: WholesaleRoute[] = []
  //   if (routes.length > 0) {
  //     routes.forEach((route) => {
  //       const weekDay = moment()
  //         .format('dddd')
  //         .toUpperCase()
  //       if (route.activeDays.indexOf(weekDay) >= 0) {
  //         activeRoutes.push({...route})
  //       }
  //     })
  //
  //     if (activeRoutes.length > 0) {
  //       data.defaultRoute = activeRoutes[0].id
  //     }
  //   }
  //   this.props.createEmptyOrder(data)
  // }


  render() {

    const {customer, priceSheets, sellerPriceSheets} = this.props;

    if (!customer) return null
    return (
      <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
                       onCreateBlankOrder={this.onCreateBlankOrder}
                       duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}/>
        <CustomersInfoHeader>
          <DetailsOverview customer={customer}/>
        </CustomersInfoHeader>
        <CustomersInfoBody>{this.renderPriceSheet(priceSheets, sellerPriceSheets)}</CustomersInfoBody>
      </PageLayout>
    )
  }

  resortData = (sortKey: string, sortDirection: string) => {
    this.setState({sortKey, sortDirection})
  }

  private renderPriceSheet(CustomerPriceSheets: CustomerPriceSheet[] | undefined, sellerCustomerPriceSheets: any[]) {
    const {sortDirection, sortKey} = this.state
    const {customer} = this.props
    const AddToOverlay =
      sellerCustomerPriceSheets.length > 0 ? (
        <Menu style={{height:300, overflow:'auto'}} >
          {sellerCustomerPriceSheets.map((p) => (
            <Menu.Item onClick={this.onAssignPriceSheet} key={p.priceSheetId}>
              {p.name}
            </Menu.Item>
          ))}
        </Menu>
      ) : (
        <Menu>
          {['N/A'].map((p) => (
            <Menu.Item key={p}>{p}</Menu.Item>
          ))}
        </Menu>
      )


    return (
      <PriceSheetWrap>
        <TableHeader className="default-pricesheet-title">
          <span style={{ fontSize: 20 }}>Price Sheets</span>
          <HeaderOptionWrap>
            <Dropdown placement="bottomCenter" overlay={AddToOverlay}>
              <Button
                type="ghost"
                style={{
                  ...addToButtonStyle,
                  border: `1px solid ${this.props.theme.primary}`,
                }}
              >
                <AddButtonWrap>
                  <div>
                    <Icon type="plus"/>
                    <span>Add customer to price sheet...</span>
                  </div>
                  <ThemeIcon type="caret-down"/>
                </AddButtonWrap>
              </Button>
            </Dropdown>
          </HeaderOptionWrap>
        </TableHeader>
        <OptionContainer>
          <ThemeCheckBox checked={!customer.displayPriceSheetPrice} onChange={this.onSwitchDisplayPriceSheetPrice}>Do not
            display prices on price sheet</ThemeCheckBox>
        </OptionContainer>
        {/*// inset checkbox to here*/}
        <TableHeader className="box-shadow-header">
          <ListHeader style={{width: '90%'}}>
            <PriceSheetListRow key={-1} isHeader={true} sortByKeyAndDirection={this.resortData}/>
          </ListHeader>
        </TableHeader>

        <CustomerPricingTable priceSheets={CustomerPriceSheets} sortDirection={sortDirection} sortKey={sortKey}
                              clientId={this.props.match.params.customerId}/>

      </PriceSheetWrap>
    );
  }
}

const mapState = (state: GlobalState) => state.customers

export const CustomerPriceSheetsContainer = withTheme(connect(CustomersModule)(mapState)(CustomerPriceSheets))
