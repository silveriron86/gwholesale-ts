/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { withTheme } from 'emotion-theming'

import { PriceSheet } from '~/schema'

import _, { cloneDeep } from 'lodash'
import { Theme } from '~/common';

import { PriceSheetWrap, ListBody } from '~/modules/pricesheet/components/pricesheets-table.style'
import { PriceSheetListRow } from './../../../pricing/component/pricesheet-list-item'

export interface PSTableProps {
  priceSheets: PriceSheet[] | undefined
  sortDirection: 'asc' | 'desc' | boolean
  sortKey: string
  clientId: string
}

type PricingTableProps = PSTableProps & { theme: Theme }

export class CustomerPricingTableComponent extends React.PureComponent<PricingTableProps>
{

  renderPriceSheetList = () => {
    const { priceSheets, sortKey, sortDirection, clientId } = this.props

    let priceSheetList = priceSheets ? cloneDeep(priceSheets.filter(el=>!el.isDefault)) : []
    let key = sortKey
    if(sortKey == 'latestOrder')
      key = 'lastOrder.updatedDate'
    priceSheetList = _.orderBy(priceSheetList, [key],[sortDirection])
    let result: any[] = []

    priceSheetList.forEach((el, index) => {
    result.push(
      <PriceSheetListRow
        key={index}
        currentPriceSheet={el}
        clientId={clientId}
        isHeader={false}
      />
    )
    });
    return result
  }

  render() {
    return (
      <PriceSheetWrap className='customer-pricing-list'>
        <ListBody>
          {this.renderPriceSheetList()}
        </ListBody>
      </PriceSheetWrap>
    )
  }
}



export const CustomerPricingTable = withTheme(CustomerPricingTableComponent)
