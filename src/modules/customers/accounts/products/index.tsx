import React, { useEffect, useState } from 'react'
import { GlobalState } from '~/store/reducer'
import PageLayout from '~/components/PageLayout'
import { connect } from 'redux-epics-decorator'
import { CustomersModule } from '../..'
import { useParams } from 'react-router'
import { Redirect } from 'react-router'
import { PriceSheet } from '~/schema'

const CustomerProducts = (props: any) => {
  const { priceSheets = [], getPriceSheet, resetPricesheet } = props
  const { customerId } = useParams<{ customerId: any }>()
  const [defaultPriceSheet, setDefaultPriceSheet] = useState<PriceSheet | undefined>()

  useEffect(() => {
    if (!priceSheets.length) {
      getPriceSheet(customerId)
      return
    }

    const defaultItem = priceSheets.find((item: any) => item.isDefault)
    if (defaultItem?.wholesaleClientId != customerId) {
      resetPricesheet()
      getPriceSheet(customerId)
    } else {
      setDefaultPriceSheet(defaultItem)
    }
    // ignore: getPriceSheet, resetPricesheet
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customerId, priceSheets])

  if (defaultPriceSheet?.priceSheetId) {
    return <Redirect to={`/pricing/${defaultPriceSheet.priceSheetId}/default-pricing/${customerId}`} />
  }

  return (
    <PageLayout currentTopMenu={'menu-Selling & Shipping-Customers'}>
      <div style={{ marginTop: 80 }}>Loading...</div>
    </PageLayout>
  )
}

const CustomerProductsPage = connect(CustomersModule)((state: GlobalState) => state.customers)(CustomerProducts)

export default CustomerProductsPage
