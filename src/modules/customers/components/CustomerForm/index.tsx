import React from 'react'
import { Row, Col, Form, Input, Select, Icon, Button } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import { withTheme } from 'emotion-theming'

import { DialogHeader, DialogBody, DialogFooter, buttonStyle } from '../../customers.style'
import { black, CACHED_QBO_LINKED, Theme } from '~/common'
import { TempCustomer } from '~/schema'

export interface CustomerFormFields {
  email: string
  name: string
  phone: string
  address1: string
  address2: string
  city: string
  state: string
  zipcode: string
  type: string
  status: string
  company: string
}

type CustomerFormProps = FormComponentProps<CustomerFormFields> & {
  customerData: TempCustomer
  theme: Theme
  onImport?: () => void
  onSubmit: (payload: any) => void
  onCancel: () => void
  type?: string
  isCustomer: boolean
}

class CustomerForm extends React.PureComponent<CustomerFormProps> {
  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const { form, onSubmit } = this.props
    form.validateFields((err, values: any) => {
      if (!err) {
        if (!values.mainContact.name) {
          values.mainContact.name = values.company
        }
        onSubmit(values)
      }
    })
  }

  render() {
    const {
      onCancel,
      onImport,
      customerData,
      form: { getFieldDecorator },
    } = this.props
    const typeLabel = this.props.isCustomer ? 'Customer' : 'Vendor'
    const qboRealmId = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    return (
      <Form onSubmit={this.handleSubmit}>
        <DialogHeader>
          <p>{this.props.type === 'update' ? 'Update ' : 'New '}{typeLabel}</p>
          {this.props.type !== 'update' && qboRealmId && <div onClick={onImport}>Import from Quickbooks</div>}
        </DialogHeader>
        <DialogBody>
          <Row style={{ width: '100%' }}>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator('company', {
                  initialValue: customerData.clientCompany.companyName,
                  rules: [{ required: true, message: `${this.props.isCustomer ? 'Account' : 'Vendor'} Name is required!` }],
                })(<Input placeholder={`${this.props.isCustomer ? 'Account' : 'Vendor'} Name`} style={{ width: '335px' }} />)}
              </Form.Item>

              <Form.Item>
                {getFieldDecorator('mainContact.name', {
                  initialValue: customerData.mainContact.name,
                })(<Input placeholder="Contact Name" style={{ width: '335px' }} />)}
              </Form.Item>
              {/* <Form.Item>
                {getFieldDecorator('mainAddress.address.street1', {
                  initialValue: customerData.mainAddress ? customerData.mainAddress.address.street1 : '',
                })(<Input placeholder="ADDRESS LINE 1" style={{ width: '335px', marginTop: '80px' }} />)}
              </Form.Item>

              <Form.Item>
                {getFieldDecorator('mainAddress.address.street2', {
                  initialValue: customerData.mainAddress ? customerData.mainAddress.address.street2 : '',
                })(<Input placeholder="ADDRESS LINE 2" style={{ width: '335px' }} />)}
              </Form.Item>

              <Form.Item>
                {getFieldDecorator('mainAddress.address.city', {
                  initialValue: customerData.mainAddress ? customerData.mainAddress.address.city : '',
                })(<Input placeholder="CITY" style={{ width: '335px' }} />)}
              </Form.Item>

              <Row>
                <Col span={4}>
                  <Form.Item>
                    {getFieldDecorator('mainAddress.address.state', {
                      initialValue: customerData.mainAddress ? customerData.mainAddress.address.state : '',
                    })(
                      <Input
                        placeholder="STATE"
                        // suffix={<Icon type="caret-down" />}
                        style={{ width: '90px' }}
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col offset={4} span={8}>
                  <Form.Item>
                    {getFieldDecorator('mainAddress.address.zipcode', {
                      initialValue: customerData.mainAddress ? customerData.mainAddress.address.zipcode : '',
                    })(<Input placeholder="ZIPCODE" style={{ width: '158px' }} />)}
                  </Form.Item>
                </Col>
              </Row>*/}
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator('mainContact.email', {
                  initialValue: customerData.mainContact.email,
                  rules: [
                    { type: 'email', message: 'Invalid email address' },
                  ],
                })(<Input placeholder="Email Address" style={{ width: '335px' }} />)}
              </Form.Item>

              {/*<Form.Item>
                {getFieldDecorator('mobilePhone', {
                  initialValue: customerData.mobilePhone,
                  rules: [
                    {
                      pattern: /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/,
                      message: 'American Phone number is required!',
                    },
                  ],
                })(<Input placeholder="PHONE NUMBER" style={{ width: '335px' }} />)}
              </Form.Item>

              <Form.Item>
                {getFieldDecorator('type', {
                  initialValue: customerData.type,
                })(
                  <Select
                    style={{ width: '335px', marginTop: '80px' }}
                    placeholder="CUSTOMER TYPE"
                    suffixIcon={<Icon style={{ color: black }} type="caret-down" />}
                  >
                    {['Individual', 'Business', 'Grocery', 'Restaurant'].map((value) => (
                      <Select.Option key={value} value={value.toLowerCase()}>
                        {value}
                      </Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>

              <Input
                placeholder="CUSTOMER TYPE"
                suffix={<Icon type="caret-down" />}
                style={{ width: '335px', marginTop: '80px' }}
                onChange={this.onChangeCreateValue('type')}
              />
              <Input
                value={editingCustomer.status}
                placeholder="STATUS"
                suffix={<Icon type="caret-down" />}
                style={{ width: '168px' }}
                onChange={this.onChangeCreateValue('status')}
              />

              <Form.Item>
                {getFieldDecorator('status', {
                  initialValue: customerData.status,
                })(
                  <Select
                    style={{ width: '168px', marginTop: '33px' }}
                    placeholder="STATUS"
                    suffixIcon={<Icon style={{ color: black }} type="caret-down" />}
                  >
                    {['Active', 'Inactive'].map((value) => (
                      <Select.Option key={value} value={value.toLowerCase()}>
                        {value}
                      </Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>*/}

              <Form.Item>
                {getFieldDecorator('dba', {
                  initialValue: customerData.dba,
                  // rules: [{ required: true, message: 'DBA Name is required!' }],
                })(<Input placeholder="DBA Name" style={{ width: '335px' }} />)}
              </Form.Item>
            </Col>
          </Row>
        </DialogBody>
        <DialogFooter>
          <Button
            type="primary"
            style={{
              ...buttonStyle,
              border: `1px solid ${this.props.theme.primary}`,
              backgroundColor: this.props.theme.primary,
            }}
            htmlType="submit"
          >
            {this.props.type === 'update' ? `Update ${typeLabel}` : `CREATE`}
          </Button>
          <div onClick={onCancel}>CANCEL</div>
        </DialogFooter>
      </Form>
    )
  }
}

export default withTheme(Form.create<CustomerFormProps>()(CustomerForm))
