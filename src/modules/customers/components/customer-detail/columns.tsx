import React from 'react'
import { ColumnProps } from 'antd/es/table'
import { Link } from 'react-router-dom'
import { PriceItem, OrderStatus } from './customer-detail.style'
import { Menu, Dropdown, Icon } from 'antd'
import { QBOImage } from '~/modules/orders/components/orders-table.style'
import qboImage from '~/modules/orders/components/qbo.png'
import { TempOrder } from '~/schema'
import { formatNumber } from '~/common/utils'
import moment from 'moment'

export const columns: ColumnProps<TempOrder>[] = [
  {
    title: 'ID #',
    dataIndex: 'id',
    sorter: (a: TempOrder, b: TempOrder) => {
      return Number(a.wholesaleOrderId) - Number(b.wholesaleOrderId)
    },
    render(data) {
      return `#${data}`
    },
  },
  {
    title: 'DATE PLACED',
    dataIndex: 'updatedDate',
    sorter: (a: TempOrder, b: TempOrder) => +new Date(a.updatedDate) - +new Date(b.updatedDate),
  },
  {
    title: 'DELIVERY DATE',
    dataIndex: 'deliveryDate',
    sorter: (a: TempOrder, b: TempOrder) => {
      return moment(a.deliveryDate).unix() - moment(b.deliveryDate).unix()
    }
  },
  {
    title: 'TOTAL SALE PRICE',
    dataIndex: 'totalPrice',
    sorter: (a: TempOrder, b: TempOrder) => a.totalPrice - b.totalPrice,
    render(data) {
      return <PriceItem>${formatNumber(data, 2)}</PriceItem>
    },
  },
  {
    title: 'STATUS',
    dataIndex: 'status',
    width: 132,
    render(data) {
      const statusOverlay = (
        <Menu>
          {['PLACED', 'DELIVERED', 'RECEIVED'].map((s) => (
            <Menu.Item key={s}>{s}</Menu.Item>
          ))}
        </Menu>
      )
      return (
        <Dropdown overlay={statusOverlay} trigger={['click']}>
          <OrderStatus>
            <span>{data}</span>
            <Icon type="caret-down" />
          </OrderStatus>
        </Dropdown>
      )
    },
  },
  {
    title: 'QBO',
    dataIndex: 'qboId',
    sorter: (a: TempOrder, b: TempOrder) => a.qboId - b.qboId,
    render: (text) => {
      if (text) {
        return <QBOImage src={qboImage} />
      } else {
        return ''
      }
    },
  },
  {
    width: 150,
    render(order, __, _index) {
      return (
        <Link to={`/order/${order.linkToken}`}>
          VIEW <Icon type="arrow-right" />
        </Link>
      )
    },
  },
] as ColumnProps<TempOrder>[]
