import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { Icon, Button, Dropdown, Menu, notification, Modal } from 'antd'
import { ColumnProps } from 'antd/es/table'
import { withTheme } from 'emotion-theming'
import { rgba } from 'polished'

import { Icon as IconSvg, MutiSortTable } from '~/components'
import { Theme } from '~/common'

import {
  CustomersModule,
  CustomersDispatchProps,
  TempCustomersStateProps,
  CustomerPriceSheet,
} from '../../customers.module'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'

import {
  CustomerDetailWrap,
  Header,
  Flex,
  Status,
  Name,
  Info,
  Operation,
  TableHeader,
  TextWrap,
  BackButton,
  InfoSpan,
  Divider,
  buttonStyle,
  disabledButtonStyle,
  AddButtonWrap,
  addToButtonStyle,
  HeaderOptionWrap,
  PriceSheetWrap,
  PriceSheetId,
  PriceSheetName,
  EmptyTr,
  Container,
  Items,
} from './customer-detail.style'
import { columns } from './columns'
import { Link } from 'react-router-dom'
import CustomerForm from '../CustomerForm'
import { Customer, CustomerStatus, CustomerType } from '~/schema'
import { format } from 'date-fns'

type CustomerDetailProps = CustomersDispatchProps &
  TempCustomersStateProps &
  RouteComponentProps<{ id: string }> & {
    theme: Theme
  }

export class CustomerDetail extends React.PureComponent<CustomerDetailProps> {
  readonly state = {
    currentPriceSheetIndex: -1,
    showOrder: true,
    showPriceSheet: true,
    editingInfo: false,
    editValues: {},
    message: '',
    type: null,
  }

  componentDidMount() {
    //Tao TODO:
    this.props.resetLoading()
    const id = this.props.match.params.id
    this.props.getCustomer(id)
    this.props.getOrder(id)
    this.props.getPriceSheet(id)
    //TODO: If seller
    this.props.getSellerPriceSheet()
  }

  // onClickStatus = ({ key }: { key: string }) => {
  //   const id = this.props.customer.id
  //   this.props.changeStatus({
  //     id,
  //     status: key,
  //   })
  // }

  // componentWillReceiveProps(nextProps: CustomerDetailProps) {
  //   if (nextProps.message !== this.state.message) {
  //     this.setState({
  //       message: nextProps.message,
  //       type: nextProps.type,
  //     })
      // if (nextProps.type != null) {
      //   notification[nextProps.type!]({
      //     message: nextProps.type!.toLocaleUpperCase(),
      //     description: nextProps.message,
      //     onClose: nextProps.resetLoading,
      //   })
      // }
  //   }
  // }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Go to Pricesheet below to place a new order',
      onClick: () => {
        //console.log('Notification Clicked!')
      },
    })
  }

  onAssignPriceSheet = ({ key }: { key: string }) => {
    this.props.assignPriceSheet(+key)
  }

  onUnassignPriceSheet = (priceSheet: CustomerPriceSheet) => () => {
    this.props.unAssignPriceSheet(priceSheet.clientId)
  }

  onToggleOrder = () => {
    const { showOrder } = this.state
    this.setState({
      showOrder: !showOrder,
    })
  }

  onTogglePriceSheet = () => {
    const { showPriceSheet } = this.state
    this.setState({
      showPriceSheet: !showPriceSheet,
    })
  }

  onClickEditInfo = () => {
    this.setState({
      editingInfo: true,
    })
  }

  onClickActiveIcon = () => {
    const { customer } = this.props
    customer.status === CustomerStatus.ACTIVE
      ? this.setState(
          {
            editValues: {
              status: CustomerStatus.INACTIVE,
            },
          },
          () => this.props.updateCurrentCustomer(this.state.editValues),
        )
      : this.setState(
          {
            editValues: {
              status: CustomerStatus.ACTIVE,
            },
          },
          () => this.props.updateCurrentCustomer(this.state.editValues),
        )
  }

  onClickDeleteIcon = () => {
    this.setState(
      {
        editValues: {
          status: CustomerStatus.DELETE,
        },
      },
      () => this.props.updateCurrentCustomer(this.state.editValues),
    )
  }

  render() {
    if (!this.props.customer) return null

    return (
      <CustomerDetailWrap>
        {/*<MessageHeader message={this.props.message} type={this.props.type} description={this.props.description} />*/}
        <Container>
          {this.renderHeader()}
          {this.renderOrders()}
          {this.renderPriceSheet()}
          {this.renderEditModal()}
        </Container>
      </CustomerDetailWrap>
    )
  }

  private renderHeader() {
    const { customer } = this.props
    const iconType = customer.type === CustomerType.INDIVIDUAL ? 'user' : 'home'
    const address = customer.mainAddress ? this.formatAddress(customer.mainAddress) : 'N/A'

    const CustomerInfo = (
      <>
        <Name>{!!customer.website ? customer.website : 'N/A'}</Name>
        <Flex style={{ alignItems: 'center' }}>
          <div style={{ textAlign: 'center', paddingTop: '10px' }}>
            <Icon type={iconType} style={{ fontSize: '40px' }} />
            <Status>{!!customer.type ? customer.type.toUpperCase() : 'N/A'}</Status>
          </div>
          <div style={{ marginLeft: '20px' }}>
            <Flex style={{ alignItems: 'center' }}>
              <Info>{!!customer.mainContact ? customer.mainContact : 'N/A'}</Info>
              <Divider />
              <Info>{!!customer.mobilePhone ? customer.mobilePhone : 'N/A'}</Info>
            </Flex>
            <Flex style={{ alignItems: 'center', marginTop: '5px' }}>
              <Info>{!!customer.clientCompany ? customer.clientCompany.companyName : 'N/A'}</Info>
              <Divider />
              <Info>{address}</Info>
            </Flex>
          </div>
        </Flex>
      </>
    )

    const HeaderButtons = (
      <BackButton onClick={this.onClickEditInfo}>
        <Icon type="edit" />
        <span>EDIT INFO</span>
      </BackButton>
    )

    return (
      <Header>
        <Flex style={{ marginTop: '36px' }}>
          <BackButton onClick={this.props.goBack}>
            <Icon type="arrow-left" />
            <span>back</span>
          </BackButton>
          <div>
            <Flex>
              <InfoSpan>{customer.status} CUSTOMER</InfoSpan>
              {HeaderButtons}
            </Flex>
            {CustomerInfo}
          </div>
        </Flex>
        <Flex style={{ marginTop: '30px' }}>
          <Operation>
            <Icon type="user" onClick={this.onClickActiveIcon} />
            <span onClick={this.onClickActiveIcon}>
              {' '}
              MARK {customer.status === CustomerStatus.ACTIVE ? 'INACTIVE' : 'ACTIVE'}
            </span>
          </Operation>
          <Operation>
            <Icon type="delete" onClick={this.onClickDeleteIcon} />
            <span onClick={this.onClickDeleteIcon}> DELETE CUSTOMER</span>
          </Operation>
        </Flex>
      </Header>
    )
  }

  private formatAddress(address: any) {
    if (!address.city) {
      return ''
    }
    return (
      (address.street1 ? address.street1 + ', ' : '') +
      (address.street2 ? address.street2 + ', ' : '') +
      address.city +
      (address.state ? ', ' + address.state : '') +
      (address.zipcode ? ' ' + address.zipcode : '')
    )
  }

  private renderOrders() {
    const { orders, theme } = this.props
    const { showOrder } = this.state
    const title = () => {
      return (
        <TableHeader>
          <span style={{ marginTop: '33px', marginLeft: '42px', fontWeight: 700 }}>Orders</span>
          <HeaderOptionWrap>
            <Button
              size="large"
              icon="plus"
              type="primary"
              style={{
                ...buttonStyle,
                border: `1px solid ${theme.primary}`,
                backgroundColor: theme.primary,
              }}
              onClick={this.comingSoon()}
            >
              New Order
            </Button>
            {showOrder ? (
              <IconSvg
                type="shrink"
                onClick={this.onToggleOrder}
                height={40}
                style={{ marginLeft: '45px', marginTop: '-10px' }}
              />
            ) : (
              <IconSvg
                type="expand"
                onClick={this.onToggleOrder}
                height={40}
                style={{ marginLeft: '45px', marginTop: '-10px' }}
              />
            )}
          </HeaderOptionWrap>
        </TableHeader>
      )
    }

    const orderTable = !showOrder ? null : <MutiSortTable columns={columns} dataSource={orders} />

    return (
      <>
        {title()}
        {orderTable}
      </>
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody className="ant-table-tbody">
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }

  private renderPriceSheet() {
    const { priceSheets, match, sellerPriceSheets, theme } = this.props
    const { showPriceSheet } = this.state
    const { currentPriceSheetIndex } = this.state
    const { onUnassignPriceSheet } = this
    const columns: ColumnProps<CustomerPriceSheet>[] = [
      {
        title: 'ID #',
        dataIndex: 'id',
        sorter: (a: CustomerPriceSheet, b: CustomerPriceSheet) => a.id - b.id,
        render: (data) => (
          <PriceSheetId>
            <span>#{data}</span>
            <div />
          </PriceSheetId>
        ),
      },
      {
        title: 'NAME',
        dataIndex: 'name',
        key: 'name',
        sorter: (a: CustomerPriceSheet, b: CustomerPriceSheet) => a.name.localeCompare(b.name),
        render(data) {
          return <PriceSheetName>{data}</PriceSheetName>
        },
      },
      {
        title: 'LATEST ORDER',
        dataIndex: 'updatedAt',
        sorter: (a: CustomerPriceSheet, b: CustomerPriceSheet) => +new Date(a.updatedAt) - +new Date(b.updatedAt),
        render(text) {
          return text ? format(text, 'MM/D/YY') : ''
        },
      },
      // {
      //   title: 'ASSIGNED',
      //   dataIndex: 'assigned',
      //   sorter: (a: PriceSheet, b: PriceSheet) => a.assigned - b.assigned,
      // },
      {
        title: 'ITEMS',
        dataIndex: 'itemCount',
        key: 'itemCount',
        sorter: (a: CustomerPriceSheet, b: CustomerPriceSheet) => a.items - b.items,
        render(text) {
          return <Items>{`${text}`}</Items>
        },
      },
      {
        render(value) {
          return (
            <TextWrap onClick={onUnassignPriceSheet(value)}>
              <Icon type="minus-circle" theme="filled" />
              unassign
            </TextWrap>
          )
        },
      },
      {
        render(_text, priceSheet, index) {
          return index === currentPriceSheetIndex ? (
            <Link to={`/order/pricesheet/${priceSheet.id}/customer/${match.params.id}/new`}>
              <Button
                type="primary"
                style={{
                  ...buttonStyle,
                  border: `1px solid ${theme.primary}`,
                  backgroundColor: theme.primary,
                }}
              >
                New Order
              </Button>
            </Link>
          ) : (
            <Button
              type="primary"
              disabled={true}
              style={{
                ...disabledButtonStyle,
                backgroundColor: rgba(theme.primary, 0.25),
              }}
            >
              New Order
            </Button>
          )
        },
      },
      {
        width: 20,
      },
      {
        width: 100,
        className: 'view',
        render(_text, priceSheet, index) {
          return index === currentPriceSheetIndex ? (
            <Link
              to={`/pricesheet/${priceSheet.id}`}
              style={{
                color: theme.primary,
              }}
            >
              VIEW <Icon type="arrow-right" />
            </Link>
          ) : (
            <></>
          )
        },
      },
      {
        width: 30,
      },
    ]

    const AddToOverlay =
      sellerPriceSheets.length > 0 ? (
        <Menu>
          {sellerPriceSheets.map((p) => (
            <Menu.Item onClick={this.onAssignPriceSheet} key={p.priceSheetId}>
              {p.name}
            </Menu.Item>
          ))}
        </Menu>
      ) : (
        <Menu>
          {['N/A'].map((p) => (
            <Menu.Item key={p}>{p}</Menu.Item>
          ))}
        </Menu>
      )

    const title = () => {
      return (
        <TableHeader>
          <span style={{ marginTop: '33px', marginLeft: '42px', fontWeight: 700 }}>Assigned Price Sheets</span>
          <HeaderOptionWrap>
            <Dropdown overlay={AddToOverlay}>
              <Button
                type="ghost"
                style={{
                  ...addToButtonStyle,
                  border: `1px solid ${this.props.theme.primary}`,
                }}
              >
                <AddButtonWrap>
                  <div>
                    <Icon type="plus" />
                    <span>Add to...</span>
                  </div>
                  <Icon type="caret-down" />
                </AddButtonWrap>
              </Button>
            </Dropdown>
            {showPriceSheet ? (
              <IconSvg
                type="shrink"
                onClick={this.onTogglePriceSheet}
                height={40}
                style={{ marginLeft: '45px', marginTop: '-10px' }}
              />
            ) : (
              <IconSvg
                type="expand"
                onClick={this.onTogglePriceSheet}
                height={40}
                style={{ marginLeft: '45px', marginTop: '-10px' }}
              />
            )}
          </HeaderOptionWrap>
        </TableHeader>
      )
    }

    const onRow = (_: any, index: number) => {
      return {
        onMouseEnter: () => {
          this.setState({
            currentPriceSheetIndex: index,
          })
        },
        onMouseLeave: () => {
          this.setState({
            currentPriceSheetIndex: -1,
          })
        },
      }
    }

    const components = {
      body: {
        wrapper: this.wrapper,
      },
    }

    return (
      <PriceSheetWrap>
        {title()}
        {showPriceSheet && (
          <MutiSortTable components={components} columns={columns} dataSource={priceSheets} onRow={onRow} />
        )}
      </PriceSheetWrap>
    )
  }

  private renderEditModal() {
    const onCancel = () => {
      this.setState({
        editingInfo: false,
      })
    }
    const onSubmit = (customer: Customer) => {
      this.props.updateCurrentCustomer(customer)
      this.setState({
        editingInfo: false,
      })
    }
    return (
      <Modal width={880} footer={null} visible={this.state.editingInfo} onCancel={onCancel}>
        <CustomerForm
          key={`${Date.now()}`}
          type="update"
          customerData={this.props.customer}
          onSubmit={onSubmit}
          onCancel={onCancel}
        />
      </Modal>
    )
  }
}

const mapState = (state: GlobalState) => state.customers

export const CustomerDetailContainer = withTheme(connect(CustomersModule)(mapState)(CustomerDetail))
