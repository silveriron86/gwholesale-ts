import styled from '@emotion/styled'
import { css } from '@emotion/core'
import {
  white,
  mediumGrey,
  black,
  brightGreen,
  lightGrey,
  transparent,
  lightGreen,
  darkGrey,
  backgroundGreen,
} from '~/common'

export const CustomerDetailWrap = styled('div')({
  width: '100%',
  minHeight: '100%',
  backgroundColor: white,
  boxSizing: 'border-box',
})

export const Container = styled('div')({
  paddingLeft: '36px',
  paddingRight: '50px',
})

export const Header = styled('div')({
  width: '100%',
  height: '232px',
  boxSizing: 'border-box',
  display: 'flex',
  justifyContent: 'space-between',

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const Flex = styled('div')({
  display: 'flex',
})

export const Status = styled('div')({
  color: mediumGrey,
  fontSize: '13px',
  letterSpacing: '0.05em',
  marginTop: '10px',
})

export const Name = styled('div')({
  color: black,
  fontSize: '36px',
  fontWeight: 700,
  lineHeight: '52px',
  letterSpacing: '0.05em',
})

export const Info = styled('div')({
  color: mediumGrey,
  fontSize: '14px',
  lineHeight: '25px',
  letterSpacing: '0.05em',
})

export const Operation = styled('div')((props) => ({
  display: 'flex',
  color: props.theme.primary,
  fontSize: '14px',
  lineHeight: '15px',
  height: '15px',
  letterSpacing: '0.05em',
  cursor: 'pointer',
  '& + &': {
    marginLeft: '45px',
  },
  '& > i': {
    fontSize: '16px',
    margin: '0 10px',
  },
  '& > span': {
    fontWeight: 700,
  },
}))

export const TableHeader = styled('div')((props) => ({
  '&.box-shadow-header': {
    // boxShadow: '0px 12px 14px -6px rgba(0,0,0,0.15)',
    borderBottom: "1px solid rgba(0,0,0,0.15)"
  },
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '100%',
  minWidth: 1200,
  boxSizing: 'border-box',
  backgroundColor: transparent,
  '&.default-pricesheet-title': {
    borderTop: `5px solid ${props.theme.main}`,
  },

  '& > span': {
    color: mediumGrey,
    fontSize: '24px',
    marginTop: '33px',
    fontWeight: 700,
    lineHeight: '30px',
  },
  '& > i': {
    fontSize: '18px',
    color: props.theme.primary,
  },
}))

export const OptionContainer = styled("div")({
  textAlign: "left",
  paddingTop: "20px"
});

export const Items = styled('span')({
  fontFamily: '"Museo Sans Rounded"',
  color: black,
  fontSize: '14px',
  fontWeight: 700,
})

export const TextWrap = styled('div')((props) => ({
  fontSize: '14px',
  color: props.theme.primary,
  lineHeight: '15px',
  textTransform: 'uppercase',
  cursor: 'pointer',
  fontWeight: 700,

  '& > i': {
    margin: '10px',
  },
}))

export const BackButton = styled('div')((props) => ({
  color: props.theme.primary,
  cursor: 'pointer',
  fontSize: '12px',
  marginRight: '30px',

  '& > span': {
    textTransform: 'uppercase',
    fontWeight: 700,
    marginLeft: '8px',
  },
}))

export const InfoSpan = styled('div')({
  fontSize: '12px',
  fontWeight: 700,
  textTransform: 'uppercase',
  color: mediumGrey,
  marginRight: '18px',
})

export const Divider = styled('span')({
  width: '1px',
  height: '15px',
  backgroundColor: lightGrey,
  margin: '0 14px',
})

export const buttonStyle: React.CSSProperties = {
  borderRadius: '200px',
  height: '40px',
  border: `1px solid ${brightGreen}`,
  backgroundColor: brightGreen,
}

export const disabledButtonStyle: React.CSSProperties = {
  borderRadius: '200px',
  height: '40px',
  margin: '0 11px',
  border: `1px solid ${transparent}`,
  color: '#fcfcfc',
}

export const addToButtonStyle: React.CSSProperties = {
  borderRadius: '200px',
  height: '40px',
  width: '264px',
  border: `1px solid ${brightGreen}`,
}

export const AddButtonWrap = styled('div')((props) => ({
  width: '100%',
  height: '40px',
  boxSizing: 'border-box',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  color: props.theme.primary,
  paddingLeft: '10px',

  '& > div': {
    fontSize: '14px',

    '& > i': {
      marginRight: '8px',
      fontSize: '12px',
    },
  },
}))

export const PriceItem = styled('span')({
  fontWeight: 700,
  color: black,
  fontSize: '18px',
})

export const HeaderOptionWrap = styled('div')((props) => ({
  marginTop: '20px',
  '& > i': {
    color: props.theme.main,
    cursor: 'pointer',
  },
}))

export const ViewSpan = styled('div')((props) => ({
  color: props.theme.primary,
  fontSize: '14px',
  letterSpacing: '0.05em',
  cursor: 'pointer',

  '& > span': {
    marginRight: '9px',
  },
}))

export const DefaultPricing = styled('div')({})

export const PriceSheetWrap = styled('div')({
  marginTop: '20px',
  '& table': {
    overflow: 'hidden',
  },
  '& .ant-table-thead > tr.ant-table-row-hover:not(.ant-table-expanded-row) > td, .ant-table-tbody > tr.ant-table-row-hover:not(.ant-table-expanded-row) > td, .ant-table-thead > tr:hover:not(.ant-table-expanded-row) > td, .ant-table-tbody > tr:hover:not(.ant-table-expanded-row) > td': {
    backgroundColor: lightGreen,
  },
  '& .ant-table-row': {
    '& > .view': {
      display: 'none',
    },
  },
  '& .ant-table-row > td:nth-of-type(9)': {
    display: 'none',
  },
  '& .ant-table-row:hover': {
    '& > td:nth-of-type(7)': {},
    '& > .view': {
      borderTopRightRadius: '15px',
      borderBottomRightRadius: '15px',
      display: 'table-cell',
    },
  },
  '& .ant-table-row-level-0': {
    height: '80px',
    borderTop: '10px solid #fff',
    borderBottom: '15px solid #fff',

    '& > td': {
      overflow: 'hidden',
      backgroundColor: backgroundGreen,
      padding: '18px 18px',
    },

    '& > td:first-of-type': {
      overflow: 'hidden',
      borderTopLeftRadius: '20px',
      borderBottomLeftRadius: '20px',
    },
  },
})

export const OrderStatus = styled('div')((props) => ({
  boxSizing: 'border-box',
  cursor: 'pointer',
  borderBottom: `1px solid ${props.theme.primary}`,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',

  '& > span': {
    fontSize: '14px',
    fontWeight: 700,
    color: black,
    textTransform: 'uppercase',
  },

  '& > i': {
    fontSize: '12px',
    color: props.theme.primary,
  },
}))

export const SvgExpand = css({
  width: '12px',
  height: '24px',
  marginTop: '-4px',
})

export const PriceSheetId = styled('div')({
  fontSize: '18px',
  letterSpacing: '0.05em',
  color: black,
  position: 'relative',
  textAlign: 'center',

  '& > div': {
    position: 'absolute',
    width: '132px',
    height: '112px',
    backgroundColor: mediumGrey,
    zIndex: 0,
    opacity: 0.1,
    borderRadius: '500px',
    top: '-44px',
    left: '-92px',
  },

  '& > span': {
    zIndex: 10,
  },
})

export const PriceSheetName = styled('div')({
  fontSize: '24px',
  letterSpacing: '0.05em',
  color: darkGrey,
  fontWeight: 700,
  width: '850px',
})

export const EmptyTr = styled('tr')({
  height: '15px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})
