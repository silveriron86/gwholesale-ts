import React from 'react'
import { Modal, Input, Button, notification, Icon, Spin, Tooltip } from 'antd'
import { cloneDeep } from 'lodash'
import { ColumnProps } from 'antd/es/table'

import { Icon as IconSvg } from '~/components'
import { TempCustomer } from '~/schema'
//import { QBOImage, tableCss, EmptyTr } from '~/pricesheet/components/pricesheet-detail-table.style'
//import { DialogHeader, DialogFooter } from '~/pricesheet/components/pricesheet-detail-header.style'

import qboImage from './qbo.png'

import organicImg from '~/modules/inventory/components/organic.png'
import { MutiSortTable } from '~/components'
import { Theme, blue, CACHED_QBO_LINKED, CACHED_NS_LINKED } from '~/common'
import { format } from 'date-fns'
import { formatNumber } from '~/common/utils'
import { Link } from 'react-router-dom'
import { ThemeLink, DialogFooter, DialogHeader } from '~/modules/customers/customers.style'
import moment from 'moment'
import { EmptyTr } from '~/modules/orders/components/order-pricesheet-table.style'
import { tableCss } from '~/modules/orders/components/order-detail.style'
import { withTheme } from 'emotion-theming'

export interface CustomerSyncModalProps {
  theme: Theme
  customers: TempCustomer[]
  loading: boolean
  onSelectionChange: (changes: any) => void
}

export class CustomerSyncModalComponent extends React.PureComponent<
  CustomerSyncModalProps & {
    visible: boolean
    onOk: (keys: string[]) => void
    onCancel: () => void
  },
  {
    selectedRowKeys: string[]
  }
> {
  state = {
    selectedRowKeys: [],
  }

  private mutiSortTable = React.createRef<any>()

  onSortString = (a: any, b: any) => {
    const stringA = a ? a : ''
    const stringB = b ? b : ''
    return stringA.localeCompare(stringB)
  }

  wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }

  render() {
    const { customers } = this.props

    let columns: ColumnProps<TempCustomer>[] = [
      {
        title: 'ID #',
        dataIndex: 'clientId',
        key: 'clientId',
        width: 50,
        render: (d) => `# ${d}`,
      },
      {
        title: 'NAME',
        dataIndex: 'clientCompanyName',
        key: 'clientCompanyName',
        width: 80,
        sorter: (a, b) => this.onSortString(a, b),
      },
      {
        title: 'LAST SYNC',
        dataIndex: 'lastQBOUpdate',
        key: 'lastQBOUpdate',
        width: 80,
        sorter: (a, b) => a.lastQBOUpdate - b.lastQBOUpdate,
        render: (lastQBOUpdate) => format(lastQBOUpdate, 'MM/D/YY'),
      },
    ]

    const rowSelection = {
      onChange: (selectedRowKeys: any) => {
        this.setState({ selectedRowKeys })
      },
    }

    return (
      <Modal
        width={'92%'}
        bodyStyle={{ padding: '0' }}
        visible={this.props.visible}
        onCancel={this.props.onCancel}
        footer={[
          <DialogFooter key="footer">
            <Button
              type="primary"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              {this.props.loading ? (
                <span>
                  <Spin /> Loading..
                </span>
              ) : (
                <span>
                  <Icon type="cloud" theme="filled" /> Sync Customers
                </span>
              )}
            </Button>
            <Button onClick={this.props.onCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <DialogHeader>{localStorage.getItem(CACHED_NS_LINKED) != 'null' ? 'NS' : 'QBO'} Customer Sync</DialogHeader>
        <MutiSortTable
          ref={this.mutiSortTable}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={customers}
          rowKey="clientId"
          css={tableCss}
        />
      </Modal>
    )
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { selectedRowKeys } = this.state
    if (!selectedRowKeys.length) {
      return notification.error({
        message: 'Select one at least',
      })
    }
    this.props.onOk(selectedRowKeys)
    this.setState({
      selectedRowKeys: [],
    })
  }
}

export const CustomerSyncModal = withTheme(CustomerSyncModalComponent)
