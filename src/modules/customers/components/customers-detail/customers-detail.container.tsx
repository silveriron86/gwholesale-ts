import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { Layout, notification, Row } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'

import { Theme, CACHED_NS_LINKED, CACHED_QBO_LINKED } from '~/common'
import { CustomersDispatchProps, CustomersModule, TempCustomersStateProps } from '../../customers.module'
import { SalesTableSearchComponent } from './../searcher'
import { SalesOrdersTable } from './../sales-orders-table'

import { GlobalState } from '~/store/reducer'
import { CustomersInfoBody, CustomersInfoHeader, layoutStyle } from './customers-detail.style'
import { OrderService } from '~/modules/orders/order.service'
import { SaleItem, WholesaleRoute } from '~/schema'
import PageLayout from '~/components/PageLayout'
import DetailsOverview from '../details-overview'
import moment, { Moment } from 'moment'
import { ThemeOutlineButton, ThemeSpin, ThemeModal, ThemeButton } from '../../customers.style'
import { Level3WithTable } from '~/common/jqueryHelper'
import {
  getFulfillmentDate,
  needShowDuplicateConfirmModal,
  updateDisMatchDuplicateOrderItemsForGroup,
} from '~/common/utils'
import { FlexDiv, Item, DialogSubContainer, DuplicateConfirmDiv, BoldSpan } from '../../nav-sales/styles'

import { HeaderActions } from '~/modules/customers/accounts/header-actions/header-actions'
import { CustomerBaseComponent } from '~/modules/customers/accounts/base-component/CustomerBaseComponent'
import { checkError } from '~/common/utils'


type CustomersDetailProps = CustomersDispatchProps &
  TempCustomersStateProps &
  RouteComponentProps<{ customerId: string }> & {
    theme: Theme
  }

export class CustomersDetail extends CustomerBaseComponent {
  salesOrdersTableRef = React.createRef<any>()
  state: any

  constructor(props: CustomersDetailProps) {
    super(props)
    const sessionSearch = localStorage.getItem('CUSTOMER_ORDERS')
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search)
    const search = urlParams.get('search')
    const startDate = urlParams.get('datestart')
    const endDate = urlParams.get('dateend')

    const fromDate = startDate ? moment(startDate, 'MMDDYY') : moment().subtract(30, 'days')
    const toDate = endDate ? moment(endDate, 'MMDDYY') : moment().add(30, 'days')
    this.state = {
      curPreset: 8,
      currentOrderIndex: -1,
      currentPriceSheetIndex: -1,
      showOrder: true,
      showPriceSheet: true,
      editingInfo: false,
      editValues: {},
      message: '',
      type: null,
      showDrawer: false,
      showFilter: false,
      item: {} as SaleItem,
      saleCategories: [],
      search,
      loading: false,
      fromDate,
      toDate,
      currentPage: 1,
      pageSize: 10,
      id: -1,
      orderByFilter: '',
      direction: '',
      duplicateConfirmModal: false,
      duplicateOrderItems: [],
      duplicateParam: null,
    }
  }

  componentDidMount() {
    this.props.resetLoading()
    const id = this.props.match.params.customerId
    this.setState({
      id,
    })
    this.props.getCustomer(id)
    this.getAllSalesOrders()
    this.props.getPriceSheet(id)
    this.props.getSellerPriceSheet()
    this.props.getAllRoutes()
    if (localStorage.getItem(CACHED_NS_LINKED) != 'null')
      this.props.getLatestNSBalance(id)
    if (localStorage.getItem(CACHED_QBO_LINKED) != 'null')
      this.props.getLatestQBOBalance(id)
    this.props.getSellerSetting()
    Level3WithTable('level-3-with-table')
  }

  duplicateOrder = (data: any, orderItems: any) => {
    console.log(data)
    const _ = this;
    OrderService.instance.getOrderItemListByOrderId(data.wholesaleOrderId).subscribe({
      next(res: any) {
        const showConfirmModal = needShowDuplicateConfirmModal(res.body.data, 1)
        if (showConfirmModal) {
          _.setState({ duplicateConfirmModal: true, duplicateOrderItems: res.body.data, duplicateParam: data })
        } else {
          _.props.duplicateOrder(data)
        }
      },
      error(err) {
        checkError(err)
      },
    })
    // const showConfirmModal = needShowDuplicateConfirmModal(orderItems, 1)
    // // const orderDate = moment().format('MM/DD/YYYY')
    // // data = { ...data, orderDate }
    // if (showConfirmModal) {
    //   this.setState({ duplicateConfirmModal: true, duplicateOrderItems: orderItems, duplicateParam: data })
    // } else {
    //   this.props.duplicateOrder(data)
    // }
  }

  clickOkFromDuplicateConfirmModal = () => {
    const { duplicateParam } = this.state
    this.setState({ duplicateConfirmModal: false })
    this.props.duplicateOrder(duplicateParam)
  }

  // componentWillReceiveProps(nextProps: CustomersDetailProps) {
  //   if (nextProps.orders.length > 0) {
  //     if (nextProps.newItemId > 0) {
  //       setTimeout(() => {
  //         const query = nextProps.message === 'DUPLICATED_SUCCESS' ? '?duplicated=success' : ''
  //         this.props.history.push(`/sales-order/${nextProps.newItemId}${query}`)
  //         this.props.resetNewOrderId()
  //       }, 300)
  //     }
  //   }
  // }

  getAllSalesOrders = () => {
    const id = this.props.match.params.customerId
    const searchObj = {
      id: this.state.id != -1 ? this.state.id : id,
      from: this.state.fromDate.format('MM/DD/YYYY'),
      to: this.state.toDate.format('MM/DD/YYYY'),
      page: this.state.currentPage - 1,
      pageSize: this.state.pageSize,
      queryParam: this.state.search,
      orderByFilter: this.state.orderByFilter,
      direction: this.state.direction,
    }
    this.updateURL()
    this.props.resetLoading()
    this.props.getOrder(searchObj)
  }

  updateURL = () => {
    var url = new URL(window.location.href)
    var queryParams = url.searchParams
    const { search, fromDate, toDate } = this.state
    if (search) {
      queryParams.set('search', search)
    }
    if (fromDate) {
      queryParams.set('datestart', moment(fromDate).format('MMDDYY'))
    }
    if (toDate) {
      queryParams.set('dateend', moment(toDate).format('MMDDYY'))
    }
    url.search = queryParams.toString()
    localStorage.setItem('CUSTOMER_ORDERS', url.search)
    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString())
  }

  changeDuplicateConfirmModalStatus = () => {
    this.setState({
      duplicateConfirmModal: !this.state.duplicateConfirmModal,
    })
  }

  renderDuplicateText = (orderItems: any) => {
    const dismatchOrderItems = updateDisMatchDuplicateOrderItemsForGroup(orderItems, 1)
    return (
      <DuplicateConfirmDiv>
        <p>The following updates have been made to your duplicated order:</p>
        <ul>
          {dismatchOrderItems.map((orderItem: any) => {
            if (orderItem.duplicateType == 1) {
              return (
                <li>
                  The item <BoldSpan>{orderItem.wholesaleItem.variety} </BoldSpan>is currently inactive. This item has
                  not been duplicated.
                </li>
              )
            } else if (orderItem.duplicateType == 2) {
              return (
                <li>
                  The original UOM for{' '}
                  <BoldSpan>
                    {orderItem.wholesaleItem.variety},{orderItem.uom},
                  </BoldSpan>
                  is currently not available for use.This item has been duplicated with the default selling UOM.
                  {orderItem.wholesaleItem.InventoryUOM}
                </li>
              )
            } else if (orderItem.duplicateType == 3) {
              return (
                <li>
                  The original UOM for{' '}
                  <BoldSpan>
                    {orderItem.wholesaleItem.variety},{orderItem.pricingUOM},
                  </BoldSpan>
                  is currently not available for use.This item has been duplicated with the default price UOM.
                  {orderItem.wholesaleItem.InventoryUOM}
                </li>
              )
            }
          })}
        </ul>
      </DuplicateConfirmDiv>
    )
  }

  // onDuplicateMostRecentOrder: () => void = () => {
  //   const { sellerSetting } = this.props

  //   this.props.resetLoading()
  //   const fulfillmentDate = getFulfillmentDate(sellerSetting)
  //   this.props.duplicateMostRecentOrder({
  //     clientId: this.id,
  //     body: { deliveryDate: fulfillmentDate },
  //   })
  // }

  render() {
    const { loading } = this.state
    const { customer, orders } = this.props
    if (!customer) return null
    return (
      <PageLayout className="no-init-scroll" currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <HeaderActions
          onDuplicateMostRecentOrder={this.onDuplicateMostRecentOrder}
          onCreateBlankOrder={this.onCreateBlankOrder}
          duplicateMostRecentOrderLoading={this.props.duplicateMostRecentOrderLoading}
        />

        <CustomersInfoHeader>
          <DetailsOverview customer={customer} />
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <ThemeSpin tip="Loading..." spinning={loading}>
            {this.renderSalesOrders()}
            {this.renderActionButtons()}
          </ThemeSpin>
        </CustomersInfoBody>
      </PageLayout>
    )
  }

  onSearch = (text: string) => {
    this.setState(
      {
        search: text,
        currentPage: 1,
      },
      () => {
        this.getAllSalesOrders()
      },
    )
  }

  onNewOrder = () => {
    const { routes, match, customer, sellerSetting } = this.props
    if (customer && customer.creditLimit > 0 && customer.openBalance > customer.creditLimit) {
      notification.error({
        message: 'ERROR',
        description: 'Customer open balance is greater than credit limit',
        onClose: () => {},
      })
      return
    }

    const fulfillmentDate = getFulfillmentDate(sellerSetting)

    let data = {
      wholesaleClientId: match.params.customerId,
      deliveryDate: fulfillmentDate,
      orderDate: moment().format('MM/DD/YYYY'),
      itemList: [],
    }

    let activeRoutes: WholesaleRoute[] = []
    if (routes.length > 0) {
      routes.forEach((route) => {
        const weekDay = moment()
          .format('dddd')
          .toUpperCase()
        if (route.activeDays.indexOf(weekDay) >= 0) {
          activeRoutes.push({ ...route })
        }
      })

      if (activeRoutes.length > 0) {
        data.defaultRoute = activeRoutes[0].id
      }
    }
    this.props.createEmptyOrder(data)
  }

  onPageChange = (page: any, filter: any, sorter: any) => {
    const state = { ...this.state }
    if (page !== null) {
      state['currentPage'] = page.current
    }
    if (sorter && Array.isArray(sorter)) {
      const filterStr = sorter
        .map((el: any) => {
          return el.dataIndex
        })
        .join(',')
      const directionStr = sorter
        .map((el: any) => {
          return el.sortOrder === 'ascend' ? 'ASC' : 'DESC'
        })
        .join(',')
      state['orderByFilter'] = filterStr ? filterStr : ''
      state['direction'] = directionStr ? directionStr : ''
    }
    this.setState(state, () => {
      this.getAllSalesOrders()
    })
  }

  onDateChange = (dates: [Moment, Moment], dateStrings: [string, string]) => {
    if (dates.length < 2 || dateStrings.length < 2) return
    this.setState({ fromDate: dates[0], toDate: dates[1], currentPage: 1 }, () => {
      this.getAllSalesOrders()
    })
  }

  private renderSalesOrders() {
    const { orders, history, orderTotal } = this.props
    const { currentPage, pageSize, curPreset, search, duplicateConfirmModal, duplicateOrderItems } = this.state

    return (
      <Layout className="level-3-with-table">
        <ThemeModal
          title={`Duplicate order alert`}
          visible={duplicateConfirmModal}
          onCancel={this.changeDuplicateConfirmModalStatus}
          bodyStyle={{ padding: 0 }}
          footer={
            <DialogSubContainer>
              <ThemeButton type="primary" onClick={this.clickOkFromDuplicateConfirmModal}>
                OK
              </ThemeButton>
            </DialogSubContainer>
          }
          width={600}
        >
          {this.renderDuplicateText(duplicateOrderItems)}
        </ThemeModal>
        <SalesTableSearchComponent
          curPreset={curPreset}
          search={search}
          rawData={orders}
          onShowDrawer={() => {}}
          onSearch={this.onSearch}
          theme={this.props.theme}
          handleNewOrder={this.onNewOrder}
          fromDateProps={this.state.fromDate}
          toDateProps={this.state.toDate}
          onDateChange={this.onDateChange}
        />
        <SalesOrdersTable
          columns={orders}
          editItem={() => {}}
          search={this.state.search}
          showDrawer={this.state.showDrawer}
          loading={this.props.loading}
          history={history}
          theme={this.props.theme}
          duplicateOrder={this.duplicateOrder}
          changePage={this.onPageChange}
          currentPage={currentPage}
          pageSize={pageSize}
          total={orderTotal}
          sellerSetting={this.props.sellerSetting}
          resetDuplicateOnRowLoadingStatus={this.props.resetAccountOrderTableLoadingStatus}
          ref={this.salesOrdersTableRef}
        />
      </Layout>
    )
  }

  private renderActionButtons() {
    return (
      <Layout style={layoutStyle}>
        <Row style={{ marginTop: '25px' }}>
          {/* <Col lg={6} style={{ textAlign: 'left' }}>
              <CustomButton>Add to QBO</CustomButton>
              </Col>
             <Col>
               <Row type="flex" justify="end" style={{ paddingBottom: 20 }}>
                 <CustomButton>Update QBO</CustomButton>
                 <CustomButton>View in QBO</CustomButton>
                 <CustomButton>New Sales Order Form Template</CustomButton>
                 <CustomButton>New Blank Sales Order</CustomButton>
               </Row>
             </Col>*/}
        </Row>
      </Layout>
    )
  }
}

const mapState = (state: GlobalState) => state.customers

export const CustomersDetailContainer = withTheme(connect(CustomersModule)(mapState)(CustomersDetail))
