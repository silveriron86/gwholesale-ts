import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { white, mediumGrey, black, lightGrey, badgeColor } from '~/common'
import { Row } from 'antd'

export const Header = styled('div')({
  width: '100%',
  boxSizing: 'border-box',
  marginBottom: '10px',
  display: 'flex',
  justifyContent: 'space-between',

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const Flex = styled('div')({
  display: 'flex',
})

export const HeaderButtonStyle: React.CSSProperties = {
  color: mediumGrey,
  fontWeight: 'bold',
}

export const BadgeWrapper = styled('div')({
  position: 'relative',
  width: '66px',
  paddingLeft: '7px',
  textAlign: 'left',
})

export const badgeStyle: React.CSSProperties = {
  backgroundColor: badgeColor,
  width: '16px',
  height: '20px',
  zIndex: 1,
}

export const layoutStyle: React.CSSProperties = {
  backgroundColor: white,
}

export const transLayout: React.CSSProperties = {
  backgroundColor: 'transparent',
}

export const DeailWrapper = styled.div`
  padding: 18px 28px 4px 28px;
  text-align: left;
`
export const NormalSpan = styled.p`
  font-size: 12px;
  line-height: 14px;
  color: ${mediumGrey};
  height: 20px;
  margin-bottom: 6px;
`
export const BoldSpan = styled.p`
  font-size: 12px;
  line-height: 144%;
  color: ${black};
  height: 20px;
  margin-top: -1px;
  font-weight: bold;
  overflow-wrap: break-word;
`

export const detailWrapperDivider: React.CSSProperties = {
  height: '94px',
  backgroundColor: lightGrey,
  marginTop: 18,
}

export const CustomersInfoHeader = styled.div`
  padding: 12px 24px;
  margin-bottom: 50px;
  padding-top: 20px;
  box-shadow: 0px 12px 14px -6px rgba(0, 0, 0, 0.15);
`

export const CustomersInfoBody = styled.div`
  margin: 0 19px;
`

interface CustomHeaderCardProps {
  active: boolean
}

export const CardTextWrapper = styled.div<CustomHeaderCardProps>`
  color: ${(props: CustomHeaderCardProps) => (props.active ? badgeColor : mediumGrey)};
  font-size: 12px;
  line-height: 12px;
  font-weight: bold;
  letter-spacing: 0.05em;
`

export const CustomerHeaderCard = styled.div<CustomHeaderCardProps>`
  padding: 13px 12px 0;
  margin-left: 20px;
  border: 1px solid #dddddd;
  border-color: ${(props: CustomHeaderCardProps) => (props.active ? badgeColor : '#DDDDDD')};
  border-radius: 6px;
  box-xizing: border-box;
  min-height: 66px;
  text-align: left;
  transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);

  &:hover {
    transform: scale(1.02, 1.02);
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 3px 10px 0 rgba(0, 0, 0, 0.19);
  }
`

// export const detailWrapperStyle: React.CSSProperties = {
//     backgroundColor: 'rgba(82, 158, 99, 0.1)',
//     borderRadius: '6px'
// }
export const DetailWrapperStyle = styled(Row)((props) => ({
  backgroundColor: props.theme.lighter,
  borderRadius: '6px',
}))

export const activeCustomerCardStyle: React.CSSProperties = {
  borderColor: badgeColor,
}

export const Name = styled('div')({
  color: mediumGrey,
  fontSize: '24px',
  fontWeight: 700,
  lineHeight: '144%',
  letterSpacing: '0.05em',
  textAlign: 'left',
  // marginLeft: 10,
})

export const Operation = styled('div')((props) => ({
  display: 'flex',
  color: props.theme.primary,
  fontSize: '14px',
  lineHeight: '15px',
  height: '15px',
  letterSpacing: '0.05em',
  cursor: 'pointer',
  '& + &': {
    marginLeft: '45px',
  },
  '& > i': {
    fontSize: '16px',
    margin: '0 10px',
  },
  '& > span': {
    fontWeight: 700,
  },
}))

export const InfoSpan = styled('div')({
  fontSize: '12px',
  fontWeight: 700,
  textTransform: 'uppercase',
  color: mediumGrey,
})

export const Divider = styled('span')({
  width: '1px',
  height: '25px',
  backgroundColor: lightGrey,
  margin: '0 30px',
  marginTop: '-6px',
})

export const PriceItem = styled('span')({
  fontWeight: 700,
  color: black,
  fontSize: '18px',
})

export const OrderStatus = styled('div')((props) => ({
  boxSizing: 'border-box',
  cursor: 'pointer',
  borderBottom: `1px solid ${props.theme.primary}`,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',

  '& > span': {
    fontSize: '14px',
    fontWeight: 700,
    color: black,
    textTransform: 'uppercase',
  },

  '& > i': {
    fontSize: '12px',
    color: props.theme.primary,
  },
}))

export const SvgExpand = css({
  width: '12px',
  height: '24px',
  marginTop: '-4px',
})
