import * as React from 'react'
import { Layout, Row, Col, Divider as AntDivider } from 'antd'
import moment from 'moment'
import {
  Flex,
  layoutStyle,
  DetailWrapperStyle,
  DeailWrapper,
  detailWrapperDivider,
  NormalSpan,
  BoldSpan,
  Header,
  InfoSpan,
  Name,
} from '../components/customers-detail/customers-detail.style'
import { ml0, small } from '../customers.style'
import { formatNumber, padString } from '~/common/utils'
import OverviewSection from './overview-section'
import { TempCustomer } from '~/schema'

interface DetailsOverviewProps {
  customer: TempCustomer
}

export class DetailsOverview extends React.PureComponent<DetailsOverviewProps> {
  render() {
    const { customer } = this.props
    const category = location.href.indexOf('vendor/') >= 0 ? 'Vendor' : 'Account'
    return (
      <>
        <Header>
          <Layout style={layoutStyle}>
            <Row type="flex" justify="space-between">
              <Col xs={14}>
                <Flex>
                  <InfoSpan>{category} Name</InfoSpan>
                </Flex>
                <Name>{customer && customer.clientCompany && customer.clientCompany.companyName}</Name>
              </Col>
            </Row>
          </Layout>
        </Header>

        <Layout style={layoutStyle}>
          <Row type="flex" justify="space-between">
            <Col xs={24} sm={12} md={6}>
              <OverviewSection
                active={false}
                label="OPEN BALANCE"
                timestamp={`Updated ${
                  customer && customer.openUpdatedAt ? moment(customer.openUpdatedAt).fromNow() : moment().fromNow()
                }`}
                value={
                  customer && typeof customer.openBalance !== 'undefined'
                    ? `$${formatNumber(customer.openBalance, 2)}`
                    : '--'
                }
                style={ml0}
              />
            </Col>
            <Col xs={24} sm={12} md={6}>
              <OverviewSection
                active={true}
                label="OVERDUE"
                timestamp={`Updated ${
                  customer && customer.overdueUpdatedAt
                    ? moment(customer.overdueUpdatedAt).fromNow()
                    : moment().fromNow()
                }`}
                value={
                  customer && typeof customer.overdue !== 'undefined' ? `$${formatNumber(customer.overdue, 2)}` : '--'
                }
              />
            </Col>
            <Col xs={24} sm={12} md={6}>
              <OverviewSection
                active={false}
                label="PAYMENT TERMS"
                value={customer && customer.paymentTerm ? customer.paymentTerm : ''}
              />
            </Col>
            <Col xs={24} sm={12} md={6}>
              <OverviewSection
                active={false}
                label="CREDIT LIMIT"
                value={
                  customer && typeof customer.creditLimit !== 'undefined'
                    ? `$${formatNumber(customer.creditLimit, 2)}`
                    : '--'
                }
              />
            </Col>
          </Row>
        </Layout>
        <Row>
          <Col span={12} style={{ textAlign: 'left', marginTop: 20 }}>
            <InfoSpan>{category} Details</InfoSpan>
          </Col>
        </Row>
        <Layout style={layoutStyle}>
          <Row style={{ marginTop: 4 }} type="flex" justify="space-between" />
          <DetailWrapperStyle type="flex" justify="space-between">
            <Col lg={6} md={12}>
              <DeailWrapper>
                <Flex>
                  <div style={{ width: '45%', minWidth: 113 }}>
                    <NormalSpan>STATUS</NormalSpan>
                  </div>
                  <div style={{ flex: 1 }}>
                    <BoldSpan>{customer ? customer.status : ''}</BoldSpan>
                  </div>
                </Flex>
                <Flex>
                  <div style={{ width: '45%', minWidth: 113 }}>
                    <NormalSpan>BUSINESS TYPE</NormalSpan>
                  </div>
                  <div style={{ flex: 1 }}>
                    <BoldSpan>{customer ? customer.businessType : ''}</BoldSpan>
                  </div>
                </Flex>
                <Flex>
                  <div style={{ width: '45%', minWidth: 113 }}>
                    <NormalSpan>MAIN PHONE</NormalSpan>
                  </div>
                  <div style={{ flex: 1 }}>
                    <BoldSpan>{customer && customer.mobilePhone ? customer.mobilePhone : ''}</BoldSpan>
                  </div>
                </Flex>
                <Flex>
                  <div style={{ width: '45%', minWidth: 113 }}>
                    <NormalSpan>DBA NAME</NormalSpan>
                  </div>
                  <div style={{ flex: 1 }}>
                    <BoldSpan>{customer && customer.dba ? customer.dba : ''}</BoldSpan>
                  </div>
                </Flex>
              </DeailWrapper>
            </Col>
            <Col lg={6} md={12}>
              <Flex>
                <AntDivider type="vertical" style={detailWrapperDivider} />
                <DeailWrapper style={{ flex: 1 }}>
                  <Row>
                    <Col lg={10} md={24}>
                      <NormalSpan>ALT. PHONE</NormalSpan>
                    </Col>
                    <Col lg={14} md={24}>
                      <BoldSpan>{customer && customer.alternativePhone ? customer.alternativePhone : ''}</BoldSpan>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={10} md={24}>
                      <NormalSpan>FAX</NormalSpan>
                    </Col>
                    <Col lg={14} md={24}>
                      <BoldSpan>{customer ? customer.fax : ''}</BoldSpan>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={10} md={24}>
                      <NormalSpan>EMAIL</NormalSpan>
                    </Col>
                    <Col lg={14} md={24}>
                      {/* <BoldSpan>genevieve.wang+deluca@gmail.com</BoldSpan> */}
                      <BoldSpan>{customer ? customer.email : ''}</BoldSpan>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg={10} md={24}>
                      <NormalSpan>WEBSITE</NormalSpan>
                    </Col>
                    <Col lg={14} md={24}>
                      <BoldSpan>{customer ? customer.website : ''}</BoldSpan>
                    </Col>
                  </Row>
                </DeailWrapper>
              </Flex>
            </Col>
            <Col lg={6} md={12}>
              <Flex>
                <AntDivider type="vertical" style={detailWrapperDivider} />
                <DeailWrapper style={{ flex: 1 }}>
                  <Flex>
                    <div style={{ width: '45%', minWidth: 128 }}>
                      <NormalSpan>ACCOUNTING REP</NormalSpan>
                    </div>
                    <div style={{ flex: 1 }}>
                      <BoldSpan>
                        {customer && customer.accountant
                          ? `${customer.accountant.firstName} ${customer.accountant.lastName}`
                          : ''}
                      </BoldSpan>
                    </div>
                  </Flex>
                  {category == 'Account' && (
                    <Flex>
                      <div style={{ width: '45%', minWidth: 128 }}>
                        <NormalSpan>SALES REP</NormalSpan>
                      </div>
                      <div style={{ flex: 1 }}>
                        <BoldSpan>
                          {customer?.seller?.firstName}
                          {customer?.seller?.lastName}
                        </BoldSpan>
                      </div>
                    </Flex>
                  )}
                  {category == 'Vendor' && (
                    <Flex>
                      <div style={{ width: '45%', minWidth: 128 }}>
                        <NormalSpan>PURCHASER</NormalSpan>
                      </div>
                      <div style={{ flex: 1 }}>
                        <BoldSpan>
                          {customer?.purchaser?.firstName}
                          {customer?.purchaser?.lastName}
                        </BoldSpan>
                      </div>
                    </Flex>
                  )}
                </DeailWrapper>
              </Flex>
            </Col>
            <Col lg={6} md={12}>
              <Flex>
                <AntDivider type="vertical" style={detailWrapperDivider} />
                <DeailWrapper style={{ flex: 1 }}>
                  <Row>
                    <Col lg={6} md={24}>
                      <NormalSpan>NOTES</NormalSpan>
                    </Col>
                    <Col lg={18} md={24}>
                      <BoldSpan>{customer ? customer.notes : ''}</BoldSpan>
                    </Col>
                  </Row>
                </DeailWrapper>
              </Flex>
            </Col>
          </DetailWrapperStyle>
        </Layout>
      </>
    )
  }
}

export default DetailsOverview
