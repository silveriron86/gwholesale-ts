import * as React from 'react'
import { Layout, Row, Col } from 'antd'
import { CustomerHeaderCard, CardTextWrapper, transLayout } from '../components/customers-detail/customers-detail.style'
import { CardValue } from '../customers.style'

interface OverviewSectionProps {
  active: boolean
  label: string
  timestamp?: string
  value?: string
  style?: any
}

export class OverviewSection extends React.PureComponent<OverviewSectionProps> {
  render() {
    const { active, label, timestamp, value, style } = this.props

    return (
      <CustomerHeaderCard active={active} style={style}>
        <CardTextWrapper active={active}>
          <Layout style={transLayout}>
            <Row>
              <Col xs={6} style={{ whiteSpace: 'nowrap' }}>
                {label}
              </Col>
              <Col xs={18} style={{ textAlign: 'right', fontSize: 11, fontWeight: 'normal' }}>
                {timestamp}
              </Col>
            </Row>
          </Layout>
          <CardValue active={active}>{value ? value : '--'}</CardValue>
        </CardTextWrapper>
      </CustomerHeaderCard>
    )
  }
}

export default OverviewSection
