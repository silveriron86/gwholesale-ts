import React from 'react'
import { ColumnProps } from 'antd/es/table'
import update from 'immutability-helper'
import { cloneDeep, groupBy, get } from 'lodash'
import { EmptyTr, tableCss } from './table.style'
import { RouteProps } from 'react-router'
// import { SalesOrder } from './../../customers.model'
import { TempOrder } from '~/schema'
import { formatOrderStatus, getFulfillmentDate, getOrderPrefix } from '~/common/utils'
import { ThemeIconButton } from '../../customers.style'
import { Icon as IconSvg } from "~/components";
import { History } from 'history'
import moment from 'moment'
// import { Icon } from 'antd'
import { Flex } from '../customer-detail/customer-detail.style'
import { NSImage, QBOImage } from '~/modules/orders/components/orders-table.style'
import qboImage from '~/modules/orders/components/qbo.png'
import { Level3WithTable } from '~/common/jqueryHelper'
import { MultiBackSortTable } from '~/components/multi-back-sortable'
import { CACHED_QBO_LINKED, Theme } from '~/common'
import { Tooltip } from 'antd'

export type SalesOrdersTableProps = RouteProps & {
  columns: TempOrder[]
  editItem: (item: TempOrder) => void
  search: string
  showDrawer: boolean
  loading: boolean
  history: History
  theme: Theme
  duplicateOrder: Function
  currentPage: any
  pageSize: any
  total: any
  sellerSetting: any
  resetDuplicateOnRowLoadingStatus:Function
  duplicateOrderOnRowLoading:boolean
  changePage: (page: any, filter: any, sorter: any) => void
}

const emtpyNode = () => null

export class SalesOrdersTable extends React.PureComponent<SalesOrdersTableProps> {
  state = {
    filteredCategory: [],
    initItems: cloneDeep(this.props.columns),
    // saveItems: this.props.columns,
    selectedItemIndex: -1,
    expandedRowKeys: [],
    sellerSetting: this.props.sellerSetting,
  }

  private mutiSortTable = React.createRef<any>()

  constructor(props: SalesOrdersTableProps) {
    super(props)
  }

  componentWillReceiveProps(nextProps: SalesOrdersTableProps) {
    if (nextProps.columns) {
      if (nextProps.columns.length > 0) {
        Level3WithTable('level-3-with-table')
      }
      this.setState({
        initItems: cloneDeep(nextProps.columns),
      })
    }
    if (JSON.stringify(this.props.sellerSetting) != JSON.stringify(nextProps.sellerSetting)) {
      this.setState({ sellerSetting: nextProps.sellerSetting })
    }
  }

  wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }

  updateArrayItem = (saleOrder: TempOrder, insert: boolean) => {
    const items = this.state.initItems
    const index = items.findIndex((i) => i.wholesaleOrderId === saleOrder.wholesaleOrderId)
    if (insert === false) {
      const newItems = update(items, {
        $splice: [[index, 1, saleOrder]],
      })
      this.setState({
        initItems: newItems,
      })
    } else {
      const newItems = update(items, { $push: [saleOrder] })
      this.setState({
        initItems: newItems,
      })
    }
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    //this.props.clearRelatedOrders()
    if (this.state.expandedRowKeys.length > 0) {
      const row = []
      for (const value of expandedRowKeys) if (value !== this.state.expandedRowKeys[0]) row.push(value)
      this.setState({
        expandedRowKeys: row,
      })
    } else {
      this.setState({
        expandedRowKeys: expandedRowKeys,
      })
    }
  }

  onDuplicateMostRecentOrder = () => {
    const items = [...this.state.initItems];
    const sorted = items.sort((a, b) => b.wholesaleOrderId - a.wholesaleOrderId);
    this.onDuplicateOrder(sorted[0])
  }

  onDuplicateOrder = (currentOrder: any) => {
    const { sellerSetting } = this.state

    // console.log(sellerSetting);
    //
    // return;
    // var delivery: any = new Date(currentOrder!.deliveryDate)
    // delivery = moment(delivery).add(2, 'days').format('MM/DD/YYYY')
    // const items = get(currentOrder, 'orderItems', []).map(v => ({ ...v, displayOrderProduct: ~~v.displayOrder }))

    // const orderItemByProduct = groupBy(items, 'displayOrderProduct')

    // const duplicateItems = Object.values(orderItemByProduct).map(v => v[0])

    const fulfillmentDate = getFulfillmentDate(sellerSetting)
    // let tempOrderItems = duplicateItems.map((obj: any) => {
    //     return {
    //       wholesaleItemId: obj.wholesaleItem ? obj.wholesaleItem.wholesaleItemId : null,
    //       note: obj.note,
    //       UOM: obj.uom,
    //       price: obj.price,
    //       cost: obj.cost,
    //       itemName: obj.itemName,
    //       displayOrder: obj.displayOrder,
    //       pricingUOM: obj.pricingUOM,
    //       ratio: obj.ratio,
    //       pricingLogic: obj.pricingLogic,
    //       pricingGroup: obj.pricingGroup,
    //       editedItemName: obj.editedItemName
    //     }
    //   })
    const data = {
      deliveryDate: fulfillmentDate,
      // wholesaleCustomerClientId: currentOrder!.wholesaleClient.clientId,
      // itemList: tempOrderItems,
      wholesaleOrderId: currentOrder.wholesaleOrderId
    }
    this.props.resetDuplicateOnRowLoadingStatus()
    this.props.duplicateOrder(data, currentOrder.orderItems)
  }

  isItemNameContained = (item: TempOrder) => {
    const { search } = this.props
    let res = ''
    if (item.orderItems && typeof item.orderItems !== 'undefined' && item.orderItems.length > 0) {
      item.orderItems.forEach((el, index) => {
        if (index > 0) {
          res += ', '
        }
        res += el.wholesaleItem.variety
      })
    }
    return res.includes(search.toUpperCase())
  }

  renderLinkedIcon: any = () => {
    if (localStorage.getItem(CACHED_QBO_LINKED) != 'null') return <QBOImage src={qboImage} />
    return <NSImage />
  }

  render() {
    const { initItems } = this.state
    const { search, history, currentPage, pageSize, total, sellerSetting } = this.props
    const onSortString = (key: string, a: any, b: any) => {
      const stringA: string = a[key] ? a[key] : ''
      const stringB: string = b[key] ? b[key] : ''
      return stringA.localeCompare(stringB)
    }
    const capitalize = (s: any) => {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.toLowerCase().slice(1)
    }

    // let listItems: TempOrder[] = cloneDeep(initItems)

    // if (search) {
    //   console.log('is in search`123' + search);
    //   listItems = listItems.filter((item) => {
    //     return (
    //       formatNumber(item.totalPrice, 2).includes(search.toUpperCase()) ||
    //       item.status.includes(search.toUpperCase()) ||
    //       item.wholesaleOrderId.toString().includes(search.toUpperCase()) ||
    //       item.updatedDate.toString().includes(search.toUpperCase()) ||
    //       item.deliveryDate.toString().includes(search.toUpperCase()) ||
    //       this.isItemNameContained(item)
    //     )
    //   })
    // }

    const columns: ColumnProps<TempOrder>[] = [
      {
        title: 'TARGET FULFILLMENT DATE',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        align: 'center',
        width: 120,
        render: (date) => {
          return moment(date).format('MM/DD/YYYY')
        },
        // sorter: (a, b) => {
        //   if (a.deliveryDate && b.deliveryDate) {
        //     return new Date(a.deliveryDate).getTime() - new Date(b.deliveryDate).getTime()
        //   }
        // },
        sorter: true,
      },
      {
        title: 'SO #',
        dataIndex: 'wholesaleOrderId',
        key: 'wholesaleOrderId',
        width: 100,
        // sorter: (a, b) => {
        //   return a.wholesaleOrderId - b.wholesaleOrderId
        // },
        sorter: true,
        render(text) {
          const redirectPath = window.location.origin + window.location.pathname + `#/sales-order/${text}`
          return <span><a href={redirectPath}>{getOrderPrefix(sellerSetting, 'sales')}{text}</a></span>
        },
      },
      {
        title: 'REFERENCE',
        dataIndex: 'reference',
        key: 'reference',
        width: 150,
        sorter: true,
        // sorter: (a, b) => {
        //   return onSortString('reference', a, b)
        // },
      },
      {
        title: '# OF ITEMS',
        dataIndex: 'totalItems',
        key: 'number of items',
        width: 100,
        align: 'center',
        // render: (orderItems: any[]) => {
        //   return orderItems && typeof orderItems !== 'undefined' ? orderItems.length : 0
        // },
      },
      {
        title: 'ITEMS',
        dataIndex: 'itemNames',
        key: 'items',
        // render: (orderItems: any[]) => {
          // let res = ''
          // if (orderItems && typeof orderItems !== 'undefined' && orderItems.length > 0) {
          //   orderItems.forEach((item, index) => {
          //     if (index > 0) {
          //       res += ', '
          //     }
          //     if (item.wholesaleItem != null) {
          //       res += item.wholesaleItem.variety
          //     } else {
          //       res += item.itemName
          //     }
          //   })
          // }
          // return res
        // },
      },
      {
        title: 'UNITS ORDERED / SHIPPED',
        dataIndex: 'orderItems',
        key: 'units ordered/shipped',
        width: 150,
        align: 'center',
        render: (totalQuantity: number,record: any) => {
          return `${record.totalQuantity === 0 ? '--' : record.totalQuantity} / ${record.totalPicked === 0 ? '--' : record.totalPicked}`
        },
      },
      {
        title: 'ORDER STATUS',
        dataIndex: 'status',
        key: 'wholesaleOrderStatus',
        width: 100,
        // sorter: (a, b) => {
        //   const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
        //     (meta: any) => meta.dataIndex === 'status',
        //   )
        //   if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('status', b, a)
        //   return this.onSortString('status', a, b)
        // },
        sorter: true,
        render: (status: string) => {
          return (
            <Flex style={{ alignItems: 'center' }}>
              {status === 'INVOICED' ? this.renderLinkedIcon() : null}
              <span style={{ marginLeft: 10 }}>{capitalize(formatOrderStatus(status, sellerSetting))}</span>
            </Flex>
          )
        },
      },
      {
        title: '',
        key: 'duplicate',
        align: 'center',
        width: 100,
        render: (_v: any, record: any) => {
          // const url = location.href.indexOf('/vendor') >= 0 ? 'purchase-cart' : 'sales-cart'
          // return <ThemeLink to={`/order/${record.wholesaleOrderId}/${url}`}>View <ThemeIcon type="arrow-right" /></ThemeLink>
          return (
            <Tooltip title="Duplicate Order">
              <ThemeIconButton disabled={record.status == 'CANCEL'} className="no-border" onClick={() => this.onDuplicateOrder(record)}>
                <IconSvg
                  type="duplicate"
                  viewBox={void 0}
                  className={record.status == 'CANCEL' ? 'cancel-icon' : ''}
                  style={{ width: 24, height: 24, marginRight: 4, fill: 'transparent' }}
                />
              </ThemeIconButton>
            </Tooltip>
          )
        },
      },
    ]

    const onRow = (record: any, index: number) => {
      return {
        onClick: (_event: any) => {
          _event.preventDefault()
          const tagName = _event.target.tagName
          if (tagName !== 'button' && tagName !== 'path' && tagName !== 'svg') {
            history.push(`/sales-order/${record.wholesaleOrderId}`)
          }
        },
      }
    }

    return (
      <MultiBackSortTable
        ref={this.mutiSortTable}
        css={tableCss(false, false)}
        columns={columns}
        dataSource={initItems}
        rowKey="id"
        expandRowByClick
        expandIconAsCell={false}
        expandIcon={emtpyNode}
        expandedRowKeys={this.state.expandedRowKeys}
        onExpandedRowsChange={this.onExpendedRowsChange}
        onRow={onRow}
        pagination={{
          total: total,
          pageSize: pageSize,
          current: currentPage,
          defaultCurrent: 1,
        }}
        onChange={this.props.changePage}
        onSort={this.props.changePage}
        loading={this.props.loading}
      />
    )
  }
}
