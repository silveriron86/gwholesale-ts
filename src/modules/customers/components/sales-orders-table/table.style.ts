import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { yellow, mutedGreen, red, white } from '~/common'

export const TableTr = styled('tr')({
  height: '50px',
})

export const expandDivClx = css({})

export const tableCss = (hasPadding: boolean, isInventory = false) =>
  css({
    '&': {
      padding: hasPadding ? '0 50px' : '0',
      '.ant-pagination': {
        paddingRight: '30px',
      },
      '& .ant-table-content': {
        '& .ant-table-body': {
          '& > table': {
            borderTop: 'none',
            '& .ant-table-thead': {
              // boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
              '& > tr > th:first-of-type': {
                paddingLeft: isInventory ? '59px' : '',
              },
              '& > tr > th:last-of-type': {
                paddingRight: isInventory ? '50px' : '',
              },
              '& > tr > th': {
                backgroundColor: white,
                border: 'none',
              },
            },
            [`& ${TableTr} td`]: {
              padding: '3px 10px',
              textAlign: 'left',
              whiteSpace: 'pre-wrap',
              wordWrap: 'break-word',
            },
            '& .ant-table-tbody': {
              '& > tr > .buttonCell': {
                paddingTop: '0px',
                paddingBottom: '0px',
                paddingRight: '0px',
                paddingLeft: '7px',
                minWidth: '110px',
              },
              '& > tr > td:first-of-type > span': {
                marginLeft: isInventory ? '50px' : '',
              },
              '& > tr > td': {
                paddingTop: '19px',
                paddingBottom: '14px',
                paddingLeft: '9px',
                paddingRight: '9px',
              },
            },
          },
        },
      },
    },
  })

export const Stock = styled('div')(({ stock }: { stock: number }) => ({
  borderLeft: `solid 5px ${stock === 0 ? yellow : stock > 0 ? mutedGreen : red}`,
  height: '17px',
  display: 'flex',
  fontWeight: 700,
  alignItems: 'center',
  justifyContent: 'center',
  paddingLeft: '10px',
}))

export const polygonCss = css({
  width: '12px',
  height: '12px',
  marginLeft: '5px',
})

export const EmptyTr = styled('tr')({
  height: '15px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})

export const QBOImage = styled('img')({
  width: '24px',
  height: '24px',
})
