/**@jsx jsx */
import React, { useCallback, useEffect } from 'react'
import { jsx } from '@emotion/core'
import lodash from 'lodash'
import GrubSuggest from '~/components/GrubSuggest'
import { SaleItem } from '~/schema'
import { Theme } from '~/common'
import { onLoadFocusOnFirst } from '~/common/jqueryHelper'
import { HeaderContainer } from '~/modules/inventory/components/inventory-header.style'

// 外部处理
interface SearchData {
  name: string[]
  category: string[]
  provider: string[]
}

function pureFind(data: string[], query: string) {
  // avoid repeation of `toLowerCase`
  const q = query.toLowerCase()
  return data.filter((n) => n && n.toLowerCase().indexOf(q) > -1).slice(0, 5)
}

function searchResult(datum: SearchData, query: string, isVendor: boolean) {
  const titles = [`Search By ${isVendor ? 'Vendor' : 'Customer'} Name`, 'Search By Contact Name', 'Search By Address']
  return ['customerName', 'contactName', 'address'].map((key, index) => ({
    title: titles[index],
    children: pureFind(datum[key], query).map((n) => ({ title: n })),
  }))
}

export interface Props {
  waiting: boolean
  rawData: Customer[]
  onSearch: (search: string) => void
  theme: Theme
  changeShow: Function
  getAddress: Function
  isVendor?: boolean
  search: string
  active: boolean
}

export const SearchHeader: React.SFC<Props> = (props) => {
  useEffect(() => {
    onLoadFocusOnFirst('simple-input-wrapper')
  }, [])
  // 外部处理
  // data processing | useCallback to memorize result
  const getSearchSource = useCallback(
    () => {
      let filtered = props.rawData
      filtered = props.rawData.filter(d =>  d.status === (props.active ? 'ACTIVE' : 'INACTIVE'))
      return {
        customerName: lodash.uniq(filtered.map((n) => (n.clientCompanyName ? n.clientCompanyName : ''))).sort(),
        contactName: lodash.uniq(filtered.map((n) => (n.mainContactName ? n.mainContactName : ''))).sort(),
        address: lodash
          .uniq(filtered.map((n) => props.getAddress(n.mainBillingAddress ? n.mainBillingAddress.address : '')))
          .sort(),
      }
    },
    [props.rawData, props.active],
  )

  // search by keywords
  const handleSuggest = (keyword: string) => {
    const searchSource = getSearchSource()
    return searchResult(searchSource, keyword, props.isVendor)
  }

  return (
    <HeaderContainer className="simple-input-wrapper" style={{ padding: 0 }}>
      <GrubSuggest
        defaultValue={props.search}
        placeholder={`Search ${props.isVendor ? 'vendor' : 'customer'} name, contact name, or address`}
        onSuggest={handleSuggest}
        onSearch={props.onSearch}
        theme={props.theme}
        width={557}
      />
    </HeaderContainer>
  )
}
