/**@jsx jsx */
import React, { useCallback, useEffect, useState } from 'react'
import { jsx } from '@emotion/core'
import lodash from 'lodash'
import moment, { Moment } from 'moment'

import GrubSuggest from '~/components/GrubSuggest'
import { Theme } from '~/common'
import { TempOrder } from '~/schema'
import { Row, Col, DatePicker, Select } from 'antd'
import { DetailsWrapper, DetailsTitle, floatLeft, ThemeIcon } from '../../customers.style'
import { DateEditorWrapper } from '~/modules/orders/components/orders-header.style'

interface SearchData {
  wholesaleOrderId: number[]
  fullName: string[]
  updatedDate: number[]
  deliveryDate: number[]
}

function pureFind(data: string[], query: string) {
  // avoid repeation of `toLowerCase`
  const q = query.toLowerCase()
  return data.filter((n) => n && n.toLowerCase().indexOf(q) > -1).slice(0, 5)
}

function searchResult(datum: SearchData, query: string) {
  return ['fullName'].map((key) => ({
    title: key,
    children: pureFind(datum[key], query).map((n) => ({ title: n })),
  }))
}

export interface Props {
  rawData: TempOrder[]
  onShowDrawer: () => void
  onSearch: (search: string) => void
  theme: Theme
  handleNewOrder: Function
  onDateChange: Function
  fromDateProps: Moment
  toDateProps: Moment
  curPreset?: number
  search?: string
}

const presets = [
  {
    key: 0,
    label: 'Past 7 Days',
    from: moment().subtract(7, 'days'),
    to: moment(),
  },
  {
    key: 1,
    label: 'Past 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment(),
  },
  {
    key: 2,
    label: 'Today',
    from: moment(),
    to: moment(),
  },
  {
    key: 3,
    label: 'Tomorrow',
    from: moment().add(1, 'days'),
    to: moment().add(1, 'days'),
  },
  {
    key: 4,
    label: 'Today and Tomorrow',
    from: moment(),
    to: moment().add(1, 'days'),
  },
  {
    key: 5,
    label: 'Next 7 Days',
    from: moment(),
    to: moment().add(7, 'days'),
  },
  {
    key: 6,
    label: 'Next 30 Days',
    from: moment(),
    to: moment().add(30, 'days'),
  },
  {
    key: 7,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment().add(30, 'days'),
  },
  {
    key: 8,
    label: 'Custom Date Range',
  },
]

export const SalesTableSearchComponent: React.SFC<Props> = (props) => {
  const { fromDateProps, toDateProps, onDateChange } = props
  // data processing | useCallback to memorize result
  const type = location.href.indexOf('/vendor') >= 0 ? 'Vendor' : 'Account'
  const [fromDate, setFromDate] = useState(fromDateProps ? fromDateProps : moment().subtract(30, 'days'))
  const [toDate, setToDate] = useState(toDateProps ? toDateProps : moment().add(30, 'days'))
  const [curPreset, setCurPreset] = useState(props.curPreset)
  const dateFormat = 'MM/DD/YYYY'
  const calendarIcon = <ThemeIcon type="calendar" />
  const { RangePicker } = DatePicker
  useEffect(() => {
    setFromDate(fromDateProps)
  }, [fromDateProps])

  useEffect(() => {
    setToDate(toDateProps)
  }, [toDateProps])

  const getSearchSource = useCallback(
    () => ({
      wholesaleOrderId: lodash.uniq(props.rawData.map((n) => n.wholesaleOrderId)).sort(),
      fullName: lodash.uniq(props.rawData.map((n) => n.fullName)).sort(),
      totalPrice: lodash.uniq(props.rawData.map((n) => n.totalPrice)).sort(),
      updatedDate: lodash.uniq(props.rawData.map((n) => n.updatedDate)).sort(),
      deliveryDate: lodash.uniq(props.rawData.map((n) => n.deliveryDate)).sort(),
    }),
    [props.rawData],
  )

  // search by keywords
  const handleSuggest = (keyword: string) => {
    const searchSource = getSearchSource()
    return searchResult(searchSource, keyword)
  }

  const onPresetChange = (val: number) => {
    setCurPreset(val)
    const dateFormat = 'MM/DD/YYYY'
    const selected = presets[val]
    if (selected.from && selected.to) {
      onDateChange([selected.from, selected.to], [selected.from.format(dateFormat), selected.to.format(dateFormat)])
    }
  }

  return (
    <DetailsWrapper style={{ backgroundColor: 'white' }}>
      <div className="action-container">
        <Row>
          <Col md={18} style={{ display: 'flex' }}>
            <DetailsTitle style={{ ...floatLeft, marginRight: 20, padding: '4px 0' }}>
              {type === 'Vendor' ? 'Purchase Orders' : 'Sales Orders'}
            </DetailsTitle>
            <DateEditorWrapper className="orders-delivery-date" style={{ minWidth: 230 }}>
              {curPreset == 8 && (
                <RangePicker
                  defaultValue={[fromDate, toDate]}
                  format={dateFormat}
                  suffixIcon={calendarIcon}
                  onChange={onDateChange}
                  className="tab-able-el"
                  allowClear={false}
                />
              )}
              <Select
                defaultValue={3}
                value={curPreset}
                className="date-filter-select"
                onChange={onPresetChange}
                dropdownClassName="full-select"
              >
                {presets.map((el: any) => {
                  return (
                    <Select.Option value={el.key} key={el.key}>
                      {el.label}
                    </Select.Option>
                  )
                })}
              </Select>
            </DateEditorWrapper>
          </Col>
          <Col md={6}>
            <Row type="flex" justify="end">
              <div style={{ float: 'right', marginRight: 10 }} className="search-30">
                <GrubSuggest
                  defaultValue={props.search}
                  onSuggest={handleSuggest}
                  onSearch={props.onSearch}
                  theme={props.theme}
                />
              </div>
            </Row>
          </Col>
        </Row>
      </div>
    </DetailsWrapper>
  )
}
