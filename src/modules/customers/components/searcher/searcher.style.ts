import styled from '@emotion/styled'

import { black, mediumGrey } from '~/common'

export const HeaderContainer = styled('div')((props) => ({
  borderTop: `5px solid ${props.theme.main}`,
  width: '100%',
  backgroundColor: 'white',
  padding: '17px 19px 46px 12px',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
}))

export const Header = styled('div')({
  width: '100%',
  height: '232px',
  boxSizing: 'border-box',
  display: 'flex',
  justifyContent: 'space-between',
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const ItemName = styled('div')({
  color: black,
  fontSize: '36px',
  fontWeight: 700,
  letterSpacing: '0.05em',
  lineHeight: '50px',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const TableHeaderText = styled.p`
  font-size: 20px;
  font-weight: bold;
  line-height: 23px;
  color: ${mediumGrey};
`
