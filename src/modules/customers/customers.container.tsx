/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps, RouteProps } from 'react-router'
import { cloneDeep, sortBy } from 'lodash'
import { Dropdown, Button, Menu, Icon, Modal, notification, Popover } from 'antd'
import { withTheme } from 'emotion-theming'
import { Link } from 'react-router-dom'
import jQuery from 'jquery'

import {
  ThemeCheckbox,
  ThemeLink,
  ThemeTable,
  tableCss,
  ThemeSpin,
  LoadingOverlay,
} from '~/modules/customers/customers.style'

import { Theme, CACHED_NS_LINKED } from '~/common'
import { Icon as IconSvg, MessageType } from '~/components'

import { GlobalState } from '~/store/reducer'
import { CustomerType, CustomerStatus, TempCustomer, Address, BriefClient } from '~/schema'
import { CustomersDispatchProps, TempCustomersStateProps, CustomersModule } from './customers.module'
import { ImportCustomerModal } from '../account/components/import-table.component'
import InfiniteScroll from 'react-infinite-scroll-component'

import {
  CustomerContainer,
  HeaderContainer,
  HeaderTitle,
  HeaderOptions,
  buttonStyle,
  ghostButtonStyle,
  SortByIcon,
  BodyContainer,
  SortByText,
  CardTop,
  CardStatus,
  CardType,
  CardTypeIcon,
  CardBody,
  CardText,
  CardLine,
  CardCircle,
  CardInfo,
  CardName,
  MainToLink,
  phoneLink,
  ThemeCard,
} from './customers.style'
import CustomerForm from './components/CustomerForm'
import PageLayout from '~/components/PageLayout'
import { ViewSwitcherEl } from '~/components/elements/view-selector'
import { SearchHeader } from './components/search-header.component'
import { CustomerSyncModal } from './components/customer-sync-modal.container'
import { onSortString } from '~/common/utils'

export type CustomersProps = CustomersDispatchProps &
  TempCustomersStateProps &
  RouteComponentProps &
  RouteProps & {
    theme: Theme
  }

interface CustomerState {
  createShow: boolean
  syncShow: boolean
  selectedRows: TempCustomer[]
  sort: string
  search: string
  showModalImportCustomer: boolean
  selectedQBCustomers: TempCustomer[]
  message: string
  type: MessageType | null
  viewType: string
  currentPage: number
  page: number
}

const newCustomer: any = {
  mainContact: {
    name: '',
    email: '',
  },
  mobilePhone: '',
  mainAddress: {
    address: {
      address1: '',
      address2: '',
      city: '',
      state: '',
      zipCode: '',
    },
  },
  clientCompany: {
    companyName: '',
  },
  type: CustomerType.INDIVIDUAL,
  status: CustomerStatus.ACTIVE,
}

export class CustomersComponent extends React.PureComponent<CustomersProps, CustomerState> {
  multiSortTable = React.createRef<any>()

  state: CustomerState
  constructor(props: CustomersProps) {
    super(props)
    const sessionSearch = localStorage.getItem('CUSTOMERS_SEARCH')
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search)
    const search = urlParams.get('search')
    const page = urlParams.get('page')

    this.state = {
      createShow: false,
      syncShow: false,
      sort: '',
      search: search ? search : '',
      showModalImportCustomer: false,
      selectedQBCustomers: [],
      message: '',
      type: null,
      page: page ? parseInt(page, 10) : 0,
      currentPage: 0,
      pageSize: 12,
      queryParam: '',
      active: 1,
      viewType: 'table',
      selectedRows: [],
    }
  }

  componentDidMount() {
    const sessionSearch = localStorage.getItem('CUSTOMERS_SEARCH')
    if (sessionSearch) {
      this.updateURL()
    }
    // Tab Order
    window.localStorage.removeItem('CUSTOM-TAB-INDEX')
    function setScroll(cards, tabIndex) {
      if (cards.length === 0) {
        return
      }
      if (tabIndex == -1) {
        jQuery('html, body').animate(
          {
            scrollTop: 0,
          },
          100,
        )
        jQuery('.search-customer-input input').focus()
        return
      }
      let currentCard = jQuery(cards[tabIndex])
      if (currentCard.length) {
        currentCard.css('borderWidth', 2)
        jQuery('html, body').animate(
          {
            scrollTop: currentCard.offset().top - 200,
          },
          100,
        )
      }
    }

    let index = window.localStorage.getItem('CUSTOM-TAB-INDEX')
    if (!index) {
      window.localStorage.setItem('CUSTOM-TAB-INDEX', '0')
    } else {
      const cards = jQuery('.customer-card')
      if (cards.length > 0) {
        setScroll(cards, index)
      } else {
        window.localStorage.setItem('CUSTOM-TAB-INDEX', '0')
      }
    }

    jQuery('.search-customer-input').ready(function (e: any) {
      jQuery('.search-customer-input input').focus()
    })

    jQuery('.search-customer-input input').focus(() => {
      window.localStorage.setItem('CUSTOM-TAB-INDEX', '0')
      const cards = jQuery('.customer-card')
      if (cards.length > 0) {
        cards.css('borderWidth', 0)
      }
    })

    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode === 13) {
          // Enter
          if (e.target.tagName !== 'INPUT') {
            e.preventDefault()
            let tabIndex: number = parseInt(window.localStorage.getItem('CUSTOM-TAB-INDEX'), 10)
            const cards = jQuery('.customer-card')
            const currentCard = cards[tabIndex]
            if (jQuery(currentCard).find('a')[0]) {
              window.location.href = jQuery(currentCard).find('a')[0].href
            }
          }
        } else if (e.keyCode === 9) {
          // Tab
          if (e.target.tagName === 'INPUT') {
            const cards = jQuery('.customer-card')
            if (cards.length > 0) {
              jQuery(cards[0]).css('borderWidth', 2)
            }
          } else {
            e.preventDefault()
            const cards = jQuery('.customer-card')
            let tabIndex: number = parseInt(window.localStorage.getItem('CUSTOM-TAB-INDEX'), 10)
            if (e.shiftKey) {
              tabIndex--
              tabIndex = tabIndex < 0 ? -1 : tabIndex
            } else {
              tabIndex++
              tabIndex = tabIndex >= cards.length ? -1 : tabIndex
            }
            cards.css('borderWidth', 0)
            setScroll(cards, tabIndex)
            window.localStorage.setItem('CUSTOM-TAB-INDEX', tabIndex.toString())
          }
        }
      })

    // this.props.getAllCustomersByCondition({
    //   queryParam: this.state.queryParam,
    //   page: this.state.currentPage,
    //   pageSize: this.state.pageSize,
    //   sortType: this.state.sort,
    //   active: this.state.active
    // });

    if (this.props.loadingCustomers) {
      this.props.resetLoading()
      this.props.getBriefCustomers()
    }
  }

  componentWillUnmount() {
    this.props.setSearchParam({ searchParam: '', sortType: '' })
    this.props.dispose$()
  }

  componentWillReceiveProps(nextProps: CustomersProps) {
    if (nextProps.message !== this.state.message) {
      this.setState({
        message: nextProps.message,
        type: nextProps.type,
      })
      if (this.state.createShow && nextProps.message === 'Created Customer Successfully') {
        this.props.setSearchParam({ searchParam: '', sortType: '' })
        // this.props.getAllCustomersByCondition({ queryParam: "", page: 0, pageSize: this.state.pageSize, active: 1 });
        this.props.getAllCustomersForUser()
        this.setState({
          createShow: false,
        })
      }
      if (this.state.showModalImportCustomer && nextProps.message === 'Customers Imported Successfully') {
        this.props.setSearchParam({ searchParam: '', sortType: '' })
        // this.props.getAllCustomersByCondition({ queryParam: "", page: 0, pageSize: this.state.pageSize, active: 1 });
        this.props.getAllCustomersForUser()
        this.setState({
          showModalImportCustomer: nextProps.showModalImportCustomer,
        })
      }
      if (nextProps.type != null) {
        notification[nextProps.type!]({
          message: nextProps.type!.toLocaleUpperCase(),
          description: nextProps.message,
          onClose: this.onCloseNotification,
          duration: nextProps.type === 'success' ? 5: 4.5
        })
      }
    }

    // if (this.props.items === null || nextProps.items.length !== this.props.items.length) {
    if (nextProps.newCustomerId > 0) {
      setTimeout(() => {
        this.props.history.push(`/customer/${nextProps.newCustomerId}/orders`)
        this.props.endAdding()
      }, 300)
    }
    // }
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  fetchMoreData = () => {
    const currPage = this.state.currentPage + 1
    // this.props.getAllCustomersByCondition({
    //   queryParam: this.state.search,
    //   page: currPage,
    //   pageSize: this.state.pageSize,
    //   sortType: this.state.sort,
    //   active: this.state.active
    // });
    setTimeout(() => {
      this.setState({
        currentPage: currPage,
      })
    }, 200)
  }

  changeShow = (e: any) => {
    console.log(e)
    this.setState({
      active: e.target.checked ? 0 : 1,
      currentPage: 0,
    })
    // this.props.setSearchParam({ searchParam: this.state.search, sortType: this.state.sort });
    // this.props.getAllCustomersByCondition({
    //   queryParam: this.state.search,
    //   page: 0,
    //   pageSize: this.state.pageSize,
    //   sortType: this.state.sort,
    //   active: e.target.checked ? 0 : 1
    // });
  }

  render() {
    const { loadingCustomers, addingCustomers, briefCustomers } = this.props
    const { viewType } = this.state
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Selling & Shipping-Customers'}>
        <CustomerContainer>
          {this.renderHeader()}

          <ThemeSpin spinning={loadingCustomers} wrapperClassName="full-width">
            <div style={{ minHeight: 300 }}>
              {viewType == 'table' ? this.renderBodyTableView() : this.renderBodyListView()}
            </div>
          </ThemeSpin>

          {this.renderCreateCustomerModal()}
          {this.renderImportCustomerModal()}

          <CustomerSyncModal
            visible={this.state.syncShow}
            onCancel={this.onCloseBatchSyncCustomer}
            onOk={this.onSyncCustomer}
            customers={briefCustomers ? briefCustomers : []}
            loading={this.props.loading}
            onSelectionChange={this.onRowSelection}
          />
        </CustomerContainer>
        {addingCustomers && (
          <LoadingOverlay>
            <ThemeSpin tip="Loading..." spinning={true} size="large" />
          </LoadingOverlay>
        )}
      </PageLayout>
    )
  }

  _getAddress = (address: Address) => {
    let ret = ''
    if (address.street1) {
      ret += address.street1
    }
    if (address.street2) {
      ret += (ret && ' ') + address.street2
    }
    if (address.city) {
      ret += (ret && ' ') + address.city
    }
    if (address.zipcode) {
      ret += (ret && ' ') + address.zipcode
    }
    if (address.state) {
      ret += (ret && ' ') + address.state
    }
    if (address.country) {
      ret += (ret && ' ') + address.country
    }
    return ret
  }

  private onClickImportQBCustomer = () => {
    this.props.resetLoading()
    this.props.getQBCustomers()
    this.setState({
      createShow: false,
      showModalImportCustomer: true,
    })
  }

  onSyncCustomer = (clientIds: string[]) => {
    if (this.props.loading == true) return
    this.props.resetLoading()

    var isNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    if (!isNS) this.props.syncQBOCustomers(clientIds)
    //else this.props.syncNSOrders(orderIds)
  }

  private onClickBatchSyncCustomer = () => {
    this.props.resetLoading()
    this.setState({
      syncShow: true,
      selectedRows: [],
    })
  }

  private onCloseBatchSyncCustomer = () => {
    this.setState({
      syncShow: false,
      selectedRows: [],
    })
  }

  private onRowSelection = (selectedRows: TempCustomer[]) => {
    this.setState({
      selectedRows: selectedRows,
    })
  }

  private onClickNewCustomer = () => {
    this.setState({
      createShow: true,
    })
  }

  private onCloseNewCustomer = () => {
    this.setState({
      createShow: false,
    })
  }

  private onClickCreate = (customerData: any) => {
    this.props.startAdding()
    this.props.createCustomer(customerData)
    this.setState({
      createShow: false,
    })
  }

  private onClickSortMenu = ({ key }: { key: string }) => {
    if (key != this.state.sort) {
      this.setState({
        sort: key.toLowerCase(),
        currentPage: 0,
      })
      this.props.setSearchParam({ searchParam: this.state.search, sortType: key })
      // this.props.getAllCustomersByCondition({
      //   queryParam: this.state.search,
      //   page: 0,
      //   pageSize: this.state.pageSize,
      //   sortType: key.toLowerCase(),
      //   active: this.state.active
      // });
    }
  }

  private onSearch = (value: string, isSelected?: boolean) => {
    const searchStr = value.toUpperCase()
    const listItems = this.props.briefCustomers.filter(
      (item) =>
        (item.clientCompanyName && item.clientCompanyName.toUpperCase() === searchStr) ||
        (item.mainContactName && item.mainContactName.toUpperCase() === searchStr) ||
        (item.mainBillingAddress && this._getAddress(item.mainBillingAddress.address).toUpperCase() === searchStr),
    )
    console.log(value, isSelected)
    if (listItems.length === 1 && isSelected === true) {
      window.location.href = `#/customer/` + listItems[0].clientId + `/orders`
      return
    }

    if (this.state.search != value) {
      this.setState(
        {
          search: value,
          page: 0,
          // currentPage: 0
        },
        () => {
          this.updateURL()
        },
      )
      // this.props.setSearchParam({ searchParam: value, sortType: this.state.sort });
      // this.props.getAllCustomersByCondition({
      //   queryParam: value,
      //   page: 0,
      //   pageSize: this.state.pageSize,
      //   sortType: this.state.sort,
      //   active: this.state.active
      // });
    }
  }

  updateURL = () => {
    var url = new URL(window.location.href)
    var queryParams = url.searchParams
    const { search, page } = this.state
    if (search) {
      queryParams.set('search', search)
    }
    if (page) {
      queryParams.set('page', page)
    }
    url.search = queryParams.toString()
    localStorage.setItem('CUSTOMERS_SEARCH', url.search)
    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString())
  }

  onChangePage = (page, pageSize) => {
    this.setState(
      {
        page: page - 1,
      },
      () => {
        this.updateURL()
      },
    )
  }

  private onTablePaginatorChanged = (page: number, pageSize: number | undefined) => {
    this.props.setSearchParam({ searchParam: this.state.search, sortType: this.state.sort })
    const pageNumber = page > 0 ? page - 1 : 0
    // this.props.getAllCustomersByCondition({
    //   queryParam: this.state.search,
    //   page: pageNumber,
    //   pageSize: pageSize,
    //   sortType: this.state.sort,
    //   active: this.state.active
    // });
    this.setState({ currentPage: pageNumber, pageSize: pageSize })
  }

  private onSwitchingView = () => {
    this.setState({ viewType: this.state.viewType == 'table' ? 'list' : 'table', currentPage: 0 })
    // this.props.setSearchParam('')
    // this.props.getAllCustomersByCondition({
    //   queryParam: this.state.search,
    //   page: 0,
    //   pageSize: this.state.pageSize,
    //   sortType: this.state.sort,
    //   active: this.state.active
    // });
  }

  private renderSortByMenu() {
    const sorts = ['Type', 'Status', 'Name']
    return (
      <Menu onClick={this.onClickSortMenu} style={{ paddingLeft: '8px' }}>
        {sorts.map((sort) => (
          <Menu.Item key={sort}>{sort}</Menu.Item>
        ))}
      </Menu>
    )
  }

  changeSearchShow = (e: any) => {
    this.props.getAllCustomersForUser()
  }

  private renderHeader() {
    const title = location.href.indexOf('vendor-') >= 0 ? 'Vendors' : 'Customers'
    const { viewType, search, active } = this.state

    return (
      <HeaderContainer>
        <HeaderTitle>{title}</HeaderTitle>
        <HeaderOptions className="search-customer-input">
          <div style={{ width: 557 }}>
            <SearchHeader
              search={search}
              rawData={this.props.briefCustomers}
              waiting={false}
              // onShowDrawer={this.onSubmit}
              onSearch={this.onSearch}
              theme={this.props.theme}
              getAddress={this._getAddress}
              active={active}
            />
          </div>
          {/* <Input.Search
            placeholder="Search customer name, contact name, or address"
            size="large"
            style={{ width: '557px', border: '0' }}
            enterButton={searchButton}
            onSearch={this.onSearch}
          /> */}
          <div style={{ flex: 1, textAlign: 'left', marginTop: '15px', marginLeft: '20px' }}>
            <ThemeCheckbox onChange={this.changeShow}>Show inactive</ThemeCheckbox>
          </div>
          <div>
            {/*
            <Button
              size="large"
              icon="plus"
              type="primary"
              style={{
                ...buttonStyle,
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.onClickBatchSyncCustomer}
            >
              Batch Sync
            </Button>
            */}
            <Button
              size="large"
              icon="plus"
              type="primary"
              style={{
                ...buttonStyle,
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.onClickNewCustomer}
            >
              Add Customer
            </Button>

            <Dropdown overlay={this.renderSortByMenu()} placement="bottomCenter">
              <Button size="large" style={ghostButtonStyle}>
                <SortByText>SORT BY</SortByText>
                <Icon type="caret-down" style={SortByIcon} />
              </Button>
            </Dropdown>
          </div>
        </HeaderOptions>
        <div style={{ marginTop: -20, textAlign: 'left', position: 'relative', paddingBottom: 30, marginLeft: -60 }}>
          <ViewSwitcherEl viewType={viewType} onClick={this.onSwitchingView} />
        </div>
      </HeaderContainer>
    )
  }

  _searchItem = (item, search) => {
    let text =
      (item.mainContactName
        ? item.mainContactName + ' ' + (item.clientCompanyName ? item.clientCompanyName : '')
        : '') + ' '
    if (item.mainBillingAddress && item.mainBillingAddress.address) {
      text += this._getAddress(item.mainBillingAddress.address)
    }
    return text.toUpperCase().includes(search.toUpperCase())
  }

  private renderBodyTableView() {
    const { briefCustomers, loadingCustomers } = this.props
    const { sort, search, active, pageSize, currentPage } = this.state

    if (!briefCustomers) {
      return null
    }

    let customerList = briefCustomers.filter((item) => {
      if (active == 0 && item.status === 'ACTIVE') {
        return false
      } else if (active == 1 && item.status == 'INACTIVE') {
        return false
      }
      return true
    })

    let hasMore = false
    if (search) {
      customerList = customerList.filter((item) => {
        return this._searchItem(item, search)
      })
      hasMore = false
    } else {
      hasMore = (currentPage + 1) * pageSize < customerList.length
      customerList = customerList.slice(0, (currentPage + 1) * pageSize)
    }
    let listItems: BriefClient[] = cloneDeep(customerList)

    if (sort != '') {
      let key = sort.toLowerCase()
      if (sort == 'name') {
        key = 'clientCompanyName'
        listItems = sortBy(listItems, [
          function (o) {
            if (o.clientCompanyId && o.clientCompanyName) {
              return o.clientCompanyName.toLowerCase()
            }
          },
        ])
      } else {
        listItems = sortBy(listItems, key)
      }
    }

    const List = listItems.map((item) => (
      <ThemeCard key={item.clientId} className="customer-card">
        <Link to={`/customer/${item.clientId.toString()}/orders`}>
          <CardBody style={{ paddingTop: 48 }}>
            <CardTop>
              <CardStatus>
                {item.status === 'ACTIVE' ? (
                  <Icon type="check-circle" width={13} height={13} viewBox="0 0 13 13" />
                ) : (
                  <Icon type="close-circle" />
                )}
                <span style={{ marginLeft: '5px' }}>{item.status.toUpperCase()}</span>
              </CardStatus>
              <Link to={`/customer/${item.clientId.toString()}/account`}>
                <CardType className="c-type">
                  {item.businessType ? item.businessType.toUpperCase() : 'INDIVIDUAL'}
                </CardType>
              </Link>
              <Link to={`/customer/${item.clientId.toString()}/addresses`}>
                <CardTypeIcon className="c-home">
                  <span className="c-home">
                    {/* {item.businessType == null || item.businessType === CustomerType.INDIVIDUAL ? <Icon type="user" /> : <Icon type="home" />} */}
                    <span className="v-home">
                      <Icon type="home" />
                    </span>
                  </span>
                </CardTypeIcon>
              </Link>
            </CardTop>
            {item.clientCompanyName ? (
              <Popover content={item.clientCompanyName}>
                <CardName>{item.clientCompanyName}</CardName>
              </Popover>
            ) : (
              <CardName>{'N/A'}</CardName>
            )}
            {item.mainBillingAddress ? (
              <Popover content={this._getAddress(item.mainBillingAddress.address)}>
                <CardInfo>{this._getAddress(item.mainBillingAddress.address)}</CardInfo>
              </Popover>
            ) : (
              <CardInfo>N/A</CardInfo>
            )}
            <CardLine />
            {item.mainContactName && (
              <CardText>
                <IconSvg type="information" width={18} height={18} viewBox="0 0 18 18" />
                <Popover content={item.mainContactName}>
                  <span>{item.mainContactName}</span>
                </Popover>
              </CardText>
            )}
          </CardBody>
        </Link>
        <MainToLink
          href={
            item.mainContactEmail ? `mailto:${item.mainContactEmail}` : `/#/customer/${item.clientId.toString()}/orders`
          }
        >
          <CardText style={{ textDecoration: 'underline' }}>
            <IconSvg type="email" width={18} height={18} viewBox="-1 -4 18 18" />
            {item.mainContactEmail ? (
              <Popover content={item.mainContactEmail}>
                <span>{item.mainContactEmail}</span>
              </Popover>
            ) : (
              <span>N/A</span>
            )}
          </CardText>
        </MainToLink>
        <a
          href={
            item.mobilePhone
              ? `tel:${item.mobilePhone ? item.mobilePhone : ''}`
              : `/#/customer/${item.clientId.toString()}/orders`
          }
          style={phoneLink}
        >
          <CardText>
            <IconSvg type="phone" width={18} height={18} viewBox="0 0 18 18" />
            <span>{item.mobilePhone ? item.mobilePhone : 'N/A'}</span>
          </CardText>
        </a>
        <CardCircle />
      </ThemeCard>
    ))

    return (
      <div>
        <InfiniteScroll
          dataLength={customerList.length}
          next={this.fetchMoreData}
          hasMore={hasMore}
          loader={<h4>Loading...</h4>}
          scrollableTarget="document_body"
          endMessage={
            customerList.length > 0 && (
              <p style={{ textAlign: 'center' }}>
                <b>END</b>
              </p>
            )
          }
        >
          <BodyContainer>
            {customerList.length > 0 ? List : !loadingCustomers ? <div>No matching results</div> : ''}
          </BodyContainer>
        </InfiniteScroll>
      </div>
    )
  }

  private renderBodyListView = () => {
    const column: any[] = [
      {
        title: 'ID #',
        dataIndex: 'clientId',
        sorter: (a: any, b: any) => a.clientId - b.clientId,
      },
      {
        title: 'Name',
        dataIndex: 'clientCompanyName',
        sorter: (a: any, b: any) => {
          return onSortString('clientCompanyName', a, b)

        },
      },
      {
        title: 'Email',
        dataIndex: 'mainContactEmail',
        sorter: (a: any, b: any) => {
          return onSortString('mainContactEmail', a, b)
        },
      },
      {
        title: 'Phone',
        dataIndex: 'mobilePhone',
        sorter: (a: any, b: any) => {
          return onSortString('mobilePhone', a, b)
        },
      },
      {
        title: 'Address',
        dataIndex: 'mainBillingAddress.address',
        sorter: (a: any, b: any) => {
          const address1 = a.mainBillingAddress && a.mainBillingAddress.address ? this._getAddress(a.mainBillingAddress.address) : ''
          const address2 = b.mainBillingAddress && b.mainBillingAddress.address ? this._getAddress(b.mainBillingAddress.address) : ''
          return address1.localeCompare(address2)
        },
        render: (text: any, record: any) => {
          return record.mainBillingAddress && record.mainBillingAddress.address
            ? this._getAddress(record.mainBillingAddress.address)
            : ''
        },
      },
      {
        width: 100,
        className: 'view',
        render: (text: any, record: any) => (
          <ThemeLink className="tab-view-link" to={`/customer/${record.clientId}/account`}>
            VIEW <Icon type="arrow-right" />
          </ThemeLink>
        ),
      },
    ]

    const { briefCustomers, customerTotal, loadingCustomers } = this.props
    const { search, active } = this.state

    if (!briefCustomers) {
      return null
    }

    // let listItems: TempCustomer[] = cloneDeep(customerList)
    let listItems: BriefClient[] = cloneDeep(briefCustomers)
    // if (search) {
    listItems = listItems.filter((item) => {
      if (active == 0 && item.status === 'ACTIVE') {
        return false
      } else if (active == 1 && item.status == 'INACTIVE') {
        return false
      }
      if (search) {
        return this._searchItem(item, search)
      } else {
        return true
      }
    })
    // }

    return (
      <BodyContainer>
        <ThemeSpin spinning={loadingCustomers} wrapperClassName="full-width">
          <ThemeTable
            ref={this.multiSortTable}
            style={{ paddingLeft: 0, paddingRight: 0, width: '98%' }}
            columns={column}
            dataSource={listItems}
            rowKey="clientId"
            css={tableCss(false)}
            pagination={{ current: this.state.page + 1, defaultCurrent: 0, onChange: this.onChangePage, pageSize: 30 }}
          // loading={loadingCusotmer}
          // pagination={{
          //   total: customerTotal,
          //   pageSize: this.state.pageSize,
          //   current: this.state.currentPage,
          //   onChange: this.onTablePaginatorChanged,
          //   defaultCurrent: 1
          // }}
          />
        </ThemeSpin>
      </BodyContainer>
    )
  }

  private renderImportCustomerModal() {
    const { showModalImportCustomer } = this.state
    const { importCustomers, loadingCustomers } = this.props

    const onCloseImportQBCustomer = () => {
      this.setState({
        showModalImportCustomer: false,
      })
    }
    const onClickImport = () => {
      this.props.resetLoading()
      this.props.createCustomers(this.state.selectedQBCustomers)
    }
    const onRowSelection = (selectedRows: TempCustomer[]) => {
      this.setState({
        selectedQBCustomers: selectedRows,
      })
    }

    return (
      <ImportCustomerModal
        visible={showModalImportCustomer}
        customers={importCustomers}
        loading={loadingCustomers}
        onImport={onClickImport}
        onSelectionChange={onRowSelection}
        onCancel={onCloseImportQBCustomer}
      />
    )
  }

  private renderCreateCustomerModal() {
    return (
      <Modal width={880} footer={null} visible={this.state.createShow} onCancel={this.onCloseNewCustomer}>
        <CustomerForm
          key={`${Date.now()}`}
          customerData={newCustomer}
          onImport={this.onClickImportQBCustomer}
          onSubmit={this.onClickCreate}
          onCancel={this.onCloseNewCustomer}
          isCustomer={true}
        />
      </Modal>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.customers

export const Customers = withTheme(connect(CustomersModule)(mapStateToProps)(CustomersComponent))
