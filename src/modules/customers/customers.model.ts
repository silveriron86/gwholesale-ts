export interface SalesOrder {
  id: number
  product_name: string
  items_count: number
  ordered_shipped: number
  fulfilment_date: number
}

export interface Contact {
  id: number
  title: string
  salutation: string
  first_name: string
  last_name: string
  main_phone: string
  mobile_phone: string
  email: string
  fax: string
  notes: string
}

export interface Address {
  id: number
  name: string
  street: string
  city: string
  state: string
  zip_code: string
}

export interface Document {
  id: number
  timestamp: string
  account: string
  type: string
  notes: string
}

export interface Order {
  id: number
  unit: number
  uom: string
  product_name: string
  lot: string
  quoted_price: number
  notes: string
  permanent_note?: string
  temporary_note?: string

  is_picked?: boolean
  picked?: number
  quantity?: number
  is_shipped?: boolean
  extended?: number
  item_total?: number
  revenue?: number
  error?: string

  return_unit?: number
  return_net_weight?: number
  return_reason?: string
  re_inventory?: number
}

export interface Picking {
  id: number
  picked: number
  is_picked?: boolean
  quantity: number
}

export interface Shipping {
  id: number
  extended: number
  item_total: number
  is_shipped?: boolean
}

export interface Checkout {
  reference: string
  sales_reprentative: string
  delivery_contact: string
  delivery_phone: string
  delivery_instruction: string
  note_to_picker: string
  note_to_customer: string
}

export interface CreditMemoModel {
  id: number
  return_unit: number
  return_net_weight: number
  return_reason: string
  re_inventory: number
}

export interface MarginModel {
  id: number
  revenue: number
  cost: number
  markup: number
  gross_margin: number
  weighted_margin: number
}

export interface Message {
  id: number
  type: number
  sender_name: string
  sender_avatar: string
  content: string
  sent_date: string
}

export interface Sale {
  id: number
  ship_date: string
  lot: string
  cust_no: string
  customer: string
  sales_price: number
  units_comitted: number
  units_shipped: number
}
