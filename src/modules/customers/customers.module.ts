import {Module, EffectModule, ModuleDispatchProps, Effect, StateObservable, DefineAction, Reducer} from 'redux-epics-decorator'
import {Observable, of, from} from 'rxjs'
import { switchMap, map, takeUntil, endWith, catchError, mergeMap, startWith } from 'rxjs/operators'
import {Action} from 'redux-actions'
import {push, goBack} from 'connected-react-router'
import {format} from 'date-fns'
import moment from 'moment'

import {CustomerService} from './customers.service'
import {GlobalState} from '~/store/reducer'
import {
  Customer,
  TempCustomer,
  TempOrder,
  CustomerStatus,
  CustomerType,
  MainAddress,
  Document,
  PriceSheet,
  MainContact,
  Address,
  WholesaleRoute,
  Chat,
  BriefClient
} from '~/schema'
import {MessageType} from '~/components'
import {checkError, responseHandler} from '~/common/utils'
import {DeliveryService} from '../delivery/delivery.service'
import {OrderService} from '../orders/order.service'
import {SettingService} from '../setting/setting.service'

export interface CustomerPriceSheet {
  id: number
  priceSheetClientId: number
  wholesaleClientId: number
  updatedAt: string
  name: string
  assigned: number
  items: number
}

export interface CustomersStateProps {
  items: Customer[] | null // null means loading
  customer: Customer
  importCustomers: Customer[]
  orders: TempOrder[]
  priceSheets: PriceSheet[]
  sellerPriceSheets: PriceSheet[]
  loadingCustomers: boolean
  message: string
  description: string
  type: MessageType | null
  showModalImportCustomer: boolean
}

export interface TempCustomersStateProps {
  items: TempCustomer[] | null // null means loading
  customer: TempCustomer
  importCustomers: TempCustomer[]
  orders: TempOrder[]
  priceSheets: PriceSheet[]
  sellerPriceSheets: PriceSheet[]
  loadingCustomers: boolean
  addingCustomers: boolean
  message: string
  description: string
  type: MessageType | null
  showModalImportCustomer: boolean
  contacts: MainContact[]
  addresses: MainAddress[]
  documents: Document[]
  documentsLoading: boolean
  routes: WholesaleRoute[]
  currentCompanyUsers: any[]
  newItemId: number
  newCustomerId: number
  chatList: Chat[]
  customerTotal: number
  customerList: TempCustomer[]
  hasMore: boolean
  searchParam: string
  loadingCusotmer: boolean
  loading: boolean
  sortType: string
  orderTotal: number
  documentSearchProps: any
  documentsTotal: number
  salesOrderHistory: any[]
  salesHistoryTotal: number
  sellerSetting: any
  briefCustomers: BriefClient[] | null
  companyProductTypes: any
  duplicateMostRecentOrderLoading: boolean
  loadingPaymentAddresses: boolean
  loadingDeliveryAddresses: boolean
  loadingContacts: boolean
}

@Module('customers')
export class CustomersModule extends EffectModule<TempCustomersStateProps> {
  defaultState = {
    items: null,
    customer: {} as TempCustomer,
    importCustomers: [],
    orders: [],
    priceSheets: [] as PriceSheet[],
    sellerPriceSheets: [] as PriceSheet[],
    loadingCustomers: true,
    addingCustomers: false,
    message: '',
    description: '',
    type: null,
    showModalImportCustomer: false,
    contacts: [],
    addresses: [],
    documents: [],
    documentsLoading: false,
    routes: [],
    currentCompanyUsers: [],
    chatList: [],
    newItemId: -1,
    newCustomerId: -1,
    customerTotal: 0,
    customerList: [],
    hasMore: false,
    searchParam: '',
    loadingCusotmer: true,
    loading: true,
    sortType: '',
    orderTotal: 0,
    documentSearchProps: {},
    documentsTotal: 0,
    salesOrderHistory: [],
    salesHistoryTotal: 0,
    sellerSetting: {},
    briefCustomers: null,
    companyProductTypes: null,
    duplicateMostRecentOrderLoading:false,
    loadingPaymentAddresses: false,
    loadingDeliveryAddresses: false,
    loadingContacts: false,
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(
    private customer: CustomerService,
    private delivery: DeliveryService,
    private order: OrderService,
    private setting: SettingService) {
    super()
  }

  @Reducer()
  resetPricesheet(state: TempCustomersStateProps){
    return { ...state, priceSheets: [] }
  }

  @Reducer()
  resetDocuments(state:TempCustomersStateProps){
    return {...state, documents:[], documentsLoading: false, documentsTotal: 0 }
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        message: '',
        description: '',
        type: null,
        loadingCustomers: true,
        newCustomerId: -1,
        loading: true
      }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        duplicateMostRecentOrderLoading: !state.duplicateMostRecentOrderLoading
      }
    },
  })
  resetMostRecentLoadingStatus(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        loading: !state.loading
      }
    },
  })
  resetAccountOrderTableLoadingStatus(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }


  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, addingCustomers: true, newCustomerId: -1}
    },
  })
  startAdding(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, addingCustomers: false}
    },
  })
  endAdding(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {...state, documentSearchProps: payload}
    },
  })
  setDocumentSearchProps(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, loadingPaymentAddresses: true}
    },
  })
  startLoadingPaymentAddress(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, loadingDeliveryAddresses: true}
    },
  })
  startLoadingDeliveryAddress(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<{ [key: string]: string }>) => {
      const newValue = action.payload
      return {
        ...state,
        customer: {
          ...state.customer,
          ...newValue,
        },
        // message: 'Customer Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loading: false
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateCurrentCustomer(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.customer.updateCustomer(state$.value.customers.customer.clientId!, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data).pipe(
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getCustomer)(state$.value.customers.customer.clientId)),
            catchError((error) => of(checkError(error)))
          ))
        ),
      ),
    )
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect()
  goto(action$: Observable<string>) {
    return action$.pipe(map((path) => push(path)))
  }

  @Effect({
    done: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {...state, importCustomers: payload, loadingCustomers: false}
    },
  })
  getQBCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser ? state$.value.currentUser.userId : '1').pipe(
          switchMap((data1) =>
            this.customer.getImportCustomers().pipe(
              switchMap((data2) => of(this.compareImportCustomer(data1, data2))),
              takeUntil(this.dispose$),
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }


  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      window.location.reload()
      return {
        ...state,
        loading: false,
      }
    },
  })
  syncQBOCustomers(action$: Observable<any>) {
    return action$.pipe(
      switchMap((customerIds: any) => from(customerIds)),
      mergeMap((customerId: any) => {
        return this.customer.syncQBOClient(customerId)
      }, 8),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<Customer[]>) => {
      return {
        ...state,
        loadingCustomers: false,
        items: action.payload,
        addingCustomers: false,
      }
    },
  })
  getAllCustomersForUser(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser.userId).pipe(
          switchMap((data: any) => of(responseHandler(data).body.data)),
          switchMap((data: any) => of(this.formatCustomers(data))),
          map(this.createAction('done')),
          takeUntil(this.dispose$),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<Customer[]>) => {
      return {
        ...state,
        loadingCustomers: false,
        briefCustomers: action.payload?.sort((a: any, b: any) => {
            return a.clientCompanyName.localeCompare(b.clientCompanyName)
          }
        ),
        addingCustomers: false,
      }
    },
  })
  getBriefCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getBriefCustomers().pipe(
          switchMap((data: any) => of(responseHandler(data).body.data)),
          switchMap((data: any) => of(this.formatBriefClient(data))),
          map(this.createAction('done')),
          takeUntil(this.dispose$),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      // let customerList = state.customerList
      // if(state.searchParam != action.payload.searchParam){
      //   customerList = [];
      // }

      // if(state.sortType != action.payload.sortType){
      //   customerList = [];
      // }
      return {
        ...state,
        searchParam: action.payload.searchParam,
        customerList: [],
        loadingCusotmer: true,
        hasMore: false
      }
    },
  })
  setSearchParam(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      const customerList = state.customerList.concat(action.payload.dataList)
      return {
        ...state,
        customerList: customerList,
        customerTotal: action.payload.total,
        customer: {},
        orders: [],
        contacts: [],
        addresses: [],
        documents: [],
        hasMore: customerList.length < action.payload.total ? true : false,
        loadingCusotmer: false,
        newCustomerId: -1
      }
    },
  })
  getAllCustomersByCondition(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.customer.getAllCustomersByCondition({...data, userId: state$.value.currentUser.userId}).pipe(
          switchMap((data: any) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          takeUntil(this.dispose$),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<Customer>) => {
      return {
        ...state,
        customer: action.payload,
        newCustomerId: -1
      }
    },
  })
  getCustomer(action$: Observable<string>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getCustomerInfo(customerId)),
      switchMap((data: any) => of(responseHandler(data, false).body.data)),
      switchMap((data) => of(this.formatCustomer(data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, {payload}: Action<any>) => {
      return {...state, currentCompanyUsers: payload.userList}
    },
  })
  getCompanyUsers(action$: Observable<any>) {
    return action$.pipe(
      switchMap(() => this.customer.getCompanyUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<Customer>) => {
      return {
        ...state,
        customer: action.payload,
      }
    },
  })
  getLatestNSBalance(action$: Observable<string>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getLatestNSBalance(customerId)),
      switchMap((data: any) => of(responseHandler(data, false).body.data)),
      switchMap((data) => of(this.formatCustomer(data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<Customer>) => {
      return {
        ...state,
        customer: action.payload,
      }
    },
  })
  getLatestQBOBalance(action$: Observable<string>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getLatestQBOBalance(customerId)),
      switchMap((data: any) => of(responseHandler(data, false).body.data)),
      switchMap((data) => of(this.formatCustomer(data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      const {payload} = action
      return {
        ...state,
        loading: false,
        loadingCusotmers: false,
        orders: payload.dataList,
        newItemId: -1,
        orderTotal: payload.total
      }
    },
  })
  getOrder(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) => this.customer.getCustomerOrders(parseInt(data.id, 10), data.from, data.to, data.page, data.pageSize, data.queryParam, data.orderByFilter, data.direction)),
      switchMap((data: any) => of(responseHandler(data, false).body.data)),
      map((data) => {
        const total = data.total
        const orderList = data.dataList
        const newList = this.formatOrder(orderList)
        return {total, dataList: newList}
      }),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      let newOrders = state.orders
      let newItemId = -1
      if (action.payload && action.payload.length > 0) {
        newOrders.unshift(...action.payload)
        newItemId = action.payload[0].wholesaleOrderId
      }

      return {
        ...state,
        orders: newOrders,
        newItemId: newItemId
        // message: 'Created Order Successfully',
        // type: MessageType.SUCCESS,

      }
    },
    error_message: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
        // type: MessageType.ERROR,
        // error: action.payload,
      }
    },
  })
  createEmptyOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) => of(this.formatOrder([responseHandler(data, true).body.data])).pipe(
            map(data => push(`/sales-order/${data[0].wholesaleOrderId}`)),
            // map(this.createAction('done')),
            // endWith(this.createActionFrom(this.getOrder)(state$.value.customers.customer.clientId)),
            catchError((error) => of(checkError(error)))))
        ),
      ),
    );
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<PriceSheet[]>) => {
      return {
        ...state,
        sellerPriceSheets: action.payload,
      }
    },
  })
  getSellerPriceSheet(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getSellerPriceSheet(state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error)))
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<CustomerPriceSheet[]>) => {
      return {
        ...state,
        priceSheets: action.payload,
      }
    },
  })
  getPriceSheet(action$: Observable<string>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getCustomerPriceSheet(customerId)),
      switchMap((data: any) => of(responseHandler(data).body.data)),
      // switchMap((data: any) => of(this.formatPriceSheet(data))),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
        newCustomerId: action.payload.clientId,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        addingCustomers: false
      }
    },
  })
  createCustomer(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) => this.customer.createCustomer(state$.value.currentUser.userId, data)),
      switchMap((data) => of(responseHandler(data, true).body.data).pipe(
        map(this.createAction('done')),
        endWith(this.createActionFrom(this.syncCompany)(data.body.data.companyId)),
        // endWith(this.createActionFrom(this.getBriefCustomers)()),
        catchError((error) => of(checkError(error)))))
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state
      }
    },
  })
  syncCompany(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.syncCompany(customerId)),
      map(this.createAction('done')),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state
      }
    },
  })
  syncContactUser(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((contactUserId) => this.customer.syncContactUser(contactUserId)),
      map(this.createAction('done')),
    )
  }


  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        showModalImportCustomer: false,
        message: 'Customers Imported Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loadingCustomers: false,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  createCustomers(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customers: any) => from(customers)),
      mergeMap((customer: any) => this.customer.createCustomer(state$.value.currentUser.userId, customer)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        message: 'Assigned PriceSheet Successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  assignPriceSheet(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((priceSheetId) =>
        this.customer.assignPriceSheet(priceSheetId, +state$.value.customers.customer.clientId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheet)(state$.value.customers.customer.clientId)),
          catchError((error) => of(checkError(error)))
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, message: '', description: '', type: null}
    },
  })
  resetNotif(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, newItemId: -1, message: ''}
    },
  })
  resetNewOrderId(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        // message: 'Unassigned PriceSheet Successfully',
        // type: MessageType.SUCCESS,
      }
    },
  })
  unAssignPriceSheet(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((priceSheetClientId) =>
        this.customer.unAssignPriceSheet(priceSheetClientId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheet)(state$.value.customers.customer.clientId)),
          catchError((error) => of(checkError(error)))
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<Customer>) => {
      return {
        ...state,
        contacts: action.payload,
      }
    },
  })
  getContacts(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getContacts(customerId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      // switchMap((data) => of(this.formatCustomers(data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    before: state => {
      return {...state, loadingContacts: true }
    },
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        loadingContacts: false,
        // message: 'Create a contact successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loadingContacts: false,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createContact(action$: Observable<MainContact>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: MainContact) =>
        this.customer.createContact(state$.value.customers.customer.clientId!, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          endWith(this.createActionFrom(this.getContacts)(state$.value.customers.customer.clientId)),
          catchError((error) => of(checkError(error)))
        ),
      ),
    )
  }

  @Effect({
    before: state => {
      return {...state, loadingContacts: true }
    },
    done: (state: TempCustomersStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        loadingContacts: false,
        // message: 'Set Main Contact',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loadingContacts: false,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  setMainContact(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((contactId: number) =>
        this.customer.setMainContact(state$.value.customers.customer.clientId!, contactId)
          .pipe(
            switchMap((data) =>
              of(responseHandler(data, false).body.data).pipe(
                endWith(this.createActionFrom(this.syncContactUser)(data.body.data.mainContact.user.userId)),
              )
            ),
            map(this.createAction('done')),
            startWith(this.createAction('before')()),
            endWith(this.createActionFrom(this.getContacts)(state$.value.customers.customer.clientId!)),
            catchError((error) => of(checkError(error))),
          )
      )
    )
  }

  @Effect({
    before: state => {
      return {...state, loadingContacts: true }
    },
    done: (state: TempCustomersStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        loadingContacts: false
        // message: 'Contact Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loadingContacts: false
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateContact(action$: Observable<MainContact>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((contact: MainContact) =>
        this.customer.updateContact(state$.value.customers.customer.clientId!, contact).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          endWith(this.createActionFrom(this.getContacts)(state$.value.customers.customer.clientId)),
          catchError((error) => of(checkError(error)))
        ),
      ),
    )
  }

  @Effect({
    before: state => {
      return {...state, loadingContacts: true }
    },
    done: (state: TempCustomersStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        loadingContacts: false
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loadingContacts: false
      }
    },
  })
  deleteContact(action$: Observable<MainContact>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((contact: MainContact) =>
        this.customer.deleteContact(state$.value.customers.customer.clientId!, contact.id, contact).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          endWith(this.createActionFrom(this.getContacts)(state$.value.customers.customer.clientId)),
          catchError((error) => of(checkError(error)))
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<Customer>) => {
      return {
        ...state,
        addresses: action.payload,
        loadingPaymentAddresses: false,
        loadingDeliveryAddresses: false
      }
    },
  })
  getAddresses(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getAddresses(customerId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        // message: 'Create an address successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loadingPaymentAddresses: false,
        loadingDeliveryAddresses: false
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createAddress(action$: Observable<MainAddress>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: MainAddress) =>
        this.customer.createAddress(state$.value.customers.customer.clientId!, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data).pipe(
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getAddresses)(state$.value.customers.customer.clientId!)),
            catchError((error) => of(checkError(error))))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        // message: 'Address Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        loadingPaymentAddresses: false,
        loadingDeliveryAddresses: false
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateAddress(action$: Observable<MainAddress>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: MainAddress) =>
        this.customer.updateAddress(state$.value.customers.customer.clientId!, data.wholesaleAddressId, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data).pipe(
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getAddresses)(state$.value.customers.customer.clientId!)),
            catchError((error) => of(checkError(error))))),
        ),
      ),
    )
  }

  @Effect({
    before: state => {
      return {...state, documentsLoading:true}
    },
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      const {payload} = action
      return {
        ...state,
        documentsLoading: false,
        documents: typeof payload === 'string' ? [] : (payload && payload.dataList ? payload.dataList : []),
        documentsTotal: typeof payload === 'string' ? 0 : (payload && payload.total ? payload.total : 0),
      }
    },
  })
  getDocuments(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => this.customer.getDocuments(data).pipe(
        switchMap((data) => of(responseHandler(data, false).body.data)),
        map(this.createAction('done'), takeUntil(this.dispose$)),
        startWith(this.createAction('before')(true)),
        catchError((error) => of(checkError(error)))
      )),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {
        ...state,
        // message: 'Create a document successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createDocument(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.customer.createDocument(state$.value.customers.customer.clientId!, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getDocuments)({
            ...state$.value.customers.documentSearchProps,
            customerId: state$.value.customers.customer.clientId!
          })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        // message: 'Docuement Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateDocument(action$: Observable<Document>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: Document) =>
        this.customer.updateDocument(state$.value.customers.customer.clientId!, data.id, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getDocuments)({
            ...state$.value.customers.documentSearchProps,
            customerId: state$.value.customers.customer.clientId!
          })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      console.log(action)
      return {
        ...state,
        test: action.payload,
      }
    },
    error_message: (state: TempCustomersStateProps, action: Action<any>) => {
      //error
      return {
        ...state,
        error: action.payload,
        hasError: true,
      }
    },
  })
  demoTest(action$: Observable<T>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() => this.customer.getDemoTest()),
      switchMap((data) => of(responseHandler(data))),
      map(this.createAction('done')),
      endWith(this.createActionFrom(this.getAllCustomersForUser)()),
      catchError((error) => of(checkError(error)))
    )
  }


  @Effect({
    done: (state: TempCustomersStateProps, {payload}: Action<WholesaleRoute[]>) => {
      return {
        ...state,
        routes: payload
      }
    },
  })
  getAllRoutes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.delivery.getAllRoutes()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, {payload}: Action<WholesaleRoute[]>) => {
      return {
        ...state,
        userSetting: payload
      }
    },
  })
  getUserSetting(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.delivery.getAllRoutes()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  formatCustomer(data: any): TempCustomer {
    return data
    // const status = data.status ? data.status.toLowerCase() : CustomerStatus.INACTIVE
    // const type = data.type ? data.type.toLowerCase() : CustomerType.INDIVIDUAL

    // return {
    //   type: type,
    //   id: data.customerId,
    //   name: data.fullName,
    //   company: data.company,
    //   phone: data.phone,
    //   address: data.address as Address,
    //   email: data.emailAddress,
    //   status,
    //   qboId: data.qboId,
    // } as Customer
  }

  formatCustomers(datas: any[]): TempCustomer[] {
    // TODO
    // 'address' always to be null
    return datas.map((data) => {
      // const status = data.status ? data.status.toLowerCase() : CustomerStatus.INACTIVE
      // const type = data.type ? data.type.toLowerCase() : CustomerType.INDIVIDUAL
      return data
    })
  }

  formatBriefClient(datas: any[]): TempCustomer[] {
    return datas.map((data) => {
      if (data.mainBillingAddressId) {
        return {
          clientId: data.clientId,
          clientCompanyName: data.clientCompanyName,
          wholesaleCompanyId: data.wholesaleCompanyId,
          clientCompanyId: data.clientCompanyId,
          type: data.type,
          businessType: data.businessType,
          mainContactName: data.mainContactName,
          mainContactEmail: data.mainContactEmail,
          mobilePhone: data.mobilePhone,
          lastQBOUpdate: data.lastQBOUpdate,
          status: data.status,
          mainBillingAddress: {
            address: {
              street1: data.street1,
              street2: data.street2,
              zipcode: data.zipcode,
              city: data.city,
              country: data.country,
              state: data.state,
            }
          }
        }
      } else {
        return data
      }

    })
  }

  compareImportCustomer(data1: Customer[], data2: any): Customer[] {
    const customerList = {}
    const customerList2 = {}
    const importCustomerList = []
    for (const customer of data1) {
      if (customer.qboId != null) customerList[customer.qboId] = customer
    }
    for (const customer of data2.customerList) {
      const status = customer.status ? CustomerStatus.ACTIVE : CustomerStatus.INACTIVE
      if (customerList2[customer.id] === 1) {
        continue
      }
      customerList2[customer.id] = 1
      const tempCustomer: Customer = {
        type: CustomerType.INDIVIDUAL,
        id: '',
        company: customer.company,
        name: '',
        phone: '',
        email: '',
        address: {} as Address,
        status: status,
        qboId: customer.id,
      }
      if (customer.company == null || customer.company.length === 0) tempCustomer.company = customer.fullyQualifiedName
      if (customer.primaryEmailAddr != null && customer.primaryEmailAddr.address != null)
        tempCustomer.email = customer.primaryEmailAddr.address
      if (customer.primaryPhone != null && customer.primaryPhone.freeFormNumber != null)
        tempCustomer.phone = customer.primaryPhone.freeFormNumber
      if (customer.givenName != null) tempCustomer.name += customer.givenName
      if (customer.familyName != null) {
        if (tempCustomer.name.length > 0) tempCustomer.name += ' '
        tempCustomer.name += customer.familyName
      }
      if (tempCustomer.name == null || tempCustomer.name.length === 0) {
        tempCustomer.name = customer.displayName
      }
      if (customerList[customer.id] == null) {
        importCustomerList.push(tempCustomer)
      }
    }

    return importCustomerList.map((data) => ({
      type: data.type,
      id: '',
      company: data.company,
      name: data.name,
      phone: data.phone,
      email: data.email,
      address: data.address,
      status: data.status,
      qboId: data.qboId,
    }))
  }

  formatOrder(datas: any[]): TempOrder[] {
    if (datas.length == 0) return []
    return datas.map((data) => {
      data.createdDate = format(data.createdDate, 'MM/D/YY')
      data.deliveryDate = moment.utc(data.deliveryDate).format('MM/D/YY')
      data.updatedDate = format(data.updatedDate, 'MM/D/YY')
      if (data.wholesaleOrderStatus != null)
        data.status = data.wholesaleOrderStatus
      return data
    })
  }

  formatPriceSheet(datas: any[]): CustomerPriceSheet[] {
    // TODO MOCK_DATA
    // data need 'assigned' & 'items'
    return datas.map((data) => ({
      id: data.priceSheetId,
      priceSheetClientId: data.priceSheetClientId,
      wholesaleClientId: data.wholesaleClientId,
      updatedAt: format(data.updatedDate, 'MM/D/YY'),
      name: data.name,
      assigned: 0,
      items: 0,
    }))
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
        // message: 'Create Chat successfully',
        // type: MessageType.SUCCESS
      }
    },
    error_message: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
        // type: MessageType.ERROR,
        // error: action.payload,
      }
    },
  })
  createChat(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.customer.createChat({...data, userId: state$.value.currentUser.userId}).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getChatListByClientId)(state$.value.customers.customer.clientId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, {payload}: Action<any>) => {
      return {
        ...state,
        chatList: payload
      }
    }
  })
  getChatListByClientId(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.customer.getChatListByClientId(data).pipe(
          switchMap((data) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      let newOrders = state.orders
      let newItemId = -1;
      if (action.payload && action.payload.length > 0) {
        newOrders.unshift(action.payload[0]);
        newItemId = action.payload[0].wholesaleOrderId;
      }
      // console.log('action.payload', action.payload)
      return {
        ...state,
        orders: newOrders,
        newItemId: newItemId
        // message: 'DUPLICATED_SUCCESS',
        // type: MessageType.SUCCESS,
      };
    },
    error_message: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  duplicateOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.duplicateOrder(data).pipe(
          switchMap((data) => of(responseHandler(data).body.data)),
          switchMap((data) => of(this.formatOrder([data])).pipe(
            map(data => push(`/sales-order/${data[0].wholesaleOrderId}?duplicated=success`)),
            // map(this.createAction('done')),
            endWith(this.createActionFrom(this.resetAccountOrderTableLoadingStatus)()),
            catchError((error) => of(checkError(error)))))
        ),
      ),
    );
  }


  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      const orders = [...state.orders];
      let newItemId = -1;
      if (action.payload && Array.isArray(action.payload)) {
        orders.unshift(action.payload[0])
        newItemId = action.payload[0].wholesaleOrderId;
      }

      return {
        ...state,
        orders: orders,
        loading: false,
        newItemId: newItemId,
        // message: 'DUPLICATED_SUCCESS',
      };
    }
  })
  duplicateMostRecentOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) => this.order.duplicateMostRecentOrder(data).pipe(
        switchMap((data) => of(responseHandler(data).body.data)),
        switchMap((data) => of(this.formatOrder([data])).pipe(
          map((data) => push(`/sales-order/${data[0].wholesaleOrderId}?duplicated=success`)),
          // map(this.createAction('done')),
          endWith(this.createActionFrom(this.resetMostRecentLoadingStatus)()),
          catchError((error) => of(checkError(error)))))
      ))
    );
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      const {payload} = action
      return {
        ...state,
        loading: false,
        salesOrderHistory: payload.dataList,
        newItemId: -1,
        salesHistoryTotal: payload.total
      }
    },
    error_message: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false
      }
    },
  })
  getAllSalesOrderHistory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.customer.getAllSalesOrderHistory(data).pipe(
          switchMap((data) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error)))
        ),
      ),
    )
  }

  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
        sellerSetting: action.payload.userSetting,
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getUserSetting(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(map(this.createAction('done'))),
      ),
      catchError((error) => of(checkError(error))),
    )
  }


  @Effect({
    done: (state: TempCustomersStateProps, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
    )
  }

  formatProductTypes = (datas: any[]) => {
    const result = {
      unitOfMeasure: [],
      returnReason: [],
      carrier: [],
      modifier: [],
      costStructure: [],
      modeOfTransportation: [],
      suppliers: [],
      freightTypes: [],
      shippingTerms: [],
      paymentTerms: [],
      extraCOO: [],
      extraCharges: [],
      customerTypes: [],
      vendorTypes: [],
      reasonType: []
      QBOpaymentTerms: [],
    }

    // UNIT_OF_MEASURE, RETURN_REASON, CARRIER,MODIFIER,COST_STRUCTURE,MODE_OF_TRANSPORTATION
    datas.map((type: any) => {
      switch (type.type) {
        case 'UNIT_OF_MEASURE':
          result.unitOfMeasure.push(type)
          break
        case 'RETURN_REASON':
          result.returnReason.push(type)
          break
        case 'CARRIER':
          result.carrier.push(type)
          break
        case 'MODIFIER':
          result.modifier.push(type)
          break
        case 'COST_STRUCTURE':
          result.costStructure.push(type)
          break
        case 'MODE_OF_TRANSPORTATION':
          result.modeOfTransportation.push(type)
          break
        case 'SUPPLIERS':
          result.suppliers.push(type)
          break
        case 'FREIGHT':
          result.freightTypes.push(type)
          break
        case 'SHIPPING_TERM':
          result.shippingTerms.push(type)
          break
        case 'FINANCIAL_TERM':
          result.paymentTerms.push(type)
          result.QBOpaymentTerms.push(type)
          break
        case 'FINANCIAL_TERM_FIXED_DAYS':
          result.QBOpaymentTerms.push(type)
          break;
        case 'EXTRA_COO':
          result.extraCOO.push(type)
          break
        case 'EXTRA_CHARGE':
          result.extraCharges.push(type)
          break
        case 'CUSTOMER_BUSINESS_TYPE':
          result.customerTypes.push(type)
          break
        case 'VENDOR_BUSINESS_TYPE':
          result.vendorTypes.push(type)
          break
        case 'REASON_TYPE':
          result.reasonType.push(type)
          break
      }
    })
    return result
  }
}

export type CustomersDispatchProps = ModuleDispatchProps<CustomersModule>
