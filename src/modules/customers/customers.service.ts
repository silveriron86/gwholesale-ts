import {Http} from '~/common'
import {Injectable} from 'redux-epics-decorator'
import {Customer} from '~/schema'

@Injectable()
export class CustomerService {
  constructor(private readonly http: Http) {
  }

  static get instance() {
    return new CustomerService(new Http())
  }

  getAllCustomers(userId: string) {
    return this.http.get<any>(`/account/user/${userId}/customers/list`)
  }

  getBriefCustomers() {
    return this.http.get<any>(`/account/brief-customers`)
  }

  getSimplifyCustomers() {
    return this.http.get<any>(`/account/simplify-customers`)
  }

  getSystemCustomers() {
    return this.http.get<any[]>(`/account/all/customers/list`)
  }

  getAllCustomersByCondition(data: any) {
    const queryParam = escape(data.queryParam)
    return this.http.get<any>(
      `/account/customers?queryParam=${queryParam}&page=${data.page}&pageSize=${data.pageSize}&sortType=${data.sortType}&active=${data.active}`,
    )
  }

  getLatestNSBalance(customerId: string) {
    return this.http.get<Customer>(`/account/getLatestNSBalance/${customerId}`)
  }

  getLatestQBOBalance(customerId: string) {
    return this.http.get<Customer>(`/account/getLatestQBOBalance/${customerId}`)
  }

  getCustomerInfo(customerId: string) {
    return this.http.get<Customer>(`/account/user/getCustomer/${customerId}`)
  }

  getCompanyUsers() {
    return this.http.get<any[]>(`/session/getCompanyUsers`)
  }

  getDemoTest() {
    return this.http.post(`/demo/yoyo`)
  }

  // getDemoTest() {
  //   return this.http.get(`/demo/test`)
  // }

  // getDemoTest() {
  //   return this.http.post(`/demo/yoyo`)
  // }
  getImportCustomers() {
    // return this.http.get<any[]>(`importCustomers`)
    return this.http.get<any[]>(`/account/getQBOcustomers`)
  }

  syncQBOClient(clientId: string) {
    return this.http.post<any>(`/quickbook/sendQBOClient/${clientId}`)
  }

  getImportNSCustomers() {
    // return this.http.get<any[]>(`importCustomers`)
    return this.http.get<any[]>(`/netsuite/customer`)
  }

  createCustomer(userId: string, data: any) {
    return this.http.post(`/account/user/${userId}/createCustomer`, {
      body: JSON.stringify(data),
    })
  }

  updateCustomer(userId: number, data: any) {
    return this.http.post(`/account/user/updateCustomer/${userId}`, {
      body: JSON.stringify(data),
    })
  }

  getCustomerOrders(
    customerId: number,
    fromDate: string,
    toDate: string,
    page: number,
    pageSize: number,
    queryParam: string,
    orderBy: string,
    direction: string,
  ) {
    let url = `/account/client/${customerId}/orders?from=${fromDate}&to=${toDate}`
    if (page != null && pageSize != null) {
      url += `&page=${page}&pageSize=${pageSize}`
    }
    if (queryParam) {
      url += `&queryParam=${queryParam}`
    }
    if (orderBy != '') url += `&orderBy=${orderBy}&direction=${direction}`
    return this.http.get<any[]>(url)
  }

  getSellerPriceSheet(userId: string) {
    return this.http.get(`/pricesheet/userCompany/${userId}`)
  }

  getCustomerPriceSheet(customerId: string) {
    return this.http.get(`/account/user/${customerId}/pricesheets/list`)
  }

  assignPriceSheet(priceSheetId: number, clientId: number) {
    return this.http.post(`/pricesheet/${priceSheetId}/assign/customer/${clientId}`)
  }

  unAssignPriceSheet(priceSheetClientId: number) {
    return this.http.post(`/pricesheet/unassign/pricesheetclient/${priceSheetClientId}`)
  }

  getContacts(customerId: string) {
    return this.http.get(`/account/client/${customerId}/contacts`)
  }

  createContact(customerId: number, data: any) {
    return this.http.post(`/account/client/${customerId.toString()}/contact`, {
      body: JSON.stringify(data),
    })
  }

  updateContact(customerId: number, data: any) {
    return this.http.put(`/account/client/${customerId.toString()}/contact`, {
      body: JSON.stringify(data),
    })
  }

  setMainContact(customerId: number, contactId: number) {
    return this.http.post(`/account/user/customer/${customerId.toString()}/setMainContact/${contactId.toString()}`)
  }

  deleteContact(customerId: number, contactId: number, data: any) {
    return this.http.delete(`/account/client/${customerId.toString()}/contact/${contactId}`, {
      body: JSON.stringify(data),
    })
  }

  getAddresses(customerId: string) {
    return this.http.get(`/account/user/getAddress/${customerId}`)
  }

  createAddress(customerId: number, data: any) {
    return this.http.post(`/account/user/createAddress/${customerId.toString()}`, {
      body: JSON.stringify(data),
    })
  }

  updateAddress(customerId: number, addressId: number, data: any) {
    return this.http.post(`/account/user/updateAddress/${customerId.toString()}/${addressId.toString()}`, {
      body: JSON.stringify(data),
    })
  }

  getDocuments(data: any) {
    const {customerId, ...rest} = data
    return this.http.get(
      `/account/user/getDocuments/${customerId}`,{query: rest}
    )
  }

  createDocument(customerId: number, data: any) {
    return this.http.post(`/account/user/createDocument/${customerId.toString()}`, {
      body: JSON.stringify(data),
    })
  }

  updateDocument(customerId: number, id: number, data: any) {
    return this.http.put(`/account/user/updateDocument/${customerId.toString()}/${id.toString()}`, {
      body: JSON.stringify(data),
    })
  }

  createChat(data: any) {
    return this.http.post(`/account/user/chats`, {
      body: JSON.stringify(data),
    })
  }

  getChatListByClientId(clientId: string) {
    return this.http.get(`/account/user/chats/${clientId}`)
  }

  syncCompany(data: any) {
    return this.http.post(`/account/sync/company/${data}`)
  }

  syncContactUser(data: any) {
    return this.http.post(`/account/sync/contact/${data}`)
  }

  getAllSalesOrderHistory(data: any) {
    let url = `/inventory/order-history?from=${data.from}&to=${data.to}&page=${data.page}&pageSize=${data.pageSize}&clientId=${data.clientId}`
    if (data.search != '') {
      url += `&search=${data.search}`
    }

    if (data.orderBy != '') url += `&orderBy=${data.orderBy}&direction=${data.direction}`
    return this.http.get<any[]>(url)
  }

  getDefaultPriceCustomerList(clientId: number | string) {
    return this.http.get(`/account/customers/list/${clientId}`)
  }

  setCustomerProductListAutoSync(clientId: number, syncStatus: boolean) {
    return this.http.post(`/account/set-sync/${clientId}?sync=${syncStatus}`)
  }
}
