import styled from '@emotion/styled'
import { rgba } from 'polished'
import {
  Form,
  Row,
  Button,
  Icon,
  Card,
  Input,
  Radio,
  Modal,
  Select,
  Checkbox,
  Tabs,
  Alert,
  Spin,
  DatePicker,
  InputNumber,
  Switch,
  Upload
} from 'antd'
import { MutiSortTable } from '~/components/muti-sort-table'
import {
  white,
  white4,
  black,
  brownGrey,
  filterGreen,
  mediumGrey,
  mediumGrey2,
  lightGrey,
  darkGrey,
  transparent,
  badgeColor,
  cancelRed,
  topLightGrey, gray01, gray02,
} from '~/common'
import { Link } from 'react-router-dom'
import { css } from '@emotion/core'
import { Icon as IconSvg } from "~/components/icon/index";


const headerHeight = '270px'

export const CustomerContainer = styled('div')({})

export const HeaderContainer = styled('div')({
  width: '100%',
  height: headerHeight,
  backgroundColor: white,
  padding: '50px 81px 20px 131px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  fontWeight: 700,
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const HeaderOptions = styled('div')((props) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: props.theme.primary,
    '&::after': {
      display: 'none',
    },
  },
  '.clear-icon': {
    display: 'none'
  },
  '& .ant-input-search:hover .clear-icon.show': {
    display: 'block'
  }
}))

export const buttonStyle: React.CSSProperties = {
  borderRadius: '200px',
  width: '180px',
  height: '40px',
  marginRight: '31px',
  fontSize: '16px',
}

export const ghostButtonStyle: React.CSSProperties = {
  borderRadius: '5px',
  width: '130px',
  height: '35px',
  border: `1px solid ${brownGrey}`,
  backgroundColor: transparent,
  color: `${brownGrey} !important`,
}

export const SortByText = styled('span')({
  color: `${brownGrey} !important`,
})

export const SortByIcon: React.CSSProperties = {
  color: `${brownGrey} !important`,
  width: '18px',
  height: '13px',
}

export const BodyContainer = styled('div')({
  width: '100%',
  minHeight: `calc(100vh - ${headerHeight} - 68px)`,
  backgroundColor: white,
  boxSizing: 'border-box',
  display: 'flex',
  flexWrap: 'wrap',
  padding: '19px 0px 40px 66px',
  boxShadow: `inset 0px 4px 4px ${lightGrey}`,

  '& .ant-card-body': {
    padding: '0',
  },
  '& .ant-card:hover': {
    backgroundColor: `rgba(82, 158, 99, 0.1) !important`,
  },
})

export const cardStyle: React.CSSProperties = {
  width: '388px',
  height: '260px',
  backgroundColor: filterGreen,
  margin: '20px',
  cursor: 'pointer',
  overflow: 'hidden',
  border: '0',
  borderRadius: '0 10px',
  boxShadow: `2px 2px 4px 1px ${lightGrey}`,
  position: 'relative',
  transition: 'all 0.2s 0s ease',
  fontWeight: 700,
}

export const ThemeCard: any = styled(Card)((props) => ({
  width: '388px',
  height: '260px',
  backgroundColor: filterGreen,
  margin: '20px',
  cursor: 'pointer',
  overflow: 'hidden',
  borderColor: props.theme.primary,
  borderWidth: 0,
  borderStyle: 'solid',
  borderRadius: '0 10px',
  boxShadow: `2px 2px 4px 1px ${lightGrey}`,
  position: 'relative',
  transition: 'all 0.2s 0s ease',
  fontWeight: 700,
}))

export const CardTop = styled('div')({
  position: 'absolute',
  top: '0',
  right: '0',
  backgroundColor: 'transparent',
  display: 'flex',
  alignItems: 'center',
})

export const CardStatus = styled('div')((props) => ({
  color: props.theme.primary,
  marginRight: '10px',
  display: 'flex',
  alignItems: 'center',

  '& > svg': {
    fontSize: '12px',
    marginRight: '10px',
  },
  '& > span': {
    fontSize: '12px',
    color: props.theme.primary,
  },
}))

export const CardType = styled('div')((props) => ({
  height: '33px',
  width: '123px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: '12px',
  color: white,
  backgroundColor: rgba(props.theme.light, 0.6),
  borderRadius: '0 0 0 10px',
  marginRight: '1px',
}))

export const CardTypeIcon = styled('a')((props) => ({
  height: '33px',
  width: '50px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: props.theme.light,

  '& > span': {
    width: '24px',
    height: '24px',
    border: '0',
    borderRadius: '24px',
    backgroundColor: white,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '& > i': {
      fontSize: '16px',
      color: props.theme.light,
    },
  },
}))

export const CardBody = styled('div')({
  height: '260px',
  width: '100%',
  padding: '58px 20px 20px 20px',
  backgroundColor: 'transparent',
  position: 'absolute',
  top: 0,
  left: 0,
  zIndex: 1,
})

export const CardName = styled('div')({
  fontSize: '24px',
  letterSpacing: '0.05em',
  color: darkGrey,
  marginBottom: '10px',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
})

export const CardInfo = styled('div')({
  fontSize: '14px',
  lineHeight: '22px',
  letterSpacing: '0.05em',
  color: darkGrey,
  width: '100%',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  // textOverflow: 'ellipsis',
})

export const CardLine = styled('div')((props) => ({
  width: '73px',
  border: `1px solid ${props.theme.light}`,
  height: '2px',
  boxSizing: 'border-box',
  margin: '20px 0 15px',
}))

export const CardDetailSpan = styled('span')((props) => ({
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textAlign: 'left'
}))

export const CardText = styled('div')((props) => ({
  marginTop: '10px',
  display: 'flex',
  alignItems: 'center',

  '& > svg': {
    color: props.theme.light,
    fontSize: '16px',
    marginRight: '18px',
  },
  '& > span': {
    fontSize: '14px',
    lineHeight: '22px',
    letterSpacing: '0.05em',
    color: mediumGrey,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
}))

export const CardMail = styled(CardText)((props) => ({
  marginTop: '10px',
  display: 'flex',
  alignItems: 'center',

  '& > svg': {
    color: props.theme.light,
    fontSize: '16px',
    marginRight: '18px',
  },
  '& > span': {
    fontSize: '14px',
    lineHeight: '22px',
    letterSpacing: '0.05em',
    color: mediumGrey,
    textDecoration: 'underline',
  },
}))

export const CardCircle = styled('div')({
  position: 'absolute',
  left: '-98px',
  top: '32px',
  width: '196px',
  height: '196px',
  border: '0',
  borderRadius: '196px',
  backgroundColor: white4,
  zIndex: 0,
})

export const DialogHeader = styled('div')((props) => ({
  display: 'flex',
  alignItems: 'flex-end',
  marginLeft: '43px',
  marginBottom: '21px',
  padding: '24px',
  fontSize: '28px',
  color: mediumGrey,
  lineHeight: '30px',
  letterSpacing: '0.05em',

  '& > p': {
    margin: '0',
    fontSize: '28px',
    color: mediumGrey,
    lineHeight: '30px',
    letterSpacing: '0.05em',
  },
  '& > div': {
    marginLeft: '20px',
    fontSize: '14px',
    textDecoration: 'underline',
    color: props.theme.primary,
    cursor: 'pointer',
  },
}))

export const DialogBody = styled('div')({
  display: 'flex',
  paddingLeft: '46px',

  '& input': {
    marginTop: '30px',
  },

  '& .ant-input-affix-wrapper .ant-input-suffix': {
    marginTop: '15px',
  },

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-select-selection': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    fontWeight: 300,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const ModalTitle = styled('div')({
  display: 'flex',
  marginLeft: '26px',
  padding: '24px',
  fontSize: '50px',
  color: 'black',
  lineHeight: '30px',
  letterSpacing: '0.05em',
})

export const DialogFooter = styled('div')({
  width: '100%',
  boxSizing: 'border-box',
  padding: '50px 0',
  marginTop: '10px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  '& > div': {
    color: brownGrey,
    fontSize: '14px',
    lineHeight: '15px',
    textDecoration: 'underline',
    marginLeft: '25px',
    cursor: 'pointer',
  },
})

interface activeProps {
  active: boolean
}

export const CardValue = styled.p<activeProps>`
  color: ${(props: activeProps) => (props.active ? badgeColor : '#555F61')};
  margin-top: 15px;
  margin-bottom: 0;
  font-size: 20px;
  font-weight: bold;
  letter-spacing: 0.05em;
`

export const mb10: React.CSSProperties = {
  marginBottom: 10,
}

export const ml0: React.CSSProperties = {
  marginLeft: 0,
}

export const mh10: React.CSSProperties = {
  margin: '0 10px',
}

export const pd20: React.CSSProperties = {
  padding: '0 20px',
}

export const small = {
  fontSize: 18,
  letterSpacing: 'unset',
  marginTop: 6,
}

export const DetailsWrapper = styled('div')((props) => ({
  borderColor: props.theme.main,
  borderTopStyle: 'solid',
  borderWidth: 5,
  width: '100%',
  padding: '20px 0 1px 0',
  textAlign: 'left',
  '& .ant-calendar-picker-input.ant-input': {
    '&.focused-calendar': {
      border: `3px solid ${props.theme.primary} !important`
    }
  },
  '&>div': {
    height: 36,
    '& .ant-input, & .ant-btn': {
      height: '36px !important'
    },
    '.search-30': {
      '.ant-input': {
        height: '30px !important',
        paddingTop: 0,
        paddingBottom: 0
      },
      '.ant-select-selection__placeholder': {
        marginTop: -7
      }
    }
  },
  '&>div .ant-select-selection__rendered': {
    height: '36px !important'
  },
  '& .action-container': {
    marginBottom: 10
  }
}))

export const CustomerDetailsWrapper = styled('div')((props) => ({
  borderColor: props.theme.main,
  borderTopStyle: 'solid',
  borderWidth: 5,
  width: '100%',
  padding: 20,
  textAlign: 'left',
  '& .ant-calendar-picker-input.ant-input': {
    '&.focused-calendar': {
      border: `3px solid ${props.theme.primary} !important`
    }
  }
}))

export const DetailsTitle = styled('div')({
  fontSize: 20,
  fontWeight: 'bold',
  lineHeight: '23px',
  color: mediumGrey,
  textAlign: 'left',
  // paddingBottom: 20,
  '&.quick-print': {
    paddingTop: 24,
    marginBottom: 10,
    //color: 'rgba(0, 0, 0, 0.65)',
    color: gray01,
    fontFamily: "Arial",
    // fontFamily: "Museo Sans Rounded",
    fontWeight: 'normal',
    fontSize: 14,
    // color: darkGrey,
    lineHeight: '19.6px',
  }
})

export const CustomerDetailsTitle = styled('div')({
  fontSize: 20,
  fontWeight: 'bold',
  lineHeight: '23px',
  color: mediumGrey,
  textAlign: 'left',
  paddingBottom: 20,
})

export const InputRow: any = styled(Row)((props) => ({
  marginBottom: 5,
}))

export const RowDivider = styled('hr')({
  height: 1,
  backgroundColor: '#d9d9d9',
  width: '100%',
  border: 0,
  marginTop: 5,
  marginBottom: 10,
})

export const InputLabel = styled('div')((props) => ({
  //color: '#4A5355',
  color: gray01,
  fontSize: 12,
  lineHeight: '12px',
  letterSpacing: '0.05em',
  alignItems: 'center',
  padding: '10px 0',
  '&.warning': {
    color: '#EB5757',
    letterSpacing: 0,
    textAlign: 'center'
  },
  '&.theme': {
    color: props.theme.theme,
  },
  '&.mt-12': {
    marginTop: 12
  }
}))

export const DetailsRow: React.CSSProperties = {
  margin: '0 -20px 20px',
}

export const floatLeft: React.CSSProperties = {
  float: 'left',
}

export const floatRight: React.CSSProperties = {
  float: 'right',
}

export const Clear = styled('div')({
  clear: 'both',
})

export const tableRow: React.CSSProperties = {
  minHeight: 30,
  padding: '5px 0',
  display: 'flex',
  alignItems: 'center',
}

export const tableLineRow: React.CSSProperties = {
  minHeight: 50,
  display: 'flex',
  alignItems: 'center',
  borderBottom: `1px solid ${lightGrey}`,
}

export const textCenter: React.CSSProperties = {
  textAlign: 'center',
}

export const textRight: React.CSSProperties = {
  textAlign: 'right',
}

export const PopoverWrapper = styled('div')((props) => ({
  padding: 10,
  '.purchase-order-item-modal': {
    '.clear-icon': {
      display: 'none',
      marginRight: 6
    },
    '& .add-item-modal-input:hover .clear-icon.show': {
      display: 'block'
    }
  }
}))

export const ThemeUpload: any = styled(Upload)((props) => ({
  backgroundColor: props.theme.primary,
  borderColor: props.theme.primary,
  minWidth: 'fit-content',
  height: 32,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 5,
  '.ant-upload': {
    color: 'white',
  },
  '&:hover': {
    backgroundColor: props.theme.theme,
    borderColor: props.theme.theme,
  },
  '&:focus': {
    backgroundColor: props.theme.primary,
    borderColor: props.theme.primary,
  },
  '&.outline': {
    borderWidth: 1,
    borderStyle: 'solid',
    cursor: 'pointer',
    backgroundColor: 'white',
    '.ant-upload': {
      color: props.theme.primary
    },
    '&:hover': {
      backgroundColor: 'white',
      borderColor: props.theme.theme,
      '.ant-upload': {
        color: `${props.theme.main} !important`,
      },
    },
    '&:focus': {
      backgroundColor: 'white',
      borderColor: props.theme.theme,
      '.ant-upload': {
        color: props.theme.theme
      },
    },
  },
}))

export const ThemeButton: any = styled(Button)((props) => ({
  color: 'white !important',
  backgroundColor: props.theme.primary,
  borderColor: props.theme.primary,
  minWidth: 'fit-content',

  '&[disabled]': {
    color: "rgba(0,0,0,.5) !important"
  },
  '&.ant-btn-circle, &.ant-btn-circle-outline': {
    minWidth: 32
  },
  '&:hover': {
    color: 'white',
    backgroundColor: props.theme.theme,
    borderColor: props.theme.theme,
  },
  '&:focus': {
    color: 'white',
    backgroundColor: `${props.theme.primary} !important`,
    borderColor: `${props.theme.primary} !important`,
  },
  '&.sync-btn': {
    height: 42,
    padding: '0 58px',
    display: 'flex',
    alignItems: 'center',
    svg: {
      marginRight: 11
    },
    span: {
      fontSize: 18,
      fontFamily: 'Arial'
    },
    '&.mobile': {
      height: 25,
      padding: '0 24px',
      right: '40px !important',
      span: {
        fontSize: 14,
      }
    }
  },
  '&.header-button': {
    color: props.theme.primary,
    backgroundColor: 'white',
  },
  '&:header-button:hover': {
    color: props.theme.theme,
    backgroundColor: 'white',
    borderColor: 'white',
  },
  '&.icon': {
    display: 'flex',
    alignItems: 'center',
    'svg': {
      display: 'block'
    },
    'i': {
      display: 'block'
    },
    '&.enter-purchase': {
      marginLeft: 20,
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0,
      right: 1
    },
    '&.border-noleft': {
      borderTopLeftRadius: 0,
      borderBottomLeftRadius: 0,
      padding: '0 5px'
    }
  },
  '&.dark': {
    backgroundColor: `${props.theme.theme} !important`,
    borderColor: `${props.theme.theme} !important`,
    height: 41,
    '&.disabled': {
      opacity: '0.6 !important'
    },
    span: {
      fontSize: 15,
      fontStyle: 'normal',
      fontWeight: 600,
    },
  }
}))

export const ThemeSwitchWrap: any = styled('div')((props) => ({
  '.ant-switch-checked': {
    backgroundColor: props.theme.theme,
  }
}))

export const ThemeSwitch: any = styled(Switch)((props) => ({
  '&.ant-switch-checked': {
    backgroundColor: props.theme.theme,
  }
}))

export const ThemeColorDivRound: any = styled('div')((props) => ({
  color: 'white',
  backgroundColor: props.theme.primary,
  borderColor: props.theme.primary,
  padding: '7px 10px',
  borderRadius: '50%'
}))

export const ThemeOutlineButton: any = styled(Button)((props) => ({
  backgroundColor: 'white !important',
  color: props.theme.primary,
  borderColor: props.theme.primary,
  borderWidth: 1,
  borderStyle: 'solid',

  '&:hover': {
    borderColor: props.theme.main,
    color: `${props.theme.main} !important`,
  },
  '&:focus': {
    borderColor: props.theme.primary,
    color: props.theme.primary,
  },
  '& svg path': {
    stroke: props.theme.primary,
  },
  '&.fill svg path': {
    stroke: props.theme.primary,
  },

  '&:disabled': {
    'span': {
      color: 'rgba(0, 0, 0, 0.25) !important'
    },
    'svg': {
      fill: 'rgba(0, 0, 0, 0.25) !important',
      'path': {
        stroke: 'rgba(0, 0, 0, 0.25) !important',
      }
    },
  }
}))

export const ThemeOutlineCancelButton: any = styled(Button)((props) => ({
  color: cancelRed,
  borderColor: cancelRed,
  borderWidth: 1,
  borderStyle: 'solid',

  '&:hover': {
    borderColor: cancelRed,
    backgroundColor: cancelRed,
    color: white,
  },
  '&:focus': {
    borderColor: cancelRed,
    backgroundColor: cancelRed,
    color: white,
  },
}))

export const ThemeIconButton: any = styled(Button)((props) => ({
  padding: 0,
  color: props.theme.primary,
  '&:hover': {
    color: props.theme.main,
  },
  '&:focus': {
    color: props.theme.primary,
  },
  '&.no-border': {
    border: 0,
    background: "transparent"
  }
  '& svg path': {
    stroke: props.theme.primary,
  },
  '&.fill svg path': {
    stroke: props.theme.primary,
  },
  '&.dark': {
    height: 41,
    color: props.theme.theme,
    boxShadow: 'none',
    span: {
      fontWeight: 600,
      fontSize: 15
    }
  },
  '&.black': {
    //color: 'rgba(0,0,0,0.65) !important'
    color: `${gray01} !important`
  },
}))

export const ThemeTextButton: any = styled(Button)((props) => ({
  background: 'transparent',
  border: 0,
  boxShadow: 'none',
  '&.underline': {
    span: {
      color: props.theme.primary,
      textTransform: 'uppercase',
      textDecoration: 'underline',
      fontFamily: 'Museo Sans Rounded',
      fontSize: 14,
      fontWeight: 600,
      lineHeight: '15px'
    }
  },
  '&.bold': {
    span: {
      fontWeight: 'bold',
      color: `${props.theme.primary} !important`,
    }
  },
  '&.main': {
    i: {
      color: props.theme.theme,
    },
    span: {
      color: props.theme.theme,
    },
  }
}));

export const ThemeIcon: any = styled(Icon)((props) => ({
  color: props.theme.primary,
  borderColor: props.theme.primary,
}))

export const ThemeCheckBox: any = styled(Checkbox)((props) => ({
  "&.ant-checkbox-wrapper:hover .ant-checkbox-inner, .ant-checkbox:hover .ant-checkbox-inner, .ant-checkbox-input:focus + .ant-checkbox-inner": {
    borderColor: `${props.theme.primary} !important`,
  },
  "& .ant-checkbox-checked .ant-checkbox-inner": {
    backgroundColor: `${props.theme.primary} !important`,
    borderColor: `${props.theme.primary} !important`,
  },
  "& .ant-checkbox-checked::after": {
    borderColor: `${props.theme.primary} !important`,
  }
}));

export const ThemeFilledIcon: any = styled(Icon)((props: any) => ({
  'svg': {
    fill: props.fill
  },
}))

export const ThemeLink: any = styled(Link)((props) => ({
  color: props.theme.primary,
  '&:focus, &:hover': {
    color: props.theme.light
  }
}))

export const TextLink: any = styled(Link)((props) => ({
  color: `${gray01} !important`,
  fontSize: 14,
  // display: 'flex',
  // alignItems: 'center',
  width: '100%',
  height: '100%',
  paddingLeft: 9,
  paddingBottom: 14,
  paddingTop: 19,
  paddingRight: 0,
  '&.inventory': {
    //color: 'rgba(0, 0, 0, 0.65) !important',
    color: `${gray01} !important'`,
    paddingRight: '9px !important',
    paddingTop: 0,
    paddingBottom: 0
  }
}))

export const ThemeInput: any = styled(Input)((props) => ({
  color: gray01,
  '&:hover': {
    borderColor: props.theme.primary,
  },
  '&:focus': {
    borderColor: props.theme.primary,
    boxShadow: 'none',
  },
  '&.ant-input-affix-wrapper:hover .ant-input:not(.ant-input-disabled), &.ant-input-affix-wrapper:hover .ant-input:not(.ant-input-disabled)': {
    borderColor: props.theme.primary,
  },
  '&.warning': {
    backgroundColor: '#FEEAE9',
    border: '1px solid #EB5757',
    boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
  },
  '& .clear-icon.show': {
    display: 'block !important'
  }
}))

export const ThemeCheckbox: any = styled(Checkbox)((props) => ({
  '.ant-checkbox-inner': {
    borderColor: props.theme.primary,
  },
  '.ant-checkbox-input:hover': {
    '+ .ant-checkbox-inner': {
      borderColor: props.theme.primary,
    },
  },
  '.ant-checkbox-checked': {
    '.ant-checkbox-inner': {
      backgroundColor: props.theme.primary,
      borderColor: props.theme.primary,
    },
    '&::after': {
      borderColor: props.theme.primary,
    },
  },
  '.ant-checkbox-disabled': {
    '.ant-checkbox-inner': {
      color: 'rgba(0, 0, 0, 0.25)',
      backgroundColor: '#f5f5f5',
      borderColor: '#d9d9d9',
    }
  },

}))

export const ThemeSelect: any = styled(Select)((props) => ({
  color: gray01,
  '&:hover': {
    '.ant-select-selection': {
      borderColor: props.theme.primary,
    },
  },
  '&.ant-select-focused': {
    '.ant-select-selection': {
      borderColor: props.theme.primary,
      boxShadow: 'none',
    },
  },
  '&.ant-select-disabled .ant-select-selection': {
    backgroundColor: "#FEFEFE !important"
  },
  '&.pl-select .ant-select-selection--single': {
    height: '42px',
  },
  '&.pl-select .ant-select-selection__rendered': {
    lineHeight: '42px'
  },
  '&.select-vendor': {
    '.ant-select-selection__placeholder': {
      //color: 'rgba(0, 0, 0, 0.65)'
      color: gray01
    }
  }
}))

export const ThemeTransSelect: any = styled(Select)((props) => ({
  width: '100%',
  paddingBottom: 2,
  height: 37,
  borderWidth: 1,
  borderColor: props.theme.primary,
  borderStyle: 'solid',

  '.ant-select-selection__placeholder': {
    fontSize: 17,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#000000',
  },
  '.ant-select-selection': {
    background: 'transparent',
    border: 0,
  },
  '&:hover': {
    '.ant-select-selection': {
      borderColor: props.theme.primary,
    },
  },
  '&.ant-select-focused': {
    borderWidth: '3px !important',
    '.ant-select-selection': {
      borderColor: props.theme.primary,
      boxShadow: 'none',
    },
  },
  '.ant-select-selection__rendered': {
    marginLeft: 0,
    marginRight: 0,
  },
  '.ant-select-selection-selected-value': {
    fontSize: 17,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    color: '#000000',
    whiteSpace: 'pre',
    lineHeight: 1.1,
    marginTop: -2
  }
  '.ant-select-arrow': {
    display: 'none'
  }
}))

export const ThemeTextArea: any = styled(Input.TextArea)((props) => ({
  color: gray01,
  '&:hover': {
    borderColor: props.theme.primary,
  },
  '&:focus': {
    borderColor: props.theme.primary,
    boxShadow: 'none',
  },
}))

export const ThemeRadio: any = styled(Radio)((props) => ({
  color: gray01,
  '.ant-radio-checked .ant-radio-inner': {
    borderColor: props.theme.primary,
    '&::after': {
      backgroundColor: props.theme.primary,
    },
  },
  '.ant-radio-checked:after': {
    border: `1px solid ${props.theme.primary}`,
  },
}))

export const TableTr = styled('tr')({
  height: '50px',
})

export const ThemeTable = styled(MutiSortTable)((props) => ({
  overflowX: 'auto',
  // '.ant-table': {
  //   overflowX: 'auto'
  // },
  '.ant-checkbox-checked .ant-checkbox-inner': {
    backgroundColor: props.theme.primary,
    borderColor: props.theme.primary,
  },
  '.ant-checkbox-wrapper:hover .ant-checkbox-inner, .ant-checkbox:hover .ant-checkbox-inner, .ant-checkbox-input:focus + .ant-checkbox-inner': {
    borderColor: props.theme.primary,
  },
  '.ant-checkbox-indeterminate .ant-checkbox-inner::after': {
    backgroundColor: props.theme.primary,
  },
  '.ant-table-placeholder': {
    display: 'none',
  },
  '&.min-height-placeholder': {
    '.ant-table-placeholder': {
      minHeight: 300
    }
  },
  '&.simple-expandable-report-table': {
    '.ant-table-row-level-1': {
      'td:first-of-type': {
        paddingLeft: '40px !important',
        'a': {
          display: 'none !important'
        }
      }
    }
  },
  '&:not(.extra-charge)': {
    backgroundColor: 'white',
    padding: 0,
    '.focused-row': {
      backgroundColor: props.theme.lighter;
    }
    '.ant-spin-dot-item': {
      backgroundColor: props.theme.main,
    },
    '.ant-pagination': {
      paddingRight: '30px',
      '.ant-pagination-item-active': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-item:focus, .ant-pagination-item:hover': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-prev:focus .ant-pagination-item-link, .ant-pagination-next:focus .ant-pagination-item-link, .ant-pagination-prev:hover .ant-pagination-item-link, .ant-pagination-next:hover .ant-pagination-item-link': {
        borderColor: props.theme.light,
        color: props.theme.light
      }
    },
    '& .ant-table-content': {
      '.ant-table-thead > tr > th': {
        textAlign: 'center',
        '&.th-left': {
          textAlign: 'left',
          '&.pl-0': {
            paddingLeft: 9
          }
        },
        '&.th-right': {
          textAlign: 'right',
          '&.pdr-9': {
            paddingRight: 9,
          },
        },
        '&.p9': {
          paddingLeft: 9,
          paddingRight: 9
        },
        '&.th-cost': {
          paddingRight: 29
        }
      },
      '& .ant-table-body': {
        '& > table': {
          // borderTop: 'none',
          // borderLeft: 'none',
          '& .ant-table-thead': {
            // backgroundColor: props.theme.lighter,
            // boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
            '& tr:first-of-type th': {
              // backgroundColor: `${props.theme.light}45`,
              '& .ant-table-column-title': {
                fontSize: 18,
                fontWeight: 'bold',
                color: props.theme.main,
              },
            },
            '& tr:last-of-type th': {
              // backgroundColor: props.theme.lighter,
              '& .ant-table-column-title': {
                fontSize: 12,
                fontWeight: 700,
                color: mediumGrey,
              },
            },
            '& > tr > th': {
              // backgroundColor: props.theme.lighter,
              // borderLeft: '2px solid white',
              paddingTop: 12,
              paddingBottom: 6,
              color: '#555F61',
              '&:first-of-type': {
                borderLeft: 0,
              },
            },
          },
          [`& ${TableTr} td`]: {
            padding: '3px 10px',
            textAlign: 'left',
            whiteSpace: 'pre-wrap',
            wordWrap: 'break-word',
          },
          '& .ant-table-tbody': {
            '& > tr > .buttonCell': {
              paddingTop: '0px',
              paddingBottom: '0px',
              paddingRight: '0px',
              paddingLeft: '7px',
              minWidth: '110px',
            },
            '& > tr': {
              '.visible': {
                visibility: 'hidden'
              },
              '.block': {
                display: 'block'
              },
              '.none': {
                display: 'none'
              }
            }
            '& > tr:hover': {
              '.colorable': {
                border: `1px solid ${props.theme.primary} !important`,
                backgroundColor: `${props.theme.primary} !important`,
              },
              '.visible': {
                visibility: 'visible !important',
              },
              // '.block': {
              //   display: 'block !important',
              // },
              // '.none': {
              //   display: 'none !important',
              // }
            },
            '& > tr > td': {
              paddingTop: 0,
              paddingBottom: 0,
              paddingLeft: '9px',
              paddingRight: '9px',
              height: 50,
              //color: 'rgba(0, 0, 0, 0.65)',
              color: gray01,
              '.product-name': {
                color: props.theme.dark
                '&:hover': {
                  textDecoration: 'underline',
                }
              },
            },
          },
        },
      },
      '.tab-able:focus': {
        border: `2px dotted ${props.theme.main}`
      },
      '&.no-top-border': {
        '.ant-table': {
          table: {
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            '.ant-table-thead > tr: first-of-type > th: first-of-type': {
              borderTopLeftRadius: 0,
            },
            '.ant-table-thead > tr:first-of-type > th:last-child': {
              borderTopRightRadius: 0,
            }
          },
          '&.ant-table-empty': {
            '.ant-table-body': {
              overflowX: 'hidden !important'
            }
          }
        },
      },
      '&.nested-head:not(.extra-charge)': {
        '.ant-table-thead': {
          'tr:first-of-type': {
            'th': {
              backgroundColor: `${props.theme.lighter} !important`
            }
          }
        }
      },
      '&.reports-table': {
        'th:first-of-type': {
          paddingLeft: '49px !important'
        },
      },
      '&.lots-table': {
        '&.ant-table-wrapper': {
          '.ant-table-thead': {
            boxShadow: 'none !important'
            'th': {
              padding: '12px 8px 8px'
            },
          },
          '.ant-table-content .ant-table-body .ant-table-tbody > tr': {
            'td': {
              fontWeight: 'normal',
              backgroundColor: 'white',
              '&:last-child': {
                borderRight: '1px solid #EDF1EE'
              }
            }
          },
          '.ant-table tbody tr:hover': {
            td: {
              backgroundColor: `${topLightGrey} !important`,
            }
          },
          '.ant-table-footer': {
            backgroundColor: 'white',
            padding: 0,
            display: 'flex',
            alignItems: 'center',
            height: 50,
            '.ant-btn': {
              //color: 'rgba(0, 0, 0, 0.65) !important',
              color: `${gray01} !important`
              fontWeight: 'normal !important',
              fontSize: 14,
              textAlign: 'left !important',
              width: '100%',
              paddingLeft: 9,
              border: 'none !important'
            }
          }
        }
      }
    }
  }))

export const ThemeModal = styled(Modal)((props: any) => ({
  '&': {
    '.ant-modal-title': {
      color: props.theme.main,
      fontSize: 20
    },
    '.ant-btn': {
      color: props.theme.primary,
      borderColor: props.theme.primary,
      '&:hover': {
        borderColor: props.theme.main,
        color: props.theme.main,
      },
      '&:focus': {
        borderColor: props.theme.primary,
        color: props.theme.primary,
      },
      '&.ant-btn-primary': {
        backgroundColor: props.theme.primary,
        borderColor: props.theme.primary,
        color: 'white',
        '&:hover': {
          backgroundColor: props.theme.theme,
        },
        '&:focus': {
          backgroundColor: props.theme.primary,
          borderColor: props.theme.primary,
        },
        '&:disabled': {
          backgroundColor: mediumGrey2,
          borderColor: mediumGrey2
          '&:hover': {
            backgroundColor: mediumGrey2,
          },
          '&:focus': {
            backgroundColor: mediumGrey2,
            borderColor: mediumGrey2,
          },
        },
        '&.ant-btn[disabled]': {
          backgroundColor: '#d9d9d9',
          borderColor: '#d9d9d9'
        }
      },
    },
    '.ant-btn-link':{
      borderColor: 'transparent',
    },
    '&.price-modal': {
      '.ant-modal-header': {
        paddingLeft: 33,
        height: 54,
        display: 'flex',
        alignItems: 'center',
        backgroundColor: props.theme.primary,
        '.ant-modal-title': {
          marginTop: 4,
          color: 'white',
          fontFamily: 'Museo Sans Rounded',
          fontSize: 24,
          fontWeight: 600
        }
      },
      '.ant-modal-body': {
        padding: '23px 41px 38px'
      }
    },
    '.add-item-modal': {
      '.ant-select-selection__placeholder': {
        fontFamily: 'Museo Sans Rounded',
        fontSize: 14,
        color: '#82898A'
      },
      '.ant-input': {
        letterSpacing: 'unset !important',
        '&::placeholder': {
          fontFamily: 'Museo Sans Rounded',
          fontSize: 14,
          color: '#82898A !important',
        }
      }
      '.clear-icon': {
        display: 'none',
        marginRight: 6
      },
      '& .add-item-modal-input:hover .clear-icon.show': {
        display: 'block'
      }
    }
  },
  '&.batch-edit-confirm .ant-modal-footer>div': {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'row-reverse'
  },
  '&.onboarding': {
    height: 870,
    paddingBottom: 0,
    '.ant-spin-spinning': {
      height: 870,
      maxHeight: 870,
    },
    '.ant-spin-container': {
      height: 870
    },
    '.ant-modal-content': {
      width: '100%',
      height: '100%',
      '.ant-modal-body': {
        height: '100%',
        padding: 0,
        '.left': {
          backgroundColor: props.theme.dark,
          width: 350,
          height: '100%',
          float: 'left',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          padding: '0, 27px',
          p: {
            marginTop: 143,
            fontFamily: 'Museo Sans Rounded',
            fontStyle: 'normal',
            fontWeight: 600,
            lineHeight: '115%',
            fontSize: 36,
            color: 'white',
            textAlign: 'center',
            marginBottom: 90
          }
        },
        '.right': {
          float: 'right',
          padding: '74px 62px',
          width: 850,
          '.title': {
            fontFamily: 'Museo Sans Rounded',
            fontStyle: 'normal',
            fontSize: 24,
            fontWeight: 600,
            textAlign: 'center',
            color: props.theme.main
          },
          '.content': {
            fontFamily: 'Museo Sans Rounded',
            marginTop: 60,
            marginBottom: 10,
            height: 545,
            width: '100%',
            overflowY: 'auto',
            '.c2': {
              direction: 'ltr',
              lineHeight: 1.2,
              paddingBottom: '5pt',
              paddingTop: '5pt'
            },
            '.c3': {
              fontWeight: 'bold'
            }
          },
          '.checkAccept': {
            textAlign: 'right',
            margin: '10px 10px 20px 0'
          },
          '.ant-btn-round': {
            fontFamily: 'Museo Sans Rounded',
            fontStyle: 'normal',
            fontSize: 18,
            height: 58,
            width: 198,
            '&.accept': {
              float: 'right',
              color: 'white',
              '&:disabled': {
                backgroundColor: '#ccc',
                borderColor: '#ccc',
                color: '#877a7a !important'
              }
            },
            '&.decline': {
              float: 'left'
            }
          }
        },
        '.clearfix': {
          clear: 'both'
        }
      }
    }
  },
  '&.product-vertical': {
    '.ant-modal-body': {
      padding: '8px 24px 24px',
    },
    '*': {
      fontFamily: 'Museo Sans Rounded !important',
      fontWeight: 'normal'
    },
    '.ant-form-item': {
      marginBottom: 5
    },
    '.ant-form-item-label': {
      lineHeight: '18px',
      paddingTop: 16,
      span: {
        fontWeight: 'normal',
        textFransform: 'none'
      }
    },
    '.ant-form-explain': {
      display: 'none'
    },
    '.ant-select-selection__placeholder': {
      fontSize: 13,
      fontWeight: 'normal'
    },
    '.ant-form-item-control': {
      lineHeight: 'normal',
    },
    '.ant-modal-footer': {
      display: 'flex'
    },
    '.ant-btn': {
      '&.ant-btn-primary': {
        float: 'left',
        marginRight: 5
      },
      '&.ant-btn-link': {
        border: 0
      }
    },
    '.ant-select-auto-complete.ant-select .ant-input': {
      //color: 'rgba(0, 0, 0, 0.65)',
      color: gray01,
      fontSize: 14
    },
    'span.ant-radio + *': {
      paddingTop: 3
    },
    '.ant-select-selection__placeholder': {
      lineHeight: '23px',
      fontSize: 14
    }
  },
  '&.select-document-modal': {
    '.check-row': {
      marginTop: 12,
    },
    '.ant-modal-footer': {
      backgroundColor: '#f7f7f7'
    }
  },
  '&.dark': {
    '.ant-modal-title': {
      color: gray01,
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: 18,
    },
    '.ant-modal-body': {
      padding: '32px 23px',
      label: {
        color: gray01, //'#4A5355',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: 14,
        lineHeight: '140%',
        display: 'block'
      },
      '.ant-form-item': {
        marginTop: 4,
        marginBottom: 0,
        '.ant-select-selection__placeholder': {
          color: '#82898A',
          fontStyle: 'normal',
          fontWeight: 600,
          fontSize: 14,
          lineHeight: '140%',
          display: 'block'
        }
      },
      '.input-account': {
        width: 248,
        height: 32,
        borderRadius: 2,
        borderColor: '#D8DBDB',
      }
    },
    '.ant-modal-footer': {
      padding: '13px 23px',
      backgroundColor: '#F7F7F7'
    }
  },
  '&.sync-qbo-modal': {
    '.ant-modal-body': {
      padding: 0,
      p: {
        margin: '12px 25px',
      },
      '.content': {
        borderTop: '1px solid #ddd',
        '.items': {
          borderRight: '1px solid #ddd',
          width: '60%',
          padding: '10px 25px'
        },
        '.bill': {
          padding: '10px 25px'
        },
        label: {
          fontWeight: 'normal',
          '&.new': {
            marginTop: 18,
            marginBottom: 18,
            display: 'flex'
          }
        },
        '.ant-radio-group': {
          label: {
            marginTop: 18,
            marginBottom: 18,
            display: 'flex'
          }
        }
      }
    }
  },
}))

export const ThemeTabs = styled(Tabs)((props) => ({
  '&': {

    '&.ant-tabs.ant-tabs-card': {
      '.ant-tabs-nav': {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: 3,
      },
      '.tab-focus': {
        borderWidth: '1px',
        borderStyle: 'solid',
        borderColor: props.theme.primary
      },
      '.ant-tabs-tab-active': {
        color: props.theme.primary,
      },
      '.ant-tabs-tab:hover': {
        color: props.theme.primary,
      },
    },
  }
}))

export const FlexRow = styled('div')((props) => ({
  display: 'flex',
  flexDirection: 'row',
}))

export const Flex = styled('div')((props: any) => ({
  display: 'flex',
  '&.v-center': {
    alignItems: 'center',
  },
  '&.bottom-align': {
    alignItems: "end",
  },
  '&.h-center': {
    justifyContent: 'center',
  },
  '&.space-between': {
    justifyContent: 'space-between',
  },
}))

export const Flex1 = styled('div')((props) => ({
  flex: 1,
}))

export const Characters = styled('div')((props) => ({
  fontSize: 12,
  margin: '15px 10px',
  color: 'black',
}))

export const Incoming = styled('div')((props) => ({
  backgroundColor: `${props.theme.primary}50`,
  padding: '1px 12px',
  borderRadius: 5,
  color: black,
  fontSize: 11,
  fontWeight: 'normal',
  width: 'fit-content',
}))

export const FlexCenter = styled('div')((props: any) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
}))

export const ErrorAlert = styled(Alert)((props) => ({
  '&': {
    // backgroundColor: '#fff1f0',
    // border: '1px solid #ffa39e',
    padding: '6px 20px',
    justifyContent: 'flex-start',
    '.ant-alert-message': {
      // color: '#f5222d',
      marginTop: 1,
    },
  },
}))

export const PriceSheetsTable = styled(MutiSortTable)((props) => ({
  '&': {
    '.ant-pagination': {
      paddingRight: '30px',
      '.ant-pagination-item-active': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-item:focus, .ant-pagination-item:hover': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-prev:focus .ant-pagination-item-link, .ant-pagination-next:focus .ant-pagination-item-link, .ant-pagination-prev:hover .ant-pagination-item-link, .ant-pagination-next:hover .ant-pagination-item-link': {
        borderColor: props.theme.light,
        color: props.theme.light
      }
    },
    '& .ant-table-content': {
      '& .ant-table-body': {
        '& > table': {
          borderCollapse: 'collapse',
          border: 'none',
          '.ant-table-row': {
            borderCollapse: 'separate',
            borderSpacing: '0 15px',
          },
        },
      },
    },
  },
}))

export const SalesHeader = styled('div')((props) => ({
  backgroundColor: `${props.theme.primary}90`,
  textAlign: 'center',
  fontSize: 18,
  color: 'white',
  padding: '5px 0',
}))

export const ThemeSpin = styled(Spin)((props) => ({
  '&': {
    maxHeight: 'fit-content !important',
    color: props.theme.light,
    '.ant-spin-dot-item': {
      backgroundColor: props.theme.light,
    }
  },
}))

export const ThemeDatePicker = styled(DatePicker.RangePicker)((props) => ({
  color: gray01,
  '.ant-calendar-picker-container': {
    border: '10px solid green',
  },
  '&': {
    '.ant-calendar-picker-container': {
      border: '10px solid green',
    },
    backgroundColor: `blue !important`,
    borderWidth: 1,
    borderColor: 'red',
    '.ant-calendar-range .ant-calendar-selected-start-date .ant-calendar-date:hover, .ant-calendar-range .ant-calendar-selected-end-date .ant-calendar-date:hover': {
      backgroundColor: `${props.theme.main} !important`,
    },
    '.ant-calendar-range .ant-calendar-selected-start-date .ant-calendar-date, .ant-calendar-range .ant-calendar-selected-end-date .ant-calendar-date': {
      background: 'red',
    }
  },
  '.ant-calendar-range .ant-calendar-selected-start-date .ant-calendar-date:hover, .ant-calendar-range .ant-calendar-selected-end-date .ant-calendar-date:hover': {
    backgroundColor: `${props.theme.main} !important`,
  }
}))

export const EditA = styled('div')((props) => ({
  borderTop: '1px solid #eee',
  marginTop: 1,
  padding: '8px',
  display: 'block',
  cursor: 'pointer'
}))

export const FullInputNumber = styled(InputNumber)((props: any) => ({
  width: '100% !important'

}))

export const ThemeInputNumber = styled(InputNumber)((props) => ({
  width: props.width ? props.width : '100% !important',
  color: gray01,
  '&:hover': {
    borderColor: props.theme.primary,
  },
  '&:focus': {
    borderColor: props.theme.primary,
    boxShadow: 'none',
  },
  '&.ant-input-number-focused': {
    borderColor: props.theme.primary,
    boxShadow: 'none',
  },
  '&.ant-input-number-input-wrap:hover .ant-input-number-input-wrap:focus': {
    borderColor: props.theme.primary,
  },
  '&.warning': {
    backgroundColor: '#FEEAE9',
    border: '1px solid #EB5757',
    boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
    // color: props.theme.theme
  },
  '&.readonly': {
    '.ant-input-number-input': {
      // backgroundColor: 'white !important',
      //color: 'rgba(0, 0, 0, 0.65)',
      color: gray01,
      cursor: 'text !important'
    }
  }
}))


export const palletPrintCol: React.CSSProperties = {
  textAlign: 'center',
  marginTop: 50,
}

export const MainToLink = styled('a')((props) => ({
  position: 'absolute',
  bottom: 53,
  left: 20,
  zIndex: 1,
  'span': {
    maxWidth: 305
  },
  textDecoration: 'none'
}))

export const phoneLink: React.CSSProperties = {
  position: 'absolute',
  bottom: 21,
  left: 20,
  zIndex: 1
}

export const QuantityTableWrapper = styled('div')((props) => ({
  '&': {
    '.ant-table-content': {
      '& .ant-table-body': {
        '& > table': {
          '& .ant-table-tbody': {
            '& > tr': {
              '&:nth-child(even)': {
                backgroundColor: props.theme.lighter
              },
              'td': {
                height: 40,
                '.ant-form-item': {
                  height: 35,
                  'input': {
                    height: '35px !important'
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}))
export const ThemeForm = styled(Form)((props) => ({
  '.ant-form-item': {
    textAlign: 'left',
  },
}))

export const PriceModalWrapper = styled('div')((props) => ({
  fontFamily: 'Museo Sans Rounded',
  '.sub-title': {
    fontWeight: 500,
    fontSize: 28,
    color: props.theme.theme,
    marginBottom: 15,
  },
  '.blod': {
    fontWeight: 500
  },
  '.left-side': {
    float: 'left',
    width: 524,
  },
  '.section-title': {
    fontWeight: 600,
    fontSize: 18,
    lineHeight: '20px',
    color: props.theme.main,
  },
  '.right-side': {
    float: 'right',
    width: 681,
    '.right': {
      '.ant-btn': {
        border: 'none !important',
        span: {
          fontSize: 14,
          fontWeight: 500,
          lineHeight: '16px'
        }
      }
    },
    '.has-border': {
      padding: '12px 16px',
      '.radio-label': {
        fontWeight: 500,
        fontSize: 14,
        lineHeight: '16px',
        color: darkGrey,
        marginBottom: 11
      },
      '.ant-radio-wrapper': {
        span: {
          color: darkGrey,
          fontSize: 12,
          lineheight: '13.8px',
          fontWeight: 500
        }
      },
      '.vertical-radio': {
        display: 'block',
        height: 30,
        lineHeight: '30px',
      },
      '.ant-table': {
        'table': {
          border: 'none !important'
        },
        'th': {
          padding: '1px 0 !important',
          height: 22,
          fontSize: 10,
          lineHeight: '12px',
          fontWeight: 600,
          color: darkGrey,
          backgroundColor: 'transparent',
          borderColor: 'white',
          borderWidth: 4,
          // '&:nth-child(3)': {
          //   color: '#1A61A3',
          // },
          // '&:nth-child(3)': {
          //   color: '#C01700',
          // },
        },
        'td': {
          fontWeight: 500,
          fontSize: 12,
          lineHeight: '13.8px',
          color: 'black',
          backgroundColor: 'rgba(82, 158, 99, 0.1)',
          borderColor: 'white',
          borderWidth: 4,
          padding: '10px 0 9px',
          '&:nth-child(1)': {
            fontSize: 14,
            fontWeight: 500,
          },
          '&:nth-child(3)': {
            backgroundColor: '#1A61A310',
            // color: '#1A61A3',
            fontWeight: 500,
          },
          '&:nth-child(4)': {
            backgroundColor: '#FF715E10',
            color: '#C01700',
            fontWeight: 500
          },
          '&:nth-child(5)': {
            fontSize: 10,
            lineHeight: '11.5px',
          },
          '&:nth-child(6)': {
            color: '#1C6E31',
          }
        }
      }
    }
  },
  '.has-border': {
    border: '1px solid #DDDDDD',
    borderRadius: 6,
    padding: '12px 15px 11px',
    '.label': {
      fontWeight: 600,
      fontSize: 14,
      lineHeight: '16px',
      color: mediumGrey,
      textDecoration: 'uppercase'
    },
    '.value': {
      fontWeight: 500,
      fontSize: 24,
      lineHeight: '27px',
      color: darkGrey,
      marginTop: 2
    },
    '.ant-calendar-picker-input': {
      paddingLeft: 22,
      color: props.theme.main,
      fontSize: 14,
      fontWeight: 600
    },
    '.ant-calendar-picker-icon': {
      display: 'block',
      float: 'left',
      left: '0 !important'
    },
    '.dp-label': {
      fontWeight: 600,
      fontSize: 13,
      color: darkGrey,
    }
  },
  '&.within-table': {
    '.right-side': {
      '.ant-table-thead': {
        boxShadow: 'none !important',
        '&>tr>th': {
          borderColor: '#fafafa !important',
          '.ant-table-header-column': {
            width: '100%',
            height: 'calc(100% + 2px)',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            background: '#fafafa',
            borderWidth: 0,
            marginTop: -1,
            marginBottom: -1
          }
        }
      },
      'td': {
        borderColor: '#fafafa !important',
        height: '36px !important',
        '&:nth-child(1), &:nth-child(2), &:nth-child(5), &:nth-child(6)': {
          backgroundColor: `${props.theme.lighter} !important`
        },
      }
    }
  }
}))

export const tableCss = () =>
  css({
    '&': {
      padding: 0,
      '.ant-pagination': {
        paddingRight: '30px',
      },
      '& .ant-table-content': {
        '& .ant-table-body': {
          '& > table': {
            border: 'none',
            '& .ant-table-thead': {
              boxShadow: 'none',
              '& > tr > th:first-of-type': {
                paddingLeft: '',
              },
              '& > tr > th:last-of-type': {
                paddingRight: '',
              },
              '& > tr > th': {
                backgroundColor: white,
                border: 'none',
              },
            },
            [`& ${TableTr} td`]: {
              padding: '3px 10px',
              textAlign: 'left',
              whiteSpace: 'pre-wrap',
              wordWrap: 'break-word',
            },
            '& .ant-table-tbody': {
              '& > tr > .buttonCell': {
                paddingTop: '0px',
                paddingBottom: '0px',
                paddingRight: '0px',
                paddingLeft: '7px',
                minWidth: '110px',
              },
              '& > tr > td:first-of-type > span': {
                marginLeft: '',
              },
              '& > tr > td': {
                paddingTop: '19px',
                paddingBottom: '14px',
                paddingLeft: '9px',
                paddingRight: '9px',
              },
            },
          },
        },
      },
    },
  })

export const flexStyle: React.CSSProperties = {
  display: 'flex',
  alignItems: 'center'
}

export const LoadingOverlay = styled('div')((props) => ({
  position: 'absolute',
  top: 60,
  left: 0,
  width: '100%',
  height: 'calc(100% - 60px)',
  backgroundColor: 'rgba(255, 255, 255, 0.85)',
  zIndex: 9999,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  '.ant-spin': {
    color: props.theme.light,
    fontWeight: 'bold',
    fontSize: '14px !important'
  }
}))

export const RedSpan = styled('span')((props) => ({
  color: 'red',
}))
