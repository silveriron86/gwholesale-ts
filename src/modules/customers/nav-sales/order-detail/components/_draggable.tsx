import React, { useState, useEffect } from 'react'
import { Icon, Select, Table, Tooltip, Popconfirm, Button, Spin } from 'antd'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import _ from 'lodash'
import jQuery from 'jquery'

import { Icon as IconSVG } from '~/components'
import UomSelect from './uom-select'
import { ReplaceItem } from '../modals/replace-item'
import { BodyRow, HeaderRow, noDragBodyRow } from './draggable/draggable-row'
import { ResizeableTitle } from './resizable/resizable-title'
import { gray01 } from '~/common'
import { OrderItem } from '~/schema'
import { history } from '~/store/history'
import {
  formatNumber,
  inventoryQtyToRatioQty,
  mathRoundFun,
  ratioPriceToBasePrice,
  ratioQtyToInventoryQty,
  numberMultipy,
  judgeConstantRatio,
  notify,
  formatItemDescription,
  getAllowedEndpoint,
  basePriceToRatioPrice,
} from '~/common/utils'

import { CustomButton, Flex, ItemHMargin4, Unit } from '../../styles'
import { ThemeCheckbox, ThemeInputNumber, ThemeSelect, ThemeSpin } from '~/modules/customers/customers.style'
import FGFooter from './footer/fg-footer'
import GeneralFooter from './footer/general-footer'


export type DragSortTableProps = {
  orderItems: OrderItem[]
  dataSource: OrderItem[]
  selectedRows: OrderItem[]
  simplifyItems: any[]
  simplifyItemsByOrderId: any[]
  orderItemByProduct: any
  catchWeightValues: any
  onRow: any
  hasFooter: any
  rowKey: string
  renderFloatingMenu: Function
  onToggleSelect: Function
  openModal: Function
  onClickAddItem: Function
  validateOrderStatusIsShipped: Function
  draggable: boolean
  updating: boolean
  updatingField: boolean
  openAddModal: Function
  openAddItemModal: boolean
  sellerSetting: any
  isFreshGreen: boolean
  addingCartItem: boolean
  updateOrderItemByProductId: Function
  updateOrderItemPrice: Function
  updateSortOrderItemDisplayOrder: Function
}

export const DragSortingTable: React.FC<DragSortTableProps> = (props: any) => {
  const {
    dataSource,
    rowKey,
    hasFooter,
    selectedRows,
    catchWeightValues,
    renderFloatingMenu,
    onToggleSelect,
    openModal,
    onClickAddItem,
    handlerUpdateSOUOMRedux,
    validateOrderStatusIsShipped,
    draggable,
    updating,
    sellerSetting,
    isFreshGreen,
    addingCartItem,
    updateOrderItemByProductId,
    updateOrderItemPrice,
    updateSortOrderItemDisplayOrder,
    page,
    pageSize,
    total
  } = props

  const [hoverIndex, setHoverIndex] = useState(-1)
  const [dragIndex, setDragIndex] = useState(-1)
  const [data, setData] = useState(dataSource)
  const [currentPage, setCurrentPage] = useState(page + 1)
  const [pageSize] = useState(pageSize)
  const [request, setRequest] = useState<any[]>([])
  const [curDisplayOrder, setCurDisplayOrder] = useState(null)
  const [initialColumn, setInitinalColumn] = useState<any[]>([])
  const [finalTableColumns, setFinalTableColumns] = useState<any[]>([])
  const [freshGreenColumns, setFreshGreenColumns] = useState<any[]>([])
  const [nonFGColumns, setNonFGColumns] = useState<any[]>([])
  const [commonColumns, setCommonColumns] = useState<any[]>([])
  const [configurableOption, setConfigurableOption] = useState<any[]>([])
  const [footer, setFooter] = useState<any>(null)

  const isShipped = validateOrderStatusIsShipped()
  const accountType = localStorage.getItem('accountType') || ''
  const isDisablePickingStep = props.company?.isDisablePickingStep
  const readOnly = props.currentOrder.wholesaleOrderStatus === 'CANCEL'

  /* Define table DOM action handler */
  const components = {
    body: {
      row: BodyRow,
    },
    header: {
      row: HeaderRow,
      cell: ResizeableTitle,
    },
  }

  const noDragComponents = {
    body: {
      row: noDragBodyRow,
    },
    header: {
      row: HeaderRow,
      cell: ResizeableTitle,
    },
  }

  useEffect(() => {
    setCurrentPage(page + 1)
  }, [page])

  const handleResize = (index: number) => (e: any, { size }: any) => {
    if (!finalTableColumns[index]?.width) return

    const copyInitial = [...initialColumn]
    copyInitial.splice(index, 1, { ...copyInitial[index], width: size.width })
    setInitinalColumn(copyInitial)
  }

  const moveRow = (dragIndex: number, hoverIndex: number) => {
    setDragIndex(dragIndex)
    setHoverIndex(hoverIndex)
  }

  const dropRow = (item: any) => {
    const newCoverIndex = (currentPage - 1) * pageSize + hoverIndex
    if (props.dataSource[newCoverIndex].displayOrderProduct === item.displayOrder) return
    props.startUpdating()
    const newDisplayOrder =
      newCoverIndex > dragIndex
        ? props.dataSource[newCoverIndex].displayOrderProduct - 1
        : props.dataSource[newCoverIndex].displayOrderProduct

    //update display order in database
    updateSortOrderItemDisplayOrder({
      orderId: props.currentOrder.wholesaleOrderId,
      productId: item.itemId,
      oldDisplayOrder: item.displayOrder,
      newDisplayOrder,
    })
  }
  /* End table DOM action handler */

  /* Define useEffect hooks */
  useEffect(() => {
    setFreshGreenColumns([
      {
        title: '',
      },
      {
        title:
          draggable && !readOnly ? (
            <>
              <ThemeCheckbox
                style={{ width: 30 }}
                onChange={onToggleSelect.bind(this, 'All')}
                checked={data.length && selectedRows.length == data.length}
              />
              Order Qty
            </>
          ) : (
            'Order Qty'
          ),
        dataIndex: 'quantity',
        width: 40,
        render: (quantity: number, record: any, index: number) => {
          return (
            <div className="name-column order-quantity-input">
              <Flex className="v-center">
                <>
                  <span className={draggable && !readOnly ? 'row-index-number' : 'w30'}>
                    {draggable === false ? index + 1 : record.displayOrderProduct + 1}
                  </span>
                  {draggable && !readOnly && (
                    <ThemeCheckbox
                      className="row-selection"
                      style={{ display: selectedRows.includes(record.displayOrderProduct) ? 'block' : 'none' }}
                      onChange={onToggleSelect.bind(this, record.displayOrderProduct)}
                      checked={selectedRows.includes(record.displayOrderProduct)}
                    />
                  )}
                  <Flex className="v-center" style={{ flex: 1 }}>
                    <ThemeInputNumber
                      className={`order-qty no-stepper`}
                      value={quantity}
                      style={{ marginRight: 4, width: 60, textAlign: 'right' }}
                      onBlur={(e) =>
                        handleChangeItem({
                          record,
                          type: 'quantity',
                          editType: 'isUpdateOrderQty',
                          value: e.target.value || 0,
                        })
                      }
                      step={1}
                      onFocus={(e) => e.target.select()}
                      readOnly={addingCartItem}
                      disabled={record.status === 'SHIPPED' || readOnly}
                    />
                  </Flex>
                </>
              </Flex>
              {draggable && !readOnly && (
                <div className="add-new-row">
                  <div style={{ position: 'relative' }}>
                    <Icon
                      className="icon-add-row"
                      type="plus-circle"
                      style={{ width: 20, height: 20 }}
                      onClick={onClickAddItem.bind(this, record.wholesaleOrderItemId)}
                    />
                  </div>
                </div>
              )}
            </div>
          )
        },
      },
      {
        title: 'Item',
        dataIndex: 'variety',
        width: 350,
        render: (variety: string, record: any, index: number) => {
          const editableStatus = record.status != 'SHIPPED' && record.status != 'CANCEL'
          const redirectPath =
            window.location.origin +
            window.location.pathname +
            `#/product/${record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`
          return (
            <div style={{ minWidth: 250, width: '100%' }} className="name-column">
              <Flex className="space-between">
                <Flex style={{ paddingLeft: 8, display: record.visibleDropdown ? 'none' : 'flex' }}>
                  <span className="product-name">
                    <a href={`${redirectPath}`}>
                      {formatItemDescription(variety, record.SKU, sellerSetting, record.customerProductCode)}
                    </a>
                  </span>
                  <div>
                    <Tooltip placement="top" title={'Edit item name for display'}>
                      <IconSVG
                        onClick={openModal.bind(this, 'openEditItemNameModal', record)}
                        className={`icon-edit-item-name ${record.editedItemName ? 'icon-green-permanent' : ''}`}
                        type="edit"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          marginTop: 2,
                          fill: 'none',
                        }}
                      />
                    </Tooltip>
                  </div>
                  <div>
                    <Tooltip placement="top" title={'Note added'}>
                      <IconSVG
                        onClick={openModal.bind(this, 'addNoteModal', record)}
                        type="add-note"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          marginTop: 2,
                          display: record.note && record.note != 'null' ? 'block' : 'none',
                          fill: 'none',
                        }}
                      />
                    </Tooltip>
                  </div>
                  <div>
                    <Tooltip placement="top" title={'Work order added'}>
                      <IconSVG
                        type="add-wo"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          display: record.workOrderId ? 'block' : 'none',
                          cursor: 'normal',
                        }}
                        onClick={() => {
                          history.push(`/manufacturing/${record.workOrderId}/wo-overview`)
                        }}
                      />
                    </Tooltip>
                  </div>
                </Flex>
                <div className="item-name-wrapper" style={{ alignSelf: 'center' }}>
                  {editableStatus && !record.visibleDropdown && (
                    <div style={{ width: 20, height: 20 }}>
                      <Icon
                        type="caret-down"
                        viewBox="0 0 20 20"
                        className="replace-item-handler"
                        style={{ ...ItemHMargin4, cursor: 'pointer', width: 20, height: 20 }}
                        onClick={(e) => {
                          if (e.isTrusted) {
                            localStorage.setItem('trigger-by-keyboard', '0')
                            localStorage.setItem('editing-row-key', index.toString())
                          }
                          onClickReplaceItem(record.wholesaleOrderItemId)
                        }}
                      />
                    </div>
                  )}
                </div>
              </Flex>
              {record.editedItemName && <div className="edited-item-name pl8">{record.editedItemName}</div>}
              {record.visibleDropdown && (
                <Flex style={{ display: record.visibleDropdown ? 'flex' : 'none' }}>
                  <ReplaceItem {...props} current={record} hideReplaceDropdown={hideReplaceDropdown} />
                </Flex>
              )}
            </div>
          )
        },
      },
      {
        title: 'UOM',
        dataIndex: 'UOM',
        width: 150,
        render: (uom: string, record: any, index: number) => {
          return (
            <>
              <UomSelect
                readOnly={addingCartItem}
                disabled={record.status === 'SHIPPED' || readOnly}
                orderItem={record}
                handlerChangeUom={(orderItemId: any, value: any) => {
                  const row = data.find((el: any) => el.wholesaleOrderItemId == orderItemId)
                  if (row) {
                    handleChangeItem({ record: row, type: 'UOM', editType: 'isUpdateUOM', value })
                  }
                }}
                type={1}
                parent="cart-order-qty"
                isFreshGreen={isFreshGreen}
              />
            </>
          )
        },
      },
    ])
    setNonFGColumns([
      {
        title: '',
      },
      {
        title:
          draggable && !readOnly ? (
            <>
              <ThemeCheckbox
                style={{ width: 30 }}
                onChange={onToggleSelect.bind(this, 'All')}
                checked={data.length && selectedRows.length == data.length}
              />
              Item
            </>
          ) : (
            'Item'
          ),
        dataIndex: 'variety',
        width: 350,
        render: (variety: string, record: any, index: number) => {
          const editableStatus = record.status != 'SHIPPED' && record.status != 'CANCEL'
          const redirectPath =
            window.location.origin +
            window.location.pathname +
            `#/product/${record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`

          return (
            <>
              <div className="name-column" style={{ minWidth: 200 }}>
                <Flex className="space-between">
                  <Flex style={{ paddingLeft: 8, display: record.visibleDropdown ? 'none' : 'flex' }}>
                    <span
                      className={draggable && !readOnly ? 'row-index-number' : 'w30'}
                      style={{
                        visibility:
                          draggable && !readOnly && selectedRows.includes(record.displayOrderProduct)
                            ? 'hidden'
                            : 'visible',
                      }}
                    >
                      {draggable === false ? index + 1 : record.displayOrderProduct + 1}
                    </span>
                    <div>
                      <div style={{ flex: 1 }}>
                        {draggable && !readOnly && (
                          <ThemeCheckbox
                            className="row-selection"
                            style={{
                              display: selectedRows.includes(record.displayOrderProduct) ? 'block' : 'none',
                              position: 'absolute',
                              marginLeft: -24,
                            }}
                            onChange={onToggleSelect.bind(this, record.displayOrderProduct)}
                            checked={selectedRows.includes(record.displayOrderProduct)}
                          />
                        )}
                        <span className="product-name">
                          <a href={`${redirectPath}`}>
                            {formatItemDescription(variety, record.SKU, sellerSetting, record.customerProductCode)}
                          </a>
                        </span>
                      </div>
                      {record.editedItemName && <div className="edited-item-name">{record.editedItemName}</div>}
                    </div>
                    <div>
                      <Tooltip placement="top" title={'Edit item name for display'}>
                        <IconSVG
                          onClick={openModal.bind(this, 'openEditItemNameModal', record)}
                          className={`icon-edit-item-name ${record.editedItemName ? 'icon-green-permanent' : ''}`}
                          type="edit"
                          viewBox="0 0 20 20"
                          width={20}
                          height={20}
                          style={{
                            ...ItemHMargin4,
                            marginTop: 2,
                            fill: 'none',
                          }}
                        />
                      </Tooltip>
                    </div>
                    <div>
                      <Tooltip placement="top" title={'Note added'}>
                        <IconSVG
                          onClick={openModal.bind(this, 'addNoteModal', record)}
                          type="add-note"
                          viewBox="0 0 20 20"
                          width={20}
                          height={20}
                          style={{
                            ...ItemHMargin4,
                            marginTop: 2,
                            display: record.note && record.note != 'null' ? 'block' : 'none',
                            fill: 'none',
                          }}
                        />
                      </Tooltip>
                    </div>
                    <div>
                      <Tooltip placement="top" title={'Work order added'}>
                        <IconSVG
                          type="add-wo"
                          viewBox="0 0 20 20"
                          width={20}
                          height={20}
                          style={{ ...ItemHMargin4, display: record.workOrderId ? 'block' : 'none', cursor: 'normal' }}
                          onClick={() => {
                            history.push(`/manufacturing/${record.workOrderId}/wo-overview`)
                          }}
                        />
                      </Tooltip>
                    </div>
                  </Flex>
                  <div style={{ alignSelf: 'center' }}>
                    {editableStatus && !record.visibleDropdown && (
                      <div style={{ width: 20, height: 20 }}>
                        <Icon
                          type="caret-down"
                          viewBox="0 0 20 20"
                          className="replace-item-handler"
                          style={{ ...ItemHMargin4, cursor: 'pointer', width: 20, height: 20 }}
                          onClick={(e) => {
                            if (e.isTrusted) {
                              localStorage.setItem('trigger-by-keyboard', '0')
                              localStorage.setItem('editing-row-key', index.toString())
                            }
                            onClickReplaceItem(record.wholesaleOrderItemId)
                          }}
                        />
                      </div>
                    )}
                  </div>
                </Flex>

                {record.visibleDropdown && (
                  <Flex style={{ paddingLeft: 24, display: record.visibleDropdown ? 'flex' : 'none' }}>
                    <ReplaceItem {...props} current={record} hideReplaceDropdown={hideReplaceDropdown} />
                  </Flex>
                )}
              </div>
              {draggable && !readOnly && (
                <div className="add-new-row">
                  <div style={{ position: 'relative' }}>
                    <Icon
                      className="icon-add-row"
                      type="plus-circle"
                      style={{ width: 20, height: 20 }}
                      onClick={onClickAddItem.bind(this, record.wholesaleOrderItemId)}
                    />
                  </div>
                </div>
              )}
            </>
          )
        },
      },
      {
        title: 'Brand',
        dataIndex: 'modifiers',
        width: 130,
        render: (modifiers: string, record: any) => {
          return _.get(record, 'lotIds.length', 0) ? modifiers : ''
        },
      },
      {
        title: 'Origin',
        dataIndex: 'extraOrigin',
        width: 130,
        render: (extraOrigin: string, record: any) => {
          return _.get(record, 'lotIds.length', 0) ? extraOrigin : ''
        },
      },
      {
        title: 'Order Qty',
        dataIndex: 'quantity',
        width: 180,
        render: (quantity: number, record: any, index: number) => {
          return (
            <Flex className="v-center order-quantity-input">
              <ThemeInputNumber
                className={`order-qty no-stepper`}
                value={quantity}
                style={{ marginRight: 4, width: 60, textAlign: 'right' }}
                onBlur={(e) =>
                  handleChangeItem({
                    record,
                    type: 'quantity',
                    editType: 'isUpdateOrderQty',
                    value: e.target.value || 0,
                  })
                }
                step={1}
                precision={2}
                formatter={(v) => v.match(/^\d*(\.?\d{0,2})/g)[0]}
                onFocus={(e) => e.target.select()}
                readOnly={addingCartItem}
                disabled={record.status === 'SHIPPED' || readOnly}
              />
              <UomSelect
                readOnly={addingCartItem}
                disabled={record.status === 'SHIPPED' || readOnly}
                orderItem={record}
                handlerChangeUom={(orderItemId: any, value: any) => {
                  const row = data.find((el: any) => el.wholesaleOrderItemId == orderItemId)
                  if (row) {
                    handleChangeItem({ record: row, type: 'UOM', editType: 'isUpdateUOM', value })
                  }
                }}
                type={1}
                isFreshGreen={isFreshGreen}
              />
            </Flex>
          )
        },
      },
      {
        title: 'Lot',
        dataIndex: 'lotIds',
        width: 180,
        render: (lotIds: string[], record: any, index: number) => {
          // lot pop up display number
          if (dataSource && dataSource[index] && dataSource[index].items) {
            const notSelect = dataSource[index].items.find((item: any) => {
              if (item.lotId == undefined) {
                return item
              }
            })
            if (notSelect && (notSelect.quantity > 0 || notSelect.picked > 0)) {
              index = lotIds.indexOf('No lot selected')
              if (index == -1) {
                lotIds.push('No lot selected')
              }
              if (lotIds[0] == null) {
                lotIds.splice(0, 1)
              }
            } else {
              index = lotIds.indexOf('No lot selected')
              if (index !== -1) {
                lotIds.splice(index, 1)
              }
            }
          }
          let nullIndex = -1
          if (lotIds) {
            lotIds.forEach((item, index) => {
              if (item == null) {
                nullIndex = index
              }
            })
            if (nullIndex > 0) {
              lotIds.splice(nullIndex, 1)
            }
          }
          const lotAvailableQtys = record.items ? record.items.map((v: any) => v.lotAvailableQty) : []
          const cond = lotAvailableQtys.some((v: number) => v < 0)
          const lotButton = (
            <CustomButton
              disabled={record.status === 'SHIPPED' || readOnly}
              onClick={openModal.bind(this, 'lotsModal', record)}
              className={`${cond ? 'warning' : ''}`}
              style={{ width: '100%', height: '100%' , color: gray01}}
            >
              {!_.compact(lotIds).length ? (
                'No lot selected'
              ) : (
                <span>
                  {lotIds[0]} {lotIds.length > 1 && `+${lotIds.length - 1}`}
                </span>
              )}
            </CustomButton>
          )
          const lotBtn = props.updateOrderQuantityLoading.lotLoading ? (
            <Spin />
          ) : record.quantity > 0 || record.picked > 0 ? (
            lotButton
          ) : (
            <Button style={{ color: 'white', width: '100%' }}>empty</Button>
          )

          if (cond) {
            return (
              <Tooltip placement="top" title="The ordered quantity is greater than the selected lot inventory.">
                {lotBtn}
              </Tooltip>
            )
          }
          if (_.get(lotIds, 'length', 0) < 2) return lotBtn
          return (
            <Tooltip placement="top" title={lotIds.filter((lot) => lot !== '').join(',')}>
              {lotBtn}
            </Tooltip>
          )
        },
      },
    ])
    setCommonColumns([
      {
        title: 'Price',
        dataIndex: 'price',
        width: 220,
        render: (value: number, record: any) => {
          let price = value
          const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          let cost = record.lotCost
          if (_.isNumber(price)) {
            price = basePriceToRatioPrice(pricingUOM, value, record)
          }

          if (_.isNumber(cost)) {
            cost = basePriceToRatioPrice(pricingUOM, cost + record.perAllocateCost, record)
          }
          let tooltipText =
            _.isNumber(value) && price > 0 && price < cost
              ? `Sale price is below cost ($${formatNumber(cost, 2)}/${pricingUOM})`
              : ''
          tooltipText = price == 0 ? 'Sales price is zero' : tooltipText
          const warningClass = tooltipText ? 'warning' : ''

          return (
            <Flex className="v-center">
              <Tooltip placement="top" title={tooltipText}>
                <div style={{ width: 85 }}>
                  <ThemeInputNumber
                    className={`no-stepper cart-price ${warningClass} ${record.status === 'SHIPPED' ? 'readonly' : ''}`}
                    style={{ textAlign: 'right' }}
                    step={0.01}
                    value={price}
                    precision={2}
                    onBlur={(e) =>
                      handleChangeItemPrice({ record, type: 'price', price: e.target.value.replace('$', '') })
                    }
                    formatter={(value) => `$${value}`}
                    onFocus={(e) => e.target.select()}
                    parser={(value: string | undefined) => (value ? value.replace('$', '') : '')}
                    readOnly={addingCartItem}
                    disabled={record.status === 'SHIPPED' || readOnly} //don't need to addingCartItem flag because wholesaleOrderItem is is empty when order item is created
                  />
                </div>
              </Tooltip>
              <Unit style={{ flex: 1, minWidth: 50 }}>/{pricingUOM}</Unit>
              {props.updateOrderQuantityLoading.lotLoading || props.updateOrderQuantityLoading.fetching ? (
                <></>
              ) : (
                <>
                  <Icon
                    type="bar-chart"
                    style={{ fontSize: 20, marginRight: 5 }}
                    className="price-setting"
                    onClick={() => openModal('priceModal', record)}
                  />
                  {!readOnly && (
                    <Icon
                      style={{}}
                      type="setting"
                      className="price-setting"
                      onClick={() => {
                        openModal('itemPriceModal', record)
                      }}
                    />
                  )}
                </>
              )}
            </Flex>
          )
        },
      },
      {
        title: 'Subtotal', //title: 'Total Price',
        dataIndex: 'totalPrice',
        // width: 150,
        render: (data: number, record: any) => {
          
          return (
            <ThemeSpin spinning={record.rowLoading === true ? true : false}>
              <ThemeInputNumber
                className={`no-stepper ${record.status === 'SHIPPED' ? 'readonly' : ''}`}
                style={{ textAlign: 'right', minWidth: 85 }}
                step={0.01}
                value={getSubtotal(record)}
                precision={2}
                onFocus={(e) => e.target.select()}
                onBlur={(evt) => updatePriceFromTotal(evt, record)}
                formatter={(value) => `$${value}`}
                parser={(value: string | undefined) => (value ? value.replace('$', '') : '')}
                readOnly={addingCartItem}
                disabled={
                  record.status === 'SHIPPED' ||
                  readOnly ||
                  (!record.oldCatchWeight && !record.catchWeightQty && record.catchWeightQty == 0)
                }
              />
            </ThemeSpin>
          )
        },
      },
      {
        title: 'Status',
        dataIndex: 'status',
        render: (status: string, record: any) => {
          const statues = ['New', 'Picking', 'Shipped']
          if (isDisablePickingStep) {
            statues.splice(1, 1)
          }

          let formattedStatus = status
          if (status === 'PLACED') {
            formattedStatus = 'NEW'
          }
          if (isDisablePickingStep && status === 'PICKING') {
            formattedStatus = 'NEW'
          }

          return (
            <div className="select-container-parent" style={{ width: 100 }}>
              {record.status != 'SHIPPED' ? (
                <ThemeSelect
                  onChange={(val: any) => onSelectChange(record, record.wholesaleOrderItemId, 'status', val)}
                  className={`status-selection orderItemId-${record.wholesaleOrderItemId}`}
                  value={formattedStatus}
                  suffixIcon={<Icon type="caret-down" />}
                  style={{ width: '100%', borderRadius: 20 }}
                  disabled={record.status === 'SHIPPED' || readOnly}
                  dropdownRender={(el: any) => {
                    return <div className="sales-cart-status-selector">{el}</div>
                  }}
                >
                  {statues.map((el: string, index: number) => {
                    return (
                      <Select.Option value={el.toUpperCase()} key={index} disabled={el.toUpperCase() == 'SHIPPED'}>
                        <Flex className="v-center">
                          <div className={`status-indicator ${el.toLowerCase()}`} />
                          <div className="value">{el == 'Picking' ? 'Picked' : el}</div>
                        </Flex>
                      </Select.Option>
                    )
                  })}
                </ThemeSelect>
              ) : (
                <div className="status-selection not-selector">
                  <Flex className="v-center">
                    <div className={`status-indicator shipped`} />
                    <div className="value">Shipped</div>
                  </Flex>
                </div>
              )}
            </div>
          )
        },
      },
    ])
    setConfigurableOption([
      {
        title: (
          <>
            Picked Qty
            {sellerSetting && sellerSetting.company && !sellerSetting.company.warehousePickEnabled && !isShipped && (
              <Popconfirm
                title={
                  <div style={{ width: 200 }}>
                    Auto-fill picked quantities to match the order quantities (non-catchweight items only).
                  </div>
                }
                okText="Auto-fill quantities"
                onConfirm={() => confirmCopyOrderQty()}
              >
                <Icon type="copy" style={ItemHMargin4} className="copy-order-qty" />
              </Popconfirm>
            )}
          </>
        ),
        dataIndex: 'picked',
        width: 150,
        render: (picked: number, record: any) => {
          let tooltipText = ''
          if (record.picked !== null && record.picked > 0 && record.quantity !== null && record.quantity >= 0) {
            if (parseFloat(record.picked) > parseFloat(record.quantity)) {
              tooltipText = `Overpicked by ${record.picked - record.quantity} ${record.UOM}`
            } else if (record.picked < record.quantity) {
              tooltipText = `Short by ${(parseFloat(record.quantity) - parseFloat(record.picked)).toFixed(2)} ${
                record.UOM
              }`
            }
          }
          const warningClass = tooltipText ? 'warning' : ''
          let showCatchWeightModal = false
          let constantRatio = judgeConstantRatio(record)
          let customBtn = null
          const parsingFailed = false
          if (!constantRatio) {
            showCatchWeightModal = true
            customBtn = (
              <CustomButton
                style={{ width: 70, textAlign: 'right', paddingRight: 11, flex: 'unset' }}
                className={`${record.picked ? '' : 'empty'} ${warningClass} ${parsingFailed ? 'warning' : ''}`}
                onClick={openModal.bind(this, 'pickedQuantityModal', record)}
                disabled={record.status === 'SHIPPED' || readOnly}
              >
                {record.picked ? record.picked : <span>&nbsp;</span>}
              </CustomButton>
            )
          }

          if (showCatchWeightModal && parsingFailed) {
            if (tooltipText) {
              tooltipText += ', '
            }
            tooltipText += 'Scanning errors found!'
          }
          if (sellerSetting && sellerSetting.company && sellerSetting.company.warehousePickEnabled) {
            return (
              <Flex className="v-center">
                <div>
                  {picked === null ||
                  typeof picked == 'undefined' ||
                  (picked == 0 && (record.status == 'PLACED' || record.status == 'NEW'))
                    ? ''
                    : picked}
                </div>
                <Unit>{record.UOM}</Unit>
                {tooltipText && (
                  <Tooltip placement="top" title={tooltipText}>
                    <div className="anticon-warning">
                      <Icon type="warning" />
                    </div>
                  </Tooltip>
                )}
              </Flex>
            )
          } else {
            return (
              <Flex className="v-center">
                <Tooltip placement="top" title={tooltipText}>
                  <div style={{ width: 70 }}>
                    {showCatchWeightModal ? (
                      customBtn
                    ) : (
                      <ThemeInputNumber
                        disabled={record.status === 'SHIPPED' || readOnly || updating}
                        readOnly={addingCartItem}
                        style={{ textAlign: 'right', marginRight: 4 }}
                        className={`${warningClass} no-stepper`}
                        step={1}
                        value={
                          picked === null ||
                          typeof picked == 'undefined' ||
                          (picked == 0 && (record.status == 'PLACED' || record.status == 'NEW'))
                            ? ''
                            : picked
                        }
                        onBlur={(e) =>
                          handleChangeItem({
                            record,
                            type: 'picked',
                            editType: 'isUpdatePickedQty',
                            value: e.target.value || 0,
                          })
                        }
                        formatter={(v) => v.match(/^\d*(\.?\d{0,2})/g)[0]}
                        onFocus={(e) => e.target.select()}
                      />
                    )}
                  </div>
                </Tooltip>
                <Unit>{record.UOM}</Unit>
              </Flex>
            )
          }
        },
      },
      {
        title: 'Billable Qty',
        dataIndex: 'catchWeightQty',
        width: 150,
        render: (weight: number, record: any) => {
          let showBillableQty,
            showValue = false
          if (record.picked || record.catchWeightQty) {
            showValue = true
          }

          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM

          let constantRatio = judgeConstantRatio(record)
          if (record.oldCatchWeight) {
            showBillableQty = record.catchWeightQty
          } else {
            if (!constantRatio) {
              showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
            } else {
              showBillableQty = inventoryQtyToRatioQty(
                pricingUOM,
                ratioQtyToInventoryQty(unitUOM, record.picked || 0, record, 12),
                record,
                2
              )
            }
          }

          return (
            <Flex className="v-center">
              <div style={{ textAlign: 'right', minWidth: 60 , color: gray01}}>
                {showValue ? mathRoundFun(showBillableQty, 4) : ''}
              </div>
              <Unit style={{ flex: 'unset' }}>{pricingUOM}</Unit>
            </Flex>
          )
        },
      },
    ])
    initializeColumns()
  }, [
    selectedRows,
    data,
    JSON.stringify(catchWeightValues),
    draggable,
    currentPage,
    props.currentOrder.wholesaleOrderStatus,
    props.saleItems,
    props.simplifyItems,
  ])

  useEffect(() => {
    initializeColumns(true)
  }, [isFreshGreen])

  useEffect(() => {
    if (hasFooter) {
      setFooter(isFreshGreen ? <FGFooter {...props} /> : <GeneralFooter {...props} />)
    } else {
      setFooter(null)
    }
  }, [hasFooter, isFreshGreen, props])

  useEffect(() => {
    const initialCols = initialColumn.map((col, index) => {
      if (col.width) {
        return {
          ...col,
          width: col.width,
          onHeaderCell: (column: any) => ({
            width: column.width,
            onResize: handleResize(index),
          }),
        }
      }
      return { ...col }
    })

    setFinalTableColumns(initialCols)
  }, [initialColumn])

  useEffect(() => {
    initializeColumns()
  }, [isDisablePickingStep])

  useEffect(() => {
    let curPage = currentPage
    const pages = Math.ceil(dataSource.length / pageSize)
    if (pages > 1 && dataSource.length < data.length && dataSource.length % pageSize === 0) {
      // when deleting an order item, if the pages is decreased
      curPage = pages
      setCurrentPage(curPage)
    }
    const isNewPage = dataSource.length === data.length + 1 && dataSource.length > pageSize && curPage < pages
    const newData: any = []
    dataSource.forEach((item: any) => {
      item.quantity = typeof item.quantity == 'number' ? item.quantity.toFixed(2) : item.quantity
      item.picked = typeof item.picked == 'number' ? item.picked.toFixed(2) : item.picked
      newData.push(item)
    })

    setData(newData)
    if (isNewPage) {
      setTimeout(() => {
        jQuery('.ant-pagination-item')
          .last()
          .trigger('click')
      }, 100)
      setTimeout(() => {
        if (!isFreshGreen) {
          jQuery('.order-quantity-input input')
            .last()
            .trigger('focus')
        } else {
          const uomParent = jQuery('.uom-select-container')
            .last()
            .parent()

          jQuery(uomParent).addClass('custom-ant-select-focused')
        }
      }, 1500)
    }
  }, [dataSource])

  useEffect(() => {
    if (data.length == dataSource.length && hoverIndex > -1 && dragIndex > -1) {
      setDragIndex(-1)
      setHoverIndex(-1)
    }
  }, [JSON.stringify(data)])

  useEffect(() => {
    if (!props.updateOrderQuantityLoading.fetching && request.length) {
      props.updateOrderQuantityLoading.fetching = true
      if (request[0].isUpdatePrice) {
        const data = { ...request[0], isUpdatePrice: undefined }
        updateOrderItemPrice(data)
      } else {
        updateOrderItemByProductId(request[0])
      }
      setRequest(request.slice(1))
    }
  }, [props.updateOrderQuantityLoading.fetching])

  /* End useEffect hooks */

  /* Implement sales logic */
  const confirmCopyOrderQty = () => {
    props.startUpdating()
    props.updateAutofillPicked({ orderId: props.currentOrder.wholesaleOrderId })
  }

  const hideReplaceDropdown = (orderItemId: number) => {
    let cloneData = _.cloneDeep(data)
    _.find(cloneData, (el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el['visibleDropdown'] = false
      }
    })
    setData(cloneData)
  }

  const getSubtotal = (record: OrderItem) => {
    let total = 0
    let constantRatio = judgeConstantRatio(record)
    let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
    // let recordPrice = props.companyName === 'Pay-Less Logistics, Inc.' ? _.toNumber(record.price.toFixed(2)) : record.price
    let recordPrice = record.price

    if (record.oldCatchWeight) {
      recordPrice = mathRoundFun(recordPrice, 2)
      total = numberMultipy(recordPrice, record.catchWeightQty)
    } else {
      if (!constantRatio) {
        let ratioPrice = basePriceToRatioPrice(pricingUOM, recordPrice, record, 2)
        total = numberMultipy(ratioPrice, inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record, 2))
      } else {
        recordPrice = basePriceToRatioPrice(pricingUOM, recordPrice, record)
        const catchWeight = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
        total = numberMultipy(recordPrice, catchWeight)
      }
    }
    return total
  }

  const handleChangeItem = ({ record, type, editType, value }: any) => {
    setCurDisplayOrder(record.displayOrderProduct)
    if (isShipped) {
      return
    }

    if (record[type] == value) return
    if (value === null) return
    if ((type == 'quantity' || type == 'picked') && parseFloat(record[type]) == parseFloat(value)) {
      if (!(type == 'picked' && parseFloat(record[type]) == 0 && parseFloat(value) == 0)) {
        return
      }
    }

    if (type === 'quantity' && props.pageTotalInfo.totalQuantity) {
      props.pageTotalInfo.totalQuantity = props.pageTotalInfo.totalQuantity - Number(record[type]) + Number(value)
    }

    const recordPrice = record.price
    if (type === 'quantity' || type === 'picked') {
      props.updateOrderQuantityLoading.lotLoading = true
      if (record.items.length > 1) {
        props.updateOrderQuantityLoading.loading = true
      }
      if (isDisablePickingStep) {
        props.orderItemByProduct[record.displayOrderProduct]['picked'] = value
      }
    }
    if (type === 'picked' || (isDisablePickingStep && type === 'quantity')) {
      props.orderItemByProduct[record.displayOrderProduct]['catchWeightQty'] = value
      props.orderItemByProduct[record.displayOrderProduct]['status'] = 'PICKING'
      if (props.pageTotalInfo.totalPicked) {
        props.pageTotalInfo.totalPicked = props.pageTotalInfo.totalPicked - Number(record[type]) + Number(value)
      }
    }
    props.orderItemByProduct[record.displayOrderProduct][type] = value

    if (type === 'UOM') {
      handlerUpdateSOUOMRedux({
        displayOrder: record.displayOrderProduct,
        pricingUOM: value,
      })
    }

    let status = record.status
    let lotAssignmentMethod = record.lotAssignmentMethod
    if (type === 'picked') {
      status = 'PICKING'
    }

    let enableLotOverflowValue = record.lotAssignmentMethod == 3 ? false : record.enableLotOverflow

    if (type === 'quantity' && record.lotAssignmentMethod === 3) {
      props.orderItemByProduct[record.displayOrderProduct]['lotAssignmentMethod'] = 1
      lotAssignmentMethod = 1
    }

    console.log(type, value)

    const data = {
      orderId: props.currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod,
        enableLotOverflow: enableLotOverflowValue,
        wholesaleItemId: record.itemId,
        picked: isDisablePickingStep && type === 'quantity' ? value : record.picked,
        quantity: record.quantity,
        [type]: value === '' ? 0 : value,
        [editType]: true,
        price: recordPrice,
        pricingUOM: type === 'UOM' ? value : record.pricingUOM,
        status,
      },
    }
    if (props.updateOrderQuantityLoading.fetching && curDisplayOrder === record.displayOrderProduct) {
      return setRequest([...request, data])
    }

    props.updateOrderQuantityLoading.fetching = true
    updateOrderItemByProductId(data)
  }

  const handleChangeItemPrice = ({ record, type, price }: any) => {
    setCurDisplayOrder(record.displayOrderProduct)
    if (isShipped) {
      return
    }
    if (formatNumber(record[type], 8) == formatNumber(ratioPriceToBasePrice(record.pricingUOM, price, record, 12), 8)) return

    props.pageTotalInfo.totalPrice = Number(props.pageTotalInfo.totalPrice) - Number(getSubtotal(record)) + Number(getSubtotal({ ...record, price: ratioPriceToBasePrice(record.pricingUOM, price, record, 12) }))

    handlerUpdateSOUOMRedux({
      displayOrder: record.displayOrderProduct,
      price: ratioPriceToBasePrice(record.pricingUOM, price, record, 12),
    })

    if (record.items.length > 1) {
      props.updateOrderQuantityLoading.loading = true
    }



    const data = {
      isUpdatePrice: true,
      orderId: props.currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod: record.lotAssignmentMethod,
        enableLotOverflow: record.enableLotOverflow,
        wholesaleItemId: record.itemId,
        pricingLogic: 'CUSTOM_PRICE',
        pricingGroup: record.pricingGroup,
        pricingUOM: record.pricingUOM,
        price,
      },
    }

    if (props.updateOrderQuantityLoading.fetching && curDisplayOrder === record.displayOrderProduct) {
      return setRequest([...request, data])
    }

    props.updateOrderQuantityLoading.fetching = true
    updateOrderItemPrice(data)
  }

  const initializeColumns = (isCompanyChanged = false) => {
    let common: any[] = [...commonColumns]
    if (isDisablePickingStep === false) {
      common.splice(1, 0, ...configurableOption)
    }
    let cols = isFreshGreen ? [...freshGreenColumns, ...common] : [...nonFGColumns, ...common]
    if (initialColumn.length) {
      cols = cols.map((el, index) => {
        return {
          ...el,
          width: isCompanyChanged ? el.width : initialColumn[index]?.width,
        }
      })
    }

    setInitinalColumn(cols)
  }

  const onTablePageChange = (page: number) => {
    console.log(page)
    props.setSalesOrderPage(page - 1)
  }

  const onSelectChange = (record: any, orderItemId: string, type: string, value: any) => {
    if (isShipped) {
      return
    }
    let cloneData = _.cloneDeep(data)
    if (type == 'status' && value == 'NEW') {
      value = 'PLACED'
    }

    if (record.oldCatchWeight) {
      notify('warn', 'Warn', 'This data cannot be operated, please contact the system administrator')
      return
    }
    setData(cloneData)

    handleChangeItem({ record, type: 'status', editType: 'isUpdateStatus', value })
  }

  const onClickReplaceItem = (orderItemId: number) => {
    let cloneData = _.cloneDeep(data)
    _.find(cloneData, (el: any) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el['visibleDropdown'] = true
      } else {
        el['visibleDropdown'] = false
      }
    })
    setData(cloneData)
  }

  const updatePriceFromTotal = (evt: any, record: any) => {
    let total = 0
    let price = 0
    const strVal = evt.target.value.replace('$', '').trim()
    if (strVal !== '') {
      total = parseFloat(strVal)
    }

    let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM

    if (record.oldCatchWeight) {
      price = total / record.catchWeightQty
    } else {
      price = total / inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record, 12)
    }
    price = mathRoundFun(price, 12)
    handleChangeItemPrice({ record, type: 'price', price })
  }
  /* End implement sales logic */

  /* Render table */
  // Render no-draggable-table when it is preview mode
  if (props.draggable === false) {
    const filteredItems = data.filter((el: any) => {
      if (el.quantity > 0) {
        return true
      }

      let pickedQty = 0
      const constantRatio = judgeConstantRatio(el)
      if (constantRatio === false) {
        const recordWeights = catchWeightValues[el.wholesaleOrderItemId]
        pickedQty = recordWeights ? recordWeights.length : 0
      } else {
        pickedQty = el.picked
      }

      if (pickedQty > 0) {
        return true
      }

      return false
    })

    return (
      <ThemeSpin spinning={updating}>
        <Table
          className={isFreshGreen ? 'fresh-green' : 'other'}
          pagination={false}
          dataSource={filteredItems}
          columns={finalTableColumns}
          rowKey={rowKey}
          components={noDragComponents}
          footer={() => (readOnly ? null : footer)}
          onRow={(record: OrderItem, index: number) => ({
            index,
            record,
            tableHoverIndex: hoverIndex,
            wholesaleOrderItemId: record.wholesaleOrderItemId,
            selectedRows: selectedRows,
            renderFloatingMenu: renderFloatingMenu,
          })}
        />
      </ThemeSpin>
    )
  }

  // Render no draggable component when it is preview mode
  if (readOnly) {
    return (
      <ThemeSpin spinning={updating}>
        <Table
          className={isFreshGreen ? 'fresh-green' : 'other'}
          pagination={{ pageSize, hideOnSinglePage: true }}
          dataSource={data}
          columns={finalTableColumns}
          rowKey={rowKey}
          components={noDragComponents}
          footer={() => null}
          onRow={(record: OrderItem, index: number) => ({
            index,
            record,
            tableHoverIndex: hoverIndex,
            wholesaleOrderItemId: record.wholesaleOrderItemId,
            selectedRows: selectedRows,
            renderFloatingMenu: renderFloatingMenu,
          })}
        />
      </ThemeSpin>
    )
  }

  // Render table as default
  return (
    <ThemeSpin spinning={props.getOrderItemsLoading || updating || props.simplifyItems.length === 0 || props.updateOrderQuantityLoading.loading}>
      <DndProvider backend={HTML5Backend}>
        <Table
          className={`${hoverIndex > -1 ? 'drag-table' : ''} ${isFreshGreen ? 'fresh-green' : 'other'}`}
          pagination={{ pageSize, hideOnSinglePage: true, onChange: onTablePageChange, current: currentPage, total }}
          dataSource={data}
          columns={finalTableColumns}
          rowKey={rowKey}
          components={components}
          footer={() => footer}
          onRow={(record: OrderItem, index: number) => ({
            index,
            record,
            tableHoverIndex: hoverIndex,
            displayOrder: record.displayOrderProduct,
            itemId: record.itemId,
            wholesaleOrderItemId: record.wholesaleOrderItemId,
            selectedRows: selectedRows,
            renderFloatingMenu: renderFloatingMenu,
            moveRow,
            dropRow,
            updateSortOrderItemDisplayOrder,
          })}
        />
      </DndProvider>
    </ThemeSpin>
  )
  /*End render table */
}
