import * as React from 'react'
import { Button, Dropdown, Icon, Menu, Tooltip } from 'antd'
import {
  CustomDropdownWrapper,
  FlexDiv,
  SelectedDropdownItem,
} from './../../styles'
import { OrderItem } from '~/schema'
import { ThemeIcon } from '~/modules/customers/customers.style'
import { Icon as IconSVG } from '~/components'

type CustomDropdownProps = {
  orderItems: OrderItem[]
  productIds: string
  index: number
  onSelectProduct: Function
  palletLabels: any[]
}

export class CustomDropdown extends React.PureComponent<CustomDropdownProps> {
  state = {
    productIdArr: []
  }

  componentDidMount() {
    this.initProductNames(this.props.productIds)
  }

  componentWillReceiveProps(nextProps: CustomDropdownProps) {
    if (this.props.productIds != nextProps.productIds) {
      this.initProductNames(nextProps.productIds)
    }
  }

  initProductNames = (productIds: string) => {
    const { orderItems } = this.props
    const idArr = productIds ? productIds.split(';') : []
    let productIdArr: any = []
    idArr.forEach((orderItemId: any) => {
      const orderItem = orderItems.find(el => el.wholesaleOrderItemId == orderItemId)
      if (orderItem) {
        productIdArr.push(orderItem.wholesaleOrderItemId)
      }
    });
    this.setState({ productIdArr })
  }

  renderProductNames = () => {
    const { index, orderItems } = this.props
    const { productIdArr } = this.state
    let result: any = []
    productIdArr.forEach((id: any) => {
      const orderItem = orderItems.find(el => el.wholesaleOrderItemId == id)
      if (orderItem) {
        result.push(
          <SelectedDropdownItem key={id}>
            {orderItem.variety}
            <ThemeIcon type="close" onClick={() => this.props.onSelectProduct(id, index)} />
          </SelectedDropdownItem>)
      }
    });
    return result
  }

  getAddedPalletLabelsCount = (orderItemId: number) => {
    const { palletLabels } = this.props
    const allProductIdArr: any[] = palletLabels.map(el => { return el.productIds })
    let count = 0
    allProductIdArr.forEach(element => {
      const idArr = element ? element.split(';') : []
      if (idArr.indexOf(orderItemId.toString()) > -1) {
        count++
      }
    });
    return count
  }

  renderAvailableProducts = () => {
    const { orderItems, index } = this.props
    const { productIdArr } = this.state

    return (
      <Menu>
        {orderItems.map((item) => (
          <Menu.Item key={item.wholesaleOrderItemId} onClick={() => this.props.onSelectProduct(item.wholesaleOrderItemId, index)}>
            <FlexDiv className='space-between'>
              <div>{item.variety}</div>
              {productIdArr.findIndex((el: any) => el == item.wholesaleOrderItemId) > -1 &&
                <div style={{ width: 15 }}>
                  <Tooltip title={`${item.variety} has been added to ${this.getAddedPalletLabelsCount(item.wholesaleOrderItemId)} pallet${this.getAddedPalletLabelsCount(item.wholesaleOrderItemId) > 1 ? 's' : ''}`}>
                    <IconSVG type="check" viewBox="0 0 15 14" width={15} height={14} style={{ fill: 'none', marginBottom: 6 }} />
                  </Tooltip>
                </div>
              }
            </FlexDiv>
          </Menu.Item>
        ))}
      </Menu>
    )
  }

  render() {
    return (
      <CustomDropdownWrapper>
        <FlexDiv style={{ flexWrap: 'wrap' }}>
          {this.renderProductNames()}
          <Dropdown trigger={['click']} overlay={this.renderAvailableProducts()} overlayStyle={{ zIndex: 9999 }}>
            <Button type="link" className='btn-add-item'>
              <Icon type="plus" />
              Add Product
            </Button>
          </Dropdown>

        </FlexDiv>
      </CustomDropdownWrapper>
    )
  }
}

export default CustomDropdown
