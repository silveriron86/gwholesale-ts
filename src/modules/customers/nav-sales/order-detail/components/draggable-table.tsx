import * as React from 'react'
import { Dropdown, Icon, Select, Table, Tooltip, Checkbox, Popconfirm, Button } from 'antd'
import { DndProvider, useDrag, useDrop } from 'react-dnd'
import { getEmptyImage, HTML5Backend } from 'react-dnd-html5-backend'
import update from 'immutability-helper'
import { OrderItem } from '~/schema'
import { Icon as IconSVG } from '~/components'
import { CustomButton, Flex, FloatingMenu, ItemHMargin4, Unit } from '../../styles'
import {
  ThemeButton,
  ThemeCheckbox,
  ThemeInputNumber,
  ThemeInput,
  ThemeSelect,
  ThemeSpin,
  FlexCenter,
} from '~/modules/customers/customers.style'
import {
  formatNumber,
  inventoryQtyToRatioQty,
  mathRoundFun,
  ratioPriceToBasePrice,
  ratioQtyToInventoryQty,
  numberMultipy,
  judgeConstantRatio,
  notify,
  formatItemDescription,
  getAllowedEndpoint,
} from '~/common/utils'
import { CustomDragLayer } from './custom-drag-layer'
import { Resizable } from 'react-resizable'
import { cloneDeep } from 'lodash'
import UomSelect from './uom-select'
import _ from 'lodash'
import jQuery from 'jquery'
import { basePriceToRatioPrice } from '~/common/utils'
import { history } from '~/store/history'
import { AddAutocomplete } from '../tabs/add-autocomplete'
import { ReplaceItem } from '../modals/replace-item'

export type DragSortTableProps = {
  dataSource: OrderItem[]
  selectedRows: OrderItem[]
  catchWeightValues: any
  onRow: any
  footer: any
  rowKey: string
  renderFloatingMenu: Function
  onToggleSelect: Function
  updateRowItem: Function
  openModal: Function
  onClickAddItem: Function
  updateOrderItemsDisplayOrder: Function
  validateOrderStatusIsShipped: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  draggable: boolean
  updating: boolean
  updatingField: boolean
  copyOrderQty: Function
  openAddModal: Function
  openAddItemModal: boolean
  sellerSetting: any
  isFreshGreen: boolean
  addingCartItem: boolean
  updateOrderItemByProductId: Function
  updateOrderItemPrice: Function
  updateSortOrderItemDisplayOrder: Function
}

function BodyRow(props: any) {
  const {
    isOver,
    connectDragSource,
    connectDropTarget,
    moveRow,
    dropRow,
    endDragging,
    selectedRows,
    renderFloatingMenu,
    index,
    displayOrder,
    itemId,
    wholesaleOrderItemId,
    record,
    tableHoverIndex,
    updateSortOrderItemDisplayOrder,
    ...restProps
  } = props
  let { className } = restProps
  if (tableHoverIndex == index) {
    className += ' on-hover-row-bottom'
  } else if (tableHoverIndex - 1 == index && tableHoverIndex > 0) {
    className += ' on-hover-row-up'
  } else {
    className += ''
  }
  const [{ isDragging }, drag, preview] = useDrag({
    item: { type: 'row', index, displayOrder, itemId },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
    end: (dropResult, monitor) => {
      endDragging()
    },
  })

  React.useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true })
  }, [])

  const dropRef = React.useRef(null)
  const dragRef = React.useRef(null)

  const [, drop] = useDrop({
    accept: 'row',
    hover(item: any, monitor) {
      if (!dropRef.current) {
        return
      }
      const dragIndex = item.index

      const hoverIndex = index
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }

      // Determine rectangle on screen
      const hoverBoundingRect = dropRef.current.getBoundingClientRect()
      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
      // Determine mouse position
      const clientOffset = monitor.getClientOffset()
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }
      // Time to actually perform the action
      moveRow(dragIndex, hoverIndex)
      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      // item.index = hoverIndex
    },
    drop: (item, monitor) => {
      dropRow(item)
    },
  })

  drop(dropRef)
  drag(dragRef)

  const selectedRowIndex = selectedRows.findIndex((el: OrderItem) => {
    return el && el.wholesaleOrderItemId == wholesaleOrderItemId
  })

  if (_.isEmpty(record)) {
    return (
      <tr className={`${className} ${selectedRows.includes(record.displayOrderProduct) ? 'selected-row' : ''}`}>
        <td />
        {restProps.children}
      </tr>
    )
  }

  return (
    <>
      <tr
        ref={dropRef}
        className={`${className} ${selectedRows.includes(record.displayOrderProduct) ? 'selected-row' : ''} ${
          isDragging ? 'dragging' : ''
        }`}
        data-row-key={props.wholesaleOrderItemId}
      >
        <td ref={dragRef}>
          <div className="drag-handler">
            <IconSVG type="draggable" viewBox="0 0 20 24" width={16} height={20} />
          </div>
        </td>
        {restProps.children}
        <Dropdown
          overlay={renderFloatingMenu(props.wholesaleOrderItemId, props.record)}
          placement="bottomLeft"
          trigger={['click']}
        >
          <FloatingMenu className="floating-menu-body">
            <IconSVG type="ellipsis" viewBox="0 0 16 4" width={16} height={8} />
          </FloatingMenu>
        </Dropdown>
        <CustomDragLayer record={record} selectedRowIndex={selectedRowIndex} />
      </tr>
    </>
  )
}

function noDragBodyRow(props: any) {
  const { selectedRows, wholesaleOrderItemId, renderFloatingMenu, record, ...restProps } = props
  let { className } = restProps

  return (
    <>
      <tr className={`${className} ${selectedRows.includes(record.displayOrderProduct) ? 'selected-row' : ''}`}>
        <td />
        {restProps.children}
        <Dropdown overlay={renderFloatingMenu(props.itemId, props.record)} placement="bottomLeft" trigger={['click']}>
          <FloatingMenu className="floating-menu-body">
            <IconSVG type="ellipsis" viewBox="0 0 16 4" width={16} height={8} />
          </FloatingMenu>
        </Dropdown>
      </tr>
    </>
  )
}

function HeaderRow(props: any) {
  return (
    <tr>
      <th />
      {props.children}
    </tr>
  )
}

const ResizeableTitle = (props: any) => {
  const { onResize, width, ...restProps } = props

  return (
    <Resizable width={width} axis="x" onResize={onResize}>
      <th {...restProps} />
    </Resizable>
  )
}

// SO Cart 4-18 发现此文件没被用到，见_draggable.tsx
export const DragSortingTable: React.FC<DragSortTableProps> = (props: any) => {
  const {
    dataSource,
    rowKey,
    footer,
    selectedRows,
    catchWeightValues,
    renderFloatingMenu,
    onToggleSelect,
    updateRowItem,
    openModal,
    onClickAddItem,
    updateOrderItemsDisplayOrder,
    handlerOrderItemsRedux,
    handlerUpdateSOUOMRedux,
    validateOrderStatusIsShipped,
    onChangeOrderItemPricingLogicWithoutRefresh,
    draggable,
    updating,
    copyOrderQty,
    sellerSetting,
    isFreshGreen,
    addingCartItem,
    updateOrderItemByProductId,
    updateOrderItemPrice,
    updateSortOrderItemDisplayOrder,
    simplifyItems,
    simplifyItemsByOrderId,
  } = props
  const [initialQuantity, setInitialQuantity] = React.useState('')
  const [hoverIndex, setHoverIndex] = React.useState(-1)
  const [dragIndex, setDragIndex] = React.useState(-1)
  const [data, setData] = React.useState(dataSource)
  const [previousDataSource, setPreviousDatasource] = React.useState([...dataSource])
  const [currentPage, setCurrentPage] = React.useState(1)
  const [pageSize] = React.useState(20)
  const [currentOrderItemId, setCurrentOrderItemId] = React.useState<any>(-1)
  const [updateInfo, setUpdateInfo] = React.useState({})
  const [updateInfoField, setUpdateInfoField] = React.useState([])
  const [currentEditField, setCurrentEditField] = React.useState('')
  const [previousIsUpdatePrice, setPreviousIsUpdatePrice] = React.useState(false)

  const accountType = localStorage.getItem('accountType') || ''
  let editingPicked = false
  let priceKeyboardEnabled = false // define this variable to prevent price column change by up/down arrow because onChange event contains only value itself

  React.useEffect(() => {
    let isNewPage =
      dataSource.length === data.length + 1 && dataSource.length > pageSize && dataSource.length % pageSize === 1
    if (isFreshGreen) {
      isNewPage =
        dataSource.length === data.length + 1 && dataSource.length >= pageSize && dataSource.length % pageSize === 0
    }
    const newData: any = []
    dataSource.forEach((item: any) => {
      item.quantity = typeof item.quantity == 'number' ? item.quantity.toFixed(2) : item.quantity
      item.picked = typeof item.picked == 'number' ? item.picked.toFixed(2) : item.picked
      newData.push(item)
    })
    setData(newData)
    if (isNewPage) {
      setTimeout(() => {
        jQuery('.ant-pagination-item')
          .last()
          .trigger('click')
      }, 100)
      setTimeout(() => {
        jQuery('.order-quantity-input input')
          .last()
          .trigger('focus')
      }, 1500)
    }
  }, [dataSource])

  React.useEffect(() => {
    if (updateInfoField.length) {
      if (_.last(dataSource).wholesaleOrderItemId) {
        updateInfoField.forEach((v) => {
          updateRowItem({ ...updateInfo, wholesaleOrderItemId: _.last(dataSource)['wholesaleOrderItemId'] }, v)
        })
        setUpdateInfoField([])
      }
    }
  }, [dataSource])

  const isDisablePickingStep = props.company && props.company.isDisablePickingStep

  React.useEffect(() => {
    const updatedColumns = initialColumn.map((col: any, index: number) => {
      return {
        ...col,
        width: columns[index] ? columns[index].width : 150,
        onHeaderCell: (column) => ({
          width: column.width,
          onResize: handleResize(index + 1),
        }),
      }
    })
    setColumns(updatedColumns)
  }, [
    selectedRows,
    data,
    JSON.stringify(catchWeightValues),
    draggable,
    currentPage,
    props.currentOrder.wholesaleOrderStatus,
    props.saleItems,
    props.simplifyItems,
    initialQuantity,
    isFreshGreen,
  ])

  React.useEffect(() => {
    if (data.length == dataSource.length && hoverIndex > -1 && dragIndex > -1) {
      // let updateData: OrderItem[] = cloneDeep(data)
      // updateData.forEach((el: OrderItem, index: number) => {
      //   el.displayOrder = index
      // })
      // updateOrderItemsDisplayOrder({ itemsList: updateData })
      setDragIndex(-1)
      setHoverIndex(-1)
    }
  }, [JSON.stringify(data)])

  const getOrderItemIdAndItemId = (orderItems: any) => {
    return _.map(orderItems, (saleItem) => {
      return {
        wholesaleOrderItemId: saleItem.wholesaleOrderItemId,
        wholesaleItemId: saleItem.wholesaleItemId,
        lotId: saleItem.lotId,
      }
    })
  }

  //for update order item's item id , need refresh previous data
  // React.useEffect(() => {
  //   setPreviousDatasource([...dataSource])
  // }, [JSON.stringify(getOrderItemIdAndItemId(dataSource))])

  const onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  const hideReplaceDropdown = (orderItemId: number) => {
    let cloneData = cloneDeep(data)
    let row: OrderItem | null = null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el['visibleDropdown'] = false
        row = el
        return
      }
    })

    setData(cloneData)
  }

  // const handleSave = (row: OrderItem | null, field: string) => {
  //   if (row && field != 'price') {
  //     row.quantity = row.quantity ? row.quantity : 0
  //     if (props.company && props.company.isDisablePickingStep && (field == 'quantity' || field == 'uom')) {
  //       //setting picked qty
  //       row.picked = row.quantity ? row.quantity : 0
  //     } else {
  //       row.picked = row.picked ? row.picked : 0
  //     }
  //     if (row.wholesaleOrderItemId) {
  //       updateRowItem(row, field)
  //     } else {
  //       setUpdateInfo(row)
  //       setUpdateInfoField([...updateInfoField, field])
  //     }
  //   } else if (row && field == 'price') {
  //     const data = {
  //       pricingUOM: row.pricingUOM ? row.pricingUOM : row.inventoryUOM,
  //       price: row.price,
  //       pricingGroup: 'A',
  //       pricingLogic: 'CUSTOM_PRICE',
  //       wholesaleOrderItemId: row.wholesaleOrderItemId,
  //     }

  //     onChangeOrderItemPricingLogicWithoutRefresh(data)
  //   }
  // }

  // const updateOrderItem = (
  //   orderItemId: number,
  //   type: string,
  //   pas: boolean,
  //   forceData: any,
  //   value: any,
  //   isTotalPrice?: boolean = false,
  // ) => {
  //   const _data = cloneDeep(forceData ? forceData : data)
  //   const sourceRecord = previousDataSource.find((el) => el.wholesaleOrderItemId == orderItemId)
  //   if (type == 'price') {
  //     //update total price value is number
  //     if (typeof value == 'string') {
  //       value = value.replace('$', '').trim()
  //     }
  //     if (isNaN(value)) {
  //       return
  //     }
  //     const pricingUOM = sourceRecord['pricingUOM'] ? sourceRecord['pricingUOM'] : sourceRecord['inventoryUOM']
  //     const price = ratioPriceToBasePrice(pricingUOM, value, sourceRecord, 4)
  //     //Because the front end is accurate according to two digits, all judgments are equal to two digits
  //     if (price == mathRoundFun(sourceRecord[type], 4)) {
  //       return
  //     }
  //     setPreviousIsUpdatePrice(true)
  //   } else {
  //     if (1 /*forceData*/) {
  //       //hack for : picked qty can be empty string
  //       if (type == 'picked') {
  //         if (_.isNaN(Number(value))) {
  //           // handler non-integer value
  //           return
  //         }
  //         if (value == sourceRecord[type]) {
  //           return
  //         }
  //       } else {
  //         if (value == sourceRecord[type] && sourceRecord[type] != 0) {
  //           return
  //         }
  //       }
  //     }
  //     setPreviousIsUpdatePrice(false)
  //   }

  //   if (validateOrderStatusIsShipped()) {
  //     if (pas === true && type === 'price') {
  //       // The price is lock once you are shipped. What we want is make it editable if it is shipped/lock but have a PAS on that order item.
  //     } else {
  //       return
  //     }
  //   }

  //   let row = _data.find((el: any) => {
  //     return el.wholesaleOrderItemId == orderItemId
  //   })
  //   previousDataSource.find((el) => {
  //     if (el.wholesaleOrderItemId == orderItemId) {
  //       //only update total price need set price to base
  //       if (type == 'price' /*&& isTotalPrice*/) {
  //         const pricingUOM = row.pricingUOM ? row.pricingUOM : row.inventoryUOM
  //         el[type] = ratioPriceToBasePrice(pricingUOM, value, el, 12)
  //         row[type] = value//el[type]
  //       } else {
  //         el[type] = value
  //         row[type] = value
  //       }

  //       if (!el['updatedPrice']) {
  //         delete el['updatedPrice']
  //       }
  //     }
  //   })
  //   if (type == 'picked') {
  //     if (value !== null) {
  //       row['status'] = 'PICKING'
  //     } else {
  //       row['status'] = 'PLACED'
  //     }
  //   }
  //   if (type == 'quantity') {
  //     row['qtyUpdatedFlag'] = true
  //     handlerOrderItemsRedux(row)
  //   }

  //   if (type === 'price') {
  //     const pricingUOM = row.pricingUOM ? row.pricingUOM : row.inventoryUOM
  //     const price = ratioPriceToBasePrice(pricingUOM, value, row, 12)
  //     handlerUpdateSOUOMRedux({
  //       pricingUOM: row.pricingUOM ? row.pricingUOM : row.inventoryUOM,
  //       price: price,
  //       pricingGroup: 'A',
  //       pricingLogic: 'CUSTOM_PRICE',
  //       wholesaleOrderItemId: row.wholesaleOrderItemId,
  //     })
  //   }

  //   handleSave(row, type)
  //   setPreviousDatasource(previousDataSource)
  // }

  const [request, setRequest] = React.useState<any[]>([])
  const [curDisplayOrder, setCurDisplayOrder] = React.useState(null)
  React.useEffect(() => {
    if (!props.updateOrderQuantityLoading.fetching && request.length) {
      props.updateOrderQuantityLoading.fetching = true
      if (request[0].isUpdatePrice) {
        const data = { ...request[0], isUpdatePrice: undefined }
        updateOrderItemPrice(data)
      } else {
        updateOrderItemByProductId(request[0])
      }
      setRequest(request.slice(1))
    }
  }, [props.updateOrderQuantityLoading.fetching])

  const handleChangeItem = ({ record, type, editType, value }: any) => {
    setCurDisplayOrder(record.displayOrderProduct)
    if (validateOrderStatusIsShipped()) {
      return
    }
    if (record[type] == value) return
    if (value === null) return

    const recordPrice = record.price
    if (type === 'quantity' || type === 'picked') {
      if (record.items.length > 1) {
        props.updateOrderQuantityLoading.loading = true
      }
      if (isDisablePickingStep) {
        props.orderItemByProduct[record.displayOrderProduct]['picked'] = value
      }
    }
    if (type === 'picked' || (isDisablePickingStep && type === 'quantity')) {
      props.orderItemByProduct[record.displayOrderProduct]['catchWeightQty'] = value
    }
    props.orderItemByProduct[record.displayOrderProduct][type] = value

    if (type === 'UOM') {
      handlerUpdateSOUOMRedux({
        displayOrder: record.displayOrderProduct,
        pricingUOM: value,
      })
      // props.orderItemByProduct[record.displayOrderProduct]['pricingUOM'] = value
    }

    let status = record.status
    let lotAssignmentMethod = record.lotAssignmentMethod
    if (type === 'picked') {
      status = value === '' ? 'PICKING' : 'PLACED'
    }

    if (type === 'quantity' && record.lotAssignmentMethod === 3) {
      props.orderItemByProduct[record.displayOrderProduct]['lotAssignmentMethod'] = 1
      lotAssignmentMethod = 1
    }

    const data = {
      orderId: props.currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod,
        enableLotOverflow: record.enableLotOverflow,
        wholesaleItemId: record.itemId,
        picked: isDisablePickingStep && type === 'quantity' ? value : record.picked,
        quantity: record.quantity,
        [type]: value === '' ? 0 : value,
        [editType]: true,
        price: recordPrice,
        pricingUOM: type === 'UOM' ? value : record.pricingUOM,
        status,
      },
    }
    if (props.updateOrderQuantityLoading.fetching && curDisplayOrder === record.displayOrderProduct) {
      return setRequest([...request, data])
    }

    props.updateOrderQuantityLoading.fetching = true
    updateOrderItemByProductId(data)
  }

  const handleChangeItemPrice = ({ record, type, price }: any) => {
    setCurDisplayOrder(record.displayOrderProduct)
    if (validateOrderStatusIsShipped()) {
      return
    }
    if (formatNumber(record[type], 8) == formatNumber(ratioPriceToBasePrice(record.pricingUOM, price, record, 12), 8))
      return
    handlerUpdateSOUOMRedux({
      displayOrder: record.displayOrderProduct,
      price: ratioPriceToBasePrice(record.pricingUOM, price, record, 12),
    })
    // props.orderItemByProduct[record.displayOrderProduct][type] = ratioPriceToBasePrice(record.pricingUOM, price, record, 12)
    if (record.items.length > 1) {
      props.updateOrderQuantityLoading.loading = true
    }

    const data = {
      isUpdatePrice: true,
      orderId: props.currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod: record.lotAssignmentMethod,
        enableLotOverflow: record.enableLotOverflow,
        wholesaleItemId: record.itemId,
        pricingLogic: 'CUSTOM_PRICE',
        pricingGroup: record.pricingGroup,
        pricingUOM: record.pricingUOM,
        price,
      },
    }

    if (props.updateOrderQuantityLoading.fetching && curDisplayOrder === record.displayOrderProduct) {
      return setRequest([...request, data])
    }

    props.updateOrderQuantityLoading.fetching = true
    updateOrderItemPrice(data)
  }

  // const onChangeItem = _.debounce(
  //   (orderItemId: string, type: string, value: any, pas: boolean, reload: boolean = false) => {
  //     if (validateOrderStatusIsShipped()) {
  //       if (pas === true && type === 'price') {
  //         // The price is lock once you are shipped. What we want is make it editable if it is shipped/lock but have a PAS on that order item.
  //       } else {
  //         return
  //       }
  //     }

  //     const curRecord = props.dataSource.find((el) => el.wholesaleOrderItemId == orderItemId)
  //     if (type == 'price') {
  //       //update price , value is string include '$'
  //       if (isNaN(value)) {
  //         value = value.replace('$', '').trim()
  //         if ((value != '' && isNaN(Number(value))) || curRecord.price == value) {
  //           return
  //         }
  //       } else {
  //         // update total price , value is number and value is ratio price
  //         let newValue = ratioPriceToBasePrice(curRecord.pricingUOM, value, curRecord, 12)
  //         // if (curRecord.price == newValue) {
  //         //hack for big data cann't use more decimal to judge equal
  //         if (mathRoundFun(curRecord.price, 4) == mathRoundFun(newValue, 4)) {
  //           return
  //         }
  //       }
  //     } else {
  //       if (_.isNaN(Number(value))) {
  //         //handler non-integer value
  //         return
  //       }
  //     }

  //     let cloneData = cloneDeep(dataSource)
  //     let row = null,
  //       oldRow = null
  //     cloneData.forEach((el: OrderItem, index: number) => {
  //       if (el.wholesaleOrderItemId == orderItemId) {
  //         oldRow = _.cloneDeep(el)
  //         el[type] = value
  //         if (type == 'picked') {
  //           if (value !== null) {
  //             el['status'] = 'PICKING'
  //           } else {
  //             el['status'] = 'PLACED'
  //           }
  //         } else if (type == 'price') {
  //           el['pricingLogic'] = 'CUSTOM_PRICE'
  //           el['updatedPrice'] = true
  //           const pricingUOM = el.pricingUOM ? el.pricingUOM : el.inventoryUOM
  //           el[type] = ratioPriceToBasePrice(pricingUOM, value, el, 12)
  //         } else if (type == 'quantity') {
  //           //need set for redux
  //           if (props.company && props.company.isDisablePickingStep) {
  //             el['picked'] = value
  //           }
  //         }
  //         if (reload) {
  //           el['updateTotalPrice'] = true
  //         }
  //         row = el
  //         return
  //       }
  //     })

  //     // setData(cloneData)
  //     if (type == 'price' && _.isNumber(Number(value))) {
  //       const pricingUOM = row.pricingUOM ? row.pricingUOM : row.inventoryUOM
  //       const price = ratioPriceToBasePrice(pricingUOM, value, row, 12)
  //       const rowLoading = reload && oldRow.price != price ? true : false
  //       handlerUpdateSOUOMRedux({
  //         pricingUOM: row.pricingUOM ? row.pricingUOM : row.inventoryUOM,
  //         price: price,
  //         pricingGroup: 'A',
  //         pricingLogic: 'CUSTOM_PRICE',
  //         wholesaleOrderItemId: row.wholesaleOrderItemId,
  //         rowLoading: rowLoading,
  //       })
  //       row['rowLoading'] = rowLoading
  //     } else {
  //       if (type != 'quantity') {
  //         row['rowLoading'] = true
  //       } else {
  //         // row['qtyUpdatedLoading'] = true
  //       }
  //       handlerOrderItemsRedux(row)
  //     }

  //     // //update total price
  //     if (reload) {
  //       updateOrderItem(orderItemId, type, pas, cloneData, value)
  //     }

  //     //when in preview model and set order/picked qty to 0 ,need hack it, because it will not enter the onblur function
  //     if (
  //       props.draggable === false &&
  //       ((type == 'quantity' && row.picked == 0) || (type == 'picked' && row.quantity == 0)) &&
  //       value == 0
  //     ) {
  //       updateOrderItem(orderItemId, type, pas, cloneData, value)
  //     }
  //   },
  //   350,
  // )

  const onSelectChange = (record: any, orderItemId: string, type: string, value: any) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    let cloneData = cloneDeep(data)
    let row: OrderItem | null = null
    if (type == 'status' && value == 'NEW') {
      value = 'PLACED'
    }

    if (record.oldCatchWeight) {
      notify('warn', 'Warn', 'This data cannot be operated, please contact the system administrator')
      return
    }
    setData(cloneData)

    handleChangeItem({ record, type: 'status', editType: 'isUpdateStatus', value })
  }

  // const onChangeUom = (orderItemId: string, value: any) => {
  //   if (validateOrderStatusIsShipped()) {
  //     return
  //   }
  //   // let cloneData = cloneDeep(dataSource)
  //   let row: OrderItem | null = null
  //   // cloneData.forEach((el: OrderItem) => {
  //   //   if (el.wholesaleOrderItemId == orderItemId) {
  //   //     el['UOM'] = value
  //   //     el['pricingUOM'] = value
  //   //     row = el
  //   //     return
  //   //   }
  //   // })
  //   let oldUOM
  //   previousDataSource.find((el) => {
  //     if (el.wholesaleOrderItemId == orderItemId) {
  //       // el['price'] = ratioPriceToBasePrice(el.pricingUOM, el.price, el, 12)
  //       oldUOM = el.pricingUOM
  //       el['UOM'] = value
  //       el['pricingUOM'] = value
  //       row = el
  //       return
  //     }
  //   })

  //   // setPreviousDatasource(previousDataSource)
  //   // handlerOrderItemsRedux(row)

  //   if (previousIsUpdatePrice && oldUOM) {
  //     row['price'] = ratioPriceToBasePrice(oldUOM, row.price, row, 12)
  //   }
  //   setPreviousIsUpdatePrice(false)
  //   handleSave(row, 'uom')
  // }

  const onClickReplaceItem = (orderItemId: number) => {
    let cloneData = cloneDeep(data)
    let row: OrderItem | null = null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el['visibleDropdown'] = el.visibleDropdown ? false : true
        row = el
        return
      }
    })
    setData(cloneData)
  }

  const confirmCopyOrderQty = () => {
    // const items = data
    //   .filter((el: OrderItem) => {
    //     const constantRatio = judgeConstantRatio(el)
    //     return constantRatio === true && (el.status === 'PLACED' || el.status === 'NEW')
    //   })
    //   .map((el) => {
    //     return {
    //       wholesaleOrderItemId: el.wholesaleOrderItemId,
    //       quantity: el.quantity,
    //     }
    //   })
    props.startUpdating()
    props.updateAutofillPicked({ orderId: props.currentOrder.wholesaleOrderId })
  }

  const updatePriceFromTotal = (evt: any, record: any) => {
    let total = 0
    let price = 0
    const strVal = evt.target.value.replace('$', '').trim()
    if (strVal !== '') {
      total = parseFloat(strVal)
    }

    let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
    // let ratioPrice = basePriceToRatioPrice(pricingUOM, record.price, record, 6)

    // let constantRatio = judgeConstantRatio(record)
    if (record.oldCatchWeight) {
      price = total / record.catchWeightQty
    } else {
      price = total / inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record, 12)
    }
    price = mathRoundFun(price, 12)
    handleChangeItemPrice({ record, type: 'price', price })
  }

  const readOnly = props.currentOrder.wholesaleOrderStatus === 'CANCEL'
  const freshGreenColumns: any[] = [
    {
      title:
        draggable && !readOnly ? (
          <>
            <ThemeCheckbox
              style={{ width: 30 }}
              onChange={onToggleSelect.bind(this, 'All')}
              checked={data.length && selectedRows.length == data.length}
            />
            Order Qty
          </>
        ) : (
          'Order Qty'
        ),
      dataIndex: 'quantity',
      width: 40,
      // sorter: (a: any, b: any) => a.quantity - b.quantity,
      render: (quantity: number, record: any, index: number) => {
        return (
          <div className="name-column order-quantity-input">
            <Flex className="v-center">
              {_.isEmpty(record) ? (
                <>
                  <span className="w30" />
                  <Flex className="v-center">
                    <ThemeInput
                      className={`order-qty`}
                      value={initialQuantity}
                      readOnly={addingCartItem}
                      disabled={record.status === 'SHIPPED' || readOnly}
                      style={{ marginRight: 4, width: 120, textAlign: 'right' }}
                      onChange={(evt: any) => {
                        const { value } = evt.target
                        const reg = /^-?\d*(\.\d*)?$/
                        if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
                          setInitialQuantity(value)
                        }
                      }}
                    />
                  </Flex>
                </>
              ) : (
                <>
                  <span
                    className={draggable && !readOnly ? 'row-index-number' : 'w30'}
                    // style={{ display: draggable && !readOnly && curIndex != -1 ? 'none' : 'block' }}
                  >
                    {(currentPage - 1) * pageSize + (index + 1)}
                  </span>
                  {draggable && !readOnly && (
                    <ThemeCheckbox
                      className="row-selection"
                      style={{ display: selectedRows.includes(record.displayOrderProduct) ? 'block' : 'none' }}
                      onChange={onToggleSelect.bind(this, record.displayOrderProduct)}
                      checked={selectedRows.includes(record.displayOrderProduct)}
                    />
                  )}
                  <Flex className="v-center" style={{ flex: 1 }}>
                    <ThemeInputNumber
                      className={`order-qty no-stepper`}
                      value={quantity ? quantity : ''}
                      style={{ marginRight: 4, width: 60, textAlign: 'right' }}
                      // onChange={(evt: any) => {
                      //   // console.log(evt)
                      //   onChangeItem(record.wholesaleOrderItemId, 'quantity', evt || 0, record.pas)
                      // }}
                      // onBlur={(e: any) =>
                      //   updateOrderItem(record.wholesaleOrderItemId, 'quantity', false, null, e.target.value)
                      // }
                      onBlur={(e) =>
                        handleChangeItem({
                          record,
                          type: 'quantity',
                          editType: 'isUpdateOrderQty',
                          value: e.target.value || 0,
                        })
                      }
                      step={1}
                      onFocus={(e) => e.target.select()}
                      readOnly={addingCartItem}
                      disabled={record.status === 'SHIPPED' || readOnly}
                    />
                  </Flex>
                </>
              )}
            </Flex>
            {draggable && !readOnly && (
              <div className="add-new-row">
                <div style={{ position: 'relative' }}>
                  <Icon
                    className="icon-add-row"
                    type="plus-circle"
                    width={20}
                    height={20}
                    onClick={onClickAddItem.bind(this, record.wholesaleOrderItemId)}
                  />
                </div>
              </div>
            )}
          </div>
        )
      },
    },
    {
      title: 'Item',
      dataIndex: 'variety',
      // sorter: (a: any, b: any) => onSortString('variety', a, b),
      render: (variety: string, record: any, index: number) => {
        if (_.isEmpty(record)) {
          return (
            <div style={{ minWidth: window.innerWidth / 3, width: '100%' }} className="name-column">
              <AddAutocomplete
                {...props.acProps}
                isDisablePickingStep={isDisablePickingStep}
                quantity={initialQuantity}
                callBack={() => setInitialQuantity('')}
                simplifyItems={
                  sellerSetting?.company?.enableAddItemFromProductList ? simplifyItemsByOrderId : simplifyItems
                }
              />
            </div>
          )
        }
        const editableStatus = record.status != 'SHIPPED' && record.status != 'CANCEL'
        const redirectPath =
          window.location.origin +
          window.location.pathname +
          `#/product/${record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`
        return (
          <div style={{ minWidth: 380, width: '100%' }} className="name-column">
            <Flex className="space-between">
              <Flex style={{ paddingLeft: 8, display: record.visibleDropdown ? 'none' : 'flex' }}>
                <span className="product-name">
                  <a href={`${redirectPath}`}>
                    {formatItemDescription(variety, record.SKU, sellerSetting, record.customerProductCode)}
                  </a>
                </span>
                <div>
                  <Tooltip placement="top" title={'Edit item name for display'}>
                    <IconSVG
                      onClick={openModal.bind(this, 'openEditItemNameModal', record)}
                      className={`icon-edit-item-name ${record.editedItemName ? 'icon-green-permanent' : ''}`}
                      type="edit"
                      viewBox="0 0 20 20"
                      width={20}
                      height={20}
                      style={{
                        ...ItemHMargin4,
                        marginTop: 2,
                        fill: 'none',
                      }}
                    />
                  </Tooltip>
                </div>
                <div>
                  <Tooltip placement="top" title={'Note added'}>
                    <IconSVG
                      onClick={openModal.bind(this, 'addNoteModal', record)}
                      type="add-note"
                      viewBox="0 0 20 20"
                      width={20}
                      height={20}
                      style={{
                        ...ItemHMargin4,
                        marginTop: 2,
                        display: record.note && record.note != 'null' ? 'block' : 'none',
                        fill: 'none',
                      }}
                    />
                  </Tooltip>
                </div>
                <div>
                  <Tooltip placement="top" title={'Work order added'}>
                    <IconSVG
                      // onClick={openModal.bind(this, 'addWOModal', record)}
                      type="add-wo"
                      viewBox="0 0 20 20"
                      width={20}
                      height={20}
                      style={{
                        ...ItemHMargin4,
                        display: record.workOrderId ? 'block' : 'none',
                        cursor: 'normal',
                      }}
                      onClick={() => {
                        history.push(`/manufacturing/${record.workOrderId}/wo-overview`)
                      }}
                    />
                  </Tooltip>
                </div>
              </Flex>
              <div className="item-name-wrapper" style={{ alignSelf: 'center' }}>
                {editableStatus && !record.visibleDropdown && (
                  <div style={{ width: 20, height: 20 }}>
                    <Icon
                      type="caret-down"
                      viewBox="0 0 20 20"
                      className="replace-item-handler"
                      width={20}
                      height={20}
                      style={{ ...ItemHMargin4, cursor: 'pointer' }}
                      onClick={(e) => {
                        if (e.isTrusted) {
                          localStorage.setItem('trigger-by-keyboard', '0')
                          localStorage.setItem('editing-row-key', index.toString())
                        }
                        onClickReplaceItem(record.wholesaleOrderItemId)
                      }}
                    />
                  </div>
                )}
              </div>
            </Flex>
            {record.editedItemName && <div className="edited-item-name pl8">{record.editedItemName}</div>}
            {record.visibleDropdown && (
              <Flex style={{ display: record.visibleDropdown ? 'flex' : 'none' }}>
                <ReplaceItem {...props} current={record} hideReplaceDropdown={hideReplaceDropdown} />
              </Flex>
            )}
          </div>
        )
      },
    },
    {
      title: 'UOM',
      dataIndex: 'UOM',
      render: (uom: string, record: any, index: number) => {
        if (_.isEmpty(record)) {
          return { props: { colSpan: 0 } }
        }
        return (
          <>
            <UomSelect
              readOnly={addingCartItem}
              disabled={record.status === 'SHIPPED' || readOnly}
              orderItem={record}
              handlerChangeUom={(orderItemId: any, value: any) => {
                const row = data.find((el: any) => el.wholesaleOrderItemId == orderItemId)
                if (row) {
                  handleChangeItem({ record: row, type: 'UOM', editType: 'isUpdateUOM', value })
                }
              }}
              type={1}
              parent="cart-order-qty"
              isFreshGreen={isFreshGreen}
            />
          </>
        )
      },
    },
  ]
  const otherColumns: any[] = [
    {
      title:
        draggable && !readOnly ? (
          <>
            <ThemeCheckbox
              style={{ width: 30 }}
              onChange={onToggleSelect.bind(this, 'All')}
              checked={data.length && selectedRows.length == data.length}
            />
            Item
          </>
        ) : (
          'Item'
        ),
      dataIndex: 'variety',
      // sorter: (a: any, b: any) => onSortString('variety', a, b),
      render: (variety: string, record: any, index: number) => {
        let visibleWarningTooltip = record.picked != 0
        const constantRatio = judgeConstantRatio(record)
        if (constantRatio == false) {
          const weights = catchWeightValues[record.wholesaleOrderItemId]
          visibleWarningTooltip = weights && weights.length ? true : false
        }
        const editableStatus = record.status != 'SHIPPED' && record.status != 'CANCEL'
        const redirectPath =
          window.location.origin +
          window.location.pathname +
          `#/product/${record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`

        return (
          <>
            <div className="name-column" style={{ minWidth: 200 }}>
              <Flex className="space-between">
                <Flex style={{ paddingLeft: 8, display: record.visibleDropdown ? 'none' : 'flex' }}>
                  <span
                    className={draggable && !readOnly ? 'row-index-number' : 'w30'}
                    style={{
                      visibility:
                        draggable && !readOnly && selectedRows.includes(record.displayOrderProduct)
                          ? 'hidden'
                          : 'visible',
                    }}
                  >
                    {draggable === false ? index + 1 : record.displayOrderProduct + 1}
                  </span>
                  <div>
                    <div style={{ flex: 1 }}>
                      {draggable && !readOnly && (
                        <ThemeCheckbox
                          className="row-selection"
                          style={{
                            display: selectedRows.includes(record.displayOrderProduct) ? 'block' : 'none',
                            position: 'absolute',
                            marginLeft: -32,
                          }}
                          onChange={onToggleSelect.bind(this, record.displayOrderProduct)}
                          checked={selectedRows.includes(record.displayOrderProduct)}
                        />
                      )}
                      <span className="product-name">
                        <a href={`${redirectPath}`}>
                          {formatItemDescription(variety, record.SKU, sellerSetting, record.customerProductCode)}
                        </a>
                      </span>
                    </div>
                    {record.editedItemName && <div className="edited-item-name">{record.editedItemName}</div>}
                  </div>
                  <div>
                    <Tooltip placement="top" title={'Edit item name for display'}>
                      <IconSVG
                        onClick={openModal.bind(this, 'openEditItemNameModal', record)}
                        className={`icon-edit-item-name ${record.editedItemName ? 'icon-green-permanent' : ''}`}
                        type="edit"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          marginTop: 2,
                          fill: 'none',
                        }}
                      />
                    </Tooltip>
                  </div>
                  <div>
                    <Tooltip placement="top" title={'Note added'}>
                      <IconSVG
                        onClick={openModal.bind(this, 'addNoteModal', record)}
                        type="add-note"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          marginTop: 2,
                          display: record.note && record.note != 'null' ? 'block' : 'none',
                          fill: 'none',
                        }}
                      />
                    </Tooltip>
                  </div>
                  <div>
                    <Tooltip placement="top" title={'Work order added'}>
                      <IconSVG
                        // onClick={openModal.bind(this, 'addWOModal', record)}
                        type="add-wo"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{ ...ItemHMargin4, display: record.workOrderId ? 'block' : 'none', cursor: 'normal' }}
                        onClick={() => {
                          history.push(`/manufacturing/${record.workOrderId}/wo-overview`)
                        }}
                      />
                    </Tooltip>
                  </div>
                </Flex>
                <div style={{ alignSelf: 'center' }}>
                  {editableStatus && !record.visibleDropdown && (
                    <div style={{ width: 20, height: 20 }}>
                      <Icon
                        type="caret-down"
                        viewBox="0 0 20 20"
                        className="replace-item-handler"
                        width={20}
                        height={20}
                        style={{ ...ItemHMargin4, cursor: 'pointer' }}
                        onClick={(e) => {
                          if (e.isTrusted) {
                            localStorage.setItem('trigger-by-keyboard', '0')
                            localStorage.setItem('editing-row-key', index.toString())
                          }
                          onClickReplaceItem(record.wholesaleOrderItemId)
                        }}
                      />
                    </div>
                  )}
                </div>
              </Flex>

              {record.visibleDropdown && (
                <Flex style={{ paddingLeft: 24, display: record.visibleDropdown ? 'flex' : 'none' }}>
                  <ReplaceItem {...props} current={record} hideReplaceDropdown={hideReplaceDropdown} />
                </Flex>
              )}
            </div>
            {draggable && !readOnly && (
              <div className="add-new-row">
                <div style={{ position: 'relative' }}>
                  <Icon
                    className="icon-add-row"
                    type="plus-circle"
                    width={20}
                    height={20}
                    onClick={onClickAddItem.bind(this, record.wholesaleOrderItemId)}
                  />
                </div>
              </div>
            )}
          </>
        )
      },
    },
    {
      title: 'Brand',
      dataIndex: 'modifiers',
      width: 80,
      render: (modifiers: string, record: any) => {
        return _.get(record, 'lotIds.length', 0) ? modifiers : ''
      },
    },
    {
      title: 'Origin',
      dataIndex: 'extraOrigin',
      width: 80,
      render: (extraOrigin: string, record: any) => {
        return _.get(record, 'lotIds.length', 0) ? extraOrigin : ''
      },
    },
    {
      title: 'Order Qty',
      dataIndex: 'quantity',
      width: 180,
      // sorter: (a: any, b: any) => a.quantity - b.quantity,
      render: (quantity: number, record: any, index: number) => {
        return (
          <Flex className="v-center order-quantity-input">
            <ThemeInputNumber
              className={`order-qty no-stepper`}
              value={quantity || ''}
              style={{ marginRight: 4, width: 60, textAlign: 'right' }}
              // onChange={
              //   (evt: any) => onChangeItem(record.wholesaleOrderItemId, 'quantity', evt || 0, record.pas)
              //   // onChangeItem(record.wholesaleOrderItemId, 'quantity', evt.target.value, record.pas)
              // }
              // onBlur={(e: any) => {
              //   updateOrderItem(record.wholesaleOrderItemId, 'quantity', false, null, e.target.value)
              // }}
              onBlur={(e) =>
                handleChangeItem({ record, type: 'quantity', editType: 'isUpdateOrderQty', value: e.target.value || 0 })
              }
              // onChange={(value) => handleChangeItem({ record, type: 'quantity', editType: 'isUpdateOrderQty', value })}
              step={1}
              onFocus={(e) => e.target.select()}
              readOnly={addingCartItem}
              disabled={record.status === 'SHIPPED' || readOnly}
            />
            <UomSelect
              readOnly={addingCartItem}
              disabled={record.status === 'SHIPPED' || readOnly}
              orderItem={record}
              handlerChangeUom={(orderItemId: any, value: any) => {
                const row = data.find((el: any) => el.wholesaleOrderItemId == orderItemId)
                if (row) {
                  handleChangeItem({ record: row, type: 'UOM', editType: 'isUpdateUOM', value })
                }
              }}
              type={1}
              isFreshGreen={isFreshGreen}
            />
          </Flex>
        )
      },
    },
    {
      title: 'Lot',
      dataIndex: 'lotIds',
      width: 180,
      render: (lotIds: string[], record: any, index: number) => {
        // lot pop up display number
        if (dataSource && dataSource[index] && dataSource[index].items) {
          const notSelect = dataSource[index].items.find((item: any) => {
            if (item.lotId == undefined) {
              return item
            }
          })
          if (notSelect && (notSelect.quantity > 0 || notSelect.picked > 0)) {
            index = lotIds.indexOf('No lot selected')
            if (index == -1) {
              lotIds.push('No lot selected')
            }
            if (lotIds[0] == null) {
              lotIds.splice(0, 1)
            }
          } else {
            index = lotIds.indexOf('No lot selected')
            if (index !== -1) {
              lotIds.splice(index, 1)
            }
          }
        }
        let nullIndex = -1
        if (lotIds) {
          lotIds.forEach((item, index) => {
            if (item == null) {
              nullIndex = index
            }
          })
          if (nullIndex > 0) {
            lotIds.splice(nullIndex, 1)
          }
        }
        const lotAvailableQtys = record.items ? record.items.map((v) => v.lotAvailableQty) : []
        const cond = lotAvailableQtys.some((v) => v < 0)
        const lotButton = (
          <CustomButton
            disabled={record.status === 'SHIPPED' || readOnly}
            onClick={openModal.bind(this, 'lotsModal', record)}
            className={`${cond ? 'warning' : ''}`}
            style={{ width: '100%', height: '100%' }}
          >
            {!_.compact(lotIds).length ? (
              'No lot selected'
            ) : (
              <span>
                {lotIds[0]} {lotIds.length > 1 && `+${lotIds.length - 1}`}
              </span>
            )}
          </CustomButton>
        )

        const lotBtn =
          record.quantity > 0 || record.picked > 0 ? (
            lotButton
          ) : (
            <Button style={{ color: 'white', width: '100%' }}>empty</Button>
          )
        if (cond) {
          return (
            <Tooltip placement="top" title="The ordered quantity is greater than the selected lot inventory.">
              {lotBtn}
            </Tooltip>
          )
        }
        if (_.get(lotIds, 'length', 0) < 2) return lotBtn
        return (
          <Tooltip placement="top" title={lotIds.filter((lot) => lot !== '').join(',')}>
            {lotBtn}
          </Tooltip>
        )
      },
      // sorter: (a: any, b: any) => onSortString('lotId', a, b),
    },
  ]
  const commonColumns: any[] = [
    {
      title: 'Price',
      dataIndex: 'price',
      width: 220,
      // sorter: (a: any, b: any) => a.price - b.price,
      render: (value: number, record: any) => {
        if (_.isEmpty(record)) {
          return {
            children: '',
            props: {
              colSpan: 6,
            },
          }
        }

        let price = value
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        let cost = record.lotCost
        if (_.isNumber(price)) {
          price = basePriceToRatioPrice(pricingUOM, value, record)
        }

        if (_.isNumber(cost)) {
          cost = basePriceToRatioPrice(pricingUOM, cost + record.perAllocateCost, record)
        }
        let tooltipText =
          _.isNumber(value) && price > 0 && price < cost
            ? `Sale price is below cost ($${formatNumber(cost, 2)}/${pricingUOM})`
            : ''
        tooltipText = price == 0 ? 'Sales price is zero' : tooltipText
        const warningClass = tooltipText ? 'warning' : ''

        return (
          <Flex className="v-center">
            <Tooltip placement="top" title={tooltipText}>
              <div style={{ width: 85 }}>
                <ThemeInputNumber
                  className={`no-stepper cart-price ${warningClass} ${record.status === 'SHIPPED' ? 'readonly' : ''}`}
                  style={{ textAlign: 'right' }}
                  step={0.01}
                  value={price}
                  precision={2}
                  onKeyDown={(evt: any) => {
                    priceKeyboardEnabled = evt.keyCode == 38 || evt.keyCode == 40 ? false : true
                  }}
                  // onChange={(price) => handleChangeItemPrice({ record, type: 'price', price })}
                  onBlur={(e) =>
                    handleChangeItemPrice({ record, type: 'price', price: e.target.value.replace('$', '') })
                  }
                  // onBlur={(evt: any) =>
                  //   updateOrderItem(record.wholesaleOrderItemId, 'price', record.pas, null, evt.target.value || 0)
                  // }
                  formatter={(value) => `$${value}`}
                  onFocus={(e) => e.target.select()}
                  parser={(value) => value.replace('$', '')}
                  readOnly={addingCartItem}
                  disabled={record.status === 'SHIPPED' || readOnly} //don't need to addingCartItem flag because wholesaleOrderItem is is empty when order item is created
                  addonBefore={'$'}
                />
              </div>
            </Tooltip>
            <Unit style={{ flex: 1, minWidth: 50 }}>/{pricingUOM}</Unit>
            <Icon
              type="bar-chart"
              style={{ fontSize: 20, marginRight: 5 }}
              className="price-setting"
              onClick={() => openModal('priceModal', record)}
            />
            {!readOnly && (
              <Icon
                style={{}}
                type="setting"
                className="price-setting"
                onClick={() => {
                  openModal('itemPriceModal', record)
                }}
              />
            )}
          </Flex>
        )
      },
    },
    {
      title: 'Total Price',
      dataIndex: 'totalPrice',
      // width: 10,
      // sorter: (a: any, b: any) => a.catchWeightQty - b.catchWeightQty,
      render: (data: number, record: any) => {
        if (_.isEmpty(record)) {
          return { props: { colSpan: 0 } }
        }
        let showValue = false
        let total = 0

        if (record.picked || record.catchWeightQty) {
          showValue = true
        }

        let constantRatio = judgeConstantRatio(record)
        let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
        let recordPrice = record.price
        // if (record.rowLoading) {
        //   // Fix: When the user change the uom to the none-base UOM, and change the price, the total price is wrong firstly, then change to the correct price finally
        //   recordPrice = ratioPriceToBasePrice(pricingUOM, record.price, record, 6)
        // }
        let ratioPrice = basePriceToRatioPrice(pricingUOM, recordPrice, record, 12)

        if (record.oldCatchWeight) {
          total = numberMultipy(recordPrice, record.catchWeightQty)
        } else {
          if (!constantRatio) {
            total = numberMultipy(ratioPrice, inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record, 12))
          } else {
            total = numberMultipy(recordPrice, record.catchWeightQty)
          }
        }
        return (
          <ThemeSpin spinning={record.rowLoading === true ? true : false}>
            <ThemeInputNumber
              className={`no-stepper ${record.status === 'SHIPPED' ? 'readonly' : ''}`}
              style={{ textAlign: 'right', minWidth: 85 }}
              step={0.01}
              value={total}
              precision={2}
              // onChange={(evt: any) => onChangeItem(record.wholesaleOrderItemId, 'price', evt, record.pas)}
              onFocus={(e) => e.target.select()}
              onBlur={(evt) => updatePriceFromTotal(evt, record)}
              formatter={(value) => `$${value}`}
              parser={(value) => value.replace('$', '')}
              readOnly={addingCartItem}
              disabled={
                record.status === 'SHIPPED' ||
                readOnly ||
                (!record.oldCatchWeight && !record.catchWeightQty && record.catchWeightQty == 0)
              }
            />
            {/* {showValue && total ? `$${mathRoundFun(Math.abs(total), 2)}` : ''} */}
          </ThemeSpin>
        )
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      // sorter: (a: any, b: any) => onSortString('status', a, b),
      render: (status: string, record: any) => {
        if (_.isEmpty(record)) {
          return { props: { colSpan: 0 } }
        }

        const statues = ['New', 'Picking', 'Shipped']
        if (isDisablePickingStep) {
          statues.splice(1, 1)
        }

        let formattedStatus = status
        if (status === 'PLACED') {
          formattedStatus = 'NEW'
        }
        if (isDisablePickingStep && status === 'PICKING') {
          formattedStatus = 'NEW'
        }

        return (
          <div className="select-container-parent" style={{ width: 100 }}>
            {record.status != 'SHIPPED' ? (
              <ThemeSelect
                onChange={(val: any) => onSelectChange(record, record.wholesaleOrderItemId, 'status', val)}
                className={`status-selection orderItemId-${record.wholesaleOrderItemId}`}
                value={formattedStatus}
                suffixIcon={<Icon type="caret-down" />}
                style={{ width: '100%', borderRadius: 20 }}
                disabled={record.status === 'SHIPPED' || readOnly}
                dropdownRender={(el: any) => {
                  return <div className="sales-cart-status-selector">{el}</div>
                }}
              >
                {statues.map((el: string, index: number) => {
                  return (
                    <Select.Option value={el.toUpperCase()} key={index} disabled={el.toUpperCase() == 'SHIPPED'}>
                      <Flex className="v-center">
                        <div className={`status-indicator ${el.toLowerCase()}`} />
                        <div className="value">{el == 'Picking' ? 'Picked' : el}</div>
                      </Flex>
                    </Select.Option>
                  )
                })}
              </ThemeSelect>
            ) : (
              <div className="status-selection not-selector">
                <Flex className="v-center">
                  <div className={`status-indicator shipped`} />
                  <div className="value">Shipped</div>
                </Flex>
              </div>
            )}
          </div>
        )
      },
    },
  ]

  /**
   * if configurable option unchecked show it
   */
  const configurableOption: any[] = [
    {
      title: (
        <>
          Picked Qty
          {sellerSetting && sellerSetting.company && !sellerSetting.company.warehousePickEnabled && (
            <Popconfirm
              title={
                <div style={{ width: 200 }}>
                  Auto-fill picked quantities to match the order quantities (non-catchweight items only).
                </div>
              }
              okText="Auto-fill quantities"
              onConfirm={() => confirmCopyOrderQty()}
            >
              <Icon type="copy" style={ItemHMargin4} className="copy-order-qty" />
            </Popconfirm>
          )}
        </>
      ),
      dataIndex: 'picked',
      // width: 150,
      // sorter: (a: any, b: any) => a.picked - b.picked,
      render: (picked: number, record: any) => {
        if (_.isEmpty(record)) {
          return { props: { colSpan: 0 } }
        }
        // const recordWeights = catchWeightValues[record.wholesaleOrderItemId]
        // let weightCounts = recordWeights ? recordWeights.length : ''

        let tooltipText = ''
        if (record.picked !== null && record.picked > 0 && record.quantity !== null && record.quantity >= 0) {
          if (parseFloat(record.picked) > parseFloat(record.quantity)) {
            tooltipText = `Overpicked by ${record.picked - record.quantity} ${record.UOM}`
          } else if (record.picked < record.quantity) {
            tooltipText = `Short by ${(parseFloat(record.quantity) - parseFloat(record.picked)).toFixed(2)} ${
              record.UOM
            }`
          }
        }
        const warningClass = tooltipText ? 'warning' : ''
        let showCatchWeightModal = false
        let constantRatio = judgeConstantRatio(record)
        // let constantRatio = false
        let customBtn = null
        // const parsingFailed = localStorage.getItem(`PARSING_FAILED_${record.wholesaleOrderItemId.toString()}`)
        const parsingFailed = false
        if (!constantRatio) {
          showCatchWeightModal = true
          customBtn = (
            <CustomButton
              style={{ width: 70, textAlign: 'right', paddingRight: 11, flex: 'unset' }}
              className={`${record.picked ? '' : 'empty'} ${warningClass} ${parsingFailed ? 'warning' : ''}`}
              onClick={openModal.bind(this, 'pickedQuantityModal', record)}
              disabled={record.status === 'SHIPPED' || readOnly}
            >
              {record.picked ? record.picked : <span>&nbsp;</span>}
            </CustomButton>
          )
        }

        if (showCatchWeightModal && parsingFailed) {
          if (tooltipText) {
            tooltipText += ', '
          }
          tooltipText += 'Scanning errors found!'
        }
        if (sellerSetting && sellerSetting.company && sellerSetting.company.warehousePickEnabled) {
          return (
            <Flex className="v-center">
              <div>
                {picked === null ||
                typeof picked == 'undefined' ||
                (picked == 0 && (record.status == 'PLACED' || record.status == 'NEW'))
                  ? ''
                  : picked}
              </div>
              <Unit>{record.UOM}</Unit>
              {tooltipText && (
                <Tooltip placement="top" title={tooltipText}>
                  <div className="anticon-warning">
                    <Icon type="warning" />
                  </div>
                </Tooltip>
              )}
            </Flex>
          )
        } else {
          return (
            <Flex className="v-center">
              <Tooltip placement="top" title={tooltipText}>
                <div style={{ width: 70 }}>
                  {showCatchWeightModal ? (
                    customBtn
                  ) : (
                    <ThemeInputNumber
                      disabled={record.status === 'SHIPPED' || readOnly || updating}
                      readOnly={addingCartItem}
                      style={{ textAlign: 'right', marginRight: 4 }}
                      className={`${warningClass} no-stepper`}
                      step={1}
                      value={
                        picked === null ||
                        typeof picked == 'undefined' ||
                        (picked == 0 && (record.status == 'PLACED' || record.status == 'NEW'))
                          ? ''
                          : picked
                      }
                      // onChange={(value) => handleChangeItem({ record, type: 'picked', editType: 'isUpdatePickedQty', value })}
                      onBlur={(e) =>
                        handleChangeItem({
                          record,
                          type: 'picked',
                          editType: 'isUpdatePickedQty',
                          value: e.target.value || 0,
                        })
                      }
                      onFocus={(e) => e.target.select()}
                      // onBlur={(e: any) =>
                      //   updateOrderItem(record.wholesaleOrderItemId, 'picked', record.pas, null, e.target.value)
                      // }
                    />
                  )}
                </div>
              </Tooltip>
              <Unit>{record.UOM}</Unit>
            </Flex>
          )
        }
      },
    },
    {
      title: 'Billable Qty',
      dataIndex: 'catchWeightQty',
      // width: 200,
      // sorter: (a: any, b: any) => a.billableQuantity - b.billableQuantity,
      render: (weight: number, record: any) => {
        if (_.isEmpty(record)) {
          return { props: { colSpan: 0 } }
        }

        let showBillableQty,
          showValue = false
        if (record.picked || record.catchWeightQty) {
          showValue = true
        }

        let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
        let unitUOM = record.UOM

        let constantRatio = judgeConstantRatio(record)
        if (record.oldCatchWeight) {
          showBillableQty = record.catchWeightQty
        } else {
          if (!constantRatio) {
            showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
          } else {
            showBillableQty = inventoryQtyToRatioQty(
              pricingUOM,
              ratioQtyToInventoryQty(unitUOM, record.picked || 0, record, 12),
              record,
            )
          }
        }

        return (
          <Flex className="v-center">
            <div style={{ textAlign: 'right', minWidth: 60 }}>{showValue ? mathRoundFun(showBillableQty, 4) : ''}</div>
            <Unit style={{ flex: 'unset' }}>{pricingUOM}</Unit>
          </Flex>
        )
      },
    },
  ]
  if (isDisablePickingStep === false) {
    commonColumns.splice(1, 0, ...configurableOption)
  }
  const initialColumn: any[] = isFreshGreen
    ? [...freshGreenColumns, ...commonColumns]
    : [...otherColumns, ...commonColumns]

  const [columns, setColumns] = React.useState(initialColumn)

  React.useEffect(() => {
    setColumns(isFreshGreen ? [...freshGreenColumns, ...commonColumns] : [...otherColumns, ...commonColumns])
  }, [isDisablePickingStep])

  const components = {
    body: {
      row: BodyRow,
    },
    header: {
      row: HeaderRow,
      cell: ResizeableTitle,
    },
  }

  const noDragComponents = {
    body: {
      row: noDragBodyRow,
    },
    header: {
      row: HeaderRow,
      cell: ResizeableTitle,
    },
  }

  const handleResize = (index: number) => (e: any, { size }) => {
    if (index < 1) return
    const newColumns = [...columns]
    newColumns[index] = {
      ...newColumns[index],
      width: !isNaN(size.width) ? size.width : '',
    }

    setColumns(newColumns)
  }

  const moveRow = (dragIndex: number, hoverIndex: number) => {
    setDragIndex(dragIndex)
    setHoverIndex(hoverIndex)
  }

  const dropRow = (item) => {
    // const dragRow = data[dragIndex]
    // if (dragIndex > hoverIndex) {
    //   setData(
    //     update(data, {
    //       $splice: [
    //         [dragIndex, 1],
    //         [hoverIndex, 0, dragRow],
    //       ],
    //     }),
    //   )
    // } else if (hoverIndex > 0) {
    //   setData(
    //   update(data, {
    //     $splice: [
    //       [dragIndex, 1],
    //       [hoverIndex - 1, 0, dragRow],
    //     ],
    //   }),
    //   )
    // }
    const newCoverIndex = (currentPage - 1) * pageSize + hoverIndex
    if (props.dataSource[newCoverIndex].displayOrderProduct === item.displayOrder) return
    props.startUpdating()
    const newDisplayOrder =
      newCoverIndex > dragIndex
        ? props.dataSource[newCoverIndex].displayOrderProduct - 1
        : props.dataSource[newCoverIndex].displayOrderProduct
    updateSortOrderItemDisplayOrder({
      orderId: props.currentOrder.wholesaleOrderId,
      productId: item.itemId,
      oldDisplayOrder: item.displayOrder,
      newDisplayOrder,
    })
    // setDragIndex(-1)
    // setHoverIndex(-1)
    //update display order in database
  }

  const endDragging = () => {
    // setDragIndex(-1)
    // setHoverIndex(-1)
  }

  const finalColumns = columns.map((col, index) => ({
    ...col,
    onHeaderCell: (column) => ({
      width: column.width,
      onResize: handleResize(index + 1),
    }),
  }))

  const AddItemButton = (
    <ThemeButton
      onClick={props.openAddModal}
      className="sales-cart-input add-item add-sales-cart-item header-last-tab bold-blink"
      style={{ margin: '30px auto' }}
    >
      <Icon type="plus-circle" />
      Add an item
    </ThemeButton>
  )

  if (props.draggable === false) {
    const filteredItems = data.filter((el: any) => {
      if (el.quantity > 0) {
        // Order Qty > 0
        return true
      }

      let pickedQty = 0
      const constantRatio = judgeConstantRatio(el)
      if (constantRatio === false) {
        const recordWeights = catchWeightValues[el.wholesaleOrderItemId]
        pickedQty = recordWeights ? recordWeights.length : 0
      } else {
        pickedQty = el.picked
      }

      if (pickedQty > 0) {
        // Picked Qty > 0
        return true
      }

      return false
    })

    return (
      <ThemeSpin spinning={updating}>
        <Table
          className={isFreshGreen ? 'fresh-green' : 'other'}
          pagination={false}
          dataSource={filteredItems}
          columns={finalColumns}
          rowKey={rowKey}
          components={noDragComponents}
          footer={() => (readOnly ? null : footer)}
          onRow={(record: OrderItem, index: number) => ({
            index,
            record,
            tableHoverIndex: hoverIndex,
            wholesaleOrderItemId: record.wholesaleOrderItemId,
            selectedRows: selectedRows,
            renderFloatingMenu: renderFloatingMenu,
          })}
          locale={{
            emptyText: ' ',
          }}
        />
      </ThemeSpin>
    )
  }

  if (readOnly) {
    return (
      <ThemeSpin spinning={updating}>
        <Table
          className={isFreshGreen ? 'fresh-green' : 'other'}
          pagination={{ pageSize, hideOnSinglePage: true }}
          dataSource={data}
          columns={finalColumns}
          rowKey={rowKey}
          components={noDragComponents}
          footer={() => null}
          onRow={(record: OrderItem, index: number) => ({
            index,
            record,
            tableHoverIndex: hoverIndex,
            wholesaleOrderItemId: record.wholesaleOrderItemId,
            selectedRows: selectedRows,
            renderFloatingMenu: renderFloatingMenu,
          })}
          locale={{
            emptyText: ' ',
          }}
        />
      </ThemeSpin>
    )
  }

  const onTablePageChange = (page: number) => {
    setCurrentPage(page)
  }

  const isShipped = validateOrderStatusIsShipped()
  return (
    <ThemeSpin spinning={updating || props.simplifyItems.length === 0 || props.updateOrderQuantityLoading.loading}>
      <DndProvider backend={HTML5Backend}>
        <Table
          className={`${hoverIndex > -1 ? 'drag-table' : ''} ${isFreshGreen ? 'fresh-green' : 'other'}`}
          pagination={{ pageSize, hideOnSinglePage: true, onChange: onTablePageChange, current: currentPage }}
          dataSource={isShipped || !isFreshGreen ? data : [...data, ...{}]}
          columns={finalColumns}
          rowKey={rowKey}
          components={components}
          footer={() => (isFreshGreen || isShipped ? null : footer)}
          onRow={(record: OrderItem, index: number) => ({
            index,
            record,
            tableHoverIndex: hoverIndex,
            displayOrder: record.displayOrderProduct,
            itemId: record.itemId,
            wholesaleOrderItemId: record.wholesaleOrderItemId,
            selectedRows: selectedRows,
            renderFloatingMenu: renderFloatingMenu,
            moveRow,
            dropRow,
            endDragging,
            updateSortOrderItemDisplayOrder,
          })}
          locale={{
            emptyText: ' ',
          }}
        />
      </DndProvider>
    </ThemeSpin>
  )
}
