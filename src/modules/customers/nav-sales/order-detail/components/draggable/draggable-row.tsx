import React, { useEffect, useRef } from "react"
import { Dropdown } from "antd"
import { useDrag, useDrop } from "react-dnd"
import { getEmptyImage } from "react-dnd-html5-backend"
import _ from "lodash"

import { Icon as IconSVG } from '~/components'
import { FloatingMenu } from "../../../styles"

export function BodyRow(props: any) {
  const {
    isOver,
    connectDragSource,
    connectDropTarget,
    moveRow,
    dropRow,
    selectedRows,
    renderFloatingMenu,
    index,
    displayOrder,
    itemId,
    wholesaleOrderItemId,
    record,
    tableHoverIndex,
    updateSortOrderItemDisplayOrder,
    ...restProps
  } = props
  let { className } = restProps
  if (tableHoverIndex == index) {
    className += ' on-hover-row-bottom'
  } else if (tableHoverIndex - 1 == index && tableHoverIndex > 0) {
    className += ' on-hover-row-up'
  } else {
    className += ''
  }
  const [{ isDragging }, drag, preview] = useDrag({
    item: { type: 'row', index, displayOrder, itemId },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  })

  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true })
  }, [])

  const dropRef = useRef(null)
  const dragRef = useRef(null)

  const [, drop] = useDrop({
    accept: 'row',
    hover(item: any, monitor) {
      if (!dropRef.current) {
        return
      }
      const dragIndex = item.index

      const hoverIndex = index
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }

      // Determine rectangle on screen
      const refCurrent: any = dropRef.current
      const hoverBoundingRect = refCurrent.getBoundingClientRect()
      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
      // Determine mouse position
      const clientOffset: any = monitor.getClientOffset()
      // Get pixels to the top
      const hoverClientY = clientOffset?.y - hoverBoundingRect.top
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }
      // Time to actually perform the action
      moveRow(dragIndex, hoverIndex)
    },
    drop: (item) => {
      dropRow(item)
    },
  })

  drop(dropRef)
  drag(dragRef)

  if (_.isEmpty(record)) {
    return (
      <tr className={`${className} ${selectedRows.includes(record.displayOrderProduct) ? 'selected-row' : ''}`}>
        {/* <td /> */}
        {restProps.children}
      </tr>
    )
  }
  const dragger = restProps.children[0]
  const rest = restProps.children.map((el: any, index: number)=> { if(index > 0) return el })
  if(!dragger) return null

  return (
    <tr
      ref={dropRef}
      className={`${className} ${selectedRows.includes(record.displayOrderProduct) ? 'selected-row' : ''} ${isDragging ? 'dragging' : ''}`}
      data-row-key={wholesaleOrderItemId}
    >
      {/* <>
        {dragger}
      </> */}
      <td ref={dragRef} width={20} {...dragger.props}>
        <div className="drag-handler">
          <IconSVG type="draggable" viewBox="0 0 20 24" width={16} height={20} />
        </div>
      </td>
      {rest}
      <Dropdown overlay={renderFloatingMenu(props.wholesaleOrderItemId, props.record)} placement="bottomLeft" trigger={['click']}>
        <FloatingMenu className="floating-menu-body">
          <IconSVG type="ellipsis" viewBox="0 0 16 4" width={16} height={8} />
        </FloatingMenu>
      </Dropdown>
    </tr>
  )
}

export function noDragBodyRow(props: any) {
  const { selectedRows, wholesaleOrderItemId, renderFloatingMenu, record, ...restProps } = props
  let { className } = restProps
  const rest = restProps.children.map((el: any, index: number)=> { if(index > 0) return el })
  return (
    <tr
      className={`${className} 
      ${selectedRows.includes(record.displayOrderProduct) ? 'selected-row' : ''}`}
      data-row-key={wholesaleOrderItemId}
    >
      <td />
      {rest}
      <Dropdown overlay={renderFloatingMenu(props.itemId, props.record)} placement="bottomLeft" trigger={['click']}>
        <FloatingMenu className="floating-menu-body">
          <IconSVG type="ellipsis" viewBox="0 0 16 4" width={16} height={8} />
        </FloatingMenu>
      </Dropdown>
    </tr>
  )
}

export function HeaderRow(props: any) {
  return (
    <tr>
      {/* <th /> */}
      {props.children}
    </tr>
  )
}