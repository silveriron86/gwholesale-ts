import React, { PureComponent } from 'react'
import { Form, Popconfirm, Select, InputNumber, Divider, Button, Modal, Input } from 'antd'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { SettingsProps, SettingsModule } from '../../../../settings/settings.module'
import { textCenter, ThemeButton, ThemeIcon } from '~/modules/customers/customers.style'
import { OrderItem } from '~/schema'
import { EditableEl, StyleTable } from './extra-charges-table.style'
import { CACHED_QBO_LINKED } from '~/common'
import _ from 'lodash'

const { Option } = Select

type IProps = SettingsProps & {
  dataSource: any[]
  handleSave: (record: OrderItem) => void
  editable?: boolean
  addOne: (name?: string) => void
  deleteRow: (id: number) => void
  pagination: any
}

interface IState {
  modelVisible: boolean
  chargeValue: string
}

class ExtraChargeTable extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      modelVisible: false,
      chargeValue: '',
    }
  }

  componentDidMount() {
    const { getCompanyProductAllTypes } = this.props
    getCompanyProductAllTypes()
  }

  get extraChargeColumn() {
    return [
      {
        title: 'Charge type',
        dataIndex: 'itemName',
        key: 'itemName',
        align: 'left',
        width: '25%',
        editable: this.props.editable,
        sorter: (a: any, b: any) => a.itemName.localeCompare(b.itemName),
      },
      {
        title: 'Description',
        dataIndex: 'chargeDesc',
        key: 'chargeDesc',
        align: 'left',
        editable: this.props.editable,
        sorter: (a: any, b: any) => a.itemName.localeCompare(b.itemName),
        render: (t: any, r: any) => `${t && t.toLowerCase() != 'null' ? t : ''}${r.note ? ` - ${r.note}` : ''}`,
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity',
        align: 'center',
        key: 'unit',
        width: 150,
        type: 'number',
        editable: this.props.editable,
        sorter: (a: any, b: any) => a.quantity - b.quantity,
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        width: 190,
        editable: this.props.editable,
        sorter: (a: any, b: any) => a.price - b.price,
        render: (value: string, record: any) => {
          return (
            <div style={textCenter}>
              ${Number(value).toFixed(2)}/{record.UOM}
            </div>
          )
        },
      },
      {
        title: 'Total Price',
        dataIndex: 'item_total',
        align: 'left',
        key: 'item_total',
        width: 240,
        sorter: (a: any, b: any) => a.item_total - b.item_total,
        render: (data: number, record: any, index: number) => {
          const extended = Number(record.price ? record.price : 0) + Number(record.freight ? record.freight : 0)
          let total = 0
          total = extended * record.quantity
          const prefix = total >= 0 ? '' : '-'
          return (
            <>
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                ${total.toFixed(2)}
                {this.props.editable && <Popconfirm
                  title="Permanently delete this one related record?"
                  okText="Delete"
                  onConfirm={() => this.props.deleteRow(record.wholesaleOrderItemId)}
                >
                  <ThemeIcon type="delete" className="delete-btn" />
                </Popconfirm>}
              </div>
            </>
          )
        },
      },
    ]
  }

  handleOk = () => {
    const { chargeValue } = this.state
    const { setCompanyProductType } = this.props
    setCompanyProductType({
      type: 'extraCharges',
      useFor: location.href.includes('sales-order') ? 1 : 2,
      name: chargeValue,
    })
    this.props.addOne(chargeValue)
    this.setState({ modelVisible: false, chargeValue: '' })
  }

  handleCancel = () => {
    this.setState({ modelVisible: false, chargeValue: '' })
  }

  showModal = (show: boolean) => {
    this.setState({ modelVisible: show })
  }

  render() {
    const { dataSource, handleSave, companyProductTypes, addOne, pagination, sellerSetting, editable } = this.props
    const { modelVisible, chargeValue } = this.state
    const extraCharges = (companyProductTypes?.extraCharges ?? []).sort((a: any, b: any) => a.name.localeCompare(b.name))

    const isQBOEnabled = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    const useFor = location.href.includes('sales-order') ? 1 : 2

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    }
    const columns = this.extraChargeColumn.map((col) => {
      if (!col.editable) {
        return col
      }
      return {
        ...col,
        onCell: (record: any) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave,
          extraCharges: companyProductTypes?.extraCharges ?? [],
          sellerSetting,
          showModal: this.showModal,
        }),
      }
    })

    return (
      <>
        <StyleTable
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
          pagination={pagination}
          footer={() => {
            return (
              <>
                <Select
                  placeholder="Select type..."
                  style={{ width: '220px' }}
                  onChange={(value?: string) => {
                    // value && addOne(value)
                    // console.log(this.state)
                    this.setState({ modelVisible: false, chargeValue: value })
                  }}
                  value={this.state.chargeValue || undefined}
                  disabled={!this.props.editable}
                  dropdownRender={(menu) => (
                    <>
                      {menu}
                      {isQBOEnabled && !sellerSetting.nsRealmId && (
                        <>
                          <Divider style={{ margin: '4px 0' }} />
                          <div onMouseDown={(e) => e.preventDefault()}>
                            <Button type="link" icon="plus" className="delete-btn" onClick={() => this.showModal(true)}>
                              Add new charge type...
                            </Button>
                          </div>
                        </>
                      )}
                    </>
                  )}
                >
                  {extraCharges
                    .filter((e) => e.useFor === useFor)
                    .map((type: any) => (
                      <Option value={type.name}>{type.name}</Option>
                    ))}
                </Select>
                <ThemeButton
                  disabled={!this.state.chargeValue}
                  style={{ marginLeft: 10 }}
                  onClick={() => this.props.addOne(this.state.chargeValue)}
                >
                  Add charge
                </ThemeButton>
              </>
            )
          }}
        />
        <Modal title="Add New Charge Type" visible={modelVisible} onOk={this.handleOk} onCancel={this.handleCancel}>
          <Input
            placeholder="Enter extra charge type"
            value={chargeValue}
            onChange={(e) => {
              this.setState({ chargeValue: e.target.value })
            }}
          />
        </Modal>
      </>
    )
  }
}

const EditableContext = React.createContext({})

const EditableRow = ({ form, index, ...props }: any) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
)

const EditableFormRow = Form.create()(EditableRow)

class EditableCell extends PureComponent<any, { editing: boolean; descValue: string; inputNumber: number }> {
  state = {
    editing: false,
    descValue: '',
    inputNumber: 0,
  }

  toggleEdit = () => {
    const editing = !this.state.editing
    this.setState({ editing })
  }

  handleChange = (key: string, value: string | number) => {
    const { record, handleSave } = this.props
    const row = { ...record }
    row[key] = value
    handleSave(row)
  }

  renderFromSwitch = () => {
    const { record, title, showModal, extraCharges, sellerSetting } = this.props
    const isQBOEnabled = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    const useFor = location.href.includes('sales-order') ? 1 : 2

    if (title === 'Charge type') {
      return (
        <Select
          onChange={(value: string) => {
            this.handleChange('itemName', value)
          }}
          placeholder="Select type..."
          style={{ width: '100%' }}
          dropdownRender={(menu) => (
            <>
              {menu}
              {isQBOEnabled && !sellerSetting.nsRealmId && (
                <>
                  <Divider style={{ margin: '4px 0' }} />
                  <div onMouseDown={(e) => e.preventDefault()}>
                    <Button type="link" icon="plus" className="delete-btn" onClick={() => this.showModal(true)}>
                      Add new charge type...
                    </Button>
                  </div>
                </>
              )}
            </>
          )}
        >
          {extraCharges
            .filter((e) => e.useFor === useFor)
            .map((type: any) => (
              <Option value={type.name}>{type.name}</Option>
            ))}
        </Select>
      )
    }

    if (title === 'Description') {
      return (
        <Input
          defaultValue={record?.chargeDesc || ''}
          onChange={(e) => {
            const value = e.target.value
            this.setState({ descValue: value })
          }}
          onFocus={(e) => e.target.select()}
          autoFocus
          onBlur={(e) => {
            const value = e.target.value.trim()
            this.handleChange('chargeDesc', value)
            this.setState({ editing: false })
          }}
        />
      )
    }

    if (title === 'Quantity') {
      return (
        <InputNumber
          defaultValue={record.quantity}
          onChange={(value: number | undefined) => {
            this.setState({ inputNumber: value })
          }}
          autoFocus
          onFocus={(e) => e.target.select()}
          onBlur={(e) => {
            const value = e.target.value.trim()
            this.setState({ editing: false })
            this.handleChange('quantity', value)
          }}
          min={0}
        />
      )
    }

    if (title === 'Price') {
      return (
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <InputNumber
            defaultValue={record.price}
            onChange={(value: number | undefined) => {
              this.setState({ inputNumber: value })
            }}
            onFocus={(e) => e.target.select()}
            autoFocus
            onBlur={(e) => {
              const value = e.target.value.trim()
              this.setState({ editing: false })
              this.handleChange('price', value)
            }}
          // min={0}
          />
          /each
        </div>
      )
    }
  }

  renderChildren = () => {
    const { title, children } = this.props
    const editableTitles = ['Charge type', 'Description']
    return editableTitles.includes(title) ? <EditableEl>{children}</EditableEl> : children
  }

  renderCell = () => {
    const { editing } = this.state
    return editing ? this.renderFromSwitch() : <div onClick={this.toggleEdit}>{this.renderChildren()}</div>
  }

  render() {
    const { editable, dataIndex, title, record, index, handleSave, children, ...restProps } = this.props
    return (
      <td {...restProps}>
        {editable ? <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer> : children}
      </td>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.settings

export default connect(SettingsModule)(mapStateToProps)(ExtraChargeTable)
