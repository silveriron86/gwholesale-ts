import styled from '@emotion/styled'
import { Table } from 'antd'

export const EditableEl = styled.div({
  padding: '5px 12px',
  cursor: 'pointer',
  minHeight: '30px',

  '&:hover': {
    border: '1px solid #d9d9d9',
    borderRadius: '4px',
    padding: '4px 11px',
  },
})

export const StyleTable = styled(Table)({
  '.ant-table-bordered .ant-table-thead > tr > th, .ant-table-bordered .ant-table-tbody > tr > td': {
    borderRight: '1px solid #EDF1EE',
  },

  '.delete-btn': {
    fontSize: '24px',
  },
})
