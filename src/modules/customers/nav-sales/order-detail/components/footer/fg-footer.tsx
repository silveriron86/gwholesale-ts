import React, { useState } from 'react'
import { ThemeInput } from '~/modules/customers/customers.style'
import { OrderDetail } from '~/schema'
import { Flex } from '../../../styles'
import { AddAutocomplete } from '../../tabs/add-autocomplete'

export interface FooterProps {
  currentOrder: OrderDetail
  sellerSetting: any
  orderItems: any[]
  orderItemByProduct: any
  simplifyItems: any[]
  simplifyItemsByOrderId: any[]
  addingCartItem: boolean
  isDisablePickingStep: boolean
  updateOrderItemByProductId: Function
  updateOrderItemPrice: Function
  updateSortOrderItemDisplayOrder: Function
}

export default function FGFooter(props: FooterProps) {
  const [initialQuantity, setInitialQuantity] = useState('')
  const { 
    addingCartItem,
    sellerSetting,
    simplifyItemsByOrderId,
    simplifyItems,
    isDisablePickingStep,
    currentOrder
   } = props
   
  return (    
    <Flex className='v-center'>
      <div className="name-column order-quantity-input" style={{padding: '0 12px 0 40px'}}>      
        <ThemeInput
          className={`order-qty`}
          value={initialQuantity}
          readOnly={addingCartItem}
          disabled={currentOrder.wholesaleOrderStatus === 'SHIPPED' || currentOrder.wholesaleOrderStatus === 'CANCEL'}
          style={{ marginRight: 4, width: 60, textAlign: 'right' }}
          onChange={(evt: any) => {
            const { value } = evt.target
            const reg = /^-?\d*(\.\d*)?$/
            if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
              setInitialQuantity(value)
            }
          }}
        />
      
      </div>
      <div className="footer-add-item name-column">
        <AddAutocomplete
            {...props}
            isDisablePickingStep={isDisablePickingStep}
            quantity={initialQuantity}
            callBack={() => setInitialQuantity('')}
            simplifyItems={
              sellerSetting?.company?.enableAddItemFromProductList ? simplifyItemsByOrderId : simplifyItems
            }
          />
      </div>
    </Flex>
  )
}