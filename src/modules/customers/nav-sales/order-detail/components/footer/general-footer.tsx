import React from 'react'
import { AddAutocomplete } from '../../tabs/add-autocomplete'

export default function GeneralFooter(props: any) {
  const { 
    sellerSetting,
    dataSource,
    simplifyItemsByOrderId,
    simplifyItems
   } = props
  return (    
    <div>
      <div className="footer-add-item" key={`add-ac-${dataSource.length}`}>
        <AddAutocomplete {...props} simplifyItems={sellerSetting?.company?.enableAddItemFromProductList? simplifyItemsByOrderId : simplifyItems} />
      </div>
    </div>
  )
}