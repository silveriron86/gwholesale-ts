import * as React from 'react'
import { Icon, Select, DatePicker, TimePicker } from 'antd'
import { FlexDiv, Item, PaddingContainer } from '../../styles'
import { BlinkBorderText, DetailLabel } from '~/modules/customers/sales/_style'
import { ShippingPopoverModal } from '~/modules/vendors/purchase-orders/_components/shipping-popover'
import { datePickerEscKeyupEvent } from '~/common/jqueryHelper'
import { Flex, ThemeIcon, ThemeSelect } from '~/modules/customers/customers.style'
import moment from 'moment'
import { formatAddress } from '~/common/utils'
import OrderDetails from '../../../../vendors/purchase-orders/order-details'
import { CACHED_USER_ID, CACHED_COMPANY } from '~/common'

const calendarIcon = <ThemeIcon type="calendar" />

type OrderInfoProps = { propsValue: any }
export class HeaderOrderDetail extends React.PureComponent<OrderInfoProps> {
  state: any
  quotedDatePicker:
    | string
    | ((instance: ClassicComponent<DatePickerProps, any> | null) => void)
    | RefObject<ClassicComponent<DatePickerProps, any>>
    | null
    | undefined
  constructor(props: OrderInfoProps) {
    super(props)
    const { order } = props
    this.state = {
      isCollapsed: true,
      orderDatePicker: false,
      quotedDatePicker: false,
      scheduledDatePicker: false,
      shippingModalVisible: false,
      purchaser: order.purchaser ? order.purchaser.userId : null,
      receiver: order.receiver ? order.receiver.userId : null,
    }
    console.log(this.props.propsValue)
  }

  componentDidMount() {
    const handleEscClick = (event: any) => {
      if (event.keyCode == 27) {
        this.setState({
          orderDatePicker: false,
          quotedDatePicker: false,
          scheduledDatePicker: false,
        })
      }
    }

    datePickerEscKeyupEvent(handleEscClick)
  }

  componentWillReceiveProps(nextProps: OrderInfoProps) {
    if(JSON.stringify(this.props.order) !== JSON.stringify(nextProps.order)) {
      this.setState({
        purchaser: nextProps.order.purchaser ? nextProps.order.purchaser.userId : null,
        receiver: nextProps.order.receiver ? nextProps.order.receiver.userId : null,
      })
    }
  }

  handleCollapse = () => {
    this.setState({ isCollapsed: !this.state.isCollapsed })
  }

  handleShippingVisibleChange = () => {
    this.setState({
      shippingModalVisible: !this.state.shippingModalVisible,
      accountNames: [],
    })
  }

  onDatePickerOpenChange = (type: number, open: boolean) => {
    switch (type) {
      case 1:
        this.setState({ orderDatePicker: open })
        break
      case 2:
        this.setState({ quotedDatePicker: open })
        break
      case 3:
        this.setState({ scheduledDatePicker: open })
        break
    }
  }

  onDateChange = (type, _dateMoment, _dataString) => {
    const { currentOrder } = this.props
    if (currentOrder && _dataString != null) {
      let data = {}
      data['wholesaleOrderId'] = currentOrder.wholesaleOrderId
      if (type == 1) data['orderDate'] = _dataString
      if (type == 2) data['quotedDate'] = _dataString
      if (type == 3) data['deliveryDate'] = _dataString
      this.props.updateOrderInfo(data)
    }
  }

  onScheduledDeliveryTimeChange = (time: any, timeString: string) => {
    const { currentOrder } = this.props
    let data: any = {}
    data['wholesaleOrderId'] = currentOrder.wholesaleOrderId
    data['scheduledDeliveryTime'] = timeString
    this.props.updateOrderInfo(data)
  }

  render() {
    const { order, sellerSetting, currentCompanyUsers } = this.props
    const { isCollapsed, purchaser, receiver } = this.state

    const current = moment().startOf('day');
    const daysDiff = moment.duration(moment(moment(order.createdDate).format('YYYY-MM-DD')).diff(current)).asDays();

    const scheduledDeliveryTime = order.scheduledDeliveryTime ? moment(order.scheduledDeliveryTime, 'h:mm a') : moment();

    let orderDate = null;
    let quotedDate = null;
    let deliveryDate = null;

    // For old POs, PO Order Date and Quoted Delivery Date should not auto-update
    if (daysDiff >= 0) {
      orderDate = moment();
      quotedDate = moment();
      deliveryDate = moment();
    }

    if (order.orderDate) {
      orderDate = moment(order.orderDate);
    }
    if (order.quotedDate) {
      quotedDate = moment(order.quotedDate);
    }
    if (order.deliveryDate) {
      deliveryDate = moment(order.deliveryDate);
    }

    const sellerAddress =
      sellerSetting && sellerSetting.userSetting && sellerSetting.userSetting.company
        ? sellerSetting.userSetting.company.address
        : null

    const address =
      order.shippingAddress && order.shippingAddress.address && order.shippingAddress.address
        ? order.shippingAddress.address
        : sellerAddress
    const destinationName = address ? formatAddress(address) : ''
    const userId = localStorage.getItem(CACHED_USER_ID)
    const companyId = localStorage.getItem(CACHED_COMPANY)

    const orderDisabled = !order || order.isLocked || order.wholesaleOrderStatus == 'CANCEL'

    return (
      <PaddingContainer style={{ borderBottom: 0 }} className="purchase-order-header">
        <FlexDiv style={{ padding: '4px 0 8px 50px' }}>
          <Item className="left icon-item">
            <Icon type={isCollapsed ? 'caret-up' : 'caret-down'} onClick={this.handleCollapse} />
            {isCollapsed ? 'Show' : 'Hide'} Order Details
          </Item>
        </FlexDiv>
        {!isCollapsed && (
          <>
            <FlexDiv style={{ paddingBottom: 10, paddingLeft: 50 }}>
              <div style={{ width: 248, marginRight: 16 }}>
                <DetailLabel className="text-left">Purchaser</DetailLabel>
                {currentCompanyUsers && (
                  <ThemeSelect
                    value={purchaser}
                    onChange={(id) =>
                      this.setState({
                        purchaser: id
                      }, () => {
                        this.props.updateOrderInfo({ purchaser: id, wholesaleOrderId: order.wholesaleOrderId })
                      })
                    }
                    style={{ width: '100%' }}
                    disabled={orderDisabled || (companyId == 'Growers Produce' && !(userId == '110151' || userId ==  '97960' || userId == '110154'))}
                  >
                    {currentCompanyUsers.filter(v => (v.role !== 'SUPERADMIN' && v.role !== 'WAREHOUSE')).map(
                      (
                        item: { userId: string | number | undefined; firstName: string; lastName: string },
                        index: string | number | undefined,
                      ) => {
                        return (
                          <Select.Option key={index} value={item.userId}>
                            {item.firstName + '  ' + item.lastName}
                          </Select.Option>
                        )
                      },
                    )}
                  </ThemeSelect>
                )}
              </div>
              <div style={{ width: 360, marginRight: 30 }}>
                <DetailLabel className="text-left">Delivery Address</DetailLabel>
                <ShippingPopoverModal
                  order={order}
                  visible={this.state.shippingModalVisible}
                  onVisibleChange={() => {
                    if (!orderDisabled) {
                      this.handleShippingVisibleChange()
                    }
                  }}
                >
                  <BlinkBorderText value={destinationName} className="black-bottom bold-blink" />
                </ShippingPopoverModal>
              </div>
              <div className="inputDisabled" style={{ marginRight: 30 }}>
                <DetailLabel className="text-left nowrap">Order Date</DetailLabel>
                <DatePicker
                  // disabledDate={this.disabledDate}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={calendarIcon}
                  value={orderDate}
                  onChange={this.onDateChange.bind(this, 1)}
                  className="no-border black-bottom bold-blink"
                  style={{ width: 160, display: 'block' }}
                  // disabled
                  open={this.state.orderDatePicker}
                  onOpenChange={this.onDatePickerOpenChange.bind(this, 1)}
                  allowClear={false}
                  disabled={orderDisabled}
                />
              </div>
              <div className="inputDisabled" style={{ marginRight: 30 }}>
                <DetailLabel className="text-left nowrap">Quoted delivery date</DetailLabel>
                <DatePicker
                  // disabledDate={this.disabledDate}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={calendarIcon}
                  value={quotedDate}
                  onChange={this.onDateChange.bind(this, 2)}
                  className="no-border black-bottom bold-blink"
                  style={{ width: 160, display: 'block' }}
                  open={this.state.quotedDatePicker}
                  onOpenChange={this.onDatePickerOpenChange.bind(this, 2)}
                  ref={this.quotedDatePicker}
                  allowClear={false}
                  disabled={orderDisabled}
                />
              </div>
              <div className="inputDisabled">
                <DetailLabel className="text-left nowrap">Scheduled delivery date & time</DetailLabel>
                <Flex>
                  <DatePicker
                    // disabledDate={this.disabledDate}
                    placeholder={'MM/DD/YYYY'}
                    format={'MM/DD/YYYY'}
                    suffixIcon={calendarIcon}
                    value={deliveryDate}
                    onChange={this.onDateChange.bind(this, 3)}
                    className="no-border black-bottom bold-blink"
                    style={{ width: 160, display: 'block' }}
                    open={this.state.scheduledDatePicker}
                    onOpenChange={this.onDatePickerOpenChange.bind(this, 3)}
                    allowClear={false}
                    disabled={orderDisabled}
                  />
                  <TimePicker
                    value={scheduledDeliveryTime}
                    use12Hours
                    format="h:mm A"
                    className="no-border black-bottom bold-blink theme-icon"
                    onChange={this.onScheduledDeliveryTimeChange}
                    style={{ marginLeft: 8, width: 110 }}
                    allowClear={false}
                    disabled={orderDisabled}
                  />
                </Flex>
              </div>
              {/* <Col md={3}>
                <div style={{ width: '90%' }}>
                  <DetailLabel>ORDER #</DetailLabel>
                  <BlinkBorderText
                    value={order ? order.wholesaleOrderId : ''}
                    readOnly
                    className="black-bottom bold-blink"
                  />
                </div>
              </Col> */}
            </FlexDiv>

            <FlexDiv style={{ paddingTop: 10, paddingLeft: 50 }}>
              <div className="inputDisabled" style={{ width: 248, marginRight: 16 }}>
                <DetailLabel className="text-left">Receiver</DetailLabel>
                {currentCompanyUsers && (
                  <ThemeSelect
                    value={receiver}
                    onChange={(id) =>
                      this.setState({
                        receiver: id
                      }, () => {
                        this.props.updateOrderInfo({ receiver: id, wholesaleOrderId: order.wholesaleOrderId })
                      })
                    }
                    style={{ width: '100%' }}
                    disabled={orderDisabled || (companyId == 'Growers Produce' && !(userId == '116137' || userId == '110151' || userId ==  '97960' || userId == '110154'))}
                  >
                    {currentCompanyUsers.map(
                      (
                        item: { userId: string | number | undefined; firstName: string; lastName: string },
                        index: string | number | undefined,
                      ) => {
                        return (
                          <Select.Option key={index} value={item.userId}>
                            {item.firstName + '  ' + item.lastName}
                          </Select.Option>
                        )
                      },
                    )}
                  </ThemeSelect>
                )}
              </div>
            </FlexDiv>
            <OrderDetails {...this.props.propsValue} />
          </>
        )}
      </PaddingContainer>
    )
  }
}

export default HeaderOrderDetail
