import * as React from 'react'
import { AutoComplete, DatePicker, Icon, Menu, Dropdown, Select, Tooltip, Spin } from 'antd'
import moment from 'moment'
import {
  ThemeIcon,
  ThemeButton,
  ThemeOutlineButton,
  ThemeInput,
  ThemeCheckbox,
  ThemeModal,
  ThemeSelect,
  ThemeIconButton,
} from '~/modules/customers/customers.style'
import {
  BalanceLabel,
  Flex,
  FlexDiv,
  Item,
  ItemHMargin4,
  OrderInfoItem,
  OrderStatusLabel,
  PaddingContainer,
  ValueLabel,
  InputLabel,
  VerticalPadding12,
  HeaderActionMenu,
  CustomerName,
  DialogSubContainer,
  DuplicateConfirmDiv,
  BoldSpan,
  noPaddingFooter,
  WarningText,
} from './../../styles'
import { OrderItem, OrderDetail, WholesaleSalesOrderPalletLabel, AuthUser, UserRole } from '~/schema'
import { Moment } from 'moment'
import {
  calculateTotalOrder,
  calculateTotalOrderTax,
  formatNumber,
  getFulfillmentDate,
  getOrderPrefix,
  formatOrderStatus,
  isCustomOrderNumberEnabled,
  needShowDuplicateConfirmModal,
  updateDisMatchDuplicateOrderItemsForGroup,
  formatTimeDifference,
} from '~/common/utils'
import { Icon as IconSvg } from '~/components/icon/'
import { Link } from 'react-router-dom'
import { CACHED_QBO_LINKED, CACHED_USER_ID, CACHED_COMPANY } from '~/common'
import { CACHED_NS_LINKED, gray01 } from '~/common'
import { history } from '~/store/history'
import { NewOrderFormModal } from '~/modules/orders/components'
import jQuery from 'jquery'
import { ClassNames } from '@emotion/core'
import EPickSheet from '../modals/epick-sheet'
import scan_small from './../../images/scan_small.png'
import PrintPalletLabel from '../modals/print-pallet-label'
import _ from 'lodash'

const { SubMenu } = Menu

const unFormatNumber = (num: string) => parseFloat(num.replace(/[^\d.-]/g, ""))

type HeaderProps = {
  loading: boolean
  loadingCurrentOrder: boolean
  orderItems: OrderItem[]
  oneOffItems: OrderItem[]
  currentOrder: OrderDetail
  resetLoading: Function
  syncQBOOrder: Function
  syncNSOrder: Function
  duplicateOrder: Function
  updateOrderInfo: Function
  onDoHeaderActions: Function
  getQuantityCatchWeight: Function
  setOrderItemId: Function
  printSetting: any
  sellerSetting: any
  simplifyCustomers: any[]
  catchWeights: any
  catchWeight: any[]
  resetOnEPickSheet: Function
  loadingOnEPick: boolean
  updateOrder: Function
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  saveEPickedValues: Function
  updateOrderClient: Function
  salesOrderPalletLabels: WholesaleSalesOrderPalletLabel[]
  saveSalesOrderPalletLabels: Function
  getSalesOrderPalletLabels: Function
  currentUser: AuthUser
  selectedTab: string
  setCurrentPrintContainerId: Function
  getOrderContainerList: Function
  getWholesaleContainer: Function
  setIsHealthBol: Function
}

export class OrderSummary extends React.PureComponent<HeaderProps> {
  orderHeaderInputId = localStorage.getItem('ORDER-HEADER-INPUT-ID')
  state = {
    orderDate: undefined,
    deliveryDate: undefined,
    reference: '',
    showCustomerModal: false,
    createShow: false,
    enableEditOrderNo: this.orderHeaderInputId == '2' ? true : false,
    duplicateConfirmModal: false,
    showEPickSheetModal: false,
    showPalletLabelModal: false,
    customOrderNo: '',
    orderStatus: '',
    financialTerms: '',
    isReferenceDuplicate: false,
    isDuplicatedOrderNo: false,
    visibleBalance: false,
  }

  componentDidMount() {
    jQuery('html')
      // .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.altKey && e.keyCode === 78) {
          this.setState({
            createShow: true,
          })
        }
      })
    if (this.props.currentOrder && this.props.sellerSetting) {
      this.setState({
        customOrderNo: this.props.currentOrder.customOrderNo,
        enableEditOrderNo:
          this.orderHeaderInputId == '2'
            ? true
            : this.props.sellerSetting.company.visibleCustomOrderNo && this.props.currentOrder.customOrderNo,
      })
    }
    this.props.getReferenceList()
    this.props.getCustomOrderNoList()
    this.focusWhenComponentMounted()
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      if (nextProps.currentOrder && !this.state.deliveryDate) {
        this.setState({
          orderDate: nextProps.currentOrder.orderDate,
          deliveryDate: nextProps.currentOrder.deliveryDate,
          reference: nextProps.currentOrder.reference,
        })
      }
    }
    const { currentOrder } = this.props
    if (
      (!currentOrder && nextProps.currentOrder) ||
      (currentOrder && nextProps.currentOrder && currentOrder.customOrderNo != nextProps.currentOrder.customOrderNo) ||
      (!this.props.sellerSetting && nextProps.sellerSetting)
    ) {
      const { sellerSetting } = nextProps
      const isCustomerOrderEnabled =
        sellerSetting && sellerSetting.company && sellerSetting.company.visibleCustomOrderNo
      console.log('update enable order', isCustomerOrderEnabled)
      this.setState({
        customOrderNo: nextProps.currentOrder ? nextProps.currentOrder.customOrderNo : '',
        enableEditOrderNo:
          this.orderHeaderInputId == '2'
            ? true
            : isCustomerOrderEnabled && nextProps.currentOrder && nextProps.currentOrder.customOrderNo,
      })
    }

    if (nextProps.currentOrder) {
      this.setState({
        orderStatus: nextProps.currentOrder.wholesaleOrderStatus,
        financialTerms: nextProps.currentOrder.financialTerms,
      })
    }

    if (nextProps.currentOrder) {
      if (!this.props.currentOrder || (JSON.stringify(this.props.currentOrder) !== JSON.stringify(nextProps.currentOrder)) || (JSON.stringify(this.props.cashReferences) !== JSON.stringify(nextProps.cashReferences))) {
        this.onReferenceChange({
          target: {
            value: nextProps.currentOrder.reference
          }
        }, nextProps)
      }

      if (!this.props.currentOrder || (JSON.stringify(this.props.currentOrder) !== JSON.stringify(nextProps.currentOrder)) || (JSON.stringify(this.props.customOrderNumbers) !== JSON.stringify(nextProps.customOrderNumbers))) {
        this.onChangeOrderNo({
          target: {
            value: nextProps.currentOrder.customOrderNo
          }
        }, nextProps)
      }
    }
    if ((this.props.currentOrder?.fulfillmentType !== nextProps.currentOrder?.fulfillmentType && nextProps.currentOrder?.fulfillmentType === 4) || (this.props.currentOrder?.wholesaleOrderId !== nextProps.currentOrder?.wholesaleOrderId && nextProps.currentOrder?.fulfillmentType === 4)) {
      this.props.getOrderContainerList(nextProps?.currentOrder?.wholesaleOrderId)
    }
    if ((this.props.currentOrder?.fulfillmentType !== nextProps.currentOrder?.fulfillmentType && nextProps.currentOrder?.fulfillmentType === 5) || (this.props.currentOrder?.wholesaleOrderId !== nextProps.currentOrder?.wholesaleOrderId && nextProps.currentOrder?.fulfillmentType === 5)) {
      if (nextProps?.currentOrder?.wholesaleContainerId) {
        this.props.getWholesaleContainer(nextProps?.currentOrder?.wholesaleContainerId)
      }
    }
  }

  focusWhenComponentMounted = () => {
    const orderHeaderInputId = localStorage.getItem('ORDER-HEADER-INPUT-ID')
    if (orderHeaderInputId) {
      const headerInputFields = jQuery(
        '.header-input-field input, input.header-input-field, textarea.header-input-field',
      )
      const index = parseInt(orderHeaderInputId)
      if (index == 2) {
        jQuery.each(headerInputFields, (i, el) => {
          if (jQuery(el).data('id') == index) {
            jQuery(el).trigger('focus')
            return
          }
        })
      }
    }
  }

  handleFocusIn = () => {
    jQuery('.header-input-field input, input.header-input-field, textarea.header-input-field')
      .unbind()
      .bind('focus', function (e) {
        const id = jQuery(e.target).data('id')
        localStorage.setItem('ORDER-HEADER-INPUT-ID', `${id}`)
      })
      .bind('blur', function (e) {
        localStorage.removeItem('ORDER-HEADER-INPUT-ID')
      })
  }

  onFulfillmentDateChange = (date: Moment | null, dateString: string) => {
    const today = moment().format('MM/DD/YYYY')
    let data: any = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      deliveryDate: dateString,
      showNotif: false,
    }

    if (moment(today) > moment(dateString)) {
      data = { ...data, orderDate: dateString }
    }

    if (this.props.sellerSetting.company.defaultLastUsedDateEnabled === true) {
      localStorage.setItem('LAST_USED_TARGET_FULFILLMENT_DATE', dateString)
    }

    this.props.updateOrderInfo(data)
    this.setState({ deliveryDate: dateString, orderDate: moment(today) > moment(dateString) ? dateString : this.state.orderDate })
  }

  onOrderDateChange = (date: Moment | null, dateString: string) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      orderDate: dateString,
      showNotif: false,
    }

    this.props.updateOrderInfo(data)
    this.setState({ orderDate: dateString })
  }

  onReferenceChange = (e: any, props: any) => {
    const val = e.target.value
    const { cashReferences, currentOrder } = props
    const references = cashReferences
      .filter((el: any) => el.wholesaleOrderId != currentOrder.wholesaleOrderId)
      .map((el: any) => {
        return el.reference
      })

    this.setState({
      isReferenceDuplicate: references.indexOf(val) > -1 ? true : false,
      reference: val
    })
  }

  onSaveReference = () => {
    const { cashReferences, currentOrder } = this.props
    const { reference } = this.state
    const references = cashReferences
      .filter((el: any) => el.wholesaleOrderId != currentOrder.wholesaleOrderId)
      .map((el: any) => {
        return el.reference
      })
    this.setState(
      {
        isReferenceDuplicate: references.indexOf(reference) > -1 ? true : false,
      },
      () => {
        const data = {
          wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
          reference,
          cashSale: false,
          showNotif: false,
        }
        this.props.updateOrderInfo(data)
      },
    )
  }

  openCustomOrderNo = () => {
    this.setState(
      {
        enableEditOrderNo: true,
      },
      () => {
        this.handleFocusIn()
      },
    )
  }

  onSaveCustomOrderNo = (customOrderNo: string) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      customOrderNo,
    }
    this.props.updateOrderInfo(data)
  }

  onCloseCustomerOrderNo = () => {
    this.setState(
      {
        enableEditOrderNo: false,
        customOrderNo: '',
      },
      () => {
        this.onSaveCustomOrderNo('')
        this.handleFocusIn()
      },
    )
  }

  onCashSaleChange = (e: any) => {
    const data = {
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      cashSale: e.target.checked,
    }
    this.props.updateOrderInfo(data)
  }

  handleSyncQBOOrder = () => {
    const { currentOrder } = this.props
    if (currentOrder) {
      this.props.resetLoading()
      this.props.syncQBOOrder(currentOrder.wholesaleOrderId)
    }
  }

  handleSyncNSOrder = () => {
    const { currentOrder } = this.props
    if (currentOrder) {
      this.props.resetLoading()
      this.props.syncNSOrder(currentOrder.wholesaleOrderId)
    }
  }

  handleDuplicateOrder = () => {
    const { orderItemByProduct, currentOrder, oneOffItems, sellerSetting } = this.props

    // var delivery: any = new Date(currentOrder!.deliveryDate)
    // delivery = moment(delivery).add(2, 'days').format('MM/DD/YYYY')

    // const items = Object.values(orderItemByProduct).map(v => v.items[0])

    // var delivery = moment().format('MM/DD/YYYY')
    let fulfillmentDate = getFulfillmentDate(sellerSetting)

    // let newOrderItems = items.concat(oneOffItems)
    // let tempOrderItems = newOrderItems.map((obj) => {
    //   return {
    //     wholesaleItemId: obj.itemId,
    //     note: obj.note,
    //     UOM: obj.UOM,
    //     price: obj.price,
    //     cost: obj.cost,
    //     itemName: obj.itemName,
    //     displayOrder: obj.displayOrder,
    //     pricingUOM: obj.pricingUOM,
    //     ratio: obj.ratio,
    //     pricingLogic: obj.pricingLogic,
    //     pricingGroup: obj.pricingGroup,
    //     editedItemName: obj.editedItemName,
    //   }
    // })
    const data = {
      deliveryDate: fulfillmentDate,
      // orderDate: delivery,
      // wholesaleCustomerClientId: currentOrder!.wholesaleClient.clientId,
      // itemList: tempOrderItems,
      wholesaleOrderId: currentOrder!.wholesaleOrderId,
    }
    this.props.duplicateOrder(data)
  }

  onSelectAction = ({ key }) => {
    console.log(key)
    const { currentOrder, orderItems } = this.props
    if (key == 1) {
      this.setState({ showPalletLabelModal: true })
    } else if (key == 2) {
      const showConfirmModal = needShowDuplicateConfirmModal(orderItems, 2)
      if (showConfirmModal) {
        this.setState({ duplicateConfirmModal: true })
      } else {
        this.handleDuplicateOrder()
      }
    } else if (key == 3) {
      // cancel order
      this.props.onDoHeaderActions('cancel')
    } else if (key == 4) {
      // lock order
      this.props.onDoHeaderActions(currentOrder.isLocked ? 'unlock' : 'lock')
    } else if (key == 5) {
      this.props.onDoHeaderActions('view_manifest')
    } else if (key == 6) {
      this.props.onDoHeaderActions('view_bill')
    } else if (key == 7) {
      this.props.setIsHealthBol(true)
      this.props.onDoHeaderActions('view_bill')
    } else if (key === 'sales-confirmation') {
      this.props.onDoHeaderActions('view_sales_confirmation')
    } else if (key.includes('container')) {
      this.props.onDoHeaderActions('view_bill')
      this.props.setCurrentPrintContainerId(_.replace(key, 'container', ''))
    }
  }

  renderQboContainerInvoices = (qboId: string) => {
    const qboIds = JSON.parse(qboId)
    console.log(qboIds)
    var i = 1;
    var rows = [];
    for (const k in qboIds) {
      // note: we are adding a key prop here to allow react to uniquely identify each
      // element in this array. see: https://reactjs.org/docs/lists-and-keys.html
      rows.push(
        <Menu.Item key={i}>
          <a href={`https://qbo.intuit.com/app/invoice?txnId=${qboIds[k]}`} target="_blank">
            Open QBO Invoice {qboIds[k]}
          </a>
        </Menu.Item>
      );
      i++
    }
    return (
      <HeaderActionMenu>
        {rows}
      </HeaderActionMenu>
    )
  }

  renderQboSyncButton = () => {
    const {
      loading,
      currentOrder: { wholesaleOrderStatus, qboId },
      orderItemByProduct,
      oneOffItems,
      catchWeightValues,
      lastQboUpdate,
    } = this.props
    const totalOrder = calculateTotalOrder(orderItemByProduct, oneOffItems, catchWeightValues)
    const qboRealmId = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    const userId = localStorage.getItem(CACHED_USER_ID)
    const companyId = localStorage.getItem(CACHED_COMPANY)
    const disabled = wholesaleOrderStatus === 'CANCEL' || this.props.updating || totalOrder === '0.00'
    if (qboRealmId) {
      const lastUpdated = new Date(lastQboUpdate)
      const style = { borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }
      return (
        <Flex>
          <Tooltip
            placement={'top'}
            title={totalOrder === '0.00' ? 'You cannot sync an order with $0 total.' : lastQboUpdate ? `Synced to Quickbooks ${formatTimeDifference(lastUpdated)} ago` : ''}
          >
            <ThemeOutlineButton
              disabled={disabled}
              size="large"
              onClick={() => this.handleSyncQBOOrder()}
              loading={loading}
              style={qboId ? style : {}}
              hidden={((companyId == 'Growers Produce' && !(userId == '105629' || userId == '116137' || userId == '110151' || userId == '97960' || userId == '110154' || userId == '110148' || userId == '110149') ||
                (companyId == 'PayLess Logistics' && !(userId == '105120' || userId == '105165' || userId == '105159' || userId == '105160'))))}
            >
              <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
              {qboId ? 'Re-' : ''}Sync to QBO
            </ThemeOutlineButton>
          </Tooltip>
          {qboId && (
            <Dropdown
              overlay={
                qboId.includes("{") ?
                  (
                    this.renderQboContainerInvoices(qboId)
                  ) : (
                    <HeaderActionMenu>
                      <Menu.Item key="1">
                        <a href={`https://qbo.intuit.com/app/invoice?txnId=${qboId}`} target="_blank">
                          Open In Quickbooks
                        </a>
                      </Menu.Item>
                    </HeaderActionMenu>
                  )}
              trigger={['click']}
              placement="bottomRight"
            >
              <ThemeOutlineButton
                size="large"
                type="primary"
                style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
              >
                <Icon type="down" />
              </ThemeOutlineButton>
            </Dropdown>
          )}
        </Flex>
      )
    }
    return <span />
  }

  renderNsSyncButton = () => {
    const {
      loading,
      currentOrder: { wholesaleOrderStatus, nsId },
      lastNsUpdate,
      orderItemByProduct,
      oneOffItems,
      catchWeightValues,
    } = this.props
    const totalOrder = calculateTotalOrder(orderItemByProduct, oneOffItems, catchWeightValues)
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED)
    const userId = localStorage.getItem(CACHED_USER_ID)
    const companyId = localStorage.getItem(CACHED_COMPANY)
    const disabled = wholesaleOrderStatus === 'CANCEL' || this.props.updating || totalOrder === '0.00'
    if (nsRealmId && nsRealmId != 'null') {
      const lastUpdated = new Date(lastNsUpdate)
      const style = { borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }
      return (
        <Flex>
          <Tooltip
            placement={'top'}
            title={totalOrder === '0.00' ? 'You cannot sync an order with $0 total.' : lastNsUpdate ? `Synced to NetSuite ${formatTimeDifference(lastUpdated)} ago` : ''}
          >
            <ThemeOutlineButton
              disabled={disabled}
              size="large"
              onClick={() => this.handleSyncNSOrder()}
              loading={loading}
              style={nsId ? style : {}}
              hidden={((companyId == 'Growers Produce' && !(userId == '105629' || userId == '116137' || userId == '110151' || userId == '97960' || userId == '110154' || userId == '110148' || userId == '110149') ||
                (companyId == 'PayLess Logistics' && !(userId == '105120' || userId == '105165' || userId == '105159' || userId == '105160'))))}
            >
              <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
              {nsId ? 'Re-' : ''}Sync to NS
            </ThemeOutlineButton>
          </Tooltip>
          {nsId && (
            <Dropdown
              overlay={
                <HeaderActionMenu>
                  <Menu.Item key="1">
                    <a
                      href={`https://${nsRealmId}.app.netsuite.com/app/accounting/transactions/custinvc.nl?id=${nsId}`}
                      target="_blank"
                    >
                      Open In NetSuite
                    </a>
                  </Menu.Item>
                </HeaderActionMenu>
              }
              trigger={['click']}
              placement="bottomRight"
            >
              <ThemeOutlineButton
                size="large"
                type="primary"
                style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
              >
                <Icon type="down" />
              </ThemeOutlineButton>
            </Dropdown>
          )}
        </Flex>
      )
    }
    return <span />
  }

  onChangeCustomer = (selectedCustomer: any, actionType: string) => {
    this.setState({ showCustomerModal: false }, () => {
      if (selectedCustomer && actionType == 'ok') {
        const data = {
          wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
          wholesaleClientId: selectedCustomer,
        }
        this.props.updateOrderClient(data)
      }
    })
  }

  private onClickShow = () => {
    this.setState({
      createShow: true,
    })
  }

  private onCloseNewOrder = () => {
    this.setState({
      createShow: false,
    })
  }

  private onClickCreate = (clientNumber: string) => {
    if (clientNumber) {
      const quotedDate = moment(new Date()).format('MM/DD/YYYY')
      const { sellerSetting } = this.props
      let fulfillmentDate = getFulfillmentDate(sellerSetting)
      let data = {
        wholesaleClientId: clientNumber,
        orderDate: quotedDate,
        quotedDate: quotedDate,
        deliveryDate: fulfillmentDate,
        itemList: [],
      }
      this.props.createEmptyOrder(data)
    }
    this.setState({
      createShow: false,
    })
  }

  changeDuplicateConfirmModalStatus = () => {
    this.setState({
      duplicateConfirmModal: !this.state.duplicateConfirmModal,
    })
  }

  renderDuplicateText = (orderItems: any) => {
    const dismatchOrderItems = updateDisMatchDuplicateOrderItemsForGroup(orderItems, 2)
    return (
      <DuplicateConfirmDiv>
        <p>The following updates have been made to your duplicated order:</p>
        <ul>
          {dismatchOrderItems.map((orderItem: any) => {
            if (orderItem.duplicateType == 1) {
              return (
                <li>
                  The item <BoldSpan>{orderItem.variety} </BoldSpan>is currently inactive. This item has not been
                  duplicated.
                </li>
              )
            } else if (orderItem.duplicateType == 2) {
              return (
                <li>
                  The original UOM for{' '}
                  <BoldSpan>
                    {orderItem.variety},{orderItem.UOM},
                  </BoldSpan>
                  is currently not available for use.This item has been duplicated with the default selling UOM.
                  {orderItem.InventoryUOM}
                </li>
              )
            } else if (orderItem.duplicateType == 3) {
              return (
                <li>
                  The original UOM for{' '}
                  <BoldSpan>
                    {orderItem.variety},{orderItem.pricingUOM},
                  </BoldSpan>
                  is currently not available for use.This item has been duplicated with the default price UOM.
                  {orderItem.InventoryUOM}
                </li>
              )
            }
          })}
        </ul>
      </DuplicateConfirmDiv>
    )
  }

  onChangeOrderStatus = (status: string) => {
    const { currentOrder, updateOrderInfo } = this.props
    updateOrderInfo({
      wholesaleOrderId: currentOrder.wholesaleOrderId,
      deliveryDate: moment.utc(currentOrder.deliveryDate).format('MM/DD/YYYY'),
      wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
      status: status,
    })
  }

  handleChangeFinancialTerms = (financialTerms: string) => {
    const { currentOrder, updateOrderInfo } = this.props
    updateOrderInfo({
      wholesaleOrderId: currentOrder.wholesaleOrderId,
      financialTerms,
    })
  }

  handleViewPrint = (type: string, { key }: { key: string }) => {
    this.props.getOrderItemsById(this.props.match.params.orderId)
    if (key == 'ePick') {
      return this.setState({ showEPickSheetModal: true })
    }
    this.props.setCurrentPrintContainerId(key)
    this.props.onDoHeaderActions(type)
  }

  toggleModal = (key: string, status: boolean) => {
    if (key == 'showEPickSheetModal') {
      this.setState({ showEPickSheetModal: status })
    } else if (key == 'showPalletLabelModal') {
      this.setState({ showPalletLabelModal: status })
    }
  }

  showInput = (order: any) => {
    const wholesaleOrderStatus = order ? order.wholesaleOrderStatus : ''
    if (wholesaleOrderStatus == 'SHIPPED') {
      return true
    } else {
      return false
    }
  }

  onChangeOrderNo = (e: any, props: any) => {
    const { value } = e.target

    const { customOrderNumbers, currentOrder } = props
    const orderNumbers = customOrderNumbers
      .filter((el: any) => el.wholesaleOrderId != currentOrder.wholesaleOrderId)
      .map((el: any) => {
        return el.customOrderNo
      })

    this.setState({
      isDuplicatedOrderNo: orderNumbers.indexOf(value) > -1 ? true : false,
      customOrderNo: value,
    })
  }

  toggleBalance = () => {
    const { visibleBalance } = this.state
    this.setState({
      visibleBalance: !visibleBalance
    })
    if (!visibleBalance) {
      this.props.getOrderBalance(this.props.currentOrder.wholesaleOrderId)
    }
  }

  render() {
    const {
      currentOrder,
      loading,
      printSetting,
      sellerSetting,
      simplifyCustomers,
      orderItems,
      companyProductTypes,
      currentUser,
      gettingOrder,
    } = this.props
    const {
      orderDate,
      deliveryDate,
      reference,
      duplicateConfirmModal,
      orderStatus,
      financialTerms,
      isReferenceDuplicate,
      isDuplicatedOrderNo,
      visibleBalance,
    } = this.state
    const paymentTerms = [
      ..._.get(companyProductTypes, 'paymentTerms', []),
      ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
    ]
    // const orderTotal = currentOrder
    //   ? currentOrder.totalPrice + (currentOrder.shippingCost ? currentOrder.shippingCost : 0)
    //   : 0
    const userPrintSetting = JSON.parse(printSetting)
    const orderDisabled =
      !currentOrder ||
      (currentOrder &&
        (currentOrder.wholesaleOrderStatus == 'CANCEL' || currentOrder.wholesaleOrderStatus == 'SHIPPED'))

    let shippingAddress = ''
    if (currentOrder && currentOrder.wholesaleClient && currentOrder.wholesaleClient.mainShippingAddress) {
      const { street1, city, state } = currentOrder.wholesaleClient.mainShippingAddress.address
      shippingAddress = `${street1}, ${city}, ${state}`
    }
    const orderPrefix = getOrderPrefix(sellerSetting, 'sales')
    const redirectToCustomer = currentOrder
      ? window.location.origin + window.location.pathname + `#/customer/${currentOrder.wholesaleClient.clientId}/orders`
      : ''
    const grossWeightTotal = _.reduce(
      this.props.orderItems,
      (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
      0,
    )
    const grossVolumeTotal = _.reduce(
      this.props.orderItems,
      (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
      0,
    )
    const disabled = !currentOrder || currentOrder.wholesaleOrderStatus === 'CANCEL' || this.props.updating

    const orderTotalTax = (currentOrder && currentOrder.wholesaleClient.taxable) ? unFormatNumber(calculateTotalOrderTax(this.props.orderItemByProduct, this.props.oneOffItems, this.props.catchWeightValues)) : 0
    const orderSubtotal = this.props.pageTotalInfo.totalPrice ? this.props.pageTotalInfo.totalPrice : unFormatNumber(calculateTotalOrder(this.props.orderItemByProduct, this.props.oneOffItems, this.props.catchWeightValues, 2))

    const orderTotal = _.toNumber(orderTotalTax + orderSubtotal)

    return (
      <>
        <ThemeModal
          title={`Duplicate order alert`}
          visible={duplicateConfirmModal}
          onCancel={this.changeDuplicateConfirmModalStatus}
          bodyStyle={{ padding: 0 }}
          footer={
            <DialogSubContainer>
              <ThemeButton type="primary" onClick={this.handleDuplicateOrder}>
                OK
              </ThemeButton>
            </DialogSubContainer>
          }
          width={600}
        >
          {this.renderDuplicateText(orderItems)}
        </ThemeModal>
        <Flex
          className="v-center"
          style={{
            ...VerticalPadding12,
            justifyContent: 'space-between',
            position: 'fixed',
            marginLeft: -40,
            width: '100%',
            height: 60,
            backgroundColor: this.props.theme.theme,
            zIndex: 3,
          }}
        >
          <FlexDiv>
            <Item className="left icon-item">
              <a href="#/sales-orders" style={{ paddingLeft: 82, color: 'white' }}>
                <ThemeIcon type="arrow-left" viewBox="0 0 20 20" width={13} height={11} style={{ color: 'white' }} />
                <ValueLabel>Sales Orders</ValueLabel>
              </a>
            </Item>
          </FlexDiv>
          <FlexDiv>
            {currentUser.accountType != UserRole.WAREHOUSE && (
              <Item className="header-btns">
                {currentOrder && currentOrder.parentOrder == null ? (
                  <span>
                    {this.renderQboSyncButton()}
                    {this.renderNsSyncButton()}
                  </span>
                ) : (
                  <span />
                )}
              </Item>
            )}
            {sellerSetting &&
              (typeof sellerSetting.company.quickprintInvoiceEnabled === 'undefined' ||
                sellerSetting.company.quickprintInvoiceEnabled ||
                typeof sellerSetting.company.quickprintPicksheetEnabled === 'undefined' ||
                sellerSetting.company.quickprintPicksheetEnabled ||
                typeof sellerSetting.company.quickprintInvoiceOfficeEnabled === 'undefined' ||
                sellerSetting.company.quickprintInvoiceOfficeEnabled ||
                (typeof sellerSetting.company.quickprintBolEnabled !== 'undefined' && sellerSetting.company.quickprintBolEnabled)) && (
                <Item className="header-btns right">
                  <ThemeOutlineButton
                    size="large"
                    onClick={() => this.props.onDoHeaderActions('quick_print')}
                    loading={this.props.updateOrderQuantityLoading.fetching}
                    disabled={disabled}
                  >
                    <Icon type="printer" />
                    Quick Print
                  </ThemeOutlineButton>
                </Item>
              )}
            {userPrintSetting &&
              (typeof userPrintSetting.pick_enabled === 'undefined' || userPrintSetting.pick_enabled) && (
                <Item className="header-btns right">
                  {/* <ThemeOutlineButton
                    size="large"
                    onClick={() => this.props.onDoHeaderActions('view_pick')}
                    disabled={currentOrder === null || currentOrder.wholesaleOrderStatus == 'CANCEL'}
                  >
                    View{' '}
                    {userPrintSetting &&
                      typeof userPrintSetting.pick_title !== 'undefined' &&
                      userPrintSetting.pick_title
                      ? userPrintSetting.pick_title
                      : 'Pick Sheet'}
                  </ThemeOutlineButton> */}
                  <Flex>
                    <ThemeOutlineButton
                      size="large"
                      type="primary"
                      style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }}
                      onClick={() => this.props.onDoHeaderActions('view_pick')}
                      loading={this.props.updateOrderQuantityLoading.fetching}
                      disabled={disabled}
                    >
                      View{' '}
                      {userPrintSetting &&
                        typeof userPrintSetting.pick_title !== 'undefined' &&
                        userPrintSetting.pick_title
                        ? userPrintSetting.pick_title
                        : 'Pick Sheet'}
                    </ThemeOutlineButton>
                    <Dropdown
                      overlay={
                        <HeaderActionMenu onClick={(key) => this.handleViewPrint('view_pick', key)}>
                          <Menu.Item key="ePick">
                            <img src={scan_small} />
                            ePick Sheet
                          </Menu.Item>
                          {this.props?.currentOrder?.fulfillmentType === 4 && this.props.containerList.map(v => (
                            <Menu.Item key={v.containerId}>
                              <Icon type="printer" /> {v.containerName} Load Sheet
                            </Menu.Item>
                          ))}
                          {this.props?.currentOrder?.fulfillmentType === 5 && this.props.containerList.map(v => (
                            <Menu.Item key={v.containerId}>
                              <Icon type="printer" /> {v.no} Load Sheet
                            </Menu.Item>
                          ))}
                        </HeaderActionMenu>
                      }
                      trigger={['click']}
                      placement="bottomRight"
                    >
                      <ThemeOutlineButton
                        size="large"
                        type="primary"
                        style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
                      >
                        <Icon type="down" />
                      </ThemeOutlineButton>
                    </Dropdown>
                  </Flex>
                </Item>
              )}

            {userPrintSetting &&
              currentUser.accountType != UserRole.WAREHOUSE &&
              (typeof userPrintSetting.invoice_enabled === 'undefined' || userPrintSetting.invoice_enabled) && (
                <Item className="header-btns right">
                  <ThemeOutlineButton
                    size="large"
                    style={{ textTransform: 'capitalize', borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }}
                    onClick={() => this.props.onDoHeaderActions('view_invoice')}
                    loading={this.props.updateOrderQuantityLoading.fetching}
                    disabled={disabled}
                  >
                    View{' '}
                    {userPrintSetting && userPrintSetting.invoice_title ? userPrintSetting.invoice_title : 'Invoice'}
                  </ThemeOutlineButton>
                  {(this.props?.currentOrder?.fulfillmentType === 4 || this.props?.currentOrder?.fulfillmentType === 5) && <Dropdown
                    overlay={
                      <HeaderActionMenu onClick={(key) => this.handleViewPrint('view_invoice', key)}>
                        {this.props.containerList.map(v => (
                          <Menu.Item key={v.containerId}>
                            <Icon type="printer" /> {this.props?.currentOrder?.fulfillmentType === 5 ? v.no : v.containerName} Invoice
                          </Menu.Item>
                        ))}
                      </HeaderActionMenu>
                    }
                    trigger={['click']}
                    placement="bottomRight"
                  >
                    <ThemeOutlineButton
                      size="large"
                      type="primary"
                      style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
                    >
                      <Icon type="down" />
                    </ThemeOutlineButton>
                  </Dropdown>}
                </Item>
              )}

            {currentUser.accountType != UserRole.WAREHOUSE && (
              <ThemeButton
                className="icon enter-purchase"
                size="large"
                type="primary"
                onClick={this.onClickShow}
                style={{ marginLeft: 10 }}
              >
                <Icon type="plus-circle" style={{ fontSize: 20 }} />
                New Order
              </ThemeButton>
            )}
            {/* <Item className="right">

              <ThemeOutlineButton shape="round" onClick={() => this.handleDuplicateOrder()}>
                Duplicate Order
              </ThemeOutlineButton>
            </Item> */}
            {currentOrder !== null && (
              <Dropdown
                overlay={
                  <HeaderActionMenu onClick={this.onSelectAction}>
                    {(this.props?.currentOrder?.fulfillmentType === 4 || this.props?.currentOrder?.fulfillmentType === 5) ?
                      userPrintSetting && (typeof userPrintSetting.bill_enabled === 'undefined' || userPrintSetting.bill_enabled) && (
                        <SubMenu key="sub3" title={
                          <span>
                            <IconSvg
                              type="pallet-label"
                              viewBox={void 0}
                              style={{ width: 16, height: 17, fill: 'transparent', marginLeft: 4, marginRight: 8 }}
                            />
                            View {userPrintSetting && userPrintSetting.bill_title ? userPrintSetting.bill_title : 'Bill of Lading'}
                          </span>
                        }>
                          <Menu.Item key="6">
                            <Icon type="printer" /> View {userPrintSetting && userPrintSetting.bill_title ? userPrintSetting.bill_title : 'Bill of Lading'}
                          </Menu.Item>
                          {(this.props?.currentOrder?.fulfillmentType === 4 || this.props?.currentOrder?.fulfillmentType === 5) && this.props.containerList.map(v => (
                            <Menu.Item key={`container${v.containerId}`}>
                              <Icon type="printer" /> MATU {this.props?.currentOrder?.fulfillmentType === 5 ? v.no : v.containerName} BOL
                            </Menu.Item>
                          ))}
                        </SubMenu>
                      )
                      :
                      userPrintSetting && (typeof userPrintSetting.bill_enabled === 'undefined' || userPrintSetting.bill_enabled) && (
                        <Menu.Item key="6">
                          <Flex className="h-center" style={{ width: 24, marginRight: 4 }}>
                            <IconSvg
                              type="pallet-label"
                              viewBox={void 0}
                              style={{ width: 16, height: 17, fill: 'transparent' }}
                            />
                          </Flex>
                          View {userPrintSetting && userPrintSetting.bill_title ? userPrintSetting.bill_title : 'Bill of Lading'}
                        </Menu.Item>
                      )
                    }
                    {userPrintSetting && (typeof userPrintSetting.invoice_enabled === 'undefined' || userPrintSetting.invoice_enabled) &&
                      (typeof userPrintSetting.invoice_sales_confirmation_enabled !== 'undefined' && userPrintSetting.invoice_sales_confirmation_enabled === true) && (
                        <Menu.Item key="sales-confirmation">
                          <Flex className="h-center" style={{ width: 24, marginRight: 4 }}>
                            <IconSvg
                              type="pallet-label"
                              viewBox={void 0}
                              style={{ width: 16, height: 17, fill: 'transparent' }}
                            />
                          </Flex>
                          View Sales Confirmation
                        </Menu.Item>
                      )
                    }
                    <Menu.Item key="1" disabled={disabled}>
                      <Flex className="h-center" style={{ width: 24, marginRight: 4 }}>
                        <IconSvg
                          type="pallet-label"
                          viewBox={void 0}
                          style={{ width: 16, height: 17, fill: 'transparent' }}
                        />
                      </Flex>
                      Print Pallet Labels...
                    </Menu.Item>

                    {userPrintSetting && (typeof userPrintSetting.manifest_enabled === 'undefined' || userPrintSetting.manifest_enabled) && (
                      <Menu.Item key="5">
                        <Flex className="h-center" style={{ width: 24, marginRight: 4 }}>
                          <IconSvg
                            type="pallet-label"
                            viewBox={void 0}
                            style={{ width: 16, height: 17, fill: 'transparent' }}
                          />
                        </Flex>
                        View {userPrintSetting && userPrintSetting.manifest_title ? userPrintSetting.manifest_title : 'Shipping Manifest'}
                      </Menu.Item>
                    )}
                    {(this.props?.currentOrder?.fulfillmentType === 4 || this.props?.currentOrder?.fulfillmentType === 5) ?
                      userPrintSetting && (typeof userPrintSetting.bill_enabled === 'undefined' || userPrintSetting.bill_enabled) && (
                        <SubMenu key="sub4" title={
                          <span>
                            <IconSvg
                              type="pallet-label"
                              viewBox={void 0}
                              style={{ width: 16, height: 17, fill: 'transparent', marginLeft: 4, marginRight: 8 }}
                            />
                            View Health Document
                          </span>
                        }>
                          <Menu.Item key="7">
                            <Icon type="printer" /> View Health Document
                          </Menu.Item>
                          {(this.props?.currentOrder?.fulfillmentType === 4 || this.props?.currentOrder?.fulfillmentType === 5) && this.props.containerList.map(v => (
                            <Menu.Item key={`container${v.containerId}`} onClick={() => this.props.setIsHealthBol(true)}>
                              <Icon type="printer" /> MATU {this.props?.currentOrder?.fulfillmentType === 5 ? v.no : v.containerName} Health Document
                            </Menu.Item>
                          ))}
                        </SubMenu>
                      )
                      :
                      userPrintSetting && (typeof userPrintSetting.bill_enabled === 'undefined' || userPrintSetting.bill_enabled) && (
                        <Menu.Item key="7">
                          <Flex className="h-center" style={{ width: 24, marginRight: 4 }}>
                            <IconSvg
                              type="pallet-label"
                              viewBox={void 0}
                              style={{ width: 16, height: 17, fill: 'transparent' }}
                            />
                          </Flex>
                          View Health Document
                        </Menu.Item>
                      )
                    }
                    {currentUser.accountType != UserRole.WAREHOUSE && (
                      <Menu.Item
                        key="2"
                        disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}
                      >
                        <Flex className="h-center" style={{ width: 24, marginRight: 4 }}>
                          <IconSvg
                            type="duplicate"
                            viewBox={void 0}
                            style={{ width: 24, height: 24, fill: 'transparent' }}
                          />
                        </Flex>
                        Duplicate Order
                      </Menu.Item>
                    )}
                    {currentUser.accountType != UserRole.WAREHOUSE && (
                      <Menu.Item
                        key="3"
                        disabled={disabled || currentOrder.isLocked}
                      >
                        <Flex className="h-center" style={{ width: 24, marginRight: 4 }}>
                          <Icon
                            type="close"
                            style={{ fontSize: 18, color: this.props.theme.theme }}
                          />
                        </Flex>
                        Cancel Order
                      </Menu.Item>
                    )}
                    {/* <Menu.Item key="4">
                      <IconSvg
                        type={currentOrder.isLocked ? 'unlock' : 'lock'}
                        viewBox="0 0 440 440"
                        style={{ width: 20, height: 20, fill: this.props.theme.theme, marginRight: 6, marginLeft: 2 }}
                      />
                      {currentOrder.isLocked ? 'UnLock' : 'Lock'} Order
                    </Menu.Item> */}
                  </HeaderActionMenu>
                }
                placement="bottomLeft"
                trigger={['click']}
              >
                <IconSvg type="more" viewBox={void 0} style={{ width: 24, height: 24, margin: '9px 24px 5px 12px' }} />
              </Dropdown>
            )}
          </FlexDiv>
        </Flex>
        <PaddingContainer style={{ paddingTop: 76, paddingBottom: 24 }}>
          <FlexDiv className="space-between">
            <FlexDiv>
              <OrderInfoItem className="left">
                {/* <InputLabel style={{ position: 'relative' }}>Account Name</InputLabel> */}
                <CustomerName style={{ paddingTop: 8 }}>
                  <ValueLabel
                    className="link-text"
                  // onClick={() => history.push(`/customer/${currentOrder.wholesaleClient.clientId}/orders`)}
                  >
                    <a href={redirectToCustomer}>
                      {currentOrder && currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
                        ? currentOrder.wholesaleClient.clientCompany.companyName
                        : ''}
                    </a>
                  </ValueLabel>
                  <IconSvg
                    type="edit"
                    viewBox="0 0 18 18"
                    width={16}
                    height={16}
                    className={orderDisabled ? 'disabled' : 'enabled'}
                    onClick={() => {
                      this.setState({ showCustomerModal: true })
                    }}
                  />
                  {this.state.showCustomerModal && (
                    <NewOrderFormModal
                      visible={this.state.showCustomerModal}
                      onOk={(e) => this.onChangeCustomer(e, 'ok')}
                      onCancel={(e) => this.onChangeCustomer(e, 'close')}
                      clients={simplifyCustomers}
                      okButtonName="Change Customer"
                      isNewOrderFormModal={false}
                      defaultClientId={currentOrder ? currentOrder.wholesaleClient.clientId : 'No Client'}
                    />
                  )}
                </CustomerName>
                {shippingAddress && <ValueLabel className="small black">{shippingAddress}</ValueLabel>}
                {currentOrder && currentOrder.wholesaleClient && currentUser.accountType !== UserRole.WAREHOUSE && (
                  <Flex className='v-center' style={{ marginTop: 8, height: 32 }}>
                    <ValueLabel className="small black" style={{marginRight: 12, marginTop: 0}}>
                      <span style={{ fontWeight: 'bold', marginRight: 3 }}>Customer balance:</span>
                      {visibleBalance && !gettingOrder ?
                        <>
                          ${formatNumber(currentOrder.wholesaleClient.openBalance, 2)}{' '}
                          {currentOrder.wholesaleClient.overdue > 0 && (
                            <span>
                              (<span className="red">${formatNumber(currentOrder.wholesaleClient.overdue, 2)}</span> overdue)
                            </span>
                          )}
                        </>
                        :
                        <span>$X.XX</span>
                      }
                    </ValueLabel>
                    {gettingOrder ? (
                      <Spin />
                    ) : (
                      <Tooltip placement="right" title="Click to show/hide this customer’s account balance information.">
                          <ThemeIconButton onClick={this.toggleBalance} className="no-border" style={{height: 18}}>
                            <Icon type={visibleBalance ? 'eye' : 'eye-invisible'} style={{transform: 'scaleX(-1)'}} />
                          </ThemeIconButton>
                      </Tooltip>
                    )}
                  </Flex>
                )}
              </OrderInfoItem>
              <Item>
                <InputLabel>Order Date</InputLabel>
                <DatePicker style={{ width: '142px' }}
                  data-id={0}
                  className="header-input-field"
                  allowClear={false}
                  onChange={this.onOrderDateChange}
                  value={orderDate ? moment(orderDate) : undefined}
                  placeholder="MM/DD/YYYY"
                  format="MM/DD/YYYY"
                  disabled={disabled}
                  suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                />
              </Item>
              <Item>
                <InputLabel>Target Fulfillment Date</InputLabel>
                <DatePicker style={{ width: '142px' }}
                  data-id={0}
                  className="header-input-field"
                  allowClear={false}
                  onChange={this.onFulfillmentDateChange}
                  value={deliveryDate ? moment(deliveryDate) : undefined}
                  placeholder="MM/DD/YYYY"
                  format="MM/DD/YYYY"
                  disabled={disabled}
                  suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                />
              </Item>
              <Item>
                <InputLabel>Customer PO No.</InputLabel>
                <ThemeInput style={{ width: '135px' }}
                  value={reference}
                  onChange={(e: any) => this.onReferenceChange(e, this.props)}
                  onBlur={this.onSaveReference}
                  disabled={this.showInput(currentOrder) || disabled}
                  className='header-input-field normal'
                  data-id={1}
                />
                <WarningText className={isReferenceDuplicate ? 'show' : 'hide'}>
                  Duplicate Customer PO No
                </WarningText>
              </Item>
              <Item className="right padding32-item">
                <InputLabel>Payment Terms</InputLabel>
                <ThemeSelect
                  value={financialTerms}
                  style={{ minWidth: 120 }}
                  onChange={(v) => this.handleChangeFinancialTerms(v)}
                  disabled={disabled}
                >
                  {paymentTerms.map((v) => (
                    <Select.Option value={v.name}>{v.name}</Select.Option>
                  ))}
                </ThemeSelect>
                {_.get(
                  paymentTerms.find((v) => v.name === financialTerms),
                  'type',
                  '',
                ) === 'FINANCIAL_TERM_FIXED_DAYS' &&
                  localStorage.getItem('qboLinked') !== 'null' && (
                    <p style={{ color: '#a2a2a2', marginTop: 10, marginBottom: 0 }}>
                      Due &nbsp;
                      {moment(deliveryDate)
                        .add(
                          _.get(
                            paymentTerms.find((v) => v.name === financialTerms),
                            'dueDays',
                            0,
                          ),
                          'days',
                        )
                        .format('MM/DD/YY')}
                    </p>
                  )}
              </Item>
              {/* <Item style={{ alignSelf: 'flex-start', marginTop: 37 }}>
                <ThemeCheckbox
                  checked={currentOrder && currentOrder.cashSale ? true : false}
                  onChange={this.onCashSaleChange}
                  disabled={orderDisabled}
                  className="sales-cart-input"
                >
                  Cash Sale
                </ThemeCheckbox>
              </Item> */}
            </FlexDiv>
            <FlexDiv style={{ color: gray01 }}>
              <Item
                className={`left padding32-item ${isCustomOrderNumberEnabled(sellerSetting) ? 'custom-order-no-enabled' : ''
                  }`}
              >
                <InputLabel> {!this.state.enableEditOrderNo ? 'Order No.' : 'Custom Order Number'}</InputLabel>
                <Flex className="v-center">
                  {this.state.enableEditOrderNo ? (
                    <>
                      <ThemeInput
                        data-id={2}
                        className={`header-input-field custom-order-input ${orderPrefix.length === 3 ? 'pad40' : orderPrefix.length === 4 ? 'pad50' : ''
                          }`}
                        prefix={<span>{orderPrefix}</span>}
                        suffix={<Icon type="close" style={{ fontSize: 20 }} onClick={this.onCloseCustomerOrderNo} />}
                        onBlur={() => this.onSaveCustomOrderNo(this.state.customOrderNo)}
                        value={this.state.customOrderNo}
                        onChange={(e: any) => this.onChangeOrderNo(e, this.props)}
                        disabled={this.showInput(currentOrder) || disabled}
                      />
                      <WarningText style={{ position: 'absolute', marginTop: 67 }} className={isDuplicatedOrderNo ? 'show' : 'hide'}>
                        Duplicate order number
                      </WarningText>
                      <ValueLabel className="grey small" style={{ marginTop: 0 }}>
                        {currentOrder ? `(WSW-${currentOrder.wholesaleOrderId})` : ''}
                      </ValueLabel>
                    </>
                  ) : (
                    <ValueLabel style={{ marginTop: 0 }}>
                      {currentOrder && currentOrder.priceSheetId ? 'E-Commerce-' : orderPrefix}
                      {currentOrder ? currentOrder.wholesaleOrderId : ''}
                    </ValueLabel>
                  )}
                  {!this.state.enableEditOrderNo && (
                    <IconSvg
                      type="edit"
                      viewBox="0 0 18 18"
                      width={16}
                      height={16}
                      className={'icon-edit-order-no'}
                      onClick={this.openCustomOrderNo}
                    />
                  )}
                </Flex>
              </Item>
              <Item className="padding32-item" style={{ marginLeft: '80px' }}>
                <InputLabel>Order Status</InputLabel>
                {currentOrder && currentOrder.wholesaleOrderStatus === 'CANCEL' ? (
                  <OrderStatusLabel>
                    {formatOrderStatus(currentOrder ? currentOrder.wholesaleOrderStatus : '', sellerSetting)}
                  </OrderStatusLabel>
                ) : (
                  (sellerSetting && sellerSetting.company && sellerSetting.company.isDisablePickingStep) ? (
                    <ThemeSelect
                      onChange={(val: any) => this.onChangeOrderStatus(val)}
                      className="status-selection"
                      value={orderStatus.toUpperCase() === "PICKING" ? "New" : orderStatus}
                      suffixIcon={<Icon type="caret-down" />}
                      style={{ width: '100%', borderRadius: 20, marginTop: -5 }}
                      disabled={orderStatus === 'SHIPPED'}
                      dropdownRender={(el: any) => {
                        return <div className="sales-cart-status-selector">{el}</div>
                      }}
                    >
                      {['New', 'Shipped'].map((el: string, index: number) => {
                        return (
                          <Select.Option disabled={el.toUpperCase() === 'SHIPPED'} value={el.toUpperCase()} key={index}>
                            {/* {el.toUpperCase() == 'SHIPPED'
                            ? formatOrderStatus(currentOrder ? currentOrder.wholesaleOrderStatus : '', sellerSetting)
                            : el} */}
                            {el}
                          </Select.Option>
                        )
                      })}
                    </ThemeSelect>
                  ) : (
                    <ThemeSelect
                      onChange={(val: any) => this.onChangeOrderStatus(val)}
                      className="status-selection"
                      value={orderStatus}
                      suffixIcon={<Icon type="caret-down" />}
                      style={{ width: '100%', borderRadius: 20, marginTop: -5 }}
                      disabled={orderStatus === 'SHIPPED'}
                      dropdownRender={(el: any) => {
                        return <div className="sales-cart-status-selector">{el}</div>
                      }}
                    >
                      {['New', 'Picking', 'Shipped'].map((el: string, index: number) => {
                        return (
                          <Select.Option disabled={el.toUpperCase() === 'SHIPPED'} value={el.toUpperCase()} key={index}>
                            {/* {el.toUpperCase() == 'SHIPPED'
                            ? formatOrderStatus(currentOrder ? currentOrder.wholesaleOrderStatus : '', sellerSetting)
                            : el} */}
                            {el}
                          </Select.Option>
                        )
                      })}
                    </ThemeSelect>
                  )
                )}
              </Item>
              {this.props.selectedTab == '8' && (
                <Item className="padding32-item">
                  <InputLabel>Gross Totals</InputLabel>
                  <div>{formatNumber(grossWeightTotal, 2)} {_.get(sellerSetting, 'company.weightUOM')}</div>
                  <div>{formatNumber(grossVolumeTotal, 2)} {_.get(sellerSetting, 'company.volumeUOM')}</div>
                </Item>
              )}
              {currentUser.accountType != UserRole.WAREHOUSE && orderTotalTax > 0 && (
                <>
                  <Item>
                    <InputLabel>Subtotal</InputLabel>
                    <ValueLabel>${formatNumber(orderSubtotal, 2)}</ValueLabel>
                  </Item>
                  <Item>
                    <InputLabel>Tax</InputLabel>
                    <ValueLabel>${formatNumber(orderTotalTax, 2)}</ValueLabel>
                  </Item>
                </>
              )}
              {currentUser.accountType != UserRole.WAREHOUSE && (
                <>
                  <Item className="right padding32-item">
                    <InputLabel>Order Total</InputLabel>
                    <ValueLabel>${formatNumber(orderTotal, 2)}</ValueLabel>
                  </Item>
                </>
              )}
            </FlexDiv>
          </FlexDiv>

          <NewOrderFormModal
            theme={this.props.theme}
            visible={this.state.createShow}
            onOk={this.onClickCreate}
            onCancel={this.onCloseNewOrder}
            clients={simplifyCustomers}
            okButtonName="CREATE"
            modalType="order"
            isNewOrderFormModal={true}
          />
          <ClassNames>
            {({ css, cx }) => (
              this.state.showEPickSheetModal && <ThemeModal
                visible={this.state.showEPickSheetModal}
                title={`ePick Sheet`}
                onCancel={() => this.toggleModal('showEPickSheetModal', false)}
                okText={'Save'}
                cancelText={'Cancel'}
                bodyStyle={{ padding: 0 }}
                className={`${cx(css(noPaddingFooter))}`}
                width={800}
                style={{ height: 600 }}
                footer={null}
              >
                {this.state.showEPickSheetModal && (
                  <EPickSheet
                    visible={this.state.showEPickSheetModal}
                    onClose={this.toggleModal}
                    orderItems={this.props.allOrderItems}
                    order={currentOrder}
                    sellerSetting={sellerSetting}
                    catchWeights={this.props.catchWeights}
                    resetLoading={this.props.resetOnEPickSheet}
                    loading={this.props.loadingOnEPick}
                    getQuantityCatchWeight={this.props.getQuantityCatchWeight}
                    setOrderItemId={this.props.setOrderItemId}
                    updateOrder={this.props.updateOrder}
                    getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                    catchWeightValues={this.props.catchWeightValues}
                    saveEPickedValues={this.props.saveEPickedValues}
                    orderItemByProduct={this.props.orderItemByProduct}
                    startUpdating={this.props.startUpdating}
                    getOrderItemsById={this.props.getOrderItemsById}
                  />
                )}
              </ThemeModal>
            )}
          </ClassNames>
          <ClassNames>
            {({ css, cx }) => (
              <ThemeModal
                visible={this.state.showPalletLabelModal}
                title={`Print Pallet Labels`}
                onCancel={() => this.toggleModal('showPalletLabelModal', false)}
                okText={'Save'}
                cancelText={'Cancel'}
                bodyStyle={{ padding: 0 }}
                className={`${cx(css(noPaddingFooter))}`}
                width={800}
                footer={null}
              >
                {this.state.showPalletLabelModal && (
                  <PrintPalletLabel
                    visible={this.state.showPalletLabelModal}
                    onClose={this.toggleModal}
                    orderItems={this.props.allOrderItems}
                    order={currentOrder}
                    sellerSetting={this.props.sellerSetting}
                    salesOrderPalletLabels={this.props.salesOrderPalletLabels}
                    saveSalesOrderPalletLabels={this.props.saveSalesOrderPalletLabels}
                    getSalesOrderPalletLabels={this.props.getSalesOrderPalletLabels}
                  />
                )}
              </ThemeModal>
            )}
          </ClassNames>
        </PaddingContainer>
      </>
    )
  }
}

export default OrderSummary
