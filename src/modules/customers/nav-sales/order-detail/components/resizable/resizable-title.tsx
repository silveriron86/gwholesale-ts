import React from  'react'
import { Resizable } from 'react-resizable';

export const ResizeableTitle = (props: any) => {
    const { onResize, width, ...restProps } = props
    
    if(width) {
      return (
        <Resizable 
          width={width} 
          height={0} 
          draggableOpts={{ enableUserSelectHack: false }} 
          onResize={onResize}
          handle={
            <span
              className="react-resizable-handle"
              onClick={e => {
                e.stopPropagation();
              }}
            />
          }
          >
          <th {...restProps} />
        </Resizable>
      )
    }
    return <th {...restProps} />
  }