import * as React from 'react'
import { Button, Dropdown, Icon, Select, Table, Tooltip } from 'antd'
import { ThemeCheckbox, ThemeInput, ThemeSelect } from '~/modules/customers/customers.style'
import { Unit } from '../../styles'
import { OrderItem, OrderDetail } from '~/schema'
import jQuery from 'jquery'
import { SideItemSelectedBorder } from '~/modules/product/product.style'
import _ from 'lodash'
import { ENGINE_METHOD_CIPHERS } from 'constants'

type SelectProps = {
  resock:boolean
  orderItem: OrderItem
  from?: string
  handlerChangeUom: Function
  orderIndex?: number
  disabled: boolean
  type: number
  parent?: string
  isFreshGreen?: boolean
  defaultUom?: string
  readOnly?: boolean
}

class UomSelect extends React.PureComponent<SelectProps> {
  componentDidMount() {
    setTimeout(() => {
      this.setKeyboardEvent()
    }, 50)
  }

  setKeyboardEvent = () => {
    const _this = this
    const jQueryWrapper = this.props.type == 1 ? 'div#root' : 'div.ant-modal-wrap'
    jQuery(jQueryWrapper)
      .unbind()
      .bind('keydown', function(e: any) {
        const parent = jQuery(e.target).parents('.uom-select-container')
        if (!parent.length) return
        let rowIndex = typeof _this.props.orderIndex != 'undefined' ? `-${_this.props.orderIndex}` : ''
        let orderItemId: any = null
        if (!rowIndex) {
          const trObj = jQuery(e.target).parents('tr')
          rowIndex = jQuery(trObj).data('row-key')
          orderItemId = rowIndex
          rowIndex = rowIndex ? `-${rowIndex}` : ''
        }
        const options = jQuery(`.sales-cart-uom${rowIndex} li.ant-select-dropdown-menu-item`)
        const active = jQuery(`.sales-cart-uom${rowIndex} li.ant-select-dropdown-menu-item-active`)
        const selected = jQuery(`.sales-cart-uom${rowIndex} li.ant-select-dropdown-menu-item-selected`)

        if (options.length > 1) {
          const index = jQuery(options).index(active[0])
          let newIndex = 0
          if (e.keyCode == 39 || e.keyCode == 40) {
            if (index != options.length - 1) {
              newIndex = index + 1
            } else {
              newIndex = 0
            }
            setTimeout(() => {
              jQuery.each(options, function(i: number, el: any) {
                jQuery(el).removeClass('ant-select-dropdown-menu-item-active')
              })
              jQuery(options[newIndex]).addClass('ant-select-dropdown-menu-item-active')
            }, 50)
            localStorage.setItem('uom-arrow', '1')
          } else if (e.keyCode == 37 || e.keyCode == 38) {
            if (index != 0) {
              newIndex = index - 1
            } else {
              newIndex = options.length - 1
            }
            setTimeout(() => {
              jQuery.each(options, function(i: number, el: any) {
                jQuery(el).removeClass('ant-select-dropdown-menu-item-active')
              })
              jQuery(options[newIndex]).addClass('ant-select-dropdown-menu-item-active')
            }, 50)
            localStorage.setItem('uom-arrow', '1')
          }
          if ((e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 9) && active[0] && selected[0]) {
            if (jQuery(active[0]).text() != jQuery(selected[0]).text()) {
              e.preventDefault()
              setTimeout(() => {
                _this.props.handlerChangeUom(orderItemId, jQuery(active[0]).text(), _this.props.orderIndex, true)
              }, 10)
              setTimeout(() => {
                if (_this.props.isFreshGreen && e.keyCode == 32) {
                  const curEl = e.target
                  if (jQuery(curEl).hasClass('ant-select-selection')) {
                    const ancestor = jQuery(curEl).parents('.select-container-parent')
                    jQuery(e.target).trigger('blur')
                    console.log('call this action when uom is changed')
                    jQuery(ancestor).addClass('custom-ant-select-focused')
                  }
                }
              }, 10)
            }
          }
        }
      })
      .bind('keyup', (e: any) => {
        if (e.keyCode == 13) {
          localStorage.setItem('uom-arrow', '-1')
        }
      })
  }

  getInventorySelect = (orderItem: OrderItem,resock:boolean) => {
    if (
      (orderItem && orderItem.useForSelling == true && this.props.type == 1) ||
      (orderItem && orderItem.useForPurchasing == true && this.props.type == 2)||
      resock
    ) {
      return (
        <Select.Option value={orderItem.inventoryUOM} title={orderItem.inventoryUOM}>
          {orderItem.inventoryUOM}
        </Select.Option>
      )
    }
  }

  render() {
    const { orderItem, from, orderIndex, disabled, type, defaultUom, readOnly,resock } = this.props

    let uom = orderItem.UOM ? orderItem.UOM : orderItem.inventoryUOM
    if (from == 'pricingModal') {
      uom = orderItem.pricingUOM ? orderItem.pricingUOM : orderItem.inventoryUOM
    } else if (from == 'creditMemo') {
      uom = orderItem.returnUom ? orderItem.returnUom : orderItem.inventoryUOM
    }
    let rowKey = typeof this.props.orderIndex != 'undefined' ? `-${this.props.orderIndex}` : ''
    if (type == 1 && orderItem) {
      rowKey = `-${orderItem.wholesaleOrderItemId}`
    }
    return (
      <div className="select-container-parent">
        <ThemeSelect
          readOnly={!!readOnly}
          className="uom-select-container"
          dropdownMatchSelectWidth={false}
          value={defaultUom || uom}
          suffixIcon={<Icon type="caret-down" />}
          onChange={(val: any) => this.props.handlerChangeUom(orderItem.wholesaleOrderItemId, val, orderIndex)}
          style={{ minWidth: 80, width: '100%' }}
          disabled={disabled}
          dropdownRender={(el: any) => {
            return <div className={`sales-cart-uom${rowKey}`}>{el}</div>
          }}
        >
          {this.getInventorySelect(orderItem,resock)}
          {orderItem.wholesaleProductUomList != null && orderItem.wholesaleProductUomList.length > 0
            ? orderItem.wholesaleProductUomList.map((uom) => {
                if (!uom.deletedAt && ((type == 1 && uom.useForSelling) || (type == 2 && uom.useForPurchasing))) {
                  return (
                    <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                      {uom.name}
                    </Select.Option>
                  )
                }
              })
            : ''}
        </ThemeSelect>
      </div>
    )
  }
}

export default UomSelect
