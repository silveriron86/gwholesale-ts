import { Checkbox, DatePicker, Input } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas34A from '~/modules/product/components/Label/components/LabelCanvas34A'
import { label34AObj, label34AObjSetting } from '~/modules/product/components/Label/components/type/type'

interface IProps {
  labelObj: label34AObj
  passSetting: (obj: label34AObjSetting) => void
  print: () => void
}

const Label34A = ({ labelObj, passSetting, print }: IProps): JSX.Element => {
  const [contractNumber, setContractNumber] = useState(labelObj.contractNumber)
  const [NSN, setNSN] = useState(labelObj.NSN)
  const [packDate, setPackDate] = useState(labelObj.packDate)
  const [sellByDate, setSellByDate] = useState(labelObj.sellByDate)
  const [note, setNote] = useState(labelObj.note)
  const [mfgSku, setMfgSku] = useState(labelObj.mfgSku)
  const [originEnable, setOriginEnable] = useState(false)
  const [packDateEnable, setPackDateEnable] = useState(false)
  const [bestBySellDateEnable, setBestBySellDateEnable] = useState(false)
  const [storage, setStorage] = useState(labelObj.storage)
  const [brand, setBrand] = useState(labelObj.brand)
  const [origin, setOrigin] = useState(labelObj.origin)
  const [lot, setLot] = useState(labelObj.lot)
  const [manufacturer, setManufacturer] = useState(labelObj.manufacturer)
  const [generic, setGeneric] = useState<boolean>(false)

  const productName = labelObj.productName
  const PO = labelObj.PO
  const packSize = labelObj.packSize

  useEffect(() => {
    setContractNumber(labelObj.contractNumber)
    setNSN(labelObj.NSN)
    setPackDate(labelObj.packDate)
    setSellByDate(labelObj.sellByDate)
    setNote(labelObj.note)
    setMfgSku(labelObj.mfgSku)
    setPackDateEnable(labelObj.packDateEnable)
    setBestBySellDateEnable(labelObj.sellByDateEnable)
    setStorage(labelObj.storage)
    setBrand(labelObj.brand)
    setOrigin(labelObj.origin)
    setLot(labelObj.lot)
    setManufacturer(labelObj.manufacturer)
    setGeneric(labelObj.brand == '-1' ? true : false)
  }, [labelObj])

  useEffect(() => {
    const obj: label34AObjSetting = {
      contractNumber: contractNumber,
      NSN: NSN,
      packDateEnable: packDateEnable,
      sellByDateEnable: bestBySellDateEnable,
      packDate: packDate,
      sellByDate: sellByDate,
      note: note,
      mfgSku: mfgSku,
      originEnable: originEnable,
      storage: storage,
      brand: brand,
      origin: origin,
      lot: lot,
      manufacturer: manufacturer,
    }
    passSetting(obj)
  }, [
    contractNumber,
    NSN,
    packDate,
    sellByDate,
    note,
    mfgSku,
    originEnable,
    packDateEnable,
    bestBySellDateEnable,
    storage,
    brand,
    origin,
    lot,
    manufacturer,
    generic,
  ])

  const getLabelObj = (): label34AObj => {
    return {
      productName: productName,
      contractNumber: contractNumber,
      PO: PO,
      NSN: NSN,
      brand: brand,
      packSize: packSize,
      storage: storage,
      origin: origin,
      originEnable: originEnable,
      packDateEnable: packDateEnable,
      packDate: packDate,
      sellByDateEnable: bestBySellDateEnable,
      sellByDate: sellByDate,
      lot: lot,
      manufacturer: manufacturer,
      note: note,
      mfgSku: mfgSku,
    }
  }

  const handleSellByDate = (e: any) => {
    if (e) {
      const date = new Date(e)
      const year = date.getFullYear()
      const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
      const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
      setSellByDate(`${month}-${day}-${year}`)
    } else {
      setSellByDate(``)
    }
  }

  const handlePackDate = (e: any) => {
    if (e) {
      const date = new Date(e)
      const year = date.getFullYear()
      const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
      const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
      setPackDate(`${month}-${day}-${year}`)
    } else {
      setPackDate(``)
    }
  }

  const time = (time: number | string) => {
    if (typeof time == 'number') {
      var date = new Date(time)
      const year = date.getFullYear()
      const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
      const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
      return `${month}-${day}-${year}`
    } else {
      return time
    }
  }

  return (
    <div
      style={{
        borderLeft: '1px solid #D8DBDB',
        height: 'auto',
      }}
    >
      <div style={{ width: '460px', marginLeft: '33px' }}>
        <div style={{ paddingTop: '23px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            Product Name
          </p>
          {productName}
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Contract Number
            </p>
            <Input
              style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
              value={contractNumber}
              onChange={(e) => setContractNumber(e.target.value)}
            />
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              PO#
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{PO}</p>
          </div>
        </div>
        <div style={{ marginTop: '8px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            NSN#
          </p>
          <Input
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
            value={NSN}
            onChange={(e) => setNSN(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Brand
            </p>
            {generic ? (
              <Input
                style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
                value={brand == '-1' ? '' : brand}
                onChange={(e) => setBrand(e.target.value)}
              />
            ) : (
              <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{brand}</p>
            )}
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Pack Size
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{packSize}</p>
          </div>
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Storage
            </p>
            <Input
              style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
              value={storage}
              onChange={(e) => setStorage(e.target.value)}
            />
          </div>
          <div style={{ width: '50%' }}>
            <div style={{ display: 'flex' }}>
              <Checkbox
                style={{ marginRight: '4px' }}
                checked={originEnable}
                onChange={(e) => setOriginEnable(e.target.checked)}
              />{' '}
              <p
                style={{
                  fontSize: '14px',
                  lineHeight: '19px',
                  marginBottom: '0px',
                  fontWeight: 'normal',
                }}
              >
                Origin
              </p>
            </div>
            {generic ? (
              <Input
                style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
                value={origin == '-1' ? '' : origin}
                onChange={(e) => setOrigin(e.target.value)}
              />
            ) : (
              <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{origin}</p>
            )}
          </div>
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '3px',
                fontWeight: 'normal',
              }}
            >
              Pack Date
            </p>
            <div style={{ display: 'flex' }}>
              <Checkbox
                style={{ marginTop: '4px', marginRight: '4px' }}
                checked={packDateEnable}
                onChange={(e) => setPackDateEnable(e.target.checked)}
              />
              <DatePicker
                defaultValue={moment(time(parseInt(packDate)), 'MM-DD-YYYY')}
                onChange={(e) => handlePackDate(e)}
              />
            </div>
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '3px',
                fontWeight: 'normal',
              }}
            >
              Best By Date
            </p>
            <div style={{ display: 'flex' }}>
              <Checkbox
                style={{ marginTop: '4px', marginRight: '4px' }}
                checked={bestBySellDateEnable}
                onChange={(e) => setBestBySellDateEnable(e.target.checked)}
              />
              <DatePicker value={moment(time(sellByDate), 'MM-DD-YYYY')} onChange={(e) => handleSellByDate(e)} />
            </div>
          </div>
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Lot #
            </p>
            {generic ? (
              <Input
                style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
                value={lot == '-1' ? '' : lot}
                onChange={(e) => setLot(e.target.value)}
              />
            ) : (
              <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{lot}</p>
            )}
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Manufacturer
            </p>
            {generic ? (
              <Input
                style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
                value={manufacturer == '-1' ? '' : manufacturer}
                onChange={(e) => setManufacturer(e.target.value)}
              />
            ) : (
              <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>
                {manufacturer}
              </p>
            )}
          </div>
        </div>
        <div style={{ width: '90%' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            Note
          </p>
          <Input
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
            value={note}
            onChange={(e) => setNote(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '21px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
              marginTop: '28px',
            }}
          >
            MFG SKU
          </p>
          <Input
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
            value={mfgSku}
            onChange={(e) => setMfgSku(e.target.value)}
          />
        </div>
      </div>
      <div style={{ height: '445px' }}>
        <p
          style={{
            marginLeft: '33px',
            fontSize: '15px',
            lineHeight: '19px',
            marginBottom: '0px',
            fontWeight: 'normal',
            marginTop: '28px',
          }}
        >
          Preview
        </p>
        <LabelCanvas34A labelObj={getLabelObj()} />
      </div>
      <div
        style={{
          paddingLeft: '33px',
          paddingTop: '14px',
          paddingBottom: '14px',
          textAlign: 'left',
          display: 'felx',
          backgroundColor: '#e8e8e8',
          width: '554px',
          position: 'relative',
          borderTop: '1px solid #F7F7F7',
          height: '68px',
        }}
      >
        <ThemeButton
          style={{ right: '-390px', borderRadius: '40px', height: '40px' }}
          onClick={() => {
            print()
          }}
        >
          Print Label
        </ThemeButton>
      </div>
    </div>
  )
}

export default Label34A
