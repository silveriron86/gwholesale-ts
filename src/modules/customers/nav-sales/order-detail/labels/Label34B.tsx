import { Button, Checkbox, Input, Radio } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas34B from '~/modules/product/components/Label/components/LabelCanvas34B'
import { label34BObj, label34BObjSetting } from '~/modules/product/components/Label/components/type/type'

interface IProps {
  labelObj: label34BObj
  passSetting: (obj: label34BObjSetting) => void
  print: () => void
}

const Label34B = ({ labelObj, passSetting, print }: IProps): JSX.Element => {
  const [contractNumber, setContractNumber] = useState(labelObj.contractNumber)
  const [NSN, setNSN] = useState(labelObj.NSN)
  const [mfgSkuEnable, setMfgSkuEnable] = useState(labelObj.mfgSkuEnable)
  const [originEnable, setOriginEnable] = useState(false)
  const [storage, setStorage] = useState(labelObj.storage)
  const [brand, setBrand] = useState(labelObj.brand)
  const [origin, setOrigin] = useState(labelObj.origin)
  const [generic, setGeneric] = useState<boolean>(false)

  const productName = labelObj.productName
  const PO = labelObj.PO
  const packSize = labelObj.packSize
  const uom = labelObj.uom
  const grossWeight = labelObj.grossWeight
  const netWeight = labelObj.netWeight
  const mfgSku = labelObj.mfgSku

  useEffect(() => {
    const obj: label34BObjSetting = {
      contractNumber: contractNumber,
      NSN: NSN,
      mfgSkuEnable: mfgSkuEnable !== undefined ? mfgSkuEnable : false,
      originEnable: originEnable !== undefined ? originEnable : false,
      storage: storage,
      brand: brand,
      origin: origin,
    }
    passSetting(obj)
  }, [contractNumber, NSN, mfgSkuEnable, originEnable, brand, origin, generic])

  useEffect(() => {
    setContractNumber(labelObj.contractNumber)
    setNSN(labelObj.NSN)
    setMfgSkuEnable(labelObj.mfgSkuEnable)
    setBrand(labelObj.brand)
    setOrigin(labelObj.origin)
    setGeneric(labelObj.brand == '-1' ? true : false)
  }, [labelObj])

  const getLabelObj = (): label34BObj => {
    return {
      productName: productName,
      contractNumber: contractNumber,
      PO: PO,
      NSN: NSN,
      brand: brand,
      packSize: packSize,
      storage: storage,
      uom: uom,
      origin: origin,
      originEnable: originEnable,
      grossWeight: grossWeight,
      netWeight: netWeight,
      mfgSku: mfgSku,
      mfgSkuEnable: mfgSkuEnable,
    }
  }

  return (
    <div
      style={{
        borderLeft: '1px solid #D8DBDB',
        height: 'auto',
      }}
    >
      <div style={{ width: '460px', marginLeft: '33px' }}>
        <div style={{ paddingTop: '23px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            Product Name
          </p>
          {productName}
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Contract Number
            </p>
            <Input
              style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
              value={contractNumber}
              onChange={(e) => setContractNumber(e.target.value)}
            />
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              PO#
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{PO}</p>
          </div>
        </div>
        <div style={{ marginTop: '8px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            NSN#
          </p>
          <Input
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
            value={NSN}
            onChange={(e) => setNSN(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Brand
            </p>
            {generic ? (
              <Input
                style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
                value={brand == '-1' ? '' : brand}
                onChange={(e) => setBrand(e.target.value)}
              />
            ) : (
              <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{brand}</p>
            )}
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Pack Size
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{packSize}</p>
          </div>
        </div>
        <div style={{ marginTop: '8px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            Storage
          </p>
          <Input
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
            value={storage}
            onChange={(e) => setStorage(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              UOM
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{uom}</p>
          </div>
          <div style={{ width: '50%' }}>
            <div style={{ display: 'flex' }}>
              <Checkbox
                style={{ marginRight: '4px' }}
                checked={originEnable}
                onChange={(e) => setOriginEnable(e.target.checked)}
              />{' '}
              <p
                style={{
                  fontSize: '14px',
                  lineHeight: '19px',
                  marginBottom: '0px',
                  fontWeight: 'normal',
                }}
              >
                Origin
              </p>
            </div>
            {generic ? (
              <Input
                style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px', width: '90%' }}
                value={origin == '-1' ? '' : origin}
                onChange={(e) => setOrigin(e.target.value)}
              />
            ) : (
              <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{origin}</p>
            )}
          </div>
        </div>
        <div style={{ marginTop: '8px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Gross Weight
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>
              {grossWeight}
            </p>
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              Net Weight
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{netWeight}</p>
          </div>
        </div>
        <div style={{ marginTop: '8px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
              marginTop: '1px',
            }}
          >
            MFG SKU
          </p>
          <div style={{ display: 'flex' }}>
            <Checkbox checked={mfgSkuEnable} onChange={(e) => setMfgSkuEnable(e.target.checked)} />
            <p style={{ fontSize: '15px', lineHeight: '1px', fontWeight: 'normal', marginTop: '9px' }}>{mfgSku}</p>
          </div>
        </div>
      </div>
      <div style={{ height: '445px' }}>
        <p
          style={{
            marginLeft: '33px',
            fontSize: '15px',
            lineHeight: '19px',
            marginBottom: '0px',
            fontWeight: 'normal',
            marginTop: '28px',
          }}
        >
          Preview
        </p>
        <LabelCanvas34B labelObj={getLabelObj()} />
      </div>
      <div
        style={{
          paddingLeft: '33px',
          paddingTop: '14px',
          paddingBottom: '14px',
          textAlign: 'left',
          display: 'felx',
          backgroundColor: '#e8e8e8',
          width: '554px',
          position: 'relative',
          borderTop: '1px solid #F7F7F7',
          height: '68px',
        }}
      >
        <ThemeButton
          style={{ right: '-390px', borderRadius: '40px', height: '40px' }}
          onClick={() => {
            print()
          }}
        >
          Print Label
        </ThemeButton>
      </div>
    </div>
  )
}

export default Label34B
