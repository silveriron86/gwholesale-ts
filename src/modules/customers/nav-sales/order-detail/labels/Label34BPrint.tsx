import React, { useEffect, useState } from 'react'
import { label34BObj } from '~/modules/product/components/Label/components/type/type'
import { Flex, ThemeButton } from '~/modules/customers/customers.style'
import Barcode from 'react-barcode'
import { printWindow } from '~/common/utils'
import { Spin } from 'antd'

interface IProps {
  labelObjs: label34BObj[]
}

const Label34BPrint = ({ labelObjs }: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false)
  const [elements, setElements] = useState<any>(null)

  useEffect(() => {
    setLoading(true)
    setElements(getHtmlElements())
    setTimeout(() => {
      setLoading(false)
    }, 3000)
  }, [labelObjs])

  useEffect(() => {
    //console.log(loading)
  }, [loading])

  useEffect(() => {
    //console.log(elements)
  }, [elements])

  const printCanvas = () => {
    printWindow('Print34LabelsModal')
  }

  const getHtmlElements = () => {
    return labelObjs.map((item, index) => {
      return (
        <div
          key={`row-${index}`}
          style={{
            height: '401px',
            width: '300px',
            transform: 'rotate(90deg)',
            position: 'absolute',
            top: `${-50 + 315 * index}px`,
            left: '80px',
          }}
        >
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`Contract Number: ${item.contractNumber}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`PO#: ${item.PO}`}
            </p>
          </div>
          <div>
            <p
              style={{
                fontSize: '14px',
                marginBottom: '0px',
                maxHeight: '40px',
                lineHeight: '20px',
                color: 'black',
                width: '100%',
                wordBreak: 'break-all',
              }}
            >
              {`NSN#: ${item.NSN}`}
            </p>
          </div>
          <div>
            <p
              style={{
                fontSize: '14px',
                marginBottom: '0px',
                maxHeight: '60px',
                lineHeight: '20px',
                color: 'black',
                overflow: 'hidden',
              }}
            >
              {`${item.productName}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`Brand: ${item.brand == '-1' ? '' : item.brand}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`Pack Size: ${item.packSize}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`${item.storage}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`${item.uom}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`Gross Weight: ${item.grossWeight ? item.grossWeight : ''}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`Net Weight: ${item.netWeight ? item.netWeight : ''}`}
            </p>
          </div>
          <div>
            <p style={{ fontSize: '14px', marginBottom: '0px', height: '20px', lineHeight: '20px', color: 'black' }}>
              {`Origin: ${item.origin == '-1' ? '' : item.origin}`}
            </p>
          </div>
          {item.mfgSkuEnable && item.mfgSku !== '' ? (
            <div style={{ textAlign: 'left', justifyContent: 'left' }}>
              <Barcode value={item.mfgSku} displayValue={true} fontSize={14} width={1} height={40} />
            </div>
          ) : (
            <></>
          )}
          <div style={{ pageBreakAfter: 'always' }} />
        </div>
      )
    })
  }

  return (
    <div style={{ height: 'auto', padding: '20px' }}>
      <Spin style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == true ? 'inline' : 'none'}` }} />
      <ThemeButton
        style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == false ? 'inline' : 'none'}` }}
        onClick={() => {
          printCanvas()
        }}
      >
        Print
      </ThemeButton>
      <div id="Print34LabelsModal" style={{ position: 'relative', display: 'none' }}>
        {elements ? elements : ''}
      </div>
    </div>
  )
}

export default Label34BPrint
