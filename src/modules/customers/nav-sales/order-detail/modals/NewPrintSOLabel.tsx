import { Modal, Select } from 'antd'
import React, { useEffect, useState } from 'react'
import {
  label34AObj,
  label34AObjSetting,
  label34BObj,
  label34BObjSetting,
} from '~/modules/product/components/Label/components/type/type'
import Label34A from '../labels/Label34A'
import Label34APrint from '../labels/Label34APrint'
import Label34B from '../labels/Label34B'
import Label34BPrint from '../labels/Label34BPrint'
import _ from 'lodash'

interface IProps {
  order: any
  poObj: any
  orderItem: any
  contractNumber: string
  NSN: string
  weightUOM: string
}

const { Option } = Select

const getPoId = (lotId: string) => {
  const index = lotId.indexOf('-')
  return index == -1 ? lotId : lotId.slice(0, index)
}

const NewPrintSOLabel = ({ order, poObj, orderItem, contractNumber, NSN, weightUOM }: IProps): JSX.Element => {
  const [unit, setUnit] = useState<string>('3x4A')
  const [showModal, setShowModal] = useState<boolean>(false)
  const [lotOptions, setLotOptions] = useState<string[]>([])
  const [lotIndex, setLotIndex] = useState<number>(0)
  const [quantity, setQuantity] = useState<string>('')
  const [picked, setPicked] = useState<string>('')

  const getLabel34AObj = (index: number): label34AObj => {
    return {
      productName: orderItem.variety,
      contractNumber: contractNumber,
      PO: index > -1 ? (orderItem.items[index].lotId ? getPoId(orderItem.items[index].lotId) : '') : '',
      NSN: orderItem.customerProductCode ? orderItem.customerProductCode : '',
      brand: index > -1 ? (orderItem.modifiers ? orderItem.modifiers : '') : '-1',
      packSize: orderItem.packing ? orderItem.packing : '',
      storage: 'Keep Chilled',
      origin: index > -1 ? orderItem.extraOrigin : '-1',
      originEnable: false,
      packDateEnable: true,
      packDate: new Date().valueOf().toString(),
      sellByDateEnable: true,
      sellByDate:
        index > -1
          ? orderItem.items[index].receivedDate
            ? orderItem.items[index].receivedDate
            : new Date('2000-01-01').valueOf()
          : new Date('2000-01-01').valueOf(),
      lot: index > -1 ? orderItem.items[index].lotId : '-1',
      manufacturer: index > -1 ? (orderItem.modifiers ? orderItem.modifiers : '') : '-1',
      note: '',
      mfgSku: index > -1 ? (orderItem.items[index].upc ? orderItem.items[index].upc : '') : '',
    }
  }

  const getLabel34BObj = (index: number): label34BObj => {
    return {
      productName: orderItem.variety,
      contractNumber: contractNumber,
      PO: index > -1 ? (orderItem.items[index].lotId ? getPoId(orderItem.items[index].lotId) : '') : '',
      NSN: orderItem.customerProductCode ? orderItem.customerProductCode : '',
      brand: index > -1 ? (orderItem.modifiers ? orderItem.modifiers : '') : '-1',
      packSize: orderItem.packing ? orderItem.packing : '',
      storage: 'Keep Chilled',
      uom: orderItem.UOM,
      origin: index > -1 ? orderItem.extraOrigin : '-1',
      originEnable: false,
      grossWeight:
        index > -1
          ? orderItem.items[index].grossWeight
            ? `${orderItem.items[index].grossWeight} ${weightUOM ? weightUOM : ''}`
            : ''
          : order.totalGrossWeight,
      netWeight:
        index > -1
          ? orderItem.items[index].grossWeight
            ? `${orderItem.items[index].grossWeight} ${weightUOM ? weightUOM : ''}`
            : ''
          : order.totalGrossWeight,
      mfgSku: index > -1 ? (orderItem.items[index].upc ? orderItem.items[index].upc : '') : '',
      mfgSkuEnable: false,
    }
  }

  const [label34AObj, setLabel34AObj] = useState<label34AObj>(getLabel34AObj(0))

  const [label34BObj, setLabel34BObj] = useState<label34BObj>(getLabel34BObj(0))

  const getLabel34AObjSetting = (): label34AObjSetting => {
    return {
      contractNumber: label34AObj.contractNumber,
      NSN: label34AObj.NSN,
      packDateEnable: label34AObj.packDateEnable,
      sellByDateEnable: label34AObj.sellByDateEnable,
      packDate: label34AObj.packDate,
      sellByDate: label34AObj.sellByDate,
      note: label34AObj.note,
      mfgSku: label34AObj.mfgSku,
      originEnable: label34AObj.originEnable ? label34AObj.originEnable : false,
      storage: label34AObj.storage,
    }
  }

  const getLabel34BObjSetting = () => {
    return {
      contractNumber: label34BObj.contractNumber,
      NSN: label34BObj.NSN,
      mfgSkuEnable: label34BObj.mfgSkuEnable ? label34BObj.mfgSkuEnable : false,
      originEnable: label34BObj.originEnable ? label34BObj.originEnable : false,
      storage: label34AObj.storage,
    }
  }

  const [label34AObjSetting, setLabel34AObjSetting] = useState<label34AObjSetting>(getLabel34AObjSetting())

  const [label34BObjSetting, setLabel34BObjSetting] = useState<label34BObjSetting>(getLabel34BObjSetting())

  useEffect(() => {
    setLotIndex(0)
    const options: string[] = orderItem.items.map((item: any) => item.lotId)
    setLotOptions(options)
    setQuantity(orderItem.items[0].quantity)
    setPicked(orderItem.items[0].picked)
    //update page
    setLabel34AObj(getLabel34AObj(0))
    setLabel34BObj(getLabel34BObj(0))
    setLabel34AObjSetting(getLabel34AObjSetting())
    setLabel34BObjSetting(getLabel34BObjSetting())
  }, [orderItem])

  useEffect(() => {
    if (lotIndex >= 0) {
      setLabel34AObj(getLabel34AObj(lotIndex))
      setLabel34BObj(getLabel34BObj(lotIndex))
      setLabel34AObjSetting(getLabel34AObjSetting())
      setLabel34BObjSetting(getLabel34BObjSetting())
      setQuantity(orderItem.items[lotIndex].quantity)
      setPicked(orderItem.items[lotIndex].picked)
    } else {
      setLabel34AObj(getLabel34AObj(lotIndex))
      setLabel34AObjSetting(getLabel34AObjSetting())
      setLabel34BObj(getLabel34BObj(lotIndex))
      setLabel34BObjSetting(getLabel34BObjSetting())
      setQuantity('0')
      setPicked('0')
    }
  }, [lotIndex])

  useEffect(() => {
    //console.log(label252ObjSetting)
  }, [label34AObjSetting, label34BObjSetting, label34AObj, quantity, picked])

  useEffect(() => {
    setLabel34AObjSetting({
      contractNumber: label34AObj.contractNumber,
      NSN: label34AObj.NSN,
      packDateEnable: label34AObj.packDateEnable,
      sellByDateEnable: label34AObj.sellByDateEnable,
      packDate: label34AObj.packDate,
      sellByDate: label34AObj.sellByDate,
      note: label34AObj.note,
      mfgSku: label34AObj.mfgSku,
      originEnable: label34AObj.originEnable ? label34AObj.originEnable : false,
      storage: label34AObj.storage,
    })
    setLabel34BObjSetting({
      contractNumber: label34BObj.contractNumber,
      NSN: label34BObj.NSN,
      mfgSkuEnable: label34BObj.mfgSkuEnable ? label34BObj.mfgSkuEnable : false,
      originEnable: label34BObj.originEnable ? label34BObj.originEnable : false,
      storage: label34AObj.storage,
    })
  }, [label34AObj, label34BObj])

  useEffect(() => {
    const newLabel34AObj = label34AObj
    newLabel34AObj.contractNumber = contractNumber
    setLabel34AObj(_.cloneDeep(newLabel34AObj))
    const newLabel34BObj = label34BObj
    newLabel34BObj.contractNumber = contractNumber
    setLabel34BObj(_.cloneDeep(newLabel34BObj))
  }, [contractNumber])

  const getRightPart = () => {
    switch (unit) {
      case '3x4A':
        return (
          <Label34A
            labelObj={label34AObj}
            passSetting={(obj: label34AObjSetting) => setLabel34AObjSetting(obj)}
            print={() => setShowModal(true)}
          />
        )
      case '3x4B':
        return (
          <Label34B
            labelObj={label34BObj}
            passSetting={(obj: label34BObjSetting) => setLabel34BObjSetting(obj)}
            print={() => setShowModal(true)}
          />
        )
    }
  }

  const getMutipleCanvas = () => {
    const index = orderItem.items[lotIndex] ? lotIndex : 0
    switch (unit) {
      case '3x4A':
        const labels34A: label34AObj[] = []
        labels34A.push({
          productName: orderItem.variety,
          contractNumber: label34AObjSetting.contractNumber,
          PO: index > -1 ? (orderItem.items[index].lotId ? getPoId(orderItem.items[index].lotId) : '') : '',
          NSN: label34AObjSetting.NSN,
          brand: lotIndex == -1 ? label34AObjSetting.brand : orderItem.modifiers ? orderItem.modifiers : '',
          packSize: orderItem.packing ? orderItem.packing : '',
          storage: label34AObjSetting.storage,
          origin: lotIndex == -1 ? label34AObjSetting.origin : orderItem.extraOrigin ? orderItem.extraOrigin : '',
          originEnable: label34AObjSetting.originEnable,
          packDateEnable: label34AObjSetting.packDateEnable,
          sellByDateEnable: label34AObjSetting.sellByDateEnable,
          packDate: label34AObjSetting.packDate,
          sellByDate: label34AObjSetting.sellByDate,
          lot: lotIndex == -1 ? label34AObjSetting.lot : orderItem.items[index].lotId,
          manufacturer:
            lotIndex == -1 ? label34AObjSetting.manufacturer : orderItem.modifiers ? orderItem.modifiers : '',
          note: label34AObjSetting.note,
          mfgSku: label34AObjSetting.mfgSku,
        })
        return <Label34APrint labelObjs={labels34A} />
      case '3x4B':
        const labels34B: label34BObj[] = []
        labels34B.push({
          productName: orderItem.variety,
          contractNumber: label34BObjSetting.contractNumber,
          PO: index > -1 ? (orderItem.items[index].lotId ? getPoId(orderItem.items[index].lotId) : '') : '',
          NSN: label34BObjSetting.NSN,
          brand: lotIndex == -1 ? label34BObjSetting.brand : orderItem.modifiers ? orderItem.modifiers : '',
          packSize: orderItem.packing ? orderItem.packing : '',
          storage: label34BObjSetting.storage,
          uom: orderItem.UOM,
          origin: lotIndex == -1 ? label34BObjSetting.origin : orderItem.extraOrigin ? orderItem.extraOrigin : '',
          originEnable: label34BObjSetting.originEnable,
          grossWeight:
            lotIndex == -1
              ? order.totalGrossWeight.toString()
              : orderItem.items[index].grossWeight
              ? `${orderItem.items[index].grossWeight} ${weightUOM ? weightUOM : ''}`
              : '',
          netWeight:
            lotIndex == -1
              ? order.totalGrossWeight.toString()
              : orderItem.items[index].grossWeight
              ? `${orderItem.items[index].grossWeight} ${weightUOM ? weightUOM : ''}`
              : '',
          mfgSku: orderItem.items[index].upc ? orderItem.items[index].upc : '',
          mfgSkuEnable: label34BObjSetting.mfgSkuEnable,
        })
        return <Label34BPrint labelObjs={labels34B} />
    }
  }

  return (
    <div style={{ padding: '0px' }}>
      <div style={{ height: '70px', borderBottom: '1px solid #D8DBDB', display: 'flex' }}>
        <p style={{ marginLeft: '20px', paddingTop: '16px', fontSize: '18px' }}>{`Labels - ${
          unit == '3x4A' ? '3x4A' : '3x4B'
        }`}</p>
        <div style={{ display: 'flex', height: '70px', margin: '3px' }}>
          <div style={{ marginLeft: '20px' }}>
            <p style={{ marginBottom: '0px' }}>Lot #</p>
            <Select
              value={lotIndex > -1 ? label34AObj.lot : 'Generic'}
              style={{ width: 120 }}
              onChange={(e: any) => {
                const index = lotOptions.findIndex((x) => x == e)
                setLotIndex(index)
              }}
            >
              <Option value={'Generic'} key={-1}>
                {'Generic'}
              </Option>
              {lotOptions.map((option, index) => {
                if (option != '') {
                  return (
                    <Option value={option} key={index}>
                      {option}
                    </Option>
                  )
                }
              })}
            </Select>
          </div>
          <div style={{ marginLeft: '20px' }}>
            <p style={{ marginBottom: '0px' }}>Quantity</p>
            <p style={{ marginBottom: '0px' }}>{quantity}</p>
          </div>
          <div style={{ marginLeft: '20px' }}>
            <p style={{ marginBottom: '0px' }}>Picked</p>
            <p style={{ marginBottom: '0px' }}>{picked}</p>
          </div>
        </div>
      </div>
      <div style={{ display: 'flex', backgroundColor: 'white' }}>
        <div
          style={{
            width: '160px',
            display: 'inline',
            textAlign: 'left',
            position: 'relative',
          }}
        >
          <p
            style={{
              paddingLeft: '24px',
              cursor: 'pointer',
              height: '36px',
              lineHeight: '36px',
              marginBottom: '0px',
              background: `${unit == '3x4A' ? '#F2F9F3' : 'white'}`,
            }}
            onClick={() => setUnit('3x4A')}
          >
            UNIT 3x4A
          </p>
          <p
            style={{
              paddingLeft: '24px',
              cursor: 'pointer',
              height: '36px',
              lineHeight: '36px',
              marginBottom: '0px',
              background: `${unit == '3x4B' ? '#F2F9F3' : 'white'}`,
            }}
            onClick={() => setUnit('3x4B')}
          >
            UNIT 3x4B
          </p>
          <div
            style={{
              height: '67px',
              width: '149px',
              background: '#e8e8e8',
              position: 'absolute',
              bottom: '0px',
            }}
          >
            <p style={{ color: '#e8e8e8' }}>s</p>
          </div>
        </div>
        {getRightPart()}
        <Modal
          style={{ padding: '0px' }}
          width={703}
          footer={null}
          visible={showModal}
          onCancel={() => {
            setShowModal(false)
          }}
        >
          <div>{getMutipleCanvas()}</div>
        </Modal>
      </div>
    </div>
  )
}

export default NewPrintSOLabel
