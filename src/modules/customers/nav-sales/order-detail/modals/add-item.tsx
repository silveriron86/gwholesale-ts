import { Button, Icon, Table, Tabs, Tooltip, Checkbox } from 'antd'
import * as React from 'react'
import { withTheme } from 'emotion-theming'
import moment from 'moment'
import lodash, {isArray, cloneDeep} from 'lodash'
import {
  AddItemNameStyle,
  AddItemTableWrapper,
  EmptyResult,
  Flex,
  GridTableWrapper,
  ItemHMargin4,
  TabsWrapper,
  ThemeColorSpan,
} from '../../styles'
import {
  formatNumber,
  getFileUrl,
  combineUomForInventory,
  mathRoundFun,
  inventoryQtyToRatioQty,
  formatTimeDifference,
} from '~/common/utils'
import { OrdersDispatchProps, OrdersModule, OrdersStateProps } from '~/modules/orders'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { Icon as IconSVG } from '~/components/icon'
import { ThemeButton, ThemeOutlineButton, ThemeSpin } from '~/modules/customers/customers.style'
import SearchInput from '~/components/Table/search-input'
import { ThumbnailImage } from '~/modules/inventory/components/inventory-table.style'
import { OrderItem } from '~/schema'
import jQuery from 'jquery'

const { TabPane } = Tabs

type OrderItemModalProps = OrdersDispatchProps &
  OrdersStateProps & {
    visible: boolean
    onClose: Function
    onSelect?: Function
    addOffItem: Function
    salesType: string
    theme: Theme
    insertItemPosition?: number
    wo?: boolean
  }

class AddOrderItemModal extends React.PureComponent<OrderItemModalProps> {
  focusedIndex: number = 0
  focusedRowIndex: number = 0
  moveDirection: string = ''
  isNavigation: boolean = false
  tabEls: any[] = []
  tableRows: any[] = []
  state = {
    searchText: '',
    searchWithIn: '',
    pureSearchText: '',
    placeholder: 'Search by product name, SKU, or Lot #...',
    isExpanded: false,
    currentItemId: 0,
    saleItems: []
  }

  componentDidMount() {
    const { salesType, currentOrder } = this.props

    this.props.setLoadingForGetItems()
    this.props.getItemForAddItemModal({
      type: salesType,
      clientId: currentOrder?.wholesaleClient?.clientId
    }) //set the salesType SELL or PURCHASE to get the items list in each page
    if (this.props.wo !== true && currentOrder) {
      this.props.getFrequentSalesOrderItems({ clientId: currentOrder.wholesaleClient.clientId, search: '' })
    }
    // setTimeout(() => {
    //   this.setTab(true)
    // }, 50)
  }

  onTabClick = () => {
    jQuery('.add-item-table-wrapper').animate({ scrollLeft: 0 }, 200, 'swing', function () { })
  }

  componentWillReceiveProps(nextProps: OrderItemModalProps) {
    if (nextProps.visible !== this.props.visible) {
      this.setState({
        searchText: '',
        searchWithIn: '',
      })
    }
    if (nextProps.visible !== this.props.visible && nextProps.visible) {
      const { currentOrder } = this.props
      if (!this.props.saleItems.length) {
        this.props.getItemForAddItemModal({
          type: this.props.salesType,
          clientId: currentOrder?.wholesaleClient?.clientId
        }) //set the salesType SELL or PURCHASE to get the items list in each page
      }
      if (this.props.wo !== true && currentOrder) {
        this.props.getFrequentSalesOrderItems({ clientId: currentOrder.wholesaleClient.clientId, search: '' })
      }
      setTimeout(() => {
        this.setTab(true)
      }, 50)
    }
    if (
      (!this.props.saleItems.length && nextProps.saleItems.length) ||
      (this.props.saleItems !== nextProps.saleItems) ||
      (!this.props.availableItemLots.length && nextProps.availableItemLots.length) ||
      (!this.props.frequentItems.length && nextProps.frequentItems.length)
    ) {
      setTimeout(() => {
        this.setState({
          saleItems: cloneDeep(nextProps.saleItems)
        })
        this.setTab(true)
      }, 50)
    }
    if (nextProps.visible && this.props.loading === true && nextProps.loading === false) {
      setTimeout(() => {
        this.setTab(true)
      }, 50)
    }

    if (JSON.stringify(this.props.itemAvailableItemLots) !== JSON.stringify(nextProps.itemAvailableItemLots)) {
      this.setChildren(nextProps.itemAvailableItemLots)
    }
  }

  initFocusEvent = () => {
    jQuery('.add-cart-item-modal .ant-btn.add-btn')
      .unbind()
      .bind('focus', (e: any) => {
        jQuery('.add-cart-item-modal tr.highlighted').removeClass('highlighted')
        jQuery(e.target)
          .parents('tr')
          .addClass('highlighted')
      })
  }

  onChangePage = (page: number, pageSize: number) => {
    this.setState({
      searchWithIn: '',
    }, () => {
      setTimeout(() => {
        this.initFocusEvent()
      }, 100)
    })
  }

  setTab = (initable) => {
    this.initFocusEvent()
    const actionFieldsLength = jQuery('.add-item-modal').find('.ant-input, .ant-tabs-tab').length
    // this.tabEls = jQuery('.add-item-modal').find('.ant-input, .ant-tabs-tab, ul.ant-table-pagination li')
    if (this.state.isExpanded) {
      this.tabEls = jQuery('.add-item-modal').find(
        '.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-tbody tr, .ant-tabs-tabpane-active ul.ant-table-pagination li',
      )
      this.tableRows = jQuery('.add-item-modal').find('.ant-tabs-tabpane-active .ant-table-row')
    } else {
      this.tabEls = jQuery('.add-item-modal').find(
        '.ant-input, .ant-tabs-tab, .ant-tabs-tabpane-active .ant-table-tbody tr.ant-table-row-level-0, .ant-tabs-tabpane-active ul.ant-table-pagination li',
      )
      this.tableRows = jQuery('.add-item-modal').find('.ant-tabs-tabpane-active tr.ant-table-row-level-0')
    }
    if (initable && this.tabEls.length) {
      this.setFocus(this.tabEls[0])
      this.focusedIndex = 0
    }

    const _this = this
    jQuery('.add-item-modal-input input')
      .unbind()
      .bind('focus', () => {
        _this.focusedIndex = 0
        _this.focusedRowIndex = 0
      })

    jQuery('.add-item-modal .anticon-plus, .add-item-modal .anticon-minus')
      .unbind()
      .bind('click', (e: any) => {
        jQuery(e.target)
          .parents('tr')
          .find('.add-btn')
          .focus()
      })

    jQuery('.add-item-modal')
      .unbind()
      .bind('keydown', (e: any) => {
        if (e.keyCode == 9) {
          if (e.shiftKey) {
            _this.focusedIndex--
            if (_this.focusedIndex < 0) {
              _this.focusedIndex = _this.tabEls.length - 1
            }
          } else {
            if (jQuery(e.target).hasClass('ant-pagination-next')) {
              _this.focusedIndex = 0
            } else {
              _this.focusedIndex++
              if (_this.focusedIndex > _this.tabEls.length - 1) {
                _this.focusedIndex = 0
              }
            }
          }
          _this.setFocus(_this.tabEls[_this.focusedIndex])
          _this.moveDirection = 'tab'
          _this.isNavigation = true
        } else if (e.keyCode >= 37 && e.keyCode <= 40) {
          if (_this.tableRows.length) {
            if (e.keyCode == 38) {
              if (jQuery(e.target).hasClass('add-btn')) {
                _this.focusedIndex--
                if (_this.focusedIndex < actionFieldsLength) {
                  // _this.focusedIndex = _this.tabEls.length - 1
                  _this.focusedIndex = actionFieldsLength
                  jQuery(_this.tableRows[0]).removeClass('highlighted')
                  setTimeout(() => {
                    jQuery(_this.tableRows[0]).addClass('highlighted')
                  }, 100)
                  return
                }
                _this.setFocus(_this.tabEls[_this.focusedIndex])
              }
            } else if (e.keyCode == 40) {
              if (jQuery(e.target).hasClass('add-btn')) {
                _this.focusedIndex++
                if (_this.focusedIndex > actionFieldsLength + this.tableRows.length - 1) {
                  // _this.focusedIndex = 0
                  _this.focusedIndex = actionFieldsLength + this.tableRows.length - 1
                  jQuery(_this.tableRows[this.tableRows.length - 1]).removeClass('highlighted')
                  setTimeout(() => {
                    jQuery(_this.tableRows[this.tableRows.length - 1]).addClass('highlighted')
                  }, 100)
                  return
                }
                _this.setFocus(_this.tabEls[_this.focusedIndex])
              }
            } else if (e.keyCode == 37) {
              // left
              e.preventDefault()
              const minsBtn = jQuery('tr.highlighted').find('.anticon-minus')
              if (minsBtn.length > 0) {
                minsBtn.trigger('click')
              } else {
                if (jQuery('tr.highlighted').hasClass('ant-table-row-level-1')) {
                  let prevItem = jQuery('tr.highlighted').prev()
                  while (prevItem.hasClass('ant-table-row-level-0') === false) {
                    prevItem = prevItem.prev()
                  }
                  prevItem.find('.anticon-minus').trigger('click')
                }
              }
              _this.setTabElsWithExpandedRows(false)
            } else if (e.keyCode == 39) {
              // right
              e.preventDefault()
              jQuery('tr.highlighted')
                .find('.anticon-plus')
                .trigger('click')
              _this.setTabElsWithExpandedRows(true)
            } else {
              _this.moveDirection = ''
            }
          }

          if (jQuery(_this.tabEls[_this.focusedIndex]).hasClass('ant-tabs-tab')) {
            return false
          }
          _this.isNavigation = true
        } else if (e.keyCode == 13) {
          if (_this.moveDirection == 'down' || _this.moveDirection == 'up') {
            jQuery.each(_this.tableRows, function (index: number, val: any) {
              if (jQuery(val).hasClass('highlighted')) {
                _this.handleSelectEl(val)
                return false
              }
            })
          } else {
            _this.handleSelectEl(_this.tabEls[_this.focusedIndex])
          }
        }
      })
  }

  setTabElsWithExpandedRows = (isExpanded: boolean) => {
    this.setState({ isExpanded })
  }

  setFocus = (el: any) => {
    if (!jQuery(el).hasClass('ant-table-row')) {
      jQuery.each(this.tabEls, function (index: number, val: any) {
        if (jQuery(val).hasClass('tab-highlighted')) {
          jQuery(val).removeClass('tab-highlighted')
        }
      })
    }

    jQuery.each(this.tableRows, function (index: number, val: any) {
      if (jQuery(val).hasClass('highlighted')) {
        jQuery(val).removeClass('highlighted')
      }
    })
    setTimeout(() => {
      if (jQuery(el).hasClass('ant-input')) {
        jQuery(el).trigger('focus')
        if (this.isNavigation) {
          jQuery(el).trigger('select')
        }
      } else if (jQuery(el).hasClass('ant-tabs-tab')) {
        if (jQuery(el).hasClass('ant-tabs-tab')) {
          jQuery(el).addClass('tab-highlighted')
        }
      } else if (jQuery(el).hasClass('ant-table-row')) {
        jQuery('.ant-tabs-tab.tab-highlighted').removeClass('tab-highlighted')
        // jQuery(el).addClass('highlighted')
        if (jQuery(el).find('.ant-btn.add-btn').length) {
          jQuery(el)
            .find('.ant-btn.add-btn')
            .focus()
        } else {
          jQuery(el).addClass('highlighted')
        }
      }
    }, 50)
  }

  handleSelectEl = (el: any) => {
    if (jQuery(el).prop('tagName') == 'LI') {
      setTimeout(() => {
        jQuery('.add-item-table-wrapper').animate({ scrollLeft: 0 }, 500, 'swing', function () { })
      }, 100)
    } else if (jQuery(el).hasClass('ant-table-row')) {
      this.clickRow(el)
    } else if (jQuery(el).hasClass('ant-tabs-tab')) {
      jQuery(el).trigger('click')
      setTimeout(() => {
        this.focusedRowIndex = 0
        this.setTab(false)
      }, 50)
    }
  }

  clickRow = (el: any) => {
    let { availableItemLots, frequentItems } = this.props
    let { saleItems } = this.state

    let orderItemId = jQuery(el).data('row-key')
    let selectedTab = ''
    let item: any = null
    jQuery.each(this.tabEls, function (index: number, el: any) {
      if (jQuery(el).hasClass('ant-tabs-tab-active')) {
        selectedTab = jQuery(el).text()
      }
    })
    if (selectedTab.toLowerCase() == 'all products') {
      if (orderItemId.toString().indexOf('lotItemId') > -1) {
        item = availableItemLots.find((obj) => {
          return `lotItemId-${obj.wholesaleOrderItemId}` == orderItemId
        })
      } else {
        item = saleItems.find((obj) => {
          return obj.itemId == orderItemId
        })
      }
    } else if (selectedTab.toLowerCase() == 'frequently sold') {
      item = frequentItems.find((obj) => {
        return obj.wholesaleOrderItemId == orderItemId
      })
      delete item.wholesaleOrderItemId
    }

    if (item) {
      this.handleAddItem(item)
    }
  }

  frequentColumns = [
    {
      title: '',
      dataIndex: 'cover',
      align: 'center',
      width: 50,
      render: (src: string) => {
        return (
          <Flex className="v-center">
            {src !== null ? (
              <ThumbnailImage className="gray-circle" src={getFileUrl(src, false)} />
            ) : (
              <div className="gray-circle" />
            )}
            <Icon className="plus-circle" type="plus-circle" style={ItemHMargin4} />
            <Button className="add-btn transparent-button" style={{ width: 0, padding: 0 }} />
            {name}
          </Flex>
        )
      },
    },
    {
      title: 'UOM',
      dataIndex: 'inventoryUOM',
      width: 120,
    },

    {
      title: 'SKU',
      dataIndex: 'SKU',
      width: 150,
    },
    {
      title: 'PRODUCT NAME',
      dataIndex: 'variety',
      width: 200,
    },
    {
      title: 'AVAILABLE TO SELL',
      dataIndex: 'availableQty',
      key: 'available_to_sell',
      width: 250,
      render: (_text: any, item: any, _index: any) => {
        let quantity = item.availableQty === null ? 0 : item.availableQty
        // if (quantity <= 0) {
        //   return ''
        // }
        // if (item.inventoryUOM == item.baseUOM && item.constantRatio == true && item.ratioUOM == 1) {
        //   return `(${quantity} ${item.baseUOM ? ' ' + item.baseUOM : ''})`
        // } else if (item.inventoryUOM != item.baseUOM && item.constantRatio != true) {
        //   return quantity + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
        // } else {
        //   let ratioQty = formatNumber(quantity * item.ratioUOM, 2)
        //   return (
        //     <div>
        //       {quantity} {item.inventoryUOM ? ' ' + item.inventoryUOM : ''}
        //       <br />
        //       {ratioQty} {item.baseUOM ? ' ' + item.baseUOM : ''}
        //     </div>
        //   )
        // }
        if (item.subUom) {
          if (quantity > 0) {
            return isArray(item.wholesaleProductUomList) &&
              item.wholesaleProductUomList.length > 0 &&
              item.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
              <Tooltip
                title={
                  <>
                    {item.wholesaleProductUomList.map((uom: any) => {
                      if (uom.useForInventory) {
                        return (
                          <div>
                            {inventoryQtyToRatioQty(uom.name, quantity, item, 2)} {uom.name}
                          </div>
                        )
                      }
                    })}
                  </>
                }
              >
                <span style={{ marginRight: '20px' }}>
                  {mathRoundFun(quantity, 2)} {item.inventoryUOM}
                </span>
              </Tooltip>
            ) : (
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(quantity, 2)} {item.inventoryUOM}
              </span>
            )
          } else {
            return ''
          }
        } else {
          return mathRoundFun(quantity, 2) + ' ' + item.inventoryUOM
        }
      },
    },
    {
      title: 'UNITS ON HAND',
      dataIndex: 'onHandQty',
      key: 'inventory_on_hand',
      width: 250,
      render(_text: any, item: any, _index: any) {
        let onHandQty = item.onHandQty === null ? 0 : item.onHandQty
        // if (onHandQty <= 0) {
        //   return ''
        // }
        // if (item.inventoryUOM == item.baseUOM && item.constantRatio == true && item.ratioUOM == 1) {
        //   return `(${onHandQty} ${item.baseUOM ? ' ' + item.baseUOM : ''})`
        // } else if (item.inventoryUOM != item.baseUOM && item.constantRatio != true) {
        //   return onHandQty + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
        // } else {
        //   let ratioQty = formatNumber(onHandQty * item.ratioUOM, 2)
        //   return (
        //     <div>
        //       {onHandQty} {item.inventoryUOM ? ' ' + item.inventoryUOM : ''}
        //       <br />
        //       {ratioQty} {item.baseUOM ? ' ' + item.baseUOM : ''}
        //     </div>
        //   )
        // }
        if (item.subUom) {
          if (onHandQty > 0) {
            return isArray(item.wholesaleProductUomList) &&
              item.wholesaleProductUomList.length > 0 &&
              item.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
              <Tooltip
                title={
                  <>
                    {item.wholesaleProductUomList.map((uom: any) => {
                      if (uom.useForInventory) {
                        return (
                          <div>
                            {inventoryQtyToRatioQty(uom.name, onHandQty, item, 2)} {uom.name}
                          </div>
                        )
                      }
                    })}
                  </>
                }
              >
                <span style={{ marginRight: '20px' }}>
                  {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
                </span>
              </Tooltip>
            ) : (
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
              </span>
            )
          } else {
            return ''
          }
        } else {
          return mathRoundFun(onHandQty, 2) + ' ' + item.inventoryUOM
        }
      },
    },
    // {
    //   title: 'Delivery Date',
    //   dataIndex: 'deliveryDate',
    //   key: 'deliveryDate',
    //   render: (value: number) => {
    //     return value ? moment(value).format('MM/DD/YYYY') : ''
    //   },
    // },
    // {
    //   title: 'Price',
    //   dataIndex: 'orderPrice',
    //   key: 'orderPrice',
    //   render: (value: number) => {
    //     return value ? `$${value.toFixed(2)}` : ''
    //   },
    // },
  ]

  allProductsColumns: any[] = [
    {
      title: 'NAME',
      dataIndex: 'x',
    },
    {
      title: (
        <Tooltip title="Indicates whether the item is on the current customer’s product list.">
          ON
          <br />
          CPL
          <Icon style={{ marginLeft: '3px' }} type="info-circle" theme="filled" />
        </Tooltip>
      ),
      dataIndex: 'checkbox',
      align: 'center',
      width: 80,
      render: (value: any, record: any) => {
        return record.isChild ? null : (
          <Checkbox
            checked={record.isCpl}
            disabled={record.isCpl}
            onChange={() => {
              this.onChangeChecked(
                record.isCpl,
                record.defaultPriceSheetId!,
                record.itemId!
              )
            }}
          />
        )
      }
    },
    {
      title: 'SKU',
      dataIndex: 'SKU',
      align: 'center',
      width: 150,
    },
    {
      title: 'INVENTORY UOM',
      dataIndex: 'inventoryUOM',
      align: 'center',
      width: 150,
    },
    {
      title: 'BRAND',
      dataIndex: 'suppliers',
      align: 'center',
      width: 150,
    },
    {
      title: 'ORIGIN',
      dataIndex: 'extraOrigin',
      align: 'center',
      width: 150,
      render: (value: number, record: any) => {
        return (typeof record.origin !== 'undefined') ? record.origin : value
      }
    },
    {
      title: 'ARRIVAL DATE',
      dataIndex: 'deliveryDate',
      align: 'right',
      width: 150,
      render: (value: number) => {
        if (value != null) {
          const year = moment.utc(value).get('year')
          // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
          if (year != 1990) {
            return moment.utc(value).format('MM/DD/YYYY')
          }
        } else {
          return ''
        }
      },
    },
    {
      title: 'COST',
      dataIndex: 'cost',
      key: 'total_cost',
      align: 'right',
      width: 100,
      render: (value: number, record: any) => {
        let cost = value ? value : 0
        cost += (record.perAllocateCost ?? 0)
        return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(cost ? cost : 0, 2)}</div>
      },
    },
    {
      title: 'UNITS ON HAND',
      dataIndex: 'onHandQty',
      align: 'right',
      width: 150,
      render(_text: any, record: any, _index: any) {
        let wholesaleProductUomList = null
        if (record.wholesaleItem) {
          wholesaleProductUomList = record.wholesaleItem.wholesaleProductUomList
        } else if(typeof record.wholesaleProductUomList !== 'undefined') {
          wholesaleProductUomList = record.wholesaleProductUomList
        }

        let item = record
        let onHandQty = 0,
          subUom = null,
          subRatio = 0
        if (typeof item.onHandQty !== 'undefined') {
          onHandQty = item.onHandQty === null ? 0 : item.onHandQty
          item = record.wholesaleItem
          if (item) {
            if (lodash.isArray(wholesaleProductUomList) && wholesaleProductUomList.length > 0) {
              subUom = wholesaleProductUomList[0].name
              subRatio = wholesaleProductUomList[0].ratio
            }
          }
        } else {
          onHandQty = item.onHandQty === null ? 0 : item.onHandQty
          subUom = item.subUom
          subRatio = item.subRatio
        }
        if (onHandQty <= 0) {
          return ''
        }
        if (item && onHandQty > 0) {
          return isArray(wholesaleProductUomList) &&
            wholesaleProductUomList.length > 0 &&
            wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
            <Tooltip
              title={
                <>
                  {wholesaleProductUomList.map((uom: any) => {
                    if (uom.useForInventory) {
                      return (
                        <div>
                          {inventoryQtyToRatioQty(uom.name, onHandQty, item, 2)} {uom.name}
                        </div>
                      )
                    }
                  })}
                </>
              }
            >
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
              </span>
            </Tooltip>
          ) : (
            <span style={{ marginRight: '20px' }}>
              {mathRoundFun(onHandQty, 2)} {item.inventoryUOM}
            </span>
          )
        } else {
          return ''
        }
      },
    },
    {
      title: 'AVAILABLE TO SELL',
      dataIndex: 'availableQty',
      align: 'right',
      key: 'available_to_sell',
      width: 250,
      render: (_text: any, record: any, _index: any) => {
        let item = record
        let quantity = 0,
          subUom = null,
          subRatio = 0
        if (typeof item.lotAvailableQty !== 'undefined') {
          quantity = item.lotAvailableQty === null ? 0 : item.lotAvailableQty
          let wholesaleProductUomList = null
          if (record.wholesaleItem) {
            wholesaleProductUomList = record.wholesaleItem.wholesaleProductUomList
          } else if(typeof record.wholesaleProductUomList !== 'undefined') {
            wholesaleProductUomList = record.wholesaleProductUomList
          }
          if (wholesaleProductUomList) {
            if (lodash.isArray(wholesaleProductUomList) && wholesaleProductUomList.length > 0) {
              subUom = wholesaleProductUomList[0].name
              subRatio = wholesaleProductUomList[0].ratio
            }
          }
        } else {
          quantity = item.availableQty === null ? 0 : item.availableQty
          subUom = item.subUom
          subRatio = item.subRatio
        }
        // if (quantity <= 0) {
        //   return ''
        // }
        // if (item.inventoryUOM == item.baseUOM && item.constantRatio == true && item.ratioUOM == 1) {
        //   return `(${quantity} ${item.baseUOM ? ' ' + item.baseUOM : ''})`
        // } else if (item.inventoryUOM != item.baseUOM && item.constantRatio != true) {
        //   return quantity + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
        // } else if (item.inventoryUOM == item.baseUOM && item.constantRatio != true) {
        //   return quantity + (item.inventoryUOM ? ' ' + item.inventoryUOM : '')
        // } else {
        //   let ratioQty = formatNumber(quantity * item.ratioUOM, 2)
        //   return (
        //     <div>
        //       {quantity} {item.inventoryUOM ? ' ' + item.inventoryUOM : ''}
        //       <br />
        //       {ratioQty} {item.baseUOM ? ' ' + item.baseUOM : ''}
        //     </div>
        //   )
        // }
        if (item && quantity > 0) {
          return isArray(item.wholesaleProductUomList) &&
            item.wholesaleProductUomList.length > 0 &&
            item.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
            <Tooltip
              title={
                <>
                  {item.wholesaleProductUomList.map((uom: any) => {
                    if (uom.useForInventory) {
                      return (
                        <div>
                          {inventoryQtyToRatioQty(uom.name, quantity, item, 2)} {uom.name}
                        </div>
                      )
                    }
                  })}
                </>
              }
            >
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(quantity, 2)} {item.inventoryUOM}
              </span>
            </Tooltip>
          ) : (
            <span style={{ marginRight: '20px' }}>
              {mathRoundFun(quantity, 2)} {item.inventoryUOM}
            </span>
          )
        } else {
          return ''
        }
      },
    },
    // {
    //   title: 'LAST RECEIVED',
    //   dataIndex: 'lastReceived',
    //   align: 'right',
    //   width: 150,
    //   render: (timestamp: number, record: any) => {
    //     let value = timestamp
    //     if (record.deliveryDate && typeof record.deliveryDate !== 'undefined') {
    //       value = record.deliveryDate
    //     }
    //     if (value == undefined) {
    //       return ''
    //     }
    //     const year = moment.utc(value).get('year')
    //     // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
    //     if (year != 1990) {
    //       return moment.utc(value).format('MM/DD/YYYY')
    //     }
    //   },
    // },
    // {
    //   title: 'NEXT ARRIVAL',
    //   dataIndex: 'nextArrival',
    //   align: 'center',
    //   width: 120,
    //   render: (value: number) => {
    //     if (value == undefined) {
    //       return ''
    //     }
    //     const year = moment.utc(value).get('year')
    //     // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
    //     if (year != 1990) {
    //       return moment.utc(value).format('MM/DD/YYYY')
    //     }
    //   },
    // },
  ]

  onChangeChecked = (checked: boolean, priceSheetId: number, itemId: number) => {
    if (checked) {
      this.props.removePriceSheetItems({
        priceSheetId,
        deleteItemListId: [itemId]
      });
    } else {
      this.props.addPriceSheetItems({
        priceSheetId,
        assignItemListId: [itemId],
        clientId: this.props.currentOrder?.wholesaleClient?.clientId,
      });
    }
  }

  onSuggest = (key: string) => {
    return []
  }

  onSearch = (search: string, pureSearch: string) => {
    this.setState({ searchText: search, pureSearchText: pureSearch })
    this.isNavigation = false
    setTimeout(() => {
      this.setTab(true)
    }, 50)
  }

  onSearchWithIn = (search: string) => {
    this.setState({ searchWithIn: search })
  }

  handleAddItem = lodash.debounce((newItem: any) => {
    this.props.resetLoading()
    if (this.props.wo === true && this.props.onSelect) {
      this.props.onSelect(newItem)
      // this.props.onClose()
      return
    }
    // if (newItem.wholesaleOrderItemId) {
    //   delete newItem.variety
    //   delete newItem.SKU
    //   delete newItem.provider
    //   delete newItem.itemId
    // } else {
    //   delete newItem.children
    // }
    // // const { orderItems } = this.props
    // // tslint:disable-next-line:prefer-const
    // let itemsList: any[] = []
    // // orderItems.forEach((item) => {
    // //   itemsList.push(this._formatItem(item))
    // // })onAddItem
    // itemsList.push(this._formatNewItem(newItem))
    // this._updateOrder(itemsList)

    const { currentOrder, insertItemPosition } = this.props
    let displayOrder = undefined
    if (typeof insertItemPosition === 'number' && insertItemPosition > -1) {
      displayOrder = insertItemPosition + 1
    }
    if (currentOrder) {
      // this.props.onClose()
      this.props.addOrderItemForOrder({
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        wholesaleItemId: newItem.wholesaleItem ? newItem.wholesaleItem.wholesaleItemId : newItem.wholesaleItemId,
        lotId: newItem.lotId,
        displayOrder,
        quantity: 0,
      })
    }
    if (~~(this.props.total / this.props.pageSize) !== this.props.page) {
      setTimeout(() => {
        this.props.setSalesOrderPage(~~(this.props.total / this.props.pageSize))
      }, 500)
    }
  }, 200)

  /*
  _updateOrder = (itemsList: any[]) => {
    const { currentOrder, insertItemPosition } = this.props
    console.log(insertItemPosition)
    if (currentOrder) {
      if (itemsList.length > 0) {
        itemsList.map((item: any) => {
          item.wholesaleOrderId = currentOrder.wholesaleOrderId
        })
      }
      if (itemsList.length == 1 && typeof insertItemPosition != 'undefined' && insertItemPosition > -1) {
        itemsList[0]['displayOrder'] = insertItemPosition
      }
      const formData = {
        deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        userId: currentOrder.user.userId,
        totalPrice: currentOrder.totalPrice,
        wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
        itemList: itemsList,
        status: currentOrder.wholesaleOrderStatus,
      }

      this.props.onClose()
      this.props.resetLoading()
      this.props.updateOrder(formData)
    }
  }
*/
  _formatNewItem = (newItem: any) => {
    let quotedPrice = ''
    let UOM = ''
    let lotId = ''
    let margin = 0
    if (typeof newItem.wholesaleOrderItemId === 'undefined') {
      // if coming from product catalogy
      // The quoted price will be cost*default_margin
      // Newitem is wholesaleItem is different with wholesaleOrderItem
      if (typeof newItem.wholesaleItem === 'undefined') {
        const markup = (newItem.defaultMargin ? newItem.defaultMargin : 0) / 100
        // if (newItem.constantRatio == true) {
        //   quotedPrice = formatNumber(newItem.cost ? ((1 + markup) * newItem.cost) / newItem.ratioUOM : 0, 2)
        // } else {
        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost : 0, 2)
        // }
        UOM = newItem.inventoryUOM
      } else {
        const markup = (newItem.margin ? newItem.margin : 0) / 100
        const freight = newItem.freight ? newItem.freight : 0
        if (newItem.uom == newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio == true) {
          quotedPrice = formatNumber(
            newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
            2,
          )
        } else {
          quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
        }
        UOM = newItem.UOM
      }
    } else {
      // if coming for lot
      // The quoted price will be cost of lot*lot_margin if it is greater than 0, if not lot*default_margin
      // quotedPrice = newItem.cost * newItem.lotMargin
      // if(quotedPrice <= 0) {
      //   quotedPrice = newItem.cost * newItem.wholesaleItem.defaultMargin
      // }
      const markup = (newItem.margin ? newItem.margin : 0) / 100
      const freight = newItem.freight ? newItem.freight : 0
      if (newItem.uom == newItem.wholesaleItem.inventoryUOM && newItem.wholesaleItem.constantRatio == true) {
        quotedPrice = formatNumber(
          newItem.cost ? (1 + markup) * (newItem.cost / newItem.wholesaleItem.ratioUOM) + freight : 0,
          2,
        )
      } else {
        quotedPrice = formatNumber(newItem.cost ? (1 + markup) * newItem.cost + freight : 0, 2)
      }
      if (newItem.uom) {
        UOM = newItem.uom
      }
      lotId = newItem.lotId
    }

    return {
      wholesaleItemId: newItem.wholesaleItem ? newItem.wholesaleItem.wholesaleItemId : newItem.itemId,
      wholesaleOrderItemId: null,
      quantity: 0,
      cost: newItem.cost,
      price: quotedPrice,
      margin: margin,
      freight: 0,
      // status: newItem.wholesaleItem ? 'PLACED' : newItem.status,
      status: 'PLACED',
      UOM: UOM,
      lotId: lotId,
    }
  }

  formatAllProductsList = () => {
    const { saleItems, searchText } = this.state
    const lowerSearchText = searchText.length >= 2 ? searchText.toLowerCase() : ''
    let filteredProductItems: any[] = []

    if (saleItems) {
      let matches1: any[] = []
      let matches2: any[] = []
      let matches3: any[] = []
      cloneDeep(saleItems).forEach((row: any, index: number) => {
        if (typeof row.children === 'undefined') {
          row.children = []
        }
        if (typeof row.SKU === 'undefined' && row.sku) {
          row.SKU = row.sku
        }
        if (row.SKU && row.SKU.toLowerCase().indexOf(lowerSearchText) === 0) {
          // full match on 2nd filed
          matches2.push(row)
        } else if (row.variety && row.variety.toLowerCase().indexOf(lowerSearchText) === 0) {
          // full match on variety
          matches1.push(row)
        } else if (
          (row.category && row.category.toLowerCase().indexOf(lowerSearchText) > 0) ||
          (row.variety && row.variety.toLowerCase().indexOf(lowerSearchText) > 0) ||
          (row.SKU && row.SKU.toLowerCase().indexOf(lowerSearchText) > 0)||
          (row.customerProductCode && row.customerProductCode.indexOf(searchText) >= 0)
        ) {
          // others
          matches3.push(row)
        }
      })

      if (matches1) {
        matches1 = matches1.sort((a, b) => a.variety.localeCompare(b.variety))
      }
      if (matches2) {
        matches2 = matches2.sort((a, b) => a.SKU.localeCompare(b.SKU))
      }
      filteredProductItems = [...matches2, ...matches1, ...matches3]
    }

    return filteredProductItems
  }

  expandColumn = (props: any) => {
    const {expanded, record} = props
    const label = typeof props.record.wholesaleItem !== 'undefined' ? props.record.variety : props.record.lotId
    return (
      <Flex className="space-between">
        <Flex className="v-center">
          {typeof record.isChild === 'undefined' ? (
            <Icon
              className="icon"
              type={expanded ? 'minus' : 'plus'}
              style={{ ...ItemHMargin4, padding: 2, cursor: 'pointer' }}
              onClick={() => {
                window.event.preventDefault()
                props.onExpand(props.record)
                setTimeout(() => {
                  this.setTab(false)
                }, 50)
              }}
            />
          ) : (
            <div style={{ width: 26.5 }} />
          )}
          {label.length > 27 ? (
            <Tooltip title={label}>
              <div style={AddItemNameStyle}>{label}</div>
            </Tooltip>
          ) : (
            <div style={AddItemNameStyle}>{label}</div>
          )}
        </Flex>
        <ThemeButton
          className="add-btn"
          style={{ color: 'white' }}
          onClick={this.handleAddItem.bind(this, props.record)}
        >
          Add
        </ThemeButton>
      </Flex>
    )
  }

  refreshData = () => {
    const { clientId } = this.props.currentOrder.wholesaleClient;
    this.props.getItemForAddItemModal({
      type: this.props.salesType,
      clientId
    })
    if (this.props.currentOrder) {
      this.props.getFrequentSalesOrderItems({ clientId, search: '' })
    }
  }

  setChildren = (itemAvailableItemLots: array[]) => {
    const { currentItemId } = this.state

    const children = itemAvailableItemLots.length > 0 ? itemAvailableItemLots.map((ro, i) => {
      return {
        ...ro,
        itemId: `lotItemId-${ro.wholesaleItemId}-${i}`,
        SKU: ro.sku,
        suppliers: ro.brand,
        isChild: true
      }
    }) : []


    const saleItems = cloneDeep(this.state.saleItems).map(item => {
      if (item.wholesaleItemId === currentItemId) {
        return {
          ...item,
          children
        }
      }
      return item
    })
    this.setState({
      saleItems
    })
  }

  onExpand = (expanded: boolean, record: any) => {
    const currentItemId = record.wholesaleItemId
    if (expanded) {
      this.setState({
        currentItemId
      }, () => {
        const { saleItems } = this.state
        const found = saleItems.find((item: any) => item.wholesaleItemId === currentItemId)
        if(found) {
          console.log(found)
          if (typeof found.children === 'undefined' || !found.children.length) {
            this.props.getItemAvailableItemLotById(record.wholesaleItemId)
          }
        }
      })
    }
  }

  render() {
    const {
      loading,
      loadingAvailableItemLots,
      loadingSaleItems,
      frequentItems,
      addItemDataRefreshedAt,
    } = this.props

    const { saleItems, searchText, searchWithIn, pureSearchText, placeholder, currentItemId } = this.state
    const lowerSearchText = searchText.length >= 2 ? searchText.toLowerCase() : ''

    const allProducts = this.formatAllProductsList()

    const filteredFrequentItems = frequentItems
      ? frequentItems.filter((row) => {
        return (
          (row.category && row.category.toLowerCase().indexOf(lowerSearchText) >= 0) ||
          (row.baseUOM && row.baseUOM.toLowerCase().indexOf(lowerSearchText) >= 0) ||
          (row.variety && row.variety.toLowerCase().indexOf(lowerSearchText) >= 0) ||
          (row.SKU && row.SKU.toLowerCase().indexOf(lowerSearchText) >= 0)
        )
      })
      : []

    if (filteredFrequentItems.length > 0) {
      filteredFrequentItems.map((item, index) => {
        let wholesaleItem = saleItems.filter((data) => item.itemId == data.itemId)
        if (wholesaleItem.length > 0) {
          item.wholesaleProductUomList = wholesaleItem[0].wholesaleProductUomList
        }
        item.id = index + 1
        return item
      })
    }

    const allProductsComponent = (
      <GridTableWrapper className="all-products">
        <AddItemTableWrapper className="add-item-table-wrapper">
          <Table
            columns={this.allProductsColumns}
            dataSource={allProducts}
            pagination={{ pageSize: 10, onChange: this.onChangePage }}
            rowKey="itemId"
            expandIcon={this.expandColumn}
            onExpand={this.onExpand}
            indentSize={0}
            scroll={{ x: true }}
            rowClassName={(row, index) => {
              if (!searchWithIn) {
                return '';
              }
              const lowerSearchWithIn = searchWithIn.toLowerCase()
              if ((row.category && row.category.toLowerCase().indexOf(lowerSearchWithIn) >= 0) ||
                (row.variety && row.variety.toLowerCase().indexOf(lowerSearchWithIn) >= 0) ||
                (row.SKU && row.SKU.toLowerCase().indexOf(lowerSearchWithIn) >= 0)||
                (row.customerProductCode && row.customerProductCode.indexOf(searchWithIn) >= 0)) {
                return ''
              }
              return 'hide-row'
            }}
            onRow={(record, _rowIndex) => {
              return {
                onClick: (_event) => {
                  const clicked = _event.target
                  const tagName = jQuery(clicked).prop('tagName').toLowerCase()
                  if (
                    tagName != 'svg' &&
                    tagName != 'path' &&
                    tagName != 'input' &&
                    !jQuery(clicked).hasClass('anticon')
                  ) {
                    this.handleAddItem(record)
                  }
                },
              }
            }}
            style={{ minWidth: '100%' }}
          />
        </AddItemTableWrapper>
      </GridTableWrapper>
    )

    return (
      <ThemeSpin spinning={loadingAvailableItemLots || loadingSaleItems || loading}>
        <div style={{ minHeight: 400 }} className="add-item-modal">
          <Flex className="space-between">
            <Flex className='v-center' style={{height: 32}}>
              <div style={{ width: 350 }}>
                <SearchInput
                  className="add-item-modal-input"
                  handleSearch={this.onSearch}
                  placeholder="Search by product name, SKU, or Lot #..."
                  defaultValue={searchText}
                  withApiCall={false}
                />
              </div>
              <span style={{fontWeight: 'normal', marginLeft: 50, marginRight: 10}}>Search within results:</span>
              <div style={{ width: 200 }}>
                <SearchInput
                  className="add-item-modal-input"
                  handleSearch={this.onSearchWithIn}
                  placeholder="Search text"
                  defaultValue={searchWithIn}
                  withApiCall={false}
                />
              </div>
            </Flex>
            <div style={{ textAlign: 'right' }}>
              <Button onClick={this.refreshData}>
                <Icon type="sync" />
                Refresh
              </Button>
              <br />
              <span style={{ fontWeight: '100' }}>
                {addItemDataRefreshedAt ? `Refreshed ${formatTimeDifference(addItemDataRefreshedAt)} ago` : ''}
              </span>
            </div>
          </Flex>
          {this.props.wo === true ? (
            <div style={{ marginTop: 15 }}>{allProductsComponent}</div>
          ) : (
            <>
              {(allProducts.length > 0 || filteredFrequentItems.length > 0) && (
                <TabsWrapper className="modal-tab">
                  <Tabs defaultActiveKey="1" onTabClick={this.onTabClick}>
                    <TabPane tab="All Products" key="1">
                      {allProductsComponent}
                    </TabPane>
                    {/* <TabPane tab="Lots" key="2">
                      <GridTableWrapper>
                        <AddItemTableWrapper className='add-item-table-wrapper'>
                          <Table
                            columns={this.lotColumns}
                            dataSource={filteredLotItems}
                            pagination={{ pageSize: 10 }}
                            rowKey='wholesaleOrderItemId'
                            onRow={(record, _rowIndex) => {
                              return {
                                onClick: (_event) => {
                                  this.handleAddItem(record)
                                },
                              }
                            }}
                            style={{ minWidth: '100%' }}
                          />
                        </AddItemTableWrapper>
                      </GridTableWrapper>
                    </TabPane> */}
                    <TabPane tab="Frequently Sold" key="2">
                      <GridTableWrapper>
                        <AddItemTableWrapper className="add-item-table-wrapper">
                          <Table
                            columns={this.frequentColumns}
                            rowKey="wholesaleOrderItemId"
                            dataSource={filteredFrequentItems}
                            pagination={{ pageSize: 10 }}
                            onRow={(record, _rowIndex) => {
                              return {
                                onClick: (_event) => {
                                  delete record.wholesaleOrderItemId
                                  this.handleAddItem({
                                    ...record,
                                    wholesaleItemId: record.itemId
                                  })
                                },
                              }
                            }}
                            style={{ minWidth: '100%' }}
                          />
                        </AddItemTableWrapper>
                      </GridTableWrapper>
                    </TabPane>
                  </Tabs>
                </TabsWrapper>
              )}
            </>
          )}
          {this.props.wo !== true && allProducts.length == 0 && filteredFrequentItems.length == 0 && searchText && (
            <EmptyResult>
              <div className="icon">
                <IconSVG type="no-search-result" viewBox="0 0 45 45" width="45" height="45" />
              </div>
              <div style={{ color: '#4A5355' }}>No results for &quot;{pureSearchText}&quot; found</div>
              <div className="button">
                {/*<ThemeButton onClick={() => this.props.showProductModal(pureSearchText)}>Create a Product</ThemeButton>*/}
              </div>
              <div className="extra-charge">
                <ThemeOutlineButton onClick={this.props.addOffItem} style={{ border: 'none' }}>
                  Add an Extra Charge to the Order
                </ThemeOutlineButton>
              </div>
              <div style={{ color: '#82898A', fontSize: 14 }}>
                Extra charges can be used for any non-inventory items like service fees, shipping materials, or
                untracked products.
              </div>
            </EmptyResult>
          )}
        </div>
      </ThemeSpin>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(AddOrderItemModal))
