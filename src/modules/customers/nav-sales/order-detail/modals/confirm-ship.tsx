import * as React from 'react'
import { InputLabel, ThemeCheckbox, ThemeTextArea } from '~/modules/customers/customers.style'
import { OrderItem } from '~/schema'
import {
  Description,
  DialogBodyDiv,
  DialogSubContainer,
  FlexDiv,
  Item,
  ValueLabel,
  VerticalPadding8,
} from '../../styles'
import { judgeConstantRatio } from '~/common/utils'

type ConfirmShippingModalProps = {
  nonPickedItems: OrderItem[]
  type: number
}

class ConfirmShippingModal extends React.PureComponent<ConfirmShippingModalProps> {
  state = {
    confirmedShipping: 0,
  }

  componentDidMount() {
    const confirmedMessageBox = localStorage.getItem('confirm-ship-message')
    console.log(confirmedMessageBox)
    this.setState({ confirmedShipping: confirmedMessageBox ? Number.parseInt(confirmedMessageBox) : 0 })
  }

  onCheckedChange = (e: any) => {
    console.log(e.target.checked)
    this.setState({ confirmedShipping: e.target.checked })
    let value = 0
    if (e.target.checked) {
      value = 1
    }
    localStorage.setItem('confirm-ship-message', `${value}`)
  }

  render() {
    const { nonPickedItems, type } = this.props
    const { confirmedShipping } = this.state

    return (
      <>
        <DialogBodyDiv>
          {type == 0 ? (
            <>
              <InputLabel style={VerticalPadding8}>
                The following items are in "NEW" status with no catchweight values entered.
              </InputLabel>
              {nonPickedItems.map((el: OrderItem) => {
                if (!judgeConstantRatio(el)) {
                  return <InputLabel style={{ padding: '4px 0' }}>- {el.variety}</InputLabel>
                }
              })}
            </>
          ) : (
            <>
              <Description>There are one ore more items in "NEW" status with no picked quantity.</Description>
              <InputLabel style={VerticalPadding8}>
                Would you like the system to automatically update the picked quantity to equal the order quantity for these items (not: catchweight items will not be updated)?
              </InputLabel>

              {/* <ThemeCheckbox onChange={this.onCheckedChange} style={{ ...VerticalPadding8 }} checked={confirmedShipping}>Do not display this message again</ThemeCheckbox> */}
            </>
          )}
        </DialogBodyDiv>
      </>
    )
  }
}

export default ConfirmShippingModal
