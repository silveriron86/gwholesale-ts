import * as React from 'react'
import { InputLabel, ThemeInput } from '~/modules/customers/customers.style'
import { OrderDetail, OrderItem } from '~/schema'
import { Description, DialogBodyDiv, Item } from '../../styles'
import jQuery from 'jquery'

type EditItemNameProps = {
  ref: any
  currentOrder: OrderDetail
  record: OrderItem
  open: boolean
  orderItemByProduct: any
  updateOrderItemByProductId: Function
}

class EditItemName extends React.PureComponent<EditItemNameProps> {
  ref = this.props.ref
  state = {
    editedItemName: ''
  }

  componentDidMount() {
    if (this.props.record) {
      this.setState({
        editedItemName: this.props.record.editedItemName ? this.props.record.editedItemName : this.props.record.variety
      })
    }

    setTimeout(() => {
      jQuery('.edit-item-name-input').trigger('focus')
    }, 50)

  }

  componentWillReceiveProps(nextProps: EditItemNameProps) {
    if (!this.props.record && nextProps.record || !this.props.open && nextProps.open) {
      this.setState({
        editedItemName: nextProps.record.editedItemName
      })
    }
  }

  onTextChange = (evt: any) => {
    this.setState({ editedItemName: evt.target.value })
  }

  handleSave = () =>{
    const { editedItemName } = this.state
    const { currentOrder, orderItemByProduct, record, updateOrderItemByProductId } = this.props
    orderItemByProduct[record.displayOrderProduct]['editedItemName'] = editedItemName

    updateOrderItemByProductId({
      orderId: currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod: record.lotAssignmentMethod,
        enableLotOverflow: record.enableLotOverflow,
        wholesaleItemId: record.itemId,
        picked: record.picked,
        quantity: record.quantity,
        isUpdateEditedItemName: true,
        editedItemName: editedItemName,
      }
    })
  }


  render() {
    const { editedItemName } = this.state
    return (
      <div 
        ref={this.ref}
        className={`item-note-modal-body ${this.props.open ? 'open' : 'hidden'}`}
      >
        <DialogBodyDiv>
          <Item style={{ margin: 0 }}>Edit item display name</Item>
          <Description style={{ color: 'black', fontWeight: 500 }}>Edit the item name that is displayed on the pick sheet, invoice and bill of lading (customer will see this.)</Description>
          <InputLabel style={{ fontWeight: 500 }}>Item display name</InputLabel>
          <ThemeInput
            defaultValue={editedItemName}
            className='edit-item-name-input'
            style={{ width: '100%' }}
            value={editedItemName ? editedItemName : ''}
            onChange={this.onTextChange}
          />
        </DialogBodyDiv>
      </div>
    )
  }
}

export default EditItemName