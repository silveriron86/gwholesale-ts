import { Col, Icon as AntIcon, Row } from 'antd'
import * as React from 'react'
import { Icon } from '~/components'
import { OrderItem } from '~/schema'
import { EPickSheetRowItem, EPickSheetRowItemWrapper, Flex, Item } from '~/modules/customers/nav-sales/styles'
import { formatNumber, judgeConstantRatio } from '~/common/utils'
import { NumericInput } from './picked-quanitty'
import { InputLabel, ThemeButton, ThemeInput, ThemeSpin } from '~/modules/customers/customers.style'
import jQuery from 'jquery'
import _ from 'lodash'
import styled from '@emotion/styled'

type EPickSheetRowProps = {
  index: number
  orderItem: OrderItem & { quantityDetailList: any[], displayOrderProduct: number }
  catchWeights: any[]
  loading: boolean
  resetLoading: Function
  getQuantityCatchWeight: Function
  setOrderItemId: Function
  catchWeight: any
  sellerSetting: any
  picked: number
  scanMode: boolean
  setParsingStatus: Function
  updateLastScanStatus: Function
  cwItemValues: any[]
  onDeleteCWItem: Function
  expandedOrderItemId: number
  updateManualInputCatchWeight: Function
  handleChangeUnitWeight: Function
  handleDeleteUnitWeight: Function
  handleAddUnitWeight: Function
}

type EPickSheetRowState = {
  clickedItemId: number
  ids: any[]
  loaded: boolean
  firstOpen: boolean
  isExpanded: boolean
  catchWeightItems: any[]
  totalBillableQty: number
  isLastScanFailed: boolean
  lastPickedQty: number
  isOverPicked: boolean
}

class EPickSheetRow extends React.PureComponent<EPickSheetRowProps, EPickSheetRowState> {
  constructor(props: EPickSheetRowProps) {
    super(props)

    this.state = {
      clickedItemId: -1,
      ids: [],
      loaded: false,
      firstOpen: false,
      isExpanded: true,
      catchWeightItems: [],
      totalBillableQty: 0,
      isLastScanFailed: false,
      lastPickedQty: 0,
      isOverPicked: false
    }
  }
  // componentDidMount() {
  //   this.setCatchWeightItems(this.props.cwItemValues)
  // }

  // componentWillReceiveProps(nextProps: EPickSheetRowProps) {
  //   if (JSON.stringify(this.props.cwItemValues) != JSON.stringify(nextProps.cwItemValues)) {
  //     this.setCatchWeightItems(nextProps.cwItemValues)
  //   }
  //   if (this.props.orderItem &&
  //     this.props.expandedOrderItemId != nextProps.expandedOrderItemId &&
  //     nextProps.expandedOrderItemId &&
  //     nextProps.expandedOrderItemId == this.props.orderItem.wholesaleOrderItemId) {
  //     this.setState({
  //       isExpanded: true,
  //       clickedItemId: nextProps.expandedOrderItemId
  //     }, () => {
  //       this.setCatchWeightItems(nextProps.cwItemValues)
  //     })
  //   }
  //   if (this.props.scanMode != nextProps.scanMode && !nextProps.scanMode) {
  //     setTimeout(() => {
  //       this.initEventHandler()
  //     }, 50)
  //   }
  // }

  // setCatchWeightItems = (catchWeightItems: any) => {
  //   const { orderItem } = this.props
  //   if (!catchWeightItems[orderItem.wholesaleOrderItemId]) {
  //     return
  //   }
  //   let weightItems = [...catchWeightItems[orderItem.wholesaleOrderItemId]]
  //   const needEmptyRows = orderItem.quantity - weightItems.length
  //   if (weightItems.length < orderItem.quantity) {
  //     for (let i = 0; i < needEmptyRows; i++) {
  //       weightItems.push({
  //         unitWeight: 0
  //       })
  //     }
  //   }
  //   // debugger
  //   let totalQuantity = 0
  //   weightItems.forEach((el, index) => {
  //     totalQuantity += isNaN(el.unitWeight) ? 0 : Number.parseFloat(el.unitWeight)
  //   });
  //   this.setState({
  //     catchWeightItems: weightItems,
  //     totalBillableQty: totalQuantity,
  //     isOverPicked: weightItems.length > orderItem.quantity,
  //     loaded: true,
  //   }, () => {
  //     this.setFocusOnInput(orderItem.wholesaleOrderItemId)
  //   })
  // }

  // setFocusOnInput = (orderItemId: number) => {
  //   setTimeout(() => {
  //     if (this.props.scanMode) {
  //       const fields = jQuery(`.item-wrapper-${orderItemId}`).find('.ant-input, .ant-btn')
  //       const validUnitCount = this.state.catchWeightItems.filter((el: any) => el.unitWeight && !isNaN(el.unitWeight)).length
  //       if (validUnitCount > 0 && validUnitCount < fields.length) {
  //         const focusIndex = validUnitCount
  //         jQuery(fields[focusIndex]).trigger('focus')
  //       }
  //     } else {
  //     }
  //     if (!this.props.scanMode)
  //       this.initEventHandler();
  //   }, 50)
  // }

  // initEventHandler = () => {
    // jQuery('.quantity-item input').unbind()

    // // Move focus to next field by carriage return
    // jQuery('.quantity-item input').unbind().bind('click', (e: any) => {
    //   // When user clicks on field, full contents of field should be highlighted, so when user starts typing, full content of field is replaced
    //   jQuery(e.target).select();
    // }).bind('keypress', (e: any) => {
    //   if (e.keyCode === 13 || e.keyCode === 9) {
    //     this.moveToNextField(e)
    //   }
    // }).bind('focus', (e: any) => {
    //   jQuery(e.target).trigger('select')
    // }).bind('blur', (e: any) => {
    //   this.moveToNextField(e)
    // })
  // }

  // moveToNextField = (e: any) => {
  //   const { orderItem } = this.props
  //   const fields = jQuery(`.item-wrapper-${orderItem.wholesaleOrderItemId}`).find('.ant-input')
  //   const curIndex = fields.index(e.target)
  //   if (curIndex < fields.length - 1) {
  //     setTimeout(() => {
  //       jQuery(fields[curIndex + 1]).trigger('focus')
  //     }, 10)
  //   }
  // }

  // getQuantityCatchWeight = (orderItemId: any) => {
  //   this.props.setOrderItemId(parseInt(orderItemId, 10))
  //   this.props.getQuantityCatchWeight(orderItemId)
  // }

  // deleteQuantity = (index: number, evt: any) => {
  //   const { orderItem } = this.props
  //   const updateItems = [...this.state.catchWeightItems]
  //   updateItems.splice(index, 1)
  //   this.setState({
  //     catchWeightItems: updateItems
  //   }, () => {
  //     this.props.onDeleteCWItem(orderItem.wholesaleOrderItemId, updateItems)
  //   })
  // }

  // onChangeUnitWeight = (index: number, value: number) => {
  //   const { orderItem } = this.props
  //   let updatedItems = [...this.state.catchWeightItems]

  //   updatedItems.forEach((el: any, idx: number) => {
  //     if (idx === index) {
  //       el.unitWeight = value
  //     }
  //   })
  //   this.setState({ catchWeightItems: updatedItems }, () => {
  //     this.initEventHandler()
  //     this.props.updateManualInputCatchWeight(updatedItems, orderItem.wholesaleOrderItemId)
  //   })
  // }

  // onBlurCatchWeightField = (lastScanWeight: any) => {
  //   const { scanMode, orderItem } = this.props
  //   const { catchWeightItems } = this.state
  //   if (!scanMode) {
  //     const totalBillableQty = _.sumBy(catchWeightItems, (el) => {
  //       return !isNaN(el.unitWeight) ? Number.parseFloat(el.unitWeight) : 0
  //     })

  //     let isOverPicked = false
  //     let pickedAmount = 0
  //     catchWeightItems.forEach(el => {
  //       if (el.unitWeight && !isNaN(el.unitWeight)) {
  //         pickedAmount++
  //       }
  //     });
  //     if (orderItem.quantity < pickedAmount) {
  //       isOverPicked = true
  //     }
  //     const lastScanItem = {
  //       ...orderItem,
  //       lastScanWeight
  //     }
  //     this.props.updateLastScanStatus(false, isOverPicked, lastScanItem, catchWeightItems)
  //     this.setState({ totalBillableQty })
  //   }
  // }

  // renderQuantitiesField = () => {
  //   const { catchWeightItems, isOverPicked } = this.state
  //   const { orderItem, scanMode } = this.props
  //   let result: any[] = []
  //   catchWeightItems.forEach((el: any, index: number) => {
  //     let warning = isOverPicked && orderItem.quantity < index + 1
  //     result.push(
  //       <Item className={`quantity-item item-wrapper-${orderItem.wholesaleOrderItemId}`} key={index}>
  //         <Flex className="v-center space-between">
  //           <InputLabel className={warning ? 'warning' : ''}>
  //             {index == 0 && orderItem ? orderItem.UOM : ''} {index + 1}
  //           </InputLabel>
  //           <AntIcon type="close-circle" className="close-quantity" onClick={this.deleteQuantity.bind(this, index)} />
  //         </Flex>
  //         <div>
  //           {scanMode ? (
  //             <ThemeInput
  //               className={warning ? "warning" : ''}
  //               value={el.unitWeight == 0 ? '' : el.unitWeight}
  //               onChange={(e: any) => this.onChangeUnitWeight(index, e.target.value)}
  //               onBlur={() => this.onBlurCatchWeightField(el.unitWeight)} />
  //           ) : (
  //             <NumericInput
  //               className={warning ? "warning" : ''}
  //               value={el.unitWeight == 0 ? '' : el.unitWeight}
  //               onChange={this.onChangeUnitWeight.bind(this, index)}
  //               onBlur={() => this.onBlurCatchWeightField(el.unitWeight)} />
  //           )}
  //         </div>
  //       </Item>,
  //     )
  //   })
  //   return result
  // }

  showCatchWeightValues = (item: OrderItem) => {
    this.setState({
      isExpanded: !this.state.isExpanded,
      clickedItemId: item.wholesaleOrderItemId
    })
  }

  getTotalPicked = (orderItem) => {
    const total = orderItem.picked
    if (!judgeConstantRatio(orderItem)) {
      return _.get(orderItem, 'quantityDetailList', []).filter(v => _.get(v, 'unitWeight', 0) && !_.get(v, 'isDeleted', undefined)).length
    }
    return total
  }
  
  render() {
    const { index, orderItem, loading, picked, catchWeights } = this.props
    const { isExpanded, clickedItemId, totalBillableQty, catchWeightItems } = this.state
    let cwNonEmptyValues = catchWeightItems.filter(el => !isNaN(el.unitWeight) && el.unitWeight > 0).length
    const isNonCatchWeightItem = judgeConstantRatio(orderItem)

    const RenderRowTip = () => {
     if (this.getTotalPicked(orderItem) > orderItem.quantity) {
        return <AntdIconWrap type="warning" style={{ color: '#1c6e31' }} />
      } 
      return <span className='index'/>
    }

    return (
      <EPickSheetRowItemWrapper>
        {/* <EPickSheetRowItem>
          <Col span={4}>
            <Flex className='h-center amount'>
              <div className={`picked ${(isNonCatchWeightItem && picked) || (!isNonCatchWeightItem && cwNonEmptyValues) ? 'green' : ''}`}>
                {this.getTotalPicked(orderItem)}
              </div>
                /
              <span className='quantity' style={{ marginRight: 6, minWidth: 20 }}>{orderItem.quantity}</span> 
              {orderItem.UOM}
            </Flex>
          </Col>
          <Col span={5} style={{ margin: '0 0 0 17px' }}>
            {orderItem.lotId}
          </Col >
          <Col span={5}>
            {orderItem.variety}
          </Col>
          <Col span={2}>
            {orderItem.modifiers}
          </Col>
          <Col span={2}>
            {orderItem.extraOrigin}
          </Col>
          {orderItem.quantityDetailList && <Col span={1}>
            <Icon
              onClick={() => this.showCatchWeightValues(orderItem)}
              type={!isExpanded ? 'row-expand' : 'row-collapse'}
              style={{ fill: 'transparent', cursor: 'pointer' }}
              viewBox={'0 0 12 18'}
              width={12}
              height={18}
            />
          </Col>}
        </EPickSheetRowItem> */}
        <div className='collapseHeader'>
          <p>
            <RenderRowTip />
            <p><span className='picked'>{this.getTotalPicked(orderItem)}</span>/<span className='quantity'>{orderItem.quantity}</span> {orderItem.UOM}</p>
            <span className='lot'>{orderItem.lotId}</span>
            <span>{orderItem.editedItemName || orderItem.variety}</span>
            <span>{orderItem.modifiers}</span> 
            <span>{orderItem.extraOrigin}</span> 
          </p>
          {orderItem.quantityDetailList && <Icon
              onClick={() => this.showCatchWeightValues(orderItem)}
              type={!isExpanded ? 'row-expand' : 'row-collapse'}
              style={{ fill: 'transparent', cursor: 'pointer' }}
              viewBox={'0 0 12 18'}
              width={12}
              height={18}
            />
          }
        </div>
        {orderItem.quantityDetailList && isExpanded &&
          <div style={{ padding: '0 40px 0 26px' }}>
            <div style={{ fontWeight: 'normal', padding: '5px 0' }}>Total Billable Qty Picked = {_.sumBy(orderItem.quantityDetailList, 'unitWeight').toFixed(2)} {orderItem.pricingUOM}</div>
            <Row gutter={[20, 20]} type='flex'>
              {orderItem.quantityDetailList.filter(v => !v.isDeleted).map(v => (
                <Col span={4} key={v.id}>
                  <AntIcon type="close-circle" style={{ marginLeft: 80, cursor: 'pointer' }} onClick={() => this.props.handleDeleteUnitWeight(orderItem.displayOrderProduct, orderItem.wholesaleOrderItemId, v.id)} />
                  <ThemeInput
                    className={v.unitWeight > orderItem.quantity ? "warning" : ''}
                    value={v.unitWeight == 0 ? '' : v.unitWeight}
                    onChange={(e: any) => this.props.handleChangeUnitWeight(orderItem.displayOrderProduct, orderItem.wholesaleOrderItemId, v.id, e.target.value)}
                  />
                </Col>
              ))}
              <Col span={4} style={{ display: 'flex', alignItems: 'flex-end' }}>
                <ThemeButton onClick={() => this.props.handleAddUnitWeight(orderItem.displayOrderProduct, orderItem.wholesaleOrderItemId)}>Add Unit</ThemeButton>
              </Col>
            </Row>
            <ThemeSpin size={'small'} spinning={loading && clickedItemId == orderItem.wholesaleOrderItemId} style={{ width: '100%', textAlign: 'center' }} />
          </div>
        }
      </EPickSheetRowItemWrapper>
    )
  }
}

export default EPickSheetRow

const AntdIconWrap = styled(AntIcon)`
  font-size: 20px !important;
  margin-left: -7px !important;
  margin-right: 2px;
`
