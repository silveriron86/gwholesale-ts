import { Row, Col, Collapse, Icon as AntIcon, Button } from 'antd'
import * as React from 'react'
import { DialogSubContainer, FlexDiv, DialogBodyDiv, EPickStatus } from '~/modules/customers/nav-sales/styles'
import { ThemeButton, ThemeOutlineButton, ThemeSwitch } from '~/modules/customers/customers.style'
import { OrderDetail, OrderItem } from '~/schema'
import { Item, ItemValue } from '~/modules/vendors/purchase-orders/_style'
import EPickSheetRow from './epick-sheet-row'
import { checkError, formatNumber, formatOrderItem, getOrderPrefix, judgeConstantRatio } from '~/common/utils'
import scan_big from './../../images/scan_big.png'
import scan_finish from './../../images/scan_finish.png'
import { Icon } from '~/components'
import _, { cloneDeep } from 'lodash'
import jQuery from 'jquery'
import styled from '@emotion/styled'
import { OrderService } from '~/modules/orders/order.service'
import { v4 as uuidv4 } from 'uuid'

const { Panel } = Collapse;

type EPickSheetProps = {
  onClose: Function
  getQuantityCatchWeight: Function
  resetLoading: Function
  setOrderItemId: Function
  visible: boolean
  orderItems: OrderItem[]
  order: OrderDetail
  sellerSetting: any
  catchWeights: any
  catchWeight: any[]
  loading: boolean
  updateOrder: Function
  catchWeightValues: any
  getWeightsByOrderItemIds: Function
  saveEPickedValues: Function
  orderItemByProduct: any
  startUpdating: Function
  getOrderItemsById: Function
}

class EPickSheet extends React.PureComponent<EPickSheetProps> {
  state = {
    isParsing: false,
    isLastScanFailed: undefined,
    isLastScanPartiallyCompleted: false,
    isLastScanCompleted: false,
    isLastScanOverPicked: false,
    isPickingSuccess: false,
    lastCheckItem: null,
    scanMode: true,
    newItems: cloneDeep(this.props.orderItems),
    cwItemValues: this.props.catchWeightValues ? this.props.catchWeightValues : [],
    isCatchWeightItemOverPicked: false,
    isOverPicked: false,
    expandedOrderItemId: 0,
    isNonCatchWeightItemScanned: undefined,
    collapseActiveKey: Object.keys(this.props.orderItemByProduct),
    orderItemByProduct: this.props.orderItemByProduct,
    loading: false,
    saveButtonLoading: false,
    currentWaringType: 0,
    currentWaringItem: null,
    isAddedItem: false,
    prevOrderItemByProduct: null,
    isTriggerWarning: true,
    scanHistory: [],
    catchWeightByWholesaleOrderItemIds: {}
  }

  componentDidMount() {
    const _this = this
    this.setState({ loading: true })

    OrderService.instance.getOrderEPickCatchWeight(this.props.order.wholesaleOrderId).subscribe({
      next(resp) {
        const orderItemByProduct = {}
        const catchWeightByWholesaleOrderItemIds = _.groupBy(resp.body.data, 'wholesaleOrderItemId')
        const data = Object.values(_this.state.orderItemByProduct).map(v => ({
            ...v,
            items: v.items.map((item: any) => {
              if (judgeConstantRatio(item)) return item
              return {
                ...item,
                quantityDetailList: _.sortBy(catchWeightByWholesaleOrderItemIds[item.wholesaleOrderItemId], 'displayNumber')
              }
            })
          })
        )
        data.forEach(v => {
          orderItemByProduct[v.displayOrderProduct] = v
        })
        _this.setState({ loading: false, orderItemByProduct, catchWeightByWholesaleOrderItemIds, scanHistory: [orderItemByProduct] })
      },
      error(err) { checkError(err) },
      complete() {
        _this.setState({ loading: false })
      }
    })

    this.setFocusToHiddenBarcodeInput()
    jQuery('#hidden-barcode-input')
      .unbind()
      .bind('keyup', (e: any) => {
        if (e.keyCode == 13) {
          _this.handleParsing(e.target.value)
          jQuery(e.target).trigger('select')
        }
      })
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if (this.props.orderItemByProduct !== nextProps.orderItemByProduct) {
      console.log(1)
      const orderItemByProduct = {}
      const data = Object.values(nextProps.orderItemByProduct).map(v => ({
          ...v,
          items: v.items.map((item: any) => {
            if (judgeConstantRatio(item)) return item
            return {
              ...item,
              quantityDetailList: _.sortBy(this.state.catchWeightByWholesaleOrderItemIds[item.wholesaleOrderItemId], 'displayNumber')
            }
          })
        })
      )
      data.forEach(v => {
        orderItemByProduct[v.displayOrderProduct] = v
      })
      this.setState({ loading: false, orderItemByProduct, scanHistory: [orderItemByProduct] })
    }
  }

  handleParsing = (barcode: string) => {
    jQuery('#hidden-barcode-input').select()

    const { sellerSetting } = this.props
    const { orderItemByProduct, newItems, scanHistory } = this.state
    this.setState({ isAddedItem: false, currentWaringType: 0 })
    // let isFailed = false

    // if (sellerSetting && sellerSetting.company.companyName == 'Winner\'s Meats') {
    //   if (barcode.length !== 12) {
    //     isFailed = true
    //   } else {
    //     // digits 2 ~ 6
    //     let sku = barcode.substr(1, 5)
    //     if (catchWeightItem.SKU && catchWeightItem.SKU.indexOf(sku) === 0) {
    //       // 7 to 11, real value

    //     } else {
    //       isFailed = true
    //     }
    //   }
    // } else {
    //   parsedWithJana = this.checkWithJanaAlgorithm(barcode, catchWeightItem)
    //   isFailed = parsedWithJana[0]
    //   isJanaAlgorithm = true
    // }

    const findItem = _.flattenDeep(Object.values(orderItemByProduct).map(v => v.items)).find((v: any) => {
      if (!v.lotId) return
      return barcode.includes(_.get(v, 'lotId', '').match(/-(\S*)/)[1])
    })

    this.props.orderItems.forEach(v => {
      if (this.checkWithJanaAlgorithm(barcode, v)[0]) {

      }
    })
    
    if (findItem) {
      if (!judgeConstantRatio(findItem)) {
        const parseUnitWeight = this.checkWithJanaAlgorithm(barcode, findItem)[1]
        jQuery('#hidden-barcode-input').val('')
        this.setState({
          orderItemByProduct: {
            ...orderItemByProduct,
            [~~findItem.displayOrder]: {
              ...orderItemByProduct[~~findItem.displayOrder],
              items: orderItemByProduct[~~findItem.displayOrder].items.map((v: any) => {
                if (v.lotId === findItem.lotId) {
                  if (v.quantityDetailList.some(u => u.unitWeight === 0)) {
                    const quantityDetailList = v.quantityDetailList
                    for (let [index, value] of v.quantityDetailList.entries()) {
                      if (!value.unitWeight) {
                        quantityDetailList[index].unitWeight = Number(parseUnitWeight)
                        break
                      }
                    }
                    return ({
                      ...v,
                      quantityDetailList
                    })
                  } else {
                    return ({
                      ...v,
                      isUpdated: true,
                      quantityDetailList: [
                        ...v.quantityDetailList,
                        {
                          unitWeight:
                          Number(parseUnitWeight),
                          id: uuidv4(),
                          displayNumber: _.last(v.quantityDetailList).displayNumber + 1
                        }
                      ]
                    })
                  }
                }
                return v
              })
            }
          }
        })
      } else {
        console.log(findItem)

        const currentProduct = orderItemByProduct[~~findItem.displayOrder]
        if (findItem.picked >= findItem.quantity && this.state.isTriggerWarning) {
          this.setState({
            currentWaringType: 2,
            currentWaringItem: findItem,
            prevOrderItemByProduct: orderItemByProduct,
            isTriggerWarning: false
          })
        } else {
          jQuery('#hidden-barcode-input').val('')
          this.setState({
            orderItemByProduct: {
              ...orderItemByProduct,
              [~~findItem.displayOrder]: {
                ...orderItemByProduct[~~findItem.displayOrder],
                picked: Number(orderItemByProduct[~~findItem.displayOrder].picked) + 1,
                items: orderItemByProduct[~~findItem.displayOrder].items.map((v: any) => {
                  if (v.lotId === findItem.lotId) {
                    return ({ ...v, isUpdated: true, picked: Number(v.picked) + 1 })
                  }
                  return v
                })
              }
            }
          })
        }
      }
    } else {
      if (barcode.length > 18 && barcode.substr(18).includes('-')) {
        const orderItemId = barcode.substr(barcode.length > 35 ? 28 : 18).split('-')[0]
        const _this = this
        OrderService.instance.getOrderItemByOrderItemId(orderItemId).subscribe({
          next(resp) {
            const data = resp.body.data
            const variety = data.wholesaleItem.variety
            const findItem = Object.values(orderItemByProduct).find(v => v.variety === variety)
            if (findItem) {
              if (!judgeConstantRatio(findItem)) {
                const parseUnitWeight = _this.checkWithJanaAlgorithm(barcode, findItem)[1]

                _this.setState({
                  currentWaringType: 1,
                  prevOrderItemByProduct: orderItemByProduct,
                  currentWaringItem: {
                    variety: data.wholesaleItem.variety,
                    modifiers: data.modifiers,
                    extraOrigin: data.extraOrigin,
                    quantity: 0,
                    picked: 1,
                    wholesaleItemId: data.wholesaleItem.wholesaleItemId,
                    lotId: data.lotId,
                    cost: data.cost,
                    price: findItem.price,
                    displayOrder: ((findItem.displayOrderProduct * 100) + findItem.items.length) / 100,
                    displayOrderProduct: findItem.displayOrderProduct,
                    UOM: data.wholesaleItem.inventoryUOM,
                    pricingUOM: data.wholesaleItem.pricingUOM,
                    ratio: data.ratio,
                    lotAssignmentMethod: data.lotAssignmentMethod,
                    enableLotOverflow: data.enableLotOverflow,
                    inventoryUOM: data.wholesaleItem.inventoryUOM,
                    quantityDetailList: [
                      {
                        unitWeight: Number(parseUnitWeight),
                        id: uuidv4(),
                        displayNumber: 1
                      }
                    ],
                    wholesaleProductUomList: data.wholesaleItem.wholesaleProductUomList,
                    isAdded: true
                  },
                })
              } else {
                _this.setState({
                  currentWaringType: 1,
                  prevOrderItemByProduct: orderItemByProduct,
                  currentWaringItem: {
                    variety: data.wholesaleItem.variety,
                    modifiers: data.modifiers,
                    extraOrigin: data.extraOrigin,
                    quantity: 0,
                    picked: 1,
                    wholesaleItemId: data.wholesaleItem.wholesaleItemId,
                    lotId: data.lotId,
                    cost: data.cost,
                    price: findItem.price,
                    displayOrder: ((findItem.displayOrderProduct * 100) + findItem.items.length) / 100,
                    displayOrderProduct: findItem.displayOrderProduct,
                    UOM: data.wholesaleItem.inventoryUOM,
                    pricingUOM: data.pricingUOM,
                    ratio: data.ratio,
                    lotAssignmentMethod: data.lotAssignmentMethod,
                    enableLotOverflow: data.enableLotOverflow,
                    isAdded: true
                  },
                })
              }
            } else {
              _this.setState({
                currentWaringType: 4,
                currentWaringItem: {
                  variety: data.wholesaleItem.variety,
                  modifiers: data.modifiers,
                  extraOrigin: data.extraOrigin,
                }
              })
            }
          },
          error(err) { checkError(err) },
          complete() {
            _this.setState({ loading: false })
          }
        })
      } else {
        const nonCatchWeightItems = newItems.filter(el => judgeConstantRatio(el))
        const catchWeightItems = _.difference(newItems, nonCatchWeightItems);
        for (let i = 0; i < catchWeightItems.length; i++) {
          const catchWeightItem = catchWeightItems[i]
          console.log(catchWeightItem)
          if (sellerSetting && sellerSetting.company.companyName == 'Winner\'s Meats') {
            if (barcode.length !== 12) {
              this.setState({
                currentWaringType: 3,
              })
              break;
            } else {
              // digits 2 ~ 6
              let sku = barcode.substr(1, 5)
              if (catchWeightItem.SKU && catchWeightItem.SKU.indexOf(sku) === 0) {
                // 7 to 11, real value

              } else {
                this.setState({
                  currentWaringType: 3,
                })
                break;
              }
            }
          } else {
            console.log(1)
            this.setState({
              currentWaringType: 3,
            })
            break;
          }
        }
        this.setState({
          currentWaringType: 3,
        })
      }
    }


    // const { newItems, cwItemValues, scanMode } = this.state
    // this.setState({
    //   isParsing: true
    // })
    // let isFailed = false
    // let lastCheckItem: OrderItem | null = null
    // let isOverPicked = false
    // let isNonCatchWeightItemScanned = undefined
    // // check on non-catchweight items
    // const nonCatchWeightItems = newItems.filter(el => judgeConstantRatio(el))
    // for (let i = 0; i < nonCatchWeightItems.length; i++) {
    //   isNonCatchWeightItemScanned = true
    //   isFailed = this.parseLabelSerialForNonCWItemsFailed(barcode, nonCatchWeightItems[i])
    //   if (!isFailed) {
    //     lastCheckItem = nonCatchWeightItems[i]
    //     if (lastCheckItem.picked < lastCheckItem.quantity) {
    //       lastCheckItem.picked++
    //     } else {
    //       isFailed = true //display warning message when overpicked
    //       isOverPicked = true
    //     }
    //     if (lastCheckItem.picked > 0) {
    //       lastCheckItem.status = 'PICKING'
    //     }
    //     const findIndex = newItems.findIndex(el => el.wholesaleOrderItemId == lastCheckItem.wholesaleOrderItemId)
    //     let newData = [...newItems]
    //     newData[findIndex] = lastCheckItem

    //     this.setState({
    //       lastCheckItem,
    //       newItems: newData,
    //       isLastScanFailed: isFailed,
    //       isParsing: false,
    //       isOverPicked
    //     })
    //     break
    //   }
    // };

    // // check on catchweight items
    // if (!lastCheckItem) {
    //   const { sellerSetting } = this.props
    //   const catchWeightItems = _.difference(newItems, nonCatchWeightItems);
    //   for (let i = 0; i < catchWeightItems.length; i++) {
    //     isNonCatchWeightItemScanned = false
    //     let isJanaAlgorithm = false
    //     isFailed = false
    //     const catchWeightItem = catchWeightItems[i]
    //     if (scanMode) {
    //       let parsedWithJana: [boolean, string] = [false, '0']
    //       if (sellerSetting && sellerSetting.company.companyName == 'Winner\'s Meats') {
    //         if (barcode.length !== 12) {
    //           isFailed = true
    //         } else {
    //           // digits 2 ~ 6
    //           let sku = barcode.substr(1, 5)
    //           if (catchWeightItem.SKU && catchWeightItem.SKU.indexOf(sku) === 0) {
    //             // 7 to 11, real value

    //           } else {
    //             isFailed = true
    //           }
    //         }
    //       } else {
    //         parsedWithJana = this.checkWithJanaAlgorithm(barcode, catchWeightItem)
    //         isFailed = parsedWithJana[0]
    //         isJanaAlgorithm = true
    //       }
    //       // debugger
    //       //if isFailed = false, we picked this orderitem and enter the value in the unitweight cell
    //       const orderItemId = catchWeightItem.wholesaleOrderItemId
    //       let catchWeightValues = cwItemValues[orderItemId]
    //       if (!isFailed && catchWeightValues) {
    //         // this.setState({ expandedOrderItemId: orderItemId })
    //         let lastEmptyIndex = 0
    //         let validUnitsCount = catchWeightValues.filter((el: any) => el.unitWeight && !isNaN(el.unitWeight)).length
    //         if (validUnitsCount > 0 || catchWeightValues.length == 0) {
    //           if (catchWeightValues.length == validUnitsCount) {
    //             catchWeightValues = [...catchWeightValues, { unitWeight: 0 }]
    //           }
    //           lastEmptyIndex = validUnitsCount
    //         }
    //         // let lastEmptyIndex = catchWeightValues.lastIndexOf((el: any) => !el.unitWeight || _.trim(el.unitWeight))

    //         if (isJanaAlgorithm && parsedWithJana.length) {
    //           catchWeightValues[lastEmptyIndex].unitWeight = isNaN(parseFloat(parsedWithJana[1])) ? 0 : parseFloat(parsedWithJana[1])
    //         } else {
    //           const val = barcode.substr(6, 3) + '.' + barcode.substr(9, 2)
    //           catchWeightValues[lastEmptyIndex].unitWeight = isNaN(parseFloat(val)) ? 0 : parseFloat(val)
    //         }
    //         if (catchWeightValues[lastEmptyIndex].unitWeight) {
    //           validUnitsCount++
    //         }
    //         const copyCwItems = cloneDeep(cwItemValues)
    //         copyCwItems[orderItemId] = catchWeightValues
    //         this.setState({ expandedOrderItemId: orderItemId, cwItemValues: copyCwItems })
    //         const lastScanItem = { ...catchWeightItem, lastScanWeight: catchWeightValues[lastEmptyIndex].unitWeight }
    //         isOverPicked = validUnitsCount > catchWeightItem.quantity
    //         this.updateLastScanStatus(isFailed, isOverPicked, lastScanItem, catchWeightValues)
    //         this.setState({
    //           isParsing: false
    //         })
    //         //after validate the barcode, we will break from this for-loop
    //         break;
    //       }
    //     }
    //   }
    // }

    // if (isFailed && !lastCheckItem) {
    //   this.setState({
    //     isLastScanFailed: true,
    //     lastCheckItem: null,
    //     isParsing: false,
    //     isNonCatchWeightItemScanned
    //   })
    // } else {
    //   this.setState({
    //     isParsing: false,
    //     isNonCatchWeightItemScanned
    //   })
    // }
    this.setState({
      scanHistory: [...this.state.scanHistory, this.state.orderItemByProduct]
    })
  }

  checkWithJanaAlgorithm = (unitWeight: string, orderItem: OrderItem): [boolean, string] => {
    // debugger
    let supplierSkus = orderItem && orderItem.supplierSkus ? orderItem.supplierSkus.split(',') : []
    supplierSkus = supplierSkus.map(el => { return el.replaceAll(' ', '').trim() })
    const recordLabelSerial = orderItem.labelSerial
    //If digits 5 to 7  is not equal to "WSW" then use https://trello.com/c/mhP4jIiC/2218-gs128-parsing-algorithm-slice-1
    const isWSW = unitWeight.substr(4, 3) === 'WSW'
    let isFailed: boolean = false
    let unitValue: string = '0'
    if (!isWSW) {
      return this.parseByGS128(unitWeight, supplierSkus)
    } else {
      const labelSerial = unitWeight.substr(10, 5)
      if (recordLabelSerial != labelSerial) {
        isFailed = true
      }
      const realValueStr = unitWeight.substr(20, 6)
      unitValue = realValueStr.substr(0, realValueStr.length - 2) + '.' + realValueStr.substr(-2)
    }

    return [isFailed, unitValue]
  }

  parseByGS128 = (unitWeight: string, supplierSkus: string[]): [boolean, string] => {
    let isFailed: boolean = false
    let unitValue: string = '0'
    //GS 128 parsing algorithm
    const prefix = unitWeight.substr(0, 2)
    if (prefix != '01' && prefix != '02') {
      isFailed = true
    }

    if (unitWeight.length > 17) {
      //17th digit
      const supplierSku = unitWeight.substr(10, 5)
      if (supplierSkus.indexOf(supplierSku) == -1) {
        isFailed = true
      }
      const flag = unitWeight[16]
      let suffix = ''
      if (flag != '1') {
        suffix = unitWeight.substr(16)
      } else if (unitWeight.length > 24) {
        suffix = unitWeight.substr(24)
      }
      if (!suffix || (suffix.indexOf('320') != 0 && suffix.indexOf('310') != 0)) {
        isFailed = true
      }

      if (suffix.length > 3) {
        const floatPoint = parseInt(suffix[3])
        if (isNaN(floatPoint)) {
          isFailed = true
        }

        const realValueStr = suffix.substr(4, 6)
        const strValue = realValueStr.substr(0, realValueStr.length - floatPoint) + '.' + realValueStr.substr(-floatPoint)
        if (suffix.indexOf('320') == 0) {
          unitValue = formatNumber(parseFloat(strValue), 2)
        } else if (suffix.indexOf('310') == 0) {
          unitValue = formatNumber(parseFloat(strValue) * 2.20483, 2)
        }
      }
    }
    return [isFailed, unitValue]
  }

  setFocusToHiddenBarcodeInput = () => {
    jQuery('#hidden-barcode-input').trigger('focus')
  }

  componentWillReceiveProps(nextProps: EPickSheetProps) {
    if (JSON.stringify(this.props.catchWeightValues) != JSON.stringify(nextProps.catchWeightValues)) {
      this.setState({ cwItemValues: nextProps.catchWeightValues })
    }
  }

  onChangeScanMode = (scanMode: boolean) => {
    this.setState({ scanMode })
  }

  parseLabelSerialForNonCWItemsFailed = (value: string, orderItem: OrderItem) => {
    const isWSW = value.substr(4, 3) === 'WSW'
    // orderItem.labelSerial = '21999'//for test
    const recordLabelSerial = orderItem.labelSerial
    const labelSerial = value.substr(10, 5)
    if (!isWSW) {
      const prefix = value.substr(0, 2)
      if (prefix != '01' && prefix != '02') {
        return true
      }
      const flag = value[16]
      let suffix = ''
      if (flag != '1') {
        suffix = value.substr(16)
      } else if (value.length > 24) {
        suffix = value.substr(24)
      }
      if (!suffix || (suffix.indexOf('320') != 0 && suffix.indexOf('310') != 0)) {
        return true
      }
    } else if (recordLabelSerial != labelSerial) {
      return true
    }
    return false;
  }

  updateManualInputCatchWeight = (catchWeightValues: any[], orderItemId: number) => {
    const copyCwItems = cloneDeep(this.state.cwItemValues)
    copyCwItems[orderItemId] = catchWeightValues
    this.setState({ cwItemValues: copyCwItems })
  }

  handleChangeCollapse = (collapseActiveKey: number[]) => {
    this.setState({ collapseActiveKey })
  }

  handleAllCollapse = () => {
    if (this.state.collapseActiveKey.length) {
      this.setState({ collapseActiveKey: [] })
    } else {
      this.setState({ collapseActiveKey: Object.keys(this.state.orderItemByProduct) })
    }
  }

  handleChangeUnitWeight = (displayOrderProduct: number, wholesaleOrderItemId: number, id: number, unitWeight: string) => {
    this.setState({
      orderItemByProduct: {
        ...this.state.orderItemByProduct,
        [displayOrderProduct]: {
          ...this.state.orderItemByProduct[displayOrderProduct],
          items: this.state.orderItemByProduct[displayOrderProduct].items.map(item => {
            if (item.wholesaleOrderItemId === wholesaleOrderItemId) {
              return ({
                ...item,
                isUpdated: true,
                quantityDetailList: item.quantityDetailList.map((catchWeight: any) => {
                  if (catchWeight.id === id) return ({ ...catchWeight, unitWeight: Number(unitWeight) })
                  return catchWeight
                })
              })
            }
            return item
          })
        }
      }
    })
  }

  handleDeleteUnitWeight = (displayOrderProduct: number, wholesaleOrderItemId: number, id: number) => {
    this.setState({
      orderItemByProduct: {
        ...this.state.orderItemByProduct,
        [displayOrderProduct]: {
          ...this.state.orderItemByProduct[displayOrderProduct],
          items: this.state.orderItemByProduct[displayOrderProduct].items.map(item => {
            if (item.wholesaleOrderItemId === wholesaleOrderItemId) {
              return ({
                ...item,
                isUpdated: true,
                quantityDetailList: item.quantityDetailList.map((catchWeight: any) => {
                  if (catchWeight.id === id) return ({ ...catchWeight, isDeleted: true })
                  return catchWeight
                })
              })
            }
            return item
          })
        }
      }
    })
  }

  handleAddUnitWeight = (displayOrderProduct: number, wholesaleOrderItemId: number) => {
    this.setState({
      orderItemByProduct: {
        ...this.state.orderItemByProduct,
        [displayOrderProduct]: {
          ...this.state.orderItemByProduct[displayOrderProduct],
          items: this.state.orderItemByProduct[displayOrderProduct].items.map(item => {
            if (item.wholesaleOrderItemId === wholesaleOrderItemId) {
              return ({
                ...item,
                quantityDetailList: [
                  ...item.quantityDetailList,
                  { unitWeight: 0,
                    id: uuidv4(),
                    displayNumber: _.last(item.quantityDetailList).displayNumber + 1
                  }
                ]
              })
            }
            return item
          })
        }
      }
    })
  }

  getTotalPicked = (orderItem) => {
    const total = orderItem.picked
    if (!judgeConstantRatio(orderItem)) {
      return _.flatten(orderItem.items.map(v => _.get(v, 'quantityDetailList', []))).filter(v => _.get(v, 'unitWeight', 0) && !_.get(v, 'isDeleted', undefined)).length
    }
    return total
  }


  renderItems = () => {
    const { catchWeight, catchWeights, loading, getQuantityCatchWeight, resetLoading, setOrderItemId, sellerSetting } = this.props
    const { scanMode, orderItemByProduct } = this.state

    const RenderRowTip = ({ item, index }: any) => {
      if (item.isAdded) {
        return <AntdIconWrap type="warning" style={{ marginRight: '2px', color: '#1c6e31' }} />
      } else if (this.getTotalPicked(item) > item.quantity) {
        return <AntdIconWrap type="warning" style={{ marginRight: '2px', color: '#1c6e31' }} />
      } else if (this.getTotalPicked(item) == item.quantity) {
        return <AntdIconWrap type="check" style={{ marginRight: '6px', color: '#1c6e31' }} />
      } else {
        return <span className='index'>{index + 1}.</span>
      }
    }

    return (
      <CollapseWrap
        bordered={false}
        expandIcon={(props) => props.isActive ? <AntIcon type="minus" /> : <AntIcon type="plus" />}
        activeKey={this.state.collapseActiveKey}
        onChange={this.handleChangeCollapse}
      >
        {Object.values(orderItemByProduct).map((v: any, index: number) => {
          const Header = () => (
            <div className='collapseHeader'>
              <RenderRowTip item={v} index={index} />
              <p><span className='picked'>{this.getTotalPicked(v)}</span>/<span className='quantity'>{v.quantity}</span> {v.UOM || v.inventoryUOM}</p>
              <span className='lot'>{_.get(v, 'lotIds[0]', '')} {v.items.length > 1 && `+${v.items.length - 1}`}</span>
              <span>{v.editedItemName || v.variety}</span>
              <span>{v.modifiers}</span>
              <span>{v.extraOrigin}</span>
            </div>
          )
          return (
            <Panel header={<Header />} key={v.displayOrderProduct}>
              {v.items.map((item: OrderItem) => (
                <EPickSheetRow
                  key={item.wholesaleOrderItemId}
                  handleChangeUnitWeight={this.handleChangeUnitWeight}
                  handleDeleteUnitWeight={this.handleDeleteUnitWeight}
                  handleAddUnitWeight={this.handleAddUnitWeight}
                  index={index + 1}
                  orderItem={item}
                  picked={item.picked}
                  catchWeights={catchWeights}
                  catchWeight={catchWeight}
                  loading={loading}
                  getQuantityCatchWeight={getQuantityCatchWeight}
                  setOrderItemId={setOrderItemId}
                  resetLoading={resetLoading}
                  sellerSetting={sellerSetting}
                  scanMode={scanMode}
                  setParsingStatus={this.setParsingStatus}
                  updateLastScanStatus={this.updateLastScanStatus}
                  cwItemValues={this.state.cwItemValues}
                  onDeleteCWItem={this.onDeleteCWItem}
                  expandedOrderItemId={this.state.expandedOrderItemId}
                  updateManualInputCatchWeight={this.updateManualInputCatchWeight}
                />
              ))}
            </Panel>
          )
        })}
      </CollapseWrap>
    )
  }

  setParsingStatus = () => {
    this.setState({
      isParsing: true
    })
  }

  onDeleteCWItem = (orderItemId: number, cwItems: any[]) => {
    const { cwItemValues } = this.state
    if (cwItemValues[orderItemId] && cwItems.length) {
      cwItemValues[orderItemId] = cwItems
    }
    this.setState({
      cwItemValues
    })
  }

  updateLastScanStatus = (lastScanFailed: boolean, isOverPicked: boolean, item: any, catchWeightValues: any[]) => {
    const { cwItemValues } = this.state
    if (cwItemValues[item.wholesaleOrderItemId] && catchWeightValues.length) {
      cwItemValues[item.wholesaleOrderItemId] = catchWeightValues
    }
    this.setState({
      isLastScanFailed: lastScanFailed,
      isCatchWeightItemOverPicked: isOverPicked,
      lastCheckItem: lastScanFailed && !isOverPicked ? null : item,
      isParsing: false,
      cwItemValues
    })
  }

  renderStatusIconAndMessage = () => {
    const { isLastScanFailed, lastCheckItem, newItems, isOverPicked, isCatchWeightItemOverPicked, isNonCatchWeightItemScanned, currentWaringType, currentWaringItem } = this.state

    // const isAllPicked = newItems.every(el => {
    //   if (judgeConstantRatio(el)) {
    //     return el.quantity == el.picked
    //   } else {
    //     const catchWeightValues = this.state.cwItemValues[el.wholesaleOrderItemId]
    //     if (catchWeightValues && catchWeightValues.length) {
    //       let nonEmptyCount = 0
    //       catchWeightValues.forEach((element: any) => {
    //         if (element.unitWeight && !isNaN(element.unitWeight)) {
    //           nonEmptyCount++
    //         }
    //       });

    //       return el.quantity == nonEmptyCount
    //     } else {
    //       return false
    //     }
    //   }
    // })
    let icon: any = null
    let message: any = null
    // // // debugger
    // if (typeof isLastScanFailed == 'undefined') {
    //   icon = <img src={scan_big} />
    //   message = 'Scan Any Item to Pick'
    // } else if (isAllPicked && !isOverPicked && !isCatchWeightItemOverPicked) {
    //   icon = <img src={scan_finish} />
    //   message = 'Success - Order Complete'
    // } else if (lastCheckItem) {
    //   if (!isNonCatchWeightItemScanned && isCatchWeightItemOverPicked) {
    //     icon = <Icon type='triangle-warning' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
    //     message = <><span className='warning'>Overpicked {lastCheckItem.variety}</span><br /> <span className='warning'>Remove Items</span></>
    //   } else if (isNonCatchWeightItemScanned && isOverPicked) {
    //     icon = <Icon type='triangle-warning' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
    //     message = <><span className='warning'>Overpicked {lastCheckItem.variety}</span><br /> <span className='warning'>Item Will Not Be Added</span></>
    //   } else {
    //     icon = <Icon type='circle-checked' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
    //     message = `${lastCheckItem.quantity == lastCheckItem.picked ? 'Completed' : 'Picked'} ${lastCheckItem.variety} ${lastCheckItem.lastScanWeight ? lastCheckItem.lastScanWeight + ' ' + lastCheckItem.inventoryUOM : ''}`
    //   }
    // } else if (lastCheckItem == null) {
    //   icon = <Icon type='triangle-warning' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
    //   message = <span className='warning'>Wrong barcode</span>
    // }

    if (currentWaringType === 0) {
      icon = <img src={scan_big} />
      message = 'Scan Any Item to Pick'
    } else if (currentWaringType === 1) {
      icon = <Icon type='triangle-warning' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
      message = <span className='warning'>Warning: Scanned Incorrect Lot for {currentWaringItem.variety} {currentWaringItem.modifiers} {currentWaringItem.extraOrigin}</span>
    } else if (currentWaringType === 2) {
      icon = <Icon type='triangle-warning' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
      message = <span className='warning'>Warning: Overpicked {currentWaringItem.variety} {currentWaringItem.modifiers} {currentWaringItem.extraOrigin}</span>
    } else if (currentWaringType === 3) {
      icon = <Icon type='triangle-warning' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
      message = <span className='warning'>Wrong barcode</span>
    } else if (currentWaringType === 4) {
      icon = <Icon type='triangle-warning' viewBox='0 0 56 56' width={56} height={56} style={{ fill: 'transparent' }} />
      message = <span className='warning'>Wrong Item {currentWaringItem.variety} {currentWaringItem.modifiers} {currentWaringItem.extraOrigin}</span>
    }

    return [icon, message]
  }

  onSaveEPickSheet = () => {
    this.setState({ saveButtonLoading: true })
    const allItems = _.flatten(Object.values(this.state.orderItemByProduct).map((v: any) => v.items))
    const wholesaleOrderItemList = allItems.map((v: any) => {
      let quantityDetailList = undefined
      if (v.quantityDetailList) {
        quantityDetailList = v.quantityDetailList.filter(u => (_.isNumber(u.id) || (_.isString(u.id) && !u.isDeleted))).map((i: any, index: number) => ({
          unitWeight: i.unitWeight,
          displayNumber: index + 1,
          isDeleted: i.isDeleted,
          id: _.isNumber(i.id) ? i.id : undefined
        }))
      }
      return {
        lotId: v.isAdded ? v.lotId: undefined,
        cost: v.isAdded ? v.cost: undefined,
        price: v.isAdded ? v.price: undefined,
        displayOrder: v.displayOrder ? v.displayOrder: undefined,
        UOM: v.isAdded ? v.UOM: undefined,
        pricingUOM: v.isAdded ? v.pricingUOM: undefined,
        ratio: v.isAdded ? v.ratio: undefined,
        lotAssignmentMethod: (v.isAdded || v.isUpdated) ? v.lotAssignmentMethod: undefined,
        enableLotOverflow: (v.isAdded || v.isUpdated) ? v.enableLotOverflow: undefined,
        manuallyAssignState: (v.isAdded || v.isUpdated) ? v.manuallyAssignState : undefined,
        wholesaleItemId: v.isAdded ? v.wholesaleItemId: undefined,
        wholesaleOrderItemId: v.wholesaleOrderItemId,
        picked: v.quantityDetailList ? v.quantityDetailList.length : v.picked,
        isUpdated: v.isUpdated,
        quantityDetailList
      }
    })

    const _this = this
    OrderService.instance.saveOrderEPickCatchWeight(this.props.order.wholesaleOrderId, { wholesaleOrderItemList }).subscribe({
      error(err) { checkError(err) },
      complete() {
        _this.setState({ saveButtonLoading: false })
        _this.props.onClose('showEPickSheetModal', false)
        _this.props.startUpdating()
        _this.props.getOrderItemsById(_this.props.order.wholesaleOrderId)
      }
    })
  }

  calculateTotalPicked = () => {
    const { orderItemByProduct } = this.state
    let total = 0
    Object.values(orderItemByProduct).forEach(v => {
      total += Number(this.getTotalPicked(v))
    })
    // const totalNoNCWPicked = _.sumBy(newItems, (el: any) => {
    //   if (judgeConstantRatio(el)) {
    //     return el.picked
    //   }
    // })
    // let totalCWPicked = 0
    // newItems.forEach(el => {
    //   if (!judgeConstantRatio(el)) {
    //     const catchWeights = cwItemValues[el.wholesaleOrderItemId]
    //     if (!catchWeights) return
    //     catchWeights.forEach((element: any) => {
    //       if (element.unitWeight && !isNaN(element.unitWeight)) {
    //         totalCWPicked++
    //       }
    //     });
    //   }
    // });
    return total
  }

  handleResetItem = () => {
    jQuery('#hidden-barcode-input').select()
    const scanHistory = this.state.scanHistory
    scanHistory.pop()
    this.setState({
      orderItemByProduct: _.last(scanHistory),
      currentWaringType: 0,
      currentWaringItem: null,
      isAddedItem: false,
    })
  }

  handleAddOrderItem = () => {
    jQuery('#hidden-barcode-input').select()
    const { currentWaringItem, orderItemByProduct, currentWaringType } = this.state
    if (currentWaringType === 1) {
      return this.setState({
        isAddedItem: true,
        orderItemByProduct: {
          ...orderItemByProduct,
          [currentWaringItem.displayOrderProduct]: {
            ...orderItemByProduct[currentWaringItem.displayOrderProduct],
            picked: Number(orderItemByProduct[currentWaringItem.displayOrderProduct].picked) + 1,
            isAdded: true,
            items: [...orderItemByProduct[currentWaringItem.displayOrderProduct].items.map(v => ({ ...v, isUpdated: true, lotAssignmentMethod: 3, enableLotOverflow: false, manuallyAssignState: true })), { ...currentWaringItem, lotAssignmentMethod: 3, enableLotOverflow: false, manuallyAssignState: true }]
          }
        }
      })
    }
    this.setState({
      isAddedItem: true,
      orderItemByProduct: {
        ...orderItemByProduct,
        [~~currentWaringItem.displayOrder]: {
          ...orderItemByProduct[~~currentWaringItem.displayOrder],
          picked: Number(orderItemByProduct[~~currentWaringItem.displayOrder].picked) + 1,
          items: orderItemByProduct[~~currentWaringItem.displayOrder].items.map((v: any) => {
            if (v.lotId === currentWaringItem.lotId) {
              return ({ ...v, isUpdated: true, picked: Number(v.picked) + 1 })
            }
            return v
          })
        }
      }
    })
  }

  handleUndoAddItem = () => {
    jQuery('#hidden-barcode-input').select()
    if (this.state.scanHistory.length === 1) return
    const scanHistory = this.state.scanHistory
    scanHistory.pop()
    this.setState({
      orderItemByProduct: _.last(scanHistory),
      currentWaringType: 0,
      currentWaringItem: null,
      isAddedItem: false,
    })
  }

  render() {
    const { sellerSetting, order } = this.props
    const { scanMode, newItems, currentWaringType, isAddedItem } = this.state
    const pickStatus = this.renderStatusIconAndMessage()
    const totalPicked = this.calculateTotalPicked()
    const toalQuantity = _.sumBy(newItems, (el) => {
      return Number(el.quantity)
    })
    return (
      <>
        <DialogSubContainer className='bordered'>
          <FlexDiv className='space-between'>
            <div style={{ maxWidth: '40%' }}>
              <Item>Sales Order</Item>
              <ItemValue>{order ? `${getOrderPrefix(sellerSetting, 'sales')}${order.wholesaleOrderId}` : ''}</ItemValue>
            </div>
            <div>
              <Item>Customer</Item>
              <ItemValue>{order && order.wholesaleClient ? order.wholesaleClient.clientCompany.companyName : ''}</ItemValue>
            </div>
            <Row style={{ width: 200 }}>
              <Col span={14}>
                <Item>Units Ordered</Item>
                <Item>{toalQuantity}</Item>
              </Col>
              <Col span={1}>
                <Item>/</Item>
                <Item></Item>
              </Col>
              <Col span={9}>
                <Item>Picked</Item>
                <Item>{totalPicked}</Item>
              </Col>
            </Row>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv style={{ padding: '8px' }}>
          <EPickStatus>
            {/* <div className='btn-switch'>
              <Item>Scan Mode</Item>
              <ThemeSwitch checked={scanMode} onChange={this.onChangeScanMode} />
            </div> */}
            <div className='hidden-input'>
              <input type='text' id='hidden-barcode-input' onChange={() => this.setState({ isTriggerWarning: true })} />
            </div>
            <div className='icon-body'>
              <div className='icon'>
                {pickStatus[0]}
              </div>
              <div className='description'>
                {pickStatus[1]}
                { (currentWaringType === 1 || currentWaringType === 2) && <p>
                  { !isAddedItem ?
                    <div style={{ marginTop: 10 }}>
                      <ThemeButton onClick={this.handleAddOrderItem} style={{ marginRight: 20 }}>Add to Order</ThemeButton>
                      <ThemeButton onClick={this.handleResetItem}>Do Not Add</ThemeButton>
                    </div> :
                    <div style={{ marginTop: 10 }}>
                      <span>{currentWaringType === 1 ? 'Warning: Incorrect Lot.' : 'Warning: Overpicked QTY'}</span>
                      <Button style={{ marginLeft: 20 }} onClick={this.handleUndoAddItem}>Undo Add?</Button>
                    </div>
                  }
                </p>}
              </div>
            </div>
          </EPickStatus>
          <hr />
          <div>
            <p onClick={this.handleAllCollapse} style={{ paddingLeft: 15, marginBottom: 0, cursor: 'pointer' }}>
              {this.state.collapseActiveKey.length ? <AntIcon type="shrink" /> : <AntIcon type="arrows-alt" />}
              <span style={{ fontSize: 13, marginLeft: 5 }}>{this.state.collapseActiveKey.length ? 'Collapse all' : 'Expand all'}</span>
            </p>
            {this.renderItems()}
          </div>
        </DialogBodyDiv>
        <DialogSubContainer style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <ThemeButton type="primary" onClick={this.onSaveEPickSheet} loading={this.state.saveButtonLoading}>
              Save
            </ThemeButton>
            <span style={{ marginLeft: 15 }} onClick={this.handleUndoAddItem}>Undo</span>
          </div>
        </DialogSubContainer>
      </>
    )
  }
}

export default EPickSheet

const AntdIconWrap = styled(AntIcon)`
  font-size: 20px !important;
`

const CollapseWrap = styled(Collapse)`
  border: none;
  .ant-collapse-item {
    border-bottom: none;
  }
  .ant-collapse-header {
    background: #fff;
  }
  .ant-collapse-content {
    border-top: none;
  }
  .ant-collapse-content-box {
    padding: 0 0 0 50px;
    background: #fff;
  }
  .collapseHeader {
    display: flex;
    align-items: center;
    p {
      margin-bottom: 0;
      display: flex;
      .picked{
        margin-right: 15px;
        min-width: 20px;
        text-align: right;
      }
      .quantity{
        margin: 0 6px 0 15px;
        text-align: left;
        min-width: 20px;
      }
    }
    .index {
      color: #828282;
      margin-right: 14px;
      font-weight: normal;
    }
    .lot {
      margin: 0 20px 0 25px;
      min-width: 130px;
    }
  }
`
