import * as React from 'react'
import { InputLabel, ThemeTextArea } from '~/modules/customers/customers.style'
import { OrderDetail, OrderItem } from '~/schema'
import { Description, DialogBodyDiv, DialogSubContainer, FlexDiv, Item, ValueLabel } from '../../styles'

type AddItemNoteModalProps = {
  ref: any
  currentOrder: OrderDetail
  record: OrderItem
  open: boolean
  orderItemByProduct: any
  updateOrderItemByProductId: Function
}

class AddItemNoteModal extends React.PureComponent<AddItemNoteModalProps> {
  ref = this.props.ref
  state = {
    note: ''
  }

  componentDidMount() {
    if (this.props.record) {
      this.setState({
        note: this.props.record.note
      })
    }

  }

  componentWillReceiveProps(nextProps: AddItemNoteModalProps) {
    if (!this.props.record && nextProps.record || !this.props.open && nextProps.open) {
      this.setState({
        note: nextProps.record.note
      })
    }
  }

  onTextChange = (evt: any) => {
    this.setState({ note: evt.target.value })
  }

  handleSave = (modalType: string) =>{
    const { note } = this.state
    const { currentOrder, orderItemByProduct, record, updateOrderItemByProductId } = this.props
    orderItemByProduct[record.displayOrderProduct]['note'] = note
    if (modalType === 'deleteItemNote') {
      orderItemByProduct[record.displayOrderProduct]['note'] = ''
    }
    updateOrderItemByProductId({
      orderId: currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod: record.lotAssignmentMethod,
        enableLotOverflow: record.enableLotOverflow,
        wholesaleItemId: record.itemId,
        picked: record.picked,
        quantity: record.quantity,
        isUpdateNote: true,
        note: modalType === 'deleteItemNote' ? '' : note,
      }
    })
  }


  render() {
    const { record } = this.props
    const { note } = this.state

    return (
      <div
        ref={this.ref}
        className={`item-note-modal-body ${this.props.open ? 'open' : 'hidden'}`}
      >
        <DialogSubContainer className='bordered'>
          <FlexDiv>
            <Item>
              <InputLabel>Item</InputLabel>
              <ValueLabel className='medium'>{record ? record.variety : ''}</ValueLabel>
            </Item>
            <Item>
              <InputLabel>Lot</InputLabel>
              <ValueLabel className='medium'>{record ? record.lotIds.join(', ') : ''}</ValueLabel>
            </Item>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <Description>This internal note will be printed on the pick sheet.</Description>
          <InputLabel>Internal Note</InputLabel>
          <ThemeTextArea
            rows={4}
            style={{ width: '100%' }}
            placeholder={'Note...'}
            value={note && note != 'null' ? note : ''}
            onChange={this.onTextChange}
          />
        </DialogBodyDiv>
      </div>
    )
  }
}

export default AddItemNoteModal
