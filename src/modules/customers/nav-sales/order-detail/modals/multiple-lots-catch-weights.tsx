import * as React from 'react'
import { withRouter } from 'react-router-dom'
import { Switch, Icon, Button } from 'antd'
import * as _ from 'lodash'
import { v4 as uuidv4 } from 'uuid'
import jQuery from 'jquery'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders'
import { DialogBodyDiv, DialogSubContainer, Flex, FlexDiv, Item, ValueLabel, VerticalPadding8 } from '../../styles'
import { InputLabel, ThemeInput, ThemeSpin } from '~/modules/customers/customers.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkWithJanaAlgorithm, NumericInput } from './picked-quanitty'
import { ratioQtyToInventoryQty, formatNumber } from '~/common/utils'

type MultipleLotCatchWeightsProps =  OrdersStateProps & OrdersDispatchProps & {
  record: any
  open: boolean
}

type MultipleLotCatchWeightsState = {
  scanMode: boolean
  catchWeights: any
  parsingLotOrderItemId: number
  parseFailedIndexes: any
  parseSuccessIndexes: any
  removedCatchWeight: any[]
  loading: boolean
}

class MultipleLotCatchWeights extends React.PureComponent<MultipleLotCatchWeightsProps, MultipleLotCatchWeightsState> {
  constructor(props: MultipleLotCatchWeightsProps) {
    super(props)
    this.state = {
      catchWeights: {},
      loading: false,
      parseFailedIndexes: {},
      parseSuccessIndexes: {},
      parsingLotOrderItemId: 0,
      removedCatchWeight: [],
      scanMode: localStorage.getItem('SCAN_MODE') === 'true',
    }
  }

  componentDidMount() {
    if (this.props.currentOrder && this.props.record) {
      this.setState({ loading: true })
      const _this = this
      OrderService.instance.getOrderItemCatchWeights({ 
        orderId: this.props.currentOrder.wholesaleOrderId,
        productId: this.props.record.itemId,
        displayOrder: this.props.record.displayOrderProduct,
      }).subscribe({
        next(resp: any) { 
          const catchWeights = _.groupBy(resp.body.data, 'wholesaleOrderItemId')
          let parseFailedIndexes: any = {}
          Object.keys(catchWeights).map((wholesaleOrderItemId: any) => {
            const fails = localStorage.getItem(`PARSING_FAILED_${wholesaleOrderItemId.toString()}`);
            if (fails) {
              parseFailedIndexes[wholesaleOrderItemId] = JSON.parse(fails)
            } else {
              parseFailedIndexes[wholesaleOrderItemId] = []
            }
          })
          console.log(parseFailedIndexes)
          _this.setState({
            catchWeights,
            parseFailedIndexes,
          })
        },
        complete() { 
          _this.setState({ loading: false })
          _this.setFocusOnInput()
        }
      })
    }
  }

  onChangeScanMode = (scanMode: boolean) => {
    localStorage.setItem('SCAN_MODE', scanMode.toString())
    this.setState({
      scanMode
    }, () => {
      setTimeout(() => {
        this.initEventHandler()
      }, 10)
    })
  }

  setFocusOnInput = () => {
    setTimeout(() => {
      const fields = jQuery('.quantity-item').find('.ant-input, .ant-btn')
      if (fields.length) {
        jQuery(fields[0]).trigger('focus')
      }
      this.initEventHandler();
    }, 500)
  }

  initEventHandler = () => {
    jQuery('.quantity-item input').unbind()

    // Move focus to next field by carriage return
    jQuery('.quantity-item input').unbind().bind('click', (e: any) => {
      // When user clicks on field, full contents of field should be highlighted, so when user starts typing, full content of field is replaced
      jQuery(e.target).select();
    }).bind('keypress', (e: any) => {
      if (e.keyCode === 13 || e.keyCode === 9) {
        this.handleParsing(e)
      }
    }).bind('focus', (e: any) => {
      jQuery(e.target).trigger('select')
    }).bind('blur', (e: any) => {
      console.log(e);
      this.setState({parsingLotOrderItemId: 0})
      this.handleParsing(e)
    })
  }

  handleParsing = (e: any) => {
    // Only that textbox is parsed when press enter key or tab or blur
    const { record, settingCompanyName } = this.props
    const lotItems = record.items
    const { catchWeights, scanMode, parsingLotOrderItemId } = this.state
    const items = catchWeights[parsingLotOrderItemId]
    const parsingLotItem = lotItems.find((el: any)=> el.wholesaleOrderItemId == parsingLotOrderItemId)
    const currentEl = jQuery(e.currentTarget).parent().parent()
    let index = jQuery(`.quantites-wrapper-${parsingLotOrderItemId} .quantity-item`).index(currentEl[0])
    if (index < 0 || index >= items.length) return
    let isFailed: boolean = false
    let janaParsingResult: any[] = []
    const unitWeight = items[index].unitWeight;
    let successIndexes =  [...this.state.parseSuccessIndexes[parsingLotOrderItemId]]
    if (scanMode && unitWeight && successIndexes.indexOf(index) == -1) {
      if (settingCompanyName == 'Winner\'s Meats') {  // When Winner's Meats
        if (unitWeight.length !== 12) {
          isFailed = true
        } else {
          // digits 2 ~ 6
          let sku = unitWeight.substr(1, 5)
          if (record.SKU && record.SKU.indexOf(sku) === 0) {
            // 7 to 11, real value
          } else {
            isFailed = true
          }
        }
      } else {          //When not a Winner's Meats
        janaParsingResult = checkWithJanaAlgorithm(unitWeight, parsingLotItem)
        isFailed = janaParsingResult[0]
      }

      let parsingItemQuantities = [...this.state.catchWeights[parsingLotOrderItemId] ? this.state.catchWeights[parsingLotOrderItemId] : []]
      let parseFailedIndexes = [...this.state.parseFailedIndexes[parsingLotOrderItemId] ? this.state.parseFailedIndexes[parsingLotOrderItemId] : []]
      let val = unitWeight.substr(6, 3) + '.' + unitWeight.substr(9, 2)
      if (janaParsingResult.length) {
        parsingItemQuantities[index].unitWeight = isNaN(parseFloat(janaParsingResult[1])) ? 0 : parseFloat(janaParsingResult[1])
      } else {
        parsingItemQuantities[index].unitWeight = isNaN(parseFloat(val)) ? 0 : parseFloat(val)
      }

      if (isFailed === true) {
        // When not able to parse, it should stay in the field, but clear the text value
        if (parseFailedIndexes.indexOf(index) < 0) {
          parseFailedIndexes.push(index);
        }
        const foundSuccessIndex = successIndexes.indexOf(index)
        if (foundSuccessIndex >= 0) {
          successIndexes.splice(foundSuccessIndex, 1)
        }
        this.setState({
          catchWeights: {
            ...this.state.catchWeights,
            [parsingLotOrderItemId]: parsingItemQuantities,
          },
          parseFailedIndexes: {
            ...this.state.parseFailedIndexes,
            [parsingLotOrderItemId]: parseFailedIndexes,
          },
          parseSuccessIndexes: {
            ...this.state.parseSuccessIndexes,
            [parsingLotOrderItemId]: successIndexes
          }
        })
      } else {
        const foundSuccessIndex = successIndexes.indexOf(index)
        if (foundSuccessIndex == -1) {
          successIndexes.push(index)
        }
        const foundIndex = parseFailedIndexes.indexOf(index)
        if (foundIndex >= 0) {
          parseFailedIndexes.splice(foundIndex, 1);
        }

        this.setState({
          catchWeights: {
            ...this.state.catchWeights,
            [parsingLotOrderItemId]: parsingItemQuantities,
          },
          parseFailedIndexes: {
            ...this.state.parseFailedIndexes,
            [parsingLotOrderItemId]: parseFailedIndexes,
          },
          parseSuccessIndexes: {
            ...this.state.parseSuccessIndexes,
            [parsingLotOrderItemId]: successIndexes
          }
        })
      }
      if (index + 1 === jQuery(`.quantites-wrapper-${parsingLotOrderItemId} .quantity-item`).length - 1) {
        index = -1;
      }
      jQuery(jQuery(`.quantites-wrapper-${parsingLotOrderItemId} .quantity-item`)[index + 1]).find('input').trigger('focus');
    }
  }

  handleAddCatchWeight = (wholesaleOrderItemId: number) => {
    this.setState({
      catchWeights: {
        ...this.state.catchWeights,
        [wholesaleOrderItemId]: [
          ...this.state.catchWeights[wholesaleOrderItemId],
          {
            id: `n${uuidv4()}`,
            unitWeight: 0,
            displayNumber: this.state.catchWeights[wholesaleOrderItemId].length + 1,
            wholesaleOrderItemId
          }
        ]
      }
    }, () => {
      this.setFocusOnInput()
    })
  }

  handleRemoveCatchWeight = (id: any, wholesaleOrderItemId: number) => {
    this.setState({
      removedCatchWeight: [...this.state.removedCatchWeight, _.flattenDeep(Object.values(this.state.catchWeights)).find((v: any) => v.id == id)],
      catchWeights: {
        ...this.state.catchWeights,
        [wholesaleOrderItemId]: this.state.catchWeights[wholesaleOrderItemId].filter((v: any) => v.id !== id).map((v: any, index: number) => ({ ...v, displayNumber: index + 1 }))
      }
    })
  }

  handleLotItemQuantityChange = (value: any, index: number, id: number, wholesaleOrderItemId: number) => {
    console.log('value change', wholesaleOrderItemId)
    let successIndexes: any[] = this.state.parseSuccessIndexes[wholesaleOrderItemId] ? this.state.parseSuccessIndexes[wholesaleOrderItemId] : []
    let failedIndexes = this.state.parseFailedIndexes[wholesaleOrderItemId] ? this.state.parseFailedIndexes[wholesaleOrderItemId] : []
    

    if (value !== '') {
      const successIndex = successIndexes.indexOf(index)
      if (successIndex > -1) {
        successIndexes.splice(successIndex, 1);
      }

      const failedIndex = failedIndexes.indexOf(index)
      if (failedIndex > -1) {
        failedIndexes.splice(failedIndex, 1);
      }
    }

    this.setState({
      catchWeights: {
        ...this.state.catchWeights,
        [wholesaleOrderItemId]: this.state.catchWeights[wholesaleOrderItemId].map((v: any) => {
          if (v.id === id) return { ...v, unitWeight: value }
          return v
        })
      },
      parseSuccessIndexes: {
        ...this.state.parseSuccessIndexes,
        [wholesaleOrderItemId]: successIndexes
      },
      parseFailedIndexes: {
        ...this.state.parseFailedIndexes,
        [wholesaleOrderItemId]: failedIndexes
      },
      parsingLotOrderItemId: wholesaleOrderItemId
    })
  }

  handleSave = () => {
    const _this = this
    const { currentOrder, record, updateOrderItemByProductId } = this.props
    if (!currentOrder || !record) return
    const quantityDetailList = [
      ..._.flatten(Object.values(this.state.catchWeights)).map((v: any) => String(v.id).startsWith('n') ? ({ ...v, id: undefined }) : v), 
      ...this.state.removedCatchWeight.filter((v: any) => !String(v.id).startsWith('n')).map((v: any) => ({ ...v, isDeleted: true }))
    ]
    OrderService.instance.updateOrderItemCatchWeights({ 
      orderId: currentOrder.wholesaleOrderId, 
      productId: this.props.record.itemId, 
      displayOrder: this.props.record.displayOrderProduct,
      data: { quantityDetailList }
    }).subscribe({
      next(resp) { 
        const originCatchWeightQty = _.flatten(Object.values(_this.state.catchWeights)).map((v: any) => v.unitWeight).reduce((n, p) => n + p, 0)
        // const constantRaio = judgeConstantRatio(_this.props.record)
        const catchWeightQty = ratioQtyToInventoryQty(record.pricingUOM, originCatchWeightQty, record, 12)

        _this.props.updateOrderItemCatchWeight({ displayOrderProduct: record.displayOrderProduct, catchWeightQty, picked: quantityDetailList.filter(v => !v.isDeleted).length })
      },
      complete() {
        updateOrderItemByProductId({
          orderId: currentOrder.wholesaleOrderId,
          productId: record.itemId,
          displayOrderProduct: record.displayOrderProduct,
          data: {
            lotAssignmentMethod: record.lotAssignmentMethod,
            enableLotOverflow: record.enableLotOverflow,
            wholesaleItemId: record.itemId,
            picked: quantityDetailList.filter(v => !v.isDeleted).length,
            quantity: record.quantity,
            isUpdatePicked: true,
          }
        })
        
        Object.keys(_this.state.parseFailedIndexes).forEach((orderItemId: any) => {
          const fails = _this.state.parseFailedIndexes[orderItemId]
          if (fails.length > 0) {
            localStorage.setItem(`PARSING_FAILED_${orderItemId.toString()}`, JSON.stringify(fails))
          } else {
            localStorage.removeItem(`PARSING_FAILED_${orderItemId.toString()}`)
          }
        })
      }
    })
  
  }

  render () {
    const { record, orderItems } = this.props
    const { scanMode, loading, catchWeights, parseFailedIndexes } = this.state
    let totalWeight = 0
    if (_.size(this.state.catchWeights)) {
      totalWeight = _.flatten(Object.values(this.state.catchWeights)).filter((v: any)=> !isNaN(v.unitWeight)).map((v: any) => v.unitWeight).reduce((n, p) => n + p, 0)
    }
    return (
      <>
        <DialogSubContainer className="bordered">
          <FlexDiv className="space-between">
            <FlexDiv>
              <Item>
                <InputLabel>Item</InputLabel>
                <ValueLabel className="medium">{record ? record.variety : ''}</ValueLabel>
              </Item>
              <Item>
                <InputLabel>Pricing UOM</InputLabel>
                <ValueLabel className="medium">
                  {record ? (record.pricingUOM ? record.pricingUOM : record.inventoryUOM) : ''}
                </ValueLabel>
              </Item>
            </FlexDiv>
            <Item>
              <InputLabel className="theme">Scan Mode</InputLabel>
              <Switch checked={scanMode} onChange={this.onChangeScanMode} />
            </Item>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <InputLabel style={VerticalPadding8}>
            Total Billable Quantity: {formatNumber(totalWeight, 2)} {record ? record.baseUOM : ''}
          </InputLabel>
            <ThemeSpin spinning={loading}>
              <div>
                {
                  Object.keys(catchWeights).map((orderItemId: any) => {
                    const currentOrderItem = orderItems.find(orderItem => orderItem.wholesaleOrderItemId == orderItemId)
                    return (
                      <LotItem 
                        key={orderItemId} 
                        orderItemId={orderItemId}
                        handleAddCatchWeight={() => this.handleAddCatchWeight(orderItemId)} 
                        handleRemoveCatchWeight={(id: any) => this.handleRemoveCatchWeight(id, orderItemId)}
                        handleChange={this.handleLotItemQuantityChange} 
                        record={record} 
                        scanMode={scanMode}
                        lotId={_.get(currentOrderItem, 'lotId', '')} 
                        items={catchWeights[orderItemId]}
                        parseFailedIndexes={parseFailedIndexes[orderItemId] ? parseFailedIndexes[orderItemId] : []}
                      />
                    )
                  })
                }
              </div>
            </ThemeSpin>
        </DialogBodyDiv>
      </>
    )
  }
}

const LotItem: React.FC<any> = ({ 
  orderItemId,
  items,
  record,
  lotId,
  scanMode,
  handleChange,
  handleAddCatchWeight,
  handleRemoveCatchWeight,
  parseFailedIndexes
}) => {
  console.log('check parseFailed indexes', parseFailedIndexes)
  return (
    <div style={{ marginBottom: 20 }}>
      Lot {lotId}
      <Flex className={`quantites-wrapper quantites-wrapper-${orderItemId}`} style={{ flexWrap: 'wrap', alignItems: 'flex-end', minHeight: 64 }}>
        {_.sortBy(items, 'displayNumber').map((el, index) => {
          const warning = parseFailedIndexes.indexOf(index) >= 0
          return (
            <Item className="quantity-item" key={index}>
              <Flex className="v-center space-between">
                {warning ? (
                  <InputLabel className="warning">{index + 1} WRONG SKU!</InputLabel>
                ) : (
                  <InputLabel>
                    {index == 0 && record ? record.UOM : ''} {index + 1}
                  </InputLabel>
                )}
                <Icon type="close-circle" className="close-quantity" onClick={() => handleRemoveCatchWeight(el.id)} />
              </Flex>
              <div>
                {scanMode ? (
                  <ThemeInput className={warning ? "warning" : ''} value={el.unitWeight} onChange={(e: any) => handleChange(e.target.value || 0, index, el.id, orderItemId)} />
                ) : (
                  <NumericInput className={warning ? "warning" : ''} value={el.unitWeight} onChange={(value: string) => handleChange(value || 0, index, el.id, orderItemId)} />
                )}
              </div>
            </Item>
          )
        })}
        <Item className="quantity-item ">
          <Button onClick={handleAddCatchWeight}>Add {_.get(record, 'UOM', '')}</Button>
        </Item>
      </Flex>
    </div>
  )
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withRouter(connect(OrdersModule)(mapStateToProps, null, null, { forwardRef: true })(MultipleLotCatchWeights))
