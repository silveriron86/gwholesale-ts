import * as React from 'react'
import { Button, Icon, Switch } from 'antd'
import { connect } from 'redux-epics-decorator'
import { InputLabel, ThemeInput } from '~/modules/customers/customers.style'
import { OrdersDispatchProps, OrdersStateProps, OrdersModule } from '~/modules/orders'
import { OrderItem } from '~/schema'
import { GlobalState } from '~/store/reducer'
import { DialogBodyDiv, DialogSubContainer, Flex, FlexDiv, Item, ValueLabel, VerticalPadding8 } from '../../styles'
import jQuery from 'jquery'
import { formatNumber } from '~/common/utils'
import { withRouter } from 'react-router-dom'
import _, { cloneDeep, cloneDeepWith } from 'lodash'

type PickedQuantityProps = OrdersStateProps &
  OrdersDispatchProps & {
    record: OrderItem
    isWO?: boolean
    updateSaleOrderItemForWO?: Function
    open: boolean
    catchWeights: any
    parsingLotOrderItemId: number
    scanMode: boolean
  }

export class NumericInput extends React.Component<{
  className: string
  value: any
  onChange: Function
}> {
  onChange = (e) => {
    const { value } = e.target
    const reg = /^-?\d*(\.\d*)?$/
    if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      this.props.onChange(value)
    }
  }

  // '.' at the end or only '-' in the input box.
  onBlur = () => {
    const { value, onBlur, onChange } = this.props
    let valueTemp = value
    if ((value.length > 0 && value.charAt(value.length - 1) === '.') || value === '-') {
      valueTemp = value.slice(0, -1)
    }
    if (value !== valueTemp) {
      onChange(valueTemp.replace(/0*(\d+)/, '$1'))
    }
    if (onBlur) {
      onBlur()
    }
  }

  render() {
    return <ThemeInput {...this.props} onChange={this.onChange} onBlur={this.onBlur} maxLength={25} />
  }
}

export const checkWithJanaAlgorithm = (unitWeight: string, record: any) => {
  let supplierSkus = record && record.supplierSkus ? record.supplierSkus.split(',') : []
  supplierSkus = supplierSkus.map((el: any) => { return el.replaceAll(' ', '').trim() })
  const recordLabelSerial = record.labelSerial
  //If digits 5 to 7  is not equal to "WSW" then use https://trello.com/c/mhP4jIiC/2218-gs128-parsing-algorithm-slice-1
  const isWSW = unitWeight.substr(4, 3) === 'WSW'
  let isFailed = false
  let unitValue = '0'
  if (!isWSW) {
    return parseByGS128(unitWeight, supplierSkus)
  } else {
    const labelSerial = unitWeight.substr(10, 5)
    if (recordLabelSerial != labelSerial) {
      isFailed = true
    }
    const realValueStr = unitWeight.substr(20, 6)
    unitValue = realValueStr.substr(0, realValueStr.length - 2) + '.' + realValueStr.substr(-2)
  }

  return [isFailed, unitValue]
}

export const parseByGS128 = (unitWeight: string, supplierSkus: string[]) => {
  let isFailed = false
  let unitValue = '0'
  //GS 128 parsing algorithm
  const prefix = unitWeight.substr(0, 2)
  if (prefix != '01' && prefix != '02') {
    isFailed = true
  }

  if (unitWeight.length > 17) {
    //17th digit
    const supplierSku = unitWeight.substr(10, 5)
    if (supplierSkus.indexOf(supplierSku) == -1) {
      isFailed = true
    }
    const flag = unitWeight[16]
    let suffix = ''
    if (flag != '1') {
      suffix = unitWeight.substr(16)
    } else if (unitWeight.length > 24) {
      suffix = unitWeight.substr(24)
    }
    if (!suffix || (suffix.indexOf('320') != 0 && suffix.indexOf('310') != 0)) {
      isFailed = true
    }

    if (suffix.length > 3) {
      const floatPoint = parseInt(suffix[3])
      if (isNaN(floatPoint)) {
        isFailed = true
      }

      const realValueStr = suffix.substr(4, 6)
      const strValue = realValueStr.substr(0, realValueStr.length - floatPoint) + '.' + realValueStr.substr(-floatPoint)
      if (suffix.indexOf('320') == 0) {
        unitValue = formatNumber(parseFloat(strValue), 2)
      } else if (suffix.indexOf('310') == 0) {
        unitValue = formatNumber(parseFloat(strValue) * 2.20483, 2)
      }
    }
  }
  return [isFailed, unitValue]
}


class PickedQuantity extends React.PureComponent<PickedQuantityProps> {
  constructor(props: PickedQuantityProps) {
    super(props)

    let parsedFailedIndexes = []
    const parsedFailed = localStorage.getItem(`PARSING_FAILED_${props.record.wholesaleOrderItemId.toString()}`);
    if (parsedFailed) {
      parsedFailedIndexes = JSON.parse(parsedFailed)
    }

    this.state = {
      newValue: '',
      items: [],
      loaded: false,
      firstOpen: false,
      ids: [],
      scanMode: localStorage.getItem('SCAN_MODE') === 'true',
      parseSuccessIndexes: [],
      parsedFailedIndexes, //  Index of the catch weight that parse is failed in scann mode
      catchWeights: {},
      removeCatchWeight: [],
      loading: false,
    }
  }

  componentDidMount() {
    if (this.props.record) {
      this.props.resetLoading()
      this.getQuantityCatchWeight(this.props.record.wholesaleOrderItemId)
    }
  }

  initEventHandler = () => {
    jQuery('.quantity-item input').unbind()

    // Move focus to next field by carriage return
    jQuery('.quantity-item input').unbind().bind('click', (e: any) => {
      // When user clicks on field, full contents of field should be highlighted, so when user starts typing, full content of field is replaced
      jQuery(e.target).select();
    }).bind('keypress', (e: any) => {
      if (e.keyCode === 13 || e.keyCode === 9) {
        this.handleParsing(e)
      }
    }).bind('focus', (e: any) => {
      jQuery(e.target).trigger('select')
    }).bind('blur', (e: any) => {
      console.log(e);
      this.setState({parsingLotOrderItemId: 0})
      this.handleParsing(e)
    })
  }

  handleParsing = (e: any) => {
    // Only that textbox is parsed when press enter key or tab or blur
    const currentEl = jQuery(e.currentTarget).parent().parent()
    let index = jQuery('.quantites-wrapper .quantity-item').index(currentEl[0])

    const { record, settingCompanyName } = this.props
    const { items, scanMode, parseSuccessIndexes } = this.state
    let isFailed: boolean = false
    let janaParsingResult: any[] = []
    const unitWeight = items[index].unitWeight;
    if (scanMode && unitWeight && parseSuccessIndexes.indexOf(index) == -1) {
      if (settingCompanyName == 'Winner\'s Meats') {  // When Winner's Meats
        if (unitWeight.length !== 12) {
          isFailed = true
        } else {
          // digits 2 ~ 6
          let sku = unitWeight.substr(1, 5)
          if (record.SKU && record.SKU.indexOf(sku) === 0) {
            // 7 to 11, real value
          } else {
            isFailed = true
          }
        }
      } else {          //When not a Winner's Meats
        janaParsingResult = checkWithJanaAlgorithm(unitWeight, record)
        isFailed = janaParsingResult[0]
      }

      let newItems = cloneDeep(items)
      let parsedFailedIndexes = [...this.state.parsedFailedIndexes]
      let successIndexes = [...parseSuccessIndexes]
      let val = unitWeight.substr(6, 3) + '.' + unitWeight.substr(9, 2)
      if (janaParsingResult.length) {
        newItems[index].unitWeight = isNaN(parseFloat(janaParsingResult[1])) ? 0 : parseFloat(janaParsingResult[1])
      } else {
        newItems[index].unitWeight = isNaN(parseFloat(val)) ? 0 : parseFloat(val)
      }

      const foundSuccessIndex = parseSuccessIndexes.indexOf(index)
      if (foundSuccessIndex == -1) {
        successIndexes.push(index)
      }
      if (isFailed === true) {
        // When not able to parse, it should stay in the field, but clear the text value
        if (parsedFailedIndexes.indexOf(index) < 0) {
          parsedFailedIndexes.push(index);
        }
        // const foundSuccessIndex = successIndexes.indexOf(index)
        // if (foundSuccessIndex > -1) {
        //   successIndexes.splice(foundSuccessIndex, 1)
        // }
        this.setState({
          items: newItems,
          parsedFailedIndexes,
          parseSuccessIndexes: successIndexes
        })
      } else {
        const foundIndex = parsedFailedIndexes.indexOf(index)
        if (foundIndex >= 0) {
          parsedFailedIndexes.splice(foundIndex, 1);
        }

        this.setState({
          items: newItems,
          parsedFailedIndexes,
          parseSuccessIndexes: successIndexes
        })
      }
      if (index + 1 === jQuery('.quantites-wrapper .quantity-item').length - 1) {
        index = -1;
      }
      jQuery(jQuery('.quantites-wrapper .quantity-item')[index + 1]).find('input').focus();
    }
  }

  componentWillReceiveProps(nextProps: PickedQuantityProps) {
    if (!this.props.open && nextProps.open) {
      let parsedFailedIndexes = []
      const parsedFailed = localStorage.getItem(`PARSING_FAILED_${nextProps.record.wholesaleOrderItemId.toString()}`);
      if (parsedFailed) {
        parsedFailedIndexes = JSON.parse(parsedFailed)
      }

      // new modal
      this.setState({
        newValue: '',
        items: [],
        loaded: false,
        firstOpen: false,
        ids: [],
        parsedFailedIndexes,
        scanMode: localStorage.getItem('SCAN_MODE') === 'true'
      })
      console.log('nextprops catch', nextProps.catchWeights)
      if (typeof nextProps.catchWeights[nextProps.record.wholesaleOrderItemId] === 'undefined') {
        this.props.resetLoading()
        this.getQuantityCatchWeight(nextProps.record.wholesaleOrderItemId)
      } else {
        let newItems: any[] = []
        const catchWeights = nextProps.catchWeights[nextProps.record.wholesaleOrderItemId]
        if (catchWeights.length > 0) {
          newItems = JSON.parse(JSON.stringify(catchWeights))
        } else {
          if (nextProps.record && nextProps.record.quantity > 0) {
            for (let i = 0; i < nextProps.record.quantity; i++) {
              newItems.push({
                unitWeight: 0,
              })
            }
          }
        }
        let parseSuccessIndexes: number[] = []
        newItems.forEach((el, index) => {
          parseSuccessIndexes.push(index)
        });
        this.setState({ items: newItems, parseSuccessIndexes })
      }
    }

    if (!nextProps.loading && !this.state.firstOpen) {
      this.setFocusOnInput()
      this.setState({ firstOpen: true })
    }

    if (this.props.loading !== nextProps.loading && nextProps.loading === false) {
      if (this.state.loaded === false) {
        let ids: any[] = []
        nextProps.catchWeight.forEach((v, i) => {
          ids.push(v.id)
        })

        let newItems: any[] = []
        if (nextProps.catchWeight && nextProps.catchWeight.length > 0) {
          newItems = JSON.parse(JSON.stringify(nextProps.catchWeight))
        } else {
          if (nextProps.record && nextProps.record.quantity > 0) {
            for (let i = 0; i < nextProps.record.quantity; i++) {
              newItems.push({
                unitWeight: 0,
              })
            }
          }
        }
        let parseSuccessIndexes: number[] = []
        newItems.forEach((el, index) => {
          parseSuccessIndexes.push(index)
        });
        this.setState({
          items: newItems,
          loaded: true,
          ids,
          parseSuccessIndexes
        })
      }
    }
    if (JSON.stringify(nextProps.catchWeight) !== JSON.stringify(this.props.catchWeight)) {
      if (nextProps.catchWeight) {
        this.setState({ items: nextProps.catchWeight ? JSON.parse(JSON.stringify(nextProps.catchWeight)) : null })
      }
      if (this.props.catchWeight.length == 0 && nextProps.catchWeight.length > 0) {
        this.setFocusOnInput()
      }
      const orderItem: any = { ...nextProps.record, ...{ picked: nextProps.catchWeight.length } }
      if (orderItem.wholesaleOrderItemId) {
        let totalWeight = 0
        const filled = nextProps.catchWeight.length
        if (filled > 0) {
          nextProps.catchWeight.forEach((item: any) => {
            if (item.unitWeight !== '') {
              totalWeight += parseFloat(item.unitWeight)
            }
          })
        }
      }
    }
  }

  setFocusOnInput = () => {
    setTimeout(() => {
      const fields = jQuery('.quantity-item').find('.ant-input, .ant-btn')
      if (fields.length) {
        jQuery(fields[0]).trigger('focus')
      }
      this.initEventHandler();
    }, 500)
  }

  getQuantityCatchWeight = (orderItemId: number) => {
    this.props.setOrderItemId(parseInt(orderItemId, 10))
    this.props.getQuantityCatchWeight(orderItemId)
  }

  onChangeUnitWeight = (index: number, value: number) => {
    let parsedFailedIndexes = [...this.state.parsedFailedIndexes]
    let parseSuccessIndexes = [...this.state.parseSuccessIndexes]
    if (value !== '') {
      const foundIndex = parsedFailedIndexes.indexOf(index)
      if (foundIndex >= 0) {
        parsedFailedIndexes.splice(foundIndex, 1);
      }
    }
    let updatedItems = [...this.state.items]

    updatedItems.forEach((el: any, idx: number) => {
      if (idx === index) {
        el.unitWeight = value
        const findIndex = parseSuccessIndexes.indexOf(index)
        if (findIndex > -1) {
          parseSuccessIndexes.splice(findIndex, 1);
        }
      }
    })
    this.setState({ items: updatedItems, parsedFailedIndexes, parseSuccessIndexes })
  }

  updateQuantity = (id: number, evt: any) => {
    const { catchWeight } = this.props
    const found = catchWeight.find((c) => c.id === id)
    const newValue = Number.parseFloat(evt.target.value)
    if (id && !isNaN(evt.target.value) && newValue >= 0 && Number.parseFloat(found.unitWeight) !== newValue) {
      const data = {
        qtyDetailId: id,
        orderItemId: this.props.record.wholesaleOrderItemId,
        unitWeight: newValue,
      }
      this.props.resetLoading()
      this.props.setOrderItemId(this.props.record.wholesaleOrderItemId)
      this.props.updateQuantityCatchWeight(data)
    }
  }

  deleteQuantity = (index: number, evt: any) => {
    // const item = this.state.items[index]
    // if (item && item.id) {
    //   this.props.resetLoading()
    //   this.props.deleteQuantityCatchWeight({
    //     orderItemId: this.props.record.wholesaleOrderItemId,
    //     qtyDetailId: item.id,
    //   })
    // }
    const updateItems = [...this.state.items]
    updateItems.splice(index, 1)

    let parsedFailedIndexes = [...this.state.parsedFailedIndexes]
    let parseSuccessIndexes = [...this.state.parseSuccessIndexes]
    const found = parsedFailedIndexes.indexOf(index)
    if (found >= 0) {
      parsedFailedIndexes.splice(found, 1)
    }
    const foundSuccesIndex = parseSuccessIndexes.indexOf(index)
    if (foundSuccesIndex >= 0) {
      parseSuccessIndexes.splice(foundSuccesIndex, 1)
    }
    if (parsedFailedIndexes.length > 0) {
      parsedFailedIndexes.forEach((failedIndex, i) => {
        if (failedIndex > index) {
          parsedFailedIndexes[i] = failedIndex - 1;
        }
      })
    }
    this.setState({ items: updateItems, parsedFailedIndexes, parseSuccessIndexes })
  }

  renderQuantitiesField = () => {
    const { items, scanMode, parsedFailedIndexes } = this.state
    const { record } = this.props
    let result: any[] = []
    items.forEach((el: any, index: number) => {
      const warning = /* scanMode && */parsedFailedIndexes.indexOf(index) >= 0
      result.push(
        <Item className="quantity-item" key={index}>
          <Flex className="v-center space-between">
            {warning ? (
              <InputLabel className="warning">{index + 1} WRONG SKU!</InputLabel>
            ) : (
              <InputLabel>
                {index == 0 && record ? record.UOM : ''} {index + 1}
              </InputLabel>
            )}
            <Icon type="close-circle" className="close-quantity" onClick={this.deleteQuantity.bind(this, index)} />
          </Flex>
          <div>
            {scanMode ? (
              <ThemeInput className={warning ? "warning" : ''} value={el.unitWeight} onChange={(e: any) => this.onChangeUnitWeight(index, e.target.value)} />
            ) : (
              <NumericInput className={warning ? "warning" : ''} value={el.unitWeight} onChange={this.onChangeUnitWeight.bind(this, index)} />
            )}
          </div>
        </Item>,
      )
    })
    return result
  }

  handleAdd = () => {
    const { items } = this.state
    let updateItems: any[] = [...items]
    // this.props.resetLoading()
    let newItem: any = {
      unitWeight: 0,
    }
    updateItems.push({ ...newItem })
    this.setState({ items: updateItems }, () => {
      this.setFocusOnInput()
    })
    /*
    const orderItemId = this.props.record.wholesaleOrderItemId
    newItem = { ...newItem, orderItemId }
    this.props.addQuantityCatchWeight(newItem)
    */
  }

  handleSave = () => {
    const { items, scanMode, parsedFailedIndexes } = this.state
    const { record } = this.props

    const itemList = cloneDeepWith(items).map((item: any, index: number) => {
      // if ((item.unitWeight === '') || (scanMode && parsedFailedIndexes.indexOf(index) >= 0)) {
      if ((item.unitWeight === '')) {
        item.unitWeight = 0
      }
      return item
    })

    this.props.setOrderItemId(record.wholesaleOrderItemId)
    this.props.addQuantityCatchWeightByList({
      orderItemId: record.wholesaleOrderItemId,
      data: { itemList },
    })

    if (parsedFailedIndexes.length > 0) {
      localStorage.setItem(`PARSING_FAILED_${record.wholesaleOrderItemId.toString()}`, JSON.stringify(parsedFailedIndexes))
    } else {
      localStorage.removeItem(`PARSING_FAILED_${record.wholesaleOrderItemId.toString()}`)
    }

    let totalWeight = 0
    if (items && items.length > 0) {
      items.forEach((item: any) => {
        if (item.unitWeight !== '') {
          totalWeight += parseFloat(item.unitWeight)
        }
      })
    }
    return {
      picked: items.length,
      catchWeightQty: totalWeight
    }
  }

  onChangeScanMode = (scanMode: boolean) => {
    localStorage.setItem('SCAN_MODE', scanMode.toString())
    this.setState({
      scanMode
    }, () => {
      setTimeout(() => {
        this.initEventHandler()
      }, 10)
    })
  }

  render() {
    const { items, scanMode } = this.state
    const { record } = this.props
    let totalWeight = 0
    if (items && items.length > 0) {
      items.forEach((item: any) => {
        if (item.unitWeight !== '') {
          totalWeight += parseFloat(item.unitWeight)
        }
      })
    }
    
    return (
      <>
        <DialogSubContainer className="bordered">
          <FlexDiv className="space-between">
            <FlexDiv>
              <Item>
                <InputLabel>Item</InputLabel>
                <ValueLabel className="medium">{record ? record.variety : ''}</ValueLabel>
              </Item>
              <Item>
                <InputLabel>Lot</InputLabel>
                <ValueLabel className="medium">{record ? record.items[0].lotId : ''}</ValueLabel>
              </Item>
              <Item>
                <InputLabel>Pricing UOM</InputLabel>
                <ValueLabel className="medium">
                  {record ? (record.pricingUOM ? record.pricingUOM : record.inventoryUOM) : ''}
                </ValueLabel>
              </Item>
            </FlexDiv>
            <Item>
              <InputLabel className="theme">Scan Mode</InputLabel>
              <Switch checked={scanMode} onChange={this.onChangeScanMode} />
            </Item>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <InputLabel style={VerticalPadding8}>
            Total Billable Quantity: {totalWeight} {record ? record.baseUOM : ''}
          </InputLabel>
          <Flex className="quantites-wrapper" style={{ flexWrap: 'wrap', alignItems: 'flex-end', minHeight: 64 }}>
            {this.renderQuantitiesField()}
            <Item className="quantity-item ">
              <Button onClick={this.handleAdd}>Add {record ? record.UOM : ''}</Button>
            </Item>
          </Flex>
        </DialogBodyDiv>
      </>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withRouter(connect(OrdersModule)(mapStateToProps, null, null, { forwardRef: true })(PickedQuantity))
