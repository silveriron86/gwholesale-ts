import * as React from 'react'
import { Icon, Modal } from 'antd'
import { DialogSubContainer, FlexDiv, DialogBodyDiv, ItemHMargin4, PalletLabelsTable } from '~/modules/customers/nav-sales/styles'
import { ThemeButton, ThemeInputNumber, ThemeOutlineButton, ThemeSelect, ThemeSwitch, ThemeTable } from '~/modules/customers/customers.style'
import { OrderDetail, OrderItem, WholesaleSalesOrderPalletLabel } from '~/schema'
import { CustomButton, Icon as IconSVG } from '~/components'
import { Item, ItemValue } from '~/modules/vendors/purchase-orders/_style'
import _, { cloneDeep } from 'lodash'
import { generealPadString, printWindow } from '~/common/utils'
import CustomDropdown from '../components/custom-dropdown'
import moment from 'moment'
import { fullButton } from '~/modules/customers/sales/_style'
import SOPalletPrintPreview from './so-pallet-print-preview'

type PrintPalletLabelProps = {
  onClose: Function
  visible: boolean
  orderItems: OrderItem[]
  order: OrderDetail
  sellerSetting: any
  salesOrderPalletLabels: WholesaleSalesOrderPalletLabel[]
  saveSalesOrderPalletLabels: Function
  getSalesOrderPalletLabels: Function

}

class PrintPalletLabel extends React.PureComponent<PrintPalletLabelProps> {
  state = {
    palletLabels: [],
    currentPage: 0,
    pageSize: 10,
    printRef: null,
    printLabelsReviewShow: false
  }

  columns: any[] = [
    {
      title: 'PALLET NO.',
      dataIndex: 'palletNumber',
      align: 'left',
      width: 150,
      render: (palletNumber: string, record: any, index: number) => {
        const orderPickupReference = this.props.order.pickupReferenceNo
        const palletIndex = (this.state.pageSize * this.state.currentPage) + (index + 1)
        return orderPickupReference ? `${orderPickupReference}-${generealPadString(palletIndex, 3)}` : palletNumber
      }
    },
    {
      title: 'QTY.',
      dataIndex: 'palletUnits',
      width: 100,
      align: 'left',
      render: (palletUnits: number, record: any, index: number) => {
        return (
          <ThemeInputNumber
            // style={{ width: '100px !important' }}
            min={0}
            value={palletUnits}
            onChange={(val: any) => { this.onIncreasePalletUnits(index, val) }} />
        )
      }
    },
    {
      title: 'PRODUCTS',
      dataIndex: 'productIds',
      align: 'left',
      render: (productIds: string, record: any, index: number) => {
        return (
          <div>
            <CustomDropdown
              orderItems={this.props.orderItems}
              palletLabels={this.state.palletLabels}
              productIds={productIds}
              index={index}
              onSelectProduct={this.onSelectProduct}
            />
          </div>
        )
      }
    },
    {
      title: 'DELETE',
      dataIndex: 'x',
      width: 100,
      align: 'center',
      render: (value: any, record: any, index: number) => {
        return (
          <IconSVG type='trash' viewBox="0 0 24 24" width={20} height={20} onClick={() => { this.onDeletePalletLabel(index) }} style={{ cursor: 'pointer' }} />
        )
      }
    }
  ]

  componentDidMount() {
    const { order } = this.props
    if (order) {
      this.props.getSalesOrderPalletLabels(order.wholesaleOrderId)
    }
  }

  componentWillUnmount() {
    const { order } = this.props
    const { palletLabels } = this.state
    const orderPickupReference = order.pickupReferenceNo
    const data = palletLabels.map((el, index) => {
      if (el.wholesaleOrder) {
        delete el.wholesaleOrder
      }
      if (orderPickupReference) {
        el.palletNumber = `${orderPickupReference}-${generealPadString(index + 1, 3)}`
      }
      return el
    })
    if (this.props.order) {
      this.props.saveSalesOrderPalletLabels({
        itemsList: data,
        orderId: this.props.order.wholesaleOrderId
      })
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.salesOrderPalletLabels) != JSON.stringify(nextProps.salesOrderPalletLabels)) {
      this.setState({ palletLabels: nextProps.salesOrderPalletLabels })
    }
  }

  onSelectProduct = (orderItemId: number, index: number) => {
    const { orderItems } = this.props
    const { palletLabels, currentPage, pageSize } = this.state
    const arrayIndex = currentPage * pageSize + index
    const oldLabels = cloneDeep(palletLabels)
    const oldProductIds = oldLabels[arrayIndex].productIds
    const ids = oldProductIds ? oldProductIds.split(';') : []
    const findIndex = ids.findIndex((el: any) => el == orderItemId)
    let isAdd = true
    if (findIndex > -1) {
      isAdd = false
      ids.splice(findIndex, 1)
    } else {
      ids.push(orderItemId)
    }

    oldLabels[arrayIndex].productIds = ids.join(';')
    const orderItem = orderItems.find(el => el.wholesaleOrderItemId == orderItemId)
    if (isAdd) {
      const alreadyAddedInOtherLabels = palletLabels.filter(el => {
        if (el.wholesaleOrderItemId != orderItemId) {
          // debugger
          const productIds = el.productIds ? el.productIds : ''
          const idArr = productIds.split(';')
          if (idArr.indexOf(orderItemId.toString()) > -1) {
            return true
          } else {
            return false
          }
        } else {
          return false
        }
      })
      console.log(alreadyAddedInOtherLabels)
      oldLabels[arrayIndex].palletUnits += orderItem && alreadyAddedInOtherLabels.length == 0 ? orderItem.quantity : 0;
    } else {
      oldLabels[arrayIndex].palletUnits -= orderItem ? orderItem.quantity : 0;
      if (oldLabels[arrayIndex].palletUnits < 0) {
        oldLabels[arrayIndex].palletUnits = 0
      }
      if (!oldLabels[arrayIndex].productIds) {
        oldLabels[arrayIndex].palletUnits = 0
      }
    }
    this.setState({ palletLabels: oldLabels })
  }

  onIncreasePalletUnits = (index: number, value: any) => {
    const { palletLabels, currentPage, pageSize } = this.state
    const arrayIndex = currentPage * pageSize + index
    const oldLabels = cloneDeep(palletLabels)
    oldLabels[arrayIndex].palletUnits = value
    console.log(oldLabels[arrayIndex])
    this.setState({ palletLabels: oldLabels })
  }

  onDeletePalletLabel = (index: number) => {
    const { order } = this.props
    const { palletLabels, currentPage, pageSize } = this.state
    const arrayIndex = currentPage * pageSize + index
    const oldLabels = cloneDeep(palletLabels)
    oldLabels.splice(arrayIndex, 1)
    const pickupReferenceNo = order.pickupReferenceNo ? order.pickupReferenceNo : order.wholesaleOrderId + "-pallet"
    oldLabels.forEach((el, index) => {
      el.palletNumber = pickupReferenceNo + '-' + generealPadString((index + 1), 3)
      el.displayNumber = index + 1
    })
    console.log(oldLabels)
    this.setState({ palletLabels: oldLabels })
  }

  addNewPallet = () => {
    console.log('add new pallet')
    const { palletLabels } = this.state
    const { order } = this.props
    const lastPalletLabel: WholesaleSalesOrderPalletLabel | null = palletLabels.length ? palletLabels[palletLabels.length - 1] : null
    const lastPalletLabelNumber = lastPalletLabel ? lastPalletLabel.displayNumber + 1 : 1
    const pickupReferenceNo = order.pickupReferenceNo ? order.pickupReferenceNo : order.wholesaleOrderId + "-pallet"
    let newPalletLabels = cloneDeep(palletLabels)
    const emptyValue = {
      id: 0,
      palletNumber: pickupReferenceNo + '-' + generealPadString(lastPalletLabelNumber, 3),
      palletUnits: 0,
      productIds: '',
      displayNumber: lastPalletLabelNumber
    }
    console.log(emptyValue)
    newPalletLabels.push(emptyValue)
    this.setState({ palletLabels: newPalletLabels })
  }

  onPageChange = (currentPage: number) => {
    this.setState({ currentPage: currentPage - 1 })
  }

  getAssignedItems = () => {
    const { palletLabels } = this.state
    let productIds: any[] = []
    palletLabels.forEach(el => {
      const ids = el.productIds
      const idArr = ids ? ids.split(';') : []
      productIds = _.union(productIds, idArr)
    });
    return productIds
  }

  onPrint: any = () => {
    return this.state.printRef
  }

  openPrintModal = () => {
    this.setState({
      printLabelsReviewShow: true,
    })
  }

  closePrintModal = () => {
    this.setState({
      printLabelsReviewShow: false,
    })
  }

  render() {
    const { orderItems, order, sellerSetting } = this.props
    const { palletLabels, currentPage, pageSize } = this.state
    const totalPickedQty = _.sumBy(orderItems, 'picked')
    const totalPalletLabelUnits = _.sumBy(palletLabels, 'palletUnits')
    const assignedItems = this.getAssignedItems()
    return (
      <div>
        <DialogSubContainer
          className='bordered'
        >
          <FlexDiv className='space-between'>
            <div style={{ maxWidth: '40%' }}>
              <Item>Pickup Reference No.</Item>
              <ItemValue>{order.pickupReferenceNo ? order.pickupReferenceNo : 'N/A'}</ItemValue>
            </div>
            <div>
              <Item>Customer</Item>
              <ItemValue>{order.wholesaleClient && order.wholesaleClient.clientCompany ? order.wholesaleClient.clientCompany.companyName : 'N/A'}</ItemValue>
            </div>
            <div>
              <Item>Fuflillment Date</Item>
              <ItemValue>{moment(order.deliveryDate).format('MM/DD/YYYY')}</ItemValue>
            </div>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv style={{ padding: '8px' }}>
          <FlexDiv className='space-between' style={{ padding: '32px 24px' }}>
            <Item>Specify products per pallets</Item>
            <ThemeOutlineButton onClick={this.addNewPallet}><Icon type='plus' style={ItemHMargin4} />Add new pallet</ThemeOutlineButton>
          </FlexDiv>
          <PalletLabelsTable>
            <ThemeTable
              columns={this.columns}
              dataSource={palletLabels}
              pagination={{ pageSize: pageSize, current: currentPage + 1, onChange: this.onPageChange }}
            />
          </PalletLabelsTable>
          <FlexDiv className='total-values' style={{ padding: '12px 24px' }}>
            <div className='child' style={{ marginRight: assignedItems.length ? 40 : 0 }}>
              <Item>Products assigned to pallets</Item>
              <Item className='bold-big'>{assignedItems.length} of {orderItems.length}</Item>
            </div>
            {assignedItems.length ?
              <div className='child' >
                <Item>Qty. assigned to pallets</Item>
                <Item className={`bold-big ${totalPalletLabelUnits > totalPickedQty ? 'warning-bold' : ''}`}>{totalPalletLabelUnits} of {totalPickedQty}</Item>
              </div>
              : ''
            }

          </FlexDiv>
          <div className={`warning ${totalPalletLabelUnits > totalPickedQty ? '' : 'hide'}`}>
            <Icon type="warning" />
            Qty. assigned to pallets ({totalPalletLabelUnits})  exceeds picked qty. ({totalPickedQty})
          </div>
        </DialogBodyDiv>
        <DialogSubContainer style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <ThemeButton type="primary" onClick={() => this.props.onClose("showPalletLabelModal", false)}>
              Save
            </ThemeButton>
          </div>
          <div>
            <ThemeButton type="primary" onClick={this.openPrintModal}>
              Print {palletLabels.length} Pallet Labels
            </ThemeButton>
          </div>
          <Modal
            width={1100}
            footer={null}
            visible={this.state.printLabelsReviewShow}
            onCancel={this.closePrintModal}
          >
            <div id={'PrintSalesOrderPalletLabel'} style={{ padding: 20 }}>
              <SOPalletPrintPreview
                palletLabels={palletLabels}
                order={order}
                orderItems={orderItems}
                sellerSetting={sellerSetting}
                ref={(el) => {
                  this.setState({ printRef: el })
                }}
              />
            </div>
            <CustomButton style={fullButton} onClick={() => printWindow('PrintSalesOrderPalletLabel', this.onPrint.bind(this))}>
              <Icon type="printer" theme="filled" />
              Print Labels
            </CustomButton>
          </Modal>
        </DialogSubContainer>
      </div>
    )
  }
}

export default PrintPalletLabel
