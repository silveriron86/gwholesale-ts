import * as React from 'react'
import { useState } from 'react'
import { Select } from 'antd'
import { AddACWrapper } from '../../styles'
import { handlerNoLotNumber } from '~/common/utils'
import { OrderDetail, OrderItem } from '~/schema'
import { History } from 'history'
import _ from 'lodash'
import jQuery from 'jquery'
import { ThemeSelect } from '~/modules/customers/customers.style'
import InventoryAsterisk from '~/components/inventory/inventoryAsterisk'

interface CartProps {
  currentOrder: OrderDetail
  orderItems: OrderItem[]
  loading: boolean
  toggleModal: Function
  updateSaleOrderItem: Function
  updateSaleOrderItemWithoutRefresh: Function
  startUpdating: Function
  changeSelectedItems: Function
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  history: History
  getOrderItemsById: Function
  updateOrder: Function
  updateOrderItemsDisplayOrder: Function
  onChagneInserItemPosition: Function
  onChangeOrderItemPricingLogic: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  toggleOpenChildModal: Function
  visibleItemModal: boolean
  condensedMode: boolean
  updating: boolean
  copyOrderQty: Function
  current: any
  hideReplaceDropdown: Function
  isFreshGreen: boolean
}

export const ReplaceItem: React.SFC<CartProps> = (props: any) => {
  const [productSKUs, setProductSKUs] = useState<any[]>([])
  const [search, setSearch] = useState('')
  const ref = React.useRef<any>()

  const visibleDropdown = props?.current?.visibleDropdown
  const currentVariety = props?.current?.variety
  const { sellerSetting, simplifyItemsByOrderId, simplifyItems } = props
  const _simplifyItems = sellerSetting?.company?.enableAddItemFromProductList ? simplifyItemsByOrderId : simplifyItems


  React.useEffect(() => {
    if (visibleDropdown) {
      jQuery('.replace-item-dropdown input').trigger('focus')
      // setTabKeyboardEvent()
      checkDropdownIsAdded()
    }
  }, [visibleDropdown])

  const initProductSkus = (searchStr: string, type: string, isLoading: boolean) => {
    if (!isLoading && searchStr.length < 2) {
      setProductSKUs([])
      return
    }

    const options: any[] = []
    if (_simplifyItems && _simplifyItems.length > 0) {
      _simplifyItems.forEach((item: any, index: number) => {
        let row = item
        if (typeof item.wholesaleItem !== 'undefined') {
          row = { ...item, ...item.wholesaleItem }
          row.SKU = row.sku
        }

        if (
          isLoading ||
          (!isLoading && row[type] && row[type].toLowerCase().trim().indexOf(searchStr.toLowerCase().trim()) >= 0) ||
          (!isLoading && row.variety && row.variety.toLowerCase().trim().indexOf(searchStr.toLowerCase().trim()) >= 0)
        ) {
          const productName = type === 'SKU' ? `${row.variety} ${row[type] ? `(${row[type]})` : ''}` : row[type]
          options.push({
            key: productName,
            value: row.itemId,
            itemInfo: `${row[type] ? row[type] : ''} ${row.variety} ${handlerNoLotNumber(row, 1)} ${row.inventoryUOM}
            ${(<InventoryAsterisk item={row} flag={1}></InventoryAsterisk>)} available (${handlerNoLotNumber(row, 2)} ${
              row.inventoryUOM
            }
            ${(<InventoryAsterisk item={row} flag={2}></InventoryAsterisk>)}&nbsp;on hand)`,
            text: (
              <AddACWrapper className="space-between">
                <div className="sku ellipsis">{row[type] ? row[type] : ''}</div>
                <div className="product-name ellipsis">{row.variety}</div>
                <div className="inventory ellipsis">
                  {handlerNoLotNumber(row, 1)} {row.inventoryUOM}
                  <InventoryAsterisk item={row} flag={1}></InventoryAsterisk> available ({handlerNoLotNumber(row, 2)}{' '}
                  {row.inventoryUOM}
                  <InventoryAsterisk item={row} flag={2}></InventoryAsterisk>&nbsp;on hand)
                </div>
              </AddACWrapper>
            ),
          })
        }
      })
    }
    let result = options.sort((a, b) => a.key.localeCompare(b.key))
    if (isLoading) {
      const currentItemIndex = result.findIndex((el) => el.key.toLowerCase().trim().indexOf(searchStr.toLowerCase().trim()) > -1)
      const startIndex = currentItemIndex > 20 ? currentItemIndex - 20 : 0
      const endIndex = startIndex + 40 > result.length ? result.length : startIndex + 40
      result = result.slice(startIndex, endIndex)
    }
    if (result.length > 50) {
      setProductSKUs(result.slice(0, 50))
    } else {
      setProductSKUs(result)
    }
  }
  ref.current = initProductSkus

  React.useEffect(() => {
    if (_simplifyItems.length) {
      ref.current(currentVariety, 'SKU', true)
    }
  }, [_simplifyItems, currentVariety])

  React.useEffect(() => {
    if (search) {
      const triggeredSource = localStorage.getItem('trigger-by-keyboard')
      let defaultOpen = true
      if (triggeredSource === '1') {
        defaultOpen = false
      }
      const open = defaultOpen || search.length > 1

      const dropdowns = jQuery('.ant-select-dropdown.ant-select-dropdown--single')
      let replaceDropdown = null
      let itemsContainer = null
      jQuery.each(dropdowns, function(i: number, el: any) {
        if (jQuery(el).find('.replace-dropdown-items-container').length) {
          replaceDropdown = el
          itemsContainer = jQuery(replaceDropdown).find('.replace-dropdown-items-container')
        }
      })
      if (replaceDropdown && itemsContainer) {
        if (open) {
          jQuery(replaceDropdown).css('visibility', 'visible')
          jQuery(itemsContainer).removeClass('replace-item-hidden')
        } else {
          jQuery(replaceDropdown).css('visibility', 'hidden')
          jQuery(itemsContainer).addClass('replace-item-hidden')
        }
      }
    }
  }, [search])

  const checkDropdownIsAdded = () => {
    jQuery('body').on('DOMNodeInserted', function(e) {
      if (
        jQuery(e.target).find('.ant-select-dropdown.ant-select-dropdown--single') &&
        jQuery(e.target).find('.replace-dropdown-items-container').length
      ) {
        const triggeredSource = localStorage.getItem('trigger-by-keyboard')
        let defaultOpen = true
        if (triggeredSource === '1') {
          defaultOpen = false
        }
        const replaceDropdown = jQuery(e.target).find('.ant-select-dropdown.ant-select-dropdown--single')[0]
        const itemsContainer = jQuery(e.target).find('.replace-dropdown-items-container')
        if (defaultOpen) {
          jQuery(replaceDropdown).css('visibility', 'visible')
          jQuery(itemsContainer).removeClass('replace-item-hidden')
        } else {
          jQuery(replaceDropdown).css('visibility', 'hidden')
          jQuery(itemsContainer).addClass('replace-item-hidden')
        }
      }
    })
  }

  const onSelectSKU = (itemId: any, option: any) => {
    if (props.current.itemId != itemId) {
      _updateOrder(itemId)
    }
    onBlur()
  }

  const _updateOrder = (wholesaleItemId: any[]) => {
    const { updateOrderItemProduct, currentOrder, current, startUpdating } = props
    startUpdating()
    updateOrderItemProduct({
      orderId: currentOrder.wholesaleOrderId,
      productId: current.itemId,
      displayOrderProduct: current.displayOrderProduct,
      data: {
        wholesaleItemId,
        quantity: current.quantity,
        picked: current.picked
      }
    })
  }

  const onSearch = (value: string) => {
    initProductSkus(value, 'SKU', false)
    setSearch(value)
  }

  const onBlur = () => {
    props.hideReplaceDropdown(props.current.wholesaleOrderItemId)
  }

  const renderOptions = () => {
    const children = productSKUs.map((el: any) => (
      <Select.Option key={el.key} value={el.value} title={el.value} itemInfo={el.itemInfo}>
        {el.text}
      </Select.Option>
    ))

    return children
  }

  return (
    <ThemeSelect
      defaultActiveFirstOption={false}
      className="replace-item-dropdown"
      defaultOpen={true}
      autoFocus={true}
      value={props.current.itemId}
      showSearch
      style={{ width: props.isFreshGreen ? '100%' : '90%', position: 'absolute', top: -15 }}
      placeholder="Replace item..."
      optionFilterProp="children"
      onSearch={onSearch}
      onSelect={onSelectSKU}
      onBlur={onBlur}
      filterOption={(input: any, option: any) => {
        return option.props.itemInfo.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }}
      dropdownStyle={{ minWidth: 700 }}
      dropdownRender={(el: any) => {
        return <div className="replace-dropdown-items-container">{el}</div>
      }}
    >
      {renderOptions()}
    </ThemeSelect>
  )
}
