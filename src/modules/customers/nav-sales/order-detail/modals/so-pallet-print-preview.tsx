import moment from 'moment'
import * as React from 'react'
import { FlexDiv } from '~/modules/cashSales/nav-sales/styles'
import { PageBreakAfter } from '~/modules/manufacturing/processing-list/print/print-modal.style'
import { OrderDetail, OrderItem } from '~/schema'
import { LabelWrapper, PalletLabelRow } from '../../styles'

type SOPalletPrintPreviewProps = {
  palletLabels: any[]
  order: OrderDetail
  orderItems: OrderItem[]
  sellerSetting: any
}

class SOPalletPrintPreview extends React.PureComponent<SOPalletPrintPreviewProps> {
  renderDetail = () => {
    const { palletLabels, order, orderItems, sellerSetting } = this.props
    let result: any[] = []

    palletLabels.forEach((el, index) => {
      const idArr = el.productIds ? el.productIds.split(';') : []
      let productNames: string[] = []
      idArr.forEach((id) => {
        const orderItem = orderItems.find((el) => el.wholesaleOrderItemId == id)
        if (orderItem) {
          productNames.push(orderItem.variety)
        }
      })
      const element = (borderRight: string) => (
        <LabelWrapper style={{ borderRight: borderRight }} className="print-block">
          {/*
            <PalletLabelRow>
            <p style={{ fontFamily: 'Arial', fontSize: '22px', fontWeight: 'bold' }}>
              {order.wholesaleClient && order.wholesaleClient.clientCompany
                ? order.wholesaleClient.clientCompany.companyName
                : 'N/A'}
            </p>
          </PalletLabelRow>
          */}
          <PalletLabelRow style={{ display: 'flex' }}>
            <div style={{ height: '42px', marginBottom: '20px', fontSize: '42px', fontWeight: 'bold' }}>
              {order.wholesaleClient.abbreviation ?? ''}
            </div>
            <img src={`${process.env.AWS_PUBLIC}/${sellerSetting.imagePath}`} style={{ marginLeft: '40px', maxWidth: '150px' }} />
          </PalletLabelRow>
          <PalletLabelRow>
            <div className="pallet-label-small">PICK UP #</div>
            <div className="pallet-label-bold">{el.palletNumber}</div>
          </PalletLabelRow>
          <PalletLabelRow>
            <div className="pallet-label-small">FULFILLMENT DATE</div>
            <div className="pallet-label-bold">{moment(order.deliveryDate).format('MM/DD/YYYY')}</div>
          </PalletLabelRow>
          <PalletLabelRow style={{ height: 150 }}>
            <div className="pallet-label-small">ITEM(S)</div>
            <div className="pallet-label-medium">{productNames.length ? productNames.join(', ') : ''}</div>
          </PalletLabelRow>

          <PalletLabelRow style={{ marginTop: 40 }}>
            <div className="pallet-label-small">QTY</div>
            <div className="pallet-label-bold">{el.palletUnits} UNITS</div>
          </PalletLabelRow>
          <hr />
          <PalletLabelRow>
            <div className="pallet-label-big">
              PALLET{' '}
              <strong>
                {index + 1} of {palletLabels.length}
              </strong>
            </div>
          </PalletLabelRow>
        </LabelWrapper>
      )

      result.push(element('1px solid black'))
      result.push(element(''))
      if (index > 0 && index % 2 == 1) {
        result.push(<PageBreakAfter />)
      }
    })
    return result
  }

  render() {
    return <FlexDiv style={{ flexWrap: 'wrap' }}>{this.renderDetail()}</FlexDiv>
  }
}

export default SOPalletPrintPreview
