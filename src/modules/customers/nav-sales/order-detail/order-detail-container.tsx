import React from 'react'
import { notification } from 'antd'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import jQuery from 'jquery'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import SalesOrderDetailHeader from './order-detail-header'
import SalesOrderItems from './order-items'
import { Theme } from '~/common'
import { History } from 'history'
import { AuthUser, UserRole } from '~/schema'
import { ThemeSpin } from '../../customers.style'

type SODetailProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    history: History
    theme: Theme
    currentUser: AuthUser
  }

class SalesOrderDetailContainer extends React.PureComponent<SODetailProps> {
  state = {
    selectedTab: this.props.currentUser.accountType != UserRole.WAREHOUSE ? '1' : '8',
  }
  childRef = React.createRef<any>()

  componentDidMount() {
    const urlParams = new URLSearchParams(this.props.location.search)
    const search = urlParams.get('duplicated')
    if (search === 'success') {
      setTimeout(() => {
        window.history.replaceState({}, '', `#${this.props.location.pathname}`)
        setTimeout(() => {
          notification.success({
            message: 'SUCCESS',
            description: 'Order successfully duplicated.',
            onClose: () => {},
            duration: 5
          })
        }, 1500)
      }, 100)
    }

    const orderId = this.props.match.params.orderId
    console.log('here reloading orders')
    this.props.resetLoading()
    this.props.getOrderDetail(orderId)
    this.props.getSalesOrderItems(orderId)
    this.props.getSimplifyCustomers()
    this.props.getSellerSetting()
    this.props.getOrderAdjustments(orderId)
    this.props.getPrintSetting()
    this.props.getCompanyUsers()
    //new api to get order items by id and sort
    setTimeout(() => {
      this.props.getOneOffItems(orderId)
    }, 50)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      this.props.setAddressLoading()
      this.props.getAddresses()
    }
    if (this.props.page !== nextProps.page) {
      this.props.getSalesOrderItems(this.props.match.params.orderId)
    }
  }

  onDoHeaderActions = (actionName: string) => {
    console.log('actionName = ', actionName)
    this.childRef.current.onDoHeaderActions(actionName)
  }

  updateSelectedTab = (selectedTab: string) => {
    this.setState({ selectedTab })
  }

  render() {
    const { cancelingOrder, duplicateOrderLoading, currentOrder } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Selling & Shipping-Sales Orders'}>
        <ThemeSpin spinning={cancelingOrder || duplicateOrderLoading || !currentOrder} wrapperClassName="order-detail-spinner">
          <SalesOrderDetailHeader
            {...this.props}
            onDoHeaderActions={this.onDoHeaderActions}
            selectedTab={this.state.selectedTab}
          />
          <SalesOrderItems {...this.props} ref={this.childRef} updateSelectedTab={this.updateSelectedTab} />
        </ThemeSpin>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.orders,
    currentUser: state.currentUser,
  }
}
const Comp = withTheme(connect(OrdersModule)(mapStateToProps)(SalesOrderDetailContainer))
export default Comp
