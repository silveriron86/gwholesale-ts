import * as React from 'react'
import { useState, useEffect } from 'react'
import { Menu, Button, Icon, notification } from 'antd'
import { Icon as IconSVG } from '~/components'
import { ThemeButton, ThemeModal, ThemeSpin } from '~/modules/customers/customers.style'
import {
  ItemHMargin4,
  GridTableWrapper,
  CartTableWrapper,
  FloatMenuItem,
  DialogSubContainer,
  noPaddingFooter,
} from '../../styles'

import { cloneDeep } from 'lodash'
import AddItemNoteModal from '../modals/item-note'
import { ClassNames } from '@emotion/core'
import { checkError } from '~/common/utils'
import { OrderDetail, OrderItem } from '~/schema'
// import { DragSortingTable } from '../components/draggable-table'
import { DragSortingTable } from '../components/_draggable'
import ItemPrice from '../modals/item-price'
import PickedQuantity from '../modals/picked-quanitty'
import WorkOrderModal from '~/modules/manufacturing/processing-list/modals/wo-modal'
import { History } from 'history'
import _ from 'lodash'

import PriceModal from '~/modules/customers/sales/cart/modals/price-modal'
import EditItemName from '../modals/edit-item-name'
import LotSection from '~/modules/customers/sales/cart/tables/new-lot-table'
import LotSectionV2 from '~/modules/customers/sales/cart/tables/new-lot-table_v2'
import MultipleLotCatchWeights from '../modals/multiple-lots-catch-weights'
import { NewLabelModal } from '~/modules/vendors/purchase-orders/_style'
import NewPrintSOLabel from '../modals/NewPrintSOLabel'
import { OrderService } from '~/modules/orders/order.service'

interface CartProps {
  currentOrder: OrderDetail
  orderItems: OrderItem[]
  orderItemByProduct: any
  // simplifyItems: any[]
  // simplifyItemsByOrderId: any[]
  loading: boolean
  toggleModal: Function
  updateSaleOrderItem: Function
  updateSOItemForUOM: Function
  updateSaleOrderItemWithoutRefresh: Function
  startUpdating: Function
  changeSelectedItems: Function
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  history: History
  getOrderItemsById: Function
  updateOrder: Function
  updateOrderItemsDisplayOrder: Function
  onChagneInserItemPosition: Function
  onChangeOrderItemPricingLogic: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  toggleOpenChildModal: Function
  visibleItemModal: boolean
  condensedMode: boolean
  updating: boolean
  startUpdatingField: Function
  updatingField: boolean
  handlerOrderItemsRedux: Function
  handlerUpdateSOUOMRedux: Function
  // addCartOrderItem: Function
  // removeOrderItemForOrder: Function
  // addingCartItem: Boolean
  updateOrderItemByProductId: Function
}

export const Cart: React.SFC<CartProps> = (props: any) => {
  const {
    currentOrder,
    orderItems,
    toggleModal,
    catchWeightValues,
    history,
    loading,
    updateSaleOrderItem,
    handlerOrderItemsRedux,
    handlerUpdateSOUOMRedux,
    startUpdating,
    changeSelectedItems,
    getOrderItemsById,
    updateOrderItemsDisplayOrder,
    onChagneInserItemPosition,
    toggleOpenChildModal,
    onChangeOrderItemPricingLogicWithoutRefresh,
    condensedMode,
    updating,
    settingCompanyName,
    updatingField,
    updateOrderItemByProductId,
    updateOrderQuantityLoading,
  } = props
  const [dataSource, setDataSource] = useState(orderItems)
  const [openAddItemModal, setOpenAddItemModal] = useState(false)
  const [openItemNoteModal, setOpenItemNoteModal] = useState(false)
  const [openSOPrintModal, setOpenSOPrintModal] = useState(false)
  const [openAddWOModal, setOpenAddWOModal] = useState(false)
  const [openPriceModal, setOpenPriceModal] = useState(false)
  const [openItemPriceModal, setOpenItemPriceModal] = useState(false)
  const [openPickedQuantityModal, setOpenPickedQuantityModal] = useState(false)
  const [openLotsModal, setOpenLotsModal] = useState(false)
  const [openEditItemNameModal, setOpenEditItemNameModal] = useState(false)
  const [selectedRows, setSelectedRows] = useState([])
  const [currentRecord, setCurrentRecord] = useState<OrderItem | null>(null)
  const [clickedMenuId, setClickedMenuId] = useState(-1)
  const [isFreshGreen, setIsFreshGreen] = useState(false)

  const pickedQuantityRef = React.useRef(null)
  const itemNoteRef = React.useRef(null)
  const editItemRef = React.useRef(null)

  useEffect(() => {
    if (settingCompanyName === 'Fresh Green' || settingCompanyName === 'Fresh Green Inc') {
      setIsFreshGreen(true)
    }
  }, [settingCompanyName])

  useEffect(() => {
    setDataSource(orderItems)
    if (currentOrder) {
      checkAndUpdateDisplayOrder()
    }

    changeSelectedRows()
    // if (currentRecord) {
    //   const found = orderItems.find((oi) => oi.wholesaleOrderItemId === currentRecord.wholesaleOrderItemId)
    //   setCurrentRecord(cloneDeep(found))
    // }
  }, [orderItems])

  useEffect(() => {
    changeSelectedItems(selectedRows)
  }, [selectedRows.length])

  const changeSelectedRows = () => {
    let refreshedSelectedRows: any[] = []
    const oldSelectedIds = selectedRows.map((el) => {
      return el.wholesaleOrderItemId
    })
    orderItems.forEach((el) => {
      if (oldSelectedIds.indexOf(el.wholesaleOrderItemId) > -1) {
        refreshedSelectedRows.push(el)
      }
    })
    setSelectedRows(refreshedSelectedRows)
  }

  const checkAndUpdateDisplayOrder = () => {
    const dispalyOrders = orderItems.map((el: OrderItem) => {
      return el.displayOrder
    })
    const isDuplicated = checkDuplicates(dispalyOrders)

    if (isDuplicated) {
      let cloneItems = cloneDeep(orderItems)
      cloneItems.forEach((el: OrderItem, index: number) => {
        el.displayOrder = index
      })
    }
  }

  const checkDuplicates = (arr: any[]) => {
    let valuesAlreadySeen = []

    for (let i = 0; i < arr.length; i++) {
      let value = arr[i]
      if (valuesAlreadySeen.indexOf(value) !== -1) {
        return true
      }
      valuesAlreadySeen.push(value)
    }
    return false
  }

  const handleWorkorderCreated = () => {
    if (!currentOrder) return
    getOrderItemsById(currentOrder.wholesaleOrderId)
  }

  const validateOrderStatusIsShipped = () => {
    if (currentOrder && currentOrder.wholesaleOrderStatus == 'SHIPPED') {
      return true
    }
    return false
  }

  const renderFloatMenuItems = (productId: string, record: any) => {
    const currentProduct = props.orderItemByProduct[record.displayOrderProduct]
    return (
      <Menu onClick={menuClick.bind(this, record)}>
        <Menu.Item key={0}>
          <FloatMenuItem className="fill">
            <IconSVG type="trash" style={ItemHMargin4} viewBox="0 0 24 24" width={20} height={20} />
            Delete Item
          </FloatMenuItem>
        </Menu.Item>
        <Menu.Item key={1}>
          <FloatMenuItem>
            <IconSVG type="add-note" style={ItemHMargin4} viewBox="0 0 20 20" width={20} height={20} />
            Add a Note
          </FloatMenuItem>
        </Menu.Item>
        {!(currentProduct && currentProduct.workOrderStatus) && (
          <Menu.Item
            key={2}
            disabled={currentProduct == null ? true : currentProduct.UOM != currentProduct.inventoryUOM}
          >
            <FloatMenuItem className="fill">
              <IconSVG type="add-wo" style={ItemHMargin4} viewBox="0 0 20 20" width={20} height={20} />
              Add a Work Order
            </FloatMenuItem>
          </Menu.Item>
        )}
        <Menu.Item key={3}>
          <FloatMenuItem className="fill">
            <IconSVG type="add-wo" style={ItemHMargin4} viewBox="0 0 20 20" width={20} height={20} />
            Print Unit Labels
          </FloatMenuItem>
        </Menu.Item>
        {props.currentOrder.fulfillmentType === 4 && (
          <Menu.Item key={4}>
            <FloatMenuItem className="fill">
              <IconSVG type="add-wo" style={ItemHMargin4} viewBox="0 0 20 20" width={20} height={20} />
              Sync Container Item Data
            </FloatMenuItem>
          </Menu.Item>
        )}
      </Menu>
    )
  }

  const deleteOrderItem = (record: any) => {
    props.deleteOrderItem({
      orderId: props.currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
    })
  }

  const onToggleSelect = (type: string, e: any) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    if (type === 'All') {
      if (e.target.checked) {
        setSelectedRows(Object.keys(props.orderItemByProduct).map(Number))
      } else {
        setSelectedRows([])
      }
    } else {
      if (selectedRows.includes(type)) {
        setSelectedRows(selectedRows.filter((key) => key !== type))
      } else {
        setSelectedRows([...selectedRows, type])
      }
    }
  }

  const onCancelModal = (type: string) => {
    switch (type) {
      case 'openAddItemModal':
        setOpenAddItemModal(false)
        break
      case 'openItemNoteModal':
        setOpenItemNoteModal(false)
        break
      case 'openAddWOModal':
        setOpenAddWOModal(false)
        break
      case 'openItemPriceModal':
        setOpenItemPriceModal(false)
        break
      case 'openPickedQuantityModal':
        setOpenPickedQuantityModal(false)
        break
      case 'priceModal':
        setOpenPriceModal(false)
        break
      case 'lotsModal':
        setOpenLotsModal(false)
        break
      case 'openEditItemNameModal':
        setOpenEditItemNameModal(false)
        break
    }
    setClickedMenuId(-1)
    toggleOpenChildModal(false)
    if (type == 'openItemPriceModal' || type === 'openPickedQuantityModal') {
      return
    }
    setTimeout(() => {
      setCurrentRecord(null)
    }, 200)
  }

  const menuClick = (record: any, { key }: any) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    if (key == '0') {
      startUpdating()
      deleteOrderItem(record)
      // Remove catchWeight parsing failed data from localstorage
      if (record.items && record.items.length === 1) {
        localStorage.removeItem(`PARSING_FAILED_${record.wholesaleOrderItemId.toString()}`)
      } else if (record.items) {
        record.items.forEach((el: any) => {
          localStorage.removeItem(`PARSING_FAILED_${el.wholesaleOrderItemId.toString()}`)
        })
      }
    } else if (key == '1') {
      if (record) {
        setCurrentRecord(record)
        toggleOpenChildModal(true)
        setOpenItemNoteModal(true)
      }
    } else if (key == '2') {
      if (record) {
        setCurrentRecord(record)
      }
      toggleOpenChildModal(true)
      setOpenAddWOModal(true)
    } else if (key == '3') {
      if (record) {
        setCurrentRecord(record)
      }
      setOpenSOPrintModal(true)
    } else if (key == '4') {
      OrderService.instance
        .syncOrderItemTms(currentOrder.wholesaleOrderId, record.itemId, record.displayOrderProduct)
        .subscribe({
          next(resp) {
            notification.open({
              message: resp.body.message,
              duration: 3,
            })
          },
          error(err) {
            checkError(err)
          },
          complete() {},
        })
    }
  }

  const openModal = (type: string, record: OrderItem) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    toggleOpenChildModal(true)
    switch (type) {
      case 'itemPriceModal':
        getOrderItemsById(currentOrder.wholesaleOrderId)
        setCurrentRecord(record)
        setOpenItemPriceModal(true)
        break
      case 'pickedQuantityModal':
        setCurrentRecord(record)
        setOpenPickedQuantityModal(true)
        break
      case 'addNoteModal':
        setCurrentRecord(record)
        setOpenItemNoteModal(true)
        break
      case 'addWOModal':
        setClickedMenuId(record.wholesaleOrderItemId)
        setOpenAddWOModal(true)
        break
      case 'priceModal':
        setCurrentRecord(record)
        setOpenPriceModal(true)
        break
      case 'lotsModal':
        setCurrentRecord(record)
        setOpenLotsModal(true)
        break
      case 'openEditItemNameModal':
        setCurrentRecord(record)
        setOpenEditItemNameModal(true)
        break
    }
  }

  const handleRecordSave = (type: string) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    if (type === 'openPickedQuantityModal') {
      pickedQuantityRef.current.handleSave()
    } else if (type == 'deleteItemNote' && currentRecord) {
      let row = { ...currentRecord, note: '' }
      onCancelModal('openItemNoteModal')
      handlerOrderItemsRedux(row)
      updateSaleOrderItem(row)
    } else if (currentRecord) {
      handlerOrderItemsRedux(currentRecord)
      updateSaleOrderItem(currentRecord)
    }

    onCancelModal(type)

    if (type !== 'openPickedQuantityModal') {
      setCurrentRecord(null)
    }
  }

  const handleSaveByModalType = (modalType: string, editType: string, type: string) => {
    if (modalType == 'deleteItemNote' || modalType == 'openItemNoteModal') {
      if (itemNoteRef && itemNoteRef.current) {
        itemNoteRef.current.handleSave(modalType)
      }
    } else if (modalType == 'openEditItemNameModal') {
      if (editItemRef && editItemRef.current) {
        editItemRef.current.handleSave()
      }
    }
    if (modalType === 'deleteItemNote') {
      return onCancelModal('openItemNoteModal')
    }
    onCancelModal(modalType)
  }

  const onClickAddItem = (orderItemId: string) => {
    if (validateOrderStatusIsShipped()) {
      return
    }
    const item = orderItems.find((el: OrderItem) => {
      return el.wholesaleOrderItemId == orderItemId
    })
    const index = orderItems.findIndex((el: OrderItem) => {
      return el.wholesaleOrderItemId == orderItemId
    })
    onChagneInserItemPosition(item && item.displayOrder ? item.displayOrder : index)
    toggleModal(true)
  }

  return (
    <div style={{ paddingRight: 20 }}>
      <GridTableWrapper>
        <CartTableWrapper className="sales-cart-table">
          <DragSortingTable
            dataSource={Object.values(props.orderItemByProduct)}
            rowKey={'wholesaleOrderItemId'}
            onRow={(record: OrderItem, index: number) => ({
              index,
              wholesaleOrderItemId: record.wholesaleOrderItemId,
            })}
            hasFooter={
              condensedMode ||
              currentOrder.wholesaleOrderStatus === 'CANCEL' ||
              currentOrder.wholesaleOrderStatus === 'SHIPPED'
                ? false
                : true
            }
            acProps={props}
            saleItems={props.saleItems}
            selectedRows={selectedRows}
            catchWeightValues={catchWeightValues}
            renderFloatingMenu={renderFloatMenuItems}
            openModal={openModal}
            openAddModal={toggleModal}
            onToggleSelect={onToggleSelect}
            onClickAddItem={onClickAddItem}
            updateOrderItemsDisplayOrder={updateOrderItemsDisplayOrder}
            handlerOrderItemsRedux={handlerOrderItemsRedux}
            handlerUpdateSOUOMRedux={handlerUpdateSOUOMRedux}
            validateOrderStatusIsShipped={validateOrderStatusIsShipped}
            onChangeOrderItemPricingLogicWithoutRefresh={onChangeOrderItemPricingLogicWithoutRefresh}
            draggable={!condensedMode}
            currentOrder={currentOrder}
            updating={updating}
            updatingField={updatingField}
            openAddItemModal={openAddItemModal}
            isFreshGreen={isFreshGreen}
            updateOrderItemByProductId={updateOrderItemByProductId}
            updateOrderQuantityLoading={updateOrderQuantityLoading}
            {...props}
          />
        </CartTableWrapper>
      </GridTableWrapper>
      <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`Item Notes`}
            visible={openItemNoteModal}
            onCancel={onCancelModal.bind(this, 'openItemNoteModal')}
            okText={'Save'}
            cancelText={'Cancel'}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={
              <DialogSubContainer>
                <ThemeButton
                  type="primary"
                  onClick={handleSaveByModalType.bind(this, 'openItemNoteModal', 'isUpdateNote', 'note')}
                >
                  Save
                </ThemeButton>
                <Button onClick={handleSaveByModalType.bind(this, 'deleteItemNote', 'isUpdateNote', 'note')}>
                  Delete Note
                </Button>
                <Button onClick={onCancelModal.bind(this, 'openItemNoteModal')}>CANCEL</Button>
              </DialogSubContainer>
            }
            width={600}
          >
            <AddItemNoteModal
              record={currentRecord}
              currentOrder={currentOrder}
              setCurrentRecord={setCurrentRecord}
              open={openItemNoteModal}
              updateOrderItemByProductId={updateOrderItemByProductId}
              orderItemByProduct={props.orderItemByProduct}
              ref={itemNoteRef}
            />
          </ThemeModal>
        )}
      </ClassNames>
      <NewLabelModal
        style={{ width: '703px', height: '930px', padding: '0px' }}
        width={703}
        footer={null}
        visible={openSOPrintModal}
        onCancel={() => setOpenSOPrintModal(false)}
      >
        <NewPrintSOLabel
          order={props.currentOrder}
          poObj={props.currentOrder.reference}
          orderItem={currentRecord}
          contractNumber={props.currentOrder.contractNumber ? props.currentOrder.contractNumber : ''}
          NSN={props.currentOrder.nsn ? props.currentOrder.nsn : ''}
          weightUOM={
            props.sellerSetting ? (props.sellerSetting.company ? props.sellerSetting.company.weightUOM : '') : ''
          }
        />
      </NewLabelModal>
      <ClassNames>
        {({ css, cx }) =>
          openItemPriceModal && (
            <ThemeModal
              title={`Item Price`}
              visible={openItemPriceModal}
              onCancel={onCancelModal.bind(this, 'openItemPriceModal')}
              okText={'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              footer={null}
              width={700}
            >
              <ItemPrice
                changeOrderItemPrice={console.log}
                changeModalStatus={onCancelModal}
                orderItemByProduct={props.orderItemByProduct}
                updateOrderItemPrice={props.updateOrderItemPrice}
                record={currentRecord}
                order={currentOrder}
              />
            </ThemeModal>
          )
        }
      </ClassNames>
      <ClassNames>
        {({ css, cx }) =>
          openPickedQuantityModal && (
            <ThemeModal
              title={`Picked Quantities (catchweight item)`}
              visible={openPickedQuantityModal}
              onCancel={onCancelModal.bind(this, 'openPickedQuantityModal')}
              okText={'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={handleRecordSave.bind(this, 'openPickedQuantityModal')}>
                    <ThemeSpin spinning={loading} className="button-spinner" size={'small'} />
                    Save
                  </ThemeButton>
                  <Button onClick={onCancelModal.bind(this, 'openPickedQuantityModal')}>CANCEL</Button>
                </DialogSubContainer>
              }
              width={700}
            >
              {currentRecord && currentRecord.items.length === 1 ? (
                <PickedQuantity
                  record={currentRecord}
                  open={openPickedQuantityModal}
                  wrappedComponentRef={pickedQuantityRef}
                  updateOrderItemByProductId={updateOrderItemByProductId}
                />
              ) : (
                <MultipleLotCatchWeights
                  record={currentRecord}
                  open={openPickedQuantityModal}
                  wrappedComponentRef={pickedQuantityRef}
                />
              )}
            </ThemeModal>
          )
        }
      </ClassNames>
      <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`${currentRecord ? currentRecord.variety : ''}`}
            visible={openEditItemNameModal}
            onCancel={onCancelModal.bind(this, 'openEditItemNameModal')}
            okText={'Save'}
            cancelText={'Cancel'}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={
              <DialogSubContainer>
                <ThemeButton
                  type="primary"
                  onClick={handleSaveByModalType.bind(
                    this,
                    'openEditItemNameModal',
                    'isUpdateEditedItemName',
                    'editedItemName',
                  )}
                >
                  <ThemeSpin spinning={loading} className="button-spinner" size={'small'} />
                  Save
                </ThemeButton>
                <Button onClick={onCancelModal.bind(this, 'openEditItemNameModal')}>CANCEL</Button>
              </DialogSubContainer>
            }
            width={400}
          >
            {openEditItemNameModal && (
              <EditItemName
                record={currentRecord}
                open={openEditItemNameModal}
                currentOrder={currentOrder}
                updateOrderItemByProductId={updateOrderItemByProductId}
                orderItemByProduct={props.orderItemByProduct}
                ref={editItemRef}
              />
            )}
          </ThemeModal>
        )}
      </ClassNames>
      {currentOrder !== null && (
        <WorkOrderModal
          orderId={currentOrder.wholesaleOrderId}
          orderItemId={clickedMenuId}
          quantity={currentRecord ? currentRecord.quantity : 0}
          isSpecial={true}
          visible={openAddWOModal}
          handleOk={onCancelModal.bind(this, 'openAddWOModal')}
          onWordOrderCreated={handleWorkorderCreated}
          history={history}
          handleCancel={onCancelModal.bind(this, 'openAddWOModal')}
        />
      )}
      {openLotsModal && (
        <ThemeModal
          title={'Lots'}
          keyboard={true}
          visible={openLotsModal}
          onOk={onCancelModal.bind(this, 'lotsModal')}
          onCancel={onCancelModal.bind(this, 'lotsModal')}
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={'75%'}
          style={{ minWidth: 1200 }}
          footer={null}
        >
          <LotSectionV2
            currentRecord={currentRecord}
            currentOrder={currentOrder}
            getOrderItemsById={getOrderItemsById}
            orderItems={orderItems}
            updateOrderQuantityLoading={updateOrderQuantityLoading}
            onCancel={onCancelModal.bind(this, 'lotsModal')}
            isDisablePickingStep={props.company.isDisablePickingStep}
            sellerSetting={props.sellerSetting}
          />
        </ThemeModal>
      )}
      {openPriceModal && (
        <ThemeModal
          className="price-modal"
          title="PRICING INFORMATION"
          closeIcon={<Icon type="close" style={{ fontSize: '20px', color: 'white' }} />}
          keyboard={true}
          visible={openPriceModal}
          onOk={onCancelModal.bind(this, 'priceModal')}
          onCancel={onCancelModal.bind(this, 'priceModal')}
          okText="Close [esc]"
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={1330}
          footer={null}
          style={{ height: 868 }}
          centered
        >
          <PriceModal
            itemId={currentRecord ? currentRecord.itemId : 0}
            clientId={currentOrder ? currentOrder.wholesaleClient.clientId.toString() : ''}
          />
        </ThemeModal>
      )}
    </div>
  )
}
