import React, { FC, useEffect, useState } from 'react'
import { Select, Table } from 'antd'
import { ThemeButton, ThemeModal, ThemeSelect, ThemeSpin, ThemeTable } from '~/modules/customers/customers.style'
import styled from '@emotion/styled'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, mathRoundFun } from '~/common/utils'
import { WrapModal } from './container-detail-modal'
import _ from 'lodash'
import CreateNewContainerModal from './createNewContainerModal'

const ContainerAssignModal: FC<any> = ({ onCancel, currentOrderId, callback, getUnassignOrderItemsCount, company, match }) => {
  const [selectedRows, setSelectedRows] = useState([])
  const [selectedRowKeys, setSelectedRowKeys] = useState([])
  const [loading, setLoading] = useState(false)
  const [saveLoading, setSaveLoading] = useState(false)
  const [createNewContainerLoading, setCreateNewContainerLoading] = useState(false)
  const [selectedContainer, setSelectedContainer] = useState(undefined)
  const [data, setData] = useState([])
  const [containerList, setContainerList] = useState([])
  const [createNewContainerModalVisible, setCreateNewContainerModalVisible] = useState(false)


  const [query, setQuery] = useState<any>({
    page: 1,
    pageSize: 20,
    order: undefined,
    columnKey: undefined
  })

  useEffect(() => {
    setLoading(true)
    OrderService.instance.getUnassignOrderItems(currentOrderId).subscribe({
      next(resp) {
        setData(resp.body.data)
      },
      error(err) { checkError(err) },
      complete() {
        setLoading(false)
      }
    })
    fetchAssignModalContainers()
  }, [currentOrderId])

  const fetchAssignModalContainers = () => {
    OrderService.instance.getAssignModalContainers().subscribe({
      next(resp) {
        setContainerList(resp.data)
      },
      error(err) { checkError(err) },
      complete() {

      }
    })
  }
  const handleCreateContainer = () => {
    setCreateNewContainerModalVisible(true)
    // setCreateNewContainerLoading(true)
    // OrderService.instance.createContainer(`Container ${containerList.length + 1}`, company).subscribe({
    //   next(resp) {  },
    //   error(err) { checkError(err) },
    //   complete() {
    //     fetchAssignModalContainers()
    //     setCreateNewContainerLoading(false)
    //   }
    // })
  }

  const handleSave = () => {
    if (!selectedContainer) return
    setSaveLoading(true)
    const data = {
      containerId: selectedContainer,
      containerName: containerList.find(v => v.id === selectedContainer).no,
      tmsFlowItems: selectedRows.map((v: any) => ({
        source: v.wholesaleOrderItemId,
        quantity: v.picked || v.quantity,
        uom: v.UOM,
        tmsFlow: null
      }))
    }
    OrderService.instance.setTmsFlowItems(data, currentOrderId).subscribe({
      next(resp) {
        callback()
        getUnassignOrderItemsCount(match.params.orderId)
      },
      error(err) { checkError(err) },
      complete() {
        setSaveLoading(false)
        onCancel()
      }
    })
  }

  const columns = [
    {
      title: 'Item',
      dataIndex: 'itemName',
      sorter: true,
    },
    {
      title: 'Brand',
      dataIndex: 'modifiers',
      sorter: true,
    },
    {
      title: 'Origin',
      dataIndex: 'extraOrigin',
      sorter: true,
    },
    {
      title: 'Class',
      dataIndex: 'commodityClass',
      sorter: true,
    },
    {
      title: 'Total Gross Volume ft³',
      dataIndex: 'grossVolume',
      sorter: true,
      render: (t, r) => mathRoundFun(_.multiply(r.catchWeightQty, t), 2)
    },
    {
      title: 'Total Gross Weight LBs',
      dataIndex: 'grossWeight',
      sorter: true,
      render: (t, r) => mathRoundFun(_.multiply(r.catchWeightQty, t), 2)
    },
  ]

  const onSelectChange = (selectedRowKeys: React.SetStateAction<never[]>, rows: any) => {
    setSelectedRows(rows)
    setSelectedRowKeys(selectedRowKeys)
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }
  return (
    <WrapModal
      title='Assign Items to Containers'
      onCancel={onCancel}
      width={800}
      visible
      footer={[
        <ThemeButton key={1} onClick={handleSave} loading={saveLoading}>
          Save
        </ThemeButton>,
        <span key={2} style={{ marginLeft: 20, cursor: 'pointer' }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <ThemeSpin spinning={loading}>
        <div>
          <p>1. Select Unassigned Items</p>
          <WrapTable
            rowSelection={rowSelection}
            columns={columns}
            dataSource={data}
            // pagination={{
            //   pageSize: query.pageSize,
            //   current: query.page,
            //   total: 100,
            // }}
            pagination={false}
            rowKey='wholesaleOrderItemId'
            onChange={(pagination, _filters, sorter) => {
              setQuery({
                ...query,
                page: pagination.current,
                order: sorter.order,
                columnKey: sorter.columnKey
              })
            }}
          />
        </div>
        <div style={{ marginTop: 20 }}>
          <p>
            <span>2. Choose A Container to Assign Items</span>
            <ThemeSelect
              style={{ width: 150, margin: '0 20px' }}
              value={selectedContainer}
              onChange={(value: string) => setSelectedContainer(value)}
              placeholder='Please Select'
            >
              {containerList.map((v: any, index: number) => {
                return (
                  <Select.Option value={v.id} key={v.id}>
                    {v.no}
                  </Select.Option>
                )
              })}
            </ThemeSelect>
            <ThemeButton loading={createNewContainerLoading} onClick={handleCreateContainer}>Create A New Container</ThemeButton>
          </p>
        </div>
      </ThemeSpin>
      {createNewContainerModalVisible && <CreateNewContainerModal onCancel={() => setCreateNewContainerModalVisible(false)} company={company} orderId={currentOrderId} callback={fetchAssignModalContainers} />}
      {/* <CreateNewContainerModal /> */}
    </WrapModal>
  )
}

const WrapTable = styled(Table)`
  .ant-table-tbody > tr > td {
    padding: 5px 16px
  }
`

export default ContainerAssignModal
