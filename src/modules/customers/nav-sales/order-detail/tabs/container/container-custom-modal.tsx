import React, { FC, useEffect, useState } from 'react'
import { Col, Form, Row } from 'antd'
import { Wrap, WrapModal } from './container-detail-modal'
import { ThemeButton, ThemeInput, ThemeSpin } from '~/modules/customers/customers.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkError } from '~/common/utils'

const ContainerCustomFields: FC<any> = ({ form, onCancel, currentContainerId, callback, isSingle }) => {
  const [loading, setLoading] = useState(false)
  const [saveLoading, setSaveLoading] = useState(false)
  const [container, setContainer] = useState<any>({})
  const { getFieldDecorator, validateFieldsAndScroll } = form

  useEffect(() => {
    setLoading(true)
    if (isSingle) {
      OrderService.instance.getWholesaleContainer(currentContainerId).subscribe({
        next(resp) {
          setContainer({
            ...resp.body.data,
            tempRecorderList: resp.body.data.tempRecorderList ? JSON.parse(resp.body.data.tempRecorderList) : []
          })
        },
        error(err) { checkError(err) },
        complete() {
          setLoading(false)
        }
      })
    } else {
      OrderService.instance.getContainerDetail(currentContainerId).subscribe({
        next(resp) {
          setContainer(resp.data)
        },
        error(err) { checkError(err) },
        complete() {
          setLoading(false)
        }
      })
    }
    
  }, [currentContainerId, isSingle])

  const handleSave = () => {
    setSaveLoading(true)
    if (isSingle) {
      validateFieldsAndScroll((_err: any, values: any) => {
        const data = {
          ...values,
          id: container.containerId,
        }
        OrderService.instance.updateWholesaleContainer(data).subscribe({
          next(resp) {
            callback()
          },
          error(err) { checkError(err) },
          complete() {
            setSaveLoading(false)
            onCancel()
          }
        })
      })
    } else {
      validateFieldsAndScroll((_err: any, values: any) => {
        const data = {
          ...container,
          ...values
        }
        OrderService.instance.updateContainerDetail(data).subscribe({
          next(resp) {
            callback()
          },
          error(err) { checkError(err) },
          complete() {
            setSaveLoading(false)
            onCancel()
          }
        })
      })
    }
  }

  return (
    <WrapModal
      title={`Edit ${container.no} Customer Fields`}
      onCancel={onCancel}
      visible
      footer={[
        <ThemeButton key={1} onClick={handleSave} loading={saveLoading}>
          Save
        </ThemeButton>,
        <span key={2} style={{ marginLeft: 20, cursor: 'pointer'  }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <ThemeSpin spinning={loading}>
        <Wrap>
          <Form form={form}>
            <Row gutter={[32, 16]}>
              <Col span={24}>
                <Form.Item label='SCAC'>
                  {getFieldDecorator('scac', {
                    initialValue: container.scac,
                  })(<ThemeInput placeholder="SCAC" />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label='DODAAC'>
                  {getFieldDecorator('dodaac', {
                    initialValue: container.dodaac,
                  })(<ThemeInput placeholder="DODAAC" />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label='Contractual RDD'>
                  {getFieldDecorator('contractualRdd', {
                    initialValue: container.contractualRdd,
                  })(<ThemeInput placeholder="Contractual RDD" />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label='Container TCN'>
                  {getFieldDecorator('tcn', {
                    initialValue: container.tcn,
                  })(<ThemeInput placeholder="TCN" />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label='POE'>
                  {getFieldDecorator('poe', {
                    initialValue: container.poe,
                  })(<ThemeInput placeholder="POE" />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label='POD'>
                  {getFieldDecorator('pod', {
                    initialValue: container.pod,
                  })(<ThemeInput placeholder="POD" />)}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Wrap>
      </ThemeSpin>
    </WrapModal>
  )
}

const WrappedContainerCustomFieldsModal = Form.create()(ContainerCustomFields)

export default WrappedContainerCustomFieldsModal
