import React, { FC, useEffect, useState } from 'react'
import styled from '@emotion/styled'
import { Col, Divider, Form, Icon, Row, Select } from 'antd'
import { ThemeButton, ThemeInput, ThemeModal, ThemeSelect, ThemeSpin } from '~/modules/customers/customers.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkError } from '~/common/utils'
import _ from 'lodash'

const { Option } = Select

const ContainerDetail: FC<any> = ({ form, onCancel, currentContainerId, callback, setCompanyProductType, companyProductTypes, isSingle }) => {
  const [loading, setLoading] = useState(false)
  const [saveLoading, setSaveLoading] = useState(false)
  const [container, setContainer] = useState<any>({})
  const [addContainerTypeModalVisible, setAddContainerTypeModalVisible] = useState(false)
  const { getFieldDecorator, validateFieldsAndScroll, validateFields } = form

  useEffect(() => {
    setLoading(true)
    if (isSingle) {
      OrderService.instance.getWholesaleContainer(currentContainerId).subscribe({
        next(resp) {
          setContainer({
            ...resp.body.data,
            tempRecorderList: resp.body.data.tempRecorderList ? JSON.parse(resp.body.data.tempRecorderList) : []
          })
        },
        error(err) { checkError(err) },
        complete() {
          setLoading(false)
        }
      })
    } else {
      OrderService.instance.getContainerDetail(currentContainerId).subscribe({
        next(resp) {
          setContainer({
            ...resp.data,
            tempRecorderList: resp.data.tempRecorderList || []
          })
        },
        error(err) { checkError(err) },
        complete() {
          setLoading(false)
        }
      })
    }
  }, [currentContainerId, isSingle])

  const handleSave = () => {
    if (isSingle) {
      validateFields((err: any, values: any) => {
        if (err) return 
        setSaveLoading(true)
        const data = {
          ...container,
          ...values,
          tempRecorderList: JSON.stringify(container.tempRecorderList),
          containerType: {
            id: values.containerTypeId
          },
          id: container.containerId,
          status: container.containerStatus
        }

        delete data.containerStatus
        delete data.containerId
        delete data.bookingNo
        delete data.logisticOrderId
        delete data.logisticOrderName
        delete data.pcfn
        delete data.etdDate
        delete data.containerTypeId

        OrderService.instance.updateWholesaleContainer(data).subscribe({
          next(resp) {
            callback()
          },
          error(err) { checkError(err) },
          complete() {
            setSaveLoading(false)
            onCancel()
          }
        })
      })
    } else {
      validateFields((err: any, values: any) => {
        if (err) return 
        setSaveLoading(true)
        const data = {
          ...container,
          ...values
        }
        OrderService.instance.updateContainerDetail(data).subscribe({
          next(resp) {
            callback()
          },
          error(err) { checkError(err) },
          complete() {
            setSaveLoading(false)
            onCancel()
          }
        })
      })
    }
  }

  const handleAddTemperature = () => {
    setContainer({
      ...container,
      tempRecorderList: [...container.tempRecorderList, { no: '', temp: '' }]
    })
  }

  const handleChangeTemp = (index, field, value) => {
    setContainer({
      ...container,
      tempRecorderList: container.tempRecorderList.map((v, i) => i === index ? ({ ...v, [field]: value }) : v)
    })
  }

  return (
    <WrapModal
      title={`Edit ${container.no} Details`}
      onCancel={onCancel}
      width={800}
      visible
      footer={[
        <ThemeButton key={1} onClick={handleSave} loading={saveLoading}>
          Save
        </ThemeButton>,
        <span key={2} style={{ marginLeft: 20 }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <ThemeSpin spinning={loading}>
        <Wrap>
          <Form
            form={form}
          >
            <Row gutter={[32, 16]} type='flex'>
              <Col span={12}>
                <Form.Item label='Container No. '>
                  {getFieldDecorator('no', {
                    initialValue: container.no,
                  })(<ThemeInput placeholder="Enter Container No. " />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='Seal No.'>
                  {getFieldDecorator('sealNo', {
                    initialValue: container.sealNo,
                  })(<ThemeInput placeholder="Enter Seal No." />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='Container Type'>
                  {getFieldDecorator('containerTypeId', {
                    initialValue: container.containerTypeId ? Number(container.containerTypeId) : undefined,
                  })(
                    <ThemeSelect
                      showSearch
                      filterOption={(input: any, option: any) => {
                        if (option.props.children) {
                          return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        } else {
                          return true
                        }
                      }}
                      style={{ fontWeight: 'normal' }}
                      dropdownMatchSelectWidth={false}
                      dropdownRender={(el: any) => {
                        return (
                          <div>
                            {el}
                            <div
                              style={{ padding: '4px 8px', cursor: 'pointer' }}
                              onMouseDown={(e) => e.preventDefault()}
                              onClick={(e) => setAddContainerTypeModalVisible(true)}
                            >
                              <Icon type="plus" /> Add New
                            </div>
                          </div>
                        )
                      }}
                      placeholder="Select a Container Type"
                    >
                      {companyProductTypes.containerType.map(v => (
                        <Option value={v.id} key={v.id}>{v.name}</Option>
                      ))}
                    </ThemeSelect>
                  )}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='Length'>
                  {getFieldDecorator('length', {
                    initialValue: container.length,
                  })(<ThemeInput placeholder="Enter Length" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='Container Max. Weight (LBs)'>
                  {getFieldDecorator('maxWeight', {
                    initialValue: container.maxWeight,
                    rules: [
                      {
                        pattern: new RegExp('^[0-9\.\-]{1,}$','g'),
                        message: 'please input number'
                      }
                    ]
                  })(<ThemeInput placeholder="Enter Container Max. Weight (LBs)" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='Container Max. Volume (ft³)'>
                  {getFieldDecorator('maxVolume', {
                    initialValue: container.maxVolume,
                    rules: [
                      {
                        pattern: new RegExp('^[0-9\.\-]{1,}$','g'),
                        message: 'please input number'
                      }
                    ]
                  })(<ThemeInput placeholder="Enter Container Max. Volume (ft³)" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='Tare Weight (LBs)'>
                  {getFieldDecorator('tareWeight', {
                    initialValue: container.tareWeight,
                    rules: [
                      {
                        pattern: new RegExp('^[0-9\.\-]{1,}$','g'),
                        message: 'please input number'
                      }
                    ]
                  })(<ThemeInput placeholder="Enter Tare Weight (LBs)" />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                {
                  _.get(container, 'tempRecorderList', []).map((v, i) => (
                    <div key={i}>
                      <Col span={12} className='noPadding'>
                        <Form.Item label={`Temp Recorder (${i + 1}) No. `}>
                          <ThemeInput placeholder="placeholder" value={v.no} onChange={(e) => handleChangeTemp(i, 'no', e.target.value)} />
                        </Form.Item>
                      </Col>
                      <Col span={12} className='noPadding'>
                        <Form.Item label={`Temp Recording ${i + 1} (°F)`}>
                          <ThemeInput placeholder="placeholder" value={v.temp} onChange={(e) => handleChangeTemp(i, 'temp', e.target.value.replace(/[^\d.]/g,''))} />
                        </Form.Item>
                      </Col>
                    </div>
                  ))
                }
                <div onClick={handleAddTemperature}>
                  + Add New Temperature Recorder
                </div>
              </Col>
              <Col span={12}>
                <Form.Item label='Container Temperature Setting (°F)'>
                  {getFieldDecorator('temperatureSetting', {
                    initialValue: container.temperatureSetting,
                    rules: [
                      {
                        pattern: new RegExp('^[0-9\.\-]{1,}$','g'),
                        message: 'please input number'
                      }
                    ]
                  })(<ThemeInput placeholder="Enter Container Temperature Setting (°F)" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='Vent Setting'>
                  {getFieldDecorator('ventSetting', {
                    initialValue: container.ventSetting,
                    rules: [
                      {
                        pattern: new RegExp('^[0-9\.\-]{1,}$','g'),
                        message: 'please input number'
                      }
                    ]
                  })(<ThemeInput placeholder="Enter Vent Setting" />)}
                </Form.Item>
              </Col>
              <Col span={10}>
                <Form.Item label='Control Atmosphere'>
                  {getFieldDecorator('controlAtmosphere', {
                    initialValue: container.controlAtmosphere,
                    rules: [
                      {
                        pattern: new RegExp('^[0-9\.\-]{1,}$','g'),
                        message: 'please input number'
                      }
                    ]
                  })(<ThemeInput placeholder="Enter Control Atmosphere" />)}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Wrap>
      </ThemeSpin>
      {addContainerTypeModalVisible && <AddContainerTypeModal setCompanyProductType={setCompanyProductType} onCancel={() => setAddContainerTypeModalVisible(false)} />}
    </WrapModal>
  )
}

const AddContainerTypeModal = ({ onCancel, setCompanyProductType }) => {
  const [value, setValue] = useState('')
  const handleSave = () => {
    setCompanyProductType({
      type: 'containerType',
      name: value
    })
    onCancel()
  }
  return (
    <WrapModal
      title='Add New Container Type'
      visible
      onCancel={onCancel}
      footer={[
        <ThemeButton onClick={handleSave}>Save</ThemeButton>,
        <span style={{ marginLeft: 15, cursor: 'pointer' }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <div>Container Type</div>
      <ThemeInput value={value} onChange={e => setValue(e.target.value)} />
    </WrapModal>
  )
}

export const Wrap = styled.div`
  .ant-form-item {
    margin-bottom: 0
  }
  .ant-form-item-label {
    font-weight: normal
  }
  .noPadding {
    padding: 8px 16px 8px 0 !important;
  }
`
export const WrapModal = styled(ThemeModal)`
  .ant-modal-footer {
    text-align: left;
  }
`

const WrappedContainerDetailModal = Form.create()(ContainerDetail)

export default WrappedContainerDetailModal
