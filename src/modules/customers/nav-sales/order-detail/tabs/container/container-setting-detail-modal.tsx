import React, { FC, useEffect, useState } from 'react'
import { Col, DatePicker, Form, Row, TimePicker } from 'antd'
import { Wrap, WrapModal } from './container-detail-modal'
import { ThemeButton, ThemeInput, ThemeSpin } from '~/modules/customers/customers.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkError } from '~/common/utils'
import moment from 'moment'
import _ from 'lodash'

const ContainerDetailSetting: FC<any> = ({ form, onCancel, currentContainerId, callback, isSingle }) => {
  const [loading, setLoading] = useState(false)
  const [saveLoading, setSaveLoading] = useState(false)
  const [container, setContainer] = useState<any>({})
  const { getFieldDecorator, validateFieldsAndScroll } = form

  useEffect(() => {
    setLoading(true)
    if (isSingle) {
      OrderService.instance.getWholesaleSailing(currentContainerId).subscribe({
        next(resp) {
          const { body } = resp
          if (!body.data) return 
          setContainer({
            ...body.data,
            etdDate1: body.data.etdDate ? moment(body.data.etdDate) : '',
            etdTime1: body.data.etdDate ? moment(body.data.etdDate) : '',
            etaDate1: body.data.etaDate ? moment(body.data.etaDate) : '',
            etaTime1: body.data.etaDate ? moment(body.data.etaDate) : ''
          })
        },
        error(err) { checkError(err) },
        complete() {
          setLoading(false)
        }
      })
    } else {
      OrderService.instance.getLogisticOrder(currentContainerId).subscribe({
        next(resp) {
          const { data } = resp
          if (!data) return 
          setContainer({
            ...data,
            etdDate1: data.etdDate ? moment(data.etdDate) : '',
            etdTime1: data.etdDate ? moment(data.etdDate) : '',
            etaDate1: data.etaDate ? moment(data.etaDate) : '',
            etaTime1: data.etaDate ? moment(data.etaDate) : ''
          })
        },
        error(err) { checkError(err) },
        complete() {
          setLoading(false)
        }
      })
    }
  }, [currentContainerId, isSingle])

  const handleSave = () => {
    setSaveLoading(true)
    validateFieldsAndScroll((_err: any, values: any) => {
      const data = {
        ...container,
        ...values,
        etdDate: moment(`${values['etdDate1'] && moment(values['etdDate1']).format('YYYY-MM-DD')} ${values['etdTime1'] && moment(values['etdTime1']).format('HH:mm')}`).valueOf(),
        etaDate: moment(`${values['etaDate1'] && moment(values['etaDate1']).format('YYYY-MM-DD')} ${values['etaTime1'] && moment(values['etaTime1']).format('HH:mm')}`).valueOf(),
      }
      delete data.etdDate1
      delete data.etdTime1
      delete data.etaDate1
      delete data.etaTime1

      if (isSingle) {
        OrderService.instance.updateWholesaleSailing(data).subscribe({
          next(resp) {
            callback()
          },
          error(err) { checkError(err) },
          complete() {
            setSaveLoading(false)
            onCancel()
          }
        })
      } else {
        OrderService.instance.updateLogisticOrder(data).subscribe({
          next(resp) {
            callback()
          },
          error(err) { checkError(err) },
          complete() {
            setSaveLoading(false)
            onCancel()
          }
        })
      }
    })
  }

  return (
    <WrapModal
      title={`Edit Sailing Details for ${_.get(container, 'containers[0].no', '')}`}
      onCancel={onCancel}
      visible
      footer={[
        <ThemeButton key={1} onClick={handleSave} loading={saveLoading}>
          Save
        </ThemeButton>,
        <span key={2} style={{ marginLeft: 20, cursor: 'pointer' }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <ThemeSpin spinning={loading}>
        <Wrap>
          <Form>
            <Row gutter={[32, 16]}>
              <Col span={24}>
                <Form.Item label='Vessel Name'>
                  {getFieldDecorator('name', {
                    initialValue: container.name,
                  })(<ThemeInput placeholder="Vessel Name" />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label='Booking No. '>
                  {getFieldDecorator('bookingNo', {
                    initialValue: container.bookingNo,
                    rules: [
                      {
                        pattern: new RegExp('^[0-9\.\-]{1,}$','g'),
                        message: 'please input number'
                      }
                    ]
                  })(<ThemeInput placeholder="Booking No." />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='ETD Date'>
                  {getFieldDecorator('etdDate1', {
                    initialValue: container.etdDate1,
                  })(<DatePicker format={'YYYY-MM-DD'} placeholder='Departure Date'/>)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='ETD Time'>
                  {getFieldDecorator('etdTime1', {
                    initialValue: container.etdTime1,
                  })(<TimePicker format={'HH:mm'} placeholder='Departure Time' />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='ETA Date'>
                  {getFieldDecorator('etaDate1', {
                    initialValue: container.etaDate1,
                  })(<DatePicker format={'YYYY-MM-DD'} placeholder='Arrival Date' />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label='ETA Time'>
                  {getFieldDecorator('etaTime1', {
                    initialValue: container.etaTime1,
                  })(<TimePicker format={'HH:mm'} placeholder='Arrival Time' />)}
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item label='PCFN'>
                  {getFieldDecorator('pcfn', {
                    initialValue: container.pcfn,
                  })(<ThemeInput placeholder="Port Call File Number" />)}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Wrap>
      </ThemeSpin>
    </WrapModal>
  )
}


const WrappedContainerDetailSettingModal = Form.create()(ContainerDetailSetting)

export default WrappedContainerDetailSettingModal
