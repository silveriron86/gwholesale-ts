import React, { FC, useState } from 'react'
import { WrapModal } from './container-detail-modal'
import { Form, DatePicker } from 'antd'
import { ThemeButton, ThemeInput } from '~/modules/customers/customers.style'
import moment from 'moment'
import { OrderService } from '~/modules/orders/order.service'
import { checkError } from '~/common/utils'

interface Iprops {
  onCancel: Function
  form: any
  company: any,
  callback: any
  orderId?: string
}

const CreateContainerFields: FC<Iprops> = ({
  onCancel,
  form,
  company,
  callback,
  orderId
}) => {
  const [saveLoading, setSaveLoading] = useState(false)
  const { getFieldDecorator, validateFieldsAndScroll } = form
  const nextMon = moment().add(7 - moment().day() + 1, 'days').format('YYYY-MM-DD')

  const handleSave = () => {
    validateFieldsAndScroll((err: any, values: any) => {
      if (!err) {
        setSaveLoading(true)
        const data = {
          no: values.no,
          company,
          oms: orderId,
          etaDate: moment(values['etdDate']).add(14, 'days').startOf('week').add('3', 'days').format('YYYY-MM-DD'),
          etdDate: moment(values['etdDate']).format('YYYY-MM-DD')
        }
        if (!orderId) {
          delete data.oms
        }

        OrderService.instance.createContainer(data).subscribe({
          next(resp) { 
            onCancel()
            callback()
          },
          error(err) { checkError(err) },
        })
      }
    })
  }

  return (
    <WrapModal
      title='Create a new container'
      visible
      onCancel={onCancel}
      footer={[
        <ThemeButton key={1} onClick={handleSave} loading={saveLoading}>
          Create container
        </ThemeButton>,
        <span key={2} style={{ marginLeft: 20, cursor: 'pointer' }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <Form form={form}>
        <Form.Item label='Container no. *'>
          {getFieldDecorator('no', {
            initialValue: '',
            rules: [{ required: true, message: 'Container no. is required' }],
          })(<ThemeInput placeholder="Container no." />)}
        </Form.Item>
        <Form.Item label='ETD date (est. time of departure) *'>
          {getFieldDecorator('etdDate', {
            initialValue: moment.utc(nextMon),
          })(<DatePicker format={'MM/DD/YY'} placeholder='ETD Date' />)}
        </Form.Item>
      </Form>
    </WrapModal>
  )
}

const CreateNewContainerModal = Form.create()(CreateContainerFields)

export default CreateNewContainerModal

