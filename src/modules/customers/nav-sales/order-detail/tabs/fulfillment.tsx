import * as React from 'react'
import {
  ThemeSpin,
} from '~/modules/customers/customers.style'
import { Flex } from '~/modules/orders/order-detail.style'
import { FulfillmentWrapper } from '../../styles'
import FulfillmentForm from './fulfillment_form'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import moment from 'moment'

type FulfillmentProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
  }
export default class Fulfillment extends React.PureComponent<FulfillmentProps> {
  componentDidMount() {
    this.props.getAllRoutes()
    this.props.setAddressLoading()
    this.props.getAddresses()
    this.props.getCompanyUsers()
    this.props.getCompanyProductAllTypes()
    this.props.getCompanyShippingAddresses()
    if (this.props.currentOrder) {
      this.props.getContacts(this.props.currentOrder.wholesaleClient.clientId.toString())
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.currentOrder && nextProps.currentOrder &&
      this.props.currentOrder.wholesaleOrderId != nextProps.currentOrder.wholesaleOrderId) {
      console.log('get contacts in will receive')
      this.props.getContacts(nextProps.currentOrder.wholesaleClient.clientId.toString())
    }
  }

  clean = (obj: any) => {
    for (var propName in obj) {
      if (obj[propName] === null) {
        delete obj[propName];
      }
    }
    return obj
  }

  onSave = (arg: any) => {
    const { currentOrder } = this.props
    if (currentOrder) {
      const data = this.clean({ ...arg })
      data.wholesaleOrderId = currentOrder.wholesaleOrderId
      data.deliveryDate = moment(currentOrder.deliveryDate).format('MM/DD/YYYY')
      data.wholesaleCustomerClientId = currentOrder.wholesaleClient.clientId
      data.status = currentOrder.wholesaleOrderStatus

      if (data.warehousePickupTime !== 'null' && data.warehousePickupTime) {
        data.warehousePickupTime = moment(data.warehousePickupTime).format('hh:mm A')
      }

      this.props.resetLoading()
      this.props.updateOrderInfo(data)
    }
  }

  render() {
    const { currentOrder, loadingAddress, loadingCurrentOrder } = this.props
    if (!currentOrder) {
      return null
    }

    return (
      <ThemeSpin spinning={loadingCurrentOrder || loadingAddress}>
        <FulfillmentWrapper>
          <div className="tab-header">
            <Flex className="v-center">
              <h3>Fulfillment</h3>
            </Flex>
          </div>
          <FulfillmentForm {...this.props} onSave={this.onSave} isCashSale={this.props.isCashSale ?? false}/>
        </FulfillmentWrapper>
      </ThemeSpin>
    )
  }
}
