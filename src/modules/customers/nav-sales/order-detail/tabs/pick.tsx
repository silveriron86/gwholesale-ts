import { ClassNames } from '@emotion/core'
import { Button, Icon, Select, Tooltip, Switch, Input, Popconfirm, notification } from 'antd'
import _, { cloneDeep } from 'lodash'
import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { checkError, formatNumber, getDefaultUnit, inventoryQtyToRatioQty, isFloat, judgeConstantRatio, mathRoundFun, ratioQtyToInventoryQty, responseHandler } from '~/common/utils'
import { ThemeButton, ThemeInputNumber, ThemeModal, ThemeSelect, ThemeSpin, ThemeSwitchWrap, ThemeTable } from '~/modules/customers/customers.style'
import { OrdersDispatchProps, OrdersModule, OrdersStateProps } from '~/modules/orders'
import { GlobalState } from '~/store/reducer'
import { CustomButton, DialogSubContainer, Flex, ItemHMargin4, noPaddingFooter, SalesOrderPickWrapper, TabHeaderBigSpan, TableHeaderItem, Unit } from '../../styles'
import PickedQuantity from './../modals/picked-quanitty'
import SwitchTooltip from '~/components/SwitchTooltip'
import { OrderService } from '~/modules/orders/order.service'
import { UserRole } from '~/schema'

type SalesOrderPickProps = OrdersStateProps & OrdersDispatchProps & {
  shipOrder: Function
}

class SalesOrderPick extends React.PureComponent<SalesOrderPickProps> {
  pickedQuantityRef = React.createRef<any>()
  state = {
    updatingRecordId: -1,
    openPickedQuantityModal: false,
    currentRecord: null,
    isCashSales: location.hash.includes('cash-sales'),
    lotSpecificationModalVisible: false,
    syncContainerButtonLoading: false
  }

  get validateOrderStatusIsShipped() {
    if (this.props.currentOrder && this.props.currentOrder.wholesaleOrderStatus == 'SHIPPED') {
      return true
    }
    return false
  }

  confirmCopyOrderQty() {
    this.props.startUpdating()
    this.props.updateAutofillPicked({ orderId: this.props.match.params.orderId })
  }

  columns: any[] = [
    {
      title: 'Item',
      dataIndex: 'variety',
      align: 'left',
      width: 300,
    },
    {
      title: 'Brand',
      dataIndex: 'modifiers',
      align: 'left',
      width: 80,
      render: (modifiers: string, record: any) => {
        return record.lotId ? modifiers : ''
      },
    },
    {
      title: 'Origin',
      dataIndex: 'extraOrigin',
      align: 'left',
      width: 80,
      render: (extraOrigin: string, record: any) => {
        return record.lotId ? extraOrigin : ''
      },
    },
    {
      title: `${this.state.isCashSales ? 'Lot' : 'Lots'}`,
      dataIndex: 'lotIds',
      align: 'left',
      width: 150,
      render: (value: string[], record: any) => {
        if (!this.state.isCashSales) {
          return _.get(record, 'lotIds', []).map(lotId => (
            <span style={{ display: 'block' }} key={lotId}>{lotId}</span>
          ))
        } else {
          return record.lotId
        }
      }
    },
    {
      title: 'Order Qty',
      dataIndex: 'quantity',
      align: 'left',
      width: 150,
      render: (quantity: number, record: any, index: number) => {
        return <div>{formatNumber(quantity, 2)} <span style={{ paddingLeft: 4 }}>{record.UOM}</span></div>
      }
    },
    {
      title: 'Locations',
      dataIndex: 'locations',
      align: 'left',
      width: 200,
      render: (locationNames: any, record: any) => {
        const data = record.locationNames && record.locationNames.length ? record.locationNames.slice(0, 3) : [];
        return data.length ? (
          <Tooltip title={record.locationNames.length > 3 ? record.locationNames.join(', ') : ''}>
            <div>{data.join(', ')}{record.locationNames.length > 3 ? '...' : ''}</div>
          </Tooltip>
        ) : ''
      }
    },
    {
      title: (
        <>
          Picked Qty
          {!this.validateOrderStatusIsShipped && <Popconfirm
            title={
              <div style={{ width: 200 }}>
                Auto-fill picked quantities to match the order quantities (non-catchweight items only).
              </div>
            }
            okText="Auto-fill quantities"
            onConfirm={() => this.confirmCopyOrderQty()}
          >
            <Icon type="copy" style={ItemHMargin4} className="copy-order-qty" />
          </Popconfirm>}
        </>
      ),
      dataIndex: 'picked',
      align: 'left',
      width: 150,
      render: (picked: number, record: any, index: number) => {

        let tooltipText = ''
        if (record.picked !== null && record.picked > 0 && record.quantity !== null && record.quantity >= 0) {
          if (parseFloat(record.picked) > parseFloat(record.quantity)) {
            tooltipText = `Overpicked by ${record.picked - record.quantity} ${record.UOM}`
          } else if (parseFloat(record.picked) < parseFloat(record.quantity)) {
            tooltipText = `Short by ${record.quantity - record.picked} ${record.UOM}`
          }
        }
        const warningClass = tooltipText ? 'warning' : ''
        let showCatchWeightModal = false
        let constantRatio = judgeConstantRatio(record)
        let customBtn = null
        const parsingFailed = localStorage.getItem(`PARSING_FAILED_${record.wholesaleOrderItemId.toString()}`)
        if (!constantRatio) {
          showCatchWeightModal = true
          const readOnly = this.props.currentOrder && this.props.currentOrder.wholesaleOrderStatus == 'CANCEL' ? true : false
          customBtn = (
            <CustomButton
              style={{ width: 70, textAlign: 'right', paddingRight: 11, flex: 'unset' }}
              className={`${record.picked ? '' : 'empty'} ${warningClass} ${parsingFailed ? 'warning' : ''}`}
              onClick={() => this.openPickedQuantity(record)}
              disabled={readOnly}
            >
              {record.picked ? record.picked : <span>&nbsp;</span>}
            </CustomButton>
          )
        }

        if (showCatchWeightModal && parsingFailed) {
          if (tooltipText) {
            tooltipText += ', '
          }
          tooltipText += 'Scanning errors found!'
        }
        return (
          <Flex className="v-center">
            <Tooltip placement="top" title={tooltipText}>
              <div style={{ width: 70 }}>
                {showCatchWeightModal ? (
                  customBtn
                ) : (
                  this.renderPickedInput(record, picked, warningClass)
                )}
              </div>
            </Tooltip>
            <Unit>{record.UOM}</Unit>
          </Flex>
        )
      }
    },
    {
      title: 'Billable Qty',
      dataIndex: 'catchWeightQty',
      align: 'left',
      width: 150,
      render: (weight: number, record: any) => {
        if (_.isEmpty(record)) {
          return { props: { colSpan: 0 } }
        }

        let showBillableQty,
          showValue = false
        if (record.picked || record.catchWeightQty) {
          showValue = true
        }

        let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
        let unitUOM = record.UOM

        let constantRatio = judgeConstantRatio(record)
        if (record.oldCatchWeight) {
          showBillableQty = record.catchWeightQty
        } else {
          if (!constantRatio) {
            showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
          } else {
            showBillableQty = inventoryQtyToRatioQty(
              pricingUOM,
              ratioQtyToInventoryQty(unitUOM, record.picked || 0, record, 12),
              record,
            )
          }
        }

        return (
          <Flex className="v-center">
            <div style={{ textAlign: 'right', minWidth: 60 }}>{showValue ? mathRoundFun(showBillableQty, 4) : ''}</div>
            <Unit style={{ flex: 'unset' }}>{pricingUOM}</Unit>
          </Flex>
        )
      },
    },
    {
      title: 'Commodity Class',
      dataIndex: 'commodityClass'
    },
    {
      title: 'Total Weight',
      dataIndex: 'grossWeight',
      align: 'left',
      width: 150,
      render: (grossWeight: number, record: any) => {
        return (
          <div>
            {/* {grossWeight || 0} {_.get(record, 'grossWeightUnit', getDefaultUnit('WEIGHT', this.props.sellerSetting))} */}
            {(isFloat(grossWeight) ? formatNumber(grossWeight, 2) : grossWeight) || 0} {_.get(this.props.sellerSetting, 'company.weightUOM')}
            {/* <Icon
              style={{ marginLeft: 10 }}
              type="setting"
              className="price-setting"
              onClick={() => {
                this.setState({
                  currentRecord: record,
                  lotSpecificationModalVisible: true
                })
              }}
            /> */}
          </div>
        )
      }
    },
    {
      title: 'Total Volume',
      dataIndex: 'grossVolume',
      align: 'left',
      width: 150,
      render: (grossVolume: number, record: any) => {
        return (
          <div>
            {/* {grossVolume || 0} {_.get(this.props, 'grossVolumeUnit', getDefaultUnit('VOLUME', this.props.sellerSetting))} */}
            {(isFloat(grossVolume) ? formatNumber(grossVolume, 2) : grossVolume) || 0} {_.get(this.props.sellerSetting, 'company.volumeUOM')}
            <Icon
              style={{ marginLeft: 10 }}
              type="setting"
              className="price-setting"
              onClick={() => {
                this.setState({
                  currentRecord: record,
                  lotSpecificationModalVisible: true
                })
              }}
            />
          </div>
        )
      }
    },
    {
      title: 'Status',
      dataIndex: 'status',
      align: 'left',
      width: 100,
      // sorter: (a: any, b: any) => onSortString('status', a, b),
      render: (status: string, record: any) => {
        if (_.isEmpty(record)) {
          return { props: { colSpan: 0 } }
        }

        const statues = ['New', 'Picking', 'Shipped']

        let formattedStatus = status
        if (status == 'PLACED') {
          formattedStatus = 'NEW'
        }
        return (
          <div className="select-container-parent" style={{ width: 110 }}>
            {record.status != 'SHIPPED' ? (
              <ThemeSelect
                onChange={(val: any) => this.handleChangeItemStatus(record, 'status', val)}
                className={`status-selection orderItemId-${record.wholesaleOrderItemId}`}
                value={formattedStatus}
                suffixIcon={<Icon type="caret-down" />}
                style={{ width: '100%', borderRadius: 20 }}
                dropdownRender={(el: any) => {
                  return <div className="sales-cart-status-selector">{el}</div>
                }}
              >
                {statues.map((el: string, index: number) => {
                  return (
                    <Select.Option value={el.toUpperCase()} key={index} disabled={el.toUpperCase() == 'SHIPPED'}>
                      <Flex className="v-center">
                        <div className={`status-indicator ${el.toLowerCase()}`} />
                        <div className="value">{el == 'Picking' ? 'Picked' : el}</div>
                      </Flex>
                    </Select.Option>
                  )
                })}
              </ThemeSelect>
            ) : (
              <div className="status-selection not-selector">
                <Flex className="v-center">
                  <div className={`status-indicator shipped`} />
                  <div className="value">Shipped</div>
                </Flex>
              </div>
            )}
          </div>
        )
      },
    },
  ]

  renderPickedInput = (record: any, picked: any, warningClass) => {
    if (this.state.isCashSales) {
      return (
        <ThemeSpin spinning={this.state.updatingRecordId == record.wholesaleOrderItemId && this.props.updatingField} size={'small'} >
          <ThemeInputNumber
            style={{ textAlign: 'right', marginRight: 4 }}
            className={`${warningClass} no-stepper`}
            step={1}
            value={
              picked === null ||
                typeof picked == 'undefined' ||
                (picked == 0 && (record.status == 'PLACED' || record.status == 'NEW'))
                ? ''
                : picked
            }
            formatter={(v) => v.match(/^\d*(\.?\d{0,2})/g)[0]}
            onFocus={(e) => e.target.select()}
            onBlur={(e: any) => this.onChangePickedValue(record.wholesaleOrderItemId, e.target.value)}
            disabled={record.status === 'SHIPPED'}
          />
        </ThemeSpin>
      )
    } else {
      return (
        <ThemeInputNumber
          style={{ textAlign: 'right', marginRight: 4 }}
          className={`${warningClass} no-stepper`}
          step={1}
          value={
            picked === null ||
              typeof picked == 'undefined' ||
              (picked == 0 && (record.status == 'PLACED' || record.status == 'NEW'))
              ? ''
              : picked
          }
          onFocus={(e) => e.target.select()}
          formatter={(v) => v.match(/^\d*(\.?\d{0,2})/g)[0]}
          onBlur={(e: any) => this.handleChangeItem({ record, value: e.target.value })}
          disabled={record.status === 'SHIPPED'}
        />
      )
    }
  }

  openPickedQuantity = (currentRecord: any) => {
    this.setState({
      openPickedQuantityModal: true,
      currentRecord
    })
  }

  onClosePickedQuantity = () => {
    this.setState({
      openPickedQuantityModal: false,
      currentRecord: null
    })
  }

  handleChangeItemStatus = (record, field, val) => {
    const status = val === 'NEW' ? 'PLACED' : val
    const { updateOrderQuantityLoading, orderItemByProduct, updateOrderItemByProductId, currentOrder } = this.props
    updateOrderQuantityLoading.fetching = true
    updateOrderQuantityLoading.loading = true
    orderItemByProduct[record.displayOrderProduct][field] = status
    updateOrderItemByProductId({
      orderId: currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod: record.lotAssignmentMethod,
        enableLotOverflow: record.enableLotOverflow,
        wholesaleItemId: record.itemId,
        picked: record.picked,
        quantity: record.quantity,
        isUpdateStatus: true,
        status,
      }
    })
  }

  handleChangeItem = ({ record, value }: any) => {
    const { updateOrderQuantityLoading, orderItemByProduct, updateOrderItemByProductId, currentOrder, pageTotalInfo } = this.props
    if (record['picked'] == value) return
    updateOrderQuantityLoading.fetching = true
    updateOrderQuantityLoading.loading = true
    if (pageTotalInfo.totalPicked) {
      pageTotalInfo.totalPicked = Number(pageTotalInfo.totalPicked) - Number(record['picked']) + Number(value)
    }
    orderItemByProduct[record.displayOrderProduct]['picked'] = value
    orderItemByProduct[record.displayOrderProduct]['status'] = 'PICKING'
    updateOrderItemByProductId({
      orderId: currentOrder.wholesaleOrderId,
      productId: record.itemId,
      displayOrderProduct: record.displayOrderProduct,
      data: {
        lotAssignmentMethod: record.lotAssignmentMethod,
        enableLotOverflow: record.enableLotOverflow,
        wholesaleItemId: record.itemId,
        picked: value === '' ? 0 : value,
        quantity: record.quantity,
        isUpdatePickedQty: true,
        status: value === '' ? 'PICKING' : 'PLACED',
        isFromPickTab: true
      }
    })
  }

  onChangePickedValue = (orderItemId: string, value: any) => {
    const { currentOrder, orderItems } = this.props

    if (currentOrder && currentOrder.wholesaleOrderStatus == 'SHIPPED') {
      return
    }
    if (isNaN(Number(value))) {
      return
    }
    this.setState({
      updatingRecordId: orderItemId
    })
    const curRecord: any = orderItems.find((el: any) => el.wholesaleOrderItemId == orderItemId)
    let data = cloneDeep(curRecord)
    data['picked'] = value ? value : 0

    if (value !== null) {
      data['status'] = 'PICKING'
    } else {
      data['status'] = 'PLACED'
    }

    this.props.handlerOrderItemsRedux(data)
    this.props.startUpdatingField()
    this.props.updateSaleOrderItemWithoutRefresh(data)
  }

  onPickedQuantitySave = () => {
    this.props.startUpdating()
    this.pickedQuantityRef.current.handleSave()
    this.onClosePickedQuantity()
  }

  callback = () => {
    this.props.startUpdating()
    this.props.getOrderItemsById(this.props.currentOrder?.wholesaleOrderId)
  }

  handleSyncToContainer = () => {
    const _this = this
    this.setState({ syncContainerButtonLoading: true })
    OrderService.instance.syncContainer(this.props.currentOrder?.wholesaleOrderId).subscribe({
      next(resp) {
        notification.open({
          message: resp.message,
        })
        _this.setState({ syncContainerButtonLoading: false })
      },
      error(err) { checkError(err) },
    })
  }


  render() {
    const { updatingRecordId, openPickedQuantityModal, currentRecord } = this.state
    const { currentUser, currentOrder, orderItems, orderItemByProduct, isShippedSwitchLoading, isShippedSwitchCheck, loading, disableShip, updateOrderItemByProductId } = this.props
    return (
      <SalesOrderPickWrapper>
        <div style={{ marginRight: 40 }}>
          <TableHeaderItem className="sales-cart-ship-toggle">
            <Flex className="v-center flex-end">
              {this.props.currentOrder?.fulfillmentType === 4 && <Button style={{ marginRight: 20 }} loading={this.state.syncContainerButtonLoading} onClick={this.handleSyncToContainer}>Sync to containers</Button>}
              {(currentUser.accountType !== UserRole.WAREHOUSE) && (
                <>
                  <TabHeaderBigSpan>
                    {currentOrder && currentOrder.wholesaleOrderStatus == 'SHIPPED'
                      ? 'Unship to edit order'
                      : 'Ship and lock order'}
                  </TabHeaderBigSpan>
                  <SwitchTooltip
                    showTip={disableShip}
                    switchProps={{
                      disabled: disableShip,
                      checked: isShippedSwitchCheck,
                      loading: this.props.updateOrderQuantityLoading.fetching || isShippedSwitchLoading,
                      onChange: this.props.shipOrder,
                    }}
                    tooltipProps={{
                      title:
                        'One or more items does not have a lot selected and/or one or more selected lots does not have enough inventory to fulfill the order/picked quantity',
                    }}
                  />
                </>
              )}
            </Flex>
          </TableHeaderItem>
          <ThemeSpin spinning={this.props.updating}>
            <ThemeTable
              style={{ marginTop: 8 }}
              columns={this.columns}
              loading={this.props.getOrderItemsLoading}
              dataSource={this.state.isCashSales ? orderItems : Object.values(orderItemByProduct)}
              pagination={{ pageSize: this.props.pageSize, hideOnSinglePage: true, onChange: page => this.props.setSalesOrderPage(page - 1), current: this.props.page + 1, total: this.props.total }}
            />
          </ThemeSpin>
        </div>
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              title={`Picked Quantities (catchweight item)`}
              visible={openPickedQuantityModal}
              onCancel={this.onClosePickedQuantity}
              okText={'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={this.onPickedQuantitySave}>
                    <ThemeSpin spinning={loading} className="button-spinner" size={'small'} />
                    Save
                  </ThemeButton>
                  <Button onClick={this.onClosePickedQuantity}>CANCEL</Button>
                </DialogSubContainer>
              }
              width={700}
            >
              <PickedQuantity
                record={currentRecord}
                open={openPickedQuantityModal}
                wrappedComponentRef={this.pickedQuantityRef}
                updateOrderItemByProductId={updateOrderItemByProductId}
              />
            </ThemeModal>
          )}
        </ClassNames>
        {this.state.lotSpecificationModalVisible &&
          <LotSpecificationModal
            onCancel={() => this.setState({ lotSpecificationModalVisible: false, currentRecord: null })}
            record={currentRecord}
            orderId={this.props.currentOrder?.wholesaleOrderId}
            sellerSetting={this.props.sellerSetting}
            callback={this.callback}
          />
        }
      </SalesOrderPickWrapper>
    )
  }
}

const LotSpecificationModal = ({ onCancel, record, sellerSetting, orderId, callback }) => {
  const [saveLoading, setSaveLoading] = React.useState(false)
  const [items, setItems] = React.useState(record.items)
  const handleSave = () => {
    setSaveLoading(true)
    const data = items.map(v => ({
      wholesaleOrderItemId: v.wholesaleOrderItemId,
      grossWeight: Number(v.grossWeight) || null,
      grossVolume: Number(v.grossVolume) || null
    }))
    OrderService.instance.updateOrderItemWeightVolume(orderId, data).subscribe({
      next(resp) {
      },
      error(err) { checkError(err) },
      complete() {
        setSaveLoading(false)
        onCancel()
        callback()
      }
    })
  }

  const handleChange = (value, type, wholesaleOrderItemId) => {
    setItems(items.map(v => {
      if (v.wholesaleOrderItemId === wholesaleOrderItemId) {
        return ({ ...v, [type]: value })
      }
      return v
    }))
  }

  return (
    <ThemeModal
      title='Lot Specification'
      onCancel={onCancel}
      visible
      width={600}
      footer={
        <ThemeButton loading={saveLoading} onClick={handleSave}>Save</ThemeButton>
      }
    >
      {items.map(item => (
        <div style={{ marginBottom: 20 }}>
          <p style={{ marginBottom: 10 }}>Lot {item.lotId} {item.variety}</p>
          <Flex>
            <div style={{ marginRight: 50 }}>
              <p style={{ fontWeight: 'normal', marginBottom: 5 }}>Unit Gross Weight - {_.get(sellerSetting, 'company.weightUOM')}</p>
              <Input value={item.grossWeight || ''} onChange={e => handleChange(e.target.value, 'grossWeight', item.wholesaleOrderItemId)} />
            </div>
            <div>
              <p style={{ fontWeight: 'normal', marginBottom: 5 }}>Unit Gross Volume - {_.get(sellerSetting, 'company.volumeUOM')}</p>
              <Input value={item.grossVolume || ''} onChange={e => handleChange(e.target.value, 'grossVolume', item.wholesaleOrderItemId)} />
            </div>
          </Flex>
        </div>
      ))}
    </ThemeModal>
  )
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders }
}

const D = connect(OrdersModule)(mapStateToProps)(SalesOrderPick)
export default D
