import { css } from '@emotion/core'
import styled from '@emotion/styled'
import { Button, Menu, Row } from 'antd'
import { borderRadius } from 'polished'
import {lightGrey, mediumGrey, topLightGrey, white, mediumGrey2, gray01, gray02, darkGrey} from '~/common'
import { ProductTitleTips } from '~/modules/product/components/product.component.style'

/**
 * Override Html DOM
 */
export const OrderListHeaderContainer = styled('div')({
  height: 200,
  border: '1px solid #FEFEFE',
})

export const OrderListBodyContainer = styled('div')({})

export const OrderDetailHeaderContainer = styled('div')({
  paddingLeft: 40,
})

export const OrderDetailContainer = styled('div')({
  paddingLeft: 40,
})

export const PaddingContainer = styled('div')((props: any) => ({
  padding: '12px 40px',
  borderBottom: `1px solid ${topLightGrey}`,
  '& .dark-theme-color': {
    color: props.theme.dark,
  },
  '& .bold': {
    fontWeight: 'bold',
  },
  '&.purchase-order-header': {
    '.bold-blink, input, select': {
      fontWeight: 'normal !important',
      //color: 'rgba(0, 0, 0, 0.65) !important',
      color: `${gray01} !important`,
    },
  },
}))

export const FlexDiv = styled('div')({
  display: 'flex',
  '&.space-between': {
    justifyContent: 'space-between',
  },
  '&.total-values': {
    justifyContent: 'flex-end',
    '.child': {
      textAlign: 'center'
    }
  }
})

export const AlignLeftDiv = styled('div')({
  textAlign: 'left',
})

export const Item = styled('div')((props: any) => ({
  textAlign: 'left',
  margin: '0 12px',
  maxWidth: 300,
  '&.padding32-item': {
    margin: '0 16px',
  },
  '& a.back': {
    //color: 'rgba(0, 0, 0, 0.65)',
    color: gray01,
  },
  '&.left': {
    marginLeft: 0,
  },
  '&.right': {
    marginRight: 12,
  },
  '&.icon-item': {
    i: {
      marginRight: 4,
      cursor: 'pointer',
    },
    marginLeft: -18,
    color: gray01,
  },
  '&.header-btns': {
    '.ant-btn': {
      span: {
        fontSize: 15,
        fontFamily: 'Museo Sans Rounded',
        fontWeight: 'bold',
      },
    },
  },
  '&.quantity-item': {
    width: 100,
    whiteSpace: 'nowrap',
    '&.add-quantity': {
      color: props.theme.primary,
      cursor: 'pointer',
      paddingTop: 35,
    },
    '&.add-quantity:active': {
      color: props.theme.dark,
    },
    '.close-quantity': {
      display: 'none',
      cursor: 'pointer'
    },
    '&:hover': {
      '.close-quantity': {
        display: 'block',
        cursor: 'pointer',
      },
    },
  },
  '.icon-edit-order-no': {
    visibility: 'hidden',
    marginLeft: 8,
    cursor: 'pointer',
    '& path': {
      fill: `${props.theme.dark} !important`,
    },
  },
  '&.custom-order-no-enabled': {
    maxWidth: 500,
    '.custom-order-input': {
      flex: 2,
      marginRight: 8,
      '&.pad40 .ant-input': {
        paddingLeft: 40,
      },
      '&.pad50 .ant-input': {
        paddingLeft: 50,
      },
    },
  },
  '&.custom-order-no-enabled:hover': {
    '.icon-edit-order-no': {
      visibility: 'visible',
    },
  },
}))

export const OrderInfoItem = styled('div')({
  textAlign: 'left',
  margin: '0 13px',
  minWidth: 250,
  maxWidth: 625,
  '&.left': {
    marginLeft: 0,
  },
  '&.right': {
    marginRight: 0,
  },
  '& .note-text': {
    height: 48,
  },
})

export const InputLabel = styled('div')({
  color: gray01,
  fontSize: 12,
  lineHeight: '12px',
  fontWeight: 'bold',
  letterSpacing: '0.05em',
  alignItems: 'center',
  padding: '8px 0',
})

export const ValueLabel = styled('span')((props: any) => ({
  fontSize: 20,
  fontWeight: 400,
  lineHeight: '23px',
  marginTop: 8,
  '&.bold': {
    fontWeight: 'bold'
  },
  '&.small': {
    //color: 'rgba(0, 0, 0, 0.65)',
    color: gray01,
    fontSize: 14,
    lineHeight: '15px',
    display: 'block',
    '&.black': {
      color: 'black',
    },
  },
  '&.medium': {
    fontSize: 17,
  },
  '&.link-text': {
    color: props.theme.dark,
  },
  '.red': {
    color: 'red',
  },
  '&.grey': {
    color: 'grey',
  },
}))

export const CustomerName = styled('div')((props: any) => ({
  cursor: 'pointer',
  '& .disabled': {
    visibility: 'hidden',
    display: 'inline-block',
  },
  '& .enabled': {
    marginLeft: 8,
    visibility: 'hidden',
    display: 'inline-block',
  },
  '&:hover': {
    '& .enabled': {
      visibility: 'visible',
    },
  },
  '& svg path': {
    fill: `${props.theme.dark} !important`,
  },
}))

export const OrderStatusLabel = styled(ValueLabel)({
  padding: '4px 16px',
  border: `1px solid ${topLightGrey}`,
  borderRadius: 16,
})

export const BalanceLabel = styled(ValueLabel)({
  padding: '6px 8px',
  background: 'rgba(255, 113, 94, 0.1)',
  color: '#B50F04',
  borderRadius: 4,
})

export const ThemeColorSpan = styled('span')((props: any) => ({
  fontSize: 14,
  fontWeight: 'bold',
  '&.dark-color': {
    color: props.theme.dark,
  },
  '&.fill svg path': {
    fill: props.theme.dark,
  },
}))

export const TabHeaderBigSpan = styled('span')((props: any) => ({
  fontSize: 17,
  fontWeight: 'bold',
  color: gray01,
}))

export const TableHeaderItem = styled('div')((props: any) => ({
  cursor: 'pointer',
  '&.fill svg path': {
    fill: props.theme.dark,
  },
  '&.none svg path': {
    fill: 'none',
  },
  '&.sales-cart-ship-toggle': {
    '.toggle': {
      cursor: 'pointer',
      '.idle-status': {
        display: 'block',
      },
      '.hover-status': {
        display: 'none',
      },
    },
    '.toggle:hover': {
      '.idle-status': {
        display: 'none !important',
      },
      '.hover-status': {
        display: 'block',
      },
    },
  },
}))

export const NoOutlineButton = styled(Button)((props) => ({
  color: props.theme.dark,
  boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
  borderRadius: 3,
}))

export const GridTableWrapper = styled('div')((props: any) => ({
  '& tbody tr': {
    cursor: 'pointer',
    '&:hover': {
      '& td': {
        backgroundColor: `${props.theme.lighter} !important`,
      },
    },
  },
  '& tbody tr.highlighted': {
    '& td': {
      backgroundColor: `${props.theme.lighter} !important`,
    },
  },
  '& th': {
    backgroundColor: '#F7F7F7 !important',
    //color: `rgba(0, 0, 0, 0.65) !important`,
    color: `${gray01} !important`,
    borderTop: `1px solid ${topLightGrey}`,
    borderLeft: `1px solid ${topLightGrey}`,
    '&:first-of-type': {
      borderRight: `2px solid ${topLightGrey}`,
    },
    '&:last-child': {
      borderRight: `1px solid ${topLightGrey}`,
    },
  },
  '& td': {
    backgroundColor: 'white !important',
    //color: `rgba(0, 0, 0, 0.65) !important`,
    color: `${gray01} !important`,
    borderLeft: `1px solid ${topLightGrey}`,
    '&:first-of-type': {
      borderRight: `2px solid ${topLightGrey}`,
      '& svg path': {
        fill: lightGrey,
      },
    },
    '&:last-child': {
      borderRight: `1px solid ${topLightGrey}`,
    },
  },
}))

export const AddItemTableWrapper = styled('div')((props: any) => ({
  width: '100%',
  '.ant-table-wrapper': {
    width: '100%',
    '.ant-spin-nested-loading': {
      width: '100%',
      '.ant-spin-container': {
        width: '100%',
        '.ant-table': {
          overflow: 'auto',
          minWidth: '100%',
          maxWidth: '100%',
          display: 'block',
          th: {
            color: gray02, //color: '#4A5355',
            paddingTop: 3,
            paddingBottom: '0 !important',
            textAlign: 'center',
            height: 36,
            borderColor: '#D8DBDB !important',
            '&:first-child': {
              textAlign: 'left',
              paddingLeft: 34,
            },
            '.ant-table-column-title': {
              fontFamily: 'Arial',
              fontSize: 14,
              fontWeight: 'bold',
              color:`${gray02} !important`
            },
          },
          '& tbody tr': {
            td: {
              borderColor: '#D8DBDB !important',
              fontFamily: 'Arial',
              fontSize: 14,
              fontWeight: 'normal',
              color: '#22282A !important',
              paddingTop: 3,
              paddingBottom: 0,
              paddingLeft: 8,
              paddingRight: 8,
              height: 44,
              // textAlign: 'left !important',
              whiteSpace: 'nowrap',
              div: {
                paddingRight: '0 !important',
                whiteSpace: 'nowrap',
                '.anticon': {
                  width: '18.5px',
                  height: '18.5px',
                },
              },
              '.gray-circle': {
                width: 28,
                height: 28,
                borderRadius: 14,
                backgroundColor: '#D8DBDB',
                marginRight: '12px !important',
              },
              '.plus-circle': {
                marginTop: '3px !important',
                marginRight: '16px!important',
                marginLeft: '5.5px!important',
                display: 'none',
              },
              '.icon': {
                '& svg path': {
                  fill: '#22282A',
                },
              },
            },
            '&:hover': {
              '& td': {
                backgroundColor: `${props.theme.lighter}50 !important`,
                '&:first-of-type': {
                  '& svg path': {
                    fill: props.theme.primary,
                  },
                  color: `${props.theme.primary} !important`,
                },
                '.gray-circle': {
                  display: 'none',
                },
                '.plus-circle': {
                  display: 'block',
                },
              },
            },
          },
        },
      },
    },
  },
}))

export const CartTableWrapper = styled('div')((props: any) => ({
  '.react-resizable': {
    position: 'relative',
    backgroundClip: 'padding-box',
  },
  '.react-resizable-handle': {
    position: 'absolute',
    width: '10px',
    height: '100%',
    bottom: 0,
    right: -5,
    cursor: 'col-resize',
    zIndex: 1,
  },
  '.ant-table-placeholder': {
    borderRight: '1px solid #D8DBDB',
    borderLeft: '1px solid #D8DBDB',
    display: 'none',
  },
  '.drag-table': {
    table: {
      backgroundColor: `${props.theme.dark} !important`,
    },
  },
  '.product-name': {
    color: props.theme.dark,
  },
  '.product-name:hover': {
    textDecoration: 'underline',
  },
  '.ant-table tbody tr': {
    '.floating-menu-body': {
      display: 'none',
    },
    '&.on-hover-row-up': {
      'td:first-of-type': {
        borderBottomLeftRadius: 8,
      },
      'td:nth-of-type(10)': {
        borderBottomRightRadius: 8,
      },
    },
    '&.on-hover-row-bottom': {
      td: {
        borderTop: `1px solid ${props.theme.dark}`,
      },
      'td:first-of-type': {
        borderTopLeftRadius: 8,
      },
      'td:nth-of-type(10)': {
        borderTopRightRadius: 8,
      },
    },
    td: {
      '.drag-handler': {
        cursor: 'move',
        display: 'none',
      },
      '.price-setting': {
        visibility: 'hidden',
        cursor: 'pointer',
      },
      '.name-column': {
        position: 'relative',
        '.row-index-number, .w30': {
          minWidth: 20,
        },
        '.row-selection': {
          minWidth: 20,
        },
        '.replace-item-handler': {
          display: 'none',
        },
        '.icon-edit-item-name': {
          display: 'none',
          '&.icon-green-permanent': {
            display: 'block !important',
            'path': {
              fill: `${props.theme.dark} !important`
            }
          },
        },
        '.edited-item-name': {
          '&.pl8': {
            paddingLeft: 8,
          },
          color: 'grey'
        }
      },
    },
    'td:has(div.selected-row)': {
      backgroundColor: props.theme.light,
    },
    '&:hover': {
      '.floating-menu-body': {
        display: 'table-cell',
      },
      td: {
        backgroundColor: `${topLightGrey} !important`,
        '.drag-handler': {
          display: 'block',
        },
        '.name-column': {
          '.row-index-number': {
            visibility: 'hidden !important',
          },
          '.row-selection': {
            display: `block !important`,
          },
          '.replace-item-handler': {
            display: 'block',
          },
          '.icon-edit-item-name': {
            display: 'block'
          }
        },
        '.price-setting': {
          visibility: 'visible',
        },
      },
    }
  },
  '.ant-table tbody tr:last-of-type': {
    td: {
      '.add-new-row': {
        display: 'none',
      },
    },
  },
  // '.ant-table tbody tr:hover': {

  // },
  '.ant-table tbody tr.selected-row': {
    td: {
      backgroundColor: `${props.theme.lighter} !important`,
    },
  },
  '.ant-table-footer': {
    padding: 0,
  },
  'table': {
    border: '1px solid #D8DBDB',
    borderBottom: 'none',
    thead: {
      boxShadow: 'none !important',
      'tr:first-of-type th': {
        backgroundColor: '#F7F7F7 !important',
      },
      th: {
        borderLeft: '1px solid #D8DBDB',
        padding: 12,
        '.copy-order-qty': {
          paddingTop: 2,
          svg: {
            width: 14,
            height: 14,
            cursor: 'pointer',
          },
        },
      },
      'th:first-of-type': {
        borderLeft: 'none',
        borderRight: 'none',
        width: '20px !important',
        paddingLeft: 0,
        paddingRight: 0,
      },
      'th:nth-of-type(2)': {
        borderLeft: 'none',
        paddingLeft: '0 !important',
        width: '100px !important',
      },
    },
    tbody: {
      tr: {
        '&.dragging': {
          'td .drag-handler': {
            cursor: 'grabbing',
          },
        },
        td: {
          borderLeft: '1px solid #D8DBDB',
          padding: '8px 12px',
          '.status-selection': {
            '&.not-selector': {
              width: 110,
              border: '1px solid #D8DBDB',
              borderRadius: 20,
              padding: '4px 10px',
            },
            'div.ant-select-selection': {
              borderRadius: 20,
            },
          },
        },
        'td:first-of-type': {
          borderLeft: 'none',
          borderRight: 'none',
          width: '20px !important',
          paddingLeft: 0,
          paddingRight: 0,
        },
        'td:nth-of-type(2)': {
          borderLeft: 'none',
          paddingLeft: 0,
          width: '100px !important',
          position: 'relative',
          '.add-new-row': {
            height: 16,
            width: '100%',
            position: 'absolute',
            bottom: -7,
            zIndex: 10,
            '.icon-add-row': {
              position: 'absolute',
              display: 'none',
              svg: {
                width: 20,
                height: 20,
              },
              'svg path': {
                fill: 'rgba(0,0,0,1)',
              },
            },
          },
          '.add-new-row:hover': {
            '.icon-add-row': {
              display: 'inline-block',
            },
          },
        },
      },
    },
  },
  '.ant-table-content .ant-table-footer': {
    border: '1px solid #D8DBDB',
    backgroundColor: white,
    padding: 0,
    '.footer-add-item': {
      margin: 5,
      width: 'calc(21% + 15px)',
    },
  },
  '&.sales-order-table': {
    '.ant-table-placeholder': {
      display: 'none',
    },
  },
  '.custom-ant-select-focused': {
    border: `1px solid ${props.theme.primary}`,
    borderRadius: 5,
  }
}))

export const DialogSubContainer = styled('div')({
  padding: '12px',
  backgroundColor: '#F7F7F7',
  textAlign: 'left',
  '&.bordered': {
    borderBottom: '1px solid #D8D8D8',
  },
})

export const DialogBodyDiv = styled('div')((props: any) => ({
  padding: '16px 24px',
  '&.item-price-modal': {
    fontWeight: 300,
  },
  '.warning': {
    textAlign: 'center',
    fontWeight: 500,
    'i': {
      marginRight: 8
    },
    '&.hide': {
      visibility: 'hidden'
    }
  }
}))

export const Description = styled('div')((props: any) => ({
  padding: '16px 0',
  fontWeight: 300,
  color: 'rgb(196, 196, 196)',
}))

export const PalletLabelsTable = styled('div')((props: any) => ({

}))

export const EmptyResult = styled('div')((props: any) => ({
  fontFamily: 'Museo Sans Rounded',
  fontSize: 15,
  textAlign: 'center',
  padding: '100px 0',
  width: 500,
  margin: 'auto',
  '& .icon': {
    marginBottom: 33,
    '& svg path': {
      fill: 'none',
    },
  },
  '& .button': {
    margin: '33px 0',
    button: {
      width: 'auto',
      height: 'auto',
      color: 'white !important',
      fontSize: 15,
      padding: 10,
    },
  },
  '& .extra-charge': {
    span: {
      fontWeight: 'bold',
      fontSize: 15,
    },
    marginBottom: 9,
  },
}))

export const FloatingMenu = styled('td')((props: any) => ({
  height: 28,
  padding: '2px !important',
  width: 36,
  textAlign: 'center',
  backgroundColor: white,
  border: '1px solid grey',
  boxShadow: '0px 1px 3px 1px rgba(0,0,0,0.65);',
  position: 'absolute',
  right: 42,
  margin: '12px 0 !important',
  borderRadius: 4,
  '&:hover': {
    boxShadow: '0px 1px 1px 1px rgba(0,0,0,0.45);',
    backgroundColor: topLightGrey,
  },
}))

export const FloatMenuItem = styled('div')((props: any) => ({
  color: props.theme.dark,
  fontWeight: 500,
  fontSize: 15,
  lightHeight: '21px',
  padding: '4px 0',
  '& svg path': {
    fill: 'none',
  },
  '&.fill svg path': {
    fill: props.theme.dark,
  },
  '&.black': {
    color: '#333333'
  }
}))

/**
 * React.CSSProperties
 */

export const ItemMarginTop8: React.CSSProperties = {
  marginTop: 8,
}

export const ItemMarginTop25: React.CSSProperties = {
  marginTop: 25,
}

export const ItemHMargin4: React.CSSProperties = {
  margin: '0 4px',
}

export const ItemHMargin40: React.CSSProperties = {
  margin: '0 20px',
}

export const VerticalPadding12: React.CSSProperties = {
  padding: '12px 0',
}

export const VerticalPadding8: React.CSSProperties = {
  padding: '8px 0',
}

export const AddItemNameStyle: React.CSSProperties = {
  marginRight: 8,
  maxWidth: 282,
  overflow: 'hidden',
  textOverflow: 'ellipsis',
}

export const HeaderActionMenu = styled(Menu)((props: any) => ({
  '.ant-dropdown-menu-item': {
    backgroundColor: 'transparent !important',
    display: 'flex',
    alignItems: 'center',
    color: '#22282A',
    svg: {
      path: {
        stroke: '#22282A'
      },
    },
    '.anticon': {
      color: '#22282A !important',
    },
    '&.ant-dropdown-menu-item-disabled': {
      color: 'rgba(0, 0, 0, 0.25)',
      svg: {
        path: {
          stroke: 'rgba(0, 0, 0, 0.25) !important'
        },
      },
      '.anticon': {
        color: 'rgba(0, 0, 0, 0.25) !important',
      },
    },
  },
}))

export const noPaddingFooter = css({
  '& .button-spinner': {
    marginRight: 8,
    'span i': {
      background: 'white',
    },
  },
  '& .ant-modal-footer': {
    padding: 0,
  },
  '& .quick-enter-po-add-brand .ant-select-selection': {
    minHeight: 40,
    '.ant-select-selection__rendered': {
      minHeight: 38,
      padding: '4px 0',
    },
  },
})

export const Flex = styled('div')((props: any) => ({
  display: 'flex',
  '&.v-center': {
    alignItems: 'center',
  },
  '&.h-center': {
    justifyContent: 'center',
  },
  '&.space-between': {
    justifyContent: 'space-between',
  },
  '&.flex-end': {
    justifyContent: 'flex-end'
  },
}))

export const TabsWrapper = styled('div')((props: any) => ({
  textAlign: 'left',
  paddingLeft: 5,
  '&.page-tab': {
    '.ant-tabs-bar': {
      marginBottom: 18,
    },
    '.ant-tabs-nav-wrap': {
      paddingLeft: 75,
      '.ant-tabs-nav': {
        '.ant-tabs-tab': {
          fontFamily: 'Museo Sans Rounded',
          fontWeight: 'normal',
          fontSize: 17,
          lineHeight: '22px',
          color: gray02, //color: '#4A5355',
          paddingLeft: 0,
          paddingRight: 0,
          paddingBottom: 8,
          '&.ant-tabs-tab-active': {
            borderBottom: `4px solid ${props.theme.theme}`,
            color: props.theme.theme,
            fontWeight: 'bold',
          },
        },
        '.ant-tabs-ink-bar': {
          height: 3,
        },
      },
    },
    '.ant-tabs-top-content > .ant-tabs-tabpane': {
      paddingLeft: 75,
      '.tab-header': {
        // paddingLeft: 80,
        fontFamily: 'Museo Sans Rounded',
        // height: 50,
        paddingRight: 20,
        paddingBottom: 8,
        // borderBottom: '3px solid #DBDBDB',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        h3: {
          fontSize: 20,
          // lineHeight: '27.6px',
          color: gray01,
          marginBottom: 6,
          marginRight: 12,
        },
        label: {
          fontSize: 16,
          lineHeight: '22.4px',
          color: gray01, //color: '#4A5355',
        },
      },
      '.ant-table-wrapper': {
        // paddingLeft: 80,
        '.ant-table-content .ant-table-body .ant-table-thead > tr > th': {
          paddingLeft: 16,
          '&.th-left': {
            paddingLeft: 9,
            textAlign: 'left',
          },
        },
        '.ant-table-content .ant-table-body .ant-table-tbody > tr > td ': {
          //backgroundColor: 'transparent',
        },
      },
      '.ant-pagination': {
        marginRight: 20,
      },
    },
    '&.purchase': {
      '.ant-tabs-nav-wrap': {
        paddingLeft: 85,
        // '.ant-tabs-nav .ant-tabs-tab': {
        //   margin: '0 70px 0 0'
        // },
      },
      '.ant-tabs-top-content > .ant-tabs-tabpane': {
        paddingLeft: 63,
        '.ant-tabs-bar.ant-tabs-top-bar': {
          '.ant-tabs-nav-wrap': {
            paddingLeft: 64,
          },
        },
      },
      '.ant-tabs-tabpane': {
        '.ant-tabs-top-content > .ant-tabs-tabpane': {
          paddingLeft: 13,
        },
        '.ant-tabs-nav-wrap': {
          paddingLeft: '14px !important',
        },
      },
    },
  },
  '&.modal-tab': {
    '.ant-tabs-bar': {
      margin: 0,
    },
    '.tab-highlighted': {
      border: '1px dashed',
    },
    '.ant-tabs-nav-wrap': {
      '.ant-tabs-nav': {
        '.ant-tabs-tab': {
          fontFamily: 'Museo Sans Rounded',
          fontWeight: 'normal',
          fontSize: 15,
          lineHeight: '22px',
          color: gray02, //'#4A5355',
          paddingLeft: 0,
          paddingRight: 0,
          '&.ant-tabs-tab-active': {
            borderBottom: `4px solid ${props.theme.theme}`,
            color: props.theme.theme,
            fontWeight: 'bold',
          },
        },
        '.ant-tabs-ink-bar': {
          height: 3,
        },
      },
    },
  },
}))

export const DragItem = styled('div')({
  padding: 12,
  borderTop: `1px solid ${topLightGrey}`,
  borderBottom: `1px solid ${topLightGrey}`,
  borderRight: `1px solid ${topLightGrey}`,
  cursor: 'grabbing !important',
  width: '12%',
  '&.product-name': {
    borderLeft: `1px solid ${topLightGrey}`,
    width: 'calc(21% + 0px)',
  },
  boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.7)',
  zIndex: 100,
  backgroundColor: 'white',
})

export const CustomButton = styled(Button)((props: any) => ({
  padding: '4px 6px',
  border: `1px solid ${topLightGrey}`,
  boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.3)',
  borderRadius: 4,
  marginRight: 4,
  flex: 1,
  '&.empty': {
    height: '100%',
  },
  // '&:active': {
  //   backgroundColor: topLightGrey,
  //   boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.3)',
  // },
  // '&.selected': {
  //   backgroundColor: topLightGrey,
  //   boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.3)',
  // }
  '&[disabled]': {
    backgroundColor: '#FEFEFE !important',
  },
  '&.warning': {
    backgroundColor: '#FEEAE9',
    border: '1px solid #EB5757',
    boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
    //color: 'rgba(0, 0, 0, 0.65) !important',
    color: `${gray01} !important`,
  },
  '&.no-lots': {
    border: 0,
    boxShadow: 'none',
    //color: 'rgba(0, 0, 0, 0.65)',
    color: gray01,
    background: 'transparent',
  },
}))

export const Unit = styled('div')((props: any) => ({
  flex: 1,
  paddingLeft: 4,
  color: gray01,
}))

export const SelectWrapper = styled('div')((props: any) => ({
  padding: '10px 0px',
  '.ant-select': {
    width: '70%',
  },
}))

export const NavHeaderButtonsWrapper = styled('div')((props: any) => ({
  position: 'absolute',
  top: 10,
  zIndex: 1000,
  right: 0,
}))

export const CostWrapper = styled('div')((props: any) => ({
  paddingTop: 10,
  paddingBottom: 10,
}))

export const FulfillmentWrapper = styled('div')((props: any) => ({
  paddingRight: 60,
  paddingBottom: 30,
  '.ant-form-vertical .ant-form-item': {
    flex: 1,
    marginBottom: 0,
    paddingBottom: 0,
  },
  '.ant-btn.ant-btn-link': {
    padding: '0 !important',
    border: '0 !important',
    margin: '-1px 13px 0px 5px',
    '&.select-delivery': {
      marginLeft: 5,
      marginTop: -5,
      svg: {
        float: 'left',
        marginTop: 2,
      },
      span: {
        fontFamily: 'Museo Sans Rounded',
        marginLeft: 8.6,
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: '21px',
        color: props.theme.theme,
      },
    },
  },
  '.label': {
    fontSize: 14,
    lineHeight: '19.6px',
    //color: '#4A5355',
    color: gray01,
    marginBottom: 4,
  },
  hr: {
    borderTop: '1px solid #D8DBDB',
    borderBottom: 0,
    margin: '28px 42px 31px 0',
  },
  '.mt28': {
    marginTop: 28,
  },
  '.mt14': {
    marginTop: 14,
  },
  '.os-wrapper': {
    display: 'flex',
    justifyContent: 'flex-end',
    '.order-summary': {
      width: 300,
      border: '1px solid #D8DBDB',
      padding: 24,
      backgroundColor: 'white',
      fontFamily: 'Museo Sans Rounded',
      h4: {
        fontSize: 18,
        color: '#22282A',
        marginBottom: 0,
      },
      '.value': {
        fontSize: 24,
        lineHeight: '27.6px',
        color: '#22282A',
      },
      '.os-row': {
        marginTop: 20,
      },
    },
  },
  '.fulfillment-link-icon path': {
    stroke: props.theme.dark,
  },
}))

export const AddACWrapper = styled('div')({
  width: 800,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  '.product-name, .inventory': {
    flex: 1,
  },
  '.sku': {
    width: 150,
    padding: '0 15px',
  },
  '.ellipsis': {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
})

export const DuplicateConfirmDiv = styled('div')({
  padding: '20px 30px',
  fontWeight: 500,
})

export const BoldSpan = styled('span')({
  fontWeight: 'bold',
})

export const EPickStatus = styled('div')({
  minHeight: 100,
  position: 'relative',
  padding: '0 40px',
  '.icon-body': {
    textAlign: 'center'
  },
  '.btn-switch': {
    display: 'flex',
    justifyContent: 'flex-end',
    marginRight: -30,
    'button': {
      marginLeft: 10
    }
  },
  '.description': {
    padding: '24px 0',
    'span.warning': {
      color: '#B50F04'
    }
  }
})

export const EPickSheetRowItemWrapper = styled('div')((props: any) => ({
  paddingBottom: 20,
  '.quantity-item': {
    '.close-quantity': {
      display: 'none',
    },
    '&:hover': {
      '.close-quantity': {
        display: 'block',
        cursor: 'pointer',
      },
    }
  },
  '.collapseHeader': {
    display: 'flex',
    alignItems: 'center',
    paddingRight: '40px',
    fontWeight: 'normal',
    justifyContent: 'space-between',
    p: {
      marginBottom: 0,
      display: 'flex',
      '.picked': {
        marginRight: '15px',
        minWidth: '20px',
        textAlign: 'right',
      },
      '.quantity': {
        margin: '0 6px 0 15px',
        textAlign: 'left',
        minWidth: '20px',
      }
    },
    '.index': {
      color: '#828282',
      marginRight: '15px',
      fontWeight: 'normal',
    },
    '.lot': {
      margin: '0 20px 0 25px',
      minWidth: '130px',
    }
  }
}))

export const EPickSheetRowItem = styled(Row)((props: any) => ({
  padding: '5px',
  '.amount': {
    '.picked': {
      minWidth: 20,
      textAlign: 'right',
      display: 'inline-block',
      marginRight: 15
      '&.green': {
        color: props.theme.primary
      }
    },
    '.quantity': {
      display: 'inline-block',
      marginLeft: 15
    }
  },
  '.ant-col': {
    fontWeight: "500 !important"
  }
}))

export const SelectedDropdownItem = styled('div')((props: any) => ({
  background: props.theme.lighter,
  padding: '2px 14px',
  fontSize: 12,
  fontWeight: 500,
  color: props.theme.dark,
  border: `1px solid ${props.theme.dark}`,
  borderRadius: 16,
  boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
  whiteSpace: 'nowrap',
  margin: 2,
  height: 24,
  '& i': {
    marginLeft: 8,
    cursor: 'pointer'
  }
}))


export const CustomDropdownWrapper = styled('div')((props: any) => ({
  '& .btn-add-item': {
    border: 'none',
    height: 24,
    margin: 2
  }
}))

export const LabelWrapper = styled('div')({
  width: '50%',
  padding: '5px 0'
})

export const PalletLabelRow = styled('div')({
  padding: '5px 15px',
  fontFamily: 'Arial',
  color: 'black',
  marginTop: 12,
  '& .pallet-label-small': {
    fontSize: 28,
    fontWeight: 500,
    lineHeight: '10px'
  },
  '& .pallet-label-medium': {
    fontSize: 34,
    fontWeight: 500,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: '-webkit-box',
    WebkitLineClamp: 2, /* number of lines to show */
    WebkitBoxOrient: 'vertical',
  },
  '& .pallet-label-bold': {
    fontSize: 38,
    fontWeight: 'bold'
  },
  '& .pallet-label-big': {
    fontSize: 70,
    fontWeight: 500,
    textAlign: 'center',
  }
})

export const SalesOrderPickWrapper = styled('div')((props: any) => ({
  'table': {
    'thead': {
      'th': {
        borderRight: '1px solid #EDF1EE !important'
      }
    },
    'tbody': {
      'td': {
        paddingLeft: '16px !important',
        paddingRight: '16px !important',
        borderRight: '1px solid #EDF1EE !important'
      }
    }
  }
}))

export const WarningText = styled('p')((props: any) => ({
  marginTop: 4,
  color: '#EB5757',
  '&.hide': {
    display: 'none'
  },
  '&.show': {
    display: 'block'
  }
}))

export const NoBorderButton = styled('button')((props: any) => ({
  border: '0 !important',
  color: props.theme.theme,
  backgroundColor: 'transparent',
  cursor: 'pointer'
}))

export const StyledActivityLogWrapper = styled('div')((props: any) => ({
  padding: 12,
  '.log-item': {
    display: 'flex',
    marginBottom: 12,
    color: darkGrey,
    '.time': {
      width: 150
    },

    '.content':{
      width: 750,
      '.bold': {
        fontWeight: 'bold'
      }
    }
  }
}))

