import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { notification, Layout, Row, Col, Spin } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'

import { Theme } from '~/common'
import { GlobalState } from '~/store/reducer'
import PageLayout from '~/components/PageLayout'
import { CustomersDispatchProps, CustomersModule, TempCustomersStateProps } from '../customers.module'
import { ReportPageContainer } from '~/modules/location/location.style'
import OrderReportHeader from '~/modules/vendors/purchase-order-report/order-report-header'
import moment from 'moment'
import SalesOrderReportBody from './order-report-body'

type CustomersDetailProps = CustomersDispatchProps &
  TempCustomersStateProps & {
    theme: Theme
  }

class SalesOrderReport extends React.PureComponent<CustomersDetailProps> {
  state = {
    from: moment().subtract(30, 'days'),
    to: moment(),
    search: '',
    page: 1,
    id: 0,
    pageSize: 12,
    orderBy: '',
    direction: ''
  }

  componentDidMount() {
    this.props.getAllCustomersForUser()
    this.getAllSalesOrderHistory()
  }

  getAllSalesOrderHistory = () => {
    const searchObj = {
      ...this.state,
      from: this.state.from.format('MM/DD/YYYY'),
      to: this.state.to.format('MM/DD/YYYY'),
      page: this.state.page - 1,
      clientId: this.state.id
    }
    this.props.resetLoading()
    this.props.getAllSalesOrderHistory(searchObj)
  }

  onChange = (type: string, data: any) => {
    if (type == 'date') {
      this.setState({ from: data.from, to: data.to })
    } else {
      let obj = this.state
      obj[type] = data
      this.setState({ ...obj })
    }

    setTimeout(() => {
      this.getAllSalesOrderHistory()
    }, 100)
  }

  onTableChange = (page: any, filter: any, sorter: any) => {
    console.log(page, sorter)
    const state = { ...this.state }
    if (page !== null) {
      state['page'] = page.current
    }
    if (sorter && Array.isArray(sorter)) {
      const filterStr = sorter.map((el: any) => { return el.dataIndex }).join(',')
      const directionStr = sorter.map((el: any) => { return el.sortOrder === "ascend" ? 'ASC' : 'DESC' }).join(',')
      state['orderBy'] = filterStr ? filterStr : ''
      state['direction'] = directionStr ? directionStr : ''
    }
    this.setState(state, () => {
      this.getAllSalesOrderHistory()
    })
  }

  render() {
    const { from, to, search, page, pageSize } = this.state
    const { items, loading, salesOrderHistory, salesHistoryTotal } = this.props
    return (
      <PageLayout currentTopMenu={'menu-Customers-Sales Report'} noSubMenu={true}>
        <ReportPageContainer>
          <OrderReportHeader
            onSearchChange={this.onChange}
            from={from}
            to={to}
            search={search}
            theme={this.props.theme}
            title={'Sales Order Reports'}
            placeholder={"Search by all fields"}
            clients={items ? items : []}
          />
          <SalesOrderReportBody
            data={salesOrderHistory}
            onChange={this.onTableChange}
            loading={loading}
            total={salesHistoryTotal}
            currentPage={page}
            pageSize={pageSize}
          />
        </ReportPageContainer>
      </PageLayout>
    )
  }

}

const mapState = (state: GlobalState) => state.customers

export default withTheme(connect(CustomersModule)(mapState)(SalesOrderReport))
