import React from 'react'
import { MultiBackSortTable } from '~/components/multi-back-sortable'
export type SalesOrderReportBodyProps = {
  data: any
  total: number
  currentPage: number
  pageSize: number
  loading: boolean
  onChange: (page: any, filter: any, sorter: any) => void
}

class SalesOrderReportBody extends React.PureComponent<SalesOrderReportBodyProps> {
  columns = [
    {
      title: 'id_Sales',
      dataIndex: 'wholesaleOrderId',
      key: 'order.wholesaleOrderId',
      sorter: true,
    },
    {
      title: 'SalesOrders::idRandom',
      dataIndex: 'wholesaleOrderId',
      key: '',
      // sorter: true,
    },
    {
      title: 'SalesOrders::salesRepresentative',
      dataIndex: 'representative',
      key: 'order.seller.firstName',
      sorter: true,
      // render: (seller: any) => {
      //   return seller && seller.firstName && seller.lastName ? seller.firstName + ' ' + seller.lastName : ''
      // }
    },
    {
      title: 'SalesOrders::id_Accounts',
      dataIndex: 'clientId',
      key: 'order.wholesaleClient.clientId',
      sorter: true,
    },
    {
      title: 'SalesOrders::accountName',
      dataIndex: 'companyName',
      key: 'order.wholesaleClient.clientCompany.companyName',
      sorter: true,
    },
    {
      title: 'PickSheets_ProductSku',
      dataIndex: 'SKU',
      key: 'wholesaleItem.SKU',
      sorter: true,
    },
    {
      title: 'PickSheets_ProductName',
      dataIndex: 'variety',
      key: 'wholesaleItem.variety',
      sorter: true,
    },
    {
      title: 'PickSheets_Units',
      dataIndex: 'quantity',
      key: 'quantity',
      sorter: true,
    },
    {
      title: 'PickSheets_UOM',
      dataIndex: 'UOM',
      key: 'UOM',
      sorter: true,
    },
    {
      title: 'unstored_totalWeightTruncated',
      dataIndex: 'orderWeight',
      key: 'orderWeight',
      sorter: true,
    },
    {
      title: 'PickSheets_Cost',
      dataIndex: 'cost',
      key: 'cost',
      sorter: true,
    },
    {
      title: 'PickSheets_Price',
      dataIndex: 'price',
      key: 'price',
      sorter: true,
    },
  ]

  render() {
    const { currentPage, total, pageSize, loading, data } = this.props
    return (
      <MultiBackSortTable
        columns={this.columns}
        dataSource={data}
        loading={loading}
        onSort={this.props.onChange}
        onChange={this.props.onChange}
        rowKey="wholesaleOrderItemId"
        pagination={{
          hideOnSinglePage: true,
          pageSize: pageSize,
          current: currentPage,
          total: total,
          defaultCurrent: 1
        }}
      />
    )
  }
}

export default SalesOrderReportBody