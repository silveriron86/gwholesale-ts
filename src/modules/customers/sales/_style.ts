import styled from '@emotion/styled'
import { Layout, Icon, Row, Col, Input } from 'antd'
import {darkGrey, badgeColor, black, mediumGrey, lightGrey, medLightGrey, topLightGrey, gray01 } from '~/common'
import { RowProps } from 'antd/lib/row'
import { isBrowser } from 'react-device-detect'

interface ActiveProps {
  active?: boolean
}

interface StatusProps {
  status: number
  statusName: string
}

export const ProgressBar = styled(Layout)((props) => ({
  background: '#FFFFFF',
  border: `1px solid ${props.theme.primary}`,
  boxSizing: 'border-box',
  borderRadius: 6,
}))

export const CancelProgressBar = styled(Layout)((props) => ({
  background: 'rgba(181, 0, 0, 0.1)',
  border: `1px solid #B50000`,
  boxSizing: 'border-box',
  borderRadius: 6,
  color: '#B50000',
}))

export const InfoHeader = styled('div')((props: any) => ({
  padding: '18px 24px',
  marginBottom: 50,
  boxShadow: '0px 12px 14px -6px rgba(0, 0, 0, 0.15)',
  '.theme-icon svg path': {
    fill: props.theme.primary
  }
}))

export const ColWrapper = styled('div')<StatusProps>((props) => ({
  position: 'relative',
  backgroundColor: props.status === 2 ? props.theme.lighter : 'transparent',
  width: 'calc(100%  - 25px)',
  height: 37,
  borderRadius: 4,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  border: props.status === 2 ? `1px solid ${props.theme.light}60` : 0,
  borderTopRightRadius: 0,
  borderBottomRightRadius: 0,
  borderRight: 0,
  fontWeight: props.status >= 2 ? 'bold' : 0,
  color: props.statusName ? '#B50000' : props.status >= 2 ? props.theme.main : darkGrey,
}))

export const ColTriangle = styled('div')((props) => ({
  display: 'block',
  height: 27,
  width: 27,
  backgroundColor: props.theme.lighter,
  border: 'inherit',
  position: 'absolute',
  top: 4,
  right: -13,
  clipPath: 'polygon(0% 0%, 100% 100%, 0% 100%)',
  transform: 'rotate(225deg)',
  borderRadius: ' 0 0 0 6px',
}))

export const ColMain = styled('div')((props) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}))

export const ProgressCircle = styled('div')<StatusProps>((props) => ({
  width: 15,
  height: 15,
  border: props.statusName
    ? `2px solid #B50000`
    : props.status >= 2
      ? `2px solid ${props.theme.main}`
      : `1px solid ${props.theme.light}`,
  borderRadius: 7.5,
  marginRight: 10,
  overflow: 'hidden',
}))

export const Filled = styled('div')<StatusProps>((props) => ({
  height: 11,
  width: props.status === 2 ? 5.5 : 11,
  backgroundColor: props.statusName ? `#B50000` : props.theme.main,
}))

export const CheckIcon = styled(Icon)({
  color: 'white',
  fontWeight: 900,
  marginTop: 1,
  position: 'absolute',
  marginLeft: -5,
  fontSize: '10px',
})

export const SalesOverview = styled('div')({
  padding: 30,
})

export const AccountLabel = styled('div')({
  paddingBottom: 5,
  '&.left-align': {
    textAlign: 'left'
  }
})

export const SalesContainer = styled('div')({
  padding: '0 18px',
})

export const accountLayoutStyle: React.CSSProperties = {
  backgroundColor: 'transparent',
  textAlign: 'left',
  margin: '15px 10px',
}

export const DetailsSection = styled('div')((props) => ({
  backgroundColor: props.theme.lighter,
  borderRadius: 6,
  textAlign: 'left',
  padding: '15px 0 15px 30px',
}))

export const DetailLabel = styled('p')<ActiveProps>((props) => ({
  fontSize: `${isBrowser ? 12 : 9}px`,
  //color: props.active ? badgeColor : mediumGrey,
  color: props.active ? badgeColor : gray01,
  marginBottom: 10,
  '&.nowrap': {
    whiteSpace: 'nowrap'
  }
}))

export const DetailValue = styled('div')<ActiveProps>((props) => ({
  fontSize: '17px',
  fontFamily: 'Arial',
  fontWeight: 'bold',
  color: props.active ? badgeColor : black,
  height: 37,
  alignItems: 'center',
  display: 'flex',
}))

export const NoBorderText: React.CSSProperties = {
  backgroundColor: 'transparent',
  borderRadius: 0,
  border: 0,
  overflowY: 'hidden',
  borderBottom: '1px solid black',
  resize: 'none',
  fontSize: '17px',
  fontFamily: 'Arial',
  fontWeight: 'bold',
  color: 'black',
  lineHeight: '18px',
  width: '100%',
  padding: 0,
}

export const BlinkBorderText = styled(Input)((props) => ({
  backgroundColor: 'transparent',
  borderRadius: 0,
  overflowY: 'hidden',
  resize: 'none',
  fontSize: '17px',
  fontFamily: 'Arial',
  fontWeight: 'bold',
  color: 'black',
  lineHeight: '18px',
  width: '100%',
  padding: 0,
  borderBottom: '1px solid black',
  '&:focus': {
    borderColor: props.theme.primary,
  },
}))

export const HorizontalFlex = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  flex: 1,
  justifyContent: 'space-between',
})

export const fullButton: React.CSSProperties = {
  marginLeft: 0,
  width: 'calc(100% - 15px)',
  minWidth: 'fit-content',
}

export const SAModalRow = styled('div')((props) => ({
  '&': {
    borderTop: `1px solid ${lightGrey}`,
    height: 60,
    display: 'flex',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    '&:hover': {
      backgroundColor: props.theme.lighter,
    },
  },
}))

export const SAModalFooter: React.CSSProperties = {
  justifyContent: 'flex-end',
  backgroundColor: 'white',
  paddingRight: 0,
}

export const SADefault = styled('div')((props) => ({
  display: 'flex',
  alignSelf: 'flex-start',
  color: props.theme.primary,
  marginTop: 5,
}))

export const noteIcon: React.CSSProperties = {
  fontSize: 23,
}

export const noteGreyIcon: React.CSSProperties = {
  fontSize: 23,
  color: lightGrey,
}

export const QuantityModalRow = styled(Row)<RowProps & ActiveProps>((props) => ({
  '&': {
    backgroundColor: props.theme.lighter,
    height: 36,
    border: '1px solid white',
    '&:hover': {
      border: `1px solid ${props.theme.primary}`,
      borderRadius: 3,
    },
  },
}))

export const QuantityModalCol = styled(Col)({
  height: 36,
  alignItems: 'center',
  display: 'flex',
  padding: '0 10px',
})

export const mb0: React.CSSProperties = {
  marginBottom: 0,
}

export const odPL: React.CSSProperties = {
  paddingLeft: 13,
}

export const odPR: React.CSSProperties = {
  paddingRight: 13,
}

export const odRow: React.CSSProperties = {
  marginTop: 18,
}

export const odInputsLayout: React.CSSProperties = {
  backgroundColor: 'transparent',
  paddingBottom: 35,
  margin: '35px 0 25px',
  borderTop: `1px solid ${medLightGrey}`,
  borderBottom: `1px solid ${medLightGrey}`,
}

export const odOrderSummaryLayout: React.CSSProperties = {
  backgroundColor: 'transparent',
  paddingBottom: 35,
}

export const odDestLayout: React.CSSProperties = {
  backgroundColor: topLightGrey,
  borderRadius: 6,
  padding: '10px 20px 15px',
  border: `1px solid ${lightGrey}`,
  height: 132,
}

export const DestinationText = styled('div')({
  fontWeight: 'bold',
  fontSize: 15,
  lineHeight: '18px',
})

export const OoOrderSummaryCol = styled('div')({
  width: 158,
  height: 47,
  display: 'flex',
  alignItems: 'flex-start',
  flexDirection: 'column',
  justifyContent: 'center',
})

export const changeButton: React.CSSProperties = {
  width: '100%',
  fontWeight: 'bold',
  paddingRight: 10,
  marginTop: 35,
}

export const LockText = styled('span')({
  color: mediumGrey,
  fontWeight: 'bold',
  marginLeft: '10px',
  paddingTop: '7px',
  fontSize: "16px"
})

export const PrintModalHeader = styled('div')((props) => ({
  backgroundColor: '#F7F7F7',
  margin: -24,
  marginBottom: 0,
  padding: 12,
  paddingRight: 70,
  borderBottom: '1px solid #D8DBDB',
  '.ant-btn': {
    float: 'right',
    color: 'white !important'
  }
}))

export const LogisticRow = styled('div')((props: any) => ({
  width: 248,
  color: gray01,
  marginBottom: 18,
  '&.large': {
    width: 540
  }
}))

export const CMLabel = styled('div')((props) => ({
  //color: 'rgba(0, 0, 0, 0.65)',
  color: gray01,
  fontSize: 16
}))

export const CMValue = styled('div')((props) => ({
  color: 'black',
  fontSize: 22
}))

export const CreditMemoTableWrapper = styled('div')((props: any) => ({
  'table': {
    'thead th .ant-table-column-title': {
      fontWeight: '500 !important',
      fontSize: '14px !important',
      color: 'black'
    },
    'tbody td.th-right': {
      textAlign: 'right',
      paddingRight: 22
    },
    'tbody td': {
      fontWeight: 500,
      paddingTop: 8,
      paddingBottom: 8
    }
  },
  '.clear-icon': {
    display: 'none',
    marginRight: 6
  },
  '& .ant-input-search:hover .clear-icon.show': {
    display: 'block'
  }
}))

export const AddEmails = styled('div')((props) => ({
  margin: '10px 0',
  color: 'black',
  fontSize: 13,
  fontWeight: 'normal',
  a: {
    marginLeft: 8,
    color: 'black',
    textDecoration: 'underline',
  }
}))
