import React, { useEffect, useState } from 'react'
import { withTheme } from 'emotion-theming'
import { SalesContainer, fullButton, LockText, PrintModalHeader, AddEmails } from '../_style'
import {
  CustomerDetailsWrapper as DetailsWrapper,
  CustomerDetailsTitle as DetailsTitle,
  floatLeft,
  floatRight,
  ThemeOutlineButton,
  ThemeModal,
  ThemeSpin,
  ThemeButton,
  ThemeOutlineCancelButton,
  ThemeSwitch,
  Flex,
} from '../../customers.style'
import { Row, Col, Icon, Modal, Popconfirm, Tooltip, Form, Input, Select, Button } from 'antd'
import { History } from 'history'
import { CustomButton } from '~/components'
import { CartOrderTable } from './tables'
import { OneOffItemTable } from './tables'
import ProductModal from './modals/product-modal'
import { OrderDetail, OrderItem } from '~/schema'
import { PrintPickSheet } from './print'
import PrintBillOfLading from '~/modules/customers/sales/cart/print/print-bill-lading'
import ViewSalesConfirmation from '~/modules/customers/sales/cart/print/view-sales-confirmation'
import PrintInvoice from '~/modules/customers/sales/cart/print/print-invoice'
import jQuery from 'jquery'
import { red } from '~/common'
import {
  printWindow,
  judgeConstantRatio,
  responseHandler,
  printWindowAsync,
  printQuickPrintAsync,
} from '~/common/utils'
import _, { cloneDeep } from 'lodash'
import { Cart } from '../../nav-sales/order-detail/tabs/cart'
import styled from '@emotion/styled'
import sendEmail from './print/sendEmail'
import { OrderService } from '~/modules/orders/order.service'
import { of } from 'rxjs'

import { Icon as IconSvg } from '~/components'
import { CustomerService } from '../../customers.service'

interface CartContainerProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  company: any
  logo: string
  loading: boolean
  onAddItem: Function
  onSwapItem: Function
  onRemoveItem: Function
  onUpdateItem: Function
  onUpdateOrderStatus: Function
  onUpdateAllItemStatus: Function
  onRefreshOrderItems: Function
  onDuplicateOrder: Function
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
  deleteProductType: Function
  updateProductType: Function
  companyProductTypes: any[]
  history: History
  catchWeightValues: any
  getWeightsByOrderItemIds: Function
  addCatchWeightByNumber: Function
  deleteCatchWeightByNumber: Function
  orderPrintVersion: Function
  openAddOffItem: Function
  oneOffItemList: OrderItem[]
  onUpsetOffItem: Function
  onRemoveOffItem: Function
  resetLoading: Function
  updateOrderLocked: Function
  onlyContent?: boolean
  extraCharge?: boolean
  addChargeClicked?: boolean
  toggleModal?: Function
  toggleOpenChildModal?: Function
  onChangeOrderItemPricingLogic: Function
  onChangeOrderItemPricingLogicWithoutRefresh: Function
  setShippingOrder?: Function
  printSetting: string
  sellerSetting: any
  visibleAddItemModal: boolean
  currentPrintContainerId: string
  clearCurrentPrintContainerId: Function
  setIsHealthBol: Function
  isHealthBol: boolean
  orderSortItems: any[]
}

export class CartContainer extends React.PureComponent<CartContainerProps> {
  state = {
    newModalVisible: false,
    printPickSheetReviewShow: false,
    PrintInvoiceReviewShow: false,
    printBillShow: false,
    viewSalesConfirmation: false,
    printManifestShow: false,
    printPickSheetRef: null,
    PrintBillOfLadingRef: null,
    PrintManifestRef: null,
    PrintInvoiceRef: null,
    searchInput: '',
    addItemClicked: false,
    addOneOffClicked: false,
    isLocked: this.props.currentOrder.isLocked,
    finishLoadingPrintLogo: this.props.logo == 'default' ? true : false,
    sendEmailModalVisible: false,
    sendEmailLoading: false,
    allEmails: []
  }

  componentDidMount() {
    const _this = this
    const { wholesaleClient } = this.props.currentOrder

    CustomerService.instance.getContacts(wholesaleClient.clientId).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        if (res.body.data) {
          let allEmails = []
          const emails = res.body.data.map(e => {
            return e.email
          })

          if (wholesaleClient.email) {
            allEmails.push(wholesaleClient.email)
          }
          allEmails = [...allEmails, ...emails]

          _this.setState({
            allEmails: [...new Set(allEmails)]
          })
        }
      },
      complete() {
        //
      },
    })

    localStorage.removeItem('FIRST_OPENED_CART')
    let map = { 65: false, 18: false, 91: false, 73: false, 17: false }
    let os = 'WIN'
    if (navigator.appVersion.indexOf('Mac') != -1) {
      os = 'MAC'
    }
    const _this = this
    jQuery('html')
      // .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode in map) {
          map[e.keyCode] = true
        }
        if (map[18] && map[65] && !_this.props.visibleAddItemModal) {
          setTimeout(function() {
            jQuery('.sales-cart-input.add-sales-cart-item').trigger('focus')
            jQuery('.sales-cart-input.add-sales-cart-item').trigger('click')

            // footer button
            const addItemBtn = jQuery('.footer-add-item .ant-input.ant-select-search__field')
            addItemBtn.val('')
            // addItemBtn.trigger('click')
          }, 30)
        }
        if (os == 'WIN') {
          if (map[17] && map[73]) {
            setTimeout(function() {
              if (_this.state.printPickSheetReviewShow) {
                jQuery('.print-pick-sheet').trigger('click')
              } else if (_this.state.PrintInvoiceReviewShow) {
                jQuery('.print-bill-lading').trigger('click')
              } else if (_this.state.printBillShow) {
                jQuery('.print-bill').trigger('click')
              } else if (_this.state.printManifestShow) {
                jQuery('.print-manifest').trigger('click')
              }
            }, 30)
          }
        } else if (os == 'MAC') {
          if (map[91] && map[73]) {
            setTimeout(function() {
              if (_this.state.printPickSheetReviewShow) {
                jQuery('.print-pick-sheet').trigger('click')
              } else if (_this.state.PrintInvoiceReviewShow) {
                jQuery('.print-bill-lading').trigger('click')
              } else if (_this.state.printBillShow) {
                jQuery('.print-bill').trigger('click')
              } else if (_this.state.printManifestShow) {
                jQuery('.print-manifest').trigger('click')
              }
            }, 30)
          }
        }
      })
      .bind('keyup', (e: any) => {
        for (const keycode in map) {
          map[keycode] = false
        }
      })
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.printingQuick !== nextProps.printingQuick && nextProps.printingQuick == false && nextProps.orderSortItems.length) {
      const copiesNum = this.props.sellerSetting.company.quickprintPicksheetCopies
      if (copiesNum) {
        const timer = 1 + Math.ceil(copiesNum / 5)
        setTimeout(()=> {
          printQuickPrintAsync('QuickPrintModal', timer)
        }, 10)
      }
    }
  }

  handleSelectOk = () => {
    this.setState({
      newModalVisible: false,
      searchInput: '',
    })
    window.localStorage.setItem('SALES-CART-MODAL-INDEX', '0')
    jQuery(`.modal-order-tab`)
      .find('.ant-tabs-tab')
      .removeClass('tab-focus')
    jQuery(`.modal-order-tab`)
      .find('.ant-table-row')
      .removeClass('focused-row')
  }

  handleAddOrderItem = (item: any) => {
    this.setState({ addItemClicked: true })
    this.props.onAddItem(item)
    this.handleSelectOk()
  }

  openAddModal = () => {
    this.setState({
      newModalVisible: true,
    })
    setTimeout(() => {
      jQuery(`.modal-order-tab`)
        .find('.ant-input')[0]
        .focus()
    }, 0)
  }

  handleUpdateStatus = (status: any) => {
    this.props.onUpdateOrderStatus(status)
  }

  handleDuplicateOrder = () => {
    this.props.onDuplicateOrder()
  }

  onSearch = (text: string) => {
    this.setState({
      searchInput: text,
    })
    window.localStorage.setItem('SALES-CART-MODAL-INDEX', '0')
  }

  openAddOffItem = (name?: string) => {
    this.setState({ addOneOffClicked: true })
    this.props.openAddOffItem(name)
  }

  onChangeSwitch = (checked: Boolean) => {
    this.setState({
      isLocked: checked,
    })
    this.props.updateOrderLocked(checked)
  }

  doPrintBill = (modalName: string, index: number) => {
    const { currentOrder, printSetting } = this.props
    let layout = 'portrait'
    const userPrintSetting = JSON.parse(printSetting)

    console.log(modalName)

    const orderId = this.props.match.params.orderId

    if (modalName.includes('PrintInvoiceModal') && typeof userPrintSetting.invoice_layout !== 'undefined') {
      layout = userPrintSetting.invoice_layout
      OrderService.instance.recordPrintActivity('invoice', orderId).subscribe()
    } else if (modalName === 'printPickSheetModal' && typeof userPrintSetting.pick_layout !== 'undefined') {
      layout = userPrintSetting.pick_layout
      OrderService.instance.recordPrintActivity('pickSheet', orderId).subscribe()
    } else if (modalName === 'printBillModal' && typeof userPrintSetting.bill_layout !== 'undefined') {
      layout = userPrintSetting.bill_layout
      OrderService.instance.recordPrintActivity('BOL', orderId).subscribe()
    } else if (modalName === 'printManifestModal' && typeof userPrintSetting.manifest_layout !== 'undefined') {
      layout = userPrintSetting.manifest_layout
      OrderService.instance.recordPrintActivity('shippingManifest', orderId).subscribe()
    }

    printWindowAsync(modalName, null, layout)
    if (currentOrder && currentOrder.wholesaleOrderStatus != 'SHIPPED') {
      if (index === 1) {
        // Picksheet
        this.handleUpdateStatus('PICKING')
      } else {
        // BOL, Invoice
        if (!this.props.disableShip) {
          this.props.setShippingOrder('print')
        }
      }
    }

    this.setState({
      printPickSheetReviewShow: false,
      PrintInvoiceReviewShow: false,
      printBillShow: false,
      printManifestShow: false,
    })
  }

  printContent: any = (type: number) => {
    let content
    const currentOrder = this.props.currentOrder
    if (type === 1) {
      content = this.state.printPickSheetRef
      if (currentOrder.wholesaleOrderStatus == 'NEW' || currentOrder.wholesaleOrderStatus == 'PLACED') {
        this.handleUpdateStatus('PICKING')
      }
    } else if (type === 2) {
      content = this.state.PrintInvoiceRef
    } else if (type == 3) {
      content = this.state.PrintBillOfLadingRef
      // if (currentOrder.wholesaleOrderStatus == 'PICKING') {
      //   this.handleUpdateStatus('SHIPPED')
      // }
    } else {
      content = this.state.PrintManifestRef
    }
    return content
  }

  changePrintLogoStatus = () => {
    this.setState({
      finishLoadingPrintLogo: true,
    })
  }
  render() {
    const { newModalVisible, searchInput, isLocked, finishLoadingPrintLogo, allEmails } = this.state
    const {
      loading,
      currentOrder,
      onlyContent,
      extraCharge,
      catchWeightValues,
      printSetting,
      companyProductTypes,
      printingQuick,
    } = this.props
    const userPrintSetting = JSON.parse(printSetting)
    const printDeliveryItems = Object.values(this.props.orderItemByProduct).filter((el) => {
      if (el.quantity > 0) {
        // Order Qty > 0
        return true
      }

      let pickedQty = 0
      if (!judgeConstantRatio(el)) {
        const recordWeights = catchWeightValues[el.wholesaleOrderItemId]
        pickedQty = recordWeights ? recordWeights.length : 0
      } else {
        pickedQty = el.picked
      }

      if (pickedQty > 0) {
        // Picked Qty > 0
        return true
      }
    })
    const printAllDeliveryItems = printDeliveryItems.concat(this.props.oneOffItemList)

    //Get Invoice Template
    const customInvoiceTemplates = _.get(companyProductTypes, 'customInvoiceTemplate', [])
    let customInvoiceTemplateName = '';

    if(customInvoiceTemplates && customInvoiceTemplates.length === 1){
      customInvoiceTemplateName = customInvoiceTemplates[0].name
    }

    const printInvoiceName = customInvoiceTemplateName.replace(' ', '') + 'PrintInvoiceModal'

    const itemsCountTextSuffix =
      this.props.orderItems.length > 0 ? (this.props.orderItems.length == 1 ? 'item' : 'items') : ''
    return (
      <ThemeSpin tip="Loading..." spinning={loading || printingQuick}>
        <SalesContainer className="sales-cart-tabs-area" style={{ padding: onlyContent ? '0' : '0 18px' }}>
          <ThemeModal
            closable={false}
            keyboard={true}
            okText="Close [esc]"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={'75%'}
            visible={newModalVisible}
            onOk={this.handleSelectOk}
            onCancel={this.handleSelectOk}
          >
            <ProductModal
              hasTabs={true}
              isAdd={true}
              selected={searchInput}
              onSelect={this.handleAddOrderItem}
              onSearch={this.onSearch}
              salesType="SELL"
            />
          </ThemeModal>

          <DetailsWrapper style={{ padding: onlyContent ? '0 0 20px' : 20, borderTopWidth: onlyContent ? 0 : 5 }}>
            {onlyContent !== true && (
              <Row>
                <Col lg={4} md={4}>
                  <DetailsTitle style={floatLeft}>
                    Cart{' '}
                    {this.props.orderItems.length > 0
                      ? `(${this.props.orderItems.length} ${itemsCountTextSuffix})`
                      : ''}
                  </DetailsTitle>
                </Col>
                <Col lg={10} md={10}>
                  <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    {currentOrder.wholesaleOrderStatus != 'SHIPPED' && currentOrder.wholesaleOrderStatus != 'CANCEL' ? (
                      <div>
                        <ThemeOutlineButton
                          style={{ ...{ marginTop: -5, marginRight: 10 } }}
                          shape="round"
                          onClick={this.openAddModal}
                          className="sales-cart-input add-item bold-blink"
                        >
                          <Icon type="plus-circle" />
                          Add Item
                        </ThemeOutlineButton>
                      </div>
                    ) : (
                      ''
                    )}
                    <div>
                      <ThemeOutlineButton
                        style={{ ...{ marginTop: -5, marginRight: 10 } }}
                        shape="round"
                        onClick={this.openAddOffItem}
                        className="sales-cart-input bold-blink one-off-item"
                      >
                        <Icon type="plus-circle" />
                        Add One Off Item
                      </ThemeOutlineButton>
                    </div>
                  </div>
                </Col>
                <Col lg={10} md={10}>
                  <ThemeButton
                    style={{ ...floatRight, ...{ marginTop: -5, marginRight: 20 } }}
                    shape="round"
                    onClick={() => this.handleDuplicateOrder()}
                    className="sales-cart-input header-last-tab duplicate-order"
                  >
                    <Icon type="copy" theme="filled" />
                    Duplicate Order
                  </ThemeButton>
                </Col>
              </Row>
            )}
            {extraCharge === true ? (
              <Row>
                <Col lg={24} md={24}>
                  <div style={{ minHeight: 400 }} className="one-off-table">
                    <OneOffItemTable
                      editable={
                        this.props.currentOrder &&
                        currentOrder.wholesaleOrderStatus !== 'CANCEL' &&
                        currentOrder.wholesaleOrderStatus !== 'SHIPPED'
                      }
                      currentOrder={this.props.currentOrder}
                      orderItems={this.props.oneOffItemList}
                      handleSave={this.props.onUpsetOffItem}
                      handleRemove={this.props.onRemoveOffItem}
                      handleUpdateAllItemStatus={this.props.onUpdateAllItemStatus}
                      addItemButtonClicked={this.state.addOneOffClicked || this.props.addChargeClicked}
                      isExtraCharge={extraCharge}
                      openAddOffItem={this.openAddOffItem}
                      companyProductTypes={this.props.companyProductTypes}
                    />
                  </div>
                </Col>
              </Row>
            ) : (
              <Row>
                <Col lg={24} md={24}>
                  <div
                    style={{ minHeight: 400 }}
                    className={`sales-order-table ${currentOrder.wholesaleOrderStatus == 'CANCEL' ? 'read-only' : ''}`}
                  >
                    <Cart
                      {...this.props}
                      toggleModal={this.props.toggleModal}
                      changeSelectedItems={this.props.changeSelectedItems}
                      onChagneInserItemPosition={this.props.onChagneInserItemPosition}
                      toggleOpenChildModal={this.props.toggleOpenChildModal}
                      onChangeOrderItemPricingLogic={this.props.onChangeOrderItemPricingLogic}
                      updateOrderItemForLot={this.props.updateOrderItemForLot}
                      onChangeOrderItemPricingLogicWithoutRefresh={
                        this.props.onChangeOrderItemPricingLogicWithoutRefresh
                      }
                    />
                  </div>
                </Col>
              </Row>
            )}

            {extraCharge !== true && false && (
              <Row style={{ marginTop: 20, paddingBottom: 20 }}>
                <Col lg={20} md={20}>
                  <Row>
                    {/* <Col lg={6} md={24} style={ml0}>
                    <ThemeButton shape="round" style={fullButton} disabled>
                      <Icon type="reload" />
                      Update Picking
                    </ThemeButton>
                  </Col> */}
                    <Col md={8} lg={4} style={{ paddingTop: '7px' }}>
                      <ThemeSwitch checked={isLocked} onChange={this.onChangeSwitch} />
                      <LockText>Lock/Finalize Order</LockText>
                    </Col>
                    <Col md={8} lg={4}>
                      <ThemeButton
                        shape="round"
                        style={fullButton}
                        onClick={this.openPrintModal.bind(this, 1)}
                        disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}
                      >
                        <Icon type="printer" theme="filled" />
                        Preview Pick Sheet
                      </ThemeButton>
                    </Col>
                    <Col md={8} lg={4}>
                      <ThemeButton
                        shape="round"
                        style={fullButton}
                        onClick={this.openPrintModal.bind(this, 2)}
                        disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}
                      >
                        <Icon type="printer" theme="filled" />
                        Preview Delivery List
                      </ThemeButton>
                    </Col>
                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleUpdateStatus('CANCEL')}
                      disabled={currentOrder.wholesaleOrderStatus == 'CANCEL'}>

                      <Icon type="close-circle" theme="filled" />
                      Cancel Order
                    </ThemeButton>
                  </Col> */}

                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleUpdateStatus('INVOICED')}>
                      <Icon type="dollar-circle" theme="filled" />
                      Set to Invoiced
                    </ThemeButton>
                  </Col> */}
                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleDuplicateOrder()}>
                      <Icon type="copy" theme="filled" />
                      Duplicate Order
                    </ThemeButton>
                  </Col> */}
                    {/* <Col lg={4} md={24}>
                    <ThemeButton shape="round" style={fullButton} onClick={() => this.handleSyncQBOOrder()}>
                      <Icon type={this.props.loading ? "loading" : "cloud"} theme="filled" />
                      QBO Sync
                    </ThemeButton>
                  </Col> */}
                  </Row>
                </Col>
                <Col md={4}>
                  <Col lg={24} md={24}>
                    <Popconfirm
                      placement="topRight"
                      title="Are you sure to cancel the order?"
                      onConfirm={() => this.handleUpdateStatus('CANCEL')}
                      okText="Yes"
                      cancelText="No"
                    >
                      <ThemeOutlineCancelButton
                        shape="round"
                        style={fullButton}
                        disabled={currentOrder.wholesaleOrderStatus == 'CANCEL' || isLocked}
                      >
                        <Icon type="close-circle" theme="filled" />
                        Cancel Order
                      </ThemeOutlineCancelButton>
                    </Popconfirm>
                  </Col>
                </Col>
              </Row>
            )}
          </DetailsWrapper>
          {this.state.PrintInvoiceReviewShow && (
            <Modal
              width={1200}
              footer={null}
              visible={this.state.PrintInvoiceReviewShow}
              onCancel={this.closePrintModal.bind(this, 2)}
              wrapClassName="print-modal"
            >
              <ThemeSpin spinning={this.props.loading || this.props.updateOrderQuantityLoading.fetching}>
                <PrintModalHeader>
                  <Tooltip title="Print invoice and set order status to Shipped" placement="bottom">
                    <ThemeButton
                      size="large"
                      className="print-bill-lading"
                      onClick={() => this.doPrintBill(printInvoiceName, 2)} /* 'PrintInvoiceModal' */
                      // disabled={!finishLoadingPrintLogo}
                    >
                      <Icon type="printer" theme="filled" />
                      Print{' '}
                      {userPrintSetting && userPrintSetting.invoice_title ? userPrintSetting.invoice_title : 'Invoice'}
                    </ThemeButton>
                  </Tooltip>
                  <ThemeButton
                    size="large"
                    className="print-bill-lading"
                    style={{ marginRight: 10 }}
                    onClick={() => this.setState({ sendEmailModalVisible: true })}
                    // disabled={!finishLoadingPrintLogo}
                  >
                    <Icon type="mail" theme="filled" />
                    Email{' '}
                    {userPrintSetting && userPrintSetting.invoice_title
                      ? userPrintSetting.invoice_title
                      : 'Invoice'}{' '}
                  </ThemeButton>
                  <div className="clearfix" />
                  {this.state.sendEmailModalVisible && (
                    <WrappedSendEmail
                      title="Invoice"
                      allEmails={allEmails}
                      onCancel={() => this.setState({ sendEmailModalVisible: false })}
                      receiver={this.props.currentOrder.wholesaleClient.mainContact.email}
                      sendEmailLoading={this.state.sendEmailLoading}
                      clientId={this.props.currentOrder.wholesaleClient.clientId}
                      handleSendEmail={(values) => {
                        this.setState({ sendEmailLoading: true }, () => {
                          sendEmail({
                            ...values,
                            title: 'Invoice ' + this.props.match.params.orderId,
                            contentType: 'invoice',
                            elementId: printInvoiceName,/* 'PrintInvoiceModal' */
                            orderId: this.props.match.params.orderId,
                            layout: userPrintSetting.invoice_layout,
                            handleSuccess: (v) => this.setState({ sendEmailLoading: v, sendEmailModalVisible: false }),
                          })
                        })
                      }}
                    />
                  )}
                </PrintModalHeader>
                <div id={printInvoiceName}>
                  {/* 'PrintInvoiceModal' */}
                  <PrintInvoice
                    getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                    catchWeightValues={this.props.catchWeightValues}
                    orderItems={printAllDeliveryItems}
                    currentOrder={this.props.currentOrder}
                    companyName={this.props.companyName}
                    company={this.props.company}
                    logo={this.props.logo}
                    printSetting={this.props.printSetting}
                    sellerSetting={this.props.sellerSetting}
                    changePrintLogoStatus={this.changePrintLogoStatus}
                    fulfillmentOptionType={this.props.fulfillmentOptionType}
                    type={'invoice'}
                    currentPrintContainerId={this.props.currentPrintContainerId}
                    orderId={this.props.match.params.orderId}
                    companyProductTypes={companyProductTypes}
                    oneOffItemList={this.props.oneOffItemList}
                    paymentTerms={[
                      ..._.get(companyProductTypes, 'paymentTerms', []),
                      ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                    ]}
                    /*customInvoiceTemplate={[
                      ..._.get(companyProductTypes, 'customInvoiceTemplate', [])
                    ]}*/
                    ref={(el) => {
                      this.setState({ PrintInvoiceRef: el })
                    }}
                  />
                </div>
              </ThemeSpin>
            </Modal>
          )}
          {this.state.viewSalesConfirmation && (
            <Modal
              width={1200}
              footer={null}
              visible={this.state.viewSalesConfirmation}
              onCancel={this.closePrintModal.bind(this, 6)}
              wrapClassName="print-modal"
            >
              <ThemeSpin spinning={this.props.loading || this.props.updateOrderQuantityLoading.fetching}>
                <ViewSalesConfirmation
                  getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                  catchWeightValues={this.props.catchWeightValues}
                  orderItems={printAllDeliveryItems}
                  currentOrder={this.props.currentOrder}
                  companyName={this.props.companyName}
                  company={this.props.company}
                  logo={this.props.logo}
                  printSetting={this.props.printSetting}
                  sellerSetting={this.props.sellerSetting}
                  changePrintLogoStatus={this.changePrintLogoStatus}
                  fulfillmentOptionType={this.props.fulfillmentOptionType}
                  type={'invoice'}
                  currentPrintContainerId={this.props.currentPrintContainerId}
                  orderId={this.props.match.params.orderId}
                  companyProductTypes={companyProductTypes}
                  oneOffItemList={this.props.oneOffItemList}
                  paymentTerms={[
                    ..._.get(companyProductTypes, 'paymentTerms', []),
                    ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                  ]}
                />
              </ThemeSpin>
            </Modal>
          )}
          {this.state.printBillShow && (
            <Modal
              width={1200}
              footer={null}
              visible={this.state.printBillShow}
              onCancel={this.closePrintModal.bind(this, 3)}
              wrapClassName="print-modal"
            >
              <PrintModalHeader>
                <Tooltip title="Print bill of lading and set order status to Shipped" placement="bottom">
                  <ThemeButton
                    size="large"
                    className="print-bill"
                    onClick={() => this.doPrintBill('printBillModal', 3)}
                    disabled={!finishLoadingPrintLogo}
                  >
                    <Icon type="printer" theme="filled" />
                    Print{' '}
                    {userPrintSetting && userPrintSetting.bill_title ? userPrintSetting.bill_title : 'Bill of Lading'}
                  </ThemeButton>
                </Tooltip>
                <ThemeButton
                  size="large"
                  className="print-bill-lading"
                  style={{ marginRight: 10 }}
                  onClick={() => this.setState({ sendEmailModalVisible: true })}
                  // disabled={!finishLoadingPrintLogo}
                >
                  <Icon type="mail" theme="filled" />
                  Email
                </ThemeButton>
                <div className="clearfix" />
                {this.state.sendEmailModalVisible && (
                  <WrappedSendEmail
                    title="Bill of Lading"
                    allEmails={allEmails}
                    onCancel={() => this.setState({ sendEmailModalVisible: false })}
                    receiver={this.props.currentOrder.wholesaleClient.mainContact.email}
                    sendEmailLoading={this.state.sendEmailLoading}
                    clientId={this.props.currentOrder.wholesaleClient.clientId}
                    handleSendEmail={(values) => {
                      this.setState({ sendEmailLoading: true }, () => {
                        sendEmail({
                          ...values,
                          title: 'bill of lading',
                          contentType: 'BOL',
                          elementId: 'printBillModal',
                          orderId: this.props.match.params.orderId,
                          layout: userPrintSetting.bill_layout,
                          handleSuccess: (v) => this.setState({ sendEmailLoading: v, sendEmailModalVisible: false }),
                        })
                      })
                    }}
                  />
                )}
              </PrintModalHeader>
              <div id={'printBillModal'}>
                <PrintBillOfLading
                  title={'Bill of Lading'.toUpperCase()}
                  getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                  catchWeightValues={this.props.catchWeightValues}
                  orderItems={printAllDeliveryItems}
                  currentOrder={this.props.currentOrder}
                  companyName={this.props.companyName}
                  company={this.props.company}
                  logo={this.props.logo}
                  printSetting={this.props.printSetting}
                  sellerSetting={this.props.sellerSetting}
                  changePrintLogoStatus={this.changePrintLogoStatus}
                  type={'bill'}
                  fulfillmentOptionType={this.props.fulfillmentOptionType}
                  currentPrintContainerId={this.props.currentPrintContainerId}
                  orderId={this.props.match.params.orderId}
                  companyProductTypes={companyProductTypes}
                  isHealthBol={this.props.isHealthBol}
                  paymentTerms={[
                    ..._.get(companyProductTypes, 'paymentTerms', []),
                    ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                  ]}
                  ref={(el) => {
                    this.setState({ PrintBillOfLadingRef: el })
                  }}
                />
              </div>
            </Modal>
          )}
          <Modal
            width={1200}
            footer={null}
            visible={this.state.printManifestShow}
            onCancel={this.closePrintModal.bind(this, 4)}
            wrapClassName="print-modal"
          >
            <PrintModalHeader>
              <Tooltip title="Print shipping manifest and set order status to Shipped" placement="bottom">
                <ThemeButton
                  size="large"
                  className="print-manifest"
                  onClick={() => this.doPrintBill('printManifestModal', 4)}
                  disabled={!finishLoadingPrintLogo}
                >
                  <Icon type="printer" theme="filled" />
                  Print{' '}
                  {userPrintSetting && userPrintSetting.manifest_title
                    ? userPrintSetting.manifest_title
                    : 'Shipping Manifest'}
                </ThemeButton>
              </Tooltip>
              <div className="clearfix" />
            </PrintModalHeader>
            <div id={'printManifestModal'}>
              <PrintBillOfLading
                title={'Shipping Manifest'.toUpperCase()}
                getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                catchWeightValues={this.props.catchWeightValues}
                orderItems={printAllDeliveryItems}
                currentOrder={this.props.currentOrder}
                companyName={this.props.companyName}
                company={this.props.company}
                logo={this.props.logo}
                printSetting={this.props.printSetting}
                sellerSetting={this.props.sellerSetting}
                changePrintLogoStatus={this.changePrintLogoStatus}
                type={'manifest'}
                fulfillmentOptionType={this.props.fulfillmentOptionType}
                currentPrintContainerId={this.props.currentPrintContainerId}
                orderId={this.props.match.params.orderId}
                companyProductTypes={companyProductTypes}
                isHealthBol={this.props.isHealthBol}
                paymentTerms={[
                  ..._.get(companyProductTypes, 'paymentTerms', []),
                  ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                ]}
                ref={(el) => {
                  this.setState({ PrintManifestRef: el })
                }}
              />
            </div>
          </Modal>

          {this.state.printPickSheetReviewShow && (
            <Modal
              width={1080}
              footer={null}
              visible={this.state.printPickSheetReviewShow}
              onCancel={this.closePrintModal.bind(this, 1)}
              wrapClassName="print-modal"
            >
              <ThemeSpin
                spinning={this.props.loading || this.props.updating || this.props.updateOrderQuantityLoading.fetching}
              >
                <PrintModalHeader>
                  <Tooltip title="Print pick sheet and set order status to Picking123" placement="bottom">
                    <ThemeButton
                      size="large"
                      className="print-pick-sheet"
                      onClick={() => this.doPrintBill('printPickSheetModal', 1)}
                      disabled={!finishLoadingPrintLogo}
                    >
                      <Icon type="printer" theme="filled" />
                      {this.props.currentPrintContainerId
                        ? 'Print Load Sheet'
                        : `Print ${
                            userPrintSetting && userPrintSetting.pick_title ? userPrintSetting.pick_title : 'Pick Sheet'
                          }`}
                    </ThemeButton>
                  </Tooltip>
                  <ThemeButton
                    size="large"
                    className="print-bill-lading"
                    style={{ marginRight: 10 }}
                    onClick={() => this.setState({ sendEmailModalVisible: true })}
                    // disabled={!finishLoadingPrintLogo}
                  >
                    <Icon type="mail" theme="filled" />
                    Email
                  </ThemeButton>
                  <div className="clearfix" />
                  {this.state.sendEmailModalVisible && (
                    <WrappedSendEmail
                      title="Pick Sheet"
                      allEmails={allEmails}
                      onCancel={() => this.setState({ sendEmailModalVisible: false })}
                      receiver={''}
                      sendEmailLoading={this.state.sendEmailLoading}
                      clientId={this.props.currentOrder.wholesaleClient.clientId}
                      handleSendEmail={(values) => {
                        this.setState({ sendEmailLoading: true }, () => {
                          sendEmail({
                            ...values,
                            title: 'pick sheet',
                            contentType: 'picksheet',
                            elementId: 'printPickSheetModal',
                            orderId: this.props.match.params.orderId,
                            layout: userPrintSetting.pick_layout,
                            handleSuccess: (v) => this.setState({ sendEmailLoading: v, sendEmailModalVisible: false }),
                          })
                        })
                      }}
                    />
                  )}
                </PrintModalHeader>
                <div id={'printPickSheetModal'}>
                  <PrintPickSheet
                    orderItems={this.props.orderSortItems}
                    currentOrder={this.props.currentOrder}
                    companyName={this.props.companyName}
                    company={this.props.company}
                    catchWeightValues={this.props.catchWeightValues}
                    logo={this.props.logo}
                    printSetting={this.props.printSetting}
                    sellerSetting={this.props.sellerSetting}
                    changePrintLogoStatus={this.changePrintLogoStatus}
                    currentPrintContainerId={this.props.currentPrintContainerId}
                    orderId={this.props.match.params.orderId}
                    ref={(el) => {
                      this.setState({ printPickSheetRef: el })
                    }}
                  />
                </div>
              </ThemeSpin>
            </Modal>
          )}
        </SalesContainer>
        <div id="QuickPrintModal" style={{ display: 'none' }}>
          {this.props.sellerSetting &&
            (typeof this.props.sellerSetting.company.quickprintPicksheetEnabled === 'undefined' ||
              this.props.sellerSetting.company.quickprintPicksheetEnabled) && (
              <>
                {[...Array(this.props.sellerSetting.company.quickprintPicksheetCopies ?? 1).keys()].map((i) => (
                  <div key={`print-picksheet-${i}`}>
                    <PrintPickSheet
                      orderItems={this.props.orderSortItems}
                      currentOrder={this.props.currentOrder}
                      companyName={this.props.companyName}
                      company={this.props.company}
                      catchWeightValues={this.props.catchWeightValues}
                      logo={this.props.logo}
                      printSetting={this.props.printSetting}
                      sellerSetting={this.props.sellerSetting}
                      currentPrintContainerId={this.props.currentPrintContainerId}
                      orderId={this.props.match.params.orderId}
                      changePrintLogoStatus={this.changePrintLogoStatus}
                    />
                    <div style={{ pageBreakAfter: 'always' }} />
                  </div>
                ))}
              </>
            )}

          {this.props.sellerSetting &&
            (typeof this.props.sellerSetting.company.quickprintInvoiceEnabled === 'undefined' ||
              this.props.sellerSetting.company.quickprintInvoiceEnabled) && (
              <>
                {[...Array(this.props.sellerSetting.company.quickprintInvoiceCopies ?? 1).keys()].map((i) => (
                  <div key={`print-invoice-${i}`}>
                    <PrintInvoice
                      getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                      catchWeightValues={this.props.catchWeightValues}
                      orderItems={printAllDeliveryItems}
                      currentOrder={this.props.currentOrder}
                      companyName={this.props.companyName}
                      company={this.props.company}
                      logo={this.props.logo}
                      printSetting={this.props.printSetting}
                      sellerSetting={this.props.sellerSetting}
                      changePrintLogoStatus={this.changePrintLogoStatus}
                      fulfillmentOptionType={this.props.fulfillmentOptionType}
                      type={'invoice'}
                      currentPrintContainerId={this.props.currentPrintContainerId}
                      orderId={this.props.match.params.orderId}
                      companyProductTypes={companyProductTypes}
                      oneOffItemList={this.props.oneOffItemList}
                      overrideCustomLayout={true}
                      modalName={'QuickPrintModal'}
                      paymentTerms={[
                        ..._.get(companyProductTypes, 'paymentTerms', []),
                        ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                      ]}
                    />
                    <div style={{ pageBreakAfter: 'always' }} />
                  </div>
                ))}
              </>
            )}

          {this.props.sellerSetting &&
            (typeof this.props.sellerSetting.company.quickprintInvoiceOfficeEnabled === 'undefined' ||
              this.props.sellerSetting.company.quickprintInvoiceOfficeEnabled) && (
              <>
                {[...Array(this.props.sellerSetting.company.quickprintInvoiceOfficeCopies ?? 1).keys()].map((i) => (
                  <div key={`print-invoice-${i}`}>
                    <PrintInvoice
                      getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                      catchWeightValues={this.props.catchWeightValues}
                      orderItems={printAllDeliveryItems}
                      currentOrder={this.props.currentOrder}
                      companyName={this.props.companyName}
                      company={this.props.company}
                      logo={this.props.logo}
                      printSetting={this.props.printSetting}
                      sellerSetting={this.props.sellerSetting}
                      changePrintLogoStatus={this.changePrintLogoStatus}
                      fulfillmentOptionType={this.props.fulfillmentOptionType}
                      type={'invoice'}
                      isCopy={true}
                      currentPrintContainerId={this.props.currentPrintContainerId}
                      companyProductTypes={companyProductTypes}
                      orderId={this.props.match.params.orderId}
                      oneOffItemList={this.props.oneOffItemList}
                      overrideCustomLayout={true}
                      paymentTerms={[
                        ..._.get(companyProductTypes, 'paymentTerms', []),
                        ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                      ]}
                    />
                    <div style={{ pageBreakAfter: 'always' }} />
                  </div>
                ))}
              </>
            )}

          {this.props.sellerSetting && this.props.sellerSetting.company.quickprintBolEnabled && (
            <>
              {[...Array(this.props.sellerSetting.company.quickprintBolCopies ?? 1).keys()].map((i) => (
                <div key={`print-bol-${i}`}>
                  <PrintBillOfLading
                    title={'Bill of Lading'.toUpperCase()}
                    getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                    catchWeightValues={this.props.catchWeightValues}
                    orderItems={printAllDeliveryItems}
                    currentOrder={this.props.currentOrder}
                    companyName={this.props.companyName}
                    company={this.props.company}
                    logo={this.props.logo}
                    printSetting={this.props.printSetting}
                    sellerSetting={this.props.sellerSetting}
                    changePrintLogoStatus={this.changePrintLogoStatus}
                    type={'bill'}
                    fulfillmentOptionType={this.props.fulfillmentOptionType}
                    currentPrintContainerId={this.props.currentPrintContainerId}
                    orderId={this.props.match.params.orderId}
                    companyProductTypes={companyProductTypes}
                    isHealthBol={this.props.isHealthBol}
                    paymentTerms={[
                      ..._.get(companyProductTypes, 'paymentTerms', []),
                      ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                    ]}
                  />
                  <div style={{ pageBreakAfter: 'always' }} />
                </div>
              ))}
            </>
          )}
        </div>
      </ThemeSpin>
    )
  }

  openPrintModal = (type: number) => {
    console.log('* type = ', type)
    const orderId = this.props.match.params.orderId
    const time = setInterval(() => {
      if (!this.props.updateOrderQuantityLoading.fetching) {
        // this.props.getOrderItemsById(orderId)
        const printDeliveryItems = this.props.orderItems.filter((el) => el.picked && el.picked != 0)
        const ids = printDeliveryItems.filter((el) => !judgeConstantRatio(el)).map((el) => el.wholesaleOrderItemId)
        this.props.getWeightsByOrderItemIds(ids)
        clearInterval(time)
      }
    }, 100)

    if (type === 1) {
      this.setState({
        printPickSheetReviewShow: true,
      })
      this.props.orderPrintVersion(orderId)

      // sort items
      const jsonObj = JSON.parse(this.props.printSetting)
      const item_sort = jsonObj.item_sort
      const timer = setInterval(() => {
        if (!this.props.updateOrderQuantityLoading.fetching) {
          this.props.startUpdating()
          this.props.getOrderItemsByIdAndSort({
            id: orderId,
            sort: item_sort,
          })
          clearInterval(timer)
        }
      }, 100)
      // this.props.onUpdateOrderStatus("PICKING")
    } else {
      if (type === 2) {
        this.setState({
          PrintInvoiceReviewShow: true,
        })
      } else if (type === 3) {
        this.setState({
          printBillShow: true,
        })
      } else if (type === 4) {
        this.setState({
          printManifestShow: true,
        })
      } else if (type === 5) {
        // Quick Print

        this.props.startQuickPrint()
        const jsonObj = JSON.parse(this.props.printSetting)
        const item_sort = jsonObj.item_sort
        const timer = setInterval(() => {
          if (!this.props.updateOrderQuantityLoading.fetching) {
            this.props.getOrderItemsByIdAndSort({
              id: orderId,
              sort: item_sort,
            })
            clearInterval(timer)
          }
        }, 200)

        OrderService.instance.recordPrintActivity('invoice', orderId).subscribe()
        OrderService.instance.recordPrintActivity('pickSheet', orderId).subscribe()
        OrderService.instance.recordPrintActivity('BOL', orderId).subscribe()
      } else if (type === 6) {
        console.log('*** viewSalesConfirmation')

        // Sales Confirmation
        this.setState({
          viewSalesConfirmation: true,
        })
      }
    }
  }

  closePrintModal = (type: number) => {
    this.props.clearCurrentPrintContainerId()
    this.props.setIsHealthBol(false)
    if (type === 1) {
      this.setState({
        printPickSheetReviewShow: false,
      })
    } else if (type === 2) {
      this.setState({
        PrintInvoiceReviewShow: false,
      })
    } else if (type === 3) {
      this.setState({
        printBillShow: false,
      })
    } else if (type === 6) {
      this.setState({
        viewSalesConfirmation: false,
      })
    } else {
      this.setState({
        printManifestShow: false,
      })
    }
  }
}

const SendEmailModal: React.FC<any> = ({
  title,
  onCancel,
  receiver,
  form,
  handleSendEmail,
  sendEmailLoading,
  clientId,
  allEmails
}) => {
  const { getFieldDecorator, validateFields } = form
  const [loading, setLoading] = useState(false)
  const [emails, setEmails] = useState(allEmails ?? [])
  const [latestInvoiceEmail, setLatestInvoiceEmail] = useState({
    content: '',
    cc: [],
    receiver: [],
  })

  useEffect(() => {
    setLoading(true)
    OrderService.instance.getLatestInvoiceEmail(clientId).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        if (res.body.data) {
          setLatestInvoiceEmail({
            content: _.get(res, 'body.data.content', ''),
            cc: res.body.data.cc ? _.compact(res.body.data.cc.split(',')) : [],
            receiver: title === 'Pick Sheet' ? [] : _.compact(res.body.data.receiver.split(',')),
          })
        }
      },
      complete() {
        setLoading(false)
      },
    })
  }, [])

  useEffect(() => {
    const initialValue = latestInvoiceEmail.receiver.length
      ? latestInvoiceEmail.receiver
      : receiver
        ? [receiver]
        : [];
    let notAddedEmails = [];
    if (allEmails) {
      allEmails.forEach(e => {
        if (initialValue.indexOf(e) < 0) {
          notAddedEmails.push(e)
        }
      })
    }
    setEmails(notAddedEmails)
  }, [latestInvoiceEmail, receiver, allEmails])

  const handleSubmit = () => {
    validateFields((err, values) => {
      if (!err) {
        handleSendEmail(values)
      }
    })
  }

  const onAddEmail = (email: string) => {
    let receiver = form.getFieldValue('receiver')
    receiver.push(email);
    const found = emails.indexOf(email);
    emails.splice(found, 1);
    setEmails(emails)
    form.setFieldsValue({
      receiver,
    });
  }

  const onDeSelect = (receiver: string) => {
    emails.push(receiver)
    setEmails(emails)
  }

  const CustomSelect = styled(Select)`
    .ant-select-selection__choice {
      border-radius: 20px;
      background: #dfdfdf;
    }
    .ant-select-selection__choice__content {
      line-height: 15px;
      overflow: initial;
    }
  `

  return (
    <Modal
      width={800}
      visible
      title={`Email ${title}`}
      onCancel={onCancel}
      footer={[
        <Button key="back" onClick={onCancel}>
          Cancel
        </Button>,
        <ThemeButton key="submit" type="primary" loading={sendEmailLoading} onClick={handleSubmit}>
          {!sendEmailLoading && (
            <IconSvg
              type="send-email"
              viewBox="0 0 1024 1024"
              width="18"
              height="18"
              style={{ marginRight: 5, verticalAlign: 'sub' }}
            />
          )}
          Send {title}
        </ThemeButton>,
      ]}
      cancelText="Cancel"
    >
      <ThemeSpin spinning={loading}>
        <Form layout="vertical">
          <Form.Item label="Send to" className={emails.length ? 'no-error' : ''}>
            {getFieldDecorator('receiver', {
              initialValue: latestInvoiceEmail.receiver.length
                ? latestInvoiceEmail.receiver
                : receiver
                ? [receiver]
                : [],
              rules: [
                {
                  required: true,
                  message: 'Please input recipient',
                },
              ],
            })(<CustomSelect mode="tags" placeholder="" dropdownStyle={{ display: 'none' }} onDeselect={onDeSelect} />)}
          </Form.Item>
          {emails.length > 0 && (
            <AddEmails>
              Add:
              {emails.map((e: string, index: number) => {
                return (
                  <a key={`email-${index}`} onClick={() => onAddEmail(e)}>{e}</a>
                )
              })}
            </AddEmails>
          )}
          <Form.Item label="CC">
            {getFieldDecorator('cc', {
              initialValue: latestInvoiceEmail.cc,
              rules: [],
            })(<CustomSelect mode="tags" placeholder="" dropdownStyle={{ display: 'none' }} />)}
          </Form.Item>
          <Form.Item label="Email body">
            {getFieldDecorator('content', {
              initialValue: latestInvoiceEmail.content,
              rules: [],
            })(<Input.TextArea placeholder="" rows={4} />)}
          </Form.Item>
        </Form>
      </ThemeSpin>
    </Modal>
  )
}
export const WrappedSendEmail = Form.create()(SendEmailModal)

const D = withTheme(CartContainer)
export default D
