import React from 'react'
import moment from 'moment'
import { Col, DatePicker, Icon, Row, Table, Radio } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { PriceModalWrapper, ThemeIconButton } from '~/modules/customers/customers.style'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import { Icon as IconSvg } from '~/components'
import _ from 'lodash'

const calendarIcon = <IconSvg type="dark-calendar" viewBox={void 0} />

type PriceModalProps = OrdersDispatchProps &
  OrdersStateProps & {
    itemId: number
    clientId: string
    isExpanded?: boolean
  }

class PriceModal extends React.PureComponent<PriceModalProps> {
  state: any
  dateFormat = 'MM/DD/YYYY'
  constructor(props: PriceModalProps) {
    super(props)
    this.state = {
      from: moment().subtract(6, 'month'),
      to: moment(),
    }
  }

  onChangeRadio = (e) => {
    console.log('radio checked', e.target.value)
    this.setState({
      value: e.target.value,
    })
  }

  componentWillReceiveProps(nextProps: PriceModalProps) {
    // if (this.props.itemId !== nextProps.itemId) {
    //   this.props.getProductItem(nextProps.itemId)
    //   this._loadData(nextProps.itemId)
    // }
  }

  onFromDateChange = (_dateMoment: any, dateString: string) => {
    this.setState(
      {
        from: _dateMoment,
      },
      () => {
        this._loadData(this.props.itemId)
      },
    )
  }

  onToDateChange = (_dateMoment: any, dateString: string) => {
    this.setState(
      {
        to: _dateMoment,
      },
      () => {
        this._loadData(this.props.itemId)
      },
    )
  }

  componentDidMount() {
    this.props.getProductItem({itemId: this.props.itemId, clientId: this.props.currentOrder?.wholesaleClient.clientId})
    this._loadData(this.props.itemId)
  }

  _loadData = (itemId: any) => {
    const { from, to } = this.state
    const { clientId } = this.props
    this.props.getSalesPrice({
      itemId,
      clientId,
      from: from.format(this.dateFormat),
      to: to.format(this.dateFormat),
    })
  }

  render() {
    const { salesPrice, product, isExpanded } = this.props
    const { from, to } = this.state

    let prices: any[] = []
    if (salesPrice && salesPrice.length > 0) {
      salesPrice.forEach((item) => {
        const dateStr = moment(item.createdDate).format('MM/DD/YY')
        const foundIndex = prices.findIndex(price => moment(price[0]).format('MM/DD/YY') === dateStr)
        if (foundIndex > -1) {
          // use max price in case of same date
          prices[foundIndex][1] = Math.max(item.price, prices[foundIndex][1]);
        } else {
          prices.push([item.createdDate, item.price])
        }
      })
    }
    prices.sort((a, b) => a[0] - b[0])

    let dataSource: any[] = []
    if (product) {
      const groups = ['A', 'B', 'C', 'D', 'E']
      groups.forEach((el, index) => {
        dataSource.push({
          id: index + 1,
          level: el,
          cost: product.cost,
          low: product[`low${el}`],
          high: product[`high${el}`],
          updatedDate: moment(product.updatedDate).format('MM/DD/YYYY h:mm A'),
          margin: product[`margin${el}`],
          markup: product[`markup${el}`],
        })
      })
    }

    const columns = [
      {
        title: 'LEVEL',
        dataIndex: 'level',
        align: 'center',
        render: t => <span className='blod'>{t}</span>
      },
      // {
      //   title: 'COST',
      //   dataIndex: 'cost',
      //   align: 'center',
      //   width: 98,
      //   render: (cost: number) => `$${cost.toFixed(2)}`,
      // },
      // {
      //   title: 'LOW',
      //   dataIndex: 'low',
      //   align: 'center',
      //   width: 95,
      //   render: (cost: number) => `$${cost.toFixed(2)}`,
      // },
      // {
      //   title: 'HIGH',
      //   dataIndex: 'high',
      //   align: 'center',
      //   width: 98,
      //   render: (cost: number) => `$${cost.toFixed(2)}`,
      // },
      {
        title: 'LAST UPDATED',
        dataIndex: 'updatedDate',
        align: 'center',
        width: 136,
      },
      {
        title: 'PRICING FOMULA',
        dataIndex: 'margin',
        align: 'center',
        width: 136,
        render: (margin: number, record: any) => {
          return margin != 0 ? `${margin}%` : 'Not set up'
        },
      },
    ]

    const mainColor = this.props.theme.primary

    return (
      <PriceModalWrapper className={isExpanded ? 'within-table' : ''}>
        {isExpanded !== true && <div className="sub-title">{product ? product.variety : '--'}</div>}
        <div className="left-side">
          <Row gutter={[26, 18]}>
            {/* <Col md={12}>
              <div className="has-border">
                <div className="label">Current group</div>
                <div className="value">{product ? product.defaultGroup : '--'}</div>
              </div>
            </Col> */}
            <Col md={12}>
              <div className="has-border">
                <div className="label">DEFAULT PRICE</div>
                <div className="value">${_.get(product, 'price', 0).toFixed(2)}</div>
              </div>
            </Col>
            <Col md={12}>
              <div className="has-border">
                <div className="label">LAST PRICE</div>
                <div className="value">{_.get(product, 'lastPrice') ? _.get(product, 'lastPrice').toFixed(2) : '--'}</div>
              </div>
            </Col>
            <Col md={12}>
              <div className="has-border">
                <div className="label">COST</div>
                <div className="value">${_.get(product, 'cost', 0).toFixed(2)}</div>
              </div>
            </Col>
            <Col md={12}>
              <div className="has-border">
                <div className="label">MINIMUM PRICE</div>
                <div className="value">${(prices.length ? _.min(prices.map(v => v[1])) : 0).toFixed(2)}</div>
              </div>
            </Col>
            <Col md={12}>
              <div className="has-border">
                <div className="label">AVERAGE PRICE</div>
                <div className="value">${(prices.length ? _.sum(prices.map(v => v[1])) / prices.length : 0).toFixed(2)}</div>
              </div>
            </Col>
            <Col md={12}>
              <div className="has-border">
                <div className="label">MAXIMUM PRICE</div>
                <div className="value">${(prices.length ? _.max(prices.map(v => v[1])) : 0).toFixed(2)}</div>
              </div>
            </Col>
          </Row>
        </div>
        <div className="right-side">
          <div className="left section-title">Pricing Levels</div>
          <div className="clearfix" />
          <Row className="has-border" style={{ marginTop: 9 }}>
            <Col md={24}>
              <Table bordered dataSource={dataSource} columns={columns} rowKey="id" pagination={false} />
            </Col>
          </Row>
        </div>
        <div className="clearfix" />
        <div className="section-title" style={{ margin: '21px 0 9px' }}>
          Customer Sales History
        </div>
        <div className="has-border" style={{ padding: '13px 23px 0' }}>
          <Flex style={{ flexDirection: 'row' }}>
            <div style={{ width: 114 }}>
              <div className="dp-label">From: </div>
              <DatePicker
                placeholder={'MM/DD/YYYY'}
                format={'MM/DD/YYYY'}
                suffixIcon={calendarIcon}
                defaultValue={from}
                allowClear={false}
                onChange={this.onFromDateChange}
                className="no-border"
                style={{ marginTop: 5 }}
              />
            </div>
            <div style={{ width: 55 }} />
            <div style={{ width: 114 }}>
              <div className="dp-label">To: </div>
              <DatePicker
                placeholder={'MM/DD/YYYY'}
                format={'MM/DD/YYYY'}
                suffixIcon={calendarIcon}
                defaultValue={to}
                allowClear={false}
                onChange={this.onToDateChange}
                className="no-border"
                style={{ marginTop: 5 }}
              />
            </div>
          </Flex>
          <div style={{ marginTop: 40, marginLeft: -12 }}>
            <HighchartsReact
              highcharts={Highcharts}
              options={{
                chart: {
                  zoomType: 'x',
                  width: 1214,
                  height: '200',
                },
                title: {
                  text: '',
                },
                credits: {
                  enabled: false,
                },
                legend: {
                  enabled: false,
                },
                xAxis: {
                  type: 'datetime',
                  labels: {
                    style: {
                      color: '#555F61',
                      fontSize: '10px',
                    },
                    formatter: (v) => {
                      return moment(v.value).format('M/D')
                    },
                  },
                },
                yAxis: {
                  title: {
                    enabled: false,
                  },
                  labels: {
                    style: {
                      color: '#555F61',
                      fontSize: '10px',
                    },
                    formatter: (v) => {
                      return v.value >= 0 ? `$${v.value}` : ''
                    },
                  },
                },
                plotOptions: {
                  area: {
                    fillColor: {
                      linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1,
                      },
                      stops: [
                        [0, Highcharts.color(mainColor).setOpacity(0.5).get('rgba')],
                        [1, Highcharts.color(mainColor).setOpacity(0).get('rgba')],
                      ],
                    },
                    marker: {
                      enabled: true,
                      symbol: 'circle',
                      radius: 4,
                      fillColor: mainColor,
                      lineColor: 'white',
                      lineWidth: 2,
                    },
                    lineWidth: 2,
                    lineColor: mainColor,
                    states: {
                      hover: {
                        lineWidth: 1,
                      },
                    },
                    threshold: null,
                  },
                },
                series: [
                  {
                    type: 'area',
                    data: prices,
                  },
                ],
              }}
            />
          </div>
        </div>
      </PriceModalWrapper>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(PriceModal))
