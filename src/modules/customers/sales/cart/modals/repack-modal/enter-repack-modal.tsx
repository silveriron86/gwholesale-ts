import * as React from 'react'
import { connect } from "redux-epics-decorator";
import { DatePicker, Select, notification } from 'antd'
import moment from 'moment'
import { OrdersModule } from "~/modules/orders";
import { GlobalState } from "~/store/reducer";
import { Flex, ThemeButton, ThemeModal, ThemeIconButton, ThemeInput, ThemeSelect, ThemeIcon, ThemeSpin } from '~/modules/customers/customers.style'
import { RepackModalWrapper } from './repack.style'
import RepackInputTable from "./input-table"
import RepackOutputTable from "./output-table"
import RepackYieldTable from "./yield-table"
import { getAvailableToSell } from '~/common/utils';
type EnterRepackModalProps = {
  sellerSetting: any
  selectedItem: any
  visible: boolean
  repacking: boolean
  repack: any
  onClose: Function
  getSimplifyItems: Function
  saveRepack: Function
  getOrderItemsById: Function
  repackSalesOrder: Function
  currentOrder: any
}

type EnterRepackModalStates = {
  input: any[],
  output: any[],
  yieldLossPaidType: number,
  repackDate: any,
  repackReferenceNo: string
}

class EnterRepackModal extends React.PureComponent<EnterRepackModalProps, EnterRepackModalStates> {
  constructor(props: EnterRepackModalProps) {
    super(props)
    this.state = {
      input: [],
      output: [],
      yieldLossPaidType: 1,
      repackDate: moment(),
      repackReferenceNo: ""
    }
  }
  componentDidMount() {
    this.fillData(this.props)
  }

  componentWillReceiveProps(nextProps: EnterRepackModalProps) {
    if (nextProps.selectedItem && JSON.stringify(this.props.selectedItem) !== JSON.stringify(nextProps.selectedItem)) {
      this.fillData(nextProps)
    }

    if (this.props.repacking !== nextProps.repacking && nextProps.repacking === false) {
      if (nextProps.repack && nextProps.repack.data === false) {
        notification.error({
          message: 'ERROR',
          description: nextProps.repack.message,
          onClose: () => { },
        })
      } else {
        this.props.onClose(true);
        this.props.getOrderItemsById(nextProps.currentOrder.wholesaleOrderId);
      }
    }
  }

  fillData = (props: EnterRepackModalProps) => {
    const { selectedItem } = props
    const { overrideUOM, inventoryUOM } = selectedItem
    this.setState({
      input: {
        ...selectedItem,
        uom: overrideUOM ? overrideUOM : inventoryUOM,
        quantity: +selectedItem.quantity,
        netWeight: 0,
        repackType: 0,
      },
      output: [],
      yieldLossPaidType: 1,
      repackDate: moment(),
      repackReferenceNo: ""
    })
  }

  onUpdateInput = (input: any) => {
    this.setState({ input })
  }

  onUpdateOutput = (output: any[]) => {
    this.setState({ output })
  }

  onchange = (value: any, type: string) => {
    let state = {...this.state}
    state[type] = value
    this.setState(state)
  }

  onSave = () => {
    const { input, output, repackDate, repackReferenceNo } = this.state
    
    const data = {
      repackReferenceNo,
      repackDate: moment(repackDate).format('MM/DD/YYYY'),
      inputs: [{
        quantity: parseFloat(output[0].quantity),
        lotId: output[0].lot,
        itemId: output[0].itemId
      }],
      outputs: [{
        quantity: parseFloat(input.quantity),
        itemId: input.itemId
      }]
    }
    this.props.repackSalesOrder(data)
    
  }

  render() {
    const { visible, onClose, sellerSetting, selectedItem, repacking } = this.props
    const { input, output, yieldLossPaidType, repackDate, repackReferenceNo } = this.state
    const logisticUnit = sellerSetting ? sellerSetting.company.logisticUnit : "IMPERIAL"
    const unit = logisticUnit === "IMPERIAL" ? 'lb' : 'kg'

    const availableToSell = selectedItem ? getAvailableToSell(selectedItem) : 0
    const disabled = availableToSell < parseFloat(input.quantity)

    return (
      <ThemeModal
        title={`Enter repack`}
        visible={visible}
        onCancel={onClose}
        okText={'Save'}
        className='dark'
        footer={[
          <Flex>
            <ThemeButton className={`dark ${repacking || disabled && output.length === 0 ? 'disabled' : ''}`} type="primary" onClick={this.onSave} disabled={repacking}>
              Enter repack
            </ThemeButton>
            <ThemeIconButton className="no-border dark" onClick={onClose} style={{marginLeft: 24}}>Cancel</ThemeIconButton>
          </Flex>,
        ]}
        width={800}
      >
        <RepackModalWrapper>
          <ThemeSpin spinning={repacking}>
            <Flex>
              <div>
                <label>Repack reference no.</label>
                <ThemeInput style={{ width: 260 }} value={repackReferenceNo} onChange={(e: any) => this.onchange(e.target.value, 'repackReferenceNo')} />
              </div>
              <div style={{marginLeft: 20}}>
                <label>Repack date</label>
                <DatePicker
                  value={repackDate}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={<ThemeIcon type="calendar" />}
                  style={{ width: 160, display: 'block' }}
                  allowClear={false}
                  onChange={(v: any) => this.onchange(v, 'repackDate')}
                />
              </div>
            </Flex>
            <RepackOutputTable {...this.props} dataSource={output} logisticUnit={unit} onUpdate={this.onUpdateOutput} />
            <RepackInputTable dataSource={[input]} logisticUnit={unit} onUpdate={this.onUpdateInput} />
          
          </ThemeSpin>
        </RepackModalWrapper>
      </ThemeModal>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export default connect(OrdersModule)(mapStateToProps)(EnterRepackModal)
