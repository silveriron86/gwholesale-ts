import * as React from "react";
import { Table } from 'antd';
import {Flex, ThemeInput} from "~/modules/customers/customers.style";
import { cloneDeep } from "lodash";
import { getAvailableToSell } from "~/common/utils";

interface RepackInputTableprops {
  dataSource: any
  logisticUnit: string
  onUpdate: Function
}

export default class RepackInputTable extends React.PureComponent<RepackInputTableprops> {
  onUpdate = (e: any, type: string) => {
    const { value } = e.target
    let dataSource = cloneDeep(this.props.dataSource[0])
    dataSource[type] = value
    this.props.onUpdate(dataSource)
  }

  calcQuantity = (e: any) => {
    const { value } = e.target
    let dataSource = cloneDeep(this.props.dataSource[0])
    dataSource['quantity'] = value

    const { grossWeight } = this.props.dataSource[0]

    if (grossWeight) {
      const quantity = parseFloat(value)
      if (!isNaN(quantity) && quantity !== null) {
        dataSource['netWeight'] = quantity * grossWeight;
      }
    }

    this.props.onUpdate(dataSource)
  }

  formatValues = (type: string) => {
    let dataSource = cloneDeep(this.props.dataSource[0])
    if (dataSource[type] !== '') {
      dataSource[type] = parseFloat(dataSource[type])
      this.props.onUpdate(dataSource)
    }
  }

  render() {
    const { dataSource, logisticUnit } = this.props
    const columns = [
      {
        title: 'Product(s)',
        dataIndex: 'variety',
        render: (variety: string, record: any) => {
          if (variety) {
            return variety
          }
        }
      },
      {
        title: 'Output qty',
        dataIndex: 'quantity',
        width: 166,
        render: (quantity: number, record: any) => {
          const availableToSell = getAvailableToSell(record)
          const warning = availableToSell < quantity
          const textInput = <ThemeInput className={warning ? "warning" : ""} style={{ width: 60, marginRight: 5}} value={quantity} onChange={(e: any) => this.onUpdate(e, 'quantity')} />
          return (
            <Flex className="v-center">
              {textInput}
              {record.uom}
            </Flex>
          )
        }
      },
      {
        title: ' ',
        width: 305,
      },
    ]
    return (
      <>
        <div className="table-header">{'Output'}</div>
        <Table dataSource={dataSource} columns={columns} pagination={false}/>
      </>
    )
  }
}
