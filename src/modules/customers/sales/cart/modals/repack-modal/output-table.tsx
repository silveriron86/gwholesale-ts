import * as React from "react";
import {Select, Table} from 'antd';
import { AddAutocomplete } from "~/modules/customers/nav-sales/order-detail/tabs/add-autocomplete";
import {ThemeInput, Flex, ThemeSelect} from "~/modules/customers/customers.style";
import {cloneDeep} from "lodash";
import {Icon as IconSVG} from "~/components";
import UomSelect from "~/modules/customers/nav-sales/order-detail/components/uom-select";
import { formatDate, mathRoundFun } from "~/common/utils";


interface RepackOutputTableprops {
  logisticUnit: string
  dataSource: any[]
  onUpdate: Function
  getItemLotLoading: boolean
  getItemLot: Function
  itemLots: any[]
  selectedItem: any
}

export default class RepackOutputTable extends React.PureComponent<RepackOutputTableprops> {
  componentWillReceiveProps(nextProps: RepackOutputTableprops) {
    if (this.props.getItemLotLoading !== nextProps.getItemLotLoading && nextProps.getItemLotLoading === false) {
      let defaultLot = null;
      if (nextProps.itemLots.length > 0) {
        const available = nextProps.itemLots.find((v: any) => {
          const disabled = v.lotAvailableQty > 0 && v.lotAvailableQty < nextProps.dataSource[0].quantity
          return !disabled;
        })

        if (available) {
          defaultLot = available.lotId
        }
      }
      this.onUpdate(defaultLot, 'lot', 0)
    }
  }

  onAddItem = (record: any) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource.push({
      ...record,
      netWeight: 0,
      uom: record.overrideUOM ? record.overrideUOM : record.inventoryUOM,
      repackType: 1,
    })
    this.props.getItemLot(record.itemId);
    this.props.onUpdate(dataSource)
  }

  onUpdate = (value: any, type: string, index: number) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource[index][type] = value
    this.props.onUpdate(dataSource)
  }

  calcQuantity = (value: any, index: number) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource[index]['quantity'] = value

    const { defaultGrossWeight } = this.props.dataSource[index]
    if (defaultGrossWeight) {
      const quantity = parseFloat(value)
      if (!isNaN(quantity) && quantity !== null) {
        dataSource[index]['netWeight'] = quantity * defaultGrossWeight;
      }
    }

    this.props.onUpdate(dataSource)
  }

  deleteRow = (index: number) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource.splice(index, 1)
    this.props.onUpdate(dataSource)
  }

  onChangeUom = (orderItemId: string, value: any, index: number) => {
    this.onUpdate(value, 'uom', index)
  }

  render() {
    const { dataSource, selectedItem } = this.props
    const columns = [
      {
        title: 'Product',
        dataIndex: 'variety',
        render: (variety: string, record: any) => {
          if (variety) {
            return variety
          }
          return (
            <AddAutocomplete {...this.props} onAddToRepack={this.onAddItem} />
          )
        }
      },
      {
        title: 'Input qty',
        dataIndex: 'quantity',
        width: 166,
        render: (quantity: number, record: any, index: number) => {
          if (!record.variety) {
            return ''
          }
          const { overrideUOM, inventoryUOM } = selectedItem
          return (
            <Flex className="v-center">
              <ThemeInput style={{ width: 60, marginRight: 5}} value={quantity} onChange={(e: any) => this.onUpdate(e.target.value, 'quantity', index)} />
              {overrideUOM ? overrideUOM : inventoryUOM}
            </Flex>
          )
        }
      },
      {
        title: 'Lot',
        dataIndex: 'lot',
        width: 290,
        render: (lot: number, record: any, index: number) => {
          if (!record.variety) {
            return ''
          }
          return (
            <Flex className="v-center">
              <ThemeSelect
                loading={this.props.getItemLotLoading}
                value={lot}
                style={{ width: 260, marginRight: 15 }}
                onChange={(lotId: string) => this.onUpdate(lotId, 'lot', index)}
              >
                {this.props.itemLots.map((v) => {
                  const disabled = v.lotAvailableQty > 0 && v.lotAvailableQty < record.quantity
                  return (
                    <Select.Option disabled={disabled} value={v.lotId}>{v.lotId} ({mathRoundFun(v.lotAvailableQty, 2)} {v.UOM}; {v.receivedQty > 0 ? 'received' : formatDate(v.deliveryDate)})</Select.Option>
                  )
                })}
                <Select.Option value=''>No lot selected</Select.Option>
              </ThemeSelect>
              <div className="trash" onClick={() => this.deleteRow(index)}>
                <IconSVG
                  type="trash"
                  viewBox="0 0 24 24"
                  width="16"
                  height="16"
                  style={{ margin: '0 4px 2px -2px' }}
                />
              </div>
            </Flex>
          )
        }
      }
    ]

    const data = dataSource.length > 0 ? dataSource : [
      ...dataSource,
      {
        variety: '',
      }
    ]

    return (
      <>
        <div className="table-header">{'Input'}</div>
        <Table dataSource={data} columns={columns} pagination={false}/>
      </>
    )
  }
}
