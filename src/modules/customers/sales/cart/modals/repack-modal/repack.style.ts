// eslint
import styled from "@emotion/styled";

export const RepackModalWrapper = styled('div')((props: any) => ({
    margin: '-15px 0px',
    padding: 5,
    label: {
      marginBottom: 3,
      fontWeight: 'normal !important' as 'normal',
      color: 'rgba(0, 0, 0, 0.65) !important'
    },
    '.table-header': {
      fontSize: 15,
      fontWeight: 500,
      marginTop: 20,
      marginBottom: 5,
      color: '#3c4244',
    },
    '.ant-table-wrapper': {
      '.ant-table-content': {
        '.ant-table-body': {
          'table': {
            borderTop: '3px solid #ddd',
            borderRadius: 0
          },
          '.ant-table-thead': {
            '& > tr > th': {
              padding: '10px 16px'
            },
            '.ant-table-column-title': {
              fontSize: 14,
              color: '#515a5c !important',
            }
          },
          '.ant-table-tbody > tr > td': {
            backgroundColor: 'white !important',
            padding: '0px 16px',
            height: 44,
            '.trash': {
              position: 'absolute',
              right: 5,
              cursor: 'pointer',
              svg: {
                path: {
                  fill: props.theme.theme
                }
              }
            },
            '.percent': {
              fontSize: 12,
              color: '#888',
              height: 18,
            }
          }
        }
      }
    },
    '.total-table': {
      marginTop: 20,
      '.ant-table-wrapper': {
        '.ant-table-content': {
          '.ant-table-body': {
            '.ant-table-tbody > tr > td': {
              height: 60
            }
          }
        }
      }
    },
    '&.in-tab': {
      '.table-header': {
        position: 'relative',
        marginTop: 30,
        fontWeight: 'bold',
        color: '#666',
      },
      '.sub-header': {
        margin: '8px 0 18px',
        fontSize: 13,
        color: '#333',
        lineHeight: '23px'
      },
      '.repack-delete': {
        marginTop: 26,
        cursor: 'pointer',
        marginLeft: 20,
        'path': {
          fill: props.theme.theme
        }
      },
      '.repack-download': {
        position: 'absolute',
        right: 0
      }
    }
  }))
  