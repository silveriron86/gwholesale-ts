import styled from '@emotion/styled'
import { Layout, Row } from 'antd'
import { badgeColor, medLightGrey } from '~/common'

export const PrintPickSheetWrapper = styled('div')({
  padding: '0 30px',
  backgroundColor: 'transparent',
  '.f10': {
    fontSize: '10px !important'
  },
  '.f12': {
    fontSize: '12px !important'
  },
  '.f14': {
    fontSize: '14px !important'
  },
  '.f16': {
    fontSize: '16px !important'
  },
  '.f18': {
    fontSize: '18px !important'
  },
  '.freight-carrier-section': {
    marginTop: 8,
    'div': {
      color: 'black',
      fontWeight: 500
    },
    '.label-row':{
      lineHeight: 2,
      // paddingBottom: '1em'
    }
  },
  '.padding-left-20':{
    paddingLeft: 20
  }
})

export const HeaderTextRightBox = styled('div')({
  textAlign: 'right',
  fontWeight: 'bold',
  '&.normal': {
    fontWeight: 'normal'
  },
  '&.black': {
    color: 'black'
  }
})

export const HeaderTextCenterBox = styled('div')({
  textAlign: 'center',
})

export const HeaderTextLeftBox = styled('div')({
  textAlign: 'left',
  fontWeight: 'bold',
  '&.normal': {
    fontWeight: 'normal'
  },
  '&.black': {
    color: 'black'
  },
  '&.picksheet': {
    fontWeight: 500,
    fontSize: 12,
    '&.f16': {
      fontSize: 16
    }
  }
})

export const HeaderBox = styled('div')({
  '.ant-col': {
    fontWeight: 'normal',
    color: 'black'
  }
})

export const WeightSpan = styled('div')({
  minWidth: 50,
  padding: '2px 4px 2px 0',
  fontSize: '13px !important'
})

export const WeightWrapper = styled('div')({
  // textAlign: 'center',
  display: 'flex',
  // paddingLeft: 50
})

export const ModalDetailText = styled('p')({
  marginTop: 3,
  padding: '0px 0px 20px',
  fontSize: 12,
  fontWeight: 'normal',
  letterSpacing: 'normal',
  color: 'black',
  'b': {
    fontSize: 14
  }
})

export const UnderLineWrapper = styled('div')({
  marginLeft: 40,
  marginTop: 30,
  marginBottom: 40,
  display: 'flex',
  justifyContent: 'space-between',
  width: '60%',
})

export const Underline = styled('div')({
  flex: 1,
  marginLeft: 2,
  // minWidth: '65%',
  borderBottom: '1px solid black',
})

export const DeliverySction = styled('div')({
  border: '1px solid black',
  width: '100%',
  padding: 5,
  marginTop: 10,
  color: 'black',
  '& span': {
    fontSize: 10,
    fontWeight: 100,
    letterSpacing: 0,
  },
  '& h4': {
    fontWeight: 'bold',
  },
})

export const PrintPickSheetTable = styled('div')({
  margin: '20px 0',
  '&.pick-sheet': {
    margin: '10px 0 0',
    '.ant-table-tbody': {
      fontWeight: 500
    },
    'th, td': {
      padding: '5px 8px !important',
    },
    'th div, th span': {
      fontSize: '14px !important',
    },
    'td, td div, td span': {
      fontSize: '16px !important',
    }
  },
  '& .ant-table-thead': {
    'th.name-order': {
      paddingTop: '0 !important',
      paddingBottom: '0 !important'
    },
    'th:not(.name-order)': {
      paddingTop: '8px !important',
      paddingBottom: '8px !important'
    }
    'th.ordered-quantity': {
      paddingLeft: 0,
      textAlign: 'left !important'
    }
  },
  '& .ant-table-tbody': {
    fontSize: '12px !important',
    // fontWeight:100,
    'td': {
      // padding: '0',
      paddingTop: 5,
      paddingBottom: 5,
      verticalAlign: 'middle!important;',
      borderBottom: 'none !important',
      '&.ordered-quantity': {
        paddingLeft: 0
      }
    },
    '.ant-table-expanded-row': {
      td: {
        padding: '0 !important',
        border: 0,
      },
    },
    '& .ant-table-tbody td svg': {
      // marginTop: 1
    },
    '.ant-table-thead > tr > th, .ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr > td': {
      backgroundColor: 'white',
    },
    'th': {
      '&.th-left': {
        paddingLeft: '16px !important'
      }
    },
  },
  '& .alternate-row': {
    'thead th': {
      backgroundColor: 'rgba(196,196,196,0.2) !important',
      borderBottom: '1px solid #EDF1EE !important',
      '&.name-order': {
        background: 'white !important',
        textAlign: 'left !important',
        paddingLeft: 0,
        paddingTop: 0
      }
    },
    'tbody tr': {
      'td': {
        borderBottom: '1px solid #EDF1EE !important',
      },
      '&.alternative': {
        td: {
          backgroundColor: 'rgba(196, 196, 196, 0.2) !important',
        }
      }
    }
  },
  '.isHealthBol' {
    display: 'none'
  }
})

export const DeliveryListTable = styled('div')({
  fontWeight: 500,
  '& .ant-table thead th.ant-table-expand-icon-th': {
    display: 'none',
  },
  '& .ant-table tbody td.ant-table-row-expand-icon-cell': {
    display: 'none',
  },
  '& .ant-table-row': {
    fontSize: '13px',
    'td': {
      padding: '2px 2px !important',
    }
  },
  '& .ant-table-align-center': {
    fontSize: '14px',
  },
  'th': {
    paddingLeft: 0,
    paddingRight: 0,
    'span': {
      fontWeight: '500 !important'
    },
    '&.th-left': {
      paddingLeft: '16px !important'
    }
  },
  'th, td': {
    background: 'white !important',
    borderLeft: '0 !important',
    borderRight: '0 !important',
    '&.p4': {
      paddingLeft: '4px !important'
      paddingRight: '4px !important'
    }
  },
  // '& .alternate-row': {
  //   'thead th': {
  //     backgroundColor: 'rgba(196,196,196,0.2) !important',
  //     borderBottom: '1px solid #EDF1EE !important',
  //     '&.name-order': {
  //       background: 'white !important',
  //       textAlign: 'left !important',
  //       paddingLeft: 0,
  //       paddingTop: 0
  //     }
  //   },
  //   'tbody tr': {
  //     'td': {
  //       borderBottom: '1px solid #EDF1EE !important',
  //     },
  //     '&.alternative': {
  //       td: {
  //         backgroundColor: 'rgba(196, 196, 196, 0.2) !important',
  //       }
  //     }
  //   }
  // }
})

export const DeliveryMemoListTable = styled('div')({
  fontWeight: 500,
  '& .ant-table thead th.ant-table-expand-icon-th': {
    display: 'none',
  },
  '& .ant-table tbody td.ant-table-row-expand-icon-cell': {
    display: 'none',
  },
  '& .ant-table-row': {
    fontSize: '14px',
  },
  '& .ant-table-align-center': {
    fontSize: '14px',
  },
  'th': {
    paddingLeft: 0,
    paddingRight: 0,
    'span': {
      fontWeight: '500 !important'
    },
    '&.th-left': {
      paddingLeft: '16px !important'
    }
  },
  'th, td': {
    background: 'white !important',
    borderLeft: '0 !important',
    borderRight: '0 !important',
    // height:"100px !important"
    '&.p4': {
      paddingLeft: '4px !important'
      paddingRight: '4px !important'
    }
  },
  '& .alternate-row': {
    'tbody tr': {
      'td': {
        padding: '2px 2px',
        borderBottom: '1px solid #EDF1EE !important',
      },
      '&.alternative': {
        td: {
          // height: '100px !important',
          paddingBottom:"20px !important",
        }
      }
    }
  }
})

export const EllipseText = styled('div')({
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
})

export const FlexWrap = styled('div')({
  display: 'flex',
  flexWrap: 'wrap',
  paddingTop: 5,
  paddingBottom: 5,
  maxWidth: 900,
  '.ant-input': {
    margin: '0 10px',
    background: 'transparent',
    border: 0,
    outline: 'none',
    borderBottom: '1px solid #cdcdcd',
    borderRadius: 0,
    width: 80,
  },
})

export const Inputs = styled('div')({
  width: '100%',
  height: '100%',
  padding: 0,
  borderBottom: '1px solid #EDF1EE',
  '.ant-input': {
    marginLeft: '0 !important',
    '&:nth-child(10n+0)': {
      marginRight: '0 !important'
    }
  },

})

export const LightColoredRow = styled(Row)((props: any) => ({
  background: props.theme.lighter,
  color: props.theme.primary
}))

export const pl24: React.CSSProperties = {
  paddingLeft: 24
}

export const DisplayLabel = styled('p')({
  fontSize: "14px",
  fontFamily: "Inter",
  color: "black",
  fontWeight: "normal",
  lineHeight: "19.6px",
  textAlign: "right",
  marginBottom: "0px",
  width:"200px"
})

export const InfoLabel = styled('p')({
  fontSize: "13px",
  fontFamily: "Inter",
  minWidth: '130px',
  marginBottom: '6px',
})

export const InfoValue = styled('p')({
  fontSize: "13px",
  fontFamily: "Inter",
  marginBottom: '6px',
})

export const DisplayLeftLabel = styled('p')({
  fontSize: "14px",
  fontFamily: "Inter",
  color: "black",
  fontWeight: "bold",
  lineHeight: "19.6px",
  textAlign: "left",
  marginBottom: "0px"
})

export const DisplayValue = styled('p')({
  fontSize: "14px",
  fontFamily: "Inter",
  color: "black",
  fontWeight: "normal",
  lineHeight: "19.6px",
  textAlign: "left",
  marginBottom: "0px",
  minHeight: "19px",
  width:"200px"
})

//SELECT FRESH PRINT BEGIN
export const SelectFreshPrintableBodyWrapper = styled.div`
  @media print {
     transform: rotate(270deg);
  }
;`

export const SelectFreshInvoiceBody = styled('div')({
    width: '414px',//418
    height: '650px',
    position: 'relative',
    fontFamily: 'arial, helvetica, sans-serif',
    pageBreakAfter: 'always',
    pageBreakInside: 'avoid',
    display: 'block',
    //fontStretch: 'condensed',
    backgroundImage: "url(" + "./src/modules/customers/8.5_X_5.5_Forms_4_Parts_No_CarbonV2.jpg" + ")",
    backgroundRepeat: 'no-repeat',
    backgroundSize: '430px 698px', //450 740
    transform: `scaleX(123%) scaleY(123%)`,//127 127
    marginTop: '260px',
    marginLeft: '228px',
    backgroundPositionY: '-44px', //-34
    backgroundPositionX: '4px', //-0
  },
)
export const SelectFreshInvoiceBillTo = styled('div')({
		width: '188px',
		height: '50px',
		position: 'absolute',
		top: '131px', //126
		left: '28px', //33
		fontSize: '8px'
	},
)
export const SelectFreshInvoiceShipTo = styled('div')({
		width: '188px',
		height: '50px',
		position: 'absolute',
		top: '131px',//126
		left: '226px',//231
		fontSize: '8px'
  },
)
export const SelectFreshInvoiceNumber = styled('div')({
    width: '50px',
    height: '17px',
    position: 'absolute',
    top: '205px',//201
    left: '28px',//33
    fontSize: '8px',
    lineHeight: '10px',
  },
)
export const SelectFreshInvoiceDate = styled('div')({
    	width: '55px',
    	height: '17px',
    	position: 'absolute',
    	top: '205px',//201
    	left: '86px',//91
    	fontSize: '8px',
      lineHeight: '10px',
  },
)
export const SelectFreshInvoiceTerms = styled('div')({
    width: '78px',
    height: '17px',
    position: 'absolute',
    top: '205px',//201
    left: '152px',//157
    fontSize: '8px',
    lineHeight: '10px',
  },
)
export const SelectFreshInvoiceRep = styled('div')({
    width: '50px',
    height: '17px',
    position: 'absolute',
    top: '205px',//201
    left: '234px',//239
    fontSize: '8px',
    lineHeight: '8px',
  },
)
export const SelectFreshInvoiceShipMethod = styled('div')({
    width: '125px',
    height: '17px',
    position: 'absolute',
    top: '205px',//201
    left: '286px',//291
    fontSize: '8px',
    lineHeight: '10px',
  },
)
export const SelectFreshInvoiceLineItems = styled('div')({
    width: '388px',
    height: '322px',
    position: 'absolute',
    top: '243px',//238
    left: '20px',//25
    fontSize: '8px'
  },
)
export const SelectFreshInvoiceItemRow = styled('table')({
		fontSize: '8px'
	},
)

export const SelectFreshInvoiceWrapper = styled('div')({

})

export const SelectFreshGrandTotal = styled('div')({
  width: '60px',
  height: '15px',
  position: 'absolute',
  top: '570px',//564
  left: '338px',//343
  fontSize: '8px',
  lineHeight: '10px',
  textAlign: 'right',
})

export const SelectFreshPageSection = styled('div')({
  width: '60px',
  height: '15px',
  position: 'absolute',
  top: '570px',//564
  left: '28px',//33
  fontSize: '8px',
  lineHeight: '10px',
})


export const SelectFreshWeightSpan = styled('div')({
  fontSize: '8px'
})

//SELECT FRESH PRINT END

//BELEN TRADING PRINT BEGIN
export const BelenTradingInvoiceWrapper = styled('div')({
  padding: '0 30px',
  backgroundColor: 'transparent',
  '.f10': {
    fontSize: '10px !important'
  },
  '.f12': {
    fontSize: '12px !important'
  },
  '.f14': {
    fontSize: '14px !important'
  },
  '.f16': {
    fontSize: '16px !important'
  },
  '.f18': {
    fontSize: '18px !important'
  },
  '.freight-carrier-section': {
    marginTop: 8,
    'div': {
      color: 'black',
      fontWeight: 500
    },
    '.label-row':{
      lineHeight: 2,
      // paddingBottom: '1em'
    }
  },
  '.padding-left-20':{
    paddingLeft: 20
  },
  '.green-box':{
    backgroundColor: '#008013',
    paddingLeft: '5px',
    paddingRight: '5px',
    paddingTop: '8px',
    paddingBottom: '8px'
  },
  '.static-footer': {
    textAlign: "center",
    width: "100%",
    color: "black",
    fontSize: "16px",
    fontWeight: "normal",
    marginTop: "40px"
  },
  '@media print': {
    '.static-footer': {
      marginTop: "0px",
      position: "fixed",
      bottom: 0,
      width: "90%"
    }
  }
})

export const BelenTradingDisplayLeftLabel = styled('div')({
  fontSize: "16px",
  color: "black",
  fontWeight: "bold",
  lineHeight: "19.6px",
  textAlign: "left",
  marginBottom: "0px"
})

export const BelenTradingDisplayLeftValue = styled('p')({
  fontSize: "14px",/*
  fontFamily: "Inter",*/
  color: "black",
  fontWeight: "normal",
  lineHeight: "19.6px",
  textAlign: "left",
  marginBottom: "0px",
  minHeight: "19px",
  whiteSpace: "pre-line"
})

export const BelenTradingDisplayRightValue = styled('p')({
  fontSize: "14px",/*
  fontFamily: "Inter",*/
  color: "black",
  fontWeight: "normal",
  lineHeight: "19.6px",
  textAlign: "right",
  marginBottom: "0px",
  minHeight: "19px"/*,
  whiteSpace: "pre-line"*/
})

export const BelenTradingDisplayGreenBoxLabel = styled('p')({
  fontSize: "16px",
  color: "white",
  fontWeight: "bold",
  lineHeight: "19.6px",
  textAlign: "left",
  marginBottom: "0px",
  minHeight: "19px",/*
  whiteSpace: "pre-line",*/
  backgroundColor: "#008013"
})

export const BelenTradingDisplayGreenBoxValue = styled('p')({
  fontSize: "14px",
  color: "white",
  fontWeight: "bold",
  lineHeight: "19.6px",
  textAlign: "left",
  marginBottom: "0px",
  minHeight: "19px",/*
  whiteSpace: "pre-line",*/
  backgroundColor: "#008013",
  paddingLeft: "5px"
})

export const BelenTradingPrintPickSheetTable = styled('table')({
  width: "100%",
  marginTop: "30px",
  'th': {
    backgroundColor: "#008013",
    color: "white",
    fontWeight: "bold",
    fontSize: "14px",
    paddingTop: "8px",
    paddingRight: "10px",
    paddingBottom: "8px",
    paddingLeft: "20px"
  },
  'td': {
    color: "black",
    fontWeight: "normal",
    fontSize: "14px",
    paddingTop: "8px",
    paddingRight: "10px",
    paddingBottom: "8px",
    paddingLeft: "20px",
  },
  '.right-align': {
    textAlign: "right"
  },
  '.pr-20': {
    paddingRight: "20px"
  },
  'tbody tr':{
    borderTop: "1px solid #8CADAE"
  }
})

// END BELEN TRADING PRINT END
