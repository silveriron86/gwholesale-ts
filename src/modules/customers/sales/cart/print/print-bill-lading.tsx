import React, { FC } from 'react'
import { OrderDetail, OrderItem } from '~/schema'
import { Table, Row, Col } from 'antd'
import _ from 'lodash'
import Barcode from 'react-barcode'
import {
  HeaderTextRightBox,
  HeaderTextLeftBox,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
  ModalDetailText,
  UnderLineWrapper,
  Underline,
  DeliverySction,
  DeliveryListTable,
  WeightSpan,
  WeightWrapper,
  LightColoredRow,
  pl24,
  DisplayLabel,
  DisplayValue,
  DisplayLeftLabel,
  InfoLabel,
  InfoValue,
} from '~/modules/customers/sales/cart/print/_styles'
import Moment from 'react-moment'
import {
  formatNumber,
  formatAddress,
  basePriceToRatioPrice,
  mathRoundFun,
  getOrderPrefix,
  inventoryQtyToRatioQty,
  ratioQtyToInventoryQty,
  numberMultipy,
  judgeConstantRatio,
  formatItemDescription,
  isCustomOrderNumberEnabled,
  checkError,
  formatPrintOriginAddress
} from '~/common/utils'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import { Icon } from '~/components'
import { timingFunctions } from 'polished'
import { timeout } from 'rxjs/operators'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import _, { cloneDeep } from 'lodash'
import moment from 'moment'
import { PrintSetting } from '~/modules/setting/components/print-setting'
import { connect } from 'react-redux'
import { url2base64 } from './sendEmail'
import "../print/printBOL.css";
import { CACHED_COMPANY } from '~/common'
import { OrderService } from '~/modules/orders/order.service'

interface PrintBillOfLadingProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  logo: string
  company: any
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  title?: string
  multiple?: boolean
  printSetting: any
  type: string
  changePrintLogoStatus: Function
  fulfillmentOptionType?: number
  sellerSetting?: any
  paymentTerms: any[]
  selectedType?: number
  currentPrintContainerId: string
  orderId: string
  isHealthBol: boolean
}

export class PrintBillOfLading extends React.PureComponent<PrintBillOfLadingProps> {
  state = {
    totalQuantity: 0,
    totalAmount: 0,
    totalWeight: 0,
    visibleAlert: false,
    selectedRowKeys: [],
    orderItems: this.props.orderItems,
    pagePrintSetting: null,
    logo: '',
    containerData: {}
  }

  componentDidMount(): void {
    const _this = this

    this.getAmount(this.props.orderItems)
    if (this.props.printSetting) {
      this.setPrintSetting(this.props.printSetting, this.props.type)
    }
    if (this.props.logo) {
      url2base64(this.props.logo.replace('www.', '')).then(logo => {
        this.setState({ logo })
      })
    }

    if (this.props.currentPrintContainerId && this.props.orderId) {
      if (this.props.currentOrder.fulfillmentType === 5) {
        OrderService.instance.getSingleContainerPrint(this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: {
                ...resp.body.data,
                tempRecorderList: JSON.parse(resp.body.data.tempRecorderList)
              }
            })
          },
          error(err) { checkError(err) },
        })
      } else {
        OrderService.instance.getPrintContainer(this.props.currentPrintContainerId, this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: resp.data
            })
          },
          error(err) { checkError(err) },
        })
      }
    }
    if (this.props.currentPrintContainerId && !this.props.orderId) {
      OrderService.instance.getPrintContainer(this.props.currentPrintContainerId).subscribe({
        next(resp) {
          _this.setState({
            containerData: resp.data
          })
        },
        error(err) { checkError(err) },
      })
    }
  }

  componentWillReceiveProps(nextProps: PrintBillOfLadingProps) {
    if (this.props.orderItems !== nextProps.orderItems) {
      this.getAmount(nextProps.orderItems)
      this.setState({ orderItems: nextProps.orderItems })
    }

    if (this.props.printSetting != nextProps.printSetting && nextProps.printSetting != null) {
      this.setPrintSetting(nextProps.printSetting, nextProps.type)
    }
  }

  setPrintSetting = (jsonSetting: string, modalType: string) => {
    const pageSetting = JSON.parse(jsonSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`${modalType}_`) > -1) {
        const newKey = el.substring(`${modalType}_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    this.setState({ pagePrintSetting: data })
  }

  getAmount = (items: any) => {
    let _totalQuantity = 0
    let _totalAmount = 0
    let _totalWeight = 0

    for (let i = 0; i < items.length; i++) {
      let amount = 0
      let constantRatio = judgeConstantRatio(items[i])
      _totalQuantity += _.toNumber(items[i].picked)

      let pricingUOM = items[i].pricingUOM ? items[i].pricingUOM : items[i].UOM
      let unitUOM = items[i].UOM
      let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(items[i].price), items[i], 12)

      if (items[i].oldCatchWeight) {
        _totalWeight += _.toNumber(items[i].catchWeightQty)
        amount = numberMultipy(_.toNumber(items[i].price), _.toNumber(items[i].catchWeightQty))
      } else {
        if (!constantRatio) {
          _totalWeight += inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i])
          amount = numberMultipy(
            ratioPrice,
            inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i], 12),
          )
        } else {
          const quantity = inventoryQtyToRatioQty(
            pricingUOM,
            ratioQtyToInventoryQty(unitUOM, _.toNumber(items[i].picked), items[i], 12),
            items[i],
            12,
          )
          _totalWeight += quantity
          amount = numberMultipy(ratioPrice, quantity)
        }
      }
      amount = Math.round(amount * 100) / 100
      items[i]['amount'] = '$' + formatNumber(amount, 2)
      _totalAmount += amount
    }

    this.setState({
      totalQuantity: _totalQuantity,
      totalAmount: _totalAmount,
      totalWeight: _totalWeight,
      orderItems: items,
    })
  }

  getExpandableRowKeys = () => {
    const { orderItems } = this.state
    let result: any[] = []
    orderItems.forEach((el) => {
      if (!el.constantRatio) {
        result.push(el.wholesaleOrderItemId)
      }
    })
    return result
  }

  renderCatchWeightValues = (weights: any[]) => {
    // const { catchWeightValues, multiple } = this.props

    // let weights: any[] = []
    // if (multiple && typeof record.catchWeightValues !== 'undefined') {
    //   weights = record.catchWeightValues
    // } else {
    //   weights = catchWeightValues[record.wholesaleOrderItemId] ? catchWeightValues[record.wholesaleOrderItemId] : []
    // }
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (type == 'invoice' && pagePrintSetting && !pagePrintSetting.catchWeightValues) {
    }
    if (weights.length > 0) {
      let result: any[] = []
      let sub: any[] = []
      weights.forEach((el: any, index: number) => {
        if (index > 0 && (index - 10) % 10 == 0) {
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
          sub = []
        }

        const weightEl = <WeightSpan>{el.unitWeight ? el.unitWeight : ''}</WeightSpan>
        if (index == weights.length - 1) {
          sub.push(weightEl)
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
        } else {
          sub.push(weightEl)
        }
      })
      return result
    }

    return []
  }

  checkWeights = (row: any) => {
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (type == 'invoice' && pagePrintSetting && !pagePrintSetting.catchWeightValues) {
      return false
    }
    return typeof row.weights !== 'undefined' && row.weights.length > 0
  }

  getDisplayingColumns = (columns: any[]) => {
    const { pagePrintSetting } = this.state
    let newColumns: any[] = []
    if (!pagePrintSetting) return columns

    columns.forEach((el) => {
      const key = el.key
      if (pagePrintSetting[key] !== false) {
        newColumns.push(el)
      }
    })

    return [
      {
        title: this.getNameAndOrderNo(),
        className: 'name-order',
        children: newColumns,
      },
    ]
    // return newColumns
  }

  getNameAndOrderNo = () => {
    const { currentOrder, sellerSetting } = this.props
    const { pagePrintSetting } = this.state

    if (!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.so || typeof pagePrintSetting.so === 'undefined'))) {
      const clientName =
        currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
          ? currentOrder.wholesaleClient.clientCompany.companyName
          : 'N/A'
          const customerPO = pagePrintSetting?pagePrintSetting['primary_number']?pagePrintSetting['primary_number'] == "customerPO#"?true:false:false:false
      return (
        <HeaderTextRightBox style={{ color: 'black' }}>
          {customerPO?
          <>
            {clientName} | #{currentOrder.reference}
          </>
          :
          <>
          {clientName} | #{getOrderPrefix(sellerSetting, 'sales')}
          {currentOrder.wholesaleOrderId}</>}
        </HeaderTextRightBox>
      )
    }

    return ''
  }

  onImageLoad = () => {
    this.props.changePrintLogoStatus()
  }

  render() {
    const {
      currentOrder,
      title,
      type,
      fulfillmentOptionType,
      sellerSetting,
      catchWeightValues,
      multiple,
      printSetting,
      company,
      paymentTerms
    } = this.props
    const { orderItems, pagePrintSetting } = this.state
    const carrierSetting = pagePrintSetting ? pagePrintSetting.carrier_reference : false;
    const origin = pagePrintSetting ? pagePrintSetting.origin : false;
    const soldto = pagePrintSetting ? pagePrintSetting.soldto : false;
    const shipto = pagePrintSetting ? pagePrintSetting.shipto : false;

    let tableClass = ''
    let bill_temperature = false
    let bill_nameAndSignature = false

    if (printSetting) {
      const setting = JSON.parse(printSetting)
      bill_temperature = setting.bill_temperature ?? false
      bill_nameAndSignature = setting.bill_nameAndSignature ?? false
      if (
        (type === 'invoice' && setting.invoice_alternateRow === true) ||
        (type !== 'invoice' && setting.bill_alternateRow === true)
      ) {
        tableClass = 'alternate-row'
      }
    }

    let items = cloneDeep(orderItems)
    orderItems.forEach((record, i) => {
      const index = items.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
      items[index].key = i
      if (
        type == 'bill' ||
        type == 'manifest' ||
        (type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightValues)))
      ) {
        let showCatchWeightModal = false
        let constantRatio = judgeConstantRatio(record)
        if (!constantRatio) {
          showCatchWeightModal = true
        }
        if (showCatchWeightModal) {
          let weights: any[] = []
          if (multiple && typeof record.catchWeightValues !== 'undefined') {
            weights = record.catchWeightValues
          } else {
            weights = catchWeightValues[record.wholesaleOrderItemId]
              ? catchWeightValues[record.wholesaleOrderItemId]
              : []
          }

          if (weights.length > 0) {
            items.splice(index + 1, 0, {
              // ...cloneDeep(record),
              relatedOrderItemId: record.wholesaleOrderItemId,
              weights,
            })
          }
        }
      }
    })

    // const expandedRowKeys = this.getExpandableRowKeys()
    const columns: Array<any> = [
      {
        title: '#',
        dataIndex: 'key',
        key: 'index',
        align: 'center',
        width: 60,
        render: (key: number, row: OrderItem) => {
          return this.checkWeights(row) ? '' : key + 1
        },
      },
      {
        title: 'ORDERED',
        dataIndex: 'quantity',
        key: 'quantity',
        align: 'center',
        className: 'ordered-quantity',
        width: 70,
        render: (value: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }

          return mathRoundFun(value, 2)
        }
      },
      {
        title: 'SHIPPED',
        dataIndex: 'picked',
        key: 'picked',
        align: 'center',
        // width: 80,
        render: (value: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }
          return mathRoundFun(value, 2)
        }
      },
      {
        title: 'SHIPPED UOM',
        dataIndex: 'UOM',
        key: 'UOM',
        align: 'center',
        width: 70,
      },
      {
        title: 'PRODUCT',
        dataIndex: 'variety',
        key: 'variety',
        align: 'left',
        className: 'th-left',
        render: (variety: string, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return {
              children: this.renderCatchWeightValues(row.weights),
              props: {
                colSpan: 8,
              },
            }
          }
          if (row.itemId) {
            return row.editedItemName ? row.editedItemName : formatItemDescription(variety, row.SKU, sellerSetting, row.customerProductCode)
          }
          const desc = row.chargeDesc ? ` (${row.chargeDesc})` : ''
          return `${variety}${desc}`
        },
      },
      {
        title: 'BRAND',
        dataIndex: 'modifiers',
        key: 'modifiers',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'extraOrigin',
        key: 'extraOrigin',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'LOT',
        dataIndex: 'lot',
        key: 'lot',
        align: 'center',
        render: (value: string, row: any) => {
          return (
            <div>
              {_.get(row, 'lotIds', []).map((lotId) => (
                <span key={lotId} style={{ display: 'block' }}>
                  {lotId}
                </span>
              ))}
            </div>
          )
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'BILLABLE QTY',
        dataIndex: 'catchWeightQty',
        key: 'catchWeightQty',
        align: 'center',
        width: 70,
        render: (val: any, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showBillableQty,
            showValue = false
          if (record.picked || record.catchWeightQty) {
            showValue = true
          }

          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM

          let constantRatio = judgeConstantRatio(record)
          if (record.oldCatchWeight) {
            showBillableQty = record.catchWeightQty
          } else {
            if (!constantRatio) {
              showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
            } else {
              showBillableQty = inventoryQtyToRatioQty(
                pricingUOM,
                ratioQtyToInventoryQty(unitUOM, record.picked, record, 12),
                record,
              )
            }
          }

          return showValue ? `${mathRoundFun(showBillableQty, 2)}${pricingUOM != null ? ' ' + pricingUOM : ''}` : ''
        },
      },
      // {
      //   title: 'BILLABLE UOM',
      //   dataIndex: 'UOM',
      //   key: 'UOM',
      //   align: 'center',
      //   render: (price: number, record: any) => {
      //     return record.pricingUOM ? record.pricingUOM : record.baseUOM
      //   },
      // },
      {
        title: 'PRICE',
        dataIndex: 'price',
        align: 'right',
        key: 'price',
        className: 'p4',
        width: 100,
        render: (price: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          var uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          var isPricingUOMEnabled = (!pagePrintSetting || (pagePrintSetting &&
            (pagePrintSetting.pricingUOM || typeof pagePrintSetting.pricingUOM === 'undefined')))
          price = basePriceToRatioPrice(uom, price, record)
          return (
            <div style={{ fontWeight: 500, whiteSpace: 'nowrap' }}>
              ${formatNumber(price, 2)}
              {isPricingUOMEnabled && uom != null ? '/' + uom : ''}
            </div>
          )
        },
      },
      {
        title: 'SUBTOTAL',
        dataIndex: 'quantity',
        key: 'total',
        align: 'right',
        className: 'p4',
        width: 90,
        render: (data: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showValue = false
          let total = 0

          if (record.picked || record.catchWeightQty) {
            showValue = true
          }
          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM
          let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(record.price), record, 2)

          let constantRatio = judgeConstantRatio(record)

          if (!constantRatio) {
            total = numberMultipy(
              ratioPrice,
              inventoryQtyToRatioQty(pricingUOM, _.toNumber(record.catchWeightQty), record, 12),
            )
          } else {
            let quantity = inventoryQtyToRatioQty(
              pricingUOM,
              ratioQtyToInventoryQty(unitUOM, _.toNumber(record.picked), record, 12),
              record,
              12,
            )
            total = numberMultipy(ratioPrice, quantity)
          }
          return (
            <div style={{ fontWeight: 500 }}>{showValue && total ? `$${formatNumber(total, 2)}` : ''}</div>
          )
        },
      },
    ]

    if (type === 'bill') {
      const sizeIndex = columns.findIndex(el => el.dataIndex == 'size')
      if (sizeIndex <= 1) return
      columns.splice(sizeIndex + 1, 0 ...[
        {
          title: 'GROSS WT.',
          dataIndex: 'grossWeight',
          key: 'grossWeight',
          align: 'center',
          render: (value: string, row: any) => {
            if (this.checkWeights(row)) {
              return {
                children: '',
                props: {
                  colSpan: 0,
                },
              }
            }
            return mathRoundFun(Number(value),1) ?? ''
          },
        },
        {
          title: 'GROSS VOL.',
          dataIndex: 'grossVolume',
          key: 'grossVolume',
          align: 'center',
          render: (value: string, row: any) => {
            if (this.checkWeights(row)) {
              return {
                children: '',
                props: {
                  colSpan: 0,
                },
              }
            }
            return mathRoundFun(Number(value),1) ?? ''
          },
        }
      ])
    }

    if (type == 'manifest') {
      let sizeIndex = columns.findIndex(el => el.dataIndex == 'catchWeightQty')
      if (sizeIndex > 1) {
        columns.splice(sizeIndex + 1, 0 ...[
          {
            title: 'Gross Weight',
            dataIndex: 'grossWeight',
            key: 'grossWeight',
            align: 'right',
            render: (value: string, row: any) => {
              if (this.checkWeights(row)) {
                return {
                  children: '',
                  props: {
                    colSpan: 0,
                  },
                }
              }

              const unit = sellerSetting && sellerSetting.company ? sellerSetting.company.logisticUnit === 'IMPERIAL' ? 'LBS' : 'KG' : ''
              return (
                <div style={{ whiteSpace: 'nowrap' }}>
                  {row.defaultGrossWeight === null || row.defaultGrossWeight === '' || typeof row.defaultGrossWeight === 'undefined' ? '' : `${row.picked * row.defaultGrossWeight} ${unit}`}
                </div>
              )
            }
          },
        ])
      }

      sizeIndex = columns.findIndex(el => el.dataIndex == 'size')
      if (sizeIndex > 1) {
        columns.splice(sizeIndex + 1, 0 ...[
          {
            title: 'Gross Volume',
            dataIndex: 'grossVolume',
            key: 'grossVolume',
            align: 'center',
            render: (value: string, row: any) => {
              if (this.checkWeights(row)) {
                return {
                  children: '',
                  props: {
                    colSpan: 0,
                  },
                }
              }
              return value ?? ''
            },
          }
        ])
      }
    }

    let total_gross_weight = 0
    items.forEach(row => {
      if (row.defaultGrossWeight !== null && row.defaultGrossWeight !== '' && typeof row.defaultGrossWeight !== 'undefined') {
        total_gross_weight += row.picked * row.defaultGrossWeight
      }
    });
    const logisticUnit = sellerSetting && sellerSetting.company ? sellerSetting.company.logisticUnit === 'IMPERIAL' ? 'LBS' : 'KG' : ''

    const totals = (
      <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24}>
        <Col span={8}>
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.totalUnits || typeof pagePrintSetting.totalUnits === 'undefined'))) && (
              <Row gutter={24}>
                <Col span={16}>
                  <HeaderTextLeftBox className="normal black">TOTAL UNITS</HeaderTextLeftBox>
                </Col>
                <Col span={8}>
                  <HeaderTextLeftBox className="black">{this.state.totalQuantity}</HeaderTextLeftBox>
                </Col>
              </Row>
            )}
        </Col>
        <Col span={8}></Col>
        <Col span={8}>
          {type === 'manifest' && (!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.grossWeight || typeof pagePrintSetting.grossWeight === 'undefined'))) && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextLeftBox className="normal black">TOTAL GROSS WEIGHT</HeaderTextLeftBox>
              </Col>
              <Col span={8}>
                <HeaderTextLeftBox className="black">{total_gross_weight} {logisticUnit}</HeaderTextLeftBox>
              </Col>
            </Row>
          )}
        </Col>
      </Row>
    )

    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'
    company = company
      ? company
      : currentOrder.wholesaleClient && currentOrder.wholesaleClient.wholesaleCompany
        ? currentOrder.wholesaleClient.wholesaleCompany
        : null

    const soldToCol = (soldto ? (
      <div style={{ width: "250px" }}>
        <h6 style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "bold", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>SOLD TO:</h6>
        <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{clientName}</p>
        <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>
          {currentOrder.billingAddress ? formatAddress(currentOrder.billingAddress.address, true).trim() : 'N/A'}
        </p>
        {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
        <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>
          {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
            ? currentOrder.wholesaleClient.mainBillingAddress.phone
            : currentOrder.wholesaleClient.mobilePhone}
        </p>
      </div>) : <></>
    )

    const shipToCol = (shipto ? (
      <div style={{ width: "250px" }}>
        <h6 style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "bold", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{fulfillmentOptionType == 2 ? 'PICKUP ADDRESS' : 'SHIP TO'}:{' '}:</h6>
        {
          fulfillmentOptionType != 2 &&
          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{clientName}</p>
        }
        <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>
          {fulfillmentOptionType == 2 ? (
            <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
          ) : (
            <>
              {currentOrder.shippingAddress
                ? formatAddress(currentOrder.shippingAddress.address, true).trim()
                : 'N/A'}
            </>
          )}
        </p>
        {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
        <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>
          {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
            ? currentOrder.wholesaleClient.mainShippingAddress.phone
            : currentOrder.wholesaleClient.mobilePhone}
        </p>
      </div>) : <></>
    )

    const logisticCols = (
      <>
        <div style={{ width: "750px", height: "110px", display: "flex", marginTop: "20px" }}>
          {carrierSetting ? (<div style={{ width: "250px", left: "250px" }}>
            <DisplayLeftLabel>CARRIER </DisplayLeftLabel>
            <DisplayValue>{currentOrder.carrier}</DisplayValue>
            <DisplayValue>{currentOrder.carrierReferenceNo}</DisplayValue>
          </div>) : <></>}
          {origin ? (
            <div style={{ width: "250px", left: "500px" }}>
              <DisplayLeftLabel>ORIGIN </DisplayLeftLabel>
              <DisplayValue>{formatPrintOriginAddress(currentOrder.originAddress)[0]}</DisplayValue>
                <DisplayValue>{formatPrintOriginAddress(currentOrder.originAddress)[1]}</DisplayValue>
            </div>
          ) : <></>}
          <div style={{ width: "250px", left: "750px" }}>
            <DisplayLeftLabel>DESTINATION </DisplayLeftLabel>
            <DisplayValue>{clientName}</DisplayValue>
            <DisplayValue>
              {currentOrder.shippingAddress ? formatAddress(currentOrder.shippingAddress.address, true).trim() : 'N/A'}
            </DisplayValue>
            <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>
              {currentOrder.wholesaleClient.mainShippingAddress &&
                currentOrder.wholesaleClient.mainShippingAddress.phone
                ? currentOrder.wholesaleClient.mainShippingAddress.phone
                : currentOrder.wholesaleClient.mobilePhone}
            </p>
          </div>
        </div>
      </>
    )

    let phone = ''
    if (company) {
      phone = (company.mainBillingAddress &&
        company.mainBillingAddress.phone &&
        company.mainBillingAddress.phone !== 'null'
        ? company.mainBillingAddress.phone
        : company.phone
          ? company.phone
          : ''
      ).replace('+1-', '')
    }

    let logo = this.props.logo
    if (logo && logo !== 'default') {
      if (!location.origin.includes('www')) {
        logo = logo.replace('www.', '')
      };
    }

    if (_.isEmpty(pagePrintSetting)) {
      pagePrintSetting = null
    }

    const companyId = localStorage.getItem(CACHED_COMPANY)

    const RenderInfo = () => {
      if (this.props.currentPrintContainerId) {
        const { containerData } = this.state
        const totalGrossVolume = _.get(this.state, 'containerData.wholesaleOrderList', []).map(v => _.reduce(
          _.get(v, 'wholesaleOrderItemList', []),
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
          0,
        ))
        const totalGrossWeight = _.get(this.state, 'containerData.wholesaleOrderList', []).map(v => _.reduce(
          _.get(v, 'wholesaleOrderItemList', []),
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
          0,
        ))
        return (
          <Col span={11} className="f12" /*style={{ position: 'absolute', right: 10, top: 10 }}*/>
            <Flex >
              <div style={{ marginRight: 10 }}>
                <Flex>
                  <InfoLabel>Fulfillment Date: </InfoLabel>
                  <InfoValue>
                    {currentOrder.deliveryDate}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>PCFN: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'pcfn', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Booking: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'bookingNo', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Net/Gross LBs: </InfoLabel>
                  <InfoValue>
                    {formatNumber(_.sum(totalGrossWeight), 1)}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Net/Gross KG: </InfoLabel>
                  <InfoValue>
                    {formatNumber(_.divide(_.sum(totalGrossWeight), 2.2046), 2)}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Tare Weight LBs: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'tareWeight', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Vessel: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'logisticOrderName', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container #: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'containerName', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Seal#: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'sealNo', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>SCAC: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'scac', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Customer PO#: </InfoLabel>
                  <InfoValue>
                    {currentOrder.reference}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Customer: </InfoLabel>
                  <InfoValue>
                    {currentOrder.wholesaleClient.abbreviation}
                  </InfoValue>
                </Flex>
              </div>
              <div>
                <Flex>
                  <InfoLabel>DODAAC: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'dodaac', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container Type: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'containerType', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Temp Recorder #: </InfoLabel>
                  <InfoValue>
                    {containerData?.tempRecorderList ? _.get(containerData, 'tempRecorderList', []).map((v, i) => `${v.no} #${i + 1}`).join(',') : ''}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Temp (Range): </InfoLabel>
                  <InfoValue>
                    {containerData?.tempRecorderList ? _.get(containerData, 'tempRecorderList', []).map(v => `${v.temp}°F`).join(', ') : ''}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Contractual RDD: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'contractualRdd', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container TCN: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'tcn', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Payment Terms: </InfoLabel>
                  <InfoValue>
                    {currentOrder.financialTerms}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>POE: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'poe', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>POD: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'pod', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel># of Containers: </InfoLabel>
                  <InfoValue>

                  </InfoValue>
                </Flex>
                {this.props.orderId && <Flex>
                  <InfoLabel>Due Date: </InfoLabel>
                  <InfoValue>
                    {moment(currentOrder.deliveryDate)
                      .add(
                        _.get(
                          paymentTerms.find((v) => v.name === currentOrder.financialTerms),
                          'dueDays',
                          0,
                        ),
                        'days',
                      ).format('MM/DD/YY')}
                  </InfoValue>
                </Flex>}
              </div>
            </Flex>
          </Col>
        )
      }
      return (
        <div style={{ display: "inline", position: "absolute", right: "0px", top: "0px", width: "350px" }}>
          {(!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.pickupReferenceNo || typeof pagePrintSetting.pickupReferenceNo === 'undefined'))) &&
            currentOrder.pickupReferenceNo && fulfillmentOptionType != 1 && (
              <div style={{ display: "flex" }}>
                <div>
                  <DisplayLabel>Pickup Reference:</DisplayLabel>
                </div>
                <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                  <DisplayValue>
                    {currentOrder.pickupReferenceNo}
                  </DisplayValue>
                </div>
              </div>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting && (pagePrintSetting.fulfillmentLabel || typeof pagePrintSetting.fulfillmentLabel === 'undefined'))) && (
              <div style={{ display: "flex" }}>
                <div>
                  <DisplayLabel>{pagePrintSetting ? pagePrintSetting.fulfillmentLabel : 'Target Fulfillment Date'}:</DisplayLabel>
                </div>
                <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                  <DisplayValue>{currentOrder.deliveryDate}</DisplayValue>
                </div>
              </div>
            )}
          {(currentOrder && currentOrder.warehousePickupTime) ? (
            <div style={{ display: "flex" }}>
              <div>
                <DisplayLabel>Pickup Time:</DisplayLabel>
              </div>
              <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                <DisplayValue>{currentOrder.warehousePickupTime}</DisplayValue>
              </div>
            </div>) : <></>}
          {(!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.so || typeof pagePrintSetting.so === 'undefined'))) && (
            <div style={{ display: "flex" }}>
              <div>
                <DisplayLabel>Sales Order #:</DisplayLabel>
              </div>
              <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                <DisplayValue>
                  {getOrderPrefix(sellerSetting, 'sales')}
                  {currentOrder.wholesaleOrderId}
                </DisplayValue>
              </div>
            </div>
          )}
          {(!pagePrintSetting ||
            (pagePrintSetting && (pagePrintSetting.po || typeof pagePrintSetting.po === 'undefined'))) &&
            currentOrder.reference && (
              <div style={{ display: "flex" }}>
                <div>
                  <DisplayLabel>Customer PO #:</DisplayLabel>
                </div>
                <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                  <DisplayValue>
                    {currentOrder.reference}
                  </DisplayValue>
                </div>
              </div>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.orderDate || typeof pagePrintSetting.orderDate === 'undefined'))) && (
              <div style={{ display: "flex" }}>
                <div>
                  <DisplayLabel>Order Date:</DisplayLabel>
                </div>
                <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                  <DisplayValue>
                    <Moment format="MM/DD/YYYY" date={currentOrder.orderDate} />
                  </DisplayValue>
                </div>
              </div>
            )}
          <div style={{ display: "flex" }}>
            <div>
              <DisplayLabel>Sales Rep:</DisplayLabel>
            </div>
            <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
              <DisplayValue>{`${currentOrder.seller ? currentOrder.seller.firstName : ""} ${currentOrder.seller ? currentOrder.seller.lastName : ""}`}</DisplayValue>
            </div>
          </div>
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.paymentTerm || typeof pagePrintSetting.paymentTerm === 'undefined'))) &&
            (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
              <div style={{ display: "flex" }}>
                <div>
                  <DisplayLabel>Payment Term:</DisplayLabel>
                </div>
                <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                  <DisplayValue>
                    {currentOrder.financialTerms
                      ? currentOrder.financialTerms
                      : currentOrder.wholesaleClient.paymentTerm}
                  </DisplayValue>
                </div>
              </div>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.numberOfPallets || typeof pagePrintSetting.numberOfPallets === 'undefined'))) &&
            currentOrder.numberOfPallets && (
              <div style={{ display: "flex" }}>
                <div>
                  <DisplayLabel>No. of pallets:</DisplayLabel>
                </div>
                <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                  <DisplayValue>
                    {currentOrder.numberOfPallets}
                  </DisplayValue>
                </div>
              </div>
            )}
          {bill_temperature && (
            <div style={{ display: "flex" }}>
              <div>
                <DisplayLabel>Temperature:</DisplayLabel>
              </div>
              <div style={{ display: "45%", marginLeft: "5%", minHeight: "19px" }}>
                <DisplayValue>
                  {currentOrder.temperature ?? ''}
                </DisplayValue>
              </div>
            </div>
          )}
        </div>
      )
    }

    const RenderTable = () => {
      const { currentOrder, sellerSetting } = this.props
      const clientName =
        currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
          ? currentOrder.wholesaleClient.clientCompany.companyName
          : 'N/A'

      if (this.props.currentPrintContainerId) {
        const itemList = _.flattenDeep(_.get(this.state, 'containerData.wholesaleOrderList', []).map(v => v.wholesaleOrderItemList))
        const totalUnit = _.reduce(
          itemList,
          (sum, n) => sum + n.picked,
          0,
        )
        const totalGrossWeight = _.reduce(
          itemList,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
          0,
        )
        const totalGrossVolume = _.reduce(
          itemList,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
          0,
        )
        return (
          <div style={{ marginBottom: 30, marginTop: 10 }} >
            <HeaderTextLeftBox style={{ color: 'black' }}>
              {this.props.orderId ? <>
                {clientName} | #{getOrderPrefix(sellerSetting, 'sales')}
                {currentOrder.wholesaleOrderId}
              </>: _.get(this.state.containerData, 'containerName', '')}
            </HeaderTextLeftBox>
            {_.get(this.state, 'containerData.wholesaleOrderList', []).map((v, index) => {
              const tableOrderItemsList = v.wholesaleOrderItemList.map(l => ({ ...l, displayOrderProduct: ~~l.displayOrder }))
              const groupByDisplayOrder = _.groupBy(tableOrderItemsList, 'displayOrderProduct')
              const items = Object.keys(groupByDisplayOrder).map(g => ({
                ...groupByDisplayOrder[g][0],
                quantity: _.reduce(
                  groupByDisplayOrder[g],
                  (sum, n) => sum + n.quantity,
                  0,
                ),
                picked: _.reduce(
                  groupByDisplayOrder[g],
                  (sum, n) => sum + n.picked,
                  0,
                ),
                catchWeightQty: _.reduce(
                  groupByDisplayOrder[g],
                  (sum, n) => sum + n.catchWeightQty,
                  0,
                )
              }))
              return (
                <div key={index} style={{ marginTop: 20 }}>
                  {!this.props.orderId && <p style={{ color: '#000' }}>
                    <span style={{ marginRight: 300 }}>
                      SHIP TO: {v.customerName}
                    </span>
                    ORDER NO. {getOrderPrefix(sellerSetting, 'sales')}{v.wholesaleOrderId}
                  </p>}
                  <PrintContainerInvoice tableClass={tableClass} items={_.sortBy(items, 'displayOrderProduct')} />
                </div>
              )
            })}
            <Flex style={{ justifyContent: 'space-between', marginTop: 10 }}>
              <div>
                <span>Total Units: </span>
                <span>{totalUnit}</span>
              </div>
            </Flex>
            <div>Total Gross Weight: <span style={{ fontWeight: 'bold' }}>{formatNumber(totalGrossWeight, 1)} LBs / {_.divide(totalGrossWeight, 2.2046).toFixed(2)} KG</span></div>
            <div>Total Gross Volume: <span style={{ fontWeight: 'bold' }}>{formatNumber(totalGrossVolume, 1)} ft³ / {_.divide(totalGrossVolume, 35.315).toFixed(2)} m³</span></div>
          </div>
        )
      }
      return (
        <>
          <PrintPickSheetTable style={{ marginTop: 8 }}>
            {/* {totals} */}
            <DeliveryListTable>
              <Table
                className={tableClass}
                bordered={false}
                style={{ width: '100%' }}
                rowKey="wholesaleOrderItemId"
                rowClassName={(record, index) => {
                  let rowOrderItemId = record.wholesaleOrderItemId
                  if (this.checkWeights(record)) {
                    rowOrderItemId = record.relatedOrderItemId
                  }
                  const i = orderItems.findIndex((oi) => oi.wholesaleOrderItemId === rowOrderItemId)
                  return i % 2 === 1 ? 'alternative' : ''
                }}
                columns={this.getDisplayingColumns(columns)}
                dataSource={items}
                pagination={false}
              />
            </DeliveryListTable>
            <hr />
            {totals}
          </PrintPickSheetTable>
          {type == 'invoice' && <InvoiceCreditMemoSection summary={this.state.totalAmount} tableClass={tableClass} />}
        </>
      )
    }

    const RenderTitle = () => {
      if (this.props.isHealthBol && !this.props.currentPrintContainerId) {
        return (
          <Row style={{ textAlign: 'center' }}>
            <h1 style={{ marginBottom: '0.25em' }}>USDA</h1>
          </Row>
        )
      }
      return (
        <Row style={{ textAlign: 'center' }}>
          {!this.props.currentPrintContainerId ?
            <h1 style={{ marginBottom: '0.25em' }}>
              {pagePrintSetting && pagePrintSetting.title
                ? pagePrintSetting.title.toUpperCase()
                : type == 'invoice'
                  ? 'INVOICE'
                  : type == 'manifest'
                    ? 'SHIPPING MANIFEST'
                    : 'BILL OF LADING'}
            </h1>
            :
            <h1 style={{ marginBottom: '0.25em' }}>{this.props.isHealthBol ? 'USDA'  : 'BILL OF LADING'} # {this.props.orderId} - {_.get(this.state.containerData, 'containerName', '')}</h1>
          }
        </Row>
      )
    }

    return (
      <PrintPickSheetWrapper id="invoiceContent">
        <HeaderBox style={{ fontWeight: 'normal' }}>
          {(this.props.orderId && !pagePrintSetting || (pagePrintSetting && (pagePrintSetting.barcode || typeof pagePrintSetting.barcode === 'undefined'))) && (
            <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} width={2} height={30} />
          )}
          <RenderTitle />
          <Row style={{ position: "relative", borderBottom: '1px solid #aaa' }}>
            <Col span={13}>
              <CompanyInfoWrapper>
                <div style={{ textAlign: 'left' }}>
                  {(!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.logo || typeof pagePrintSetting.logo === 'undefined'))) && (
                    <div style={{ marginRight: 10 }}>
                      {logo === 'default' ? (
                        <LogoWrapper>
                          <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                        </LogoWrapper>
                      ) : (
                        <img src={logo} style={{ maxWidth: 150 }} onLoad={this.onImageLoad} />
                      )}
                    </div>
                  )}
                  {company && (
                    <div className="left">
                      {company.companyName !== null && (
                        <HeaderTextLeftBox className="f18">{company.companyName}</HeaderTextLeftBox>
                      )}
                      {type !== 'bill' && type !== 'manifest' && (pagePrintSetting && (pagePrintSetting.submitPaymentWord || typeof pagePrintSetting.submitPaymentWord === 'undefined')) &&
                        <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "bold", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>Please submit payment to:</p>
                      }
                      {company.email !== null && <div style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>{company.email}</div>}
                      {company.mainBillingAddress !== null && (
                        <div style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>{formatAddress(company.mainBillingAddress.address, false)}</div>
                      )}
                      {(phone || company.fax) && (
                        <div style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>
                          Phone: {company.phone}{company.fax ? ` / Fax: ${company.fax}` : ''}
                        </div>
                      )}
                    </div>
                  )}
                </div>
              </CompanyInfoWrapper>
              <Row style={{ marginTop: 8 }} className="f12">
                {fulfillmentOptionType != 3 ? (
                  <>
                    {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
                      <div style={{ display: "flex" }}>
                        <div style={{ width: "250px" }}>
                          <h6 style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "bold", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>PICKUP ADDRESS</h6>
                          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{clientName}</p>
                          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{currentOrder.pickupAddress}</p>
                          {currentOrder.wholesaleClient &&
                            currentOrder.wholesaleClient.mainContact &&
                            (!pagePrintSetting ||
                              (pagePrintSetting &&
                                (pagePrintSetting.deliveryContact || typeof pagePrintSetting.deliveryContact === 'undefined'))) && (
                              <div style={{ display: "flex" }}>
                                <div>
                                  <p className="normal" style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>{fulfillmentOptionType == 2 ? 'Pickup Contact:' : ''}</p>
                                </div>
                                <div>
                                  <p className="normal" style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", marginLeft: "10px" }}>{fulfillmentOptionType == 2
                                    ? currentOrder.pickupContactName
                                    : ""}</p>
                                </div>
                              </div>
                            )}
                        </div>
                        <div style={{ width: "250px" }}>
                          <h6 style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "bold", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>SOLD TO:</h6>
                          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>{clientName}</p>
                          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>
                            {currentOrder.billingAddress ? formatAddress(currentOrder.billingAddress.address, true).trim() : 'N/A'}
                          </p>
                          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
                          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>
                            {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
                              ? currentOrder.wholesaleClient.mainBillingAddress.phone
                              : currentOrder.wholesaleClient.mobilePhone}
                          </p>
                        </div>
                      </div>
                    ) : (
                      <div style={{ display: "flex" }}>
                        {soldToCol}
                        {shipToCol}
                      </div>
                    )}
                  </>
                ) : (
                  <>{logisticCols}</>
                )}


              </Row>
              {currentOrder.wholesaleClient &&
                currentOrder.wholesaleClient.mainContact &&
                (!pagePrintSetting ||
                  (pagePrintSetting &&
                    (pagePrintSetting.deliveryContact || typeof pagePrintSetting.deliveryContact === 'undefined'))) && (
                  <Row
                    style={{
                      marginTop: 20,
                      //borderBottom: '1px solid #aaa',
                      paddingBottom: 5,
                      marginBottom: 5,
                    }}
                    className="f12"
                  >
                    <Col span={5}>
                      <HeaderTextLeftBox className="normal" style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>{fulfillmentOptionType == 2 ? '' : 'Delivery Contact'}</HeaderTextLeftBox>
                    </Col>
                    <Col span={18}>
                      <div style={{ paddingLeft: 15 }} style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>
                        {fulfillmentOptionType == 2
                          ? ""
                          : currentOrder.deliveryContact
                            ? currentOrder.deliveryContact
                            : currentOrder.wholesaleClient.mainContact
                              ? currentOrder.wholesaleClient.mainContact.name
                              : ''}
                      </div>
                    </Col>
                    {fulfillmentOptionType == 1 && (
                      <Col span={6} style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px" }}>
                        {currentOrder.deliveryPhone
                          ? currentOrder.deliveryPhone
                          : currentOrder.wholesaleClient.mainContact
                            ? currentOrder.wholesaleClient.mainContact.mobilePhone
                            : ''}
                      </Col>
                    )}
                  </Row>
                )}
            </Col>
            <RenderInfo />
          </Row>



          {type == 'invoice' &&
            (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.driver_payment_collection)) && (
              <Row style={{ padding: '10px 0' }} className="f12">
                <Col span={7}>
                  <Flex>
                    <HeaderTextLeftBox className="normal">CASH RECEIVED $</HeaderTextLeftBox>
                    <Underline>{this.state.totalQuantity}</Underline>
                  </Flex>
                </Col>

                <Col span={7} offset={1}>
                  <Flex>
                    <HeaderTextLeftBox className="normal">CHECK RECEIVED $</HeaderTextLeftBox>
                    <Underline>{this.state.totalQuantity}</Underline>
                  </Flex>
                </Col>
                <Col span={7} offset={1}>
                  <Flex>
                    <HeaderTextLeftBox className="normal">CHECK #</HeaderTextLeftBox>
                    <Underline>{this.state.totalQuantity}</Underline>
                  </Flex>
                </Col>
              </Row>
            )}
          {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.account_balance_forward)) && (
            <>
              <LightColoredRow style={{ padding: '4px 0px' }} className="f12">
                <Col span={4}>DATE</Col>
                <Col span={16}>ACCOUNT SUMMARY</Col>
                <Col span={4} style={{ textAlign: 'right' }}>
                  AMOUNT
                </Col>
              </LightColoredRow>
              <Row style={{ padding: '4px 0px' }} className="f12">
                <Col span={4}>01/12/2015</Col>
                <Col span={16}>
                  <div>Balance Forward</div>
                  <div>New charges(details below)</div>
                  <div>Total Amount Due</div>
                </Col>
                <Col span={4} style={{ textAlign: 'right' }}>
                  <div>100</div>
                  <div>100</div>
                  <div>100</div>
                </Col>
              </Row>
            </>
          )}
          {currentOrder.customerNote && (
            <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
              <Col span={3}>
                <HeaderTextLeftBox>Notes to Customer</HeaderTextLeftBox>
              </Col>
              <Col span={6}>
                <div style={{ paddingLeft: 15 }}>{currentOrder.customerNote}</div>
              </Col>
            </Row>
          )}
          {currentOrder.deliveryInstruction && (
            <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
              <Col span={24}>
                <HeaderTextLeftBox hidden={(companyId == 'Growers Produce')}>
                  {fulfillmentOptionType === 1 || fulfillmentOptionType === 2 ? 'Fulfillment ' : 'Delivery '}
                  Instructions
                </HeaderTextLeftBox>
                <div >{currentOrder.deliveryInstruction}</div>
              </Col>
            </Row>
          )}
        </HeaderBox>
        <RenderTable />
        {/* <UnderLineWrapper>
          <div>Warehouse Verification (Signature):</div>
        </UnderLineWrapper> */}
        <div className='floating-to-bottom'>
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.verificationSignature ||
                typeof pagePrintSetting.verificationSignature === 'undefined'))) && (
              <Flex>
                <div style={{ color: 'black' }}>Warehouse Verification (Signature):</div>
                <Underline />
              </Flex>
            )}
          {/* <ModalDetailText>
          {clientName === 'TBD'
            ? 'The perishable agricultural commodities listed on this invoice are sold subject to the statutory trust authorized by section 5(c) of the Perishable Agricultural Commodities Act, 1930 (7 U.S.C. 499e(c)). The seller of these commodities retains a trust claim over these commodities, all inventories of food or other products derived from these commodities, and any receivables or proceeds from the sale of these commodities until full payment is received.'
            : 'All Claim for damaged or shortage of goods should be made within 24 hrs upon receipt of merchandise/goods. The perishable commodities listed on this invoice are sold subject to the statutory trust authorized by section 5c of the Perishable Agricultural Commodities Act, 1930 (7 U.S. C499E c). Friends Development (USA), Inc. retains a trust claim over these said commodities, all inventories or other products derived from these commodities and any receivables or proceeds from the sales of these commodities until full payment is received (Net 15 Days).'}
          <br />
          <b>溫馨提醒: 請客戶務必當面點清貨品數量後再簽收, 過後發現丟失, 本公司概不負責! 敬請見諒!</b>
        </ModalDetailText> */}
          {pagePrintSetting && pagePrintSetting.terms &&
            <ModalDetailText className="f10" style={{ paddingBottom: 0, marginBottom: 0 }}>
              {pagePrintSetting.terms}
            </ModalDetailText>
          }

          {(type == 'invoice' || bill_nameAndSignature == true) && (
            <Flex style={{ marginTop: 20 }}>
              <div style={{ color: 'black' }}>Customer Print Name:</div>
              <Underline />
              <div style={{ color: 'black' }}>Signature:</div>
              <Underline />
            </Flex>
          )}
          <div className="freight-carrier-section" style={{ display: type == 'invoice' ? 'none' : 'block' }}>
            <Row>
              <Col span={12}>
                {(!pagePrintSetting || (pagePrintSetting &&
                  (pagePrintSetting.carrier ||
                    typeof pagePrintSetting.carrier === 'undefined'))) &&
                  <Row className="label-row">
                    <Col span={8}>Carrier: </Col>
                    <Col offset={2} span={14}>{currentOrder.carrier ? currentOrder.carrier : ''}</Col>
                  </Row>
                }
                {(!pagePrintSetting || (pagePrintSetting &&
                  (pagePrintSetting.carrierPhoneNo ||
                    typeof pagePrintSetting.carrierPhoneNo === 'undefined'))) &&
                  <Row className="label-row">
                    <Col span={8}>Carrier Phone No.: </Col>
                    <Col offset={2} span={14}>{currentOrder.carrierPhoneNo ? currentOrder.carrierPhoneNo : ''}</Col>
                  </Row>
                }
                {(!pagePrintSetting || (pagePrintSetting &&
                  (pagePrintSetting.driverName ||
                    typeof pagePrintSetting.driverName === 'undefined'))) &&
                  <Row className="label-row">
                    <Col span={8}>Driver Name: </Col>
                    <Col offset={2} span={14}>{currentOrder.driverName ? currentOrder.driverName : ''}</Col>
                  </Row>
                }
                {(!pagePrintSetting || (pagePrintSetting &&
                  (pagePrintSetting.driverLicense ||
                    typeof pagePrintSetting.driverLicense === 'undefined'))) &&
                  <Row className="label-row">
                    <Col span={8}>Driver License No.: </Col>
                    <Col offset={2} span={14}>{currentOrder.driverLicense ? currentOrder.driverLicense : ''}</Col>
                  </Row>
                }
              </Col>
              {(!pagePrintSetting || (pagePrintSetting &&
                (pagePrintSetting.driverLicenseImageField ||
                  typeof pagePrintSetting.driverLicenseImageField === 'undefined'))) &&
                <Col span={12}>
                  <Row className="label-row">
                    <Col span={24}>
                      <div style={{ color: 'black' }}>Driver License Image:</div>
                      <div style={{ width: '7.9375cm', height: '5.3975cm' }}></div>
                    </Col>
                  </Row>
                </Col>
              }
            </Row>
            {(!pagePrintSetting || (pagePrintSetting &&
              (pagePrintSetting.driverSignatureField ||
                typeof pagePrintSetting.driverSignatureField === 'undefined'))) &&
              <Flex style={{ marginTop: 20 }}>
                <div style={{ color: 'black' }}>Driver Signature:</div>
                <Underline />
              </Flex>
            }
            {(pagePrintSetting &&
              (pagePrintSetting.carrierTerms || typeof pagePrintSetting.carrierTerms === 'undefined') &&
              pagePrintSetting.carrierTermsText) &&
              <ModalDetailText className="f10" style={{ marginTop: 20 }}>
                {pagePrintSetting.carrierTermsText}
              </ModalDetailText>
            }
          </div>

          {(!pagePrintSetting ||
            (pagePrintSetting && (
              pagePrintSetting.barcode ||
              typeof pagePrintSetting.barcode === 'undefined' ||
              pagePrintSetting.printTime ||
              typeof pagePrintSetting.printTime === 'undefined' ||
              pagePrintSetting.so ||
              typeof pagePrintSetting.so === 'undefined'
            ))) && (
              <div style={{ marginTop: 20 }}>
                <div>
                  {(!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.barcode || typeof pagePrintSetting.barcode === 'undefined'))) ? (
                    <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} width={2} height={30} />
                  ) : (
                    <div>&nbsp;</div>
                  )}
                </div>
                <Flex className="v-center between" style={{ fontSize: 16, paddingTop: 5 }}>
                  <div>
                    {(!pagePrintSetting ||
                      (pagePrintSetting && (pagePrintSetting.printTime || typeof pagePrintSetting.printTime === 'undefined'))) && (
                        <HeaderTextRightBox style={{ letterSpacing: 0, color: 'black' }}>
                          Printed {moment().format('MM/DD/YY hh:mm A')}
                        </HeaderTextRightBox>
                      )}
                  </div>
                  <div>
                    {(!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.so || typeof pagePrintSetting.so === 'undefined'))) && this.getNameAndOrderNo()}
                  </div>
                </Flex>
              </div>
            )}
        </div>
      </PrintPickSheetWrapper >
    )
  }
}

const CreditMemoSection = ({ adjustments, summary, printSetting, tableClass }: any) => {
  if (!printSetting.invoice_credit_memo) return null
  const dataSource = adjustments
    .map((v) => ({
      quantity: v.quantity,
      uom: v.uom,
      orderId: v.order.wholesaleOrderId,
      deliveryDate: v.order.deliveryDate,
      returnInInventory: v.returnInInventory,
      variety: v.orderItem.wholesaleItem.variety,
      sku: v.orderItem.wholesaleItem.sku,
      wholesaleProductUomList: v.item.wholesaleProductUomList,
      inventoryUOM: v.item.inventoryUOM,
      price: v.salePrice,
      pricingUOM: v.orderItem.pricingUOM,
    }))
    .filter((ad) => ad.quantity)

  const getBillableQtyWithUOM = (record: any) => {
    let showBillableQty,
      uom,
      subUom = null,
      pricingSubUom = null

    if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
      let tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.uom,
      )
      subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
      tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.pricingUOM,
      )
      pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
    }

    if (record.quantity) {
      uom = record.pricingUOM
      if (record.pricingUOM === record.inventoryUOM) {
        if (record.uom === record.inventoryUOM) {
          showBillableQty = record.quantity ? record.quantity : 0
        } else if (subUom != null && record.uom === subUom.name) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity / subUom.ratio, 4) : 0
        } else {
        }
      } else {
        if (record.uom === record.inventoryUOM && pricingSubUom != null) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity * pricingSubUom.ratio, 4) : 0
        } else if (subUom != null && record.uom === subUom.name && pricingSubUom != null) {
          showBillableQty = record.quantity ? (record.quantity / subUom.ratio) * pricingSubUom.ratio : 0
        }
      }
    } else {
      showBillableQty = record.quantity
      uom = record.pricingUOM
    }

    if (!record.pricingUOM) {
      showBillableQty = record.quantity
    }
    return {
      showBillableQty,
      uom,
    }
  }

  const columns = [
    {
      title: '#',
      key: 'index',
      align: 'center',
      width: 80,
      render: (_key: any, _row: any, index: number) => index + 1,
    },
    {
      title: 'CREDITED',
      align: 'right',
      width: 100,
      render: (r) => `-${r.quantity} ${r.uom || ''}`,
    },
    {
      title: 'ORDER',
      align: 'left',
      width: 120,
      render: (r) => {
        return (
          <div>
            {r.orderId}
            <br />
            {r.deliveryDate}
          </div>
        )
      },
    },
    {
      title: 'RETURN',
      align: 'left',
      render: (r) => (r.returnInInventory ? 'YES' : 'NO'),
    },
    {
      title: 'PRODUCT',
      align: 'left',
      dataIndex: 'variety',
      render: (t) => <div style={{ maxWidth: 200 }}>{t}</div>,
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
    },
    {
      title: 'BILLABLE QTY',
      align: 'right',
      dataIndex: 'catchWeightQty',
      render: (weight: number, record: any) => {
        const { uom, showBillableQty } = getBillableQtyWithUOM(record)
        return (
          <>
            -{mathRoundFun(showBillableQty, 4)} {uom}
          </>
        )
      },
    },
    {
      title: 'PRICE',
      dataIndex: 'price',
      align: 'right',
      width: 120,
      render: (value: number, record: any) => {
        let price = value
        // const pricingUOM = record.returnUom
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        if (_.isNumber(price)) {
          price = basePriceToRatioPrice(pricingUOM, price, record)
        }
        var isPricingUOMEnabled = (!printSetting || (printSetting &&
          (printSetting.invoice_pricingUOM || typeof printSetting.invoice_pricingUOM === 'undefined')))
        return (
          <>
            {`${price ? '$' + formatNumber(price, 2) : '$0'}`}{isPricingUOMEnabled ? `/${pricingUOM}` : ''}
          </>
        )
      },
    },
    {
      title: 'SUBTOTAL',
      align: 'right',
      dataIndex: 'totalPrice',
      render: (data: number, record: any) => {
        const extended = Number(record.price ? record.price : 0)
        const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
        const total = extended * quantity

        return <div>{total ? `-$${formatNumber(total, 2)}` : ''}</div>
      },
    },
  ]

  let totalReturn = 0
  let totalQty = 0
  let totalUnit = 0

  if (dataSource.length > 0) {
    dataSource.forEach((record, index) => {
      const extended = Number(record.price ? record.price : 0)
      const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
      let showValue = false
      if (quantity) {
        showValue = true
      }
      totalReturn += showValue ? extended * quantity : 0

      const { showBillableQty } = getBillableQtyWithUOM(record)
      totalQty += showBillableQty

      totalUnit += typeof record.quantity !== 'undefined' ? parseFloat(record.quantity) : 0
    })
  }
  const Totals = () => {
    return (
      <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24} type="flex" justify="space-between">
        <Col span={8}>
          {!!totalUnit && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextLeftBox className="normal black">TOTAL CREDITED UNITS</HeaderTextLeftBox>
              </Col>
              <Col span={8}>
                <HeaderTextLeftBox className="black">-{totalUnit}</HeaderTextLeftBox>
              </Col>
            </Row>
          )}
        </Col>
        <Col span={8}>
          {!!totalReturn && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextRightBox className="normal black">TOTAL CREDIT</HeaderTextRightBox>
              </Col>
              <Col span={8}>
                <HeaderTextRightBox className="black">-${formatNumber(Math.abs(totalReturn), 2)}</HeaderTextRightBox>
              </Col>
            </Row>
          )}
          <Row gutter={24}>
            <Col span={16}>
              <HeaderTextRightBox className="normal black">SUMMARY</HeaderTextRightBox>
            </Col>
            <Col span={8}>
              <HeaderTextRightBox className="black">
                {summary - totalReturn < 0 && '-'}${formatNumber(Math.abs(summary - Math.abs(totalReturn)), 2)}
              </HeaderTextRightBox>
            </Col>
          </Row>
        </Col>
      </Row>
    )
  }

  return dataSource.length ? (
    <div style={{ marginBottom: '20px' }}>
      <PrintPickSheetTable style={{ marginTop: 10 }}>
        {!!totalReturn && (
          <DeliveryListTable>
            <Table
              style={{ width: '100%' }}
              rowKey="wholesaleOrderItemId"
              columns={columns}
              dataSource={dataSource}
              pagination={false}
              className={tableClass}
              bordered={false}
              rowKey="wholesaleOrderItemId"
              rowClassName={(record, index) => {
                return index % 2 === 1 ? 'alternative' : ''
              }}
            />
          </DeliveryListTable>
        )}
      </PrintPickSheetTable>
      <hr />
      <Totals />
    </div>
  ) : (
    <></>
  )
}

const InvoiceCreditMemoSection = connect(({ orders, setting }) => ({
  adjustments: orders.adjustments,
  printSetting: JSON.parse(orders.printSetting),
}))(CreditMemoSection)

export default PrintBillOfLading

const PrintContainerInvoice: FC<any> = ({ tableClass, items, isHealthBol }) => {
  const columns = [
    // {
    //   title: '#',
    //   width: 70,
    //   render: (_, r, i) => `${i + 1}`
    // },
    {
      title: 'ORDERED',
      dataIndex: 'quantity',
      align: 'center',
      className: isHealthBol ? 'isHealthBol' : ''
    },
    {
      title: 'SHIPPED',
      dataIndex: 'picked',
      align: 'center'
    },
    // {
    //   title: 'SHIPPED UOM',
    //   dataIndex: 'UOM'
    // },
    {
      title: 'PRODUCT',
      dataIndex: 'itemName'
    },
    {
      title: 'SIZE',
      dataIndex: 'size'
    },
    {
      title: 'BRAND',
      dataIndex: 'modifiers'
    },
    {
      title: 'ORIGIN',
      dataIndex: 'extraOrigin'
    },
    {
      title: 'UNIT VOL.',
      align: 'right',
      render: (r) => {
        if (!r.grossVolume) return
        return <span>{r.grossVolume}</span>
      }
    },
    {
      title: 'GROSS VOL.',
      align: 'right',
      render: (r) => {
        if (!r.catchWeightQty || !r.grossVolume) return
        return <span>{formatNumber(_.multiply(r.catchWeightQty, r.grossVolume || 0), 1)}</span>
      }
    },
    {
      title: 'UNIT WT.',
      align: 'right',
      render: (r) => {
        if (!r.grossWeight) return
        return <span>{r.grossWeight}</span>
      }
    },
    {
      title: 'GROSS WT.',
      align: 'right',
      render: (r) => {
        if (!r.catchWeightQty || !r.grossWeight) return
        return <span>{formatNumber(_.multiply(r.catchWeightQty, r.grossWeight || 0), 1)}</span>
      }
    },
  ]
  return (
    <PrintPickSheetTable className="pick-sheet" style={{ marginTop: 20 }}>
      <Table
        className={tableClass}
        rowKey="wholesaleOrderItemId"
        columns={columns}
        rowClassName={(record, index) => {
          return index % 2 === 1 ? 'alternative' : ''
        }}
        dataSource={items}
        pagination={false}
      />
    </PrintPickSheetTable>
  )
}
