import React from 'react'
import { OrderDetail, OrderItem } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode'
import {
  HeaderTextRightBox,
  HeaderTextLeftBox,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
  ModalDetailText,
  UnderLineWrapper,
  Underline,
  DeliverySction,
  DeliveryListTable,
  WeightSpan,
  WeightWrapper,
  LightColoredRow,
  pl24,
  DisplayLabel,
  DisplayValue,
  DisplayLeftLabel,
  InfoLabel,
  InfoValue,
  SelectFreshInvoiceWrapper,
  SelectFreshInvoiceBody,
  SelectFreshInvoiceBillTo,
  SelectFreshInvoiceShipTo,
  SelectFreshInvoiceNumber,
  SelectFreshInvoiceDate,
  SelectFreshInvoiceTerms,
  SelectFreshInvoiceRep,
  SelectFreshInvoiceShipMethod,
  SelectFreshInvoiceLineItems,
  SelectFreshInvoiceItemRow,
  SelectFreshGrandTotal,
  SelectFreshPageSection,
  SelectFreshPrintableBodyWrapper,
  SelectFreshWeightSpan,
  BelenTradingInvoiceWrapper,
  BelenTradingDisplayLeftLabel,
  BelenTradingDisplayLeftValue,
  BelenTradingDisplayRightValue,
  BelenTradingDisplayGreenBoxLabel,
  BelenTradingDisplayGreenBoxValue,
  BelenTradingPrintPickSheetTable
} from '~/modules/customers/sales/cart/print/_styles'
import Moment from 'react-moment'
import {
  formatNumber,
  formatAddress,
  basePriceToRatioPrice,
  mathRoundFun,
  getOrderPrefix,
  inventoryQtyToRatioQty,
  ratioQtyToInventoryQty,
  numberMultipy,
  judgeConstantRatio,
  formatItemDescription,
  isCustomOrderNumberEnabled,
  checkError,
  formatPrintOriginAddress,
} from '~/common/utils'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import { Icon } from '~/components'
import { timingFunctions } from 'polished'
import { timeout } from 'rxjs/operators'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import _, { cloneDeep } from 'lodash'
import moment from 'moment'
import { PrintSetting } from '~/modules/setting/components/print-setting'
import { connect } from 'react-redux'
import { url2base64 } from './sendEmail'
import { OrderService } from '~/modules/orders/order.service'
import {CACHED_COMPANY} from '~/common'

const selectFreshInvoiceTemplateImg = require('~/modules/customers/8.5_X_5.5_Forms_4_Parts_No_CarbonV2.jpg')

interface PrintBillOfLadingProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  logo: string
  company: any
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  title?: string
  multiple?: boolean
  printSetting: any
  type: string
  changePrintLogoStatus: Function
  fulfillmentOptionType?: number
  sellerSetting?: any
  paymentTerms: any[]
  selectedType?: number
  currentPrintContainerId: string
  orderId: string
  oneOffItemList: OrderItem[]
}

export class PrintInvoice extends React.PureComponent<PrintBillOfLadingProps> {
  state = {
    totalQuantity: 0,
    totalAmount: 0,
    totalWeight: 0,
    totalTax: 0,
    totalSubtotal: 0,
    visibleAlert: false,
    selectedRowKeys: [],
    orderItems: this.props.orderItems,
    pagePrintSetting: null,
    logo: '',
    containerData: {},
    overrideCustomLayout: false,
    //customInvoiceTemplate: [],
  }

  componentDidMount(): void {
    const _this = this
    this.getAmount(this.props.orderItems)
    if (this.props.printSetting) {
      this.setPrintSetting(this.props.printSetting, this.props.type)
    }
    if (this.props.logo) {
      url2base64(this.props.logo.replace('www.', '')).then((logo) => {
        this.setState({ logo })
      })
    }
    if (this.props.currentPrintContainerId && this.props.orderId) {
      if (this.props.currentOrder.fulfillmentType === 5) {
        OrderService.instance.getSingleContainerPrint(this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: {
                ...resp.body.data,
                tempRecorderList: JSON.parse(resp.body.data.tempRecorderList)
              }
            })
          },
          error(err) { checkError(err) },
        })
      } else {
        OrderService.instance.getPrintContainer(this.props.currentPrintContainerId, this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: resp.data
            })
          },
          error(err) { checkError(err) },
        })
      }
    }
    if (this.props.currentPrintContainerId && !this.props.orderId) {
      OrderService.instance.getPrintContainer(this.props.currentPrintContainerId).subscribe({
        next(resp) {
          _this.setState({
            containerData: resp.data
          })
        },
        error(err) { checkError(err) },
      })
    }
  }

  componentWillReceiveProps(nextProps: PrintBillOfLadingProps) {
    if (this.props.orderItems !== nextProps.orderItems) {
      this.getAmount(nextProps.orderItems)
      this.setState({ orderItems: nextProps.orderItems })
    }

    if (this.props.printSetting != nextProps.printSetting && nextProps.printSetting != null) {
      this.setPrintSetting(nextProps.printSetting, nextProps.type)
    }
  }

  setPrintSetting = (jsonSetting: string, modalType: string) => {
    const pageSetting = JSON.parse(jsonSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`${modalType}_`) > -1) {
        const newKey = el.substring(`${modalType}_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    this.setState({ pagePrintSetting: data })
  }

  getAmount = (items: any) => {
    let _totalQuantity = 0
    let _totalWeight = 0
    let _totalTax =  0
    let _totalSubtotal = 0
    let _totalAmount = 0

    for (let i = 0; i < items.length; i++) {
      let taxRate = _.toNumber(items[i].taxRate ? items[i].taxRate : 0)
      let taxRateRounded = mathRoundFun(taxRate / 100, 6)
      let constantRatio = judgeConstantRatio(items[i])
      let picked = _.toNumber(items[i].picked)
      let subtotal = 0
      let tax = 0
      let taxEnabled = items[i].taxEnabled

      let pricingUOM = items[i].pricingUOM ? items[i].pricingUOM : items[i].UOM
      let unitUOM = items[i].UOM
      let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(items[i].price), items[i], 2)

      if (items[i].oldCatchWeight) {
        _totalWeight += _.toNumber(items[i].catchWeightQty)
        subtotal = numberMultipy(_.toNumber(items[i].price), _.toNumber(items[i].catchWeightQty))
      } else {
        if (!constantRatio) {
          _totalWeight += inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i])
          subtotal = numberMultipy(
            ratioPrice,
            inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i], 12),
          )
        } else {
          const quantity = inventoryQtyToRatioQty(
            pricingUOM,
            ratioQtyToInventoryQty(unitUOM, _.toNumber(items[i].picked), items[i], 12),
            items[i],
            2,
          )
          _totalWeight += quantity
          subtotal = numberMultipy(ratioPrice, quantity)
        }
      }

      tax = taxEnabled ? subtotal * taxRateRounded : 0
      subtotal = Math.round(subtotal * 100) / 100
      items[i]['amount'] = '$' + formatNumber(subtotal, 2)

      _totalQuantity += picked
      _totalSubtotal += subtotal
      _totalTax += tax
      _totalAmount += subtotal + tax
    }

    this.setState({
      totalQuantity: _totalQuantity,
      totalAmount: _totalAmount,
      totalWeight: _totalWeight,
      totalTax: _totalTax,
      totalSubtotal: _totalSubtotal,
      orderItems: items,
    })
  }

  getExpandableRowKeys = () => {
    const { orderItems } = this.state
    let result: any[] = []
    orderItems.forEach((el) => {
      if (!el.constantRatio) {
        result.push(el.wholesaleOrderItemId)
      }
    })
    return result
  }

  renderCatchWeightValues = (weights: any[]) => {
    // const { catchWeightValues, multiple } = this.props

    // let weights: any[] = []
    // if (multiple && typeof record.catchWeightValues !== 'undefined') {
    //   weights = record.catchWeightValues
    // } else {
    //   weights = catchWeightValues[record.wholesaleOrderItemId] ? catchWeightValues[record.wholesaleOrderItemId] : []
    // }
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (weights.length > 0) {
      let result: any[] = []
      let sub: any[] = []
      weights.forEach((el: any, index: number) => {
        if (index > 0 && (index - 10) % 10 == 0) {
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
          sub = []
        }

        const weightEl = <WeightSpan>{el.unitWeight ? el.unitWeight : ''}</WeightSpan>
        if (index == weights.length - 1) {
          sub.push(weightEl)
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
        } else {
          sub.push(weightEl)
        }
      })
      return result
    }

    return []
  }

  checkWeights = (row: any) => {
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (type == 'invoice' && pagePrintSetting && !pagePrintSetting.catchWeightValues) {
      return false
    }
    return typeof row.weights !== 'undefined' && row.weights.length > 0
  }

  getDisplayingColumns = (columns: any[], isLooseItems: boolean) => {
    const { pagePrintSetting } = this.state
    let newColumns: any[] = []
    if (!pagePrintSetting) return columns

    columns.forEach((el) => {
      const key = el.key
      if (pagePrintSetting[key] !== false) {
        newColumns.push(el)
      }
    })

    if (isLooseItems) {
      return newColumns;
    }

    return [
      {
        title: this.getNameAndOrderNo(),
        className: 'name-order',
        children: newColumns,
      },
    ]
    // return newColumns
  }

  getNameAndOrderNo = () => {
    const { currentOrder, sellerSetting } = this.props
    const { pagePrintSetting } = this.state
    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'
    const customerPO = pagePrintSetting?pagePrintSetting['primary_number']?pagePrintSetting['primary_number'] == "customerPO#"?true:false:false:false
    return (
      <HeaderTextRightBox style={{ color: 'black' }}>
        {customerPO?
        <>
          {clientName} | #{currentOrder.reference}
        </>
        :
        <>
        {clientName} | #{getOrderPrefix(sellerSetting, 'sales')}
        {currentOrder.wholesaleOrderId}</>}
      </HeaderTextRightBox>
    )
  }

  onImageLoad = () => {
    this.props.changePrintLogoStatus()
  }

  render() {
    const {
      currentOrder,
      title,
      type,
      fulfillmentOptionType,
      sellerSetting,
      catchWeightValues,
      multiple,
      printSetting,
      company,
      paymentTerms,
      companyProductTypes
      //customInvoiceTemplate,
    } = this.props
    const { orderItems, pagePrintSetting } = this.state
    const carrierSetting = pagePrintSetting ? pagePrintSetting.carrier_reference : false
    console.log(pagePrintSetting)
    const origin = pagePrintSetting ? pagePrintSetting.origin : false
    const soldto = pagePrintSetting ? pagePrintSetting.soldto : false
    const shipto = pagePrintSetting ? pagePrintSetting.shipto : false

    let items = orderItems.filter(el=>el.quantity > 0)

    let tableClass = ''
    if (printSetting) {
      const setting = JSON.parse(printSetting)
      if (
        (type === 'invoice' && setting.invoice_alternateRow === true) ||
        (type !== 'invoice' && setting.bill_alternateRow === true)
      ) {
        tableClass = 'alternate-row'
      }
      if (this.props.companyName === 'Hung San' || setting.invoice_item_sort === 'sku') {
        items = _.orderBy(items, ({SKU})=> SKU ?? '', 'asc')
      } else if(setting.invoice_item_sort == 'location') {
        items = _.orderBy(items, (record: any)=> {
          if(!!currentOrder?.isWorkorder) {
            return record.location
          } else if (sellerSetting.company && sellerSetting.company.locationMethod == 1 && record.itemLocation) {
            return record.itemLocation
          } else if (record.locationNames && record.locationNames.length > 0) {
            return record.locationNames.join(', ')
          } else {
            return ''
          }
        }, 'asc')
      }
    }

    // those code will print catchweight item, so it can't be removed
    orderItems.forEach((record, i) => {
      const index = items.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
      if (index >= 0) {
        items[index].key = i
        if (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightValues)) {
          let showCatchWeightModal = false
          let constantRatio = judgeConstantRatio(record)
          if (!constantRatio) {
            showCatchWeightModal = true
          }
          if (showCatchWeightModal) {
            let weights: any[] = []
            if (multiple && typeof record.catchWeightValues !== 'undefined') {
              weights = record.catchWeightValues
            } else {
              weights = catchWeightValues[record.wholesaleOrderItemId]
                ? catchWeightValues[record.wholesaleOrderItemId]
                : []
            }

            if (weights.length > 0) {
              items.splice(index + 1, 0, {
                // ...cloneDeep(record),
                relatedOrderItemId: record.wholesaleOrderItemId,
                weights,
              })
            }
          }
        }
      }
    })

    // const expandedRowKeys = this.getExpandableRowKeys()
    const columns: Array<any> = [
      // {
      //   title: '#',
      //   dataIndex: 'key',
      //   key: 'index',
      //   align: 'center',
      //   width: 60,
      //   render: (key: number, row: OrderItem) => {
      //     return this.checkWeights(row) ? '' : key + 1
      //   },
      // },
      {
        title: 'ORDERED',
        dataIndex: 'quantity',
        key: 'quantity',
        align: 'center',
        className: 'ordered-quantity',
        width: 70,
        render: (value: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }

          return mathRoundFun(value, 2)
        }
      },
      {
        title: 'SHIPPED',
        dataIndex: 'picked',
        key: 'picked',
        align: 'center',
        // width: 80,
        render: (value: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }

          return mathRoundFun(value, 2)
        }
      },
      {
        title: 'SHIPPED UOM',
        dataIndex: 'UOM',
        key: 'UOM',
        align: 'center',
        width: 70,
      },
      {
        title: 'PRODUCT',
        dataIndex: 'variety',
        key: 'variety',
        align: 'left',
        className: 'th-left',
        render: (variety: string, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return {
              children: this.renderCatchWeightValues(row.weights),
              props: {
                colSpan: 8,
              },
            }
          }
          if (row.itemId) {
            return row.editedItemName ? row.editedItemName : formatItemDescription(variety, row.SKU, sellerSetting, row.customerProductCode)
          }
          const desc = row.chargeDesc ? ` (${row.chargeDesc})` : ''
          return `${variety}${desc}`
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'UPC',
        dataIndex: 'UPC',
        key: 'UPC',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'BRAND',
        dataIndex: 'modifiers',
        key: 'modifiers',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'extraOrigin',
        key: 'extraOrigin',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        // wait plu from backend
        title: 'PLU',
        dataIndex: 'plu',
        key: 'PLU',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value ? value : ''
        },
      },
      {
        title: 'BILLABLE QTY',
        dataIndex: 'catchWeightQty',
        key: 'catchWeightQty',
        align: 'right',
        width: 70,
        render: (val: any, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showBillableQty,
            showValue = false
          if (record.picked || record.catchWeightQty) {
            showValue = true
          }

          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM

          let constantRatio = judgeConstantRatio(record)
          if (record.oldCatchWeight) {
            showBillableQty = record.catchWeightQty
          } else {
            if (!constantRatio) {
              showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
            } else {
              showBillableQty = inventoryQtyToRatioQty(
                pricingUOM,
                ratioQtyToInventoryQty(unitUOM, record.picked, record, 12),
                record,
              )
            }
          }

          return showValue ? `${mathRoundFun(showBillableQty, 2)}${pricingUOM != null ? ' ' + pricingUOM : ''}` : ''
        },
      },
      // {
      //   title: 'BILLABLE UOM',
      //   dataIndex: 'UOM',
      //   key: 'UOM',
      //   align: 'center',
      //   render: (price: number, record: any) => {
      //     return record.pricingUOM ? record.pricingUOM : record.baseUOM
      //   },
      // },
      {
        title: 'PRICE',
        dataIndex: 'price',
        align: 'right',
        key: 'price',
        className: 'p4',
        width: 100,
        render: (price: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          var uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          var isPricingUOMEnabled =
            !pagePrintSetting ||
            (pagePrintSetting && (pagePrintSetting.pricingUOM || typeof pagePrintSetting.pricingUOM === 'undefined'))
          price = basePriceToRatioPrice(uom, price, record)
          return (
            <div style={{ fontWeight: 500, whiteSpace: 'nowrap' }}>
              ${formatNumber(price, 2)}
              {isPricingUOMEnabled && uom != null ? '/' + uom : ''}
            </div>
          )
        },
      },
      {
        title: 'SUBTOTAL',
        dataIndex: 'quantity',
        key: 'total',
        align: 'right',
        className: 'p4',
        width: 90,
        render: (data: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showValue = false
          let total = 0

          if (record.picked || record.catchWeightQty) {
            showValue = true
          }
          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM
          let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(record.price), record, 2)

          let constantRatio = judgeConstantRatio(record)

          if (!constantRatio) {
            total = numberMultipy(
              ratioPrice,
              inventoryQtyToRatioQty(pricingUOM, _.toNumber(record.catchWeightQty), record, 12),
            )
          } else {
            let quantity = inventoryQtyToRatioQty(
              pricingUOM,
              ratioQtyToInventoryQty(unitUOM, _.toNumber(record.picked), record, 12),
              record,
              2,
            )
            total = numberMultipy(ratioPrice, quantity)
          }
          let dispTaxInd = showValue && total && currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && record.taxEnabled && record.tax && _.toNumber(record.tax) > 0;
          let dispTaxMargin = currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && _.toNumber(this.state.totalTax) > 0 && !dispTaxInd ? 12 : 0;
          return <div style={{ fontWeight: 500, marginRight: dispTaxMargin }}>{showValue && total ? `$${formatNumber(total, 2)}` : ''} { dispTaxInd ? "T" : "" }</div>

        },
      },
    ]

    if (
      !pagePrintSetting ||
      (pagePrintSetting && (pagePrintSetting.lot || typeof pagePrintSetting.lot === 'undefined'))
    ) {
      const sizeIndex = columns.findIndex((el) => el.dataIndex == 'extraOrigin')
      if (sizeIndex <= 1) return
      columns.splice(sizeIndex + 1, 0, {
        title: 'LOT',
        dataIndex: 'lotId',
        key: 'lotId',
        width: 120,
        align: 'center',
        render: (value: string, row: any) => {
          return (
            <div>
              {_.get(row, 'lotIds', []).map((lotId) => (
                <span key={lotId} style={{ display: 'block' }}>
                  {lotId}
                </span>
              ))}
            </div>
          )
        },
      })
    }

    const totals = (
      <>
        <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24}>
          <Col span={8}>
            {(!pagePrintSetting ||
              (pagePrintSetting &&
                (pagePrintSetting.totalUnits || typeof pagePrintSetting.totalUnits === 'undefined'))) && (
              <Row gutter={24}>
                <Col span={16}>
                  <HeaderTextLeftBox className="normal black">TOTAL UNITS</HeaderTextLeftBox>
                </Col>
                <Col span={8}>
                  <HeaderTextLeftBox className="black">{this.state.totalQuantity}</HeaderTextLeftBox>
                </Col>
              </Row>
            )}
          </Col>
          <Col span={8}>
            {type === 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightQty)) && (
              <Row gutter={24}>
                <Col span={16}>
                  <HeaderTextLeftBox className="normal black">TOTAL BILLABLE QTY</HeaderTextLeftBox>
                </Col>
                <Col span={8}>
                  <HeaderTextLeftBox className="black">{formatNumber(this.state.totalWeight, 0)}</HeaderTextLeftBox>
                </Col>
              </Row>
            )}
          </Col>
          <Col span={8} style={{ paddingRight: (currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && this.state.totalTax > 0) ? 16 : 6 }}>
            {type === 'invoice' && this.state.totalTax > 0 && (
              <>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={8}></Col>
                <Col span={7}>
                  <HeaderTextLeftBox className="normal black">SUBTOTAL</HeaderTextLeftBox>
                </Col>
                <Col span={9} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">${formatNumber(this.state.totalSubtotal, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={8}></Col>
                <Col span={7}>
                  <HeaderTextLeftBox className="normal black">TAX</HeaderTextLeftBox>
                </Col>
                <Col span={9} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">${formatNumber(this.state.totalTax, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
              </>
              )}
            {type === 'invoice' && (
              <Row gutter={24}>
                <Col span={8}></Col>
                <Col span={7}>
                  <HeaderTextLeftBox className="normal black">TOTAL</HeaderTextLeftBox>
                </Col>
                <Col span={9} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">${formatNumber(this.state.totalAmount, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
            )}
          </Col>
        </Row>
      </>
    )

    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'
    company = company
      ? company
      : currentOrder.wholesaleClient && currentOrder.wholesaleClient.wholesaleCompany
        ? currentOrder.wholesaleClient.wholesaleCompany
        : null

    let bUseSelectFreshInvoice = false;
    let bUseBelenTradingInvoice = false;

    const companyName = this.props.companyName;

    //Get Invoice Template
    const customInvoiceTemplates = _.get(companyProductTypes, 'customInvoiceTemplate', [])
    let customInvoiceTemplateName = '';

    if(customInvoiceTemplates && customInvoiceTemplates.length === 1){
      customInvoiceTemplateName = customInvoiceTemplates[0].name
    }

    if(customInvoiceTemplateName != '' && this.props.overrideCustomLayout) {
        customInvoiceTemplateName = ''
    }

    const isSoldTo = (type == 'bill' && !pagePrintSetting) || (type == 'invoice' && !pagePrintSetting)
    const isShipTo = (type == 'bill' && !pagePrintSetting) || (type == 'invoice' && !pagePrintSetting)

    const soldToCol = soldto ? (
      <Col span={isSoldTo ? 7 : 14}>
        <div style={{ width: "250px" }}>
          <h6 style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "bold", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>Sold to:</h6>
          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{clientName}</p>
          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px", whiteSpace: 'pre-line' }}>
            {currentOrder.billingAddress ? formatAddress(currentOrder.billingAddress.address, true).trim() : 'N/A'}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
              ? currentOrder.wholesaleClient.mainBillingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    ) : (
      <></>
    )

    const shipToCol = shipto ? (
      <Col span={isShipTo ? 7 : 14}>
        <div className="f12">
          <h6
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'bold',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {fulfillmentOptionType == 2 ? 'Pickup Address' : 'Ship to'}:{' '}
          </h6>
          {
            fulfillmentOptionType != 2 &&
            <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{clientName}</p>
          }
          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px", whiteSpace: 'pre-line' }}>
            {fulfillmentOptionType == 2 ? (
              <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
            ) : (
              <>
                {currentOrder.shippingAddress
                  ? formatAddress(currentOrder.shippingAddress.address, true).trim()
                  : 'N/A'}
              </>
            )}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    ) : (
      <></>
    )

    const logisticCols = (
      <>
        {currentOrder && currentOrder.fulfillmentType === 3 && (
          <>
            {carrierSetting && (
              <div className="f12" style={{width: "250px"}}>
                <DisplayLeftLabel>Carrier </DisplayLeftLabel>
                <DisplayValue>{currentOrder.carrier}</DisplayValue>
                <DisplayValue>{currentOrder.carrierReferenceNo}</DisplayValue>
              </div>
            )}
            {origin && (
              <div className="f12" style={{width: "250px"}}>
                <DisplayLeftLabel>Origin </DisplayLeftLabel>
                <DisplayValue>{formatPrintOriginAddress(currentOrder.originAddress)[0]}</DisplayValue>
                <DisplayValue>{formatPrintOriginAddress(currentOrder.originAddress)[1]}</DisplayValue>
              </div>
            )}
          </>
        )}
        <div className="f12" style={{width: "250px"}}>
          <DisplayLeftLabel>Destination </DisplayLeftLabel>
          <DisplayValue>{clientName}</DisplayValue>
          <DisplayValue style={{ whiteSpace: 'pre-wrap' }}>
            {currentOrder.shippingAddress ? formatAddress(currentOrder.shippingAddress.address, true).trim() : 'N/A'}
          </DisplayValue>
          <DisplayValue>
            {currentOrder.wholesaleClient.mainShippingAddress &&
              currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </DisplayValue>
        </div>
      </>
    )

    let phone = ''
    if (company) {
      phone = (company.mainBillingAddress &&
        company.mainBillingAddress.phone &&
        company.mainBillingAddress.phone !== 'null'
        ? company.mainBillingAddress.phone
        : company.phone
          ? company.phone
          : ''
      ).replace('+1-', '')
    }

    let logo: string = this.props.logo
    if (logo && logo !== 'default') {
      if (!location.origin.includes('www')) {
        if (!location.origin.includes('www')) {
        logo = logo.replace('www.', '')
      }
      }
    }

    const RenderInfo = () => {
      if (this.props.currentPrintContainerId) {
        const { containerData } = this.state
        const totalGrossVolume = _.get(this.state, 'containerData.wholesaleOrderList', []).map(v => _.reduce(
          _.get(v, 'wholesaleOrderItemList', []),
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
          0,
        ))
        const totalGrossWeight = _.get(this.state, 'containerData.wholesaleOrderList', []).map(v => _.reduce(
          _.get(v, 'wholesaleOrderItemList', []),
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
          0,
        ))
        return (
          <Col span={11} className="f12" /*style={{ position: 'absolute', right: 10, top: 10 }}*/>
            <Flex >
              <div style={{ marginRight: 15 }}>
                <Flex>
                  <InfoLabel>Fulfillment Date: </InfoLabel>
                  <InfoValue>
                    {currentOrder.deliveryDate}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>PCFN: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'pcfn', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Booking: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'bookingNo', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Net/Gross LBs: </InfoLabel>
                  <InfoValue>
                    {formatNumber(_.sum(totalGrossWeight), 1)}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Net/Gross KG: </InfoLabel>
                  <InfoValue>
                    {formatNumber(_.divide(_.sum(totalGrossWeight), 2.2046), 2)}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Tare Weight LBs: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'tareWeight', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Vessel: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'logisticOrderName', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container #: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'containerName', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Seal#: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'sealNo', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>SCAC: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'scac', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Customer PO#: </InfoLabel>
                  <InfoValue>
                    {currentOrder.reference}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Customer: </InfoLabel>
                  <InfoValue>
                    {currentOrder.wholesaleClient.abbreviation}
                  </InfoValue>
                </Flex>
              </div>
              <div>
              <Flex>
                  <InfoLabel>DODAAC: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'dodaac', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container Type: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'containerType', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Temp Recorder #: </InfoLabel>
                  <InfoValue>
                    {containerData?.tempRecorderList ? _.get(containerData, 'tempRecorderList', []).map((v, i) => `${v.no} #${i + 1}`).join(',') : ''}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Temp (Range): </InfoLabel>
                  <InfoValue>
                    {containerData?.tempRecorderList ? _.get(containerData, 'tempRecorderList', []).map(v => `${v.temp}°F`).join(', ') : ''}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Contractual RDD: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'contractualRdd', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container TCN: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'tcn', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Payment Terms: </InfoLabel>
                  <InfoValue>
                    {currentOrder.financialTerms}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>POE: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'poe', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>POD: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'pod', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel># of Containers: </InfoLabel>
                  <InfoValue>

                  </InfoValue>
                </Flex>
                {this.props.orderId && <Flex>
                  <InfoLabel>Due Date: </InfoLabel>
                  <InfoValue>
                    {moment(currentOrder.deliveryDate)
                      .add(
                        _.get(
                          paymentTerms.find((v) => v.name === currentOrder.financialTerms),
                          'dueDays',
                          0,
                        ),
                        'days',
                      ).format('MM/DD/YY')}
                  </InfoValue>
                </Flex>}
              </div>
            </Flex>
          </Col>
        )
      }
      return (
        <Col span={11} className="f12">
          {!pagePrintSetting ||
            (pagePrintSetting && pagePrintSetting.so && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>SALES ORDER#: </DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.wholesaleOrderId}
                </DisplayValue>
              </Row>
            ))}
          {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.num)) && (
            <Row style={{ display: 'flex' }}>
              <DisplayLabel>Invoice #: </DisplayLabel>
              <DisplayValue span={13} style={pl24}>
                {getOrderPrefix(sellerSetting, 'sales')}
                {isCustomOrderNumberEnabled(sellerSetting) && currentOrder.customOrderNo
                  ? currentOrder.customOrderNo
                  : currentOrder.wholesaleOrderId}
              </DisplayValue>
            </Row>
          )}
          {(!pagePrintSetting ||
            (pagePrintSetting && (pagePrintSetting.po || typeof pagePrintSetting.po === 'undefined'))) &&
            currentOrder.reference && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Customer PO #:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.reference}
                </DisplayValue>
              </Row>
            )}
          {/* {typeof title === 'undefined' && ( */}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.paymentTerm || typeof pagePrintSetting.paymentTerm === 'undefined'))) &&
            (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Payment term:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.financialTerms
                    ? currentOrder.financialTerms
                    : currentOrder.wholesaleClient.paymentTerm}
                </DisplayValue>
              </Row>
            )}
          {_.get(pagePrintSetting, 'paymentTermDueDate', false) &&
            localStorage.getItem('nsLinked') === 'null' &&
            (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Due date:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {moment(currentOrder.deliveryDate)
                    .add(
                      _.get(
                        paymentTerms.find((v) => v.name === currentOrder.financialTerms),
                        'dueDays',
                        0,
                      ),
                      'days',
                    )
                    .format('MM/DD/YY')}
                </DisplayValue>
              </Row>
            )}
          {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
            <>
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Pickup Time:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.warehousePickupTime}
                </DisplayValue>
              </Row>
            </>
          ) : (
            <>
              {!pagePrintSetting ||
                (pagePrintSetting && pagePrintSetting.window && (
                  <Row style={{ display: 'flex' }}>
                    <DisplayLabel>{fulfillmentOptionType === 2 ? 'Pickup Time' : 'Window'}:</DisplayLabel>
                    <DisplayValue span={13} style={pl24}>
                      {currentOrder.shippingAddress && currentOrder.shippingAddress.deliveryWindows
                        ? currentOrder.shippingAddress.deliveryWindows
                        : 'N/A'}
                    </DisplayValue>
                  </Row>
                ))}
            </>
          )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.numberOfPallets || typeof pagePrintSetting.numberOfPallets === 'undefined'))) &&
            currentOrder.numberOfPallets && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>No. of pallets:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.numberOfPallets}
                </DisplayValue>
              </Row>
            )}
          {/* <HeaderTextLeftBox>&nbsp;</HeaderTextLeftBox> */}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.orderDate || typeof pagePrintSetting.orderDate === 'undefined'))) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Order Date:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  <Moment format="MM/DD/YYYY" date={currentOrder.orderDate} />
                </DisplayValue>
              </Row>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.deliveryDate || typeof pagePrintSetting.deliveryDate === 'undefined'))) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>
                  {pagePrintSetting && pagePrintSetting.fulfillmentLabel
                    ? pagePrintSetting.fulfillmentLabel
                    : 'Fulfillment Date'}
                  :
                </DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  <Moment format="MM/DD/YYYY" date={currentOrder.deliveryDate} />
                </DisplayValue>
              </Row>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.salesRepresentative ||
                typeof pagePrintSetting.salesRepresentative === 'undefined'))) &&
            currentOrder.seller && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Sales Rep:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.seller ? `${currentOrder.seller.firstName} ${currentOrder.seller.lastName}` : 'N/A'}
                </DisplayValue>
              </Row>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.pickupReferenceNo || typeof pagePrintSetting.pickupReferenceNo === 'undefined'))) &&
            currentOrder.pickupReferenceNo &&
            fulfillmentOptionType != 1 && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Pickup Reference:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.pickupReferenceNo}
                </DisplayValue>
              </Row>
            )}
          {fulfillmentOptionType === 3 && (
            <Row style={{ display: 'flex' }}>
              <DisplayLabel>Shipping Terms:</DisplayLabel>
              <DisplayValue span={13} style={pl24}>
                {currentOrder.shippingTerm ? currentOrder.shippingTerm : ''}
              </DisplayValue>
            </Row>
          )}
          {/* {typeof title === 'undefined' && ( */}
          {(!pagePrintSetting || (pagePrintSetting && pagePrintSetting.driver)) && fulfillmentOptionType != 3 && (
            <Row style={{ display: 'flex' }}>
              <DisplayLabel>Delivery Driver:</DisplayLabel>
              <DisplayValue span={13} style={pl24}>
                {fulfillmentOptionType == 2
                  ? 'Customer Pickup'
                  : currentOrder.overrideDriver
                    ? `${currentOrder.overrideDriver.firstName} ${currentOrder.overrideDriver.lastName}`
                    : currentOrder.defaultDriver
                      ? `${currentOrder.defaultDriver.firstName} ${currentOrder.defaultDriver.lastName}`
                      : 'N/A'}
              </DisplayValue>
            </Row>
          )}
        </Col>
      )
    }

    const RenderTable = () => {
      const { currentOrder, sellerSetting } = this.props
      const { pagePrintSetting } = this.state
      const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'

      if (this.props.currentPrintContainerId) {
        const itemList = [..._.flattenDeep(_.get(this.state, 'containerData.wholesaleOrderList', []).map(v => v.wholesaleOrderItemList)), ...this.props.oneOffItemList]

        const totalUnit = _.reduce(
          itemList,
          (sum, n) => sum + n.picked,
          0,
        )
        const totalPrice = _.reduce(
          itemList,
          (sum, n) => sum + (_.multiply(n.catchWeightQty, basePriceToRatioPrice(n.UOM, n.price, n))),
          0,
        )
        const totalGrossWeight = _.reduce(
          itemList,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
          0,
        )
        const totalGrossVolume = _.reduce(
          itemList,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
          0,
        )
        return (
          <div style={{ marginBottom: 30, marginTop: 10}} /*marginTop: 110 */>
            <HeaderTextLeftBox style={{ color: 'black' }}>
              {this.props.orderId ? <>
                {clientName} | #{getOrderPrefix(sellerSetting, 'sales')}
                {currentOrder.wholesaleOrderId}
              </>: _.get(this.state.containerData, 'containerName', '')}
            </HeaderTextLeftBox>
            {_.get(this.state, 'containerData.wholesaleOrderList', []).map((v, index) => {
              const tableOrderItemsList = v.wholesaleOrderItemList.map(l => ({ ...l, displayOrderProduct: ~~l.displayOrder }))
              const groupByDisplayOrder = _.groupBy(tableOrderItemsList, 'displayOrderProduct')
              const items = Object.keys(groupByDisplayOrder).map(g => ({
                ...groupByDisplayOrder[g][0],
                quantity: _.reduce(
                  groupByDisplayOrder[g],
                  (sum, n) => sum + n.quantity,
                  0,
                ),
                picked: _.reduce(
                  groupByDisplayOrder[g],
                  (sum, n) => sum + n.picked,
                  0,
                ),
                catchWeightQty: _.reduce(
                  groupByDisplayOrder[g],
                  (sum, n) => sum + n.catchWeightQty,
                  0,
                )
              }))
              return (
                <div key={index} style={{ marginTop: 20 }}>
                  {!this.props.orderId && <p style={{ color: '#000' }}>
                    <span style={{ marginRight: 300, }}>
                      SHIP TO: {v.customerName}
                    </span>
                    ORDER NO. {getOrderPrefix(sellerSetting, 'sales')}{v.wholesaleOrderId}
                  </p>}
                  <PrintContainerInvoice tableClass={tableClass} items={this.props.orderId ? [..._.sortBy(items, 'displayOrderProduct'), ...this.props.oneOffItemList] : _.sortBy(items, 'displayOrderProduct')} />
                </div>
              )
            })}
            <Flex style={{ justifyContent: 'space-between', marginTop: 10 }}>
              <div>
                <span>Total Units: </span>
                <span>{totalUnit}</span>
              </div>
              <div>
                <span style={{ marginLeft: 50 }}>Total Summary: </span>
                <span>${formatNumber(totalPrice, 2)}</span>
              </div>
            </Flex>
            <div>Total Gross Weight: <span style={{ fontWeight: 'bold' }}>{formatNumber(totalGrossWeight, 1)} LBs / {_.divide(totalGrossWeight, 2.2046).toFixed(2)} KG</span></div>
            <div>Total Gross Volume: <span style={{ fontWeight: 'bold' }}>{formatNumber(totalGrossVolume, 1)} ft³ / {_.divide(totalGrossVolume, 35.315).toFixed(2)} m³</span></div>
          </div>
        )
      }

      const defaultItems = items.filter((orderItem: any) => {
        const uom = orderItem.UOM ? orderItem.UOM : orderItem.inventoryUOM
        return (uom == orderItem.defaultSellingPricingUOM)
      });
      const looseItems = items.filter((orderItem: any) => {
        const uom = orderItem.UOM ? orderItem.UOM : orderItem.inventoryUOM
        return (uom != orderItem.defaultSellingPricingUOM)
      });

      return (
        <>
          <PrintPickSheetTable style={{ marginTop: 8 }}>
            {/* {totals} */}
            <DeliveryListTable>
              <Table
                className={tableClass}
                bordered={false}
                style={{ width: '100%' }}
                rowKey="wholesaleOrderItemId"
                rowClassName={(record, index) => {
                  let rowOrderItemId = record.wholesaleOrderItemId
                  if (this.checkWeights(record)) {
                    rowOrderItemId = record.relatedOrderItemId
                  }
                  const i = orderItems.findIndex((oi) => oi.wholesaleOrderItemId === rowOrderItemId)
                  return i % 2 === 1 ? 'alternative' : ''
                }}
                columns={this.getDisplayingColumns(columns, false)}
                dataSource={pagePrintSetting && pagePrintSetting.separateLooseItemsTable === true ? defaultItems : items}
                pagination={false}
              />
            </DeliveryListTable>
            <hr />
            {pagePrintSetting && pagePrintSetting.separateLooseItemsTable === true && (
              <div>
                <DeliveryListTable>
                  <HeaderTextLeftBox style={{ color: 'black', marginTop: 15 }}>Loose Items</HeaderTextLeftBox>
                  <Table
                    className={tableClass}
                    bordered={false}
                    style={{ width: '100%' }}
                    rowKey="wholesaleOrderItemId"
                    rowClassName={(record, index) => {
                      let rowOrderItemId = record.wholesaleOrderItemId
                      if (this.checkWeights(record)) {
                        rowOrderItemId = record.relatedOrderItemId
                      }
                      const i = orderItems.findIndex((oi) => oi.wholesaleOrderItemId === rowOrderItemId)
                      return i % 2 === 1 ? 'alternative' : ''
                    }}
                    columns={this.getDisplayingColumns(columns, true)}
                    dataSource={looseItems}
                    pagination={false}
                  />
                </DeliveryListTable>
                <hr />
              </div>
            )}
            {totals}
          </PrintPickSheetTable>
          {type == 'invoice' && <InvoiceCreditMemoSection isFromInvoice={true} summary={this.state.totalAmount} tableClass={tableClass} currentOrder = {this.props.currentOrder} />}
        </>
      )
    }

    if (customInvoiceTemplateName === '')
    {
      return (
        <PrintPickSheetWrapper id="invoiceContent">
          <HeaderBox style={{ fontWeight: 'normal' }}>
            {(this.props.orderId && !pagePrintSetting ||
              (pagePrintSetting && pagePrintSetting.barcode && (
                <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} width={2} height={30} />
              ))}
            <Row style={{ textAlign: 'center' }}>
              {!this.props.currentPrintContainerId ? <h1 style={{ marginBottom: 0 }}>
                {pagePrintSetting && pagePrintSetting.title
                  ? pagePrintSetting.title.toUpperCase()
                  : type == 'invoice'
                    ? 'INVOICE'
                    : 'BILL OF LADING'}{' '}
                {this.props.isCopy === true ? '(OFFICE COPY)' : ''}
              </h1>: <h1 style={{ marginBottom: 0 }}>Invoice # {this.props.orderId} - {_.get(this.state.containerData, 'containerName', '')}</h1>}
            </Row>
            <Row>
              <Col span={11}>
                <CompanyInfoWrapper>
                  <div style={{ textAlign: 'left' }}>
                    {!pagePrintSetting ||
                      (pagePrintSetting && pagePrintSetting.logo && (
                        <div style={{ marginRight: 10 }}>
                          {logo === 'default' ? (
                            <LogoWrapper style={{ textAlign: 'left' }}>
                              <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                            </LogoWrapper>
                          ) : (
                            <img src={logo} style={{ maxWidth: 150 }} onLoad={this.onImageLoad} />
                          )}
                        </div>
                      ))}
                    {company && (
                      <div className="left">
                        {company.companyName !== null && (
                          <HeaderTextLeftBox className="f18">{company.companyName}</HeaderTextLeftBox>
                        )}
                        {(!pagePrintSetting ||
                          (pagePrintSetting &&
                            (pagePrintSetting.submitPaymentWord ||
                              typeof pagePrintSetting.submitPaymentWord === 'undefined'))) && (
                            <p
                              style={{
                                fontSize: '14px',
                                fontFamily: 'Inter',
                                color: 'black',
                                fontWeight: 'bold',
                                lineHeight: '19.6px',
                                textAlign: 'left',
                                marginBottom: '0px',
                              }}
                            >
                              Please submit payment to:
                            </p>
                          )}
                        {company.email !== null && (
                          <p
                            style={{
                              fontSize: '14px',
                              fontFamily: 'Inter',
                              color: 'black',
                              fontWeight: 'normal',
                              lineHeight: '19.6px',
                              textAlign: 'left',
                              marginBottom: '0px',
                            }}
                          >
                            {company.email}
                          </p>
                        )}
                        {company.mainBillingAddress !== null && (
                          <p
                            style={{
                              fontSize: '14px',
                              fontFamily: 'Inter',
                              color: 'black',
                              fontWeight: 'normal',
                              lineHeight: '19.6px',
                              textAlign: 'left',
                              marginBottom: '0px',
                            }}
                          >
                            {formatAddress(company.mainBillingAddress.address, false)}
                          </p>
                        )}
                        {(phone || company.fax) && (
                          <p
                            style={{
                              fontSize: '14px',
                              fontFamily: 'Inter',
                              color: 'black',
                              fontWeight: 'normal',
                              lineHeight: '19.6px',
                              textAlign: 'left',
                              marginBottom: '0px',
                            }}
                          >
                            Phone: {company.phone}{company.fax ? ` / Fax: ${company.fax}` : ''}
                          </p>
                        )}
                      </div>
                    )}
                  </div>
                </CompanyInfoWrapper>
                {fulfillmentOptionType != 3 ? (
                  <Row style={{ marginTop: 8 }} className="f12">
                    {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
                      <>
                        <Col span={7}>
                          <div>
                            <h6
                              style={{
                                fontSize: '14px',
                                fontFamily: 'Inter',
                                color: 'black',
                                fontWeight: 'bold',
                                lineHeight: '19.6px',
                                textAlign: 'left',
                                marginBottom: '0px',
                                width: '250px',
                              }}
                            >
                              PICKUP ADDRESS
                            </h6>
                            <p
                              style={{
                                fontSize: '14px',
                                fontFamily: 'Inter',
                                color: 'black',
                                fontWeight: 'normal',
                                lineHeight: '19.6px',
                                textAlign: 'left',
                                marginBottom: '0px',
                                width: '250px',
                              }}
                            >
                              {clientName}
                            </p>
                            <p
                              style={{
                                fontSize: '14px',
                                fontFamily: 'Inter',
                                color: 'black',
                                fontWeight: 'normal',
                                lineHeight: '19.6px',
                                textAlign: 'left',
                                marginBottom: '0px',
                                width: '250px',
                              }}
                            >
                              {currentOrder.pickupAddress}
                            </p>
                          </div>
                        </Col>
                        {soldToCol}
                      </>
                    ) : (
                      <div style={{ display: 'flex' }}/*, width: '50%'*/>
                        {soldToCol}
                        {shipToCol}
                      </div>
                    )}
                  </Row>
                ) : (
                  <div style={{display: 'flex'}}>
                    {logisticCols}
                  </div>
                )}
              </Col>

              <RenderInfo />
            </Row>


            {!this.props.currentPrintContainerId && <>
              {currentOrder.wholesaleClient &&
                currentOrder.wholesaleClient.mainContact &&
                (!pagePrintSetting ||
                  (pagePrintSetting &&
                    (pagePrintSetting.deliveryContact || typeof pagePrintSetting.deliveryContact === 'undefined'))) && (
                  <Row
                    style={{
                      marginTop: 20,
                      borderBottom: '1px solid #aaa',
                      paddingBottom: 5,
                      marginBottom: 5,
                    }}
                    className="f12"
                  >
                    <Col span={3}>
                      <HeaderTextLeftBox className="normal">
                        {fulfillmentOptionType == 2 ? 'Pickup Contact' : 'Delivery Contact'}
                      </HeaderTextLeftBox>
                    </Col>
                    <Col span={6}>
                      <div style={{ paddingLeft: 15 }}>
                        {fulfillmentOptionType == 2
                          ? currentOrder.pickupContactName
                          : currentOrder.deliveryContact
                            ? currentOrder.deliveryContact
                            : currentOrder.wholesaleClient.mainContact
                              ? currentOrder.wholesaleClient.mainContact.name
                              : ''}
                      </div>
                    </Col>
                    {fulfillmentOptionType == 1 && (
                      <Col span={6}>
                        {currentOrder.deliveryPhone
                          ? currentOrder.deliveryPhone
                          : currentOrder.wholesaleClient.mainContact
                            ? currentOrder.wholesaleClient.mainContact.mobilePhone
                            : ''}
                      </Col>
                    )}
                  </Row>
                )}
              {type == 'invoice' &&
                (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.driver_payment_collection)) && (
                  <Row style={{ padding: '10px 0' }} className="f12">
                    <Col span={7}>
                      <Flex>
                        <HeaderTextLeftBox className="normal">CASH RECEIVED $</HeaderTextLeftBox>
                        <Underline>{this.state.totalQuantity}</Underline>
                      </Flex>
                    </Col>

                    <Col span={7} offset={1}>
                      <Flex>
                        <HeaderTextLeftBox className="normal">CHECK RECEIVED $</HeaderTextLeftBox>
                        <Underline>{this.state.totalQuantity}</Underline>
                      </Flex>
                    </Col>
                    <Col span={7} offset={1}>
                      <Flex>
                        <HeaderTextLeftBox className="normal">CHECK #</HeaderTextLeftBox>
                        <Underline>{this.state.totalQuantity}</Underline>
                      </Flex>
                    </Col>
                  </Row>
                )}
              {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.account_balance_forward)) && (
                <>
                  <LightColoredRow style={{ padding: '4px 0px' }} className="f12">
                    <Col span={4}>DATE</Col>
                    <Col span={16}>ACCOUNT SUMMARY</Col>
                    <Col span={4} style={{ textAlign: 'right' }}>
                      AMOUNT
                    </Col>
                  </LightColoredRow>
                  <Row style={{ padding: '4px 0px' }} className="f12">
                    <Col span={4}>01/12/2015</Col>
                    <Col span={16}>
                      <div>Balance Forward</div>
                      <div>New charges(details below)</div>
                      <div>Total Amount Due</div>
                    </Col>
                    <Col span={4} style={{ textAlign: 'right' }}>
                      <div>100</div>
                      <div>100</div>
                      <div>100</div>
                    </Col>
                  </Row>
                </>
              )}
              {currentOrder.customerNote && (
                <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
                  <Col span={3}>
                    <HeaderTextLeftBox>Notes to Customer</HeaderTextLeftBox>
                  </Col>
                  <Col span={6}>
                    <div style={{ paddingLeft: 15 }}>{currentOrder.customerNote}</div>
                  </Col>
                </Row>
              )}
              {currentOrder.deliveryInstruction && (
                <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
                  <Col span={24}>
                    <HeaderTextLeftBox>
                      {fulfillmentOptionType === 1 || fulfillmentOptionType === 2 ? 'Fulfillment ' : 'Delivery '}
                      Instructions
                    </HeaderTextLeftBox>
                    <div>{currentOrder.deliveryInstruction}</div>
                  </Col>
                </Row>
              )}
            </>}
          </HeaderBox>

          {RenderTable()}
          {/* <UnderLineWrapper>
            <div>Warehouse Verification (Signature):</div>
          </UnderLineWrapper> */}
          <div className="floating-to-bottom">
            {(!pagePrintSetting ||
              (pagePrintSetting &&
                (pagePrintSetting.verificationSignature ||
                  typeof pagePrintSetting.verificationSignature === 'undefined'))) && (
              <Flex>
                <div style={{ color: 'black' }}>Warehouse Verification (Signature):</div>
                <Underline />
              </Flex>
            )}
            {/* <ModalDetailText>
            {clientName === 'TBD'
              ? 'The perishable agricultural commodities listed on this invoice are sold subject to the statutory trust authorized by section 5(c) of the Perishable Agricultural Commodities Act, 1930 (7 U.S.C. 499e(c)). The seller of these commodities retains a trust claim over these commodities, all inventories of food or other products derived from these commodities, and any receivables or proceeds from the sale of these commodities until full payment is received.'
              : 'All Claim for damaged or shortage of goods should be made within 24 hrs upon receipt of merchandise/goods. The perishable commodities listed on this invoice are sold subject to the statutory trust authorized by section 5c of the Perishable Agricultural Commodities Act, 1930 (7 U.S. C499E c). Friends Development (USA), Inc. retains a trust claim over these said commodities, all inventories or other products derived from these commodities and any receivables or proceeds from the sales of these commodities until full payment is received (Net 15 Days).'}
            <br />
            <b>溫馨提醒: 請客戶務必當面點清貨品數量後再簽收, 過後發現丟失, 本公司概不負責! 敬請見諒!</b>
          </ModalDetailText> */}
            {pagePrintSetting && pagePrintSetting.customMsg && (
              <ModalDetailText className="f18" style={{ fontWeight: 'bold', marginTop: 10, marginBottom: 0, paddingBottom: 10 }}>
                {pagePrintSetting.customMsg}
              </ModalDetailText>
            )}
            {pagePrintSetting && pagePrintSetting.terms && (
              <ModalDetailText className="f10" style={{ paddingBottom: 0, marginBottom: 0 }}>
                {pagePrintSetting.terms}
              </ModalDetailText>
            )}

            <Flex style={{ marginTop: 20 }}>
              <div style={{ color: 'black' }}>Customer Print Name:</div>
              <Underline />
              <div style={{ color: 'black' }}>Signature:</div>
              <Underline />
            </Flex>
            <div className="freight-carrier-section" style={{ display: type == 'invoice' ? 'none' : 'block' }}>
              <Row>
                <Col span={12}>
                  {(!pagePrintSetting ||
                    (pagePrintSetting &&
                      (pagePrintSetting.carrier || typeof pagePrintSetting.carrier === 'undefined'))) && (
                    <Row>
                      <Col span={8}>Carrier: </Col>
                      <Col offset={2} span={14}>
                        {currentOrder.carrier ? currentOrder.carrier : ''}
                      </Col>
                    </Row>
                  )}
                  {(!pagePrintSetting ||
                    (pagePrintSetting &&
                      (pagePrintSetting.carrierPhoneNo || typeof pagePrintSetting.carrierPhoneNo === 'undefined'))) && (
                    <Row>
                      <Col span={8}>Carrier Phone No.: </Col>
                      <Col offset={2} span={14}>
                        {currentOrder.carrierPhoneNo ? currentOrder.carrierPhoneNo : ''}
                      </Col>
                    </Row>
                  )}
                  {(!pagePrintSetting ||
                    (pagePrintSetting &&
                      (pagePrintSetting.driverName || typeof pagePrintSetting.driverName === 'undefined'))) && (
                    <Row>
                      <Col span={8}>Driver Name: </Col>
                      <Col offset={2} span={14}>
                        {currentOrder.driverName ? currentOrder.driverName : ''}
                      </Col>
                    </Row>
                  )}
                  {(!pagePrintSetting ||
                    (pagePrintSetting &&
                      (pagePrintSetting.driverLicense || typeof pagePrintSetting.driverLicense === 'undefined'))) && (
                    <Row>
                      <Col span={8}>Driver License No.: </Col>
                      <Col offset={2} span={14}>
                        {currentOrder.driverLicense ? currentOrder.driverLicense : ''}
                      </Col>
                    </Row>
                  )}
                </Col>
                {(!pagePrintSetting ||
                  (pagePrintSetting &&
                    (pagePrintSetting.driverLicenseImageField ||
                      typeof pagePrintSetting.driverLicenseImageField === 'undefined'))) && (
                  <Col span={12}>
                    <div style={{ color: 'black' }}>Driver License Image:</div>
                    <div style={{ width: '7.9375cm', height: '5.3975cm' }}></div>
                  </Col>
                )}
              </Row>
              {(!pagePrintSetting ||
                (pagePrintSetting &&
                  (pagePrintSetting.driverSignatureField ||
                    typeof pagePrintSetting.driverSignatureField === 'undefined'))) && (
                <Flex style={{ marginTop: 20 }}>
                  <div style={{ color: 'black' }}>Driver Signature:</div>
                  <Underline />
                </Flex>
              )}
              {pagePrintSetting &&
                (pagePrintSetting.carrierTerms || typeof pagePrintSetting.carrierTerms === 'undefined') &&
                pagePrintSetting.carrierTermsText && (
                  <ModalDetailText className="f10" style={{ marginTop: 20 }}>
                    {pagePrintSetting.carrierTermsText}
                  </ModalDetailText>
                )}
            </div>

            {(!pagePrintSetting ||
              (pagePrintSetting && (pagePrintSetting.barcode || pagePrintSetting.printTime || pagePrintSetting.so))) && (
              <div style={{ marginTop: 20 }}>
                <div>
                  {!pagePrintSetting || (pagePrintSetting && pagePrintSetting.barcode) ? (
                    <Barcode
                      value={currentOrder.wholesaleOrderId.toString()}
                      displayValue={false}
                      width={2}
                      height={30}
                    />
                  ) : (
                    <div>&nbsp;</div>
                  )}
                </div>
                <Flex className="v-center between" style={{ fontSize: 16, paddingTop: 5 }}>
                  <div>
                    {!pagePrintSetting ||
                      (pagePrintSetting && pagePrintSetting.printTime && (
                        <HeaderTextRightBox style={{ letterSpacing: 0, color: 'black' }}>
                          Printed {moment().format('MM/DD/YY hh:mm A')}
                        </HeaderTextRightBox>
                      ))}
                  </div>
                  <div>{!pagePrintSetting || (pagePrintSetting && pagePrintSetting.so && this.getNameAndOrderNo())}</div>
                </Flex>
              </div>
            )}
          </div>
        </PrintPickSheetWrapper>
      )
    }
    else if(customInvoiceTemplateName === 'Belen Trading')
    {
      const BelenTradingCompanyAddress = (
        <div>
          {company.companyName !== null && (
            <BelenTradingDisplayLeftLabel className="f16">{company.companyName}</BelenTradingDisplayLeftLabel>
          )}
          {company.mainBillingAddress !== null && (
            <BelenTradingDisplayLeftValue>
              {currentOrder.billingAddress ? formatAddress(company.mainBillingAddress.address, true).trim() : 'N/A'}
            </BelenTradingDisplayLeftValue>
          )}
          {(company.phone || company.fax) && (
            <BelenTradingDisplayLeftValue>
              Phone: {company.phone}{company.fax ? ` / Fax: ${company.fax}` : ''}
            </BelenTradingDisplayLeftValue>
          )}
          {company.email !== null && (
            <BelenTradingDisplayLeftValue>
              {company.email}
            </BelenTradingDisplayLeftValue>
          )}
        </div>
      )

      const BelenTradingBillToCol = (
        <div>
          <BelenTradingDisplayLeftLabel>BILL TO</BelenTradingDisplayLeftLabel>
          <BelenTradingDisplayLeftValue>{clientName}</BelenTradingDisplayLeftValue>
          <BelenTradingDisplayLeftValue>
            {currentOrder.billingAddress ? formatAddress(currentOrder.billingAddress.address, true).trim() : 'N/A'}
          </BelenTradingDisplayLeftValue>
          <BelenTradingDisplayLeftValue>
            {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
              ? currentOrder.wholesaleClient.mainBillingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </BelenTradingDisplayLeftValue>
        </div>
      )

      const BelenTradingShipToCol = (
        <div>
          <BelenTradingDisplayLeftLabel>{fulfillmentOptionType == 2 ? 'PICKUP ADDRESS' : 'SHIP TO'}</BelenTradingDisplayLeftLabel>
          {fulfillmentOptionType != 2 &&
            <BelenTradingDisplayLeftValue>{clientName}</BelenTradingDisplayLeftValue>
          }
          <BelenTradingDisplayLeftValue>
            {fulfillmentOptionType == 2 ? (
              <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
            ) : (
              <>
                {currentOrder.shippingAddress
                  ? formatAddress(currentOrder.shippingAddress.address, true).trim()
                  : 'N/A'}
              </>
            )}
          </BelenTradingDisplayLeftValue>
          <BelenTradingDisplayLeftValue>
            {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </BelenTradingDisplayLeftValue>
        </div>
      )

      const BelenTradingInvoiceCol = (
        <div>
          <Row>
            <Col span={24}>
              <BelenTradingDisplayGreenBoxLabel className="green-box f18">INVOICE {currentOrder.wholesaleOrderId}</BelenTradingDisplayGreenBoxLabel>
            </Col>
          </Row>
          <Row style={{marginTop: 20}}>
            <Col span={10}>
              <Flex className="green-box">
                <BelenTradingDisplayGreenBoxLabel>DATE</BelenTradingDisplayGreenBoxLabel>
                <BelenTradingDisplayGreenBoxValue>{currentOrder.createdDate ? moment(currentOrder.createdDate).format("MM/DD/YYYY") : '' }</BelenTradingDisplayGreenBoxValue>
              </Flex>
            </Col>
            <Col span={14}>
              <Flex className="green-box">
                <BelenTradingDisplayGreenBoxLabel>TERMS</BelenTradingDisplayGreenBoxLabel>
                <BelenTradingDisplayGreenBoxValue style={{paddingRight: 5}}>
                  {(!pagePrintSetting || (pagePrintSetting && (pagePrintSetting.paymentTerm || typeof pagePrintSetting.paymentTerm === 'undefined'))) &&
                    (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
                      <span>{currentOrder.financialTerms ? currentOrder.financialTerms : currentOrder.wholesaleClient.paymentTerm}</span>
                    )}
                </BelenTradingDisplayGreenBoxValue>
              </Flex>
            </Col>
          </Row>
          <Row style={{marginTop: 20}}>
            <Col span={24}>
              <Flex className="green-box">
                <BelenTradingDisplayGreenBoxLabel>DUE DATE</BelenTradingDisplayGreenBoxLabel>
                <BelenTradingDisplayGreenBoxValue>{moment(currentOrder.deliveryDate)
                  .add(
                    _.get(
                      paymentTerms.find((v) => v.name === currentOrder.financialTerms),
                      'dueDays',
                      0,
                    ),
                    'days',
                  ).format('MM/DD/YY')}</BelenTradingDisplayGreenBoxValue>
              </Flex>
            </Col>
          </Row>
        </div>
      )

      const BelenTradingRenderTable = () => {
        const { currentOrder, sellerSetting } = this.props
        const clientName =
          currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
            ? currentOrder.wholesaleClient.clientCompany.companyName
            : 'N/A'

        return (
          <>
            <Row>
              <Col span={24}>
                <BelenTradingPrintContainerInvoice style={{ marginTop: 8 }} tableClass={tableClass} items={items} />
              </Col>
            </Row>

            <Row className="f14" gutter={24}>
              <Col span={12} offset={12}>
                {type === 'invoice' && this.state.totalTax > 0 && (
                  <>
                    <Row gutter={24} style={{ marginBottom: 10 }}>
                      <Col span={7} offset={8}>
                        <BelenTradingDisplayLeftLabel>SUBTOTAL</BelenTradingDisplayLeftLabel>
                      </Col>
                      <Col span={9}>
                        <BelenTradingDisplayRightValue style={{paddingRight: "20px"}} className="f18">${formatNumber(this.state.totalSubtotal, 2)}</BelenTradingDisplayRightValue>
                      </Col>
                    </Row>
                    <Row gutter={24} style={{ marginBottom: 10 }}>
                      <Col span={7} offset={8}>
                        <BelenTradingDisplayLeftLabel>TAX</BelenTradingDisplayLeftLabel>
                      </Col>
                      <Col span={9}>
                        <BelenTradingDisplayRightValue style={{paddingRight: "20px"}} className="f18">${formatNumber(this.state.totalTax, 2)}</BelenTradingDisplayRightValue>
                      </Col>
                    </Row>
                  </>
                )}
                {type === 'invoice' && (
                  <Row style={{marginTop: "20px"}}>
                    <Col span={12}>
                      <div className="green-box"><BelenTradingDisplayGreenBoxLabel>TOTAL DUE</BelenTradingDisplayGreenBoxLabel></div>
                    </Col>
                    <Col span={12} style={{textAlign: "right"}}>
                      <div className="green-box"><BelenTradingDisplayGreenBoxValue style={{textAlign: "right", paddingRight: "15px"}} className="f18">${formatNumber(this.state.totalAmount, 2)}</BelenTradingDisplayGreenBoxValue></div>
                    </Col>
                  </Row>
                )}
              </Col>
            </Row>
            {type == 'invoice' && <InvoiceCreditMemoSection summary={this.state.totalAmount} tableClass={tableClass} currentOrder = {this.props.currentOrder} />}
          </>
        )
      }

      return (
        <BelenTradingInvoiceWrapper id="invoiceContent">

          <Row style={{paddingTop: 10}} className="f12">
            <Col span={24} className="padding-left-20">
                <div style={{display: 'flex'}}>
                  {!pagePrintSetting ||
                    (pagePrintSetting && pagePrintSetting.logo && (
                      <div style={{ marginRight: 10 }}>
                        {logo === 'default' ? (
                          <LogoWrapper style={{ textAlign: 'left' }}>
                            <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                          </LogoWrapper>
                        ) : (
                          <img src={logo} style={{ maxWidth: 150 }} onLoad={this.onImageLoad} />
                        )}
                      </div>
                    ))}
                  {company && (
                    <div>
                      {BelenTradingCompanyAddress}
                    </div>
                    )}
                </div>
            </Col>
          </Row>

          <Row type="flex" align="top" style={{ paddingTop: 30 }}>
            <Col span={13}>
              <Row style={{minHeight: "147px", borderBottom: "8px solid #008013" }}>
                <Col span={12} className="padding-left-20">{BelenTradingBillToCol}</Col>
                <Col span={12}>{BelenTradingShipToCol}</Col>
              </Row>
            </Col>
            <Col span={11} style={{paddingLeft: 5 }}>{BelenTradingInvoiceCol}</Col>
          </Row>

          <Row style={{ paddingTop: 20 }}>
            <Col span={6} className="padding-left-20">
              <BelenTradingDisplayLeftLabel style={{marginBottom: "5px"}}>SHIP DATE</BelenTradingDisplayLeftLabel>
              <BelenTradingDisplayLeftValue>{currentOrder.shippingDate ? moment(currentOrder.shippingDate).format("MM/DD/YYYY") : 'N/A'}</BelenTradingDisplayLeftValue>
            </Col>
            <Col span={6}>
              <BelenTradingDisplayLeftLabel style={{marginBottom: "5px"}}>SHIP VIA</BelenTradingDisplayLeftLabel>
              <BelenTradingDisplayLeftValue>{currentOrder.carrier}</BelenTradingDisplayLeftValue>
            </Col>
            <Col span={6}>
              <BelenTradingDisplayLeftLabel style={{marginBottom: "5px"}}>TRACKING NO.</BelenTradingDisplayLeftLabel>
              <BelenTradingDisplayLeftValue>{currentOrder.reference ? 'PO ' + currentOrder.reference : ''}</BelenTradingDisplayLeftValue>
            </Col>
            <Col span={6}>
              <BelenTradingDisplayLeftLabel style={{marginBottom: "5px"}}>PICK UP#</BelenTradingDisplayLeftLabel>
              <BelenTradingDisplayLeftValue>{currentOrder.pickupReferenceNo}</BelenTradingDisplayLeftValue></Col>
          </Row>

          <BelenTradingRenderTable />

          <Row>
            <Col span={24}>
              <Row style={{marginTop: "20px"}}>
                <Col span={24} className="f12">
                  <p>* Weights, Color, Shapes and Sizes are approximate</p>
                  <p>* Claims will be accepted up to 24hrs after receipt of products.</p>
                  <p>
                    The perishable agricultural commodities listed on this invoice are sold subject to the statutory trust
                    authorized by section 5(c) of the Perishable Agricultural Commodities Act, 1930 (7 U.S.C. 499e(c)).  The
                    seller of these commodities retains a trust claim over these commodities, all inventories of food or other
                    products derived from these commodities, and any receivables or proceeds from the sale of these commodities
                    until full payment is received.
                  </p>
                </Col>
              </Row>
              <Row>
                <Col span={24}><div>Please make checks payable to BELEN TRADING</div></Col>
              </Row>
            </Col>
          </Row>
        </BelenTradingInvoiceWrapper>
      )
    }
    else if(customInvoiceTemplateName === 'Select Fresh') {

      //START SELECT FRESH SECTION
      const selectFreshSoldToCol = soldto ? (
          <SelectFreshInvoiceBillTo>
            <>{clientName}<br/>
              {currentOrder.billingAddress ? '' : 'N/A'}
              {currentOrder.billingAddress && currentOrder.billingAddress.address.department ?
                currentOrder.billingAddress.address.department : ''}
              {currentOrder.billingAddress && currentOrder.billingAddress.address.department ?
                (<br/>) : ('')}
              {currentOrder.billingAddress ?
                currentOrder.billingAddress.address.street1.trim() : ''}
              {currentOrder.billingAddress ?
                (<br/>) : ('')}
              {currentOrder.billingAddress ?
                currentOrder.billingAddress.address.city + ' ' + currentOrder.billingAddress.address.zipcode : ''}
              <br/>
              {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
                ? currentOrder.wholesaleClient.mainBillingAddress.phone
                : currentOrder.wholesaleClient.mobilePhone}
            </>
          </SelectFreshInvoiceBillTo>
        ) :
        (
          <></>
        )

      const selectFreshShipToCol = shipto ? (
          <SelectFreshInvoiceShipTo>
            {
              fulfillmentOptionType != 2 &&
              <>{clientName}<br/></>
            }
            {fulfillmentOptionType == 2 ? (
              <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
            ) : (
              <>
                {currentOrder.shippingAddress ? '' : 'N/A'}
                {currentOrder.shippingAddress ?
                  currentOrder.shippingAddress.address.street1.trim() : ''}
                {currentOrder.shippingAddress ?
                  (<br/>) : ('')}
                {currentOrder.shippingAddress ?
                  currentOrder.shippingAddress.address.city + ' ' + currentOrder.shippingAddress.address.zipcode : ''}
              </>
            )}
            <br/>
            {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </SelectFreshInvoiceShipTo>
        ) :
        (
          <SelectFreshInvoiceShipTo></SelectFreshInvoiceShipTo>
        )

      const getSelectFreshPrice = (record: OrderItem) => {
        if (this.checkWeights(record)) {
          return <></>
        }

        let price = record.price

        var uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        price = basePriceToRatioPrice(uom, price, record)
        return (
          <>
            {`${price ? '$' + formatNumber(price, 2) : '$0'}`}
          </>
        )
      }

      const getSelectFreshSubTotal = (record: OrderItem) => {
        // const extended = Number(record.price ? record.price : 0)
        // const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
        // const total = extended * quantity
        // return <>{total ? `$${formatNumber(total, 2)}` : ''}</>

        if (this.checkWeights(record)) {
          return <></>
        }

        let showValue = false
        let total = 0

        if (record.picked || record.catchWeightQty) {
          showValue = true
        }
        let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
        let unitUOM = record.UOM
        let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(record.price), record, 12)

        let constantRatio = judgeConstantRatio(record)

        if (!constantRatio) {
          total = numberMultipy(
            ratioPrice,
            inventoryQtyToRatioQty(pricingUOM, _.toNumber(record.catchWeightQty), record, 12),
          )
        } else {
          let quantity = inventoryQtyToRatioQty(
            pricingUOM,
            ratioQtyToInventoryQty(unitUOM, _.toNumber(record.picked), record, 12),
            record,
            12,
          )
          total = numberMultipy(ratioPrice, quantity)
        }
        //return <div style={{ fontWeight: 500 }}>{showValue && total ? `$${formatNumber(total, 2)}` : ''}</div>
        return <>{showValue && total ? `$${formatNumber(total, 2)}` : ''}</>
      }

      const renderSelectFreshCatchWeightValues = (weights: any[]) => {
        const {type} = this.props
        const {pagePrintSetting} = this.state
        if (weights.length > 0) {
          let result: any[] = []
          let sub: any[] = []
          weights.forEach((el: any, index: number) => {
            if (index > 0 && (index - 10) % 10 == 0) {
              result.push(<WeightWrapper>{sub}</WeightWrapper>)
              sub = []
            }

            const weightEl = <SelectFreshWeightSpan>{el.unitWeight ? el.unitWeight : ''}</SelectFreshWeightSpan>
            if (index == weights.length - 1) {
              sub.push(weightEl)
              result.push(<WeightWrapper>{sub}</WeightWrapper>)
            } else {
              sub.push(weightEl)
            }
          })
          return result
        }

        return []
      }

      const getSelectFreshItemDesc = (record: OrderItem) => {
        if (this.checkWeights(record) && company && company.isDisablePickingStep === false) {
          return renderSelectFreshCatchWeightValues(record.weights)
        }
        if (record.itemId) {
          return record.editedItemName ? record.editedItemName : formatItemDescription(record.variety, record.SKU, sellerSetting, record.customerProductCode)
        }
        const desc = record.chargeDesc ? ` (${record.chargeDesc})` : ''
        return `${record.variety}${desc}`
      }

      const getSelectFreshBillableQty = (record: OrderItem) => {
        if (this.checkWeights(record)) {
          return <></>
        }

        let showBillableQty,
          showValue = false

        if (record.picked || record.catchWeightQty) {
          showValue = true
        }

        let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
        let unitUOM = record.UOM

        let constantRatio = judgeConstantRatio(record)
        if (record.oldCatchWeight) {
          showBillableQty = record.catchWeightQty
        } else {
          if (!constantRatio) {
            showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
          } else {
            showBillableQty = inventoryQtyToRatioQty(
              pricingUOM,
              ratioQtyToInventoryQty(unitUOM, record.picked, record, 12),
              record,
            )
          }
        }

        return showValue ? `${mathRoundFun(showBillableQty, 2)}` : ''
      }


      const selectFreshOrderListItems = (pageNum: any, pageItems: any[]) => {
        let myItems = [];
        //pageNum = pageNum + 1;

        //myItems = items.slice((pageNum - 1) * 20, pageNum * 20);
        //console.log(pageItems[pageNum])
        let strStartStop = pageItems[pageNum];
        let arrStartStop = strStartStop.split(',');
        myItems = items.slice(arrStartStop[0], arrStartStop[1]);

        return myItems.map((itm) =>
          <tr>
            <td width={'27px'} align='center'>{getSelectFreshBillableQty(itm)}</td>
            {/*27*/}
            <td width={'45px'} align='center'>{itm.pricingUOM}</td>
            {/*45*/}
            <td width={'212px'}>{getSelectFreshItemDesc(itm)}</td>
            {/*212*/}
            <td width={'40px'} align='right'>{/*40*/}
              {getSelectFreshPrice(itm)}
            </td>
            <td width={'60px'} align='right'>{/*60*/}
              {getSelectFreshSubTotal(itm)}
            </td>
          </tr>
        )
      }

      const selectFreshPrintBody = (pageNum: any, totalPages: any, pageItems: any[]) => {
        return (
          <SelectFreshPrintableBodyWrapper style={{border: '1px dotted white'}}>
            <SelectFreshInvoiceBody>
              {selectFreshSoldToCol}
              {selectFreshShipToCol}
              {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.num)) && (
                <SelectFreshInvoiceNumber>
                  {getOrderPrefix(sellerSetting, 'sales')}
                  {isCustomOrderNumberEnabled(sellerSetting) && currentOrder.customOrderNo
                    ? currentOrder.customOrderNo
                    : currentOrder.wholesaleOrderId}
                </SelectFreshInvoiceNumber>
              )}
              {(!pagePrintSetting ||
                (pagePrintSetting &&
                  (pagePrintSetting.orderDate || typeof pagePrintSetting.orderDate === 'undefined'))) && (
                <SelectFreshInvoiceDate>
                  <Moment format="MM/DD/YYYY" date={currentOrder.orderDate}/>
                </SelectFreshInvoiceDate>
              )}
              {(!pagePrintSetting ||
                  (pagePrintSetting &&
                    (pagePrintSetting.paymentTerm || typeof pagePrintSetting.paymentTerm === 'undefined'))) &&
                (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
                  <SelectFreshInvoiceTerms>
                    {currentOrder.financialTerms
                      ? currentOrder.financialTerms
                      : currentOrder.wholesaleClient.paymentTerm}
                  </SelectFreshInvoiceTerms>
                )}
              {(!pagePrintSetting ||
                  (pagePrintSetting &&
                    (pagePrintSetting.salesRepresentative ||
                      typeof pagePrintSetting.salesRepresentative === 'undefined'))) &&
                currentOrder.seller && (
                  <SelectFreshInvoiceRep>
                    {currentOrder.seller ? `${currentOrder.seller.firstName} ${currentOrder.seller.lastName}` : 'N/A'}
                  </SelectFreshInvoiceRep>
                )}
              <SelectFreshInvoiceShipMethod>
                Route
              </SelectFreshInvoiceShipMethod>
              <SelectFreshInvoiceLineItems>
                <SelectFreshInvoiceItemRow>
                  {selectFreshOrderListItems(pageNum, pageItems)}
                </SelectFreshInvoiceItemRow>
              </SelectFreshInvoiceLineItems>
              <SelectFreshGrandTotal>
                ${formatNumber(this.state.totalAmount, 2)}
              </SelectFreshGrandTotal>
              <SelectFreshPageSection>
                Page {pageNum + 1} of {totalPages}
              </SelectFreshPageSection>
            </SelectFreshInvoiceBody>
          </SelectFreshPrintableBodyWrapper>
        )
      }

      let myFormRepeats = [];
      //var totalItemCount = items.length;
      //var numOfRepeats = Math.ceil(totalItemCount / 20);

      const itemSectionHeight = 420;//396
      let grandTotalItemHeight = 0.00;
      let tempItemDesc = '';
      let numItemLineWrap = 0;
      const singleLineHeight = 17.78;//16.00; //15.88
      const additionalWrapHeight = 15.24;//14.00; //12.29??
      let currentLineHeight = 0.00;
      let curPageNum = 1;
      let curPageLineHeigthRemaining = itemSectionHeight;
      let pageItems = [];
      let nextPageIndexStart = 0;
      const maxCharBeforeWrap = 40;//50

      items.forEach((itm) => {
        currentLineHeight = 0.00;
        tempItemDesc = getSelectFreshItemDesc(itm);
        if (tempItemDesc.length <= maxCharBeforeWrap) {
          currentLineHeight += singleLineHeight;
        }
        if (tempItemDesc.length > maxCharBeforeWrap) {
          numItemLineWrap = Math.ceil(tempItemDesc.length / maxCharBeforeWrap);

          currentLineHeight += singleLineHeight;
          if (numItemLineWrap > 1) {
            currentLineHeight += (additionalWrapHeight * (numItemLineWrap - 1))
          }
        }

        curPageLineHeigthRemaining = curPageLineHeigthRemaining - currentLineHeight;

        if (curPageLineHeigthRemaining < 0) {
          //the additional line would have gone on the next page
          pageItems.push(nextPageIndexStart + ',' + (items.indexOf(itm) - 1));
          nextPageIndexStart = items.indexOf(itm) - 1;
          curPageNum += 1
          //reset running total
          curPageLineHeigthRemaining = itemSectionHeight;
        }

        grandTotalItemHeight += currentLineHeight;

        if (items.indexOf(itm) === items.length - 1) {
          //if it is the last item add the final page
          pageItems.push(nextPageIndexStart + ',' + (items.indexOf(itm) + 1));
        }

      })

      let numOfRepeats = Math.ceil(grandTotalItemHeight / itemSectionHeight);

      for (var i = 0; i < numOfRepeats; i++) {
        //for Select Fresh we need to print each for 4 times for their special Paper
        for (var j = 0; j < 4; j++) {
          myFormRepeats.push(selectFreshPrintBody(i, numOfRepeats, pageItems))
        }
      }

      return (
        <SelectFreshInvoiceWrapper id="SelectFreshInvoiceContent"
                                   style={{width: '490px',}}>  {/* className="invoiceBackground" */}
          {myFormRepeats}
        </SelectFreshInvoiceWrapper>
      )

      //END SELECT FRESH SECTION
    }
  }
}

const CreditMemoSection = ({ adjustments, summary, printSetting, tableClass, currentOrder }: any) => {
  if (!printSetting || !printSetting.invoice_credit_memo) {
    return null
  }
  const dataSource = adjustments
    .map((v) => ({
      quantity: v.quantity,
      uom: v.uom,
      orderId: v.order.wholesaleOrderId,
      deliveryDate: v.order.deliveryDate,
      returnInInventory: v.returnInInventory,
      variety: v.orderItem.wholesaleItem.variety,
      sku: v.orderItem.wholesaleItem.sku,
      wholesaleProductUomList: v.item.wholesaleProductUomList,
      inventoryUOM: v.item.inventoryUOM,
      price: v.salePrice,
      pricingUOM: v.orderItem.pricingUOM,
      tax: v.orderItem.tax,
      taxRate: v.orderItem.taxRate,
      taxEnabled: v.orderItem.wholesaleItem.taxEnabled
    }))
    .filter((ad) => ad.quantity)

  const getBillableQtyWithUOM = (record: any) => {
    let showBillableQty,
      uom,
      subUom = null,
      pricingSubUom = null

    if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
      let tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.uom,
      )
      subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
      tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.pricingUOM,
      )
      pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
    }

    if (record.quantity) {
      uom = record.pricingUOM
      if (record.pricingUOM === record.inventoryUOM) {
        if (record.uom === record.inventoryUOM) {
          showBillableQty = record.quantity ? record.quantity : 0
        } else if (subUom != null && record.uom === subUom.name) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity / subUom.ratio, 4) : 0
        } else {
        }
      } else {
        if (record.uom === record.inventoryUOM && pricingSubUom != null) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity * pricingSubUom.ratio, 4) : 0
        } else if (subUom != null && record.uom === subUom.name && pricingSubUom != null) {
          showBillableQty = record.quantity ? (record.quantity / subUom.ratio) * pricingSubUom.ratio : 0
        }
      }
    } else {
      showBillableQty = record.quantity
      uom = record.pricingUOM
    }

    if (!record.pricingUOM) {
      showBillableQty = record.quantity
    }
    return {
      showBillableQty,
      uom,
    }
  }

  let totalReturn = 0
  let totalUnit = 0
  let totalReturnSubtotal = 0
  let totalReturnTax = 0

  if (dataSource.length > 0) {
    dataSource.forEach((record, index) => {
      const extended = Number(record.price ? record.price : 0)
      const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
      const taxRate = Number(record.taxRate ? record.taxRate : 0)
      const taxRateRounded = mathRoundFun(taxRate / 100, 6)
      const taxEnabled = record.taxEnabled
      let showValue = quantity ? true : false
      let subtotal = showValue ? (extended * quantity) : 0
      let tax = showValue && taxEnabled ? (subtotal * taxRateRounded) : 0
      let total = showValue ? subtotal + tax : 0

      totalReturnSubtotal += subtotal
      totalReturnTax += tax
      totalReturn += total

      const { showBillableQty } = getBillableQtyWithUOM(record)
      totalUnit += typeof record.quantity !== 'undefined' ? parseFloat(record.quantity) : 0
    })
  }

  const columns = [
    {
      title: '#',
      key: 'index',
      align: 'center',
      width: 40,
      render: (_key: any, _row: any, index: number) => index + 1,
    },
    {
      title: 'CREDITED',
      align: 'center',
      width: 100,
      render: (r) => `-${r.quantity} ${r.uom || ''}`,
    },
    {
      title: 'ORDER',
      align: 'left',
      width: 120,
      render: (r) => {
        return (
          <div>
            {r.orderId}
            <br />
            {r.deliveryDate}
          </div>
        )
      },
    },
    {
      title: 'RETURN',
      align: 'center',
      render: (r) => (r.returnInInventory ? 'YES' : 'NO'),
    },
    {
      title: 'PRODUCT',
      align: 'left',
      dataIndex: 'variety',
      render: (t) => <div style={{ maxWidth: 200 }}>{t}</div>,
    },
    {
      title: 'SKU',
      align: 'center',
      dataIndex: 'sku',
    },
    {
      title: 'BILLABLE QTY',
      align: 'center',
      dataIndex: 'catchWeightQty',
      render: (weight: number, record: any) => {
        const { uom, showBillableQty } = getBillableQtyWithUOM(record)
        return (
          <>
            -{mathRoundFun(showBillableQty, 4)} {uom}
          </>
        )
      },
    },
    {
      title: 'PRICE',
      dataIndex: 'price',
      align: 'right',
      className: 'p4',
      width: 120,
      render: (value: number, record: any) => {
        let price = value
        // const pricingUOM = record.returnUom
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        if (_.isNumber(price)) {
          price = basePriceToRatioPrice(pricingUOM, price, record)
        }
        var isPricingUOMEnabled =
          !printSetting ||
          (printSetting && (printSetting.invoice_pricingUOM || typeof printSetting.invoice_pricingUOM === 'undefined'))
        return (
          <>
            {`${price ? '$' + formatNumber(price, 2) : '$0'}`}
            {isPricingUOMEnabled ? `/${pricingUOM}` : ''}
          </>
        )
      },
    },
    {
      title: 'SUBTOTAL',
      align: 'right',
      dataIndex: 'totalPrice',
      className: 'p4',
      width: 90,
      render: (data: number, record: any) => {
        const extended = Number(record.price ? record.price : 0)
        const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
        const total = extended * quantity

        console.log('taxEnabled: %s, tax: %d', record.taxEnabled, record.tax)
        const dispTaxInd = total && currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && record.taxEnabled && record.tax && _.toNumber(record.tax) > 0
        const dispTaxMargin = currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && _.toNumber(totalReturnTax) > 0 && !dispTaxInd ? 12 : 0
        return <div style={{ fontWeight: 500, marginRight: dispTaxMargin }}>{total ? `-$${formatNumber(total, 2)}` : ''} { total && dispTaxInd ? "T" : "" }</div>
      },
    },
  ]

  const Totals = () => {
    return (
      <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24} type="flex" justify="space-between">
        <Col span={8}>
          {!!totalUnit && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextLeftBox className="normal black">TOTAL CREDITED UNITS</HeaderTextLeftBox>
              </Col>
              <Col span={8}>
                <HeaderTextLeftBox className="black">-{totalUnit}</HeaderTextLeftBox>
              </Col>
            </Row>
          )}
        </Col>
        <Col span={8}></Col>
        <Col span={8} style={{ paddingRight: (currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && totalReturnTax > 0) ? 16 : 6 }}>
          {!!totalReturn && totalReturnTax > 0 && (
            <>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={8}></Col>
                <Col span={9}>
                  <HeaderTextLeftBox className="normal black">SUBTOTAL</HeaderTextLeftBox>
                </Col>
                <Col span={7} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">-${formatNumber(totalReturnSubtotal, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={8}></Col>
                <Col span={9}>
                  <HeaderTextLeftBox className="normal black">TAX</HeaderTextLeftBox>
                </Col>
                <Col span={7} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">-${formatNumber(totalReturnTax, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
            </>
          )}
          {!!totalReturn && (
            <Row gutter={24} style={{ marginBottom: 10 }}>
              <Col span={8}></Col>
              <Col span={9}>
                <HeaderTextLeftBox className="normal black">TOTAL CREDIT</HeaderTextLeftBox>
              </Col>
              <Col span={7} style={{ paddingRight: 0 }}>
                <HeaderTextRightBox className="black">-${formatNumber(Math.abs(totalReturn), 2)}</HeaderTextRightBox>
              </Col>
            </Row>
          )}

          <Row gutter={24} style={{ marginBottom: 10 }}>
            <Col span={8}></Col>
            <Col span={9}>
              <HeaderTextLeftBox className="normal black">SUMMARY</HeaderTextLeftBox>
            </Col>
            <Col span={7} style={{ paddingRight: 0 }}>
              <HeaderTextRightBox className="black">
                {summary - totalReturn < 0 && '-'}${formatNumber(Math.abs(summary - Math.abs(totalReturn)), 2)}
              </HeaderTextRightBox>
            </Col>
          </Row>
          {/* <Row gutter={24}>
            <Col span={16}>
              <HeaderTextRightBox className="normal black">SUMMARY</HeaderTextRightBox>
            </Col>
            <Col span={8}>
              <HeaderTextRightBox className="black">
                {summary - totalReturn < 0 && '-'}${formatNumber(Math.abs(summary - Math.abs(totalReturn)), 2)}
              </HeaderTextRightBox>
            </Col>
          </Row> */}

        </Col>
      </Row>
    )
  }

  return dataSource.length ? (
    <div style={{ marginBottom: '3px' }}>
      <PrintPickSheetTable style={{ marginTop: 10 }}>
        {!!totalReturn && (
          <DeliveryListTable>
            <Table
              style={{ width: '100%' }}
              rowKey="wholesaleOrderItemId"
              columns={columns}
              dataSource={dataSource}
              pagination={false}
              className={tableClass}
              bordered={false}
              rowKey="wholesaleOrderItemId"
              rowClassName={(record, index) => {
                return index % 2 === 1 ? 'alternative' : ''
              }}
            />
          </DeliveryListTable>
        )}
      </PrintPickSheetTable>
      <hr />
      <Totals />
    </div>
  ) : (
    <></>
  )
}

const InvoiceCreditMemoSection = connect(({ orders, setting }) => ({
  adjustments: orders.adjustments,
  printSetting: JSON.parse(orders.printSetting),
}))(CreditMemoSection)

export default PrintInvoice

const PrintContainerInvoice = ({ tableClass, items }) => {
  const columns = [
    // {
    //   title: '#',
    //   width: 70,
    //   render: (_, r, i) => `${i + 1}`
    // },
    {
      title: 'ORDERED',
      dataIndex: 'quantity',
      align: 'center'
    },
    {
      title: 'SHIPPED',
      align: 'center',
      dataIndex: 'picked'
    },
    // {
    //   title: 'SHIPPED UOM',
    //   dataIndex: 'UOM'
    // },
    {
      title: 'PRODUCT',
      dataIndex: 'itemName'
    },
    {
      title: 'SIZE',
      dataIndex: 'size'
    },
    {
      title: 'UPC',
      dataIndex: 'upc'
    },
    {
      title: 'NOTES',
      dataIndex: 'note'
    },
    {
      title: 'GROSS WT.',
      render: (r) => {
        return <span>{formatNumber(_.multiply(r.catchWeightQty, r.grossWeight || 0), 1)}</span>
      }
    },
    {
      title: 'GROSS VOL.',
      render: (r) => {
        return <span>{formatNumber(_.multiply(r.catchWeightQty, r.grossVolume || 0), 1)}</span>
      }
    },
    {
      title: 'PRICE',
      dataIndex: 'price',
      align: 'right',
      render: (t, r) => {
        const price = basePriceToRatioPrice(r.UOM, t, r)

        return <span>${formatNumber(price,2)}</span>
      }
    },
    {
      title: 'SUBTOTAL',
      dataIndex: 'price',
      align: 'right',
      render: (t, r) => {
        const price = basePriceToRatioPrice(r.UOM, t, r)

        return <span>${formatNumber(_.multiply(r.catchWeightQty, price), 2)}</span>
      }
    },
  ]
  return (
    <PrintPickSheetTable className="pick-sheet" style={{ marginTop: 20 }}>
      <Table
        className={tableClass}
        rowKey="wholesaleOrderItemId"
        columns={columns}
        rowClassName={(record, index) => {
          return index % 2 === 1 ? 'alternative' : ''
        }}
        dataSource={items}
        pagination={false}
      />
    </PrintPickSheetTable>
  )
}

const BelenTradingPrintContainerInvoice = ({ tableClass, items }) => {
  return (
    <BelenTradingPrintPickSheetTable>
      <thead>
      <tr>
        <th>SKU</th>
        <th>PRODUCT</th>
        <th className="right-align">QTY</th>
        <th className="right-align">PRICE</th>
        <th className="right-align pr-20">AMOUNT</th>
      </tr>
      </thead>
      <tbody>
      {
        items.map((i) => {
          const price = basePriceToRatioPrice(i.UOM, i.price, i)

          return (
          <tr>
            <td>{i.SKU}</td>
            <td>{i.itemName ? i.itemName : i.variety}</td>
            <td className="right-align">{formatNumber(i.quantity,0)}</td>
            <td className="right-align">{formatNumber(price,2)}</td>
            <td className="right-align pr-20">{formatNumber(_.multiply(i.catchWeightQty, price), 2)}</td>
          </tr>
          )
        })
      }
      </tbody>
    </BelenTradingPrintPickSheetTable>
  )
}
