import React from 'react'
import { OrderDetail, OrderItem } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode'
import {
  HeaderTextRightBox,
  HeaderTextLeftBox,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
  ModalDetailText,
  UnderLineWrapper,
  Underline,
  DeliverySction,
  DeliveryMemoListTable,
  WeightSpan,
  WeightWrapper,
  LightColoredRow,
  pl24,
  DisplayLabel,
  DisplayValue,
  DisplayLeftLabel,
} from '~/modules/customers/sales/cart/print/_styles'
import {
  formatNumber,
  formatAddress,
  basePriceToRatioPrice,
  mathRoundFun,
  getOrderPrefix,
  inventoryQtyToRatioQty,
  ratioQtyToInventoryQty,
  numberMultipy,
  judgeConstantRatio,
  formatItemDescription,
  formatDate,
} from '~/common/utils'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles'
import _, { cloneDeep } from 'lodash'
import { connect } from 'react-redux'
import { url2base64 } from './sendEmail'
import { Icon } from '~/components/icon/'

interface PrintBillOfLadingProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  logo: string
  company: any
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  title?: string
  multiple?: boolean
  printSetting: any
  type: string
  fulfillmentOptionType?: number
  sellerSetting?: any
  paymentTerms: any[]
  selectedType?: number
  reasonArr: any[]
  reasonTypes: any[]
}

export class PrintMemo extends React.PureComponent<PrintBillOfLadingProps> {
  state = {
    totalQuantity: 0,
    totalAmount: 0,
    totalWeight: 0,
    totalTax: 0,
    totalSubtotal: 0,
    visibleAlert: false,
    selectedRowKeys: [],
    orderItems: this.props.orderItems,
    pagePrintSetting: null,
    logo: '',
    reasonArr: [],
  }

  componentDidMount(): void {
    this.getAmount(this.props.orderItems)
    if (this.props.printSetting) {
      this.setPrintSetting(this.props.printSetting, this.props.type)
    }
    if (this.props.logo) {
      url2base64(this.props.logo.replace('www.', '')).then((logo) => {
        this.setState({ logo })
      })
    }
    if (this.props.reasonArr) {
      this.setState({ reasonArr: this.props.reasonArr })
    }
  }

  componentWillReceiveProps(nextProps: PrintBillOfLadingProps) {
    if (this.props.orderItems !== nextProps.orderItems) {
      this.getAmount(nextProps.orderItems)
      this.setState({ orderItems: nextProps.orderItems })
    }

    if (this.props.reasonArr !== nextProps.reasonArr) {
      this.setState({ reasonArr: nextProps.reasonArr })
    }

    if (JSON.stringify(this.props.printSetting) != JSON.stringify(nextProps.printSetting)) {
      this.setPrintSetting(nextProps.printSetting, nextProps.type)
    }
  }

  setPrintSetting = (jsonSetting: string, modalType: string) => {
    const pageSetting = JSON.parse(jsonSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`${modalType}_`) > -1) {
        const newKey = el.substring(`${modalType}_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    this.setState({ pagePrintSetting: data })
  }

  getAmount = (items: any) => {
    let _totalQuantity = 0
    let _totalAmount = 0
    let _totalWeight = 0
    let _totalTax = 0
    let _totalSubtotal = 0

    for (let i = 0; i < items.length; i++) {
      let constantRatio = judgeConstantRatio(items[i])
      _totalQuantity += _.toNumber(items[i].picked)

      let pricingUOM = items[i].pricingUOM ? items[i].pricingUOM : items[i].UOM
      let unitUOM = items[i].UOM
      let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(items[i].price), items[i], 12)
      let taxRate = items[i].taxRate
      let taxRateRounded = mathRoundFun(taxRate / 100, 6)
      let taxEnabled = items[i].taxEnabled
      let tax = 0
      let subtotal = 0

      if (items[i].oldCatchWeight) {
        _totalWeight += _.toNumber(items[i].catchWeightQty)
        subtotal = numberMultipy(_.toNumber(items[i].price), _.toNumber(items[i].catchWeightQty))
      } else {
        if (!constantRatio) {
          _totalWeight += inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i])
          subtotal = numberMultipy(
            ratioPrice,
            inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i], 12),
          )
        } else {
          const quantity = inventoryQtyToRatioQty(
            pricingUOM,
            ratioQtyToInventoryQty(unitUOM, _.toNumber(items[i].picked), items[i], 12),
            items[i],
            12,
          )
          _totalWeight += quantity
          subtotal = numberMultipy(ratioPrice, quantity)
        }
      }
      tax = taxEnabled ? subtotal * taxRateRounded: 0
      subtotal = Math.round(subtotal * 100) / 100
      items[i]['amount'] = '$' + formatNumber(subtotal, 2)

      _totalTax += tax
      _totalSubtotal += subtotal
      _totalAmount += subtotal + tax
    }

    this.setState({
      totalQuantity: _totalQuantity,
      totalAmount: _totalAmount,
      totalWeight: _totalWeight,
      totalTax: _totalTax,
      totalSubtotal: _totalSubtotal,
      orderItems: items,
    })
  }

  getExpandableRowKeys = () => {
    const { orderItems } = this.state
    let result: any[] = []
    orderItems.forEach((el) => {
      if (!el.constantRatio) {
        result.push(el.wholesaleOrderItemId)
      }
    })
    return result
  }

  renderCatchWeightValues = (weights: any[]) => {
    // const { catchWeightValues, multiple } = this.props

    // let weights: any[] = []
    // if (multiple && typeof record.catchWeightValues !== 'undefined') {
    //   weights = record.catchWeightValues
    // } else {
    //   weights = catchWeightValues[record.wholesaleOrderItemId] ? catchWeightValues[record.wholesaleOrderItemId] : []
    // }
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (weights.length > 0) {
      let result: any[] = []
      let sub: any[] = []
      weights.forEach((el: any, index: number) => {
        if (index > 0 && (index - 10) % 10 == 0) {
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
          sub = []
        }

        const weightEl = <WeightSpan>{el.unitWeight ? el.unitWeight : ''}</WeightSpan>
        if (index == weights.length - 1) {
          sub.push(weightEl)
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
        } else {
          sub.push(weightEl)
        }
      })
      return result
    }

    return []
  }

  onImageLoad = () => {
    //
  }

  render() {
    const {
      currentOrder,
      type,
      fulfillmentOptionType,
      sellerSetting,
      catchWeightValues,
      multiple,
      printSetting,
      company,
      paymentTerms,
      reasonArr,
    } = this.props
    const { orderItems, pagePrintSetting } = this.state

    let tableClass = 'alternate-row'
    if (printSetting) {
      const setting = JSON.parse(printSetting)
    }

    let items = cloneDeep(orderItems)
    orderItems.forEach((record, i) => {
      const index = items.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
      items[index].key = i
      if (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightValues)) {
        let showCatchWeightModal = false
        let constantRatio = judgeConstantRatio(record)
        if (!constantRatio) {
          showCatchWeightModal = true
        }
        if (showCatchWeightModal) {
          let weights: any[] = []
          if (multiple && typeof record.catchWeightValues !== 'undefined') {
            weights = record.catchWeightValues
          } else {
            weights = catchWeightValues[record.wholesaleOrderItemId]
              ? catchWeightValues[record.wholesaleOrderItemId]
              : []
          }

          if (weights.length > 0) {
            items.splice(index + 1, 0, {
              // ...cloneDeep(record),
              relatedOrderItemId: record.wholesaleOrderItemId,
              weights,
            })
          }
        }
      }
    })

    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'
    company = company
      ? company
      : currentOrder.wholesaleClient && currentOrder.wholesaleClient.wholesaleCompany
      ? currentOrder.wholesaleClient.wholesaleCompany
      : null

    const soldToCol = (
      <Col>
        <div style={{ width: '250px' }}>
          <h6
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'bold',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            Sold to:
          </h6>
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {clientName}
          </p>
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.billingAddress ? formatAddress(currentOrder.billingAddress.address, true).trim() : 'N/A'}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
              ? currentOrder.wholesaleClient.mainBillingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    )

    const shipToCol = (
      <Col span={7}>
        <div className="f12">
          <h6
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'bold',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {fulfillmentOptionType == 2 ? 'Pickup Address' : 'Ship to'}:{' '}
          </h6>
          {fulfillmentOptionType != 2 && (
            <p
              style={{
                fontSize: '14px',
                fontFamily: 'Inter',
                color: 'black',
                fontWeight: 'normal',
                lineHeight: '19.6px',
                textAlign: 'left',
                marginBottom: '0px',
                width: '250px',
              }}
            >
              {clientName}
            </p>
          )}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {fulfillmentOptionType == 2 ? (
              <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
            ) : (
              <>
                {currentOrder.shippingAddress
                  ? formatAddress(currentOrder.shippingAddress.address, true).trim()
                  : 'N/A'}
              </>
            )}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    )

    let phone = ''
    if (company) {
      phone = (company.mainBillingAddress &&
      company.mainBillingAddress.phone &&
      company.mainBillingAddress.phone !== 'null'
        ? company.mainBillingAddress.phone
        : company.phone
        ? company.phone
        : ''
      ).replace('+1-', '')
    }

    let logo = this.props.logo
    if (logo && logo !== 'default') {
      if (!location.origin.includes('www')) {
        logo = logo.replace('www.', '')
      }
    }

    const carrierSetting = pagePrintSetting ? pagePrintSetting.carrier_reference : false

    const logisticCols = (
      <>
        {currentOrder && currentOrder.fulfillmentType === 3 && (
          <>
            {carrierSetting && (
              <Col span={4}>
                <div className="f12">
                  <DisplayLeftLabel>Carrier </DisplayLeftLabel>
                  <DisplayValue>{currentOrder.carrier}</DisplayValue>
                  <DisplayValue>{currentOrder.carrierReferenceNo}</DisplayValue>
                </div>
              </Col>
            )}
            {origin && (
              <Col span={6}>
                <div className="f12">
                  <DisplayLeftLabel>Origin </DisplayLeftLabel>
                  <DisplayValue>{currentOrder.originAddress}</DisplayValue>
                </div>
              </Col>
            )}
          </>
        )}
        <Col span={6}>
          <div className="f12">
            <DisplayLeftLabel>Destination </DisplayLeftLabel>
            <DisplayValue>{clientName}</DisplayValue>
            <DisplayValue style={{ whiteSpace: 'pre-wrap' }}>
              {currentOrder.shippingAddress ? formatAddress(currentOrder.shippingAddress.address, true).trim() : 'N/A'}
            </DisplayValue>
            <DisplayValue>
              {currentOrder.wholesaleClient.mainShippingAddress &&
              currentOrder.wholesaleClient.mainShippingAddress.phone
                ? currentOrder.wholesaleClient.mainShippingAddress.phone
                : currentOrder.wholesaleClient.mobilePhone}
            </DisplayValue>
          </div>
        </Col>
      </>
    )

    return (
      <PrintPickSheetWrapper id="memoContent">
        <HeaderBox style={{ fontWeight: 'normal' }}>
          <Row style={{ textAlign: 'center' }}>
            <h1 style={{ marginBottom: '0.25em' }}>{'CREDIT MEMO'}</h1>
          </Row>
          <Row>
            <Col span={14}>
              <CompanyInfoWrapper>
                <div style={{ textAlign: 'left' }}>
                  <div style={{ marginRight: 10 }}>
                    {logo === 'default' ? (
                      <LogoWrapper style={{textAlign: 'left'}}>
                        <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                      </LogoWrapper>
                    ) : (
                      <img src={logo} style={{ maxWidth: 150 }} onLoad={this.onImageLoad} />
                    )}
                  </div>
                  {company && (
                    <div className="left">
                      {company.companyName !== null && (
                        <HeaderTextLeftBox className="f18">{company.companyName}</HeaderTextLeftBox>
                      )}
                      {company.mainBillingAddress !== null && (
                        <p
                          style={{
                            fontSize: '14px',
                            fontFamily: 'Inter',
                            color: 'black',
                            fontWeight: 'normal',
                            lineHeight: '19.6px',
                            textAlign: 'left',
                            marginBottom: '0px',
                          }}
                        >
                          {formatAddress(company.mainBillingAddress.address, true)}
                        </p>
                      )}
                      {(phone || company.fax) && (
                        <p
                          style={{
                            fontSize: '14px',
                            fontFamily: 'Inter',
                            color: 'black',
                            fontWeight: 'normal',
                            lineHeight: '19.6px',
                            textAlign: 'left',
                            marginBottom: '0px',
                          }}
                        >
                          Phone/Fax: {phone}
                          {company.fax ? `/${company.fax}` : ''}
                        </p>
                      )}
                    </div>
                  )}
                </div>
              </CompanyInfoWrapper>
            </Col>
            <Col span={10} className="f12">
              <Row style={{ display: 'flex' }}>
                <DisplayLabel style={{ fontWeight: 'bold' }}>Credit Memo#: </DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  <p style={{ fontWeight: 'bold' }}>{`#${currentOrder.wholesaleOrderId}C`}</p>
                </DisplayValue>
              </Row>
            </Col>
          </Row>
          <Row style={{ marginTop: 8 }} className="f12">
            {fulfillmentOptionType != 3 ? (
              <>
                {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
                  <>
                    <Col span={7}>
                      <div>
                        <h6
                          style={{
                            fontSize: '14px',
                            fontFamily: 'Inter',
                            color: 'black',
                            fontWeight: 'bold',
                            lineHeight: '19.6px',
                            textAlign: 'left',
                            marginBottom: '0px',
                            width: '250px',
                          }}
                        >
                          PICKUP ADDRESS
                        </h6>
                        <p
                          style={{
                            fontSize: '14px',
                            fontFamily: 'Inter',
                            color: 'black',
                            fontWeight: 'normal',
                            lineHeight: '19.6px',
                            textAlign: 'left',
                            marginBottom: '0px',
                            width: '250px',
                          }}
                        >
                          {clientName}
                        </p>
                        <p
                          style={{
                            fontSize: '14px',
                            fontFamily: 'Inter',
                            color: 'black',
                            fontWeight: 'normal',
                            lineHeight: '19.6px',
                            textAlign: 'left',
                            marginBottom: '0px',
                            width: '250px',
                          }}
                        >
                          {currentOrder.pickupAddress}
                        </p>
                      </div>
                    </Col>
                    {soldToCol}
                  </>
                ) : (
                  <div style={{ display: 'flex', width: '50%' }}>
                    {soldToCol}
                    {shipToCol}
                  </div>
                )}
              </>
            ) : (
              logisticCols
            )}
          </Row>
        </HeaderBox>
        <InvoiceCreditMemoSection
          summary={this.state.totalAmount}
          tableClass={tableClass}
          reasonArr={this.props.reasonArr}
          pagePrintSetting={this.state.pagePrintSetting}
          currentOrder = {this.props.currentOrder}
        />
        {!pagePrintSetting || pagePrintSetting.receivedby !== false && (
          <>
            <div style={{ color: 'black' }}>Received by:</div>
            <br />
            <div style={{ display: 'flex' }}>
              <div style={{ color: 'black' }}>Print Name:</div>
              <Underline />
              <div style={{ color: 'black' }}>Signature:</div>
              <Underline />
              <div style={{ color: 'black' }}>Date:</div>
              <Underline />
            </div>
          </>
        )}
      </PrintPickSheetWrapper>
    )
  }
}

const CreditMemoSection = ({ adjustments, summary, printSetting, tableClass, reasonArr, companyProductTypes, pagePrintSetting, currentOrder }: any) => {
  const dataSource = adjustments
    .map((v) => ({
      quantity: v.quantity,
      uom: v.uom,
      orderId: v.order.wholesaleOrderId,
      deliveryDate: v.order.deliveryDate,
      returnInInventory: v.returnInInventory,
      variety: v.orderItem.wholesaleItem.variety,
      sku: v.orderItem.wholesaleItem.sku,
      wholesaleProductUomList: v.item.wholesaleProductUomList,
      inventoryUOM: v.item.inventoryUOM,
      price: v.salePrice,
      pricingUOM: v.orderItem.pricingUOM,
      reasonType: v.reasonType,
      tax: v.orderItem.tax,
      taxRate: v.orderItem.taxRate,
      taxEnabled: v.orderItem.wholesaleItem.taxEnabled
    }))
    .filter((ad) => ad.quantity)


  const getDisplayingColumns = (columns: any[]) => {
    let newColumns: any[] = []
    if (!pagePrintSetting) return columns

    columns.forEach((el) => {
      const key = el.key
      if (pagePrintSetting[key] !== false) {
        newColumns.push(el)
      }
    })
    return newColumns
  }

  const getBillableQtyWithUOM = (record: any) => {
    let showBillableQty,
      uom,
      subUom = null,
      pricingSubUom = null

    if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
      let tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.uom,
      )
      subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
      tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.pricingUOM,
      )
      pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
    }

    if (record.quantity) {
      uom = record.pricingUOM
      if (record.pricingUOM === record.inventoryUOM) {
        if (record.uom === record.inventoryUOM) {
          showBillableQty = record.quantity ? record.quantity : 0
        } else if (subUom != null && record.uom === subUom.name) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity / subUom.ratio, 4) : 0
        } else {
        }
      } else {
        if (record.uom === record.inventoryUOM && pricingSubUom != null) {
          showBillableQty = record.quantity ? mathRoundFun(record.quantity * pricingSubUom.ratio, 4) : 0
        } else if (subUom != null && record.uom === subUom.name && pricingSubUom != null) {
          showBillableQty = record.quantity ? (record.quantity / subUom.ratio) * pricingSubUom.ratio : 0
        }
      }
    } else {
      showBillableQty = record.quantity
      uom = record.pricingUOM
    }

    if (!record.pricingUOM) {
      showBillableQty = record.quantity
    }
    return {
      showBillableQty,
      uom,
    }
  }

  let totalReturn = 0
  let totalUnit = 0
  let totalReturnSubtotal = 0
  let totalReturnTax = 0

  if (dataSource.length > 0) {
    dataSource.forEach((record, index) => {
      const extended = Number(record.price ? record.price : 0)
      const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
      const taxRate = Number(record.taxRate ? record.taxRate : 0)
      const taxRateRounded = mathRoundFun(taxRate / 100, 6)
      const taxEnabled = record.taxEnabled
      let showValue = quantity ? true : false
      let subtotal = showValue ? (extended * quantity) : 0
      let tax = showValue && taxEnabled ? (subtotal * taxRateRounded) : 0
      let total = showValue ? subtotal + tax : 0

      totalReturnSubtotal += subtotal
      totalReturnTax += tax
      totalReturn += total

      const { showBillableQty } = getBillableQtyWithUOM(record)
      totalUnit += typeof record.quantity !== 'undefined' ? parseFloat(record.quantity) : 0
    })
  }

  const columns = [
    {
      title: '#',
      key: 'index',
      align: 'center',
      width: 30,
      height: 100,
      render: (_key: any, _row: any, index: number) => {
        return <p>{index + 1}</p>
      },
    },
    {
      title: 'CREDITED',
      align: 'right',
      height: 100,
      dataIndex: 'credited',
      key: 'credited',
      render: (_key: any, _row: any, index: number) => {
        const reasonObj = companyProductTypes.reasonType.filter((item: any) => item.id == _row.reasonType)
        console.log(reasonObj)
        return (
          <div style={{ position: 'relative' }}>
            <p>{`-${_row.quantity} ${_row.uom || ''}`}</p>
            <p
              style={{ position: 'absolute', top: '35px', width: '300px', left: '0px', textAlign: 'left' }}
            >{`Reason Type: ${reasonObj.length > 0 ? reasonObj[0].name : ''}`}</p>
          </div>
        )
      },
    },
    {
      title: 'ORDER',
      align: 'left',
      height: 100,
      dataIndex: 'order',
      key: 'order',
      render: (value: any, r: any) => {
        console.log(r)
        return (
          <div>
            {r.orderId}
            <br />
            {formatDate(r.deliveryDate)}
          </div>
        )
      },
    },
    {
      title: 'RETURN',
      align: 'left',
      height: 100,
      dataIndex: 'return',
      key: 'return',
      render: (value: any, r: any) => (r.returnInInventory ? 'YES' : 'NO'),
    },
    {
      title: 'PRODUCT',
      align: 'left',
      dataIndex: 'variety',
      key: 'variety',
      height: 100,
      render: (t) => <div style={{ maxWidth: 200 }}>{t}</div>,
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      key: 'sku',
      height: 100,
    },
    {
      title: 'BILLABLE QTY',
      align: 'right',
      dataIndex: 'catchWeightQty',
      key: 'catchWeightQty',
      render: (weight: number, record: any) => {
        const { uom, showBillableQty } = getBillableQtyWithUOM(record)
        return (
          <>
            -{mathRoundFun(showBillableQty, 4)} {uom}
          </>
        )
      },
    },
    {
      title: 'PRICE',
      dataIndex: 'price',
      key: 'price',
      align: 'right',
      render: (value: number, record: any) => {
        let price = value
        // const pricingUOM = record.returnUom
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        if (_.isNumber(price)) {
          price = basePriceToRatioPrice(pricingUOM, price, record)
        }
        var isPricingUOMEnabled =
          !printSetting ||
          (printSetting && (printSetting.invoice_pricingUOM || typeof printSetting.invoice_pricingUOM === 'undefined'))
        return (
          <>
            {`${price ? '$' + formatNumber(price, 2) : '$0'}`}
            {isPricingUOMEnabled ? `/${pricingUOM}` : ''}
          </>
        )
      },
    },
    {
      title: 'SUBTOTAL',
      align: 'right',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      height: 100,
      render: (data: number, record: any, props: any) => {
        const extended = Number(record.price ? record.price : 0)
        const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
        const total = extended * quantity

        const dispTaxInd = total && currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && record.taxEnabled && record.tax && _.toNumber(record.tax) > 0;
        const dispTaxMargin = currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && _.toNumber(totalReturnTax) > 0 && !dispTaxInd ? 12 : 0;
        return <div style={{ fontWeight: 500, marginRight: dispTaxMargin }}>{total ? `-$${formatNumber(total, 2)}` : ''} { total && dispTaxInd ? "T" : "" }</div>
      },
    },
  ]

  const Totals = () => {
    return (
      <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24} type="flex" justify="space-between">
        <Col span={12}>
          {!!totalUnit && (
            <Row gutter={24}>
              <Col span={16}>
                <HeaderTextLeftBox className="normal black">TOTAL CREDITED UNITS</HeaderTextLeftBox>
              </Col>
              <Col span={8}>
                <HeaderTextLeftBox className="black">-{totalUnit}</HeaderTextLeftBox>
              </Col>
            </Row>
          )}
        </Col>
        <Col span={12} style={{ paddingRight: (currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && totalReturnTax > 0) ? 16 : 6 }}>
          {!!totalReturn && totalReturnTax > 0 && (
            <>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={10}></Col>
                <Col span={8}>
                  <HeaderTextLeftBox className="normal black">SUBTOTAL</HeaderTextLeftBox>
                </Col>
                <Col span={6} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">-${formatNumber(totalReturnSubtotal, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={10}></Col>
                <Col span={8}>
                  <HeaderTextLeftBox className="normal black">TAX</HeaderTextLeftBox>
                </Col>
                <Col span={6} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">-${formatNumber(totalReturnTax, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
            </>
          )}
          {!!totalReturn && (
            <Row gutter={24}>
              <Col span={10}></Col>
              <Col span={8}>
                <HeaderTextLeftBox className="normal black">TOTAL CREDIT</HeaderTextLeftBox>
              </Col>
              <Col span={6} style={{ paddingRight: 0 }}>
                <HeaderTextRightBox className="black">-${formatNumber(Math.abs(totalReturn), 2)}</HeaderTextRightBox>
              </Col>
            </Row>
          )}
        </Col>
      </Row>
    )
  }

  const filteredColumns = getDisplayingColumns(columns)

  return dataSource.length ? (
    <div style={{ marginBottom: '20px', position: 'relative' }}>
      <DeliveryMemoListTable>
        <Table
          style={{ width: '100%' }}
          rowKey="wholesaleOrderItemId"
          columns={filteredColumns}
          dataSource={dataSource}
          pagination={false}
          className={tableClass}
          bordered={false}
          rowKey="wholesaleOrderItemId"
          rowClassName={(record, index) => {
            return 'alternative'
          }}
        />
      </DeliveryMemoListTable>
      <hr />
      <Totals />
    </div>
  ) : (
    <></>
  )
}

const InvoiceCreditMemoSection = connect(({ orders, setting }) => ({
  adjustments: orders.adjustments,
  printSetting: JSON.parse(orders.printSetting),
  companyProductTypes: orders.companyProductTypes,
}))(CreditMemoSection)

export default PrintMemo
