import React, { FC, useEffect, useState } from 'react'
import { OrderDetail, OrderItem } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode'
import {
  HeaderTextLeftBox,
  EllipseText,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
  FlexWrap,
  Inputs,
} from '~/modules/customers/sales/cart/print/_styles'
import Moment from 'react-moment'
import _moment from 'moment'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import {
  formatAddress,
  formatItemDescription,
  getOrderPrefix,
  judgeConstantRatio,
  isCustomOrderNumberEnabled,
  formatNumber,
  mathRoundFun,
  checkError,
} from '~/common/utils'
import { Icon } from '~/components'
import moment from 'moment'
import { Icon as IconSvg } from '~/components'
import { Flex, ThemeInput } from '~/modules/customers/customers.style'
import _, { cloneDeep } from 'lodash'
import { OrderService } from '~/modules/orders/order.service'

interface PrintPickSheetProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  logo: string
  isWorkOrder?: boolean
  catchWeightValues: any
  multiple?: boolean
  printSetting?: string
  changePrintLogoStatus: Function
  sellerSetting?: any
  company: any
  currentPrintContainerId: string
  orderId: string
}

export class PrintPickSheet extends React.PureComponent<PrintPickSheetProps> {
  state = {
    visibleAlert: false,
    selectedRowKeys: [],
    pagePrintSetting: null,
    containerData: {},
  }

  componentDidMount() {
    const _this = this
    if (this.props.printSetting) {
      this.setPrintSetting(this.props.printSetting)
    }
    if (this.props.currentPrintContainerId && this.props.orderId) {
      if (this.props.currentOrder.fulfillmentType === 5) {
        OrderService.instance.getSingleContainerPrint(this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: {
                ...resp.body.data,
                tempRecorderList: JSON.parse(resp.body.data.tempRecorderList)
              }
            })
          },
          error(err) { checkError(err) },
        })
      } else {
        OrderService.instance.getPrintContainer(this.props.currentPrintContainerId, this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: resp.data
            })
          },
          error(err) { checkError(err) },
        })
      }
    }
    if (this.props.currentPrintContainerId && !this.props.orderId) {
      OrderService.instance.getPrintContainer(this.props.currentPrintContainerId).subscribe({
        next(resp) {
          _this.setState({
            containerData: resp.data,
          })
        },
        error(err) {
          checkError(err)
        },
      })
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.printSetting != nextProps.printSetting && nextProps.printSetting != null) {
      this.setPrintSetting(nextProps.printSetting)
    }
  }

  setPrintSetting = (jsonSetting: string) => {
    const pageSetting = JSON.parse(jsonSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`pick_`) > -1) {
        const newKey = el.substring(`pick_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    this.setState({ pagePrintSetting: data })
  }

  getDisplayingColumns = (columns: any[]) => {
    const { pagePrintSetting } = this.state
    let newColumns: any[] = []
    if (!pagePrintSetting) return columns
    columns.forEach((el) => {
      const key = el.key
      if (pagePrintSetting[key] !== false) {
        newColumns.push(el)
      }
    })
    return newColumns
  }

  onImageLoad = () => {
    this.props.changePrintLogoStatus()
  }

  checkWeights = (row: any) => {
    return typeof row.weights !== 'undefined' && row.weights.length > 0
  }

  render() {
    const {
      orderItems,
      currentOrder,
      isWorkOrder,
      catchWeightValues,
      multiple,
      sellerSetting,
      printSetting,
      company,
    } = this.props
    let items: any[] = []
    if (orderItems) {
      items = orderItems.filter((el) => {
        return el.quantity > 0
      })
    }

    let settings: any = null
    let tableClass = ''
    if (printSetting) {
      settings = JSON.parse(printSetting)
      if (settings.pick_alternateRow === true) {
        tableClass = 'alternate-row'
      }
      if (this.props.companyName === 'Hung San' || settings.pick_item_sort === 'sku') {
        items = _.orderBy(items, ({SKU})=> SKU ?? '', 'asc')
      } else if(settings.pick_item_sort == 'location') {
        items = _.orderBy(items, (record)=> {
          if(isWorkOrder) {
            return record.location
          } else if (sellerSetting.company && sellerSetting.company.locationMethod == 1 && record.itemLocation) {
            return record.itemLocation
          } else if (record.locationNames && record.locationNames.length > 0) {
            return record.locationNames.join(', ')
          } else {
            return ''
          }
        }, 'asc')
      }
    }

    // TODO: remove code block, if unnecessary
    /*
    let items = cloneDeep(filteredItems)

    filteredItems.forEach((record) => {
      let weights: any[] = []
      let showCatchWeightModal = false
      let constantRatio = judgeConstantRatio(record)
      if (!constantRatio) {
        showCatchWeightModal = true
      }
      if (showCatchWeightModal) {
        if (multiple && typeof record.catchWeightValues !== 'undefined') {
          weights = record.catchWeightValues
        } else {
          weights = catchWeightValues[record.wholesaleOrderItemId] ? catchWeightValues[record.wholesaleOrderItemId] : []
        }
        const index = items.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
        if (weights.length > 0) {
          items.splice(index + 1, 0, {
            weights,
          })
        }
      }
    })
    */
    const columns: Array<any> = [
      // {
      //   title: 'TYPE',
      //   dataIndex: isWorkOrder ? 'wholesaleOrderItemStatus' : 'status',
      //   key: 'status',
      //   align: 'center',
      //   width: 100,
      // },
      {
        title: 'ORDERED',
        dataIndex: 'quantity',
        key: 'quantity',
        align: 'center',
        width: 50,
        render: (quantity: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return <span style={{ whiteSpace: 'nowrap' }}>{mathRoundFun(quantity, 2)}</span>
        },
      },
      {
        title: 'UOM',
        dataIndex: isWorkOrder ? 'uom' : 'UOM',
        key: 'UOM',
        align: 'center',
        width: 50,
        render: (uom: string) => {
          return <span style={{ whiteSpace: 'nowrap' }}>{uom}</span>
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        align: 'center',
        width: 50,
        render: (sku: string) => {
          return <span style={{ whiteSpace: 'nowrap' }}>{sku}</span>
        },
      },
      {
        title: 'PRODUCT',
        dataIndex: isWorkOrder ? 'wholesaleItem.variety' : 'variety',
        key: 'variety',
        align: 'left',
        className: 'th-left',
        render: (variety: string, record: any) => {
          const catchWeightValuesShow = settings
            ? settings.catchWeightValues
              ? settings.catchWeightValues
              : false
            : false
          if (this.checkWeights(record) && company && company.isDisablePickingStep === false) {
            const { weights } = record
            let el = null
            el = (
              <Inputs>
                <FlexWrap>
                  {weights.map((w: any, i) => {
                    return (
                      <ThemeInput
                        key={`${record.variety}-${i}`}
                        value={w.unitWeight ? w.unitWeight : ''}
                        readOnly
                        style={{ width: 68, textAlign: 'center' }}
                      />
                    )
                  })}
                </FlexWrap>
              </Inputs>
            )
            if (catchWeightValuesShow) {
              return {
                children: el,
                props: {
                  colSpan: 6,
                },
              }
            } else {
              return {
                children: '',
                props: {
                  colSpan: 0,
                },
              }
            }
          }
          return record.editedItemName ? record.editedItemName : variety
          //: formatItemDescription(variety, record.SKU, sellerSetting, record.customerProductCode)
        },
      },
      {
        title: 'BRAND',
        dataIndex: 'modifiers',
        key: 'modifiers',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'extraOrigin',
        key: 'extraOrigin',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'LOT',
        dataIndex: 'lotId',
        key: 'lotId',
        align: 'center',
        width: 150,
        render: (data: string, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return (
            <div style={{ height: 30 }}>
              {data != null && data.length > 0 && (
                <>
                  {settings && settings.pick_lotType === 'barcode' ? (
                    <Barcode value={data} width={1} height={30} margin={0} displayValue={false} />
                  ) : (
                    data
                  )}
                </>
              )}
              {data == null && <>No lot selected</>}
            </div>
          )
        },
      },
      {
        title: 'LOCATION',
        dataIndex: isWorkOrder ? 'location' : 'locationNames',
        align: 'center',
        key: 'locationNames',
        render: (locationNames: string[], record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let ret = ''
          if (sellerSetting.company && sellerSetting.company.locationMethod == 1 && record.itemLocation) {
            return record.itemLocation
          } else if (locationNames && locationNames.length > 0) {
            ret = locationNames.join(', ')
            if (locationNames.length > 5) {
              ret += ', ...'
            }
          }
          return ret
        },
      },
      {
        title: 'PICKED',
        dataIndex: 'status',
        key: 'status',
        align: 'center',
        width: 60,
        render: (status: string, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          //old code
          //return status !== 'PLACED' && status !== 'NEW' ? (record.picked ? record.picked : 0) : ''
          if (status == 'PLACED') {
            return record.picked !== 0 && record.picked !== undefined ? formatNumber(record.picked, 2) : ''
          } else if (status !== 'NEW') {
            return record.picked && record.picked !== 0 ? formatNumber(record.picked, 2) : ''
          }
        },
      },
      {
        title: '',
        dataIndex: 'isSpecialCut',
        width: 10,
        render: (isSpecialCut: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return isSpecialCut > 0 ? <IconSvg viewBox="0 0 24 24" width="15" height="15" type="star" /> : null
        },
      },
      {
        title: 'NOTE',
        dataIndex: 'note',
        key: 'note',
        align: 'left',
        width: 100,
        render: (note: string, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return <EllipseText>{!note || (note && note.toLowerCase() == 'null') ? '' : note}</EllipseText>
        },
      },
    ]

    if (_.get(this.state, 'pagePrintSetting.weight', false)) {
      columns.splice(7, 0, {
        title: 'UNIT WT.',
        dataIndex: 'grossWeight',
        align: 'center',
        width: 60,
        render: (grossWeight: number) => {
          if (!grossWeight) return
          return grossWeight
        },
      })
    }

    if (_.get(this.state, 'pagePrintSetting.weight', false)) {
      columns.splice(8, 0, {
        title: 'WT.',
        dataIndex: 'grossWeight',
        align: 'center',
        width: 60,
        render: (grossWeight: number, record: any) => {
          if (!_.multiply(grossWeight || 0, record.quantity)) return
          return  `${_.multiply(grossWeight, record.quantity).toFixed(0)}`
        },
      })
    }

    if (_.get(this.state, 'pagePrintSetting.volume', false)) {
      columns.splice(_.get(this.state, 'pagePrintSetting.weight', false) ? 9 : 7, 0, {
        title: 'UNIT VOL.',
        dataIndex: 'grossVolume',
        align: 'center',
        width: 60,
        render: (grossVolume: number) => {
          if (!grossVolume) return
          return  grossVolume
        },
      })
    }

    if (_.get(this.state, 'pagePrintSetting.volume', false)) {
      columns.splice(_.get(this.state, 'pagePrintSetting.weight', false) ? 10 : 8, 0, {
        title: 'VOL.',
        dataIndex: 'grossVolume',
        align: 'center',
        width: 60,
        render: (grossVolume: number, record: any) => {
          if (!_.multiply(grossVolume || 0, record.quantity)) return
          return  `${_.multiply(grossVolume, record.quantity).toFixed(0)}`
        },
      })
    }

    if (isWorkOrder === true) {
      columns.splice(5, 1)
    }

    const isLogoEnabled = !settings || (settings && (settings.pick_logo || typeof settings.pick_logo === 'undefined'))
    let logo = this.props.logo
    if (logo && logo !== 'default') {
      if (!location.origin.includes('www')) {
        logo = logo.replace('www.', '')
      }
    }

    let accountMgr = ''
    if (this.props.orderId) {
      if (isWorkOrder === true && currentOrder) {
        accountMgr = currentOrder.user.firstName + ' ' + currentOrder.user.lastName
      } else if (currentOrder.wholesaleClient && currentOrder.wholesaleClient.accountant) {
        accountMgr =
          currentOrder.wholesaleClient.accountant.firstName + ' ' + currentOrder.wholesaleClient.accountant.lastName
      }
    }

    const RenderHeaderBox = () => {
      if (this.props.currentPrintContainerId) {
        const allItems = _.flatten(_.get(this.state, 'containerData.wholesaleOrderList', []).map(v => v.wholesaleOrderItemList))
        const totalUnits = _.reduce(allItems, (sum, n) => sum + n.quantity, 0)
        const totalWeight = _.reduce(allItems, (sum, n) => sum + (n.quantity * n.grossWeight || 0), 0)
        const totalVolume = _.reduce(allItems, (sum, n) => sum + (n.quantity * n.grossVolume || 0), 0)

        return (
          <HeaderBox>
            <Row gutter={24}>
              <Col span={6} style={{ textAlign: 'left', paddingLeft: 18 }}>
                <CompanyInfoWrapper>
                  {isLogoEnabled && (
                    <>
                      {logo === 'default' ? (
                        <LogoWrapper>
                          <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                        </LogoWrapper>
                      ) : (
                        <img src={logo} onLoad={this.onImageLoad.bind(this)} />
                      )}
                    </>
                  )}
                  <span style={{ fontSize: 14, textAlign: isLogoEnabled ? 'center' : 'left' }}>
                    {_.get(this.props, 'companyName', '').replace(' ', '&nbsp;')}
                  </span>
                </CompanyInfoWrapper>
              </Col>
              <Col offset={2} span={12} style={{ textAlign: 'center' }}>
                <h1>Container&nbsp;Load&nbsp;Sheet</h1>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={14} style={{ paddingLeft: 18 }}>
                <Row gutter={24}>
                  <Col span={24}>
                    <Flex>
                      <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                        CONTAINER NAME:
                      </HeaderTextLeftBox>
                      <HeaderTextLeftBox className="picksheet">
                        {this.state.containerData.containerName}
                      </HeaderTextLeftBox>
                    </Flex>
                  </Col>
                  <Col span={24}>
                    <Flex>
                      <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                        VESSEL NAME:
                      </HeaderTextLeftBox>
                      <HeaderTextLeftBox className="picksheet">
                        {this.state.containerData.logisticOrderName}
                      </HeaderTextLeftBox>
                    </Flex>
                  </Col>
                  <Col span={24}>
                    <Flex>
                      <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                        ETD:
                      </HeaderTextLeftBox>
                      <HeaderTextLeftBox className="picksheet">
                        <Moment format="MM/DD/YYYY HH:mm:ss" date={moment.utc(this.state.containerData.etdDate)} />
                      </HeaderTextLeftBox>
                    </Flex>
                  </Col>
                  <Col span={24}>
                    <Flex>
                      <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                        BOOKING NUMBER:
                      </HeaderTextLeftBox>
                      <HeaderTextLeftBox className="picksheet">{this.state.containerData.bookingNo}</HeaderTextLeftBox>
                    </Flex>
                  </Col>
                </Row>
              </Col>
              <Col span={10} style={{ paddingLeft: 18 }}>
                {this.props.orderId && (
                  <>
                    <Flex>
                      <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                        TARGET FULFILLMENT:
                      </HeaderTextLeftBox>
                      <HeaderTextLeftBox className="picksheet">
                        <Moment format="MM/DD/YY" date={moment.utc(currentOrder.deliveryDate)} />
                      </HeaderTextLeftBox>
                    </Flex>
                    <Flex>
                      <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                        PRINT VERSION:
                      </HeaderTextLeftBox>
                      <HeaderTextLeftBox className="picksheet">
                        {currentOrder.printVersion ? currentOrder.printVersion : <span>&nbsp;</span>}
                      </HeaderTextLeftBox>
                    </Flex>
                  </>
                )}
                <Flex>
                  <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                    PRINT TIME:
                  </HeaderTextLeftBox>
                  <HeaderTextLeftBox className="picksheet">
                    <Moment format="MM/DD/YY h:mm A" date={_moment().format()} />
                  </HeaderTextLeftBox>
                </Flex>
                <Flex>
                  <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                    ACCOUNT MGR.:
                  </HeaderTextLeftBox>
                  <HeaderTextLeftBox className="picksheet">
                    {accountMgr !== '' && <HeaderTextLeftBox className="picksheet">{accountMgr}</HeaderTextLeftBox>}
                  </HeaderTextLeftBox>
                </Flex>
                <Flex>
                  <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                    TOTAL UNITS:
                  </HeaderTextLeftBox>
                  <HeaderTextLeftBox className="picksheet">
                    <HeaderTextLeftBox className="picksheet">{totalUnits}</HeaderTextLeftBox>
                  </HeaderTextLeftBox>
                </Flex>
                <Flex>
                  <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                    TOTAL WEIGHT:
                  </HeaderTextLeftBox>
                  <HeaderTextLeftBox className="picksheet">
                    {_.get(this.state, 'pagePrintSetting.weight', false) && <HeaderTextLeftBox className="picksheet">{totalWeight.toFixed(0)} LB</HeaderTextLeftBox>}
                  </HeaderTextLeftBox>
                </Flex>
                <Flex>
                  <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                    TOTAL VOLUME:
                  </HeaderTextLeftBox>
                  <HeaderTextLeftBox className="picksheet">
                    {_.get(this.state, 'pagePrintSetting.volume', false) && <HeaderTextLeftBox className="picksheet">{totalVolume.toFixed(0)} FT³</HeaderTextLeftBox>}
                  </HeaderTextLeftBox>
                </Flex>
              </Col>
            </Row>
          </HeaderBox>
        )
      }
      const totalWeight = _.reduce(items, (sum, n) => sum + (n.quantity * n.grossWeight || 0), 0)
      const totalVolume = _.reduce(items, (sum, n) => sum + (n.quantity * n.grossVolume || 0), 0)
      return (
        <HeaderBox>
          <Row gutter={24}>
            <Col span={6} style={{ textAlign: 'left', paddingLeft: 18 }}>
              <CompanyInfoWrapper>
                {isLogoEnabled && (
                  <>
                    {logo === 'default' ? (
                      <LogoWrapper>
                        <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                      </LogoWrapper>
                    ) : (
                      <img src={logo} onLoad={this.onImageLoad.bind(this)} />
                    )}
                  </>
                )}
                <span style={{ fontSize: 14, textAlign: 'left' }}>{this.props.companyName}</span>
              </CompanyInfoWrapper>
            </Col>
            <Col offset={2} span={8} style={{ textAlign: 'center' }}>
              <h1>{settings && settings.pick_title ? settings.pick_title.toUpperCase() : 'PICK SHEET'}</h1>
            </Col>
            <Col span={7} style={{ textAlign: 'right' }}>
              {(!settings || (settings && (settings.pick_barcode || typeof settings.pick_barcode === 'undefined'))) && (
                <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} height={90} />
              )}
            </Col>
            <Col span={1} />
          </Row>

          <Row gutter={24}>
            <Col span={12} style={{ paddingLeft: 18 }}>
              <Flex>
                <HeaderTextLeftBox className="picksheet" style={{ width: 170 }}>
                  SHIP TO:
                </HeaderTextLeftBox>
                <HeaderTextLeftBox className="picksheet f16">
                  {currentOrder.wholesaleClient ? (
                    currentOrder.wholesaleClient.clientCompany.companyName
                  ) : (
                    <span>&nbsp;</span>
                  )}
                </HeaderTextLeftBox>
              </Flex>
            </Col>
            <Col span={1} />
            <Col span={5} style={{ padding: 0 }}>
              <HeaderTextLeftBox className="picksheet">{isWorkOrder ? 'WORK ORDER:' : 'ORDER NO.'} </HeaderTextLeftBox>
            </Col>
            <Col span={5}>
              <HeaderTextLeftBox className="picksheet f16">
                {getOrderPrefix(sellerSetting, 'sales')}
                {isCustomOrderNumberEnabled(sellerSetting) && currentOrder.customOrderNo
                  ? currentOrder.customOrderNo
                  : currentOrder.wholesaleOrderId}
              </HeaderTextLeftBox>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={12} style={{ paddingLeft: 18 }}>
              <Flex>
                <HeaderTextLeftBox style={{ width: 170 }} className="picksheet">
                  SHIPPING ADDRESS:
                </HeaderTextLeftBox>
                {currentOrder.shippingAddress != null ? (
                  <div style={{ whiteSpace: 'pre-wrap' }}>
                    <HeaderTextLeftBox className="picksheet">
                      {currentOrder.shippingAddress && currentOrder.shippingAddress.address ? (
                        formatAddress(currentOrder.shippingAddress.address, true)
                      ) : (
                        <span>&nbsp;</span>
                      )}
                    </HeaderTextLeftBox>
                    <HeaderTextLeftBox className="picksheet">
                      {currentOrder.wholesaleClient.mainShippingAddress &&
                      currentOrder.wholesaleClient.mainShippingAddress.phone
                        ? currentOrder.wholesaleClient.mainShippingAddress.phone
                        : currentOrder.wholesaleClient.mobilePhone}
                    </HeaderTextLeftBox>
                  </div>
                ) : (
                  <HeaderTextLeftBox className="picksheet">N/A</HeaderTextLeftBox>
                )}
              </Flex>
            </Col>
            <Col span={1} />
            <Col span={5} style={{ padding: 0 }}>
              {(!settings ||
                (settings && (settings.pick_customerPONo || typeof settings.pick_customerPONo === 'undefined'))) && (
                <HeaderTextLeftBox className="picksheet">CUSTOMER PO NO.</HeaderTextLeftBox>
              )}
              <HeaderTextLeftBox className="picksheet">TARGET FULFILLMENT: </HeaderTextLeftBox>
              {(!settings ||
                (settings && (settings.pick_driverName || typeof settings.pick_driverName === 'undefined'))) && (
                <HeaderTextLeftBox className="picksheet">DRIVER: </HeaderTextLeftBox>
              )}
              <HeaderTextLeftBox className="picksheet">PRINT VERSION: </HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">PRINT TIME: </HeaderTextLeftBox>
              {accountMgr !== '' && <HeaderTextLeftBox className="picksheet">ACCOUNT MGR.: </HeaderTextLeftBox>}
              {(!settings ||
                (settings && (settings.pick_salesRep || typeof settings.pick_salesRep === 'undefined'))) && (
                <HeaderTextLeftBox className="picksheet">SALES REP: </HeaderTextLeftBox>
              )}
              {_.get(this.state, 'pagePrintSetting.weight', false) && <HeaderTextLeftBox className="picksheet">TOTAL WEIGHT: </HeaderTextLeftBox>}
              {_.get(this.state, 'pagePrintSetting.volume', false) && <HeaderTextLeftBox className="picksheet">TOTAL VOLUME: </HeaderTextLeftBox>}
            </Col>
            <Col span={5} style={{ paddingRight: 0 }}>
              {(!settings ||
                (settings && (settings.pick_customerPONo || typeof settings.pick_customerPONo === 'undefined'))) && (
                <HeaderTextLeftBox className="picksheet">{currentOrder.reference ?? <>&nbsp;</>}</HeaderTextLeftBox>
              )}
              <HeaderTextLeftBox className="picksheet">
                <Moment format="MM/DD/YY" date={moment.utc(currentOrder.deliveryDate)} />
              </HeaderTextLeftBox>
              {(!settings ||
                (settings && (settings.pick_driverName || typeof settings.pick_driverName === 'undefined'))) && (
                <HeaderTextLeftBox className="picksheet">
                  {currentOrder.overrideDriver ? (
                    currentOrder.overrideDriver.firstName + ' ' + currentOrder.overrideDriver.lastName
                  ) : currentOrder.defaultDriver ? (
                    currentOrder.defaultDriver.firstName + ' ' + currentOrder.defaultDriver.lastName
                  ) : (
                    <span>N/A</span>
                  )}
                </HeaderTextLeftBox>
              )}
              <HeaderTextLeftBox className="picksheet">
                {currentOrder.printVersion ? currentOrder.printVersion : <span>&nbsp;</span>}
              </HeaderTextLeftBox>
              <HeaderTextLeftBox className="picksheet">
                <Moment format="MM/DD/YY h:mm A" date={_moment().format()} />
              </HeaderTextLeftBox>
              {accountMgr !== '' && <HeaderTextLeftBox className="picksheet">{accountMgr}</HeaderTextLeftBox>}
              {(!settings ||
                (settings && (settings.pick_salesRep || typeof settings.pick_salesRep === 'undefined'))) && (
                <HeaderTextLeftBox className="picksheet">
                  {currentOrder.seller ? `${currentOrder.seller.firstName} ${currentOrder.seller.lastName}` : ''}
                </HeaderTextLeftBox>
              )}
              {_.get(this.state, 'pagePrintSetting.weight', false) && <HeaderTextLeftBox className="picksheet">{totalWeight.toFixed(0)} LB</HeaderTextLeftBox>}
              {_.get(this.state, 'pagePrintSetting.volume', false) && <HeaderTextLeftBox className="picksheet">{totalVolume.toFixed(0)} FT³</HeaderTextLeftBox>}
            </Col>
          </Row>
          {currentOrder.pickerNote && (
            <Row gutter={24}>
              <Col span={12} style={{ paddingLeft: 18 }}>
                <Flex>
                  <HeaderTextLeftBox style={{ width: 135 }} className="picksheet">
                    NOTES:
                  </HeaderTextLeftBox>
                  <HeaderTextLeftBox className="picksheet">{currentOrder.pickerNote}</HeaderTextLeftBox>
                </Flex>
              </Col>
            </Row>
          )}
        </HeaderBox>
      )
    }

    const RenderTable = () => {
      if (this.props.currentPrintContainerId) {
        return _.get(this.state, 'containerData.wholesaleOrderList', []).map((v, index) => {
          const tableOrderItemsList = v.wholesaleOrderItemList.map((l) => ({
            ...l,
            displayOrderProduct: ~~l.displayOrder,
          }))
          const groupByDisplayOrder = _.groupBy(tableOrderItemsList, 'displayOrderProduct')
          const items = Object.keys(groupByDisplayOrder).map((g) => ({
            ...groupByDisplayOrder[g][0],
            quantity: _.reduce(groupByDisplayOrder[g], (sum, n) => sum + n.quantity, 0),
            picked: _.reduce(groupByDisplayOrder[g], (sum, n) => sum + n.picked, 0),
            catchWeightQty: _.reduce(groupByDisplayOrder[g], (sum, n) => sum + n.catchWeightQty, 0),
            lotIds: groupByDisplayOrder[g].map((v) => v.lotId),
          }))
          return (
            <div key={index} style={{ marginTop: 30 }}>
              <p style={{ color: '#000'}}>
                <span style={{ marginRight: 300 }}>SHIP TO: {v.customerName}</span>
                ORDER NO. {getOrderPrefix(sellerSetting, 'sales')}
                {v.wholesaleOrderId}
              </p>
              <PrintContainerLoadSheet sellerSetting={sellerSetting} tableClass={tableClass} items={_.sortBy(items, 'displayOrderProduct')} />
            </div>
          )
        })
      }

      const { pagePrintSetting } = this.state

      const defaultItems = items.filter((orderItem: any) => {
        const uom = orderItem.UOM ? orderItem.UOM : orderItem.inventoryUOM
        return (uom == orderItem.defaultSellingPricingUOM)
      });
      const looseItems = items.filter((orderItem: any) => {
        const uom = orderItem.UOM ? orderItem.UOM : orderItem.inventoryUOM
        return (uom != orderItem.defaultSellingPricingUOM)
      });

      return (
        <PrintPickSheetTable className="pick-sheet">
          <Table
            className={tableClass}
            rowKey="wholesaleOrderItemId"
            columns={this.getDisplayingColumns(columns)}
            rowClassName={(record, index) => {
              let rowOrderItemId = record.wholesaleOrderItemId
              if (this.checkWeights(record)) {
                rowOrderItemId = record.relatedOrderItemId
              }
              const i = orderItems.findIndex((oi) => oi.wholesaleOrderItemId === rowOrderItemId)
              return i % 2 === 1 ? 'alternative' : ''
            }}
            dataSource={pagePrintSetting && pagePrintSetting.separateLooseItemsTable === true ? defaultItems : items}
            pagination={false}
          />

          {pagePrintSetting && pagePrintSetting.separateLooseItemsTable === true && (
            <div>
              <HeaderTextLeftBox style={{ color: 'black', fontSize: 18, marginTop: 25, marginBottom: 5 }}>Loose Items</HeaderTextLeftBox>
              <Table
                className={tableClass}
                rowKey="wholesaleOrderItemId"
                columns={this.getDisplayingColumns(columns)}
                rowClassName={(record, index) => {
                  let rowOrderItemId = record.wholesaleOrderItemId
                  if (this.checkWeights(record)) {
                    rowOrderItemId = record.relatedOrderItemId
                  }
                  const i = orderItems.findIndex((oi) => oi.wholesaleOrderItemId === rowOrderItemId)
                  return i % 2 === 1 ? 'alternative' : ''
                }}
                dataSource={looseItems}
                pagination={false}
              />
            </div>
          )}
        </PrintPickSheetTable>
      )
    }

    return (
      <PrintPickSheetWrapper>
        <RenderHeaderBox />
        {RenderTable()}
      </PrintPickSheetWrapper>
    )
  }
}

const PrintContainerLoadSheet: FC<any> = ({ sellerSetting, tableClass, items }) => {
  const [pagePrintSetting, setPagePrintSetting] = useState(null)
  useEffect(() => {
    const pageSetting = JSON.parse(sellerSetting.company.printSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`pick_`) > -1) {
        const newKey = el.substring(`pick_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    setPagePrintSetting(data)
  }, [])

  const columns = [
    {
      title: 'ORDERED',
      dataIndex: 'quantity',
      align: 'center'
    },
    {
      title: 'PICKED',
      align: 'center',
      render: (r) => {
        if (!r.picked || r.picked === 0) {
          return<span><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></span>
        }
        else {
          return <span style={{ paddingBottom: 5, borderBottom: '2px solid', minWidth: 30, display: 'inline-block' }}>{r.picked}</span>
        }
      }
    },
    {
      title: 'PRODUCT',
      dataIndex: 'itemName',
    },
    {
      title: 'SIZE',
      dataIndex: 'size',
    },
]

  if (_.get(pagePrintSetting, 'volume', false)) {
    columns.splice(4, 0, {
      title: 'UNIT VOL.',
      align: 'right',
      render: (r) => {
        if (!r.grossVolume) return
        return <span>{r.grossVolume}</span>
      }
    })
  }

  if (_.get(pagePrintSetting, 'volume', false)) {
    columns.splice(5, 0, {
      title: 'VOL.',
      align: 'right',
      render: (r) => {
        if (!r.quantity || !r.grossVolume) return
        return <span>{formatNumber(_.multiply(r.quantity, r.grossVolume || 0), 1)}</span>
      }
    })
  }

  if (_.get(pagePrintSetting, 'weight', false)) {
    columns.splice(_.get(pagePrintSetting, 'volume', false) ? 6 : 4, 0, {
      title: 'UNIT WT.',
      align: 'right',
      render: (r) => {
        if (!r.grossWeight) return
        return <span>{r.grossWeight}</span>
      }
    })
  }

  if (_.get(pagePrintSetting, 'weight', false)) {
    columns.splice(_.get(pagePrintSetting, 'volume', false) ? 7 : 5, 0, {
      title: 'WT.',
      align: 'right',
      render: (r) => {
        if (!r.quantity || !r.grossWeight) return
        return <span>{formatNumber(_.multiply(r.quantity, r.grossWeight || 0), 1)}</span>
      }
    })
  }

  return (
    <PrintPickSheetTable className="pick-sheet">
      <Table
        className={tableClass}
        rowKey="wholesaleOrderItemId"
        columns={columns}
        rowClassName={(record, index) => {
          return index % 2 === 1 ? 'alternative' : ''
        }}
        dataSource={items}
        pagination={false}
      />
    </PrintPickSheetTable>
  )
}

export default PrintPickSheet
