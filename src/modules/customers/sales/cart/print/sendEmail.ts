import $ from 'jquery'
import html2canvas from 'html2canvas'
import jsPDF from 'jspdf'
import { notification } from 'antd'

const sendEmail = (values: {
  title: string
  contentType?: 'BOL' | 'invoice'
  content: string | Blob
  cc: any[]
  orderId: string | Blob
  receiver: string | Blob
  elementId: string
  handleSuccess: (arg0: boolean) => void
  layout: string
}) => {
  html2canvas(document.getElementById(values.elementId), {
    dpi: 172,
    allowTaint: true,
    scale: 2,
    background: '#fff',
    useCORS: true,
    height: document.getElementById(values.elementId)?.scrollHeight,
    width: document.getElementById(values.elementId)?.scrollWidth,
  }).then((canvas) => {
    const pdf = new jsPDF(values.layout === 'portrait' ? 'p' : 'l', 'mm', 'a4')
    let ctx = canvas.getContext('2d'),
      a4w = values.layout === 'portrait' ? 198 : 277,
      a4h = values.layout === 'portrait' ? 277 : 198,
      imgHeight = Math.floor((a4h * canvas.width) / a4w),
      renderedHeight = 0

    while (renderedHeight < canvas.height) {
      const page = document.createElement('canvas')
      page.width = canvas.width
      page.height = Math.min(imgHeight, canvas.height - renderedHeight)

      page
        ?.getContext('2d')
        .putImageData(
          ctx.getImageData(0, renderedHeight, canvas.width, Math.min(imgHeight, canvas.height - renderedHeight)),
          0,
          0,
        )
      pdf.addImage(
        page.toDataURL('image/jpeg', 1.0),
        'JPEG',
        6,
        6,
        a4w,
        Math.min(a4h, (a4w * page.height) / page.width),
      )

      renderedHeight += imgHeight
      if (renderedHeight < canvas.height) {
        pdf.addPage()
      }
    }

    const filename = values.title + '.pdf'
    const pdffile = pdf.output('datauristring')
    const blob = dataURLtoFile(pdffile, filename)
    /*const myfile = blobToFile(blob, filename);*/
    const formData = new FormData()
    formData.append('attachment', blob)
    formData.append('content', values.content)
    formData.append('cc', values.cc.join(','))
    formData.append('orderId', values.orderId)
    formData.append('receiver', values.receiver)

    values.contentType && formData.append('contentType', values.contentType)

    $.ajax({
      url: process.env.WSW2_HOST + '/api/v1/wholesale/inventory/invoice-email',
      type: 'POST',
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
      success: (res: any) => {
        values.handleSuccess(false)
        if (res.body.message) {
          const description = res.body.message.replace('invoice', values.title)
          notification.success({
            message: 'SUCCESS',
            description,
            duration: 5,
          })
        }
      },
    })
    // OrderService.instance.sendEmailPdf(formData).subscribe({
    //   next(res: any) {
    //     of(responseHandler(res, false).body.data)
    //   },
    //   error(err) {
    //     checkError(err)
    //   },
    //   complete() { },
    // })
  })
}

function dataURLtoFile(dataurl: any, filename: string) {
  const arr = dataurl.split(',')
  const mime = arr[0].match(/:(.*?);/)[1]
  const bstr = atob(arr[1])
  let n = bstr.length
  const u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new File([u8arr], filename, { type: mime })
}

export const url2base64 = async (url: RequestInfo) => {
  const data = await fetch(url)
  const blob = await data.blob()
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.readAsDataURL(blob)
    reader.onloadend = () => {
      const base64data = reader.result
      resolve(base64data)
    }
  })
}

export default sendEmail
