import React from 'react'
import { OrderDetail, OrderItem } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode'
import {
  HeaderTextRightBox,
  HeaderTextLeftBox,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
  ModalDetailText,
  UnderLineWrapper,
  Underline,
  DeliverySction,
  DeliveryListTable,
  WeightSpan,
  WeightWrapper,
  LightColoredRow,
  pl24,
  DisplayLabel,
  DisplayValue,
  DisplayLeftLabel,
  InfoLabel,
  InfoValue,
  SelectFreshInvoiceWrapper,
  SelectFreshInvoiceBody,
  SelectFreshInvoiceBillTo,
  SelectFreshInvoiceShipTo,
  SelectFreshInvoiceNumber,
  SelectFreshInvoiceDate,
  SelectFreshInvoiceTerms,
  SelectFreshInvoiceRep,
  SelectFreshInvoiceShipMethod,
  SelectFreshInvoiceLineItems,
  SelectFreshInvoiceItemRow,
  SelectFreshGrandTotal,
  SelectFreshPageSection,
  SelectFreshPrintableBodyWrapper,
  SelectFreshWeightSpan
} from '~/modules/customers/sales/cart/print/_styles'
import Moment from 'react-moment'
import {
  formatNumber,
  formatAddress,
  basePriceToRatioPrice,
  mathRoundFun,
  getOrderPrefix,
  inventoryQtyToRatioQty,
  ratioQtyToInventoryQty,
  numberMultipy,
  judgeConstantRatio,
  formatItemDescription,
  isCustomOrderNumberEnabled,
  checkError,
} from '~/common/utils'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import { Icon } from '~/components'
import { timingFunctions } from 'polished'
import { timeout } from 'rxjs/operators'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import _, { cloneDeep } from 'lodash'
import moment from 'moment'
import { PrintSetting } from '~/modules/setting/components/print-setting'
import { connect } from 'react-redux'
import { url2base64 } from './sendEmail'
import { OrderService } from '~/modules/orders/order.service'
import {CACHED_COMPANY} from '~/common'

const selectFreshInvoiceTemplateImg = require('~/modules/customers/8.5_X_5.5_Forms_4_Parts_No_CarbonV2.jpg')

interface PrintBillOfLadingProps {
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  companyName: string
  logo: string
  company: any
  getWeightsByOrderItemIds: Function
  catchWeightValues: any
  title?: string
  multiple?: boolean
  printSetting: any
  type: string
  changePrintLogoStatus: Function
  fulfillmentOptionType?: number
  sellerSetting?: any
  paymentTerms: any[]
  selectedType?: number
  currentPrintContainerId: string
  orderId: string
  oneOffItemList: OrderItem[]
}

export class ViewSalesConfirmation extends React.PureComponent<PrintBillOfLadingProps> {
  state = {
    totalQuantity: 0,
    totalAmount: 0,
    totalWeight: 0,
    totalTax: 0,
    totalSubtotal: 0,
    visibleAlert: false,
    selectedRowKeys: [],
    orderItems: this.props.orderItems,
    pagePrintSetting: null,
    logo: '',
    containerData: {},
    //overrideSelectFresh: false,
    overrideCustomLayout: false,
  }

  componentDidMount(): void {
    const _this = this
    this.getAmount(this.props.orderItems)
    if (this.props.printSetting) {
      this.setPrintSetting(this.props.printSetting, this.props.type)
    }
    if (this.props.logo) {
      url2base64(this.props.logo.replace('www.', '')).then((logo) => {
        this.setState({ logo })
      })
    }
    if (this.props.currentPrintContainerId && this.props.orderId) {
      if (this.props.currentOrder.fulfillmentType === 5) {
        OrderService.instance.getSingleContainerPrint(this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: {
                ...resp.body.data,
                tempRecorderList: JSON.parse(resp.body.data.tempRecorderList)
              }
            })
          },
          error(err) { checkError(err) },
        })
      } else {
        OrderService.instance.getPrintContainer(this.props.currentPrintContainerId, this.props.orderId).subscribe({
          next(resp) {
            _this.setState({
              containerData: resp.data
            })
          },
          error(err) { checkError(err) },
        })
      }
    }
    if (this.props.currentPrintContainerId && !this.props.orderId) {
      OrderService.instance.getPrintContainer(this.props.currentPrintContainerId).subscribe({
        next(resp) {
          _this.setState({
            containerData: resp.data
          })
        },
        error(err) { checkError(err) },
      })
    }
  }

  componentWillReceiveProps(nextProps: PrintBillOfLadingProps) {
    if (this.props.orderItems !== nextProps.orderItems) {
      this.getAmount(nextProps.orderItems)
      this.setState({ orderItems: nextProps.orderItems })
    }

    if (this.props.printSetting != nextProps.printSetting && nextProps.printSetting != null) {
      this.setPrintSetting(nextProps.printSetting, nextProps.type)
    }
  }

  setPrintSetting = (jsonSetting: string, modalType: string) => {
    const pageSetting = JSON.parse(jsonSetting)
    const keys = Object.keys(pageSetting)
    let data = {}
    keys.forEach((el) => {
      if (el.indexOf(`${modalType}_`) > -1) {
        const newKey = el.substring(`${modalType}_`.length)
        data[newKey] = pageSetting[el]
      }
    })
    this.setState({ pagePrintSetting: data })
  }

  getAmount = (items: any) => {
    let _totalQuantity = 0
    let _totalWeight = 0
    let _totalTax =  0
    let _totalSubtotal = 0
    let _totalAmount = 0

    for (let i = 0; i < items.length; i++) {
      let taxRate = _.toNumber(items[i].taxRate ? items[i].taxRate : 0)
      let taxRateRounded = mathRoundFun(taxRate / 100, 6)
      let constantRatio = judgeConstantRatio(items[i])
      let picked = _.toNumber(items[i].picked)
      let subtotal = 0
      let tax = 0
      let taxEnabled = items[i].taxEnabled

      let pricingUOM = items[i].pricingUOM ? items[i].pricingUOM : items[i].UOM
      let unitUOM = items[i].UOM
      let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(items[i].price), items[i], 2)

      if (items[i].oldCatchWeight) {
        _totalWeight += _.toNumber(items[i].catchWeightQty)
        subtotal = numberMultipy(_.toNumber(items[i].price), _.toNumber(items[i].quantity))
      } else {
        if (!constantRatio) {
          _totalWeight += inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].catchWeightQty), items[i])
          subtotal = numberMultipy(
            ratioPrice,
            inventoryQtyToRatioQty(pricingUOM, _.toNumber(items[i].quantity), items[i], 12),
          )
        } else {
          const quantity = inventoryQtyToRatioQty(
            pricingUOM,
            ratioQtyToInventoryQty(unitUOM, _.toNumber(items[i].quantity), items[i], 12),
            items[i],
            2,
          )
          _totalWeight += quantity
          subtotal = numberMultipy(ratioPrice, quantity)
        }
      }

      tax = taxEnabled ? subtotal * taxRateRounded : 0
      subtotal = Math.round(subtotal * 100) / 100
      items[i]['amount'] = '$' + formatNumber(subtotal, 2)

      _totalQuantity += picked
      _totalSubtotal += subtotal
      _totalTax += tax
      _totalAmount += subtotal + tax
    }

    this.setState({
      totalQuantity: _totalQuantity,
      totalAmount: _totalAmount,
      totalWeight: _totalWeight,
      totalTax: _totalTax,
      totalSubtotal: _totalSubtotal,
      orderItems: items,
    })
  }

  getExpandableRowKeys = () => {
    const { orderItems } = this.state
    let result: any[] = []
    orderItems.forEach((el) => {
      if (!el.constantRatio) {
        result.push(el.wholesaleOrderItemId)
      }
    })
    return result
  }

  renderCatchWeightValues = (weights: any[]) => {
    // const { catchWeightValues, multiple } = this.props

    // let weights: any[] = []
    // if (multiple && typeof record.catchWeightValues !== 'undefined') {
    //   weights = record.catchWeightValues
    // } else {
    //   weights = catchWeightValues[record.wholesaleOrderItemId] ? catchWeightValues[record.wholesaleOrderItemId] : []
    // }
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (weights.length > 0) {
      let result: any[] = []
      let sub: any[] = []
      weights.forEach((el: any, index: number) => {
        if (index > 0 && (index - 10) % 10 == 0) {
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
          sub = []
        }

        const weightEl = <WeightSpan>{el.unitWeight ? el.unitWeight : ''}</WeightSpan>
        if (index == weights.length - 1) {
          sub.push(weightEl)
          result.push(<WeightWrapper>{sub}</WeightWrapper>)
        } else {
          sub.push(weightEl)
        }
      })
      return result
    }

    return []
  }

  checkWeights = (row: any) => {
    const { type } = this.props
    const { pagePrintSetting } = this.state
    if (type == 'invoice' && pagePrintSetting && !pagePrintSetting.catchWeightValues) {
      return false
    }
    return typeof row.weights !== 'undefined' && row.weights.length > 0
  }

  getDisplayingColumns = (columns: any[]) => {
    const { pagePrintSetting } = this.state
    let newColumns: any[] = []
    if (!pagePrintSetting) return columns

    columns.forEach((el) => {
      const key = el.key
      if (pagePrintSetting[key] !== false) {
        newColumns.push(el)
      }
    })

    return [
      {
        title: this.getNameAndOrderNo(),
        className: 'name-order',
        children: newColumns,
      },
    ]
    // return newColumns
  }

  getNameAndOrderNo = () => {
    const { currentOrder, sellerSetting } = this.props
    const { pagePrintSetting } = this.state
    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'
    const customerPO = pagePrintSetting?pagePrintSetting['primary_number']?pagePrintSetting['primary_number'] == "customerPO#"?true:false:false:false
    return (
      <HeaderTextRightBox style={{ color: 'black' }}>
        {customerPO?
        <>
          {clientName} | #{currentOrder.reference}
        </>
        :
        <>
        {clientName} | #{getOrderPrefix(sellerSetting, 'sales')}
        {currentOrder.wholesaleOrderId}</>}
      </HeaderTextRightBox>
    )
  }

  onImageLoad = () => {
    this.props.changePrintLogoStatus()
  }

  render() {
    const {
      currentOrder,
      title,
      type,
      fulfillmentOptionType,
      sellerSetting,
      catchWeightValues,
      multiple,
      printSetting,
      company,
      paymentTerms,
    } = this.props
    const { orderItems, pagePrintSetting } = this.state
    const carrierSetting = pagePrintSetting ? pagePrintSetting.carrier_reference : false
    const origin = pagePrintSetting ? pagePrintSetting.origin : false
    const soldto = pagePrintSetting ? pagePrintSetting.soldto : false
    const shipto = pagePrintSetting ? pagePrintSetting.shipto : false

    let bUseSelectFreshInvoice = false;
    let bUseStandardInvoice = true;

    const companyName = this.props.companyName;

    //DEBUG
    //const companyName = 'Select Fresh Produce'
    //END DEBUG

    if (companyName === 'Select Fresh Produce' || companyName === 'Select Fresh, Inc' || companyName === 'WholesaleWare Demo 2')
    {
      if (!this.props.overrideCustomLayout) {
        bUseSelectFreshInvoice = true;
        bUseStandardInvoice = false;
      }
    }

    let tableClass = ''
    if (printSetting) {
      const setting = JSON.parse(printSetting)
      if (
        (type === 'invoice' && setting.invoice_alternateRow === true) ||
        (type !== 'invoice' && setting.bill_alternateRow === true)
      ) {
        tableClass = 'alternate-row'
      }
    }
    const filteredOrderItems = orderItems.filter(el=>el.quantity > 0)
    let items = cloneDeep(filteredOrderItems)
    filteredOrderItems.forEach((record, i) => {
      const index = items.findIndex((item) => item.wholesaleOrderItemId === record.wholesaleOrderItemId)
      items[index].key = i
      if (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightValues)) {
        let showCatchWeightModal = false
        let constantRatio = judgeConstantRatio(record)
        if (!constantRatio) {
          showCatchWeightModal = true
        }
        if (showCatchWeightModal) {
          let weights: any[] = []
          if (multiple && typeof record.catchWeightValues !== 'undefined') {
            weights = record.catchWeightValues
          } else {
            weights = catchWeightValues[record.wholesaleOrderItemId]
              ? catchWeightValues[record.wholesaleOrderItemId]
              : []
          }

          if (weights.length > 0) {
            items.splice(index + 1, 0, {
              // ...cloneDeep(record),
              relatedOrderItemId: record.wholesaleOrderItemId,
              weights,
            })
          }
        }
      }
    })

    // const expandedRowKeys = this.getExpandableRowKeys()
    const columns: Array<any> = [
      // {
      //   title: '#',
      //   dataIndex: 'key',
      //   key: 'index',
      //   align: 'center',
      //   width: 60,
      //   render: (key: number, row: OrderItem) => {
      //     return this.checkWeights(row) ? '' : key + 1
      //   },
      // },
      {
        title: 'ORDERED',
        dataIndex: 'quantity',
        key: 'quantity',
        align: 'center',
        className: 'ordered-quantity',
        width: 70,
        render: (value: number, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return ''
          }

          return mathRoundFun(value, 2)
        }
      },
      {
        title: 'UOM',
        dataIndex: 'UOM',
        key: 'UOM',
        align: 'center',
        width: 70,
      },
      {
        title: 'PRODUCT',
        dataIndex: 'variety',
        key: 'variety',
        align: 'left',
        className: 'th-left',
        render: (variety: string, row: any) => {
          if (this.checkWeights(row) && company && company.isDisablePickingStep === false) {
            return {
              children: this.renderCatchWeightValues(row.weights),
              props: {
                colSpan: 8,
              },
            }
          }
          if (row.itemId) {
            return row.editedItemName ? row.editedItemName : formatItemDescription(variety, row.SKU, sellerSetting, row.customerProductCode)
          }
          const desc = row.chargeDesc ? ` (${row.chargeDesc})` : ''
          return `${variety}${desc}`
        },
      },
      {
        title: 'BRAND',
        dataIndex: 'modifiers',
        key: 'modifiers',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'extraOrigin',
        key: 'extraOrigin',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value
        },
      },
      {
        // wait plu from backend
        title: 'PLU',
        dataIndex: 'plu',
        key: 'PLU',
        align: 'center',
        render: (value: string, row: any) => {
          if (this.checkWeights(row)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          return value ? value : ''
        },
      },
      {
        title: 'BILLABLE QTY',
        dataIndex: 'catchWeightQty',
        key: 'catchWeightQty',
        align: 'right',
        width: 70,
        render: (val: any, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showBillableQty,
            showValue = false
          if (record.picked || record.catchWeightQty) {
            showValue = true
          }

          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM

          let constantRatio = judgeConstantRatio(record)
          if (record.oldCatchWeight) {
            showBillableQty = record.catchWeightQty
          } else {
            if (!constantRatio) {
              showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
            } else {
              showBillableQty = inventoryQtyToRatioQty(
                pricingUOM,
                ratioQtyToInventoryQty(unitUOM, record.picked, record, 12),
                record,
              )
            }
          }

          return showValue ? `${mathRoundFun(showBillableQty, 2)}${pricingUOM != null ? ' ' + pricingUOM : ''}` : ''
        },
      },
      // {
      //   title: 'BILLABLE UOM',
      //   dataIndex: 'UOM',
      //   key: 'UOM',
      //   align: 'center',
      //   render: (price: number, record: any) => {
      //     return record.pricingUOM ? record.pricingUOM : record.baseUOM
      //   },
      // },
      {
        title: 'PRICE',
        dataIndex: 'price',
        align: 'right',
        key: 'price',
        className: 'p4',
        width: 100,
        render: (price: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }
          var uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          var isPricingUOMEnabled =
            !pagePrintSetting ||
            (pagePrintSetting && (pagePrintSetting.pricingUOM || typeof pagePrintSetting.pricingUOM === 'undefined'))
          price = basePriceToRatioPrice(uom, price, record)
          return (
            <div style={{ fontWeight: 500, whiteSpace: 'nowrap' }}>
              ${formatNumber(price, 2)}
              {isPricingUOMEnabled && uom != null ? '/' + uom : ''}
            </div>
          )
        },
      },
      {
        title: 'SUBTOTAL',
        dataIndex: 'quantity',
        key: 'total',
        align: 'right',
        className: 'p4',
        width: 90,
        render: (data: number, record: any) => {
          if (this.checkWeights(record)) {
            return {
              children: '',
              props: {
                colSpan: 0,
              },
            }
          }

          let showValue = false
          let total = 0

          if (record.quantity) {
            showValue = true
          }
          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM
          let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(record.price), record, 2)

          let constantRatio = judgeConstantRatio(record)

          if (!constantRatio) {
            total = numberMultipy(
              ratioPrice,
              inventoryQtyToRatioQty(pricingUOM, _.toNumber(record.quantity), record, 12),
            )
          } else {
            let quantity = inventoryQtyToRatioQty(
              pricingUOM,
              ratioQtyToInventoryQty(unitUOM, _.toNumber(record.quantity), record, 12),
              record,
              2,
            )
            total = numberMultipy(ratioPrice, quantity)
          }
          let dispTaxInd = showValue && total && currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && record.taxEnabled && record.tax && _.toNumber(record.tax) > 0;
          let dispTaxMargin = currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && _.toNumber(this.state.totalTax) > 0 && !dispTaxInd ? 12 : 0;
          return <div style={{ fontWeight: 500, marginRight: dispTaxMargin }}>{showValue && total ? `$${formatNumber(total, 2)}` : ''} { dispTaxInd ? "T" : "" }</div>

        },
      },
    ]

    if (
      !pagePrintSetting ||
      (pagePrintSetting && (pagePrintSetting.lot || typeof pagePrintSetting.lot === 'undefined'))
    ) {
      const sizeIndex = columns.findIndex((el) => el.dataIndex == 'extraOrigin')
      if (sizeIndex <= 1) return
      columns.splice(sizeIndex + 1, 0, {
        title: 'LOT',
        dataIndex: 'lotId',
        key: 'lotId',
        width: 120,
        align: 'center',
        render: (value: string, row: any) => {
          return (
            <div>
              {_.get(row, 'lotIds', []).map((lotId) => (
                <span key={lotId} style={{ display: 'block' }}>
                  {lotId}
                </span>
              ))}
            </div>
          )
        },
      })
    }

    const totals = (
      <>
        <Row className="f14" style={{ margin: '10px 10px 0' }} gutter={24}>
          <Col span={8}>
            {(!pagePrintSetting ||
              (pagePrintSetting &&
                (pagePrintSetting.totalUnits || typeof pagePrintSetting.totalUnits === 'undefined'))) && (
              <Row gutter={24}>
                <Col span={16}>
                  <HeaderTextLeftBox className="normal black">TOTAL UNITS</HeaderTextLeftBox>
                </Col>
                <Col span={8}>
                  <HeaderTextLeftBox className="black">{this.state.totalQuantity}</HeaderTextLeftBox>
                </Col>
              </Row>
            )}
          </Col>
          <Col span={8}>
            {type === 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.catchWeightQty)) && (
              <Row gutter={24}>
                <Col span={16}>
                  <HeaderTextLeftBox className="normal black">TOTAL BILLABLE QTY</HeaderTextLeftBox>
                </Col>
                <Col span={8}>
                  <HeaderTextLeftBox className="black">{formatNumber(this.state.totalWeight, 0)}</HeaderTextLeftBox>
                </Col>
              </Row>
            )}
          </Col>
          <Col span={8} style={{ paddingRight: (currentOrder.wholesaleClient && currentOrder.wholesaleClient.taxable && this.state.totalTax > 0) ? 16 : 6 }}>
            {type === 'invoice' && this.state.totalTax > 0 && (
              <>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={8}></Col>
                <Col span={7}>
                  <HeaderTextLeftBox className="normal black">SUBTOTAL</HeaderTextLeftBox>
                </Col>
                <Col span={9} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">${formatNumber(this.state.totalSubtotal, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
              <Row gutter={24} style={{ marginBottom: 10 }}>
                <Col span={8}></Col>
                <Col span={7}>
                  <HeaderTextLeftBox className="normal black">TAX</HeaderTextLeftBox>
                </Col>
                <Col span={9} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">${formatNumber(this.state.totalTax, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
              </>
              )}
            {type === 'invoice' && (
              <Row gutter={24}>
                <Col span={8}></Col>
                <Col span={7}>
                  <HeaderTextLeftBox className="normal black">TOTAL</HeaderTextLeftBox>
                </Col>
                <Col span={9} style={{ paddingRight: 0 }}>
                  <HeaderTextRightBox className="black">${formatNumber(this.state.totalAmount, 2)}</HeaderTextRightBox>
                </Col>
              </Row>
            )}
          </Col>
        </Row>
      </>
    )

    const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'
    company = company
      ? company
      : currentOrder.wholesaleClient && currentOrder.wholesaleClient.wholesaleCompany
        ? currentOrder.wholesaleClient.wholesaleCompany
        : null

    const isSoldTo = (type == 'bill' && !pagePrintSetting) || (type == 'invoice' && !pagePrintSetting)
    const isShipTo = (type == 'bill' && !pagePrintSetting) || (type == 'invoice' && !pagePrintSetting)

    const soldToCol = soldto ? (
      <Col span={isSoldTo ? 7 : 14}>
        <div style={{ width: "250px" }}>
          <h6 style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "bold", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>Sold to:</h6>
          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{clientName}</p>
          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px", whiteSpace: 'pre-line' }}>
            {currentOrder.billingAddress ? formatAddress(currentOrder.billingAddress.address, true).trim() : 'N/A'}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
              ? currentOrder.wholesaleClient.mainBillingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    ) : (
      <></>
    )

    const shipToCol = shipto ? (
      <Col span={isShipTo ? 7 : 14}>
        <div className="f12">
          <h6
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'bold',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {fulfillmentOptionType == 2 ? 'Pickup Address' : 'Ship to'}:{' '}
          </h6>
          {
            fulfillmentOptionType != 2 &&
            <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px" }}>{clientName}</p>
          }
          <p style={{ fontSize: "14px", fontFamily: "Inter", color: "black", fontWeight: "normal", lineHeight: "19.6px", textAlign: "left", marginBottom: "0px", width: "250px", whiteSpace: 'pre-line' }}>
            {fulfillmentOptionType == 2 ? (
              <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
            ) : (
              <>
                {currentOrder.shippingAddress
                  ? formatAddress(currentOrder.shippingAddress.address, true).trim()
                  : 'N/A'}
              </>
            )}
          </p>
          {/* <div>{currentOrder.user ? currentOrder.user.phone.replace('+1-', '') : ''}</div> */}
          <p
            style={{
              fontSize: '14px',
              fontFamily: 'Inter',
              color: 'black',
              fontWeight: 'normal',
              lineHeight: '19.6px',
              textAlign: 'left',
              marginBottom: '0px',
              width: '250px',
            }}
          >
            {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </p>
        </div>
      </Col>
    ) : (
      <></>
    )

    const logisticCols = (
      <>
        {currentOrder && currentOrder.fulfillmentType === 3 && (
          <>
            {carrierSetting && (
              <div className="f12" style={{minWidth: '17%'}}>
                <DisplayLeftLabel>Carrier </DisplayLeftLabel>
                <DisplayValue>{currentOrder.carrier}</DisplayValue>
                <DisplayValue>{currentOrder.carrierReferenceNo}</DisplayValue>
              </div>
            )}
            {origin && (
              <div className="f12" style={{minWidth: '25%'}}>
                <DisplayLeftLabel>Origin </DisplayLeftLabel>
                <DisplayValue>{currentOrder.originAddress}</DisplayValue>
              </div>
            )}
          </>
        )}
        <div className="f12" style={{minWidth: '25%'}}>
          <DisplayLeftLabel>Destination </DisplayLeftLabel>
          <DisplayValue>{clientName}</DisplayValue>
          <DisplayValue style={{ whiteSpace: 'pre-wrap' }}>
            {currentOrder.shippingAddress ? formatAddress(currentOrder.shippingAddress.address, true).trim() : 'N/A'}
          </DisplayValue>
          <DisplayValue>
            {currentOrder.wholesaleClient.mainShippingAddress &&
              currentOrder.wholesaleClient.mainShippingAddress.phone
              ? currentOrder.wholesaleClient.mainShippingAddress.phone
              : currentOrder.wholesaleClient.mobilePhone}
          </DisplayValue>
        </div>
      </>
    )

    let phone = ''
    if (company) {
      phone = (company.mainBillingAddress &&
        company.mainBillingAddress.phone &&
        company.mainBillingAddress.phone !== 'null'
        ? company.mainBillingAddress.phone
        : company.phone
          ? company.phone
          : ''
      ).replace('+1-', '')
    }

    let logo: string = this.props.logo
    if (logo && logo !== 'default') {
      if (!location.origin.includes('www')) {
        if (!location.origin.includes('www')) {
        logo = logo.replace('www.', '')
      }
      }
    }

    const RenderInfo = () => {
      if (this.props.currentPrintContainerId) {
        const { containerData } = this.state
        const totalGrossVolume = _.get(this.state, 'containerData.wholesaleOrderList', []).map(v => _.reduce(
          _.get(v, 'wholesaleOrderItemList', []),
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
          0,
        ))
        const totalGrossWeight = _.get(this.state, 'containerData.wholesaleOrderList', []).map(v => _.reduce(
          _.get(v, 'wholesaleOrderItemList', []),
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
          0,
        ))
        return (
          <Col span={11} className="f12" /*style={{ position: 'absolute', right: 10, top: 10 }}*/>
            <Flex >
              <div style={{ marginRight: 15 }}>
                <Flex>
                  <InfoLabel>Fulfillment Date: </InfoLabel>
                  <InfoValue>
                    {currentOrder.deliveryDate}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>PCFN: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'pcfn', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Booking: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'bookingNo', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Net/Gross LBs: </InfoLabel>
                  <InfoValue>
                    {formatNumber(_.sum(totalGrossWeight), 1)}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Net/Gross KG: </InfoLabel>
                  <InfoValue>
                    {formatNumber(_.divide(_.sum(totalGrossWeight), 2.2046), 2)}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Tare Weight LBs: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'tareWeight', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Vessel: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'logisticOrderName', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container #: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'containerName', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Seal#: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'sealNo', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>SCAC: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'scac', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Customer PO#: </InfoLabel>
                  <InfoValue>
                    {currentOrder.reference}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Customer: </InfoLabel>
                  <InfoValue>
                    {currentOrder.wholesaleClient.abbreviation}
                  </InfoValue>
                </Flex>
              </div>
              <div>
              <Flex>
                  <InfoLabel>DODAAC: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'dodaac', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container Type: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'containerType', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Temp Recorder #: </InfoLabel>
                  <InfoValue>
                    {containerData?.tempRecorderList ? _.get(containerData, 'tempRecorderList', []).map((v, i) => `${v.no} #${i + 1}`).join(',') : ''}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Temp (Range): </InfoLabel>
                  <InfoValue>
                    {containerData?.tempRecorderList ? _.get(containerData, 'tempRecorderList', []).map(v => `${v.temp}°F`).join(', ') : ''}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Contractual RDD: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'contractualRdd', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Container TCN: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'tcn', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>Payment Terms: </InfoLabel>
                  <InfoValue>
                    {currentOrder.financialTerms}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>POE: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'poe', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel>POD: </InfoLabel>
                  <InfoValue>
                    {_.get(containerData, 'pod', '')}
                  </InfoValue>
                </Flex>
                <Flex>
                  <InfoLabel># of Containers: </InfoLabel>
                  <InfoValue>

                  </InfoValue>
                </Flex>
                {this.props.orderId && <Flex>
                  <InfoLabel>Due Date: </InfoLabel>
                  <InfoValue>
                    {moment(currentOrder.deliveryDate)
                      .add(
                        _.get(
                          paymentTerms.find((v) => v.name === currentOrder.financialTerms),
                          'dueDays',
                          0,
                        ),
                        'days',
                      ).format('MM/DD/YY')}
                  </InfoValue>
                </Flex>}
              </div>
            </Flex>
          </Col>
        )
      }
      return (
        <Col span={11} className="f12">
          {!pagePrintSetting ||
            (pagePrintSetting && pagePrintSetting.so && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>SALES ORDER#: </DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.wholesaleOrderId}
                </DisplayValue>
              </Row>
            ))}
          {(!pagePrintSetting ||
            (pagePrintSetting && (pagePrintSetting.po || typeof pagePrintSetting.po === 'undefined'))) &&
            currentOrder.reference && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Customer PO #:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.reference}
                </DisplayValue>
              </Row>
            )}
          {/* {typeof title === 'undefined' && ( */}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.paymentTerm || typeof pagePrintSetting.paymentTerm === 'undefined'))) &&
            (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Payment term:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.financialTerms
                    ? currentOrder.financialTerms
                    : currentOrder.wholesaleClient.paymentTerm}
                </DisplayValue>
              </Row>
            )}
          {_.get(pagePrintSetting, 'paymentTermDueDate', false) &&
            localStorage.getItem('nsLinked') === 'null' &&
            (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Due date:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {moment(currentOrder.deliveryDate)
                    .add(
                      _.get(
                        paymentTerms.find((v) => v.name === currentOrder.financialTerms),
                        'dueDays',
                        0,
                      ),
                      'days',
                    )
                    .format('MM/DD/YY')}
                </DisplayValue>
              </Row>
            )}
          {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
            <>
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Pickup Time:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.warehousePickupTime}
                </DisplayValue>
              </Row>
            </>
          ) : (
            <>
              {!pagePrintSetting ||
                (pagePrintSetting && pagePrintSetting.window && (
                  <Row style={{ display: 'flex' }}>
                    <DisplayLabel>{fulfillmentOptionType === 2 ? 'Pickup Time' : 'Window'}:</DisplayLabel>
                    <DisplayValue span={13} style={pl24}>
                      {currentOrder.shippingAddress && currentOrder.shippingAddress.deliveryWindows
                        ? currentOrder.shippingAddress.deliveryWindows
                        : 'N/A'}
                    </DisplayValue>
                  </Row>
                ))}
            </>
          )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.numberOfPallets || typeof pagePrintSetting.numberOfPallets === 'undefined'))) &&
            currentOrder.numberOfPallets && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>No. of pallets:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.numberOfPallets}
                </DisplayValue>
              </Row>
            )}
          {/* <HeaderTextLeftBox>&nbsp;</HeaderTextLeftBox> */}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.orderDate || typeof pagePrintSetting.orderDate === 'undefined'))) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Order Date:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  <Moment format="MM/DD/YYYY" date={currentOrder.orderDate} />
                </DisplayValue>
              </Row>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.deliveryDate || typeof pagePrintSetting.deliveryDate === 'undefined'))) && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>
                  {pagePrintSetting && pagePrintSetting.fulfillmentLabel
                    ? pagePrintSetting.fulfillmentLabel
                    : 'Fulfillment Date'}
                  :
                </DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  <Moment format="MM/DD/YYYY" date={currentOrder.deliveryDate} />
                </DisplayValue>
              </Row>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.salesRepresentative ||
                typeof pagePrintSetting.salesRepresentative === 'undefined'))) &&
            currentOrder.seller && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Sales Rep:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.seller ? `${currentOrder.seller.firstName} ${currentOrder.seller.lastName}` : 'N/A'}
                </DisplayValue>
              </Row>
            )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.pickupReferenceNo || typeof pagePrintSetting.pickupReferenceNo === 'undefined'))) &&
            currentOrder.pickupReferenceNo &&
            fulfillmentOptionType != 1 && (
              <Row style={{ display: 'flex' }}>
                <DisplayLabel>Pickup Reference:</DisplayLabel>
                <DisplayValue span={13} style={pl24}>
                  {currentOrder.pickupReferenceNo}
                </DisplayValue>
              </Row>
            )}
          {fulfillmentOptionType === 3 && (
            <Row style={{ display: 'flex' }}>
              <DisplayLabel>Shipping Terms:</DisplayLabel>
              <DisplayValue span={13} style={pl24}>
                {currentOrder.shippingTerm ? currentOrder.shippingTerm : ''}
              </DisplayValue>
            </Row>
          )}
        </Col>
      )
    }

    const RenderTable = () => {
      const { currentOrder, sellerSetting } = this.props
      const clientName =
      currentOrder.wholesaleClient && currentOrder.wholesaleClient.clientCompany
        ? currentOrder.wholesaleClient.clientCompany.companyName
        : 'N/A'

      return (
        <>
          <PrintPickSheetTable style={{ marginTop: 8 }}>
            {/* {totals} */}
            <DeliveryListTable>
              <Table
                className={tableClass}
                bordered={false}
                style={{ width: '100%' }}
                rowKey="wholesaleOrderItemId"
                rowClassName={(record, index) => {
                  let rowOrderItemId = record.wholesaleOrderItemId
                  if (this.checkWeights(record)) {
                    rowOrderItemId = record.relatedOrderItemId
                  }
                  const i = orderItems.findIndex((oi) => oi.wholesaleOrderItemId === rowOrderItemId)
                  return i % 2 === 1 ? 'alternative' : ''
                }}
                columns={this.getDisplayingColumns(columns)}
                dataSource={items}
                pagination={false}
              />
            </DeliveryListTable>
            <hr />
            {totals}
          </PrintPickSheetTable>
        </>
      )
    }

    if (bUseStandardInvoice)
    {
      return (
        <PrintPickSheetWrapper id="invoiceContent">
          <HeaderBox style={{ fontWeight: 'normal' }}>
            {(this.props.orderId && !pagePrintSetting ||
              (pagePrintSetting && pagePrintSetting.barcode && (
                <Barcode value={currentOrder.wholesaleOrderId.toString()} displayValue={false} width={2} height={30} />
              ))}
            <Row style={{ textAlign: 'center' }}>
              <h1 style={{ marginBottom: 0 }}>SALES CONFIRMATION</h1>
            </Row>
            <Row>
              <Col span={11}>
                <CompanyInfoWrapper>
                  <div style={{ textAlign: 'left' }}>
                    {!pagePrintSetting ||
                      (pagePrintSetting && pagePrintSetting.logo && (
                        <div style={{ marginRight: 10 }}>
                          {logo === 'default' ? (
                            <LogoWrapper style={{ textAlign: 'left' }}>
                              <Icon type="logo4" viewBox={void 0} style={{ width: 224, height: 37 }} />
                            </LogoWrapper>
                          ) : (
                            <img src={logo} style={{ maxWidth: 150 }} onLoad={this.onImageLoad} />
                          )}
                        </div>
                      ))}
                    {company && (
                      <div className="left">
                        {company.companyName !== null && (
                          <HeaderTextLeftBox className="f18">{company.companyName}</HeaderTextLeftBox>
                        )}
                        {company.email !== null && (
                          <p
                            style={{
                              fontSize: '14px',
                              fontFamily: 'Inter',
                              color: 'black',
                              fontWeight: 'normal',
                              lineHeight: '19.6px',
                              textAlign: 'left',
                              marginBottom: '0px',
                            }}
                          >
                            {company.email}
                          </p>
                        )}
                        {company.mainBillingAddress !== null && (
                          <p
                            style={{
                              fontSize: '14px',
                              fontFamily: 'Inter',
                              color: 'black',
                              fontWeight: 'normal',
                              lineHeight: '19.6px',
                              textAlign: 'left',
                              marginBottom: '0px',
                            }}
                          >
                            {formatAddress(company.mainBillingAddress.address, false)}
                          </p>
                        )}
                        {(phone || company.fax) && (
                          <p
                            style={{
                              fontSize: '14px',
                              fontFamily: 'Inter',
                              color: 'black',
                              fontWeight: 'normal',
                              lineHeight: '19.6px',
                              textAlign: 'left',
                              marginBottom: '0px',
                            }}
                          >
                            Phone: {company.phone}{company.fax ? ` / Fax: ${company.fax}` : ''}
                          </p>
                        )}
                      </div>
                    )}
                  </div>
                </CompanyInfoWrapper>
                {fulfillmentOptionType != 3 ? (
                  <Row style={{ marginTop: 8 }} className="f12">
                    {currentOrder.warehousePickupTime !== null && currentOrder.warehousePickupTime !== 'null' ? (
                      <>
                        <Col span={7}>
                          <div>
                            <h6
                              style={{
                                fontSize: '14px',
                                fontFamily: 'Inter',
                                color: 'black',
                                fontWeight: 'bold',
                                lineHeight: '19.6px',
                                textAlign: 'left',
                                marginBottom: '0px',
                                width: '250px',
                              }}
                            >
                              PICKUP ADDRESS
                            </h6>
                            <p
                              style={{
                                fontSize: '14px',
                                fontFamily: 'Inter',
                                color: 'black',
                                fontWeight: 'normal',
                                lineHeight: '19.6px',
                                textAlign: 'left',
                                marginBottom: '0px',
                                width: '250px',
                              }}
                            >
                              {clientName}
                            </p>
                            <p
                              style={{
                                fontSize: '14px',
                                fontFamily: 'Inter',
                                color: 'black',
                                fontWeight: 'normal',
                                lineHeight: '19.6px',
                                textAlign: 'left',
                                marginBottom: '0px',
                                width: '250px',
                              }}
                            >
                              {currentOrder.pickupAddress}
                            </p>
                          </div>
                        </Col>
                        {soldToCol}
                      </>
                    ) : (
                      <div style={{ display: 'flex' }}/*, width: '50%'*/>
                        {soldToCol}
                        {shipToCol}
                      </div>
                    )}
                  </Row>
                ) : (
                  <div style={{display: 'flex'}}>
                    {logisticCols}
                  </div>
                )}
              </Col>

              <RenderInfo />
            </Row>


            {!this.props.currentPrintContainerId && <>
              {type == 'invoice' &&
                (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.driver_payment_collection)) && (
                  <Row style={{ padding: '10px 0' }} className="f12">
                    <Col span={7}>
                      <Flex>
                        <HeaderTextLeftBox className="normal">CASH RECEIVED $</HeaderTextLeftBox>
                        <Underline>{this.state.totalQuantity}</Underline>
                      </Flex>
                    </Col>

                    <Col span={7} offset={1}>
                      <Flex>
                        <HeaderTextLeftBox className="normal">CHECK RECEIVED $</HeaderTextLeftBox>
                        <Underline>{this.state.totalQuantity}</Underline>
                      </Flex>
                    </Col>
                    <Col span={7} offset={1}>
                      <Flex>
                        <HeaderTextLeftBox className="normal">CHECK #</HeaderTextLeftBox>
                        <Underline>{this.state.totalQuantity}</Underline>
                      </Flex>
                    </Col>
                  </Row>
                )}
              {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.account_balance_forward)) && (
                <>
                  <LightColoredRow style={{ padding: '4px 0px' }} className="f12">
                    <Col span={4}>DATE</Col>
                    <Col span={16}>ACCOUNT SUMMARY</Col>
                    <Col span={4} style={{ textAlign: 'right' }}>
                      AMOUNT
                    </Col>
                  </LightColoredRow>
                  <Row style={{ padding: '4px 0px' }} className="f12">
                    <Col span={4}>01/12/2015</Col>
                    <Col span={16}>
                      <div>Balance Forward</div>
                      <div>New charges(details below)</div>
                      <div>Total Amount Due</div>
                    </Col>
                    <Col span={4} style={{ textAlign: 'right' }}>
                      <div>100</div>
                      <div>100</div>
                      <div>100</div>
                    </Col>
                  </Row>
                </>
              )}
              {currentOrder.customerNote && (
                <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
                  <Col span={3}>
                    <HeaderTextLeftBox>Notes to Customer</HeaderTextLeftBox>
                  </Col>
                  <Col span={6}>
                    <div style={{ paddingLeft: 15 }}>{currentOrder.customerNote}</div>
                  </Col>
                </Row>
              )}
              {currentOrder.deliveryInstruction && (
                <Row style={{ borderBottom: '1px solid #aaa', paddingBottom: 5, marginBottom: 5 }} className="f12">
                  <Col span={24}>
                    <HeaderTextLeftBox>
                      {fulfillmentOptionType === 1 || fulfillmentOptionType === 2 ? 'Fulfillment ' : 'Delivery '}
                      Instructions
                    </HeaderTextLeftBox>
                    <div>{currentOrder.deliveryInstruction}</div>
                  </Col>
                </Row>
              )}
            </>}
          </HeaderBox>
          <RenderTable />
          <div className="floating-to-bottom">
            {(!pagePrintSetting ||
              (pagePrintSetting && (pagePrintSetting.barcode || pagePrintSetting.printTime || pagePrintSetting.so))) && (
              <div style={{ marginTop: 20 }}>
                <div>
                  {!pagePrintSetting || (pagePrintSetting && pagePrintSetting.barcode) ? (
                    <Barcode
                      value={currentOrder.wholesaleOrderId.toString()}
                      displayValue={false}
                      width={2}
                      height={30}
                    />
                  ) : (
                    <div>&nbsp;</div>
                  )}
                </div>
                <Flex className="v-center between" style={{ fontSize: 16, paddingTop: 5 }}>
                  <div>
                    {!pagePrintSetting ||
                      (pagePrintSetting && pagePrintSetting.printTime && (
                        <HeaderTextRightBox style={{ letterSpacing: 0, color: 'black' }}>
                          Printed {moment().format('MM/DD/YY hh:mm A')}
                        </HeaderTextRightBox>
                      ))}
                  </div>
                  <div>{!pagePrintSetting || (pagePrintSetting && pagePrintSetting.so && this.getNameAndOrderNo())}</div>
                </Flex>
              </div>
            )}
          </div>
        </PrintPickSheetWrapper>
      )
    };

    //START SELECT FRESH SECTION
    const selectFreshSoldToCol = soldto ? (
      <SelectFreshInvoiceBillTo>
        <>{clientName}<br/>
        {currentOrder.billingAddress ? ''  : 'N/A'}
        {currentOrder.billingAddress && currentOrder.billingAddress.address.department ?
          currentOrder.billingAddress.address.department : ''}
        {currentOrder.billingAddress && currentOrder.billingAddress.address.department ?
          (<br/>) : ('')}
        {currentOrder.billingAddress ?
          currentOrder.billingAddress.address.street1.trim(): ''}
        {currentOrder.billingAddress ?
          (<br/>): ('')}
        {currentOrder.billingAddress ?
          currentOrder.billingAddress.address.city + ' ' + currentOrder.billingAddress.address.zipcode: ''}
        <br/>
        {currentOrder.wholesaleClient.mainBillingAddress && currentOrder.wholesaleClient.mainBillingAddress.phone
        ? currentOrder.wholesaleClient.mainBillingAddress.phone
        : currentOrder.wholesaleClient.mobilePhone}
        </>
      </SelectFreshInvoiceBillTo>
    ) :
    (
      <></>
    )

    const selectFreshShipToCol = shipto ? (
      <SelectFreshInvoiceShipTo>
        {
        fulfillmentOptionType != 2 &&
        <>{clientName}<br/></>
        }
        {fulfillmentOptionType == 2 ? (
        <>{currentOrder.pickupAddress ? currentOrder.pickupAddress : ''}</>
        ) : (
        <>
        {currentOrder.shippingAddress ? ''  : 'N/A'}
        {currentOrder.shippingAddress ?
          currentOrder.shippingAddress.address.street1.trim(): ''}
        {currentOrder.shippingAddress ?
          (<br/>): ('')}
        {currentOrder.shippingAddress ?
          currentOrder.shippingAddress.address.city + ' ' + currentOrder.shippingAddress.address.zipcode: ''}
        </>
        )}
        <br/>
        {currentOrder.wholesaleClient.mainShippingAddress && currentOrder.wholesaleClient.mainShippingAddress.phone
        ? currentOrder.wholesaleClient.mainShippingAddress.phone
        : currentOrder.wholesaleClient.mobilePhone}
      </SelectFreshInvoiceShipTo>
    ) :
    (
      <SelectFreshInvoiceShipTo></SelectFreshInvoiceShipTo>
    )

    const getSelectFreshPrice = (record: OrderItem) => {
        if (this.checkWeights(record)) {
          return  <></>
        }

        let price = record.price

        var uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        price = basePriceToRatioPrice(uom, price, record)
        return (
          <>
          {`${price ? '$' + formatNumber(price, 2) : '$0'}`}
          </>
        )
    }

    const getSelectFreshSubTotal  = (record: OrderItem) => {
      // const extended = Number(record.price ? record.price : 0)
      // const quantity = ratioQtyToInventoryQty(record.uom, record.quantity, record)
      // const total = extended * quantity
      // return <>{total ? `$${formatNumber(total, 2)}` : ''}</>

      if (this.checkWeights(record)) {
        return <></>
      }

      let showValue = false
      let total = 0

      if (record.picked || record.catchWeightQty) {
        showValue = true
      }
      let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
      let unitUOM = record.UOM
      let ratioPrice = basePriceToRatioPrice(pricingUOM, _.toNumber(record.price), record, 12)

      let constantRatio = judgeConstantRatio(record)

      if (!constantRatio) {
        total = numberMultipy(
          ratioPrice,
          inventoryQtyToRatioQty(pricingUOM, _.toNumber(record.catchWeightQty), record, 12),
        )
      } else {
        let quantity = inventoryQtyToRatioQty(
          pricingUOM,
          ratioQtyToInventoryQty(unitUOM, _.toNumber(record.picked), record, 12),
          record,
          12,
        )
        total = numberMultipy(ratioPrice, quantity)
      }
      //return <div style={{ fontWeight: 500 }}>{showValue && total ? `$${formatNumber(total, 2)}` : ''}</div>
      return <>{showValue && total ? `$${formatNumber(total, 2)}` : ''}</>
    }

    const renderSelectFreshCatchWeightValues = (weights: any[]) => {
      const { type } = this.props
      const { pagePrintSetting } = this.state
      if (weights.length > 0) {
        let result: any[] = []
        let sub: any[] = []
        weights.forEach((el: any, index: number) => {
          if (index > 0 && (index - 10) % 10 == 0) {
            result.push(<WeightWrapper>{sub}</WeightWrapper>)
            sub = []
          }

          const weightEl = <SelectFreshWeightSpan>{el.unitWeight ? el.unitWeight : ''}</SelectFreshWeightSpan>
          if (index == weights.length - 1) {
            sub.push(weightEl)
            result.push(<WeightWrapper>{sub}</WeightWrapper>)
          } else {
            sub.push(weightEl)
          }
        })
        return result
      }

      return []
    }

    const getSelectFreshItemDesc = (record: OrderItem) => {
      if (this.checkWeights(record) && company && company.isDisablePickingStep === false) {
        return renderSelectFreshCatchWeightValues(record.weights)
      }
      if (record.itemId) {
        return record.editedItemName ? record.editedItemName : formatItemDescription(record.variety, record.SKU, sellerSetting, record.customerProductCode)
      }
      const desc = record.chargeDesc ? ` (${record.chargeDesc})` : ''
      return `${record.variety}${desc}`
    }

    const getSelectFreshBillableQty = (record: OrderItem) => {
      if (this.checkWeights(record)) {
        return <></>
      }

      let showBillableQty,
      showValue = false

      if (record.picked || record.catchWeightQty) {
        showValue = true
      }

      let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
      let unitUOM = record.UOM

      let constantRatio = judgeConstantRatio(record)
      if (record.oldCatchWeight) {
        showBillableQty = record.catchWeightQty
      } else {
        if (!constantRatio) {
          showBillableQty = inventoryQtyToRatioQty(pricingUOM, record.catchWeightQty, record)
        } else {
          showBillableQty = inventoryQtyToRatioQty(
            pricingUOM,
            ratioQtyToInventoryQty(unitUOM, record.picked, record, 12),
            record,
          )
        }
      }

      return showValue ? `${mathRoundFun(showBillableQty, 2)}` : ''
    }


    const selectFreshOrderListItems = (pageNum: any, pageItems: any[]) => {
      let myItems = [];
      //pageNum = pageNum + 1;

      //myItems = items.slice((pageNum - 1) * 20, pageNum * 20);
      //console.log(pageItems[pageNum])
      let strStartStop = pageItems[pageNum];
      let arrStartStop = strStartStop.split(',');
      myItems = items.slice(arrStartStop[0],arrStartStop[1]);

      return myItems.map((itm) =>
      <tr>
        <td width={'27px'} align='center'>{getSelectFreshBillableQty(itm)}</td>{/*27*/}
        <td width={'45px'} align='center'>{itm.pricingUOM}</td>{/*45*/}
        <td width={'212px'}>{getSelectFreshItemDesc(itm)}</td>{/*212*/}
        <td width={'40px'} align='right'>{/*40*/}
          {getSelectFreshPrice(itm)}
        </td>
        <td width={'60px'} align='right'>{/*60*/}
          {getSelectFreshSubTotal(itm)}
        </td>
      </tr>
      )

    }

    const selectFreshPrintBody = (pageNum: any, totalPages: any, pageItems : any[]) => {
      return(
      <SelectFreshPrintableBodyWrapper style={{border: '1px dotted white'}}>
        <SelectFreshInvoiceBody>
          {selectFreshSoldToCol}
          {selectFreshShipToCol}
          {type == 'invoice' && (!pagePrintSetting || (pagePrintSetting && pagePrintSetting.num)) && (
            <SelectFreshInvoiceNumber>
              {getOrderPrefix(sellerSetting, 'sales')}
                {isCustomOrderNumberEnabled(sellerSetting) && currentOrder.customOrderNo
                  ? currentOrder.customOrderNo
                  : currentOrder.wholesaleOrderId}
            </SelectFreshInvoiceNumber>
          )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.orderDate || typeof pagePrintSetting.orderDate === 'undefined'))) && (
                <SelectFreshInvoiceDate>
                  <Moment format="MM/DD/YYYY" date={currentOrder.orderDate} />
                </SelectFreshInvoiceDate>
          )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.paymentTerm || typeof pagePrintSetting.paymentTerm === 'undefined'))) &&
            (currentOrder.financialTerms || currentOrder.wholesaleClient.paymentTerm) && (
              <SelectFreshInvoiceTerms>
                {currentOrder.financialTerms
                ? currentOrder.financialTerms
                : currentOrder.wholesaleClient.paymentTerm}
              </SelectFreshInvoiceTerms>
          )}
          {(!pagePrintSetting ||
            (pagePrintSetting &&
              (pagePrintSetting.salesRepresentative ||
                typeof pagePrintSetting.salesRepresentative === 'undefined'))) &&
              currentOrder.seller && (
                <SelectFreshInvoiceRep>
                  {currentOrder.seller ? `${currentOrder.seller.firstName} ${currentOrder.seller.lastName}` : 'N/A'}
                </SelectFreshInvoiceRep>
          )}
          <SelectFreshInvoiceShipMethod>
          Route
          </SelectFreshInvoiceShipMethod>
          <SelectFreshInvoiceLineItems>
            <SelectFreshInvoiceItemRow>
              {selectFreshOrderListItems(pageNum, pageItems)}
            </SelectFreshInvoiceItemRow>
          </SelectFreshInvoiceLineItems>
          <SelectFreshGrandTotal>
            ${formatNumber(this.state.totalAmount, 2)}
          </SelectFreshGrandTotal>
          <SelectFreshPageSection>
            Page {pageNum + 1} of {totalPages}
          </SelectFreshPageSection>
        </SelectFreshInvoiceBody>
      </SelectFreshPrintableBodyWrapper>
      )
    }

    if (bUseSelectFreshInvoice)
    {
      let myFormRepeats = [];
      //var totalItemCount = items.length;
      //var numOfRepeats = Math.ceil(totalItemCount / 20);

      const itemSectionHeight = 420;//396
      let grandTotalItemHeight = 0.00;
      let tempItemDesc = '';
      let numItemLineWrap = 0;
      const singleLineHeight = 17.78;//16.00; //15.88
      const additionalWrapHeight = 15.24;//14.00; //12.29??
      let currentLineHeight = 0.00;
      let curPageNum = 1;
      let curPageLineHeigthRemaining = itemSectionHeight;
      let pageItems = [];
      let nextPageIndexStart = 0;
      const maxCharBeforeWrap = 40;//50

      items.forEach((itm) => {
        currentLineHeight = 0.00;
        tempItemDesc = getSelectFreshItemDesc(itm);
        if (tempItemDesc.length <= maxCharBeforeWrap) {
          currentLineHeight += singleLineHeight;
        }
        if (tempItemDesc.length > maxCharBeforeWrap) {
          numItemLineWrap = Math.ceil(tempItemDesc.length / maxCharBeforeWrap);

          currentLineHeight +=singleLineHeight;
          if (numItemLineWrap > 1){
            currentLineHeight += (additionalWrapHeight * (numItemLineWrap - 1))
          }
        }

        curPageLineHeigthRemaining = curPageLineHeigthRemaining - currentLineHeight;

        if(curPageLineHeigthRemaining < 0 ){
          //the additional line would have gone on the next page
          pageItems.push(nextPageIndexStart + ',' + (items.indexOf(itm) - 1));
          nextPageIndexStart = items.indexOf(itm) - 1;
          curPageNum += 1
          //reset running total
          curPageLineHeigthRemaining = itemSectionHeight;
        }

        grandTotalItemHeight += currentLineHeight;

        if (items.indexOf(itm) === items.length - 1) {
          //if it is the last item add the final page
          pageItems.push(nextPageIndexStart + ',' + (items.indexOf(itm) + 1));
        }

      })

      let numOfRepeats = Math.ceil(grandTotalItemHeight / itemSectionHeight);

      for (var i = 0; i < numOfRepeats; i++) {
        //for Select Fresh we need to print each for 4 times for their special Paper
        for (var j = 0; j < 4; j++) {
          myFormRepeats.push(selectFreshPrintBody(i, numOfRepeats, pageItems))
        }
      }

      return (
        <SelectFreshInvoiceWrapper id="SelectFreshInvoiceContent"  style={{width: '490px',}} >  {/* className="invoiceBackground" */}
            {myFormRepeats}
        </SelectFreshInvoiceWrapper>
      )
    };

    //END SELECT FRESH SECTION
  }
}

export default ViewSalesConfirmation
