import React, { FC, useEffect, useState } from 'react'
import styled from '@emotion/styled'
import {
  Flex,
  ThemeButton,
  ThemeCheckbox,
  ThemeModal,
  ThemeRadio,
  ThemeTable,
} from '~/modules/customers/customers.style'
import { Button, Icon, Input, Radio, Select, Switch, Tooltip, Spin } from 'antd'
import { OrderService } from '~/modules/orders/order.service'
import { of } from 'rxjs'
import { formatNumber, inventoryQtyToRatioQty, ratioQtyToInventoryQty, responseHandler } from '~/common/utils'
import { OrderDetail, OrderItem } from '~/schema'
import moment from 'moment'
import _, { round } from 'lodash'
import { useKeyPress } from 'ahooks'
import { Icon as IconSvg } from '~/components'
import EnterRepackModal from '../modals/repack-modal/enter-repack-modal'

type NotSelectTable = {
  brand: null
  companyName: string
  cost: string
  deliveryDate: string
  inventoryUOM: string
  lotAvailableQty: string
  lotId: string
  note: null
  onHandQty: string
  origin: null
  perAllocateCost: number
  picked: number
  purchaseOrderItemId: number
  quantity: number
}
interface Iprops {
  sellerSetting: any
  currentRecord: any
  currentOrder: OrderDetail
  getOrderItemsById: Function
  onCancel: Function
  updateOrderQuantityLoading: any
  orderItems: any
  isDisablePickingStep: boolean
}

const toFixed2 = (n: number) => {
  if (n % 1 === 0) {
    return n
  }
  return Number(n.toFixed(2))
}
const { Option } = Select

const LotSection: FC<Iprops> = ({
  sellerSetting,
  currentRecord,
  currentOrder,
  getOrderItemsById,
  onCancel,
  updateOrderQuantityLoading,
  orderItems,
  isDisablePickingStep,
}) => {
  const [lotAssignmentMethod, setLotAssignmentMethod] = useState(-1)
  const [enableLotOverflow, setEnableLotOverflow] = useState(null)
  const [loading, setLoading] = useState(false)
  const [saveButtonLoading, setSaveButtonLoading] = useState(false)
  const [dataSource, setDataSource] = useState<any[]>([])
  const [priceUOMDataSource, setPriceUOMDataSource] = useState<any[]>([])
  const [originDataSource, setOriginDataSource] = useState<any[]>([])
  const [isTotalLessCurrent, setIsTotalLessCurrent] = useState(false)
  const [counter, setCounter] = useState(0)
  const [refresh, setRefresh] = useState(0)
  const [isReassignLots, setIsReassignLots] = useState(false)
  const [confirmUpdatePickedModalVisible, setConfirmUpdatePickedModalVisible] = useState(false)
  const [baseQuantity] = useState(
    ratioQtyToInventoryQty(currentRecord.UOM, currentRecord.quantity || 0, currentRecord, 12),
  )
  const [basePicked] = useState(ratioQtyToInventoryQty(currentRecord.UOM, currentRecord.picked || 0, currentRecord, 12))
  const [currentItems, setCurrentItems] = useState<any[]>([])
  // select measure
  const [selectUOM, setSelectUOM] = useState<string>('')
  const [options, setOptions] = useState<string[]>([])
  const [UOMRatio, setUOMratio] = useState<number>(1)
  //manually assign switch
  const [manuallyAssignState, setManuallyAssignState] = useState<boolean>(true)
  //not select
  const [defaultOrderQty, setDefaultOrderQty] = useState<number>(0)
  const [defaultPickedQty, setDefaultPickedQty] = useState<number>(0)
  const [notSelect, setNotSelect] = useState<NotSelectTable>({
    brand: null,
    companyName: '',
    cost: '',
    deliveryDate: 'Not Lot Selected',
    inventoryUOM: '',
    lotAvailableQty: '',
    lotId: '',
    note: null,
    onHandQty: '',
    origin: null,
    perAllocateCost: 0,
    picked: defaultPickedQty,
    purchaseOrderItemId: 0,
    quantity: defaultOrderQty,
  })
  const [radioSelect, setRadioSelect] = useState<number>(-1)
  const [clickTime, setClickTime] = useState<number>(0)
  const [update, setUpdate] = useState<number>(0)
  const [radioUpdate, setRadioUpdate] = useState<number>(0)

  const [edit, setEdit] = useState<boolean>(true)
  const [repackModalVisible, setRepackModalVisible] = useState<boolean>(false)

  useEffect(() => {
    setCurrentItems(_.intersectionBy(orderItems, currentRecord.items, 'wholesaleOrderItemId'))
    setManuallyAssignState(currentRecord.manuallyAssignState)
  }, [orderItems])

  useEffect(() => {
    //set uom
    if (currentRecord.UOM !== currentRecord.inventoryUOM) {
      setSelectUOM(currentRecord.UOM)
      setOptions([currentRecord.inventoryUOM, currentRecord.UOM])
      const ratioUom = currentRecord.wholesaleProductUomList.find((x: any) => x.name == currentRecord.UOM)
      setUOMratio(ratioUom ? ratioUom.ratio : 1)
    } else {
      setSelectUOM(currentRecord.inventoryUOM)
      setOptions([currentRecord.inventoryUOM])
    }
  }, [])

  useEffect(() => {
    setLotAssignmentMethod(currentRecord.lotAssignmentMethod)
    if (currentRecord.lotAssignmentMethod == 2) {
      const quantity = selectUOM == currentRecord.inventoryUOM ? baseQuantity : currentRecord.quantity
      const picked = selectUOM == currentRecord.inventoryUOM ? basePicked : currentRecord.picked
      setNotSelectAllQuantityAndPicked(quantity, picked)
      setLotAssignmentMethod(3)
    }
    console.log(currentRecord)
    console.log(orderItems)
  }, [currentRecord.lotAssignmentMethod])

  useEffect(() => {
    setEnableLotOverflow(currentRecord.enableLotOverflow)
  }, [currentRecord.enableLotOverflow])

  useEffect(() => {
    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      resetRadio()
      setSelectModeDate()
      setRadioSelect(0)
    }

    const newNotSelect: any = _.cloneDeep(notSelect)
    if (selectUOM == currentRecord.inventoryUOM) {
      const quantity =
        newNotSelect.quantity !== ''
          ? Math.floor((parseFloat(newNotSelect.quantity) / UOMRatio) * 100000000000) / 100000000000
          : 0
      const picked =
        newNotSelect.picked !== ''
          ? Math.floor((parseFloat(newNotSelect.picked) / UOMRatio) * 100000000000) / 100000000000
          : 0
      setNotSelectAllQuantityAndPicked(quantity, picked)
    } else {
      const quantity =
        newNotSelect.quantity !== ''
          ? Math.floor(parseFloat(newNotSelect.quantity) * UOMRatio * 100000000000) / 100000000000
          : 0
      const picked =
        newNotSelect.picked !== ''
          ? Math.floor(parseFloat(newNotSelect.picked) * UOMRatio * 100000000000) / 100000000000
          : 0
      setNotSelectAllQuantityAndPicked(quantity, picked)
    }
    if (manuallyAssignState == true && lotAssignmentMethod == 3 && update !== 1 && edit !== true) {
      const cloneData = _.cloneDeep(dataSource)
      setDataSource(
        cloneData.map((item, index) => {
          item.picked =
            selectUOM == currentRecord.inventoryUOM
              ? Math.floor((parseFloat(item.picked) / UOMRatio) * 100000000000) / 100000000000
              : Math.floor(parseFloat(item.picked) * UOMRatio * 100000000000) / 100000000000
          item.quantity =
            selectUOM == currentRecord.inventoryUOM
              ? Math.floor((parseFloat(item.quantity) / UOMRatio) * 100000000000) / 100000000000
              : Math.floor(parseFloat(item.quantity) * UOMRatio * 100000000000) / 100000000000
          return item
        }),
      )
    }
  }, [selectUOM])

  useEffect(() => {
    //
  }, [isReassignLots])

  useEffect(() => {
    if (enableLotOverflow) {
      setUpdate(update + 1)
    } else {
      /*if (isReassignLots) {
        setNotSelectAllQuantityAndPicked(0, 0)
      } else {
        getDataSourceNoLotSelect()
      }*/
      if (originDataSource.length > 0) {
        setNotSelectAllQuantityAndPicked(0, 0)
      }
    }
  }, [enableLotOverflow])

  useEffect(() => {
    let iniManuallyAssignState = currentRecord.manuallyAssignState
    if (update <= 1 && currentRecord.lotAssignmentMethod == 3 && iniManuallyAssignState == false) {
      const quantity =
        selectUOM == currentRecord.inventoryUOM
          ? baseQuantity
          : Math.floor(baseQuantity * UOMRatio * 10000000000) / 10000000000

      const picked =
        selectUOM == currentRecord.inventoryUOM
          ? basePicked
          : Math.floor(basePicked * UOMRatio * 10000000000) / 10000000000
      setDefaultOrderQty(quantity)
      setDefaultPickedQty(picked)
      let radioIndex = 0
      originDataSource.forEach((item: any, index: number) => {
        if (item.quantity) {
          const uomItemQuantity =
            selectUOM == currentRecord.inventoryUOM ? item.quantity.toFixed(2) : (item.quantity * UOMRatio).toFixed(2)
          if (uomItemQuantity == quantity.toFixed(2)) {
            radioIndex = index + 1
          }
        }
      })
      setRadioSelect(radioIndex)
    }
  }, [originDataSource])

  useEffect(() => {
    if (lotAssignmentMethod == 1) {
      setUpdate(update + 1)
      setTimeout(() => {
        const e = document.getElementById('headerRadio1')
        if (e) {
          e.focus()
        }
      }, 1000)
      setManuallyAssignState(false)
    }
    if (manuallyAssignState == true && lotAssignmentMethod == 3) {
      setUpdate(update + 1)
    }

    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      if (update > 0) {
        //set default value
        const quantity =
          selectUOM == currentRecord.inventoryUOM
            ? baseQuantity
            : Math.floor(baseQuantity * UOMRatio * 100000000000) / 100000000000
        const picked =
          selectUOM == currentRecord.inventoryUOM
            ? basePicked
            : Math.floor(basePicked * UOMRatio * 100000000000) / 100000000000
        setDefaultOrderQty(roundNum(quantity))
        setDefaultPickedQty(roundNum(picked))

        //set not select table
        setNotSelectAllQuantityAndPicked(roundNum(quantity), roundNum(picked))

        //set radio
        setRadioSelect(0)
        //setDataSource
        const cloneData = _.cloneDeep(dataSource)
        setDataSource(
          cloneData.map((item, index) => {
            item.picked = 0
            item.quantity = 0
            return item
          }),
        )
        const clonePriceData = _.cloneDeep(priceUOMDataSource)
        setPriceUOMDataSource(
          clonePriceData.map((item, index) => {
            item.picked = 0
            item.quantity = 0
            return item
          }),
        )

        setRadioUpdate(radioUpdate + 1)
        setUpdate(update + 1)
      }
    }
  }, [lotAssignmentMethod])

  useEffect(() => {}, [notSelect])

  // switch manually state
  useEffect(() => {
    const doubleClick = localStorage.getItem('doubleClick')
    const doubleClickFromSelect = localStorage.getItem('doubleClickFromSelect')
    if (manuallyAssignState == false) {
      if (update > 0) {
        setSelectModeDate()

        //setDataSource
        const cloneData = _.cloneDeep(dataSource)
        setDataSource(
          cloneData.map((item, index) => {
            item.picked = 0
            item.quantity = 0
            return item
          }),
        )
        const clonePriceData = _.cloneDeep(priceUOMDataSource)
        setPriceUOMDataSource(
          clonePriceData.map((item, index) => {
            item.picked = 0
            item.quantity = 0
            return item
          }),
        )

        setRadioUpdate(radioUpdate + 1)
      }
    } else if (doubleClick == '1' && doubleClickFromSelect != '1') {
      if (enableLotOverflow) {
        setDataSource(getOverflowDataSource(originDataSource))
        const newARR = _.cloneDeep(originDataSource)
        const formatData: any[] = []
        if (selectUOM !== currentRecord.inventoryUOM) {
          const ratioUom = currentRecord.wholesaleProductUomList.find((x: any) => x.name == currentRecord.UOM)
          let newItem = null
          newARR.forEach((v: any) => {
            newItem = v
            if (v.hasOwnProperty('quantity')) {
              newItem.quantity = Math.floor(parseFloat(v.quantity * ratioUom.ratio * 100)) / 100
            }
            if (v.hasOwnProperty('picked')) {
              newItem.picked = Math.floor(parseFloat(v.picked * ratioUom.ratio * 100)) / 100
            }
            formatData.push(newItem)
          })
          setPriceUOMDataSource(getOverflowDataSource(newARR))
        }
      } else {
        setDataSource(getOldestDataSource(originDataSource))
        const newARR = _.cloneDeep(originDataSource)
        const formatData: any[] = []
        if (selectUOM !== currentRecord.inventoryUOM) {
          const ratioUom = currentRecord.wholesaleProductUomList.find((x: any) => x.name == currentRecord.UOM)
          let newItem = null
          newARR.forEach((v: any) => {
            newItem = v
            if (v.hasOwnProperty('quantity')) {
              newItem.quantity = Math.floor(parseFloat(v.quantity * ratioUom.ratio * 100)) / 100
            }
            if (v.hasOwnProperty('picked')) {
              newItem.picked = Math.floor(parseFloat(v.picked * ratioUom.ratio * 100)) / 100
            }
            formatData.push(newItem)
          })
          setPriceUOMDataSource(getOldestDataSource(formatData))
        }
      }
    }
    if (manuallyAssignState == true && lotAssignmentMethod == 3) {
      if (update == 0) {
        setUpdate(2)
      } else {
        setUpdate(update + 1)
      }
    }

    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      //set radio
      setRadioSelect(0)
    }
  }, [manuallyAssignState])

  const getOverflowDataSource = (originData: any[]) => {
    const basQty = selectUOM == currentRecord.inventoryUOM ? baseQuantity : baseQuantity * UOMRatio
    let qty = basQty
    const pickedQty = selectUOM == currentRecord.inventoryUOM ? basePicked : basePicked * UOMRatio
    let picked = pickedQty
    const dataSourceArr = originData

    const cloneArr = _.cloneDeep(dataSourceArr).reverse()
    const newData = cloneArr.map((v, i) => {
      if (currentItems.find((i) => i.lotId === v.lotId)) {
        const findItem = currentItems.find((i) => i.lotId === v.lotId)
        const findItemQuantity =
          Math.ceil(
            ratioQtyToInventoryQty(currentRecord.UOM, findItem.quantity || 0, currentRecord, 12) * 1000000000000,
          ) / 1000000000000
        return { ...v, lotAvailableQty: v.lotAvailableQty + findItemQuantity }
      }
      return v
    })

    if (currentRecord.quantity === 0) {
      return newData
    }
    const data = newData.map((item: any, index: number) => {
      if (qty === 0) {
        const currentItem = { ...item, quantity: '', picked: '' }
        return currentItem
      }
      const itemLotAvailableQty =
        selectUOM == currentRecord.inventoryUOM ? item.lotAvailableQty : item.lotAvailableQty * UOMRatio
      if (parseFloat(qty.toFixed(2)) >= itemLotAvailableQty && itemLotAvailableQty >= 0) {
        qty = qty - itemLotAvailableQty
        // picked
        if (picked >= itemLotAvailableQty) {
          if (index === newData.length - 1) {
            //
            return {
              ...item,
              quantity: itemLotAvailableQty,
              picked: itemLotAvailableQty,
            }
          }
          const currentItem = {
            ...item,
            quantity: itemLotAvailableQty,
            picked: itemLotAvailableQty,
          }
          picked = picked - itemLotAvailableQty
          return currentItem
        } else {
          const pickedNum = selectUOM == currentRecord.inventoryUOM ? picked : picked
          const currentItem = {
            ...item,
            quantity: itemLotAvailableQty,
            picked: picked > 0 ? pickedNum : '',
          }
          picked = picked - itemLotAvailableQty
          return currentItem
        }
      } else if (qty < itemLotAvailableQty && qty > 0) {
        qty = qty - itemLotAvailableQty
        const currentItem = {
          ...item,
          quantity: Math.floor((qty + itemLotAvailableQty) * 1000000000000) / 1000000000000,
          picked: picked > 0 ? Math.floor(picked * 1000000000000) / 1000000000000 : '',
        }
        picked = picked - itemLotAvailableQty
        return currentItem
      }
      if (item.lotAvailableQty >= 0) {
        qty = qty - itemLotAvailableQty
      }
      return { ...item, quantity: '', picked: '' }
    })
    return data.reverse().map((item: any, index: number) => {
      const quantity = item.quantity
      const picked = item.picked
      return { ...item, quantity, picked }
    })
  }

  const getOldestDataSource = (originData: any[]) => {
    const basQty = selectUOM == currentRecord.inventoryUOM ? baseQuantity : baseQuantity * UOMRatio
    const pickedQty = selectUOM == currentRecord.inventoryUOM ? basePicked : basePicked * UOMRatio
    const dataSourceArr = originData

    const cloneArr = _.cloneDeep(dataSourceArr).reverse()
    return cloneArr
      .map((item, index) => {
        const findItem = currentItems.find((i) => i.lotId === item.lotId)
        if (index === 0) {
          if (findItem) {
            const findItemQuantity = ratioQtyToInventoryQty(
              currentRecord.UOM,
              findItem.quantity || 0,
              currentRecord,
              12,
            )
            return {
              ...item,
              quantity: basQty === 0 ? '' : basQty,
              picked: basePicked === 0 ? '' : pickedQty,
              lotAvailableQty: item.lotAvailableQty + findItemQuantity,
            }
          } else {
            return {
              ...item,
              quantity: basQty === 0 ? '' : basQty,
              picked: basePicked === 0 ? '' : pickedQty,
            }
          }
        }

        if (findItem) {
          const findItemQuantity = ratioQtyToInventoryQty(currentRecord.UOM, findItem.quantity || 0, currentRecord, 12)

          return { ...item, lotAvailableQty: item.lotAvailableQty + findItemQuantity, quantity: '', picked: '' }
        }
        return item
      })
      .reverse()
  }

  useEffect(() => {
    if (update != 9999 && update > 1) {
      setEdit(false)
    }
    if (update > 1) {
      if (currentItems.length == 1 && originDataSource.length == 0 && currentItems[0].lotId == undefined) {
        getDataSourceNoLotSelect()
      } else {
        if (update != 9999) {
          setNotSelectAsync()
        }
      }
    }
    if (update == 1 || update == 0) {
      getDataSourceNoLotSelect()
    }
    if (lotAssignmentMethod == 1 && enableLotOverflow == false) {
      if (originDataSource.length > 0) {
        setNotSelectAllQuantityAndPicked(0, 0)
      }
    }
    if (manuallyAssignState == true && lotAssignmentMethod == 3 && update != 9999) {
      initialFocusInput()
    }
  }, [update])

  useEffect(() => {
    setTimeout(() => {
      ;(document.getElementById('lotRadio0') as HTMLElement).focus()
    }, 100)
  }, [radioUpdate])

  //switch radio

  useEffect(() => {
    resetRadio()
  }, [radioSelect])

  const resetRadio = () => {
    if (radioSelect > 0) {
      //setDefaultPickedQty(0)
      //setDefaultOrderQty(0)
      setNotSelectAllQuantityAndPicked(0, 0)
      setDataSource(
        dataSource.map((item, index) => {
          item.picked = index == radioSelect - 1 ? defaultPickedQty : 0
          item.quantity = index == radioSelect - 1 ? defaultOrderQty : 0
          return item
        }),
      )
      setPriceUOMDataSource(
        priceUOMDataSource.map((item, index) => {
          item.picked = index == radioSelect - 1 ? defaultPickedQty : 0
          item.quantity = index == radioSelect - 1 ? defaultOrderQty : 0
          return item
        }),
      )
    }

    if (radioSelect == 0) {
      const quantityWithUom = selectUOM == currentRecord.inventoryUOM ? baseQuantity : baseQuantity * UOMRatio
      const pickedWithUom = selectUOM == currentRecord.inventoryUOM ? basePicked : basePicked * UOMRatio
      const newNotSelect = _.cloneDeep(notSelect)
      newNotSelect.picked = roundNum((pickedWithUom * 100000000000) / 100000000000)
      newNotSelect.quantity = roundNum((quantityWithUom * 100000000000) / 100000000000)
      setNotSelect(newNotSelect)
      setDataSource(
        dataSource.map((item, index) => {
          item.picked = 0
          item.quantity = 0
          return item
        }),
      )
      setPriceUOMDataSource(
        priceUOMDataSource.map((item, index) => {
          item.picked = 0
          item.quantity = 0
          return item
        }),
      )
      setTimeout(() => {
        ;(document.getElementById('lotRadio0') as HTMLElement).focus()
        ;(document.getElementById('lotRadio0') as HTMLElement).click()
      }, 100)
    }
  }

  const setSelectModeDate = () => {
    const quantity =
      selectUOM == currentRecord.inventoryUOM
        ? baseQuantity
        : Math.floor(baseQuantity * UOMRatio * 100000000000) / 100000000000
    const picked =
      selectUOM == currentRecord.inventoryUOM
        ? basePicked
        : Math.floor(basePicked * UOMRatio * 100000000000) / 100000000000
    setDefaultOrderQty(roundNum(quantity))
    setDefaultPickedQty(roundNum(picked))

    //set not select table
    setNotSelectAllQuantityAndPicked(roundNum(quantity), roundNum(picked))
  }

  const radioStyle = {
    display: 'block',
    height: '30px',
    lineHeight: '30px',
  }

  // keyboard control
  useKeyPress('TAB', () => {
    toNextRadio()
  })

  useKeyPress('shift.TAB', () => {
    if (lotAssignmentMethod == 3 && manuallyAssignState == false) {
      toPreviousRadio()
    }
  })

  useKeyPress('SPACE', () => {
    const firstDate = new Date()
    setClickTime(firstDate.valueOf())
    if (clickTime !== 0) {
      const lastDate = new Date().valueOf()
      if (lastDate - clickTime < 600) {
        setRadioSelect(0)
        setClickTime(0)
      } else {
        if (manuallyAssignState == false && lotAssignmentMethod == 3) {
          const ele = document.activeElement
          if (ele) {
            const id = ele.getAttribute('id')?.replace('lotRadio', '')
            setRadioSelect(id)
            setTimeout(() => {
              ;(document.getElementById(`lotRadio${id}`) as HTMLElement).click()
            }, 100)
          }
        } else {
          const ele = document.activeElement
          if (ele) {
            const id = ele.getAttribute('id')
            setTimeout(() => {
              ;(document.getElementById(id) as HTMLElement).click()
            }, 100)
          }
        }
      }
    }
  })

  const initialFocusInput = () => {
    let data =
      update <= 1 && enableLotOverflow == true && lotAssignmentMethod == 1 ? getIniDataSource() : getDataSource()
    if (lotAssignmentMethod == 1 && enableLotOverflow == false) {
      data = getIniDataSource()
    }
    const r: any[] = []
    data.forEach((item) => {
      item.quantity = item.quantity
        ? typeof item.quantity == 'number' && !isNaN(item.quantity)
          ? roundNum(item.quantity)
          : item.quantity.toString()
        : ''
      item.picked = item.picked
        ? typeof item.picked == 'number' && !isNaN(item.picked)
          ? roundNum(item.picked)
          : item.picked.toString()
        : ''
      r.push(item)
    })
    r.unshift(notSelect)

    if (lotAssignmentMethod == 3 && manuallyAssignState == true) {
      //
      r.forEach((item, index) => {
        if (item.quantity > 0) {
          setTimeout(() => {
            const e = document.getElementById(`quantity${index}`)
            if (e) {
              e.focus()
            }
          }, 100)
          return
        }
        if (item.picked > 0) {
          setTimeout(() => {
            const e = document.getElementById(`picked${index}`)
            if (e) {
              e.focus()
            }
          }, 100)
          return
        }
      })
    }
  }

  //keyboard functions
  const toNextRadio = () => {
    const ele = document.activeElement
    let e: any = null
    if (lotAssignmentMethod == 1) {
      if (
        ele &&
        (ele.getAttribute('id') == null ||
          ele.getAttribute('id')?.indexOf('headerRadio') == -1 ||
          ele.getAttribute('id')?.indexOf('lotRadio') == -1)
      ) {
        e = document.getElementById('headerRadio1')
        setTimeout(() => {
          e.focus()
        }, 100)
      }

      if (ele && ele.getAttribute('id')) {
        if (ele.getAttribute('id') == 'headerRadio1') {
          e = document.getElementById('headerRadio2')
          setTimeout(() => {
            e.focus()
          }, 100)
          return
        }

        if (ele.getAttribute('id') == 'headerRadio2') {
          e = document.getElementById('headerRadio1')
          setTimeout(() => {
            e.focus()
          }, 100)
          return
        }
      }
    }
    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      const length = dataSource.length
      if (
        ele &&
        (ele.getAttribute('id') == null ||
          ele.getAttribute('id')?.indexOf('headerRadio') == -1 ||
          ele.getAttribute('id')?.indexOf('lotRadio') == -1)
      ) {
        e = document.getElementById('lotRadio0')
        setTimeout(() => {
          e.focus()
        }, 100)
      }
      const eleId = ele ? (ele.getAttribute('id') ? ele.getAttribute('id') : '') : ''
      const num = eleId ? parseInt(eleId.toString().replace('lotRadio', '')) : 0
      if ((length >= 1 && num >= length) || isNaN(num)) {
        e = document.getElementById('lotRadio0')
        setTimeout(() => {
          e.focus()
        }, 100)
      } else if (length >= 1 && num < length) {
        e = document.getElementById(`lotRadio${num + 1}`)
        setTimeout(() => {
          e.focus()
        }, 100)
      }
    }
  }

  const toPreviousRadio = () => {
    const ele = document.activeElement
    let e: any = null
    if (lotAssignmentMethod == 1) {
      if (
        ele &&
        (ele.getAttribute('id') == null ||
          ele.getAttribute('id')?.indexOf('headerRadio') == -1 ||
          ele.getAttribute('id')?.indexOf('lotRadio') == -1)
      ) {
        e = document.getElementById('headerRadio1')
        setTimeout(() => {
          e.focus()
        }, 100)
      }

      if (ele && ele.getAttribute('id')) {
        if (ele.getAttribute('id') == 'headerRadio1') {
          setRadioSelect(0)
          e = document.getElementById('headerRadio2')
          setTimeout(() => {
            e.focus()
          }, 100)
          return
        }

        if (ele.getAttribute('id') == 'headerRadio2') {
          e = document.getElementById('lotRadio0')
          setTimeout(() => {
            e.focus()
          }, 100)
          return
        }

        const length = dataSource.length
        const eleId = ele ? (ele.getAttribute('id') ? ele.getAttribute('id') : '') : ''
        const num = eleId ? parseInt(eleId.toString().replace('lotRadio', '')) : 0
        if (length >= 1 && num >= length) {
          e = document.getElementById('headerRadio1')
          setTimeout(() => {
            e.focus()
          }, 100)
        } else if (length >= 1 && num < length) {
          e = document.getElementById(`lotRadio${num + 1}`)
          setTimeout(() => {
            e.focus()
          }, 100)
        }
      }
    }

    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      const length = dataSource.length
      if (
        ele &&
        (ele.getAttribute('id') == null ||
          ele.getAttribute('id')?.indexOf('headerRadio') == -1 ||
          ele.getAttribute('id')?.indexOf('lotRadio') == -1)
      ) {
        e = document.getElementById('lotRadio0')
        setTimeout(() => {
          e.focus()
        }, 100)
      }
      const eleId = ele ? (ele.getAttribute('id') ? ele.getAttribute('id') : '') : ''
      const num = eleId ? parseInt(eleId.toString().replace('lotRadio', '')) : 0
      if ((length >= 1 && num == 0) || isNaN(num)) {
        e = document.getElementById(`lotRadio${length}`)
        setTimeout(() => {
          e.focus()
        }, 100)
      } else if (length >= 1 && num <= length) {
        e = document.getElementById(`lotRadio${num - 1}`)
        setTimeout(() => {
          e.focus()
        }, 100)
      }
    }
  }

  //auto set value in table

  const setNotSelectAsync = () => {
    const totalQuantity = _.compact(getDataSource().map((v) => v.quantity)).reduce((n, p) => Number(n) + Number(p), 0)
    const totalPicked = _.compact(getDataSource().map((v) => v.picked)).reduce((n, p) => Number(n) + Number(p), 0)
    const quantityWithUom = selectUOM == currentRecord.inventoryUOM ? baseQuantity : baseQuantity * UOMRatio
    const pickedWithUom = selectUOM == currentRecord.inventoryUOM ? basePicked : basePicked * UOMRatio
    setDefaultOrderQty(Math.floor(quantityWithUom * 100000000000000) / 100000000000000 - totalQuantity)
    setDefaultPickedQty(Math.floor(pickedWithUom * 100000000000000) / 100000000000000 - totalPicked)

    const _quantity = parseFloat((quantityWithUom - totalQuantity).toFixed(2))
    const _pick = parseFloat((pickedWithUom - totalPicked).toFixed(2))
    setNotSelectAllQuantityAndPicked(roundNum(_quantity * 100) / 100, roundNum(_pick * 100) / 100)
  }

  // get data from database
  useEffect(() => {
    setLoading(true)
    if (!updateOrderQuantityLoading.fetching) {
      OrderService.instance
        .getOrderLots({ productId: currentRecord.itemId, lotIds: currentRecord.lotIds.join(',') })
        .subscribe({
          next(res: any) {
            of(responseHandler(res, false).body.data)
            const data = res.body.data.map((item: any) => {
              const findOrderItem: any = _.intersectionBy(orderItems, currentRecord.items, 'wholesaleOrderItemId').find(
                (orderItem: any) => orderItem.lotId === item.lotId,
              )
              if (findOrderItem) {
                const quantity = ratioQtyToInventoryQty(
                  currentRecord.UOM,
                  findOrderItem.quantity || 0,
                  currentRecord,
                  12,
                )
                const picked = ratioQtyToInventoryQty(currentRecord.UOM, findOrderItem.picked || 0, currentRecord, 12)
                return { ...item, lotAvailableQty: item.lotAvailableQty, quantity, picked }
              }
              return { ...item, lotAvailableQty: item.lotAvailableQty }
            })
            setDataSource(_.cloneDeep(data))
            setOriginDataSource(_.cloneDeep(data))
            if (currentRecord.UOM !== currentRecord.inventoryUOM) {
              const ratioUom = currentRecord.wholesaleProductUomList.find((x: any) => x.name == currentRecord.UOM)
              const formatData = data.map((v) => {
                if (v.hasOwnProperty('quantity')) {
                  const quantityNum =
                    Math.floor(parseFloat(v.quantity * (ratioUom ? ratioUom.ratio : 1) * 1000000000000)) / 1000000000000
                  v.quantity = quantityNum
                }
                if (v.hasOwnProperty('picked')) {
                  const pickNum =
                    Math.floor(parseFloat(v.picked * (ratioUom ? ratioUom.ratio : 1) * 1000000000000)) / 1000000000000
                  v.picked = pickNum
                }
                return v
              })
              setPriceUOMDataSource(_.cloneDeep(formatData))
            }
          },
          error(err) {
            window.location.href = '/#/login'
            window.location.reload()
          },
          complete() {
            setLoading(false)
          },
        })
    }
  }, [refresh, updateOrderQuantityLoading.fetching])

  //not select functions

  const setNotSelectAllQuantityAndPicked = (quantity: number, picked: number) => {
    const notSelectTable = _.cloneDeep(notSelect)
    notSelectTable.quantity = quantity
    notSelectTable.picked = picked
    setNotSelect(notSelectTable)
  }

  const roundNum = (num: number) => Math.floor(parseFloat((num * 100).toFixed(2))) / 100

  const getIniDataSource = () => {
    const dataSourceArr =
      selectUOM == currentRecord.inventoryUOM ? _.cloneDeep(originDataSource) : _.cloneDeep(originDataSource)
    const result = dataSourceArr.map((item, index) => {
      const findItem = currentItems.find((i) => i.lotId === item.lotId)
      if (findItem) {
        const findItemQuantity = ratioQtyToInventoryQty(currentRecord.UOM, findItem.quantity || 0, currentRecord, 12)
        const findItemQuantityWithUom = findItemQuantity
        const pickValue = ratioQtyToInventoryQty(currentRecord.UOM, findItem.picked, currentRecord, 12) || 0
        const pickValueWithUom =
          selectUOM == currentRecord.inventoryUOM
            ? pickValue
            : Math.floor(pickValue * UOMRatio * 100000000000) / 100000000000
        const quantityValueWithUom =
          selectUOM == currentRecord.inventoryUOM
            ? findItemQuantity
            : Math.floor(findItemQuantity * UOMRatio * 100000000000) / 100000000000
        return {
          ...item,
          lotAvailableQty: item.lotAvailableQty + findItemQuantityWithUom,
          quantity: quantityValueWithUom || '',
          picked: pickValueWithUom || '',
        }
      }
      return item
    })
    return result
  }

  const getDataSource = () => {
    const dataSourceArr = _.cloneDeep(dataSource)

    if (lotAssignmentMethod === 1) {
      return enableLotOverflow ? getOverflowDataSource(dataSourceArr) : getOldestDataSource(dataSourceArr)
    }
    if (lotAssignmentMethod === 3) {
      const result = dataSourceArr.map((v, i) => {
        if (currentItems.find((i) => i.lotId === v.lotId)) {
          const findItem = currentItems.find((i) => i.lotId === v.lotId)
          const findItemQuantity = toFixed2(
            ratioQtyToInventoryQty(currentRecord.UOM, findItem.quantity || 0, currentRecord, 12),
          )
          let lotAvailableQty = ''
          if (originDataSource[i] && originDataSource[i].hasOwnProperty('lotAvailableQty')) {
            if (originDataSource[i]['lotAvailableQty'] == v.lotAvailableQty) {
              lotAvailableQty = v.lotAvailableQty + findItemQuantity
            } else {
              lotAvailableQty = v.lotAvailableQty
            }
          }
          const quantity = v.quantity
          const picked = v.picked
          return { ...v, lotAvailableQty, quantity, picked }
        }
        return v
      })

      if (
        currentRecord.lotAssignmentMethod == 3 &&
        currentRecord.manuallyAssignState == true &&
        manuallyAssignState == true &&
        (update == 1 || edit == true) &&
        selectUOM !== currentRecord.inventoryUOM
      ) {
        result.map((item) => {
          item.quantity = item.quantity
            ? typeof item.quantity == 'number' && !isNaN(item.quantity)
              ? (item.quantity * UOMRatio).toFixed(2)
              : item.quantity
            : item.quantity
          item.picked = item.picked
            ? typeof item.picked == 'number' && !isNaN(item.picked)
              ? (item.picked * UOMRatio).toFixed(2)
              : item.picked
            : item.picked
          return item
        })
      }

      return result
    }

    return _.cloneDeep(dataSourceArr)
  }

  const getTableDate = () => {
    let data = update <= 1 && enableLotOverflow == true ? getIniDataSource() : getDataSource()
    if (currentRecord.lotAssignmentMethod == 3 && currentRecord.manuallyAssignState == false && update == 1) {
      data = getDataSource()
    }
    if (currentRecord.lotAssignmentMethod == 3 && currentRecord.manuallyAssignState == true && update == 1) {
      data = getDataSource()
    }
    const r: any[] = []
    data.forEach((item) => {
      item.quantity = item.quantity
        ? typeof item.quantity == 'number' && !isNaN(item.quantity)
          ? (Math.floor(parseFloat(item.quantity * 1000000000)) / 1000000000).toFixed(2)
          : item.quantity.toString()
        : ''
      item.picked = item.picked
        ? typeof item.picked == 'number' && !isNaN(item.picked)
          ? (Math.ceil(parseFloat(item.picked * 1000000000)) / 1000000000).toFixed(2)
          : item.picked.toString()
        : ''

      r.push(item)
    })
    const newNotSelect: any = _.cloneDeep(notSelect)
    if (notSelect.quantity == 0 && update == 1) {
      newNotSelect.quantity = ''
    } else {
      newNotSelect.quantity = newNotSelect.quantity
        ? typeof newNotSelect.quantity == 'number' && !isNaN(newNotSelect.quantity)
          ? (Math.floor(parseFloat(newNotSelect.quantity * 1000000000)) / 1000000000).toFixed(2)
          : newNotSelect.quantity.toString()
        : ''
    }
    if (notSelect.picked == 0 && update == 1) {
      newNotSelect.picked = ''
    } else {
      newNotSelect.picked = newNotSelect.picked
        ? typeof newNotSelect.picked == 'number' && !isNaN(newNotSelect.picked)
          ? (Math.floor(parseFloat(newNotSelect.picked * 1000000000)) / 1000000000).toFixed(2)
          : newNotSelect.picked.toString()
        : ''
    }
    r.unshift(newNotSelect)

    return r
  }

  const getDataSourceNoLotSelect = () => {
    const findItem = currentItems.find((item) => {
      if (item.lotId == undefined) {
        return item
      }
    })
    if (findItem) {
      const findItemQuantity = ratioQtyToInventoryQty(currentRecord.UOM, findItem.quantity || 0, currentRecord, 12)
      const findItemQuantityWithUom =
        selectUOM == currentRecord.inventoryUOM
          ? findItemQuantity
          : Math.floor(parseFloat((findItemQuantity * UOMRatio * 100).toFixed(2))) / 100
      const pickValue = ratioQtyToInventoryQty(currentRecord.UOM, findItem.picked, currentRecord, 12) || 0
      const pickValueWithUom =
        selectUOM == currentRecord.inventoryUOM
          ? pickValue
          : Math.floor(parseFloat((pickValue * UOMRatio * 100).toFixed(2))) / 100
      setDefaultOrderQty(findItemQuantityWithUom)
      setDefaultPickedQty(pickValueWithUom)
      setNotSelectAllQuantityAndPicked(
        parseFloat(findItemQuantityWithUom.toFixed(2)),
        parseFloat(pickValueWithUom.toFixed(2)),
      )
    } else {
      setNotSelectAllQuantityAndPicked(0, 0)
    }
  }

  //column
  const columns = [
    {
      title: '',
      dataIndex: '',
      align: 'left',
      width: 10,
      render: (t: any, record: any, index: any) => {
        if (manuallyAssignState || lotAssignmentMethod !== 3) {
          return
        }
        return <Radio id={`lotRadio${index}`} value={index}></Radio>
      },
    },
    {
      title: 'ORDER QTY',
      dataIndex: 'quantity',
      align: 'left',
      width: 130,
      render: (t: any, record: any, index: any) => {
        let className = 'lotInput'
        const lotAvailableQty =
          selectUOM == currentRecord.inventoryUOM ? record.lotAvailableQty : record.lotAvailableQty * UOMRatio
        const qty =
          typeof record.quantity == 'number'
            ? record.quantity.toFixed(2)
            : !isNaN(parseFloat(record.quantity))
            ? parseFloat(record.quantity).toFixed(2)
            : 0
        const lotQty =
          typeof lotAvailableQty == 'number'
            ? lotAvailableQty.toFixed(2)
            : !isNaN(parseFloat(lotAvailableQty))
            ? parseFloat(lotAvailableQty).toFixed(2)
            : '0'
        if (parseFloat(qty) > parseFloat(lotQty)) {
          className = 'warning lotInput'
          if ((enableLotOverflow && lotAvailableQty <= 0) || record.deliveryDate == 'Not Lot Selected') {
            className = 'lotInput'
          }
        }
        return (
          <div onDoubleClick={(e) => doubleClick(`quantity${index}`)}>
            <Input
              id={`quantity${index}`}
              style={{ textAlign: 'right' }}
              disabled={lotAssignmentMethod == 1 || manuallyAssignState == false}
              className={className}
              onChange={(e) => handleChangeQuantity(record, e.target.value)}
              value={t}
            />
          </div>
        )
      },
    },
    {
      title: 'PICKED QTY',
      dataIndex: 'picked',
      align: 'left',
      width: 130,
      className: `${isDisablePickingStep && 'displayNone'}`,
      render: (t: any, record: any, index: number) => {
        let className = 'lotInput'
        const lotAvailableQty =
          selectUOM == currentRecord.inventoryUOM ? record.lotAvailableQty : record.lotAvailableQty * UOMRatio

        const picked =
          typeof record.picked == 'number'
            ? record.picked.toFixed(2)
            : !isNaN(parseFloat(record.picked))
            ? parseFloat(record.picked).toFixed(2)
            : 0
        const lotQty =
          typeof lotAvailableQty == 'number'
            ? lotAvailableQty.toFixed(2)
            : !isNaN(parseFloat(lotAvailableQty))
            ? parseFloat(lotAvailableQty).toFixed(2)
            : '0'

        if (parseFloat(picked) > parseFloat(lotQty)) {
          className = 'warning lotInput'
          if ((enableLotOverflow && lotAvailableQty <= 0) || record.deliveryDate == 'Not Lot Selected') {
            className = 'lotInput'
          }
        }
        return (
          <div onDoubleClick={(e) => doubleClick(`picked${index}`)}>
            <Input
              id={`picked${index}`}
              style={{ textAlign: 'right' }}
              disabled={lotAssignmentMethod == 1 || manuallyAssignState == false}
              className={className}
              onChange={(e) => handleChangePicked(record, e.target.value)}
              value={t}
            />
          </div>
        )
      },
    },
    {
      dataIndex: 'tip',
      width: 20,
      render: (t: any, r: any) => {
        if (!r.onHandQty) {
          if (!r.onHandQty && r.deliveryDate !== 'Not Lot Selected') {
            return (
              <Tooltip placement="top" title={'Lot has not been received yet.'}>
                <Icon type="warning" className="red" />
              </Tooltip>
            )
          }
        }
      },
    },
    {
      title: 'DELIVERY DATE',
      dataIndex: 'deliveryDate',
      align: 'left',
      width: 150,
      render: (t: any) => {
        if (t !== 'Not Lot Selected') {
          return moment.utc(t).format('MM/DD/YYYY')
        } else {
          return `no lot selected`
        }
      },
    },
    {
      title: 'LOT #',
      dataIndex: 'lotId',
      align: 'left',
    },
    {
      title: 'VENDOR REF #',
      dataIndex: 'vendorRefNo',
      align: 'left',
    },
    {
      title: 'AVAILABLE TO SELL',
      align: 'right',
      dataIndex: 'lotAvailableQty',
      render: (t: any, r: any) => `${toFixed2(t)} ${r.inventoryUOM}`,
    },
    {
      title: 'UNITS ON HAND',
      align: 'right',
      dataIndex: 'onHandQty',
      render: (t: any, r: any) => {
        if (!t) {
          return <span className="red">{`${t} ${r.inventoryUOM}`}</span>
        }
        return `${toFixed2(t)} ${r.inventoryUOM}`
      },
    },
    {
      title: 'COST',
      align: 'right',
      dataIndex: 'cost',
      render: (t: any, r: any) => {
        if (r.deliveryDate !== 'Not Lot Selected') {
          return `$${formatNumber(t + r.perAllocateCost, 2)} ${r.inventoryUOM}`
        } else {
          return ``
        }
      },
    },
    {
      title: 'VENDOR',
      align: 'left',
      dataIndex: 'companyName',
    },
    {
      title: 'BRAND',
      align: 'left',
      dataIndex: 'brand',
    },
    {
      title: 'ORIGIN',
      align: 'left',
      dataIndex: 'origin',
    },
    {
      title: 'NOTES FROM PO',
      align: 'left',
      dataIndex: 'note',
    },
  ]

  // save functions

  const handleChangeQuantity = (record: any, value: any) =>
    changQuantityFilter(record, dataSource, setDataSource, value)

  const changQuantityFilter = (record: any, arr: any[], setFunction: (v: any) => void, value: any) => {
    setUpdate(9999)
    if (record.deliveryDate !== 'Not Lot Selected') {
      setFunction(
        arr.map((v) => {
          if (v.lotId === record.lotId && !isNaN(value) && value.toString().indexOf(' ') == -1) {
            return { ...v, quantity: value }
          }
          return v
        }),
      )
    } else {
      if (!isNaN(value) && value.toString().indexOf(' ') == -1) {
        setDefaultOrderQty(value)
        setNotSelectAllQuantityAndPicked(value, notSelect.picked)
      }
    }
  }
  const handleChangePicked = (record: any, value: any) => changPickedFilter(record, dataSource, setDataSource, value)

  const changPickedFilter = (record: any, arr: any[], setFunction: (v: any) => void, value: any) => {
    setUpdate(9999)
    if (record.deliveryDate !== 'Not Lot Selected') {
      setFunction(
        arr.map((v) => {
          if (v.lotId === record.lotId && !isNaN(value) && value.toString().indexOf(' ') == -1) {
            return { ...v, picked: value }
          }
          return v
        }),
      )
    } else {
      if (!isNaN(value) && value.toString().indexOf(' ') == -1) {
        setDefaultPickedQty(value)
        setNotSelectAllQuantityAndPicked(notSelect.quantity, value)
      }
    }
  }

  const toggleRepackModal = (isReload?: boolean) => {
    if (isReload === true) {
      handleRefresh();
    }
    setRepackModalVisible(!repackModalVisible)
  }

  const handleSaveVerify = () => {
    const baseQuantityWithUOM =
      selectUOM == currentRecord.inventoryUOM
        ? baseQuantity
        : Math.floor(baseQuantity * UOMRatio * 1000000000000) / 1000000000000
    if (lotAssignmentMethod !== 2) {
      if (roundNum(parseFloat(TotalQuantity())) - roundNum(baseQuantityWithUOM) > 0.01) return
      if (roundNum(baseQuantityWithUOM) - roundNum(parseFloat(TotalQuantity())) > 0.01)
        return setIsTotalLessCurrent(true)
      if (!isDisablePickingStep) {
        const basePickedWithUOM =
          selectUOM == currentRecord.inventoryUOM
            ? basePicked
            : Math.floor(basePicked * UOMRatio * 1000000000000) / 1000000000000
        if ((roundNum(parseFloat(TotalPicked())) - Number(basePickedWithUOM))>0.01)
          return setConfirmUpdatePickedModalVisible(true)
      }
    }

    handleSaveChange()
  }

  const handleSaveChange = () => {
    setSaveButtonLoading(true)
    setConfirmUpdatePickedModalVisible(false)

    const findItem = currentItems.find((item) => {
      if (item.lotId == undefined) {
        return item
      }
    })

    if (lotAssignmentMethod === 1) {
      const _dataSource = getDataSource()
      const arr =
        selectUOM == currentRecord.inventoryUOM
          ? _dataSource
          : _dataSource.map((v) => {
              if (v.hasOwnProperty('quantity')) {
                v.quantity = v.quantity / UOMRatio
              }
              if (v.hasOwnProperty('picked')) {
                v.picked = v.picked / UOMRatio
              }
              return v
            })
      const newArr = _.cloneDeep(arr)
      const postData: any[] = []
      newArr.reverse().forEach((item, index) => {
        const quantityNum = inventoryQtyToRatioQty(currentRecord.UOM, item.quantity || 0, currentRecord, 12)
        const pickedNum = isDisablePickingStep
          ? inventoryQtyToRatioQty(currentRecord.UOM, item.quantity || 0, currentRecord, 12)
          : inventoryQtyToRatioQty(currentRecord.UOM, item.picked || 0, currentRecord, 12)

        if (quantityNum > 0 || pickedNum > 0) {
          postData.push({
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            lotId: item.lotId,
            quantity: quantityNum,
            picked: pickedNum,
            wholesaleOrderItemId: currentItems.find((v) => v.lotId === item.lotId)
              ? currentItems.find((v) => v.lotId === item.lotId).wholesaleOrderItemId
              : undefined,
          })
        }
      })

      const pickedDisableNum = isDisablePickingStep ? notSelect.quantity : notSelect.picked

      if (parseFloat(notSelect.quantity.toString()) > 0 || parseFloat(pickedDisableNum.toString()) > 0) {
        const quantityWithoutNull = notSelect.quantity && notSelect.quantity.toString() !== '' ? notSelect.quantity : 0
        const pickedWithoutNull = pickedDisableNum.toString() !== '' ? pickedDisableNum : 0
        // if notSelect exist
        if (findItem) {
          //update
          postData.push({
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            quantity: selectUOM == currentRecord.UOM ? quantityWithoutNull : quantityWithoutNull * UOMRatio,
            picked: selectUOM == currentRecord.UOM ? pickedWithoutNull : pickedWithoutNull * UOMRatio,
            wholesaleOrderItemId: findItem.wholesaleOrderItemId,
          })
        } else {
          //add
          postData.push({
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            quantity: selectUOM == currentRecord.UOM ? quantityWithoutNull : quantityWithoutNull * UOMRatio,
            picked: selectUOM == currentRecord.UOM ? pickedWithoutNull : pickedWithoutNull * UOMRatio,
          })
        }
      } else {
        if (findItem) {
          //delete
          postData.push({
            wholesaleOrderItemId: findItem.wholesaleOrderItemId,
            isDeleted: true,
          })
        }
      }

      getDataSource().forEach((v) => {
        const findItem = currentItems.find((c) => c.lotId === v.lotId)
        if (findItem) {
          if (isDisablePickingStep) {
            if (!Number(v.quantity)) {
              postData.push({ wholesaleOrderItemId: findItem.wholesaleOrderItemId, isDeleted: true })
            }
          } else {
            if (!Number(v.quantity) && !Number(v.picked)) {
              postData.push({ wholesaleOrderItemId: findItem.wholesaleOrderItemId, isDeleted: true })
            }
          }
        }
      })

      const defaultExistOrderItemId = currentRecord['wholesaleOrderItemId']
      let newPostData: any[] = []

      if (
        (postData[0]['isDeleted'] && postData[0]['isDeleted'] == true) ||
        postData[0]['wholesaleOrderItemId'] != defaultExistOrderItemId
      ) {
        const whilesaleOrderItemIdArr = postData.map((item) => {
          if (item['wholesaleOrderItemId']) {
            return item.wholesaleOrderItemId
          } else {
            return ''
          }
        })

        const defaultIdIndex = postData.findIndex((item) => item['wholesaleOrderItemId'] == defaultExistOrderItemId)

        postData.forEach((item, index) => {
          const obj = item
          if (index == 0) {
            const addObj = {
              ...obj,
              wholesaleOrderItemId: defaultExistOrderItemId,
            }
            newPostData.push(addObj)
          } else if (index <= defaultIdIndex) {
            if (whilesaleOrderItemIdArr[index - 1] != '') {
              const addObj = {
                ...obj,
                wholesaleOrderItemId: whilesaleOrderItemIdArr[index - 1],
              }
              newPostData.push(addObj)
            } else {
              const addObj = {
                ...obj,
                displayOrder: obj.displayOrder,
                quantity: obj.quantity,
                picked: obj.picked,
                wholesaleOrderItemId: undefined,
              }
              newPostData.push(addObj)
            }
          } else {
            /*if (obj['isDeleted'] == true && obj['wholesaleOrderItemId'] == undefined) {
              console.log(obj)
            } else {
              newPostData.push(obj)
            }*/
            newPostData.push(obj)
          }
        })
      } else {
        newPostData = postData
      }

      console.log(newPostData)

      const _newPostData: any[] = []
      newPostData.forEach((item) => {
        if (item['isDeleted'] == true && item['wholesaleOrderItemId'] == undefined) {
          console.log(item)
        } else {
          _newPostData.push(item)
        }
      })

      const data = {
        lotAssignmentMethod,
        enableLotOverflow,
        existOrderItemId: defaultExistOrderItemId,
        orderItemList: _newPostData,
      }

      return sendRequest({ orderId: currentOrder.wholesaleOrderId, data })
    } else if (lotAssignmentMethod === 3) {
      let arr = []
      if (manuallyAssignState == true && edit) {
        arr = getDataSource()
      } else {
        arr =
          selectUOM == currentRecord.inventoryUOM
            ? dataSource
            : dataSource.map((v) => {
                if (v.hasOwnProperty('quantity')) {
                  v.quantity = v.quantity / UOMRatio
                }
                if (v.hasOwnProperty('picked')) {
                  v.picked = v.picked / UOMRatio
                }
                return v
              })
      }

      const newArr = _.cloneDeep(arr)
      const postData: any[] = []
      newArr.reverse().forEach((item, index) => {
        let quantityNum = 0
        let pickedNum = 0
        if (manuallyAssignState == true && edit) {
          quantityNum = item.quantity
          pickedNum = item.picked
        } else {
          quantityNum = inventoryQtyToRatioQty(currentRecord.UOM, item.quantity || 0, currentRecord, 12)
          pickedNum = isDisablePickingStep
            ? inventoryQtyToRatioQty(currentRecord.UOM, item.quantity || 0, currentRecord, 12)
            : inventoryQtyToRatioQty(currentRecord.UOM, item.picked || 0, currentRecord, 12)
        }

        if (quantityNum > 0 || pickedNum > 0) {
          postData.push({
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            lotId: item.lotId,
            quantity: quantityNum && quantityNum.toString() !== '' ? quantityNum : 0,
            picked: pickedNum && pickedNum.toString() !== '' ? pickedNum : 0,
            wholesaleOrderItemId: currentItems.find((v) => v.lotId === item.lotId)
              ? currentItems.find((v) => v.lotId === item.lotId).wholesaleOrderItemId
              : undefined,
          })
        }
      })

      const pickedDisableNum = isDisablePickingStep ? notSelect.quantity : notSelect.picked

      if (parseFloat(notSelect.quantity.toString()) > 0 || parseFloat(pickedDisableNum.toString()) > 0) {
        const quantityWithoutNull = notSelect.quantity && notSelect.quantity.toString() !== '' ? notSelect.quantity : 0
        const pickedWithoutNull = pickedDisableNum.toString() !== '' ? pickedDisableNum : 0
        // if notSelect exist
        if (findItem) {
          //update
          postData.push({
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            quantity: selectUOM == currentRecord.UOM ? quantityWithoutNull : quantityWithoutNull * UOMRatio,
            picked: selectUOM == currentRecord.UOM ? pickedWithoutNull : pickedWithoutNull * UOMRatio,
            wholesaleOrderItemId: findItem.wholesaleOrderItemId,
          })
        } else {
          //add
          postData.push({
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            quantity: selectUOM == currentRecord.UOM ? quantityWithoutNull : quantityWithoutNull * UOMRatio,
            picked: selectUOM == currentRecord.UOM ? pickedWithoutNull : pickedWithoutNull * UOMRatio,
          })
        }
      } else {
        if (findItem) {
          //delete
          postData.push({
            wholesaleOrderItemId: findItem.wholesaleOrderItemId,
            isDeleted: true,
          })
        }
      }

      newArr.forEach((v) => {
        const findItem = currentItems.find((c) => c.lotId === v.lotId)
        if (findItem) {
          if (isDisablePickingStep) {
            if (!Number(v.quantity)) {
              postData.push({ wholesaleOrderItemId: findItem.wholesaleOrderItemId, isDeleted: true })
            }
          } else {
            if (!Number(v.quantity) && !Number(v.picked)) {
              postData.push({ wholesaleOrderItemId: findItem.wholesaleOrderItemId, isDeleted: true })
            }
          }
        }
      })
      const defaultExistOrderItemId = currentRecord['wholesaleOrderItemId']
      let newPostData: any[] = []

      if (
        (postData[0]['isDeleted'] && postData[0]['isDeleted'] == true) ||
        postData[0]['wholesaleOrderItemId'] != defaultExistOrderItemId
      ) {
        const whilesaleOrderItemIdArr = postData.map((item) => {
          if (item['wholesaleOrderItemId']) {
            return item.wholesaleOrderItemId
          } else {
            return ''
          }
        })

        const defaultIdIndex = postData.findIndex((item) => item['wholesaleOrderItemId'] == defaultExistOrderItemId)

        postData.forEach((item, index) => {
          const obj = item
          if (index == 0) {
            const addObj = {
              ...obj,
              wholesaleOrderItemId: defaultExistOrderItemId,
            }
            newPostData.push(addObj)
          } else if (index <= defaultIdIndex) {
            if (whilesaleOrderItemIdArr[index - 1] != '') {
              const addObj = {
                ...obj,
                wholesaleOrderItemId: whilesaleOrderItemIdArr[index - 1],
              }
              newPostData.push(addObj)
            } else {
              const addObj = {
                ...obj,
                displayOrder: obj.displayOrder,
                quantity: obj.quantity,
                picked: obj.picked,
                wholesaleOrderItemId: undefined,
              }
              newPostData.push(addObj)
            }
          } else {
            newPostData.push(obj)
          }
        })
      } else {
        newPostData = postData
      }
      const _newPostData: any[] = []
      newPostData.forEach((item) => {
        if (item['isDeleted'] == true && item['wholesaleOrderItemId'] == undefined) {
          console.log(item)
        } else {
          _newPostData.push(item)
        }
      })

      const data = {
        lotAssignmentMethod,
        enableLotOverflow: enableLotOverflow,
        existOrderItemId: defaultExistOrderItemId,
        manuallyAssignState: manuallyAssignState,
        orderItemList: _newPostData,
      }

      return sendRequest({ orderId: currentOrder.wholesaleOrderId, data })
    }
  }

  const sendRequest = ({ orderId, data }: any) => {
    OrderService.instance.saveOrderLots({ orderId, data }).subscribe({
      next(res: any) {
        if (res.statusCode === 'CONFLICT') return setTimeout(() => sendRequest({ orderId, data }), 500)
        getOrderItemsById(currentOrder.wholesaleOrderId)
        setSaveButtonLoading(false)
        onCancel()
      },
    })
  }

  useEffect(() => {
    const timer = setInterval(() => {
      if (counter > 0) {
        setCounter(counter + 1)
      }
    }, 1000)
    return () => {
      clearInterval(timer)
    }
  }, [counter])

  const handleRefresh = () => {
    setCounter(1)
    setRefresh(refresh + 1)
  }

  const TotalQuantity = () => {
    if (update <= 1 && enableLotOverflow == true) {
      const notSelectQuantity = notSelect.quantity.toString() == '' ? 0 : parseFloat(notSelect.quantity.toString())
      if (currentRecord.lotAssignmentMethod == 3 && currentRecord.manuallyAssignState == false && update == 1) {
        return (
          parseFloat(_.compact(getDataSource().map((v) => v.quantity)).reduce((n, p) => Number(n) + Number(p), 0)) +
          notSelectQuantity
        ).toFixed(2)
      } else {
        if (radioSelect !== -1) {
          return (
            parseFloat(_.compact(getDataSource().map((v) => v.quantity)).reduce((n, p) => Number(n) + Number(p), 0)) +
            notSelectQuantity
          ).toFixed(2)
        } else {
          return (
            parseFloat(
              _.compact(getIniDataSource().map((v) => v.quantity)).reduce((n, p) => Number(n) + Number(p), 0),
            ) + notSelectQuantity
          ).toFixed(2)
        }
      }
    } else {
      const notSelectQuantity = notSelect.quantity.toString() == '' ? 0 : parseFloat(notSelect.quantity.toString())
      let totalQuantity = _.compact(getDataSource().map((v) => v.quantity)).reduce((n, p) => Number(n) + Number(p), 0)
      return (parseFloat(totalQuantity) + notSelectQuantity).toFixed(2)
    }
  }
  const TotalPicked = () => {
    if (update <= 1 && enableLotOverflow == true) {
      const notSelectPicked = notSelect.picked.toString() == '' ? 0 : parseFloat(notSelect.picked.toString())
      if (currentRecord.lotAssignmentMethod == 3 && currentRecord.manuallyAssignState == false && update == 1) {
        return (
          parseFloat(_.compact(getDataSource().map((v) => v.picked)).reduce((n, p) => Number(n) + Number(p), 0)) +
          notSelectPicked
        ).toFixed(2)
      } else {
        if(manuallyAssignState){
          return (
            parseFloat(_.compact(getIniDataSource().map((v) => v.picked)).reduce((n, p) => Number(n) + Number(p), 0)) +
            notSelectPicked
          ).toFixed(2)
        }else{
          return (
            parseFloat(_.compact(getDataSource().map((v) => v.picked)).reduce((n, p) => Number(n) + Number(p), 0)) +
            notSelectPicked
          ).toFixed(2)
        }
      }
    } else {
      const notSelectPicked = notSelect.picked.toString() == '' ? 0 : parseFloat(notSelect.picked.toString())
      let totalPicked = _.compact(getDataSource().map((v) => v.picked)).reduce((n, p) => Number(n) + Number(p), 0)
      return (parseFloat(totalPicked) + notSelectPicked).toFixed(2)
    }
  }

  const getErrorMessage = () => {
    const baseQuantityWithUOM =
      selectUOM == currentRecord.inventoryUOM
        ? baseQuantity
        : Math.ceil(baseQuantity * UOMRatio * 1000000000000) / 1000000000000
    if (lotAssignmentMethod === 2) return
    if (roundNum(parseFloat(TotalQuantity())) - parseFloat(baseQuantityWithUOM.toFixed(2)) > 0.01)
      return 'You have allocated too many units'
    if (isTotalLessCurrent) return 'Please complete lot assignment to continue.'
  }

  const handleChangeLotAssignmentMethod = (value: any) => {
    setDataSource(originDataSource)
    setLotAssignmentMethod(value)
  }

  const switchManuallyState = (e: any) => {
    if (lotAssignmentMethod == 3) {
      setManuallyAssignState(e)
    } else {
      setLotAssignmentMethod(3)
      setManuallyAssignState(true)
    }
  }

  const doubleClick = (id: string) => {
    setLotAssignmentMethod(3)
    setManuallyAssignState(true)
    localStorage.setItem('doubleClick', '1')
    if (lotAssignmentMethod == 3) {
      localStorage.setItem('doubleClickFromSelect', '1')
    }
    setTimeout(() => {
      ;(document.getElementById(id) as HTMLElement).focus()
      localStorage.setItem('doubleClick', '0')
      localStorage.setItem('doubleClickFromSelect', '0')
    }, 200)
  }

  // copy from _draggable
  const getDefaultPicked = () => {
    return currentRecord.picked === null ||
      typeof currentRecord.picked == 'undefined' ||
      (currentRecord.picked == 0 && (currentRecord.status == 'PLACED' || currentRecord.status == 'NEW'))
      ? ''
      : currentRecord.picked
  }

  return (
    <Main>
      <Header>
        <a
          style={{ fontSize: 16, fontWeight: '700' }}
          href={`#/product/${currentRecord.itemId}/activity`}
          target="_blank"
        >
          <Flex className="v-center">
            {currentRecord.variety}
            <IconSvg
              style={{ marginLeft: 6 }}
              viewBox="0 0 512 512"
              width="16"
              height="16"
              type="external-link-alt-solid"
            />
          </Flex>
        </a>

        <div className="refresh">
          <div>
            <Button onClick={handleRefresh}>Refresh</Button>
          </div>
          {counter > 0 && <p>Refreshed {counter} second ago</p>}
        </div>
      </Header>
      <div className="flex">
        <p className="noBold">
          Order qty: {currentRecord.quantity} {currentRecord.UOM} &nbsp;
          {currentRecord.UOM !== currentRecord.inventoryUOM &&
            `(${baseQuantity.toFixed(2)} ${currentRecord.inventoryUOM})`}
        </p>
        {!isDisablePickingStep && (
          <p style={{ marginLeft: 20 }} className="noBold">
            Picked qty: {getDefaultPicked()} {currentRecord.UOM} &nbsp;
            {currentRecord.UOM !== currentRecord.inventoryUOM &&
              `(${basePicked.toFixed(2)} ${currentRecord.inventoryUOM})`}
          </p>
        )}
      </div>
      {loading ? (
        <Spin />
      ) : (
        <>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 20 }}>
            <Radio.Group onChange={(e) => handleChangeLotAssignmentMethod(e.target.value)} value={lotAssignmentMethod}>
              <ThemeRadio style={radioStyle} value={1} className="noBold" id="headerRadio1">
                Auto-assign lot(s) based on FIFO
                {lotAssignmentMethod === 1 ? (
                  <ThemeCheckbox
                    style={{ marginLeft: 10 }}
                    onChange={(e: any) => setEnableLotOverflow(e.target.checked)}
                    checked={enableLotOverflow}
                  >
                    [BETA] Enable “overflow” to multiple lots{' '}
                  </ThemeCheckbox>
                ) : null}
              </ThemeRadio>
              <ThemeRadio style={radioStyle} value={3} className="noBold" id="headerRadio2">
                Manually assign to lot(s){' '}
                <Switch checked={manuallyAssignState} onChange={(e) => switchManuallyState(e)} />
                {manuallyAssignState ? `  edit quantities` : `  selection mode`}
              </ThemeRadio>
            </Radio.Group>
            <Button onClick={toggleRepackModal}>Enter repack</Button>
            <EnterRepackModal
              sellerSetting={sellerSetting}
              selectedItem={currentRecord}
              visible={repackModalVisible}
              onClose={toggleRepackModal} />
          </div>
          <div style={{ marginBottom: '8px' }}>
            <h6 style={{ fontSize: '15px', fontWeight: 'bold' }}>Unit of measure</h6>
            <Select value={selectUOM} style={{ width: 120 }} onChange={(e: any) => setSelectUOM(e)}>
              {options.map((option, index) => {
                return (
                  <Option key={index} value={option}>
                    {option}
                  </Option>
                )
              })}
            </Select>
          </div>
        </>
      )}
      <Radio.Group
        onChange={(e) => {
          setRadioSelect(e.target.value)
        }}
        value={radioSelect}
      >
        <ThemeTable
          loading={loading}
          dataSource={getTableDate()}
          columns={columns}
          rowKey="purchaseOrderItemId"
          pagination={false}
        />
      </Radio.Group>
      <Count style={{ marginLeft: '20px;' }}>
        <span style={{ marginLeft: !manuallyAssignState && lotAssignmentMethod == 3 ? '40px' : '20px' }}>
          {TotalQuantity()}
        </span>
        {!isDisablePickingStep && (
          <span style={{ marginLeft: manuallyAssignState && lotAssignmentMethod != 3 ? '20px' : '20px' }}>
            {TotalPicked()}
          </span>
        )}
      </Count>
      <WarningText>{getErrorMessage()}</WarningText>
      <ThemeButton loading={saveButtonLoading} onClick={handleSaveVerify}>
        Save changes
      </ThemeButton>{' '}
      <span style={{ cursor: 'pointer', marginLeft: '20px' }} onClick={onCancel}>
        Cancel
      </span>
      {confirmUpdatePickedModalVisible && (
        <ConfirmUpdatePickedModal onCancel={() => setConfirmUpdatePickedModalVisible(false)} onOk={handleSaveChange} />
      )}
    </Main>
  )
}

const ConfirmUpdatePickedModal: FC<{ onCancel: () => void; onOk: () => void }> = ({ onCancel, onOk }) => {
  return (
    <WrapModal
      visible
      title="Update picked qty?"
      onCancel={onCancel}
      className="confirmModal"
      footer={[
        <ThemeButton key={1} onClick={onOk}>
          Update Picked Qty
        </ThemeButton>,
        <span key={2} style={{ marginLeft: 10 }} onClick={onCancel}>
          Cancel
        </span>,
      ]}
    >
      <p>
        Would you like to update the picked quantity value in the sales order to match the quantity you have allocated?
      </p>
    </WrapModal>
  )
}

export default LotSection

const Main = styled.div`
  .ant-table-thead > tr > th {
    padding: 0 10px;
  }
  .warning {
    border: 2px solid #eb5757;
    background-color: #f6d1d1;
  }
  .warning:hover {
    border: 2px solid #eb5757 !important;
  }
  .noBold {
    font-weight: normal;
  }
  .flex {
    display: flex;
    align-items: center;
  }
  .displayNone {
    display: none;
  }
  .red {
    color: rgb(189, 39, 32);
  }
`

const WrapModal = styled(ThemeModal)`
  .ant-modal-footer {
    text-align: left;
  }
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  .refresh {
    text-align: right;
  }
`
const Count = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  display: flex;
  span {
    width: 112px;
    text-align: right;
  }
  .picked {
    margin-left: 18px;
  }
`
const WarningText = styled.p`
  color: #ff0000;
`
