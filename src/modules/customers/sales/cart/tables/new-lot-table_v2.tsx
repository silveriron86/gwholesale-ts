import React, { FC, useEffect, useState } from 'react'
import styled from '@emotion/styled'
import {
  Flex,
  ThemeButton,
  ThemeCheckbox,
  ThemeModal,
  ThemeRadio,
  ThemeTable,
} from '~/modules/customers/customers.style'
import { Button, Radio, Select, Switch, Tooltip, Spin, Input, Icon } from 'antd'
import { formatNumber, ratioQtyToInventoryQty } from '~/common/utils'
import { OrderDetail } from '~/schema'
import _ from 'lodash'
import { Icon as IconSvg } from '~/components'
import { OrderService } from '~/modules/orders/order.service'
import moment from 'moment'
import { format, evaluate } from 'mathjs'
import { useKeyPress } from 'ahooks'
import EnterRepackModal from '../modals/repack-modal/enter-repack-modal'

type TableItem = {
  brand: null
  companyName: string
  cost: string
  deliveryDate: string
  inventoryUOM: string
  lotAvailableQty: string
  lotId: string | null
  note: null
  onHandQty: string
  origin: null
  perAllocateCost: number
  picked: string
  purchaseOrderItemId: number
  quantity: string
}

export enum InputType {
  QUANTITY = 'QUANTITY',
  PICKED = 'PICKED',
}
interface Iprops {
  sellerSetting: any
  currentRecord: any
  currentOrder: OrderDetail
  getOrderItemsById: Function
  onCancel: Function
  updateOrderQuantityLoading: any
  orderItems: any
  isDisablePickingStep: boolean
}

const { Option } = Select

const toFixedString = (value: string) => {
  const num = parseFloat(value)
  return num.toFixed(2)
}

const isNumber = (value: string) => !isNaN(Number(value))

const toStringFixedTwo = (value: string) => {
  if (value) {
    const stringValue = value.toString()
    const dotIndex = stringValue.indexOf('.')
    if (dotIndex > -1) {
      if (dotIndex == stringValue.length - 1) {
        return stringValue.substring(0, dotIndex) + '.'
      } else {
        const dotString = `0${stringValue.substring(dotIndex, stringValue.length)}`
        const fixedString = dotString.length > 4 ? parseFloat(dotString).toFixed(2) : dotString
        return fixedString !== '0'
          ? stringValue.substring(0, dotIndex) + '.' + fixedString.substring(2, fixedString.length)
          : stringValue.substring(0, dotIndex)
      }
    } else {
      return stringValue
    }
  } else {
    return ''
  }
}

const LotSectionV2: FC<Iprops> = ({
  sellerSetting,
  currentRecord,
  currentOrder,
  getOrderItemsById,
  onCancel,
  updateOrderQuantityLoading,
  orderItems,
  isDisablePickingStep,
}) => {
  const [currentItems, setCurrentItems] = useState<any[]>([])
  //state
  const [lotAssignmentMethod, setLotAssignmentMethod] = useState(-1)
  const [enableLotOverflow, setEnableLotOverflow] = useState(false)
  //manually assign switch
  const [manuallyAssignState, setManuallyAssignState] = useState<boolean>(false)
  //whole number
  const [wholeNumberState, setWholeNumberState] = useState<boolean>(false)
  //loading
  const [loading, setLoading] = useState(false)
  const [saveButtonLoading, setSaveButtonLoading] = useState(false)
  //data array
  const [iniTableData, setIniTableData] = useState<TableItem[]>([])
  const [displayTableData, setDisplayTableData] = useState<TableItem[]>([])
  // select measure
  const [selectUOM, setSelectUOM] = useState<string>('')
  const [options, setOptions] = useState<string[]>([])
  const [UOMRatio, setUOMratio] = useState<number>(1)
  // radio select
  const [radioSelect, setRadioSelect] = useState<number>(0)
  const [repacking, setRepacking] = useState<boolean>(false)
  //refresh
  const [counter, setCounter] = useState(0)
  const [refresh, setRefresh] = useState(0)

  const [update, setUpdate] = useState<number>(0)

  const [baseQuantity] = useState(
    ratioQtyToInventoryQty(currentRecord.inventoryUOM, currentRecord.quantity || 0, currentRecord, 12),
  )
  const [basePicked] = useState(
    ratioQtyToInventoryQty(currentRecord.inventoryUOM, currentRecord.picked || 0, currentRecord, 12),
  )

  const [confirmUpdatePickedModalVisible, setConfirmUpdatePickedModalVisible] = useState(false)
  const [confirmUpdateQtyModalVisible, setConfirmUpdateQtyModalVisible] = useState(false)
  const [confirmUpdateUOMModalVisible, setConfirmUpdateUOMModalVisible] = useState(false)

  const [repackModalVisible, setRepackModalVisible] = useState<boolean>(false)

  useEffect(() => {
    setDisplayedData()
  }, [enableLotOverflow, iniTableData, wholeNumberState])

  useEffect(() => {
    //set initial ratio
    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      const index = displayTableData.findIndex((item) => isNumber(item.quantity) && parseFloat(item.quantity) > 0)
      setRadioSelect(index)
    }
    //set ini UOM
    if (currentRecord.UOM !== currentRecord.inventoryUOM) {
      setSelectUOM(currentRecord.UOM)
      setOptions([currentRecord.inventoryUOM, currentRecord.UOM])
      const ratioUom = currentRecord.wholesaleProductUomList.find((x: any) => x.name == currentRecord.UOM)
      setUOMratio(ratioUom ? ratioUom.ratio : 1)
    } else {
      setSelectUOM(currentRecord.inventoryUOM)
      setOptions([currentRecord.inventoryUOM])
    }
    // set manual lot assign method and repacked order item after repack
    if (repacking) {
      setLotAssignmentMethod(3)
      setRadioSelect(1)
      setRepacking(false)
    }
  }, [update])

  /*useEffect(() => {
    const newDisplayData: any = _.cloneDeep(displayTableData)
    const data: TableItem[] = newDisplayData.map((item: TableItem, index: number) => {
      const currentItem = item
      if (selectUOM == currentRecord.inventoryUOM) {
        currentItem.quantity =
          currentItem.quantity !== '' && isNumber(currentItem.quantity)
            ? (parseFloat(currentItem.quantity) / UOMRatio).toFixed(12)
            : ''
        currentItem.picked =
          currentItem.picked !== '' && isNumber(currentItem.picked)
            ? (parseFloat(currentItem.picked) / UOMRatio).toFixed(12)
            : ''
      } else {
        currentItem.quantity =
          currentItem.quantity !== '' && isNumber(currentItem.quantity)
            ? (parseFloat(currentItem.quantity) * UOMRatio).toFixed(12)
            : ''
        currentItem.picked =
          currentItem.picked !== '' && isNumber(currentItem.picked)
            ? (parseFloat(currentItem.picked) * UOMRatio).toFixed(12)
            : ''
      }
      return currentItem
    })
    setDisplayTableData(data)
  }, [selectUOM])*/

  useEffect(() => {
    setDisplayedData()
    if (lotAssignmentMethod == 1) {
      setManuallyAssignState(false)
    }
    if (lotAssignmentMethod == 3 && manuallyAssignState == false) {
      setRadioOverflowDataSource(displayTableData.length - 0 - 1)
      setRadioSelect(0)
    }
  }, [lotAssignmentMethod])

  useEffect(() => {
    if (manuallyAssignState == true && lotAssignmentMethod == 1) {
      setLotAssignmentMethod(3)
    }
    if (manuallyAssignState == false) {
      setRadioOverflowDataSource(displayTableData.length - radioSelect - 1)
    }
  }, [manuallyAssignState])

  useEffect(() => {
    setRadioOverflowDataSource(displayTableData.length - radioSelect - 1)
  }, [radioSelect])

  useEffect(() => {
    //console.log(displayTableData)
  }, [displayTableData])

  useEffect(() => {
    setEnableLotOverflow(currentRecord.enableLotOverflow)
    setLotAssignmentMethod(currentRecord.lotAssignmentMethod == 2 ? 3 : currentRecord.lotAssignmentMethod)

    setCurrentItems(_.intersectionBy(orderItems, currentRecord.items, 'wholesaleOrderItemId'))
    setManuallyAssignState(currentRecord.manuallyAssignState)
    setWholeNumberState(
      sellerSetting.company
        ? sellerSetting.company.isEnableWholeNumber
          ? sellerSetting.company.isEnableWholeNumber
          : false
        : false,
    )
    console.log(sellerSetting)
  }, [currentRecord])

  // get data from database
  useEffect(() => {
    setLoading(true)
    if (!updateOrderQuantityLoading.fetching) {
      OrderService.instance
        .getOrderLots({ productId: currentRecord.itemId, lotIds: currentRecord.lotIds.join(',') })
        .subscribe({
          next(res: any) {
            setIniData(res.body.data)
            //setDisplayedData()
          },
          error(err) {
            window.location.href = '/#/login'
            window.location.reload()
          },
          complete() {
            setLoading(false)
            setUpdate(update + 1)
          },
        })
    }
  }, [refresh, updateOrderQuantityLoading.fetching])

  //refresh
  useEffect(() => {
    const timer = setInterval(() => {
      if (counter > 0) {
        setCounter(counter + 1)
      }
    }, 1000)
    return () => {
      clearInterval(timer)
    }
  }, [counter])

  // keyboard control
  useKeyPress('TAB', () => {
    toNextRadio()
  })

  useKeyPress('shift.TAB', () => {
    if (lotAssignmentMethod == 3 && manuallyAssignState == false) {
      toPreviousRadio()
    }
  })

  const setIniData = (dataBaseArr: any[]) => {
    const organizedItems = _.intersectionBy(orderItems, currentRecord.items, 'wholesaleOrderItemId')
    // find all items that have lot id
    const data: TableItem[] = dataBaseArr.map((item: any) => {
      const findOrderItem: any = organizedItems.find((orderItem: any) => orderItem.lotId === item.lotId)
      if (findOrderItem) {
        const quantity = ratioQtyToInventoryQty(
          currentRecord.inventoryUOM,
          findOrderItem.quantity || 0,
          currentRecord,
          12,
        )
        const picked = ratioQtyToInventoryQty(currentRecord.inventoryUOM, findOrderItem.picked || 0, currentRecord, 12)
        return {
          ...item,
          lotAvailableQty: item.lotAvailableQty,
          quantity: quantity.toString(),
          picked: picked.toString(),
        }
      }
      return { ...item, lotAvailableQty: item.lotAvailableQty }
    })
    //  no lot select
    const noLotSelectItem: any = organizedItems.find((orderItem: any) => orderItem.lotId == undefined)
    if (noLotSelectItem) {
      const notLotSelect: TableItem = {
        brand: null,
        companyName: '',
        cost: '',
        deliveryDate: '',
        inventoryUOM: '',
        lotAvailableQty: '',
        lotId: null,
        note: null,
        onHandQty: '',
        origin: null,
        perAllocateCost: 0,
        picked: ratioQtyToInventoryQty(
          currentRecord.inventoryUOM,
          noLotSelectItem.picked || 0,
          currentRecord,
          12,
        ).toString(),
        purchaseOrderItemId: 0,
        quantity: ratioQtyToInventoryQty(
          currentRecord.inventoryUOM,
          noLotSelectItem.quantity || 0,
          currentRecord,
          12,
        ).toString(),
      }
      data.unshift(notLotSelect)
    } else {
      data.unshift({
        brand: null,
        companyName: '',
        cost: '',
        deliveryDate: '',
        inventoryUOM: '',
        lotAvailableQty: '',
        lotId: null,
        note: null,
        onHandQty: '',
        origin: null,
        perAllocateCost: 0,
        picked: '',
        purchaseOrderItemId: 0,
        quantity: '',
      })
    }
    setIniTableData(data)
    const iniTableDataCopy = _.cloneDeep(data)
    setDisplayTableData(resetLotAvailable(iniTableDataCopy, true))
  }

  const setDisplayedData = () => {
    if (lotAssignmentMethod == 1) {
      enableLotOverflow ? setOverflowDataSource() : setNoOverflowDataSource()
    }
  }

  const resetLotAvailable = (tableData: TableItem[], initial: boolean) => {
    return tableData.map((item: TableItem, index: number) => {
      if (item.lotId != null) {
        let findItemQuantity = isNumber(item.quantity) ? parseFloat(item.quantity) : 0
        // if initial table, should get ratio for this part
        const ratioUom = currentRecord.wholesaleProductUomList.find((x: any) => x.name == currentRecord.UOM)
        const ratio = initial ? (ratioUom ? ratioUom.ratio : 1) : UOMRatio

        findItemQuantity = currentRecord.inventoryUOM == currentRecord.UOM ? findItemQuantity : findItemQuantity / ratio
        const newLotAvailableQty = format(evaluate(`${parseFloat(item.lotAvailableQty)} + ${findItemQuantity}`), {
          precision: 14,
        })
          ? parseFloat(format(evaluate(`${parseFloat(item.lotAvailableQty)} + ${findItemQuantity}`), { precision: 14 }))
          : 0
        return { ...item, lotAvailableQty: newLotAvailableQty.toFixed(2) }
      } else {
        //no lot select
        return { ...item, lotAvailableQty: '' }
      }
    })
  }

  const setOverflowDataSource = () => {
    const iniTableDataCopy = _.cloneDeep(iniTableData)
    //rest LotAvailableQty
    const newData: TableItem[] = resetLotAvailable(iniTableDataCopy.reverse(), false)

    if (currentRecord.quantity === 0) {
      return newData
    }
    let basQty = baseQuantity
    let basPicked = basePicked

    const data = newData.map((item: TableItem, index: number) => {
      const currentItem = item

      if (basQty === 0) {
        return { ...item, quantity: '', picked: '' }
      }

      const itemLotAvailableQty =
        selectUOM == currentRecord.UOM
          ? format(evaluate(`${parseFloat(item.lotAvailableQty)} * ${UOMRatio}`), { precision: 14 })
          : item.lotAvailableQty

      if (basQty >= parseFloat(itemLotAvailableQty) && isNumber(itemLotAvailableQty)) {
        const _itemLotAvailableQty = wholeNumberState
          ? Math.floor(parseFloat(itemLotAvailableQty)) == 0
            ? '0'
            : Math.floor(parseFloat(itemLotAvailableQty)).toString()
          : itemLotAvailableQty
        // picked
        const pickedNum = basPicked
        //set quantity and picked
        currentItem.quantity = _itemLotAvailableQty
        currentItem.picked =
          basPicked >= parseFloat(_itemLotAvailableQty)
            ? _itemLotAvailableQty
            : basPicked > 0
            ? pickedNum.toString()
            : ''
        //reset quantity and picked
        basQty = format(evaluate(`${basQty} - ${_itemLotAvailableQty}`), { precision: 14 })
          ? parseFloat(format(evaluate(`${basQty} - ${_itemLotAvailableQty}`), { precision: 14 }))
          : 0
        basPicked = format(evaluate(`${basPicked} - ${_itemLotAvailableQty}`), { precision: 14 })
          ? parseFloat(format(evaluate(`${basPicked} - ${_itemLotAvailableQty}`), { precision: 14 }))
          : 0
        return currentItem
      } else if (basQty < parseFloat(itemLotAvailableQty) && isNumber(itemLotAvailableQty) && basQty > 0) {
        //set quantity and picked
        currentItem.quantity = basQty.toString()
        currentItem.picked = basPicked > 0 ? basPicked.toString() : ''
        //reset quantity and picked
        basQty = 0
        basPicked = format(evaluate(`${basPicked} + ${itemLotAvailableQty}`), { precision: 14 })
          ? parseFloat(format(evaluate(`${basPicked} + ${itemLotAvailableQty}`), { precision: 14 }))
          : 0
        return currentItem
      } else {
        currentItem.quantity = basQty > 0 ? basQty.toString() : ''
        currentItem.picked = basPicked > 0 ? basPicked.toString() : ''
        return currentItem
      }
    })
    const reverseArr = data.reverse()
    reverseArr.map((item) => {
      if (item.quantity == '0') {
        item.quantity = ''
      }
      if (item.picked == '0') {
        item.picked = ''
      }
      return item
    })
    setDisplayTableData(reverseArr)
  }

  const setNoOverflowDataSource = () => {
    const basQty = baseQuantity
    const basPicked = basePicked

    const iniTableDataCopy = _.cloneDeep(iniTableData)
    const newData: TableItem[] = iniTableDataCopy.reverse()

    // check if all items have been added to first item
    let wholeNumberExist = false

    const data = newData.map((item: TableItem, index: number) => {
      const findItem = currentItems.find((i) => i.lotId === item.lotId)

      const currentItem = item

      if (!wholeNumberState) {
        currentItem.quantity = index == 0 ? (basQty === 0 ? '' : basQty.toString()) : ''
        currentItem.picked = index == 0 ? (basePicked === 0 ? '' : basPicked.toString()) : ''
        if (findItem) {
          const findItemQuantity = ratioQtyToInventoryQty(currentRecord.UOM, findItem.quantity || 0, currentRecord, 12)
          currentItem.lotAvailableQty = format(evaluate(`${currentItem.lotAvailableQty} + ${findItemQuantity}`), {
            precision: 14,
          })
        }

        return currentItem
      } else {
        const lotAvailableQty = findItem
          ? format(
              evaluate(
                `${currentItem.lotAvailableQty} + ${ratioQtyToInventoryQty(
                  currentRecord.UOM,
                  findItem.quantity || 0,
                  currentRecord,
                  12,
                )}`,
              ),
              {
                precision: 14,
              },
            )
          : currentItem.lotAvailableQty
        if ((parseFloat(lotAvailableQty) >= 1 || index == newData.length - 1) && wholeNumberExist == false) {
          currentItem.quantity = basQty.toString()
          currentItem.picked = basePicked.toString()
          currentItem.lotAvailableQty = lotAvailableQty
          wholeNumberExist = true
        } else {
          currentItem.quantity = ''
          currentItem.picked = ''
          currentItem.lotAvailableQty = lotAvailableQty
        }
        return currentItem
      }
    })

    const reverseArr = data.reverse()
    reverseArr.map((item) => {
      if (item.quantity == '0') {
        item.quantity = ''
      }
      if (item.picked == '0') {
        item.picked = ''
      }
      return item
    })
    setDisplayTableData(reverseArr)
  }

  const setRadioOverflowDataSource = (chooseRadio: number) => {
    const basQty = baseQuantity
    const basPicked = basePicked

    const iniTableDataCopy = _.cloneDeep(iniTableData)
    const newData: TableItem[] = resetLotAvailable(iniTableDataCopy.reverse(), false)
    
    const data = newData.map((item: TableItem, index: number) => {
      const currentItem = item

      currentItem.quantity = index == chooseRadio ? (basQty === 0 ? '' : basQty.toString()) : ''
      currentItem.picked = index == chooseRadio ? (basePicked === 0 ? '' : basPicked.toString()) : ''

      return currentItem
    })

    const reverseArr = data.reverse()

    setDisplayTableData(reverseArr)
  }

  // copy from _draggable
  const getDefaultPicked = () => {
    return currentRecord.picked === null ||
      typeof currentRecord.picked == 'undefined' ||
      (currentRecord.picked == 0 && (currentRecord.status == 'PLACED' || currentRecord.status == 'NEW'))
      ? ''
      : currentRecord.picked
  }

  //keyboard
  const doubleClick = (id: string) => {
    setLotAssignmentMethod(3)
    setManuallyAssignState(true)
    setTimeout(() => {
      ;(document.getElementById(id) as HTMLElement).focus()
    }, 200)
  }

  const toNextRadio = () => {
    const ele = document.activeElement
    let e: any = null

    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      const eleId = ele ? (ele.getAttribute('id') ? ele.getAttribute('id') : '') : ''
      const num = eleId ? parseInt(eleId.toString().replace('lotRadio', '')) : -1
      const length = displayTableData.length
      e = document.getElementById(`lotRadio${num + 1 < length ? num + 1 : 0}`)
      setTimeout(() => {
        e.focus()
      }, 100)
    }
  }

  const toPreviousRadio = () => {
    const ele = document.activeElement
    let e: any = null

    if (manuallyAssignState == false && lotAssignmentMethod == 3) {
      const length = displayTableData.length
      const eleId = ele ? (ele.getAttribute('id') ? (ele.getAttribute('id') as string) : '') : ''
      const num = eleId !== '' ? parseInt(eleId.replace('lotRadio', '')) : length
      e = document.getElementById(`lotRadio${num == 0 ? length - 1 : num - 1}`)
      setTimeout(() => {
        e.focus()
      }, 100)
    }
  }

  // enableLotOverflow

  //column
  const columns: {
    title?: string
    dataIndex: string
    align?: 'left' | 'right' | 'center' | undefined
    width?: number
    className?: undefined | string
    render?: any
  }[] = [
    {
      title: '',
      dataIndex: '',
      align: 'left',
      width: 10,
      render: (value: string, record: TableItem, index: number) => {
        if (!manuallyAssignState && lotAssignmentMethod == 3) {
          return <Radio id={`lotRadio${index}`} value={index}></Radio>
        } else {
          return <></>
        }
      },
    },
    {
      title: 'ORDER QTY',
      dataIndex: 'quantity',
      align: 'left',
      width: 130,
      render: (value: string, record: TableItem, index: number) => {
        const lotQty = parseFloat(record.lotAvailableQty)
        const quantityWithUOM =
          selectUOM == currentRecord.UOM
            ? isNumber(record.quantity)
              ? parseFloat(record.quantity) / UOMRatio
              : 0
            : parseFloat(record.quantity)
        const className = parseFloat(quantityWithUOM.toFixed(2)) > lotQty ? 'warning lotInput' : 'lotInput'

        return (
          <div onDoubleClick={(e) => doubleClick(`quantity${index}`)}>
            <Input
              id={`quantity${index}`}
              style={{ textAlign: 'right' }}
              disabled={lotAssignmentMethod == 1 || manuallyAssignState == false}
              className={className}
              onChange={(e) => handleChange(index, e.target.value, InputType.QUANTITY)}
              value={toStringFixedTwo(value)}
            />
          </div>
        )
      },
    },
    {
      title: 'PICKED QTY',
      dataIndex: 'picked',
      align: 'left',
      width: 130,
      className: `${isDisablePickingStep && 'displayNone'}`,
      render: (value: string, record: TableItem, index: number) => {
        const lotQty = parseFloat(record.lotAvailableQty)
        const pickedWithUOM =
          selectUOM == currentRecord.UOM
            ? isNumber(record.picked)
              ? parseFloat(record.picked) / UOMRatio
              : 0
            : parseFloat(record.picked)

        const className = parseFloat(pickedWithUOM.toFixed(2)) > lotQty ? 'warning lotInput' : 'lotInput'
        return (
          <div onDoubleClick={(e) => doubleClick(`picked${index}`)}>
            <Input
              id={`picked${index}`}
              style={{ textAlign: 'right' }}
              disabled={lotAssignmentMethod == 1 || manuallyAssignState == false}
              className={className}
              onChange={(e) => handleChange(index, e.target.value, InputType.PICKED)}
              value={toStringFixedTwo(value)}
            />
          </div>
        )
      },
    },
    {
      dataIndex: 'tip',
      width: 20,
      render: (value: string, record: TableItem) => {
        if (!record.onHandQty) {
          if (!record.onHandQty && record.lotId !== null) {
            return (
              <Tooltip placement="top" title={'Lot has not been received yet.'}>
                <Icon type="warning" className="red" />
              </Tooltip>
            )
          }
        }
      },
    },
    {
      title: 'DELIVERY DATE',
      dataIndex: 'deliveryDate',
      align: 'left',
      width: 150,
      render: (value: string, record: TableItem) => {
        if (record.lotId !== null) {
          return moment.utc(value).format('MM/DD/YYYY')
        } else {
          return `no lot selected`
        }
      },
    },
    {
      title: 'LOT #',
      dataIndex: 'lotId',
      align: 'left',
    },
    {
      title: 'VENDOR REF #',
      dataIndex: 'vendorRefNo',
      align: 'left',
    },
    {
      title: 'AVAILABLE TO SELL',
      align: 'right',
      dataIndex: 'lotAvailableQty',
      render: (value: string, record: TableItem) => {
        if (record.lotId !== null) {
          return `${toFixedString(value)} ${record.inventoryUOM}`
        }
      },
    },
    {
      title: 'UNITS ON HAND',
      align: 'right',
      dataIndex: 'onHandQty',
      render: (value: string, record: TableItem) => {
        if (record.lotId !== null) {
          if (!value) {
            return <span className="red">{`${value} ${record.inventoryUOM}`}</span>
          }
          return `${toFixedString(value)} ${record.inventoryUOM}`
        }
      },
    },
    {
      title: 'COST',
      align: 'right',
      dataIndex: 'cost',
      render: (value: string, record: TableItem) => {
        if (record.lotId !== null) {
          return `$${formatNumber(parseFloat(value) + record.perAllocateCost, 2)} ${record.inventoryUOM}`
        } else {
          return ``
        }
      },
    },
    {
      title: 'VENDOR',
      align: 'left',
      dataIndex: 'companyName',
    },
    {
      title: 'BRAND',
      align: 'left',
      dataIndex: 'brand',
    },
    {
      title: 'ORIGIN',
      align: 'left',
      dataIndex: 'origin',
    },
    {
      title: 'NOTES FROM PO',
      align: 'left',
      dataIndex: 'note',
    },
  ]

  const handleChange = (index: number, value: string, type: InputType) => {
    const valueType = type == InputType.QUANTITY ? 'quantity' : 'picked'
    if (isNumber(value)) {
      const newData = _.cloneDeep(displayTableData)
      newData[index][valueType] = value
      setDisplayTableData(newData)
    }
  }

  //total
  const TotalValue = (type: InputType) => {
    let totalNum = 0
    displayTableData.forEach((item) => {
      const value = type == InputType.QUANTITY ? 'quantity' : 'picked'
      if (item[value] && isNumber(item[value])) {
        totalNum += parseFloat(item[value])
      }
    })
    return totalNum.toFixed(2)
  }

  //save check
  const handleSaveVerify = () => {
    const baseQuantityWithUOM = selectUOM == currentRecord.UOM ? baseQuantity : baseQuantity / UOMRatio
    const value = format(
      evaluate(`${parseFloat(TotalValue(InputType.QUANTITY))} - ${parseFloat(baseQuantityWithUOM.toFixed(2))}`),
      { precision: 14 },
    )
      ? parseFloat(
          format(
            evaluate(`${parseFloat(TotalValue(InputType.QUANTITY))} - ${parseFloat(baseQuantityWithUOM.toFixed(2))}`),
            { precision: 14 },
          ),
        )
      : 0
    if (selectUOM !== currentRecord.UOM) {
      return setConfirmUpdateUOMModalVisible(true)
    } else if (value >= 0.01) {
      return setConfirmUpdateQtyModalVisible(true)
    }
    if (!isDisablePickingStep) {
      const basePickedWithUOM = basePicked
      if (TotalValue(InputType.PICKED) !== basePickedWithUOM.toFixed(2)) {
        return setConfirmUpdatePickedModalVisible(true)
      }
    }

    handleSaveChange()
  }

  const handleSaveChange = () => {
    setSaveButtonLoading(true)
    setConfirmUpdatePickedModalVisible(false)

    const findItem = currentItems.find((item) => {
      if (item.lotId == undefined) {
        return item
      }
    })

    const _dataSource = _.cloneDeep(displayTableData)
    const postData: any[] = []
    //format data(add and update ) and set displayOrder
    _dataSource.reverse().forEach((item, index) => {
      const quantityNum =
        item.quantity != '' && isNumber(item.quantity)
          ? selectUOM == currentRecord.UOM
            ? parseFloat(item.quantity)
            : format(evaluate(`${parseFloat(item.quantity)} * ${UOMRatio}`), { precision: 14 })
          : 0
      const pickedNum =
        item.picked != '' && isNumber(item.picked)
          ? selectUOM == currentRecord.UOM
            ? parseFloat(item.picked)
            : format(evaluate(`${parseFloat(item.picked)} * ${UOMRatio}`), { precision: 14 })
          : 0

      if (item.lotId != null) {
        //add lot item
        if (quantityNum > 0 || pickedNum > 0) {
          postData.push({
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            lotId: item.lotId,
            quantity: quantityNum,
            picked: isDisablePickingStep ? quantityNum : pickedNum,
            wholesaleOrderItemId: currentItems.find((v) => v.lotId === item.lotId)
              ? currentItems.find((v) => v.lotId === item.lotId).wholesaleOrderItemId
              : undefined,
          })
        }
      } else {
        //add no lot select
        const pickedDisableNum = isDisablePickingStep ? item.quantity : item.picked

        const pickedNoLotNum =
          pickedDisableNum != '' && isNumber(pickedDisableNum)
            ? selectUOM == currentRecord.UOM
              ? parseFloat(pickedDisableNum)
              : format(evaluate(`${parseFloat(pickedDisableNum)} * ${UOMRatio}`), { precision: 14 })
            : 0

        if (quantityNum > 0 || pickedNoLotNum > 0) {
          const notLotSelectObj = {
            displayOrder: currentRecord.displayOrderProduct + postData.length / 100,
            quantity: quantityNum,
            picked: pickedNoLotNum,
          }
          if (findItem) {
            notLotSelectObj['wholesaleOrderItemId'] = findItem.wholesaleOrderItemId
          }
          postData.push(notLotSelectObj)
        }
      }
    })
    //format data(delete)
    _dataSource.forEach((v) => {
      const findItem = currentItems.find((c) => c.lotId === v.lotId)
      const wholesaleOrderItemIds = postData.map((item) => item.wholesaleOrderItemId)
      const index = wholesaleOrderItemIds.indexOf(findItem ? findItem.wholesaleOrderItemId : '')

      if (v.lotId !== null && findItem && index == -1) {
        if (isDisablePickingStep) {
          if (!Number(v.quantity)) {
            postData.push({ wholesaleOrderItemId: findItem.wholesaleOrderItemId, isDeleted: true })
          }
        } else {
          if (!Number(v.quantity) && !Number(v.picked)) {
            postData.push({ wholesaleOrderItemId: findItem.wholesaleOrderItemId, isDeleted: true })
          }
        }
      }

      if (v.lotId == null) {
        const noLotItem = currentItems.find((item) => {
          if (item.lotId == undefined) {
            return item
          }
        })
        const nolotIndex = wholesaleOrderItemIds.indexOf(noLotItem ? noLotItem.wholesaleOrderItemId : '')
        if (isDisablePickingStep) {
          if (!Number(v.quantity) && nolotIndex == -1) {
            postData.push({ wholesaleOrderItemId: noLotItem ? noLotItem.wholesaleOrderItemId : null, isDeleted: true })
          }
        } else {
          if (!Number(v.quantity) && !Number(v.picked) && nolotIndex == -1) {
            postData.push({ wholesaleOrderItemId: noLotItem ? noLotItem.wholesaleOrderItemId : null, isDeleted: true })
          }
        }
      }
    })

    //get new post array according the wholesaleOrderItemId
    const defaultExistOrderItemId = currentRecord['wholesaleOrderItemId']
    let newPostData: any[] = []

    if (
      (postData[0]['isDeleted'] && postData[0]['isDeleted'] == true) ||
      postData[0]['wholesaleOrderItemId'] != defaultExistOrderItemId
    ) {
      const whilesaleOrderItemIdArr = postData.map((item) => {
        if (item['wholesaleOrderItemId']) {
          return item.wholesaleOrderItemId
        } else {
          return ''
        }
      })

      const defaultIdIndex = postData.findIndex((item) => item['wholesaleOrderItemId'] == defaultExistOrderItemId)

      postData.forEach((item, index) => {
        const obj = item
        if (index == 0) {
          const addObj = {
            ...obj,
            wholesaleOrderItemId: defaultExistOrderItemId,
          }
          newPostData.push(addObj)
        } else if (index <= defaultIdIndex) {
          if (whilesaleOrderItemIdArr[index - 1] != '') {
            const addObj = {
              ...obj,
              wholesaleOrderItemId: whilesaleOrderItemIdArr[index - 1],
            }
            newPostData.push(addObj)
          } else {
            const addObj = {
              ...obj,
              displayOrder: obj.displayOrder,
              quantity: obj.quantity,
              picked: obj.picked,
              wholesaleOrderItemId: undefined,
            }
            newPostData.push(addObj)
          }
        } else {
          newPostData.push(obj)
        }
      })
    } else {
      newPostData = postData
    }

    //delete useless item
    const _newPostData: any[] = []
    newPostData.forEach((item) => {
      if (item['isDeleted'] == true && item['wholesaleOrderItemId'] == undefined) {
        // console.log(item)
      } else {
        _newPostData.push(item)
      }
    })

    const data = {
      lotAssignmentMethod,
      enableLotOverflow,
      existOrderItemId: defaultExistOrderItemId,
      manuallyAssignState: manuallyAssignState,
      orderItemList: _newPostData,
    }

    return sendRequest({ orderId: currentOrder.wholesaleOrderId, data })
  }

  const handleRefresh = () => {
    setCounter(1)
    setRefresh(refresh + 1)
  }

  const toggleRepackModal = (isReload?: boolean) => {
    if (isReload === true) {
      setRepacking(true)
      handleRefresh()
    }
    setRepackModalVisible(!repackModalVisible)
  }

  const sendRequest = ({ orderId, data }: any) => {
    OrderService.instance.saveOrderLots({ orderId, data }).subscribe({
      next(res: any) {
        if (res.statusCode === 'CONFLICT') return setTimeout(() => sendRequest({ orderId, data }), 500)
        getOrderItemsById(currentOrder.wholesaleOrderId)
        setSaveButtonLoading(false)
        onCancel()
      },
    })
  }

  return (
    <Main>
      <Header>
        <a
          style={{ fontSize: 16, fontWeight: 'bold' }}
          href={`#/product/${currentRecord.itemId}/activity`}
          target="_blank"
        >
          <Flex className="v-center">
            {currentRecord.variety}
            <IconSvg
              style={{ marginLeft: 6 }}
              viewBox="0 0 512 512"
              width="16"
              height="16"
              type="external-link-alt-solid"
            />
          </Flex>
        </a>

        <div className="refresh">
          <div>
            <Button onClick={handleRefresh}>Refresh</Button>
          </div>
          {counter > 0 && <p>Refreshed {counter} second ago</p>}
        </div>
      </Header>
      <div className="flex">
        <p className="noBold">
          Order qty: {currentRecord.quantity} {currentRecord.UOM} &nbsp;
          {currentRecord.UOM !== currentRecord.inventoryUOM &&
            `(${ratioQtyToInventoryQty(currentRecord.UOM, currentRecord.quantity || 0, currentRecord, 12).toFixed(2)} ${
              currentRecord.inventoryUOM
            })`}
        </p>
        {!isDisablePickingStep && (
          <p style={{ marginLeft: 20 }} className="noBold">
            Picked qty: {getDefaultPicked()} {currentRecord.UOM} &nbsp;
            {currentRecord.UOM !== currentRecord.inventoryUOM &&
              `(${ratioQtyToInventoryQty(currentRecord.UOM, currentRecord.picked || 0, currentRecord, 12).toFixed(2)} ${
                currentRecord.inventoryUOM
              })`}
          </p>
        )}
      </div>
      {loading ? (
        <Spin />
      ) : (
        <>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 20 }}>
            <Radio.Group onChange={(e) => setLotAssignmentMethod(e.target.value)} value={lotAssignmentMethod}>
              <ThemeRadio
                style={{ display: 'block', height: '30px', lineHeight: '30px' }}
                value={1}
                className="noBold"
                id="headerRadio1"
              >
                Auto-assign lot(s) based on FIFO
                {lotAssignmentMethod === 1 ? (
                  <>
                    <ThemeCheckbox
                      style={{ marginLeft: 10 }}
                      onChange={(e: any) => setEnableLotOverflow(e.target.checked)}
                      checked={enableLotOverflow}
                    >
                      [BETA] Enable “overflow” to multiple lots{' '}
                    </ThemeCheckbox>
                    {/*
                    <ThemeCheckbox
                      style={{ marginLeft: 10 }}
                      onChange={(e: any) => {
                        setWholeNumberState(e.target.checked)
                      }}
                      checked={wholeNumberState}
                    >
                      Auto-assign to lots using whole number values only
                    </ThemeCheckbox>
                    */}
                  </>
                ) : null}
              </ThemeRadio>
              <ThemeRadio style={{ display: 'block', height: '30px', lineHeight: '30px' }} value={3} className="noBold">
                Manually assign to lot(s){' '}
                <Switch checked={manuallyAssignState} onChange={(e) => setManuallyAssignState(e)} />
                {manuallyAssignState ? `  edit quantities` : `  selection mode`}
              </ThemeRadio>
            </Radio.Group>
            <Button onClick={toggleRepackModal}>Enter repack</Button>
            <EnterRepackModal
              sellerSetting={sellerSetting}
              selectedItem={currentRecord}
              visible={repackModalVisible}
              onClose={toggleRepackModal}
            />
          </div>
          <div style={{ marginBottom: '8px' }}>
            <h6 style={{ fontSize: '15px', fontWeight: 'bold' }}>Unit of measure</h6>
            <Select value={selectUOM} style={{ width: 120 }} onChange={(e: any) => setSelectUOM(e)}>
              {options.map((option, index) => {
                return (
                  <Option key={index} value={option}>
                    {option}
                  </Option>
                )
              })}
            </Select>
          </div>
        </>
      )}
      <Radio.Group
        onChange={(e) => {
          setRadioSelect(e.target.value)
        }}
        value={radioSelect}
      >
        <ThemeTable
          loading={loading}
          dataSource={displayTableData}
          columns={columns}
          rowKey="purchaseOrderItemId"
          pagination={false}
        />
      </Radio.Group>
      <Count style={{ marginLeft: '20px;' }}>
        <span style={{ marginLeft: !manuallyAssignState && lotAssignmentMethod == 3 ? '40px' : '20px' }}>
          {TotalValue(InputType.QUANTITY)}
        </span>
        {!isDisablePickingStep && (
          <span style={{ marginLeft: manuallyAssignState && lotAssignmentMethod != 3 ? '20px' : '20px' }}>
            {TotalValue(InputType.PICKED)}
          </span>
        )}
      </Count>
      <ThemeButton loading={saveButtonLoading} onClick={() => handleSaveVerify()}>
        Save changes
      </ThemeButton>
      <span style={{ cursor: 'pointer', marginLeft: '20px' }} onClick={() => onCancel()}>
        Cancel
      </span>
      {confirmUpdatePickedModalVisible && (
        <ConfirmUpdatePickedModal
          onCancel={() => setConfirmUpdatePickedModalVisible(false)}
          onOk={() => handleSaveChange()}
          title={'Update picked qty?'}
          updateButton={'Update Picked Qty'}
          context={`Would you like to update the picked quantity value in the sales order to match the quantity you have allocated?`}
        />
      )}
      {confirmUpdateQtyModalVisible && (
        <ConfirmUpdatePickedModal
          onCancel={() => setConfirmUpdateQtyModalVisible(false)}
          onOk={() => handleSaveChange()}
          title={'Update order item?'}
          updateButton={'Save updates'}
          context={`You have made modifications to this item's quantity and/or UOM. Would you like to save the updates to your order?`}
        />
      )}
      {confirmUpdateUOMModalVisible && (
        <ConfirmUpdatePickedModal
          onCancel={() => setConfirmUpdateUOMModalVisible(false)}
          onOk={() => handleSaveChange()}
          title={'Update order item?'}
          updateButton={'Save updates'}
          context={`You have made modifications to this item's quantity and/or UOM. Would you like to save the updates to your order?`}
        />
      )}
    </Main>
  )
}

const ConfirmUpdatePickedModal: FC<{
  onCancel: () => void
  onOk: () => void
  title: string
  updateButton: string
  context: string
}> = ({ onCancel, onOk, title, updateButton, context }) => {
  return (
    <WrapModal
      visible
      title={title}
      onCancel={onCancel}
      className="confirmModal"
      footer={[
        <ThemeButton key={1} onClick={onOk}>
          {updateButton}
        </ThemeButton>,
        <span key={2} style={{ marginLeft: 10 }} onClick={onCancel}>
          Cancel
        </span>,
      ]}
    >
      <p>{context}</p>
    </WrapModal>
  )
}

export default LotSectionV2

const Main = styled.div`
  .ant-table-thead > tr > th {
    padding: 0 10px;
  }
  .warning {
    border: 2px solid #eb5757;
    background-color: #f6d1d1;
  }
  .warning:hover {
    border: 2px solid #eb5757 !important;
  }
  .noBold {
    font-weight: normal;
  }
  .flex {
    display: flex;
    align-items: center;
  }
  .displayNone {
    display: none;
  }
  .red {
    color: rgb(189, 39, 32);
  }
`

const WrapModal = styled(ThemeModal)`
  .ant-modal-footer {
    text-align: left;
  }
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  .refresh {
    text-align: right;
  }
`
const Count = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  display: flex;
  span {
    width: 112px;
    text-align: right;
  }
  .picked {
    margin-left: 18px;
  }
`
const WarningText = styled.p`
  color: #ff0000;
`
