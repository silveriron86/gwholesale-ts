import React from 'react'
import { Icon, Popconfirm } from 'antd'
import jQuery from 'jquery'
import { OneOffItemTableWrapper, PureWrapper as Wrapper } from '../../../accounts/addresses/addresses.style'
import { ml0, textCenter, ThemeIcon, ThemeOutlineButton } from '~/modules/customers/customers.style'
import { formatNumber } from '~/common/utils'
import { cloneDeep } from 'lodash'
import EditableTable from '~/components/Table/editable-table'
import { OrderDetail, OrderItem } from '~/schema'
import TypeEditWrapper from '~/modules/settings/tabs/Product/TypeEditWrapper'
import _ from 'lodash'
import ExtraChargeTable from '../../../nav-sales/order-detail/components/extra-charges-table.component'
import { connect } from 'react-redux'

interface OrderTableProps {
  editable: boolean
  currentOrder: OrderDetail
  orderItems: OrderItem[]
  handleSave: Function
  handleRemove: Function
  handleUpdateAllItemStatus: Function
  addItemButtonClicked: boolean
  isExtraCharge: boolean
  openAddOffItem: (name?: string) => void
  companyProductTypes: any[]
  sellerSetting: any
}

export class CartOrderTable extends React.PureComponent<OrderTableProps> {
  state: any
  constructor(props: OrderTableProps) {
    super(props)
    this.state = {
      dataSource: cloneDeep(props.orderItems),
      orderItemId: -1,
      selectedItem: null,
      itemId: -1,
      pageSize: 30,
      currentPage: 1,
      hoverIndex: -1,
    }
  }

  componentWillReceiveProps(nextProps: OrderTableProps) {
    if (this.props.orderItems !== nextProps.orderItems) {
      this.setState({
        dataSource: cloneDeep(nextProps.orderItems),
      })

      //After add item successfully. Need to set the number of pages to the last
      if (nextProps.orderItems.length - this.props.orderItems.length == 1) {
        this.setState({
          currentPage: Math.ceil(nextProps.orderItems.length / this.state.pageSize),
        })
      }
    }
  }

  componentDidUpdate(prevProps: OrderTableProps) {
    //select first tab-able element when new item is added
    if (
      this.props.addItemButtonClicked &&
      prevProps.orderItems.length >= 0 &&
      prevProps.orderItems.length == this.props.orderItems.length - 1
    ) {
      const rows = jQuery('.one-off-table .ant-table-row')
      const allEls = jQuery('.one-off-table .ant-table-row .tab-able')
      if (rows.length > 0) {
        setTimeout(() => {
          const editableEls = jQuery(rows[rows.length - 1]).find('.tab-able')
          if (editableEls.length > 0) {
            setTimeout(() => {
              jQuery(editableEls[0]).trigger('click')
              const index = allEls.index(editableEls[0])
              window.localStorage.setItem('CLICKED-INDEX', `${index}`)
            }, 50)
          }
        }, 200)
      }
    }
  }

  handleSave = (row: OrderItem, field?: string) => {
    this._saveOffItem(row, field)
  }

  _saveOffItem(row: OrderItem, field?: string) {
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })

    this.setState({ dataSource: newData })
    row.quantity = row.quantity ? row.quantity : 0
    row.picked = row.picked ? row.picked : 0
    row.uom = row.UOM

    // console.log(row)
    this.props.handleSave(row)
  }

  upsetOffItem = (index: number) => {
    this.props.handleSave(this.state.dataSource[index])
  }

  onDeleteRow = (id: number) => {
    const dataSource = [...this.state.dataSource]
    this.setState({ dataSource: dataSource.filter((item) => item.wholesaleOrderItemId !== id) })
    this.props.handleRemove(id)
  }

  changePage = (page: any, pageSize: any) => {
    this.setState({
      currentPage: page,
    })
  }

  render() {
    const { editable, openAddOffItem, sellerSetting } = this.props
    const { dataSource, pageSize, currentPage } = this.state

    const columns: Array<any> = [
      {
        title: 'One Off Item',
        children: [
          {
            title: '#',
            dataIndex: 'wholesaleOrderItemId',
            key: 'wholesaleOrderItemId',
            width: 60,
            align: 'center',
            render: (id: number, record: OrderItem, index: number) => {
              return index + 1
            },
          },
          {
            title: 'UNIT',
            dataIndex: 'quantity',
            align: 'center',
            key: 'unit',
            editable,
            edit_width: 50,
            type: 'number',
            render: (data: number, record: OrderItem) => {
              return <div style={{ width: '100%' }}>{data}</div>
            },
          },
          {
            title: 'NAME',
            dataIndex: 'itemName',
            key: 'itemName',
            align: 'center',
            edit_width: 100,
            editable,
            render: (itemName: number, record: OrderItem) => {
              return <div style={{ width: '100%' }}>{itemName}</div>
            },
          },
          {
            title: 'PRICE',
            dataIndex: 'price',
            key: 'price',
            align: 'center',
            editable,
            edit_width: 80,
            render: (data: number, record: OrderItem) => (
              <div style={{ width: '100%' }}>{formatNumber(data ? data : 0, 2)}</div>
            ),
          },
          {
            title: 'PRICING UOM',
            dataIndex: 'uom', //beacuse off item uom just need tag input to udpate uom,don't use UOM
            key: 'uom',
            editable,
            align: 'center',
            width: 150,
            render: (uom: number, record: OrderItem) => {
              return <div style={{ width: '100%' }}>{uom}</div>
            },
          },
        ],
      },

      {
        title: '',
        children: [
          {
            title: 'TOTAL PRICE',
            dataIndex: 'item_total',
            align: 'center',
            key: 'item_total',
            render: (data: number, record: any) => {
              const extended = Number(record.price ? record.price : 0) + Number(record.freight ? record.freight : 0)
              let total = 0
              total = extended * record.quantity
              const prefix = total >= 0 ? '' : '-'
              return (
                <div style={textCenter}>
                  {prefix}${formatNumber(Math.abs(total), 2)}
                </div>
              )
            },
          },

          {
            title: '',
            key: 'delete',
            render: (_text: any, record: any, index: any) => {
              return (
                <div>
                  <Popconfirm
                    title="Permanently delete this one related record?"
                    okText="Delete"
                    onConfirm={this.onDeleteRow.bind(this, record.wholesaleOrderItemId)}
                  >
                    {/* <FlexCenter style={{ cursor: 'pointer' }}> */}
                    <ThemeIcon type="close" style={{ marginLeft: '10px' }} />
                    {/* </FlexCenter> */}
                  </Popconfirm>
                </div>
              )
            },
          },
        ],
      },
    ]

    if (editable) {
      columns[0].children.push({
        title: '',
        dataIndex: '',
        key: 'notes',
      })
    }

    const allProps = { ...this.props }
    allProps['TypeEditWrapper'] = TypeEditWrapper

    const footer = editable ? (
      <div>
        <div style={{ width: '25%', borderRight: '1px solid #D8DBDB', textAlign: 'center', padding: 12 }}>
          <ThemeOutlineButton style={{ border: 'none' }} onClick={this.props.openAddOffItem}>
            <Icon type="plus-circle" />
            Add Item
          </ThemeOutlineButton>
        </div>
      </div>
    ) : null

    const onRow = (record: any, index: number) => {
      return {
        onMouseEnter: () => {
          this.setState({ hoverIndex: index })
        }, // 鼠标移入行
        onMouseLeave: () => {
          this.setState({ hoverIndex: -1 })
        },
      }
    }

    return (
      <Wrapper style={ml0}>
        <Wrapper style={ml0}>
          <OneOffItemTableWrapper>
            {allProps.isExtraCharge ? (
              <ExtraChargeTable
                dataSource={dataSource}
                editable={editable}
                handleSave={this.handleSave}
                addOne={openAddOffItem}
                deleteRow={this.onDeleteRow}
                pagination={{ pageSize: pageSize, current: currentPage, onChange: this.changePage, defaultCurrent: 1 }}
                sellerSetting={sellerSetting}
              />
            ) : (
              <EditableTable
                className={`nested-head min-height-placeholder ${allProps.isExtraCharge ? 'extra-charge' : ''}`}
                columns={columns}
                dataSource={dataSource}
                handleSave={this.handleSave}
                rowKey="wholesaleOrderItemId"
                onRow={onRow}
                pagination={{ pageSize: pageSize, current: currentPage, onChange: this.changePage, defaultCurrent: 1 }}
                allProps={allProps}
                footer={() => footer}
              />
            )}
          </OneOffItemTableWrapper>
        </Wrapper>
      </Wrapper>
    )
  }
}

export default connect(({ orders }) => ({ sellerSetting: orders.sellerSetting }))(CartOrderTable)
