import React from 'react'
import { Icon } from 'antd'
import { PureWrapper as Wrapper, WrapperTitle } from '../../../accounts/addresses/addresses.style'
import { Order } from '../../../customers.model'
import { ml0, ThemeIconButton } from '~/modules/customers/customers.style'
import { formatNumber } from '~/common/utils'
import { cloneDeep } from 'lodash'
import EditableTable from '~/components/Table/editable-table'

interface OrderTableProps {
  editable: boolean
}

const items: Array<Order> = [
  {
    id: 1,
    unit: 2,
    uom: 'TEST1',
    product_name: 'Carrot Baby Peeled Stick 51b',
    lot: '3',
    quoted_price: 4,
    notes: 'testing1',
  },
  {
    id: 2,
    unit: 3,
    uom: 'TEST2',
    product_name: 'Romanesco 12 Count',
    lot: '4',
    quoted_price: 5,
    notes: 'testing2',
  },
]

export class OrderTable extends React.PureComponent<OrderTableProps> {
  state = {
    initItems: cloneDeep(items),
  }

  handleSave = (row: Order) => {
    const newData = [...items]
    const index = newData.findIndex((item) => row.id === item.id)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
  }

  render() {
    const { editable } = this.props
    const { initItems } = this.state
    const listItems: Order[] = cloneDeep(initItems)

    const columns: Array<any> = [
      {
        title: '#',
        dataIndex: 'id',
        editable: true,
        edit_width: 50,
      },
      {
        title: 'UNIT',
        dataIndex: 'unit',
        editable: true,
        edit_width: 50,
      },
      {
        title: 'UOM',
        dataIndex: 'uom',
        editable: true,
        edit_width: 80,
      },
      {
        title: 'PRODUCT NAME',
        dataIndex: 'product_name',
        editable: true,
        type: 'search',
      },
      {
        title: 'LOT',
        dataIndex: 'lot',
        editable: true,
        edit_width: 50,
        render: (data: any) =>
          editable ? (
            <ThemeIconButton type="link">
              <Icon type="edit" />
            </ThemeIconButton>
          ) : (
            data
          ),
      },
      {
        title: 'QUOTED',
        dataIndex: 'quoted_price',
        editable: true,
        edit_width: 80,
        render: (data: number) => (
          <>
            {editable && (
              <ThemeIconButton type="link">
                <Icon type="edit" />
              </ThemeIconButton>
            )}
            ${formatNumber(data, 2)}
            {editable ? (
              <ThemeIconButton type="link">
                <Icon type="bar-chart" style={{ fontSize: 16 }} />
              </ThemeIconButton>
            ) : (
              <ThemeIconButton type="link">
                <Icon type="info-circle" />
              </ThemeIconButton>
            )}
          </>
        ),
      },
    ]

    if (editable) {
      const col = {
        title: 'NOTES',
        dataIndex: 'notes',
        editable: true,
        render: () => (
          <ThemeIconButton type="link">
            <Icon type="file-text" />
          </ThemeIconButton>
        ),
      }
      columns.push(col)
    }

    return (
      <Wrapper style={ml0}>
        <Wrapper style={ml0}>
          <WrapperTitle>Sales Order</WrapperTitle>
          <EditableTable columns={columns} dataSource={listItems} handleSave={this.handleSave} />
        </Wrapper>
      </Wrapper>
    )
  }
}

export default OrderTable
