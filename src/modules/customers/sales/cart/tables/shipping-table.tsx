import React from 'react'
import { Icon } from 'antd'
import { PureWrapper as Wrapper, WrapperTitle } from '../../../accounts/addresses/addresses.style'
import { Shipping } from '../../../customers.model'
import {
  ThemeButton,
  ThemeTable,
  ml0,
  textCenter,
  ThemeIconButton,
  ThemeModal,
  ThemeCheckbox,
} from '~/modules/customers/customers.style'
import { cloneDeep } from 'lodash'
import { formatNumber } from '~/common/utils'

interface ShippingTableProps {
  items: Array<Shipping>
}

export class ShippingTable extends React.PureComponent<ShippingTableProps> {
  state = {
    visibleAlert: false,
    initItems: cloneDeep(this.props.items),
    selectedRowKeys: [],
  }

  onSelectChange = (selectedRowKeys: any) => {
    this.setState({ selectedRowKeys })
  }

  onDeleteRow = (item: any) => {
    this.setState({
      visibleAlert: true,
    })
  }

  handleDelete = () => {
    this.setState({
      visibleAlert: false,
    })
  }

  handleCancel = () => {
    this.setState({
      visibleAlert: false,
    })
  }

  render() {
    const { initItems, visibleAlert } = this.state
    const listItems: Shipping[] = cloneDeep(initItems)

    const columns = [
      {
        title: 'EXTENDED',
        dataIndex: 'extended',
        render: (data: number) => <div style={textCenter}>${formatNumber(data, 2)}</div>,
      },
      {
        title: 'ITEM TOTAL',
        dataIndex: 'item_total',
        render: (data: number) => <div style={textCenter}>${formatNumber(data, 2)}</div>,
      },
      {
        title: (
          <>
            PICKED
            <ThemeCheckbox style={{ margin: '-2px 0 0 2px' }} />
          </>
        ),
        dataIndex: 'shipped',
        render: () => (
          <ThemeButton type="primary" shape="circle">
            <Icon type="bulb" theme="filled" />
          </ThemeButton>
        ),
      },
      {
        title: '',
        render: (_text: any, item: any, _index: any) => (
          <ThemeIconButton type="link" onClick={this.onDeleteRow.bind(this, item)}>
            <Icon type="close" />
          </ThemeIconButton>
        ),
      },
    ]

    // const rowSelection = {
    //   selectedRowKeys,
    //   onChange: this.onSelectChange
    // }

    return (
      <Wrapper style={ml0}>
        <WrapperTitle>Shpping</WrapperTitle>
        <ThemeModal
          visible={visibleAlert}
          title="GrubMarket Confirm"
          okText="Delete"
          onCancel={this.handleCancel}
          onOk={this.handleDelete}
        >
          <p>Permanently delete this one related record?</p>
        </ThemeModal>
        <ThemeTable
          // rowSelection={rowSelection}
          pagination={{ hideOnSinglePage: true, pageSize: 12 }}
          columns={columns}
          dataSource={listItems}
        />
      </Wrapper>
    )
  }
}

export default ShippingTable
