import React from 'react'
import { withTheme } from 'emotion-theming'
import { SalesContainer } from '../_style'
import { CustomerDetailsWrapper as DetailsWrapper, CustomerDetailsTitle as DetailsTitle } from '../../customers.style'
import { Row, Col } from 'antd'
import EditForm from './edit-form'
import { OrderSummary } from './order-summary'
import Changes from './changes'
import { MainAddress, OrderDetail, OrderItem, WholesaleRoute } from '~/schema'

interface CheckoutContainerProps {
  order: OrderDetail
  orderItems: OrderItem[]
  customers: any[] | null
  onSave: Function
  catchWeightValues: any
  getWeightsByOrderItemIds: Function
  company: any
  companyName: string
  logo: string
  onlyContent?: boolean
  routes: WholesaleRoute[]
}

export class CheckoutContainer extends React.PureComponent<CheckoutContainerProps> {

  render() {
    const { order, orderItems, customers, onlyContent } = this.props
    return (
      <SalesContainer style={{paddingLeft: onlyContent ? 0 : 18}}>
        <DetailsWrapper style={{ padding: onlyContent ? '0 0 20px' : 20, borderTopWidth: onlyContent ? 0 : 5 }}>
          {onlyContent !== true && (
            <>
              <Row>
                <Col>
                  <DetailsTitle>Checkout</DetailsTitle>
                </Col>
              </Row>
              <Changes />
            </>
          )}
          <EditForm data={order} customers={customers} onSave={this.props.onSave} {...this.props} />
          <OrderSummary
            order={order}
            orderItems={orderItems}
            companyName={this.props.companyName}
            logo={this.props.logo}
            company={this.props.company}
            catchWeightValues={this.props.catchWeightValues}
            getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
          />
        </DetailsWrapper>
      </SalesContainer>
    )
  }
}

export default withTheme(CheckoutContainer)
