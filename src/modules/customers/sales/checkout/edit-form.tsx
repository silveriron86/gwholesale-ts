import * as React from 'react'
import {
  pd20,
  InputLabel,
  DetailsRow,
  ThemeInput,
  ThemeTextArea,
  ThemeSelect,
  ThemeButton,
} from '../../customers.style'
import { Layout, Row, Col, Select, Icon, Popconfirm } from 'antd'
import { editFormWrapper } from './_styles'
import { OrderDetail, WholesaleRoute } from '~/schema'
import moment from 'moment'
import { isFirefox } from 'react-device-detect'
import { FINANCIAL_TERMS } from '~/common'
const { Option } = Select
interface EditFormProps {
  data: OrderDetail
  customers: any[] | null
  onSave: Function
  onlyContent?: boolean
  routes: WholesaleRoute[]
}

export class EditForm extends React.PureComponent<EditFormProps> {
  state: any
  constructor(props: EditFormProps) {
    super(props)
    const { data } = this.props
    this.state = {
      contactId: data.wholesaleClient.mainContact ? data.wholesaleClient.mainContact.contactId : '',
      deliveryPhone: data.deliveryPhone,
      deliveryInstruction: data.deliveryInstruction,
      pickerNote: data.pickerNote,
      customerNote: data.customerNote,
      seller: data.seller ? data.seller.userId : '',
      // reference: data.reference,
      freightType: data.freightType ? data.freightType : undefined,
      financialTerms: data.financialTerms ? data.financialTerms : undefined,
      routeId: 0,
    }
  }

  componentDidMount() {
    const { data } = this.props
    if (data && (data.overrideRoute || data.defaultRoute)) {
      const routeId = data.overrideRoute ? data.overrideRoute.id : data.defaultRoute.id
      this.setState({ routeId: routeId })
    }
  }

  handleChange = (type: string, e: any) => {
    this.handleChangeValue(type, e.target.value)
  }

  handleChangeValue = (type: string, value: any) => {
    const state = { ...this.state }
    state[type] = value
    this.setState(state)
  }

  getActiveRoutes = () => {
    const { data, routes } = this.props
    console.log(data, routes)
    let activeRoutes: WholesaleRoute[] = []
    if (data !== null && routes.length > 0) {
      routes.forEach((route) => {
        const weekDay = moment(data.deliveryDate)
          .format('dddd')
          .toUpperCase()
        if (data && route.activeDays.indexOf(weekDay) >= 0) {
          activeRoutes.push({ ...route })
        }
      })
    }
    console.log(activeRoutes)
    return activeRoutes
  }

  onSave = () => {
    const {
      deliveryPhone,
      deliveryInstruction,
      pickerNote,
      customerNote,
      seller,
      freightType,
      financialTerms,
    } = this.state
    let saveData: any = {
      wholesaleOrderId: this.props.data.wholesaleOrderId,
      seller: seller,
      deliveryPhone: deliveryPhone,
      deliveryInstruction: deliveryInstruction,
      pickerNote: pickerNote,
      customerNote: customerNote,
      // reference: reference,
      freightType: freightType,
      financialTerms: financialTerms,
    }
    if (this.state.routeId) {
      saveData.defaultRoute = this.state.routeId
    }
    this.props.onSave(saveData)

    if (freightType == '0' || financialTerms == '0') {
      this.setState({
        freightType: freightType == '0' ? undefined : freightType,
        financialTerms: financialTerms == '0' ? undefined : financialTerms,
      })
    }
  }

  render() {
    const { data, currentCompanyUsers, onlyContent } = this.props
    const {
      deliveryPhone,
      deliveryInstruction,
      pickerNote,
      customerNote,
      seller,
      routeId,
      freightType,
      financialTerms,
    } = this.state
    const activeRoutes = this.getActiveRoutes()
    return (
      <Layout style={onlyContent ? { background: 'transparent' } : editFormWrapper}>
        <div style={{ margin: '0px -20px 30px -20px' }}>
          <Row style={{ marginTop: 16 }}>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Sales Representative</InputLabel>
              <ThemeSelect
                value={seller}
                onChange={this.handleChangeValue.bind(this, 'seller')}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
                style={{ width: '100%' }}
                className="sales-cart-input"
              >
                {currentCompanyUsers &&
                  currentCompanyUsers.map((item, index) => {
                    return (
                      <Select.Option key={index} value={item.userId}>
                        {item.firstName + '  ' + item.lastName}
                      </Select.Option>
                    )
                  })}
              </ThemeSelect>
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Freight</InputLabel>
              <ThemeSelect
                style={{ width: '100%' }}
                placeholder="Freight type"
                value={freightType}
                onChange={this.handleChangeValue.bind(this, 'freightType')}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
              >
                <Option value={'0'}>&nbsp;</Option>
                {['DEL', 'FOB - Shipping Point', 'COL'].map((item) => (
                  <Option key={`delivery-contact-${item}`} value={item}>
                    {item}
                  </Option>
                ))}
              </ThemeSelect>
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Financial Terms</InputLabel>
              <ThemeSelect
                style={{ width: '100%' }}
                placeholder="Financial terms"
                value={financialTerms}
                onChange={this.handleChangeValue.bind(this, 'financialTerms')}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
              >
                <Option value={'0'}>&nbsp;</Option>
                {FINANCIAL_TERMS.map((item) => (
                  <Option key={`delivery-contact-${item}`} value={item}>
                    {item}
                  </Option>
                ))}
              </ThemeSelect>
            </Col>
          </Row>
          <Row style={{ marginTop: 16 }}>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Delivery Driver / Route</InputLabel>
              <ThemeSelect
                className="sales-cart-input"
                value={routeId}
                onChange={this.handleChangeValue.bind(this, 'routeId')}
                style={{ width: '100%' }}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
              >
                <Select.Option value={0}>No Driver/Route Selected</Select.Option>
                {activeRoutes.length > 0 &&
                  activeRoutes.map((route, index) => {
                    return (
                      <Select.Option key={`route=${index}`} value={route.id}>
                        {route.routeName}
                      </Select.Option>
                    )
                  })}
              </ThemeSelect>
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Delivery Contact</InputLabel>
              <ThemeInput
                placeholder="Contact Name"
                className="sales-cart-input"
                value={data.wholesaleClient.mainContact ? data.wholesaleClient.mainContact.name : ''}
                onChange={this.handleChange.bind(this, 'name')}
                disabled
              />
              {/* <ThemeSelect style={{ width: '100%' }} value={contactId} onChange={this.handleChangeValue.bind(this, 'contactId')} disabled>
                {customers && customers.length > 0 && customers.map((item) => (
                  <Option key={`delivery-contact-${item.clientId}`} value={item.mainContact.contactId}>
                    {item.mainContact.name}
                  </Option>
                ))}
              </ThemeSelect>*/}
            </Col>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Delivery Phone</InputLabel>
              <ThemeInput
                placeholder="Phone Number"
                className="sales-cart-input"
                value={deliveryPhone}
                onChange={this.handleChange.bind(this, 'deliveryPhone')}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
              />
            </Col>
          </Row>
          {/* <Row style={{ marginTop: 16 }}>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Reference</InputLabel>
              <ThemeInput
                value={reference}
                onChange={this.handleChange.bind(this, 'reference')}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
                className="sales-cart-input"
              />
            </Col>
          </Row> */}
        </div>
        {onlyContent !== true && (
          <Row style={DetailsRow}>
            <Col lg={8} md={12} style={pd20}>
              <InputLabel>DELIVERY INSTRUCTIONS</InputLabel>
              <ThemeTextArea
                rows={4}
                value={deliveryInstruction}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
                onChange={this.handleChange.bind(this, 'deliveryInstruction')}
              />
            </Col>
            <Col lg={8} md={12} style={pd20}>
              <InputLabel>NOTE TO PICKER</InputLabel>
              <ThemeTextArea
                rows={4}
                value={pickerNote}
                onChange={this.handleChange.bind(this, 'pickerNote')}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
              />
            </Col>
            <Col lg={8} md={12} style={pd20}>
              <InputLabel>NOTE TO CUSTOMER</InputLabel>
              <ThemeTextArea
                rows={4}
                value={customerNote}
                onChange={this.handleChange.bind(this, 'customerNote')}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
              />
            </Col>
          </Row>
        )}
        {false && (
          <Row style={DetailsRow}>
            <Col lg={6} md={12} style={pd20}>
              <InputLabel>Delivery Driver/Route</InputLabel>
              <ThemeSelect
                className="sales-cart-input"
                value={routeId}
                onChange={this.handleChangeValue.bind(this, 'routeId')}
                style={{ width: '100%' }}
                disabled={data.wholesaleOrderStatus == 'CANCEL'}
              >
                <Select.Option value={0}>No Driver/Route Selected</Select.Option>
                {activeRoutes.length > 0 &&
                  activeRoutes.map((route, index) => {
                    return (
                      <Select.Option key={`route=${index}`} value={route.id}>
                        {route.routeName}
                      </Select.Option>
                    )
                  })}
              </ThemeSelect>
            </Col>
          </Row>
        )}
        <Row>
          <Col>
            {/* <Popconfirm
              title="Are you sure to save?"
              onConfirm={this.onSave}
              okText="Save"
              cancelText="Cancel"
            > */}
            <ThemeButton
              className="sales-cart-input"
              onClick={this.onSave}
              type="primary"
              size="large"
              style={{ marginBottom: 30 }}
              disabled={data.wholesaleOrderStatus == 'CANCEL'}
            >
              Save Changes
            </ThemeButton>
            {/* </Popconfirm> */}
          </Col>
        </Row>
      </Layout>
    )
  }
}

export default EditForm
