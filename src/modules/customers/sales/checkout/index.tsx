import * as React from 'react'
import { withTheme } from 'emotion-theming'
import moment from 'moment'
import { Theme } from '~/common'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import SalesOverview from '../widgets/sales-overview'
import { CheckoutContainer } from './checkout-container'
import { RouteComponentProps } from 'react-router'

type SalesCheckoutProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
    onlyContent?: boolean
  }

export class SalesCheckout extends React.PureComponent<SalesCheckoutProps> {
  componentDidMount() {
    const orderId = this.props.match.params.orderId
    // this.props.resetLoading()
    // this.props.getOrderDetail(orderId)
    this.props.getCompanyUsers();
    if (this.props.companyName == '') {
      this.props.getSellerSetting()
    }
    if (this.props.onlyContent === true) {
      this.props.getAllRoutes()
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      // this.props.getAddresses()
    }
  }

  onSave = (data: any) => {
    const { currentOrder } = this.props
    if (currentOrder) {
      data.deliveryDate = moment(currentOrder.deliveryDate).format('MM/DD/YYYY')
      data.wholesaleCustomerClientId = currentOrder.wholesaleClient.clientId
      data.status = currentOrder.wholesaleOrderStatus
      this.props.updateOrderInfo(data)
    }
  }


  render() {
    const { currentOrder, orderItems, customers, onlyContent } = this.props
    if (!currentOrder) {
      return <PageLayout currentTopMenu={'menu-Customers-Sales Orders'} />
    }

    const company = this.props.sellerSetting ? this.props.sellerSetting.company : null

    if (onlyContent) {
      return (
        <>
          <SalesOverview onlyContent={onlyContent} orderId={this.props.match.params.orderId} order={currentOrder} />
          <CheckoutContainer
            {...this.props}
            order={currentOrder}
            orderItems={orderItems}
            customers={customers}
            onSave={this.onSave}
            catchWeightValues={this.props.catchWeightValues}
            getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
            company={company}
          />
        </>
      );
    }

    return (
      <PageLayout currentTopMenu={'menu-Customers-Sales Orders'}>
        <SalesOverview orderId={this.props.match.params.orderId} order={currentOrder} />
        <CheckoutContainer
          {...this.props}
          order={currentOrder}
          orderItems={orderItems}
          customers={customers}
          onSave={this.onSave}
          catchWeightValues={this.props.catchWeightValues}
          getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
          company={company}

        />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(SalesCheckout))
