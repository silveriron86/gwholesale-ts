import React from 'react'
import { withTheme } from 'emotion-theming'
import { CustomerDetailsTitle as DetailsTitle, ThemeButton } from '../../customers.style'
import { Row, Col, Icon, Modal } from 'antd'
import { DetailsSection, DetailLabel, fullButton, mb0, SalesContainer } from '../_style'
import { DetailValue } from './_styles'
import { CustomButton } from '~/components'
import { OrderDetail, OrderItem } from '~/schema'
import { formatNumber } from '~/common/utils'
import PrintBillOfLading from '~/modules/customers/sales/cart/print/print-bill-lading'
import { printWindow } from '~/common/utils'

interface OrderSummaryProps {
  order: OrderDetail
  orderItems: OrderItem[]
  companyName: string
  logo: string
  catchWeightValues: any
  getWeightsByOrderItemIds: Function
  company: any
}

export class OrderSummary extends React.PureComponent<OrderSummaryProps> {

  state = {
    PrintBillOfLadingReviewShow: false,
    PrintBillOfLadingRef: null
  }

  openPrintModal = () => {
    this.setState({
      PrintBillOfLadingReviewShow: true,
    })
  }

  printDeliverListTrigger = () => {
    return (
      <ThemeButton shape="round" style={fullButton}>
        <Icon type="printer" theme="filled" />
        Print Delivery List
      </ThemeButton>
    )
  }

  printContent: any = () => {
    return this.state.PrintBillOfLadingRef
  }

  closePrintModal = () => {
    this.setState({
      PrintBillOfLadingReviewShow: false,
    })
  }

  render() {
    const { order, orderItems } = this.props
    let totalUnit = 0 //orderItems.length
    let totalCost = 0
    let totalQuantity = 0
    let returnTotal = 0
    if (orderItems && orderItems.length > 0) {
      orderItems.forEach((item: any) => {
        // totalUnit += item.quantity
        // totalQuantity += item.picked
        totalUnit += item.picked
        totalQuantity += item.catchWeightQty
        totalCost += item.picked * item.price
        if (item.UOM == item.inventoryUOM && item.inventoryUOM) {
          returnTotal += item.returnedQty * item.ratioUOM * item.price
        } else {
          returnTotal += item.returnedQty * item.price
        }

      })
    }
    // const orderTotal = totalCost + (order.shippingCost ? order.shippingCost : 0)
    const orderTotal = order.totalPrice + (order.shippingCost ? order.shippingCost : 0)

    return (
      <div style={{ paddingBottom: 20 }}>
        <DetailsTitle style={mb0}>ORDER SUMMARY</DetailsTitle>
        <DetailsSection>
          <Row>
            <Col md={4}>
              <DetailLabel>TOTAL UNITS</DetailLabel>
              <DetailValue>{totalUnit}</DetailValue>
            </Col>
            <Col md={4}>
              <DetailLabel>TOTAL QUANTITY</DetailLabel>
              <DetailValue>{formatNumber(totalQuantity, 2)}</DetailValue>
            </Col>
            <Col md={4}>
              <DetailLabel>ORDER TOTAL</DetailLabel>
              <DetailValue>${formatNumber(orderTotal, 2)}</DetailValue>
            </Col>
            <Col md={4}>
              <DetailLabel>RETURN TOTAL</DetailLabel>
              <DetailValue>${formatNumber(returnTotal, 2)}</DetailValue>
            </Col>
            {/* <Col md={4}>
              <ThemeButton shape="round" style={fullButton} onClick={this.openPrintModal.bind(this)} disabled={order.wholesaleOrderStatus == 'CANCEL'}>
                <Icon type="printer" theme="filled" />
                View / Print Delivery List
              </ThemeButton>
            </Col> */}
            <Modal
              width={1080}
              footer={null}
              visible={this.state.PrintBillOfLadingReviewShow}
              onCancel={this.closePrintModal.bind(this)}
            >
              <div id={'PrintBillOfLadingModal'}>

                <PrintBillOfLading
                  getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                  catchWeightValues={this.props.catchWeightValues}
                  orderItems={orderItems}
                  currentOrder={order}
                  companyName={this.props.companyName}
                  company={this.props.company}
                  logo={this.props.logo}
                  ref={(el) => {
                    this.setState({ PrintBillOfLadingRef: el })
                  }}
                />
              </div>
              <ThemeButton shape="round" style={fullButton} onClick={() => printWindow('PrintBillOfLadingModal', this.printContent.bind(this))}>
                <Icon type="printer" theme="filled" />
                Print Delivery List
              </ThemeButton>
            </Modal>
          </Row>
        </DetailsSection>
      </div>
    )
  }
}

export default withTheme(OrderSummary)
