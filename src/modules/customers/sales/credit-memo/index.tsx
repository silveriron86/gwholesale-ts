import React from 'react'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import SalesOverview from '../widgets/sales-overview'
import { RouteComponentProps } from 'react-router'
import { Row, Col } from 'antd'
import { SalesContainer } from '../_style'
import { CustomerDetailsWrapper as DetailsWrapper, CustomerDetailsTitle as DetailsTitle, floatLeft } from '../../customers.style'
import MemoTotalTable from './memo-total-table'

type CreditMemoProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
    onlyContent?: boolean
  }

export class CreditMemo extends React.PureComponent<CreditMemoProps> {
  componentDidMount() {
    const orderId = this.props.match.params.orderId
    // this.props.resetLoading()
    // this.props.getOrderDetail(orderId)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      // this.props.getAddresses()
    }
  }

  render() {
    const { currentOrder, onlyContent } = this.props
    if (!currentOrder) {
      if (onlyContent) return null
      return <PageLayout currentTopMenu={'menu-Customers-Sales Orders'} />
    }

    const content = (
      <>
        <SalesOverview onlyContent={onlyContent} orderId={this.props.match.params.orderId} order={currentOrder} />
        <SalesContainer style={{ padding: onlyContent ? 0 : '0 18px' }}>
          <DetailsWrapper style={{ padding: onlyContent ? '5px 0 20px' : 20, borderTopWidth: onlyContent ? 0 : 5 }}>
            {onlyContent !== true && (
              <Row>
                <Col>
                  <DetailsTitle style={floatLeft}>Credit Memo</DetailsTitle>
                </Col>
              </Row>
            )}
            <Row>
              <Col>
                <MemoTotalTable orderId={this.props.match.params.orderId} />
              </Col>
            </Row>
          </DetailsWrapper>
        </SalesContainer>
      </>
    )

    if (onlyContent) {
      return content;
    }

    return (
      <PageLayout currentTopMenu={'menu-Customers-Sales Orders'}>
        {content}
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(CreditMemo))
