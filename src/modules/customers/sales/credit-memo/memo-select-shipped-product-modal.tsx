import React, { CSSProperties, FC, useEffect, useState } from 'react'
import { DatePicker, Row, Select, Table, Input, Icon } from 'antd'
import { ThemeModal, ThemeSelect } from '../../customers.style'
import { ThemePagination } from '../../accounts/chat/_styles'
import { checkError, formatItemDescription, getOrderPrefix, isCustomOrderNumberEnabled, responseHandler } from '~/common/utils'
import { of } from 'rxjs'
import { formatNumber } from '~/common/utils'

import { OrderService } from '~/modules/orders/order.service'
import moment from 'moment'
import { CreditMemoTableWrapper } from '../_style'
import _ from 'lodash'
import jQuery from 'jquery'

const { Option } = Select
const { RangePicker } = DatePicker
const { Search } = Input

interface ISelectShippedProductModalProps {
  orderId: string
  onCancel: Function
  createOrderAdjustment: Function
  adjustmentList: any
  sellerSetting: any
}

const ORDER_BY_TYPES = {
  VARIETY: 'VARIETY',
  WHOLESALEORDERID: 'WHOLESALEORDERID',
  PRICE: 'PRICE',
  LOTID: 'LOTID',
  DELIVERYDATE: 'DELIVERYDATE',
}

const DIRECTION_TYPES = {
  ASC: 'ASC',
  DESC: 'DESC',
}

const ANTD_ORDER_CONVER = {
  ascend: DIRECTION_TYPES.ASC,
  descend: DIRECTION_TYPES.DESC,
}

const dateFormat = 'MM/DD/YYYY'

const presets = [
  {
    key: 0,
    label: 'Yesterday',
    from: moment()
      .subtract(1, 'days')
      .format(dateFormat),
    to: moment().format(dateFormat),
  },
  {
    key: 1,
    label: 'Past 7 Days',
    from: moment()
      .subtract(7, 'days')
      .format(dateFormat),
    to: moment().format(dateFormat),
  },
  {
    key: 2,
    label: 'Past 30 Days',
    from: moment()
      .subtract(30, 'days')
      .format(dateFormat),
    to: moment().format(dateFormat),
  },

  {
    key: 3,
    label: 'Custom date range',
  },
]

const ThemeSelectStyle = (): CSSProperties => ({
  width: '150px',
  fontWeight: 500
  // margin: '-4px 30px 0 10px'
})

const ThemePaginationStyle = (): CSSProperties => ({
  textAlign: 'center',
  marginTop: '20px',
})

const ProductStyle = (): CSSProperties => ({
  color: '#1C6E31',
})

const SelectShippedProductModal: FC<ISelectShippedProductModalProps> = ({
  orderId,
  onCancel,
  createOrderAdjustment,
  adjustmentList,
  sellerSetting,
}) => {
  const [totalDataSource, setTotalDataSource] = useState<any>([])
  const [filterData, setFilterData] = useState([])
  const [total, setDTotal] = useState(0)
  const [tableLoading, setTableLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [pageSize] = useState(10)
  const [showCustomDateRange, setshowCustomDateRange] = useState(false)
  const [dateFilter, setDateFilter] = useState({ from: presets[2].from, to: presets[2].to })
  const [orderBy, setOrderBy] = useState(null)
  const [direction, setDirection] = useState(null)
  const [searchKey, setSearchKey] = useState('')

  const fetchData = () => {
    const query = { page, pageSize, ...dateFilter, orderBy, direction, searchKey }
    setTableLoading(true)

    OrderService.instance.getRelatedOrderItemList({ orderId, query }).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        setTotalDataSource(res.body.data.dataList)
        setFilterData(res.body.data.dataList)
        setDTotal(res.body.data.total)
      },
      error(err) {
        checkError(err)
      },
      complete() {
        setTableLoading(false)
      },
    })
  }

  useEffect(() => {
    setTimeout(() => {
      jQuery('.add-credit-memo-search input').trigger('focus')
    }, 50)
  }, [])

  useEffect(() => {
    fetchData()
  }, [page, dateFilter, orderBy, direction])

  const columns = [
    {
      title: 'Product',
      sorter: {
        columnKey: ORDER_BY_TYPES.VARIETY,
      },
      width: 350,
      render: (r) => <span style={ProductStyle()}>{formatItemDescription(r.variety, r.sku, sellerSetting, r.customerProductCode)}</span>,
    },
    {
      title: 'Sales order no',
      sorter: {
        columnKey: ORDER_BY_TYPES.WHOLESALEORDERID,
      },
      render: (r) => {
        if (isCustomOrderNumberEnabled(sellerSetting) && r.customOrderNo) {
          return (<div>{`${getOrderPrefix(sellerSetting, 'sales')}${r.customOrderNo}`}</div>)
        } else {
          return (<div>{`${getOrderPrefix(sellerSetting, 'sales')}${r.wholesaleOrderId}`}</div>)
        }
      },
    },
    {
      title: 'Target Fulfillment date',
      sorter: {
        columnKey: ORDER_BY_TYPES.DELIVERYDATE,
      },
      render: (r) => <div>{moment(r.deliveryDate).format(dateFormat)}</div>,
    },
    {
      title: 'Shipped units',
      render: (r) => (
        <div>
          {r.picked} {r.UOM}
        </div>
      ),
    },
    {
      title: 'Price',
      sorter: {
        columnKey: ORDER_BY_TYPES.PRICE,
      },
      className: 'th-right',
      render: (r) => <div>${formatNumber(Math.abs(r.price), 2)}</div>,
    },
    {
      title: 'Lot',
      sorter: {
        columnKey: ORDER_BY_TYPES.LOTID,
      },
      dataIndex: 'lotId',
      render: (lotId: string, record: any) => {
        return lotId ? lotId : 'No lots available'
      },
    },
  ]

  const handleChangeDateFilter = (key: number) => {
    if (key === 3) {
      return setshowCustomDateRange(true)
    }
    setPage(0)
    setshowCustomDateRange(false)
    return setDateFilter({
      from: presets[key].from,
      to: presets[key].to,
    })
  }

  const handleChangeTable = (_pagination: any, _filters: any, sorter: any) => {
    if (sorter.column) {
      setOrderBy(sorter.column.sorter.columnKey)
      setDirection(ANTD_ORDER_CONVER[sorter.order])
    } else {
      setOrderBy(null)
      setDirection(null)
    }
  }

  const handleSearch = (value: string) => {
    setSearchKey(value)
    setPage(0)
    fetchData()
  }

  const onClearSearch = (evt: any) => {
    evt.stopPropagation()
    setSearchKey('')
    setPage(0)
    fetchData()
  }

  const handleChangeSearch = (evt: any) => {
    setSearchKey(evt.target.value)
    setPage(0)
  }

  return (
    <ThemeModal
      title={
        // <Row justify='space-between' type='flex'>
        <span>Select item to credit</span>

        //   <Row type='flex'>
        //     {showCustomDateRange &&
        //       <RangePicker
        //         style={{ marginTop: '-4px' }}
        //         placeholder={['From', 'To']}
        //         format={dateFormat}
        //         allowClear={false}
        //         onChange={(_, dateStrings) => { setDateFilter({ from: dateStrings[0], to: dateStrings[1] }), setPage(0) }}
        //       />
        //     }
        //     <ThemeSelect defaultValue={2} style={ThemeSelectStyle()} onChange={handleChangeDateFilter}>
        //       {presets.map(v => <Option key={v.key} value={v.key}>{v.label}</Option>)}
        //     </ThemeSelect>
        //   </Row>
        // </Row>
      }
      visible
      onCancel={onCancel}
      width={1100}
      footer={null}
    >
      <CreditMemoTableWrapper>
        <Row justify="space-between" type="flex" style={{ alignItems: 'center', marginBottom: 24 }}>
          <div style={{ width: '40%' }} className='add-credit-memo-search'>
            <Search
              placeholder="Search by PRODUCT NAME, SKU ..."
              onSearch={handleSearch}
              style={{ width: 300 }}
              onChange={handleChangeSearch}
              value={searchKey}
              suffix={<Icon className={`clear-icon ${searchKey ? 'show' : 'hide'}`} onClick={onClearSearch} viewBox="0 0 16 16" width="16" height="16" type="close-circle" />}
            />
          </div>

          <Row type="flex">
            {showCustomDateRange && (
              <RangePicker
                placeholder={['From', 'To']}
                format={dateFormat}
                allowClear={false}
                onChange={(_, dateStrings) => {
                  setDateFilter({ from: dateStrings[0], to: dateStrings[1] }), setPage(0)
                }}
              />
            )}
            <ThemeSelect defaultValue={2} style={ThemeSelectStyle()} onChange={handleChangeDateFilter}>
              {presets.map((v) => (
                <Option key={v.key} value={v.key}>
                  {v.label}
                </Option>
              ))}
            </ThemeSelect>
          </Row>
        </Row>
        <Table
          loading={tableLoading}
          dataSource={totalDataSource}
          columns={columns}
          pagination={false}
          onChange={handleChangeTable}
          onRow={(record) => {
            return {
              onClick: () => {
                if (adjustmentList.some((v: any) => v.wholesaleOrderItemId === record.wholesaleOrderItemId))
                  return onCancel()
                createOrderAdjustment(record)
                onCancel()
              },
            }
          }}
        />
        <ThemePagination
          onChange={(page) => setPage(page - 1)}
          current={page + 1}
          pageSize={10}
          total={total}
          style={ThemePaginationStyle()}
        />
      </CreditMemoTableWrapper>
    </ThemeModal>
  )
}

export default SelectShippedProductModal
