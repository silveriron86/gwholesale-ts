import React from 'react'
import { PureWrapper as Wrapper, WrapperTitle } from '../../accounts/addresses/addresses.style'
import { ml0 } from '~/modules/customers/customers.style'
import { CreditMemoModel } from '../../customers.model'
import { cloneDeep } from 'lodash'
import { EditableTable } from '~/components/Table/editable-table'

interface MemoTableProps {
  columns: Array<CreditMemoModel>
}

export class MemoTable extends React.PureComponent<MemoTableProps> {
  state = {
    initItems: cloneDeep(this.props.columns),
  }

  handleSave = (row: CreditMemoModel) => {
    const newData = [...this.props.columns]
    const index = newData.findIndex((item) => row.id === item.id)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
  }

  render() {
    const { initItems } = this.state
    const listItems: CreditMemoModel[] = cloneDeep(initItems)

    const columns = [
      {
        title: 'RETURN UNIT',
        dataIndex: 'return_unit',
        editable: true,
        edit_width: 60,
      },
      {
        title: 'RETURN NET WEIGHT',
        dataIndex: 'return_net_weight',
        editable: true,
        edit_width: 60,
      },
      {
        title: 'RETURN REASON',
        dataIndex: 'return_reason',
        editable: true,
        type: 'select',
        edit_width: 100,
      },
      {
        title: 'RE-INVENTORY',
        dataIndex: 're_inventory',
        editable: true,
        edit_width: 100,
      },
    ]

    return (
      <Wrapper style={ml0}>
        <WrapperTitle>Credit Memo</WrapperTitle>
        <EditableTable columns={columns} dataSource={listItems} handleSave={this.handleSave} />
      </Wrapper>
    )
  }
}

export default MemoTable
