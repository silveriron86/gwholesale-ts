import React from 'react'
import { ThemeButton, ThemeCheckbox, ThemeIcon, ThemeInput, ThemeInputNumber } from '~/modules/customers/customers.style'
import { cloneDeep } from 'lodash'
import { Icon as IconSVG } from '~/components'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import SelectShippedProductModal from './memo-select-shipped-product-modal'
import { Button, Modal, Popconfirm, Icon, Select, Table, Tooltip, notification } from 'antd'
import {
  formatNumber,
  mathRoundFun,
  basePriceToRatioPrice,
  ratioQtyToInventoryQty,
  formatItemDescription,
  isCustomOrderNumberEnabled,
  getOrderPrefix,
  judgeConstantRatio,
  numberMultipy,
  inventoryQtyToRatioQty,
  getAllowedEndpoint,
  printWindow, ratioPriceToBasePrice,
} from '~/common/utils'
import { CartTableWrapper, Flex, ItemHMargin4, Unit } from '../../nav-sales/styles'
import UomSelect from '../../nav-sales/order-detail/components/uom-select'
import _ from 'lodash'
import { OrderItem } from '~/schema'
import { CMLabel, CMValue, PrintModalHeader } from '../_style'
import moment from 'moment'
import styled from '@emotion/styled'
import AddNewTypeModal from './reasonModals/AddNewTypeModal'
import PrintMemo from '../cart/print/print-memo'
import { Http, gray01 } from '~/common'
import { SettingService } from '~/modules/setting/setting.service'

type MemoTotalTableProps = OrdersDispatchProps &
  OrdersStateProps & {
    orderId: string
    isPurchase?: Boolean
  }

const { Option } = Select
export class MemoTotalTable extends React.PureComponent<MemoTotalTableProps> {
  state = {
    dataSource: cloneDeep(this.props.adjustments),
    showTable: this.props.adjustments.length > 0 ? true : false,
    selectShippedProductModalVisible: false,
    currentUOM: '',
    addNewTypeVisible: false,
    printMemoVisible: false,
    reasonTypes: [],
  }

  componentDidMount() {
    const { orderId } = this.props
    this.props.getOrderAdjustments(orderId)
    this.props.getCompanyProductAllTypes()
  }

  onChangeUom = (orderItemId: string, value: any) => {
    const newData = [...this.state.dataSource]
    newData.map((orderItem: any) => {
      if (orderItem.wholesaleOrderItemId === orderItemId) {
        orderItem[this.state.currentUOM] = value
        this.handleSave(orderItem)
        return
      }
    })
    this.setState({ dataSource: cloneDeep(newData) })
  }

  updateOrderItem = (orderItemId: number, type: string, pas: boolean) => {
    //
  }

  onChangeItem = (wholesaleOrderItemId: number, type: string, value: string | boolean) => {
    if (type == 'reasonType' && value == 'add') {
      return
    }
    const newData = [...this.state.dataSource]
    newData.map((orderItem: any) => {
      if (orderItem.wholesaleOrderItemId === wholesaleOrderItemId) {
        orderItem[type] = value
        if (value === false) {
          orderItem['returnInventoryQuantity'] = 0
          orderItem['returnInventoryUOM'] = null
        }
        if (type === 'returnInInventory') {
          if(orderItem['adjustmentId'])
            this.handleSave(orderItem)
          else
            orderItem[type] = false
        }
        return
      }
    })
    this.setState({ dataSource: newData })
  }

  changeTableStatus = () => {
    this.setState({
      showTable: true,
    })
  }

  handleSave = (row: any, type: string = '') => {
    const { adjustments } = this.props
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
    if (adjustments.length > 0) {
      const filtered = adjustments.filter((item) => item.orderItem.wholesaleOrderItemId === row.wholesaleOrderItemId)
      if (filtered.length > 0) {
        // update
        // tslint:disable-next-line:prefer-const
        let data = this._getAdjustmentObject(row)
        data.adjustmentId = row.adjustmentId
        if (
          data.returnQuantity !== item.returnQuantity ||
          data.UOM !== item.UOM ||
          data.returnReason !== item.returnReason ||
          data.returnInInventory !== item.returnInInventory ||
          data.reasonType !== item.reasonType ||
          data.salePrice !== item.salePrice
        ) {
          this.props.updateOrderAdjustment(data)
        }
        return
      }
    } else if(type != 'returnQuantity') {
      return
    }
    this.props.createOrderAdjustment({ orderId: this.props.orderId, requestData: this._getAdjustmentObject(row) })
  }

  _getAdjustmentObject = (row: any) => {
    //console.log(row)
    const quantity = Math.abs(row.returnQuantity)
    const returnQuantity = Math.abs(row.returnInventoryQuantity)
    // const expiredQuantity = Math.abs(row.return_expired_quantity)
    return {
      adjustmentId: '',
      orderItemId: row.wholesaleOrderItemId,
      quantity: quantity ? quantity : 0,
      // expiredQuantity: expiredQuantity,
      returnInInventory: row.returnInInventory ? true : false,
      reason: row.returnReason ? row.returnReason : '',
      UOM: row.returnUom ? row.returnUom : row.UOM,
      // returnWeight: row.return_net_weight,
      type: 1,
      returnQuantity: row.returnInInventory ? returnQuantity : 0,
      returnUOM: row.returnInventoryUOM || row.returnUom || row.UOM,
      reasonType: row.reasonType,
      salePrice: row.salePrice
      // cost: row.return_net_weight
    }
  }

  componentWillReceiveProps(nextProps: MemoTotalTableProps) {
    //set reason type
    //console.log(this.props.adjustments)
    this.setState({
      reasonTypes: nextProps.companyProductTypes.reasonType.map((item: any) => {
        return {
          id: item.id,
          name: item.name,
        }
      }),
    })
    //
    const orderItemIds = nextProps.orderItems.map((v) => v.wholesaleOrderItemId)
    const adjustmentIds = nextProps.adjustments.length
      ? nextProps.adjustments.map((v) => v.orderItem.wholesaleOrderItemId)
      : []

    const extraAdjustments = nextProps.adjustments.filter(
      (ad) => !orderItemIds.includes(ad.orderItem.wholesaleOrderItemId),
    )


    if (nextProps.currentOrder.wholesaleClient.wholesaleCompany.isAutomatically) {
      if (
        nextProps.orderItems.length > 0 &&
        _.intersection(orderItemIds, adjustmentIds).length < nextProps.orderItems.length
      ) {
        const data = [...nextProps.orderItems]
        const adjustments = [...nextProps.adjustments]
        data.map((orderItem: any) => {
          const filtered = adjustments.filter(
            (ad) => ad.orderItem.wholesaleOrderItemId === orderItem.wholesaleOrderItemId,
          )
          if (filtered.length > 0) {
            orderItem.adjustmentId = filtered[0].id
            orderItem.returnQuantity = filtered[0].quantity
            // orderItem.return_expired_quantity = filtered[0].returnExpiredQty
            orderItem.returnReason = filtered[0].reason
            orderItem.reasonType = filtered[0].reasonType
            // orderItem.return_net_weight = filtered[0].returnWeight
            // orderItem.margin = filtered[0].margin
            orderItem.returnUom = filtered[0].uom
            orderItem.returnInInventory = filtered[0].returnInInventory ? true : false
            orderItem.returnInventoryQuantity = filtered[0].returnQuantity || filtered[0].quantity
            orderItem.returnInventoryUOM = filtered[0].returnUOM
            orderItem.salePrice = filtered[0].salePrice
            orderItem.tax = filtered[0].tax
            orderItem.taxRate = filtered[0].taxRate
            orderItem.taxEnabled = filtered[0].orderItem.wholesaleItem.taxEnabled
          } else {
            orderItem.returnUom = orderItem.UOM
            orderItem.returnQuantity = 0
            orderItem.salePrice = orderItem.price
          }
        })

        this.setState({
          dataSource: cloneDeep([
            ...extraAdjustments.map((v) => ({
              ...v,
              ...v.orderItem.wholesaleItem,
              ...v.orderItem,
              adjustmentId: v.id,
              returnQuantity: v.quantity,
              returnReason: v.reason,
              returnUom: v.uom,
              reasonType: v.reasonType,
              returnInInventory: v.returnInInventory ? true : false,
              UOM: v.orderItem.uom,
              returnInventoryQuantity: v.returnQuantity || v.quantity,
              returnInventoryUOM: v.returnUOM,
              tax: v.tax,
              taxRate: v.taxRate,
              taxEnabled: v.orderItem.wholesaleItem.taxEnabled,
            })),
            ...data,
          ]),
        })
      }

      if (
        nextProps.adjustments !== this.props.adjustments &&
        _.intersection(orderItemIds, adjustmentIds).length >= nextProps.orderItems.length
      ) {
        this.setState({
          dataSource: nextProps.adjustments.map((v) => ({
            ...v,
            ...v.orderItem.wholesaleItem,
            ...v.orderItem,
            adjustmentId: v.id,
            returnQuantity: v.quantity,
            returnReason: v.reason,
            returnUom: v.uom,
            returnInInventory: v.returnInInventory ? true : false,
            UOM: v.orderItem.uom,
            returnInventoryQuantity: v.returnQuantity || v.quantity,
            returnInventoryUOM: v.returnUOM,
            tax: v.tax,
            taxRate: v.taxRate,
            taxEnabled: v.orderItem.wholesaleItem.taxEnabled,
          })),
        })
      }
    } else {
      return this.setState({
        dataSource: nextProps.adjustments.map((v) => ({
          ...v,
          ...v.orderItem.wholesaleItem,
          ...v.orderItem,
          adjustmentId: v.id,
          returnQuantity: v.quantity,
          returnReason: v.reason,
          returnUom: v.uom,
          returnInInventory: v.returnInInventory ? true : false,
          UOM: v.orderItem.uom,
          returnInventoryQuantity: v.returnQuantity || v.quantity,
          returnInventoryUOM: v.returnUOM,
          tax: v.tax,
          taxRate: v.taxRate,
          taxEnabled: v.orderItem.wholesaleItem.taxEnabled,
        })),
      })
    }
  }

  // getBillableQtyWithUOM = (record: any) => {
  //   let showBillableQty,
  //     uom,
  //     showValue = false,
  //     subUom = null,
  //     pricingSubUom = null

  //   if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
  //     let tempUom = record.wholesaleProductUomList.filter(
  //       (productUom: WholesaleProductUom) => productUom.name === record.returnUom,
  //     )
  //     subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
  //     tempUom = record.wholesaleProductUomList.filter(
  //       (productUom: WholesaleProductUom) => productUom.name === record.returnUom,
  //     )
  //     pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
  //   }

  //   if (record.returnUom) {
  //     uom = record.returnUom
  //     if (record.returnUom === record.inventoryUOM) {
  //       if (record.returnUom === record.inventoryUOM) {
  //         showBillableQty = record.returnQuantity ? record.returnQuantity : 0
  //       } else if (subUom != null && uom === subUom.name) {
  //         console.log(1)
  //         showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity / subUom.ratio, 4) : 0
  //       }
  //     } else {
  //       if (record.returnUom === record.inventoryUOM && pricingSubUom != null) {
  //         showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity * pricingSubUom.ratio, 4) : 0
  //       } else if (subUom != null && uom === subUom.name && pricingSubUom != null) {
  //         showBillableQty = record.returnQuantity ? (record.returnQuantity / subUom.ratio) * pricingSubUom.ratio : 0
  //       }
  //     }
  //   } else {
  //     showBillableQty = record.returnQuantity
  //     uom = record.returnUom || record.inventoryUOM
  //   }
  //   return {
  //     showBillableQty
  //   }
  // }

  getBillableQtyWithUOM = (record: any) => {
    let showBillableQty,
      uom,
      subUom = null,
      pricingSubUom = null

    if (_.isArray(record.wholesaleProductUomList) && record.wholesaleProductUomList.length > 0) {
      let tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.returnUom,
      )
      subUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
      tempUom = record.wholesaleProductUomList.filter(
        (productUom: WholesaleProductUom) => productUom.name === record.pricingUOM,
      )
      pricingSubUom = _.isArray(tempUom) && tempUom.length > 0 ? tempUom[0] : null
    }

    if (record.returnQuantity) {
      uom = record.pricingUOM
      if (record.pricingUOM === record.inventoryUOM) {
        if (record.returnUom === record.inventoryUOM) {
          showBillableQty = record.returnQuantity ? record.returnQuantity : 0
        } else if (subUom != null && record.returnUom === subUom.name) {
          showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity / subUom.ratio, 4) : 0
        } else {
        }
      } else {
        if (record.returnUom === record.inventoryUOM && pricingSubUom != null) {
          showBillableQty = record.returnQuantity ? mathRoundFun(record.returnQuantity * pricingSubUom.ratio, 4) : 0
        } else if (subUom != null && record.returnUom === subUom.name && pricingSubUom != null) {
          showBillableQty = record.returnQuantity ? (record.returnQuantity / subUom.ratio) * pricingSubUom.ratio : 0
        }
      }
    } else {
      showBillableQty = record.returnQuantity
      uom = record.pricingUOM
    }
    if (!record.pricingUOM) {
      showBillableQty = record.returnQuantity
    }
    return {
      showBillableQty,
      uom,
    }
  }

  insertNewType = (newType: any) => {
    /*const newTypeArr: any[] = this.state.reasonTypes
    console.log(newType)
    newTypeArr.push(newType)
    this.setState({ reasonTypes: newTypeArr })
    console.log(this.state.reasonTypes)*/
    this.props.getCompanyProductAllTypes()
  }

  getPrintDeliveryItems = (catchWeightValues: any) =>
    Object.values(this.props.orderItemByProduct).filter((el) => {
      if (el.quantity > 0) {
        // Order Qty > 0
        return true
      }

      let pickedQty = 0
      if (!judgeConstantRatio(el)) {
        const recordWeights = catchWeightValues[el.wholesaleOrderItemId]
        pickedQty = recordWeights ? recordWeights.length : 0
      } else {
        pickedQty = el.picked
      }

      if (pickedQty > 0) {
        // Picked Qty > 0
        return true
      }
    })

  getPrintAllDeliveryItems = (prop: any) => {
    const printDeliveryItems = this.getPrintDeliveryItems(prop.catchWeightValues)
    printDeliveryItems.concat(prop.oneOffItemList)
    return printDeliveryItems
  }

  updatePriceFromTotal = (evt: any, record: any) => {
    let total = 0
    const strVal = evt.target.value.replace('$', '').trim()
    if (strVal !== '') {
      total = parseFloat(strVal)
    }

    const { showBillableQty } = this.getBillableQtyWithUOM(record)
    const price = total / showBillableQty


    this.handleChangeItemPrice(record, price)
  }

  handleChangeItemPrice = (record, price) => {
    if (formatNumber(record.salePrice, 8) === formatNumber(ratioPriceToBasePrice(record.pricingUOM, price, record, 12), 8)) {
      return
    }

    this.handleSave({
      ...record,
      salePrice: ratioPriceToBasePrice(record.pricingUOM, price, record, 12)
    })
  }

  //reason type

  render() {
    const { dataSource, showTable, selectShippedProductModalVisible } = this.state
    const { isPurchase, catchWeightValues, currentOrder, updating, adjustments, orderId, sellerSetting } = this.props
    // const readOnly = currentOrder.wholesaleOrderStatus !== 'SHIPPED'
    const readOnly = false
    const accountType = localStorage.getItem('accountType') || ''
    const columns: Array<any> = [
      {
        title: '',
      },
      {
        title: 'Item',
        dataIndex: 'variety',
        width: 300,
        render: (variety: string, record: any, index: number) => {
          let visibleWarningTooltip = record.picked !== 0
          if (record.constantRatio === false) {
            const weights = catchWeightValues[record.wholesaleOrderItemId]
            visibleWarningTooltip = weights && weights.length ? true : false
          }
          return (
            <>
              <div className="name-column">
                <Flex style={{ paddingLeft: 8 }}>
                  <a
                    href={`#/product/${record.wholesaleItemId || record.itemId}/${getAllowedEndpoint(
                      'product',
                      accountType,
                      'specifications',
                    )}`}
                    className="product-name"
                  >
                    {formatItemDescription(variety, record.sku, sellerSetting, record.customerProductCode)}
                  </a>
                  <div>
                    <Tooltip placement="top" title={'Note added'}>
                      <IconSVG
                        // onClick={openModal.bind(this, 'addNoteModal', record)}
                        type="add-note"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          marginTop: 2,
                          display: record.note ? 'block' : 'none',
                          fill: 'none',
                        }}
                      />
                    </Tooltip>
                  </div>
                  <div>
                    <Tooltip placement="top" title={'Work order added'}>
                      <IconSVG
                        // onClick={openModal.bind(this, 'addWOModal', record)}
                        type="add-wo"
                        viewBox="0 0 20 20"
                        width={20}
                        height={20}
                        style={{
                          ...ItemHMargin4,
                          display: record.workOrderStatus ? 'block' : 'none',
                          cursor: 'normal',
                        }}
                      />
                    </Tooltip>
                  </div>
                </Flex>
              </div>
            </>
          )
        },
      },
      {
        title: 'Original order',
        render: (r: any) => {
          if (r.order) {
            const isCustomOrderNo = isCustomOrderNumberEnabled(sellerSetting) && r.order.customOrderNo
            const orderPrefix = getOrderPrefix(sellerSetting, 'sales')
            const deliveryDate = moment(r.order.deliveryDate).format('MM/DD/YYYY')
            if (isCustomOrderNo) {
              return (
                <span>
                  {orderPrefix + r.order.customOrderNo}
                  <br />
                  <span style={{fontSize: 12}}>{deliveryDate}</span>
                </span>
              )
            } else {
              return (
                <span>
                  {orderPrefix + r.order.wholesaleOrderId}
                  <br />
                  <span style={{fontSize: 12}}>{deliveryDate}</span>
                </span>
              )
            }
          } else {
            return ''
          }
        },
      },
      {
        title: 'Lot',
        dataIndex: 'lotId',
        width: 150,
        render: (lotId: string, record: any) => {
          return lotId ? lotId : 'No lots available'
        },
      },
      {
        title: 'Shipped units',
        dataIndex: 'quantity',
        width: 120,
        render: (quantity: number, record: any) => {
          const unitUOM = record.UOM ? record.UOM : record.inventoryUOM

          let price = record.price
          const pUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          if (_.isNumber(price)) {
            price = basePriceToRatioPrice(pUOM, price, record)
          }

          // const pickedQty = judgeConstantRatio(record) ? record.picked : record.catchWeightQty
          const pickedQty = record.picked
          return (
            <>
              {pickedQty ? formatNumber(pickedQty, 2) : ''} {unitUOM}
              <div style={{fontSize: 12, whiteSpace: 'nowrap'}}>
                at {`${price ? '$' + formatNumber(price, 2) : '$0.00'}`}/{pUOM}
              </div>
            </>
          )
        },
      },
      {
        title: 'Credited units',
        dataIndex: 'returnQuantity',
        width: 200,
        render: (quantity: number, record: OrderItem) => {
          // record.UOM = record.return_uom ? record.return_uom : record.UOM
          return (
            <Flex className="v-center">
              <ThemeInput
                value={quantity ? quantity : 0}
                style={{ marginRight: 4, width: 60, textAlign: 'right' }}
                onChange={(evt: any) =>
                  this.onChangeItem(record.wholesaleOrderItemId, 'returnQuantity', evt.target.value.match(/^\d*(\.?\d{0,2})/g)[0])
                }
                onBlur={() => this.handleSave(record, 'returnQuantity')}
                disabled={readOnly}
              />
              <UomSelect
                disabled={readOnly}
                orderItem={record}
                handlerChangeUom={(orderItemId, value) => {
                  this.setState({ currentUOM: 'returnUom' }, () => {
                    this.onChangeUom(orderItemId, value)
                  })
                }}
                type={1}
                from="creditMemo"
              />
            </Flex>
          )
        },
      },
      {
        title: 'Credit/Unit',
        dataIndex: 'salePrice',
        width: 150,
        render: (value: number, record: any) => {
          let price = value
          const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          let cost = record.lotCost
          if (_.isNumber(price)) {
            price = basePriceToRatioPrice(pricingUOM, value, record)
          }

          if (_.isNumber(cost)) {
            cost = basePriceToRatioPrice(pricingUOM, cost + record.perAllocateCost, record)
          }
          let tooltipText =
            _.isNumber(value) && price > 0 && price < cost
              ? `Sale price is below cost ($${formatNumber(cost, 2)}/${pricingUOM})`
              : ''
          tooltipText = price === 0 ? 'Sales price is zero' : tooltipText
          const warningClass = tooltipText ? 'warning' : ''

          return (
            <Flex className="v-center">
              <ThemeInputNumber
                className={`no-stepper cart-price ${warningClass} ${record.status === 'SHIPPED' ? 'readonly' : ''}`}
                style={{ textAlign: 'right', minWidth: 60 }}
                step={0.01}
                value={price}
                precision={2}
                onBlur={(e) => this.handleChangeItemPrice(record, e.target.value.replace('$', '') )}
                formatter={(value) => `$${value}`}
                onFocus={(e) => e.target.select()}
                parser={(value) => value.replace('$', '')}
                readOnly={readOnly}
                disabled={record.status === 'SHIPPED' || readOnly} //don't need to addingCartItem flag because wholesaleOrderItem is is empty when order item is created
                addonBefore={'$'}
              />
              <Unit style={{ flex: 1, minWidth: 60 }}>/ {pricingUOM}</Unit>
            </Flex>
          )
        },
      },
      {
        title: 'Billable qty',
        dataIndex: 'catchWeightQty',
        // width: 200,
        align: 'left',
        render: (weight: number, record: any) => {
          const { uom, showBillableQty } = this.getBillableQtyWithUOM(record)
          const billAbleQty = parseFloat(showBillableQty.toString())
          return (
            <Flex className="v-center">
              <div style={{ textAlign: 'right' }}>{mathRoundFun(isNaN(billAbleQty)?0:billAbleQty, 4)}</div>
              <Unit style={{ flex: 'unset' }}>{uom}</Unit>
            </Flex>
          )
        },
      },
      {
        title: 'Total Credit',
        dataIndex: 'salePrice',
        key: 'totalPrice',
        render: (value: number, record: any) => {
          let price = value
          const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
          if (_.isNumber(price)) {
            price = basePriceToRatioPrice(pricingUOM, value, record)
          }
          const { showBillableQty } = this.getBillableQtyWithUOM(record)
          const billAbleQty = parseFloat(showBillableQty.toString())
          const total = price * (isNaN(billAbleQty)?0:billAbleQty)
          return (
            <ThemeInputNumber
              className={`no-stepper ${record.status === 'SHIPPED' ? 'readonly' : ''}`}
              style={{ textAlign: 'right', minWidth: 85 }}
              step={0.01}
              value={total}
              precision={2}
              // onChange={(evt: any) => this.onChangeItem(record.wholesaleOrderItemId, 'salePrice', evt)}
              onFocus={(e) => e.target.select()}
              onBlur={(evt) => this.updatePriceFromTotal(evt, record)}
              formatter={(value) => `$${value}`}
              parser={(value) => value.replace('$', '')}
              readOnly={readOnly}
              disabled={record.status === 'SHIPPED' || readOnly}
            />
          )
        },
      },
      {
        title: 'Reason Type',
        dataIndex: 'reasonType',
        width: 250,
        render: (data: number, record: any, index: number) => {
          return (
            <>
              <Select
                showSearch
                defaultValue={record.reasonType ? record.reasonType : ''}
                style={{ width: 200 }}
                placeholder="Select a type"
                optionFilterProp="children"
                onChange={(evt: any) => this.onChangeItem(record.wholesaleOrderItemId, 'reasonType', evt)}
                onBlur={() => this.handleSave(record)}
                filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              >
                {this.state.reasonTypes.map((item, index) => {
                  return (
                    <Option title={item.name} key={index} value={item.id}>
                      {item.name}
                    </Option>
                  )
                })}
                <Button
                  style={{ borderTop: '1px black solid', fontWeight: 'bold' }}
                  title={`Add new reason type`}
                  key="add"
                  onClick={() => {
                    this.setState({ addNewTypeVisible: true })
                  }}
                >{`+ Add new reason type`}</Button>
              </Select>
            </>
          )
        },
      },
      {
        title: <Tooltip title="If checked, Return Qty will be added back into inventory.">Return to Inventory</Tooltip>,
        dataIndex: 'returnInInventory',
        width: 200,
        render: (value: boolean, record: any) => {
          return (
            <Flex className="v-center">
              <ThemeCheckbox
                className="pas"
                checked={value}
                onChange={(e: any) =>
                  this.onChangeItem(record.wholesaleOrderItemId, 'returnInInventory', e.target.checked)
                }
                disabled={readOnly}
              />
              {record.returnInInventory && (
                <>
                  <ThemeInput
                    value={record.returnInventoryQuantity || 0}
                    style={{ marginRight: 4, marginLeft: 4, width: 60, textAlign: 'right' }}
                    onChange={(evt: any) =>
                      this.onChangeItem(record.wholesaleOrderItemId, 'returnInventoryQuantity', evt.target.value)
                    }
                    onBlur={() => this.handleSave(record)}
                    disabled={readOnly}
                  />
                  <UomSelect
                    disabled={readOnly}
                    defaultUom={record.returnInventoryUOM || record.UOM || record.inventoryUOM}
                    orderItem={record}
                    handlerChangeUom={(orderItemId, value) => {
                      this.setState({ currentUOM: 'returnInventoryUOM' }, () => {
                        this.onChangeUom(orderItemId, value)
                      })
                    }}
                    type={1}
                    from="creditMemo"
                  />
                </>
              )}
            </Flex>
          )
        },
      },
      {
        title: 'Reason Description',
        dataIndex: 'returnReason',
        width: 200,
        // sorter: (a: any, b: any) => onSortString('status', a, b),
        render: (value: string, record: any) => {
          return (
            <Flex className="v-center space-between">
              <ThemeInput
                disabled={readOnly}
                style={{ width: '150px' }}
                value={value}
                onChange={(evt: any) => {
                  this.onChangeItem(record.wholesaleOrderItemId, 'returnReason', evt.target.value)
                }}
                onBlur={() => this.handleSave(record)}
              />
              <Popconfirm
                title="Permanently delete this one related record?"
                okText="Delete"
                onConfirm={() => this.props.deleteOrderAdjustement(record.adjustmentId)}
              >
                {record.adjustmentId && <ThemeIcon type="delete" className="delete-btn" />}
              </Popconfirm>
            </Flex>
          )
        },
      },
    ]

    let totalReturn = 0
    let totalQty = 0
    let totalUnit = 0
    let totalReInventory = 0
    let totalSubtotal = 0
    let totalTax = 0

    if (dataSource.length > 0) {
      dataSource.forEach((record, index) => {
        const { showBillableQty, uom } = this.getBillableQtyWithUOM(record)
        const extended = Number(record.salePrice ? record.salePrice : 0)
        const quantity = ratioQtyToInventoryQty(record.returnUom, record.returnQuantity, record)
        const taxRate = Number(record.taxRate ? record.taxRate : 0)
        const taxRateRounded = mathRoundFun(taxRate / 100, 6)
        const taxEnabled = record.taxEnabled
        let showValue = quantity ? true : false
        let subtotal = 0
        let tax = 0
        let total = 0

        if (showValue) {
          subtotal = showBillableQty * basePriceToRatioPrice(uom, extended, record)
          tax = taxEnabled ? subtotal * taxRateRounded: 0
          total = subtotal + tax
        }

        totalQty += Number(showBillableQty)
        totalUnit += typeof record.returnQuantity !== 'undefined' ? parseFloat(record.returnQuantity) : 0
        totalReInventory += typeof record.returnInventoryQuantity !== 'undefined' ? parseFloat(record.returnInventoryQuantity) : 0
        totalSubtotal += subtotal
        totalTax += tax
        totalReturn += total
      })
    }

    const Totals = () => {
      let printSetting = null
      if(this.props.printSetting) {
        printSetting = JSON.parse(this.props.printSetting)
      }
      if (totalReturn == 0) return null
      return (
        <Flex style={{ paddingRight: 30 }}>
          {/* <div>
            <CMLabel>Total credited units</CMLabel>
            <CMValue>{showTable ? totalUnit : 0}</CMValue>
          </div>
          <div style={{ margin: '0 20px' }}>
            <CMLabel>Total billable units</CMLabel>
            <CMValue>{formatNumber(showTable ? Math.abs(totalQty) : 0, 2)}</CMValue>
          </div> */}
          <div style={{ margin: '0 20px' }}>
            <CMLabel>Total re-inventory units</CMLabel>
            <CMValue>{mathRoundFun(totalReInventory, 2) || 0}</CMValue>
          </div>
          <div  style={{ margin: '0 20px' }}>
            <CMLabel>Subtotal</CMLabel>
            <CMValue>${formatNumber(showTable ? Math.abs(totalSubtotal) : 0, 2)}</CMValue>
          </div>
          <div  style={{ margin: '0 20px' }}>
            <CMLabel>Tax</CMLabel>
            <CMValue>${formatNumber(showTable ? Math.abs(totalTax) : 0, 2)}</CMValue>
          </div>
          <div>
            <CMLabel>Total credit</CMLabel>
            <CMValue>${formatNumber(showTable ? Math.abs(totalReturn) : 0, 2)}</CMValue>
          </div>
          {(!printSetting || printSetting.creditmemo_enabled !== false) && (
            <div style={{ marginTop: '20px', marginLeft: '50px' }}>
              <Button
                onClick={() => {
                  this.setState({ printMemoVisible: true })
                }}
                style={{ height: '36px', lineHeight: '36px', color: '#1C6E31' }}
              >
                Print credit memo
              </Button>
            </div>
          )}
        </Flex>
      )
    }

    return (
      <>
        {!showTable && currentOrder.wholesaleOrderStatus !== 'CANCEL' ? (
          <Flex className="space-between">
            <ThemeButton onClick={this.changeTableStatus}>Create credit memo</ThemeButton>
            {showTable && <Totals />}
          </Flex>
        ) : (
          ''
        )}

        {showTable ? (
          <div style={{ paddingRight: 20 }}>
            {currentOrder && (
              <Flex className="space-between" style={{ marginBottom: 20 }}>
                <div style={{ fontSize: 16, color: gray01 }}>
                  Credit Memo No. <span>#{currentOrder.wholesaleOrderId}C</span>
                </div>
                {<Totals />}
              </Flex>
            )}

            <CartTableWrapper>
              <StyleTable
                pagination={false}
                rowClassName={() => 'row'}
                columns={columns}
                dataSource={dataSource}
                handleSave={this.handleSave}
                rowKey="id"
                scroll={{ x: 2000 }}
              />
              {!readOnly && currentOrder.wholesaleOrderStatus !== 'CANCEL' && (
                <ThemeButton
                  onClick={() => this.setState({ selectShippedProductModalVisible: true })}
                  style={{ marginTop: 24 }}
                >
                  Add Item
                </ThemeButton>
              )}
            </CartTableWrapper>
          </div>
        ) : null}
        {selectShippedProductModalVisible && (
          <SelectShippedProductModal
            orderId={orderId}
            adjustmentList={this.state.dataSource}
            sellerSetting={this.props.sellerSetting}
            onCancel={() => this.setState({ selectShippedProductModalVisible: false })}
            createOrderAdjustment={(row: any) =>
              this.props.createOrderAdjustment({
                orderId: this.props.orderId,
                requestData: this._getAdjustmentObject(row),
              })
            }
          />
        )}
        <AddNewTypeModal
          visible={this.state.addNewTypeVisible}
          addNewType={this.insertNewType}
          onCancel={() => this.setState({ addNewTypeVisible: false })}
        />
        <Modal
          width={1200}
          style={{ height: '1000px' }}
          visible={this.state.printMemoVisible}
          onCancel={() => {
            this.setState({ printMemoVisible: false })
          }}
          className="uploadModel"
          footer={<></>}
        >
          <PrintModalHeader>
            <Tooltip title="" placement="bottom">
              <ThemeButton
                size="large"
                className="print-pick-sheet"
                onClick={() => printWindow('memoContent', () => {}, '')}
              >
                <Icon type="printer" theme="filled" />
                Print Credit Memo
              </ThemeButton>
            </Tooltip>
            <div className="clearfix" />
          </PrintModalHeader>
          <PrintMemo
            getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
            catchWeightValues={this.props.catchWeightValues}
            orderItems={this.getPrintAllDeliveryItems(this.props)}
            currentOrder={this.props.currentOrder}
            companyName={this.props.companyName}
            company={this.props.company}
            logo={this.props.logo}
            printSetting={this.props.printSetting}
            sellerSetting={this.props.sellerSetting}
            fulfillmentOptionType={this.props.fulfillmentOptionType}
            reasonArr={dataSource}
            type={'creditmemo'}
            paymentTerms={}
            reasonTypes={this.state.reasonTypes}
          />
        </Modal>
      </>
    )
  }
}

const StyleTable = styled(Table)({
  '.row:hover .delete-btn': {
    display: 'block',
  },

  '.delete-btn': {
    display: 'none',
  },
})

const mapStateToProps = (state: GlobalState) => state.orders
export default connect(OrdersModule)(mapStateToProps)(MemoTotalTable)
