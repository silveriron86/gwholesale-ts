import { Button, Input, Modal, notification } from 'antd'
import React, { useEffect, useState } from 'react'
import { Http } from '~/common'
import { ThemeButton } from '~/modules/customers/customers.style'
import { SettingService } from '~/modules/setting/setting.service'
interface IProps {
  visible: boolean
  onCancel: () => void
  addNewType: (newType: any) => void
}

const AddNewTypeModal = ({ visible, onCancel, addNewType }: IProps): JSX.Element => {
  const [newTypeValue, setNewTypeValue] = useState<string>('')

  const onBack = () => {
    onCancel()
  }

  const saveNewType = () => {
    const http = new Http()
    const newSettingService = new SettingService(http)
    console.log(newTypeValue)
    newSettingService
      .saveCompanyProductType({
        name: newTypeValue,
        type: 'reasonType',
      })
      .subscribe({
        next(res: any) {
          console.log(res)
          addNewType({
            id: res.body.data.id,
            name: res.body.data.name,
          })
        },
        error(err: any) {
          notification.error({
            message: 'ERROR',
            description: 'Server error',
            onClose: () => {},
          })
        },
        complete() {},
      })

    onBack()
  }

  return (
    <>
      <Modal
        width={1200}
        style={{ height: '1000px' }}
        visible={visible}
        onCancel={onBack}
        footer={<></>}
        className="uploadModel"
      >
        <div style={{ position: 'relative' }}>
          Add New Reason Type
          <Input
            style={{ margin: '20px', width: '80%' }}
            placeholder={'new Type'}
            onChange={(e) => setNewTypeValue(e.target.value)}
          ></Input>
          <ThemeButton style={{ position: 'absolute', right: '20px', bottom: '20px' }} onClick={() => saveNewType()}>
            Save
          </ThemeButton>
        </div>
      </Modal>
    </>
  )
}

export default AddNewTypeModal
