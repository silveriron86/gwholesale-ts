import { Modal } from 'antd'
import React, { useEffect, useState } from 'react'
interface IProps {
  visible: boolean
  onCancel: () => void
  propData: any
}

const PrintMemo = ({ visible, onCancel, propData }: IProps): JSX.Element => {
  const onBack = () => {
    onCancel()
  }

  return (
    <>
      <Modal width={1200} style={{ height: '1000px' }} visible={visible} onCancel={onBack} className="uploadModel">
        add new type
      </Modal>
    </>
  )
}

export default PrintMemo
