import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import SalesOverview from '../widgets/sales-overview'
import DocumentsContainer from '../../accounts/documents/documents-container'
import { CustomersInfoBody } from '../../components/customers-detail/customers-detail.style'
import { RouteComponentProps } from 'react-router'
import { notification } from 'antd'
import moment from 'moment'

type SalesDocumentsProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
    onlyContent?: boolean
  }

export class SalesDocuments extends React.PureComponent<SalesDocumentsProps> {
  state = {
    from: moment().subtract(30, 'days'),
    to: moment(),
    curPage: 0,
    pageSize: 10,
    search: ''
  }
  componentDidMount() {
    const orderId = this.props.match.params.orderId
    // this.props.resetLoading()
    // this.props.getOrderDetail(orderId)
    this.props.resetDocuments()
    this.getSalesOrderDocument()
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.loadingCurrentOrder === true && nextProps.loadingCurrentOrder === false) {
      // this.props.getAddresses()
    }
  }

  getSalesOrderDocument = () => {
    const orderId = this.props.match.params.orderId
    const searchObj = {
      // from: this.state.from.format('MM/DD/YYYY'),
      // to: this.state.to.format('MM/DD/YYYY'),
      search: this.state.search,
      // curPage: this.state.curPage,
      // pageSize: this.state.pageSize,
    }
    this.props.setDocumentSearchProps(searchObj)
    this.props.getDocuments({ ...searchObj, orderId: orderId })
  }

  onSave = (data: any) => {
    if (data.id) {
      this.props.updateDocument(data)
    } else {
      this.props.createDocument(data)
    }
  }


  onChange = (type: string, data: any) => {
    if (type == 'search') {
      this.setState({ search: data }, this.getSalesOrderDocument)
    }
  }

  render() {
    const { currentOrder, documents, onlyContent, documentsLoading } = this.props
    const { pageSize } = this.state
    if (!currentOrder) {
      if (onlyContent) return null
      return <PageLayout currentTopMenu={'menu-Customers-Sales Orders'} />
    }

    const content = (
      <>
        <SalesOverview onlyContent={onlyContent} orderId={this.props.match.params.orderId} order={currentOrder} />
        <CustomersInfoBody style={{ marginLeft: onlyContent ? 0 : 19 }}>
          <DocumentsContainer
            loading={documentsLoading}
            isSales={true}
            getDocuments={this.getSalesOrderDocument}
            documents={documents}
            onSave={this.onSave}
            buttonStatus={currentOrder.wholesaleOrderStatus == 'CANCEL'}
            pageSize={pageSize}
            theme={this.props.theme}
            onlyContent={onlyContent} />
        </CustomersInfoBody>
      </>
    )

    if (onlyContent) {
      return content;
    }

    return (
      <PageLayout currentTopMenu={'menu-Customers-Sales Orders'}>
        {content}
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(SalesDocuments))
