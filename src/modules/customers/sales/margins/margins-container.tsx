import React from 'react'
import { withTheme } from 'emotion-theming'
import { CustomerDetailsWrapper as DetailsWrapper } from '../../customers.style'
import { Row, Col } from 'antd'
import { Marker, MarkerText } from './_styles'
import MarginsTotalTable from './margins-total-table'
import { SalesContainer } from '../_style'
import { OrderDetail, OrderItem } from '~/schema'
import { gray01 } from '~/common'

interface MarginsContainerProps {
  orderItems: OrderItem[]
  onUpdateItem: Function
  onlyContent?: boolean
  orderItemsImpacts: any[]
  currentOrder: OrderDetail
}

export class MarginsContainer extends React.PureComponent<MarginsContainerProps> {
  render() {
    const { orderItems, onlyContent, orderItemsImpacts, currentOrder } = this.props
    return (
      <SalesContainer style={{ padding: onlyContent ? 0 : '0 18px' }}>
        <DetailsWrapper style={{ padding: onlyContent ? '5px 0 20px' : 20, borderTopWidth: onlyContent ? 0 : 5 }}>
          <MarginsTotalTable onlyContent={onlyContent} editable={false} orderItems={orderItems} currentOrder={currentOrder} orderItemsImpacts={orderItemsImpacts} handleSave={this.props.onUpdateItem} />
        </DetailsWrapper>
      </SalesContainer>
    )
  }
}

export default withTheme(MarginsContainer)
