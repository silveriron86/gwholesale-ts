import React from 'react'
import { PureWrapper as Wrapper, WrapperTitle } from '../../accounts/addresses/addresses.style'
import { ml0 } from '~/modules/customers/customers.style'
import { MarginModel } from '../../customers.model'
import { cloneDeep } from 'lodash'
import { formatNumber } from '~/common/utils'
import EditableTable from '~/components/Table/editable-table'

interface MarginsTableProps {
  columns: Array<MarginModel>
}

export class MarginsTable extends React.PureComponent<MarginsTableProps> {
  handleSave = (row: MarginModel) => {
    const newData = [...this.props.columns]
    const index = newData.findIndex((item) => row.id === item.id)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
  }

  render() {
    const listItems: MarginModel[] = cloneDeep(this.props.columns)
    const columns = [
      {
        title: 'REVENUE',
        dataIndex: 'revenue',
        editable: true,
        edit_width: 50,
        render: (data: number) => <>${formatNumber(data, 2)}</>,
      },
      {
        title: 'COST',
        dataIndex: 'cost',
        render: (data: number) => <>${formatNumber(data, 2)}</>,
      },
      {
        title: 'MARKUP %',
        dataIndex: 'markup',
        render: (data: number) => <>{formatNumber(data ? data : 0, 1)}%</>,
      },
      {
        title: 'GROSS MARGIN',
        dataIndex: 'gross_margin',
        render: (data: number) => <>{formatNumber(data, 1)}%</>,
      },
      {
        title: 'WEIGHTED GROSS MARGIN %',
        dataIndex: 'weighted_margin',
        render: (data: number) => <>{formatNumber(data, 1)}%</>,
      },
    ]

    return (
      <Wrapper style={ml0}>
        <WrapperTitle>Margins</WrapperTitle>
        <EditableTable columns={columns} dataSource={listItems} handleSave={this.handleSave} />
      </Wrapper>
    )
  }
}

export default MarginsTable
