import * as React from 'react'
import { ColWrapper, ColTriangle, ColMain, ProgressCircle, Filled, CheckIcon } from '../_style'

interface ProgressColProps {
  label: string
  status: number // 1: normal, 2: current, 3: completed
  statusName: string // CANCEL ORDER UI
}

export class ProgressCol extends React.PureComponent<ProgressColProps> {
  render() {
    const { label, status,statusName } = this.props
    return (
      <ColWrapper status={status} statusName={statusName}>
        <ColMain>
          <ProgressCircle status={status} statusName={statusName}>
            {
              statusName ?
                <Filled status={status} statusName={statusName}>
                {status === 3 && <CheckIcon type="close" />}
                </Filled>
              :
              status >= 2 && 
                <Filled status={status} statusName={statusName}>
                  {status === 3 && <CheckIcon type="check" />}
                </Filled>
            }
          </ProgressCircle>
          {label}
        </ColMain>
        {status === 2 && <ColTriangle />}
      </ColWrapper>
    )
  }
}

export default ProgressCol
