import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import jQuery from 'jquery'
import {
  ProgressBar,
  CancelProgressBar,
  accountLayoutStyle,
  AccountLabel,
  DetailLabel,
  DetailValue,
  DetailsSection,
  InfoHeader,
} from '../_style'
import { Row, Col, Layout, Select, Icon, Popconfirm } from 'antd'
import ProgressCol from './progress-col'
import moment from 'moment'
import {
  ThemeButton,
  ThemeIcon,
  ThemeInput,
  ThemeLink,
  ErrorAlert,
  Flex1,
  ThemeTransSelect,
  ThemeModal,
} from '../../customers.style'
import { OrderDetail, MainAddress } from '~/schema'
import { formatNumber, formatAddress, padString } from '~/common/utils'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { AccoutName, SalesDatePicker } from '~/modules/vendors/purchase-orders/_style'
import { onPopoverOpenWithButtons } from '~/common/jqueryHelper'

const { Option } = Select
const calendarIcon = <ThemeIcon type="calendar" />
type SalesOverviewProps = OrdersDispatchProps &
  OrdersStateProps & {
    orderId: string
    errorMsg?: string
    order: OrderDetail
    onlyContent?: boolean
  }

export class SalesOverview extends React.PureComponent<SalesOverviewProps> {
  state: any
  constructor(props: SalesOverviewProps) {
    super(props)
    const shippingAddress = props.currentOrder!.shippingAddress
    this.state = {
      changeAddressModalVisible: false,
      accountName: props.order.wholesaleClient.clientCompany.companyName,
      accountId: props.order.wholesaleClient.clientId,
      shippingAddress: shippingAddress ? shippingAddress.wholesaleAddressId : '',
      disableAccountName: false,
      confirmChangeAccountName: false,
      confirmed: false,
    }
  }

  handleChangeAddressVisibleChange = (visible: boolean) => {
    this.setState({
      changeAddressModalVisible: visible,
    })
  }

  onChangeAddressModalClickRow = () => {
    this.setState({
      changeAddressModalVisible: false,
    })
  }

  triggerAccountNameChange = () => {
    this.setState({ confirmChangeAccountName: true })
  }

  confirmChangeAccount = () => {
    this.setState({ confirmChangeAccountName: false, confirmed: true })
  }

  cancelChangeAccount = () => {
    this.setState({ confirmChangeAccountName: false })
  }

  handleChangeAccount = (value: any) => {
    const { order } = this.props
    if (
      order.wholesaleOrderStatus == 'PICKED' ||
      order.wholesaleOrderStatus == 'SHIPPED' ||
      order.wholesaleOrderStatus == 'CANCEL'
    ) {
      this.setState({ disableAccountName: true })
    } else {
      if (this.state.confirmed) {
        this.setState({ accountName: value })
      } else if (/^[a-zA-Z0-9- ]*$/.test(value)) {
        jQuery('.hidden-popover-trigger').trigger('click')
      }
    }
  }

  handleSelectAccount = (value: any) => {
    this.setState(
      {
        accountId: value,
      },
      () => {
        const { order } = this.props
        this.props.updateOrderInfo({
          wholesaleOrderId: order.wholesaleOrderId,
          deliveryDate: moment.utc(order.deliveryDate).format('MM/DD/YYYY'),
          wholesaleCustomerClientId: value,
          status: order.wholesaleOrderStatus,
        })
      },
    )
  }

  componentDidMount() {
    // this.props.getOrderItemsById(this.props.orderId)
    // this.props.getCustomers()
    /*
    onPopoverOpenWithButtons('hidden-popover-trigger')

    // Tab Order
    setTimeout(() => {
      jQuery('.ant-select-search__field').focus()
    }, 1000)
    let index = window.localStorage.getItem('SALES-CART-TAB-INDEX')
    if (!index) {
      window.localStorage.setItem('SALES-CART-TAB-INDEX', '0')
    } else {
    }

    function setTab(tabIndex: number, type = 'forward') {
      const cards = jQuery('.sales-cart-input')
      if (cards.length === 0) {
        return
      }
      const buttonsCount = jQuery('button.sales-cart-input').length
      jQuery('.sales-cart-input').map((index: number, el: any) => {
        if (jQuery(el).hasClass('ant-btn')) {
          jQuery(el).removeClass('focused-btn')
        } else {
          jQuery(el).addClass('black-bottom')
        }
      })
      console.log('here check', tabIndex, buttonsCount + 2)
      if (tabIndex === buttonsCount + 2) {
        const editableEls = jQuery('.sales-cart-tabs-area .tab-able')
        if (editableEls.length > 0) {
          setTimeout(() => {
            jQuery(editableEls[0]).trigger('focus')
            if (jQuery(editableEls[0]).hasClass('editable-cell-value-wrap')) {
              if (jQuery(editableEls[0])[0].nodeName != 'BUTTON') {
                jQuery(editableEls[0]).trigger('click')
              }
            }
          }, 50)
        } else {
          tabIndex = 0
          setTimeout(() => {
            jQuery('.ant-input')
              .first()
              .focus()
          }, 50)
        }
      } else {
        let currentCard = jQuery(cards[tabIndex])
        if (currentCard.hasClass('ant-btn')) {
          currentCard.addClass('focused-btn')
        } else if ((type == 'back' && tabIndex > 0) || type == 'forward') {
          currentCard.removeClass('black-bottom')
        }
      }
      window.localStorage.setItem('SALES-CART-TAB-INDEX', tabIndex.toString())
    }

    jQuery('.account-name-first input')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9) {
          if (e.shiftKey) {
            const editableEls = jQuery('.sales-cart-tabs-area .tab-able')
            if (editableEls.length > 0) {
              window.localStorage.setItem('SALES-CART-TAB-INDEX', '3')
              setTimeout(() => {
                jQuery(editableEls[editableEls.length - 1]).trigger('focus')
                if (jQuery(editableEls[editableEls.length - 1]).hasClass('editable-cell-value-wrap')) {
                  jQuery(editableEls[editableEls.length - 1]).trigger('click')
                }
              }, 50)
            } else {
              setTimeout(() => {
                jQuery('.ant-input')
                  .first()
                  .focus()
              }, 50)
            }
          }
        }
      })

    jQuery('.sales-cart-input input')
      .unbind('focus')
      .bind('focus', (e: any) => {
        jQuery(e.currentTarget)
          .parent()
          .parent()
          .removeClass('black-bottom')
        window.localStorage.setItem('SALES-CART-TAB-INDEX', '0')
        setTab(0)
      })

    jQuery('.sales-cart-input.ant-select')
      .unbind('click')
      .bind('click', (e: any) => {
        jQuery(e.currentTarget)
          .parent()
          .parent()
          .removeClass('black-bottom')
        window.localStorage.setItem('SALES-CART-TAB-INDEX', '1')
        setTab(1)
      })
    jQuery('button.sales-cart-input')
      .unbind('focus')
      .bind('focus', (e: any) => {
        let oTabIndex: string | null = window.localStorage.getItem('SALES-CART-TAB-INDEX')
        let index = 0
        const buttonCount = jQuery('button.sales-cart-input').length
        if (oTabIndex)
          index = parseInt(oTabIndex, 10)
        if (jQuery(e.currentTarget).hasClass('add-item')) {
          index = 2
        } else if (jQuery(e.currentTarget).hasClass('one-off-item')) {
          index = buttonCount
        } else if (jQuery(e.currentTarget).hasClass('duplicate-order')) {
          index = buttonCount + 1
        }
        window.localStorage.setItem('SALES-CART-TAB-INDEX', `${index}`)
        setTab(index)
      })
    jQuery('.sales-cart-tabs-area')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        let oTabIndex: string | null = window.localStorage.getItem('SALES-CART-TAB-INDEX')
        let tabIndex: number = -1
        if (oTabIndex) {
          tabIndex = parseInt(oTabIndex, 10)
        }
        if (tabIndex === -1 || tabIndex < 5) {
          if (e.keyCode === 9) {
            // Tab
            let type = 'forward'
            if (e.shiftKey) {
              tabIndex--
              tabIndex = tabIndex < 0 ? 0 : tabIndex
              type = 'back'
            } else {
              tabIndex++
            }
            setTab(tabIndex, type)
          } else if (e.keyCode === 13) {
            // Enter
          }
        }
      })
      */
  }

  componentWillReceiveProps(nextProps: SalesOverviewProps, nextState: SalesOverviewProps) {
    if (nextProps.currentOrder) {
      this.setState({
        accountName: nextProps.currentOrder.wholesaleClient.clientCompany.companyName,
        accountId: nextProps.currentOrder.wholesaleClient.clientId,
      })

    }
    if (nextProps.currentOrder!.shippingAddress) {
      this.setState({
        shippingAddress: nextProps.currentOrder!.shippingAddress.wholesaleAddressId,
      })
    } else {
      this.setState({
        shippingAddress: '',
      })
    }
  }

  onDateChange = (date: any, dateString: string) => {
    this.props.updateOrderInfo({
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      deliveryDate: dateString,
    })
  }

  onChangeAdress = (addressId) => {
    this.props.updateOrderInfo({
      wholesaleOrderId: this.props.currentOrder.wholesaleOrderId,
      shippingAddressId: addressId,
    })
  }

  onSearch = (searchStr: string) => {
    const { customers, order } = this.props
    if (
      order.wholesaleOrderStatus == 'PICKED' ||
      order.wholesaleOrderStatus == 'SHIPPED' ||
      order.wholesaleOrderStatus == 'CANCEL'
    ) {
      return
    }
    // tslint:disable-next-line:prefer-const
    let accountNames: any[] = []
    if (customers && customers.length > 0 && searchStr) {
      customers.filter((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      accountNames: accountNames,
    })
  }

  openQboLink = () => {
    const { order } = this.props
    window.open(`https://qbo.intuit.com/app/invoice?txnId=${order.qboId}`)
  }

  handleSyncQBOOrder = () => {
    this.props.resetLoading()
    this.props.syncQBOOrder(this.props.orderId)
  }

  render() {
    const { errorMsg, order, addresses, currentOrder, loading, onlyContent } = this.props
    if (onlyContent) {
      return null;
    }

    const { accountNames, accountName, accountId, confirmChangeAccountName } = this.state
    const orderStatus = order.wholesaleOrderStatus === 'NEW' || order.wholesaleOrderStatus === 'PLACED' ? 2 : 3
    const pickingStatus =
      order.wholesaleOrderStatus === 'NEW' || order.wholesaleOrderStatus === 'PLACED'
        ? 1
        : order.wholesaleOrderStatus === 'PICKING'
          ? 2
          : 3
    let shippedStatus =
      order.wholesaleOrderStatus === 'NEW' || order.wholesaleOrderStatus === 'PLACED'
        ? 1
        : order.wholesaleOrderStatus === 'PICKING'
          ? 1
          : order.wholesaleOrderStatus === 'SHIPPED'
            ? 3
            : 2
    const invoicedStatus = order.wholesaleOrderStatus === 'INVOICED' ? 3 : 1
    const cancelStatus = order.wholesaleOrderStatus === 'CANCEL' ? 3 : 0
    if (invoicedStatus === 3) {
      shippedStatus = 3
    }

    const shippingAddresses: Array<any> = []
    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        shippingAddresses.push(address)
      }
    })
    const addressIds = shippingAddresses.map((el) => el.wholesaleAddressId)
    return (
      <InfoHeader>
        {currentOrder.wholesaleOrderStatus != 'CANCEL' ? (
          <Row gutter={[20, 0]}>
            <Col md={16}>
              <ProgressBar>
                <Row>
                  <Col md={8}>
                    <ProgressCol label="New Order" status={orderStatus} />
                  </Col>
                  <Col md={8}>
                    <ProgressCol label="Picking" status={pickingStatus} />
                  </Col>
                  <Col md={8}>
                    <ProgressCol label="Shipping" status={shippedStatus} />
                  </Col>
                  {/* <Col md={6}>
                  <ProgressCol label="Invoiced" status={invoicedStatus} />
                </Col> */}
                </Row>
              </ProgressBar>
            </Col>
            <Col md={8}>
              <Row gutter={[20, 0]}>
                <Col lg={12} md={24}>
                  {
                    currentOrder!.parentOrder == null &&
                    <ThemeButton
                      shape="round"
                      onClick={() => this.handleSyncQBOOrder()}
                      style={{ height: '37px', lineHeight: '37px', width: '100%' }}
                      loading={loading}
                    >
                      <Icon type="cloud" theme="filled" />
                      Sync to QBO
                    </ThemeButton>
                  }

                </Col>
                <Col lg={12} md={24}>
                  {/* <a href={`https://qbo.intuit.com/app/invoice?txnId=${order.qboId}`} target="_blank">
                  <QBOImage src={qboImage} style={{filter: typeof order.qboId !== 'undefined' && order.qboId !== null ? '' : 'grayscale(100%)'}}/>
                </a> */}
                  {
                    currentOrder!.parentOrder == null &&
                    <ThemeButton
                      shape="round"
                      onClick={() => this.openQboLink()}
                      style={{ height: '37px', lineHeight: '37px', width: '100%' }}
                      disabled={order.qboId == null}
                    >
                      <Icon type="cloud" theme="filled" />
                    View In QBO
                  </ThemeButton>
                  }

                </Col>
              </Row>
            </Col>
          </Row>
        ) : (
            <Row gutter={[20, 0]}>
              <Col md={16}>
                <CancelProgressBar>
                  <Row>
                    <Col md={24}>
                      <ProgressCol
                        label="Canceled"
                        status={cancelStatus}
                        statusName={currentOrder.wholesaleOrderStatus}
                      />
                    </Col>
                  </Row>
                </CancelProgressBar>
              </Col>
            </Row>
          )}

        <Layout style={accountLayoutStyle}>
          <Row>
            <Col md={errorMsg ? 14 : 19} style={{ paddingRight: 20 }}>
              <AccountLabel>
                Account Name
                <ThemeLink to={`/customer/${accountId}/orders`} style={{ marginLeft: 5 }}>
                  <ThemeIcon type="link" theme="filled" />
                </ThemeLink>
              </AccountLabel>

              <AccoutName
                className="bold-blink account-name-first"
                disabled={cancelStatus > 0}
                value={accountName}
                dataSource={accountNames}
                onSearch={this.onSearch}
                onChange={this.handleChangeAccount}
                onSelect={this.handleSelectAccount}
              />

              <Popconfirm
                title="Are you sure you want to change the account associated with this Sales Order Number?"
                visible={confirmChangeAccountName}
                onConfirm={this.confirmChangeAccount}
                onCancel={this.cancelChangeAccount}
                placement="bottom"
                okText="Yes"
                cancelText="No"
              >
                <button
                  className="hidden-popover-trigger"
                  style={{ visibility: 'hidden', width: '80%' }}
                  onClick={this.triggerAccountNameChange}
                />
              </Popconfirm>
            </Col>
            <Col md={5}>
              <AccountLabel>Account Number</AccountLabel>
              <ThemeInput
                style={{ width: '100%' }}
                value={order ? padString(order.wholesaleClient.clientId) : ''}
                className="bold-blink"
                disabled={cancelStatus > 0}
              />
            </Col>
            {errorMsg && (
              <Col md={5}>
                <AccountLabel>&nbsp;</AccountLabel>
                <Flex1 style={{ marginLeft: 18 }}>
                  <ErrorAlert message={errorMsg} type="warning" showIcon={false} />
                </Flex1>
              </Col>
            )}
          </Row>
          <ThemeModal
            title="Warning"
            visible={this.state.disableAccountName}
            onCancel={() => {
              this.setState({ disableAccountName: false })
            }}
            cancelText="Close"
            okButtonProps={{ style: { display: 'none' } }}
          >
            Order has already been placed, cannot change name. Please create new Sales Order with correct account and
            cancel this sales order
          </ThemeModal>
        </Layout>

        <DetailsSection className="sales-cart-tabs-area">
          <Row>
            <Col md={3}>
              <DetailLabel>DELIVERY DATE</DetailLabel>
              <SalesDatePicker
                placeholder={'MM/DD/YYYY'}
                format={'MM/DD/YYYY'}
                disabled={cancelStatus > 0}
                suffixIcon={calendarIcon}
                value={order && order.deliveryDate ? moment.utc(order.deliveryDate) : moment()}
                onChange={this.onDateChange}
                allowClear={false}
                className="sales-cart-input black-bottom bold-blink"
              />
            </Col>
            <Col md={5}>
              <div style={{ padding: '0 25px' }}>
                <DetailLabel>SHIPPING ADDRESS</DetailLabel>
                {currentOrder.pickup ? (
                  <DetailValue style={{ borderBottom: '1px solid black' }}>PICK UP</DetailValue>
                ) : (
                    <ThemeTransSelect
                      className="sales-cart-input black-bottom bold-blink"
                      placeholder="--"
                      disabled={cancelStatus > 0}
                      value={
                        Array.isArray(shippingAddresses) &&
                          shippingAddresses.length > 0 &&
                          addressIds.indexOf(this.state.shippingAddress) > -1
                          ? this.state.shippingAddress
                          : ''
                      }
                      onChange={this.onChangeAdress}
                    >
                      {Array.isArray(shippingAddresses)
                        ? shippingAddresses.map((item: any) => (
                          <Option key={`address-${item.wholesaleAddressId}`} value={item.wholesaleAddressId}>
                            {formatAddress(item.address, true)}
                          </Option>
                        ))
                        : null}
                    </ThemeTransSelect>
                  )}
              </div>
            </Col>
            <Col md={2}>
              <DetailLabel>INVOICE DUE</DetailLabel>
              <DetailValue>
                {order && order.invoiceDueDate ? moment(order.invoiceDueDate).format('MM/DD/YY') : '--'}
              </DetailValue>
            </Col>
            <Col md={2}>
              <DetailLabel>OPEN BALANCE</DetailLabel>
              <DetailValue>
                {order && order.wholesaleClient ? `${formatNumber(order.wholesaleClient.openBalance, 2)}` : ''}
              </DetailValue>
            </Col>
            <Col md={3}>
              <DetailLabel active={true}>OVERDUE BALANCE</DetailLabel>
              <DetailValue active={true}>
                {order && order.wholesaleClient ? `${formatNumber(order.wholesaleClient.overdue, 2)}` : ''}
              </DetailValue>
            </Col>
            <Col md={3}>
              <DetailLabel>SALES ORDER ESTIMATE</DetailLabel>
              <DetailValue>{formatNumber(order.totalPrice, 2)}</DetailValue>
            </Col>
            <Col md={3}>
              <DetailLabel>SALES ORDER ID</DetailLabel>
              <DetailValue>{order && order.wholesaleOrderId}</DetailValue>
            </Col>
            <Col md={3}>
              <DetailLabel>CREDIT MEMO</DetailLabel>
              <DetailValue>
                {order && order.wholesaleClient.creditTerm ? order.wholesaleClient.creditTerm : '--'}
              </DetailValue>
            </Col>
          </Row>
        </DetailsSection>
      </InfoHeader>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(SalesOverview))
