import React, { CSSProperties, FC, useEffect, useState } from 'react'
import styled from '@emotion/styled'
import { AutoComplete, Button, DatePicker, Dropdown, Icon, Menu, Modal, Popover, Row, Select, Table, Tooltip } from 'antd'
import { Flex, ThemeButton, ThemeInputNumber, ThemeModal, ThemeSelect, ThemeSpin, ThemeTable } from '~/modules/customers/customers.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, formatNumber, getOrderPrefix, precisionAdd, precisionMultiply, printWindowAsync } from '~/common/utils'
import moment from 'moment'
import _ from 'lodash'
import { containerStatus } from '~/modules/customers/nav-sales/order-detail/tabs/container/enum'
import AutoAssignContainerModal from '~/modules/orders/components/order-auto-assign-containers'
import WrappedContainerDetailModal from '~/modules/customers/nav-sales/order-detail/tabs/container/container-detail-modal'
import WrappedContainerDetailSettingModal from '~/modules/customers/nav-sales/order-detail/tabs/container/container-setting-detail-modal'
import WrappedContainerCustomFieldsModal from '~/modules/customers/nav-sales/order-detail/tabs/container/container-custom-modal'
import PrintPickSheet from '~/modules/customers/sales/cart/print/print-pick-sheet'
import PrintInvoice from '~/modules/customers/sales/cart/print/print-invoice'
import PrintBillOfLading from '~/modules/customers/sales/cart/print/print-bill-lading'

import { PrintModalHeader } from '~/modules/customers/sales/_style'
import { WrappedSendEmail } from '~/modules/customers/sales/cart/cart-container'
import sendEmail from '~/modules/customers/sales/cart/print/sendEmail'
import { OrdersModule } from '~/modules/orders/orders.module'
import { connect } from 'redux-epics-decorator'
import { Link } from 'react-router-dom'
import CreateNewContainerModal from '~/modules/customers/nav-sales/order-detail/tabs/container/createNewContainerModal'

const dateFormat = 'YYYY-MM-DD'
const { RangePicker } = DatePicker
const { Option } = Select

const presets = [
  {
    key: 0,
    label: 'Today',
    from: moment().startOf('day').valueOf(),
    to: moment().endOf('day').valueOf(),
  },
  {
    key: 1,
    label: 'Tomorrow',
    from: moment().add(1, 'days').startOf('day').valueOf(),
    to: moment().add(1, 'days').endOf('day').valueOf(),
  },
  {
    key: 2,
    label: 'Today and Tomorrow',
    from: moment().startOf('day').valueOf(),
    to: moment().add(1, 'days').endOf('day').valueOf(),
  },
  {
    key: 3,
    label: 'Next 7 Days',
    from: moment().startOf('day').valueOf(),
    to: moment().add(7, 'days').endOf('day').valueOf(),
  },
  {
    key: 4,
    label: 'Next 30 Days',
    from: moment().startOf('day').valueOf(),
    to: moment().add(30, 'days').endOf('day').valueOf(),
  },
  {
    key: 5,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days').startOf('day').valueOf(),
    to: moment().add(30, 'days').endOf('day').valueOf(),
  },
  {
    key: 6,
    label: 'Custom date range',
    from: localStorage.getItem('customData') ? localStorage.getItem('customData').split(',')[0] : '',
    to:  localStorage.getItem('customData') ? localStorage.getItem('customData').split(',')[1] : '',
  },
]

const Containers: FC<any> = ({ sellerSetting, setCompanyProductType, companyProductTypes, getCompanyProductAllTypes, getSellerSetting }) => {
  const [loading, setLoading] = useState(false)
  const [showCustomDateRange, setShowCustomDateRange] = useState(false)
  const [showAutoAssignContainerModalVisible, setShowAutoAssignContainerModalVisible] = useState(false)
  const [createNewContainerLoading, setCreateNewContainerLoading] = useState(false)
  const [createNewContainerModalVisible, setCreateNewContainerModalVisible] = useState(false)

  const [data, setData] = useState<any>([])
  const [query, setQuery] = useState({
    status: 1000,
    fromDate: presets[localStorage.getItem('containerDateKey') || 3].from,
    toDate: presets[localStorage.getItem('containerDateKey') || 3].to,
    // etaBeginDate: 1640052000000,
    // etaEndDate: 1640186987000,
  })

  const fetchContainerList = () => {
    setLoading(true)
    setCreateNewContainerLoading(false)
    OrderService.instance.getContainersSummary(query).subscribe({
      next(resp: any) {
        setLoading(false)
        const { data } = resp
        if (!data) return
        setData(data.map(v => {
          const groupByClient = _.groupBy(v.orderItemList, 'clientId')
          return ({
            ...v,
            entitiesByClients: Object.keys(groupByClient).map(e => ({
              clientId: e,
              companyName: groupByClient[e][0].companyName,
              itemCount: groupByClient[e].length,
              units: _.reduce(
                groupByClient[e],
                (sum, n) => precisionAdd(sum, n.picked),
                0,
              ),
              totalWeight: _.reduce(
                groupByClient[e],
                (sum, n) => precisionAdd(sum, precisionMultiply(n.catchWeightQty, n.grossWeight || 0)),
                0,
              ),
              totalVolume: _.reduce(
                groupByClient[e],
                (sum, n) => precisionAdd(sum, precisionMultiply(n.catchWeightQty, n.grossVolume || 0)),
                0,
              ),
              list: groupByClient[e]
            }))
          })
        }))
      },
      error(err) { checkError(err) },
      complete() {
        setLoading(false)
      }
    })
  }

  useEffect(() => {
    getCompanyProductAllTypes()
    getSellerSetting()
  }, [])

  useEffect(() => {
    fetchContainerList()
  }, [query])

  useEffect(() => {
    if (localStorage.getItem('containerDateKey') === '6') {
      setShowCustomDateRange(true)
    }
  }, [])

  const handleChangeDateFilter = (key: number) => {
    localStorage.setItem('containerDateKey', key)
    if (key === 6) {
      return setShowCustomDateRange(true)
    }
    setShowCustomDateRange(false)
    localStorage.removeItem('customData')
    return setQuery({
      ...query,
      fromDate: presets[key].from,
      toDate: presets[key].to,
    })
  }

  const handleCreateContainer = () => {
    setCreateNewContainerModalVisible(true)
    setCreateNewContainerLoading(true)
    // OrderService.instance.createContainer(`Container ${data.length + 1}`, sellerSetting.company.source).subscribe({
    //   next(resp) { fetchContainerList() },
    //   error(err) { checkError(err) },
    //   complete() {
    //     setCreateNewContainerLoading(false)
    //   }
    // })
  }

  const status = _.omitBy(containerStatus, _.isNumber)
  return (
    <ThemeSpin spinning={loading}>
      <WrapContainers>
        <div className='header'>
          <Flex>
            <div style={{ marginRight: 20 }}>
              Open Containers
              <h1>{data.length}</h1>
            </div>
            <div style={{ marginRight: 20 }}>
              <p>Container Status</p>
              <ThemeSelect
                defaultValue={containerStatus.OPEN}
                onChange={(status: any) => setQuery({ ...query, status })}
                style={{ width: 140, borderRadius: 20 }}
              >
                {Object.keys(status).map((v: string, index: number) => {
                  return (
                    <Select.Option value={Number(v)} key={index}>
                      <Flex className="v-center">
                        <div className="value">{status[v]}</div>
                      </Flex>
                    </Select.Option>
                  )
                })}
              </ThemeSelect>
            </div>
            <div style={{ marginRight: 20 }}>
              <p>Fulfillment Date</p>
              <Row type="flex">
                {showCustomDateRange && (
                  <RangePicker
                    placeholder={['From', 'To']}
                    format={dateFormat}
                    defaultValue={localStorage.getItem('customData') ? [moment(localStorage.getItem('customData').split(',')[0]), moment(localStorage.getItem('customData').split(',')[1])] : []}
                    allowClear={false}
                    onChange={(_, dateStrings) => {
                      localStorage.setItem('customData', [moment(dateStrings[0]).startOf('day').valueOf(), moment(dateStrings[1]).endOf('day').valueOf()])
                      setQuery({ ...query, fromDate: moment(dateStrings[0]).startOf('day').valueOf(), toDate: moment(dateStrings[1]).endOf('day').valueOf() })
                    }}
                  />
                )}
                <ThemeSelect defaultValue={Number(localStorage.getItem('containerDateKey')) || 3} style={ThemeSelectStyle()} onChange={handleChangeDateFilter}>
                  {presets.map((v) => (
                    <Option key={v.key} value={v.key}>
                      {v.label}
                    </Option>
                  ))}
                </ThemeSelect>
              </Row>
            </div>
          </Flex>
          <div>
            <ThemeButton loading={createNewContainerLoading} style={{ marginRight: 20 }} onClick={handleCreateContainer}>
              Create new Container
            </ThemeButton>
            <Button onClick={() => setShowAutoAssignContainerModalVisible(true)}>
              Auto-assign orders
            </Button>
          </div>
        </div>
        <div className='content'>
          {data.map(item => {
            return <ContainerItem key={item.containerId} containerList={data} item={item} fetchContainerList={fetchContainerList} sellerSetting={sellerSetting} setCompanyProductType={setCompanyProductType} companyProductTypes={companyProductTypes} />
          })}
        </div>
        {showAutoAssignContainerModalVisible &&
          <AutoAssignContainerModal
            onCancel={() => setShowAutoAssignContainerModalVisible(false)}
            sellerSetting={sellerSetting}
            simplifyCustomers={[]}
          />
        }
        {createNewContainerModalVisible && <CreateNewContainerModal onCancel={() => setCreateNewContainerModalVisible(false)} company={sellerSetting.company.source} callback={fetchContainerList} />}
      </WrapContainers>
    </ThemeSpin>
  )
}

const ContainerItem: FC<any> = ({ item, fetchContainerList, sellerSetting, companyProductTypes, setCompanyProductType, containerList }) => {
  const [expandedRowKeys, setExpandedRowKeys] = useState([])
  const [wrappedContainerDetailModalVisible, setWrappedContainerDetailModalVisible] = useState(false)
  const [wrappedContainerDetailSettingModalVisible, setWrappedContainerDetailSettingModalVisible] = useState(false)
  const [wrappedContainerCustomFieldsModalVisible, setWrappedContainerCustomFieldsModalVisible] = useState(false)
  const [deleteContainerModalVisible, setDeleteContainerModalVisible] = useState(false)
  const [loadSheetModalVisible, setLoadSheetModalVisible] = useState(false)
  const [invoiceModalVisible, setInvoiceModalVisible] = useState(false)
  const [BOLModalVisible, setBOLModalVisible] = useState(false)
  const [isHealthBol, setIsHealthBol] = useState(false)

  const [sendEmailModalVisible, setSendEmailModalVisible] = useState(false)
  const [sendEmailLoading, setSendEmailLoading] = useState(false)
  const [containerItem, setContainerItem] = useState(item)
  const [splitContainerModalVisible, setSplitContainerModalVisible] = useState(false)
  const [assignModalVisible, setAssignModalVisible] = useState(false)

  const [lineItem, setLineItem] = useState(null)
  const [currentLineItem, setCurrentLineItem] = useState(null)
  const [currentCustomer, setCurrentCustomer] = useState(null)

  const columns = [
    {
      title: 'CUSTOMER',
      dataIndex: 'companyName',
      render: (text, row, index) => {
        return {
          children: <span style={{ fontSize: 12 }}>
            <Tooltip placement="bottom" title={
              _.uniq(row.list.map(v => v.wholesaleOrderId)).map(orderId => (
                <Link key={orderId} to={`/sales-order/${orderId}`} style={{ color: '#fff', display: 'inline-block', marginRight: 20, marginLeft: 20 }}>{getOrderPrefix(sellerSetting, 'sales')}{orderId}</Link>
              ))
            }>
              <i className='companyName'>{text}</i>
            </Tooltip>, &nbsp;
            {row.itemCount}items, {row.units}units, <span>{formatNumber(row.totalWeight, 2)}LBs</span>, <span>{formatNumber(row.totalVolume, 2)}ft³</span>
          </span>,
          props: {
            colSpan: 5,
          },
        };
      }
    },
    {
      title: 'PRODUCT',
      render: (text, row, index) => {
        return {
          children:
            <Dropdown overlay={menu2} trigger={['click']} placement="bottomRight">
              <Icon type="more" onClick={() => setCurrentCustomer(row)} className='extra' style={{ cursor: 'pointer' }} />
            </Dropdown>,
          props: {
            colSpan: 1,
          },
        };
      }
    },
    {
      title: 'LOT#'
    },
    {
      title: 'UNITS'
    },
    {
      title: 'WEIGHT'
    },
    {
      title: 'VOL'
    },
  ]

  const status = _.omitBy(containerStatus, _.isNumber)

  useEffect(() => {
    setContainerItem(item)
  }, [item])

  const handleChangeStatus = (containerStatus) => {
    setContainerItem({ ...containerItem, containerStatus })
    OrderService.instance.updateContainerDetail({
      id: containerItem.containerId,
      status: containerStatus
    }).subscribe({
      next(resp) {

      },
      error(err) { checkError(err) },
      complete() {
        fetchContainerList()
      }
    })
  }

  const handleMouseOver = (v) => {
    setLineItem(v)
  }

  const expandedRowRender = (record: { list: any[] }) => {
    return record.list.map((v: React.SetStateAction<null>, i: React.Key | null | undefined) => (
      <div className='lineItem' key={i} onMouseOver={() => handleMouseOver(v)} onMouseLeave={() => setLineItem(null)}>
        <Link to={`/sales-order/${v.wholesaleOrderId}`} style={{ display: 'inline-block', minWidth: 160, marginRight: 20 }}>{v.itemName}</Link>
        <span style={{ display: 'inline-block', minWidth: 60, marginRight: 20 }}>{v.lotId}</span>
        <span style={{ display: 'inline-block', marginRight: 16 }}>{v.picked} {v.UOM}</span>
        <span style={{ display: 'inline-block', marginRight: 16 }}>{formatNumber(_.multiply(v.catchWeightQty, v.grossWeight), 2)}</span>
        <span>{formatNumber(_.multiply(v.catchWeightQty, v.grossVolume), 2)}</span>
        <Popover
          placement="bottomRight"
          content={
            <div style={{ cursor: 'pointer' }} onClick={() => { setSplitContainerModalVisible(true), setCurrentLineItem(v) }}>Move/Split to Other Containers</div>
          }
          trigger="click"
        >
          <Icon type="more" className='more' style={{ display: lineItem?.wholesaleOrderItemId === v.wholesaleOrderItemId ? 'block' : 'none', cursor: 'pointer' }} />
        </Popover>
      </div>
    ))
  }

  const onExpand = ({ expanded, record }: { expanded: boolean; record: any }): void => {
    if (expanded) {
      setExpandedRowKeys(expandedRowKeys.filter(id => id !== record.clientId))
    } else {
      setExpandedRowKeys([...expandedRowKeys, record.clientId])
    }
  }

  const handleDeleteContainer = () => {
    OrderService.instance.deleteContainer(containerItem.containerId).subscribe({
      next(resp: any) {
      },
      error(err) { checkError(err) },
      complete() {
        fetchContainerList()
      }
    })
  }

  const menu = () => {
    const handleClick = ({ key }) => {
      if (key === '0') {
        return setWrappedContainerDetailModalVisible(true)
      }
      if (key === '1') {
        return setWrappedContainerDetailSettingModalVisible(true)
      }
      if (key === '2') {
        return setWrappedContainerCustomFieldsModalVisible(true)
      }
      if (key === '3') {
        return setLoadSheetModalVisible(true)
      }
      if (key === '4') {
        setDeleteContainerModalVisible(true)
      }
    }

    return (
      <Menu onClick={handleClick}>
        <Menu.Item key="0">
          Edit Container Details
        </Menu.Item>
        <Menu.Item key="1">
          Edit Sailing Details
        </Menu.Item>
        <Menu.Item key="2">
          Edit Customer Fields
        </Menu.Item>
        <Menu.Item key="3">
          View Load Sheet
        </Menu.Item>
        <Menu.Item key="4">
          Delete Container
        </Menu.Item>
      </Menu>
    )
  }

  const menu2 = () => {
    const handleClick = ({ key }) => {
      if (key === '0') {
        return setAssignModalVisible(true)
      }
      if (key === '1') {
        return setInvoiceModalVisible(true)
      }
      if (key === '2') {
        setIsHealthBol(false)
        return setBOLModalVisible(true)
      }
      if (key === '3') {
        setIsHealthBol(true)
        return setBOLModalVisible(true)
      }
    }

    return (
      <Menu onClick={handleClick}>
        <Menu.Item key="0">
          Re-assign to Other Container
        </Menu.Item>
        <Menu.Item key="1">
          View Invoice
        </Menu.Item>
        <Menu.Item key="2">
          View BOL
        </Menu.Item>
        <Menu.Item key="3">
          View Health Document
        </Menu.Item>
      </Menu>
    )
  }

  const total = _.reduce(
    containerItem.orderItemList,
    (sum, n) => precisionAdd(sum, n.picked),
    0,
  )
  const totalWeight = _.reduce(
    containerItem.orderItemList,
    (sum, n) => precisionAdd(sum, precisionMultiply(n.catchWeightQty, n.grossWeight || 0)),
    0,
  )
  const totalVolume = _.reduce(
    containerItem.orderItemList,
    (sum, n) => precisionAdd(sum, precisionMultiply(n.catchWeightQty, n.grossVolume || 0)),
    0,
  )

  return (
    <div className='item'>
      <Flex className='item_header' style={{ justifyContent: 'space-between' }}>
        <div>
          <div>{containerItem.containerName}</div>
          <span>{containerItem.containerType}</span>
        </div>
        <Dropdown overlay={menu} trigger={['click']}>
          <Icon type="ellipsis" style={{ fontSize: 28, height: 26, padding: '0px 3px', boxShadow: '0 2px 8px rgb(0 0 0 / 15%)', borderRadius: 3 }} />
        </Dropdown>
      </Flex>
      <div className='item_info'>
        <Flex style={{ justifyContent: 'space-between' }}>
          <div>
            <div>{containerItem.logisticName}</div>
            <span>ETD: {containerItem.etdDate ? moment(containerItem.etdDate).format('HH:mm DD/MM/YYYY') : ''}</span>
          </div>
          <ThemeSelect
            onChange={handleChangeStatus}
            className={`status-selection`}
            value={containerItem.containerStatus}
            suffixIcon={<Icon type="caret-down" />}
            style={{ width: 100, borderRadius: 20 }}
          >
            {Object.keys(status).map((v: string, index: number) => {
              return (
                <Select.Option value={Number(v)} key={index}>
                  <Flex className="v-center">
                    <div className="value">{status[v]}</div>
                  </Flex>
                </Select.Option>
              )
            })}
          </ThemeSelect>
        </Flex>
        <Flex className='statistics'>
          <div>
            <strong>{total}</strong>
            <span>Total Units</span>
          </div>
          <div>
            {(!totalWeight || !containerItem.maxWeight) ?
              <strong>N/A</strong>
              : <strong>{_.multiply(_.divide(totalWeight, containerItem.maxWeight), 100).toFixed(2)}%</strong>
            }
            <span>Gross Weight</span>
          </div>
          <div>
            {(!totalVolume || !containerItem.maxVolume) ?
              <strong>N/A</strong>
              : <strong>{_.multiply(_.divide(totalVolume, containerItem.maxVolume), 100).toFixed(2)}%</strong>
            }
            <span>Volume</span>
          </div>
        </Flex>
      </div>
      <div className='tableCon'>
        <Table
          columns={columns}
          dataSource={containerItem.entitiesByClients}
          pagination={false}
          expandIconAsCell={false}
          expandedRowRender={expandedRowRender}
          expandedRowKeys={expandedRowKeys}
          expandIcon={({ expanded, record }) => <Icon type={expanded ? 'caret-down' : 'caret-right'} onClick={() => onExpand({ expanded, record })} />}
          rowKey={'clientId'}
        />
      </div>
      <Flex className='item_footer'>
        <span>Total</span>
        <span>{total} units</span>
        <span>{formatNumber(totalWeight, 2)} LBs</span>
        <span>{formatNumber(totalVolume, 2)} ft³</span>
      </Flex>
      {wrappedContainerDetailModalVisible &&
        <WrappedContainerDetailModal
          setCompanyProductType={setCompanyProductType}
          companyProductTypes={companyProductTypes}
          callback={fetchContainerList}
          currentContainerId={containerItem.containerId}
          onCancel={() => setWrappedContainerDetailModalVisible(false)}
        />
      }
      {wrappedContainerDetailSettingModalVisible &&
        <WrappedContainerDetailSettingModal
          callback={fetchContainerList}
          currentContainerId={containerItem.logisticOrderId}
          onCancel={() => setWrappedContainerDetailSettingModalVisible(false)}
        />
      }
      {wrappedContainerCustomFieldsModalVisible &&
        <WrappedContainerCustomFieldsModal
          callback={fetchContainerList}
          currentContainerId={containerItem.containerId}
          onCancel={() => setWrappedContainerCustomFieldsModalVisible(false)}
        />
      }
      {deleteContainerModalVisible &&
        <WrapModal
          title='Delete Container'
          visible
          footer={[
            <ThemeButton key={1} onClick={handleDeleteContainer}>Delete</ThemeButton>,
            <span key={2} style={{ marginLeft: 20 }} onClick={() => setDeleteContainerModalVisible(false)}>Cancel</span>
          ]}
        >
          <div className='desc'>
            Deleting this container will unassign items that are currently assigned to this container.
          </div>
          <div className='desc'>
            You may want to move these items to another container before deleting.
          </div>
          <div className='desc'>
            Continue?
          </div>
        </WrapModal>
      }
      {loadSheetModalVisible && <PrintLoadSheetModal setLoadSheetModalVisible={setLoadSheetModalVisible} setSendEmailModalVisible={setSendEmailModalVisible} sellerSetting={sellerSetting} containerItem={containerItem} />}
      {invoiceModalVisible && <PrintInvoiceModal setInvoiceModalVisible={setInvoiceModalVisible} setSendEmailModalVisible={setSendEmailModalVisible} sellerSetting={sellerSetting} containerItem={containerItem} />}
      {BOLModalVisible && <PrintBillOfLadingModal setBOLModalVisible={setBOLModalVisible} setSendEmailModalVisible={setSendEmailModalVisible} sellerSetting={sellerSetting} containerItem={containerItem} isHealthBol={isHealthBol} setIsHealthBol={setIsHealthBol} />}

      {splitContainerModalVisible && <SplitContainerModal fetchContainerList={fetchContainerList} containerList={containerList} onCancel={() => setSplitContainerModalVisible(false)} currentLineItem={currentLineItem} />}
      {assignModalVisible && <ReAssignModal onCancel={() => setAssignModalVisible(false)} fetchContainerList={fetchContainerList} currentCustomer={currentCustomer} sellerSetting={sellerSetting} containerList={containerList} containerItem={containerItem} />}
    </div>
  )
}

const PrintLoadSheetModal: FC<any> = ({ setLoadSheetModalVisible, setSendEmailModalVisible, sellerSetting, containerItem }) => {
  return (
    <Modal
      width={1080}
      footer={null}
      visible
      onCancel={() => setLoadSheetModalVisible(false)}
      wrapClassName="print-modal"
    >
      <PrintModalHeader>
        <ThemeButton
          size="large"
          className="print-pick-sheet"
          onClick={() => printWindowAsync('printPickSheetModal', null)}
        >
          <Icon type="printer" theme="filled" />
          Print Load Sheet
        </ThemeButton>
        <ThemeButton
          size="large"
          className="print-bill-lading"
          style={{ marginRight: 10 }}
          onClick={() => setSendEmailModalVisible(true)}
        // disabled={!finishLoadingPrintLogo}
        >
          <Icon type="mail" theme="filled" />
          Email
        </ThemeButton>
        <div className="clearfix" />
        {/* {sendEmailModalVisible && (
              <WrappedSendEmail
                title='Load Sheet'
                onCancel={() => setSendEmailModalVisible(false)}
                receiver={''}
                sendEmailLoading={sendEmailLoading}
                clientId={this.props.currentOrder.wholesaleClient.clientId}
                handleSendEmail={(values) => {
                  this.setState({ sendEmailLoading: true }, () => {
                    sendEmail({
                      ...values,
                      title: 'pick sheet',
                      elementId: 'printPickSheetModal',
                      orderId: this.props.match.params.orderId,
                      layout: userPrintSetting.invoice_layout,
                      handleSuccess: (v) => this.setState({ sendEmailLoading: v, sendEmailModalVisible: false }),
                    })
                  })
                }}
              />
            )} */}
      </PrintModalHeader>
      <div id={'printPickSheetModal'}>
        <PrintPickSheet
          orderItems={[]}
          company={sellerSetting.company}
          catchWeightValues={[]}
          logo={'default'}
          // printSetting={this.props.printSetting}
          sellerSetting={sellerSetting}
          changePrintLogoStatus={() => { }}
          currentPrintContainerId={containerItem.containerId}
        // orderId={this.props.match.params.orderId}
        />
      </div>
    </Modal>
  )
}

const PrintInvoiceModal: FC<any> = ({ setInvoiceModalVisible, setSendEmailModalVisible, sellerSetting, containerItem }) => {
  return (
    <Modal
      width={1080}
      footer={null}
      visible
      onCancel={() => setInvoiceModalVisible(false)}
      wrapClassName="print-modal"
    >
      <PrintModalHeader>
        <Tooltip title="Print invoice and set order status to Shipped" placement="bottom">
          <ThemeButton
            size="large"
            className="print-bill-lading"
            onClick={() => printWindowAsync('PrintInvoiceModal', null)}
          // disabled={!finishLoadingPrintLogo}
          >
            <Icon type="printer" theme="filled" />
            Print Invoice
          </ThemeButton>
        </Tooltip>
        <ThemeButton
          size="large"
          className="print-bill-lading"
          style={{ marginRight: 10 }}
          onClick={() => setSendEmailModalVisible(true)}
        // disabled={!finishLoadingPrintLogo}
        >
          <Icon type="mail" theme="filled" />
          Email Invoice
        </ThemeButton>
        <div className="clearfix" />
        {/* {sendEmailModalVisible && (
              <WrappedSendEmail
                title='Load Sheet'
                onCancel={() => setSendEmailModalVisible(false)}
                receiver={''}
                sendEmailLoading={sendEmailLoading}
                clientId={this.props.currentOrder.wholesaleClient.clientId}
                handleSendEmail={(values) => {
                  this.setState({ sendEmailLoading: true }, () => {
                    sendEmail({
                      ...values,
                      title: 'pick sheet',
                      elementId: 'printPickSheetModal',
                      orderId: this.props.match.params.orderId,
                      layout: userPrintSetting.invoice_layout,
                      handleSuccess: (v) => this.setState({ sendEmailLoading: v, sendEmailModalVisible: false }),
                    })
                  })
                }}
              />
            )} */}
      </PrintModalHeader>
      <div id={'PrintInvoiceModal'}>
        <PrintInvoice
          currentOrder={{ wholesaleClient: {}, wholesaleOrderId: '' }}
          orderItems={[]}
          company={sellerSetting.company}
          logo={'default'}
          sellerSetting={sellerSetting}
          changePrintLogoStatus={() => { }}
          type={'invoice'}
          currentPrintContainerId={containerItem.containerId}
          oneOffItemList={[]}
        />
      </div>
    </Modal>
  )
}

const PrintBillOfLadingModal: FC<any> = ({ setBOLModalVisible, setSendEmailModalVisible, sellerSetting, containerItem, isHealthBol, setIsHealthBol }) => {
  return (
    <Modal
      width={1080}
      footer={null}
      visible
      onCancel={() => { setBOLModalVisible(false), setIsHealthBol(false) }}
      wrapClassName="print-modal"
    >
      <PrintModalHeader>
        <Tooltip title="Print bill of lading and set order status to Shipped" placement="bottom">
          <ThemeButton
            size="large"
            className="print-bill"
            onClick={() => printWindowAsync('printBillModal', null)}
          >
            <Icon type="printer" theme="filled" />
            Print Bill of Lading
          </ThemeButton>
        </Tooltip>
        <ThemeButton
          size="large"
          className="print-bill-lading"
          style={{ marginRight: 10 }}
          onClick={() => setSendEmailModalVisible(true)}
        // disabled={!finishLoadingPrintLogo}
        >
          <Icon type="mail" theme="filled" />
          Email
        </ThemeButton>
        <div className="clearfix" />
        {/* {sendEmailModalVisible && (
              <WrappedSendEmail
                title='Load Sheet'
                onCancel={() => setSendEmailModalVisible(false)}
                receiver={''}
                sendEmailLoading={sendEmailLoading}
                clientId={this.props.currentOrder.wholesaleClient.clientId}
                handleSendEmail={(values) => {
                  this.setState({ sendEmailLoading: true }, () => {
                    sendEmail({
                      ...values,
                      title: 'pick sheet',
                      elementId: 'printPickSheetModal',
                      orderId: this.props.match.params.orderId,
                      layout: userPrintSetting.invoice_layout,
                      handleSuccess: (v) => this.setState({ sendEmailLoading: v, sendEmailModalVisible: false }),
                    })
                  })
                }}
              />
            )} */}
      </PrintModalHeader>
      <div id={'printBillModal'}>
        <PrintBillOfLading
          title={'Bill of Lading'.toUpperCase()}
          orderItems={[]}
          currentOrder={{ wholesaleClient: {}, wholesaleOrderId: '' }}
          company={sellerSetting.company}
          logo={'default'}
          sellerSetting={sellerSetting}
          changePrintLogoStatus={() => { }}
          type={'bill'}
          currentPrintContainerId={containerItem.containerId}
          isHealthBol={isHealthBol}
          paymentTerms={[]}
        />
      </div>
    </Modal>
  )
}

const SplitContainerModal: FC<any> = ({ fetchContainerList, onCancel, currentLineItem, containerList }) => {
  const [loading, setLoading] = useState(false)
  const [saveLoading, setSaveLoading] = useState(false)
  const [data, setData] = useState<any>([])
  const [currentUnit, setCurrentUnit] = useState(0)
  const [errorMessage, setErrorMessage] = useState('')
  useEffect(() => {
    setLoading(true)
    OrderService.instance.getTmsFlowItem(currentLineItem.wholesaleOrderItemId).subscribe({
      next(resp: any) {
        setData(resp.data)
        setCurrentUnit(_.reduce(
          resp.data,
          (sum, n) => precisionAdd(sum, Number(n.quantity)),
          0
        ))
      },
      error(err) { checkError(err) },
      complete() {
        setLoading(false)
      }
    })
  }, [])

  const totalQutantiy = _.reduce(
    data,
    (sum, n) => precisionAdd(sum, Number(n.quantity)),
    0
  )

  const handleSplit = () => {
    if (totalQutantiy > currentUnit) {
      return setErrorMessage('Too Many Units Assigned')
    }
    if (totalQutantiy < currentUnit) {
      return setErrorMessage('Too Few Units Assigned')
    }

    setSaveLoading(true)
    const request = data.map((v: any) => ({
      containerId: v.containerId,
      containerNo: v.containerNo,
      maxWeight: v.maxWeight,
      maxVolume: v.maxVolume,
      oms: v.oms,
      tmsFlowItemId: v.tmsFlowItemId,
      quantity: v.quantity || 0,
      source: v.source,
      uom: v.uom
    }))
    OrderService.instance.updateTmsFlowItem(request).subscribe({
      next(resp: any) {
      },
      error(err) { checkError(err) },
      complete() {
        onCancel()
        fetchContainerList()
      }
    })
  }

  const handleAddContainer = id => {
    const container = containerList.find(v => v.containerId === id)
    setData([
      ...data,
      {
        ...container,
        quantity: 0,
        grossWeight:
          currentLineItem.grossWeight,
        grossVolume: currentLineItem.grossVolume,
        uom: currentLineItem.UOM,
        containerNo: container.containerName,
        catchWeightQty: currentLineItem.catchWeightQty,
        oms: currentLineItem.wholesaleOrderId,
        source: currentLineItem.wholesaleOrderItemId,
      }
    ])
  }

  const handleChangeUnit = (quantity, record) => {
    setData(data.map(v => {
      if (v.containerId === record.containerId) return ({ ...v, quantity })
      return v
    }))
  }

  const columns = [
    {
      title: 'UNITS.',
      dataIndex: 'quantity',
      align: 'left',
      render: (t, r) => {
        return (
          <>
            <ThemeInputNumber min={0} width={70} value={t} precision={0} onChange={v => { handleChangeUnit(v, r), setErrorMessage('') }} />
            <span style={{ marginLeft: 10 }}>{r.uom}</span>
          </>
        )
      }
    },
    {
      title: 'WEIGHT',
      align: 'left',
      dataIndex: 'catchWeightQty',
      render: (t, r) => {
        return _.multiply(t, r.grossWeight)
      }
    },
    {
      title: 'VOLUME',
      align: 'left',
      dataIndex: 'catchWeightQty',
      render: (t, r) => {
        return _.multiply(t, r.grossVolume)
      }
    },
    {
      title: 'CONTAINER NO.',
      align: 'left',
      dataIndex: 'containerNo'
    },
    {
      title: 'CONTAINER',
      align: 'left',
      dataIndex: 'maxWeight',
      render: (t, r) => {
        if (!r.quantity || !r.grossWeight || !r.maxWeight) return '0.00% LBs'
        return `${_.multiply(_.divide(_.multiply(r.catchWeightQty, r.grossWeight), t), 100).toFixed(2)}% LBs`
      }
    },
    {
      title: 'CAPACITY',
      align: 'left',
      dataIndex: 'maxVolume',
      render: (t, r) => {
        if (!r.quantity || !r.grossVolume || !r.maxVolume) return '0.00% ft³'
        return `${_.multiply(_.divide(_.multiply(r.catchWeightQty, r.grossVolume), t), 100).toFixed(2)}% ft³`
      }
    },
  ]

  return (
    <SplitModal
      title='Move/Split Items to Other Containers'
      visible
      onCancel={onCancel}
      width={800}
      footer={[
        <ThemeButton onClick={handleSplit} loading={saveLoading}>Save</ThemeButton>,
        <span style={{ marginLeft: 20, cursor: 'pointer' }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <ThemeSpin spinning={loading}>
        <Flex className='header'>
          <div className='header_item'>
            <div>Customer</div>
            <div>{currentLineItem.companyName}</div>
          </div>
          <div className='header_item'>
            <div>Product</div>
            <div>{currentLineItem.itemName}</div>
          </div>
          <div className='header_item'>
            <div>Units Ordered</div>
            <div>{currentUnit}</div>
          </div>
        </Flex>
        <div className='desc'>Specify number of units to assign to open containers</div>
        <div className='table'>
          <ThemeTable
            columns={columns}
            dataSource={data}
            pagination={false}
            footer={() => {
              return (
                <AutoComplete
                  style={{ width: 200 }}
                  placeholder="Select Container"
                  onChange={handleAddContainer}
                  filterOption={(inputValue: any, option: any) =>
                    option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                  }
                >
                  {_.xorBy(data, containerList, 'containerId').map(v => <AutoComplete.Option key={v.containerId}>{v.containerName}</AutoComplete.Option>)}
                </AutoComplete>
              )
            }}
          />
          <div className='footer_desc'>
            <div className='assigned'>Qty.Assigned to Containers</div>
            <div className='count'>{totalQutantiy} of {currentUnit} </div>
            <div className='errorMessage'>{errorMessage}</div>
          </div>
        </div>
      </ThemeSpin>
    </SplitModal>
  )
}

const ReAssignModal: FC<any> = ({ onCancel, currentCustomer, sellerSetting, containerList, containerItem, fetchContainerList }) => {
  const [saveLoading, setSaveLoading] = useState(false)
  const [newContainerId, setNewContainerId] = useState(null)


  const handleSave = () => {
    setSaveLoading(true)
    const data = {
      oldContainerId: containerItem.containerId,
      newContainerId: newContainerId,
      orderItemList: currentCustomer.list.map(v => ({ wholesaleOrderId: v.wholesaleOrderId, wholesaleOrderItemId: v.wholesaleOrderItemId }))
    }

    OrderService.instance.containerReassignOrderItems(data).subscribe({
      next(resp: any) {
        fetchContainerList()
      },
      error(err) { checkError(err) },
      complete() {
        setSaveLoading(false)
        onCancel()
      }
    })
    
  }

  return (
    <SplitModal
      visible
      title='Re-Assign Order to Other Containers'
      onCancel={onCancel}
      width={800}
      footer={[
        <ThemeButton onClick={handleSave} loading={saveLoading}>Save</ThemeButton>,
        <span style={{ marginLeft: 20, cursor: 'pointer' }} onClick={onCancel}>Cancel</span>
      ]}
    >
      <Flex className='header'>
        <div className='header_item'>
          <div>Customer</div>
          <div>{currentCustomer.companyName}</div>
        </div>
        <div className='header_item'>
          <div>Sales Order</div>
          <div>{getOrderPrefix(sellerSetting, 'sales')}{currentCustomer.list[0].wholesaleOrderId}</div>
        </div>
        <div className='header_item'>
          <div>Fulfillment Date</div>
          <div>{moment(currentCustomer.list[0].deliveryDate).format('DD/MM/YYYY')}</div>
        </div>
      </Flex> 
      <div className='desc'>
        <div>Choose An Open Container to Assign this Order</div>
        <ThemeSelect style={{ width: 200, marginTop: 20 }} placeholder='Selaect A Container' onChange={setNewContainerId}>
          {containerList.map(v => (
            <Select.Option value={v.containerId} >
              {v.containerName}
            </Select.Option>
          ))}
        </ThemeSelect>
      </div>
    </SplitModal>
  )
}

const WrapModal = styled(ThemeModal)`
  .ant-modal-footer {
    text-align: left;
  }
  .desc {
    margin-bottom: 20px;
  }
`

const SplitModal = styled(WrapModal)`
  .ant-modal-body {
    padding: 24px 0;
  }
  .ant-table-thead > tr > th {
    padding: 16px 10px !important;
  }
  .header {
    border-top: 1px solid #D8DBDB;
    border-bottom: 1px solid #D8DBDB;
    padding: 24px;
    background: #f7f7f7;
    .header_item {
      margin-right: 30px;
      div {
        line-height: 24px;
      }
    }
  }
  .desc {
    padding: 36px 24px;
  }
  .table {
    padding: 0 24px;
  }
  .footer_desc {
    margin: 24px 0;
    text-align: right;
    .assigned {
      font-weight: normal;
    }
    .count {
      font-size: 30px;
    }
    .errorMessage {
      color: #EB5757;
      font-weight: normal;
    }
  }
`

const WrapContainers = styled.div`
  padding: 60px 20px 40px 60px;
  height: 100%;
  text-align: left;
  .ant-table {
    padding: 0 10px;
  }
  .ant-table-thead > tr > th, .ant-table-tbody > tr > td {
    padding: 4px 0px;
    padding-bottom: 4px !important;
    background: #fff;
  }
  .ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr > td {
    background: #fff;
  }
  .ant-table-expanded-row {
    font-size: 12px;
    .lineItem {
      line-height: 24px;
      position: relative;
    }
    .more {
      font-size: 16px;
      transform: rotate(90deg);
      position: absolute;
      top: 4px;
      right: 0;
      box-shadow: 1px 2px 4px 2px rgb(0 0 0 / 15%);
    }
  }
  .extra {
    font-size: 16px;
    transform: rotate(90deg);
    box-shadow: 1px 2px 4px 2px rgb(0 0 0 / 15%);
  }
  .ant-table-wrapper .ant-table-content .ant-table-body tbody > .ant-table-row > td {
    background: #fff;
  }
  .ant-table-thead > tr > th .ant-table-header-column {
    vertical-align: middle;
  }
  .header {
    display: flex;
    margin-bottom: 30px;
    width: 100%;
    justify-content: space-between;
  }
  .item {
    width: 420px;
    flex-shrink: 0;
    height: 620px;
    margin-right: 20px;
    border-radius: 8px !important;
    box-shadow: 0 2px 8px rgb(0 0 0 / 15%);
    position: relative;
    .companyName {
      font-style: normal;
      &:hover {
        color: #1C6E31;
      }
    }
    .tableCon {
      height: 390px;
      overflow: hidden;
      overflow-y: auto;
    }
    .item_header {
      padding: 20px 20px 15px;
    }
    .item_info {
      padding: 0 20px 20px;
    }
    .item_footer {
      height: 30px;
      justify-content: space-around;
      background: #E9F3EB;
      line-height: 30px;
      position: absolute;
      width: 100%;
      bottom: 0;
    }
    .statistics {
      margin-top: 5px;
      background: #F3F3F3;
      border-radius: 8px;
      padding: 10px 0px;
      justify-content: space-around;
      div {
        text-align: center;
      }
      strong {
        display: block;
      }
    }
    .status-selection: {
      &.not-selector: {
        width: 130,
        border: '1px solid #D8DBDB',
        borderRadius: 20,
        padding: '4px 10px',
      },
      div.ant-select-selection: {
        borderRadius: 20,
      },
    },
    .ant-select-selection {
      border-radius: 20px;
    }
  }
  .content {
    display: flex;
    overflow-x: auto;
    padding: 10px;
  }
`
const ThemeSelectStyle = (): CSSProperties => ({
  width: '270px',
  fontWeight: 500
  // margin: '-4px 30px 0 10px'
})

const ContainersWrap = connect(OrdersModule)(({ orders }) => orders)(Containers)

export default ContainersWrap
