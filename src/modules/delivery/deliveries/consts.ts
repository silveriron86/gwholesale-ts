const consts = {
  ordersMissingRoute: 'ordersMissingRoute',
  ordersMissingDriver: 'ordersMissingDriver',
  unassignedRoutes: 'unassignedRoutes',
  driver: 'driver',
}

export default consts
