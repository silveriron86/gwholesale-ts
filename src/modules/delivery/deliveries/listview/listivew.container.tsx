import * as React from 'react'
import { notification, Divider } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'

import { DeliveryModule, DeliveryDispatchProps, DailyDeliveryProps } from '../../delivery.module'

import { ListViewContainer, UserList, UserListInner, ListViewSwitcher } from './listview.style'
import { ViewSelectorProps } from '../tableview/view-selector'
import { UserInfoComponent } from './user-info'
import { RouteContent } from './route-content'
import { AccountUser } from '~/schema'
import { ViewSwitcherEl } from '~/components/elements/view-selector'
import consts from '../consts'

export type ListViewProps = DeliveryDispatchProps & DailyDeliveryProps & ViewSelectorProps & {}

export class ListViewComponent extends React.PureComponent<ListViewProps> {
  state: any
  constructor(props: ListViewProps) {
    super(props)

    this.state = {
      selectedDriver: null,
      currentMenu: 'Unassigned Routes' //default selected menu
    }
  }


  componentDidMount() { }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Go to Pricesheet below to place a new order',
      onClick: () => {
        //console.log('Notification Clicked!')
      },
    })
  }

  onClickViewSelector = (param: boolean) => {
    this.props.onClick(param)
  }

  render() {
    const { viewType, drivers } = this.props
    const { selectedDriver, currentMenu } = this.state
    // const { selectedDriverIndex } = this.state
    return (
      <ListViewContainer>
        { drivers &&
          <UserList>
            <ListViewSwitcher style={{}}>
              <ViewSwitcherEl viewType={viewType} onClick={this.onClickViewSelector} />
            </ListViewSwitcher>
            <Divider style={{ margin: '20px 0' }} />
            <UserListInner>{this.renderDrivers()}</UserListInner>
          </UserList>
        }
        <RouteContent
          selectedDriver={selectedDriver} currentMenu={currentMenu}
        />
      </ListViewContainer>
    )
  }

  renderDrivers() {
    const { drivers } = this.props
    const { selectedDriver, currentMenu } = this.state
    const user_infos: any = []
    let selected = false
    let currentDriver: any = null;

    for (let i = 0; i < drivers.length; i++) {
      if (selectedDriver && selectedDriver.id == drivers[i].id) {
        selected = true
        currentDriver = drivers[i]
      } else {
        selected = false
      }
      user_infos.push(
        <UserInfoComponent key={i + 2} onClick={this.onClickUser} driver={drivers[i]} selected={selected} currentMenu="drive" menuName="" />, //userId is test props
      )
    }
    if (currentDriver == null) {
      selected = true
    } else {
      selected = false
    }

    user_infos.unshift(
      <UserInfoComponent key={consts.unassignedRoutes} onClick={this.onClickUser} driver={null} selected={false} currentMenu={currentMenu} menuName="Unassigned Routes" />, //userId is test props
    )
    user_infos.unshift(
      <UserInfoComponent key={consts.ordersMissingDriver} onClick={this.onClickUser} driver={null} selected={false} currentMenu={currentMenu} menuName="Orders missing driver" />,
    )
    user_infos.unshift(
      <UserInfoComponent key={consts.ordersMissingRoute} onClick={this.onClickUser} driver={null} selected={false} currentMenu={currentMenu} menuName="Orders missing route" />, //userId is test props
    )
    // this.setState({ selectedDriver: currentDriver });
    return user_infos
  }

  onClickUser = (driver: AccountUser, currentMenu: string) => {
    this.setState({
      selectedDriver: driver,
      currentMenu
    })
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const ListView = withTheme(connect(DeliveryModule)(mapStateToProps)(ListViewComponent))
