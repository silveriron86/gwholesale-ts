import * as React from 'react'
import { Table, Select } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'

import { DeliveryModule, DeliveryDispatchProps, DailyDeliveryProps } from '../../delivery.module'
import { AccountUser, Order } from '~/schema'
import { Icon as IconSvg } from '~/components/icon/'
import { Link } from 'react-router-dom'
import { Theme } from '~/common'
import { Icon } from 'antd'
import { VersionNumber } from '../route-card/card.style'
import { formatOrderStatus, getOrderPrefix } from '~/common/utils'

const { Option } = Select

export type ListViewTableProps = DeliveryDispatchProps &
  DailyDeliveryProps & {
    selectedDriver: AccountUser | null
    currentMenu: string
    theme: Theme
  }

export class ListViewTableComponent extends React.PureComponent<ListViewTableProps> {
  column: any
  unAssignedRouteColumns: (
    | { title: string; dataIndex: string; key?: undefined; width?: undefined; render?: undefined }
    | {
        title: string
        dataIndex: string
        key: string
        width: number
        render: (_text: number, record: any, index: number) => JSX.Element
      }
  )[]

  constructor(props: ListViewTableProps) {
    super(props)
    const { theme } = props

    this.state = {}
    this.unAssignedRouteColumns = [
      {
        title: 'SO #',
        dataIndex: 'route_id',
      },
      {
        title: 'ROUTE NAME',
        dataIndex: 'route_name',
      },
      {
        title: '',
        dataIndex: '',
        key: 'y',
        width: 280,
        render: (_text: number, record: any, index: number) => {
          return (
            <Select
              onChange={this.assignDriver.bind(this, record.route_id)}
              style={{ width: '100%' }}
              placeholder="ASSIGN TO"
            >
              {this.renderDrivers()}
            </Select>
          )
        },
      },
    ]

    this.column = [
      {
        title: '#',
        dataIndex: 'status',
        render: (data: any, record: any) => {
          return (
            <div>
              {data ? (
                <div>
                  <IconSvg type="check" viewBox="0 0 15 14" width={15} height={14} style={{ fill: 'none' }} />
                </div>
              ) : (
                <div />
              )}
            </div>
          )
        },
      },
      {
        title: 'SALES ORDER #',
        dataIndex: 'orderId',
        render: (id: string) => {
          return `${getOrderPrefix(props.sellerSetting, 'sales')}${id}`
        },
      },
      {
        title: 'STATUS',
        dataIndex: 'wholesaleOrderStatus',
        render: (status: string) => {
          return `${formatOrderStatus(status, props.sellerSetting)}`
        },
      },
      {
        title: 'CUSTOMER',
        dataIndex: 'customer',
      },
      {
        title: '..',
        dataIndex: 'versionNumber',
        width: 40,
        align: 'center',
        render: (versionNumber: number, record: any) => {
          return (
            <VersionNumber
              style={{
                backgroundColor: record.hasWorkOrder ? '#FFDE68' : 'transparent',
                fontWeight: record.hasWorkOrder ? 'bold' : 'normal',
              }}
            >
              {versionNumber}
            </VersionNumber>
          )
        },
      },
      {
        title: '# OF UNITS',
        dataIndex: 'totalUnits',
        render: (data: any, record: any) => {
          return (
            <div>
              {record.hasWorkOrder === true && (
                <span style={{ color: theme.primary, marginRight: 5 }}>
                  <Icon type="star" theme="filled" />
                </span>
              )}
              {data}
            </div>
          )
        },
      },
      {
        title: 'OVERRIDE DRIVER',
        dataIndex: '',
        key: 'y',
        width: 280,
        render: (_text: number, order: any, index: number) => {
          return (
            <Select
              defaultValue={order.overrideDriverId}
              onChange={this.reassignDriver.bind(this, order.orderId)}
              style={{ width: '100%' }}
              placeholder="REASSIGN TO"
            >
              {this.renderDrivers()}
            </Select>
          )
        },
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        width: 190,
        render: (text: any, order: any) => (
          <Link to={`/sales-order/${order.orderId}`}>
            <span>Go To Sales Order</span>
          </Link>
        ),
      },
    ]
  }
  componentDidMount() {}

  render() {
    const table = this.getTableData()
    const { selectedDriver, unassignedRoutes, deliveries, currentMenu, unassignedOrders } = this.props

    if (selectedDriver) {
      return (
        <div>
          <Table dataSource={table.data} columns={table.column} pagination={false} rowKey="orderId" />
        </div>
      )
    }

    if (currentMenu === 'Unassigned Routes') {
      return (
        <div>
          <Table dataSource={table.data} columns={table.column} pagination={false} rowKey="route_id" />
        </div>
      )
    }

    if (currentMenu === 'Orders missing driver') {
      return (
        <div>
          <Table dataSource={table.data} columns={table.column} pagination={false} rowKey="wholesaleOrderId" />
        </div>
      )
    }

    if (currentMenu === 'Orders missing route') {
      return (
        <div>
          <Table dataSource={table.data} columns={table.column} pagination={false} rowKey="wholesaleOrderId" />
        </div>
      )
    }

    return null
  }

  getTableData = () => {
    const {
      selectedDriver,
      unassignedRoutes,
      deliveries,
      currentMenu,
      unassignedOrders,
      ordersMissingDriver,
      sellerSetting,
    } = this.props
    const data: any[] = []
    if (selectedDriver) {
      const delivery = deliveries.filter((el) => el.driver.userId == selectedDriver.id.toString())[0]
      if (delivery != undefined) {
        const orders = delivery.orders
        console.log(orders)
        for (let i = 0; i < orders.length; i++) {
          const order = orders[i]
          data.push({
            wholesaleOrderStatus: order.status,
            status: order.status == 'PLACED' ? true : false,
            orderId: order.wholesaleOrderId,
            customer: order.customer,
            versionNumber: order.versionNumber,
            totalUnits: order.sumOfUnits,
            overrideDriverId: order.overrideDriverId != 0 ? order.overrideDriverId : null,
            hasWorkOrder: order.hasWorkOrder,
          })
        }
        return {
          data: data,
          column: this.column,
        }
      }
      return {
        data: [],
        column: this.column,
      }
    }

    if (currentMenu == 'Unassigned Routes') {
      for (let i = 0; i < unassignedRoutes.length; i++) {
        const route = unassignedRoutes[i]
        data.push({
          route_id: route.id,
          route_name: route.routeName,
        })
      }
      return {
        data: data,
        column: this.unAssignedRouteColumns,
      }
    }

    const orderTitle = {
      'Orders missing route': 'Orders missing route',
      'Orders missing driver': 'Orders missing driver',
    }
    const orderColumns = [
      {
        title: 'SALES ORDER #',
        dataIndex: 'wholesaleOrderId',
        render: (id: string) => {
          return `${getOrderPrefix(sellerSetting, 'sales')}${id}`
        },
      },
      {
        title: 'CUSTOMER',
        dataIndex: 'vendorName',
      },
      {
        title: 'STATUS',
        dataIndex: 'wholesaleOrderStatus',
      },
      {
        title: 'SHIPPING ADDRESS',
        dataIndex: '',
        key: 'shippingAddress',
        render: (text: any, record: any) => {
          return (
            <span>
              {record.shippingAddress
                ? record.shippingAddress.address.street1 +
                  ',' +
                  record.shippingAddress.address.city +
                  ',' +
                  record.shippingAddress.address.state
                : '-'}
            </span>
          )
        },
      },
      {
        title: orderTitle[currentMenu],
        dataIndex: '',
        key: 's',
        width: 200,
        render: (_text: number, record: any, index: number) => {
          if (currentMenu === 'Orders missing route') {
            return (
              <Select
                placeholder="ASSIGN TO"
                style={{ width: '100%' }}
                onChange={this.assignRoute.bind(this, record.wholesaleOrderId)}
              >
                {this.renderRoutes()}
              </Select>
            )
          }
          if (currentMenu === 'Orders missing driver') {
            return (
              <Select
                placeholder="ASSIGN TO"
                style={{ width: '100%' }}
                onChange={(driverId: number) => {
                  this.reassignDriver(record.wholesaleOrderId, driverId)
                }}
              >
                {this.renderDrivers()}
              </Select>
            )
          }
          return null
        },
      },

      {
        title: '',
        dataIndex: '',
        key: 'g',
        width: 190,
        render: (text: any, order: any) => (
          <Link to={`/sales-order/${order.wholesaleOrderId}`}>
            <span className="goLink">Go To Sales Order</span>
          </Link>
        ),
      },
    ]

    if (currentMenu == 'Orders missing route') {
      return {
        data: unassignedOrders.filter((o) => o.wholesaleOrderStatus !== 'CANCEL'),
        column: orderColumns,
      }
    }
    if (currentMenu == 'Orders missing driver') {
      return {
        data: ordersMissingDriver.filter((o) => o.wholesaleOrderStatus !== 'CANCEL'),
        column: orderColumns,
      }
    }

    return { data: [], column: [] }
  }

  renderDrivers = () => {
    const rows: any[] = []
    const { drivers } = this.props
    drivers.forEach((el, index) => {
      rows.push(
        <Option value={el.id} key={index}>
          {el.firstName + ' ' + el.lastName}
        </Option>,
      )
    })
    return rows
  }

  reassignDriver = (orderId: any, driverId: number) => {
    const data = { wholesaleOrderId: orderId }
    this.props.overrideDriver({ driverId: driverId, data: data })
  }

  assignDriver = (routeId: number, driverId: number) => {
    if (driverId && routeId) {
      const data = { routeId: routeId, driverId: driverId, deliveryWeekofDay: this.props.selectedWeekDay }
      this.props.assignRoute(data)
    }
  }

  assignRoute = (orderId: number, value: number) => {
    if (orderId && value) {
      const data = {
        orderId,
        routeId: value,
        deliveryDate: this.props.selectedWeekDay,
      }
      this.props.assignOrderToRoute(data)
    }
  }

  renderRoutes = () => {
    const { routes, selectedWeekDay } = this.props
    const rows = []
    for (let i = 0; i < routes.length; i++) {
      const route = routes[i]
      if (selectedWeekDay && route.activeDays.indexOf(selectedWeekDay) > -1 && selectedWeekDay.indexOf(route.id) < 0) {
        rows.push(
          <Option key={i} value={route.id}>
            {route.routeName}
          </Option>,
        )
      }
    }
    return rows
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const ListViewTable = withTheme(connect(DeliveryModule)(mapStateToProps)(ListViewTableComponent))
