import styled from '@emotion/styled'

import { white, topLightGrey, mediumGrey } from '~/common'

export const ListViewContainer = styled('div')({
  backgroundColor: white,
  height: `calc(100vh - 189px)`,
  display: 'flex',
})

export const UserList = styled('div')({
  width: 350,
  backgroundColor: topLightGrey,
  overflow: 'auto',
})

export const UserListInner = styled('div')({})

export const RouteContentWrapper = styled('div')({
  width: 'calc(100% - 350px)',
})

export const ListViewSwitcher = styled('div')({
  padding: '0 10px 0 40px',
  marginTop: 20,
})

export const AvatarWrapper = styled('div')({
  // marginLeft: 40,
})

interface UserInfoProps {
  selected?: boolean
  userId?: number
}

export const UserInfoWrapper = styled('div')<UserInfoProps>((props) => ({
  // display: 'flex',
  padding: '5px 0',
  cursor: 'pointer',
  backgroundColor: props.selected ? props.theme.lighter : '',
  paddingBottom: props.userId === 0 ? 20 : 0,
}))

export const Name = styled('div')<UserInfoProps>((props) => ({
  padding: '12px 10px 0',
  color: props.selected ? props.theme.light : '',
  fontWeight: props.selected ? 'bold' : 'normal',
  '& i': {
    color: props.selected ? props.theme.light : '',
  },
  height: 45
}))

export const Routes = styled('div')((props) => ({
  color: mediumGrey,
  margin: 12,
  fontSize: 10,
  textAlign: 'left',
  backgroundColor: props.theme.lighter,
  minHeight: 46,
  padding: 4,
  borderRadius: 4
}))

export const UnAssignedRoutes = styled('div')((props) => ({
  color: props.theme.dark,
  margin: 12,
  fontSize: 16,
  fontWeight: 'bold',
  textAlign: 'left',
  backgroundColor: props.theme.lighter,
  borderRadius: 4,
  minHeight: 46,
  padding: 14
}))

export const Title = styled('div')((props) => ({
  padding: '4px 12px',
}))

export const Content = styled('div')((props) => ({
  padding: '4px 12px',
  fontSize: 14,
  fontWeight: 'bold',
  color: props.theme.dark
}))

export const IconWrapper = styled('span')((props) => ({
  cursor: 'pointer',
  '& path': {
    fill: props.theme.primary,
  },
}))

export const ContentNameStyle = styled('div')({
  fontSize: 20,
  fontWeight: 'bold',
  color: mediumGrey,
  marginTop: 24
})

export const StatisticInfo = styled('div')({
  color: mediumGrey,
  border: `1px solid ${mediumGrey}`,
  borderRadius: 6,
  padding: 4,
  margin: 2,
  fontSize: 10,
  textAlign: 'center',
  minHeight: 46
})

export const statisticValueStyle: React.CSSProperties = {
  fontSize: 14,
  fontWeight: 'bold',
  margin: '8px 0'
}

export const RouteTable = styled('div')((props)=> ({
  '& .ant-table-tbody tr': {
    transition: 'none !important'
  },
  '& .ant-table-tbody tr:hover td': {
    backgroundColor: '#FFF !important'
  },
  '& .goLink': {
    background: props.theme.primary,
    padding: 10,
    borderRadius: 20, 
    fontSize: 12,
    color: white,
    fontWeight: 'bold'
  },
  '& table tbody tr td:nth-of-type(7) span': {
    background: props.theme.primary,
    padding: 10,
    borderRadius: 20, 
    fontSize: 12,
    color: white,
    fontWeight: 'bold'
  },
}))