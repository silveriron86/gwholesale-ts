import * as React from 'react'
import { CardTable } from '../route-card/card-table'
import { Row, Col, Avatar, Icon as AntdIcon } from 'antd'
import { white } from '~/common'
import { Icon } from '~/components'
import { AccountUser, WholesaleRoute } from '~/schema'
import {
  RouteContentWrapper,
  Routes,
  UnAssignedRoutes,
  RouteTable,
  Title,
  ContentNameStyle,
  StatisticInfo,
  statisticValueStyle,
  Content,
  IconWrapper
} from './listview.style'
import { ListViewTable } from './listview-table'
import { DailyDeliveryProps, DeliveryDispatchProps, DeliveryModule } from '../../delivery.module'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { EditRoutePopup } from '../modal/edit-route-popup'
import { getFileUrl } from '~/common/utils'
import PrintDeliveryModal from '../route-card/print-modal'
import { Flex } from '~/modules/customers/customers.style'
import SelectDocumentModal from '../route-card/select-modal'

type RouteContentProps = DeliveryDispatchProps & DailyDeliveryProps & {
  selectedDriver: AccountUser | null
  currentMenu: string
}

export class RouteContentComponent extends React.PureComponent<RouteContentProps> {
  state: any
  constructor(props: RouteContentProps) {
    super(props)

    this.state = {
      visiblePrintModal: false,
      visibleSelectModal: false,
      visiblity: false,
      selectedDocuments: null,
      selectedRoutes: [],
      selectedType: 1
    }
  }
  componentDidMount() { }

  componentDidUpdate(prevProps: any) {
    if (prevProps.unassignedRoutes && prevProps.unassignedRoutes.length != this.props.unassignedRoutes.length) {
      this.props.getDriverSchedules()
    }
  }
  handleSelecteType = (selectedType: number) => {
    this.setState({ selectedType })
  }
  render() {
    const { selectedDriver, currentMenu, deliveries } = this.props
    const { visiblity, visiblePrintModal, visibleSelectModal, selectedDocuments, selectedType } = this.state
    const data = this.getIdInfo()
    let delivery = null
    if (selectedDriver && deliveries) {
      delivery = deliveries.filter(el => el.driver.userId == selectedDriver.id.toString())[0]
    }
    const orders = delivery ? delivery.orders : []
    const newStatusIdsByOrders = orders.filter((el: any) => el.status === 'NEW').map(v => v.wholesaleOrderId)

    return (
      <RouteContentWrapper>
        <Row>
          {selectedDriver &&
            <Col md={6}>
              <Row style={{ marginTop: 12 }}>
                <Col md={6} style={{ padding: 8 }}>
                  <Avatar size={50} icon="user" src={selectedDriver ? getFileUrl(selectedDriver.imagePath, false) : ''} />
                </Col>

                <Col md={18} style={{ textAlign: 'left' }}>
                  <ContentNameStyle>
                    {selectedDriver.firstName + ' ' + selectedDriver.lastName}
                  </ContentNameStyle>
                </Col>
              </Row>
            </Col>
          }
          <Col md={12}>
            <Row style={{ marginTop: 10 }}>
              {selectedDriver &&
                <Col md={2} style={{ marginTop: 24 }}>
                  <Flex style={{ alignItems: 'center' }}>
                    <IconWrapper onClick={this.onToggleSelect}>
                      <AntdIcon type="printer" theme="filled" style={{ fontSize: 16 }} />
                    </IconWrapper>
                    <div style={{ width: 15 }}></div>
                    <IconWrapper onClick={this.onClickEdit}>
                      <Icon type="edit" viewBox={void 0} width={20} height={16} color={white} />
                    </IconWrapper>
                    <EditRoutePopup
                      visible={visiblity}
                      direction="left"
                      driverId={selectedDriver.id.toString()}
                      cardRoutes={data.routes}
                      onClickOutside={this.onClickEdit}
                    />
                  </Flex>
                </Col>
              }
              {/* {selectedDriver &&
                <Col md={2} style={{ marginTop: 24 }}>
                  <IconWrapper onClick={this.onClickPrint}>
                    <Icon type="printer" viewBox={void 0} width={20} height={16} color={white} />
                  </IconWrapper>
                </Col>
              } */}
              <Col md={20}>
                {selectedDriver &&
                  <Routes>
                    <Title>
                      ROUTES
                    </Title>
                    <Content>
                      {this.renderRouteNames()}
                    </Content>
                  </Routes>
                }
                {!selectedDriver &&
                  <UnAssignedRoutes>
                    {currentMenu}
                  </UnAssignedRoutes>
                }
              </Col>
            </Row>
          </Col>
          {selectedDriver &&
            <Col md={6}>
              <Row style={{ padding: 20 }}>
                <Col md={12}>
                  <StatisticInfo>
                    <div>COMPLETED ORDERS</div>
                    <div style={statisticValueStyle}>{data.completedOrders}/{data.totalOrders}</div>
                  </StatisticInfo>
                </Col>
                <Col md={12}>
                  <StatisticInfo>
                    <div>COMPLETED UNITS</div>
                    <div style={statisticValueStyle}>{data.totalUnits}</div>
                  </StatisticInfo>
                </Col>
              </Row>
            </Col>
          }
        </Row>

        <RouteTable>
          <ListViewTable selectedDriver={selectedDriver} currentMenu={currentMenu} />
        </RouteTable>
        {delivery !== null && (
          <>
            <SelectDocumentModal selectedType={selectedType} handleSelecteType={this.handleSelecteType} delivery={delivery} visible={visibleSelectModal} onToggle={this.onToggleSelect} onView={this.onViewSelected} handleSelectRoute={(selectedRoutes) => this.setState({ selectedRoutes })} />
            {selectedDocuments !== null && this.state.visiblePrintModal && (
              <PrintDeliveryModal {...this.props} selectedType={selectedType} delivery={delivery} selected={selectedDocuments} newStatusIdsByOrders={newStatusIdsByOrders} onToggle={this.onClickPrint} selectedRoutes={this.state.selectedRoutes} />
            )}
          </>
        )}
      </RouteContentWrapper>
    )
  }

  onViewSelected = (state: any) => {
    this.setState({
      selectedDocuments: state,
      visibleSelectModal: false,
      visiblePrintModal: true
    })
  }

  getIdInfo = () => {
    const { deliveries, selectedDriver } = this.props
    const emptyResult = {
      totalOrders: 0,
      completedOrders: 0,
      totalUnits: 0,
      routes: []
    }
    if (selectedDriver && deliveries && deliveries.length > 0) {
      const delivery = deliveries.filter(el => el.driver.userId == selectedDriver.id.toString())[0]
      if (delivery != undefined) {
        const routes: WholesaleRoute[] = delivery ? delivery.routes : [];
        const totalOrders = delivery ? delivery.totalOrders : 0;
        const totalUnits = delivery ? delivery.totalUnits : 0;
        const orders = delivery ? delivery.orders : [];

        let completedOrdersCount = 0
        for (let i = 0; i < orders.length; i++) {
          if (orders[i].status == 'PLACED') {
            completedOrdersCount = completedOrdersCount + 1
          }
        }
        return {
          totalOrders: totalOrders,
          totalUnits: totalUnits,
          completedOrders: completedOrdersCount,
          routes: routes
        }
      } else {
        return emptyResult
      }
    } else {
      return emptyResult
    }
  }

  renderRouteNames = () => {
    const { deliveries, selectedDriver } = this.props
    const routeNames: string[] = []
    if (selectedDriver && deliveries) {
      let delivery = deliveries.filter(el => el.driver.userId == selectedDriver.id.toString())[0]
      if (delivery != undefined) {
        const routes = delivery.routes;
        routes.forEach(route => {
          routeNames.push(route.routeName)
        });
      }
    }

    return routeNames.join(', ')
  }

  onClickEdit = (visible: boolean) => {
    if (this.state.visiblity) {
      this.setState({ visiblity: false })
    } else {
      this.setState({
        visiblity: visible,
      })
    }
  }

  onClickPrint = () => {
    this.setState({
      visiblePrintModal: !this.state.visiblePrintModal,
    })
  }

  onToggleSelect = () => {
    this.setState({
      visibleSelectModal: !this.state.visibleSelectModal,
    })
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const RouteContent = withTheme(connect(DeliveryModule)(mapStateToProps)(RouteContentComponent))

