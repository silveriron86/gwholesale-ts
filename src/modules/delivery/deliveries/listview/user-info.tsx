import * as React from 'react'
import { Avatar, Icon, Row, Col } from 'antd'

import { AvatarWrapper, UserInfoWrapper, Name } from './listview.style'
import { User, AccountUser } from '~/schema'
import { getFileUrl } from '~/common/utils'


export interface UserInfoProps {
  user?: User
  selected?: boolean
  onClick: Function
  driver: AccountUser | null
  menuName: string,
  currentMenu: string
}

export class UserInfoComponent extends React.PureComponent<UserInfoProps> {
  componentDidMount() { }

  render() {
    const { onClick, selected, driver, menuName, currentMenu } = this.props //  userId in UserInfoWrapper is used for testing when create unassigned route item
    return (
      <UserInfoWrapper onClick={onClick.bind(this, driver, menuName)} selected={selected || currentMenu == menuName}>
        <Row>
          <Col md={2} xs={2}>
          </Col>
          {driver != null &&
            <Col md={4} xs={4}>
              <AvatarWrapper>{<Avatar size={40} icon="user" src={driver ? getFileUrl(driver.imagePath, false) : ''} />
              }</AvatarWrapper>
            </Col>
          }
          <Col md={driver != null ? 18 : 22} xs={driver != null ? 18 : 22}>
            <Row justify="space-between" type="flex">
              <Name selected={selected || currentMenu == menuName} style={{ marginLeft: !driver ? 20 : 0, flex: 1, textAlign: "left" }}>
                <span>{!driver ? menuName : driver.firstName + ' ' + driver.lastName}</span>
              </Name>
              {(selected || currentMenu == menuName) && (
                <Name selected={selected || currentMenu == menuName} style={{ width: "30px" }}>
                  <Icon type="right" />
                </Name>
              )}
            </Row>
          </Col>
        </Row>
      </UserInfoWrapper>
    )
  }
}
