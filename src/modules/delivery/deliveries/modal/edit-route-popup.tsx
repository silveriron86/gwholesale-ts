import * as React from 'react'
import { Select, Row, Popconfirm } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'

import { GlobalState } from '~/store/reducer'
import { DeliveryModule, DeliveryDispatchProps, DailyDeliveryProps } from '../../delivery.module'

import {
  EditModalWrapper,
  ModalArrow,
  EditPopupBody,
  EditPopupTitle,
  SelectorStyle,
  ChangeRouteWrapper,
  CardHeaderTitle,
  RemoveRoute,
} from './modal.style'
import { Icon as IconSvg } from '~/components/icon/'
import { WholesaleRoute } from '~/schema'
import { PayloadIcon } from '~/components/elements/elements.style'
import { Flex } from '~/modules/inventory/components/inventory-header.style'

const { Option } = Select

type EditRoutePopupProps = DeliveryDispatchProps &
  DailyDeliveryProps & {
    visible: boolean
    direction: string
    driverId: string
    cardRoutes: WholesaleRoute[]
    onClickOutside: Function
  }

export class EditRoutePopupComp extends React.PureComponent<EditRoutePopupProps> {
  state: any
  wrapperRef: any
  constructor(props: EditRoutePopupProps) {
    super(props)
    console.log(props)
    this.state = {
      selectedDriverId: !isNaN(parseInt(props.driverId)) ? parseInt(props.driverId) : '',
      cardRoutes: props.cardRoutes,
      driverId: props.driverId,
    }

    this.setWrapperRef = this.setWrapperRef.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
  }

  componentDidUpdate(prevProps: any) {
    if (this.state.cardRoutes.length != this.props.cardRoutes.length) {
      this.setState({ cardRoutes: this.props.cardRoutes })
    }
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillMount() {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  setWrapperRef(node: any) {
    this.wrapperRef = node
  }

  handleClickOutside(event: any) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.props.onClickOutside()
    }
  }

  handleAssignRoute = (value: number) => {
    const data = {
      routeId: value,
      driverId: this.state.selectedDriverId,
      deliveryWeekofDay: this.props.selectedWeekDay,
    }
    this.props.assignRoute(data)
  }

  renderDrivers = () => {
    const rows: any[] = []
    const { drivers } = this.props
    drivers.forEach((el, index) => {
      rows.push(
        <Option value={el.id} key={index}>
          {el.firstName + ' ' + el.lastName}
        </Option>,
      )
    })

    return rows
  }

  renderUnassignedRoutes = () => {
    const { unassignedRoutes, selectedWeekDay } = this.props
    const options: any[] = []
    if (unassignedRoutes && unassignedRoutes.length > 0) {
      unassignedRoutes.forEach((el, index) => {
        if (el.activeDays.toLowerCase().indexOf(selectedWeekDay.toLowerCase()) > -1) {
          options.push(
            <Option value={el.id} key={index}>
              {el.routeName}
            </Option>,
          )
        }
      })
    }
    return options
  }

  reAssignDriver = (value: number) => {
    console.log(this.state.cardRoutes)
    if (this.state.cardRoutes.length > 0) {
      this.props.reAssignDriver({ driverId: value, routes: this.state.cardRoutes })
    }
  }

  renderAssignRoute = () => {
    const { cardRoutes } = this.props
    const routeDom: any[] = []
    if (cardRoutes && cardRoutes.length > 0) {
      cardRoutes.forEach((el, index) => {
        routeDom.push(
          <RemoveRoute key={el.id}>
            <div style={{ flex: 1 }}>{el.routeName}</div>
            <Popconfirm title="Sure to delete route?" onConfirm={() => this.removeRoute(el.id)}>
              <a style={{ width: '50px', marginTop: 5, marginRight: -30 }}>
                <IconSvg type="dark-close" viewBox="0 0 24 24" width={24} height={24} />
              </a>
            </Popconfirm>
          </RemoveRoute>,
        )
      })
    } else {
      routeDom.push(<RemoveRoute>No Routes</RemoveRoute>)
    }

    return routeDom
  }

  removeRoute = (routeId: number) => {
    this.props.deleteDriverSchedule({ routeId, weekDay: this.props.selectedWeekDay, curDate: this.props.curDate })
  }

  render() {
    const { visible, direction } = this.props
    let driverId = this.state.selectedDriverId
    if (!isNaN(parseInt(driverId))) {
      driverId = parseInt(driverId)
    } else {
      driverId = ''
    }
    return (
      <EditModalWrapper visible={visible} direction={direction} ref={this.setWrapperRef}>
        <ModalArrow direction={direction} />
        <EditPopupBody>
          <Row>
            {/* <EditPopupTitle>CHANGE DRIVER</EditPopupTitle>
            <Select style={SelectorStyle} onChange={this.reAssignDriver}>
              {this.renderDrivers()}
            </Select> */}
            <div>
              <EditPopupTitle style={{ marginBottom: 3 }}>Edit Routes</EditPopupTitle>
              {this.renderAssignRoute()}
            </div>
          </Row>
          <Row style={{ marginTop: 5 }}>
            <ChangeRouteWrapper>
              {/* {this.renderRouteSelectors(driverId)} */}
              <EditPopupTitle>Assign Routes</EditPopupTitle>
              <Select style={SelectorStyle} onChange={this.handleAssignRoute}>
                {this.renderUnassignedRoutes()}
              </Select>
            </ChangeRouteWrapper>
          </Row>
        </EditPopupBody>
      </EditModalWrapper>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const EditRoutePopup = withTheme(connect(DeliveryModule)(mapStateToProps)(EditRoutePopupComp))
