import styled from '@emotion/styled'
import { Button } from 'antd'
import { white } from '~/common'

interface EditModalProps {
  visible: boolean
  direction: string
}

export const EditModalWrapper = styled('div')<EditModalProps>((props) => ({
  opacity: props.visible ? 1 : 0,
  visibility: props.visible ? 'visible' : 'hidden',
  transition: 'all 300ms ease-in-out',
  position: 'absolute',
  backgroundColor: white,
  width: props.visible ? 390 : 100,
  top: props.direction == 'right' ? (props.visible ? 60 : 40) : props.visible ? 30 : 10,
  left: props.direction == 'right' ? (props.visible ? -1 : 290) : props.visible ? 0 : -5,
  backgroundClip: 'padding-box',
  borderRadius: 4,
  boxShadow: 'rgba(0, 0, 0, 0.15) 0px 8px 10px 3px',
  textAlign: 'left',
  zIndex: 100,
}))

interface ModalArrowProps {
  direction: string
}

export const ModalArrow = styled('div')<ModalArrowProps>((props) => ({
  position: 'absolute',
  top: -4,
  right: props.direction == 'right' ? 25 : 'auto',
  left: props.direction == 'left' ? 10 : 'auto',
  backgroundColor: white,
  borderTopColor: white,
  borderRightColor: 'transparent',
  borderBottomColor: 'transparent',
  borderLeftColor: white,
  boxShadow: '-2px -2px 5px rgba(0, 0, 0, 0.06)',
  transform: 'rotate(45deg)',
  width: 10,
  height: 10,
}))

export const EditPopupBody = styled('div')({
  padding: 16,
})

export const EditPopupTitle = styled('div')((props) => ({
  color: props.theme.main,
  fontFamily: 'Arial',
  fontWeight: 'bold',
  fontSize: 15,
  marginBottom: 6,
}))

export const EditPopupContent = styled('div')({})

export const SelectorStyle: React.CSSProperties = {
  width: '100%',
}

export const ChangeRouteWrapper = styled('div')((props) => ({
  '& .ant-table-tbody tr': {
    transition: 'none !important',
  },
  '& .ant-table-tbody tr:hover td': {
    backgroundColor: '#FFF !important',
  },
  '& .ant-table-tbody tr td': {
    padding: '0 !important',
  },
  '& .ant-select': {
    width: '100%',
  },
  '& table .ant-select-selection': {
    border: 'none !important',
  },
}))

export const CardHeaderTitle = styled('span')((props) => ({
  color: props.theme.light,
  fontSize: '20px',
  lineHeight: '180%',
  letterSpacing: '0.05em',
}))

export const RemoveRoute = styled('div')((props) => ({
  color: 'rgba(0, 0, 0, 0.85)',
  fontWeight: 'bold',
  fontSize: 12,
  marginBottom: 5,
  marginTop: 5,
  display: 'flex',
  alignItems: 'center',
}))
