import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { Count, Number, Title } from './card.style'

interface CardHeaderCountProps {
  title: string
  count: string
}

export class CardHeaderCountComponent extends React.PureComponent<CardHeaderCountProps> {
  componentDidMount() {}

  render() {
    const { count, title } = this.props
    return (
      <Count>
        <Number>{count}</Number>
        <Title>{title}</Title>
      </Count>
    )
  }
}

export const CardHeaderCount = withTheme(CardHeaderCountComponent)
