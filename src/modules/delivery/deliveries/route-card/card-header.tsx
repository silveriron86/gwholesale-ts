import * as React from 'react'
import { Avatar, Icon } from 'antd'
import { withTheme } from 'emotion-theming'
import { CardHeaderWrapper, CardHeaderTitle, CardIconsWrapper, IconWrapper, AvatarWrapper } from './card.style'
import { Icon as IconSVG } from '~/components'
import { white } from '~/common'
import { AccountUser } from '~/schema'
import { getFileUrl } from '~/common/utils'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import consts from '../consts'


interface CardHeaderProps {
  name: string
  visibleState: boolean
  onClickEdit: Function
  user: AccountUser | null
  onClickPrint: Function
}

export class CardHeaderComponent extends React.PureComponent<CardHeaderProps> {
  state = {
    clicked: false,
    visiblePrintModal: false
  }
  componentDidMount() { }

  componentDidUpdate(prevProps: any) {
    if (this.state.clicked && !this.props.visibleState) {
      this.setState({
        clicked: false,
      })
    }
  }

  render() {
    const { name, user } = this.props
    return (
      <CardHeaderWrapper isLighter={name!==consts.driver} style={{ marginBottom: 40 }}>
        {name === consts.ordersMissingRoute && <CardHeaderTitle>Orders missing route</CardHeaderTitle>}
        {name === consts.ordersMissingDriver && <CardHeaderTitle>Orders missing driver</CardHeaderTitle>}
        {name === consts.unassignedRoutes && <CardHeaderTitle>Unassigned Routes</CardHeaderTitle>}
        {name === consts.driver && (
          <CardIconsWrapper style={{ position: 'relative' }}>
            {/* <IconWrapper onClick={this.onClickPrint}>
              <Icon type="printer" viewBox={void 0} width={20} height={16} color={white} />
            </IconWrapper> */}
            <AvatarWrapper>
              <Avatar size={64} icon="user" src={user ? getFileUrl(user.imagePath, false) : ''} />
            </AvatarWrapper>
            <div></div>
            <Flex style={{ alignItems: 'center'}}>
              <IconWrapper onClick={this.props.onClickPrint}>
                <Icon type="printer" theme="filled" style={{fontSize: 16}} />
              </IconWrapper>
              <div style={{width: 15}}></div>
              <IconWrapper onClick={this.onClickEdit}>
                <IconSVG type="edit" viewBox={void 0} width={20} height={16} color={white} />
              </IconWrapper>
              </Flex>
          </CardIconsWrapper>
        )}
      </CardHeaderWrapper>
    )
  }

  onClickEdit = () => {
    const { clicked } = this.state
    this.setState({
      clicked: !clicked,
    })
    this.props.onClickEdit(!clicked)
  }
}

export const CardHeader = withTheme(CardHeaderComponent)
