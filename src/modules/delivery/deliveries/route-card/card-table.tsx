import * as React from 'react'
import { connect } from 'redux-epics-decorator';
import { Table, Modal, Row, Select } from 'antd'
import { withTheme } from 'emotion-theming'

import { DeliveryDispatchProps, DailyDeliveryProps, DeliveryModule } from '../..';
import { ThemeSelect, ThemeButton } from '~/modules/customers/customers.style';
import { GlobalState } from '~/store/reducer';
import {
  ModalWrapper,
  ColWrapper,
  ModalTextStyle,
  smallTextStyle,
  bigTextStyle,
  buttonStyle
} from './card.style';
import consts from '../consts'

export type CardTableProps = DeliveryDispatchProps & DailyDeliveryProps & {
  columns?: Array<any>
  datasource?: Array<any>
  name: string
  driverName?: string
  bordered?: boolean
}

const { Option } = Select
export class CardTableComponent extends React.PureComponent<CardTableProps> {
  state: any
  constructor(props: CardTableProps) {
    super(props);
    this.state = {
      visibleEditOrderModal: false,
      destination: '',
      driver: props.driverName,
      deliveryDestination: '',
      selectedOrder: null,
      selectedUnassignedRouteId: null,
      selectedAssigningDriverId: null,
      selectedUnassignedOrderId: null,
      selectedUnassignedOrderRouteId: null,
      visibleAssignOrderModal: false,
      assignDriverForOrderModal: false,
    }
  }

  goToOrder = () => {
    const { selectedUnassignedOrderId } = this.state;
    window.location.href = `#/sales-order/${selectedUnassignedOrderId}`
  }

  componentDidMount() { }

  render() {
    const { datasource, columns, bordered } = this.props
    const tableData = this.formatTableData(datasource);
    return (
      <div>
        <Table
          bordered={bordered === true ? true : false}
          dataSource={tableData}
          columns={columns}
          pagination={false}
          rowKey="id"
          size="small"
          onRow={(record, rowIndex) => {
            return {
              onClick: event => { this.onRowDoubleClick(record, rowIndex) }
            }
          }} />

        <ModalWrapper
          title="Edit Sales Order"
          centered
          visible={this.state.visibleEditOrderModal}
          width={700}
          onCancel={this.handleCancel}
          footer={null}
        >
          <Row style={{ marginBottom: 30 }}>
            <ColWrapper md={12}>
              <ModalTextStyle>
                <div style={smallTextStyle}>DELIVERY ADDRESS</div>
                <div style={bigTextStyle}>{this.state.destination}</div>
              </ModalTextStyle>
            </ColWrapper>
            <ColWrapper md={12}>
              <ModalTextStyle>
                <div style={smallTextStyle}>NAME OF DRIVER</div>
                <div style={bigTextStyle}>{this.state.driver}</div>
              </ModalTextStyle>
            </ColWrapper>
          </Row>
          <Row style={{ marginBottom: 20, padding: 20 }}>
            <ThemeSelect onChange={this.reAssignOrder} placeholder="REASSIGN TO" style={{ width: '50%' }}>
              {this.renderDrivers()}
            </ThemeSelect>
          </Row>
          <Row type="flex" justify="center">
            <ThemeButton style={buttonStyle} onClick={this.handleOk}>Show Sales Order</ThemeButton>
            <ThemeButton style={buttonStyle} onClick={this.handleCancel}>Cancel</ThemeButton>
          </Row>
        </ModalWrapper>
        <ModalWrapper
          title="Assign Routes"
          centered
          visible={this.state.visibleAssigningModal}
          width={400}
          onCancel={this.handleCancelAssignModal}
          footer={null}
        >
          <Row style={{ marginBottom: 20, padding: 20, textAlign: 'center' }}>
            <ThemeSelect onChange={this.changeDriver} value={this.state.selectedAssigningDriverId} placeholder="REASSIGN TO" style={{ width: '100%' }}>
              {this.renderDrivers()}
            </ThemeSelect>
          </Row>
          <Row type="flex" justify="center">
            <ThemeButton style={buttonStyle} onClick={this.saveAssigning}>Save</ThemeButton>
            <ThemeButton style={buttonStyle} onClick={this.handleCancelAssignModal}>Cancel</ThemeButton>
          </Row>
        </ModalWrapper>
        <ModalWrapper
          title="Assign Driver"
          centered
          visible={this.state.assignDriverForOrderModal}
          width={400}
          onCancel={this._cancelAssignDriverForOrder}
          footer={null}
        >
          <Row style={{ marginBottom: 20, padding: 20, textAlign: 'center' }}>
            <ThemeSelect onChange={(val: number) => this.setState({selectMissingDriverId: val})} value={this.state.selectMissingDriverId} style={{ width: '100%' }}>
              {this.renderDrivers()}
            </ThemeSelect>
          </Row>
          <Row type="flex" justify="center">
            <ThemeButton style={buttonStyle} onClick={() => { window.location.href = `#/sales-order/${this.state.missingDriverOrderId}` }}>Go to Sales Order</ThemeButton>
            <ThemeButton style={buttonStyle} onClick={this._saveAssignDriverForOrder}>Save</ThemeButton>
            <ThemeButton style={buttonStyle} onClick={this._cancelAssignDriverForOrder}>Cancel</ThemeButton>
          </Row>
        </ModalWrapper>
        <ModalWrapper
          title="Assign Orders"
          centered
          visible={this.state.visibleAssignOrderModal}
          width={400}
          onCancel={this.handleCancelOrderModal}
          footer={null}
        >
          <Row style={{ marginBottom: 20, padding: 20, textAlign: 'center' }}>
            <ThemeSelect onChange={this.changeRoute} value={this.state.selectedUnassignedOrderRouteId} placeholder="REASSIGN TO" style={{ width: '100%' }}>
              {this.renderRouters()}
            </ThemeSelect>
          </Row>
          <Row type="flex" justify="center">
            <ThemeButton style={buttonStyle} onClick={this.goToOrder}>Go to Sales Order</ThemeButton>
            <ThemeButton style={buttonStyle} onClick={this.saveAssignOrder}>Save</ThemeButton>
            <ThemeButton style={buttonStyle} onClick={this.handleCancelOrderModal}>Cancel</ThemeButton>
          </Row>
        </ModalWrapper>
      </div>)

  }

  renderDrivers = () => {
    const { drivers } = this.props
    const rows = [];
    for (let i = 0; i < drivers.length; i++) {
      rows.push(
        <Option key={i} value={drivers[i].id}>{drivers[i].firstName + ' ' + drivers[i].lastName}</Option>
      )
    }
    return rows;
  }

  renderRouters = () => {
    const { routes, selectedWeekDay } = this.props
    const rows = [];
    for (let i = 0; i < routes.length; i++) {
      const route = routes[i];
      if (selectedWeekDay && route.activeDays.indexOf(selectedWeekDay) > -1 && selectedWeekDay.indexOf(route.id) < 0) {
        rows.push(<Option key={i} value={route.id}>{route.routeName}</Option>)
      }
    }
    return rows;
  }
  reAssignOrder = (value: any) => {
    const { drivers } = this.props
    let driver = drivers.filter(el => el.id == value)[0]
    if (driver && driver != undefined) {
      const orderId = this.state.selectedOrder.wholesaleOrderId
      this.props.overrideDriver({ driverId: value, data: { wholesaleOrderId: orderId } })
      this.setState({ driver: driver.firstName + ' ' + driver.lastName })
    }
  }
  handleOk = () => {
    const { selectedOrder } = this.state

    if (selectedOrder) {
      const orderId = selectedOrder.wholesaleOrderId
      this.setState({
        visibleEditOrderModal: false,
        selectedOrder: null
      })
      window.location.href = "#/sales-order/" + orderId
    }
  }

  handleCancel = () => {
    this.setState({
      visibleEditOrderModal: false,
      selectedOrder: null
    })
  }

  handleCancelAssignModal = () => {
    this.setState({
      visibleAssigningModal: false,
      selectedUnassignedRouteId: null,
      selectedAssigningDriverId: null
    })
  }

  handleCancelOrderModal = () => {
    this.setState({
      visibleAssignOrderModal: false,
      selectedUnassignedOrderId: null,
      selectedUnassignedOrderRouteId: null
    })
  }

  _cancelAssignDriverForOrder = () => {
    this.setState({
      assignDriverForOrderModal: false,
      missingDriverOrderId: null,
      selectMissingDriverId: null,
    })
  }

  _saveAssignDriverForOrder = () => {
    const { selectMissingDriverId, missingDriverOrderId } = this.state
    this.props.overrideDriver({ driverId: selectMissingDriverId, data: { wholesaleOrderId: missingDriverOrderId } })
    this._cancelAssignDriverForOrder()
  }

  onRowDoubleClick = (record: any, rowIndex: number) => {
    const { drivers, name } = this.props
    if (name == consts.ordersMissingRoute) {  //assign order: ;
      this.setState({
        visibleAssignOrderModal: true,
        selectedUnassignedOrderId: record.wholesaleOrderId
      })
    } else if (name == consts.unassignedRoutes) {  //assign route
      this.setState({
        visibleAssigningModal: true,
        selectedUnassignedRouteId: record.id
      })
    } else if (name == consts.driver) { //assign driver
      this.setState({
        visibleEditOrderModal: true,
        selectedOrder: record,
        destination: record.deliveryDestination,
      })
    } else if (name === consts.ordersMissingDriver) {
      this.setState({
        assignDriverForOrderModal: true,
        missingDriverOrderId: record.wholesaleOrderId,
      })
    }
  }

  formatTableData(data?: Array<any>) {
    if (data) {
      data.forEach(element => {
        element.wholesaleOrderStatus = element.status
        if (element.status === true || element.status == 'PLACED') {
          element.status = true;
        } else {
          element.status = false;
        }
        if (element.wholesaleOrderId) {
          element.id = element.wholesaleOrderId
        }
      });
    }
    return data;
  }

  changeDriver = (value: number) => {
    this.setState({
      selectedAssigningDriverId: value
    })
  }
  changeRoute = (value: number) => {
    this.setState({
      selectedUnassignedOrderRouteId: value
    })
  }
  saveAssigning = () => {
    const { selectedUnassignedRouteId, selectedAssigningDriverId } = this.state
    if (selectedAssigningDriverId && selectedUnassignedRouteId) {
      const data = { routeId: selectedUnassignedRouteId, driverId: selectedAssigningDriverId, deliveryWeekofDay: this.props.selectedWeekDay }
      this.props.assignRoute(data)
    }
    this.handleCancelAssignModal()
  }
  saveAssignOrder = () => {
    const { selectedUnassignedOrderId, selectedUnassignedOrderRouteId } = this.state
    if (selectedUnassignedOrderId && selectedUnassignedOrderRouteId) {
      const data = { routeId: selectedUnassignedOrderRouteId, orderId: selectedUnassignedOrderId, deliveryDate: this.props.selectedWeekDay }
      this.props.assignOrderToRoute(data)
    }
    this.handleCancelOrderModal();
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const CardTable = withTheme(connect(DeliveryModule)(mapStateToProps)(CardTableComponent))
