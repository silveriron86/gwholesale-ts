import * as React from 'react'
import { notification, Row, Col, Divider, Modal } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { cloneDeep } from 'lodash'

import { CardHeader } from './card-header'
import { CardTable } from './card-table'
import { CardHeaderCount } from './card-header-counts'
import { EditRoutePopup } from '../modal/edit-route-popup'
import { GlobalState } from '~/store/reducer'
import { DeliveryModule, DeliveryDispatchProps, DailyDeliveryProps } from '../../delivery.module'
import { formatOrderStatus, getOrderPrefix } from '~/common/utils'
import { WholesaleDelivery, WholesaleRoute, OrderDetail } from '~/schema'
import consts from '../consts'

import {
  CardContainer,
  CardName,
  Routes,
  Route,
  DividerStyle,
  ShowMore,
  TableWrapper,
  DeliveryOrdersList,
  VersionNumber,
  ManifestModal,
} from './card.style'
import { Icon as IconSvg } from '~/components/icon/'
import { Icon } from 'antd'
import { Theme } from '~/common'
import { ThemeButton } from '~/modules/customers/customers.style'
import moment from 'moment'
import PrintDeliveryModal from './print-modal'
import SelectDocumentModal from './select-modal'

export const unAssignedRouteColumns = [
  {
    title: 'SO #',
    dataIndex: 'id',
  },
  {
    title: 'ROUTE NAME',
    dataIndex: 'routeName',
  },
]


export type RouteCardProps = DeliveryDispatchProps &
  DailyDeliveryProps & {
    name: string
    delivery?: WholesaleDelivery
    unAssignedRoutes?: WholesaleRoute[]
    unAssignedOrders?: OrderDetail[]
    ordersMissingDriverNoCancel?: any[]
    cardName?: string
    theme: Theme
  }

export class RouteCardComponent extends React.PureComponent<RouteCardProps> {
  state: any
  unAssignedOrderColumns: any[]
  constructor(props: RouteCardProps) {
    super(props)
    this.state = {
      visiblity: false,
      showMore: false,
      routesCount: props.delivery ? props.delivery.routes.length : 0,
      visiblePrintModal: false,
      visibleSelectModal: false,
      printDeliveryRef: null,
      selectedDocuments: null,
      selectedRoutes: [],
      selectedType: 1
    }

    const { theme } = props
    this.unAssignedOrderColumns = [
      {
        title: 'SO #',
        dataIndex: 'wholesaleOrderId',
        render: (id: string) => {
          return `${getOrderPrefix(props.sellerSetting, 'sales')}${id}`
        }
      },
      {
        title: 'COMPANY NAME',
        dataIndex: 'vendorName',
      },
    ]

    this.state.assignedRouteColumns = [
      // {
      //   title: '',
      //   dataIndex: 'status',
      //   width: 40,
      //   render: (data: any, record: any) => {
      //     return (
      //       <div>
      //         {data ? (
      //           <div>
      //             <IconSvg type="check" viewBox="0 0 15 14" width={15} height={14} style={{ fill: 'none' }} />
      //           </div>
      //         ) : (
      //           <div />
      //         )}
      //       </div>
      //     )
      //   },
      // },
      {
        title: 'SO#',
        dataIndex: 'wholesaleOrderId',
        render: (id: string) => {
          return `${getOrderPrefix(props.sellerSetting, 'sales')}${id}`
        }
      },
      {
        title: 'Status',
        dataIndex: 'wholesaleOrderStatus',
        render: (status: string) => {
          return `${formatOrderStatus(status, props.sellerSetting)}`
        }
      },
      {
        title: 'Customer',
        dataIndex: 'customer',
      },
      {
        title: 'Ver',
        dataIndex: 'versionNumber',
        width: 40,
        render: (versionNumber: number, record: any) => {
          return (
            <VersionNumber
              style={{
                backgroundColor: versionNumber > 1 ? '#FFDE68' : 'transparent',
                fontWeight: record.hasWorkOrder ? 'bold' : 'normal',
              }}
            >
              {versionNumber}
            </VersionNumber>
          )
        },
      },
      {
        title: 'Ord',
        dataIndex: 'sumOfUnits',
        width: 80,
        render: (data: any, record: any) => {
          return (
            <div>
              {record.hasWorkOrder === true && (
                <span style={{ color: theme.primary, marginRight: 5 }}>
                  <Icon type="star" theme="filled" />
                </span>
              )}
              {data}
            </div>
          )
        },
      },
      {
        title: 'Picked',
        dataIndex: 'sumOfPicks',
        width: 80,
        render: (data: any, record: any) => {
          return <div>{data}</div>
        },
      },
    ]
  }

  componentDidMount() { }

  componentDidUpdate(prevProps: any) {
    if (this.props.delivery) {
      if (this.props.delivery.routes.length != this.state.routesCount) {
        this.setState({ routesCount: this.props.delivery.routes.length })
        // this.props.getUnassignedRoutes(this.props.selectedWeekDay)
        this.props.getDriverSchedules()
      }
    }
  }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Go to Pricesheet below to place a new order',
      onClick: () => { },
    })
  }

  onClickEdit = (visible: boolean) => {
    this.setState({
      visiblity: visible,
    })
  }

  onClickPrint = () => {
    this.setState({
      visiblePrintModal: !this.state.visiblePrintModal,
    })
  }

  onToggleSelect = () => {
    this.setState({
      visibleSelectModal: !this.state.visibleSelectModal,
    })
  }

  onViewSelected = (state: any) => {
    this.setState({
      selectedDocuments: state,
      visibleSelectModal: false,
      visiblePrintModal: true
    })
  }

  renderRoutes(routes: WholesaleRoute[]) {
    const rows = []
    for (let i = 0; i < routes.length; i++) {
      rows.push(<Route key={i}>{routes[i].routeName}</Route>)
    }
    return rows
  }

  showMore = () => {
    this.setState({ showMore: !this.state.showMore })
  }

  handleSelecteType = (selectedType: number) => {
    this.setState({ selectedType })
  }

  render() {
    const { name, delivery, unAssignedRoutes, unAssignedOrders, ordersMissingDriverNoCancel } = this.props
    const { visiblity, visiblePrintModal, selectedDocuments, visibleSelectModal, selectedType } = this.state

    const driver = delivery ? delivery.driver : null
    const routes: WholesaleRoute[] = delivery ? delivery.routes : []
    const totalOrders = delivery ? delivery.totalOrders : 0
    const totalUnits = delivery ? delivery.totalUnits : 0
    const totalPicks = delivery ? delivery.totalPicks : 0
    const orders = delivery ? delivery.orders : []
    const newStatusIdsByOrders = orders.filter((el: any) => el.status === 'NEW').map(v => v.wholesaleOrderId)

    let data: any = []
    let columns: any = []

    if (name === consts.ordersMissingRoute && routes) {
      data = unAssignedOrders
      columns = this.unAssignedOrderColumns
    } else if (name === consts.ordersMissingDriver ) {
      data = ordersMissingDriverNoCancel
      columns = this.unAssignedOrderColumns
    } else if (name === consts.unassignedRoutes && routes) {
      data = unAssignedRoutes
      columns = unAssignedRouteColumns
    } else if (name === consts.driver) {
      data = cloneDeep(orders)
      columns = this.state.assignedRouteColumns
    }

    return (
      <CardContainer style={{ height: this.state.showMore ? 'auto' : 553 }}>
        <div style={{ flex: 1, display: 'flex', flexDirection: 'column', overflow: 'hidden' }}>
          <CardHeader
            name={name}
            onClickPrint={this.onToggleSelect}
            onClickEdit={this.onClickEdit}
            visibleState={this.state.visiblity}
            user={driver}
          />
          <div style={{ marginTop: 80 }}>
            <EditRoutePopup
              visible={visiblity}
              direction="right"
              driverId={driver && driver.userId ? driver.userId : ''}
              cardRoutes={routes}
              onClickOutside={this.onClickEdit}
            />
            {name === consts.ordersMissingRoute && ( // orders
              <TableWrapper>
                <CardTable columns={columns} datasource={data} name={name} />
              </TableWrapper>
            )}

            {name === consts.ordersMissingDriver && ( // ordersMissingDriver
              <TableWrapper>
                <CardTable columns={columns} datasource={data} name={name} />
              </TableWrapper>
            )}

            {name === consts.unassignedRoutes && ( // routes
              <TableWrapper>
                <CardTable columns={columns} datasource={data} name={name} />
              </TableWrapper>
            )}
            {name === consts.driver && (
              <DeliveryOrdersList>
                <CardName>{driver ? driver.firstName + ' ' + driver.lastName : ''}</CardName>
                <Routes>{this.renderRoutes(routes)}</Routes>
                <Divider style={DividerStyle} />
                <Row type="flex" justify="space-between">
                  <Col lg={12}>
                    <CardHeaderCount count={totalOrders} title="Total Orders" />
                  </Col>
                  <Col lg={12}>
                    <CardHeaderCount count={`${totalUnits ? totalUnits : 0}/${totalPicks ? totalPicks : 0}`} title="Total Units" />
                  </Col>
                </Row>
                <TableWrapper>
                  <CardTable
                    columns={columns}
                    datasource={data}
                    name={name}
                    driverName={driver?.firstName + ' ' + driver?.lastName}
                  />
                </TableWrapper>
              </DeliveryOrdersList>
            )}
          </div>
        </div>
        <ShowMore onClick={this.showMore}>SHOW {this.state.showMore === false ? 'MORE' : 'LESS'}</ShowMore>

        <SelectDocumentModal selectedType={selectedType} handleSelecteType={this.handleSelecteType} delivery={delivery} visible={visibleSelectModal} onToggle={this.onToggleSelect} onView={this.onViewSelected} handleSelectRoute={(selectedRoutes) => this.setState({ selectedRoutes })} />
        {name === consts.driver && visiblePrintModal && (
          <PrintDeliveryModal {...this.props} selectedType={selectedType} selected={selectedDocuments} newStatusIdsByOrders={newStatusIdsByOrders} onToggle={this.onClickPrint} selectedRoutes={this.state.selectedRoutes} />
        )}

      </CardContainer>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const RouteCard = withTheme(connect(DeliveryModule)(mapStateToProps)(RouteCardComponent))
