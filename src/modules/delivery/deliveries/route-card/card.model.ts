export const unAssignedRouteColumns = [
  {
    title: 'SO #',
    dataIndex: 'route_id',
  },
  {
    title: 'ROUTE NAME',
    dataIndex: 'route_name',
  },
]

export const DeliveryDestinationColumns = [
  {
    title: 'DELIVERY ADDRESS',
    dataIndex: 'name',
  },
  {
    title: '**',
    dataIndex: 'count',
  },
  {
    title: '# OF UNITS',
    dataIndex: 'unit_count',
  },
]

export const ProductColumns = [
  {
    title: 'PRODUCT NAME',
    dataIndex: 'name',
  },
  {
    title: '**',
    dataIndex: 'count',
  },
  {
    title: '# OF UNITS',
    dataIndex: 'unit_count',
  },
]

export const testUnassignedData = [
  {
    key: '1',
    route_id: '2345',
    route_name: 'PM, Chinatown',
  },
  {
    key: '1',
    route_id: '2389',
    route_name: 'PM, Penninsula S of 92 W',
  },
]

export const testCardData = [
  {
    key: '1',
    name: 'Big Big Pen',
    count: 1,
    unit_count: 3,
  },
  {
    key: '2',
    name: 'Smile House Cafe',
    count: 1,
    unit_count: 9,
  },
  {
    key: '3',
    name: 'Dragon Beaux Restaurant',
    count: 1,
    unit_count: 16,
  },
  {
    key: '4',
    name: 'Tak Kee Lee REST',
    count: 1,
    unit_count: 31,
  },
  {
    key: '5',
    name: 'Giant God Co.',
    count: 2,
    unit_count: 20,
  },
  {
    key: '6',
    name: 'Guang Dong BBQ Tea House',
    count: 1,
    unit_count: 19,
  },
]
