import styled from '@emotion/styled'
import { white, lightGrey, topLightGrey, mediumGrey } from '~/common'
import { Button, Modal, Col } from 'antd'

interface HeaderTypeProps {
  isLighter: boolean
}

const CARD_HEIGHT = 600

export const CardContainer = styled('div')((props) => ({
  height: `${CARD_HEIGHT}px`,
  borderRadius: '4px',
  border: '1px solid #E5E5E5',
  background: white,
  position: 'relative',
  display: 'flex',
  flexDirection: 'column',
  minHeight: CARD_HEIGHT,
}))

export const CardHeaderWrapper = styled('div')<HeaderTypeProps>((props) => ({
  backgroundColor: props.isLighter ? props.theme.lighter : props.theme.primary,
  padding: '12px 16px',
  textAlign: 'left',
  borderTopLeftRadius: 4,
  borderTopRightRadius: 4,
  position: 'absolute',
  width: '100%',
  zIndex: 1,
}))

export const CardHeaderTitle = styled('span')((props) => ({
  color: props.theme.light,
  fontSize: '20px',
  lineHeight: '180%',
  letterSpacing: '0.05em',
}))

export const CardIconsWrapper = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  margin: '10px 0',
  position: 'relative',
})

export const IconWrapper = styled('span')((props) => ({
  cursor: 'pointer',
  '& path': {
    fill: white,
  },
}))

export const AvatarWrapper = styled('span')((props) => ({
  position: 'absolute',
  left: 'calc(((100% - 32px) / 2) - 20px)',
  bottom: '-52px',
}))

export const DeliveryOrdersList = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  // height: `calc(${CARD_HEIGHT}px - 44px)`,
})

export const TableWrapper = styled('div')({
  overflow: 'hidden',
  '& .ant-table': {
    border: 'none',
    borderRadius: 0,
  },
  '& .ant-table-body': {
    margin: '0 !important',
    // height: `calc((${CARD_HEIGHT}px / 2) - 50px)`, // assume that table body will have this height (about half of card container)
    overflow: 'hidden',
    '& tbody tr': {
      fontSize: 12,
    },
    '& tbody tr:nth-of-type(even)': {
      backgroundColor: white,
    },
    '& tbody tr:nth-of-type(odd)': {
      backgroundColor: topLightGrey,
    },
  },

  '&.scrollable table': {
    width: '100%',
    '& thead, & tbody tr': {
      display: 'table',
      width: '100%',
      tableLayout: 'fixed',
    },
    '& tbody': {
      display: 'block',
      overflowY: 'auto',
      tableLayout: 'fixed',
      // maxHeight: 250,
    },
  },
})

export const CardName = styled('p')((props) => ({
  color: props.theme.dark,
  fontSize: 20,
  lineHeight: '180%',
  textAlign: 'center',
  letterSpacing: '0.05em',
  margin: '25px 0 0 0',
}))

export const Routes = styled('div')({
  textAlign: 'center',
})

export const Route = styled('span')((props) => ({
  height: 23,
  backgroundColor: props.theme.lighter,
  borderRadius: 10,
  padding: '2px 10px',
  margin: 1,
  display: 'inline-block',
  fontSize: 11,
  fontWeight: 'bold',
  lineHeight: '156%',
  color: props.theme.dark,
}))

export const DividerStyle: React.CSSProperties = {
  minWidth: 'auto',
  width: '20%',
  margin: '10px auto 20px',
}

export const Count = styled('div')({
  margin: '0 10px',
  backgroundColor: topLightGrey,
  borderRadius: 5,
  padding: '5px 20px',
  marginBottom: 20,
})

export const Number = styled('p')((props) => ({
  fontSize: 20,
  lineHeight: '120%',
  letterSpacing: '0.05em',
  fontWeight: 'bold',
  color: props.theme.dark,
  textAlign: 'left',
  margin: 0,
}))

export const Title = styled('p')({
  fontSize: 14,
  color: mediumGrey,
  lineHeight: '120%',
  letterSpacing: '0.05em',
  textAlign: 'left',
  margin: 0,
})

export const ShowMore: any = styled(Button)((props) => ({
  backgroundColor: props.theme.lighter,
  width: '100%',
  // position: 'absolute',
  // bottom: 0,
  // left: 0,
  borderRadius: 0,
  color: props.theme.dark,
  fontWeight: 'bold',
}))

export const ModalWrapper: any = styled(Modal)((props) => ({
  '& .ant-modal-title': {
    color: `${props.theme.dark} !important`,
    fontWeight: 'bold',
    fontSize: 20,
  },
  '& .ant-modal-header': {
    borderBottom: 'none !important',
  },
  '& .ant-modal-footer': {
    borderTop: 'none !important',
    textAlign: 'center',
    padding: 20,
  },
  '& .ant-modal-footer > div': {
    display: 'flex',
    flexDirection: 'row-reverse',
    justifyContent: 'center',
  },
  '& .ant-modal-footer button': {
    borderRadius: 15,
    fontSize: 17,
    fontWeight: 'bold',
    margin: 4,
  },
  '& .ant-select-selection__placeholder': {
    color: props.theme.primary,
  },
}))

export const ColWrapper: any = styled(Col)((props) => ({}))

export const ModalTextStyle = styled('div')((props) => ({
  border: `1px solid ${mediumGrey}`,
  borderRadius: 6,
  margin: 8,
  padding: 10,
  height: 80,
}))

export const smallTextStyle: React.CSSProperties = {
  color: mediumGrey,
  fontSize: 12,
}

export const bigTextStyle: React.CSSProperties = {
  color: mediumGrey,
  fontSize: 18,
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
}

export const buttonStyle: React.CSSProperties = {
  borderRadius: 20,
  padding: '10px 20px',
  height: 40,
  margin: 6,
}

export const VersionNumber = styled('div')((props) => ({
  width: 16,
  height: 18,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: 12,
  color: 'black',
  fontWeight: 'normal',
}))

export const ManifestModal = styled('div')((props) => ({
  padding: 20,
  paddingTop: 60,
  h3: {
    fontSize: 21,
    textAlign: 'center',
  },
  '.current-date': {
    position: 'absolute',
  },
  '.manifest-table': {
    marginBottom: 5,
    width: '100%',
    borderColor: '#aaa',
    td: {
      color: 'black',
      padding: '5px 0',
      textAlign: 'center',
      fotnWeight: 500,
    },
  },
  '.ant-table': {
    border: '1px solid #aaa',
    '.ant-table-thead > tr > th': {
      borderRight: '1px solid #aaa',
      borderBottom: '1px solid #aaa',
      textAlign: 'center',
    },
    '.ant-table-tbody > tr > td': {
      borderRight: '1px solid #aaa',
      borderBottom: '1px solid #aaa',
    },
  },
}))
