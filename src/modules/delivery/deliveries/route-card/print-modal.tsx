import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { Row, Col, Modal } from 'antd'
import _, { cloneDeep } from 'lodash'
import { withTheme } from 'emotion-theming'
import moment from 'moment'
import { Theme } from '~/common'
import { GlobalState } from '~/store/reducer'
import { OrdersModule } from '~/modules/orders/orders.module'

import { CardTable } from './card-table'
import { CardHeaderCount } from './card-header-counts'
import { WholesaleDelivery, WholesaleRoute, OrderDetail, OrderDTO } from '~/schema'
import { checkError, getOrderPrefix, printWindow, responseHandler } from '~/common/utils'

import { TableWrapper, ManifestModal, VersionNumber } from './card.style'
import { Icon } from 'antd'
import { ThemeButton } from '~/modules/customers/customers.style'
import moment from 'moment'
import { PrintModalHeader } from '~/modules/customers/sales/_style'
import PrintBillOfLading from '~/modules/customers/sales/cart/print/print-bill-lading'
import PrintPickSheet from '~/modules/customers/sales/cart/print/print-pick-sheet'
import { OrderService } from '~/modules/orders/order.service'
import { of } from 'rxjs'

export type PrintDeliveryModalProps = {
  type: number
  index: number //
  delivery?: WholesaleDelivery
  unAssignedRoutes?: WholesaleRoute[]
  unAssignedOrders?: OrderDetail[]
  cardName: string
  theme: Theme
  selected: any // state selected on Select documents modal
  visible: boolean
  onToggle: Function
  printSetting: string
  startPrintOrders: Function
  getWeightsByOrderItemIds: Function
  getPrintOrders: Function
  getPrintSetting: Function
  getSellerSetting: Function
  getCompanyProductAllTypes: Function
  selectedRoutes: string[]
  newStatusIdsByOrders: number[]
  companyProductTypes: any
  selectedType: number
  clearPrintOrders: Function
}

class PrintDeliveryModal extends React.PureComponent<PrintDeliveryModalProps> {
  state: any
  constructor(props: PrintDeliveryModalProps) {
    super(props)
    this.state = {
      visiblity: false,
      showMore: false,
      routesCount: props.delivery ? props.delivery.routes.length : 0,
      visiblePrintModal: false,
      printDeliveryRef: null,
    }
  }

  componentDidMount() {
    const { delivery, index, selected } = this.props
    const orders = delivery ? delivery.orders : []

    this.props.getCompanyProductAllTypes()
    this.props.getSellerSetting()
    if (!this.props.printSetting) {
      this.props.getPrintSetting()
    }
    const orderIds: number[] = []

    if (orders.length > 0) {
      this.props.startPrintOrders()
      if (selected) {
        const data = orders.filter(
          (item: any) =>
            selected.routesChecked.includes(String(item.defaultRouteId)) ||
            selected.routesChecked.includes(String(item.overrideRouteId)),
        )
        data.forEach((o) => {
          orderIds.push(o.wholesaleOrderId)
        })
      }

      this.props.getWeightsByOrderItemIds(orderIds)
      this.props.getPrintOrders({
        body: {
          orderIds,
        },
      })
    }
  }

  printContent = () => {
    return this.state.printDeliveryRef
  }

  render() {
    const {
      index,
      type,
      delivery,
      unAssignedRoutes,
      unAssignedOrders,
      visible,
      onToggle,
      sellerSetting,
      printOrders,
      printSetting,
      loadingPrintOrders,
      selected,
      selectedRoutes,
      newStatusIdsByOrders,
      companyProductTypes,
    } = this.props

    const driver = delivery ? delivery.driver : null
    const routes: WholesaleRoute[] = delivery ? delivery.routes : []
    const totalOrders = delivery ? delivery.totalOrders : 0
    const totalUnits = delivery ? delivery.totalUnits : 0
    const totalPicks = delivery ? delivery.totalPicks : 0

    const paymentTerms = [
      ..._.get(companyProductTypes, 'paymentTerms', []),
      ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
    ]

    // The shipping manifest table should always be populated with all of the orders from the day (it should not be governed by the checkboxes in the print popup)
    const { orders } = delivery

    let data: any = []

    if (index === 0 && routes) {
      data = unAssignedOrders
    } else if (index === 1 && routes) {
      data = unAssignedRoutes
    } else if (index > 1) {
      data = cloneDeep(orders)
      if (index === 3 && selected) {
        data = data.filter((item: any) => selected.routesChecked.indexOf(item.defaultRouteId.toString()) >= 0)
      }
    }

    const colsForPrint = [
      {
        title: '',
        dataIndex: 'status',
        key: 'no',
        width: 20,
        render: (value: any, record: any, index: number) => index + 1,
      },
      {
        title: '',
        dataIndex: 'status',
        width: 40,
        render: (data: any, record: any) => {
          return (
            <div>
              {data ? (
                <div>
                  <IconSvg type="check" viewBox="0 0 15 14" width={15} height={14} style={{ fill: 'none' }} />
                </div>
              ) : (
                <div />
              )}
            </div>
          )
        },
      },
      {
        title: 'SO#',
        dataIndex: 'wholesaleOrderId',
        render: (id: string) => {
          return `${getOrderPrefix(sellerSetting, 'sales')}${id}`
        },
      },
      {
        title: 'Acc #',
        dataIndex: 'customerId',
      },
      {
        title: 'Account Name',
        dataIndex: 'customer',
      },
      {
        title: '**',
        dataIndex: 'versionNumber',
        width: 40,
        render: (versionNumber: number, record: any) => {
          return (
            <VersionNumber
              style={{
                backgroundColor: record.hasWorkOrder ? '#FFDE68' : 'transparent',
                fontWeight: record.hasWorkOrder ? 'bold' : 'normal',
              }}
            >
              {versionNumber}
            </VersionNumber>
          )
        },
      },
      {
        title: 'UNITS ORDERED',
        dataIndex: 'sumOfUnits',
        render: (data: any, record: any) => {
          return (
            <div>
              {record.hasWorkOrder === true && (
                <span style={{ color: theme.primary, marginRight: 5 }}>
                  <Icon type="star" theme="filled" />
                </span>
              )}
              {data}
            </div>
          )
        },
      },
      {
        title: 'UNITS PICKED',
        dataIndex: 'sumOfPicks',
        render: (data: any, record: any) => {
          return <div>{data}</div>
        },
      },
      {
        title: 'Total Check',
        key: 'Total Check',
      },
      {
        title: 'Total Ship',
        key: 'Total Ship',
      },
      {
        title: 'Notes',
        key: 'Notes',
        width: 180,
      },
      {
        title: 'Cash',
        key: 'Cash',
      },
      {
        title: 'Check',
        key: 'Check',
      },
      {
        title: 'Check #',
        key: 'Check #',
      },
    ]
    printOrders.sort((a, b) => a.wholesaleOrderId - b.wholesaleOrderId)
    return (
      <Modal
        width={1200}
        footer={null}
        visible
        onCancel={() => {
          onToggle()
          this.props.clearPrintOrders()
        }}
      >
        <PrintModalHeader>
          <ThemeButton
            size="large"
            className="print-pick-sheet"
            onClick={() => {
              printWindow(`printDeliveryModal${index}`, this.printContent.bind(this))
              if (newStatusIdsByOrders.length) {
                OrderService.instance.setOrdersStatusToPicked(newStatusIdsByOrders).subscribe({
                  next(res: any) {
                    of(responseHandler(res, false).body.data)
                  },
                  error(err) {
                    checkError(err)
                  },
                })
              }
              if (orders.length > 0) {
                const orderIds = orders.map((order: OrderDTO) => order.wholesaleOrderId).join(',')
                const printType = this.props.selectedType === 1 ? 'pickSheet' : 'invoice'
                OrderService.instance.recordPrintActivity(printType, orderIds).subscribe()
              }
            }}
          >
            <Icon type="printer" theme="filled" />
            Print document
          </ThemeButton>
          <div className="clearfix" />
        </PrintModalHeader>
        <div id={`printDeliveryModal${index}`}>
          <div
            ref={(el: any) => {
              this.setState({
                printDeliveryRef: el,
              })
            }}
          >
            <ManifestModal>
              <div className="current-date">Date: {moment().format('MM/DD/YYYY')}</div>
              {index !== 3 || (index === 3 && selected && selected.manifestChecked) ? (
                <>
                  <h3>SHIPPING MANIFEST</h3>
                  <table border={1} className="manifest-table">
                    <tr>
                      <td colSpan={2}>Driver</td>
                      <td colSpan={4}>{driver ? driver.firstName + ' ' + driver.lastName : ''}</td>
                      <td colSpan={2}>Ship Date</td>
                      <td colSpan={4}>{localStorage.getItem('DELIVERIES_SELECTED_DATE')}</td>
                    </tr>
                    {[1, 2, 3, 4].map((i, idx) => (
                      <tr key={`shipping-manifest-${idx}`}>
                        <td>Pallet {i}</td>
                        <td>Invoices</td>
                        <td width={80} />
                        <td>Units</td>
                        <td width={80} />
                        <td>Int.</td>
                        <td>Pallet {i + 4}</td>
                        <td>Invoices</td>
                        <td width={80} />
                        <td>Units</td>
                        <td width={80} />
                        <td>Int.</td>
                      </tr>
                    ))}
                  </table>
                </>
              ) : (
                <div style={{ height: 30, width: '100%' }}></div>
              )}
              <div style={{ fontWeight: 'normal', color: 'black' }}>
                <p>
                  <b>
                    Meal Record. Each driver is required to truthfully complete and return at the end of their shift.
                    Mark only with one “x” and complete all fields.
                  </b>
                </p>
                <p>
                  <b>
                    [<span style={{ width: 25, display: 'inline-block' }} />] Forgoing Lunch:
                  </b>{' '}
                  My employer has informed, offered, and provided me a statutory 30-min meal break as required by all
                  applicable local, state, and federal law. My employer has further informed me I cannot and should not
                  waive my 30- minute meal break if I work 6 or more hours. However, on my own free-will and accord, I
                  choose to forgo this meal break today for reasons explained below and will continue to work instead.
                  This paper serves as a record of my decision. I agree to hold my employer harmless for any and all
                  actions arising from this decision. I am aware that false statements and stealing time by overstating
                  my work hours is illegal and will result in warnings and termination of my employment. I acknowledge
                  that my employer reserves the right to check the accuracy of my statement.
                </p>
                <p style={{ display: 'flex' }}>
                  Reason (required):{' '}
                  <span style={{ marginLeft: 3, borderBottom: '1px solid black', flex: 1, height: 19 }} />
                </p>
                <p>
                  <b>------ OR ------</b>
                </p>
                <p>
                  <b>
                    [<span style={{ width: 25, display: 'inline-block' }} />]
                  </b>{' '}
                  I took my 30-minute meal break and authorize my employer to adjust my time sheet accordingly.
                </p>
                <p style={{ marginTop: 50, display: 'flex' }}>
                  I represent the above to be true and correct. Driver Initials:{' '}
                  <span style={{ marginLeft: 3, borderBottom: '1px solid black', width: 200, height: 19 }} />
                </p>
              </div>
              <Row type="flex" justify="space-between" style={{ margin: '0 -10px' }}>
                <Col lg={12}>
                  <CardHeaderCount count={totalOrders} title="Total Orders" />
                </Col>
                <Col lg={12}>
                  <CardHeaderCount count={`${totalUnits}/${totalPicks}`} title="Total Units" />
                </Col>
              </Row>
              <TableWrapper>
                <CardTable
                  showMore={true}
                  columns={colsForPrint}
                  datasource={orders}
                  type={type}
                  bordered={true}
                  driverName={driver.firstName + ' ' + driver.lastName}
                />
              </TableWrapper>
              <div style={{ pageBreakAfter: 'always' }} />
              {/* {data && data.length > 0 && ( the code don't display invoice  */}
              <>
                {printOrders.map((order: any, index: number) => {
                  if (this.props.selectedType === 1) {
                    return (
                      <div style={{ margin: '25px 0 0' }} key={`print-picksheet-${index}`}>
                        <PrintPickSheet
                          orderItems={order.printOrderItem}
                          currentOrder={order}
                          companyName={this.props.companyName}
                          company={this.props.company}
                          catchWeightValues={[]}
                          logo={this.props.logo}
                          printSetting={printSetting}
                          sellerSetting={this.props.sellerSetting}
                          changePrintLogoStatus={() => {}}
                        />
                      </div>
                    )
                  }
                  return (
                    <div style={{ margin: '25px 0 0' }} key={`print-picksheet-${index}`}>
                      <PrintBillOfLading
                        selectedType={this.props.selectedType}
                        getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                        catchWeightValues={[]}
                        orderItems={order.printOrderItem}
                        currentOrder={order}
                        multiple={true}
                        companyName={this.props.companyName}
                        company={this.props.company}
                        logo={this.props.logo}
                        printSetting={printSetting}
                        type={'invoice'}
                        changePrintLogoStatus={() => {}}
                        paymentTerms={paymentTerms}
                      />
                      <div style={{ pageBreakAfter: 'always' }} />
                    </div>
                  )
                })}
              </>

              {/* )} */}

              <p style={{ display: 'flex', marginTop: 50, float: 'right', width: '70%' }}>
                Supervisor Signature:
                <span style={{ marginLeft: 3, borderBottom: '1px solid black', flex: 1, height: 19 }} />
              </p>
            </ManifestModal>
          </div>
        </div>
        <div style={{ visibility: 'hidden' }}>
          <ThemeButton shape="round" style={{ width: '100%', height: 10 }}>
            Print
          </ThemeButton>
        </div>
      </Modal>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(PrintDeliveryModal))
