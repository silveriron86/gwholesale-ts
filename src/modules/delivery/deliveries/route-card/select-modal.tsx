import { Select } from 'antd'
import { cloneDeep } from 'lodash'
import React from 'react'
import { ThemeButton, ThemeCheckbox, ThemeModal } from '~/modules/customers/customers.style'
import { WholesaleDelivery, WholesaleRoute } from '~/schema'

export interface SelectModalProps {
  visible: boolean
  onToggle: Function
  onView: Function
  delivery?: WholesaleDelivery
  handleSelectRoute: (value: number[]) => void
  selectedType: number
  handleSelecteType: (value: number) => void
}

export default class SelectDocumentModal extends React.PureComponent<SelectModalProps> {
  state: any
  constructor(props: SelectModalProps) {
    super(props)
    this.state = {
      manifestChecked: true,
      invoiceChecked: false,
      routesChecked: []
    }
  }

  onChange = (e: any, type: string) => {
    let state = cloneDeep(this.state)
    const { checked } = e.target
    state[type] = checked
    if (type === 'invoiceChecked') {
      const { delivery } = this.props
      const routes: WholesaleRoute[] = delivery ? delivery.routes : []
      let routesChecked: string[] = []
      if (checked) {
        routes.forEach((route, index) => {
          routesChecked.push(route.id.toString())
        })
      }
      state['routesChecked'] = routesChecked
    }
    this.setState(state)
  }

  onCheckRoutes = (e: any, id: number) => {
    let invoiceChecked = this.state.invoiceChecked
    let routesChecked = cloneDeep(this.state.routesChecked)
    const { checked } = e.target
    if (checked) {
      if (routesChecked.indexOf(id.toString()) === -1) {
        routesChecked.push(id.toString())
      }
    } else {
      const index = routesChecked.indexOf(id.toString())
      routesChecked.splice(index, 1)
      invoiceChecked = false
    }
    this.setState({
      routesChecked,
      invoiceChecked,
    })
    this.props.handleSelectRoute(routesChecked)
  }

  render() {
    const { manifestChecked, invoiceChecked, routesChecked } = this.state
    const { visible, onToggle, delivery, onView, selectedType, handleSelecteType } = this.props
    const routes: WholesaleRoute[] = delivery ? delivery.routes : []
    return (
      <ThemeModal
        className="select-document-modal"
        title="Select documents"
        visible={visible}
        onCancel={onToggle}
        cancelText="Close"
        footer={
          <div>
            <ThemeButton className="left" onClick={() => onView(cloneDeep(this.state))}>
              View documents
            </ThemeButton>
            <div className="clearfix" />
          </div>
        }
        okButtonProps={{ style: { display: 'none' } }}
      >
        <div className="check-row" style={{ marginTop: 0 }}>
          <ThemeCheckbox checked={manifestChecked} onChange={(e: any) => this.onChange(e, 'manifestChecked')}>
            Delivery manifest
          </ThemeCheckbox>
        </div>
        <div>
        </div>
        <div className="check-row">
          <Select style={{ width: 200 }} value={selectedType} onChange={handleSelecteType}>
            <Select.Option value={1}>Pick Sheets</Select.Option>
            <Select.Option value={2}>Invoices</Select.Option>
          </Select>
          {/* <ThemeCheckbox checked={invoiceChecked} onChange={(e: any) => this.onChange(e, 'invoiceChecked')}>
            Invoices
          </ThemeCheckbox> */}
        </div>
        <div style={{ marginLeft: 22 }}>
          {routes.map((route, index) => {
            return (
              <div key={`route-${index}`} className="check-row">
                <ThemeCheckbox
                  checked={routesChecked.indexOf(route.id.toString()) >= 0}
                  onChange={(e: any) => this.onCheckRoutes(e, route.id)}
                >
                  {route.routeName}
                </ThemeCheckbox>
              </div>
            )
          })}
        </div>
      </ThemeModal>
    )
  }
}
