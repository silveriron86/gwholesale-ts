import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'

import { TableViewWrapper, CardWrappperStyle, RouteCardWrapper } from './tableview.style'
import { DeliveryModule, DailyDeliveryProps, DeliveryDispatchProps } from '../../delivery.module'
import { RouteCard } from '../route-card/card.container'
import { ViewSelectorProps } from './view-selector'
import { ViewSwitcherEl } from '~/components/elements/view-selector'
import consts from '../consts'


type TableViewProps = DeliveryDispatchProps & DailyDeliveryProps & ViewSelectorProps & {}

export class TableViewComponent extends React.PureComponent<TableViewProps> {
  constructor(props: TableViewProps) {
    super(props)
  }
  componentDidMount() {

  }

  onSwitchingView = (param: string) => {
    this.props.onClick(param)
  }

  render() {
    const { viewType } = this.props

    return (
      <TableViewWrapper>
        <ViewSwitcherEl viewType={viewType} onClick={this.onSwitchingView} />
        <div style={CardWrappperStyle}>{this.renderCards()}</div>
      </TableViewWrapper>
    )
  }

  renderCards() {
    const { deliveries, unassignedRoutes, unassignedOrders, ordersMissingDriver } = this.props;
    const notCanceledOrders = unassignedOrders.filter(el => el.wholesaleOrderStatus != 'CANCEL')
    const ordersMissingDriverNoCancel = ordersMissingDriver.filter(el => el.wholesaleOrderStatus != 'CANCEL')
    const rows = []
    /**
     * if type = 0: unassigned orders card
     * if type = 1: unassigned routes card
     * else : assigned deliveries card
     */

    for (let i = 0; i < deliveries.length; i++) {
      const del = deliveries[i]
      rows.push(
        <RouteCardWrapper key={del?.driver?.userId}>
          <RouteCard name={consts.driver} delivery={del} />
        </RouteCardWrapper>,
      )
    }

    if (notCanceledOrders.length > 0) {
      rows.push(
        <RouteCardWrapper key={consts.ordersMissingRoute}>
          <RouteCard name={consts.ordersMissingRoute} unAssignedOrders={notCanceledOrders} />
        </RouteCardWrapper>,
      )
    }

    if (ordersMissingDriverNoCancel.length > 0){
      rows.push(
        <RouteCardWrapper key={consts.ordersMissingDriver}>
          <RouteCard name={consts.ordersMissingDriver} ordersMissingDriverNoCancel={ordersMissingDriverNoCancel} />
        </RouteCardWrapper>,
      )
    }

    if (unassignedRoutes.length > 0) {
      rows.push(
        <RouteCardWrapper key={1}>
          <RouteCard name={consts.unassignedRoutes} unAssignedRoutes={unassignedRoutes} />
        </RouteCardWrapper>,
      )
    }
    return rows
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const TableView = withTheme(connect(DeliveryModule)(mapStateToProps)(TableViewComponent))
