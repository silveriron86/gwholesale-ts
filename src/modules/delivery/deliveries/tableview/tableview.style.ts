import styled from '@emotion/styled'
import { lightGrey, disabledGrey, topLightGrey } from '~/common'

import { ExtraItem } from '~/components/elements'

export const TableViewWrapper = styled('div')({
  padding: '30px 20px 0 60px',
})

export const CardWrappperStyle: React.CSSProperties = {
  marginTop: 30,
  display: 'flex',
  flexWrap: 'wrap',
  // justifyContent: 'space-around',
}

export const RouteCardWrapper = styled('div')({
  padding: '0 8px 40px',
  width: 420,
})
