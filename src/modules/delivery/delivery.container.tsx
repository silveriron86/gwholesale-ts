/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { Col, Tabs } from 'antd'
import moment from 'moment'

import { DeliveryModule, DeliveryDispatchProps, DailyDeliveryProps } from './delivery.module'
import { ThemeIcon } from '~/modules/customers/customers.style'
import PageLayout from '~/components/PageLayout'
import { DetailWrapperStyle } from '~/components/elements'
import { DatePickerElement } from '~/components/elements/datepicker'
import { TableView } from './deliveries/tableview/tableview.container'
import { ListView } from './deliveries/listview/listivew.container'
import { Theme } from '~/common'

import { DeliveryHeader, DeliveryBody, InfoLabel, AmountLabel, SpinWrap, HomeTabs } from './delivery.style'
import { WEEKDAYS } from '~/schema'
import Containers from './containers'
import _ from 'lodash'

const { TabPane } = Tabs

let timer = undefined
let interval = 30000
export type DeliveryComponentProps = DeliveryDispatchProps &
  DailyDeliveryProps & {
    theme: Theme
  }

export class DeliveryComponent extends React.PureComponent<DeliveryComponentProps> {
  state = {
    isTableView: 'table',
  }

  componentDidMount() {
    // timer = setInterval(() => this.initData(false), interval);
    this.initData(true)
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
  }

  // componentWillUnmount() {
  //   clearInterval(timer);
  // }

  initData = (firstTime: Boolean) => {
    this.props.startWithGetAllDeliveries()

    const today = new Date()
    const week_day = WEEKDAYS[today.getDay()]
    var strToday = moment(today).format('MM/DD/YYYY')
    localStorage.setItem('DELIVERIES_SELECTED_DATE', strToday)
    console.log(strToday)
    var selectedWeekDay, curDate
    if (firstTime) {
      selectedWeekDay = week_day
      curDate = strToday
      this.props.changeSelectedWeekDay(week_day)
      this.props.changeCurDate(strToday)
    } else {
      selectedWeekDay = this.props.selectedWeekDay
      curDate = this.props.curDate
    }
    this.props.getUnassignedRoutes(selectedWeekDay)
    this.props.getOrdersMissingDriver(strToday)
    this.props.getAllDrivers()
    this.props.getDriverSchedules()
    this.props.getAllRoutes()
    this.props.getAllDeliveries({ week_day: selectedWeekDay, current_date: curDate })
    this.props.getUnassignedOrdersByDeliverDate(curDate)
  }

  onSwitchingView = (param: string) => {
    this.setState({ isTableView: param })
  }

  onDateChange = (date: any) => {
    if (!date) return
    this.props.startWithGetAllDeliveries()
    const curDate = date.toDate()
    const weekDay = WEEKDAYS[curDate.getDay()]
    var strCurDay = moment(curDate).format('MM/DD/YYYY')
    localStorage.setItem('DELIVERIES_SELECTED_DATE', strCurDay)
    this.props.changeSelectedWeekDay(weekDay)
    this.props.getUnassignedRoutes(weekDay)
    this.props.getOrdersMissingDriver(strCurDay)
    this.props.changeCurDate(strCurDay)
    this.props.getAllDeliveries({ week_day: weekDay, current_date: strCurDay })
    this.props.getUnassignedOrdersByDeliverDate(strCurDay)

    //reset interval
    // clearInterval(timer);
    // timer = setInterval(() => this.initData(false), interval);
  }

  onBack = () => {
    this.props.goBack()
  }

  render() {
    const { isTableView } = this.state
    const totalOrders = this.getTotalOrders()
    const totalCases = this.getTotalCases()
    const totalPicks = this.getTotalPicks()
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Selling & Shipping-Fulfillment'}>
        <a
          href="javascript:;"
          onClick={this.onBack}
          style={{ paddingLeft: 70, position: 'absolute', left: 0, top: 23, zIndex: 1000 }}
        >
          <ThemeIcon type="arrow-left" viewBox="0 0 20 20" width={13} height={11} style={{ color: 'white' }} />
        </a>
        <HomeTabs
          defaultActiveKey={
            _.get(this.props, 'history.location.search', '').includes('containers') ? 'containers' : 'deliveries'
          }
          onChange={(key) => this.props.history.push(`/delivery/deliveries?tab=${key}`)}
        >
          <TabPane tab="Deliveries" key="deliveries">
            <DeliveryHeader>
              <DetailWrapperStyle style={{ paddingTop: 15 }}>
                <Col xs={12} lg={8} sm={8}>
                  <DatePickerElement onPickerChange={this.onDateChange}>DATE</DatePickerElement>
                </Col>
                <Col xs={12} lg={8} sm={8}>
                  <InfoLabel>ORDERS</InfoLabel>
                  <AmountLabel> {totalOrders} </AmountLabel>
                </Col>
                <Col xs={12} lg={8} sm={8}>
                  <InfoLabel>UNITS ORDERED/PICKED</InfoLabel>
                  <AmountLabel>
                    {totalCases}/{totalPicks}
                  </AmountLabel>
                </Col>
              </DetailWrapperStyle>
            </DeliveryHeader>
            <SpinWrap spinning={this.props.getAllDeliveriesLoading}>
              <DeliveryBody>
                {isTableView === 'table' && <TableView viewType="table" onClick={this.onSwitchingView} />}
                {isTableView === 'list' && <ListView viewType="list" onClick={this.onSwitchingView} />}
              </DeliveryBody>
            </SpinWrap>
          </TabPane>
          <TabPane tab="Containers" key="containers">
            <Containers />
          </TabPane>
        </HomeTabs>
      </PageLayout>
    )
  }

  getTotalOrders = () => {
    const { deliveries } = this.props
    let count = 0
    deliveries.forEach((el) => {
      count += el.totalOrders ? el.totalOrders : 0
    })
    return count
  }

  getTotalCases = () => {
    const { deliveries } = this.props
    let count = 0
    deliveries.forEach((el) => {
      count += el.totalUnits ? el.totalUnits : 0
    })
    return count
  }

  getTotalPicks = () => {
    const { deliveries } = this.props
    let count = 0
    deliveries.forEach((el) => {
      count += el.totalPicks ? el.totalPicks : 0
    })
    return count
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const DeliveryContainer = withTheme(connect(DeliveryModule)(mapStateToProps)(DeliveryComponent))
