import { EffectModule, Module, ModuleDispatchProps, DefineAction, Effect, StateObservable } from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { switchMap, takeUntil, map, catchError, endWith, startWith } from 'rxjs/operators'
import { Action } from 'redux-actions'
import { push, goBack } from 'connected-react-router'
import { DeliveryService } from './delivery.service'
import { CustomerService } from '../customers/customers.service'
import {
  AccountUser,
  WholesaleDelivery,
  WholesaleRoute,
  WholesalePayload,
  WholesaleRouteDetail,
  WEEKDAYS,
  Order,
  OrderDTO,
  Address,
  Customer,
  WholesaleAddress,
  TempCustomer,
  OrderDetail,
} from '~/schema'
import { checkError, responseHandler } from '~/common/utils'
import { GlobalState } from '~/store/reducer'
import { SettingService } from '../setting/setting.service'
import _ from 'lodash'

export interface DailyDeliveryProps {
  loading: boolean
  drivers: AccountUser[]
  deliveries: WholesaleDelivery[]
  routes: WholesaleRoute[]
  routeDetails: WholesaleRouteDetail[]
  unassignedRoutes: WholesaleRoute[]
  ordersMissingDriver: any[]
  driverSchedules: WholesalePayload[]
  selectedRoute: WholesaleRoute | null
  customerInfo: WholesaleRouteDetail | null
  selectedWeekDay: string
  curDate: string
  clientList: TempCustomer[]
  addresses: WholesaleAddress[]
  unassignedOrders: OrderDetail[]
  newRouteId: number
  driverOrderTotal: number
  driverOrders: any[]
  searchProps: any
  sellerSetting: any
  getAllDeliveriesLoading: boolean
}

@Module('delivery')
export class DeliveryModule extends EffectModule<DailyDeliveryProps> {
  defaultState = {
    loading: true,
    drivers: [] as AccountUser[],
    deliveries: [] as WholesaleDelivery[],
    routes: [] as WholesaleRoute[],
    routeDetails: [] as WholesaleRouteDetail[],
    unassignedRoutes: [] as WholesaleRoute[],
    ordersMissingDriver: [],
    driverSchedules: [] as WholesalePayload[],
    selectedRoute: null,
    customerInfo: null,
    selectedWeekDay: WEEKDAYS[new Date().getDay()],
    curDate: '',
    clientList: [] as TempCustomer[],
    addresses: [] as WholesaleAddress[],
    unassignedOrders: [] as OrderDetail[],
    newRouteId: -1,
    driverOrderTotal: 0,
    driverOrders: [],
    searchProps: {},
    sellerSetting: null,
    getAllDeliveriesLoading: false,
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(private delivery: DeliveryService, private customer: CustomerService, private setting: SettingService) {
    super()
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect({
    done: (state: DailyDeliveryProps) => {
      return {
        ...state,
        loading: true,
      }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<any>) => {
      return {
        ...state,
        searchProps: action.payload,
      }
    },
  })
  setSearchProps(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<string>) => {
      return { ...state, selectedWeekDay: payload }
    },
  })
  changeSelectedWeekDay(action$: Observable<string>) {
    return action$.pipe(
      switchMap((week_day) => of(week_day)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<string>) => {
      return { ...state, curDate: payload }
    },
  })
  changeCurDate(action$: Observable<string>) {
    return action$.pipe(
      switchMap((date) => of(date)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<string>) => {
      return { ...state, currentDay: payload }
    },
  })
  setCurrentDay(action$: Observable<any>) {
    return action$.pipe(
      switchMap((currentDay) => of(currentDay)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<AccountUser[]>) => {
      return { ...state, drivers: payload }
    },
  })
  getAllDrivers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.delivery.getAllDrivers()),
      switchMap((data) => of(this.formatDriver(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesalePayload[]>) => {
      return { ...state, driverSchedules: payload }
    },
  })
  getDriverSchedules(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.delivery.getDriverSchedules()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleDelivery[]>) => {
      return { ...state, getAllDeliveriesLoading: false, deliveries: payload }
    },
  })
  getAllDeliveries(action$: Observable<{ week_day: string; current_date: string }>) {
    return action$.pipe(
      switchMap((info) => this.delivery.getAllDeliveries(info.week_day, info.current_date)),
      switchMap((data) => of(this.formatDeliveries(responseHandler(data).body.data))),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps) => {
      return {
        ...state,
        getAllDeliveriesLoading: true,
      }
    },
  })
  startWithGetAllDeliveries(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute[]>) => {
      return { ...state, routes: payload, selectedRoute: payload && payload.length > 0 ? payload[0] : null }
    },
  })
  getAllRoutes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.delivery.getAllRoutes()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute[]>) => {
      return { ...state, routes: payload }
    },
  })
  updateAllRoutes(action$: Observable<WholesaleRoute[]>) {
    return action$.pipe(
      switchMap((data) => of(data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute>) => {
      let routes = [...state.routes]
      if (payload) {
        routes.push(payload)
      }

      return {
        ...state,
        selectedRoute: payload,
        newRouteId: payload.id,
        routes: routes,
      }
    },
  })
  createRoute(action$: Observable<any>) {
    return action$.pipe(
      switchMap((route) => this.delivery.createRoute(route)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<number>) => {
      let routes = [...state.routes]
      let selectedRoute: WholesaleRoute | null = null
      if (payload && payload > 0) {
        routes = routes.filter((el) => el.id != payload)
        if (routes.length > 0) {
          selectedRoute = routes[0]
        }
      }

      return { ...state, selectedRoute: selectedRoute, routes: routes }
    },
  })
  deleteRoute(action$: Observable<number>) {
    return action$.pipe(
      switchMap((route_id) => this.delivery.deleteRoute(route_id)),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute>) => {
      return { ...state, selectedRoute: payload }
    },
  })
  getRouteDetail(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) => this.delivery.getRouteDetail(data)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute>) => {
      let routes = [...state.routes]
      for (let i = 0; i < routes.length; i++) {
        const element = routes[i]
        if (payload && element.id == payload.id) {
          routes[i] = payload
        }
      }
      return { ...state, selectedRoute: payload, routes: routes }
    },
  })
  updateSelectedRoute(action$: Observable<WholesaleRoute>) {
    return action$.pipe(
      switchMap((route) => this.delivery.updateSelectedRoute(route)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute[]>) => {
      return { ...state, unassignedRoutes: payload }
    },
  })
  getUnassignedRoutes(action$: Observable<string>) {
    return action$.pipe(
      switchMap((week_day) => this.delivery.getUnassignedRoutes(week_day)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state:any, action: any) => {
      return { ...state, ordersMissingDriver: action.payload || [] }
    }
  })
  getOrdersMissingDriver(action$: Observable<string>, state$: any){
    return action$.pipe(
      switchMap((deliveryDate) => this.delivery.getOrdersMissingDriver(deliveryDate)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRouteDetail[]>) => {
      return { ...state, routeDetails: payload }
    },
  })
  getRouteDetailsByRouteId(action$: Observable<number>) {
    return action$.pipe(
      switchMap((route_id) => this.delivery.getRouteDetailsByRouteId(route_id)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<number>) => {
      if (payload != undefined && payload > 0) {
        let data = state.routeDetails.filter((el) => el.id != payload)
        return { ...state, routeDetails: data }
      }
      return state
    },
  })
  removeRouteDetailById(action$: Observable<number>) {
    return action$.pipe(
      switchMap((detail_id) => this.delivery.removeRouteDetailsById(detail_id)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute[]>) => {
      return { ...state, routeDetails: payload }
    },
  })
  updateRouteDetails(action$: Observable<WholesaleRouteDetail[]>) {
    return action$.pipe(
      switchMap((data) => of(data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRoute>) => {
      console.log(payload)
      return { ...state, selectedRoute: payload }
    },
  })
  selectedRouteChange(action$: Observable<WholesaleRoute>) {
    return action$.pipe(
      switchMap((data) => of(data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRouteDetail>) => {
      console.log(payload)
      return { ...state, customerInfo: payload }
    },
  })
  getCustomerInfoByCustomerId(
    action$: Observable<{ customerId: number; routeId: number; detailId: number; shippingAddressId: number }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) =>
        this.delivery
          .getCustomerInfoByCustomerId(info.customerId, info.routeId, info.detailId, info.shippingAddressId)
          .pipe(
            switchMap((data) => of(responseHandler(data).body.data)),
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getRouteDetailsByRouteId)(state$.value.delivery!.selectedRoute!.id!)),
            catchError((error) => of(checkError(error))),
          ),
      ),
      // switchMap((data) => of(responseHandler(data).body.data)),
      // switchMap((this.createActionFrom(this.getRouteDetailsByRouteId)(routeId)))
      // map(this.createAction('done'), takeUntil(this.dispose$)),
      // catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleRouteDetail>) => {
      if (payload) {
        state.routeDetails.forEach((el) => {
          if (el.id == payload.id) {
            el.address = payload.address
            return
          }
        })
      }
      return state
    },
  })
  updateShippingAddress(
    action$: Observable<{ detail_id: number; addressId: number }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) =>
        this.delivery.updateShippingAddress(info.detail_id, info.addressId).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getRouteDetailsByRouteId)(state$.value.delivery!.selectedRoute!.id!)),
          catchError((error) => of(checkError(error))),
        ),
      ),
      // switchMap((data) => of(responseHandler(data).body.data)),
      // map(this.createAction('done'), takeUntil(this.dispose$)),
      // catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<string[]>) => {
      //payload will be the id array with 2 elements like [1, 2]
      //first element is old driver id, second element is updated driver id in driver schedule table
      if (payload && payload.length > 0) {
        let deliveries = [...state.deliveries]
        if (deliveries && deliveries.length > 0) {
          const delivery: any = { ...deliveries.find((el) => el.driver.userId == payload[0]) }
          deliveries.forEach((element) => {
            if (element.driver.userId == payload[0]) {
              element.routes = []
              element.orders = []
              element.totalOrders = 0
              element.totalUnits = 0
            }
            if (element.driver.userId == payload[1]) {
              element.routes = element.routes.concat(delivery.routes)
              element.orders = element.orders.concat(delivery.orders)
              element.totalOrders += delivery.totalOrders
              element.totalUnits += delivery.totalUnits
            }
          })
          return { ...state, deliveries: deliveries }
        }
      }
      return state
    },
  })
  reAssignDriver(
    action$: Observable<{ driverId: number; routes: WholesaleRoute[] }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) => this.delivery.reAssignDriver(info, state$.value.delivery.selectedWeekDay)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<OrderDTO[]>) => {
      let deliveries = [...state.deliveries]

      if (payload) {
        payload.forEach((el) => {
          for (let i = 0; i < deliveries.length; i++) {
            let element = deliveries[i]
            for (let j = 0; j < element.orders.length; j++) {
              let order = element.orders[j]
              if (order.wholesaleOrderId == el.wholesaleOrderId) {
                element.orders[j] = el
              }
            }
          }
        })
      }

      return state
    },
  })
  reAssignRoute(action$: Observable<{ routeId: number; orderIds: any[] }>) {
    return action$.pipe(
      switchMap((info) => this.delivery.reAssignRoute(info.routeId, info.orderIds)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<any>) => {
      return state
    },
  })
  overrideDriver(action$: Observable<{ driverId: number; data: any }>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((info) =>
        this.delivery.overrideDriver(info.driverId, info.data).pipe(
          map(this.createAction('done'), takeUntil(this.dispose$)),
          endWith(
            this.createActionFrom(this.getAllDeliveries)({
              current_date: state$.value.delivery!.curDate,
              week_day: state$.value.delivery!.selectedWeekDay,
            }),
          ),
          endWith(this.createActionFrom(this.getOrdersMissingDriver)(state$.value.delivery!.curDate)),
          catchError((error) => of(checkError(error))),
        ),
      ),
      // switchMap((data) => of(responseHandler(data, true).body.data)),
      // map(this.createAction('done'), takeUntil(this.dispose$)),
      // catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleDelivery>) => {
      let deliveries = [...state.deliveries]
      let unassignedRoutes = [...state.unassignedRoutes]
      console.log(payload)
      if (payload && payload.routes.length > 0) {
        deliveries.forEach((element) => {
          if (element.driver.userId == payload.driver.userId) {
            element.routes.push(payload.routes[0])
            element.totalOrders += payload.totalOrders
            element.totalUnits += payload.totalUnits
            element.completedOrders += payload.completedOrders
            element.orders = element.orders.concat(payload.orders)
            return
          }
        })
        unassignedRoutes = unassignedRoutes.filter((el) => el.id != payload.routes[0].id)
      }

      return { ...state, deliveries: deliveries, unassignedRoutes: unassignedRoutes }
    },
  })
  assignRoute(
    action$: Observable<{ routeId: number; driverId: number; deliveryWeekofDay: string }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) => this.delivery.assignRoute(info, state$.value.delivery.curDate)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, { payload }: Action<WholesaleDelivery>) => {
      let deliveries = [...state.deliveries]
      let unassignedRoutes = [...state.unassignedRoutes]
      if (payload && payload.routes.length > 0) {
        deliveries.forEach((element) => {
          if (element.driver.userId == payload.driver.userId) {
            let removedRoute = payload.routes[0]
            element.routes = element.routes.filter((el) => el.id != removedRoute.id)
            element.totalOrders =
              element.totalOrders > payload.totalOrders ? element.totalOrders - payload.totalOrders : 0
            element.totalUnits = element.totalUnits > payload.totalUnits ? element.totalUnits - payload.totalUnits : 0
            element.completedOrders =
              element.completedOrders > payload.completedOrders ? element.completedOrders - payload.completedOrders : 0
            element.orders = element.orders.filter(
              (item1) => !payload.orders.some((item2) => item2.wholesaleOrderId === item1.wholesaleOrderId),
            )
            return
          }
        })
        unassignedRoutes.push(payload.routes[0])
        unassignedRoutes.sort((a, b) => (a.id > b.id ? 1 : -1))
      }
      return { ...state, deliveries: deliveries, unassignedRoutes: unassignedRoutes }
    },
  })
  removeRoute(
    action$: Observable<{ routeId: number; driverId: number; deliveryWeekofDay: string }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) => this.delivery.removeRoute(info, state$.value.delivery.curDate)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  formatDriver(arr: any[]): AccountUser[] {
    return arr.map((data) => ({
      id: data.userId,
      emailAddress: data.emailAddress,
      firstName: data.firstName,
      lastName: data.lastName,
      phone: data.phone,
      status: data.status,
      addressId: data.address ? data.address.addressId : null,
      company: data.company,
      password: '',
      imagePath: data.imagePath,
    }))
  }

  formatDeliveries(arr: any): WholesaleDelivery[] {
    const { drivers, orders, schedules } = arr
    const scheduleGroupData = _.groupBy(schedules, 'driver.userId')
    return drivers.map((driver: any) => {
      const driverOrders = orders.filter((order: any) => {
        if (order.overrideDriverId) {
          return order.overrideDriverId == driver.userId
        } else {
          return order.defaultDriverId == driver.userId
        }
      })
      return {
        driver: driver,
        routes: scheduleGroupData[driver.userId] ? _.map(scheduleGroupData[driver.userId], 'wholesaleRoute') : [],
        completedOrders: 0,
        totalOrders: driverOrders.length,
        totalUnits: _.sum(_.map(driverOrders, 'sumOfUnits')),
        totalPicks: _.sum(_.map(driverOrders, 'sumOfPicks')),
        orders: driverOrders,
      }
    })
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<Customer[]>) => {
      return {
        ...state,
        clientList: action.payload,
      }
    },
  })
  getAllCustomersForUser(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser.userId).pipe(
          switchMap((data: any) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          takeUntil(this.dispose$),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<WholesaleAddress[]>) => {
      return {
        ...state,
        addresses: action.payload,
      }
    },
  })
  getAddresses(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customerId) => this.delivery.getShippingAddress(customerId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<WholesaleAddress[]>) => {
      return {
        ...state,
        unassignedOrders: action.payload,
      }
    },
  })
  getUnassignedOrdersByDeliverDate(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => this.delivery.getUnassignedOrdersByDeliverDate(data)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  assignOrderToRoute(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.delivery.assignOrderToRoute(data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getUnassignedOrdersByDeliverDate)(state$.value.delivery!.curDate)),
          endWith(
            this.createActionFrom(this.getAllDeliveries)({
              current_date: state$.value.delivery!.curDate,
              week_day: state$.value.delivery!.selectedWeekDay,
            }),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  deleteDriverSchedule(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.delivery.deleteDriverSchedule(data).pipe(
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getAllDeliveries)({
              current_date: state$.value.delivery!.curDate,
              week_day: state$.value.delivery!.selectedWeekDay,
            }),
          ),
          endWith(this.createActionFrom(this.getUnassignedRoutes)(state$.value.delivery!.selectedWeekDay)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<any>) => {
      const { payload } = action
      return {
        ...state,
        loading: false,
        driverOrders: typeof payload === 'string' ? [] : payload && payload.dataList ? payload.dataList : [],
        driverOrderTotal: typeof payload === 'string' ? 0 : payload && payload.total ? payload.total : 0,
      }
    },
  })
  getDriverReportHistory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => this.delivery.getDriverReportHistory(data)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: DailyDeliveryProps, action: Action<any>) => {
      return {
        ...state,
        sellerSetting: action.payload.userSetting,
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getUserSetting(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(map(this.createAction('done'))),
      ),
      catchError((error) => of(checkError(error))),
    )
  }
}
export type DeliveryDispatchProps = ModuleDispatchProps<DeliveryModule>
