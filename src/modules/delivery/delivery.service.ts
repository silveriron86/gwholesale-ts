import { Http, CACHED_USER_ID } from '~/common'
import { Injectable } from 'redux-epics-decorator'
import { WholesaleRoute } from '~/schema';

@Injectable()
export class DeliveryService {
  constructor(private readonly http: Http) { }

  getAllDrivers() {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/drivers/${user_id}`)
  }

  getDriverSchedules() {
    return this.http.get<any>(`/delivery/payloads/allpayloads`)
  }

  getAllDeliveries(week_day: string, current_date: string) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/${user_id}/deliveries/${week_day}?date=${current_date}`)
  }

  getAllRoutes() {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/routes/allroutes/${user_id}`)
  }

  getUnassignedRoutes(week_day: string) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/routes/unassigned/${week_day}/${user_id}`)
  }

  getOrdersMissingDriver(deliveryDate: string) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/user/ordersMissingDriver/${user_id}?deliveryDate=${deliveryDate}`)
  }

  getRouteDetailsByRouteId(route_id: number) {
    return this.http.get<any>(`/delivery/${route_id}/routeDetail`)
  }

  removeRouteDetailsById(detail_id: number) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.delete<any>(`/delivery/${user_id}/routeDetail/${detail_id}`)
  }

  getCustomerInfoByCustomerId(customer_id: number, route_id: number, detail_id: number, shippingAddressId: number) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/customerinfo/${user_id}/${customer_id}?route_id=${route_id}&detail_id=${detail_id}&address_id=${shippingAddressId}`)
  }

  updateSelectedRoute(route: WholesaleRoute) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.put<any>(`/delivery/${user_id}/route/${route.id}`, {
      body: JSON.stringify(route),
    })
  }

  createRoute(route: any) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/route`, {
      body: JSON.stringify(route),
    })
  }

  deleteRoute(route_id: number) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.delete<any>(`/delivery/${user_id}/route/${route_id}`)
  }

  getRouteDetail(route_id: number) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/${user_id}/route/${route_id}`)
  }

  reAssignDriver(info: any, week_day: string) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/reassigndriver/${info.driverId}/${week_day}`, {
      body: JSON.stringify(info.routes),
    })
  }

  reAssignRoute(routeId: number, orderIds: any[]) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/reassignroute/${routeId}`, {
      body: JSON.stringify(orderIds),
    })
  }

  assignRoute(data: any, cur_date: string) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/assignroute?cur_date=${cur_date}`, {
      body: JSON.stringify(data),
    })
  }

  removeRoute(data: any, cur_date: string) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/removeroute?cur_date=${cur_date}`, {
      body: JSON.stringify(data),
    })
  }

  updateShippingAddress(detail_id: number, addressId: number) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/updateroutedetail/${detail_id}/${addressId}`)
  }

  overrideDriver(driverId: number, data: any) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/overridedriver/${driverId}`, {
      body: JSON.stringify(data),
    })
  }

  getShippingAddress(clientId: string) {
    return this.http.get<any>(`/account/user/getShippingAddress/${clientId}`)
  }

  getUnassignedOrdersByDeliverDate(deliveryDate: string) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.get<any>(`/delivery/user/unassignOrders/${user_id}?deliveryDate=${deliveryDate}`)
  }

  assignOrderToRoute(data: any) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.post<any>(`/delivery/${user_id}/order/${data.orderId}/assignRoute/${data.routeId}?deliveryDate=${data.deliveryDate}`)
  }

  deleteDriverSchedule(data: any) {
    const user_id = localStorage.getItem(CACHED_USER_ID);
    return this.http.delete<any>(`/delivery/${user_id}/driverSchedule/${data.routeId}?week_day=${data.weekDay}&cur_date=${data.curDate}`)
  }

  getDriverReportHistory(data: any) {
    return this.http.get<any>(`/delivery/driver-reports/?from=${data.from}&to=${data.to}&curPage=${data.curPage}&pageSize=${data.pageSize}&search=${data.search}&driverId=${data.driverId}`)
  }
}
