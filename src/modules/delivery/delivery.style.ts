import styled from '@emotion/styled'
import { mediumGrey, white } from '~/common'
import { Button, Row, Input, Spin, Tabs } from 'antd'

export const DeliveryHeader = styled('div')({
  height: 127.57,
  boxShadow: '0px 12px 14px -6px rgba(0,0,0,0.15)',
  padding: '25px 30px',
  marginLeft: 49,
  position: 'fixed',
  top: 100,
  left: 0,
  width: 'calc(100% - 49px)',
  zIndex: 999,
  background: white,
})

export const HomeTabs = styled(Tabs)`
  .ant-tabs-nav-scroll {
    text-align: left;
    padding-left: 80px
  } 
  .ant-tabs-nav-wrap {
    position: fixed;
    z-index: 1000;
    width: 100%;
    background: #fff;
    box-shadow: 10px 5px 5px #e0e0e0;
  }
  .ant-tabs-nav .ant-tabs-tab {
    padding: 16px;
  }
`

export const DeliveryBody = styled('div')({
  marginTop: 155,
  position: 'relative'
})

export const InfoLabel = styled('div')({
  fontSize: 14,
  fontFamily: 'Museo Sans Rounded',
  color: mediumGrey,
  marginBottom: 16,
  textAlign: 'left',
  width: '50%'
})

export const AmountLabel = styled('div')({
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 'bold',
  fontSize: 20,
  lineHeight: '20px',
  color: mediumGrey,
  marginBottom: 8,
  borderBottom: `1px solid ${mediumGrey}`,
  textAlign: 'left',
  width: '50%'
})

export const ThemeButton: any = styled(Button)((props) => ({
  color: 'white',
  backgroundColor: props.theme.primary,
  borderColor: props.theme.primary,
  '&:hover': {
    color: 'white',
    backgroundColor: props.theme.theme,
    borderColor: props.theme.theme,
  },
  '&:focus': {
    color: 'white',
    backgroundColor: props.theme.primary,
    borderColor: props.theme.primary,
  },
}))

export const floatRight: React.CSSProperties = {
  float: 'right',
}

export const transLayout: React.CSSProperties = {
  backgroundColor: 'transparent',
}

export const InputRow: any = styled(Row)((props) => ({
  marginBottom: 5,
}))


export const InputLabel = styled('div')({
  color: mediumGrey,
  fontSize: 12,
  lineHeight: '12px',
  fontWeight: 'bold',
  letterSpacing: '0.05em',
  alignItems: 'center',
  padding: '10px 0',
})

export const PopoverWrapper = styled('div')((props) => ({
  padding: 10,
}))


export const ThemeInput: any = styled(Input)((props) => ({
  '&:hover': {
    borderColor: props.theme.primary,
  },
  '&:focus': {
    borderColor: props.theme.primary,
    boxShadow: 'none',
  },
  '&.ant-input-affix-wrapper:hover .ant-input:not(.ant-input-disabled), &.ant-input-affix-wrapper:hover .ant-input:not(.ant-input-disabled)': {
    borderColor: props.theme.primary,
  }
}))

export const SpinWrap: any = styled(Spin)((props) => ({
  height: '100vh !important',
  maxHeight: '100vh !important',
  '&': {
    color: props.theme.light,
    '.ant-spin-dot-item': {
      backgroundColor: props.theme.light,
    }
  },
}))
