import * as React from 'react'
import { Layout, Row, Col, Divider as AntDivider } from 'antd'

import {
  layoutStyle,
  Flex,
  DetailWrapperStyle,
  DeailWrapper,
  detailWrapperDivider,
  NormalSpan,
  BoldSpan
} from '~/modules/customers/components/customers-detail/customers-detail.style'

interface DetailProps {
  driver: any
}

export class DriverDetail extends React.PureComponent<DetailProps> {
  render() {
    const { driver } = this.props
    return (
      <Layout style={layoutStyle}>
        <Row style={{ marginTop: 4 }} type="flex" justify="space-between" />
        {/* <DetailWrapperStyle type="flex" justify="space-between">
          <Col lg={6} md={12}>
            <DeailWrapper>
              <Row>
                <Col lg={10} md={24}>
                  <NormalSpan>STATUS</NormalSpan>
                </Col>
                <Col lg={14} md={24}>
                  <BoldSpan>{customer ? customer.status : '--'}</BoldSpan>
                </Col>
              </Row>
              <Row>
                <Col lg={10} md={24}>
                  <NormalSpan>BUSINESS TYPE</NormalSpan>
                </Col>
                <Col lg={14} md={24}>
                  <BoldSpan>{customer ? customer.businessType : '--'}</BoldSpan>
                </Col>
              </Row>
              <Row>
                <Col lg={10} md={24}>
                  <NormalSpan>MAIN PHONE</NormalSpan>
                </Col>
                <Col lg={14} md={24}>
                  <BoldSpan>{customer && customer.mobilePhone ? customer.mobilePhone : '--'}</BoldSpan>
                </Col>
              </Row>
              <Row>
                <Col lg={10} md={24}>
                  <NormalSpan>DBA NAME</NormalSpan>
                </Col>
                <Col lg={14} md={24}>
                  <BoldSpan>{customer && customer.dba ? customer.dba : '--'}</BoldSpan>
                </Col>
              </Row>
            </DeailWrapper>
          </Col>
          <Col lg={6} md={12}>
            <Flex>
              <AntDivider type="vertical" style={detailWrapperDivider} />
              <DeailWrapper style={{ flex: 1 }}>
                <Row>
                  <Col lg={10} md={24}>
                    <NormalSpan>ALT.PHONE</NormalSpan>
                  </Col>
                  <Col lg={14} md={24}>
                    <BoldSpan>{customer && customer.alternativePhone ? customer.alternativePhone : '--'}</BoldSpan>
                  </Col>
                </Row>
                <Row>
                  <Col lg={10} md={24}>
                    <NormalSpan>FAX</NormalSpan>
                  </Col>
                  <Col lg={14} md={24}>
                    <BoldSpan>{customer ? customer.fax : '--'}</BoldSpan>
                  </Col>
                </Row>
                <Row>
                  <Col lg={10} md={24}>
                    <NormalSpan>EMAIL</NormalSpan>
                  </Col>
                  <Col lg={14} md={24}>
                    <BoldSpan>{customer && customer.email ? customer.email : '--'}</BoldSpan>
                  </Col>
                </Row>
                <Row>
                  <Col lg={10} md={24}>
                    <NormalSpan>WEBSITE</NormalSpan>
                  </Col>
                  <Col lg={14} md={24}>
                    <BoldSpan>{customer ? customer.website : '--'}</BoldSpan>
                  </Col>
                </Row>
              </DeailWrapper>
            </Flex>
          </Col>
          <Col lg={6} md={12}>
            <Flex>
              <AntDivider type="vertical" style={detailWrapperDivider} />
              <DeailWrapper style={{ flex: 1 }}>
                <Row>
                  <Col lg={10} md={24}>
                    <NormalSpan>ACCOUNTING REP</NormalSpan>
                  </Col>
                  <Col lg={14} md={24}>
                    <BoldSpan>
                      {customer && customer.accountant
                        ? `${customer.accountant.firstName} ${customer.accountant.lastName}`
                        : '--'}
                    </BoldSpan>
                  </Col>
                </Row>
                <Row>
                  <Col lg={10} md={24}>
                    <NormalSpan>SALES REP</NormalSpan>
                  </Col>
                  <Col lg={14} md={24}>
                    <BoldSpan>
                      {customer && customer.seller && `${customer.seller.firstName} ${customer.seller.lastName}`}
                    </BoldSpan>
                  </Col>
                </Row>
                <Row>
                  <Col lg={10} md={24}>
                    <NormalSpan>ACCOUNT MANAGER</NormalSpan>
                  </Col>
                  <Col lg={14} md={24}>
                    <BoldSpan>
                      {customer && customer.manager
                        ? `${customer.manager.firstName} ${customer.manager.lastName}`
                        : '--'}
                    </BoldSpan>
                  </Col>
                </Row>
              </DeailWrapper>
            </Flex>
          </Col>
          <Col lg={6} md={12}>
            <Flex>
              <AntDivider type="vertical" style={detailWrapperDivider} />
              <DeailWrapper style={{ flex: 1 }}>
                <Row>
                  <Col lg={6} md={24}>
                    <NormalSpan>NOTES</NormalSpan>
                  </Col>
                  <Col lg={18} md={24}>
                    <BoldSpan>{customer ? customer.notes : ''}</BoldSpan>
                  </Col>
                </Row>
              </DeailWrapper>
            </Flex>
          </Col>
        </DetailWrapperStyle> */}
      </Layout>
    )
  }
}

export default DriverDetail
