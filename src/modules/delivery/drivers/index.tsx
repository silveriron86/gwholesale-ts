/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'

import { Theme } from '~/common'
import { DeliveryDispatchProps, DailyDeliveryProps, DeliveryModule } from '..'
import PageLayout from '~/components/PageLayout'
import DriverReportHeader from './report-header'
import moment from 'moment'
import DriverDetail from './driver-detail'
import DriverOrderHistory from './order-history'
import { ReportPageContainer } from '~/modules/location/location.style'


export type DriverReportsProps = DeliveryDispatchProps & DailyDeliveryProps & {
  theme: Theme,
}

class DriverReports extends React.PureComponent<DriverReportsProps> {
  state: any
  constructor(props: DriverReportsProps) {
    super(props)
    this.state = {
      from: moment().subtract(30, 'days'),
      to: moment(),
      curPage: 0,
      pageSize: 12,
      search: '',
      driverId: 0,
      curDriver: null
    }
  }
  componentDidMount() {
    this.props.getAllDrivers()
    this.getReports()
  }

  getReports = () => {
    const seachObj = {
      ...this.state,
      from: this.state.from.format('MM/DD/YYYY'),
      to: this.state.to.format('MM/DD/YYYY'),
    }
    this.props.resetLoading()
    this.props.setSearchProps(seachObj)
    this.props.getDriverReportHistory(seachObj)
  }

  onChange = (type: string, data: any) => {
    if (type == 'date') {
      this.setState({ from: data.from, to: data.to })
    } else {
      let obj = this.state
      obj[type] = data
      this.setState({ ...obj })

    }

    setTimeout(() => {
      this.getReports()
    }, 100)
  }

  render() {
    const { from, to, curPage, pageSize, search, driverId } = this.state
    const { drivers, driverOrderTotal, driverOrders, loading } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Selling & Shipping-Banking Journal'}>
        <ReportPageContainer>
          <DriverReportHeader
            onSearchChange={this.onChange}
            from={from}
            to={to}
            search={search}
            theme={this.props.theme}
            drivers={drivers}
          />
          <DriverDetail driver={drivers.length > 0 && driverId > 0 ? drivers[driverId] : null} />
          <DriverOrderHistory
            curPage={curPage}
            pageSize={pageSize}
            total={driverOrderTotal}
            onPageChange={this.onChange}
            data={driverOrders}
            loading={loading}
          />
        </ReportPageContainer>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export default withTheme(connect(DeliveryModule)(mapStateToProps)(DriverReports))
