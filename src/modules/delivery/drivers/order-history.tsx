import React from 'react'
import moment from 'moment'
import { ThemeTable } from '~/modules/customers/customers.style'
import { QBOImage, NSImage } from '~/modules/orders/components/orders-table.style'
import qboImage from '~/modules/orders/components/qbo.png'
import { Flex } from '~/modules/customers/components/customer-detail/customer-detail.style'
import { capitalize } from 'lodash'
import { CACHED_QBO_LINKED } from '~/common';

export type DriverOrderHistoryProps = {
  total: number
  pageSize: number
  curPage: number
  onPageChange: Function
  data: any[]
  loading: boolean
}

const renderLinkedIcon: any = () => {
  if (localStorage.getItem(CACHED_QBO_LINKED) != 'null')
    return <QBOImage src={qboImage} />
  return <NSImage />
}

const columns: any[] = [
  {
    title: 'DELIVERY DATE',
    dataIndex: 'deliveryDate',
    key: 'deliveryDate',
    align: 'center',
    render: (date: number) => {
      return moment(date).format('MM/DD/YYYY')
    }
  },
  {
    title: 'SO #',
    dataIndex: 'wholesaleOrderId',
    key: 'wholesaleOrderId',
    align: 'center',
    render(text: number) {
      return <span>{text ? `# ${text}` : ''}</span>
    },
  },
  {
    title: 'REFERENCE',
    dataIndex: 'reference',
    key: 'reference',
    align: 'center'
  },

  {
    title: 'ORDER STATUS',
    dataIndex: 'status',
    key: 'status',

    render: (status: string) => {
      return (
        <Flex style={{ alignItems: 'center', justifyContent: 'center' }}>
          {status === 'INVOICED' ?
            renderLinkedIcon()
            :
            null
          }
          <span style={{ marginLeft: 10 }}>{capitalize(status)}</span>
        </Flex>
      )
    }
  },

]
class DriverOrderHistory extends React.PureComponent<DriverOrderHistoryProps> {


  onPageChange = (page: any) => {
    this.props.onPageChange('curPage', page - 1)
  }
  render() {
    const { total, pageSize, curPage, data, loading } = this.props
    return (
      <ThemeTable
        pagination={{
          hideOnSinglePage: true,
          pageSize: pageSize,
          onChange: this.onPageChange,
          current: curPage + 1,
          total: total,
          defaultCurrent: 1
        }}
        columns={columns}
        dataSource={data}
        loading={loading}
        rowKey="wholesaleOrderId"
      />
    )
  }
}

export default DriverOrderHistory
