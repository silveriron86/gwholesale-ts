import React from 'react'
import { Moment } from 'moment'
import { Theme } from '~/common'

import { Row, Col, DatePicker, Select } from 'antd'
import { HeaderTitle, ThemeSelect } from '~/modules/customers/customers.style'
import GrubSuggest from '~/components/GrubSuggest'
import { HeaderElement, HistoryHeaderWrapper } from '~/modules/location/location.style'
import { AccountUser } from '~/schema'

const { RangePicker } = DatePicker
const { Option } = Select
export type ReportHeaderProps = {
  theme: Theme
  onSearchChange: Function
  from: Moment
  to: Moment
  search: string
  drivers: AccountUser[]
}

class DriverReportHeader extends React.PureComponent<ReportHeaderProps> {

  onSearch = (keyword: string) => {
    this.props.onSearchChange('search', keyword)
  }

  onDateChange = (date: any, dateString: [string, string]) => {
    this.props.onSearchChange('date', { from: date[0], to: date[1] })
  }

  onSelectChange = (value: number) => {
    let driverId = value
    if (!value) {
      driverId = 0
    }
    this.props.onSearchChange('driverId', driverId)
  }

  onSuggest = (str: string) => {
    return []
  }

  render() {
    const { from, to, theme, drivers } = this.props
    return (
      <HistoryHeaderWrapper>
        <HeaderTitle style={{ marginBottom: 20 }}>Driver Reports</HeaderTitle>
        <Row>
          <Col span={6}>
            <HeaderElement>
              <GrubSuggest theme={theme} onSuggest={this.onSuggest} onSearch={this.onSearch} />
            </HeaderElement>
          </Col>
          <Col offset={1} span={6}>
            <HeaderElement>
              <RangePicker
                placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                format={'MM/DD/YYYY'}
                defaultValue={[from, to]}
                onChange={this.onDateChange}
                allowClear={false}
              />
            </HeaderElement>
          </Col>
          <Col offset={5} span={6}>
            <HeaderElement>
              <ThemeSelect
                onChange={this.onSelectChange}
                placeholder={'Please select the driver...'}
                allowClear={true}
                onClear={this.onSelectChange}
              >

                {
                  drivers.map(el => {
                    return (<Option key={el.id} value={el.id}>{el.firstName} {el.lastName}</Option>)
                  })
                }
              </ThemeSelect>
            </HeaderElement>
          </Col>
        </Row>
      </HistoryHeaderWrapper>
    )
  }
}

export default DriverReportHeader