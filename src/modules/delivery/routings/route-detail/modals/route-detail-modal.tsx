import * as React from 'react'
import {
  transLayout,
  InputLabel,
  PopoverWrapper,
  InputRow,
  floatRight,
  ThemeInput,
  ThemeButton,
} from '../../../delivery.style'
import Form, { FormComponentProps } from 'antd/lib/form'
import { TempCustomer,Address, WholesaleAddress } from '~/schema'
import { Icon, Col, Layout, Row, Select, AutoComplete } from 'antd'
import { mb0 } from '~/modules/customers/sales/_style'

const FormItem = Form.Item
const { Option } = Select  

interface RouteDetailModalProps extends FormComponentProps {
  // data: any,
  clientList: TempCustomer[],
  selectedRoute: any,
  addRouteDetailInfo: Function,
  getAddressesByClientId: Function,
  addresses: WholesaleAddress[]
}

function renderOption(client: TempCustomer){
  return (
    <Option key={client.clientId} text={client.clientCompany.companyName}>
    <div className="global-search-item">
      {client.clientCompany.companyName}
    </div>
  </Option>
  )
}

class RouteDetailModal extends React.PureComponent<RouteDetailModalProps> {
  state: any
  constructor(props: RouteDetailModalProps) {
    super(props)
    const { clientList } = props
    this.state = {
      newRouteDetail:{
        clientId:'',
        shippingAddressId:''
      },
      clientList,
      addressList:[]
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if(this.props.addresses != nextProps.addresses){
      this.setState({
        addressList: nextProps.addresses
      })
    }

  }

  onDeleteAddress = () =>{
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const route_id = this.props.selectedRoute ? this.props.selectedRoute.id : 0
        const data = {
          customerId: values.newRouteDetail.clientId,
          routeId: route_id,
          shippingAddressId: values.newRouteDetail.shippingAddressId,
          detailId: 0
        }
        this.props.addRouteDetailInfo(data)
        form.resetFields();
      }
    })
  }

  onClickSubmit = (arg: boolean) => {
  }

  autoCompleteSelect = (value: any) => {
    this.props.getAddressesByClientId(value);
  }

  renderAddresses = (addresses: WholesaleAddress[]) => {
    let rows: any[] = []
    if(addresses && addresses.length > 0) {
      addresses.forEach((obj, index) => {
        if(obj && obj.address) {
          let addressInfo = obj.address;
          let address = addressInfo.street1 + ", " + addressInfo.city + ", " + addressInfo.state
          rows.push(<Option key={index} value={obj.wholesaleAddressId}>{address}</Option>)
        }
      });
    }
    return rows
  }
  changeCompany = () => {
    this.setState({
      'newRouteDetail.shippingAddressId':''
    })
    this.props.form.setFieldsValue({
      'newRouteDetail.shippingAddressId':''
    });
  }

  render() {
    const { newRouteDetail,clientList } = this.state
    const { getFieldDecorator } = this.props.form

    return (
      <PopoverWrapper style={{ width: 600 }}>
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
          <Layout style={transLayout}>
            <InputRow>
              <Col md={7}>
                <InputLabel>ACCOUNT NAME</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('newRouteDetail.clientId', {
                    rules: [{ required: true,message: 'Please input company name !'  }],
                    initialValue: newRouteDetail.clientId
                  })( 
                  <AutoComplete
                    style={{ width: '100%' }}
                    dataSource={clientList.map(renderOption)}
                    placeholder="Customer company name"
                    optionLabelProp="text"
                    allowClear
                    filterOption={
                      (inputValue, option) => option.props.text.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                    }
                    onSelect={this.autoCompleteSelect.bind(this)}
                    onChange={this.changeCompany}
                  />
                  )}
                </FormItem>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>SHIPPING ADDRESS</InputLabel>
              </Col>
              <Col md={17}>
                <FormItem style={mb0}>
                  {getFieldDecorator('newRouteDetail.shippingAddressId', {
                    rules: [{ required: true, message: 'Shipping address is required!' }],
                    initialValue: newRouteDetail.shippingAddressId
                  })(
                    <Select style={{width: '100%'}}>
                      {this.renderAddresses(this.state.addressList)}
                    </Select>
                  )}
                </FormItem>
              </Col>
            </InputRow>         
            <Row>
              <Col md={10}>
              </Col>
              <Col md={14}>
                <ThemeButton shape="round" type="primary" htmlType="submit" style={floatRight} onClick={this.onClickSubmit.bind(this, false)}>
                  <Icon type="save" />
                  Save
                </ThemeButton>
              </Col>
            </Row>
          </Layout>
        </Form>
      </PopoverWrapper>
    )
  }
}

export default Form.create<RouteDetailModalProps>()(RouteDetailModal)