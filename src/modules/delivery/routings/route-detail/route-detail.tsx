/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Col, Row, Popconfirm, Select, AutoComplete, Popover, Icon } from 'antd'

import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { DeliveryModule, DeliveryDispatchProps, DailyDeliveryProps } from '../../'
import {
  AddressDetailTitle,
  HeaderDivider,
  AddressOnRoute,
  EditTableWrapper,
  RouteAddressesTitle,
} from './../routing.style'
import EditableTable from '~/components/Table/editable-table'
import { WholesaleRouteDetail, Address, TempCustomer } from '~/schema'
import { PayloadIcon } from '~/components/elements/elements.style'
import { Icon as IconSvg } from '~/components/icon/'
import { ThemeButton } from '../../delivery.style'
import RouteDetailModal from './modals/route-detail-modal'
import _ from 'lodash'
import { FlexRow } from '~/modules/customers/customers.style'

export type RouteDetailProps = DeliveryDispatchProps &
  DailyDeliveryProps & {
    theme: Theme
  }

const { Option } = Select

function renderOption(client: TempCustomer) {
  return (
    <Option key={client.clientId} text={client.clientCompany.companyName}>
      <div className="global-search-item">{client.clientCompany.companyName}</div>
    </Option>
  )
}
export class RouteDetailComponent extends React.PureComponent<RouteDetailProps> {
  state: any
  selectedDetailId: number = 0
  column: any
  constructor(props: RouteDetailProps) {
    super(props)
    this.state = {
      datasource: null,
      routeDetails: null,
      clientList: [],
      newModalVisible: false,
    }
    this.column = [
      {
        title: '#',
        dataIndex: 'index',
        key: 'index',
        className: 'first-col',
      },
      {
        title: 'ACCOUNT #',
        dataIndex: 'clientId',
        key: 'clientId',
      },
      {
        title: 'ACCOUNT NAME',
        dataIndex: 'name',
      },
      {
        title: 'SHIPPING ADDRESS',
        dataIndex: '',
        key: 'address',
        render: (text: any, record: any) => {
          if (record.detail_id && !isNaN(parseInt(record.detail_id)) && parseInt(record.detail_id) > 0) {
            return (
              <Select
                style={{ width: '100%' }}
                defaultValue={record.shipping_address ? record.shipping_address.address.addressId : ''}
                onChange={this.shippingAddressChange.bind(this, record)}
              >
                {this.renderAvailableAddresses(record)}
              </Select>
            )
          } else {
            return <span />
          }
        },
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        width: 150,
        render: (text: number, record: any) => {
          if (record.detail_id && !isNaN(parseInt(record.detail_id)) && parseInt(record.detail_id) > 0) {
            return (
              <Popconfirm title="Sure to delete?" onConfirm={() => this.removeAddress(record.detail_id)}>
                <a>
                  <PayloadIcon>
                    <IconSvg type="close" viewBox="0 0 20 28" width={15} height={14} />
                  </PayloadIcon>
                </a>
              </Popconfirm>
            )
          } else {
            return <span />
          }
        },
      },
    ]
  }

  componentDidMount() {
    if (this.props.routeDetails && this.props.routeDetails.length > 0) {
      const data = this.formatTableData(this.props.routeDetails)
      this.setState({ datasource: data, routeDetails: this.props.routeDetails })
    } else {
      this.setState({ datasource: [], routeDetails: this.props.routeDetails })
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (!this.equal(this.props.customerInfo, nextProps.customerInfo)) {
      if (nextProps.customerInfo) {
        const routeDetails = [...this.state.routeDetails]
        let data: any
        if (this.selectedDetailId == 0) {
          if (_.map(routeDetails, 'detail_id').indexOf(nextProps.customerInfo.detail_id) == -1) {
            routeDetails.push(nextProps.customerInfo)
          }
        } else {
          for (let i = 0; i < routeDetails.length; i++) {
            const element = routeDetails[i]
            if (element.id == this.selectedDetailId) {
              routeDetails[i] = nextProps.customerInfo
              break
            }
          }
        }
        console.log(routeDetails)
        data = this.formatTableData(routeDetails)
        this.setState({ datasource: data, routeDetails: routeDetails })
        // this.props.updateRouteDetails(routeDetails)
      }
    }
    if (nextProps.clientList) {
      this.setState({
        clientList: nextProps.clientList,
      })
    }
    if (this.props.selectedRoute != nextProps.selectedRoute) {
      const routeDetails = nextProps.routeDetails
      const data = this.formatTableData(routeDetails)
      this.setState({ datasource: data, routeDetails: nextProps.routeDetails })
    }
  }

  componentDidUpdate(prevProps: any) {
    if (!this.equal(this.props.routeDetails, prevProps.routeDetails)) {
      const data = this.formatTableData(this.props.routeDetails)
      this.setState({ datasource: data, routeDetails: this.props.routeDetails })
    }
  }

  equal(a: any, b: any) {
    if (JSON.stringify(a) === JSON.stringify(b)) {
      return true
    } else {
      return false
    }
  }
  render() {
    const { datasource, routeDetails } = this.state
    const readyItems = datasource ? datasource.filter(record => {
      const addresses = record.availables
      return (addresses && addresses.length > 0)
    }) : []

    this.selectedDetailId = 0
    let numberOfDetails = 0
    if (routeDetails) {
      numberOfDetails = routeDetails.length
    }
    return (
      <div>
        {/* <HeaderDivider /> */}
        <RouteAddressesTitle>
          <div className="number">{numberOfDetails == 0 ? '--' : numberOfDetails}</div>
          <div className="title">Addresses On Route</div>
          <Popover
            placement="left"
            content={this.getRouteDetailModal()}
            trigger="click"
            visible={this.state.newModalVisible}
            onVisibleChange={this.handleRouteDetailVisibleChange.bind(this)}
          >
            <ThemeButton shape="round" style={{ position: 'absolute', marginLeft: 260 }}>
              NEW ROUTE DETAIL
            </ThemeButton>
          </Popover>
        </RouteAddressesTitle>
        <EditTableWrapper style={{ padding: '22px 0', overflow: 'hidden' }}>
          <EditableTable
            columns={this.column}
            dataSource={readyItems}
            handleSave={this.handleSave}
            rowKey="detail_id"
          />
        </EditTableWrapper>
      </div>
    )
  }

  handleSave = (record: any) => {}

  formatTableData = (arr: WholesaleRouteDetail[]) => {
    const data: any[] = []
    console.log(arr)
    if (arr && arr.length > 0) {
      arr.forEach((el, index) => {
        if (el.wholesaleClient) {
          data.push({
            index: index + 1,
            clientId: el.wholesaleClient.clientId,
            name: el.wholesaleClient.clientCompany ? el.wholesaleClient.clientCompany.companyName : '',
            shipping_address: el.address,
            availables: el.availableAddresses,
            detail_id: el.id,
          })
        }
      })
    }
    console.log(data)
    return data
  }

  removeAddress = (detailId: number) => {
    this.props.removeRouteDetailById(detailId)
  }

  renderAvailableAddresses = (record: any) => {
    let addresses = record.availables
    let rows: any[] = []
    if (addresses && addresses.length > 0) {
      addresses.forEach((el, index) => {
        if (el) {
          let address = el.street1 + ', ' + el.city + ', ' + el.state
          rows.push(
            <Option key={index} value={el.addressId}>
              {address}
            </Option>,
          )
        }
      })
    }
    return rows
  }

  shippingAddressChange = (record: any, value: number) => {
    const detail_id = record.detail_id
    let wholesaleAddressId = 0
    record.availables.forEach((obj: any) => {
      if (obj.addressId == value) {
        wholesaleAddressId = obj.wholesaleAddressId
      }
    })
    if (detail_id && wholesaleAddressId && !isNaN(detail_id) && !isNaN(wholesaleAddressId)) {
      this.props.updateShippingAddress({ detail_id: detail_id, addressId: wholesaleAddressId })
    }
  }

  autoCompleteFilter = (value: any) => {
    const tempList = this.props.clientList.filter((client) => {
      if (client.clientCompany.companyName.indexOf(value) > -1) {
        return true
      }
    })
    this.setState({
      clientList: tempList,
    })
  }

  autoCompleteSelect = (record: any, value: any) => {
    const route_id = this.props.selectedRoute ? this.props.selectedRoute.id : 0
    const detail_id = record.detail_id
    const currentClientId = record.clientId ? record.clientId : 0
    const data = {
      customerId: value,
      routeId: route_id,
      detailId: detail_id,
    }
    if (value && !isNaN(parseInt(value)) && parseInt(value) > 0 && value != currentClientId) {
      this.selectedDetailId = detail_id
      // this.props.getCustomerInfoByCustomerId(data)
    }
  }

  getRouteDetailModal = () => {
    return (
      <RouteDetailModal
        clientList={this.props.clientList}
        selectedRoute={this.props.selectedRoute}
        addresses={this.props.addresses}
        addRouteDetailInfo={this.addRouteDetailInfo}
        getAddressesByClientId={this.getAddressesByClientId}
      />
    )
  }

  handleRouteDetailVisibleChange = (visible: boolean) => {
    this.setState({ newModalVisible: visible })
  }

  addRouteDetailInfo = (data: any) => {
    this.props.getCustomerInfoByCustomerId(data)

    this.setState({
      newModalVisible: false,
    })
  }

  getAddressesByClientId = (clientId: string) => {
    this.props.getAddresses(clientId)
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const RouteDetail = withTheme(connect(DeliveryModule)(mapStateToProps)(RouteDetailComponent))
