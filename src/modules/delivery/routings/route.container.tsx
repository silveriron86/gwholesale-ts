/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'

import { Theme } from '~/common'
import { DeliveryDispatchProps, DailyDeliveryProps, DeliveryModule } from '..'
import PageLayout from '~/components/PageLayout'
import { RouteInfo } from './route/route-info'
import { RouteDetail } from './route-detail/route-detail'
import { TempCustomer } from '~/schema'
import { RouteComponentProps } from 'react-router'

export type DeliveryRoutingProps = DeliveryDispatchProps & DailyDeliveryProps & RouteComponentProps<{ routeId: string }> &  {
  theme: Theme,
  clientList:TempCustomer[]
}

export class DeliveryRoutingDetailComponent extends React.PureComponent<DeliveryRoutingProps> {
  state: any
  constructor(props: DeliveryRoutingProps) {
    super(props)
    this.state = {
      route: null,
      cientList:[]
    }
  }
  componentDidMount() {
    const routeId = this.props.match.params.routeId
    this.props.getRouteDetail(routeId)
    this.props.getAllCustomersForUser();
    this.props.getRouteDetailsByRouteId(routeId);
  }

  equal(a: any, b: any) {
    if(JSON.stringify(a) === JSON.stringify(b)) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const { routes, clientList, selectedRoute }  = this.props;
    // console.log(routes);
    // const selectedRoute = routes.find(route => {
    //   return route.id == this.props.match.params.routeId
    // })
    // console.log(selectedRoute);

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Deliver Routing-Routes'}>
        <RouteInfo selected={selectedRoute} />
        <RouteDetail clientList={clientList}/>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const DeliveryRoutingDetail = withTheme(connect(DeliveryModule)(mapStateToProps)(DeliveryRoutingDetailComponent))
