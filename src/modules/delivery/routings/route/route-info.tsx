/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Row, Col, Layout, Radio, Checkbox, Select, Popconfirm, Form } from 'antd'
import { cloneDeep } from 'lodash'

import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { DeliveryModule, DailyDeliveryProps, DeliveryDispatchProps } from '../../'
import { Icon as IconSvg } from '~/components/icon/'
import { Flex, InfoSpan, Name } from '~/modules/customers/components/customer-detail/customer-detail.style'
import { layoutStyle, DetailWrapperStyle } from '~/modules/customers/components/customers-detail/customers-detail.style'
import {
  InputLabel,
  ThemeRadio,
  ThemeTextArea,
  ThemeInput,
  ThemeCheckbox,
  ThemeTextButton,
} from '~/modules/customers/customers.style'
import {
  RouteInfoComponentStyle,
  InputLabelStyle,
  RouteTitleStyle,
  RouteTitleWrapper,
  BlockWithBorder,
} from './../routing.style'
import { WEEKDAYS } from '~/schema'
import { CustomButton } from '~/components'
import { FormComponentProps } from 'antd/es/form'
import { BackButton } from '~/modules/account/account-new.style'
import { Icon } from 'antd'

const { Option } = Select

interface FormFields {
  routeName: string
}

export type RouteInfoProps = DailyDeliveryProps &
  DeliveryDispatchProps &
  FormComponentProps<FormFields> & {
    theme: Theme
    selected: WholesaleRoute | null
  }

export class RouteInfoComponent extends React.PureComponent<RouteInfoProps> {
  state: any
  _isMounted: boolean
  constructor(props: RouteInfoProps) {
    super(props)
    this.state = {
      editableTitle: false,
      routeTitle: '',
      areasText: '',
    }
    this._isMounted = false
  }

  componentDidMount() {
    this._isMounted = true
    document.addEventListener('mousedown', this.handleClickBody)
    if (this.props.selected) {
      this.setState({ areasText: this.props.selected.areas, routeTitle: this.props.selected.routeName })
    }
  }

  componentDidUpdate(prevProps: any) {
    if (this._isMounted && !this.equal(this.props.selected, prevProps.selected)) {
      if (this.props.selected) {
        this.setState({ areasText: this.props.selected.areas, routeTitle: this.props.selected.routeName })
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  equal(a: any, b: any) {
    if (JSON.stringify(a) === JSON.stringify(b)) {
      return true
    } else {
      return false
    }
  }

  render() {
    const { /*routes,*/ selected } = this.props
    const {
      form: { getFieldDecorator },
    } = this.props
    let activeDays: string[] = []
    let activeTime = 'AM'
    if (selected && selected.activeDays && selected.activeDays.trim() != '') {
      activeDays = selected.activeDays.split(',')
      if (selected.activeTime == 'PM') {
        activeTime = 'PM'
      }
    }
    return (
      <div style={{ padding: '20px 20px 5px 60px' }}>
        <BackButton
          onClick={this.props.goBack}
          style={{ padding: 0, minWidth: 80, marginRight: 10, textAlign: 'left' }}
        >
          <Icon type="arrow-left" />
          <span style={{ fontWeight: 700 }}>back</span>
        </BackButton>
        <Popconfirm
          title="Are you sure you want to delete this route?"
          onConfirm={this.deleteRoute}
          okText="Delete"
          cancelText="Cancel"
          okType="danger"
          placement="bottomLeft"
        >
          <ThemeTextButton className="underline" style={{ position: 'absolute', marginTop: -20, right: 36 }}>
            {/* <IconSvg type="close-circle" viewBox="0 0 16 16" width={15} height={14} /> */}
            Delete Route
          </ThemeTextButton>
        </Popconfirm>
        <div style={{ width: '100%', marginTop: 26 }}>
          <RouteTitleWrapper type="flex" justify="space-between">
            <Col xs={14}>
              <Flex>
                <InfoSpan>ROUTE NAME</InfoSpan>
              </Flex>
              {!this.state.editableTitle && (
                <Name
                  style={RouteTitleStyle}
                  onClick={this.editTitle}
                  style={{ fontSize: 24, lineHeight: '27px', textAlign: 'left' }}
                >
                  {selected ? selected.routeName : ''}
                </Name>
              )}
              {this.state.editableTitle && (
                <Form.Item>
                  {getFieldDecorator('routeName', {
                    initialValue: this.state.routeTitle,
                    rules: [{ required: true, message: 'Please fill out the route name!' }],
                  })(
                    <ThemeInput
                      style={{ ...RouteTitleStyle, width: '50%' }}
                      onChange={this.onRouteTitleChange}
                      onKeyUp={this.saveTitle}
                    />,
                  )}
                </Form.Item>
              )}
            </Col>
          </RouteTitleWrapper>
          <Layout style={layoutStyle}>
            <Row style={{ marginTop: 10 }} type="flex" justify="space-between" />
            <Row style={{ padding: '10px 0' }}>
              {/* <BlockWithBorder type="flex" justify="space-between" style={{ padding: 10 }}></BlockWithBorder> */}
              <Col md={8}>
                <BlockWithBorder>
                  <div className="block-title">Active Days</div>
                  <Checkbox.Group style={{ width: '100%' }} value={activeDays} onChange={this.onCheckboxChange}>
                    <Row>
                      <Col md={12} style={InputLabelStyle}>
                        <Row>
                          <ThemeCheckbox value="SUNDAY">Sunday</ThemeCheckbox>
                        </Row>
                        <Row>
                          <ThemeCheckbox value="MONDAY">Monday</ThemeCheckbox>
                        </Row>
                        <Row>
                          <ThemeCheckbox value="TUESDAY">Tuesday</ThemeCheckbox>
                        </Row>
                        <Row>
                          <ThemeCheckbox value="WEDNESDAY">Wednesday</ThemeCheckbox>
                        </Row>
                        <Row>
                          <ThemeCheckbox value="THURSDAY">Thursday</ThemeCheckbox>
                        </Row>
                      </Col>
                      <Col md={12} style={InputLabelStyle}>
                        <Row>
                          <ThemeCheckbox value="FRIDAY">Friday</ThemeCheckbox>
                        </Row>
                        <Row>
                          <ThemeCheckbox value="SATURDAY">Saturday</ThemeCheckbox>
                        </Row>
                      </Col>
                    </Row>
                  </Checkbox.Group>
                </BlockWithBorder>
              </Col>
              <Col md={8}>
                <BlockWithBorder className="active-time">
                  <div className="block-title">Active Time</div>
                  <Radio.Group onChange={this.onActiveTimeChange} value={activeTime} style={{ float: 'left' }}>
                    <ThemeRadio value="AM">AM</ThemeRadio>
                    <ThemeRadio value="PM">PM</ThemeRadio>
                  </Radio.Group>
                </BlockWithBorder>
              </Col>
              <Col md={8}>
                <BlockWithBorder style={{ marginRight: 0 }}>
                  <div className="block-title">Areas</div>
                  <ThemeTextArea
                    rows={4}
                    value={this.state.areasText}
                    onBlur={this.onAreaChange}
                    onChange={this.onAreaValueChange}
                  />
                </BlockWithBorder>
              </Col>
            </Row>
          </Layout>
        </div>
      </div>
    )
  }

  editTitle = () => {
    if (this._isMounted && this.props.selected) {
      this.setState({ editableTitle: true, routeTitle: this.props.selected.routeName })
    }
  }

  saveTitle = (event: any) => {
    if (this._isMounted && this.props.selected && event.keyCode == 13) {
      if (this.state.routeTitle != this.props.selected.routeName) {
        const selected = cloneDeep(this.props.selected)
        if (selected) {
          selected.routeName = this.state.routeTitle
          this.props.updateSelectedRoute(selected)
        }
      }
      this.setState({ editableTitle: false })
    }
  }

  handleClickBody = (event: any) => {
    if (this._isMounted && event.target.tagName != 'INPUT' && this.state.routeTitle.trim() != '') {
      const { form } = this.props
      form.validateFields((err, values) => {
        if (!err) {
          if (this.props.selected) {
            if (this.state.routeTitle != this.props.selected.routeName) {
              const selected = cloneDeep(this.props.selected)
              if (selected) {
                selected.routeName = this.state.routeTitle
                this.props.updateSelectedRoute(selected)
              }
            }
          }
        }
      })
      this.setState({ editableTitle: false })
    }
  }

  onRouteTitleChange = (event: any) => {
    if (this._isMounted) {
      this.setState({ routeTitle: event.target.value })
    }
  }

  onAreaChange = (event: any) => {
    if (this.props.selected) {
      if (this.state.areasText != this.props.selected.areas) {
        const selected = cloneDeep(this.props.selected)
        if (selected) {
          selected.areas = this.state.areasText
          this.props.updateSelectedRoute(selected)
        }
      }
    }
  }

  onAreaValueChange = (event: any) => {
    this.setState({ areasText: event.target.value })
  }

  // onselectedChange = (value: number) => {
  //   const data = this.props.routes
  //   const selected = data.filter(el=>el.id == value)[0]
  //   if(selected != undefined) {
  //     this.props.selectedChange(selected)
  //   }
  // }

  deleteRoute = () => {
    if (this.props.selected) {
      const id = this.props.selected.id
      this.props.deleteRoute(id)
    }
  }

  // addNewRoute = () => {
  //   const route: any = {
  //     routeName: '',
  //     activeDays: '',
  //     activeTime: 'AM',
  //     areas: '',
  //     assigned: 0
  //   }
  //   this.props.createRoute(route)
  //   this.setState({editableTitle: true})
  // }

  onActiveTimeChange = (event: any) => {
    const activeTime = event.target.value
    const selected = cloneDeep(this.props.selected)
    if (selected) {
      selected.activeTime = activeTime
      this.props.updateSelectedRoute(selected)
    }
  }

  onCheckboxChange = (value: any) => {
    const selected = cloneDeep(this.props.selected)
    if (selected) {
      selected.activeDays = value.join()
      this.props.updateSelectedRoute(selected)
    }
  }

  // renderRouteItems = () => {
  //   const { routes } = this.props
  //   const rows = []
  //   for (let i = 0; i < routes.length; i++) {
  //     const route = routes[i];
  //     rows.push(<Option key={i} value={route.id}>{route.routeName}</Option>)
  //   }
  //   return rows;
  // }

  renderWeekDaysGroup = () => {
    const days = []
    for (let i = 0; i < WEEKDAYS.length; i++) {
      const day = WEEKDAYS[i]
      days.push(day[0] + day.slice(1).toLowerCase())
    }
    return days
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery
const RouteInfoForm = Form.create()(RouteInfoComponent)
export const RouteInfo = withTheme(connect(DeliveryModule)(mapStateToProps)(RouteInfoForm))
