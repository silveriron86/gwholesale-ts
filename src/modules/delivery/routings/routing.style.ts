import styled from '@emotion/styled'
import { Row } from 'antd'
import { darkGrey, mediumGrey } from '~/common'

export const InputLabelStyle: React.CSSProperties = {
  textAlign: 'left',
}
export const RouteInfoComponentStyle: React.CSSProperties = {
  boxShadow: '0px 12px 14px -6px rgba(0, 0, 0, 0.15)',
  padding: '20px 20px 5px 60px',
}
export const RouteTitleStyle: React.CSSProperties = {
  float: 'left',
  cursor: 'pointer',
  minWidth: '50%',
  minHeight: 20,
  textAlign: 'left',
}
export const HeaderDivider = styled('div')((props) => ({
  marginTop: 20,
  height: 4,
  width: '100%',
  backgroundColor: props.theme.dark,
}))

export const AddressOnRoute = styled('div')((props) => ({
  color: props.theme.primary,
  border: `2px solid ${props.theme.primary}`,
  padding: '10px 10px',
  borderRadius: 4,
}))

export const AddressDetailTitle = styled('div')({
  fontWeight: 'bold',
  fontSize: 20,
  textAlign: 'left',
  paddingTop: 5,
})

export const EditTableWrapper = styled('div')((props) => ({
  padding: '10px 60px',
  '& .ant-table-body tr:last-child td': {
    border: 'none',
  },
  '& .ant-table-body tr:last-child td:nth-of-type(2) .editable-cell-value-wrap': {
    padding: '5px 12px',
    cursor: 'pointer',
    width: 50,
  },
  '& .ant-table-wrapper td:last-child, & .ant-table-wrapper th:last-child': {
    // visibility: 'hidden'
  },
}))

export const RouteTitleWrapper = styled(Row)((props) => ({
  '& .ant-form-explain': {
    textAlign: 'center',
    width: '50%',
  },
}))

export const BlockWithBorder = styled(Row)((props) => ({
  height: 154,
  border: '1px solid #DDDDDD',
  borderRadius: 6,
  marginRight: 34,
  padding: '12px 15px',
  '.block-title': {
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 600,
    fontSize: 14,
    lineHeight: '16px',
    textAlign: 'left',
    marginBottom: 10,
    color: mediumGrey,
  },
  '.ant-checkbox + span': {
    paddingLeft: 10,
    color: darkGrey,
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 300,
    fontSize: 13,
  },
  '&.active-time': {
    'span.ant-radio + *': {
      fontWeight: 600,
    },
  },
}))

export const RouteAddressesTitle = styled('div')((props) => ({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  marginTop: 10,
  marginLeft: 60,
  '.number': {
    backgroundColor: props.theme.main,
    width: 24,
    height: 24,
    borderRadius: 12,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    fontSize: 12,
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 500,
    paddingTop: 2,
    marginRight: 12,
  },
  '.title': {
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 600,
    fontSize: 20,
    color: props.theme.main,
    marginTop: 2,
  },
}))
