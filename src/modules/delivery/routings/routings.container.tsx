/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'

import { Theme } from '~/common'
import { DeliveryDispatchProps, DailyDeliveryProps, DeliveryModule } from '..'
import PageLayout from '~/components/PageLayout'
import {
  ThemeTable,
  HeaderContainer,
  HeaderTitle,
  HeaderOptions,
  buttonStyle,
  ThemeIconButton,
  ThemeModal,
  ThemeInput,
  InputLabel,
  ThemeOutlineButton,
} from '~/modules/customers/customers.style'
import { tableCss } from '~/modules/account/components/import-table.style'
import { Input, Button, Icon, Popconfirm } from 'antd'
import { Icon as IconSvg } from '~/components'
import { searchButton } from '~/components/elements/search-button'

export type DeliveryRoutingsProps = DeliveryDispatchProps &
  DailyDeliveryProps & {
    theme: Theme
  }

export class DeliveryRoutesComponent extends React.PureComponent<DeliveryRoutingsProps> {
  state: any
  constructor(props: DeliveryRoutingsProps) {
    super(props)
    this.state = {
      search: '',
      routeName: '',
      newRouteModalVisible: false,
    }
  }

  componentDidMount() {
    this.props.getAllRoutes()
  }

  onSearch = (value: string) => {
    this.setState({
      search: value,
    })
  }

  onSearchTextChange = (evt: any) => {
    this.setState({ search: evt.target.value })
  }

  toggleNewRouteModal = () => {
    this.setState({
      routeName: '',
      newRouteModalVisible: !this.state.newRouteModalVisible,
    })
  }

  onNewRoute = () => {
    const { routeName } = this.state
    const route: any = {
      routeName,
      activeDays: '',
      activeTime: 'AM',
      areas: '',
      assigned: 0,
    }
    this.props.createRoute(route)
    this.toggleNewRouteModal()
  }

  onDeleteRoute = (id: number) => {
    this.props.deleteRoute(id)
  }

  onChangeRouteName = (e: any) => {
    console.log(e.target.value)
    this.setState({
      routeName: e.target.value,
    })
  }

  componentWillReceiveProps(nextProps: DeliveryRoutingsProps) {
    if (nextProps.newRouteId !== this.props.newRouteId) {
      this.props.history.push(`/delivery/route/${nextProps.newRouteId}`)
    }
  }

  onRow = (record: any, index: number) => {
    return {
      onClick: (_event: any) => {
        _event.preventDefault()
        this.props.selectedRouteChange(record)
        if (_event.target.tagName !== 'BUTTON') {
          this.props.history.push(`/delivery/route/${record.id}`)
        }
      },
    }
  }

  render() {
    const { routes } = this.props
    const { newRouteModalVisible, routeName } = this.state
    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        key: 'id',
        render: (v: any, record: any, index: number) => index + 1,
      },
      {
        title: 'ROUTE NAME',
        dataIndex: 'routeName',
        key: 'routeName',
        sorter: (a: any, b: any) => a.routeName.localeCompare(b.routeName),
      },
      {
        title: 'ACTIVE DAYS',
        dataIndex: 'activeDays',
        key: 'activeDays',
        sorter: (a: any, b: any) => a.activeDays.localeCompare(b.activeDays),
      },
      {
        title: 'ACTIVE TIME',
        dataIndex: 'activeTime',
        key: 'activeTime',
        sorter: (a: any, b: any) => a.activeTime.localeCompare(b.activeTime),
      },
      {
        title: '',
        key: 'close',
        dataIndex: 'id',
        align: 'center',
        render: (id: number) => {
          return (
            <Popconfirm
              title="Are you sure you want to delete this route?"
              onConfirm={() => this.onDeleteRoute(id)}
              okText="Delete"
              cancelText="Cancel"
              okType="danger"
              placement="bottomLeft"
            >
              <ThemeIconButton type="link">
                <Icon type="close" />
              </ThemeIconButton>
            </Popconfirm>
          )
        },
      },
    ]

    const { search } = this.state
    const filteredRoutes = routes.filter(route => {
      return route.routeName.toLocaleLowerCase().indexOf(search.toLocaleLowerCase()) > -1
    })

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Selling & Shipping-Routes'}>
        <HeaderContainer style={{ height: 'auto', paddingBottom: 30 }}>
          <HeaderTitle>Delivery Routes</HeaderTitle>
          <HeaderOptions>
            <Input.Search
              placeholder="Search route name"
              size="large"
              style={{ width: '557px', border: '0' }}
              enterButton={searchButton}
              onSearch={this.onSearch}
              onChange={this.onSearchTextChange}
              value={search}
              allowClear
            />
            <div>
              <ThemeOutlineButton type="primary" shape="round" onClick={this.toggleNewRouteModal}>
                New Route
              </ThemeOutlineButton>
              {/* <Button
                size="large"
                icon="plus"
                type="primary"
                style={{
                  ...buttonStyle,
                  border: `1px solid ${this.props.theme.primary}`,
                  backgroundColor: this.props.theme.primary,
                }}
                onClick={this.toggleNewRouteModal}
              >
                New Route
              </Button> */}
            </div>
          </HeaderOptions>
        </HeaderContainer>

        <div style={{ paddingLeft: 70, paddingRight: 70 }}>
          <ThemeTable
            pagination={{ pageSize: 20 }}
            // components={components}
            columns={columns}
            dataSource={filteredRoutes}
            rowKey="id"
            css={tableCss(false)}
            onRow={this.onRow}
          />
        </div>

        <ThemeModal
          width={500}
          title={'New Route'}
          keyboard={true}
          visible={newRouteModalVisible}
          onOk={this.onNewRoute}
          onCancel={this.toggleNewRouteModal}
          okText="Submit"
          okButtonProps={{ disabled: routeName === '' }}
        >
          <InputLabel>Route Name:</InputLabel>
          <ThemeInput value={routeName} onChange={this.onChangeRouteName} />
        </ThemeModal>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.delivery

export const DeliveryRoutings = withTheme(connect(DeliveryModule)(mapStateToProps)(DeliveryRoutesComponent))
