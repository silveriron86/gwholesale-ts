/**@jsx jsx */
import React from 'react'

import { jsx } from '@emotion/core'
import { HomePageModule } from '~/modules/homepage/homepage.module'
import {
  Footer,
  Menu,
  MenuNav,
  ContactContainer,
  ContactContent,
  ContactTitle,
  MenuLink,
  ContactNumber,
  SocialContent,
  Disclaimer,
  logoStyle,
  TryButtonDesktop,
  TryButtonMobile,
  DemoButton,
  MenuNavMobile,
  Line,
  RequestDemo,
} from './footer.style'
import { Icon as IconSvg } from '~/components'
import { Link } from 'react-router-dom'
import { white } from '~/common'
import { Button, Modal } from 'antd'
import RequestDemoForm, { RequestDemoFields } from './request-demo.component'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'

import { HeaderProps } from '~/components'
import { Flex } from '~/modules/customers/customers.style'
import { homeButtonStyle } from '~/components/header/header.style'

export class FooterComponent extends React.PureComponent<HeaderProps> {
  render() {
    const { openModal, isMobile, location } = this.props
    const socialBtns = (
      <SocialContent>
        <Link to="https://www.facebook.com/WholesaleWare/" style={{ marginRight: '43px' }}>
          <IconSvg type="fb_green" viewBox={void 0} width={40} height={40} />
        </Link>
        <Link to="https://www.instagram.com/WholesaleWare/" style={{ marginRight: '43px' }}>
          <IconSvg type="ig_green" viewBox={void 0} width={40} height={40} />
        </Link>
        <Link to="https://www.twitter.com/WholesaleWare/">
          <IconSvg type="twitter_green" viewBox={void 0} width={40} height={40} />
        </Link>
      </SocialContent>
    )
    const contactLinks = (
      <ContactContent>
        <ContactNumber>
          <IconSvg type="phone_outline" viewBox={void 0} width={24} height={25} />
          <div>Call 619-537-6767</div>
        </ContactNumber>
        <ContactNumber style={{ marginTop: 15 }}>
          <IconSvg type="email_outline" viewBox={void 0} width={24} height={25} />
          <a style={{ color: '#1C6E31' }} href="mailto:sales@wholesaleware.com">
            sales@wholesaleware.com
          </a>
        </ContactNumber>
      </ContactContent>
    )
    const copyright = (
      <div>
        <IconSvg
          type="logo5"
          viewBox={void 0}
          className="logo5"
          style={{
            width: 352,
            height: 54,
            display: 'block',
            zoom: isMobile ? 0.5 : 1,
            margin: isMobile ? '0 auto' : 0,
          }}
        />
        <div className="copyright">
          © 2021 WholesaleWare. All rights reserved.&nbsp;
          {isMobile ? <br/> : null}
          <Link to={'/home/terms'} style={{ cursor: 'pointer', whiteSpace: 'normal' }}>
            <MenuLink>Terms of service</MenuLink>
          </Link>
          <a href='https://app.hubspot.com/documents/20868706/view/297977313?accessId=8098e4' style={{ cursor: 'pointer', whiteSpace: 'normal', marginLeft: 12 }}>
            <MenuLink>Privacy policy</MenuLink>
          </a>
        </div>
      </div>
    )

    const isTerms = typeof location === 'undefined'
    console.log('footer location = ', location);

    return (
      <Footer style={isTerms ? {paddingTop: 0} : {}}>
        {(typeof location !== 'undefined' && (location.pathname === '/' || location.pathname === '/home/landing' || location.hash === '#/' || location.hash === '#/home/landing')) &&
          <RequestDemo>
            <div className="demo-wrapper">
              <p>
                For a more efficient, profitable, and strategic food wholesale operation, try a complimentary demo of
                WholesaleWare today.
              </p>
              <Flex className="h-center">
                <Button
                  // onClick={openModal}
                  css={homeButtonStyle}
                  className="landing"
                  style={isMobile ? {width: 'auto'} : {}}
                >
                  <a href="https://share.hsforms.com/1zGjYvlFmT5e475T3I4GH5Acfaea" style={isMobile ? {fontSize: 12} : {}}>{isMobile ? 'Request a Demo' : 'Request a Complimentary Demo'}<IconSvg viewBox="0 0 27 16" width="27" height="16" type="arrow-right-home" style={{ marginLeft: 20 }} /></a>
                </Button>
              </Flex>
            </div>
          </RequestDemo>
        }

        {isMobile ? (
          <ContactContainer>
            {/* {socialBtns} */}
            <div style={{ margin: '45px 0' }}>{contactLinks}</div>
            {copyright}
          </ContactContainer>
        ) : (
          <ContactContainer style={isTerms ? {borderTop: '1px solid #D8DBDB', paddingTop: 47.5} : {}}>
            {copyright}
            {contactLinks}
            {/* {socialBtns} */}
          </ContactContainer>
        )}
        {this.renderRequestDemoModal()}
      </Footer>
    )
  }

  private renderRequestDemoModal() {
    const onRequestDemo = (payload: RequestDemoFields) => {
      this.props.sendRequestDemo(payload)
    }
    const onCloseRequest = () => {
      this.props.closeModal()
    }

    const closeIcon = <IconSvg type="close-green" viewBox={void 0} width={24} height={24} />
    return (
      <Modal
        className="request-demo-modal"
        width={600}
        closeIcon={closeIcon}
        footer={null}
        visible={this.props.showModal}
        onCancel={onCloseRequest}
      >
        <RequestDemoForm onSubmit={onRequestDemo} onCancel={onCloseRequest} />
      </Modal>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.homepage
export const FooterContainer = withTheme(connect(HomePageModule)(mapStateToProps)(FooterComponent))
