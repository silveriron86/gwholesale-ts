import styled from '@emotion/styled'
import { forestGreen, white, brightGreen, darkGreen } from '~/common'
import { Button } from 'antd'

export const Footer = styled('div')({
  width: '100%',
  paddingTop: '47.5px',
  paddingBottom: '55.5px',
  paddingLeft: '50px',
  paddingRight: '50px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  backgroundColor: 'white',
  position: 'relative',
  '@media (max-width: 480px)': {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 41
  },
})

export const Menu = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginBottom: '20px',
  '@media (max-width: 480px)': {
    flexDirection: 'column',
  },
})

export const MenuNav = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  flexGrow: 2,
  justifyContent: 'space-between',
  '@media (max-width: 480px)': {
    display: 'none',
  },
})

export const MenuNavMobile = styled('div')({
  marginTop: '20px',
  marginBottom: '20px',
  display: 'flex',
  flexDirection: 'column',
  '@media (min-width: 481px)': {
    display: 'none',
  },
})

export const Line = styled('div')({
  height: '1px',
  backgroundColor: '#1C6E31',
  marginTop: '20px',
  marginBottom: '20px',
})

export const MenuLink = styled('span')({
  fontFamily: 'Museo Sans Rounded',
  fontSize: '14px',
  lineHeight: '15px',
  color: white,
  '@media (max-width: 480px)': {
    fontSize: '18px',
    lineHeight: '19px',
    fontWeight: 700,
  },
})

export const buttonStyle: React.CSSProperties = {
  fontFamily: '"Museo Sans Rounded"',
  borderRadius: '200px',
  // width: '180px',
  paddingLeft: 22,
  paddingRight: 22,
  height: '45px',
  fontSize: 20,
  backgroundColor: forestGreen,
  width: 'fit-content'
}

export const TryButtonMobile: any = styled(Button)({
  ...buttonStyle,
  border: '1px solid',
  marginTop: '20px',
  '@media (min-width: 769px)': {
    display: 'none',
  },
})

export const TryButtonDesktop: any = styled(Button)({
  ...buttonStyle,
  border: '1px solid',
  '@media (max-width: 768px)': {
    display: 'none',
  },
})

export const DemoButton: any = styled(Button)({
  ...buttonStyle,
  color: darkGreen,
  backgroundColor: white,
  fontWeight: 'bold',
  '@media (max-width: 480px)': {
    borderRadius: 5,
    fontSize: 12,
    lineHeight: '15.6px',
  }
})

export const ContactContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',

  '.copyright': {
    marginTop: 15.7,
    fontFamily: 'Museo Sans Rounded',
    fontSize: 14,
    color: '#4A5355',
    a: {
      span: {
        color: '#1C6E31',
        fontWeight: 700
      }
    }
  },
  '@media (max-width: 480px)': {
    flexDirection: 'column',
    paddingTop: '30px !important',
    paddingLeft: 10,
    paddingRight: 10,
    '.logo5': {
      zoom: '0.6 !important',
      margin: '0 auto !important',
    },
    '.copyright': {
      whiteSpace: 'nowrap',
      fontSize: 14,
      lineHeight: '18.2px',
      textAlign: 'center',
      a: {
        whiteSpace: 'normal',
        display: 'block',
        marginBottom: 18,
        span: {
          fontSize: 14,
          lineHeight: '18.2px'
        }
      }
    }
  },
})

export const ContactContent = styled('div')({
  display: 'flex',
  flexDirection: 'column',
})

export const ContactTitle = styled('span')({
  fontSize: '20px',
  fontWeight: 700,
  lineHeight: '143.04%',
  color: '#1C6E31',
  marginBottom: '17px',
})

export const ContactNumber = styled('span')({
  color: '#1C6E31',
  fontSize: '18px',
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 600,
  lineHeight: '23.4px',
  display: 'flex',
  alignItems: 'center',
  svg: {
    marginRight: 22,
    color: 'white'
  }
})

export const SocialContent = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  marginTop: '17px',
  '@media (max-width: 480px)': {
    marginTop: 0
  },
})

export const Disclaimer = styled('div')({
  color: '#1C6E31',
  marginBottom: '46px',
  fontSize: '16px',
  lineHeight: '107.28%',
  '@media (max-width: 480px)': {
    fontSize: '12px',
  },
})

export const logoStyle: React.CSSProperties = {
  height: '50px',
  minWidth: '144px',
  flexGrow: 3,
}

export const RequestDemo = styled('div')({
  // position: 'absolute',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: -190,
  width: '100%',
  paddingBottom: 56,
  borderBottom: '1px solid #D8DBDB',
  marginBottom: 47.5,
  '&.no-border': {
    marginBottom: 0,
    borderBottom: 0,
  },
  '.demo-wrapper': {
    width: 800,
    backgroundColor: '#1C6E31',
    padding: 30,
    borderRadius: 10,
    p: {
      color: 'white',
      marginBottom: 20,
      fontFamily: 'Patua One',
      fontWeight: 400,
      fontSize: 25,
      lineHeight: '33.8px',
      textAlign: 'center'
    },
    '@media (max-width: 800px)': {
      width: '100%'
    },
  },
  '@media (max-width: 480px)': {
    paddingBottom: 42,
    marginBottom: 25,
    marginTop: -80,
    '.demo-wrapper': {
      padding: 25,
      p: {
        fontSize: 14,
        lineHeight: '18.2px',
      }
    }
  }
})
