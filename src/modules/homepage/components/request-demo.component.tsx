import React from 'react'
import { Row, Col, Form, Input, Select, Icon } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import { Icon as IconSvg } from '~/components/icon/'
import { ModalTitle, DialogBody, DialogFooter, ModalSubtitle, SubmitButton, CancelButton, DemoForm } from './request-demo.style'
import { black } from '~/common'
import ReactGA from 'react-ga'

export interface RequestDemoFields {
  emailAddress: string
  fullName: string
  phone: string
  companyName: string
  companySize: string
}

interface RequestFormCallbacks {
  onSubmit: (payload: RequestDemoFields) => void
  onCancel: () => void
}

class RequestDemoForm extends React.PureComponent<
  FormComponentProps<RequestDemoFields> & RequestFormCallbacks> {

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const { form, onSubmit } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        onSubmit(values)
      }
    })
  }

  componentDidMount() {
    ReactGA.initialize('UA-143951733-1')
    ReactGA.modalview('request-demo')
  }

  render() {
    const {
      onCancel,
      form: { getFieldDecorator },
    } = this.props
    return (
      <DemoForm onSubmit={this.handleSubmit}>
        <IconSvg
          type="logo-green-sm"
          viewBox={void 0}
          width={158}
          height={24}
        />
        <ModalTitle><a href="http://go.wholesaleware.com">Let’s schedule a complimentary live demo of WholesaleWare</a></ModalTitle>
        <ModalSubtitle>Our team will reach out to schedule a free consultation and show you how to improve your operations with one simple tool, bringing greater profitability, transparency, and efficiency to your business.</ModalSubtitle>
        <DialogBody className="request-demo">
          <Row>
            <Col>
              <Form.Item>
                {getFieldDecorator('fullName', {
                  rules: [{ required: true, message: 'Full Name is required!' }],
                })(<Input placeholder="Your Name" />)}
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col>
              <Form.Item>
                {getFieldDecorator('companyName', {
                  rules: [{ required: false, message: 'Business Name is required!' }],
                })(<Input placeholder="Business Name" />)}
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col>
              <Form.Item>
                {getFieldDecorator('emailAddress', {
                  rules: [
                    { required: true, message: 'Email address is required!' },
                    { type: 'email', message: 'Invalid email address' },
                  ],
                })(<Input placeholder="Your Email" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Item>
                {getFieldDecorator('phone', {
                  rules: [{ required: true, message: 'Phone Number is required!' }],
                })(<Input placeholder="Your Phone Number" />)}
              </Form.Item>
            </Col>
          </Row>
          {/* <Row>
            <Col>
              <Form.Item>
                {getFieldDecorator('companySize', {})(
                  <Select
                    placeholder="Company Size"
                    suffixIcon={<Icon style={{ color: black }} type="caret-down" />}
                  >
                    {['1-10', '11-50', '51-100', '101-250', '250+'].map((value) => (
                      <Select.Option key={value} value={value.toLowerCase()}>
                        {value}
                      </Select.Option>
                    ))}
                  </Select>,
                )}
              </Form.Item>
            </Col>
          </Row> */}
        </DialogBody>
        <DialogFooter>
          <SubmitButton type="primary" htmlType="submit">
            Submit
          </SubmitButton>
          {/* <CancelButton onClick={onCancel}> CANCEL </CancelButton> */}
        </DialogFooter>
      </DemoForm>
    )
  }
}

export default Form.create<FormComponentProps<RequestDemoFields> & RequestFormCallbacks>()(RequestDemoForm)
