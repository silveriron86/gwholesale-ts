import styled from '@emotion/styled'
import { brownGrey } from '~/common'
import { Form, Button } from 'antd'

export const ModalTitle = styled('div')({
  marginTop: 64,
  fontFamily: 'Patua One',
  fontWeight: 400,
  fontSize: 32,
  lineHeight: '35.2px',
  color: '#1C6E31',

  '@media (max-width: 480px)': {
    marginLeft: 0,
    fontSize: '24px',
    lineHeight: '30px',
    padding: 0,
    marginTop: 40
  },
})

export const ModalSubtitle = styled('div')({
  marginTop: 20,
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 300,
  fontSize: 16,
  lineHeight: '21.6px',
  color: '#4A5355',

  '@media (max-width: 480px)': {
    marginLeft: 0,
    fontSize: '14px',
    padding: 0
  },
})

export const DialogBody = styled('div')({
  marginBottom: '20px',
  '@media (max-width: 480px)': {
    // paddingLeft: '24px',
    // paddingRight: '24px',
  },

  '& input': {
    // marginTop: '30px',
    '@media (max-width: 480px)': {
      marginTop: 0,
    },
  },

  '& .ant-select': {
    // marginTop: '30px',
    '@media (max-width: 480px)': {
      marginTop: 0,
    },
  },

  '& .ant-input-affix-wrapper .ant-input-suffix': {
    marginTop: '15px',
  },

  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-select-selection': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    fontWeight: 300,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '&.request-demo': {
    marginTop: 20,
    marginBottom: 0,
    '.ant-input, .ant-select .ant-select-selection--single': {
      width: '100%',
      backgroundColor: '#F7F7F7',
      height: 49,
      borderRadius: 3,
      borderBottom: 0,
      '.ant-select-selection__rendered': {
        lineHeight: '49px',
      },
      '&::placeholder, .ant-select-selection__placeholder': {
        fontFamily: 'Museo Sans Rounded',
        fontWeight: 700,
        fontSize: 14,
        lineHeight: '18.9px',
        color: '#82898A'
      }
    },
  }
})

export const DialogFooter = styled('div')({
  width: '100%',
  boxSizing: 'border-box',
  // padding: '20px 20px',
  // marginTop: '10px',
  display: 'flex',
  // justifyContent: 'center',
  alignItems: 'center',
})

export const buttonStyle: React.CSSProperties = {
  borderRadius: 3,
  padding: '15px 20px',
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 700,
  fontSize: 16,
  lineHeight: '20.8px',
  height: 'fit-content',
  width: 'fit-content'
}

export const SubmitButton: any = styled(Button)({
  ...buttonStyle,
})

export const CancelButton = styled('div')({
  height: '40px',
  width: '100px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  color: brownGrey,
  fontSize: '14px',
  lineHeight: '15px',
  textDecoration: 'underline',
  marginLeft: '25px',
  cursor: 'pointer',
})

export const DemoForm = styled(Form)({
  width: '100%',
  padding: '1px 31px 46px',
  '@media (max-width: 480px)': {
    padding: 0
  },
  '.has-error': {
    '.ant-form-explain': {
      fontWeight: 'normal !important'
    }
  }
})
