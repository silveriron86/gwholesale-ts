/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Slides } from '../homepage.style'
import { Icon as IconSvg } from '~/components'
import { Flex } from '../../customers/customers.style'
import { Fade } from 'react-slideshow-image'
import { Button, Collapse } from 'antd'
import { cloneDeep } from 'lodash'
import { homeButtonStyle } from '~/components/header/header.style'
import 'react-slideshow-image/dist/styles.css'
const { Panel } = Collapse

export type SlidesContainerProps = {
  isMobile: boolean
  openModal: Function
}

const slideTabs = [
  {
    icon: <IconSvg viewBox="0 0 34 34" type="home-icon-1" width="34" height="34" />,
    label: 'Inventory Management',
  },
  {
    icon: <IconSvg viewBox="0 0 43 32" type="home-icon-2" width="43" height="32" />,
    label: 'Pricing Analytics',
  },
  {
    icon: <IconSvg viewBox="0 0 25 62" type="home-icon-3" width="25" height="62" />,
    label: 'Repacking & Manufacturing',
  },
  {
    icon: <IconSvg viewBox="0 0 32 40" type="home-icon-4" width="32" height="40" />,
    label: 'Food Safety & Traceability',
  },
  {
    icon: <IconSvg viewBox="0 0 35 44" type="home-icon-5" width="35" height="44" />,
    label: 'Routing & Logistics',
  },
]

const slideImages = [
  require('../images/feature1.png'),
  require('../images/feature2.png'),
  require('../images/feature3.png'),
  require('../images/feature4.png'),
  require('../images/feature5.png'),
]

const slideTexts = [
  [
    'Precisely manage inventory with real-time updates, stock information, and logistics data. ',
    'Our inventory management module ensures that you are always up-to-date, providing clear visibility into your stock as items flow in and out.',
  ],
  [
    'Use data to choose strategic prices for your products.',
    ' By leveraging data and guidelines for geographic regions, customer groups, and individual customers, our tools set prices automatically based on margin, markup, pricing tiers, market trends, and more, to save time while increasing profitability.',
  ],
  [
    'Understand the status, timelines, pertinent details, and personnel involved in your work orders.',
    'With full transparency, WholesaleWare empowers you to conduct your repacking and manufacturing efforts with uncompromising efficiency and accuracy.',
  ],
  [
    'Stay in compliance with audits while accelerating your response time to potential recalls.',
    'Our sales and purchase order records can be viewed, sorted, and analyzed by date, customer, vendor, and lot to ensure that you know the history, location, timeline, and prices involved in past transactions.',
  ],
  [
    'Automate fulfillment and daily deliveries with ease.',
    'Our logistics and routing features ensure more accurate orders and include time-saving automations for daily delivery routing.',
  ],
]

export default class SlidesContainer extends React.PureComponent<SlidesContainerProps> {
  slideRef = React.createRef<any>()
  state: any = {
    current: 0,
    activeKeys: [],
  }

  onChange = (previous: number, next: number) => {
    this.setState({
      current: next,
    })
  }

  onChangeCollapse = (keys: string | string[]) => {
    console.log('keys = ', keys)
    this.setState({
      activeKey: keys,
    })
  }

  onCollapse = (key: string) => {
    const keys = cloneDeep(this.state.activeKey)
    keys.splice(keys.indexOf(key), 1)
    this.setState({
      activeKey: keys,
    })
  }

  goSlide = (index: number) => {
    this.setState({
      current: index
    }, () => {
      if(this.slideRef) {
        this.slideRef.current.goTo(index);
      }
    })
  }

  render() {
    const { current, activeKey } = this.state
    const { isMobile, openModal } = this.props
    if (isMobile) {
      return (
        <Slides>
          <h1>Offering end-to-end support for more profitable, efficient, and error-free operations</h1>
          <div className="c-border"></div>
          {slideTabs.map((tab, index) => {
            const texts = slideTexts[index]
            const key = (index + 1).toString()
            return (
              <div key={`collpase-${index}`} style={{paddingBottom: 25}}>
                <Collapse
                  expandIcon={(props) => {
                    return props.isActive ? null : (
                      <IconSvg viewBox="0 0 26 25" type="arrow-expand" width="26" height="25" />
                    )
                  }}
                  activeKey={activeKey}
                  onChange={this.onChangeCollapse}
                >
                  <Panel
                    header={
                      <div>
                        <Flex className="tab-col">
                          {tab.icon}
                          <div className="label">
                            {tab.label}
                            <div className="bb"></div>
                          </div>
                        </Flex>
                        {/* <div className="header-ellips">{texts[0]}</div> */}
                      </div>
                    }
                    // showArrow={false}
                    key={key}
                  >
                    <p>{texts[0]}</p>
                    <p>{texts[1]}</p>
                    <a href="javascript:;" onClick={() => this.onCollapse(key)}>
                      <IconSvg viewBox="0 0 26 25" type="arrow-collapse" width="26" height="25" />
                    </a>
                  </Panel>
                </Collapse>
                <img className="slide-img" src={slideImages[index]} alt="" />
              </div>
            )
          })}
        </Slides>
      )
    }

    return (
      <Slides>
        <h1>
          Offering end-to-end support for more
          <br />
          profitable, efficient, and error-free operations
        </h1>
        <div className="c-border"></div>
        <Flex className="v-center space-between" style={{ maxWidth: 880, margin: '0 auto' }}>
          {slideTabs.map((tab, index) => (
            <a key={`tab-${index}`} onClick={() => this.goSlide(index)}>
              <Flex className={`tab-col ${current === index ? 'active' : ''}`}>
                {tab.icon}
                <div className="label">
                  {tab.label}
                </div>
                <div className="bb"></div>
              </Flex>
            </a>
          ))}
        </Flex>
        <div>
          <Fade
            ref={this.slideRef}
            onChange={this.onChange}
            autoplay={false}
            prevArrow={
              <div className={`slide-arrow left ${current === 0 ? 'none' : ''}`}>
                <IconSvg viewBox="0 0 19 32" type="slide-left" width="19" height="32" />
              </div>
            }
            nextArrow={
              <div className={`slide-arrow right ${current === 4 ? 'none' : ''}`}>
                <IconSvg viewBox="0 0 19 32" type="slide-right" width="19" height="32" />
              </div>
            }
          >
            {slideTexts.map((texts: array, index: number) => {
              return (
                <div className="each-fade" key={`slide-${index}`}>
                  <Flex className="flex">
                    <div style={{ marginRight: 35 }}>
                      <p>{texts[0]}</p>
                      <p>{texts[1]}</p>
                      <Button css={homeButtonStyle} className="landing" onClick={openModal}>
                        <a>Learn more</a>
                      </Button>
                    </div>
                    <img src={slideImages[index]} alt="" />
                  </Flex>
                </div>
              )
            })}
          </Fade>
        </div>
      </Slides>
    )
  }
}
