import React from 'react'
import { connect } from 'redux-epics-decorator'
import { Collapse, Icon, Row, Col, Button } from 'antd'
import { GlobalState } from '~/store/reducer'
import { AccountModule } from '../account/account.module'
import { Header } from '~/components'

import { withTheme } from 'emotion-theming'
import { Icon as IconSvg } from '~/components'
import { FooterContainer } from './components/footer.component'
import ReactGA from 'react-ga'
import {
  HomePageWrapper,
  SplashSection,
  ImproveSection,
  SeamlessSection,
  BuiltSection,
  LimitlessSection,
  IntegratesSection,
  CloudSection,
} from './homepage.style'

import { Footer, RequestDemo } from './components/footer.style'
import { homeButtonStyle } from '~/components/header/header.style'
import { Flex } from '~/modules/customers/customers.style'

const { Panel } = Collapse
const productImg1 = require('./images/product-pricing-1.png')
const productBg1 = require('./images/product_bg_1.png')
const productBg2 = require('./images/GettyImages-1056341716.jpg')
const productBg3 = require('./images/phone.png')
const phoneGif = require('./images/mobile-screens.gif')
const productBg4 = require('./images/feature-screens.gif')
const productBg5 = require('./images/aws.png')

const quickbox = require('./images/quickbox.png')
const oracle = require('./images/oracle.png')
const zebra = require('./images/zebra.png')

const icon1 = require('./images/download-cloud.png')
const icon2 = require('./images/award.png')
const icon3 = require('./images/lock.png')
export class HomeProduct extends React.PureComponent {
  state = {
    mobileVersion: false,
  }

  endOfPage = React.createRef<HTMLDivElement>()

  componentDidMount() {
    this.initGA()

    window.addEventListener('resize', this.resize.bind(this))
    this.resize()
  }

  resize() {
    this.setState({ mobileVersion: window.innerWidth <= 760 })
  }

  initGA() {
    ReactGA.initialize('UA-143951733-1')
    ReactGA.pageview('/product')

    // track event where user scrolls to the end of the page by checking
    // if About Section becomes visible
    if ('IntersectionObserver' in window) {
      const callback = (change: any[]) => {
        if (change[0].isIntersecting) {
          ReactGA.event({
            category: 'Product',
            action: 'User scrolled to the end of the product page',
          })
        }
      }

      const observer = new IntersectionObserver(callback)
      if (this.endOfPage.current) {
        observer.observe(this.endOfPage.current)
      }
    }
  }

  renderSplash() {
    const { mobileVersion } = this.state
    const arrowRightIcon = (
      <IconSvg viewBox="0 0 27 16" width="27" height="16" type="arrow-right-home" style={{ marginLeft: 20 }} />
    )
    const icons = [
      <IconSvg viewBox="0 0 35 27" width="35" height="27" type="product-icon-1" />,
      <IconSvg viewBox="0 0 38 27" width="38" height="27" type="product-icon-2" />,
      <IconSvg viewBox="0 0 36 28" width="36" height="28" type="product-icon-3" />,
      <IconSvg viewBox="0 0 25 26" width="25" height="26" type="product-icon-4" />,
      <IconSvg viewBox="0 0 33 27" width="33" height="27" type="product-icon-5" />,
      <IconSvg viewBox="0 0 26 25" width="26" height="25" type="product-icon-6" />,
      <IconSvg viewBox="0 0 36 25" width="36" height="25" type="product-icon-7" />,
    ]
    const links = [
      ['Purchasing', 'Label printing', 'Reporting', 'Receiving', 'Consignment'],
      ['Warehouse Management', 'Reporting', 'Barcode scanning', 'Inventory', 'Lots'],
      ['Manufacturing/ Repacking', 'Reporting', 'Work Orders', 'Custom Labels'],
      ['Selling', 'Mobile', 'CRM', 'Reporting', 'e-Commerce', 'Pricing'],
      ['Logistics', 'Delivery Routing', 'Mobile'],
      ['HR Management', 'Time Sheets', 'Bonuses', 'Reporting'],
      ['Accounting', 'Quickbooks', 'NetSuite'],
    ]
    return (
      <SplashSection className="product-pricing">
        <Header
          isMobile={mobileVersion}
          hide={false}
          // tabConfig={headerConfig as any}
          // logo={props.logo}
          location={location}
          currentUser={null}
        />
        <div className="title">Product</div>
        <h1>Empowering food wholesalers to embrace digital transformation and boost profitability</h1>
        <div className="hr" />
        <h3>
          Centered on productivity and profitability, WholesaleWare is an innovative digital platform built for
          forward-leaning food wholesalers.{' '}
        </h3>
        <div className="flex">
          {icons.map((ic, index) => {
            return (
              <div className="prod-col">
                <div className="img-wrapper">{ic}</div>
                {links[index].map((lk, i) => {
                  return <div className={`link ${i === 0 ? 'first' : ''}`}>{lk}</div>
                })}
                <div className="clearfix"></div>
              </div>
            )
          })}
        </div>
      </SplashSection>
    )
  }

  renderCollapse = (data, prefix) => {
    return (
      <Collapse
        bordered={false}
        defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <Icon type={isActive ? 'minus' : 'plus'} />}
      >
        {data.map((row, index) => (
          <Panel header={row.title} key={`${prefix}-${index}`}>
            <p>{row.text}</p>
          </Panel>
        ))}
      </Collapse>
    )
  }

  renderImproveSection = () => {
    const data = [
      {
        title: 'Dynamic Pricing Strategies',
        text:
          "WholesaleWare's advanced pricing tool helps food wholesalers select the ideal prices for each product, making data-driven recommendations based on individual customers, customer groups, geographic region, products, lots, and pricing tiers.",
      },
      {
        title: 'Purchasing Cost History',
        text:
          'As you choose vendors and negotiate prices, our purchasing cost history data is there to help you make the right decisions, every time. Each of your past transaction dates, amounts, and vendors is recorded in our easy-to-navigate database.',
      },
      {
        title: 'Inventory Stock Analysis',
        text:
          'With seasonal shifts, changing consumer trends, and fluctuating demand, staying on top of your inventory is key to preventing costly shortages and spoilage. Our inventory stock analysis tool allows you to set par levels, calculate your run rate, and see which products need restocking.',
      },
      {
        title: 'eCommerce & Online Ordering',
        text:
          'Today, food wholesalers that have a smooth eCommerce interface drive higher sales and retain more customers than their competitors. Our eCommerce features enable you to offer an easy-to-navigate, custom-tailored online ordering portal and mobile app to your customers. ',
      },
    ]
    return (
      <ImproveSection className="relative">
        <div className="section-wrapper">
          <img src={productBg1} className="section-bg" />
          <Row>
            <Col lg={12} md={24}>
              <h1>Improve your profitability</h1>
              <div className="description">
                With real-time stock updates, strategic pricing recommendations, easy mobile and online ordering, and
                more, WholesaleWare reduces costly errors, increases customer satisfaction, and helps keep items in
                stock. Each feature is designed to increase profitability while eliminating inefficiencies.
              </div>
              {this.renderCollapse(data, 'improve')}
            </Col>
            <Col lg={12} md={24}>
              <img src={productImg1} />
            </Col>
          </Row>
        </div>
      </ImproveSection>
    )
  }

  renderSeamlessSection = () => {
    const data = [
      {
        title: 'Barcode Scanning',
        text:
          'WholesaleWare connects seamlessly with barcode scanning technology to ensure accurate inventory and stock numbers. You can scan SKUs, weights, and lots to streamline order picking and end-of-month inventory processes. ',
      },
      {
        title: 'Label Printing',
        text:
          'Create custom labels that capture barcodes, ‘best by’ dates, weights, lots, and SKUs. Each label can be used with leading label printers like Zebra, AlphaCard, and Brother. As a result, you can keep your inventory organized with ease. ',
      },
      {
        title: 'Warehouse Location Tagging',
        text:
          'A disorganized warehouse can add costly personnel hours to your wholesale operations. Our location tagging technology tracks each and every pallet in your inventory, helping your team find products in a fraction of the time.',
      },
    ]
    return (
      <SeamlessSection className="relative">
        <div className="section-wrapper">
          <Row>
            <Col lg={12} md={24}>
              <img src={productBg2} className="section-bg" />
            </Col>
              <Col lg={12} md={24} style={{ paddingLeft: 55 }}>
              <h1>Seamless lot and order tracking </h1>
              <div className="description">
                For precise order management and lot-level tracking, WholesaleWare features advanced labeling and
                barcode scanning technology that increase end-to-end visibility for your products.
              </div>
              {this.renderCollapse(data, 'seamless')}
            </Col>
          </Row>
        </div>
      </SeamlessSection>
    )
  }

  renderBuiltSection = () => {
    const data = [
      {
        title: 'Dynamic Search',
        text:
          'Find exactly what you’re looking for with our powerful, built-in search engine. Simply enter your search criteria, such as a specific name, product SKU, address, or category. In an instant, you’ll have a list of relevant results.',
      },
      {
        title: 'Custom Workflow Configurations',
        text:
          'Your workflow depends on the unique culture, niche, complexity, and size of your operations. There are simply no ‘one-size-fits-all’ solutions for food wholesale. That’s why our knowledgeable support team configures WholesaleWare to align with your unique needs.',
      },
      {
        title: 'Keyboard Entry',
        text:
          'For fast-paced operations, WholesaleWare expedites every aspect of the order intake process. With keyboard entry support, WholesaleWare increases speed without compromising accuracy, no mouse required.',
      },
      {
        title: 'Mobile App',
        text:
          'Our WholesaleWare mobile app keeps you connected whether you’re traveling or roaming the warehouse. Your customers can even place their orders directly from the mobile app - say goodbye to juggling all the phone calls, faxes, and texts.',
      },
      {
        title: '99.99% Uptime',
        text:
          'Enjoy the AWS 99.99% uptime promise, with uninterrupted data access and consistent high performance regardless of location.',
      },
    ]
    return (
      <BuiltSection className="relative">
        <div className="section-wrapper">
          <Row>
            <Col lg={14} md={24}>
              <h1>Built for speed, flexibility, and ease-of-use</h1>
              <div className="description">
                We built our user interface with your needs in mind, developing easily navigable features that align
                effortlessly with your workflow. New and long-standing employees alike can learn the ins-and-outs of
                WholesaleWare with ease.
              </div>
              {this.renderCollapse(data, 'built')}
            </Col>
            <Col lg={10} md={24}>
              <img src={productBg3} className="section-bg" />
              <img src={phoneGif} className="section-bg gif" />
            </Col>
          </Row>
        </div>
      </BuiltSection>
    )
  }

  renderGreention = () => {
    const data = [
      {
        title: 'Flexible Units of Measure',
        text:
          'WholesaleWare features limitless units of measure per product, such as pound, pallet, or case. You can even tailor pricing based on unit size, for instance offering discounts for “pallets” or premiums for “eaches”.',
      },
      {
        title: 'Delivery Routing',
        text:
          'Save countless hours with our automated daily delivery routing system. WholesaleWare considers every aspect of your logistics pipeline, complete with flexible re-routing capabilities and order change alerts to ensure accuracy on every single order.',
      },
      {
        title: 'Manufacturing & Repacking',
        text:
          'Our software offers holistic oversight and management assistance as you process, package, grade, slice, and chop products. Stay in-the-know on your inventory, costs, yield, and more.',
      },
      {
        title: 'Time Sheets',
        text:
          'Handle HR with greater transparency through digital punch in & out functionality, paid overtime tracking, holiday and PTO logging, and more.',
      },
      {
        title: 'Reporting',
        text:
          'View your overall sales, profitability, product metrics, sales team performance, and more with WholesaleWare’s dynamic reporting tool.',
      },
      {
        title: 'Additional Features',
        text:
          'With additional features like our PAS & consignment report, catchweight support, and transaction logs, you can handle 360° of your wholesale operations with confidence.',
      },
    ]
    return (
      <LimitlessSection className="relative">
        <div className="section-wrapper">
          <Row>
            <Col lg={12} md={24}>
              <img src={productBg4} className="section-bg" />
            </Col>
            <Col lg={12} md={24} style={{ paddingLeft: 55 }}>
              <h1>
                One software suite.
                <br />
                Limitless capabilities.{' '}
              </h1>
              <div className="description">
                As a food wholesaler, having a consolidated platform to manage your operations can streamline
                productivity, eliminate inefficiencies, and bolster profitability. Each feature on WholesaleWare is
                designed with results in mind.
              </div>
              {this.renderCollapse(data, 'limitless')}
            </Col>
          </Row>
        </div>
      </LimitlessSection>
    )
  }

  renderIntegratesSection = () => {
    const data = [
      {
        title: (
          <>
            <div>QuickBooks</div>
            <img src={quickbox} className="hide mobile-block quickbox" />
          </>
        ),
        text:
          'Developed by Intuit, QuickBooks is a cloud-based accounting software that helps small businesses manage everyday expenses, stay organized for tax season, and understand spending. Our software connects seamlessly with QuickBooks, passing data between the systems instantaneously and natively. No need for a lengthy process of downloading and uploading spreadsheets.',
      },
      {
        title: (
          <>
            <div>NetSuite</div>
            <img src={oracle} className="hide mobile-block" />
          </>
        ),
        text:
          'As an all-in-one accounting solution for large organizations and enterprises, NetSuite provides in-depth accounting oversight to track spending and finances. With the click of a button, WholesaleWare offers a powerful, native integration with NetSuite, automatically keeping the two systems in sync.',
      },
      {
        title: (
          <>
            <div>Zebra</div>
            <img src={zebra} className="hide mobile-block" />
          </>
        ),
        text:
          'As a top producer of thermal bar code label printers, Zebra offers unparalleled reliability that helps food wholesalers keep their products organized. Any labels created within WholesaleWare can be printed from Zebra devices, as well as most other thermal printer brands.',
      },
    ]
    return (
      <IntegratesSection className="relative">
        <div className="section-wrapper">
          <Row>
            <Col lg={14} md={24}>
              <h1>Integrates with leading platforms and APIs</h1>
              <div className="description">
                WholesaleWare complements the tools, platforms, and APIs that you may already be using to manage your
                wholesale operations, accelerating your capabilities even further.
              </div>
              {this.renderCollapse(data, 'integrates')}
            </Col>
            <Col lg={10} md={24} style={{ paddingLeft: 75, paddingTop: 123 }} className="mobile-hide">
              <img src={quickbox} className="block" />
              <img src={oracle} className="block" style={{ margin: '56px 0' }} />
              <img src={zebra} className="block" />
            </Col>
          </Row>
        </div>
      </IntegratesSection>
    )
  }

  renderCloudSection = () => {
    return (
      <CloudSection className="relative">
        <div className="section-wrapper">
          <img src={productBg5} className="section-bg mobile-hide" />
          <Row>
            <Col lg={16} md={24}>
              <h1>Cloud-backed security and data protection</h1>
              <div className="description">
                At WholesaleWare, keeping your information secure is our top priority. That’s why we use strict security
                protocols to protect your business, customers, and employees.
              </div>
            </Col>
          </Row>
          <Flex className="mobile-block">
            <div className="cloud-col">
              <Flex className="v-center">
                <img src={icon1} />
                <h3>Automatic Backups</h3>
              </Flex>
              <div className="sub-desc">
                Powered by AWS, we automatically backup your data to numerous, secure off-site server locations. You
                don’t need to worry about building your own costly backup solution.
              </div>
            </div>
            <div className="cloud-col">
              <Flex className="v-center">
                <img src={icon2} />
                <h3>Data Security</h3>
              </Flex>
              <div className="sub-desc">
                We use high-level encryption and SSL security protocols to ensure maximum protection for your data.
                Backed by AWS, our security protocols are always up-to-date.
              </div>
            </div>
            <div className="cloud-col" style={{ width: 360 }}>
              <Flex className="v-center">
                <img src={icon3} style={{ width: 51, height: 42 }} />
                <h3>Info Protection Assurance </h3>
              </Flex>
              <div className="sub-desc">
                We will never view, share, or use your data unless you give us consent for troubleshooting purposes
                only.{' '}
              </div>
            </div>
          </Flex>
          <img src={productBg5} className="mobile-bg hide mobile-block" />
        </div>
      </CloudSection>
    )
  }

  renderDemoSection = () => {
    return (
      <Footer className="product-demo">
        <RequestDemo className="no-border">
          <div className="demo-wrapper">
            <p>
              Start using WholesaleWare today to discover why top U.S. food wholesalers, distributors, and exporters
              trust us for over $100M in annual sales.
            </p>
            <Flex className="h-center">
              <Button
                // onClick={openModal}
                css={homeButtonStyle}
                className="landing"
                style={{height: 51, padding: '0 20px'}}
              >
                <a href="https://share.hsforms.com/1zGjYvlFmT5e475T3I4GH5Acfaea" className="product">
                  Complimentary Demo
                  <IconSvg
                    viewBox="0 0 27 16"
                    width="27"
                    height="16"
                    type="arrow-right-home"
                    style={{ marginLeft: 20 }}
                  />
                </a>
              </Button>
            </Flex>
          </div>
        </RequestDemo>
      </Footer>
    )
  }

  render() {
    return (
      <HomePageWrapper>
        <div style={{ backgroundColor: '#1C6E31', paddingTop: 197 }}>{this.renderSplash()}</div>
        {this.renderImproveSection()}
        {this.renderSeamlessSection()}
        {this.renderBuiltSection()}
        {this.renderGreention()}
        {this.renderIntegratesSection()}
        {this.renderCloudSection()}
        {this.renderDemoSection()}
        <FooterContainer />
      </HomePageWrapper>
    )
  }

  onClickRequest = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
  }
}

const mapStateToProps = (state: GlobalState) => state.account

export const HomeProductContainer = withTheme(connect(AccountModule)(mapStateToProps)(HomeProduct))
