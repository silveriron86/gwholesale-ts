import styled from '@emotion/styled'
import { black, darkGreen, medLightGrey } from '~/common'
import { rgba } from 'polished'

export const ProductContainer = styled('div')({
  display: 'flex',
  flexFlow: 'column',
  alignItems: 'center',
  justifyContent: 'center',

  marginTop: '136px',
  marginBottom: '-22px',

  '@media (max-width: 480px)': {
    marginTop: '30px',
  },
})

export const ProductSection = styled('div')({
  textAlign: 'left',
  maxWidth: '100%',
  marginBottom: '68px',
})

export const BannerHeader = styled('p')({
  fontWeight: 700,
  marginTop: '10px',
  paddingLeft: '50px',
  paddingRight: '50px',
  fontSize: '50px',
  textAlign: 'left',
  lineHeight: '140.04%',
  color: black,
  maxWidth: '810px',

  '@media (max-width:480px)': {
    width: '320px',
    paddingLeft: '28px',
    paddingRight: '10px',
    fontSize: '27px',
  },
})

export const BannerContent = styled('div')({
  display: 'flex',
  flexFlow: 'row wrap',
  fontWeight: 700,
  fontSize: '50px',
})

export const BannerContentText = styled('span')({
  fontWeight: 200,
  fontSize: '20px',
  lineHeight: '140.04%',
  maxWidth: '430px',
  marginTop: '38px',
  paddingLeft: '50px',
  marginBottom: '50px',

  '@media (max-width:480px)': {
    fontSize: '15px',
    marginTop: '10px',
    marginBottom: '20px',
    paddingLeft: '28px',
  },
})

export const BannerImage = styled('span')({
  overflow: 'hidden',
  //marginLeft: 'auto',
  '@media (max-width:480px)': {
    maxWidth: '100%',
  },
})

export const Image = styled('span')({})

export const HighlightSection = styled('div')({
  textAlign: 'center',
  flexFlow: 'row wrap',
  justifyContent: 'space-around',
  display: 'flex',
  marginBottom: '62px',

  '& > *': {
    flex: '1 100%',
  },

  '@media (max-width: 480px)': {
    marginBottom: '20px',
  },
})

export const Highlight = styled('div')({
  flexDirection: 'column',
  display: 'flex',
  maxWidth: '400px',
})

export const HighlightTitle = styled('span')({
  marginTop: '22px',
  marginBottom: '47px',
  color: darkGreen,
  fontWeight: 700,
  fontSize: '24px',
  lineHeight: '29px',
})

export const HighlightContent = styled('ul')({
  textAlign: 'left',
  maxWidth: '767px',

  '@media (max-width: 480px)': {
    paddingRight: '20px',
  },
})

export const HighlightContentPoint = styled('li')({
  marginBottom: '24px',
  fontSize: '16px',
  lineHeight: '150%',
})

export const ManufacturingSection = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',

  textAlign: 'center',
  width: '100%',
  paddingBottom: '34px',
  marginBottom: '60px',
  backgroundColor: medLightGrey,

  '@media (max-width: 480px)': {
    marginBottom: '20px',
  },
})

export const SectionTitle = styled('span')({
  color: black,
  marginTop: '44px',
  marginBottom: '55px',
  fontWeight: 700,
  fontSize: '40px',
  lineHeight: '48px',
})

export const ManufacturingHighlight = styled('div')({
  textAlign: 'left',
  flexDirection: 'column',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'flex-start',

  '@media (max-width: 768px)': {
    maxWidth: '400px',
  },
})

export const ManufacturingHighlightTitle = styled('span')({
  color: darkGreen,
  maxWidth: '767px',
  paddingLeft: '20px',
  paddingRight: '20px',
  fontWeight: 700,
  fontSize: '18px',
  lineHeight: '143.04%',
  marginBottom: '35px',

  '@media (max-width: 480px)': {
    paddingLeft: 0,
    paddingRight: 0,
    width: '80%',
    textAlign: 'left',
    alignSelf: 'center',
  },
})

export const HRSection = styled('div')({
  paddingLeft: '14px',
  paddingRight: '14px',
  textAlign: 'center',
  marginBottom: '125px',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  display: 'flex',

  '@media (max-width: 480px)': {
    paddingLeft: 0,
    paddingRight: 0,
    marginBottom: '20px',
  },
})

export const HRSectionTitle = styled('span')({
  color: black,
  marginTop: '44px',
  marginBottom: '55px',
  fontWeight: 700,
  fontSize: '40px',
  lineHeight: '48px',
})

export const HRBoxContainer = styled('div')({
  flexFlow: 'row wrap',
  justifyContent: 'center',
  display: 'flex',
  maxWidth: '1285px',
})

export const HRBox = styled('div')({
  display: 'flex',
  flexFlow: 'column',
  textAlign: 'left',
  width: '400px',
  height: '293px',
  background: rgba(82, 158, 99, 0.1),
  marginRight: '14px',
  marginBottom: '15px',

  '@media (max-width: 480px)': {
    width: '90%',
    marginRight: 0,
  },
})

export const BoxTitle = styled('span')({
  fontSize: '18px',
  lineHeight: '143.04%',
  fontWeight: 700,
  marginTop: '38px',
  marginLeft: '57px',
  marginRight: '20px',
  color: darkGreen,

  '@media (max-width: 480px)': {
    marginLeft: '30px',
  },
})

export const BoxContent = styled('span')({
  fontSize: '16px',
  lineHeight: '143.04%',
  marginLeft: '57px',
  marginRight: '20px',
  maxWidth: '280px',

  '@media (max-width: 480px)': {
    marginLeft: '30px',
  },
})

export const AboutSection = styled('div')({
  textAlign: 'center',
  maxWidth: '100%',
  marginBottom: '125px',
  display: 'flex',
  flexFlow: 'row wrap',
  justifyContent: 'center',
})

export const AboutSectionTitle = styled('span')({
  color: black,
  fontWeight: 700,
  marginTop: '11px',
  marginBottom: '50px',
  paddingLeft: '50px',
  paddingRight: '50px',
  fontSize: '40px',
  lineHeight: '48px',
  alignSelf: 'flex-start',

  '@media (max-width:480px)': {
    alignSelf: 'center',
    paddingLeft: 0,
    paddingRight: 0,
  },
})

export const AboutContent = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  textAlign: 'left',
  fontWeight: 700,
  fontSize: '50px',
  maxWidth: '622px',
})

export const AboutImage = styled('span')({
  overflow: 'hidden',
  marginBottom: '30px',
  '@media (max-width:480px)': {
    maxWidth: '100%',
  },
})

export const AboutContentText = styled('span')({
  fontWeight: 200,
  fontSize: '20px',
  lineHeight: '140.04%',
  paddingLeft: '50px',
  paddingRight: '50px',

  '@media (max-width:480px)': {
    width: '90%',
    fontSize: '15px',
    paddingLeft: '20px',
    paddingRight: '20px',
  },
})
