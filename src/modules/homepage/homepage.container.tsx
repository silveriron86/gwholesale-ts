/**@jsx jsx */
import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteProps } from 'react-router'
import { jsx } from '@emotion/core'
import { Row, Col, Button } from 'antd'
import {
  HomePageWrapper,
  SplashSection,
  SplashContent,
  SplashHeadline,
  SplashText,
  SplashImageWrapper,
  SplashImage,
  UserSection,
  UserContent,
  UserHeadline,
  UserLogoList,
  UserLogoWrapper,
  UserLogo,
  SectionHeadline,
  SectionBody,
  BlendedImage,
} from './homepage.style'
import ileaf from './images/GettyImages-1182645061.jpg'
import Deposit1 from './images/Depositphotos_213124958_ds.jpg'
import Deposit2 from './images/Depositphotos_58286313_ds.jpg'
import Deposit3 from './images/Depositphotos_15415999_ds.jpg'

import quickbox from './images/quickbox.png'
import oracle from './images/oracle.png'
import zebra from './images/zebra.png'
import pic0 from './images/pic0.png'
import pic1 from './images/pic1.png'
import pic2 from './images/HR_image.png'
import pic3 from './images/pic3.png'
import threeImage from './images/three_images.jpg'

import { HomePageDispatchProps, HomePageStateProps, HomePageModule } from './homepage.module'
import { GlobalState } from '~/store/reducer'
import { FooterContainer } from './components/footer.component'
import ReactGA from 'react-ga'

import { Icon as IconSvg } from '~/components'
import produceImg1 from './images/produce1.png'
import produceImg2 from './images/produce2.png'
import proteinImg1 from './images/protein1.png'
import proteinImg2 from './images/protein2.png'
import proteinImg3 from './images/protein3.png'
import { Header } from '~/components'
import { Flex } from '../customers/customers.style'
import { homeButtonStyle } from '~/components/header/header.style'
import SlidesContainer from './components/slides.container'
import jQuery from 'jquery'
import FadeInOut from './components/fade'

export type HomePageComponentProps = HomePageDispatchProps & HomePageStateProps & RouteProps

export class HomePageContainer extends React.PureComponent<HomePageComponentProps> {
  state = {
    mobileVersion: false,
    visibleImage: false,
  }

  componentDidMount() {
    this.props.logout();
    ReactGA.initialize('UA-143951733-1')
    ReactGA.pageview('/home/landing')

    setTimeout(() => {
      if (!this.state.mobileVersion) {
        this.setState({
          visibleImage: true
        })

        jQuery('.home-content')
          .css({ top: 200, opacity: 0, position: 'absolute' })
          .animate({ top: 130, opacity: 1 }, 1500)
      }
    }, 2000)

    window.addEventListener('resize', this.resize.bind(this))
    this.resize()
  }

  resize() {
    this.setState({ mobileVersion: window.innerWidth <= 760 })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resize.bind(this))
  }

  renderSplash() {
    const { openModal } = this.props
    const { mobileVersion, visibleImage } = this.state
    const arrowRightIcon = (
      <IconSvg viewBox="0 0 27 16" width="27" height="16" type="arrow-right-home" style={{ marginLeft: 20 }} />
    )
    return (
      <SplashSection>
        <Header
          isMobile={mobileVersion}
          hide={false}
          // tabConfig={headerConfig as any}
          // logo={props.logo}
          location={location}
          currentUser={null}
        />
        <div style={{ overflow: 'hidden' }}>
          <SplashContent className="home-content" style={{ opacity: mobileVersion ? 1 : 0 }}>
            <SplashHeadline style={{lineHeight: '60px', color: 'white', whiteSpace: 'pre'}}>
              Empowering the{'\n'}
              Future of Fresh Food{'\n'}
              Supply Chain Leaders
            </SplashHeadline>
            <SplashText>
              WholesaleWare is a cloud-based software solution that’s made specifically for the food wholesale industry.
            </SplashText>
            <Button css={homeButtonStyle} className="landing">
              <a href="https://share.hsforms.com/1zGjYvlFmT5e475T3I4GH5Acfaea">
                {mobileVersion ? 'Request a Demo' : 'Request a Complimentary Demo'}
                {arrowRightIcon}
              </a>
            </Button>
          </SplashContent>
        </div>
        <SplashImageWrapper>
          <FadeInOut show={visibleImage} duration={5000}>
            <SplashImage
              src={ileaf}
              className={`${mobileVersion ? 'mobile' : ''}`}
            />
          </FadeInOut>
        </SplashImageWrapper>
      </SplashSection>
    )
  }

  renderUsers() {
    const { mobileVersion } = this.state
    return (
      <UserSection style={{ marginTop: mobileVersion ? 35 : 0, paddingBottom: mobileVersion ? 0 : 40 }}>
        <UserContent>
          <Row style={{ width: '100%' }} gutter={mobileVersion ? 0 : [40, 40]}>
            <Col sm={8} xs={24}>
              <h1>Our tools are trusted for $500M+ in sales each year</h1>
              <div className="white-wrapper">
                Prior to WholesaleWare, sorting and finding items or prices was time consuming and a hassle. After
                switching, the program was{' '}
                <strong>easy to pick up, modern, and made sales and inventory management far more streamlined.</strong>{' '}
                We really enjoy the many options for customization and the feature-rich platform. Support for the
                program is excellent!
                <div className="user-company">
                  Jackey, Accounting
                  <span>Friends Development, Inc.</span>
                </div>
              </div>
            </Col>
            <Col sm={8} xs={24}>
              <div className="white-wrapper">
                As a produce wholesaler with hundreds of SKUs to manage, we searched long and hard for a software program that would <strong>increase our productivity and power our operations</strong>. WholesaleWare came highly recommended by other produce professionals. The automation features to create sales orders enable my team to <strong>output orders much faster with minimal errors</strong>. The user-friendly platform helps new employees train with ease. It allows modification of orders at any point in the process, even after shipping, without friction, for faster and accurate invoicing. As a cloud-based program, Wholesaleware can be accessed from any computer at any time - <strong>this mobility is a game changer in the produce industry. I highly recommend this program; it’s worth the investment</strong>.
                <div className="user-company">
                  Cindy, Sales
                  <span>Berti Produce</span>
                </div>
              </div>
            </Col>
            <Col sm={8} xs={24}>
              <div className="white-wrapper" style={{color: '#1C6E31'}}>
                The Covid-19 pandemic made us realize we needed to adapt our business quickly in a very uncertain environment. We weren’t going to just wait it out. WholesaleWare’s <strong>rich features surprised us, because they were delivered in an easy-to-use and lightweight website</strong>. It enabled us to cement ourselves in a rapidly-changing supply chain while <strong>profitably pivoting our food business</strong> during this critical moment.
                <div className="user-company">
                  Jimmy, Sales
                  <span>Grand Food, Inc.</span>
                </div>
              </div>
            </Col>
          </Row>
          <UserHeadline style={{marginTop: 40}}>Some of our customers</UserHeadline>
          <div style={{width: 113, height: 4, backgroundColor: '#BAD6C6', margin: '15px auto', borderRadius: 2}}></div>
          <UserLogoList>
            <UserLogoWrapper>
              <UserLogo src={produceImg1} style={{ width: 122 }} />
            </UserLogoWrapper>
            <UserLogoWrapper>
              <UserLogo src={produceImg2} style={{ width: 108 }} />
            </UserLogoWrapper>
            <UserLogoWrapper>
              {/* <UserLogo src={proteinImg3} style={{ width: 59 }} /> */}
              <BlendedImage
                style={{
                  backgroundImage: `url(${proteinImg3})`,
                  width: 59,
                  height: (59 / 120) * 104,
                }}
              />
            </UserLogoWrapper>
            <UserLogoWrapper>
              {/* <UserLogo src={proteinImg2} style={{ width: 150 }} /> */}
              <BlendedImage
                style={{
                  backgroundImage: `url(${proteinImg2})`,
                  width: 150,
                  height: (150 / 356) * 94,
                }}
              />
            </UserLogoWrapper>
            <UserLogoWrapper>
              {/* <UserLogo src={proteinImg1} style={{ width: 157 }} /> */}
              <BlendedImage
                style={{
                  backgroundImage: `url(${proteinImg1})`,
                  width: 157,
                  height: (157 / 349) * 94,
                }}
              />
            </UserLogoWrapper>
            <UserLogoWrapper>
              <p>Berti Produce</p>
            </UserLogoWrapper>
            {/* <UserLogoWrapper>
              <p>Farm Fresh Organics</p>
            </UserLogoWrapper> */}
          </UserLogoList>
        </UserContent>
      </UserSection>
    )
  }

  render() {
    const { mobileVersion } = this.state
    return (
      <HomePageWrapper>
        {this.renderSplash()}
        <div>
          <div
            style={{ margin: '100px 115px 0', paddingBottom: 100, position: 'relative', zIndex: -1 }}
            className="flex-wrapper"
          >
            <Flex className="flex" style={{ position: 'relative' }}>
              {!mobileVersion && (
                <div>
                  <img src={threeImage} alt="" style={{ maxWidth: 707 }} />
                </div>
              )}
              <div style={mobileVersion ? {paddingTop: 30 } : { marginTop: 30, marginLeft: 40 }}>
                <SectionBody className="text-left">
                  Complete with seamless inventory management, eCommerce integrations, rich selling & purchasing
                  functionality, and real-time reports, WholesaleWare is trusted by leading{' '}
                  <strong>U.S. food wholesalers, distributors, and suppliers,</strong> offering 24/7 cloud-based access
                  from anywhere.
                </SectionBody>
              </div>
              {mobileVersion && (
                <div style={{ marginTop: 29 }}>
                  <img src={threeImage} alt="" style={{ width: '100%' }} />
                </div>
              )}
            </Flex>
            <img
              src={pic0}
              alt=""
              width={mobileVersion ? 300 : 759}
              style={{ zIndex: -1, bottom: mobileVersion ? -235 : -400, right: -120, position: 'absolute' }}
            />
          </div>
        </div>
        <div style={{ marginTop: mobileVersion ? 40 : 0 }}>
          <SlidesContainer isMobile={mobileVersion} openModal={this.props.openModal} />
        </div>

        <div style={{ margin: '-100px 115px 150px', position: 'relative', zIndex: 1 }} className="flex-wrapper">
          <Flex className="v-center" style={{ position: 'relative' }}>
            <div>
              <SectionHeadline className="text-left">
                Connects easily to the
                <br />
                platforms you know and trust
              </SectionHeadline>
              <SectionBody className="text-left" style={{ marginTop: 10 }}>
                WholesaleWare seamlessly interfaces with leading platforms to cover 360° of your accounting needs. Purposefully built for food wholesalers, it fills in gaps that generic solutions leave behind.
              </SectionBody>
            </div>
            <div style={{ marginLeft: mobileVersion ? 0 : 100, width : mobileVersion ? '100%' : 'auto' }}>
              <Flex className="v-center space-between three">
                <img src={quickbox} alt="" />
                <img src={oracle} alt="" style={{margin: mobileVersion ? 0 : '0 40px'}} />
                <img src={zebra} alt="" />
              </Flex>
            </div>
          </Flex>
        </div>

        <div style={{ margin: '0 115px', paddingBottom: 65, position: 'relative' }} className="flex-wrapper">
          {!mobileVersion && (
            <img src={Deposit2} width="834" style={{ zIndex: 0, bottom: -95, marginLeft: -372, position: 'absolute' }} />
          )}
          <Flex className="v-center" style={{ position: 'relative', zIndex: 0 }}>
            {!mobileVersion && (
              <div>
                <img src={pic1} alt="" className="pic" />
              </div>
            )}
            <div style={{ width: 100 }}></div>
            <div>
              <SectionHeadline>
                Customer-centric
                <br />
                eCommerce and CRM tools
              </SectionHeadline>
              <SectionBody className="text-left" style={{ marginTop: 10 }}>
                Our proprietary, in-house eCommerce platform and built-in CRM tools empower you to understand your
                buyers, provide a custom-tailored experience, track orders, conduct fulfillment, strategically update
                prices, and accept orders with the click of a button.{' '}
              </SectionBody>
            </div>
            {mobileVersion && (
              <div>
                <img src={pic1} alt="" className="pic" />
              </div>
            )}
          </Flex>
        </div>

        <div style={{ margin: '0 115px', paddingBottom: 230, position: 'relative' }} className="flex-wrapper">
          {!mobileVersion && (
            <img
              src={Deposit3}
              alt=""
              width="604"
              style={{ zIndex: 0, bottom: 0, right: -291, position: 'absolute' }}
            />
          )}
          <Flex className="v-center" style={{ position: 'relative', zIndex: 0 }}>
            <div>
              <SectionHeadline className="text-left">
                Comprehensive HR & employee
                <br />
                management support
              </SectionHeadline>
              <SectionBody className="text-left" style={{ marginTop: 10 }}>
                Stay on top of your employees by managing and monitoring performance, timesheets, PTO, and more. Our
                tools enable you to maximize efficiency, log important documents, track hours, and award bonuses.{' '}
              </SectionBody>
            </div>
            <div style={{ marginLeft: mobileVersion ? 0 : 100 }}>
              <img src={pic2} alt="" className="pic" style={{ maxWidth: 618 }} />
            </div>
          </Flex>
        </div>

        <div style={{ margin: '-100px 126px 0' }} className="flex-wrapper">
          {mobileVersion && (
            <img
              src={Deposit3}
              alt=""
              width="271"
              style={{ zIndex: -1, bottom: -110, left: -55, position: 'absolute' }}
            />
          )}
          <Flex style={{display: mobileVersion ? 'block': 'flex'}}>
            {!mobileVersion && (
              <div>
                <img src={pic3} className="pic" alt="" />
              </div>
            )}
            <div style={{ marginLeft: mobileVersion ? 0 : 100 }}>
              <SectionHeadline style={{position: 'relative', marginTop: mobileVersion ? 0 : 90}}>
                Real-time reports and
                <br />
                data management
              </SectionHeadline>
              <SectionBody className="text-left"  style={{ marginTop: 10 }}>
                Access in-depth order information, profit margins, financial insights, and more in just a few clicks.
                With real-time updates and exportable data for independent analysis, you can take confidence in knowing
                that you’ll always be up to date with the latest information.{' '}
              </SectionBody>
            </div>
            {mobileVersion && (
              <div>
                <img src={pic3} className="pic" alt="" style={{width: '100%'}} />
              </div>
            )}
          </Flex>
        </div>
        {/* {this.renderCustomer()} */}
        {this.renderUsers()}
        <div
          style={{
            height: mobileVersion ? 'auto' : 561,
            overflow: 'hidden',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <img src={Deposit1} style={{ width: '100%' }} />
        </div>
        <FooterContainer isMobile={mobileVersion} location={location}/>
      </HomePageWrapper>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.homepage

export const HomePageComponent = connect(HomePageModule)(mapStateToProps)(HomePageContainer)
