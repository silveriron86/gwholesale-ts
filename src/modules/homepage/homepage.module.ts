import { EffectModule, Module, ModuleDispatchProps, Effect, Reducer, StateObservable } from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { switchMap, map, catchError, tap, mapTo } from 'rxjs/operators'

import { HomePageService } from './homepage.service'

import { MessageType } from '~/components/utils'
import { Company } from '~/schema'
import { SettingService } from '../setting/setting.service'
import { Action } from 'redux-actions'
import { checkError } from '~/common/utils'
import { GlobalState } from '~/store/reducer'
import { USER_LOGOUT_SUCCESS } from '~/root.module'

//import { checkError, checkMessage } from '~/common/utils'

export interface HomePageStateProps {
  message?: string
  type?: MessageType | null
  showModal?: boolean
  companyInfo: Company | null
}

@Module('homepage')
export class HomePageModule extends EffectModule<HomePageStateProps> {
  defaultState = {
    message: '',
    type: null,
    showModal: false,
    companyInfo: null
  }

  constructor(
    private readonly homepage: HomePageService,
    private readonly setting: SettingService
  ) {
    super()
  }

  @Effect({
    done: (state: HomePageStateProps) => {
      return { ...state, message: "", type: null }
    },
  })
  resetNotif(action$: Observable<void>) {
    return action$.pipe(
      map(this.createAction('done'))
    )
  }

  @Effect({
    done: (state: HomePageStateProps) => {
      return { ...state }
    },
  })
  logout(action$: Observable<void>) {
    return action$.pipe(
      map(this.createAction('done')),
      tap(() => {
        localStorage.clear()
      }),
      mapTo(this.markAsGlobal(USER_LOGOUT_SUCCESS())),
    )
  }

  @Effect({
    done: (state: HomePageStateProps) => {
      return {
        ...state,
        message: "Request Sent", type: MessageType.SUCCESS,
        showModal: false,
      }
    },
  })
  sendRequestDemo(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.homepage.sendRequestDemo(data).pipe(map(this.createAction('done'))),
      ),
    )
  }



  @Reducer()
  closeModal(state: HomePageStateProps) {
    return {
      ...state,
      showModal: false,
    }
  }

  @Reducer()
  openModal(state: HomePageStateProps) {
    return {
      ...state,
      showModal: true,
    }
  }


  @Effect({
    done: (state: HomePageStateProps, action: Action<any>) => {
      return {
        ...state,
        companyInfo: action.payload.userSetting.company,
      }
    },
  })
  getCompanyInMenuList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getUserSetting(state$.value.currentUser ? state$.value.currentUser.userId! : '0')
          .pipe(
            map(this.createAction('done')),
            catchError((error) => of(checkError(error)))
          ),
      ),
    )
  }

}

export type HomePageDispatchProps = ModuleDispatchProps<HomePageModule>
