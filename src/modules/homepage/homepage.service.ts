import { Injectable } from 'redux-epics-decorator'

import { Http } from '~/common'

@Injectable()
export class HomePageService {
  constructor(private readonly http: Http) {}

  sendRequestDemo(params: any) {
    console.log(params)
    return this.http.post<any>('/demo/requestDemo', {
      query: params,
    })
  }
}
