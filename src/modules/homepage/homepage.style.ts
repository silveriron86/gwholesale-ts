import styled from '@emotion/styled'
import css from '@emotion/css'
import { Button } from 'antd'

import { black, brightGreen, brownGrey, forestGreen } from '~/common'

import Background from './images/backgroundLanding.png'
import applesImg from './images/apples.jpg'
import productPricingImg from './images/product-pricing-bg.png'
import GettyImages from './images/product_bg_2.jpg'

export const Container = styled('div')({
  width: '100%',
  boxSizing: 'border-box',
  position: 'relative',
  marginBottom: '80px',
})

export const Headline = styled('span')({
  fontFamily: 'Museo Sans Rounded',
  fontSize: '50px',
  lineHeight: '143.04%',
  letterSpacing: '0.04em',
  fontWeight: 700,
  color: 'black',
})

export const HomePageWrapper = styled('div')({
  width: '100%',
  // backgroundImage: `url(${Background})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: '100% 100%',
  overflow: 'hidden',
  position: 'relative',
  // marginTop: '100px',
  marginBottom: '-22px',
  '.product-demo': {
    marginTop: '183px !important',
    paddingTop: 0,
    marginBotom: 0,
    paddingBottom: 0,
    '.demo-wrapper': {
      width: 1026,
      p: {
        fontSize: '24px !important',
      }
    },
  },
  '.section-bg': {
    position: 'absolute',
  },
  'a.product': {
    fontWeight: 'bold',
    color: '#1C6E31',
    fontFamily: 'Museo Sans Rounded',
    fontSize: 16,
  },
  '.relative': {
    padding: '0 0 71px',
    marginBottom: 0,
    position: 'relative',
    '.section-wrapper': {
      position: 'relative',
      maxWidth: 1224,
      margin: '0 auto',
      '.hide': {
        display: 'none',
      },
      'h1': {
        color: '#1C6E31',
        fontFamily: 'Patua One',
        fontSize: 36,
        lineHeight: '36px',
        marginBottom: 16,
      },
      '.description': {
        color: '#4A5355',
        fontFamily: 'Museo Sans Rounded',
        fontSize: 17,
        fontWeight: 300,
        lineHeight: '150%',
        paddingBottom: 45,
        borderBottom: '1px solid #D8DBDB',
        '&.empty': {
          paddingBottom: 20,
        },
        '&.no-border': {
          borderBottom: 0,
        },
      },
      '.ant-collapse-header': {
        color: '#1C6E31',
        fontFamily: 'Patua One',
        fontSize: 18,
        lineHeight: '18px',
        padding: '25px 0 25px 39px',
        i: {
          left: 5,
          fontSize: 14
        }
      },
      '.ant-collapse-content-active': {
        marginTop: -20,
      },
      '.ant-collapse-content-box': {
        p: {
          color: '#4A5355',
          fontFamily: 'Museo Sans Rounded',
          fontSize: 17,
          fontWeight: 300,
          lineHeight: '150%',
        }
      }
    }
  },
  '&.pricing': {
    h3: {
      fontFamily: 'Patua One',
      fontWeight: 400,
      color: '#1C6E31',
      fontSize: 24,
      lineHeight: '31.2px',
      textAlign: 'left',
      marginBottom: 10
    },
    '.sub-desc': {
      fontFamily: 'Museo Sans Rounded',
      fontWeight: 'normal',
      fontSize: 17,
      lineHeight: '150%',
      '&.has-border': {
        lineHeight: '25.5px',
        paddingBottom: 20,
        borderBottom: '1px solid #D8DBDB',
        marginBottom: 20,
      },
    },
    h4: {
      fontFamily: 'Patua One',
      fontWeight: 400,
      color: '#1C6E31',
      fontSize: 18,
      lineHeight: '23.4px',
      marginBottom: 5,
    },
    h5: {
      fontFamily: 'Patua One',
      fontWeight: 400,
      color: '#1C6E31',
      fontSize: 14,
      lineHeight: '18.2px',
      marginBottom: 5,
    },
  },
  '@media (max-width: 480px)': {
    backgroundImage: 'none',
    marginTop: 0,
    '.mobile-block': {
      display: 'block !important',
    },
    '.mobile-hide': {
      display: 'none !important',
    },
    '.flex-wrapper': {
      position: 'relative',
      margin: '20px !important',
      paddingBottom: '10px !important',
      '.v-center, .flex': {
        flexDirection: 'column',
        '&.three': {
          flexDirection: 'row',
          img: {
            width: 108,
            '&:first-child': {
              width: 100,
            },
            '&:last-child': {
              width: 70,
            },
          },
        },
        '.pic': {
          width: '100%',
          marginTop: 15
        },
      },
    },
    '.product-demo': {
      marginTop: '83px !important',
      '.demo-wrapper': {
        p: {
          fontSize: '14px !important',
          fontWeight: 'normal',
        }
      }
    },
    '.relative': {
      padding: '0 20px 35px',
      '.section-wrapper': {
        'h1': {
          fontSize: 24,
          lineHeight: '31.2px',
        },
        '.description': {
          fontSize: 14,
          fontWeight: 300,
          lineHeight: '21px',
          paddingBottom: 20,
        },
        '.ant-collapse-header': {
          padding: '20px 0 20px 39px',
        },
        '.ant-collapse-content-box': {
          p: {
            fontSize: 14,
            lineHeight: '21px',
          }
        },
        img: {
          width: '100%',
        },
      }
    },
    '&.pricing': {
      '.demo-wrapper': {
        '.ant-btn.landing': {
          width: 'auto !important',
        },
      },
    }
  },
})

export const buttonStyle = css({
  borderRadius: '20px',
  height: '40px',
  border: `1px solid ${brownGrey}`,
  width: '161px',
  backgroundColor: brightGreen,
  borderColor: brightGreen,
  fontSize: '16px',
  color: 'white',
})

export const largeButtonStyle = css({
  borderRadius: '25px',
  height: '50px',
  border: `1px solid #FFAA5B`,
  width: '268px',
  backgroundColor: 'white',
  borderColor: '#FFAA5B',
  boxShadow: '0px 4px 20px rgba(255, 170, 91, 0.3)',
  fontSize: '18px',
  lineHeight: '19px',
  letterSpacing: '0.05em',
  color: '#FFAA5B',
})

export const SplashSection = styled('div')({
  background: `#1d6f31 url(${applesImg}) center no-repeat`,
  display: 'flex',
  flexFlow: 'row wrap',
  justifyContent: 'flex-start',
  alignItems: 'flex-start',
  // marginTop: 79,
  paddingLeft: 100,
  paddingRight: '2%',
  paddingTop: 130,
  minHeight: 580,
  width: '100%',

  '&.product-pricing': {
    // background: `#1d6f31 url(${productPricingImg}) center no-repeat`,
    background: `linear-gradient(180deg, #1C6E31 0%, rgba(28, 110, 49, 0) 30.17%), url(${productPricingImg})`,
    backgroundBlendMode: 'normal, multiply',
    backgroundColor: '#1C6E31',
    backgroundPositionX: 'center',
    backgroundSize: '100%',
    minHeight: 636,
    height: 636,
    padding: 0,
    fontFamily: 'Patua One',
    display: 'block',
    '.ant-btn': {
      '&.service': {
        marginTop: 50,
        width: 312,
        height: 75,
        background: '#F2F9F3',
        borderRadius: '5px 0px 0px 5px',
        fontSize: 18,
        color: '#1C6E31',
        fontWeight: 'bold',
        span: {
          fontFamily: 'Museo Sans Rounded',
        },
        '&.active': {
          background: '#0D4B1C',
          boxShadow: 'inset 0px 4px 2px rgba(0, 0, 0, 0.08)',
          borderRadius: '0px 5px 5px 0px',
          color: 'white',
          fontWeight: 'normal',
        },
        img: {
          marginRight: 15,
        }
      }
    },
    '.title': {
      textAlign: 'center',
      width: '100%',
      fontFamily: 'Museo Sans Rounded',
      fontSize: 16,
      fontWeight: 700,
      color: 'white',
      marginTop: -146,
      marginBottom: 10
    },
    'h1': {
      color: '#F7F7F7',
      fontWeight: 'normal',
      fontSize: 50,
      lineHeight: '120%',
      textAlign: 'center',
      maxWidth: 1032,
      width: '100%',
      marginBottom: 50,
      margin: '0 auto'
    },
    'h3': {
      color: '#F7F7F7',
      fontSize: 24,
      lineHeight: '31.2px',
      textAlign: 'center',
      width: 818,
      margin: '0 auto'
    },
    '.hr': {
      height: 1,
      backgroundColor: '#689D7F',
      width: 1053,
      margin: '50px auto'
    },
    '.flex': {
      display: 'flex',
      flexDirection: 'row',
      width: 'fit-content',
      margin: '35px auto'
    },
    '.prod-col': {
      width: 143,
      margin: '0 20px',
      '.img-wrapper': {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        height: 55,
        borderBottom: '2px solid #689D7F',
        'svg': {
          fill: 'transparent !important'
        }
      },
      '.link': {
        color: '#BAD6C6',
        fontFamily: 'Museo Sans Rounded',
        fontWeight: 600,
        fontSize: 14,
        lineHeight: '17px',
        margin: '15px 0',
        '&.first': {
          fontSize: 18,
          color: 'white',
          lineHeight: '130%',
        }
      }
    }
  },

  '@media (max-width: 480px)': {
    padding: 0,
    marginBottom: 90,
    minHeight: 527,
    backgroundSize: '100%',
    backgroundPosition: 200,
    '&.product-pricing': {
      backgroundSize: 'auto 100%',
      height: 'auto',
      paddingBottom: 1,
      marginBottom: 0,
      '&.pricing': {
        height: 1780,
      },
      '.header-home': {
        position: 'absolute',
        width: 164,
      },
      '.title': {
        marginTop: -186,
      },
      h1: {
        fontSize: '24px !important',
        lineHeight: '110%',
        padding: '0 20px',
        fontWeight: 400,
      },
      h3: {
        width: '100% !important',
        padding: '0 20px',
        fontSize: 18,
        fontWeight: 400,
      },
      '.hr': {
        margin: '29px 20px 25px',
        width: 'auto',
      },
      '.flex': {
        display: 'block',
        margin: '25px auto',
        '.prod-col': {
          width: '100%',
          '&::after': {
            display: 'block',
            content: '',
            clear: 'both',
          },
          '.img-wrapper': {
            width: 50,
            borderBottom: 0,
            float: 'left',
          },
          '.link': {
            display: 'none',
            '&.first': {
              display: 'block',
              float: 'left',
              marginLeft: 0,
              fontSize: 16,
              marginTop: 18,
            }
          }
        }
      },
      '.service-btns': {
        margin: '0 20px',
      },
      '.ant-btn': {
        '&.service': {
          height: 62,
          display: 'flex',
          alignItems: 'center',
          marginTop: 27,
          span: {
            textAlign: 'left',
            fontWeight: 700,
            fontSize: 12,
            lineHeight: '15.6px',
            overflowWrap: 'break-word',
            wordBreak: 'break-word',
            whiteSpace: 'normal',
            flex: 1,
          }
        }
      }
    }
  },
})

export const SplashContent = styled('div')({
  // marginTop: '60px',
  maxWidth: 451,

  '@media (max-width: 480px)': {
    margin: 0,
    padding: '0 80px 100px',
    textAlign: 'center',
    button: {
      width: 'auto !important',
    },
  },
})

export const SplashHeadline = styled(Headline)({
  fontFamily: 'Patua One',
  fontWeight: 400,
  fontSize: 50,
  lineHeight: '55px',
  color: 'white',
  '@media (max-width: 480px)': {
    fontSize: 24,
    lineHeight: '26.4px',
  },
})

export const SplashText = styled('p')({
  marginTop: 20,
  fontSize: '20px',
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 400,
  lineHeight: '30px',
  color: 'white',
  '@media (max-width: 480px)': {
    fontSize: '14px',
    lineHeight: '18.9px',
    padding: '0 15px',
  },
})

export const SplashButton: any = styled(Button)({
  marginTop: '40px',

  '@media (max-width: 480px)': {
    transform: 'scale(0.9) translate(-5%,-10%)',
    marginTop: '20px',
  },
})

export const SplashImageWrapper = styled('div')({
  overflow: 'hidden',
  borderRadius: 15,
  maxWidth: '60%',
  height: 'auto',

  '@media (max-width: 480px)': {
    marginTop: '20px',
    maxWidth: '125%',
  },
})

export const SplashImage = styled('img')({
  borderRadius: 15,
  width: 739,
  height: 'auto',
  position: 'absolute',
  zIndex: 2,
  right: -44,

  '@media (max-width: 480px)': {
    width: '128%',
    '&.home-image1': {
      '&.mobile': {
        display: 'block !important',
        margin: '-120px auto 0',
        left: -40,
        width: 'calc(100% - 40px)'
      }
    }
  }
},
})

export const BusinessSection = styled('div')({
  display: 'flex',
  flexFlow: 'row wrap-reverse',
  justifyContent: 'flex-start',
  alignItems: 'center',

  // paddingLeft: '2%',
  // paddingRight: '0%',

  '@media (max-width: 480px)': {
    padding: 0,
  },
})

export const BusinessContent = styled('div')({
  marginTop: '60px',
  marginLeft: '40px',
  maxWidth: '40%',

  '@media (max-width: 480px)': {
    marginTop: '80px',
    marginLeft: 0,
    paddingLeft: '9%',
    paddingRight: '8%',
    maxWidth: '100%',
  },
})

export const BusinessImageWrapper = styled('div')({
  marginTop: '60px',
  overflow: 'hidden',
  maxWidth: '50%',
  height: 'auto',

  '@media (max-width: 480px)': {
    marginTop: '20px',
    maxWidth: '125%',
  },
})

export const BusinessImage = styled('img')({
  width: '100%',
  height: 'auto',

  '@media (max-width: 480px)': {
    width: '103%',
  },
})

export const BusinessHeadline = styled(Headline)({
  '@media (max-width: 480px)': {
    fontSize: '30px',
  },
})

export const BusinessText = styled('div')({
  marginTop: '40px',
  fontSize: '20px',
  color: black,
  lineHeight: '143.04%',
  letterSpacing: '0.05em',

  '@media (max-width: 480px)': {
    fontSize: '16px',
    marginTop: '20px',
  },
})

export const BusinessButton: any = styled(Button)({
  marginTop: '40px',

  '@media (max-width: 480px)': {
    transform: 'scale(0.9) translate(-5%)',
    marginTop: '20px',
  },
})

export const CustomerSection = styled('div')({
  width: '100%',
  marginTop: '60px',
  display: 'flex',
  flexFlow: 'column',
  justifyContent: 'center',
  alignItems: 'center',
})

export const CustomerHeadline = styled(Headline)({
  width: '100%',
  textAlign: 'center',
  paddingLeft: '9%',
  paddingRight: '8%',

  '@media (max-width: 480px)': {
    fontSize: '30px',
    textAlign: 'left',
  },
})

export const CustomerCardWrapper = styled('div')({
  width: '100%',
  overflow: 'hidden',
  textAlign: 'center',

  '@media (max-width: 480px)': {
    position: 'relative',
    height: '260px',
  },
})
export const CustomerCard = styled('div')({
  textAlign: 'left',
  marginTop: '64px',
  backgroundRepeat: 'no-repeat',
  backgroundSize: '100% 100%',
  width: '650px',
  height: '465px',
  padding: '165px 370px 15px 48px',
  overflow: 'hidden',
  display: 'inline-block',
  '@media (max-width: 480px)': {
    position: 'absolute',
    left: '50%',
    right: '50%',

    marginTop: '-85px',
    marginLeft: '-325px',
    transform: 'scale(0.5, 0.5)',
  },
})

export const CustomerText = styled('div')({
  verticalAlign: 'bottom',
  color: black,
  fontSize: '18px',
  lineHeight: '25px',
  width: '240px',
  height: '202px',
  marginBottom: '25px',
})

export const CustomerCompany = styled('div')({
  marginTop: '12px',
  color: forestGreen,
  fontSize: '14px',
  letterSpacing: '0.05em',
  lineHeight: '20px',
  textTransform: 'uppercase',
})

export const UserSection = styled('div')({
  width: '100%',
  paddingBottom: 75,
  display: 'flex',
  flexFlow: 'row nowrap',
  justifyContent: 'space-between',
  position: 'relative',
  background: '#F2F9F3',
  '@media (max-width: 480px)': {
    paddingBottom: 0,
  },
})

export const UserContent = styled('div')({
  display: 'flex',
  flexFlow: 'column',
  justifyContent: 'flex-start',
  alignItems: 'flex-start',
  width: '100%',
  margin: '0 124px',
  paddingTop: '50px',

  h1: {
    fontFamily: 'Patua One',
    fontWeight: 400,
    fontSize: 36,
    lineHeight: '46.8px',
    color: '#1C6E31',
    marginBottom: 45,
  },
  '.white-wrapper': {
    background: 'white',
    padding: 35,
    fontFamily: 'Museo Sans Rounded',
    fontWeight: 300,
    fontSize: 18,
    lineHeight: '27px',
    borderRadius: 5,
    strong: {
      fontWeight: 700,
      color: '#1C6E31',
    },
    '.user-company': {
      marginTop: 20,
      color: '#0D4B1C',
      fontFamily: 'Patua One',
      fontWeight: 400,
      fontSize: 14,
      lineHeight: '18.2px',
      span: {
        display: 'block',
        fontSize: 18,
        lineHeight: '23.4px',
        color: '#1C6E31',
      },
    },
  },
  '@media (max-width: 480px)': {
    margin: '0 0',
    padding: 20,
    h1: {
      fontSize: 18,
      lineHeight: '23.4px',
      textAlign: 'center',
      marginBottom: 15,
    },
    '.white-wrapper': {
      fontSize: 14,
      lineHeight: '18.9px',
      padding: 15,
    },

    '.ant-col': {
      marginTop: 15,
    },
  },
})

export const UserHeadline = styled(Headline)({
  marginTop: 20,
  width: '100%',
  textAlign: 'center',
  fontFamily: 'Patua One',
  fontWeight: 400,
  fontSize: 36,
  color: '#1C6E31',

  '@media (max-width: 480px)': {
    fontSize: 18,
    textAlign: 'center',
    // paddingLeft: '9%',
    // paddingRight: '8%',
  },
})

export const SubHeadline = styled('div')({
  fontFamily: 'Museo Sans Rounded',
  fontSize: 26,
  fontWeight: 'bold',
  color: '#22282A',
  '@media (max-width: 500px)': {
    paddingLeft: 30,
  },
})

export const UserLogoList = styled('div')({
  marginTop: 20,
  width: '100%',
  display: 'flex',
  flexFlow: 'row wrap',
  alignItems: 'center',
  justifyContent: 'space-between',
  '@media (max-width: 500px)': {
    // display: 'block',
    paddingLeft: 30,
  },
})

export const UserLogoWrapper = styled('div')({
  marginRight: 60,
  h3: {
    color: '#373D3F',
    marginBottom: 0,
    fontFamily: 'Museo Sans Rounded',
    fontSize: 35,
    lineHeight: '35px',
    textAlign: 'center',
  },
  '@media (max-width: 1625px)': {
    marginRight: 25,
    // img: {
    //   width: '90%'
    // },
  },
  '@media (max-width: 1024px) and (min-width: 1024px) ': {
    zoom: 0.75,
  },
  '@media (max-width: 500px)': {
    marginBottom: 20,
    '&:last-child': {
      marginBottom: 0,
    },
  },
  p: {
    color: '#1C6E31',
    fontFamily: 'Museo Sans Rounded',
    fontSize: 14,
    fontWeight: 700,
    margin: 0,
    padding: 0,
    textAlign: 'center',
  },
})

export const UserLogo = styled('img')({
  maxWidth: '100%',
  maxHeight: 60,
  objectFit: 'contain',
})

export const UserImage = styled('div')({
  maxWidth: '30%',
  textAlign: 'right',
  position: 'absolute',
  right: 0,

  '@media (max-width: 480px)': {
    width: 0,
    height: 0,
    visibility: 'hidden',
  },
})

export const BottomImage = styled('div')({
  maxWidth: '70%',
})

export const SectionHeadline = styled('div')({
  fontFamily: 'Patua One',
  fontWeight: 400,
  fontSize: 36,
  lineHeight: '46.8px',
  color: '#1C6E31',
  textAlign: 'right',
  '&.text-left': {
    textAlign: 'left',
  },
  '@media (max-width: 480px)': {
    fontSize: 18,
    lineHeight: '24px',
    textAlign: 'left !important',
  },
})

export const SectionBody = styled('div')({
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 300,
  fontSize: 20,
  lineHeight: '30px',
  color: '#4A5355',
  textAlign: 'right',
  '&.text-left': {
    textAlign: 'left',
  },
  strong: {
    fontWeight: 700,
    color: '#1C6E31',
  },
  '@media (max-width: 480px)': {
    fontSize: 14,
    lineHeight: '19px',
    textAlign: 'center !important',
    '&.text-left': {
      textAlign: 'left !important',
    },
  },
})

export const Slides = styled('div')({
  margin: '0 115px 150px',
  height: 644,
  padding: '45px 75px 75px',
  backgroundColor: '#1C6E31',
  borderRadius: 15,
  position: 'relative',
  zIndex: 1,
  h1: {
    fontFamily: 'Patua One',
    fontWeight: 400,
    fontSize: 36,
    lineHeight: '46.8px',
    color: 'white',
    width: '100%',
    textAlign: 'center',
    marginBottom: 30,
  },
  '.c-border': {
    width: 113,
    height: 4,
    backgroundColor: '#689D7F',
    margin: '0 auto 42px',
  },
  '.tab-col': {
    alignItems: 'center',
    height: 90,
    marginBottom: 30,
    position: 'relative',
    svg: {
      color: 'transparent',
      marginRight: 10,
    },
    '.label': {
      color: '#BAD6C6',
      fontFamily: 'Museo Sans Rounded',
      fontWeight: 600,
      fontSize: 14,
      lineHeight: '18.2px',
      width: 99,
    },
    '&:hover': {
      '.label': {
        color: 'white',
        fontWeight: 700,
      }
      'path': {
        stroke: 'white',
      }
    },
    '&:active, &.active': {
      '.label': {
        fontWeight: 700,
        color: 'white',
      },
      '.bb': {
        borderRadius: 5,
        height: 4,
        width: '100%',
        background: 'white',
        position: 'absolute',
        bottom: 0,
      },
    },
  },
  '.each-fade': {
    img: {
      maxWidth: 614,
      borderRadius: 3.46,
    },
    p: {
      color: 'white',
      fontFamily: 'Museo Sans Rounded',
      fontWeight: 300,
      fontSize: 17,
      lineHeight: '25.5px',
      marginBottom: 22,
    },
  },
  '.slide-arrow': {
    width: 65,
    height: 65,
    background: 'white',
    borderRadius: '50%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    boxShadow: '0px 10px 15px rgba(0, 0, 0, 0.08)',
    position: 'absolute',
    marginBottom: 150,
    cursor: 'pointer',
    '&.left': {
      left: -32.5,
    },
    '&.right': {
      right: -32.5,
    },
    '&.none': {
      display: 'none',
    },
    '&:hover': {
      background: '#0D4B1C',
      boxShadow: '0px 10px 15px rgba(0, 0, 0, 0.08)',
      'svg': {
        fill: 'none',
        path: {
          stroke: 'white'
        }
      }
    },
    '&:active': {
      background: 'linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), #0D4B1C',
      'svg': {
        fill: 'none',
        path: {
          stroke: 'white'
        }
      }
    }
  },

  '.ant-collapse': {
    background: 'transparent',
    border: 0,
    borderTop: '1px solid #689D7F',
    paddingTop: 25,
    '.ant-collapse-header': {
      padding: '0 0 25px !important'
    },
    '.ant-collapse-arrow': {
      position: 'absolute !important',
      left: 'calc(50% - 13px) !important',
      top: 'auto !important',
      bottom: -5,
      fill: 'none',
    },
    '.ant-collapse-item': {
      borderBottom: '0 !important',
    },
    '.header-ellips': {
      height: 40,
      overflow: 'hidden',
      color: 'white',
      fontFamily: 'Museo Sans Rounded',
      fontWeight: 400,
      fontSize: 14,
      lineHeight: '18.9px',
      // background: 'linear-gradient(180deg, rgba(28, 110, 49, 0) 17.76%, #1C6E31 100%), linear-gradient(0deg, #F2F9F3, #F2F9F3)',
    },
    '.ant-collapse-content': {
      // marginTop: -40,
      border: '0 !important',
      background: 'transparent',
      '.ant-collapse-content-box': {
        padding: '0 !important',
        a: {
          margin: '0 auto',
          display: 'flex',
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center'
        },
        p: {
          color: 'white',
          fontFamily: 'Museo Sans Rounded',
          fontWeight: 400,
          fontSize: 14,
          lineHeight: '18.9px',
        }
      }
    }
  },

  '@media (max-width: 480px)': {
    padding: 20,
    margin: 20,
    height: 'auto',
    '.slide-img': {
      width: '100%',
      borderRadius: '5px !important'
    },
    h1: {
      fontSize: 18,
      lineHeight: '23.4px',
    },
    '.each-fade': {
      '.flex': {
        flexDirection: 'column',
        img: {
          marginTop: 15,
        },
      },
    },
    '.tab-col': {
      height: 'auto',
      marginBottom: 0,
      justifyContent: 'center',
      '.label': {
        width: 'auto'
      }
    }
  },
})

export const BlendedImage = styled('div')({
  backgroundColor: '#f2f9f3',
  backgroundSize: 'contain',
  backgroundBlendMode: 'multiply',
  backgroundRepeat: 'no-repeat'
})

export const ImproveSection = styled('div')({
  backgroundColor: 'white',
  paddingTop: '71px !important',
  '.section-bg': {
    right: 0,
    marginRight: -175
  },
  '@media (max-width: 480px)': {
    paddingTop: '35px !important',
    '.section-wrapper': {
      paddingBottom: 300,
    },
    '.section-bg': {
      bottom: 0,
      width: '300px !important',
      marginRight: -20,
    },
  },
})

export const SeamlessSection = styled('div')({
  backgroundColor: 'white',
  h1: {
    marginTop: 32,
  },
  '.section-bg': {
    width: 691,
    right: 0,
    filter: 'drop-shadow(0px 12px 44px rgba(0, 0, 0, 0.08))',
    borderRadius: '0px 15px 15px 0px',
  },
  '@media (max-width: 480px)': {
    '.section-bg': {
      position: 'relative',
    },
    '.ant-col': {
      paddingLeft: '0 !important',
    },
  },
})

export const BuiltSection = styled('div')({
  backgroundColor: 'white',
  h1: {
    marginTop: 36,
  },
  '.section-bg': {
    left: 0,
    '&.gif': {
      width: 295,
      left: 112,
      marginTop: 8
    }
  },
  '@media (max-width: 480px)': {
    '.section-bg': {
      position: 'relative',
      '&.gif': {
        width: '55% !important',
        left: '20.5%',
        marginTop: 5,
        top: 0,
        position: 'absolute',
      }
    },
  },
})

export const LimitlessSection = styled('div')({
  background: `linear-gradient(270deg, #1C6E31 19.47%, rgba(28, 110, 49, 0) 88.45%), url(${GettyImages}), #1C6E31`,
  backgroundBlendMode: 'normal, multiply, normal',
  backgroundSize: '580px 788px',
  backgroundRepeat: 'no-repeat',
  '&.relative': {
    '.section-wrapper': {
      '.section-bg': {
        width: 600,
        marginTop: 194,
      },
      h1: {
        marginTop: 51,
        color: 'white'
      },
      '.description': {
        color: '#BAD6C6',
        borderBottomColor: '#689D7F',
      },
      '.ant-collapse-borderless': {
        background: 'transparent',
        '&> .ant-collapse-item': {
          borderColor: '#689D7F',
        },
      },
      '.ant-collapse-header': {
        color: 'white',
      },
      '.ant-collapse-content-box': {
        p: {
          color: 'white',
        }
      }
    }
  },
  '@media (max-width: 480px)': {
    backgroundImage: 'none',
    '&.relative': {
      '.section-wrapper': {
        '.section-bg': {
          width: '100%',
          marginTop: 20,
          position: 'relative',
        },
        h1: {
          marginTop: 20,
        },
        '.ant-col': {
          paddingLeft: '0 !important',
        },
      },
    },
  },
})

export const IntegratesSection = styled('div')({
  backgroundColor: 'white',
  h1: {
    marginTop: 75,
  },
  '.block': {
    display: 'block'
  },
  '@media (max-width: 480px)': {
    '&.relative': {
      '.section-wrapper': {
        h1: {
          marginTop: 55,
        },
        img: {
          '&.mobile-block': {
            width: 100,
            position: 'absolute',
            right: 0,
            marginTop: -32,
            '&.quickbox': {
              marginTop: -16,
            }
          }
        }
      },
    },
  },
})

export const CloudSection = styled('div')({
  '.section-wrapper': {
    backgroundColor: '#F2F9F3',
    borderRadius: 5,
    padding: 55,
    h1: {
      marginTop: 0,
    },
    '.section-bg': {
      right: 55
    },
    '.description': {
      borderBottom: 'none !important',
    },
    '.cloud-col': {
      width: 280,
      marginRight: 50,
      h3: {
        fontFamily: 'Patua One',
        fontSize: 24,
        fontWeight: 400,
        lineHeight: '31.2px',
        color: '#1C6E31',
        marginLeft: 15,
      },
      '.sub-desc': {
        marginTop: 5,
        fontFamily: 'Museo Sans Rounded',
        fontWeight: 300,
        fontSize: 17,
        lineHeight: '25.5px',
        color: '#4A5355'
      },
    }
  },
  '&.pricing': {
    '.cloud-col': {
      width: '33.3%',
      marginRight: 0,
    }
  },
  '@media (max-width: 480px)': {
    '.section-wrapper': {
      padding: 20,
      '.cloud-col': {
        width: '100%',
        marginBottom: 20,
        h3: {
          fontSize: 18,
          lineHeight: '23.4px',
          height: '23.4px',
          position: 'absolute',
          left: 70,
        },
        img: {
          width: 'auto',
        },
        '.sub-desc': {
          paddingLeft: 65,
        },
        '.v-center': {
          width: 51,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }
      },
      '.mobile-bg': {
        width: '78px !important',
        margin: '0 auto',
      }
    },
    '&.pricing': {
      '.cloud-col': {
        '.v-center': {
          flexDirection: 'column',
          width: '100%',
          img: {
            display: 'block',
          },
          h3: {
            position: 'relative',
            left: 'auto',
          }
        },
        '.sub-desc': {
          paddingLeft: '0 !important',
        },
      }
    }
  },
})
