/**@jsx jsx */
import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteProps } from 'react-router'
import { jsx } from '@emotion/core'
import { Button, Icon } from 'antd'
import { Container, Headline, largeButtonStyle } from './homepage.style'
import { PricingContainer, TableWrap, CellContainer } from './pricing.style'
import Shapes from './images/Shapes.png'
import ipadmacbook from './images/ipadmacbook.png'
import customerSplash from './images/customerSplash.png'
import chasinfoodlogo from './images/chasinfoodlogo.png'
import grandfoodlogo from './images/grandfoodlogo.png'
import farmboxlogo from './images/farmboxlogo.jpg'
import scfnlogo from './images/scfnlogo.jpg'
import { HomePageDispatchProps, HomePageStateProps, HomePageModule } from './homepage.module'
import { GlobalState } from '~/store/reducer'
import { white } from '~/common'

export type LandingComponentProps = HomePageDispatchProps & HomePageStateProps & RouteProps

export class LandingContainer extends React.PureComponent<LandingComponentProps> {
  renderSplash() {
    const { openModal } = this.props

    return (
      <Container
        style={{
          backgroundImage: `url(${Shapes})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: '100% 100%',
          height: '1606px',
        }}
      >
        <PricingContainer>
          <Headline>
            Reimagine your
            <br />
            Wholesale business
          </Headline>
          <br />
          <p style={{ marginTop: '80px', fontSize: '20px', color: 'black' }}>
            Modernize and digitally transform your business FOR the 21st century
          </p>
          <Button onClick={openModal} style={{ marginTop: '40px' }} css={largeButtonStyle}>
            <a href="http://go.wholesaleware.com">Request Demo</a><Icon style={{ marginLeft: '40px' }} type="arrow-right" />
          </Button>

          <TableWrap style={{ marginTop: '345px' }}>
            <CellContainer style={{ width: '50%' }}>
              <img src={ipadmacbook} />
            </CellContainer>
            <CellContainer style={{ width: '50%', textAlign: 'left' }}>
              <div style={{ marginLeft: '10px' }}>
                <Headline style={{ color: white }}>A 360 degree Wholesale Workflow software business suite</Headline>
                <br />
                <p style={{ marginTop: '25px', fontSize: '20px', lineHeight: '143.04%', color: 'white' }}>
                  Developed by Wholesalers, the WholeSaleWare software platform provides a simplistic user experience,
                  allowing all data to reside at your fingertips in a highly secure cloud-based environment
                </p>
                <Button style={{ marginTop: '40px' }} css={largeButtonStyle}>
                  Learn More <Icon style={{ marginLeft: '40px' }} type="arrow-right" />
                </Button>
              </div>
            </CellContainer>
          </TableWrap>
        </PricingContainer>
      </Container>
    )
  }

  renderCustomer() {
    return (
      <Container style={{ justifyContent: 'center', alignItems: 'center', textAlign: 'center', display: 'flex' }}>
        <div>
          <Headline>What Our Customer Say</Headline>
          <div
            style={{
              textAlign: 'left',
              display: 'table',
              padding: '22px 144px 49px 52px',
              marginTop: '64px',
              backgroundImage: `url(${customerSplash})`,
              backgroundRepeat: 'no-repeat',
              backgroundSize: '100% 100%',
              width: '700px',
              height: '500px',
            }}
          >
            <div style={{ display: 'table-row' }}>
              <span style={{ color: 'white', fontSize: '16px' }}>Grand Food, Inc.</span>
            </div>
            <div
              style={{
                lineHeight: '150%',
                display: 'table-row',
                verticalAlign: 'bottom',
                color: 'black',
                fontSize: '18px',
                height: '30%',
              }}
            >
              Wholesaleware has allowed our operation to grow in revenue by 120% in a few short years.  Our front and
              back office are able to communicate much better,  our sales people have product and stock information at
              their fingertips, and our purchasers have intelligent stock and velocity reports at their
              fingertips.  With Wholesaleware, we were confident enough to physically expand our distribution center
              square footage by 350%  while simultaneously carrying over 1000 more product SKUs that we can pick and
              deliver same-day within a matter of hours.  That’s given us a valuable edge in our highly competitive
              market space and keeps our customers satisfied.
            </div>
          </div>
        </div>
      </Container>
    )
  }

  renderUser() {
    return (
      <Container
        style={{ justifyContent: 'center', alignItems: 'center', textAlign: 'center', display: 'flex', width: '100%' }}
      >
        <div style={{ width: '100%' }}>
          <Headline>Our Users</Headline>
          <TableWrap style={{ marginTop: '20px', width: '100%' }}>
            <CellContainer style={{ width: '25%' }}>
              <img src={chasinfoodlogo} width={'200'} />
            </CellContainer>
            <CellContainer style={{ width: '25%' }}>
              <img src={grandfoodlogo} width={'300'} />
            </CellContainer>
            <CellContainer style={{ width: '25%' }}>
              <img src={farmboxlogo} width={'150'} height={'150'} />
            </CellContainer>
            <CellContainer style={{ width: '25%' }}>
              <img src={scfnlogo} />
            </CellContainer>
          </TableWrap>
        </div>
      </Container>
    )
  }

  render() {
    return (
      <div>
        {this.renderSplash()}
        {this.renderCustomer()}
        {this.renderUser()}
      </div>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.homepage

export const LandingComponent = connect(HomePageModule)(mapStateToProps)(LandingContainer)
