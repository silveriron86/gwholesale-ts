/**@jsx jsx */
import React from 'react'

import { jsx } from '@emotion/core'
import { Button, Collapse, Icon, Row, Col } from 'antd'
import { PlanSection, ProductServiceSection, SupportSection, FSQSection, MostPopular } from './pricing.style'

import { HomePageWrapper, SplashSection, CloudSection } from './homepage.style'

import { Header } from '~/components'

import { Footer, RequestDemo } from './components/footer.style'
import { homeButtonStyle, LoginWrap, navLinkHomeCss } from '~/components/header/header.style'
import { FooterContainer } from './components/footer.component'
import { Flex } from '~/modules/customers/customers.style'
import { Icon as IconSvg } from '~/components'

import ReactGA from 'react-ga'
import { NavLink } from 'react-router-dom'
const Panel = Collapse.Panel

const icon1 = require('./images/download-cloud.png')
const icon2 = require('./images/award.png')
const icon3 = require('./images/lock.png')
const supportBG = require('./images/Depositphotos_86041836_xl-2015.jpg')

const supportIcon1 = require('./images/clipboard.png')
const supportIcon2 = require('./images/layout.png')
const supportIcon3 = require('./images/go_live.png')
const supportIcon4 = require('./images/cong.png')
const appleIcon = require('./images/apple.png')
const appleWhiteIcon = require('./images/apple_white.png')
const proteinIcon = require('./images/protein.png')
const proteinGreenIcon = require('./images/protein_green.png')
const checkIcon = require('./images/check.png')
const mostPopularIcon = require('./images/most_popular.png')

export class PricingComponent extends React.PureComponent {
  state = {
    mobileVersion: false,
    subscription: 1,
  }

  componentDidMount() {
    ReactGA.initialize('UA-143951733-1')
    ReactGA.pageview('/home/pricing')
    window.addEventListener('resize', this.resize.bind(this))
    this.resize()
  }

  resize() {
    this.setState({ mobileVersion: window.innerWidth <= 760 })
  }

  setSubscription = (subscription: number) => {
    this.setState({
      subscription,
    })
  }

  renderSplash = () => {
    const { mobileVersion, subscription } = this.state
    return (
      <SplashSection className="product-pricing pricing">
        <Header
          isMobile={mobileVersion}
          hide={false}
          // tabConfig={headerConfig as any}
          // logo={props.logo}
          location={location}
          currentUser={null}
        />
        <div className="title">Packages</div>
        <h1>Meeting the unique needs of any sized wholesale operation</h1>
        <Flex className="h-center service-btns">
          <Button className={`service ${subscription === 1 ? '' : 'active'}`} onClick={() => this.setSubscription(1)}>
            <img src={subscription === 1 ? appleIcon : appleWhiteIcon} width={21} />
            Produce Subscriptions
          </Button>
          <Button className={`service ${subscription === 2 ? '' : 'active'}`} onClick={() => this.setSubscription(2)}>
            <img src={subscription === 2 ? proteinGreenIcon : proteinIcon} width={37} />
            Protein Subscriptions
          </Button>
        </Flex>
      </SplashSection>
    )
  }

  renderPlans() {
    const { subscription } = this.state
    const items1 = ['Selling & purchasing', 'Inventory', 'Delivery routing', 'Phone & email support']
    const items2 = [
      'Selling & purchasing',
      'Inventory',
      'Delivery routing',
      'Work orders',
      'E-commerce',
      'Phone & email support',
    ]
    const items3 = [
      'Selling & purchasing',
      'Inventory',
      'Delivery routing',
      'Work orders',
      'E-commerce',
      'Warehouse',
      'HR',
      'Phone & email support',
    ]
    const icon = subscription === 1 ? appleIcon : proteinGreenIcon
    return (
      <PlanSection className="relative">
        <div className={`section-wrapper subscription${subscription}`}>
          <Row gutter={40}>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <Flex className="icons v-center h-center">
                  <img src={icon} />
                </Flex>
                <h1 className="text-center">Standard Subscription</h1>
                <div className="description no-border">
                  Ideal for clear inventory management, strategic sales and purchases, and efficient deliveries, our
                  Standard Subscription is the perfect starting point for food wholesale operations of any size.
                </div>
                <Flex className="h-center">
                  <LoginWrap className="pricing">
                    <NavLink to="/pricing">Get Started</NavLink>
                  </LoginWrap>
                </Flex>
                {items1.map((item, index) => {
                  return (
                    <Flex className="list-item v-center" key={`standard-${index}`}>
                      <img src={checkIcon} />
                      <div>{item}</div>
                    </Flex>
                  )
                })}
              </div>
            </Col>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <MostPopular>
                  <img src={mostPopularIcon} />
                </MostPopular>
                <Flex className="icons v-center h-center">
                  <img src={icon} />
                  <div className="img-pad" />
                  <img src={icon} />
                </Flex>
                <h1 className="text-center">Premium Subscription</h1>
                <div className="description no-border">
                  Our Premium Subscription includes our entire standard package along with profit-boosting eCommerce and
                  holistic repacking support.
                </div>
                <Flex className="h-center">
                  <LoginWrap className="pricing">
                    <NavLink to="/pricing">Get Started</NavLink>
                  </LoginWrap>
                </Flex>
                {items2.map((item, index) => {
                  return (
                    <Flex className="list-item v-center" key={`standard-${index}`}>
                      <img src={checkIcon} />
                      <div>{item}</div>
                    </Flex>
                  )
                })}
              </div>
            </Col>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <Flex className="icons v-center h-center">
                  <img src={icon} />
                  <div className="img-pad" />
                  <img src={icon} />
                  <div className="img-pad" />
                  <img src={icon} />
                </Flex>
                <h1 className="text-center">Platinum Subscription</h1>
                <div className="description no-border">
                  Our Platinum Subscription is an all-in-one solution that streamlines even the most complex food
                  wholesale operations, providing end-to-end support for our customers.{' '}
                </div>
                <Flex className="h-center">
                  <LoginWrap className="pricing">
                    <NavLink to="/pricing">Get Started</NavLink>
                  </LoginWrap>
                </Flex>
                {items3.map((item, index) => {
                  return (
                    <Flex className="list-item v-center" key={`standard-${index}`}>
                      <img src={checkIcon} />
                      <div>{item}</div>
                    </Flex>
                  )
                })}
              </div>
            </Col>
          </Row>

          <h1 className="text-center interested">Interested in a custom solution?</h1>
          <div className="text-center interested description no-border">
            We understand firsthand that no two wholesalers are exactly alike. That’s why we invite you to reach out for
            custom wholesale inquiries and subscription plans.
          </div>
          <Flex className="h-center">
            <LoginWrap className="pricing">
              <a href="https://share.hsforms.com/1zGjYvlFmT5e475T3I4GH5Acfaea">Contact Us</a>
            </LoginWrap>
          </Flex>
        </div>
      </PlanSection>
    )
  }

  renderServiceTable() {
    return (
      <ProductServiceSection className="relative">
        <div className="section-wrapper">
          <h1 className="text-center">Countless features all in one place</h1>
          <div className="description no-border text-center">
            Without the right tools and systems in place, managing a wholesale operation can quickly become unwieldy. That’s why our platform features everything you need all under one roof and is scalable for any size operation, both big and small.
          </div>
          <Row gutter={50}>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <h3>Mobile App</h3>
                <div className="sub-desc">
                  Whether you’re on a coffee break or on the move, our mobile app keeps you in the know while enabling
                  you to communicate with customers, facilitate deliveries, take orders, and more.
                </div>
              </div>
            </Col>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <h3>eCommerce Module</h3>
                <div className="sub-desc">
                  Our user-friendly eCommerce tool allows your food wholesale customers to order in a single click. From
                  there, you can monitor the status of each order directly within the platform.
                </div>
              </div>
            </Col>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <h3>Barcode Scanning</h3>
                <div className="sub-desc">
                  Encode barcodes with important data, such as weights, lots, SKUs, and more. There’s never been an
                  easier and more accurate way to keep track of your inventory.
                </div>
              </div>
            </Col>
          </Row>
          <Row gutter={50} style={{ marginTop: 50 }}>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <h3>Manufacturing & Repacking</h3>
                <div className="sub-desc">
                  Process, package, grade, slice, and chop products while maintaining holistic oversight of your
                  inventory, costs, yield, resources, and beyond.
                </div>
              </div>
            </Col>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <h3>HR Module</h3>
                <div className="sub-desc">
                  Stay in-the-know when it comes to employee wages, important documents, bonuses, paid overtime (PTO),
                  holidays, and more with our complimentary HR module.
                </div>
              </div>
            </Col>
            <Col lg={8} md={24}>
              <div className="col-wrapper">
                <h3>Warehouse Module</h3>
                <div className="sub-desc">
                  Pinpoint the exact location for your pallets and inventory, helping pickers locate and process orders
                  with greater efficiency.
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </ProductServiceSection>
    )
  }

  renderSupportSection = () => {
    return (
      <SupportSection className="relative">
        <div className="section-wrapper">
          <Row>
            <Col lg={9} md={24}>
              <h1>Get started without interrupting your workflow</h1>
              <div className="description no-border">
                Onboarding processes are oftentimes associated with service lags, bottlenecks, and lost profits. Not
                with WholesaleWare. Between our dedicated support staff and intuitive platform, you’ll complete the
                onboarding process while maintaining a consistent workflow.
              </div>
              <img src={supportBG} className="section-bg" />
            </Col>
            <Col lg={15} md={24} style={{ paddingLeft: 75 }}>
              <h3 style={{ marginBottom: 20 }}>Get started with WholesaleWare in record time</h3>

              <Flex className="v-center">
                <div className="img-wrapper">
                  <img src={supportIcon1} />
                </div>
                <div>
                  <h4>Operational Review</h4>
                </div>
              </Flex>
              <div className="sub-desc has-border pl">
                Our first step is getting to know your unique products, workflow, and needs as a wholesaler. During our
                operational review, we assess your operations, staff roles, and existing practices.
              </div>

              <Flex className="v-center">
                <div className="img-wrapper">
                  <img src={supportIcon2} />
                </div>
                <div>
                  <h4>Data Migration & Configuration</h4>
                </div>
              </Flex>
              <div className="sub-desc has-border pl">
                Once we have a thorough understanding of your systems and processes, we import data from your existing
                platforms, like QuickBooks, onto WholesaleWare while adjusting the settings on WholesaleWare to your
                unique needs.
              </div>

              <Flex className="v-center">
                <div className="img-wrapper">
                  <img src={supportIcon3} />
                </div>
                <div>
                  <h4>Training</h4>
                </div>
              </Flex>
              <div className="sub-desc has-border pl">
                We believe that the best food wholesale platform should come with the best training. During the
                onboarding process, we ensure that teams are comfortable enough with WholesaleWare to use the platform
                to its full potential
              </div>

              <Flex className="v-center">
                <div className="img-wrapper">
                  <img src={supportIcon4} />
                </div>
                <div>
                  <h4>Go live! </h4>
                </div>
              </Flex>
            </Col>
          </Row>
          <div className="mobile-bg hide mobile-block">
            <img src={supportBG} />
          </div>
        </div>
      </SupportSection>
    )
  }

  renderCloudSection = () => {
    return (
      <CloudSection className="relative pricing">
        <div className="section-wrapper">
          <Row>
            <Col lg={24} md={24}>
              <h1 className="text-center">Trusted for our uncompromising data security</h1>
              <div className="description text-center">
                At WholesaleWare, keeping your information secure is our top priority. That’s why we use strict security
                protocols to protect your business, customers, and employees.
              </div>
            </Col>
          </Row>
          <Flex className="mobile-block">
            <div className="cloud-col">
              <Flex className="v-center">
                <img src={icon1} />
                <h3>Automatic Backups</h3>
              </Flex>
              <div className="sub-desc">
                Our cloud features numerous secure servers, ensuring that you have uninterrupted data access.
              </div>
            </div>
            <div className="cloud-col">
              <Flex className="v-center">
                <img src={icon2} />
                <h3>Data Security</h3>
              </Flex>
              <div className="sub-desc">
                We use high-level encryption and SSL security protocols from AWS to keep your data safe.
              </div>
            </div>
            <div className="cloud-col">
              <Flex className="v-center">
                <img src={icon3} style={{ width: 51, height: 42 }} />
                <h3>Information Protection</h3>
              </Flex>
              <div className="sub-desc">
                We will never view, share, or use your data unless you give us consent for troubleshooting only.
              </div>
            </div>
          </Flex>
        </div>
      </CloudSection>
    )
  }

  renderCollapse = (data, prefix) => {
    return (
      <Collapse
        bordered={false}
        defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <Icon type={isActive ? 'minus' : 'plus'} />}
      >
        {data.map((row, index) => (
          <Panel header={row.title} key={`${prefix}-${index}`}>
            <p>{row.text}</p>
          </Panel>
        ))}
      </Collapse>
    )
  }

  renderFAQ() {
    const data = [
      {
        title: 'Is WholesaleWare right for my business?',
        text:
          'We designed WholesaleWare with food wholesalers and distributors in mind, offering data-driven tools and features that increase profits and generate results for any produce or protein wholesaler.',
      },
      {
        title: 'How much does WholesaleWare cost?',
        text:
          'Pricing varies depending on your needs. We invite you to reach out to discuss pricing or to try a complimentary demo.',
      },
      {
        title: 'Can I customize my subscription plan?',
        text:
          'Yes. WholesaleWare is a modular platform that can be customized based on your unique specifications and needs.',
      },
      {
        title: 'How long does WholesaleWare take to implement?',
        text:
          'Onboarding timelines vary based on the complexity of your wholesale operations and the size of your team. The average implementation takes about 6-8 weeks, but some customers complete implementation much more quickly, while others take longer.',
      },
      {
        title: 'How does WholesaleWare keep my data safe?',
        text:
          'WholesaleWare uses cloud-based AWS technology, advanced SSL protocols, and high-level encryption to keep your information protected. We respect your privacy and uphold the strictest security measures to preserve the data integrity of long-standing and new clients alike. ',
      },
      {
        title: 'Can I switch from an existing wholesale platform to WholesaleWare?',
        text:
          'Yes. We routinely work with customers to export data from previously used platforms while implementing the proper configurations to support your desired workflow.',
      },
      {
        title: 'What internet and hardware requirements exist for WholesaleWare users?',
        text:
          'We require a PC or Mac computer with minimum of 8MB RAM and Intel 10th generation dual-core i3 processor. For internet, we require a consistent 10 Mbps minimum download speed and, for non-wired connections, a strong wireless signal.',
      },
      {
        title: 'Is QuickBooks difficult to set up and implement?',
        text:
          'QuickBooks is very easy for most customers to set up, implement, and navigate. Our support team is always available for any questions that you may have.',
      },
      {
        title: 'Are future WholesaleWare upgrades included in my subscription?',
        text:
          'Yes. We are constantly making improvements to our platform. In turn, you will automatically receive each upgrade at no additional cost.',
      },
      {
        title: 'Do you offer customer support?',
        text:
          'Yes. Our dedicated customer support team is available via email, phone, and video conference to address any issues that may arise. ',
      },
    ]
    return (
      <FSQSection className="relative">
        <div className="section-wrapper">
          <h1 className="text-center">FAQs</h1>
          <div className="description empty" />
          {this.renderCollapse(data, 'improve')}
        </div>
      </FSQSection>
    )
  }

  renderDemoSection = () => {
    return (
      <Footer className="product-demo">
        <RequestDemo className="no-border">
          <div className="demo-wrapper">
            <p>
              Whether you’d like to get started or have any further questions about WholesaleWare, our knowledgeable
              account management team is here to help. We invite you to reach out today.
            </p>
            <Flex className="h-center">
              <Button
                // onClick={openModal}
                css={homeButtonStyle}
                className="landing"
              >
                <a href="https://share.hsforms.com/1zGjYvlFmT5e475T3I4GH5Acfaea" className="product">
                  Contact Us
                  <IconSvg
                    viewBox="0 0 27 16"
                    width="27"
                    height="16"
                    type="arrow-right-home"
                    style={{ marginLeft: 20 }}
                  />
                </a>
              </Button>
            </Flex>
          </div>
        </RequestDemo>
      </Footer>
    )
  }

  render() {
    return (
      <HomePageWrapper className="pricing">
        <div style={{ backgroundColor: '#1C6E31', paddingTop: 197 }}>{this.renderSplash()}</div>
        {this.renderPlans()}
        {this.renderServiceTable()}
        {this.renderSupportSection()}
        {this.renderCloudSection()}
        {this.renderFAQ()}
        {this.renderDemoSection()}
        <FooterContainer />
      </HomePageWrapper>
    )
  }
}
