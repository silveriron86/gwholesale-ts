import styled from '@emotion/styled'
import css from '@emotion/css'

import { Headline } from './homepage.style'
import Stroke from './images/Stroke.svg'

export const PlanSection = styled('div')({
  paddingBottom: '390px ​!important',
  marginTop: '-350px !important',
  '.text-center': {
    textAlign: 'center !important',
  },
  h1: {
    marginTop: 15,
    '&.interested': {
      marginTop: 90,
    }
  },
  '.description': {
    height: 165,
    paddingBottom: '15px !important',
    '&.interested': {
      maxWidth: 903,
      margin: '0 auto',
      height: 'auto',
    }
  },
  '.col-wrapper': {
    position: 'relative',
    overflow: 'hidden',
    backgroundColor: 'white',
    height: 895,
    boxShadow: '0px 10px 77px rgba(0, 0, 0, 0.1)',
    borderRadius: 5,
    padding: 30,
    '.img-pad': {
      marginRight: 15,
    },
    '.list-item': {
      fontFamily: 'Museo Sans Rounded',
      fontSize: 14,
      fontWeight: 'normal',
      lineHeight: '130%',
      color: '#4A5355',
      borderTop: '1px solid #BAD6C6',
      height: 54,
      img: {
        width: 24,
        height: 24,
        marginRight: 15,
      }
    }
  },
  '.icons': {
    height: 35,
  },
  '.subscription1': {
    '.icons': {
      img: {
        width: 29.7,
        height: 35,
      }
    }
  },
  '.subscription2': {
    '.icons': {
      img: {
        width: 37.4,
        height: 25,
      }
    }
  },
  '@media (max-width: 480px)': {
    marginTop: '-1629px !important',
    padingBottom: '20px !important',
    '.ant-col': {
      marginBottom: 20,
      '.col-wrapper': {
        height: 'auto',
        '.description': {
          height: 'auto',
          paddingBottom: 10,
        }
      }
    },
    h1: {
      '&.interested': {
        marginTop: 55,
      },
    },
  },
})

export const PricingContainer = styled('div')({
  width: '100%',
  padding: '50px 81px 98px 131px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const SupportTableSection = styled('div')({
  backgroundColor: 'rgba(82, 158, 99, 0.16)',

  width: '100%',
  marginTop: '160px',
  marginBottom: '100px',
  padding: '100px 12% 100px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',

  '@media (max-width: 480px)': {
    marginTop: '40px',
    marginBottom: '40px',
    padding: '20px 0px 20px',
  },
})

export const SupportHeadline = styled(Headline)({
  '@media (max-width: 480px)': {
    fontSize: '30px',
    paddingLeft: '8%',
    paddingRight: '8%',
  },
})

export const SupportText = styled('p')({
  marginTop: '20px',
  fontSize: '24px',
  color: 'black',
  lineHeight: '143.04%',
  letterSpacing: '0.05em',

  '@media (max-width: 480px)': {
    fontSize: '16px',
    paddingLeft: '8%',
    paddingRight: '8%',
  },
})

export const ServicesSpan = styled('span')({
  fontSize: '22px',
  lineHeight: '125%',
  letterSpacing: '0.05em',
  fontWeight: 700,

  '@media (max-width: 768px)': {
    fontSize: '16px',
  },

  '@media (max-width: 480px)': {
    fontSize: '12px',
  },
})

export const PhoneHours = styled('p')({
  fontSize: '14px',
  marginTop: '5px',

  '@media (max-width: 480px)': {
    fontSize: '10px',
    marginTop: '4px',
  },
})

export const SupportTable = styled('div')({
  marginTop: '49px',

  '@media (max-width: 480px)': {
    marginTop: '20px',
    paddingLeft: '2%',
    paddingRight: '2%',
  },
})

export const TableWrap = styled('div')({
  display: 'table',
  width: '100%',
  borderSpacing: '12px',
  marginTop: '-12px',

  '@media (max-width: 480px)': {
    borderSpacing: '6px',
    marginTop: '-6px',
  },
})

export const CellContainer = styled('div')({
  display: 'table-cell',
  color: 'black',
  height: '162px',
  textAlign: 'center',
  verticalAlign: 'middle',

  '@media (max-width: 480px)': {
    height: '90px',
  },
})

export const LeftCell = styled(CellContainer)({
  width: '28%',
  textAlign: 'left',

  '@media (max-width: 480px)': {
    width: '33%',
  },
})

export const HeaderCell = styled(CellContainer)({
  width: '23%',
  backgroundColor: 'rgba(255,255,255,0.6)',

  '@media (max-width: 480px)': {
    width: '20%',
  },
})

export const HeaderCellTitle = styled('div')({
  fontSize: '24px',
  lineHeight: '143.04%',
  fontWeight: 700,

  '@media (max-width: 768px)': {
    fontSize: '18px',
  },

  '@media (max-width: 480px)': {
    fontSize: '12px',
  },
})

export const HeaderCellText = styled('div')({
  fontSize: '14px',
  lineHeight: '17px',
  marginTop: '10px',

  '@media (max-width: 480px)': {
    fontSize: '12px',
  },
})

export const NormalCell = styled(CellContainer)({
  width: '23%',
  backgroundColor: 'white',

  '@media (max-width: 480px)': {
    width: '20%',
  },
})

export const CheckMark = styled(Stroke)({
  width: '42px',
  height: '36px',
})

export const FAQSection = styled('div')({
  width: '100%',
  marginTop: '160px',
  marginBottom: '100px',
  paddingLeft: '12%',
  paddingRight: '12%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',

  '@media (max-width: 480px)': {
    marginTop: '20px',
    paddingLeft: '8%',
    paddingRight: '8%',
  },
})

export const FAQHeadline = styled(Headline)({
  '@media (max-width: 480px)': {
    fontSize: '30px',
  },
})

export const FAQText = styled('p')({
  marginTop: '20px',
  marginBottom: '50px',
  fontSize: '20px',
  color: 'black',
  lineHeight: '29px',
  letterSpacing: '0.05em',

  '@media (max-width: 480px)': {
    marginBottom: '20px',
    fontSize: '16px',
  },
})
export const collapseStyle = css({
  fontSize: '20px',
  letterSpacing: '0.05em',
  lineHeight: '0',
  fontWeight: 400,
  marginBottom: '20px',
  border: '0',

  '@media (max-width: 480px)': {
    fontSize: '18px',
  },
})

export const CollapseText = styled('p')({
  fontSize: '16px',
  lineHeight: '23px',
  letterSpacing: '0.05em',
  padding: '25px 25px 25px 25px',
})

export const ContactUsText = styled('span')({
  marginTop: '20px',
  color: 'black',
  fontSize: '20px',
  lineHeight: '29px',
  letterSpacing: '0.05em',

  '@media (max-width: 480px)': {
    fontSize: '16px',
  },
})

export const ProductServiceSection = styled('div')({
  paddingTop: '50px !important',
  backgroundColor: '#F2F9F3',
  '.col-wrapper': {
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 35,
    minHeight: 261,
  },
  '@media (max-width: 480px)': {
    '.ant-row': {
      marginTop: '0 !important',
    },
    '.ant-col': {
      marginBottom: 20,
      '.col-wrapper': {
        padding: 20,
        minHeight: '0 !important',
        h3: {
          fontSize: 18,
          lineHeight: '23.4px',
          fontWeight: 400,
        },
        '.sub-desc': {
          fontWeight: 400,
          fontSize: 14,
          lineHeight: '21px',
        }
      }
    }
  }
})

export const SupportSection = styled('div')({
  marginTop: 90,
  '.img-wrapper': {
    width: 66,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginRight: 6,
  },
  '.pl': {
    paddingLeft: 72,
  },
  '.section-bg': {
    width: 586,
    marginTop: -34,
    marginLeft: -126,
  },
  '@media (max-width: 480px)': {
    marginTop: 25,
    '.ant-col': {
      paddingLeft: '0 !important',
      '.v-center': {
        alignItems: 'flex-start !important',
        justifyContent: 'flex-start !important',
      },
      img: {
        maxWidth: 44,
        maxHeight: 50,
      }
    },
    '.mobile-block': {
      position: 'relative',
      height: 152,
      img: {
        position: 'absolute',
        right: 0,
        width: 389,
        marginRight: -107,
      }
    }
  }
})

export const FSQSection = styled('div')({
  '&.relative': {
    '.section-wrapper': {
      '.ant-collapse-header': {
        color: '#4A5355',
        i: {
          color: '#1C6E31',
        }
      },
      '.ant-collapse-content-box': {
        paddingLeft: 39
      },
      '.ant-collapse-item-active': {
        '.ant-collapse-header': {
          color: '#1C6E31',
        }
      },
      '@media (max-width: 480px)': {
        h1: {
          textAlign: 'left !important',
        },
        '.ant-collapse-header': {
          fontSize: 16,
          lineHeight: '20.8px',
        },
        '.ant-collapse-content-box': {
          paddingTop: 5,
          paddingBottom: 25,
          p: {
            fontWeight: 300,
            fontSize: 14,
            lineHeight: '21px',
            marginBottom: 0,
          }
        }
      }
    }
  }
})

export const MostPopular = styled('div')({
  position: 'absolute',
  left: -1,
  top: -1,
})
