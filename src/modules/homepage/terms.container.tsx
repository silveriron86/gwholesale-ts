/**@jsx jsx */
import React from 'react'

import { jsx } from '@emotion/core'
import { Collapse } from 'antd'
import { TermsPageContainer, TermsContainer, TermsContent, TermsTitle } from './terms.style'
import { FooterContainer } from './components/footer.component'
import ReactGA from 'react-ga'
import jQuery from 'jquery'
export class TermsComponent extends React.PureComponent {
  componentDidMount() {
    ReactGA.initialize('UA-143951733-1')
    ReactGA.pageview('/home/terms')
    jQuery('html, body').scrollTop(0);
  }

  render() {
    return (
      <TermsPageContainer>
        <TermsContainer>
          <TermsTitle>Terms of Service</TermsTitle>
          <TermsContent>
            <h3 className="mt0">GRUBMARKET PLATFORM AGREEMENT</h3>
            <p>
              This GrubMarket Platform Agreement (this “Agreement”) is made and entered into between GrubMarket, Inc.
              (“GrubMarket”) and Customer (defined below). This Agreement sets forth the terms pursuant to which
              Customer will receive access to use certain Subscription Services (defined below), including GrubMarket’s
              wholesale operations management platform (the “Platform”) and receive certain Professional Services
              (defined below). Each of GrubMarket and Customer is a “Party” and together, the “Parties.”
            </p>
            <p>
              BY ACCEPTING THIS AGREEMENT, EITHER BY CLICKING A BOX INDICATING YOUR ACCEPTANCE, EXECUTING AN ORDER FORM
              OR OTHER DOCUMENT THAT REFERENCES THIS AGREEMENT, USING (OR MAKING ANY PAYMENT FOR) ANY PRODUCTS (DEFINED
              BELOW), ENGAGING GRUBMARKET TO PROVIDE PROFESSIONAL SERVICES, OR OTHERWISE AFFIRMATIVELY INDICATING YOUR
              ACCEPTANCE OF THIS AGREEMENT, YOU: (A) AGREE TO THIS AGREEMENT ON BEHALF OF YOUR ORGANIZATION, [SHOULD BE
              NAME OF CLIENT], OR OTHER LEGAL ENTITY FOR WHICH YOU ACT (“Customer”); AND (B) REPRESENT THAT YOU HAVE THE
              AUTHORITY TO BIND CUSTOMER AND ITS AFFILIATES (DEFINED BELOW) TO THIS AGREEMENT. IF YOU DO NOT HAVE SUCH
              AUTHORITY, OR IF YOU DO NOT AGREE WITH THIS AGREEMENT, YOU MUST NOT ACCEPT THIS AGREEMENT AND MAY NOT USE
              ANY PRODUCT OR RECEIVE ANY PROFESSIONAL SERVICES.
            </p>
            <h3>1. STRUCTURE</h3>
            <p>
              1. Products and Services; Orders. This Agreement sets forth the terms and conditions on which GrubMarket
              may make available to Customer the Platform, one or more APIs (defined below), Subscription Services
              (defined below), and Professional Services (defined below), to the extent applicable and as expressly
              identified (a) as part of the online Platform subscription process completed by Customer on GrubMarket’s
              website (a “Website Order”) or (b) in a quote, order form, statement of work, or other ordering document
              signed by the Parties that expressly references and incorporates these Master Terms (defined below) (each,
              an “Order Form”). Collectively, the Platform, APIs, and software as a service are the “Subscription
              Service.” Each order for any Product(s) made through a Website Order or an Order Form is an “Order.” Each
              Order is subject to the terms of, and is deemed incorporated into, this Agreement.
            </p>
            <p>
              2. Subscription Term. Unless earlier terminated in accordance with this Agreement, the term of this
              Agreement with respect to the applicable Order will continue for the initial term specified in the
              applicable Order (“Initial Term”) and continually and automatically renew thereafter, unless either Party
              provides the other with notice of non-renewal prior to the date of expiration of the then-current term.
              The renewal term of the Order (each, a “Renewal Term”) shall be equal in length to the Initial Term or
              expiring Renewal Term (the Initial Term and each Renewal Term, if any, collectively, the “Order Term” of
              such Order). Notice of non-renewal of an Order must be provided by a Party at least 60 days prior to the
              end of the Initial Term or then-current Renewal Term, as applicable (or such other period as expressly
              specified on the applicable Order). Renewal of any Order may be conditioned on and/or subject to
              Customer’s agreement to changes to these Master Terms. Termination of this Agreement will terminate all
              Order Terms then in effect unless otherwise specified on the applicable Order. Orders that are solely for
              Professional Services will remain in effect until the Professional Services are completed (unless earlier
              terminated in accordance with this Agreement or the applicable Order) and will not automatically renew,
              nor will any Professional Services included in any other Order automatically renew for a Renewal Term
              unless expressly specified on the applicable Order. Customer acknowledges and agrees that its access to
              Products (or certain features thereof) may be automatically disabled upon expiration of the applicable
              Order Term.
            </p>
            <p>
              3. Conflict of Terms. Any conflict between the terms in an Order versus Sections 1–10 of this Agreement
              (the “Master Terms”) will be resolved in favor of the applicable Order.
            </p>
            <h3>2. PRODUCT RIGHTS AND RESTRICTIONS</h3>
            <p>1. Access Rights</p>
            <p>
              1. Subscription Services. Customer may access and use the Platform (i) for the term set forth in the
              applicable Order, and (ii) in accordance with all applicable Documentation (defined below) and the
              restrictions set forth in this Agreement (including the applicable Order). 2. API. If an Order indicates
              that Customer will receive access to an application programming interface, or if GrubMarket provides
              credentials to Customer that enable Customer to access a GrubMarket-provided application programming
              interface in connection with another Product (each, an “API”), then GrubMarket hereby grants Customer a
              non-exclusive, non-transferable right under GrubMarket’s rights in that API to access such API solely (i)
              for the Order Term, and (ii) in accordance with all applicable Documentation and the restrictions set
              forth in this Agreement (including the applicable Order). Without limiting the foregoing, Customer will
              comply with any volume or other usage-based restrictions described in an Order or the Documentation.
            </p>
            <p>
              2. Additional Features. Customer acknowledges that not all of the features or functionality of a Product
              may be available at Customer’s subscription level irrespective of whether such feature or functionality is
              described in the Documentation, and that access to such features or functionality may require payment of
              additional fees or the purchase of additional access. The availability of a feature or function of a
              Product may be removed or added at GrubMarket’s sole discretion.
            </p>
            <p>
              3. Evaluation Products. From time to time, GrubMarket may make available to Customer certain Products or
              Product functionality to try at Customer’s option at no additional charge, which will be designated as
              being on an evaluation, pilot, trial, beta, non-production, limited release, preview, or similar basis
              (such Products or functionality, “Eval Products”) for the period specified by GrubMarket (the “Eval
              Period”). Additional trial terms and conditions may appear on the registration page for the applicable
              Eval Product. Any such additional terms and conditions are incorporated into this Agreement by reference
              and are legally binding. If GrubMarket provides Customer with access to an Eval Product, then, subject to
              Section 7.4, Customer may use the Product only for the purpose of evaluating the functions and performance
              of the Eval Product, solely during the Eval Period, and subject to any additional usage restrictions
              specified on the applicable Order. Customer acknowledges that Eval Products may be automatically disabled
              upon expiration of the Eval Period (at the end of which Customer’s right to use the Product under the
              applicable Order also expires), and that any data stored in such Products may become unavailable at that
              time.
            </p>
            <p>
              4. Use Restrictions. Except as otherwise explicitly provided in this Agreement or as may be expressly
              permitted by applicable law, Customer will not, and will not permit or authorize third parties to: (a)
              rent, lease, or otherwise permit third parties (or other persons not authorized by this Agreement) to use
              a Product or the Documentation; (b) use a Product to provide services to third parties (e.g., as a service
              bureau); (c) use a Product for any benchmarking activity or in connection with the development of a
              competitive product; (d) circumvent or disable any security or other technological features or measures of
              a Product or use a Product in a manner that GrubMarket reasonably believes poses a threat to the security
              of GrubMarket-controlled computer systems; (e) modify, translate, reverse engineer, decompile,
              disassemble, or otherwise derive the source code or the underlying ideas, algorithms, structure, or
              organization from a Product (except to the extent that applicable law prevents the prohibition of such
              activities); or (f) use or access any Product in a manner that materially impacts or burdens GrubMarket or
              GrubMarket’s servers and other computer systems, or that interferes with GrubMarket’s ability to make
              available any Product to any third party.
            </p>
            <p>
              5. Documentation. To the extent that a Product is accompanied by any GrubMarket-provided user manuals,
              help files, specification sheets, or other documentation, in whatever form, relating to a Product
              (“Documentation”), GrubMarket hereby grants to Customer a non-exclusive, non-transferable right under
              GrubMarket’s rights in the Documentation to use such Documentation solely to enable Customer to exercise
              its rights under the applicable grant of access and usage rights for such Product set forth in Section
              2.1.
            </p>
            <p>
              6. Third-Party Products. To the extent that a Product includes or is accompanied by third-party software
              or other products (e.g., cloud hosting instances or data analysis tools) that GrubMarket provides to
              Customer or that is otherwise identified in the Documentation as being required to properly use such
              Product (“Third-Party Products”), the Third-Party Products and their use by Customer are subject to all
              access and other terms that accompany such Third-Party Products. Customer will abide by and comply with
              all such terms. Without limiting the foregoing, if GrubMarket enables Customer to access a hosted
              environment offered by a third-party cloud or platform service provider, then Customer must agree to the
              applicable service provider’s terms and conditions prior to accessing such hosted environment, and
              Customer will comply at all times with such terms and conditions.
            </p>
            <p>
              7. Compliance with Laws. Customer will use the Products and Documentation in compliance with all
              applicable laws and regulations.
            </p>
            <p>
              8. Protection against Unauthorized Use. Customer will prevent any unauthorized use of the Products and
              Documentation and will immediately notify GrubMarket in writing of any unauthorized use of which Customer
              becomes aware. Customer will immediately terminate any unauthorized use by persons having access to a
              Product or Documentation through Customer.
            </p>
            <p>
              9. Ownership; Data. As between GrubMarket and Customer, Customer retains all right, title, and interest,
              including all intellectual property rights, in and to (a) any data or information that Customer uploads or
              inputs into a Product or otherwise makes available to GrubMarket, including in connection with Customer’s
              use of a Product or receipt of Professional Services and (b) data that is generated and made available to
              Customer by any Product through use of the data described in part (a) above ((a) and (b) collectively,
              “Customer Data”). Customer hereby grants GrubMarket a non-exclusive, worldwide, royalty-free, fully paid,
              sublicensable, fully transferable, irrevocable license to use, process, transmit, store, and disclose the
              Customer Data : (a) during the Term (defined below), for the purpose of exercising GrubMarket’s rights and
              performing its obligations under this Agreement and (b) in perpetuity, in a form that does not identify
              Customer as the source thereof, for its business purposes, including to develop and improve GrubMarket’s
              and its Affiliates’ products and services. Customer represents and warrants that Customer has all rights
              necessary to grant GrubMarket the licenses set forth in this Section 2.9 and to enable GrubMarket to
              exercise its rights under the same without violation or infringement of the rights of any third party. As
              between the Parties, GrubMarket owns all right, title, and interest, including all intellectual property
              rights, in and to the Products, Documentation, and any improvements to any GrubMarket products or services
              made as a result of GrubMarket’s use, processing, or generation of Customer Data. During the applicable
              Order Term, Customer may request that GrubMarket make available to Customer a copy of Customer Data stored
              in certain Products, and GrubMarket may agree to do so for an additional fee.
            </p>
            <p>
              10. Feedback. If Customer provides any feedback to GrubMarket concerning the functionality and performance
              of a Product, any Documentation, or the Professional Services (including identifying potential errors and
              improvements), Customer hereby assigns to GrubMarket all right, title, and interest in and to the
              feedback, and GrubMarket is free to use the feedback without payment or restriction.
            </p>
            <p>
              11. Technical Support Services. For so long as Customer is current with its payment of the Fees (defined
              below), GrubMarket will use reasonable efforts to provide Customer with technical support services
              relating to the Platform as described in the Order form.
            </p>
            <h3>3. PROFESSIONAL SERVICES</h3>
            <p>
              1. Provision of Professional Services. Subject to the terms of this Agreement, GrubMarket will use
              commercially reasonable efforts to provide any implementation, configuration, or other professional
              services expressly identified on the Order (which may be in the form of a Statement of Work) (the
              “Professional Services”). GrubMarket shall perform the Professional Services in a professional manner in
              accordance with industry standards.
            </p>
            <p>
              2. Professional Services. GrubMarket retains all right, title, and interest, including all intellectual
              property rights, in and to any work product or other materials created by GrubMarket in connection with
              its performance of Professional Services. If GrubMarket provides any Professional Services to Customer
              pursuant to the applicable Order, GrubMarket hereby grants to Customer a non-exclusive, royalty-free,
              fully paid up, worldwide access under GrubMarket’s rights in the Professional Services to use and exploit
              such Professional Services in connection with the Products and Professional Services during the applicable
              Order Term.
            </p>
            <p>
              3. Modifications. The terms of this Section 3.3 will apply to any Order for Professional Services that
              does not expressly set forth a procedure in accordance with which the Parties may make changes to such
              Order. Customer may request a modification to the Professional Services to be performed pursuant to any
              particular Order by written request to GrubMarket specifying the desired modifications (each a “Change
              Order”). GrubMarket will, within a reasonable time following receipt of such Change Order request, submit
              an estimate of the cost for such modifications and a revised estimate of the time for performance of the
              Professional Services pursuant to the applicable Order. If Customer accepts a Change Order in writing
              within 10 days after receiving it, such modifications in the Change Order shall be performed under the
              terms of this Agreement.
            </p>
            <p>4. Personnel</p>
            <p>
              1. Suitability. GrubMarket will assign employees and subcontractors with qualifications suitable for the
              work described in the relevant Order. GrubMarket may replace or change employees and subcontractors, in
              its sole discretion, with other suitably qualified employees or subcontractors.
            </p>
            <p>
              2. Customer Responsibilities. Customer will make available in a timely manner, at no charge to GrubMarket,
              all technical data, computer facilities, programs, files, documentation, test data, sample output, or
              other information and resources of Customer required by GrubMarket for the performance of the Professional
              Services. Customer is responsible for, and assumes the risk of, any problems resulting from, the content,
              accuracy, completeness, and consistency of all such data, materials, and information. Customer will
              provide, at no charge to GrubMarket, office space, services, and equipment as GrubMarket reasonably
              requires to perform the Professional Services.
            </p>
            <p>
              3. Non-solicitation. The employees and consultants of GrubMarket who perform the Professional Services are
              a valuable asset to GrubMarket and are difficult to replace. Accordingly, Customer agrees that, during the
              Term, and for a period of one year after completion of the Professional Services under an Order, to the
              extent the following restriction is permitted under applicable law, it shall not solicit for employment or
              engagement (whether as an employee, independent contractor or consultant) any GrubMarket employee or
              consultant who performed any of the Professional Services under that Order. Customer is not restricted
              from hiring any personnel that respond to public job advertisements or similar general solicitations.
            </p>
            <h3>4. FEES AND PAYMENT</h3>
            <p>
              1. Fees and Payment Terms. Customer will pay GrubMarket the fees and any other amounts owing under this
              Agreement as specified in the applicable Order (the “Fees”). Any additional annual Subscription Services
              purchased by Customer shall be charged on a prorata basis so that its Term will be the same as the
              original Order. Any request to terminate additional user subscriptions shall not be effective until the
              end of the then current Term. Except as specifically stated otherwise in the applicable Order or in this
              Agreement, all fees paid by a customer are non-refundable and are for services provided and rendered or to
              be provided and rendered. If Customer purchases through an Order Form, then, unless otherwise specified in
              the applicable Order Form, Customer will pay all amounts due within 30 days of the date of the invoice (as
              defined below) or the applicable Order Form, whichever is earlier. GrubMarket may, notwithstanding its
              other rights and remedies herein, condition or restrict Agreement renewals and future Order Forms to
              adjusted payment terms from the terms stated above. Any amount not paid when due will be subject to
              finance charges equal to 1.5% of the unpaid balance per month or the highest rate permitted by applicable
              usury law, whichever is less, determined and compounded daily from the date due until the date paid. If
              Customer elects to pay fees with a credit card through the online purchasing portal, Customer hereby
              authorizes GrubMarket to charge all sums for the orders and level of service selected by Customer through
              the online purchasing portal, including all applicable Taxes (defined below), to the payment method
              specified in Customer’s account. GrubMarket may seek pre-authorization of Customer’s credit card account
              prior to purchase to verify that the credit card is valid and has the necessary funds or credit available
              to cover the purchase. Customer shall reimburse any costs or expenses (including, but not limited to,
              reasonable attorneys’ fees) incurred by GrubMarket to collect any amount that is not paid when due.
              Amounts due from Customer under this Agreement may not be withheld or offset by Customer against amounts
              due to Customer for any reason.
            </p>
            <p>
              2. Taxes. Other than net income taxes imposed on GrubMarket, Customer will bear all taxes, duties, and
              other governmental charges (collectively, “Taxes”) resulting from this Agreement. Customer will pay any
              additional Taxes as are necessary to ensure that the net amounts received by GrubMarket after all such
              Taxes are paid are equal to the amounts to which GrubMarket would have been entitled in accordance with
              this Agreement if such additional Taxes did not exist.
            </p>
            <p>
              3. Professional Services Charges. Customer shall pay GrubMarket at GrubMarket’s standard hourly rates, as
              adjusted, or based on the fees stated in the applicable Order. Customer shall reimburse GrubMarket for (a)
              reasonable travel and living expenses incurred by GrubMarket’s employees and contractors for travel from
              GrubMarket’s offices in connection with the performance of the Professional Services; (b) reasonable
              international telephone charges (if applicable) that are necessary for the performance of Professional
              Services under this Agreement; and (c) any other expenses for which reimbursement is contemplated in the
              applicable Order. Except as provided above, each Party will be responsible for its own expenses incurred
              in rendering performance under this Agreement and each applicable Order.
            </p>
            <p>
              4. Invoicing. GrubMarket will issue Customer invoices for the fees, charges and other amounts owed
              pursuant to this Agreement and the applicable Order Form. Invoices will be delivered to the Customer in
              pdf format by email to the email address indicated by the Customer in the Order Form. At Customer's
              request, the invoices can also be mailed to Customer at its principal office or any other address
              indicated in the Order Form. Customer is responsible for providing complete and accurate contact
              information for billing and communication purposes and shall timely notify GrubMarket in writing of any
              changes to its contact information.
            </p>
            <p>
              5. Discounts. Unless specifically stated in the Order, any discounts given to a Customer shall apply to
              the first year’s fees. All renewals thereafter shall be at the listed prices and then current charges.
            </p>
            <p>
              6. Changes to Fees and Charges. GrubMarket may adjust the Services Fees upon at least thirty (30) days
              prior written notice and any changes to fees and charges shall be effective upon the commencement of the
              next Subscription Term.
            </p>
            <h3>5. TERM AND TERMINATION</h3>
            <p>
              1. Term. This Agreement will remain in effect until all Orders hereunder expire or have been terminated
              (the “Term”).
            </p>
            <p>
              2. Termination for Material Breach. Either Party may terminate this Agreement or one or more Orders if the
              other Party does not cure its material breach of this Agreement or the applicable Order(s) within 30 days
              of receiving written notice of the material breach from the non-breaching Party. Termination in accordance
              with this Section 5.2 will take effect when the breaching Party receives written notice of termination
              from the non-breaching Party, which notice must not be delivered until the breaching Party has failed to
              cure its material breach during the 30-day cure period. Notwithstanding the foregoing, GrubMarket may
              immediately terminate this Agreement upon notice to Customer if GrubMarket reasonably believes that
              Customer has made or distributed any unauthorized copies of any Product, has violated Section 2.4, has
              attempted to assign any right granted by this Agreement except as expressly permitted herein, or has
              otherwise taken any actions that threaten or challenge GrubMarket’s intellectual property rights,
              including rights in and to any Product. Without limiting any other provision of this Section 5.2, if
              Customer fails to timely pay any fees, GrubMarket may, without limitation to any of its other rights or
              remedies, suspend access to the Products or performance of the Professional Services under all Orders
              until it receives all amounts due.
            </p>
            <p>
              3. Termination for Bankruptcy or Insolvency. Either Party may terminate this Agreement or one or more
              Orders if the other Party ceases to do business in the ordinary course or is insolvent (i.e., unable to
              pay its debts in the ordinary course as they come due), or is declared bankrupt, or is the subject of any
              liquidation or insolvency proceeding which is not dismissed within 120 days, or makes any assignment for
              the benefit of creditors.
            </p>
            <p>
              4. Post-Termination Obligations. If this Agreement is terminated for any reason, Customer will pay to
              GrubMarket any fees or other amounts that have accrued prior to the effective date of the termination, any
              and all liabilities accrued prior to the effective date of the termination will survive, and Customer will
              provide GrubMarket with a written certification signed by an authorized Customer representative certifying
              that all use of Products and Documentation by Customer has been discontinued and that all Confidential
              Information (defined below) in Customer’s possession or control has been returned or destroyed.
            </p>
            <p>
              5. Survival. Notwithstanding anything to the contrary herein, Sections 1, 2.9, 2.10, 3.4(c), 4, 5.4, 5.5,
              6, 7.3, 7.4, 8, 9, and 10 will survive termination or expiration of this Agreement.
            </p>

            <h3>6. PRIVACY</h3>
            <p>1. Customer Data is available solely for Customer use within the Platform.</p>
            <p>2. Customer governs access to Customer Data within the Platform. Customer may permit members of GrubMarket’s team to access Customer’s Data for the purposes of training and technical support.</p>
            <p>3. Customer Data will not be viewed, duplicated, or shared, unless Customer gives GrubMarket express permission to do so.</p>

            <h3>7. CONFIDENTIALITY</h3>
            <p>
              1. Definition. As used herein, “Confidential Information” means all confidential information disclosed by
              or otherwise obtained from a Party (“Disclosing Party”) to or by the other Party (“Receiving Party”),
              whether orally, visually, or in writing, that is designated as confidential or that reasonably should be
              understood to be confidential given the nature of the information and the circumstances of disclosure.
              “Confidential Information” of a Disclosing Party includes such Disclosing Party’s business and marketing
              plans, technology and technical information, product plans and designs, and business processes. Without
              limiting the foregoing, GrubMarket’s “Confidential Information” includes each Product, all Documentation,
              all GrubMarket technical information, and all information concerning Product-related database structure
              information and schema. However, “Confidential Information” does not include any information that (a) is
              or becomes generally known to the public without breach of any obligation owed to the Disclosing Party,
              (b) was known to the Receiving Party prior to its disclosure by the Disclosing Party without breach of any
              obligation owed to the Disclosing Party, (c) is received from a third party without breach of any
              obligation owed to the Disclosing Party, or (d) was independently developed by the Receiving Party.
            </p>
            <p>
              2. Protection of Confidential Information. Except as otherwise permitted in writing by the Disclosing
              Party, the Receiving Party will (a) use the same degree of care that it uses to protect the
              confidentiality of its own confidential information of like kind (but in no event less than reasonable
              care) not to disclose or use any Confidential Information of the Disclosing Party for any purpose outside
              the scope of this Agreement and (b) limit access to Confidential Information of the Disclosing Party to
              those of its employees, contractors and agents who need such access for purposes consistent with this
              Agreement and who have signed confidentiality agreements with the Receiving Party containing protections
              no less stringent than those herein. Notwithstanding the foregoing, GrubMarket is permitted to disclose
              Confidential Information of Customer on a need to know basis to employees, contractors, and agents of its
              Affiliates. The Receiving Party may disclose Confidential Information of the Disclosing Party if it is
              compelled by law to do so, provided the Receiving Party gives the Disclosing Party prior notice of such
              compelled disclosure (to the extent legally permitted) and reasonable assistance, at the Disclosing
              Party’s cost, if the Disclosing Party wishes to contest the disclosure. If the Receiving Party is
              compelled by law to disclose the Disclosing Party’s Confidential Information as part of a civil proceeding
              to which the Disclosing Party is a party, and the Disclosing Party is not contesting the disclosure, the
              Disclosing Party will reimburse the Receiving Party for its reasonable cost of compiling and providing
              secure access to such Confidential Information. “Affiliate” means any corporation, partnership, joint
              venture, or other entity: (i) as to which a Party owns or controls, directly or indirectly, stock or other
              interest representing more than 50% of the aggregate stock or other interest entitled to vote on general
              decisions reserved to the stockholders, partners, or other owners of such entity; (ii) if a partnership,
              as to which a Party or another Affiliate is a general partner; or (iii) that a Party otherwise is in
              common control with, controlled by, or controls in matters of management and operations.
            </p>
            <h3>8. WARRANTIES AND DISCLAIMER</h3>
            <p>
              1. Limited GrubMarket Warranties. GrubMarket hereby warrants, for the benefit of Customer only, that the
              Platform will materially conform to the applicable Documentation (the “Platform Warranty”) during the
              Order Term (the “Warranty Period”), provided that the Platform Warranty will not apply to failures to
              conform to the applicable Documentation to the extent such failures arise, in whole or in part, from any
              modification of the Platform by Customer or any third party or any combination of the Platform with APIs,
              software, hardware, or other technology not provided by GrubMarket under the applicable Order. If any
              defect or error covered by the Platform Warranty occurs, Customer will provide GrubMarket with sufficient
              detail to allow GrubMarket to reproduce the defect or error. If notified in writing by Customer during the
              Warranty Period, GrubMarket will, at its sole option, either (a) correct such error or defect in the
              Platform, at no cost to Customer and within a reasonable time, by issuing corrected instructions, a
              restriction, or a bypass or (b) refund prorated subscription fees and terminate this Agreement and any
              applicable Order Form. The foregoing sentence sets forth Customer’s sole and exclusive remedy for
              GrubMarket’s breach of the warranty described in the first sentence of this Section 8.1. GrubMarket is not
              responsible for any defect or error not reported during the Warranty Period or any defect or error caused
              by a Product that Customer has modified, misused, or damaged.
            </p>
            <p>
              2. Mutual Warranties. Each Party represents and warrants to the other that: this Agreement has been duly
              executed and delivered and constitutes a valid and binding agreement enforceable against such Party in
              accordance with its terms and no authorization or approval from any third party is required in connection
              with such Party’s execution, delivery, or performance of this Agreement.
            </p>
            <p>
              3. Disclaimer. EXCEPT FOR THE EXPRESS REPRESENTATIONS AND WARRANTIES STATED IN THIS SECTION 7 OR AN ORDER,
              GRUBMARKET MAKES NO ADDITIONAL REPRESENTATION OR WARRANTY OF ANY KIND WHETHER EXPRESS, IMPLIED (EITHER IN
              FACT OR BY OPERATION OF LAW), OR STATUTORY, AS TO ANY MATTER WHATSOEVER. GRUBMARKET EXPRESSLY DISCLAIMS
              ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUALITY, ACCURACY, TITLE, AND
              NON-INFRINGEMENT. GRUBMARKET DOES NOT WARRANT AGAINST INTERFERENCE WITH THE ENJOYMENT OF THE PRODUCTS OR
              PROFESSIONAL SERVICES. GRUBMARKET DOES NOT WARRANT THAT THE PRODUCTS, DOCUMENTATION, OR PROFESSIONAL
              SERVICES ARE ERROR-FREE OR THAT OPERATION OF THE PRODUCTS OR PROVISION OF THE PROFESSIONAL SERVICES WILL
              BE SECURE OR UNINTERRUPTED. GRUBMARKET DOES NOT WARRANT THAT ANY INFORMATION PROVIDED BY A PRODUCT OR
              DOCUMENTATION, OR IN CONNECTION WITH THE PROFESSIONAL SERVICES, IS ACCURATE OR COMPLETE OR THAT ANY SUCH
              INFORMATION WILL ALWAYS BE AVAILABLE. GRUBMARKET EXERCISES NO CONTROL OVER, AND EXPRESSLY DISCLAIMS ANY
              LIABILITY ARISING OUT OF OR BASED UPON THE RESULTS OF, CUSTOMER’S USE OF THE PRODUCTS OR DOCUMENTATION OR
              RECEIPT OF THE PROFESSIONAL SERVICES.
            </p>
            <p>
              4. Additional Disclaimer. NOTWITHSTANDING SECTIONS 8.1, 8.2, 9, AND 10, EVAL PRODUCTS ARE PROVIDED “AS-IS”
              WITHOUT ANY WARRANTY AND GRUBMARKET HAS NO INDEMNIFICATION OBLIGATIONS NOR LIABILITY OF ANY TYPE WITH
              RESPECT TO THE EVAL PRODUCTS UNLESS SUCH EXCLUSION OF LIABILITY IS NOT ENFORCEABLE UNDER APPLICABLE LAW IN
              WHICH CASE GRUBMARKET’S LIABILITY WITH RESPECT TO THE EVAL PRODUCTS WILL NOT EXCEED $100.00. WITHOUT
              LIMITING THE FOREGOING, GRUBMARKET AND ITS AFFILIATES AND ITS LICENSORS DO NOT REPRESENT OR WARRANT TO
              CUSTOMER THAT: (a) CUSTOMER’S USE OF EVAL PRODUCTS WILL MEET CUSTOMER’S REQUIREMENTS, (b) CUSTOMER’S USE
              OF THE EVAL PRODUCTS WILL BE UNINTERRUPTED, TIMELY, SECURE OR FREE FROM ERROR, AND (c) DATA PROVIDED
              DURING THE EVAL PERIOD WILL BE ACCURATE. NOTWITHSTANDING ANYTHING TO THE CONTRARY IN SECTION 9, CUSTOMER
              WILL BE FULLY LIABLE UNDER THIS AGREEMENT TO GRUBMARKET AND ITS AFFILIATES FOR ANY DAMAGES ARISING OUT OF
              CUSTOMER’S USE OF THE SERVICES DURING THE EVAL PERIOD, ANY BREACH BY CUSTOMER OF THIS AGREEMENT AND ANY OF
              CUSTOMER’S INDEMNIFICATION OBLIGATIONS HEREUNDER.
            </p>
            <h3>9. INDEMNIFICATION</h3>
            <p>
              1. Defense by GrubMarket. GrubMarket will, at its expense, either defend Customer from or settle any
              claim, proceeding, or suit (“Claim”) brought by a third party against Customer alleging that Customer’s
              use of the Platform infringes or misappropriates any patent, copyright, or trademark if: Customer gives
              GrubMarket prompt written notice of the Claim; Customer grants GrubMarket full and complete control over
              the defense and settlement of the Claim; Customer provides assistance in connection with the defense and
              settlement of the Claim as GrubMarket may reasonably request; and Customer complies with any settlement or
              court order made in connection with the Claim. Customer will not defend or settle any Claim subject to
              indemnification under this Section 9.1 without GrubMarket’s prior written consent. Customer will have the
              right to participate in the defense of the Claim at its own expense and with counsel of its own choosing,
              but GrubMarket will have sole control over the defense and settlement of the Claim.
            </p>
            <p>
              2. Indemnification by GrubMarket. GrubMarket will indemnify Customer from and pay all damages, costs, and
              attorneys’ fees finally awarded against Customer in any Claim under Section 9.1; all out-of-pocket costs
              (including reasonable attorneys’ fees) reasonably incurred by Customer in connection with the defense of a
              Claim under Section 9.1 (other than attorneys’ fees and costs incurred without GrubMarket’s consent after
              GrubMarket has accepted defense of the Claim); and all amounts that GrubMarket agrees to pay to any third
              party to settle any Claim under Section 9.1.
            </p>
            <p>
              3. Exclusions from Obligations. GrubMarket will have no obligation under this Section 8 for any
              infringement or misappropriation to the extent that it arises out of or is based upon any of the following
              (the “Excluded Claims”): use of a Product in combination with other products or services not provided by
              GrubMarket if such infringement or misappropriation would not have arisen but for such combination; a
              Product or the Professional Services are provided to comply with designs, requirements, or specifications
              required by or provided by Customer, if the alleged infringement or misappropriation would not have arisen
              but for the compliance with such designs, requirements, or specifications; use of a Product by Customer
              for purposes not intended or outside the scope of the access granted to Customer; Customer’s failure to
              use a Product in accordance with instructions provided by GrubMarket, if the infringement or
              misappropriation would not have occurred but for such failure; or any modification of a Product not made
              or authorized in writing by GrubMarket where such infringement or misappropriation would not have occurred
              absent such modification.
            </p>
            <p>
              4. Remedy. If GrubMarket becomes aware of, or anticipates, a Claim subject to indemnification under
              Sections 9.1 and 9.2 or otherwise relating to the Products, then GrubMarket may, at its option (a) modify
              the Products that are the subject of the Claim so that they become non-infringing, or substitute
              functionally equivalent products; (b) obtain a license to the third-party intellectual property rights
              giving rise to the Claim; or (c) terminate the affected Order(s) on written notice to Customer and refund
              to Customer any pre-paid but unused fees (which, in the case of perpetual access, will be calculated based
              on 3-year straight-line basis).
            </p>
            <p>
              5. Limited Remedy. Sections 9.1, 9.2, 9.3, and 9.4 state GrubMarket’s sole and exclusive liability, and
              Customer’s sole and exclusive remedy,for the actual or alleged infringement or misappropriation of any
              third-party intellectual property right by a Product or the Professional Services.
            </p>
            <p>
              6. Defense by Customer. Customer will defend GrubMarket from any actual or threatened third-party Claim
              arising out of or based upon Customer’s use of a Product, provision of the Customer Data, or breach of any
              of the provisions of this Agreement, or that is an Excluded Claim, if: GrubMarket gives Customer prompt
              written notice of the Claim; GrubMarket grants Customer full and complete control over the defense and
              settlement of the Claim; GrubMarket provides assistance in connection with the defense and settlement of
              the Claim as Customer may reasonably request; and GrubMarket complies with any settlement or court order
              made in connection with the Claim. GrubMarket will not defend or settle any Claim subject to
              indemnification under this Section 9.6 without Customer’s prior written consent. GrubMarket will have the
              right to participate in the defense of the Claim at its own expense and with counsel of its own choosing,
              but Customer will have sole control over the defense and settlement of the Claim.
            </p>
            <p>
              7. Indemnification by Customer. Customer will indemnify GrubMarket from and pay all damages, costs, and
              attorneys’ fees finally awarded against GrubMarket in any Claim under Section 9.6; all out-of-pocket costs
              (including reasonable attorneys’ fees) reasonably incurred by GrubMarket in connection with the defense of
              a Claim under Section 9.6 (other than attorneys’ fees and costs incurred without Customer’s consent after
              Customer has accepted defense of the Claim); and, all amounts that Customer agrees to pay to any third
              party to settle any Claim under Section 9.6.
            </p>

            <h3>10. LIMITATIONS OF LIABILITY</h3>
            <p>
              1. Disclaimer of Indirect Damages. NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED IN THIS AGREEMENT,
              GRUBMARKET WILL NOT, UNDER ANY CIRCUMSTANCES, BE LIABLE TO CUSTOMER FOR CONSEQUENTIAL, INCIDENTAL,
              SPECIAL, OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO LOST PROFITS OR LOSS OF BUSINESS, ARISING OUT
              OF OR RELATED TO THE SUBJECT MATTER OF THIS AGREEMENT, EVEN IF GRUBMARKET IS APPRISED OF THE LIKELIHOOD OF
              SUCH DAMAGES OCCURRING. WITHOUT LIMITING THE FOREGOING, UNDER NO CIRCUMSTANCES WILL GRUBMARKET BE LIABLE
              FOR ANY LOSS OF DATA STORED IN, OR IN CONNECTION WITH, A PRODUCT.
            </p>
            <p>
              2. Cap on Liability. UNDER NO CIRCUMSTANCES WILL GRUBMARKET’S TOTAL LIABILITY OF ALL KINDS ARISING OUT OF
              OR RELATED TO THIS AGREEMENT (INCLUDING BUT NOT LIMITED TO WARRANTY CLAIMS), REGARDLESS OF THE FORUM AND
              REGARDLESS OF WHETHER ANY ACTION OR CLAIM IS BASED ON CONTRACT, TORT, OR OTHERWISE, EXCEED THE TOTAL
              AMOUNT PAID BY CUSTOMER TO GRUBMARKET UNDER THE APPLICABLE ORDER WITH RESPECT TO WHICH THE LIABILITY AROSE
              DURING THE 12 MONTHS IMMEDIATELY PRECEDING THE CLAIM (DETERMINED AS OF THE DATE OF ANY FINAL JUDGMENT IN
              AN ACTION).
            </p>
            <p>
              3. Independent Allocations of Risk. EACH PROVISION OF THIS AGREEMENT THAT PROVIDES FOR A LIMITATION OF
              LIABILITY, DISCLAIMER OF WARRANTIES, OR EXCLUSION OF DAMAGES IS TO ALLOCATE THE RISKS OF THIS AGREEMENT
              BETWEEN THE PARTIES. THIS ALLOCATION IS REFLECTED IN THE PRICING OFFERED BY GRUBMARKET TO CUSTOMER AND IS
              AN ESSENTIAL ELEMENT OF THE BASIS OF THE BARGAIN BETWEEN THE PARTIES. EACH OF THESE PROVISIONS IS
              SEVERABLE AND INDEPENDENT OF ALL OTHER PROVISIONS OF THIS AGREEMENT. THE LIMITATIONS IN THIS SECTION 9
              WILL APPLY NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY IN THIS AGREEMENT.
            </p>

            <h3>11. GENERAL</h3>
            <p>
              1. Relationship. GrubMarket will be and act as an independent contractor (and not as the agent or
              representative of Customer) in the performance of this Agreement.
            </p>
            <p>
              2. Assignability. Neither Party may assign its right, duties, or obligations under this Agreement without
              the other Party’s prior written consent, which consent will not be unreasonably withheld or delayed,
              except that GrubMarket may assign this Agreement to an Affiliate or a successor (including a successor by
              way of Change of Control or operation of law), or in connection with the sale of all of the assets or
              business to which this Agreement relates. A Change of Control shall be deemed to cause an assignment of
              this Agreement. “Change of Control” means a merger, acquisition, divestiture, sale of assets or equity, or
              similar transaction.
            </p>
            <p>
              3. Export. Customer will comply with all applicable export and import laws, rules, and regulations in
              connection with Customer’s activities under this Agreement. Customer acknowledges that it is Customer’s
              responsibility to obtain any required licenses to export and re-export Products. The Products, including
              technical data, are subject to U.S. export control laws, including the U.S. Export Administration Act and
              its associated regulations, and may be subject to export or import regulations in other countries.
              Customer represents and warrants that the Products are not being and will not be acquired for, shipped,
              transferred, or re-exported, directly or indirectly, to proscribed or embargoed countries or their
              nationals and persons on the Table of Denial Orders, the Entity List or the List of Specifically
              Designated Nationals, unless specifically authorized by the U.S. Government for those purposes.
            </p>
            <p>
              4. Subcontractors. GrubMarket may utilize subcontractors or other third parties to perform its duties
              under this Agreement so long as GrubMarket remains responsible for all of its obligations under this
              Agreement.
            </p>
            <p>
              5. Notices. Any notice required or permitted to be given in accordance with this Agreement will be
              effective if it is in writing and sent by certified or registered mail, or insured courier, return receipt
              requested, to the appropriate Party at the address set forth on the applicable Order and with the
              appropriate postage affixed. Either Party may change its address for receipt of notice by notice to the
              other Party in accordance with this Section 11.5. Notices are deemed given 2 business days following the
              date of mailing or 1 business day following delivery to a courier.
            </p>
            <p>
              6. Force Majeure. Neither Party will be liable for, or be considered to be in breach of or default under
              this Agreement (except for failure to make payments when due) on account of, any delay or failure to
              perform as required by this Agreement as a result of any cause or condition beyond its reasonable control,
              so long as that Party uses all commercially reasonable efforts to avoid or remove the causes of
              non-performance.
            </p>
            <p>
              7. Governing Law. This Agreement will be interpreted, construed, and enforced in all respects in
              accordance with the local laws of the State of California, and not including the provisions of the 1980
              U.N. Convention on Contracts for the International Sale of Goods. Each Party hereby irrevocably consents
              to the exclusive jurisdiction and venue of the federal, state, and local courts in San Francisco,
              California in connection with any action arising out of or in connection with this Agreement.
            </p>
            <p>
              8. Waiver. The waiver by either Party of any breach of any provision of this Agreement does not waive any
              other breach. The failure of any Party to insist on strict performance of any covenant or obligation in
              accordance with this Agreement will not be a waiver of such Party’s right to demand strict compliance in
              the future, nor will the same be construed as a novation of this Agreement.
            </p>
            <p>
              9. Severability. If any part of this Agreement is found to be illegal, unenforceable, or invalid, the
              remaining portions of this Agreement will remain in full force and effect. If any material limitation or
              restriction on the use of a Product under this Agreement is found to be illegal, unenforceable, or
              invalid, Customer’s right to use Products will immediately terminate.
            </p>
            <p>
              10. Interpretation. For purposes of this Agreement, (a) the words “include,” “includes” and “including”
              will be deemed to be followed by the words “without limitation;”; (b) the words “such as”, “for example”
              “e.g.” and any derivatives of those words will mean by way of example and the items that follow these
              words will not be deemed an exhaustive list; (c) the word “or” is used in the inclusive sense of “and/or”
              and the terms “or,” “any,” and “either” are not exclusive; (d) the words “herein,” “hereof,” “hereby,”
              “hereto” and “hereunder” refer to this Agreement as a whole; (e) words denoting the singular have a
              comparable meaning when used in the plural, and vice-versa; and (f) whenever the context may require, any
              pronouns used in this Agreement will include the corresponding masculine, feminine or neuter forms, and
              the singular form of nouns and pronouns will include the plural, and vice versa. The headings set forth in
              this Agreement are for convenience of reference purposes only and will not affect or be deemed to affect
              in any way the meaning or interpretation of this Agreement or any term or provision hereof. References to
              “$” and “dollars” are to the currency of the United States of America. Any law defined or referred to
              herein means such law as from time to time amended, modified or supplemented, including (in the case of
              statutes) by succession of comparable successor laws.
            </p>
            <p>
              11. Entire Agreement. This Agreement, including all exhibits and Orders hereunder, is the final and
              complete expression of the agreement between these Parties regarding the subject matter hereof. This
              Agreement supersedes, and the terms of this Agreement govern, all previous oral and written communications
              regarding these matters, all of which are merged into this Agreement, except that this Agreement does not
              supersede any prior nondisclosure or comparable agreement between the Parties executed prior to this
              Agreement being executed, nor does it affect the validity of any agreements between the Parties relating
              to other products or services of GrubMarket that are not described in the applicable Order and with
              respect to which Customer has executed a separate agreement with GrubMarket that remains in effect. No
              employee, agent, or other representative of GrubMarket has any authority to bind GrubMarket with respect
              to any statement, representation, warranty, or other expression unless the same is specifically set forth
              in this Agreement. No usage of trade or other regular practice or method of dealing between the Parties
              will be used to modify, interpret, supplement, or alter the terms of this Agreement. This Agreement may be
              changed only by a written agreement signed by an authorized agent of the Party against whom enforcement is
              sought. GrubMarket will not be bound by, and specifically objects to, any term, condition, or other
              provision that is different from or in addition to this Agreement (whether or not it would materially
              alter this Agreement) that is proffered by Customer in any receipt, acceptance, confirmation,
              correspondence, or otherwise, unless GrubMarket specifically provides a written acceptance of such
              provision signed by an authorized agent of GrubMarket.
            </p>
          </TermsContent>
        </TermsContainer>
        <FooterContainer />
      </TermsPageContainer>
    )
  }
}
