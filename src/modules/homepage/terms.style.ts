import styled from '@emotion/styled'

export const TermsPageContainer = styled('div')({})

export const TermsContainer = styled('div')({
  padding: '56px 100px',
})

export const TermsTitle = styled('div')({
  fontFamily: 'Museo Sans Rounded',
  color: '#1C6E31',
  fontWeight: 500,
  fontSize: 36,
  lineHeight: '36px',
})

export const TermsContent = styled('div')({
  marginTop: 10,
  fontFamily: 'Museo Sans Rounded',
  color: '#4A5355',
  fontSize: 20,
  lineHeight: '30px',
  h3: {
    color: '#4A5355',
    marginTop: 65,
    marginBottom: 18,
    '&.mt0': {
      marginTop: 0,
    },
  },
})
