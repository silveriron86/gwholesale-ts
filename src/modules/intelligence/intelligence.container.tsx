/**@jsx jsx */
import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { jsx } from '@emotion/core'
import { Theme } from '~/common'
import { mediumGrey, mediumGrey2 } from '~/common'
import { GlobalState } from '~/store/reducer'
import { Card, notification } from 'antd'
import { Chart, Geom, Axis, Tooltip } from 'bizcharts'
import { IntelligenceDispatchProps, IntelligenceStateProps, IntelligenceModule } from './intelligence.module'
import {
  IntelligenceContainer,
  Container,
  ContainerTitlePanel,
  ContainerHeader,
  ContainerHeaderContent,
  TabCardBody,
  tabCardBodyCheckActive,
  tabCardStyle,
  CardContainerRow,
  ContainerBody,
  StatsCardStyle,
  ChartCardStyle,
  SummaryChartCardStyle,
  CardHeaderRow,
  StatsCardHeadStyle,
  ChartCardHeadStyle,
  StatSpan,
  UnitSpan,
  MinMaxSpan,
  MinUnitSpan,
} from './intelligence.style'
import { OrderTable } from '../orders/components/orders-table.component'
import { Link } from 'react-router-dom'

export type IntelligenceProps = IntelligenceDispatchProps &
  IntelligenceStateProps &
  RouteProps & {
    theme: Theme
  }

export interface IntelligenceState {
  activeSales: string
  activeSummary: string
  sort: string
  mock_data0: any
  mock_data1: any
}

export class IntelligenceComponent extends React.PureComponent<IntelligenceProps, IntelligenceState> {
  state = {
    sort: '',
    activeSales: '1',
    activeSummary: '2',
    mock_data1: [],
    mock_data0: [
      { year: 'Apr 28', sales: 12530 },
      { year: 'May 5', sales: 12900 },
      { year: 'May 12', sales: 11570 },
      { year: 'May 19', sales: 13600 },
      { year: 'May 26', sales: 12100 },
      { year: 'June 2', sales: 13300 },
      { year: 'June 9', sales: 14300 },
      { year: 'June 16', sales: 14107 },
      { year: 'June 23', sales: 12301 },
      { year: 'June 30', sales: 15305 },
      { year: 'July 7', sales: 14304 },
      { year: 'July 14', sales: 14310 },
    ],
  }

  componentDidMount() {
    this.props.getRecentOrders()
    this.props.getRecentPurchaseOrders()
  }

  render() {
    return (
      <IntelligenceContainer>
        {this.renderTopChart()}
        {this.renderTopTrendStats()}
        {/*{this.renderTopTrendChart()}*/}
        {this.renderRecentStats()}
      </IntelligenceContainer>
    )
  }


  private changeReport = (id: string) => () => {
    if (id === '1') {
      this.setState({
        activeSales: id,
        mock_data0: [
          { year: 'Mar 17', sales: 11320 },
          { year: 'Mar 24', sales: 9600 },
          { year: 'Mar 31', sales: 10110 },
          { year: 'Apr 7', sales: 12500 },
          { year: 'Apr 14', sales: 12340 },
          { year: 'Apr 21', sales: 12600 },
          { year: 'Apr 28', sales: 12530 },
          { year: 'May 5', sales: 12900 },
          { year: 'May 12', sales: 11570 },
          { year: 'May 19', sales: 13600 },
          { year: 'May 26', sales: 12100 },
          { year: 'June 2', sales: 13300 },
          { year: 'June 9', sales: 14300 },
          { year: 'June 16', sales: 14107 },
          { year: 'June 23', sales: 12301 },
          { year: 'June 30', sales: 15305 },
          { year: 'July 7', sales: 14304 },
          { year: 'July 14', sales: 14310 },
        ],
      })
    }
    if (id === '2') {
      this.setState({
        activeSales: id,
        mock_data0: [
          { year: 'Feb 3', sales: 11780 },
          { year: 'Feb 10', sales: 10900 },
          { year: 'Feb 17', sales: 11050 },
          { year: 'Feb 24', sales: 12100 },
          { year: 'Mar 3', sales: 11320 },
          { year: 'Mar 10', sales: 9600 },
          { year: 'Mar 17', sales: 11320 },
          { year: 'Mar 24', sales: 9600 },
          { year: 'Mar 31', sales: 10110 },
          { year: 'Apr 7', sales: 12500 },
          { year: 'Apr 14', sales: 12340 },
          { year: 'Apr 21', sales: 12600 },
          { year: 'Apr 28', sales: 12530 },
          { year: 'May 5', sales: 12900 },
          { year: 'May 12', sales: 11570 },
          { year: 'May 19', sales: 13600 },
          { year: 'May 26', sales: 12100 },
          { year: 'June 2', sales: 13300 },
          { year: 'June 9', sales: 14300 },
          { year: 'June 16', sales: 14107 },
          { year: 'June 23', sales: 12301 },
          { year: 'June 30', sales: 15305 },
          { year: 'July 7', sales: 14304 },
          { year: 'July 14', sales: 14310 },
        ],
      })
    }
    if (id === '3') {
      this.setState({
        activeSales: id,
        mock_data0: [
          { year: 'Oct 18', sales: 0 },
          { year: 'Nov 4', sales: 0 },
          { year: 'Nov 11', sales: 0 },
          { year: 'Nov 18', sales: 0 },
          { year: 'Nov 25', sales: 0 },
          { year: 'Dec 2', sales: 500 },
          { year: 'Dec 9', sales: 3000 },
          { year: 'Dec 16', sales: 8400 },
          { year: 'Dec 23', sales: 9200 },
          { year: 'Dec 30', sales: 10500 },
          { year: 'Jan 6', sales: 11300 },
          { year: 'Jan 13', sales: 9500 },
          { year: 'Jan 20', sales: 12350 },
          { year: 'Jan 27', sales: 10010 },
          { year: 'Feb 3', sales: 11780 },
          { year: 'Feb 10', sales: 10900 },
          { year: 'Feb 17', sales: 11050 },
          { year: 'Feb 24', sales: 12100 },
          { year: 'Mar 3', sales: 11320 },
          { year: 'Mar 10', sales: 9600 },
          { year: 'Mar 17', sales: 11320 },
          { year: 'Mar 24', sales: 9600 },
          { year: 'Mar 31', sales: 10110 },
          { year: 'Apr 7', sales: 12500 },
          { year: 'Apr 14', sales: 12340 },
          { year: 'Apr 21', sales: 12600 },
          { year: 'Apr 28', sales: 12530 },
          { year: 'May 5', sales: 12900 },
          { year: 'May 12', sales: 11570 },
          { year: 'May 19', sales: 13600 },
          { year: 'May 26', sales: 12100 },
          { year: 'June 2', sales: 13300 },
          { year: 'June 9', sales: 14300 },
          { year: 'June 16', sales: 14107 },
          { year: 'June 23', sales: 12301 },
          { year: 'June 30', sales: 15305 },
          { year: 'July 7', sales: 14304 },
          { year: 'July 14', sales: 14310 },
        ],
      })
    }
  }

  private renderTopChart() {
    const cols = {
      sales: {
        min: 0,
      },
      year: {
        range: [0, 1],
      },
    }
    return (
      <Container>
        <ContainerHeader>
          <ContainerTitlePanel>Weekly Sales</ContainerTitlePanel>
        </ContainerHeader>
        <ContainerBody>
          <CardContainerRow>
            <Card
              title="Sales Map"
              headStyle={{ fontWeight: 700, fontSize: '24px', lineHeight: '26px', color: mediumGrey }}
              style={SummaryChartCardStyle}
              extra={
                <CardHeaderRow>
                  <span style={{ marginRight: '6px', paddingTop: '4px', color: mediumGrey2, fontWeight: 700 }}>
                    View
                  </span>
                  <Card
                    css={tabCardBodyCheckActive(this.state.activeSales === '1')}
                    onClick={this.changeReport('1')}
                    style={{ ...tabCardStyle, borderRadius: '5px 0 0 5px', borderRight: '0px' }}
                  >
                    <TabCardBody>12 Weeks</TabCardBody>
                  </Card>
                  <Card
                    css={tabCardBodyCheckActive(this.state.activeSales === '2')}
                    onClick={this.changeReport('2')}
                    style={tabCardStyle}
                  >
                    <TabCardBody>24 Weeks</TabCardBody>
                  </Card>
                  <Card
                    css={tabCardBodyCheckActive(this.state.activeSales === '3')}
                    onClick={this.changeReport('3')}
                    style={{ ...tabCardStyle, borderRadius: '0 5px 5px 0', borderLeft: '0px' }}
                  >
                    <TabCardBody>36 Weeks</TabCardBody>
                  </Card>
                </CardHeaderRow>
              }
            >
              <Chart height={335} data={this.state.mock_data0} scale={cols} forceFit={true}>
                <Axis name="year" />
                <Axis name="sales" />
                <Tooltip
                  crosshairs={{
                    type: 'y',
                  }}
                />
                <Geom type="line" position="year*sales" size={2} />
                <Geom
                  type="point"
                  position="year*sales"
                  size={4}
                  shape={'circle'}
                  style={{
                    stroke: '#fff',
                    lineWidth: 1,
                  }}
                />
              </Chart>
            </Card>
          </CardContainerRow>
        </ContainerBody>
      </Container>
    )
  }

  private comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Better dashboard reports is coming soon!!',
      onClick: () => {
        console.log('Notification Clicked!')
      },
    })
  }

  private renderTopTrendStats() {
    return (
      <Container>
        <ContainerHeader>
          <ContainerTitlePanel>Top Trends Overview</ContainerTitlePanel>
          <ContainerHeaderContent>
            <CardContainerRow>
              <Card
                onClick={this.comingSoon()}
                css={tabCardBodyCheckActive(this.state.activeSummary === '1')}
                style={{ ...tabCardStyle, borderRadius: '5px 0 0 5px', borderRight: '0px' }}
              >
                <TabCardBody>Today</TabCardBody>
              </Card>
              <Card css={tabCardBodyCheckActive(this.state.activeSummary === '2')} style={tabCardStyle}>
                <TabCardBody>Last 7 Days</TabCardBody>
              </Card>
              <Card
                onClick={this.comingSoon()}
                css={tabCardBodyCheckActive(this.state.activeSummary === '3')}
                style={{ ...tabCardStyle, borderRadius: '0 5px 5px 0', borderLeft: '0px' }}
              >
                <TabCardBody>Year-To-Date</TabCardBody>
              </Card>
            </CardContainerRow>
          </ContainerHeaderContent>
        </ContainerHeader>
        <ContainerBody>
          <CardContainerRow>
            <Card style={StatsCardStyle} headStyle={StatsCardHeadStyle} title="Orders Placed">
              <StatSpan>9</StatSpan>
              <UnitSpan>
                Total |{' '}
                <Link onClick={this.comingSoon()} to="/intelligence">
                  AVG. (Day)
                </Link>
              </UnitSpan>
              <MinMaxSpan>
                <span style={{ marginBottom: '5px' }}>
                  0 <MinUnitSpan>MIN (Day)</MinUnitSpan>
                </span>
                <span>
                  4 <MinUnitSpan>MAX (Day)</MinUnitSpan>
                </span>
              </MinMaxSpan>
            </Card>
            <Card style={StatsCardStyle} headStyle={StatsCardHeadStyle} title="Orders sales">
              <StatSpan>$1390</StatSpan>
              <UnitSpan>Average</UnitSpan>
              <MinMaxSpan>
                <span style={{ marginBottom: '5px' }}>
                  $0 <MinUnitSpan>MIN</MinUnitSpan>
                </span>
                <span>
                  $203 <MinUnitSpan>MAX</MinUnitSpan>
                </span>
              </MinMaxSpan>
            </Card>
            <Card style={StatsCardStyle} headStyle={StatsCardHeadStyle} title="Total Sales">
              <StatSpan>$12,500</StatSpan>
              <UnitSpan>
                Total |{' '}
                <Link onClick={this.comingSoon()} to="/intelligence">
                  AVG. (Day)
                </Link>
              </UnitSpan>
              <MinMaxSpan>
                <span style={{ marginBottom: '5px' }}>
                  $0 <MinUnitSpan>MIN</MinUnitSpan>
                </span>
                <span>
                  $810 <MinUnitSpan>MAX</MinUnitSpan>
                </span>
              </MinMaxSpan>
            </Card>
          </CardContainerRow>
          <CardContainerRow>
            <Card style={StatsCardStyle} headStyle={StatsCardHeadStyle} title="Items Sold">
              <StatSpan>15</StatSpan>
              <UnitSpan>
                Total |{' '}
                <Link onClick={this.comingSoon()} to="/intelligence">
                  AVG. (Day)
                </Link>
              </UnitSpan>
              <MinMaxSpan>
                <span style={{ marginBottom: '5px' }}>
                  0 <MinUnitSpan>MIN (Day)</MinUnitSpan>
                </span>
                <span>
                  7 <MinUnitSpan>MAX (Day)</MinUnitSpan>
                </span>
              </MinMaxSpan>
            </Card>
            <Card style={StatsCardStyle} headStyle={StatsCardHeadStyle} title="Items Purchase">
              <StatSpan>0</StatSpan>
              <UnitSpan>
                Total |{' '}
                <Link onClick={this.comingSoon()} to="/intelligence">
                  AVG. (Day)
                </Link>
              </UnitSpan>
              <MinMaxSpan>
                <span style={{ marginBottom: '5px' }}>
                  0 <MinUnitSpan>MIN (Day)</MinUnitSpan>
                </span>
                <span>
                  0 <MinUnitSpan>MAX (Day)</MinUnitSpan>
                </span>
              </MinMaxSpan>
            </Card>
            <Card style={StatsCardStyle} headStyle={StatsCardHeadStyle} title="Purchase Total">
              <StatSpan>0</StatSpan>
              <UnitSpan>
                Total |{' '}
                <Link onClick={this.comingSoon()} to="/intelligence">
                  AVG. (Day)
                </Link>
              </UnitSpan>
              <MinMaxSpan>
                <span style={{ marginBottom: '5px' }}>
                  0 <MinUnitSpan>MIN (Day)</MinUnitSpan>
                </span>
                <span>
                  0 <MinUnitSpan>MAX (Day)</MinUnitSpan>
                </span>
              </MinMaxSpan>
            </Card>
          </CardContainerRow>
        </ContainerBody>
      </Container>
    )
  }

  /*
  private renderTopTrendChart() {
    return (
      <Container>
        <ContainerHeader>
          <ContainerTitlePanel>Top Trends Chart</ContainerTitlePanel>
        </ContainerHeader>
        <ContainerBody>
          <CardContainerRow>
            <Card style={ChartCardStyle} />
            <Card style={ChartCardStyle} />
          </CardContainerRow>
        </ContainerBody>
      </Container>
    )
  }
  */

  private renderRecentStats() {
    return (
      <Container>
        <ContainerHeader>
          <ContainerTitlePanel>Recently Added</ContainerTitlePanel>
        </ContainerHeader>
        <ContainerBody>
          <CardContainerRow>
            <Card style={ChartCardStyle} headStyle={ChartCardHeadStyle} title="Customers Orders">
              <OrderTable orders={this.props.orders} tableMini={true} />
            </Card>
            <Card style={ChartCardStyle} headStyle={ChartCardHeadStyle} title="Purchase Orders">
              <OrderTable orders={this.props.purchaseOrders} tableMini={true} />
            </Card>
          </CardContainerRow>
        </ContainerBody>
      </Container>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.intelligence

export const Intelligence = withTheme(connect(IntelligenceModule)(mapStateToProps)(IntelligenceComponent))
