import {
  Module,
  EffectModule,
  ModuleDispatchProps,
  Effect,
  StateObservable
} from 'redux-epics-decorator'

import { OrderService } from '../orders/order.service';
import { Action } from 'redux-actions';
import { Order } from '~/schema';
import { Observable, of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { checkError } from '~/common/utils';
import { GlobalState } from '~/store/reducer';

export interface IntelligenceStateProps {
  data: {}
  orders: Order[]
  purchaseOrders: Order[]
}

@Module('intelligence')
export class IntelligenceModule extends EffectModule<IntelligenceStateProps> {

  defaultState = {
    data: {},
    orders: [],
    purchaseOrders: []
  }

  constructor(private readonly order: OrderService) {
    super()
  }

  @Effect({
    done: (state: IntelligenceStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        orders: action.payload,
      }
    }
  })
  getRecentOrders(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.order.getRecentOrders(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET').pipe(
          map((data) => data.map((order) => this.formatOrder(order))),
          map(this.createAction('done')),
          catchError((error) => of(
            checkError(error),
          ))
        ),
      ),
    )
  }

  @Effect({
    done: (state: IntelligenceStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        purchaseOrders: action.payload,
      }
    }
  })
  getRecentPurchaseOrders(action$: Observable<void>, _state$: StateObservable<GlobalState>) {
    const MOCK_DATA = [
      {
        id: 521,
        totalPrice: 930,
        updatedDate: +new Date("7/21/2019"),
        customer: {
          id: 15,
          name: "Grubmarket"
        }
      },
      {
        id: 520,
        totalPrice: 600,
        updatedDate: +new Date("7/20/2019"),
        customer: {
          id: 15,
          name: "Grubmarket"
        }
      },
      {
        id: 519,
        totalPrice: 730,
        updatedDate: +new Date("7/20/2019"),
        customer: {
          id: 15,
          name: "Grubmarket"
        }
      },
      {
        id: 518,
        totalPrice: 2400,
        updatedDate: +new Date("7/19/2019"),
        customer: {
          id: 15,
          name: "Grubmarket"
        }
      },
      {
        id: 517,
        totalPrice: 1128,
        updatedDate: +new Date("7/19/2019"),
        customer: {
          id: 15,
          name: "Grubmarket"
        }
      },
      {
        id: 516,
        totalPrice: 652,
        updatedDate: +new Date("1/17/2019"),
        customer: {
          id: 15,
          name: "Grubmarket"
        }
      },
      {
        id: 515,
        totalPrice: 960,
        updatedDate: +new Date("7/17/2019"),
        customer: {
          id: 15,
          name: "Grubmarket"
        }
      },
    ]
    return action$.pipe(
      switchMap(() => of(this.createAction('done')(MOCK_DATA))),
      // switchMap(() =>
      //   this.order.getRecentPurchaseOrders().pipe(
      //     map((data) => data.map((order) => this.formatPurchaseOrder(order))),
      //     map(this.createAction('done')),
      //     catchError((error) => of(
      //       checkError(error),
      //     ))
      //   ),
      // ),
    )
  }

  formatPurchaseOrder(data: any) {
    return {
      id: data.wholesaleOrderId,
      totalCost: data.totalCost,
    }
  }

  formatOrder(data: any)  {
    return {
      id: data.wholesaleOrderId,
      totalCost: data.totalCost,
      totalPrice: data.totalPrice,
      status: data.wholesaleOrderStatus,
      deliveryDate: +new Date(data.deliveryDate),
      updatedDate: +new Date(data.updatedDate),
      priceSheetId: data.priceSheetId,
      qboId: data.qboId,
      customer: {
        id: data.wholesaleCustomerClientId,
        name: data.fullName,
      },
      linkToken: data.linkToken,
    }
  }

}

export type IntelligenceDispatchProps = ModuleDispatchProps<IntelligenceModule>
