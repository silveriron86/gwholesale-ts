import styled from '@emotion/styled'

import { white, mediumGrey, brownGrey, transparent, filterGreen, brightGreen2, brightGreen } from '~/common'
import css from '@emotion/css'

export const IntelligenceContainer = styled('div')({
  width: '100%',
  backgroundColor: filterGreen,
  padding: '50px 81px 98px 131px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const Container = styled('div')({
  backgroundColor: filterGreen,
  width: '100%',
  boxSizing: 'border-box',
  position: 'relative',
  marginBottom: '55px',
})

export const ContainerHeader = styled('div')({
  display: 'flex',
  flexDirection: 'column',
})

export const ContainerTitlePanel = styled('div')({
  fontWeight: 700,
  color: mediumGrey,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '39px',
  justifyContent: 'center',
  display: 'flex',
})

export const ContainerHeaderContent = styled('div')({
  display: 'flex',
  flexDirection: 'column',
})

export const CardHeaderRow = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'row',
})

export const CardContainerRow = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'row',
  marginTop: '35px',

  '& .ant-card-body': {
    padding: '0',
  },
})

export const ContainerBody = styled('div')({
  display: 'flex',
  flexDirection: 'column',
})

export const StatSpan = styled('div')({
  fontWeight: 700,
  fontSize: '72px',
  color: brightGreen,
  lineHeight: '78px',
  textAlign: 'center',
  marginTop: '38px',
})

export const UnitSpan = styled('div')({
  fontSize: '18px',
  color: mediumGrey,
  lineHeight: '19px',
  textAlign: 'center',
  marginTop: '13px',
  textTransform: 'uppercase',

  '& > a': {
    color: brightGreen,
  },
})

export const MinMaxSpan = styled('div')({
  fontSize: '17px',
  color: brightGreen,
  lineHeight: '16px',
  marginTop: '28px',
  marginLeft: '10px',
  display: 'flex',
  flexDirection: 'column',
})

export const MinUnitSpan = styled('span')({
  color: mediumGrey,
})

export const tabCardStyle: React.CSSProperties = {
  width: '142px',
  backgroundColor: white,
  height: '35px',
  cursor: 'pointer',
  border: '0.5px solid ${mediumGrey}',
  padding: '0px',
}

export const StatsCardStyle: React.CSSProperties = {
  width: '400px',
  backgroundColor: white,
  height: '300px',
  borderRadius: '5px',
  border: '0.5px solid ${mediumGrey}',
  padding: '0px',
  marginRight: '50px',
}

export const StatsCardHeadStyle: React.CSSProperties = {
  fontSize: '24px',
  color: mediumGrey,
}

export const SummaryChartCardStyle: React.CSSProperties = {
  width: '1250px',
  backgroundColor: white,
  height: '393px',
  borderRadius: '5px',
  border: '0.5px solid ${mediumGrey}',
  padding: '0px',
  marginRight: '50px',
}

export const ChartCardStyle: React.CSSProperties = {
  width: '627px',
  backgroundColor: white,
  height: '552px',
  borderRadius: '5px',
  border: '0.5px solid ${mediumGrey}',
  padding: '0px',
  marginRight: '50px',
}

export const ChartCardHeadStyle: React.CSSProperties = {
  fontSize: '30px',
  color: mediumGrey,
}

export const TabCardBody = styled('div')({
  textAlign: 'center',
  textTransform: 'uppercase',
  fontWeight: 700,
  padding: '7px',
})

export const tabCardBodyCheckActive = (actived: boolean) =>
  css({
    backgroundColor: actived ? `${brightGreen2} !important` : `${white} !important`,
    color: actived ? `${white} !important` : `${mediumGrey} !important`,
  })

export const ghostButtonStyle: React.CSSProperties = {
  borderRadius: '5px',
  width: '130px',
  height: '35px',
  border: `1px solid ${brownGrey}`,
  backgroundColor: transparent,
  color: `${brownGrey} !important`,
  position: 'absolute',
  //TAO TODO: not work on ie11 under flex children
  left: '90%',
}

export const SortByText = styled('span')({
  color: `${brownGrey} !important`,
})

export const SortByIcon: React.CSSProperties = {
  color: `${brownGrey} !important`,
  width: '18px',
  height: '13px',
}
