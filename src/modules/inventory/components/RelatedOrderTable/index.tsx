/** @jsx jsx */
import React, { useState, useEffect } from 'react'
import { jsx } from '@emotion/core'
import { Table, Icon, Button } from 'antd'
import { ColumnProps } from 'antd/es/table'
import { Link } from 'react-router-dom'
import { connect } from 'redux-epics-decorator'

import Tag from '~/components/Tag'
import StatusSelector from '~/components/StatusSelector'
import { BoldText, DateText, AmountText } from '~/components/Typo'

import { GlobalState } from '~/store/reducer'
import { OrderItem} from '~/schema'
import { InventoryModule, InventoryDispatchProps, InventoryStateProps } from '../../inventory.module'

import { RootDiv, title, bold, TitleWrapper, tableOverrideCss, buttonStyle, Stock, polygonCss } from './style'

const StatusSelect: React.SFC<{
  status: string
}> = (props) => {
  const [status, changeStatus] = useState(props.status)
  return (
    <StatusSelector onClick={changeStatus}>
      <div>
        <Tag>
          {status} <Icon type="down" />
        </Tag>
      </div>
    </StatusSelector>
  )
}

const RelatedOrderTable: React.SFC<
  InventoryDispatchProps & {
    relatedOrders: InventoryStateProps['relatedOrders']
    saleItemId: string
  }
> = (props) => {
  useEffect(() => {
    // get data in cdm
    props.getRelatedOrders(props.saleItemId)
  }, [])

  const columns: ColumnProps<OrderItem>[] = [
    {
      title: 'Order ID',
      dataIndex: 'wholesaleOrderItemId',
      key: 'wholesaleOrderItemId',
      width: 80,
      render: (text: string) => <BoldText>{text}</BoldText>,
    },
    {
      title: 'Date Placed',
      dataIndex: 'createdDate',
      key: 'createdDate',
      width: 100,
      render: (text: string) => <DateText>{text}</DateText>,
    },
    {
      title: 'Date Arriving',
      dataIndex: 'deliveryDate',
      key: 'deliveryDate',
      width: 100,
      render: (text: string) => <DateText>{text}</DateText>,
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'quantity',
      width: 80,
      render(value, orderItem) {
        let text = value
        if (orderItem.status === 'PLACED')
          text = -value
        return (
          <Stock stock={Number(text)}>
            {text}
            <Icon type="polygon" css={polygonCss} />
          </Stock>
        )
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      width: 120,
      render: (text: string) => <StatusSelect status={text} />,
    },
    {
      title: 'Cost',
      dataIndex: 'cost',
      key: 'cost',
      width: 80,
      render: (text: number) => <AmountText>{text > 0 ? (`$${text}`) : ''}</AmountText>,
    },
    {
      title: 'Price Sold',
      dataIndex: 'price',
      key: 'price',
      width: 80,
      render: (text: number) => <AmountText>{text > 0 ? (`$${text}`) : ''}</AmountText>,
    },
    /*
    {
      title: '',
      key: 'action',
      width: 120,
      render: (_, order) => (
        <Link to={`/order/${order.linkToken}`}>
          <PrimaryText>
            VIEW ORDER&nbsp;&nbsp;
            <Icon type="arrow-right" />
          </PrimaryText>
        </Link>
      ),
    },
    */
  ]
  return (
    <RootDiv>
      <TitleWrapper>
        <div css={[title, bold]}>Orders Placed</div>

        <Link to={`/inventory/item/stock/${props.saleItemId}`}>
          <Button size="large" type="primary" css={buttonStyle}>
            View Detail
          </Button>
        </Link>
        {
          /*
        <div css={title}>
          <span>Total Quantity Available:</span>
          <strong css={bold}>&nbsp;{props.relatedOrders.length}</strong>
        </div>
          */
        }
      </TitleWrapper>
      <Table rowKey="wholesaleOrderItemId" pagination={{ pageSize: 4 }} columns={columns} css={tableOverrideCss} dataSource={props.relatedOrders[props.saleItemId]} />
    </RootDiv>
  )
}

export default connect(InventoryModule)((state: GlobalState) => ({
  relatedOrders: state.inventory.relatedOrders,
}))(RelatedOrderTable)
