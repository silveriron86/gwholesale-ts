import { css } from '@emotion/core'
import styled from '@emotion/styled'

import { mediumGrey, brownGrey, brightGreen, white, yellow, mutedGreen, red } from '~/common'

export const title = css`
  font-size: 24px;
  line-height: 26px;
  color: ${mediumGrey};
`

export const bold = css`
  font-weight: bold;
`

export const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 70px;
`

export const RootDiv = styled.div`
  padding: 40px 150px;
`

export const buttonStyle = css({
  borderRadius: '200px',
  height: '40px',
  border: `1px solid ${brownGrey}`,
  '&.ant-btn-primary': {
    backgroundColor: brightGreen,
    borderColor: brightGreen,
    width: '180px',
  },
  '&.ant-dropdown-trigger': {
    borderRadius: '5px',
    borderColor: brownGrey,
  },
  '& > a': {
    '&:visited': {
      color: white,
    },
  },
})

export const Stock = styled('div')(({ stock }: { stock: number }) => ({
  borderLeft: `solid 5px ${stock === 0 ? yellow : stock > 0 ? mutedGreen : red}`,
  height: '17px',
  display: 'flex',
  fontWeight: 700,
  alignItems: 'center',
  justifyContent: 'center',
  paddingLeft: '10px',
}))

export const polygonCss = css({
  width: '12px',
  height: '12px',
  marginLeft: '5px',
})

export const tableOverrideCss = css({
  '& .ant-table-content': {
    '& .ant-table-body': {
      '& > table': {
        borderLeftWidth: 0,
        borderTopWidth: 0,
        borderRightWidth: 0,
        '& .ant-table-thead': {
          boxShadow: 'none',
          '& > tr > th:first-of-type': {
            paddingLeft: 15,
          },
          '& > tr > th': {
            backgroundColor: 'transparent',
            borderBottomWidth: 1,
            '& > div': {
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            },
          },
        },
        '& .ant-table-tbody > tr > td': {
          borderRightWidth: 0,
          '& > *': {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          },
        },
      },
    },
  },
})
