import React from 'react'
import { AutoComplete, Icon, Select, Tooltip } from 'antd'
import { of } from 'rxjs'
import {
  Flex,
  ThemeButton,
  ThemeModal,
  ThemeSpin,
  ThemeTable,
  ThemeTextButton,
  ThemeInput,
  ThemeSelect,
  ThemeIconButton,
} from '~/modules/customers/customers.style'
import { basePriceToRatioPrice, formatItemDescription, initHighlightEvent } from '~/common/utils'
import { EnterPurchaseWrapper } from '~/modules/orders/components/orders-header.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, responseHandler } from '~/common/utils'

type AdjustInventoryProps = {
  visible: boolean
  handleOk: Function
  handleCancel: Function
}

export default class AdjustInventoryModal extends React.PureComponent<AdjustInventoryProps> {
  state = {
    productSKUs: [],
    currentPage: 1,
    defaultActive: false,
    loading: false,
    items: [],
    warning: false,
  }

  constructor(props: AdjustInventoryProps) {
    super(props)
  }

  componentDidMount() {}

  onSelectSKU = (wholesaleItemId, option) => {
    let items = JSON.parse(JSON.stringify(this.state.items))
    // to sales cart
    const { saleItems, sellerSetting } = this.props

    const found = saleItems.find((item) => {
      return item.itemId == wholesaleItemId
    })

    if (found) {
      const { inventoryUOM, variety, SKU } = found
      this.getItemLots(wholesaleItemId, (lots: any) => {
        const availableLots = lots.filter((lot) => lot.onHandQty > 0 && lot.lotAvailableQty > 0)
        if (availableLots.length > 0) {
          items.push({
            wholesaleItemId,
            wholesaleOrderItemId:  availableLots[0].wholesaleOrderItemId,
            inventoryUOM,
            type: 2,
            quantity: 0,
            reason: '',
            variety,
            SKU,
            lots: availableLots,
          })

          this.setState({
            items,
            productSKUs: [],
            warning: false
          })
        } else {
          this.setState({
            warning: true
          })
        }
      })
    }
  }

  getItemLots = (itemId: any, cb: Function) => {
    const { vendorId } = this.state
    const _this = this
    this.changeLoadingStatus(true)
    OrderService.instance.getItemLot(itemId).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        cb(res.body.data)
      },
      error(err) {
        checkError(err)
      },
      complete() {
        _this.changeLoadingStatus(false)
      },
    })
  }

  changeLoadingStatus = (flag: boolean) => {
    this.setState({
      loading: flag,
    })
  }

  onProductSearch = (searchStr: string, type: string) => {
    if (searchStr.length < 2) {
      this.setState({
        productSKUs: [],
      })
      return
    }

    const searchText = searchStr.toLowerCase()
    const { saleItems } = this.props
    // tslint:disable-next-line:prefer-const
    let matches1: any[] = []
    let matches2: any[] = []
    let matches3: any[] = []

    if (saleItems && saleItems.length > 0) {
      saleItems.forEach((item, index) => {
        let row = item
        if (typeof item.wholesaleItem !== 'undefined') {
          row = { ...item, ...item.wholesaleItem }
        }
        if (typeof row.SKU === 'undefined') {
          row.SKU = row.sku
        }

        if (row[type] && row[type].toLowerCase().indexOf(searchText) === 0) {
          // full match on 2nd filed
          matches2.push(this.formartOption(row, type))
        } else if (row.variety && row.variety.toLowerCase().indexOf(searchText) === 0) {
          // full match on variety
          matches1.push(this.formartOption(row, type))
        } else if (
          (row[type] && row[type].toLowerCase().indexOf(searchText) > 0) ||
          (type === 'SKU' && row.variety && row.variety.toLowerCase().indexOf(searchText) > 0) ||
          (row.wholesaleCategory && row.wholesaleCategory.name.toLowerCase().indexOf(searchText) >= 0)
        ) {
          // others
          matches3.push(this.formartOption(row, type))
        }
      })
    }
    if (matches1.length > 0) {
      matches1 = matches1.sort((a, b) => a.text.localeCompare(b.text))
    }
    if (matches3.length > 0) {
      matches3 = matches3.sort((a, b) => a.text.localeCompare(b.text))
    }

    // When searches items, Autocomplete automatically focuses on the last 1st item.
    // Fix: It always should focus on the 1st row regardless of last one.
    this.setState({
      productSKUs: [],
      defaultActive: false,
    })

    setTimeout(() => {
      this.setState({
        productSKUs: [...matches2, ...matches1, ...matches3],
      })
      if (this.timeoutHandler) {
        clearTimeout(this.timeoutHandler)
      }

      this.timeoutHandler = setTimeout(() => {
        this.setState({
          defaultActive: true,
        })
      }, 200)
    }, 50)
  }

  formartOption = (row: any, type: string) => {
    return {
      value: row.itemId,
      text: type === 'SKU' ? `${row.variety} ${row[type] ? '(' + row[type] + ')' : ''}` : row[type],
    }
  }

  onPageChange = (page: number) => {
    this.setState({ currentPage: page })
  }

  onChange = (value: any, type: string, index: number, fromKeyEvent: boolean = false) => {
    if (this.isKeyboardNavigate && !fromKeyEvent) return
    let items = JSON.parse(JSON.stringify(this.state.items))
    items[index][type] = value
    if (fromKeyEvent) {
      items[index]['fromKeyEvent'] = true
    }
    this.setState({
      items,
    })
  }

  onDelete = (itemId: number, index: number) => {
    let items = JSON.parse(JSON.stringify(this.state.items))
    items.splice(index, 1)
    this.setState({
      items,
    })
  }

  onPostAdjustments = () => {
    const { items } = this.state
    let itemList = [];
    items.forEach(item => {
      const {wholesaleItemId, wholesaleOrderItemId, type, quantity, reason} = item
      itemList.push({
        wholesaleItemId,
        wholesaleOrderItemId,
        type,
        quantity,
        reason,
      })
    })

    this.props.startPostAdjustment();
    this.props.postAdjustments({
      itemList
    })
    this.setState({
      items: []
    })
  }

  componentWillReceiveProps(nextProps: AdjustInventoryProps) {
    if (this.props.inPostingAdjustments === true && this.props.inPostingAdjustments !== nextProps.inPostingAdjustments) {
      this.props.handleCancel()
    }
  }

  render() {
    const { loading, productSKUs, items, defaultActive, currentPage, warning } = this.state
    const { visible, handleOk, handleCancel, inPostingAdjustments } = this.props
    const columns: any = [
      {
        title: 'Product name (SKU)',
        dataIndex: 'variety',
        render: (variety: string, record: any, index: number) => {
          return (
            <Flex className="v-center">
              {record.wholesaleItemId === -1 ? (
                <AutoComplete
                  size="large"
                  style={{ width: '100%' }}
                  placeholder="Add Item..."
                  dataSource={productSKUs}
                  onSelect={this.onSelectSKU}
                  onFocus={() => this.onProductSearch('', 'SKU')}
                  onSearch={(v) => this.onProductSearch(v, 'SKU')}
                  dropdownClassName="enter-po-autocomplete"
                  defaultActiveFirstOption={defaultActive}
                />
              ) : (
                <Tooltip title={`${variety} (${record.SKU})`} placement="bottom">
                  {`${variety} (${record.SKU})`}
                </Tooltip>
              )}
            </Flex>
          )
        },
      },
      {
        title: 'Lot',
        dataIndex: 'wholesaleOrderItemId',
        render: (wholesaleOrderItemId: number, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }

          return (
            <ThemeSelect
              className="lot-selector"
              defaultValue={wholesaleOrderItemId}
              suffixIcon={<Icon type="caret-down" />}
              onChange={(val: any) => {
                this.onChange(val, 'wholesaleOrderItemId', (currentPage - 1) * 12 + index)
              }
              style={{ minWidth: 80, width: '100%' }}
              dropdownRender={(el: any) => {
                return (
                  <div className={`adjust-inventory-selector-lot-${(currentPage - 1) * 12 + index}`}>{el}</div>
                )
              }}
            >
              {record.lots != null && record.lots.length > 0
                ? record.lots.map((l) => {
                    return (
                      <Select.Option key={l.wholesaleOrderItemId} value={l.wholesaleOrderItemId} title={l.lotId}>
                        {l.lotId}{l.modifiers ? `(${l.modifiers})` : ''}
                      </Select.Option>
                    )
                  })
                : ''}
            </ThemeSelect>
          )
        }
      },
      {
        title: 'Unit adjustment',
        dataIndex: 'quantity',
        render: (quantity: number, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return (
            <Flex className="v-center">
              <ThemeInput
                value={quantity}
                style={{width: 70, marginRight: 5}}
                onChange={(e) => this.onChange(e.target.value, 'quantity', (currentPage - 1) * 12 + index)}
              />
              {record.inventoryUOM}
            </Flex>
          )
        },
      },
      {
        title: 'Reason for adjustment',
        dataIndex: 'reason',
        render: (reason: string, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }

          return (
            <Flex className="v-center">
              <ThemeInput
                value={reason}
                onChange={(e) => this.onChange(e.target.value, 'reason', (currentPage - 1) * 12 + index)}
              />
            </Flex>
          )
        },
      },
      {
        title: '',
        dataIndex: 'wholesaleItemId',
        render: (wholesaleItemId: number, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return (
            <ThemeIconButton
              className="no-border remove-purchase-item"
              onClick={() => this.onDelete(wholesaleItemId, (currentPage - 1) * 12 + index)}
            >
              <Icon type="close" />
            </ThemeIconButton>
          )
        },
      },
    ]

    const dataSource = [
      ...items,
      ...[
        {
          wholesaleItemId: -1,
          wholesaleOrderItemId:  null,
          inventoryUOM: '',
          type: 2,
          quantity: 0,
          reason: '',
          variety: '',
          SKU: '',
          lots: [],
        },
      ],
    ]

    return (
      <ThemeModal
        width={'75%'}
        style={{ minWidth: 1000, maxHeight: '95vh' }}
        className="enter-purchase-modal"
        title="Adjust inventory"
        okText="Post adjustments"
        visible={visible}
        maskClosable={false}
        onCancel={handleCancel}
        footer={
          <Flex>
            <ThemeButton type="primary" onClick={this.onPostAdjustments} disabled={items.length === 0 || loading || inPostingAdjustments}>
              Post adjustments
            </ThemeButton>
            <ThemeTextButton type="link" className="cancel-btn" onClick={handleCancel}>
              Cancel
            </ThemeTextButton>
          </Flex>
        }
      >
        <EnterPurchaseWrapper>
          <p>Enter adjustments to units on hand.</p>

          <ThemeSpin spinning={loading || inPostingAdjustments}>
            <ThemeTable
              dataSource={dataSource}
              columns={columns}
              pagination={{
                hideOnSinglePage: true,
                pageSize: 12,
                current: currentPage,
                onChange: this.onPageChange,
              }}
            />
          </ThemeSpin>
          {warning && (
            <p className="warning">Warning: Item has no inventory/lot to adjust</p>
          )}
        </EnterPurchaseWrapper>
      </ThemeModal>
    )
  }
}
