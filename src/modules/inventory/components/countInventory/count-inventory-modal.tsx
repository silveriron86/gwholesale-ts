import { Button, Checkbox, Input, Spin, Select, Table, Tooltip, notification, Switch, Icon } from 'antd'
import React, { useEffect, useState, useMemo, useCallback, useRef } from 'react'
import { Flex, ThemeModal } from '~/modules/customers/customers.style'
import CSVDownload from './CSVDownload'
import { InventoryService } from '~/modules/inventory/inventory.service'
import { printWindow, warehouseSyncParseAlgorithm, sorter } from '~/common/utils'
import './countInventory.css'
import styled from '@emotion/styled'
import moment from 'moment'
import { StyledTip, StyledSpan } from './tables/style'
import InputNumber from '~/components/Input/InputNumber'
import { isEmpty } from 'lodash'
import { confirmColumns } from './tables/util'
import { bignumber, formatBn } from '~/common/mathjs'

const { Option } = Select
const countQty = 'countQty'
const isBlank = (val: number | string) => {
  return !val && val !== 0
}

type ItemType = {
  SKU: string
  baseUOM: string | null
  cost: number
  defaultCost: number
  inventoryUOM: string
  location: string | null
  lotId: string
  palletQty: number
  palletUnits: number
  unitsOnHand: number
  variety: string
  wholesaleItemId: string
  wholesaleOrderItemId: string
  tableProductName: string
  palletNumber: string
  category: string
  countQty?: number | string
  receivedDate: number
  itemLocationName: string
}

enum SortType {
  ALPHABETICAL = 'ALPHABETICAL',
  LOCATION = 'LOCATION',
}

enum CategoryType {
  Lot = 'Lot',
  Location = 'Location',
  Pallets = 'Pallets',
  All = 'All',
}

interface IProps {
  visible: boolean
  onCancel: () => void
  propData: any
}

type CountMap = {
  [key: number | string]: number | string
}

const CountInventoryModal = ({ visible, onCancel, propData }: IProps): JSX.Element => {
  const [loading, setLoading] = useState(false)
  const [allLots, setAllLots] = useState<ItemType[]>([])
  const [sort, setSort] = useState<SortType>(SortType.ALPHABETICAL)
  const [category, setCategory] = useState<string>(CategoryType.All)
  const [systemUnitShow, setSystemUnitShow] = useState<boolean>(true)
  const [confirmVisible, setConfirmVisible] = useState<boolean>(false)
  const [scanMode, setScanMode] = useState<boolean>(false)
  const [scanedCode, setScanedCode] = useState<string>('')
  const [searchStr, setSearchStr] = useState<string>('')
  const [isWrongCode, setIsWrongCode] = useState<boolean>(false)
  const [categories, setCategories] = useState<string[]>([])
  const [clearOutChecked, setClearOutChecked] = useState<boolean>(false)
  const [showUnderstood, setShowUnderstood] = useState(false)
  const [understood, setUnderstood] = useState(false)
  const [countMap, setCountMap] = useState<CountMap>({})
  const [autoCountMap, setAutoCountMap] = useState<CountMap>({})
  const [commiting, setCommiting] = useState(false)
  const ref = useRef<any>(() => void 0)
  const locationMethod = propData?.userSetting?.company?.locationMethod

  useEffect(() => {
    const categoriesItems: string[] = Array.from(new Set(propData.saleCategories.map((item: any) => item.name)))
    setCategories(categoriesItems.sort())
  }, [propData.saleCategories])

  useEffect(() => {
    // use visible to make sure this modal can get the latest data when modal open
    if (visible) {
      setLoading(true)
      ref.current()
    }
  }, [visible])

  ref.current = () => {
    InventoryService.instance.getAllLotsItems<ItemType>().subscribe({
      next(res: any) {
        const allLotItems = res?.body?.data || []
        const itemsWithCategory: ItemType[] = allLotItems.map((lotItem: ItemType) => {
          const saleItem = propData.saleItems.find((s: any) => s.itemId == lotItem.wholesaleItemId)
          lotItem.category = saleItem?.category
          lotItem.tableProductName = lotItem.SKU
            ? `${lotItem.variety || ''} (${lotItem.SKU})`
            : `${lotItem.variety || ''}`
          return lotItem
        })
        setAllLots(itemsWithCategory)
      },
      error(err) {
        console.log(err)
        notification.error({
          message: 'ERROR',
          description: 'server error when get lot items',
        })
      },
      complete() {
        setLoading(false)
      },
    })
  }

  const onCountChange = useCallback((val: string, record: any) => {
    const { lotId } = record
    setCountMap((pre) => {
      if (val === '' || val === undefined || val === null) {
        delete pre[lotId]
      } else {
        pre[lotId] = val || 0
      }
      return { ...pre }
    })
  }, [])

  const columns = useMemo(() => {
    const prdName = {
      title: 'Product name (SKU)',
      dataIndex: 'tableProductName',
      key: 'tableProductName',
      className: 'tableTbInventory',
      render: (value: string, record: ItemType) => {
        return <p style={{ marginBottom: '0px', maxWidth: 220 }}>{record.tableProductName?.replace('()', '')}</p>
      },
    }

    const locationColumn = {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
      className: 'tableTbInventory',
      render: (value: string, record: ItemType) => {
        return <p style={{ marginBottom: '0px', maxWidth: 220 }}>{record.location}</p>
      },
    }

    const lotColumn = {
      title: 'Lot',
      dataIndex: 'Lot',
      key: 'Lot',
      className: 'tableTbInventorylot',
      render: (value: string, record: ItemType) => {
        return <p style={{ marginBottom: '0px' }}>{record.lotId}</p>
      },
    }

    const palletColumn = {
      title: 'Pallets',
      dataIndex: 'Pallets',
      key: 'Pallets',
      className: 'tableTbInventory',
      render: (value: string, record: ItemType) => {
        return <p style={{ marginBottom: '0px' }}>{record.palletQty}</p>
      },
    }

    const sysOnHandColumnArr = systemUnitShow
      ? [
          {
            title: 'System units on hand',
            dataIndex: 'System units on hand',
            key: 'System units on hand',
            className: 'tableTbInventory',
            render: (value: string, record: ItemType) => {
              return (
                <p style={{ marginBottom: '0px' }}>{`${
                  record.unitsOnHand % 1 == 0 ? record.unitsOnHand : record.unitsOnHand.toFixed(2)
                } ${record.inventoryUOM}`}</p>
              )
            },
          },
        ]
      : []

    const countedColumn = {
      title: 'Counted units on hand',
      dataIndex: countQty,
      key: 'Counted units on hand',
      className: 'tableTbInventory',
      render: (value: string, record: ItemType) => {
        const ifChanged = value && +value !== record.unitsOnHand

        return (
          <div style={{ display: 'flex' }}>
            <InputNumber
              style={{ width: '80px', marginRight: '11px' }}
              value={value}
              onChange={((val: string) => onCountChange(val, record)) as any}
              place2
            />
            <p style={{ marginTop: '5px', width: '106px', marginBottom: '0px' }}>
              {record.inventoryUOM}
              {ifChanged ? (
                <Tooltip
                  style={{ marginLeft: '20px' }}
                  title="Counted units on hand does not match system unites on hand"
                >
                  <span style={{ float: 'right' }}>
                    <img style={{ width: '15px', height: '15px', marginRight: '10px' }} src={require('./alert.png')} />
                  </span>
                </Tooltip>
              ) : (
                <></>
              )}
            </p>
          </div>
        )
      },
    }

    const receiveDateColumn = {
      title: 'Receive Date',
      dataIndex: 'receivedDate',
      key: 'receivedDate',
      className: 'tableTbInventorylot',
      render: (value: string) => {
        return value ? moment(value).format('MM/DD/YYYY') : ''
      },
    }

    const alphabeticalColumns = [
      prdName,
      lotColumn,
      receiveDateColumn,
      locationColumn,
      palletColumn,
      ...sysOnHandColumnArr,
      countedColumn,
    ]
    const locationColumns = [locationColumn, prdName, lotColumn, palletColumn, ...sysOnHandColumnArr, countedColumn]

    return sort === SortType.ALPHABETICAL ? alphabeticalColumns : locationColumns
  }, [onCountChange, sort, systemUnitShow])

  const printHandle = () => {
    printWindow('printInventory')
  }

  const sendRecountDate = async () => {
    if (clearOutChecked && !showUnderstood) {
      setShowUnderstood(true)
      return
    }
    const itemList: any = []
    const allCountMap = { ...countMap, ...autoCountMap }
    const date = moment().format('MM/DD/YYYY')
    allLots.forEach((lot) => {
      const count = allCountMap[lot.lotId]
      if (!isBlank(count)) {
        itemList.push({
          wholesaleItemId: lot.wholesaleItemId,
          wholesaleOrderItemId: lot.wholesaleOrderItemId,
          type: 2,
          quantity: +count - lot.unitsOnHand,
          reason: `Inventory Count on ${date}`,
        })
      }
    })

    setCommiting(true)
    InventoryService.instance
      .postAdjustments({
        itemList,
      })
      .subscribe({
        complete() {
          setCommiting(false)
          setShowUnderstood(false)
          setConfirmVisible(false)
          reset()
        },
      })
  }

  const dataSource = useMemo(() => {
    let res: ItemType[] = []
    allLots.forEach((item) => {
      if (category !== CategoryType.All && item.category != category) {
        return
      }
      if (searchStr && !scanMode) {
        const pass =
          (item.SKU && item.SKU.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) ||
          (item.variety && item.variety.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) ||
          (item.location && item.location.toLowerCase().indexOf(searchStr.toLowerCase()) > -1) ||
          (item.lotId && item.lotId.toLowerCase().indexOf(searchStr.toLowerCase()) > -1)
        if (!pass) {
          return
        }
      }
      if (scanMode && !isWrongCode) {
        const pass = item.palletNumber === scanedCode
        if (!pass) {
          return
        }
      }

      res.push({
        ...item,
        countQty: countMap[item.lotId],
        location: locationMethod == 1 ? item.itemLocationName : item.location,
      })
    })
    if (sort === SortType.ALPHABETICAL) {
      res = res.sort((a, b: any) => sorter(a.tableProductName, b.tableProductName))
    } else {
      res = res.sort((a, b: any) => sorter(a.location, b.location))
    }
    return res
  }, [allLots, category, countMap, isWrongCode, locationMethod, scanMode, scanedCode, searchStr, sort])

  const getPrintTable = () => {
    if (loading) {
      return <Spin size="large" />
    } else {
      return <Table rowKey="lotId" dataSource={dataSource} pagination={false} columns={columns} />
    }
  }

  const getTotal = () => {
    const allCountMap = { ...countMap, ...autoCountMap }

    const result = allLots.reduce((pre, cur) => {
      const newCount = +allCountMap[cur.lotId]
      return isBlank(newCount)
        ? pre
        : bignumber(newCount)
            .sub(cur.unitsOnHand)
            .mul(cur.defaultCost)
            .add(pre)
    }, bignumber(0))

    return formatBn(result, { comma: true, $: true, fixed: 2, showPlus: true })
  }

  const reset = () => {
    setCommiting(false)
    setCountMap({})
    setAutoCountMap({})
    onCancel()
  }

  const onFocus = (e: any) => {
    if (!scanMode) {
      return
    }
    e.target.select()
  }

  const onSearchChange = (e: any) => {
    if (!scanMode) {
      return
    }
    const search = e.target.value
    onChangeSearchStr(search)
  }

  const onChangeSearchStr = (search: string) => {
    setSearchStr(search)
    if (!scanMode) {
      setScanedCode('')
    } else {
      if (search.startsWith('WSW', 4)) {
        const parseValue = warehouseSyncParseAlgorithm(search)
        setScanedCode(parseValue)
        setIsWrongCode(false)
      } else {
        setScanedCode('')
        setIsWrongCode(true)
      }
    }
  }

  const onClearOutChange = (e: { target: { checked: boolean } }) => {
    const checked = e.target.checked
    setClearOutChecked(checked)
    if (!checked) {
      setAutoCountMap({})
      return
    }

    // Count units whose onhand is blank and system units onhand !=0
    const newAutoCountMap = {}
    allLots.forEach((item) => {
      const count = countMap[item.lotId]
      if (isBlank(count) && item.unitsOnHand) {
        newAutoCountMap[item.lotId] = -item.unitsOnHand
      }
    })
    setAutoCountMap(newAutoCountMap)
  }

  const getDownloadData = dataSource.map((item) => {
    let count: number | string = countMap[item.lotId]
    count = isBlank(count) ? '' : count
    const systemUnitColumn = systemUnitShow ? [item.unitsOnHand] : []
    if (sort == SortType.ALPHABETICAL) {
      return [
        item.tableProductName,
        item.lotId,
        item.receivedDate ? moment(item.receivedDate).format('MM/DD/YYYY') : '',
        item.location || '',
        item.palletQty,
        ...systemUnitColumn,
        count,
      ]
    }
    return [item.location || '', item.tableProductName, item.lotId, item.palletQty, [...systemUnitColumn], count]
  })

  const getConfirmContext = () => {
    const allCountMap = { ...countMap, ...autoCountMap }
    if (isEmpty(allCountMap)) {
      return (
        <div>
          <div style={{ height: 'auto', padding: 19 }}>
            <div className="textDiv" style={{ marginTop: 20 }}>
              There were no discrepancies between your counted units and the current system on hand unit values. No
              adjustments will be made to your system’s inventory values.
            </div>
          </div>
          <div style={{ padding: '0 19px', marginBottom: 50 }}>
            <Tooltip title="Check this option if you want the system to adjust quantities for all lots that have not been counted to zero.">
              <Checkbox checked={clearOutChecked} onChange={onClearOutChange}>
                Clear out uncounted lots
                <Icon type="info-circle" style={{ color: '#1C6E31', fontSize: 16, marginLeft: 5 }} />
              </Checkbox>
            </Tooltip>
          </div>
          <div style={{ display: 'flex', backgroundColor: '#F7F7F7', padding: '19px 24px' }}>
            <Button
              loading={commiting}
              style={{ backgroundColor: '#1C6E31', color: 'white', marginRight: '24px' }}
              onClick={sendRecountDate}
            >
              Complete count
            </Button>
            <Button
              style={{ backgroundColor: 'white', color: '#1C6E31', border: '1px white solid' }}
              onClick={() => setConfirmVisible(false)}
            >
              Cancel
            </Button>
          </div>
        </div>
      )
    }
    const comfirmDataSource: any[] = []
    allLots.forEach((lot) => {
      const count = countMap[lot.lotId]
      const autoCount = autoCountMap[lot.lotId]
      if (!isBlank(count) || !isBlank(autoCount)) {
        if (!isBlank(count)) {
          // put at beginning
          comfirmDataSource.unshift({
            wholesaleOrderItemId: lot.wholesaleOrderItemId,
            product: lot.tableProductName,
            lot: lot.lotId,
            adjustment: +count - lot.unitsOnHand,
            valuationImpact: (+count - lot.unitsOnHand) * lot.defaultCost,
          })
        } else {
          comfirmDataSource.push({
            wholesaleOrderItemId: lot.wholesaleOrderItemId,
            product: lot.tableProductName,
            lot: lot.lotId,
            adjustment: +autoCount - lot.unitsOnHand,
            valuationImpact: (+autoCount - lot.unitsOnHand) * lot.defaultCost,
          })
        }
      }
    })
    return (
      <>
        <div className="textDiv" style={{ margin: '20px 0', padding: '19px' }}>
          The current system on hand unit values do not match the counts entered for the following items. Click “Update
          Inventory” to update your system’s values.
        </div>
        <div style={{ padding: '0 19px' }}>
          <Tooltip title="Check this option if you want the system to adjust quantities for all lots that have not been counted to zero.">
            <Checkbox checked={clearOutChecked} onChange={onClearOutChange}>
              Clear out uncounted lots
              <Icon type="info-circle" style={{ color: '#1C6E31', fontSize: 16, marginLeft: 5 }} />
            </Checkbox>
          </Tooltip>
        </div>

        <div style={{ padding: '19px 24px' }}>
          <Table
            columns={confirmColumns}
            dataSource={comfirmDataSource}
            rowKey="lot"
            pagination={(comfirmDataSource.length > 10) as any}
          />
          <div style={{ borderTop: '1px black solid', display: 'flex' }}>
            <p style={{ width: '80%', fontWeight: 'normal' }}>Total Impact</p>
            <p style={{ width: '20%', float: 'right', textAlign: 'left', fontWeight: 'normal' }}>{getTotal()}</p>
          </div>
        </div>
        <div style={{ display: 'flex', backgroundColor: '#F7F7F7', padding: '19px 24px' }}>
          <Button
            loading={commiting}
            style={{ backgroundColor: '#1C6E31', color: 'white', marginRight: '24px' }}
            onClick={sendRecountDate}
          >
            Update inventory
          </Button>
          <Button
            style={{ backgroundColor: 'white', color: '#1C6E31', border: '1px white solid' }}
            onClick={() => {
              setConfirmVisible(false)
            }}
          >
            Cancel
          </Button>
        </div>
      </>
    )
  }

  return (
    <>
      <InventoryModal
        width={'1031px'}
        style={{ minWidth: 1000, maxHeight: '95vh' }}
        visible={visible}
        onCancel={reset}
        className="uploadInventoryModel"
        title="Enter inventory count"
        footer={[]}
        maskClosable={false}
      >
        <div style={{ padding: 20 }}>
          <div className="textDiv">
            Enter the results from your inventory count below. Upon clicking “Save updates”, the below entered values
            will be saved as the current “on hand” inventory (blank cells will not update existing values). Any
            necessary inventory adjustments will be automatically applied.
          </div>
          <Flex style={{ width: '80%', marginTop: 15 }}>
            <Input.Search
              style={{ flex: 3, marginRight: 16 }}
              onSearch={onChangeSearchStr}
              onChange={onSearchChange}
              className="inventory-count-search"
              onFocus={onFocus}
            />
            <div>
              <label>Scan Mode</label>
              <Switch
                checked={scanMode}
                onChange={(val) => {
                  setScanMode(val)
                  // setIsWrongCode(false)
                }}
              ></Switch>
            </div>
          </Flex>
          {isWrongCode && <div className="warning custom-pad12">Error: Wrong code scanned.</div>}
          <div style={{ display: 'flex', marginTop: '15px', marginBottom: '4px' }}>
            <p className="sortDiv" style={{ width: '200px', fontWeight: 'normal' }}>
              Sort by
            </p>
            <p className="sortDiv" style={{ width: '200px', marginLeft: '32px', fontWeight: 'normal' }}>
              Categories
            </p>
          </div>
          <div style={{ display: 'flex', width: '983px' }}>
            <Select
              style={{ width: '200px', fontWeight: 'normal' }}
              value={sort}
              placeholder="SortBy"
              optionFilterProp="children"
              onChange={(val: any) => setSort(val)}
            >
              <Option key={1} value={SortType.ALPHABETICAL} style={{ fontWeight: 'normal' }}>
                ALPHABETICAL
              </Option>
              <Option key={2} value={SortType.LOCATION} style={{ fontWeight: 'normal' }}>
                LOCATION
              </Option>
            </Select>
            <Select
              style={{ width: '200px', marginLeft: '32px', fontWeight: 'normal' }}
              value={category}
              optionFilterProp="children"
              onChange={(val: string) => setCategory(val)}
            >
              <Option key={'All'} value={`All`} style={{ fontWeight: 'normal' }}>
                All
              </Option>
              {categories.map((item: string) => {
                return (
                  <Option key={item} value={item} style={{ fontWeight: 'normal' }}>
                    {item}
                  </Option>
                )
              })}
            </Select>
            <Checkbox
              style={{ marginLeft: '18px', fontWeight: 'normal', width: '260px' }}
              checked={systemUnitShow}
              onChange={(e: any) => setSystemUnitShow(e.target.checked)}
            >
              Display system on hand units
            </Checkbox>
            <CSVDownload
              arr={getDownloadData as string[][]}
              headerArr={columns.map((item) => item.title)}
              className="downloadInventoryCss"
              fileName="inventoryCountSheet.csv"
            />
            <Button style={{ marginLeft: '10px', width: '93px' }} onClick={() => printHandle()}>
              <img style={{ width: '15px', height: '15px', marginRight: '5px' }} src={require('./printp.png')} />
              Print
            </Button>
          </div>
          <div>
            {loading ? (
              <Spin size="large" />
            ) : (
              <Table
                rowKey="lotId"
                dataSource={dataSource}
                pagination={{
                  style: {
                    margin: '20px 0 0 0',
                    float: 'none',
                    display: 'flex',
                    justifyContent: 'center',
                  },
                  total: dataSource.length,
                }}
                columns={columns}
              />
            )}
          </div>
          <div id={'printInventory'} style={{ display: 'none' }}>
            {getPrintTable()}
          </div>
        </div>
        <div style={{ display: 'flex', backgroundColor: '#F7F7F7', padding: '19px 24px' }}>
          <Button
            style={{ backgroundColor: '#1C6E31', color: 'white', marginRight: '24px' }}
            onClick={() => setConfirmVisible(true)}
          >
            Enter counts
          </Button>
          <Button style={{ backgroundColor: 'white', color: '#1C6E31', border: '1px white solid' }} onClick={reset}>
            Cancel
          </Button>
        </div>
      </InventoryModal>

      <InventoryModal
        width={'1031px'}
        style={{ minWidth: 1031 }}
        visible={confirmVisible}
        onCancel={() => setConfirmVisible(false)}
        title={isEmpty({ ...countMap, ...autoCountMap }) ? 'Complete count' : 'Confirm inventory updates'}
        footer={[]}
        maskClosable={false}
        afterClose={() => onClearOutChange({ target: { checked: false } })}
      >
        {getConfirmContext()}
        <ThemeModal
          visible={showUnderstood}
          footer={
            <div style={{ textAlign: 'left' }}>
              <Button type="primary" disabled={!understood} onClick={sendRecountDate} loading={commiting}>
                Save and Update inventory
              </Button>
              <Button type="link" onClick={() => setShowUnderstood(false)}>
                Cancel
              </Button>
            </div>
          }
          maskClosable={false}
          afterClose={() => setUnderstood(false)}
          onCancel={() => setShowUnderstood(false)}
        >
          <StyledTip>
            If you have “Clear out uncounted” lots enabled, all lots without a Counted units on hand value will have
            quantities adjusted to zero.
          </StyledTip>
          <Checkbox checked={understood} onChange={(e: any) => setUnderstood(e.target.checked)}>
            <StyledSpan>I understand</StyledSpan>
          </Checkbox>
        </ThemeModal>
      </InventoryModal>
    </>
  )
}

export default CountInventoryModal

const InventoryModal = styled(ThemeModal)`
  .ant-modal-body {
    padding: 0px;
  }
`
