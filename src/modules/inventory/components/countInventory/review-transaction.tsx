import React, { useMemo, useRef, useState, useReducer } from 'react'
import { ThemeModal, ThemeCheckBox, ThemeButton, ThemeTextButton } from '~/modules/customers/customers.style'
import styled from '@emotion/styled'
import UnshippedSO from './tables/unshipped-so'
import UnReceivedSO from './tables/unreceived-po'
import NoLotAssignments from './tables/no-lot-assignments'
import { Button } from 'antd'
import { useTheme } from 'emotion-theming'
import { Consts } from './tables/util'
import { StyledTip, StyledSpan } from './tables/style'

const init = {
  [Consts.shippedSO]: true,
  [Consts.unReceivedPO]: true,
  [Consts.noLot]: true,
}

const initLoading = {
  [Consts.shippedSO]: true,
  [Consts.unReceivedPO]: true,
  [Consts.noLot]: true,
}

type Props = {
  visible: boolean
  onCancel: any
  onShowEnterInventory: any
}

const ReviewTransaction: React.FC<Props> = (props) => {
  const { visible, onCancel, onShowEnterInventory } = props
  const theme: any = useTheme()
  const [showConfirm, setShowConfirm] = useState(false)
  const [checked, setChecked] = useState(false)
  const reloadRef = useRef<any>({})
  const [issue, dispatchIssue] = useReducer((state: any, action: { [key in Consts]?: boolean }) => {
    return { ...state, ...action }
  }, init)
  const hasIssue = issue[Consts.shippedSO] || issue[Consts.unReceivedPO] || issue[Consts.noLot]

  const [load, dispatchLoad] = useReducer((state: any, action: { [key in Consts]?: boolean }) => {
    return { ...state, ...action }
  }, initLoading)
  const loading = load[Consts.shippedSO] || load[Consts.unReceivedPO] || load[Consts.noLot]

  const footer = useMemo(() => {
    const skip = () => {
      if (hasIssue) {
        setShowConfirm(true)
        return
      }
      onShowEnterInventory()
      onCancel()
    }

    const refresh = () => {
      const { current } = reloadRef
      current?.reload1()
      current?.reload2()
      current?.reload3()
    }
    return (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div>
          <Button onClick={skip} type="primary" loading={loading}>
            {hasIssue ? 'Skip this step' : 'Continue'}
          </Button>
          <Button onClick={onCancel}>Cancel</Button>
        </div>
        <Button onClick={refresh}>Refresh</Button>
      </div>
    )
  }, [hasIssue, loading, onCancel, onShowEnterInventory])

  const footer2 = useMemo(() => {
    const skipAnyWay = () => {
      onShowEnterInventory()
      setShowConfirm(false)
      onCancel()
    }
    return (
      <div style={{ textAlign: 'left' }}>
        <ThemeButton disabled={!checked} theme={theme} onClick={skipAnyWay}>
          Skip this step anyway
        </ThemeButton>
        <ThemeTextButton type="link" theme={theme} onClick={() => setShowConfirm(false)}>
          Cancel
        </ThemeTextButton>
      </div>
    )
  }, [checked, onCancel, onShowEnterInventory, theme])

  return (
    <ThemeModal
      onCancel={onCancel}
      visible={visible}
      title="Review your transactions"
      width="1031px"
      footer={footer}
      style={{ top: 30 }}
      maskClosable={false}
    >
      <div style={{ height: 520, paddingRight: 30, overflow: 'auto' }}>
        <StyledSumary>
          Before you begin entering counts for your inventory, please correct the transaction issues detected by the
          system below. If you decide to skip this step, your on hand and available to sell inventory may not be
          accurate after completing the inventory count process. Please use the “Refresh” button to refresh the list of
          transaction issues once you have corrected them.
        </StyledSumary>
        {visible && (
          <div>
            <UnshippedSO
              dispatchLoad={dispatchLoad}
              dispatchIssue={dispatchIssue}
              reloadFn={(fn: any) => (reloadRef.current.reload1 = fn)}
            />
            <UnReceivedSO
              dispatchLoad={dispatchLoad}
              dispatchIssue={dispatchIssue}
              reloadFn={(fn: any) => (reloadRef.current.reload2 = fn)}
            />
            <NoLotAssignments
              dispatchLoad={dispatchLoad}
              dispatchIssue={dispatchIssue}
              reloadFn={(fn: any) => (reloadRef.current.reload3 = fn)}
            />
          </div>
        )}
      </div>
      <ThemeModal
        visible={showConfirm}
        footer={footer2}
        maskClosable={false}
        afterClose={() => setChecked(false)}
        onCancel={() => setShowConfirm(false)}
        closeIcon=" "
      >
        <StyledTip>
          Skipping this step may cause your on hand and available to sell inventory to be inaccurate after the inventory
          count process is completed.
        </StyledTip>
        <ThemeCheckBox theme={theme} checked={checked} onChange={(e: any) => setChecked(e.target.checked)}>
          <StyledSpan>I understand</StyledSpan>
        </ThemeCheckBox>
      </ThemeModal>
    </ThemeModal>
  )
}

export default ReviewTransaction

const StyledSumary = styled.div`
  font-size: 15px;
  line-height: 21px;
  font-weight: 400;
`
