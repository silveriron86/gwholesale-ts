import styled from '@emotion/styled'
import { Spin } from 'antd'
import React from 'react'

type Props = {
  columns: Array<{
    title: string
    width: string | number
    dataIndex?: string
    render?: any
  }>
  dataSource: Array<any>
  loading: boolean
  loadingMore: boolean
  noMore: boolean
}

const MyTable = React.forwardRef(function MyTableRef(props: Props, ref: any) {
  const { columns, dataSource = [], loading, loadingMore, noMore } = props
  if (loading) {
    return <Spin size="large" />
  }
  return (
    <div>
      <StyleHead>
        {columns.map((item) => {
          return (
            <div key={item.title} style={{ width: item.width }}>
              {item.title}
            </div>
          )
        })}
      </StyleHead>
      <div style={{ height: 420, overflow: 'auto' }} ref={ref}>
        {dataSource.map((item, index) => {
          return (
            <StyledDiv key={item.orderNo}>
              {columns.map((c) => {
                const { render, dataIndex, width } = c
                const val = dataIndex ? item[dataIndex] : void 0
                return (
                  <div style={{ width }} key={c.title}>
                    {render ? render(val, item, index) : val}
                  </div>
                )
              })}
            </StyledDiv>
          )
        })}
        {loadingMore && (
          <StyleBottom>
            <Spin />
          </StyleBottom>
        )}
        {noMore && <StyleBottom>end</StyleBottom>}
      </div>
    </div>
  )
})

export default MyTable

const StyleHead = styled.div`
  font-size: 14px;
  line-height: 140%;
  color: #4a5355;
  background-color: #f7f7f7;
  display: flex;
  align-items: stretch;
  border-bottom: 1px solid #d8d8d8;

  > div {
    display: flex;
    height: 45px;
    align-items: center;
    flex: 0 0 auto;
    padding: 0 10px;
    &:not(:last-child) {
      border-right: 1px solid #d8d8d8;
    }
  }
`

const StyledDiv = styled.div`
  font-size: 15px;
  line-height: 140%;
  /* color: #22282a; */
  display: flex;
  align-items: stretch;
  border-bottom: 1px solid #d8d8d8;
  > div {
    display: flex;
    height: 42px;
    align-items: center;
    padding: 0 10px;
    flex: 0 0 auto;
    &:not(:last-child) {
      border-right: 1px solid #d8d8d8;
    }
  }
`

const StyleBottom = styled.div`
  padding: 10px 0;
  text-align: center;
`
