import React, { useEffect, useRef } from 'react'
import { Title, Subtitle, Transaction, NoIssue } from './style'
import { Icon } from '~/components'
import { useInfiniteScroll } from 'ahooks'
import { getList } from './util'
import moment from 'moment'
import MyTable from './my-table'
import { Link } from 'react-router-dom'
import { Consts, Props } from './util'

const columns = [
  {
    title: 'Index',
    width: 60,
    render: (_1: any, _2: any, index: any) => index + 1,
  },
  {
    title: 'Fulfillment Date',
    dataIndex: 'deliveryDate',
    width: 160,
  },
  {
    title: 'Order No.',
    dataIndex: 'orderNo',
    width: 150,
  },
  {
    title: 'Company',
    dataIndex: 'companyName',
    width: 350,
  },
  {
    title: 'Status',
    dataIndex: 'wholesaleOrderStatus',
    width: 100,
  },
  {
    title: '',
    width: 80,
    render: (_: any, _row: any) => {
      return (
        <Link target="_blank" to={`/sales-order/${_row.wholesaleOrderId}`}>
          <Icon style={{ marginLeft: 10 }} type="link" viewBox="0 0 15 15" width="16" height="16" />
        </Link>
      )
    },
  },
]

const NoLotAssignments: React.FC<Props> = (props) => {
  const pageSize = 10
  const searchType = 3
  const currentDate = moment().format('MM/DD/YYYY')
  const { dispatchIssue, dispatchLoad } = props

  const ref = useRef<HTMLDivElement>()
  const { data, loading, loadingMore, noMore, reload } = useInfiniteScroll(
    (d: any) => {
      const page = d ? Math.ceil(d.list.length / pageSize) : 0
      return getList({ currentDate, searchType, page, pageSize }) as any
    },
    {
      target: ref,
      isNoMore: (d) => d?.total === 0 || (d?.total && d?.total <= d?.list?.length),
    },
  )

  props?.reloadFn(reload)

  useEffect(() => {
    dispatchIssue({ [Consts.noLot]: !!data?.total })
  }, [data?.total, dispatchIssue])

  useEffect(() => {
    dispatchLoad({ [Consts.noLot]: loading })
  }, [dispatchLoad, loading])

  const renderContent = () => {
    if (noMore && data?.list?.length === 0) {
      return <NoIssue />
    }
    return (
      <MyTable
        ref={ref}
        columns={columns}
        dataSource={data?.list}
        loading={loading}
        loadingMore={loadingMore}
        noMore={noMore}
      />
    )
  }

  return (
    <Transaction>
      <Title>
        <span>No Lot Assignments</span>
        <Icon type="run-report" viewBox="0 0 18 18" width="18" height="18" style={{ fill: '#fff' }} />
        <span className="run-report">Run exception report</span>
      </Title>
      <Subtitle>
        Sales orders containing items without lots assigned - please assign lots to the items with “No lot selected”.
      </Subtitle>
      {renderContent()}
    </Transaction>
  )
}

export default NoLotAssignments
