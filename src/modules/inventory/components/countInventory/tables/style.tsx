import React from 'react'
import styled from '@emotion/styled'
import { Table, Icon as IconAntd } from 'antd'

export const Transaction = styled.div`
  font-weight: normal;
  font-style: normal;
`

export const Title = styled.div`
  margin-top: 40px;
  font-size: 18px;
  line-height: 140%;
  color: #434748;
  > svg {
    margin: 0 5px 0 20px;
  }
  .run-report {
    font-style: normal;
    font-weight: 600;
    font-size: 15px;
    line-height: 140%;
    color: #1c6e31;
  }
`

export const Subtitle = styled.div`
  font-size: 16px;
  line-height: 140%;
  /* color: #595959; */
  margin: 5px 0 20px 0;
`

export const StyleTable = styled(Table)`
  .ant-table-row {
    background-color: #fff !important;
  }
  .ant-table-content .ant-table-body .ant-table-tbody > tr > td {
    background-color: #fff;
  }
`

export const StyleNoIssue = styled.div`
  font-size: 15px;
  line-height: 140%;
  color: #a2a2a2;
  padding: 30px 0;
  text-align: center;
  border-bottom: 1px solid #d8d8d8;
  border-top: 1px solid #d8d8d8;
`

export const NoIssue = () => {
  return (
    <StyleNoIssue>
      <IconAntd style={{ color: '#A2A2A2', fontSize: 18, marginRight: 10, fontWeight: 'bold' }} type="check" />
      Nice job, no issues detected!
    </StyleNoIssue>
  )
}

export const StyledTip = styled.div`
  font-style: normal;
  font-weight: 400;
  font-size: 15px;
  line-height: 140%;
  /* color: #000; */
  margin-bottom: 20px;
`

export const StyledSpan = styled.span`
  font-size: 15px;
  /* color: #22282a; */
`
