import { InventoryService } from '~/modules/inventory/inventory.service'
import { responseHandler } from '~/common/utils'
import { notification } from 'antd'
import { formatNumber } from '~/common/utils'

export enum Consts {
  shippedSO = 'shippedSO',
  unReceivedPO = 'unReceivedPO',
  noLot = 'noLot',
}

export type Props = {
  dispatchLoad: (p: { [key in Consts]?: boolean }) => void
  dispatchIssue: (p: { [key in Consts]?: boolean }) => void
  reloadFn: any
}

export const getList = (query: any) => {
  return new Promise((resolve: any, reject: any) => {
    InventoryService.instance.getInventoryOrders(query).subscribe({
      next: (data: any) => {
        responseHandler(data)
        resolve({
          list: data?.body?.data?.dataList,
          total: data?.body?.data?.total,
        })
      },
      error: (error) => {
        console.log('error', error)
        reject(error)
        notification.error({
          message: 'Request failed: inventory/order/inventoryOrders',
        })
      },
    })
  })
}

export const confirmColumns = [
  { title: 'Product (SKU)', dataIndex: 'product', width: '40%' },
  { title: 'Lot', dataIndex: 'lot', width: '20%' },
  {
    title: 'Adjustment',
    dataIndex: 'adjustment',
    width: '20%',
    render: (val: any) => {
      return formatNumber(val, 2)
    },
  },
  {
    title: 'Valuation impact',
    dataIndex: 'valuationImpact',
    width: '20%',
    render: (val: any) => {
      return val > 0 ? `+$${formatNumber(Math.abs(val), 2)}` : `-$${formatNumber(Math.abs(val), 2)}`
    },
  },
]
