import React, { useCallback, useEffect, useState } from 'react'
import { Icon, Dropdown, Menu } from 'antd'
import lodash from 'lodash'

import GrubSuggest from '~/components/GrubSuggest'
import { HeaderContainer, HeaderOptions } from './inventory-header.style'
import { SaleItem } from '~/schema'
import { InventoryDispatchProps } from '../inventory.module'
import { Theme, CACHED_COMPANY, CACHED_QBO_LINKED } from '~/common'
import { ThemeOutlineButton } from '~/modules/customers/customers.style'
import { onLoadFocusOnFirst } from '~/common/jqueryHelper'
import { ThemeCheckbox } from '~/modules/customers/customers.style'
import { FlexDiv, Item } from '~/modules/customers/nav-sales/styles'
import { basePriceToRatioPrice, formatNumber, mathRoundFun, handlerNoLotNumber } from '~/common/utils'
import moment from 'moment'

// 外部处理
interface SearchData {
  name: string[]
  category: string[]
  provider: string[]
}

function pureFind(data: SearchData, key: string, query: string) {
  const source = data[key]
  const itemIds = data['itemId']
  // avoid repeation of `toLowerCase`
  const q = query.toLowerCase()
  let indexes: number[] = []
  let result: any[] = []
  source.forEach((n: string, index: number) => {
    if (n && n.toLowerCase().indexOf(q) > -1) {
      indexes.push(index)
      if (key != 'category') {
        result.push({ title: n, itemId: itemIds[index] })
      } else {
        result.push({ title: n, itemId: null })
      }
      return true
    } else {
      return false
    }
  })
  const indexArray = indexes.slice(0, 5)
  if (key == 'SKU' || key == 'name') {
    const memoSource = data[key + 'Memo']
    let memoResult: any[] = []
    indexArray.forEach((el) => {
      memoResult.push({ title: memoSource[el], itemId: itemIds[el] })
    })
    return memoResult
  } else {
    return result.slice(0, 5)
  }
}

function searchResult(datum: SearchData, query: string) {
  const titles = ['Search By Sku', 'Search By Name', 'Search By Category', 'Search By Provider']
  const result = ['SKU', 'name', 'category', 'provider'].map((key, index) => ({
    title: titles[index],
    children: pureFind(datum, key, query), //.map((n) => ({ title: n.title,  })),
  }))

  return result
}

export interface Props {
  waiting: boolean
  rawData: SaleItem[]
  onShowAdjustInventory: () => void
  onShowReviewTransaction: () => void
  onShowDrawer: () => void
  onSearch: (search: string) => void
  uploadBertiExcel: InventoryDispatchProps['uploadBertiExcel']
  onShowUploadCSV: (mode: string) => void
  theme: Theme
  changeShow: Function
  search: string
  active: number
  getItemListByCondition: Function
  downloadData: any[]
  downloadLotData: any[]
  resetDownloadData: Function
  getDownloadLotItemList: Function
  isDownloading: boolean
  userSetting: any
  itemLocations: any[]
  getItemLocations: Function
  companyProductTypes: any
}

export const InventoryHeader: React.SFC<Props> = (props) => {
  const [locationMethod, setLocationMethod] = useState(0)
  const [palletLevelLocation, setPalletLevelLocation] = useState('')
  useEffect(() => {
    onLoadFocusOnFirst('simple-input-wrapper')
  }, [])

  useEffect(() => {
    if (props.userSetting && props.userSetting.company) {
      setLocationMethod(props.userSetting.company.locationMethod)
      if (props.userSetting.company.locationMethod == 0) {
        props.getItemLocations()
      }
    }
  }, [JSON.stringify(props.userSetting)])

  useEffect(() => {
    setPalletLevelLocation(props.itemLocations.map((el) => el.name).join(','))
  }, [props.itemLocations])

  useEffect(() => {
    if (props.downloadData.length && !props.isDownloading) {
      downloadExcelFile()
    }
    if (props.downloadLotData.length && !props.isDownloading) {
      downloadLotExcelFile()
    }
  }, [props.downloadData, props.downloadLotData])
  // 外部处理
  // data processing | useCallback to memorize result

  var companyId = localStorage.getItem(CACHED_COMPANY)

  const getSearchSource = useCallback(
    () => ({
      itemId: props.rawData.map((n) => n.itemId),
      SKU: props.rawData.map((n) => n.SKU),
      SKUMemo: props.rawData.map((n) => {
        return n.SKU + ' - ' + n.variety
      }),
      name: props.rawData.map((n) => n.variety),
      nameMemo: props.rawData.map((n) => {
        return `${n.variety} ${n.SKU ? `(${n.SKU})` : ''}`
      }),
      category: lodash.uniq(props.rawData.map((n) => (n.wholesaleCategory ? n.wholesaleCategory.name : ''))),
      provider: lodash.uniq(props.rawData.map((n) => n.provider)),
    }),
    [props.rawData],
  )

  // search by keywords
  const handleSuggest = (keyword: string) => {
    const searchSource = getSearchSource()
    return searchResult(searchSource, keyword)
  }

  const getBase64 = (file: any, cb: any) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function() {
      cb(reader.result)
    }
    reader.onerror = function(error) {
      //console.log('Error: ', error);
    }
  }

  const onChangeBertiFile = (file: File) => {
    getBase64(file, (result: any) => {
      const details = {
        ext: file.name.split('.')[1],
        excel: result,
      }

      const formBody = []
      for (const property in details) {
        const encodedKey = encodeURIComponent(property)
        const encodedValue = encodeURIComponent(details[property])
        formBody.push(encodedKey + '=' + encodedValue)
      }
      props.uploadBertiExcel(formBody.join('&'))
    })
    return false
  }

  const downloadExcelFile = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('product-list-table'))
    var wscols = [
      { wpx: 100 },
      { wpx: 70 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols
    const href = window.location.href
    const pathStr = '#/inventory'
    const index = href.indexOf(pathStr)
    if (index < 0) return
    const rootPath = href.substr(0, index)
    let target = ''

    props.downloadData.forEach((el: any, index: number) => {
      target = `${rootPath}#/product/${el.itemId}/activity`
      wb.Sheets.Sheet1['A' + (index + 2)].l = { Target: target }
    })

    const today = moment().format('MM.DD.YYYY')
    const title = `Product List-${today}.xlsx`
    XLSX.writeFile(wb, title)
    const resetAll = false
    props.resetDownloadData(resetAll)
  }

  const downloadLotExcelFile = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('product-lot-list-table'))
    var wscols = [
      { wpx: 400 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]

    wb.Sheets.Sheet1['!cols'] = wscols
    const today = moment().format('MM.DD.YYYY')
    const title = `Lots List-${today}.xlsx`
    XLSX.writeFile(wb, title)
    const resetAll = false
    props.resetDownloadData(resetAll)
  }

  const onActions = (item: any) => {
    const { key } = item
    if (key === '3') {
      const resetAll: boolean = true
      props.resetDownloadData(resetAll)
      props.getItemListByCondition({ active: props.active, download: 1 })
    } else if (key === '4') {
      const resetAll: boolean = true
      props.resetDownloadData(resetAll)
      props.getDownloadLotItemList()
    } else if (key === '5') {
      props.onShowAdjustInventory()
    } else if (key === '6') {
      props.onShowReviewTransaction()
    } else if (key === '7') {
      props.onShowUploadCSV('ItemUpdate')
    }
  }

  return (
    <div>
      <FlexDiv style={{ position: 'absolute', right: 28, top: 10, zIndex: 1000 }}>
        <ThemeOutlineButton
          size="large"
          type="primary"
          style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }}
        >
          Actions
        </ThemeOutlineButton>
        <Dropdown
          overlay={
            <Menu onClick={onActions}>
              <Menu.Item key="3">Export Product List</Menu.Item>
              <Menu.Item key="4">Export Lots List</Menu.Item>
              <Menu.Item key="7">Update Items from CSV</Menu.Item>
              <Menu.Item key="5">Adjust inventory...</Menu.Item>
              <Menu.Item key="6">Enter Inventory Count</Menu.Item>
            </Menu>
          }
          placement="bottomRight"
        >
          <ThemeOutlineButton
            size="large"
            type="primary"
            style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
          >
            <Icon type="down" />
          </ThemeOutlineButton>
        </Dropdown>
        {props.companyProductTypes !== null && (
          <Item className="header-btns right">
            <ThemeOutlineButton size="large" disabled={props.waiting === true} onClick={props.onShowDrawer}>
              <Icon type="plus" /> Create new product
            </ThemeOutlineButton>
          </Item>
        )}
      </FlexDiv>
      <HeaderContainer className="simple-input-wrapper">
        <HeaderOptions className="products-searcher">
          <GrubSuggest
            isProduct={true}
            defaultValue={props.search}
            onSuggest={handleSuggest}
            onSearch={props.onSearch}
            theme={props.theme}
            customWidth={true}
            maxDropdownWidth={600}
          />
          <div style={{ flex: 1, textAlign: 'left', marginTop: '15px', marginLeft: '20px' }}>
            <ThemeCheckbox onChange={props.changeShow}>Show inactive</ThemeCheckbox>
          </div>
        </HeaderOptions>
      </HeaderContainer>

      <table id="product-list-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
        <thead>
          <tr>
            <th>WholesaleWare ID</th>
            <th>SKU</th>
            <th>CATEGORY</th>
            <th>SUB-CATEGORY</th>
            <th>PRODUCT</th>
            <th>UPC-CODE</th>
            <th>PRODUCT MANAGER</th>
            <th>SIZE</th>
            <th>WEIGHT</th>
            <th>GRADE</th>
            <th>PACKING</th>
            <th>ORIGIN</th>
            <th>TEMPERATURE</th>
            <th>LOCATION</th>
            <th>INVENTORY UOM</th>
            <th>UNITS ON HAND</th>
            <th>UNITS INCOMING</th>
            <th>UNITS COMMITTED</th>
            <th>OVER COMMITTED</th>
            <th>PAR LEVEL</th>
            <th>WEEKLY USAGE</th>
            <th>SHELFT LIFE (DAYS)</th>
            <th>RESTOCKING LEAD TIME</th>
            <th>PREFERRED VENDOR</th>
            <th>UNIT GROSS WEIGHT</th>
            <th>UNIT GROSS VOLUME</th>
            <th>UNITS PER PALLET</th>
            <th>UNITS PER LAYER</th>
            <th>LAYERS PER PALLET</th>
            <th>GTIN</th>
            <th>DEFAULT PRICE</th>
            <th>PRICE UOM</th>
            <th>DEFAULT COST</th>
            <th>COST UOM</th>

            <th>
              {localStorage.getItem(CACHED_QBO_LINKED) && localStorage.getItem(CACHED_QBO_LINKED) != 'null'
                ? 'QBO SYNCED'
                : 'NS SYNCED'}
            </th>
          </tr>
        </thead>
        <tbody>
          {props.downloadData.map((row: any, index: number) => {
            const defaultPricingUOM = row.defaultSellingPricingUOM ? row.defaultSellingPricingUOM : row.inventoryUOM
            const price = basePriceToRatioPrice(defaultPricingUOM, row.price, row, 2, true)
            const defaultCostUOM = row.defaultPurchasingCostUOM ? row.defaultPurchasingCostUOM : row.inventoryUOM
            const cost = basePriceToRatioPrice(defaultCostUOM, row.cost, row)
            return (
              <tr key={`row-${index}`}>
                <td>{row['itemId']}</td>
                <td>{row['SKU'] ? row['SKU'] : ''}</td>
                <td>{row['section'] ? row['section'] : ''}</td>
                <td>{row.wholesaleCategory ? row.wholesaleCategory.name : ''}</td>
                <td>{row['variety']}</td>
                <td>{row['upc'] ? row['upc'] : ''}</td>
                <td>{row['managerName'] ? row['managerName'] : ''}</td>
                <td>{row['size']}</td>
                <td>{row['weight']}</td>
                <td>{row['grade']}</td>
                <td>{row['packing']}</td>
                <td>{row['origin']}</td>
                <td>{row['temperature'] ? row['temperature'] : ''}</td>
                <td>{locationMethod == 0 ? palletLevelLocation : row['location'] ? row['location'] : ''}</td>
                <td>{row['inventoryUOM']}</td>
                <td>{handlerNoLotNumber(row, 2)}</td>
                <td>{formatNumber(row.incomingQty, 2)}</td>
                <td>{formatNumber(row.commitedQty, 2)}</td>
                <td>
                  {!row.commitedQty ? 0 : formatNumber((row.commitedQty  - row.onHandQty - row.incomingQty), 2)}
                </td>
                <td>{row['reorderLevel'] ? row['reorderLevel'] : ''}</td>
                <td>{row['weeklyUsage'] ? row['weeklyUsage'] : ''}</td>
                <td>{row['shelfLife'] ? row['shelfLife'] : ''}</td>
                <td>{row['restockingTime'] ? row['restockingTime'] : ''}</td>
                <td>{row['preferredVendorName'] ? row['preferredVendorName'] : ''}</td>
                <td>{row['grossWeightUnit'] ? row['grossWeightUnit'] : ''}</td>
                <td>{row['grossVolumeUnit'] ? row['grossVolumeUnit'] : ''}</td>
                <td>{row['unitPerPallet'] ? row['unitPerPallet'] : ''}</td>
                <td>{row['unitPerLayer'] ? row['unitPerLayer'] : ''}</td>
                <td>{row['layersPerPallet'] ? row['layersPerPallet'] : ''}</td>
                <td>{row['gtin'] ? row['gtin'] : ''}</td>
                <td>{mathRoundFun(price, 2)}</td>
                <td>{defaultPricingUOM}</td>
                <td>{mathRoundFun(cost, 2)}</td>
                <td>{defaultCostUOM}</td>

                <td>{row.qboId || row.nsId ? 'YES' : 'NO'}</td>
              </tr>
            )
          })}
        </tbody>
      </table>

      <table id="product-lot-list-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
        <thead>
          <tr>
            <th>Item</th>
            <th>Brand</th>
            <th>Origin</th>
            <th>Lot</th>
            <th>PO #</th>
            <th>Vendor</th>
            <th>Received Date</th>
            <th>Units On Hand</th>
            <th>UOM</th>
          </tr>
        </thead>
        <tbody>
          {props.downloadLotData.map((row: any, index: number) => {
            return (
              <tr key={`row-${index}`}>
                <td>{row['variety']}</td>
                <td>{row['modifiers']}</td>
                <td>{row['extraOrigin']}</td>
                <td>{row['lotId']}</td>
                <td>{row['wholesaleOrderId']}</td>
                <td>{row['vendorName']}</td>
                <td>{moment(row['receiveDate']).format('MM/DD/YYYY')}</td>
                <td>{row['onHandQty']}</td>
                <td>{row['UOM']}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}
