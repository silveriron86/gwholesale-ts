import styled from '@emotion/styled'
import { css } from '@emotion/core'

import {
  white,
  black,
  brownGrey,
  transparent,
  brightGreen,
  mediumGrey,
  backgroundGreen,
  darkGrey,
  filterGreen,
} from '~/common'

export const HeaderContainer = styled('div')({
  width: '100%',
  backgroundColor: white,
  padding: '39px 38px 32px 138px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const Header = styled('div')({
  width: '100%',
  height: '232px',
  boxSizing: 'border-box',
  display: 'flex',
  justifyContent: 'space-between',
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const Flex1 = styled('div')((props: any) => ({
  flex: 1,
}))

export const ItemName = styled('div')({
  color: black,
  fontSize: '36px',
  fontWeight: 700,
  letterSpacing: '0.05em',
  lineHeight: '50px',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const HeaderOptions = styled('div')((props) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  '& .simple-input-wrapper':{
    padding:"0px"
  },
  '& .ant-select-selection__clear': {
    display: 'none',
  },
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: props.theme.primary,
    '&::after': {
      display: 'none',
    },
  },
}))

export const Flex = styled('div')({
  display: 'flex',
  '&.v-center': {
    alignItems: 'center',
  },
  '&.between': {
    justifyContent: 'space-between',
  },
})

export const BackButton = styled('div')((props) => ({
  color: props.theme.primary,
  fontSize: '12px',
  cursor: 'pointer',
  marginRight: '30px',

  '& > span': {
    textTransform: 'uppercase',
    fontWeight: 700,
    marginLeft: '8px',
  },
}))

export const InfoWrap = styled('div')({
  boxSizing: 'border-box',
  marginRight: '112px',
  '& > span': {
    fontSize: '12px',
    color: mediumGrey,
  },
})

export const OderId = styled('div')({
  color: black,
  fontSize: '36px',
  fontWeight: 700,
  letterSpacing: '0.05em',
  lineHeight: '50px',
})

export const Info = styled('div')({
  display: 'flex',
  width: '100%',
  height: '86px',
  boxSizing: 'border-box',
  backgroundColor: backgroundGreen,
  marginTop: '22px',
})

export const InfoItem = styled('div')({
  height: '86px',
  boxSizing: 'border-box',
  paddingLeft: '30px',
  paddingRight: '50px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',

  '& > span': {
    fontSize: '14px',
    color: mediumGrey,
  },
  '& > p': {
    fontSize: '18px',
    margin: 0,
    marginTop: '10px',
    color: darkGrey,
  },
})

export const InfoItemLabel = styled('span')({
  fontWeight: 700,
  fontSize: '14px',
  letterSpacing: '0.05em',
  lineHeight: '17px',
})

export const InfoItemText = styled('span')({
  fontWeight: 700,
  fontSize: '14px',
  letterSpacing: '0.05em',
  lineHeight: '34px',
})

export const MainInfoItem = styled('div')({
  height: '86px',
  boxSizing: 'border-box',
  padding: '0 30px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  backgroundColor: filterGreen,

  '& > span': {
    fontSize: '14px',
    color: mediumGrey,
  },
  '& > p': {
    fontSize: '30px',
    lineHeight: '41px',
    margin: 0,
    color: mediumGrey,
  },
})

export const Operation = styled('div')((props) => ({
  display: 'flex',
  fontSize: '14px',
  fontWeight: 700,
  color: props.theme.primary,
  lineHeight: '15px',
  height: '15px',
  letterSpacing: '0.05em',
  cursor: 'pointer',
  '& + &': {
    marginLeft: '45px',
  },
  '& > i': {
    fontSize: '16px',
    margin: '0 10px',
  },
}))

export const buttonStyle = css({
  borderRadius: '200px',
  height: '40px',
  border: `1px solid ${brownGrey}`,
  '&.ant-btn-primary': {
    backgroundColor: brightGreen,
    borderColor: brightGreen,
    width: '180px',
  },
  '&.ant-dropdown-trigger': {
    borderRadius: '5px',
    borderColor: brownGrey,
  },
  '& > a': {
    '&:visited': {
      color: white,
    },
  },
})

export const CustomActionButton = styled('a')((props) => ({
  padding: '8px 15px',
  background: 'white',
  borderRadius: '3px',

  '&:hover': {
    border: `1px solid ${props.theme.main}`,
  },
}))

export const NetSuitUoms = styled('div')((props: any) => ({
  '.ant-table': {
    'tbody tr td:last-child': {
      borderRight: '1px solid #EDF1EE',
    },
    '.default-uom-check path': {
      fill: props.theme.dark,
    },
  },
}))
