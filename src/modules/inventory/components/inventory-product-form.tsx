import React from 'react'
import { Form, Input, Row, Col, Button, Icon, InputNumber, Tooltip, Select, Checkbox, Radio, Table } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import { SetionTableLabel } from '~/modules/product/components/product.component.style'

import {
  EditA,
  ThemeSelect,
  ThemeModal,
  FullInputNumber,
  ThemeButton,
  ThemeTable,
  ThemeOutlineButton,
} from '~/modules/customers/customers.style'
import { ProductModule, ProductProps, ProductDispatchProps } from '~/modules/product'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import { Items } from '~/modules/customers/components/customer-detail/customer-detail.style'
import { CACHED_NS_LINKED } from '~/common'
import { NetSuitUoms } from './inventory-header.style'
import { InventoryService } from '../inventory.service'

const FormItem = Form.Item

interface FormFields {
  variety: string
  sku: String
  baseUOM: string
  inventoryUOM: string
  constantRatio: boolean
  ratioUOM: number
}

type InventoryProps = {
  userSetting: any
  netsuitUoms: any[]
  companyProductTypes: any[]
  visible: boolean
  handleOk: Function
  handleCancel: Function
  getAllCompanyTypes: Function
  getNetSuitUoms: Function
  getUserSetting: Function
}

type ProductFormProps = ProductProps & ProductDispatchProps & FormComponentProps<FormFields> & InventoryProps

class InventoryProductContainer extends React.PureComponent<ProductFormProps> {
  state = {
    newModalVisible: false,
    addUomModalVisible: false,
    baseUOM: null,
    inventoryUOM: null,
    selectedBaseUOMNSId: 1,
    constantRatio: true,
    isQBOConnected: !localStorage.getItem(CACHED_NS_LINKED) || localStorage.getItem(CACHED_NS_LINKED) == 'null',
    // isQBOConnected: false
    curPage: 1,
    pageSize: 5,
    defaultSubUomId: 0,
    showError: false,
    hasSpecialChars: false,
    UOMs: this.props.companyProductTypes.unitOfMeasure.sort((a, b) => a.name.localeCompare(b.name)),
  }

  columns: any[] = [
    {
      title: 'No',
      dataIndex: 'id',
      align: 'center',
      render: (id: any, record: any, index: number) => {
        return (this.state.curPage - 1) * this.state.pageSize + (index + 1)
      },
    },
    {
      title: 'Name',
      dataIndex: 'name',
      align: 'center',
      // render: (name: any, record: any, index: number) => {

      // }
    },
    {
      title: 'Ratio',
      dataIndex: 'ratio',
      align: 'center',
      // render: (ratio: any, record: any, index: number) => {

      // }
    },
    {
      title: 'Default',
      dataIndex: 'parentId',
      align: 'center',
      render: (parentId: any, record: any, index: number) => {
        return record.id == this.state.defaultSubUomId ? (
          <Icon type="check-square" viewBox="0 0 14 14" width={25} height={25} className="default-uom-check" />
        ) : (
          ''
        )
      },
    },
  ]

  componentDidMount() {
    if (!this.props.userSetting) {
      this.props.getUserSetting()
    }
    if (!this.props.netsuitUoms.length) {
      this.props.getNetSuitUoms()
    }
  }
  onSave = (e: any) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(values)
        // this.props.form.resetFields();
      }
    })
  }

  showLotModal = () => {
    this.setState({
      newModalVisible: true,
    })
  }

  onBaseUomChange = (value: String) => {
    this.setState({
      baseUOM: value,
    })
    if (value == this.state.inventoryUOM) {
      this.props.form.setFieldsValue({
        ratioUOM: 1,
      })
    } else {
      if (this.state.constantRatio) {
        this.props.form.setFieldsValue({
          ratioUOM: '',
        })
      }
    }
  }

  onInventoryUomChange = (value: String, e: any) => {
    let nsId = 0
    let defaultSubUomId = ''
    const { netsuitUoms } = this.props
    if (e.props && e.props.title) {
      nsId = parseInt(e.props.title)
      const filteredNetSuitItems = netsuitUoms.filter((el) => el.parentId == nsId)
      const netSuitBaseUOM = filteredNetSuitItems.find((el) => el.isBaseUnit)
      defaultSubUomId = netSuitBaseUOM.id
      this.props.form.resetFields(['defaultUOM'])
    }
    this.setState({
      inventoryUOM: value,
      selectedBaseUOMNSId: nsId,
      defaultSubUomId,
    })
    if (this.state.baseUOM == value) {
      this.props.form.setFieldsValue({
        ratioUOM: 1,
      })
    } else {
      if (this.state.constantRatio) {
        this.props.form.setFieldsValue({
          ratioUOM: '',
        })
      }
    }
  }

  handleSelectOk = () => {
    // const { form } = this.formRef.props;
    // form.validateFields((err, values) => {
    //   if (err) {
    //     return;
    //   }
    //   console.log('Received values of form: ', values);
    //   form.resetFields();
    // });
  }

  handleSelectCancel = () => {
    this.props.handleCancel()
  }

  toggleAddUOMModal = () => {
    const { addUomModalVisible } = this.state
    if (addUomModalVisible) {
      this.props.getAllCompanyTypes()
    }
    this.setState({
      addUomModalVisible: !addUomModalVisible,
    })
  }

  changeConstantRatio = (e: any) => {
    console.log(e.target.value)
    if (!e.target.value) {
      this.props.form.setFieldsValue({
        // ratioUOM: 1,
        ratioUOM: 'N/A',
      })
    } else {
      if (this.state.baseUOM != this.state.inventoryUOM) {
        this.props.form.setFieldsValue({
          ratioUOM: '',
        })
      }
    }

    this.setState({
      constantRatio: e.target.value,
    })
  }

  renderBaseUoms = () => {
    const { userSetting } = this.props
    const { isQBOConnected } = this.state
    const UOMs = JSON.parse(JSON.stringify(this.state.UOMs))
    if (!isQBOConnected && userSetting && !userSetting.nsCustomUom) {
      return UOMs.map((item: any, index: number) => {
        if (item.nsId) {
          return (
            <Select.Option key={index} value={item.name} title={item.nsId}>
              {item.name}
            </Select.Option>
          )
        }
      })
    } else {
      return UOMs.map(
        (item: { name: {} | null | undefined }, index: string | number | undefined) => {
          return (
            <Select.Option key={index} value={item.name}>
              {item.name}
            </Select.Option>
          )
        },
      )
    }
  }

  renderNetsuitUoms = () => {
    const { selectedBaseUOMNSId } = this.state
    const { netsuitUoms } = this.props
    if (!selectedBaseUOMNSId || !netsuitUoms || !netsuitUoms.length) return []
    return netsuitUoms.map((item: any, index: number) => {
      if (item.parentId == selectedBaseUOMNSId) {
        return (
          <Select.Option key={item.id} value={item.name} title={item.id}>
            {item.name}
          </Select.Option>
        )
      }
    })
  }

  onUomTableChange = (page: number) => {
    console.log('current page', this.state.curPage)
    console.log('new page', page)
    this.setState({ curPage: page })
  }

  onNetsuitUomChange = (value: string, evt: any) => {
    if (evt.props && evt.props.title) {
      const defaultSubUomId = evt.props.title
      this.setState({ defaultSubUomId })
    }
  }

  getAlert = () => (this.state.hasSpecialChars ? <p style={{ color: 'red', lineHeight: 'normal', fontWeight: 'normal' }}>{`Note: item name containing any of the following characters may cause unexpected behavior: \\ / " < > |`}</p> : '')

  onValuesChange = (props: any, changedValues: any, allValues: any) => {
    console.log(changedValues)
  }

  onUOMSearch = (searchStr: string) => {
    const { companyProductTypes } = this.props
    if (!companyProductTypes) {
      return []
    }

    let matches1: any[] = []
    let matches2: any[] = []
    companyProductTypes.unitOfMeasure.forEach((uom: any) => {
      // sensitive
      if (uom.name.indexOf(searchStr) === 0) {
        matches1.push(uom)
      } else if (uom.name.toLocaleLowerCase().indexOf(searchStr.toLocaleLowerCase()) === 0) {
        matches2.push(uom)
      }
    });
    if (matches1) {
      matches1 = matches1.sort((a, b) => a.name.localeCompare(b.name))
    }
    if (matches2) {
      matches2 = matches2.sort((a, b) => a.name.localeCompare(b.name))
    }
    this.setState({
      UOMs: [...matches1, ...matches2]
    })
  }

  onCreateNewItem = () => {
    const _this = this
    const sku: string = this.props.form.getFieldValue("sku");
    console.log('check sku', sku)
    InventoryService.instance.checkDuplicateSku(sku).subscribe({
      next: (res) => {
        if(!res?.body?.data) {
          _this.props.handleOk()
        } else {
          _this.props.form.setFields({sku: {
            value: sku,
            errors: [new Error('Duplicated sku. Please try with another sku!')]
          }})
        }
      }
    })
  }

  render() {
    const { form, visible, netsuitUoms, userSetting } = this.props
    const { baseUOM, inventoryUOM, addUomModalVisible, constantRatio, isQBOConnected, selectedBaseUOMNSId } = this.state
    const { getFieldDecorator } = form
    const filteredNetSuitItems = selectedBaseUOMNSId
      ? netsuitUoms.filter((el) => el.parentId == selectedBaseUOMNSId)
      : []
    const isNativeUom = userSetting && !userSetting.nsCustomUom
    const netSuitBaseUOM = filteredNetSuitItems.find((el) => el.isBaseUnit)
    // console.log(isNativeUom, selectedBaseUOMNSId, isQBOConnected)
    return (
      <ThemeModal
        width={700}
        title="Create a new product"
        okText="Create"
        visible={visible}
        onOk={this.onCreateNewItem}
        onCancel={this.props.handleCancel}
      >
        <ThemeModal
          title={`Edit Value List "Unit of Measurement"`}
          visible={addUomModalVisible}
          onCancel={this.toggleAddUOMModal}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="unitOfMeasure" title="Unit of Measurement" buttonTitle="Add New UOM" />
        </ThemeModal>
        <Form onSubmit={this.onSave}>
          <Row gutter={24}>
            <Col span={12}>
              <Form.Item
                onValuesChange={this.onValuesChange}
                colon={false}
                label={<SetionTableLabel className="form-label">Name</SetionTableLabel>}
              >
                {getFieldDecorator('variety', {
                  rules: [{ required: true, message: 'Please input name!' }],
                  getValueFromEvent: (e) => {
                    if (!e || !e.target) {
                      return e;
                    }
                    const { value } = e.target;
                    let hasSpecialChars = false
                    if (value) {
                      const reg = new RegExp("[[<\>\/\\\\|\"]")
                      const r = reg.test(value)
                      if (r == true) {
                        hasSpecialChars = true
                      }
                    }
                    this.setState({
                      hasSpecialChars
                    })

                    return value;
                  }
                })(<Input placeholder="product name" />)}
                {this.getAlert()}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={12}>
              <Form.Item colon={false} label={<SetionTableLabel className="form-label">Product SKU</SetionTableLabel>}>
                {getFieldDecorator('sku', {
                  rules: [{ required: false }],
                })(<Input placeholder="SKU" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={8}>
              <Form.Item
                colon={false}
                label={<SetionTableLabel className="form-label">Initial Inventory</SetionTableLabel>}
              >
                {getFieldDecorator('quantity', {
                  rules: [{ required: false, message: 'Please input initial inventory!' }],
                })(<InputNumber placeholder="Initial Inventory" style={{ width: '100%' }} />)}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                colon={false}
                label={<SetionTableLabel className="form-label">Unit of Measure(UOM) Type</SetionTableLabel>}
              >
                {getFieldDecorator('inventoryUOM', {
                  rules: [{ required: true, message: 'Please input UOM!' }],
                  initialValue: !isQBOConnected && userSetting && !userSetting.nsCustomUom ? 'Quantity' : '',
                })(
                  <ThemeSelect
                    style={{ width: '100%' }}
                    onChange={(value: string, e: any) => this.onInventoryUomChange(value, e)}
                    showSearch
                    optionFilterProp="children"
                    onSearch={this.onUOMSearch}
                    // filterOption={(input, option) =>
                    //   option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    // }
                    dropdownRender={(menu: any) => (
                      <div>
                        {menu}
                        {/* <EditA onMouseDown={e => e.preventDefault()} onClick={this.toggleEditSelect}>Edit...</EditA> */}
                      </div>
                    )}
                  >
                    {this.renderBaseUoms()}
                  </ThemeSelect>,
                )}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item colon={false} label={<SetionTableLabel />}>
                {isQBOConnected && (
                  <ThemeOutlineButton onClick={this.toggleAddUOMModal} shape="round">
                    <Icon type="plus" />
                    Add New UOM
                  </ThemeOutlineButton>
                )}
              </Form.Item>
            </Col>
          </Row>
          {isNativeUom && selectedBaseUOMNSId && !isQBOConnected ? (
            <Row gutter={24}>
              <Col span={8}>
                <Form.Item
                  colon={false}
                  label={<SetionTableLabel className="form-label">Default UOM</SetionTableLabel>}
                >
                  {getFieldDecorator('defaultUOM', {
                    rules: [{ required: true, message: 'Default UOM should not be empty!' }],
                    initialValue: netSuitBaseUOM && netSuitBaseUOM.name ? netSuitBaseUOM.name : '',
                  })(
                    <ThemeSelect
                      style={{ width: '100%' }}
                      showSearch
                      optionFilterProp="children"
                      onChange={this.onNetsuitUomChange}
                      filterOption={(input, option) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }
                      dropdownRender={(menu: any) => (
                        <div>
                          {menu}
                          {/* <EditA onMouseDown={e => e.preventDefault()} onClick={this.toggleEditSelect}>Edit...</EditA> */}
                        </div>
                      )}
                    >
                      {this.renderNetsuitUoms()}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
              <Col span={16}>
                <NetSuitUoms>
                  <ThemeTable
                    columns={this.columns}
                    dataSource={filteredNetSuitItems}
                    bordered={true}
                    pagination={
                      filteredNetSuitItems.length > this.state.pageSize
                        ? {
                            current: this.state.curPage,
                            pageSize: this.state.pageSize,
                            onChange: this.onUomTableChange,
                          }
                        : false
                    }
                  />
                </NetSuitUoms>
              </Col>
            </Row>
          ) : (
            ''
          )}
        </Form>
      </ThemeModal>
    )
  }
}

const InventoryProductForm = Form.create()(InventoryProductContainer)
export { InventoryProductForm }
