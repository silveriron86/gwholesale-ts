import React from 'react'
import { Form, Input, Row, Col, Button, Icon, InputNumber, Tooltip, Select, Checkbox, Radio } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import { SetionTableLabel } from '~/modules/product/components/product.component.style'

import {
  EditA,
  ThemeSelect,
  ThemeModal,
  FullInputNumber,
  ThemeButton,
  ThemeOutlineButton,
} from '~/modules/customers/customers.style'
import { ProductModule, ProductProps, ProductDispatchProps } from '~/modules/product'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import { Items } from '~/modules/customers/components/customer-detail/customer-detail.style'
import { ItemHMargin4 } from '~/modules/customers/nav-sales/styles'
import { AccoutName } from '~/modules/vendors/purchase-orders/_style'
import { CACHED_NS_LINKED } from '~/common'

const FormItem = Form.Item

interface FormFields {
  variety: string
  sku: String
  baseUOM: string
  inventoryUOM: string
  constantRatio: boolean
  ratioUOM: number
  handleOk: Function
  handleCancel: Function
  getAllCompanyTypes: Function
  visible: boolean
  companyProductTypes: any[]
}

type ProductVerticalFormProps = ProductProps & ProductDispatchProps & FormComponentProps<FormFields> & {}

class InventoryProductVerticalContainer extends React.PureComponent<ProductVerticalFormProps> {
  state = {
    newModalVisible: false,
    addUomModalVisible: false,
    baseUOM: null,
    inventoryUOM: null,
    constantRatio: true,
    accountName: '',
    accountNames: [],
    isNSRealmId: !localStorage.getItem(CACHED_NS_LINKED) || localStorage.getItem(CACHED_NS_LINKED) == 'null',
  }

  constructor(props: ProductFormProps) {
    super(props)
  }

  onSave = (e: any) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(values)
        // this.props.form.resetFields();
      }
    })
  }

  showLotModal = () => {
    this.setState({
      newModalVisible: true,
    })
  }

  onBaseUomChange = (value: String) => {
    this.setState({
      baseUOM: value,
    })
    if (value == this.state.inventoryUOM) {
      this.props.form.setFieldsValue({
        ratioUOM: 1,
      })
    } else {
      if (this.state.constantRatio) {
        this.props.form.setFieldsValue({
          ratioUOM: '',
        })
      }
    }
  }

  onInventoryUomChange = (value: String) => {
    this.setState({
      inventoryUOM: value,
    })
    if (this.state.baseUOM == value) {
      this.props.form.setFieldsValue({
        ratioUOM: 1,
      })
    } else {
      if (this.state.constantRatio) {
        this.props.form.setFieldsValue({
          ratioUOM: '',
        })
      }
    }
  }

  handleSelectOk = () => {
    // const { form } = this.formRef.props;
    // form.validateFields((err, values) => {
    //   if (err) {
    //     return;
    //   }
    //   console.log('Received values of form: ', values);
    //   form.resetFields();
    // });
  }

  handleSelectCancel = () => {
    this.props.handleCancel()
  }

  toggleAddUOMModal = () => {
    const { addUomModalVisible } = this.state
    if (addUomModalVisible) {
      this.props.getAllCompanyTypes()
    }
    this.setState({
      addUomModalVisible: !addUomModalVisible,
    })
  }

  changeConstantRatio = (e: any) => {
    console.log(e.target.value)
    if (!e.target.value) {
      this.props.form.setFieldsValue({
        // ratioUOM: 1,
        ratioUOM: 'N/A',
      })
    } else {
      if (this.state.baseUOM != this.state.inventoryUOM) {
        this.props.form.setFieldsValue({
          ratioUOM: '',
        })
      }
    }

    this.setState({
      constantRatio: e.target.value,
    })
  }

  onSearch = (searchStr: string) => {
    const { vendors } = this.props
    // tslint:disable-next-line:prefer-const
    let accountNames: any[] = []
    if (vendors.length > 0 && searchStr) {
      vendors.filter((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      accountNames: accountNames,
    })
  }

  handleChangeAccount = (value: any) => {
    this.setState({
      accountName: value,
    })
  }

  handleSelectAccount = (value: any) => {
    this.setState(
      {
        accountId: value,
      },
      () => {
        console.log('accountId= ', value)
      },
    )
  }

  render() {
    const { form, visible, companyProductTypes } = this.props
    const { getFieldDecorator } = this.props.form
    const {
      baseUOM,
      inventoryUOM,
      addUomModalVisible,
      constantRatio,
      accountName,
      accountNames,
      isNSRealmId,
    } = this.state

    return (
      <ThemeModal
        width={700}
        title="Create a new product"
        okText="Create product"
        className="product-vertical"
        visible={visible}
        onOk={this.props.handleOk}
        onCancel={this.props.handleCancel}
        cancelButtonProps={{ type: 'link' }}
        centered={true}
      >
        {isNSRealmId && (
          <ThemeModal
            title={`Edit Value List "Unit of Measurement"`}
            visible={addUomModalVisible}
            onCancel={this.toggleAddUOMModal}
            cancelText="Close"
            okButtonProps={{ style: { display: 'none' } }}
          >
            <TypesEditor isModal={true} field="unitOfMeasure" title="Unit of Measurement" buttonTitle="Add New UOM" />
          </ThemeModal>
        )}
        <Form onSubmit={this.onSave} hideRequiredMark={true}>
          <Row>
            <Col span={12}>
              <Form.Item colon={false} label={<SetionTableLabel className="normal">Name*</SetionTableLabel>}>
                {getFieldDecorator('name', {
                  rules: [{ required: true, message: 'Please input name!' }],
                })(<Input placeholder="Product name" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item colon={false} label={<SetionTableLabel className="normal">SKU</SetionTableLabel>}>
                {getFieldDecorator('sku', {
                  rules: [{ required: false }],
                })(<Input placeholder="SKU code" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item
                colon={false}
                label={
                  <SetionTableLabel className="normal">
                    Base Unit of Measure (UOM)*
                    {/* <Tooltip
                      placement="topLeft"
                      title={`The inventory unit of measure is the unit by which this product's inventory will be managed. For example, for a product that is purchased and sold by case volume, "case" is the inventory unit of measure.`}
                    >
                      <Icon style={ItemHMargin4} type="info-circle" />
                    </Tooltip> */}
                  </SetionTableLabel>
                }
              >
                <Input.Group>
                  <Row>
                    <Col span={12}>
                      {getFieldDecorator('inventoryUOM', {
                        rules: [{ required: true, message: 'Please input category!' }],
                      })(
                        <ThemeSelect
                          style={{ width: '100%' }}
                          onChange={this.onInventoryUomChange}
                          placeholder="Unit of measure for inventory"
                          showSearch
                          optionFilterProp="children"
                          onSearch={() => {}}
                          filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                          dropdownRender={(menu: any) => (
                            <div>
                              {menu}
                              {/* <EditA onMouseDown={e => e.preventDefault()} onClick={this.toggleEditSelect}>Edit...</EditA> */}
                            </div>
                          )}
                        >
                          {companyProductTypes &&
                            companyProductTypes.unitOfMeasure.map(
                              (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                                return (
                                  <Select.Option key={index} value={item.name}>
                                    {item.name}
                                  </Select.Option>
                                )
                              },
                            )}
                        </ThemeSelect>,
                      )}
                    </Col>
                    {isNSRealmId && (
                      <Col span={12}>
                        <ThemeOutlineButton onClick={this.toggleAddUOMModal} style={{ marginLeft: 15 }}>
                          <Icon type="plus" />
                          Add New UOM
                        </ThemeOutlineButton>
                      </Col>
                    )}
                  </Row>
                </Input.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item colon={false} label={<SetionTableLabel className="normal">Vendor</SetionTableLabel>}>
                {getFieldDecorator('clientId', {})(
                  // <Input placeholder="Search vendors" />
                  <AccoutName
                    className="bold-blink account-name-first"
                    value={accountName}
                    placeholder="Search vendors"
                    dataSource={accountNames}
                    onSearch={this.onSearch}
                    onChange={this.handleChangeAccount}
                    onSelect={this.handleSelectAccount}
                  />,
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item colon={false} label={<SetionTableLabel className="normal">Initial Quantity</SetionTableLabel>}>
                {getFieldDecorator('quantity', {
                  rules: [{ required: false }],
                })(<Input placeholder="0" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item
                colon={false}
                label={
                  <SetionTableLabel className="normal">
                    Default cost{inventoryUOM ? ` per ${inventoryUOM}` : ''}
                  </SetionTableLabel>
                }
              >
                {getFieldDecorator('cost', {})(<Input placeholder="$0.00" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item
                colon={false}
                label={
                  <SetionTableLabel className="normal">
                    Default price{inventoryUOM ? ` per ${inventoryUOM}` : ''}
                    <Tooltip
                      placement="topLeft"
                      title="The default price is the spelling price that will be assigned to this product by default. Other, more advanced pricing strategies may be enabled to override this default."
                    >
                      <Icon style={ItemHMargin4} type="info-circle" />
                    </Tooltip>
                  </SetionTableLabel>
                }
              >
                {getFieldDecorator('price', {})(<Input placeholder="$0.00" />)}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </ThemeModal>
    )
  }
}

const InventoryProductVerticalForm = Form.create()(InventoryProductVerticalContainer)
export { InventoryProductVerticalForm }
