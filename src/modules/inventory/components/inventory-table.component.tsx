import React from 'react'
import { Icon, Button, InputNumber, Spin, Tooltip, Select, Input } from 'antd'
import { ColumnProps } from 'antd/es/table'
import { cloneDeep } from 'lodash'
import { SaleItem, UserRole } from '~/schema'
import { QBOImage, NSImage, tableCss, EmptyTr, InputWrapper, ThumbnailImage } from './inventory-table.style'
import qboImage from '~/modules/orders/components/qbo.png'
import { ThemeTable, TextLink } from '~/modules/customers/customers.style'
import {
  getFileUrl,
  initHighlightEvent,
  mathRoundFun,
  basePriceToRatioPrice,
  handlerNoLotNumber,
  getAllowedEndpoint,
} from '~/common/utils'
import InventoryAsterisk from '~/components/inventory/inventoryAsterisk'
import { CACHED_QBO_LINKED, CACHED_NS_LINKED } from '~/common'
import _ from 'lodash'

export interface InventoryTableProps {
  saleItems: SaleItem[]
  editItem: (item: SaleItem) => void
  search: string
  showDrawer: boolean
  onDeleteItem: (itemId: string) => void
  onChangeActiveStatus: (itemId: string, value: boolean) => void
  loading: string
  updateItem: Function
  updateCost: Function
  options: any
  currentUser: any
}

const { Search } = Input
export class InventoryTable extends React.PureComponent<InventoryTableProps> {
  state = {
    filteredCategory: [],
    saveItems: this.props.saleItems,
    selectedItemIndex: -1,
    expandedRowKeys: [],
    searchValue: '',
    inputValue: '',
    show:"none"
  }

  private mutiSortTable = React.createRef<any>()

  constructor(props: InventoryTableProps) {
    super(props)
  }

  componentWillReceiveProps(nextProps: InventoryTableProps) {
    if (this.state.saveItems !== nextProps.saleItems) {
      this.setState(
        {
          saveItems: nextProps.saleItems,
        },
        () => {
          initHighlightEvent()
        },
      )
    }

    if (
      JSON.stringify(this.props.options) !== JSON.stringify(nextProps.options) ||
      this.props.search !== nextProps.search
    ) {
      initHighlightEvent()
    }
    if (nextProps.search == '') {
      this.setState({
        searchValue: '',
        inputValue: '',
        show: "none"
      })
    }else{
      this.setState({
        show: "flex"
      })
    }
  }

  componentDidMount() {
    initHighlightEvent()
  }

  wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }

  updateCategoryFilter = (categoryFilters: any[], filterFrom: number, filterTo: number, otherFilter: any) => {
    let newList = [] as SaleItem[]
    if (categoryFilters.length > 0) {
      for (const categoryId of categoryFilters) {
        const tmp = this.state.saveItems.filter((item) => {
          if (item.wholesaleCategory) {
            return item.wholesaleCategory.wholesaleCategoryId === categoryId
          } else {
            return false
          }
        })
        newList = newList.concat(tmp)
      }
    } else {
      newList = cloneDeep(this.state.saveItems)
    }

    if (filterFrom !== 0 || filterTo !== 0) {
      newList = newList.filter((item) => {
        return item.cost >= filterFrom && item.cost <= filterTo
      })
    }

    let otherItems = cloneDeep(newList)
    let result = []
    let count = 0
    for (const key in otherFilter) {
      const values = otherFilter[key]
      if (values.length > 0) {
        result = []
      }
      for (const value of values) {
        count++
        const tmp = otherItems.filter((item) => {
          return item[key] === value
        })
        result = result.concat(tmp)
      }
      if (result.length > 0) {
        otherItems = cloneDeep(result)
      }
    }
    // if (count === 0) this.setState({ initItems: newList })
    // else this.setState({ initItems: result })
    return count === 0 ? newList : result
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a && a[key] ? a[key] : ''
    const stringB = b && b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onSortVariety = (a: any, b: any) => {
    let stringA = a.variety ? a.variety : ''
    let stringB = b.variety ? b.variety : ''
    stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    //this.props.clearRelatedOrders()
    if (this.state.expandedRowKeys.length > 0) {
      const row = []
      for (const value of expandedRowKeys) if (value !== this.state.expandedRowKeys[0]) row.push(value)
      this.setState({
        expandedRowKeys: row,
      })
    } else {
      this.setState({
        expandedRowKeys: expandedRowKeys,
      })
    }
  }

  // handlePriceChange = debounce((item:any,price:number) => {
  //   console.log(item);
  //   console.log(price);
  // if(item.price != price && price > 0){
  //   this.props.updateItem({
  //     itemId:item.itemId,
  //     price
  //   });
  // }
  // },500)

  handleBlur = (item: any, event: any) => {
    let price = event.target.value
    price = price.replace(`/${item.baseUOM}`, '')
    if (item.price != price && price > 0) {
      this.props.updateItem({
        itemId: item.itemId,
        price,
      })
    }
  }

  handleCostBlur = (item: any, event: any) => {
    let cost = event.target.value
    cost = cost.replace(`/${item.baseUOM}`, '')
    if (item.cost != cost && cost > 0) {
      this.props.updateItem({
        itemId: item.itemId,
        cost,
      })
    }
  }

  redirectDetail = (item: any) => {
    const { currentUser } = this.props
    window.location.href =
      `#/product/` + item.itemId + `/${getAllowedEndpoint('product', currentUser.accountType, 'specifications')}`
  }

  onRow = (record: any) => {
    const { currentUser } = this.props
    return {
      onClick: (_event: any) => {
        _event.preventDefault()
        if (_event.target.tagName !== 'BUTTON') {
          window.location.href =
            `#/product/` +
            record.itemId +
            `/${getAllowedEndpoint('product', currentUser.accountType, 'specifications')}`
        }
      },
    }
  }

  getTextLink = (record: any, content: any) => {
    return content || ''
    // const { currentUser } = this.props
    // return (
    //   <TextLink className="inventory" to={`#/product/` + record.itemId + `/${getAllowedEndpoint('product', currentUser.accountType, 'specifications')}`}>
    //     {content ? content : ' '}
    //   </TextLink>
    // )
  }

  filterSearch = (items: any[]) => {
    const filtered = items.filter((item) => {
      if (item.isLot === true || item.variety && item.variety.toLowerCase().indexOf(this.state.searchValue.toLowerCase()) != -1) {
        return item
      }
    })
    return filtered;
  }

  render() {
    const { search, onChangeActiveStatus, loading, options, currentUser } = this.props
    const { filterCategory, filterCostFrom, filterCostTo, filterOther } = options
    const initItems = this.updateCategoryFilter(filterCategory, filterCostFrom, filterCostTo, filterOther)
    let listItems: SaleItem[] = cloneDeep(initItems)

    if (search) {
      const searchWords = _.words(search, /[^-/ \(\)]+/g)
      listItems = listItems.filter((item) => {
        if (item.isLot === true) {
          // No need the filtering with search in the case of lot
          return item
        }
        if (item.wholesaleCategory) {
          if (
            `${item.variety} ${item.wholesaleCategory.name} ${item.provider} (${item.SKU})`
              .toUpperCase()
              .includes(search.toUpperCase())
          ) {
            return item
          }
          if (
            _.intersection(
              _.words(`${item.variety} ${item.wholesaleCategory.name} ${item.provider} ${item.SKU}`, /[^-/ \(\)]+/g),
              searchWords,
            ).length === searchWords.length
          ) {
            return item
          }
          // return (item.variety + item.wholesaleCategory.name + item.provider + item.SKU)
          //   .toUpperCase()
          //   .includes(search.toUpperCase())
        } else {
          return false
        }
      })
    }

    listItems = listItems.sort((a, b) => b.availableQty - a.availableQty)

    let columns: ColumnProps<SaleItem>[] = [
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        width: 120,
        className: 'th-left',
        sorter: (a, b) => {
          // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'SKU')
          // if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('SKU', b, a)
          return this.onSortString('SKU', a, b)
        },
        render: (text, record) => {
          return this.getTextLink(
            record,
            <span style={{ whiteSpace: 'nowrap', marginLeft: 43 }}>{text ? `${text}` : ''}</span>,
          )
        },
      },
      {
        title: 'CATEGORY',
        className: 'th-left ',
        dataIndex: 'wholesaleCategory',
        key: 'wholesaleCategory.name',
        width: 140,
        sorter: (a, b) => {
          // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
          //   (meta: any) => meta.dataIndex === 'wholesaleCategory.name',
          // )
          // if (sortIndex && sortIndex.sortOrder === 'descend')
          //   return this.onSortString('name', b.wholesaleCategory, a.wholesaleCategory)
          return this.onSortString('name', a.wholesaleCategory, b.wholesaleCategory)
        },
        render: (v, record) => {
          return v ? this.getTextLink(record, v.name) : ''
        },
      },
      {
        title: 'NAME',
        className: 'th-left ',
        dataIndex: 'variety',
        key: 'variety',
        width: 280,
        sorter: (a, b) => {
          // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
          //   (meta: any) => meta.dataIndex === 'variety',
          // )
          // if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortVariety(b, a)
          return this.onSortVariety(a, b)
        },
        // render(text, item) {
        //   return item.organic === true ? (
        //     <>
        //       <QBOImage src={organicImg} /> <span>{text}</span>
        //     </>
        //   ) : (
        //       <span>{text}</span>
        //     )
        // },
        render: (v, record) => this.getTextLink(record, v),
      },
      {
        title: 'ACTIVE',
        className: 'th-left',
        dataIndex: 'active',
        width: 100,
        render: (v, record) => (
          <div onClick={(e) => e.stopPropagation()}>
            <Select onChange={(v) => onChangeActiveStatus(record.itemId, v)} value={v}>
              <Select.Option value={true}>Active</Select.Option>
              <Select.Option value={false}>Inactive</Select.Option>
            </Select>
          </div>
        ),
      },

      // {
      //   title: 'PROVIDER',
      //   dataIndex: 'provider',
      //   key: 'provider',
      //   width: 250,
      //   sorter: (a, b) => {
      //     // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
      //     //   (meta: any) => meta.dataIndex === 'provider',
      //     // )
      //     // if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('provider', b, a)
      //     return this.onSortString('provider', a, b)
      //   },
      // },
      {
        title: 'DEFAULT PRICE',
        dataIndex: 'price',
        className: 'th-left',
        key: 'price',
        width: 160,
        editable: true,
        sorter: (a, b) => {
          return a.price - b.price
        },
        render: (rowPrice, item) => {
          const defaultPricingUOM = item.defaultSellingPricingUOM ? item.defaultSellingPricingUOM : item.inventoryUOM
          const value = basePriceToRatioPrice(defaultPricingUOM, rowPrice, item, 2, true)
          return (
            <InputWrapper className="hideNumberHandler highlight-input">
              <span>$</span>
              <InputNumber
                value={mathRoundFun(value, 2)}
                placeholder="$0.00"
                step="0.25"
                min={0}
                // formatter={val => `${val}/${item.baseUOM}`}
                // parser={val => val.replace(`/${item.baseUOM}`, '')}
                // formatter={(value) => `$${value}`}
                // onChange={this.handlePriceChange.bind(this, item)}
                onBlur={this.handleBlur.bind(this, item)}
                onClick={(event: any) => {
                  event.stopPropagation()
                }}
              />
              <span>/{defaultPricingUOM}</span>
            </InputWrapper>
          )
        },
      },
      {
        title: 'DEFAULT COST',
        dataIndex: 'cost',
        className: 'th-left',
        key: 'cost',
        width: 160,
        sorter: (a, b) => {
          // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'cost')
          // if (sortIndex && sortIndex.sortOrder === 'descend') return b.cost - a.cost
          return a.cost - b.cost
        },
        render: (cost, item) => {
          const defaultCostUOM = item.defaultPurchasingCostUOM ? item.defaultPurchasingCostUOM : item.inventoryUOM
          const value = basePriceToRatioPrice(defaultCostUOM, cost, item)
          return (
            <InputWrapper className="hideNumberHandler highlight-input">
              <span>$</span>
              <InputNumber
                value={mathRoundFun(value, 2)}
                placeholder="$0.00"
                step="0.25"
                min={0}
                // formatter={val => `${val}/${item.baseUOM}`}
                // parser={val => val.replace(`/${item.baseUOM}`, '')}
                // formatter={(value) => `$${value}`}
                // onChange={this.handlePriceChange.bind(this, item)}
                onBlur={this.handleCostBlur.bind(this, item)}
                onClick={(event: any) => {
                  event.stopPropagation()
                }}
              />
              <span>/{defaultCostUOM}</span>
            </InputWrapper>
          )
        },
      },
      {
        title: 'UNITS ON HAND',
        dataIndex: 'onHandQty',
        className: 'th-left ',
        key: 'onHandQty',
        width: 140,
        sorter: (a: any, b: any) => {
          return handlerNoLotNumber(a, 2) - handlerNoLotNumber(b, 2)
        },
        render: (onHandQty, record) => {
          return (
            <TextLink
              className="inventory"
              to={
                `#/product/` +
                record.itemId +
                `/${getAllowedEndpoint('product', currentUser.accountType, 'specifications')}`
              }
            >
              {handlerNoLotNumber(record, 2)}
              <InventoryAsterisk item={record} flag={2}></InventoryAsterisk>
            </TextLink>
          )
        },
        /*
        sorter: (a, b) => {
          // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
          //   (meta: any) => meta.dataIndex === 'quantity',
          // )
          // if (sortIndex && sortIndex.sortOrder === 'descend') return b.quantity - a.quantity
          return a.availableQty - b.availableQty
        },
        render(text: any) {
          return (
            <Stock stock={Number(text)}>
              {text ? formatNumber(text, 2) : 0.00}
              <Icon type="polygon" css={polygonCss} />
            </Stock>
          )
        },
        */
      },
    ]

    if (localStorage.getItem(CACHED_QBO_LINKED) != 'null')
      columns.push({
        title: 'QBO',
        dataIndex: 'qboId',
        key: 'qboId',
        width: 40,
        className: '',
        render: (text, record) => {
          if (text) {
            return (
              <Tooltip placement="top" title={text}>
                {this.getTextLink(record, <QBOImage src={qboImage} />)}
              </Tooltip>
            )
          } else {
            return this.getTextLink(record, '')
          }
        },
      })
    if (localStorage.getItem(CACHED_NS_LINKED) != 'null')
      columns.push({
        title: 'NS',
        dataIndex: 'nsId',
        key: 'nsId',
        width: 40,
        className: '',
        render: (text, record) => {
          if (text) {
            const nsRealmId = localStorage.getItem(CACHED_NS_LINKED)
            const act = 'item.nl'
            return (
              <Tooltip placement="top" title={text}>
                <a
                  className="inventory"
                  href={`https://${nsRealmId}.app.netsuite.com/app/common/item/${act}?id=${text}`}
                  target="_blank"
                >
                  <NSImage />
                </a>
              </Tooltip>
            )
          } else {
            return this.getTextLink(record, '')
          }
        },
      })

    columns.push(
      ...[
        {
          title: 'SIZE',
          dataIndex: 'size',
          className: 'th-left ',
          key: 'size',
          width: 100,
          sorter: (a, b) => {
            // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'size')
            // if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('size', b, a)
            return this.onSortString('size', a, b)
          },
          render: (v, record) => this.getTextLink(record, v),
        },
        {
          title: 'WEIGHT',
          dataIndex: 'weight',
          className: 'th-left ',
          key: 'weight',
          width: 100,
          sorter: (a, b) => {
            // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'weight')
            // if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('weight', b, a)
            // return this.onSortString('weight', a, b)
            return a.weight - b.weight
          },
          render: (v, record) => this.getTextLink(record, v),
        },
        {
          title: 'GRADE',
          dataIndex: 'grade',
          className: 'th-left ',
          key: 'grade',
          width: 120,
          sorter: (a, b) => {
            // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'grade')
            // if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('grade', b, a)
            return this.onSortString('grade', a, b)
          },
          render: (v, record) => this.getTextLink(record, v),
        },
        {
          title: 'PACKING',
          dataIndex: 'packing',
          className: 'th-left ',
          key: 'packing',
          width: 120,
          sorter: (a, b) => {
            // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            //   (meta: any) => meta.dataIndex === 'packing',
            // )
            // if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('packing', b, a)
            return this.onSortString('packing', a, b)
          },
          render: (v, record) => this.getTextLink(record, v),
        },
        {
          title: 'ORIGIN',
          dataIndex: 'origin',
          className: 'th-left ',
          key: 'origin',
          width: 206,
          sorter: (a, b) => {
            const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
              (meta: any) => meta.dataIndex === 'origin',
            )
            if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('origin', b, a)
            return this.onSortString('origin', a, b)
          },
          render: (v, record) => this.getTextLink(record, v),
        },
      ],
    )

    columns.push({
      width: 120,
      className: 'buttonCell',
      render: (_, saleItem) => {
        return (
          <>
            {loading === saleItem.itemId ? (
              <Spin />
            ) : (
              <Button type="link" onClick={this.redirectDetail.bind(this, saleItem)}>
                VIEW <Icon type="arrow-right" />
              </Button>
            )}
          </>
        )
      },
    })
    columns.push({
      title: '',
      dataIndex: 'cover',
      key: 'cover',
      width: 40,
      className: 'pr-50',
      render: (text: string, record) => {
        if (text) {
          return this.getTextLink(record, <ThumbnailImage src={getFileUrl(text, true)} />)
        } else {
          return this.getTextLink(record, '')
        }
      },
    })

    let finalColumns = columns.filter((el) => {
      if (
        currentUser.accountType === UserRole.WAREHOUSE ||
        currentUser.accountType == UserRole.SALES ||
        currentUser.accountType == UserRole.BUYER
      ) {
        if (el.dataIndex === 'price' || el.dataIndex === 'cost') {
          return false
        }
        return true
      } else {
        return true
      }
    })

    const filteredItems = this.filterSearch(listItems)

    return (
      <>
        <div
          style={{
            height: '30px',
            display: this.state.show ,
            marginTop: '5px',
            marginLeft: '60px',
            marginBottom: '5px',
          }}
        >
          <p style={{ height: '30px', lineHeight: '30px', marginRight: '30px', fontWeight: 'bold' }}>{`${
            filteredItems.length
          } Items`}</p>
          <p style={{ height: '30px', lineHeight: '30px', marginRight: '10px' }}>{`Search within results: `}</p>
          <Search
            placeholder="Search text"
            onSearch={(e) => {
              console.log(e)
              this.setState({
                searchValue: e,
              })
            }}
            onChange={(e) => {
              this.setState({
                inputValue: e.target.value,
              })
            }}
            value={this.state.inputValue}
            style={{ width: 200 }}
            allowClear
          />
        </div>
        <ThemeTable
          ref={this.mutiSortTable}
          css={tableCss(false, true)}
          pagination={{ pageSize: 12 }}
          columns={finalColumns}
          dataSource={filteredItems}
          rowKey="itemId"
          onRow={this.onRow}
          style={{ paddingRight: 40 }}
          pagination={{ current: this.props.page + 1, defaultCurrent: 0, onChange: this.props.onChangePage }}
        />
      </>
    )
  }
}
