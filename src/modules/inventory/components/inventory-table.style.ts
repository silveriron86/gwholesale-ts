import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { css as createCss } from 'emotion'

import nsImage from '~/modules/orders/components/netsuite.svg'
import { transparent, yellow, mutedGreen, red, brightGreen, white, lightGrey, black } from '~/common'
import { Table } from 'antd'

export const TableTr = styled('tr')({
  height: '50px',
})

export const expandDivClx = css({})

export const tableCss = (hasPadding: boolean, isInventory = false) =>
  css({
    '&': {
      padding: hasPadding ? '0 50px' : '0',
      '.ant-pagination': {
        paddingRight: '30px',
      },
      '& .ant-table-content': {
        '& .ant-table-body': {
          '& > table': {
            borderTop: 'none',
            '& .ant-table-thead': {
              boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
              '& > tr > th:first-of-type': {
                paddingLeft: isInventory ? '59px' : '',
              },
              '& > tr > th:last-of-type': {
                paddingRight: isInventory ? '50px' : '',
              },
              '& > tr > th': {
                backgroundColor: white,
                border: 'none',
              },
            },
            [`& ${TableTr} td`]: {
              padding: '3px 10px',
              textAlign: 'left',
              whiteSpace: 'pre-wrap',
              wordWrap: 'break-word',
            },
            '& .ant-table-tbody': {
              '& > tr > .buttonCell': {
                paddingTop: '0px',
                paddingBottom: '0px',
                paddingRight: '0px',
                paddingLeft: '7px',
                minWidth: '110px',
              },
              '& > tr > td:first-of-type > span': {
                marginLeft: isInventory ? '50px' : '',
              },
              '& > tr > td': {
                paddingTop: '19px',
                paddingBottom: '14px',
                paddingLeft: isInventory?'16px !important':'9px',
              },
            },
          },
        },
      },
    },
  })

export const purchaseTableCss = (hasPadding: boolean) =>
  css({
    padding: hasPadding ? '0 50px' : '0',
    '.ant-pagination': {
      paddingRight: '30px',
    },
    '& .ant-table-content': {
      '& .ant-table-body': {
        '& > table': {
          borderTop: 'none',
          '& .ant-table-thead': {
            boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
            '& > tr > th:first-of-type': {
              paddingLeft: '90px',
            },
            '& > tr > th:last-of-type': {},
            '& > tr > th': {
              backgroundColor: white,
              border: 'none',
            },
          },
          [`& ${TableTr} td`]: {
            padding: '3px 10px',
            textAlign: 'left',
            whiteSpace: 'pre-wrap',
            wordWrap: 'break-word',
          },
          '& .ant-table-row > td:first-of-type': {
            paddingLeft: '90px',
          },
          '& .ant-table-row > td': {
            paddingLeft: '9px',
            borderBottom: '2px solid #EDF1EE',
            paddingTop: '19px',
            paddingBottom: '14px',
          },
        },
      },
    },
  })

export const InventoryTable = styled(Table)({
  '.ant-pagination': {
    display: 'none',
  }
})

export const Category = styled('td')({
  position: 'absolute',
  width: '100vw',
  height: '50px',
  textAlign: 'center',
  fontSize: '24px',
  lineHeight: '20px',
  fontFamily: '"Museo Sans Rounded"',
})

export const CategoryTr = styled('tr')({
  height: '50px',
  [`&:hover:not(.ant-table-expanded-row) ${Category}`]: {
    background: transparent,
  },
})

export const columnClass = createCss({
  boxShadow: '6px 0 6px -4px rgba(0,0,0,0.15)',
})

export const Stock = styled('div')(({ stock }: { stock: number }) => ({
  borderLeft: `solid 5px ${stock === 0 ? yellow : stock > 0 ? mutedGreen : red}`,
  height: '17px',
  display: 'flex',
  fontWeight: 700,
  alignItems: 'center',
  justifyContent: 'center',
  paddingLeft: '10px',
}))

export const polygonCss = css({
  width: '12px',
  height: '12px',
  marginLeft: '5px',
})

export const Quantity = styled('div')({
  display: 'flex',
  justifyContent: 'space-evenly',
  lineHeight: '40px',
})

export const PolygonWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-around',
  alignContent: 'center',
  height: '40px',
})

export const quantityPolygonCss = css({
  width: '12px',
  height: '12px',
  color: brightGreen,
  '&:nth-child(1)': {
    transform: 'rotate(180deg)',
  },
})

export const TextWrapper = styled('span')(({ len }: { len: number }) => ({
  paddingRight: len < 10 ? '6px' : '0',
}))

export const AddNewItem = styled('td')({
  position: 'absolute',
  width: '100vw',
  height: '55px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
})

export const AddNewItemTr = styled('tr')(
  ({ isEditing }: { isEditing: boolean }) => ({
    [`& ${AddNewItem}`]: {
      backgroundColor: isEditing ? transparent : 'rgba(38, 172, 95, 0.2)',
    },
    [`&:hover:not(.ant-table-expanded-row) ${AddNewItem}`]: {
      backgroundColor: isEditing ? transparent : 'rgba(38, 172, 95, 0.2)',
    },
  }),
  {
    height: '55px',
  },
)

export const AddItemsWrapper = styled('div')(
  ({ isEditing, theme }: { isEditing: boolean; theme: any }) => ({
    backgroundColor: isEditing ? theme.primary : 'transparent',
    color: isEditing ? white : theme.primary,
  }),
  {
    display: 'flex',
    fontSize: '16px',
    justifyContent: 'center',
    alignItems: 'center',
    userSelect: 'none',
    cursor: 'pointer',
    height: '55px',
    padding: '0 10px',
  },
)

export const PlusButton = styled('span')((props) => ({
  borderRadius: '50%',
  width: '20px',
  height: '20px',
  backgroundColor: props.theme.primary,
  fontSize: '20px',
  lineHeight: '20px',
  textAlign: 'center',
  color: white,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginRight: '10px',
}))

export const RadioWrapper = styled('div')({
  display: 'flex',
  padding: '0 40px',
  borderRight: `1px solid ${lightGrey}`,
  marginRight: '5px',
})

export const autocompleteCss = css({
  flexGrow: 1,
  position: 'relative',
  '& .ant-select-selection': {
    padding: '0 60px',
    '& .ant-select-search': {
      '& .ant-input': {
        borderWidth: 0,
        borderRightWidth: '0 !important',
      },
    },
  },
})

export const EmptyTr = styled('tr')({
  height: '15px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})

export const searchIconStyle = css({
  color: brightGreen,
  position: 'absolute',
  left: '340px',
})

export const Price = styled('span')({
  fontFamily: '"Museo Sans Rounded"',
  color: black,
  fontSize: '18px',
})

export const NSImage: any = styled(nsImage)((props) => ({
  width: '24px',
  height: '24px',
}))

export const QBOImage: any = styled('img')((props) => ({
  width: '24px',
  height: '24px',
  backgroundColor: props.theme.primary,
  borderRadius: 12,
}))

export const ThumbnailImage: any = styled('img')((props) => ({
  width: '36px',
  height: '36px',
  backgroundColor: `${props.theme.primary} !important`,
  borderRadius: 18,
}))

export const TableWrapper = styled('div')`
  display: flex;
  flex-direction: row wrap;
  justify-content: flex-start;
  align-items: flex-start;
`

export const ColumnSettingWrapper = styled('div')`
  position: relative;
`

export const SettingHeader = styled('div')`
  z-index: 20;
  display: flex;
  flex-direction: row wrap;
  justify-content: space-between;
  align-items: center;

  width: 163px;
  height: 53px;
  padding: 16px 16px 15px 9px;
  font-size: 12px;

  background: #e6e6e6;
`

export const SettingText = styled('span')`
  font-family: Museo Sans Rounded;
  font-size: 12px;
  line-height: 18px;
  letter-spacing: 0.05em;
  text-transform: uppercase;
  color: rgba(0, 0, 0, 0.85);
  font-weight: bold;
  &:after {
    content: 'Settings';
  }
`

export const SettingDressWrapper = styled('div')`
  position: absolute;
  top: 0;
  left: -51px;
  width: 52px;
  height: 52px;
  background-color: #fff;
  overflow: hidden;
`

export const SettingDress = styled('div')`
  width: 73px;
  height: 73px;
  margin-top: 16px;
  margin-left: 15px;
  background: #e6e6e6;

  transform: rotate(45deg);
`

export const inputCss = css({
  paddingRight: '10px',
  textAlign: 'right',
  width: '82px',
})

export const InputWrapper = styled('div')({
  position: 'relative',
  '&.hideNumberHandler': {
    display: 'flex',
    alignItems: 'center',
    '& .ant-input-number': {
      // width: '40% !important',
      // minWidth: 40
      width: 60,
      minWidth: 60,
    },
  },
})
