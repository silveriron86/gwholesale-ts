/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Input, Button, Row, Col, Cascader, Switch, Form, InputNumber, Spin } from 'antd'
import { withTheme } from 'emotion-theming'
import lodash from 'lodash'
import { FormComponentProps } from 'antd/lib/form'

import { SaleItem, SaleCategory } from '~/schema'

import { DialogBody, FormLabel, rowCss, DialogFooter, getButtonStyle } from './item-detail-form.style'
import { Theme } from '~/common'
import { InventoryLoading } from '../inventory.module';

export interface ItemDetailFormProps {
  item: SaleItem
  categories: SaleCategory[]
  onCloseDrawer: () => void
  theme: Theme
  onSubmit: (payload: FormFields) => void
  loading: string
  waiting: boolean
  resetLoading: Function
}

interface FormFields {
  provider: string
  sku: string
  categoryOptions: string[]
  cost: number
  grade: string
  isOrganic: true
  origin: string
  packing: string
  size: string
  variety: string
  weight: string
}

class ItemDetailFormComponent extends React.PureComponent<FormComponentProps<FormFields> & ItemDetailFormProps> {
  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const { form, onSubmit } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        this.props.resetLoading()
        const categoryPayload = this.getCategoryPayload(values.categoryOptions)
        onSubmit(lodash.omit({
          ...values,
          ...categoryPayload
        }, 'categoryOptions'))
      }
    })
  }

  getCategoryPayload = (categoryOptions: string[]) => {
    if (categoryOptions.length > 0) {
      const category = this.props.categories.find((category: SaleCategory) => category.wholesaleCategoryId === categoryOptions[1])
      return {
        // wholesaleCategoryId: categoryOptions[1],
        // category: category!.name,
        wholesaleCategory: category!,
      }
    }
    return {}
  }

  render() {
    const { item, categories, form: { getFieldDecorator } } = this.props

    let itemCategory = [] as any[]
    const categoryOptions = []
    if (categories.length > 0)
    {
      const sections = {}
      for (const category of categories)
      {
        if (sections[category.wholesaleSection.wholesaleSectionId] == null)
          sections[category.wholesaleSection.wholesaleSectionId] = {
            value: category.wholesaleSection.wholesaleSectionId,
            label: category.wholesaleSection.name,
            children: [] }
        sections[category.wholesaleSection.wholesaleSectionId].children.push({
          value: category.wholesaleCategoryId,
          label:category.name
        })
      }
      for (const section in sections)
        categoryOptions.push(sections[section])
      itemCategory = [categories[0].wholesaleSection.wholesaleSectionId, categories[0].wholesaleCategoryId]
    }

    if (item != null && item.wholesaleCategory != null)
      itemCategory = [item.wholesaleCategory.wholesaleSection.wholesaleSectionId, item.wholesaleCategory.wholesaleCategoryId]

    return (
      <Form onSubmit={this.handleSubmit}>
        <DialogBody>
          <Row css={rowCss} gutter={24}>
            <Col span={24}>
              <FormLabel>NAME</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('variety', {
                  initialValue: item.variety,
                  validateFirst: true,
                  rules: [
                    { required: true, message: 'Please input item name!' }
                  ]
                })(
                  <Input
                    placeholder="ITEM NAME"
                    style={{ width: '90%' }}
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss} gutter={24}>
            <Col span={12}>
              <FormLabel>SKU</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('sku', {
                  initialValue: item.SKU,
                  validateFirst: true,
                })(
                  <Input
                    placeholder="SKU"
                    style={{ width: '80%' }}
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <FormLabel>COST</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('cost', {
                  initialValue: item.cost,
                  validateFirst: true,
                  rules: [
                    { required: true, message: 'Please input cost!' },
                    { type: 'number', message: 'Cost must be a number!' }
                  ]
                })(
                  <InputNumber
                    placeholder="0.00"
                    style={{ width: '80%' }}
                    type="number"
                    step="0.05"
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss}>
            <Col span={24}>
              <FormLabel>CATEGORY</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('categoryOptions', {
                  initialValue: itemCategory
                })(
                  <Cascader options={categoryOptions} />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss} gutter={24}>
            <Col span={24}>
              <FormLabel>FARMER/PROVIDER</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('provider', {
                  initialValue: item.provider
                })(
                  <Input
                    placeholder="PROVIDER"
                    style={{ width: '90%' }}
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss} gutter={24}>
            <Col span={12}>
              <FormLabel>GRADE</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('grade', {
                  initialValue: item.grade
                })(
                  <Input
                    placeholder="GRADE"
                    style={{ width: '80%' }}
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <FormLabel>WEIGHT</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('weight', {
                  initialValue: item.weight
                })(
                  <Input
                    placeholder="WEIGHT"
                    style={{ width: '80%' }}
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss} gutter={24}>
            <Col span={24}>
              <FormLabel>SIZE</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('size', {
                  initialValue: item.size
                })(
                  <Input
                    placeholder="SIZE"
                    style={{ width: '80%' }}
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss} gutter={24}>
            <Col span={24}>
              <FormLabel>PACKING TYPE</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('packing', {
                  initialValue: item.packing
                })(
                  <Input
                    placeholder="PACKING"
                    style={{ width: '90%' }}
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss} gutter={24}>
            <Col span={24}>
              <FormLabel>ORIGIN</FormLabel>
              <br />
              <Form.Item>
                {getFieldDecorator('origin', {
                  initialValue: item.origin
                })(
                  <Input
                    placeholder="ORIGIN"
                    style={{ width: '90%' }}
                  />
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row css={rowCss} gutter={24}>
            <Col span={12}>
              <FormLabel>ORGANIC</FormLabel>
              <br />
              <br />
              {getFieldDecorator('isOrganic', {
                initialValue: item.organic,
                valuePropName: 'checked'
              })(
                <Switch />
              )}
            </Col>
            <Col span={12} />
          </Row>
        </DialogBody>
        <DialogFooter>
          <Button
            icon={this.props.waiting === true ? '': 'check'}
            type="primary"
            style={getButtonStyle(this.props.theme)}
            htmlType="submit"
            disabled={this.props.waiting === true}
          >
            { this.props.waiting === true ? (<Spin />) : ('SAVE') }
          </Button>
          <Button key="back" onClick={this.props.onCloseDrawer}>CANCEL</Button>
        </DialogFooter>
      </Form>
    )
  }
}

export const ItemDetailForm = withTheme(Form.create<FormComponentProps<FormFields> & ItemDetailFormProps>()(ItemDetailFormComponent))
