import styled from '@emotion/styled'
import { lightGrey, brightGreen, brownGrey, white, Theme } from '~/common'
import { css } from '@emotion/core'

const inputOverrideCss = {
  marginTop: '15px',
  border: 'none',
  borderBottom: '1px solid black',
  borderRadius: 0,
  '&:focus': {
    boxShadow: 'none',
  },
}

export const DialogBody = styled('div')({
  //display: 'flex',

  '& .ant-input-affix-wrapper .ant-input-suffix': {
    marginTop: '5px',
  },

  '& .ant-input': inputOverrideCss,
  '& .ant-input-number': inputOverrideCss,

  '& .ant-input-number-focused': {
    border: 'none',
    borderBottom: '1px solid black',
    boxShadow: 'none',
  }
})

export const rowCss = css({

  marginTop: '24px'
})

export const drawerCss = css({
  '.ant-drawer': {
    marginTop: '30px'
  },

})

export const FormLabel = styled('label')({
  color: lightGrey,
})

export const PlusButton = styled('span')({
  borderRadius: '50%',
  width: '18px',
  height: '18px',
  backgroundColor: brightGreen,
  fontSize: '18px',
  lineHeight: '18px',
  textAlign: 'center',
  color: white,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginRight: '10px',
})

export const InputWrapper = styled('div')({
  position: 'relative'
})

export const inputIcon = css({
  position: 'absolute',
  left: '10px',
  top: '0.4em'
})

export const inputCss = css({
  textAlign: 'right',
  width: '178px',
})

export const colFormCss = css({
  borderRight: `1px solid ${lightGrey}`,
})

export const DialogFooter = styled('div')({
  width: '100%',
  boxSizing: 'border-box',
  padding: '50px 0',
  marginTop: '10px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  '& > div': {
    color: brownGrey,
    fontSize: '14px',
    lineHeight: '15px',
    textDecoration: 'underline',
    marginLeft: '25px',
    cursor: 'pointer',
  },
})

export const Flex = styled('div')({
  display: 'flex',
})

export const getButtonStyle = (theme: Theme): React.CSSProperties => ({
  borderRadius: '15.5px',
  height: '31px',
  width: '90px',
  margin: '0 11px',
  border: `1px solid ${theme.primary}`,
  backgroundColor: theme.primary,
})
