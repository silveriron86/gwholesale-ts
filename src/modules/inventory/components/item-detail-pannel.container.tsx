/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Drawer, Row, Col } from 'antd'

import { MessageType } from '~/components'
import { SaleItem, PriceSheet, PriceSheetItem, SaleCategory } from '~/schema'

import { ItemDetailForm } from './item-detail-form.component'
import { ItemDetailPriceSheets } from './item-detail-pricesheets.component'
import { colFormCss } from './item-detail-form.style'
import { InventoryDispatchProps } from '../inventory.module'

export interface ItemDetailPannelProps {
  item: SaleItem
  priceSheets: PriceSheet[]
  showDrawer: boolean
  priceSheetItems: PriceSheetItem[]
  showPriceSheets: boolean
  categories: SaleCategory[]
  onShowPriceSheets: () => void
  onCloseDrawer: () => void
  onChangeEditInput: (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => void
  saveItem: InventoryDispatchProps['saveItem']
  onSubmit: (payload: any) => void
  onSavePriceSheet: (priceSheetItem: PriceSheetItem) => () => void
  message: string
  type: MessageType | null
  description: string
  onAddToPriceSheet: (itemId: string, priceSheetId: string) => () => void
  onRemoveFromPriceSheet: (itemId: string, priceSheetItemId: string) => () => void
  onChangeEditCategory: (value: string[]) => void
  onChangeEditSwitch: (key: string) => (value: boolean) => void
  loading: string
  waiting: boolean
  resetLoading: Function
}

export class ItemDetailPannel extends React.PureComponent<ItemDetailPannelProps> {
  render() {
    const item = this.props.item
    return (
      <Drawer
        title={item.itemId != null ? item.variety : 'New Item'}
        placement={'right'}
        closable
        onClose={this.props.onCloseDrawer}
        visible={this.props.showDrawer}
        className="new-item-drawer"
        width={item.itemId != null ? '789' : '447'}
      >
        {item.itemId ? (
          <Row gutter={24}>
            <Col span={14} css={colFormCss}>
              <ItemDetailForm
                onSubmit={this.props.onSubmit}
                categories={this.props.categories}
                item={item}
                onCloseDrawer={this.props.onCloseDrawer}
                loading={this.props.loading}
                waiting={this.props.waiting}
                newItemId={this.props.newItemId}
                resetLoading={this.props.resetLoading}
              />
            </Col>
            <Col span={10}>
              <h2>Price Sheets</h2>
              <br />
              <ItemDetailPriceSheets
                loading={this.props.loading}
                onAddToPriceSheet={this.props.onAddToPriceSheet}
                onRemoveFromPriceSheet={this.props.onRemoveFromPriceSheet}
                onSavePriceSheet={this.props.onSavePriceSheet}
                showPriceSheets={this.props.showPriceSheets}
                onShowPriceSheets={this.props.onShowPriceSheets}
                item={item}
                priceSheets={this.props.priceSheets}
                priceSheetItems={this.props.priceSheetItems}
              />
            </Col>
          </Row>
        ) : (
          <ItemDetailForm
            onSubmit={this.props.onSubmit}
            categories={this.props.categories}
            item={item}
            onCloseDrawer={this.props.onCloseDrawer}
            loading={this.props.loading}
            waiting={this.props.waiting}
            resetLoading={this.props.resetLoading}
          />
        )}
      </Drawer>
    )
  }
}
