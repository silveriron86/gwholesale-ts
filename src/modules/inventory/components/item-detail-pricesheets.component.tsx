/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import update from 'immutability-helper'
import { Input, Button, Collapse, List, Spin, Icon } from 'antd'

import { SaleItem, PriceSheet, PriceSheetItem } from '~/schema'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'

import { FormLabel, getButtonStyle, inputCss, inputIcon, InputWrapper, PlusButton } from './item-detail-form.style'

import { cloneDeep } from 'lodash'

export interface ItemDetailPriceSheetsProps {
  item: SaleItem
  priceSheets: PriceSheet[]
  priceSheetItems: PriceSheetItem[]
  showPriceSheets: boolean
  onShowPriceSheets: () => void
  onSavePriceSheet: (priceSheetItem: PriceSheetItem) => () => void
  theme: Theme
  onAddToPriceSheet: (itemId: string, priceSheetId: string) => () => void
  onRemoveFromPriceSheet: (itemId: string, priceSheetItemId: string) => () => void
  loading: string
}

class ItemDetailPriceSheetsComponent extends React.PureComponent<ItemDetailPriceSheetsProps> {
  state = {
    initItems: cloneDeep(this.props.priceSheetItems),
    saveItems: this.props.priceSheetItems,
    selectedItemIndex: -1,
  }

  componentWillReceiveProps(nextProps: ItemDetailPriceSheetsProps)
  {
    if (this.state.saveItems != nextProps.priceSheetItems)
      this.setState({
        saveItems: nextProps.priceSheetItems,
        initItems: cloneDeep(nextProps.priceSheetItems),
      })
  }

  renderPriceSheetItems() {
    const Panel = Collapse.Panel;
    const { item } = this.props
    let initItems = this.state.initItems

    const onMarginChange = (itemId: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
      const newMargin = e.target.valueAsNumber

      var items = initItems
      const index = items.findIndex(i=> i.priceSheetItemId === itemId)
      const item = items[index]
      var newSalePrice = Math.round((item.cost / (1.0 - (newMargin / 100.0)) + item.freight) * 4) / 4
      var updatedItem = update(items[index], {margin: {$set: newMargin}, salePrice: {$set: newSalePrice}})
      var newItems = update(items, {
        $splice: [[index, 1, updatedItem]]
      })

      this.setState({
        initItems: newItems
      })
    }

    const onFreightChange = (itemId: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
      const newFreight = e.target.valueAsNumber

      var items = initItems
      const index = items.findIndex(i=> i.priceSheetItemId === itemId)
      const item = items[index]
      var newSalePrice = Math.round((item.cost / (1.0 - (item.margin / 100.0)) + newFreight) * 4) / 4
      var updatedItem = update(items[index], {freight: {$set: newFreight}, salePrice: {$set: newSalePrice}})
      var newItems = update(items, {
        $splice: [[index, 1, updatedItem]]
      })

      this.setState({
        initItems: newItems
      })
    }

    const onSalePriceChange = (itemId: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
      const newSalePrice = e.target.valueAsNumber

      var items = initItems
      const index = items.findIndex(i=> i.priceSheetItemId === itemId)
      var updatedItem = update(items[index], {salePrice: {$set: newSalePrice}})
      var newItems = update(items, {
        $splice: [[index, 1, updatedItem]]
      })

      this.setState({
        initItems: newItems
      })
    }

    return initItems.map((priceSheetItem) => (
        <Panel header={priceSheetItem.priceSheetName ? priceSheetItem.priceSheetName : ""} key={priceSheetItem.priceSheetItemId}>
          <div style={{marginLeft:"36px"}}>
            <FormLabel>COST</FormLabel>
            <InputWrapper>
              <Input
              value={item.cost}
              placeholder="0.00" step="1" type="number" css={inputCss} disabled={true} />
              <b css={inputIcon}>$</b>
            </InputWrapper>
          </div>
          <div style={{marginLeft:"36px", marginTop:"10px"}}>
            <FormLabel>MARGIN</FormLabel>
            <InputWrapper>
              <Input
              value={priceSheetItem.margin}
              placeholder="0" step="1" type="number" css={inputCss} onChange={onMarginChange(priceSheetItem.priceSheetItemId)} />
              <b css={inputIcon}>%</b>
            </InputWrapper>
          </div>
          <div style={{marginLeft:"36px", marginTop:"10px"}}>
            <FormLabel>FREIGHT</FormLabel>
            <InputWrapper>
              <Input
              value={priceSheetItem.freight}
              placeholder="0.00" step="0.25" type="number" css={inputCss} onChange={onFreightChange(priceSheetItem.priceSheetItemId)} />
              <b css={inputIcon}>$</b>
            </InputWrapper>
          </div>
          <div style={{marginLeft:"36px", marginTop:"10px"}}>
            <FormLabel>SALE PRICE</FormLabel>
            <InputWrapper>
              <Input
              value={priceSheetItem.salePrice}
              placeholder="0.00" step="1" type="number" css={inputCss} onChange={onSalePriceChange(priceSheetItem.priceSheetItemId)} />
              <b css={inputIcon}>$</b>
            </InputWrapper>
          </div>
          <div style={{marginLeft:"36px", marginTop:"20px"}}>
            <Button
              type="primary"
              icon={this.props.loading == priceSheetItem.priceSheetItemId ? ( '' ): ( 'check' )}
              style={{
                ...getButtonStyle,
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.props.onSavePriceSheet(priceSheetItem)}
              disabled={this.props.loading == priceSheetItem.priceSheetItemId}
            >
              { this.props.loading == priceSheetItem.priceSheetItemId ? ( <Spin /> ): ( 'SAVE' ) }
            </Button>
            <Button style={{ marginLeft: '5px' }} type="default"
              onClick={this.props.onRemoveFromPriceSheet(this.props.item.itemId, priceSheetItem.priceSheetItemId)}
            >
              <Icon type="delete" />
            </Button>
          </div>
        </Panel>
    ))
  }


  render() {
    const { priceSheets } = this.props
    const initItems = this.state.initItems
    const priceSheetList = priceSheets.filter((item1) => {
      return initItems.filter((item2) => {
        return item1.priceSheetId == item2.priceSheetId
      }).length == 0
    })
    return (
      <div>
        <Collapse accordion>
          {this.renderPriceSheetItems()}
        </Collapse>
        <div style={{marginTop:"20px"}}>
          <span>Add Item to:</span><br />
          <div style={{marginTop:"10px"}}>
            {this.props.showPriceSheets == false ? (
              <Button style={{border: 0}} onClick={this.props.onShowPriceSheets}>
                <PlusButton>+</PlusButton> Existing Price Sheets
              </Button>
              ) : (
                <List
                  size="small"
                  bordered
                  dataSource={priceSheetList}
                  renderItem={(priceSheet: PriceSheet) => (
                    <List.Item actions={[(<PlusButton onClick={this.props.onAddToPriceSheet(this.props.item.itemId, priceSheet.priceSheetId)}>+</PlusButton>)]}>
                    {priceSheet.name}</List.Item>
                  )}
                />
            )}
          </div>
        </div>
      </div>
    )
  }
}

export const ItemDetailPriceSheets = withTheme(ItemDetailPriceSheetsComponent)
