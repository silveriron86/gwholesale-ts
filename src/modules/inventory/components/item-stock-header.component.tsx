/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Button } from 'antd'
import { Icon } from 'antd'
import { SaleItem } from '~/schema'

import {
  Header,
  HeaderOptions,
  buttonStyle,
  BackButton,
  InfoWrap,
  Info,
  InfoItem,
  ItemName,
  Flex,
  InfoItemLabel,
  InfoItemText,
} from './inventory-header.style'

export interface ItemStockHeaderProps {
  onShowDrawer: () => void
  item: SaleItem
  onClickBack: () => ReduxActions.Action<void>
}

export class ItemStockHeader extends React.PureComponent<ItemStockHeaderProps> {
  constructor(props: ItemStockHeaderProps) {
    super(props)
  }

  render() {
    const { item } = this.props

    return (
      <Header>
        <HeaderOptions>
          <Flex style={{ marginTop: '36px' }}>
            <BackButton onClick={this.props.onClickBack}>
              <Icon type="arrow-left" />
              <span>BACK</span>
            </BackButton>
            {this.props.item != null ? (
              <InfoWrap>
                <span style={{ fontWeight: 700 }}>ITEM STOCK DETAIL</span>
                <ItemName>
                  {item.wholesaleCategory ? item.wholesaleCategory.name : ''} {item.variety}
                </ItemName>
                <Info>
                  <InfoItem>
                    <InfoItemLabel>SKU</InfoItemLabel>
                    <InfoItemText>{item.SKU ? item.SKU : '-'}</InfoItemText>
                  </InfoItem>
                  <InfoItem>
                    <InfoItemLabel>SIZE</InfoItemLabel>
                    <InfoItemText>{item.size ? item.size : '-'} </InfoItemText>
                  </InfoItem>
                  <InfoItem>
                    <InfoItemLabel>WEIGHT</InfoItemLabel>
                    <InfoItemText>{item.weight ? item.weight : '-'}</InfoItemText>
                  </InfoItem>
                  <InfoItem>
                    <InfoItemLabel>GRADE</InfoItemLabel>
                    <InfoItemText>{item.grade ? item.grade : '-'} </InfoItemText>
                  </InfoItem>
                  <InfoItem>
                    <InfoItemLabel>PACKING</InfoItemLabel>
                    <InfoItemText>{item.packing ? item.packing : '-'} </InfoItemText>
                  </InfoItem>
                  <InfoItem>
                    <InfoItemLabel>ORIGIN</InfoItemLabel>
                    <InfoItemText>{item.origin ? item.origin : '-'}</InfoItemText>
                  </InfoItem>
                  <InfoItem>
                    <InfoItemLabel>PROVIDER</InfoItemLabel>
                    <InfoItemText>{item.provider ? item.provider : '-'}</InfoItemText>
                  </InfoItem>
                </Info>
              </InfoWrap>
            ) : (
              <div />
            )}
          </Flex>
          <div style={{ marginRight: '204px', marginTop: '154px' }}>
            <Button size="large" icon="plus" type="primary" css={buttonStyle} onClick={this.props.onShowDrawer}>
              Purchase Order
            </Button>
          </div>
        </HeaderOptions>
      </Header>
    )
  }
}
