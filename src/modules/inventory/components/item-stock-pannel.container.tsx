/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { DatePicker, Drawer, Button, Row, Col, Input, Select } from 'antd'
import moment from 'moment'

import { OrderItem } from '~/schema'
import { Icon } from '~/components'

import { withTheme } from 'emotion-theming'

import { DialogBody, FormLabel, rowCss, DialogFooter, getButtonStyle } from './item-detail-form.style'
import { Theme } from '~/common'

const Option = Select.Option

export interface ItemStockPannelProps {
  item: OrderItem
  showDrawer: boolean
  onCloseDrawer: () => void
  onChangeEditInput: (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => void
  onDateChange: (dateMoment: any, dateString: string) => void
  onSelectStatus: (value: any) => void
  saveItem: () => void
  theme: Theme
}

export class ItemStockPannelComponent extends React.PureComponent<ItemStockPannelProps> {
  state = {}

  render() {
    const item = this.props.item
    const calendarIcon = <Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    return (
      <Drawer
        title={item.wholesaleOrderItemId != null ? item.variety : 'New Purchase Order'}
        placement={'right'}
        closable
        onClose={this.props.onCloseDrawer}
        visible={this.props.showDrawer}
        className="new-item-drawer"
        width={'447'}
      >
        <div>
          <DialogBody>
            <Row css={rowCss} gutter={24}>
              <Col span={24}>
                <FormLabel>DELIVERY DATE</FormLabel>
                <br />
                <DatePicker
                  allowClear={false}
                  defaultValue={moment(item.deliveryDate)}
                  onChange={this.props.onDateChange}
                  placeholder={dateFormat}
                  format={dateFormat}
                  suffixIcon={calendarIcon}
                />
              </Col>
            </Row>
            <Row css={rowCss} gutter={24}>
              <Col span={24}>
                <FormLabel>STATUS</FormLabel>
                <br />
                <Select defaultValue={item.status} style={{ width: 120 }} onChange={this.props.onSelectStatus}>
                  <Option value="RECEIVED">RECEIVED</Option>
                  <Option value="INCOMING">INCOMING</Option>
                  <Option value="PLACED">PLACED</Option>
                  <Option value="CANCELLED">CANCELLED</Option>
                </Select>
              </Col>
            </Row>
            <Row css={rowCss} gutter={24}>
              <Col span={12}>
                <FormLabel>QUANTITY</FormLabel>
                <br />
                <Input
                  value={item.quantity}
                  placeholder="0"
                  style={{ width: '80%' }}
                  onChange={this.props.onChangeEditInput('quantity')}
                  type="number"
                  step="1"
                />
              </Col>
            </Row>
            <Row css={rowCss} gutter={24}>
              <Col span={12}>
                <FormLabel>COST</FormLabel>
                <br />
                <Input
                  value={item.cost}
                  placeholder="0.00"
                  style={{ width: '80%' }}
                  onChange={this.props.onChangeEditInput('cost')}
                  type="number"
                  step="0.05"
                />
              </Col>
            </Row>
            <Row css={rowCss} gutter={24}>
              <Col span={12}>
                <FormLabel>SALE PRICE</FormLabel>
                <br />
                <Input
                  value={item.price}
                  placeholder="0.00"
                  style={{ width: '80%' }}
                  onChange={this.props.onChangeEditInput('price')}
                  type="number"
                  step="0.05"
                />
              </Col>
            </Row>
          </DialogBody>
          <DialogFooter>
            <Button
              icon="check"
              type="primary"
              style={{
                ...getButtonStyle,
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.props.saveItem}
            >
              SAVE
            </Button>
            <Button key="back" onClick={this.props.onCloseDrawer}>
              CANCEL
            </Button>
          </DialogFooter>
        </div>
      </Drawer>
    )
  }
}

export const ItemStockPannel = withTheme(ItemStockPannelComponent)
