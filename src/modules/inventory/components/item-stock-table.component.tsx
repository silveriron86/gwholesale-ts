import React from 'react'
import { Icon, Button } from 'antd'
import { ColumnProps } from 'antd/es/table'
import { format } from 'date-fns'
import { cloneDeep } from 'lodash'
import { OrderItem } from '~/schema'
import { purchaseTableCss,
  Stock,
  polygonCss} from './inventory-table.style'
import { ThemeTable } from '~/modules/customers/customers.style'


export interface ItemStockTableProps {
  orderItems: OrderItem[]
  editItem: (item: OrderItem) => void
}

export class ItemStockTable extends React.PureComponent<ItemStockTableProps> {
  state = {
    initItems: cloneDeep(this.props.orderItems),
    saveItems: this.props.orderItems,
    selectedItemIndex: -1,
  }
  private mutiSortTable = React.createRef<any>()

  constructor(props: ItemStockTableProps) {
    super(props);
  }

    componentWillReceiveProps(nextProps: ItemStockTableProps)
    {
      if (this.state.saveItems != nextProps.orderItems)
        this.setState({
          saveItems: nextProps.orderItems,
          initItems: cloneDeep(nextProps.orderItems),
        })
    }


  render() {
    const { initItems } = this.state
    const listItems: OrderItem[] = cloneDeep(initItems)

    const selectItem = (item: OrderItem, _index: number) => {
      this.props.editItem(item)
    }

    const onSortString = (key: string, a: any, b: any) => {
        const stringA: string = a[key] ? a[key] : ''
        const stringB: string = b[key] ? b[key] : ''
        return stringA.localeCompare(stringB)
      }

    const columns: ColumnProps<OrderItem>[] = [
      {
        title: 'ID',
        dataIndex: 'wholesaleOrderItemId',
        key: 'wholesaleOrderItemId',
        sorter: (a, b) =>
        {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'wholesaleOrderItemId')
          if (sortIndex && sortIndex.sortOrder == 'descend')
            return parseInt(b.wholesaleOrderItemId, 10) - parseInt(a.wholesaleOrderItemId, 10)
          return parseInt(a.wholesaleOrderItemId, 10) - parseInt(b.wholesaleOrderItemId, 10)
        },
      },
      {
        title: 'DATE PLACED',
        dataIndex: 'createdDate',
        key: 'createdDate',
        sorter: (a, b) =>
        {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'createdDate')
          if (sortIndex && sortIndex.sortOrder == 'descend')
            return b.createdDate - a.createdDate
          return a.createdDate - b.createdDate
        },
        render: (createdDate) => format(createdDate, 'MM/D/YY'),
      },
      {
        title: 'DATE ARRIVING',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        sorter: (a, b) =>
        {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'deliveryDate')
          if (sortIndex && sortIndex.sortOrder == 'descend')
            return b.deliveryDate - a.deliveryDate
          return a.deliveryDate - b.deliveryDate
        },
        render: (deliveryDate) => format(deliveryDate, 'MM/D/YY'),
      },
      {
        title: 'QUANTITY',
        dataIndex: 'quantity',
        key: 'quantity',
        sorter: (a, b) =>
        {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'quantity')
          if (sortIndex && sortIndex.sortOrder == 'descend')
            return b.quantity - a.quantity
          return a.quantity - b.quantity
        },
        render(value, orderItem) {
          let text = value
          if (orderItem.status === 'PLACED')
            text = -value
          return (
            <Stock stock={Number(text)}>
              {text}
              <Icon type="polygon" css={polygonCss} />
            </Stock>
          )
        },
      },
      {
        title: 'UNIT COST',
        dataIndex: 'cost',
        key: 'cost',
        sorter: (a, b) =>
        {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'cost')
          if (sortIndex && sortIndex.sortOrder == 'descend')
            return b.cost - a.cost
          return a.cost - b.cost
        },
        render(text) {
          return <span>{text > 0 ? (`$${text}`) : ''}</span>
        },
      },
      {
        title: 'UNIT PRICE SOLD',
        dataIndex: 'price',
        key: 'price',
        sorter: (a, b) =>
        {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'price')
          if (sortIndex && sortIndex.sortOrder == 'descend')
            return b.price - a.price
          return a.price - b.price
        },
        render(text) {
          return <span>{text > 0 ? (`$${text}`) : ''}</span>
        },
      },
      {
        title: 'STATUS',
        dataIndex: 'status',
        key: 'status',
        sorter: (a, b) =>
        {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'status')
          if (sortIndex && sortIndex.sortOrder == 'descend')
            return onSortString('status', b, a)
          return onSortString('status', a, b)
        },
      },
      {
        width: 110,
        className: 'buttonCell',
        render(_item, orderItem, index) {
          return (
            <Button className="visible" type="default" onClick={() => {
              selectItem(orderItem, index)
            }}>
              <Icon type="form" />
            </Button>
          )
        },
      },
    ]

    return (
      <ThemeTable
        ref={this.mutiSortTable}
        pagination={{ pageSize: 12 }}
        columns={columns}
        dataSource={listItems}
        rowKey="wholesaleOrderItemId"
        css={purchaseTableCss(false)}
      />
    )
  }
}
