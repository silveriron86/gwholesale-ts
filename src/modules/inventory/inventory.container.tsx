import { Input, Checkbox, Row, Col, Icon } from 'antd'
import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps, RouteProps } from 'react-router'
import { cloneDeep, conformsTo } from 'lodash'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { InventoryDispatchProps, InventoryStateProps, InventoryModule, InventoryLoading } from './inventory.module'
import { Flex, FilterContainer, FormLabel, ShowMoreButton, ResetButton } from './inventory.style'

import OrderUploadCsv from '../orders/components/oderUploadCsv/order-upload-csv'

import 'rc-drawer/assets/index.css'
import { SaleItem, SaleCategory, PriceSheetItem } from '~/schema'
import { InventoryHeader, InventoryTable, InventoryProductForm } from './components'

import { forestGreen, brightGreen, Theme, CACHED_NS_LINKED } from '~/common'
import GrubDrawer from '~/components/GrubDrawer'
import { MessageType } from '~/components'
import PageLayout from '~/components/PageLayout'
import { LoadingOverlay, ThemeSpin } from '../customers/customers.style'
import { getAllowedEndpoint } from '~/common/utils'
import AdjustInventoryModal from './components/adjust-inventory-modal'
import ReviewTransaction from './components/countInventory/review-transaction'
import CountInventoryModal from './components/countInventory/count-inventory-modal'

export type InventoryProps = InventoryDispatchProps &
  InventoryStateProps &
  RouteComponentProps &
  RouteProps & { theme: Theme }

export interface InventoryState {
  showDrawer: boolean
  showPriceSheets: boolean
  showUploadCSV: boolean
  item: SaleItem
  showFilter: boolean
  saleCategories: SaleCategory[]
  search: string
  filterCategory: any[]
  filterOther: any
  filterCostFrom: number
  filterCostTo: number
  moreCategory: boolean
  moreProvider: boolean
  morePacking: boolean
  moreWeight: boolean
  moreSize: boolean
  moreGrade: boolean
  message: string
  type: MessageType | null
  page: number
}

export class InventoryComponent extends React.PureComponent<InventoryProps, InventoryState> {
  state: InventoryState
  constructor(props: InventoryProps) {
    super(props)
    const sessionSearch = localStorage.getItem('PRODUCTS_SEARCH')
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search)
    const search = urlParams.get('search')
    const costFrom = urlParams.get('costfrom')
    const costTo = urlParams.get('costto')
    const sizes = urlParams.get('size')
    const providers = urlParams.get('provider')
    const grades = urlParams.get('grade')
    const weights = urlParams.get('weight')
    const packings = urlParams.get('packing')
    const page = urlParams.get('page')

    const size = sizes ? sizes.split(',') : []
    const provider = providers ? providers.split(',') : []
    const grade = grades ? grades.split(',') : []
    const weight = weights ? weights.split(',') : []
    const packing = packings ? packings.split(',') : []
    const filterCostFrom = costFrom !== null ? parseFloat(costFrom) : 0.0
    const filterCostTo = costTo !== null ? parseFloat(costTo) : 0.0

    this.state = {
      showDrawer: false,
      showFilter: false,
      item: {} as SaleItem,
      saleCategories: [],
      search: search ? search : '',
      showPriceSheets: false,
      showUploadCSV: false,
      filterCategory: [],
      filterOther: {
        size,
        provider,
        grade,
        weight,
        packing,
      },
      filterCostFrom,
      filterCostTo,
      moreCategory: false,
      moreProvider: false,
      morePacking: false,
      moreWeight: false,
      moreSize: false,
      moreGrade: false,
      message: '',
      type: null,
      visible: false,
      adjustInventoryVisible: false,
      countInventoryVisible: false,
      reviewTransactionVisible: false,
      active: 1,
      page: page ? parseInt(page, 10) : 0,
      uploadMode: '',
    }
  }
  private tableCmp = React.createRef<InventoryTable>()

  componentDidMount() {
    const sessionSearch = localStorage.getItem('PRODUCTS_SEARCH')
    const hash = window.location.hash
    if(hash.indexOf('#/inventory#/product') != -1) {
      window.location.href =
      `#/product/` +hash.replace('#/inventory#/product/','')
    }
    if (sessionSearch) {
      this.updateURL()
    }

    if (this.props.loadingProducts) {
      this.props.getSaleCategories()
      this.props.getAllPriceSheets()
      this.props.getAllCompanyTypes()
      this.props.getNetsuitUoms()
    }
    if (!this.props.userSetting) {
      this.props.getUserSetting()
    }
    // Units on Hand values in Products List need to be updated
    this.props.getItemListByCondition({ active: this.state.active })
  }

  componentWillReceiveProps(nextProps: InventoryProps) {
    if (nextProps.message !== this.state.message) {
      this.setState({
        message: nextProps.message,
        type: nextProps.type,
      })
      if (nextProps.type != null) {
        // notification[nextProps.type!]({
        //   message: nextProps.type!.toLocaleUpperCase(),
        //   description: nextProps.message,
        //   onClose: this.onCloseNotification,
        // })

        if (!this.state.item || !this.state.item.itemId) {
          // new item
          if (nextProps.message != 'Item deleted') {
            this.props.history.push(
              `/product/${nextProps.newItemId}/${getAllowedEndpoint(
                'product',
                this.props.currentUser.accountType,
                'specifications',
              )}`,
            )
            this.props.endAdding()
            this.onCloseNotification()
          }
        }
      }
    }

    if (
      nextProps.saleCategories.length > 0 &&
      JSON.stringify(nextProps.saleCategories) !== JSON.stringify(this.props.saleCategories)
    ) {
      const urlParams = new URLSearchParams(this.props.location.search)
      const category = urlParams.get('category')
      if (category) {
        let filterCategory: any[] = []
        const categories = category.split(',')
        categories.forEach((catName, i) => {
          const found = nextProps.saleCategories.find((c) => c.name.toLowerCase() === catName.toLowerCase())
          if (found) {
            filterCategory.push(found.wholesaleCategoryId)
          }
        })
        if (filterCategory.length > 0) {
          this.onChangeCategory(filterCategory)
        }
      }
    }
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  editItem = (saleItem: SaleItem) => {
    this.props.getPriceSheetsForItem(saleItem.itemId)
    const copyItem = cloneDeep(saleItem)
    this.setState({
      item: copyItem,
      showDrawer: true,
    })
    this.tableCmp.current!.onExpendedRowsChange([])
  }

  onSearch = (value: string, isSelected?: boolean, searchType?: string) => {
    if (searchType === 'lots') {
      if (value === '') {
        this.props.getItemListByCondition({ active: this.state.active })
      } else {
        this.props.getItemsByLotId(value)
      }
    } else if (searchType === 'products') {
      this.props.getItemListByCondition({ active: this.state.active })
    }

    this.setState(
      {
        search: value.indexOf('itemId-') == -1 ? value : '',
        page: 0,
      },
      () => {
        this.updateURL()
      },
    )

    if (value.indexOf('itemId-') == 0) {
      const itemId = value.substr(7)
      const selectedItem = this.props.saleItems.find((el) => el.itemId == itemId)
      if (selectedItem) {
        this.props.history.push(
          `/product/${selectedItem.itemId}/${getAllowedEndpoint(
            'product',
            this.props.currentUser.accountType,
            'specifications',
          )}`,
        )
      }
    }
  }

  updateURL = () => {
    var url = new URL(window.location.href)
    var queryParams = url.searchParams
    const { search, filterCategory, filterCostFrom, filterCostTo, filterOther, page } = this.state
    if (search) {
      queryParams.set('search', search)
    }
    if (page) {
      queryParams.set('page', page)
    }
    if (filterCategory) {
      const { saleCategories } = this.props
      let names: string[] = []
      filterCategory.forEach((catId, _i) => {
        const found = saleCategories.find((c) => c.wholesaleCategoryId === catId)
        if (found) {
          names.push(found.name)
        }
      })
      queryParams.set('category', names.join(','))
    }

    if (filterCostFrom !== 0) {
      queryParams.set('costfrom', filterCostFrom.toString())
    }

    if (filterCostTo !== 0) {
      queryParams.set('costto', filterCostTo.toString())
    }

    const { size, provider, grade, weight, packing } = filterOther
    if (size.length > 0) {
      queryParams.set('size', size.join(','))
    }
    if (provider.length > 0) {
      queryParams.set('provider', provider.join(','))
    }
    if (grade.length > 0) {
      queryParams.set('grade', grade.join(','))
    }
    if (weight.length > 0) {
      queryParams.set('weight', weight.join(','))
    }
    if (packing.length > 0) {
      queryParams.set('packing', packing.join(','))
    }
    url.search = queryParams.toString()
    localStorage.setItem('PRODUCTS_SEARCH', url.search)
    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString())
  }

  onChangePage = (page, pageSize) => {
    this.setState(
      {
        page: page - 1,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onShowDrawer = () => {
    const newItem = {} as SaleItem
    if (this.props.saleCategories.length > 0)
      newItem.wholesaleCategoryId = this.props.saleCategories[0].wholesaleCategoryId
    this.setState({
      item: newItem,
      showDrawer: true,
    })
  }

  onCloseDrawer = () => {
    this.setState({
      item: {} as SaleItem,
      showDrawer: false,
      showPriceSheets: false,
    })
  }

  onShowFilter = () => {
    this.setState({
      showFilter: true,
    })
  }

  onCloseFilter = () => {
    this.setState({
      showFilter: false,
    })
  }

  onChangeCategory = (checkedValues: any[]) => {
    console.log('onchangecategory = ', checkedValues)
    this.setState(
      {
        filterCategory: checkedValues,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onChangeCostFrom = () => (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.valueAsNumber
    this.setState(
      {
        filterCostFrom: value,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onChangeCostTo = () => (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.valueAsNumber
    this.setState(
      {
        filterCostTo: value,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onChangeFilterOther = (checkedValues: any[], field: string) => {
    const newFilter = {
      ...this.state.filterOther,
      [field]: checkedValues,
    }
    this.setState(
      {
        filterOther: newFilter,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onChangeProviderFilter = (checkedValues: any[]) => {
    this.onChangeFilterOther(checkedValues, 'provider')
  }

  onChangePackingFilter = (checkedValues: any[]) => {
    this.onChangeFilterOther(checkedValues, 'packing')
  }

  onChangeSizeFilter = (checkedValues: any[]) => {
    this.onChangeFilterOther(checkedValues, 'size')
  }

  onChangeGradeFilter = (checkedValues: any[]) => {
    this.onChangeFilterOther(checkedValues, 'grade')
  }

  onChangeWeightFilter = (checkedValues: any[]) => {
    this.onChangeFilterOther(checkedValues, 'weight')
  }

  onShowPriceSheets = () => {
    this.setState({
      showPriceSheets: true,
    })
  }

  onChangeEditInput = (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = null
    if (channel === 'cost') value = e.target.valueAsNumber
    else value = e.target.value

    this.setState({
      item: {
        ...this.state.item,
        [channel]: value,
      },
    })
  }

  onChangeEditCategory = (value: string[]) => {
    const category = this.props.saleCategories.find(
      (category: SaleCategory) => category.wholesaleCategoryId === value[1],
    )

    if (value.length > 0) {
      this.setState({
        item: {
          ...this.state.item,
          wholesaleCategoryId: value[1],
          category: category!.name,
          wholesaleCategory: category!,
        },
      })
    }
  }

  onChangeEditSwitch = (key: string) => (value: boolean) => {
    this.setState({
      item: {
        ...this.state.item,
        [key]: value,
      },
    })
  }

  onSavePriceSheet = (priceSheetItem: PriceSheetItem) => () => {
    this.props.loadIndex(priceSheetItem.priceSheetItemId)
    this.props.savePriceSheetItem(priceSheetItem)
  }

  onShowMore = (key: string) => () => {
    // @ts-ignore
    this.setState({
      ['more' + key]: true,
    })
  }

  onResetCategory = () => {
    this.setState(
      {
        filterCategory: [],
        moreCategory: false,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onResetCost = () => {
    this.setState(
      {
        filterCostFrom: 0.0,
        filterCostTo: 0.0,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onResetFilter = (key: string) => () => {
    const newFilter = {
      ...this.state.filterOther,
      [key]: [],
    }
    // @ts-ignore
    this.setState(
      {
        filterOther: newFilter,
        ['more' + key[0].toUpperCase() + key.slice(1)]: false,
      },
      () => {
        this.updateURL()
      },
    )
  }

  onAddToPriceSheet = (itemId: string, priceSheetId: string) => () => {
    const params = { wholesaleItemId: itemId, priceSheetId: priceSheetId }
    this.props.loadIndex(InventoryLoading.INVENTORY_ADD_PS)
    this.props.assignPriceSheetItem(params)
  }

  onRemoveFromPriceSheet = (itemId: string, priceSheetItemId: string) => () => {
    const params = { wholesaleItemId: itemId, priceSheetItemId: priceSheetItemId }
    this.props.loadIndex(InventoryLoading.INVENTORY_ADD_PS)
    this.props.unassignPriceSheetItem(params)
  }

  onDeleteItem = (itemId: string) => {
    this.props.loadIndex(itemId)
    this.props.deleteItem(itemId)
  }

  onChangeActiveStatus = (itemId: string, value: boolean) => {
    this.props.loadIndex(itemId)
    if(value === true) {
      this.props.activeItem(itemId)
    }else if(value === false) {
      this.props.inactiveItem(itemId)
    }
  }

  onSubmit = () => {
    this.setState({
      visible: true,
    })
    // let payload = {
    //   variety: '',
    //   sell: true,
    //   purchase: true,
    //   active: true,
    // }
    // this.props.loadIndex(InventoryLoading.INVENTORY_ITEM_SAVE)
    // if (this.state.item && this.state.item.itemId) payload.id = this.state.item.itemId
    // this.props.saveItem(payload)
  }

  onShowAdjustInventory = () => {
    this.setState({
      adjustInventoryVisible: true,
    })
  }

  onHideAdjustInventory = () => {
    this.setState({
      adjustInventoryVisible: false,
    })
  }

  onPostAdjustments = () => {
    this.setState({
      adjustInventoryVisible: false,
    })
  }

  // enter inventory handle
  onShowEnterInventory = () => {
    this.setState({
      countInventoryVisible: true,
    })
  }

  onHideEnterInventory = () => {
    this.setState({
      countInventoryVisible: false,
    })
  }

  onShowReviewTransaction = () => {
    this.setState({ reviewTransactionVisible: true })
  }

  onHideReviewTransaction = () => {
    this.setState({ reviewTransactionVisible: false })
  }

  onShowUploadCSV = (mode: string) => {
    this.setState({
      uploadMode: mode,
    })
    this.setState({
      showUploadCSV: true,
    })
  }

  onCloseUploadCSV = () => {
    this.setState({
      showUploadCSV: false,
    })
  }

  renderTitle() {
    return (
      <div>
        <Icon style={{ marginRight: '5px', color: forestGreen }} type="menu-fold" />
        Filters
      </div>
    )
  }

  onSelect = () => {}

  renderFilterOptions(key: string) {
    const { saleItems } = this.props
    const filterOptions = [] as string[]
    let index = 0
    for (const item of saleItems) {
      if (index > 20 && this.state['more' + key[0].toUpperCase() + key.slice(1)] === false) break
      if (item[key] != null && item[key].length > 0 && filterOptions.indexOf(item[key]) === -1) {
        filterOptions.push(item[key])
        index++
      }
    }

    return filterOptions.map((filterName, index) => (
      <Col span={12} key={filterName + index}>
        <Checkbox value={filterName}>{filterName}</Checkbox>
      </Col>
    ))
  }

  handleOk = () => {
    const { form } = this.formRef.props
    form.validateFields((err, values) => {
      if (err) {
        return
      }
      values = {
        ...values,
        sell: true,
        purchase: true,
        active: true,
        isOrganic: false,
      }
      if (!values.sku || (values.sku && values.sku.trim() == '')) {
        values.sku = values.variety
      }
      if (values.defaultUOM) {
        // values.inventoryUOM = values.defaultUOM
        values.defaultSalesUnitUOM = values.defaultUOM
        values.defaultPurchaseUnitUOM = values.defaultUOM
        values.defaultPurchasingCostUOM = values.defaultUOM
        values.defaultSellingPricingUOM = values.defaultUOM
        delete values.defaultUOM
      }
      // if (values.ratioUOM === 'N/A' && values.constantRatio === false) {
      values.ratioUOM = 1
      // }
      console.log(values)
      this.props.startAdding()
      this.props.loadIndex(InventoryLoading.INVENTORY_ITEM_SAVE)
      this.props.saveItem(values)
      form.resetFields()
      this.setState({
        visible: false,
      })
    })
  }

  handleCancel = () => {
    this.setState({
      visible: false,
    })
  }

  saveFormRef = (formRef: any) => {
    this.formRef = formRef
  }

  changeShow = (e: any) => {
    this.setState({ active: e.target.checked ? 0 : 1 })
    this.props.setActiveValue({ isActive: e.target.checked ? 0 : 1 })
    this.props.getItemListByCondition({ active: e.target.checked ? 0 : 1 })
  }

  componentWillUnmount = () => {
    this.props.setActiveValue({ isActive: 1 })
  }

  render() {
    const { saleCategories = [], companyProductTypes, loadingProducts, addingProduct, isDownloading } = this.props
    const {
      visible,
      search,
      filterCategory,
      filterCostFrom,
      filterCostTo,
      filterOther,
      adjustInventoryVisible,
      countInventoryVisible,
      reviewTransactionVisible
    } = this.state

    let categoryList = []
    if (!this.state.moreCategory) {
      categoryList = saleCategories.slice(0, 20).map((category) => (
        <Col key={category.wholesaleCategoryId} span={12}>
          <Checkbox value={category.wholesaleCategoryId}>{category.name}</Checkbox>
        </Col>
      ))
    } else {
      categoryList = saleCategories.map((category) => (
        <Col key={category.wholesaleCategoryId} span={12}>
          <Checkbox value={category.wholesaleCategoryId}>{category.name}</Checkbox>
        </Col>
      ))
    }

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Products'}>
        {companyProductTypes !== null && (
          <InventoryProductForm
            wrappedComponentRef={this.saveFormRef}
            companyProductTypes={companyProductTypes}
            handleOk={this.handleOk}
            handleCancel={this.handleCancel}
            visible={visible}
            getAllCompanyTypes={this.props.getAllCompanyTypes}
            getNetSuitUoms={this.props.getNetsuitUoms}
            getUserSetting={this.props.getUserSetting}
            userSetting={this.props.userSetting}
            netsuitUoms={this.props.netsuitUoms}
          />
        )}
        <InventoryHeader
          search={search}
          rawData={this.props.saleItems}
          downloadData={this.props.downloadData}
          downloadLotData={this.props.downloadLotData}
          isDownloading={this.props.isDownloading}
          waiting={this.props.waiting}
          onShowDrawer={this.onSubmit}
          onShowAdjustInventory={this.onShowAdjustInventory}
          onShowReviewTransaction={this.onShowReviewTransaction}
          onShowUploadCSV={this.onShowUploadCSV}
          onSearch={this.onSearch}
          uploadBertiExcel={this.props.uploadBertiExcel}
          theme={this.props.theme}
          changeShow={this.changeShow}
          active={this.state.active}
          getItemListByCondition={this.props.getItemListByCondition}
          getDownloadLotItemList={this.props.getDownloadLotItemList}
          resetDownloadData={this.props.resetDownloadData}
          userSetting={this.props.userSetting}
          getItemLocations={this.props.getItemLocations}
          itemLocations={this.props.itemLocations}
          companyProductTypes={this.props.companyProductTypes}
        />
        <AdjustInventoryModal
          {...this.props}
          visible={adjustInventoryVisible}
          handleOk={this.onPostAdjustments}
          handleCancel={this.onHideAdjustInventory}
        />
        <OrderUploadCsv visible={this.state.showUploadCSV} propData={this.props} onCancel={this.onCloseUploadCSV} uploadMode={this.state.uploadMode} />
        <CountInventoryModal
          {...this.props}
          visible={countInventoryVisible}
          onCancel={this.onHideEnterInventory}
          propData={this.props}
        />
        <ReviewTransaction visible={reviewTransactionVisible} onCancel={this.onHideReviewTransaction} onShowEnterInventory={this.onShowEnterInventory} />
        <ThemeSpin spinning={loadingProducts || isDownloading} tip={isDownloading ? 'Downloading...' : null}>
          <InventoryTable
            ref={this.tableCmp}
            saleItems={this.props.saleItems}
            editItem={this.editItem}
            search={this.state.search}
            showDrawer={this.state.showDrawer}
            onDeleteItem={this.onDeleteItem}
            updateItem={this.props.updateItemPrice}
            onChangeActiveStatus={this.onChangeActiveStatus}
            currentUser={this.props.currentUser}
            loading={this.props.loading.length == 0}
            options={{
              filterCategory,
              filterCostFrom,
              filterCostTo,
              filterOther,
            }}
            page={this.state.page}
            onChangePage={this.onChangePage}
          />
        </ThemeSpin>

        <GrubDrawer selected={''} onSelect={this.onSelect} placement={'right'} subDrawer={true} width={271}>
          <React.Fragment>
            <FilterContainer>
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>Categories</h3>
                <ResetButton onClick={this.onResetCategory}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Flex>
                <Checkbox.Group
                  style={{ width: '100%' }}
                  onChange={this.onChangeCategory}
                  value={this.state.filterCategory}
                >
                  <Row>{categoryList}</Row>
                </Checkbox.Group>
                <ShowMoreButton hidden={this.state.moreCategory} onClick={this.onShowMore('Category')}>
                  Show more...
                </ShowMoreButton>
              </Flex>
            </FilterContainer>
            <FilterContainer style={{ marginTop: '20px' }}>
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>Cost</h3>
                <ResetButton onClick={this.onResetCost}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Row gutter={24} className="highlight-input">
                <Col span={12}>
                  <FormLabel>From</FormLabel>
                  <br />
                  <Input
                    placeholder="0.00"
                    style={{ width: '80%' }}
                    type="number"
                    value={this.state.filterCostFrom}
                    onChange={this.onChangeCostFrom()}
                  />
                </Col>
                <Col span={12}>
                  <FormLabel>To</FormLabel>
                  <br />
                  <Input
                    placeholder="0.00"
                    style={{ width: '80%' }}
                    type="number"
                    value={this.state.filterCostTo}
                    onChange={this.onChangeCostTo()}
                  />
                </Col>
              </Row>
            </FilterContainer>
            <FilterContainer style={{ marginTop: '20px' }}>
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>Provider</h3>
                <ResetButton onClick={this.onResetFilter('provider')}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Flex>
                <Checkbox.Group
                  style={{ width: '100%' }}
                  onChange={this.onChangeProviderFilter}
                  value={this.state.filterOther.provider}
                >
                  <Row>{this.renderFilterOptions('provider')}</Row>
                </Checkbox.Group>
                <ShowMoreButton hidden={this.state.moreProvider} onClick={this.onShowMore('Provider')}>
                  Show more...
                </ShowMoreButton>
              </Flex>
            </FilterContainer>
            <FilterContainer style={{ marginTop: '20px' }}>
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>Packing Type</h3>
                <ResetButton onClick={this.onResetFilter('packing')}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Flex>
                <Checkbox.Group
                  style={{ width: '100%' }}
                  onChange={this.onChangePackingFilter}
                  value={this.state.filterOther.packing}
                >
                  <Row>{this.renderFilterOptions('packing')}</Row>
                </Checkbox.Group>
                <ShowMoreButton hidden={this.state.morePacking} onClick={this.onShowMore('Packing')}>
                  Show more...
                </ShowMoreButton>
              </Flex>
            </FilterContainer>
            <FilterContainer style={{ marginTop: '20px' }}>
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>Size</h3>
                <ResetButton onClick={this.onResetFilter('size')}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Flex>
                <Checkbox.Group
                  style={{ width: '100%' }}
                  onChange={this.onChangeSizeFilter}
                  value={this.state.filterOther.size}
                >
                  <Row>{this.renderFilterOptions('size')}</Row>
                </Checkbox.Group>
                <ShowMoreButton hidden={this.state.moreSize} onClick={this.onShowMore('Size')}>
                  Show more...
                </ShowMoreButton>
              </Flex>
            </FilterContainer>
            <FilterContainer style={{ marginTop: '20px' }}>
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>Grade</h3>
                <ResetButton onClick={this.onResetFilter('grade')}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Flex>
                <Checkbox.Group
                  style={{ width: '100%' }}
                  onChange={this.onChangeGradeFilter}
                  value={this.state.filterOther.grade}
                >
                  <Row>{this.renderFilterOptions('grade')}</Row>
                </Checkbox.Group>
                <ShowMoreButton hidden={this.state.moreGrade} onClick={this.onShowMore('Grade')}>
                  Show more...
                </ShowMoreButton>
              </Flex>
            </FilterContainer>
            <FilterContainer style={{ marginTop: '20px' }}>
              <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>Weight</h3>
                <ResetButton onClick={this.onResetFilter('weight')}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Flex>
                <Checkbox.Group
                  style={{ width: '100%' }}
                  onChange={this.onChangeWeightFilter}
                  value={this.state.filterOther.weight}
                >
                  <Row>{this.renderFilterOptions('weight')}</Row>
                </Checkbox.Group>
                <ShowMoreButton hidden={this.state.moreWeight} onClick={this.onShowMore('Weight')}>
                  Show more...
                </ShowMoreButton>
              </Flex>
            </FilterContainer>
          </React.Fragment>
        </GrubDrawer>

        {addingProduct && (
          <LoadingOverlay>
            <ThemeSpin tip="Loading..." spinning={true} size="large" />
          </LoadingOverlay>
        )}
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return {
    currentUser: state.currentUser,
    ...state.inventory,
  }
}

export const Inventory = withTheme(connect(InventoryModule)(mapStateToProps)(InventoryComponent))
