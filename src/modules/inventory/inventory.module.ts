import {
  Module,
  EffectModule,
  ModuleDispatchProps,
  Effect,
  StateObservable,
  Reducer,
  DefineAction,
} from 'redux-epics-decorator'
import { Observable, of, from } from 'rxjs'
import { map, switchMap, takeUntil, catchError, endWith, mergeMap, startWith } from 'rxjs/operators'
import { push, goBack } from 'connected-react-router'
import { Action } from 'redux-actions'
import { sortBy } from 'lodash'

import { SaleItem, PriceSheet, SaleCategory, PriceSheetItem, OrderItem } from '~/schema'
import { GlobalState } from '~/store/reducer'
import { InventoryService } from './inventory.service'
import { MessageType } from '~/components'
import { checkError, checkMessage, responseHandler, handlerNoLotNumber } from '~/common/utils'
import { SettingService } from '../setting/setting.service'
import { OrderService } from '../orders/order.service'
import { CACHED_COMPANY } from '~/common'
import _ from 'lodash'

export interface InventoryStateProps {
  saleItems: SaleItem[]
  item: SaleItem
  priceSheets: PriceSheet[]
  priceSheetItems: PriceSheetItem[]
  saleCategories: SaleCategory[]
  editedItemId: string
  newItemId: number
  message: string
  description: string
  type: MessageType | null
  relatedOrders: {}
  orderItems: OrderItem[]
  loading: string
  waiting: boolean
  isActive: number
  vendors: any[]
  loadingProducts: boolean
  userSetting: any
  netsuitUoms: any[]
  downloadData: any[]
  downloadLotData: any[]
  isDownloading: boolean
  inPostingAdjustments: boolean
  itemLocations: any[]
}

export enum InventoryLoading {
  INVENTORY_ITEM_SAVE = 'inventory_item_save',
  INVENTORY_PS_ITEM_SAVE = 'inventory_ps_item_save',
  INVENTORY_ADD_PS = 'inventory_add_ps',
}

export const getProductItems = (state: InventoryStateProps, action: Action<SaleItem[]>, isLot: boolean) => {
  const items = action.payload
    .sort((a: any, b: any) => {
      return _.get(a, 'variety', '') || ''.localeCompare(_.get(b, 'variety', ''))
    })
    .map((item: any) => {
      //Calculate the data that needs to be displayed. It can be used directly when downloading
      let onHandQtyForDownLoad = handlerNoLotNumber(item, 2)
      let wholesaleProductUomList = []
      // for performance,  backend only send defaultPurchaseCostUOM and defaultSellsPriceUOM to frontend
      let wholesaleCategory = null
      if (item.pricingUOM) {
        wholesaleProductUomList.push({
          name: item.pricingUOM,
          ratio: item.pricingUOMRatio,
          priceFactor: item.pricingUOMFactor,
        })
      }
      if (item.costUOM) {
        wholesaleProductUomList.push({
          name: item.costUOM,
          ratio: item.costUOMRatio,
          priceFactor: item.costUOMFactor,
        })
      }
      if (item.category) {
        wholesaleCategory = { name: item.category, wholesaleCategoryId: item.categoryId }
      }
      return {
        isLot,
        ...item,
        onHandQtyForDownLoad,
        wholesaleProductUomList,
        wholesaleCategory,
      }
    })
  return {
    ...state,
    saleItems: items,
    loading: '',
    loadingProducts: false,
    isDownloading: false,
    downloadData: state.isDownloading ? items : [], // when api is called by download button, this value is initialized
  }
}

@Module('inventory')
export class InventoryModule extends EffectModule<InventoryStateProps> {
  defaultState = {
    saleItems: [],
    item: {} as SaleItem,
    priceSheets: [],
    saleCategories: [],
    priceSheetItems: [],
    newItemId: -1,
    addedItemId: '',
    message: '',
    description: '',
    type: null,
    relatedOrders: {},
    orderItems: [],
    loading: '',
    waiting: false,
    companyProductTypes: null,
    isActive: 1,
    vendors: [],
    loadingProducts: true,
    userSetting: null,
    netsuitUoms: [],
    downloadData: [],
    downloadLotData: [],
    isDownloading: false,
    inPostingAdjustments: false,
    itemLocations: []
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(
    private readonly inventory: InventoryService,
    private readonly setting: SettingService,
    private readonly order: OrderService,
  ) {
    super()
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect()
  goto(action$: Observable<string>) {
    return action$.pipe(map((path) => push(path)))
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, message: '', description: '', type: null, waiting: true, loadingProducts: true }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, inPostingAdjustments: true }
    },
  })
  startPostAdjustment(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<boolean>) => {
      return {
        ...state,
        downloadData: [],
        downloadLotData: [],
        isDownloading: action.payload ? true : false
      }
    },
  })
  resetDownloadData(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, addingProduct: true }
    },
  })
  startAdding(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, addingProduct: false }
    },
  })
  endAdding(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        saleItems: action.payload,
        loading: '',
        loadingProducts: false,
      }
    },
  })
  getSaleItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.inventory.getAllItems(
          state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
          data.type,
          data.active,
        ),
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    before: (state:any, action:any) => {
      return {
        ...state,
        loadingProducts: true,
      }
    },
    done: (state: InventoryStateProps, action: Action<SaleItem[]>) => {
      return getProductItems(state, action, false);
    },
  })
  getItemListByCondition(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.inventory.getItemListByCondition(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET', data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error)))
        )
      )
    )
  }

  @Effect({
    before: (state:any, action:any) => {
      return {
        ...state,
        loadingProducts: true,
      }
    },
    done: (state: InventoryStateProps, action: Action<SaleItem[]>) => {
      return getProductItems(state, action, true);
    },
  })
  getItemsByLotId(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((search: string) =>
        this.inventory.getItemsByLotId(search).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error)))
        )
      )
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any[]>) => {
      return {
        ...state,
        loading: '',
        loadingProducts: false,
        isDownloading: false,
        downloadLotData: state.isDownloading ? action.payload : [],
      }
    },
  })

  getDownloadLotItemList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getDownloadLotItemList(),
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      console.log(payload)
      return { ...state, isActive: payload.isActive }
    },
  })
  setActiveValue(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: any, { payload }: Action<any[]>) => {
      return {
        ...state,
        // items: payload,
        // message: 'Update Items Successful',
        // type: MessageType.SUCCESS
      }
    },
  })
  updateItemPrice(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.inventory.updateItemPrice(data).pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getItemListByCondition)({ active: state$.value.inventory.isActive })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<SaleCategory[]>) => {
      return {
        ...state,
        saleCategories: action.payload,
      }
    },
  })
  getSaleCategories(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory
          .getAllCategories(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET')
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<PriceSheet[]>) => {
      return {
        ...state,
        priceSheets: action.payload,
      }
    },
  })
  getAllPriceSheets(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    const processData = (data: PriceSheet[]) => {
      const sortedData = sortBy(data, 'priceSheetId')
      return sortedData
    }

    return action$.pipe(
      switchMap(() =>
        this.inventory.getAllPriceSheet(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          map(processData),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      return {
        ...state,
        orderItems: action.payload.orders,
        item: action.payload.item,
      }
    },
  })
  getItemPurchaseOrder(action$: Observable<string>) {
    return action$.pipe(
      switchMap((itemId) => this.inventory.getItemPurchaseOrder(itemId)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, message: 'Update Item Stock Successful', type: MessageType.SUCCESS }
    },
  })
  requestPurchaseOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.inventory
          .requestPurchaseOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '0')
          .pipe(
            switchMap((data) => of(checkMessage(data, 'Successfully create an order'))),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<PriceSheetItem[]>) => {
      return {
        ...state,
        priceSheetItems: action.payload,
      }
    },
  })
  getPriceSheetsForItem(action$: Observable<string>) {
    return action$.pipe(
      switchMap((itemId) => this.inventory.getPriceSheetsForItem(itemId)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, message: 'Price Sheet Item saved', type: MessageType.SUCCESS, loading: '' }
    },
  })
  savePriceSheetItem(action$: Observable<PriceSheetItem>) {
    return action$.pipe(
      switchMap((data: PriceSheetItem) =>
        this.inventory.savePriceSheetItem(data).pipe(
          switchMap((data) => of(checkMessage(data, 'Successfully updated PriceSheetItem'))),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      console.log(action.payload)
      let ret = {
        ...state,
        message: 'Item saved',
        type: MessageType.SUCCESS,
        loading: '',
        waiting: false,
        addingProduct: false,
      }
      if (action.payload && typeof action.payload.wholesaleItemId !== 'undefined') {
        ret.newItemId = action.payload.wholesaleItemId
      }
      return ret
    },
    error_message: (state: InventoryStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loading: '',
        waiting: false,
      }
    },
  })
  saveItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.inventory.saveItem(data).pipe(
          // switchMap((data) => of(checkMessage(data, 'Successfully update an item', 'Successfully created a new item'))),
          switchMap((data) => of(responseHandler(data, true))),
          map((response) => {
            if (response && response.statusCodeValue != 200)
              return {
                type: 'error_message',
                payload: response.body.message,
                error: true,
              }
            return {
              type: 'done',
              payload: response.body.data,
            }
          }),
          endWith(this.createActionFrom(this.getSaleItems)({ active: state$.value.inventory.isActive })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      console.log(action.payload)
      let ret = {
        ...state,
        loading: '',
        waiting: false,
      }
      return ret
    },
    error_message: (state: InventoryStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loading: '',
        waiting: false,
      }
    },
  })
  createItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((arg: any) =>
        this.inventory.createItem(arg.orderId, arg.data).pipe(
          // switchMap((data) => of(checkMessage(data, 'Successfully update an item', 'Successfully created a new item'))),
          switchMap((data) => of(responseHandler(data, true))),
          map((response) => {
            if (response && response.statusCodeValue != 200)
              return {
                type: 'error_message',
                payload: response.body.message,
                error: true,
              }
            return {
              type: 'done',
              payload: response.body.data,
            }
          }),
          endWith(this.createActionFrom(this.getSaleItems)({ active: state$.value.inventory.isActive })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, loading: '' }
    },
  })
  deleteItem(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: string) =>
        this.inventory.deleteItem(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getSaleItems)({ active: state$.value.inventory.isActive })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state:any, action:any) => {
      return {
        ...state,
        saleItems: state.saleItems.map(item => (item.itemId==action.payload? {...item,active:false}: item))
      }
    },
    done: (state: InventoryStateProps) => {
      return { ...state, loading: '' }
    },
  })
  inactiveItem(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((id: string) =>
        this.inventory.inActiveItem(id).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(id)),
          endWith(this.createActionFrom(this.getSaleItems)({ active: state$.value.inventory.isActive })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state:any, action:any) => {
      return {
        ...state,
        saleItems: state.saleItems.map(item => (item.itemId == action.payload ? {...item, active:true}: item))
      }
    },
    done: (state: InventoryStateProps) => {
      return { ...state, loading: '' }
    },
  })
  activeItem(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((id: string) =>
        this.inventory.activeItem(id).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(id)),
          endWith(this.createActionFrom(this.getSaleItems)({ active: state$.value.inventory.isActive })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      window.location.reload()
      return {
        ...state,
        loading: false,
      }
    },
  })
  syncQBOItems(action$: Observable<any>) {
    return action$.pipe(
      switchMap((itemIds: any) => from(itemIds)),
      mergeMap((itemId: any) => {
        return this.inventory.syncQBOItem(itemId)
      }, 8),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      window.location.reload()
      return {
        ...state,
        loading: false,
      }
    },
  })
  syncNSItems(action$: Observable<any>) {
    return action$.pipe(
      switchMap((itemIds: any) => from(itemIds)),
      mergeMap((itemId: any) => {
        return this.inventory.syncNSItem(itemId)
      }, 8),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, message: 'Item Added to Price Sheet', type: MessageType.SUCCESS, loading: '' }
    },
  })
  assignPriceSheetItem(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.inventory.assignPriceSheetItem(data).pipe(
          switchMap((data) => of(checkMessage(data, 'Successfully Add Item To PriceSheet'))),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetsForItem)(data.wholesaleItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, message: 'Item Removed from Price Sheet', type: MessageType.SUCCESS, loading: '' }
    },
  })
  unassignPriceSheetItem(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.inventory.unassignPriceSheetItem(data).pipe(
          switchMap((data) => of(checkMessage(data, 'Successfully Removed Item From PriceSheet'))),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetsForItem)(data.wholesaleItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, editedItemId: '' }
    },
  })
  clearEditedItemId(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: InventoryStateProps) => {
      return { ...state, message: '', description: '', type: null }
    },
  })
  resetNotif(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Reducer()
  loadIndex(state: InventoryStateProps, action: Action<string>) {
    return {
      ...state,
      loading: action.payload,
    }
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      return {
        ...state,
        // message: 'Successfully updated item(s) from excel file',
        // type: MessageType.SUCCESS,
      }
    },
  })
  uploadExcel(action$: Observable<any>) {
    return action$.pipe(
      switchMap((excel) =>
        this.inventory.uploadExcel(excel).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      return {
        ...state,
        // message: 'Successfully updated item(s) from excel file',
        // type: MessageType.SUCCESS,
      }
    },
  })
  uploadBertiExcel(action$: Observable<any>) {
    return action$.pipe(
      switchMap((excel) =>
        this.inventory.uploadBertiExcel(excel).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      return {
        ...state,
        relatedOrders: {
          ...state.relatedOrders,
          [action.payload.item.wholesaleItemId]: action.payload.orders,
        },
      }
    },
  })
  getRelatedOrders(action$: Observable<string>) {
    return action$.pipe(
      switchMap((itemId: string) => this.inventory.getRelatedOrders(itemId)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getAllCompanyTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  formatProductTypes = (datas: any[]) => {
    let result = {
      unitOfMeasure: [],
      returnReason: [],
      carrier: [],
      modifier: [],
      costStructure: [],
      modeOfTransportation: [],
    }
    let companyId = localStorage.getItem(CACHED_COMPANY)

    // UNIT_OF_MEASURE, RETURN_REASON, CARRIER,MODIFIER,COST_STRUCTURE,MODE_OF_TRANSPORTATION
    datas.map((type: any) => {
      switch (type.type) {
        case 'UNIT_OF_MEASURE':
          result.unitOfMeasure.push(type)
          break
        case 'RETURN_REASON':
          result.returnReason.push(type)
          break
        case 'CARRIER':
          result.carrier.push(type)
          break
        case 'MODIFIER':
          result.modifier.push(type)
          break
        case 'COST_STRUCTURE':
          result.costStructure.push(type)
          break
        case 'MODE_OF_TRANSPORTATION':
          result.modeOfTransportation.push(type)
          break
      }
    })
    return result
  }

  @Effect({
    done: (state: InventoryStateProps, action: Action<any[]>) => {
      return {
        ...state,
        vendors: action.payload,
      }
    },
  })
  getVendors(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.order.getAllVendors().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        userSetting: payload.userSetting,
      }
    },
  })
  getUserSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getUserSetting(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          switchMap((data) =>
            of(data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: InventoryStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        netsuitUoms: payload,
      }
    },
  })
  getNetsuitUoms(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getNetsuitUoms().pipe(
          switchMap((data) =>
            of(data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  },

  @Effect({
    done: (state: InventoryStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        inPostingAdjustments: false,
      }
    },
  })
  postAdjustments(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.inventory.postAdjustments(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        itemLocations: payload,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  getItemLocations(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getItemLocations().pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }
}

export type InventoryDispatchProps = ModuleDispatchProps<InventoryModule>
