import { Injectable } from 'redux-epics-decorator'

import { Http } from '~/common'
import { SaleItem, SaleCategory, PriceSheetItem, PriceSheet } from '~/schema'
import { CACHED_ACCESSTOKEN } from '~/common/const'

@Injectable()
export class InventoryService {
  constructor(private readonly http: Http) {}

  static get instance() {
    return new InventoryService(new Http())
  }

  getAllItems(companyName: string, type: string | null, active?: number | null) {
    let url = `/inventory/${companyName}/items/list?type=${type}`
    if (active != null) {
      url += `&active=${active}`
    }
    return this.http.get<any>(url)
  }

  getAllLotsItems<T>() {
    let url = `/inventory/all-lot-items`
    return this.http.get<T>(url)
  }

  getAllItemsForAddModal(companyName: string, type: string | null, active?: number | null, clientId?: number | null) {
    let url = `/inventory/item/list/${companyName}?type=${type}`
    if (active != null) {
      url += `&active=${active}`
    }
    if (clientId != null) {
      url += `&clientId=${clientId}`
    }
    return this.http.get<any>(url)
  }

  getPurchaseItemsForAddItemModal(companyName: string) {
    let url = `/inventory/purchase-simplify-items?warehouse=${companyName}`
    return this.http.get<any>(url)
  }

  getItemList(companyName: string, query: any) {
    let url = `/inventory/${companyName}/items`
    return this.http.get<any>(url, { query })
  }

  getImportItems() {
    return this.http.get<any[]>(`/inventory/getQBOitems`)
  }

  getImportNSItems() {
    return this.http.get<any[]>(`/netsuite/item`)
  }

  getImportNSSalesOrder() {
    return this.http.get<any[]>(`/netsuite/nsOrder/invoice`)
  }

  getImportNSPurchaseOrder() {
    return this.http.get<any[]>(`/netsuite/nsOrder/vendorbill`)
  }

  syncQBOItem(itemId: string) {
    return this.http.post<any>(`/quickbook/sendQBOItem/${itemId}`)
  }

  syncNSItem(itemId: string) {
    return this.http.post<any>(`/netsuite/sendNSItem/${itemId}`)
  }

  getAllCategories(companyName: string) {
    return this.http.get<SaleCategory[]>(`/inventory/${companyName}/categories/list`)
  }

  updateItemPrice(data: any) {
    return this.http.post<any>(`/inventory/wholesaleitem/price/${data.itemId}`, {
      body: JSON.stringify(data),
    })
  }

  getAllPriceSheet(userId: string) {
    return this.http.get<PriceSheet[]>(`/pricesheet/user/${userId}`)
  }

  getPriceSheetsForItem(itemId: string) {
    return this.http.get<PriceSheetItem[]>(`/pricesheet/${itemId}/pricesheetitem/list`)
  }

  getItemPurchaseOrder(itemId: string) {
    return this.http.get<any>(`/inventory/grubmarket/item/${itemId}/orders/list`)
  }

  requestPurchaseOrder(purchaseOrder: any, userId: string) {
    const params = { ...purchaseOrder }

    let path = '/purchaseOrder'
    if (params.wholesaleOrderItemId) path = 'update' + path
    else path = 'create' + path
    const date = new Date(purchaseOrder.deliveryDate)
    params.deliveryDate = date.toLocaleDateString()
    params.userId = userId
    return this.http.post<any>(`/inventory/${path}`, {
      body: JSON.stringify(params),
    })
  }

  saveItem(params: any) {
    return this.http.post<any>('/inventory/update/wholesaleitem', {
      body: JSON.stringify(params),
    })
  }

  checkDuplicateSku(sku: string) {
    return this.http.get<any>(`/inventory/check-duplicate-sku?sku=${sku}`)
  }

  createItem(orderId: number, params: any) {
    return this.http.put<any>(`/inventory/item/order/${orderId}`, {
      body: JSON.stringify(params),
    })
  }

  assignPriceSheetItem(params: any) {
    return this.http.post<any>(`/pricesheet/add/item`, {
      body: JSON.stringify(params),
    })
  }

  unassignPriceSheetItem(params: any) {
    return this.http.post<any>(`/pricesheet/remove/item`, {
      body: JSON.stringify(params),
    })
  }

  savePriceSheetItem(priceSheetItem: PriceSheetItem) {
    return this.http.post<any>(`/pricesheet/${priceSheetItem.priceSheetItemId}/pricesheetitem/update`, {
      body: JSON.stringify(priceSheetItem),
    })
  }

  deleteItem(itemId: string) {
    return this.http.get<any>(`/inventory/warehouse/deleteItem?itemId=${itemId}`)
  }

  inActiveItem(itemId: string) {
    return this.http.put<any>(`/inventory/warehouse/inactiveItem?itemId=${itemId}`)
  }

  activeItem(itemId: string) {
    return this.http.put<any>(`/inventory/warehouse/activeItem?itemId=${itemId}`)
  }

  getRelatedOrders(itemId: string) {
    return this.http.get<any>(`/inventory/grubmarket/item/${itemId}/orders/list`)
  }

  uploadExcel(data: any) {
    let header = new Headers()
    // @ts-ignore
    header.append(
      'Authorization',
      `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN) != null ? localStorage.getItem(CACHED_ACCESSTOKEN) : ''}`,
    )
    header.append('Content-Type', 'application/x-www-form-urlencoded')

    return this.http.post<any>(`/inventory/uploadExcel`, {
      body: data,
      headers: header,
    })
  }

  uploadBertiExcel(data: any) {
    let header = new Headers()
    // @ts-ignore
    header.append(
      'Authorization',
      `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN) != null ? localStorage.getItem(CACHED_ACCESSTOKEN) : ''}`,
    )
    header.append('Content-Type', 'application/x-www-form-urlencoded')

    return this.http.post<any>(`/inventory/uploadExcelItemName`, {
      body: data,
      headers: header,
    })
  }

  getItemListByCondition(companyName: string, query: any) {
    query = {
      ...query,
      warehouse: companyName,
    }
    return this.http.get<any>('/inventory/items', { query })
  }

  getDownloadLotItemList() {
    let url = `/inventory/download-item-lots-list`
    return this.http.get<any>(url)
  }

  getNetsuitUoms() {
    let url = `/netsuite/nsUnits`
    return this.http.get<any>(url)
  }

  postAdjustments(params: any) {
    return this.http.post<any>(`/inventory/bulk-item-adjustment`, {
      body: JSON.stringify(params),
    })
  }

  getInventoryOrders(query: any) {
    return this.http.get('/inventory/order/inventoryOrders', {
      query,
    })
  }

  getItemsByLotId(search: string) {
    return this.http.get(`/inventory/items-by-lotId?search=${search}`)
  }
}
