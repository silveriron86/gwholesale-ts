import styled from '@emotion/styled'

import { white, lightGrey, darkGrey, brightGreen } from '~/common'

export const HeaderContainer = styled('div')({
  height: '448px',
  backgroundColor: white,
  width: '100%',
  boxSizing: 'border-box',
  position: 'relative',
})

export const HeaderTitle = styled('div')((props) => ({
  marginTop: '48px',
  marginLeft: '68px',
  color: props.theme.primary,
  fontSize: '24px',
  lineHeight: '26px',
  letterSpacing: '0.05em',
}))

export const FilterContainer = styled('div')((props) => ({
  boxSizing: 'border-box',
  paddingBottom: '40px',
  borderBottom: `1px solid ${lightGrey}`,

  '& > h3': {
    color: darkGrey,
    fontFamily: '"Museo Sans Rounded"',
    lineHeight: '19px',
    fontSize: '18px',
    fontWeight: 700,
    letterSpacing: '0.05em',
    marginBottom: '20px',
  },
  '& > div > .ant-checkbox-group > .ant-row > .ant-col-12 > .ant-checkbox-wrapper > span': {
    color: darkGrey,
    fontFamily: '"Museo Sans Rounded"',
    lineHeight: '31px',
    fontSize: '12px',
    letterSpacing: '0.05em',
  },
  '.highlight-input': {
    '.ant-input::selection': {
      backgroundColor: props.theme.theme
    }
  }
}))

export const OptionContainer = styled('div')({
  width: '100%',
  height: '87px',
  position: 'absolute',
  bottom: 0,
  left: 0,
  padding: '0 37px 0 27px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: white,
  boxSizing: 'border-box',
})

export const FormLabel = styled('label')({
  color: lightGrey
})

export const SearchInput = styled('input')({
  width: '394px',
  height: '50px',
  backgroundColor: white,
  border: `1px solid ${lightGrey}`,
  borderRadius: '5px',
  boxSizing: 'border-box',
})

export const FilterWrap = styled('div')({
  flexGrow: 1,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
})

export const ButtonWrap = styled('div')({
  display: 'flex',
})

export const FakeButton = styled('button')({
  height: '47px',
  padding: '0 18px',
  border: `1px solid ${lightGrey}`,
  display: 'flex',
  borderRadius: '5px',
  marginLeft: '18px',
  cursor: 'pointer',
})

export const ResetButton = styled('button')({
  color: brightGreen,
  fontFamily: '"Museo Sans Rounded"',
  textTransfer: 'uppercase',
  fontSize: '12px',
  letterSpacing: '0.05em',
  backgroundColor: white,
  border: `1px solid ${brightGreen}`,
  borderRadius: '15px',
  marginTop: '5px',
  cursor: 'pointer',
  height: '20px',
})

export const ShowMoreButton = styled('div')({
  marginTop: '12px',
  color: brightGreen,
  fontWeight: 700,
  fontFamily: '"Museo Sans Rounded"',
  lineHeight: '31px',
  fontSize: '13px',
  letterSpacing: '0.05em',
})

export const Flex = styled('div')({
  display: 'flex',
  flexDirection: 'column',
})

export const BodyContainer = styled('div')({
  minHeight: '512px',
  width: '100%',
  backgroundColor: lightGrey,
  boxSizing: 'border-box',
})
