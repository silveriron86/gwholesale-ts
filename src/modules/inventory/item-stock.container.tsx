import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'

import { GlobalState } from '~/store/reducer'
import { InventoryDispatchProps, InventoryStateProps, InventoryModule } from './inventory.module'

import { cloneDeep } from 'lodash'
import { OrderItem } from '~/schema'

import { ItemStockTable, ItemStockPannel, ItemStockHeader } from './components'
import { notification } from 'antd';
import { MessageType } from '~/components'

export type ItemStockProps = InventoryDispatchProps & InventoryStateProps & RouteComponentProps<{ id: string }>

export interface ItemStockState {
  orderItem: OrderItem
  showDrawer: boolean
  message: String
  type: MessageType | null
}

export class ItemStockComponent extends React.PureComponent<ItemStockProps, ItemStockState> {
  state = {
    showDrawer: false,
    orderItem: {} as OrderItem,
    message: '',
    type: null,
  }

  componentDidMount() {
    const id = this.props.match.params.id

    this.setState({
        orderItem: {
          ...this.state.orderItem,
          wholesaleItemId: this.props.match.params.id,
          deliveryDate: new Date().getTime(),
        },
    })
    this.props.getItemPurchaseOrder(id)
  }

  componentWillReceiveProps(nextProps: ItemStockProps) {
    if (nextProps.message !== this.state.message) {
      this.setState({
        message: nextProps.message,
        type: nextProps.type,
      })
      if (nextProps.type === 'success')
        this.onCloseDrawer();

      if (nextProps.type != null) {
        notification[nextProps.type!]({
          message: nextProps.type!.toLocaleUpperCase(),
          description: nextProps.message,
          onClose: this.onCloseNotification,
          duration: nextProps.type === 'success' ? 5 : 4.5
        })
      }
    }
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  onDateChange = (_dateMoment: any, dateString: string) => {
    if (dateString.length > 0) {
      this.setState({
        orderItem: {
          ...this.state.orderItem,
          deliveryDate: new Date(dateString).getTime(),
        },
      })
    }
  }

  editItem = (orderItem: OrderItem) => {
    this.setState({
      orderItem: cloneDeep(orderItem),
      showDrawer: true,
    })
  }

  onShowDrawer = () => {
    this.setState({
      showDrawer: true,
    })
  }

  onCloseDrawer = () => {
    this.setState({
      orderItem: {} as OrderItem,
      showDrawer: false,
    })
  }

  onChangeEditInput = (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = null
    if (channel === 'cost' || channel === 'price' || channel === 'quantity') value = e.target.valueAsNumber
    else value = e.target.value

    this.setState({
      orderItem: {
        ...this.state.orderItem,
        [channel]: value,
      },
    })
  }

  onSelectStatus = (value: any) => {
    this.setState({
      orderItem: {
        ...this.state.orderItem,
        status: value,
      },
    })
  }

  saveItem = () => {
    this.props.requestPurchaseOrder(this.state.orderItem)
  }

  render() {
    return (
      <div>
        <ItemStockHeader onShowDrawer={this.onShowDrawer} item={this.props.item} onClickBack={this.props.goBack} />
        <ItemStockPannel
          item={this.state.orderItem}
          onChangeEditInput={this.onChangeEditInput}
          saveItem={this.saveItem}
          showDrawer={this.state.showDrawer}
          onCloseDrawer={this.onCloseDrawer}
          onDateChange={this.onDateChange}
          onSelectStatus={this.onSelectStatus}
        />
        <ItemStockTable editItem={this.editItem} orderItems={this.props.orderItems} />
      </div>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.inventory

export const ItemStock = connect(InventoryModule)(mapStateToProps)(ItemStockComponent)
