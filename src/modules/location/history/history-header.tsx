import React from 'react'
import { Moment } from 'moment'
import { Spin } from 'antd'
import debounce from 'lodash/debounce';
import { Theme } from '~/common'
import { HeaderElement, HistoryHeaderWrapper } from '../location.style'
import { Row, Col, DatePicker, Select } from 'antd'
import { Flex, flexStyle, HeaderTitle, ThemeButton, ThemeSelect } from '~/modules/customers/customers.style'
import GrubSuggest from '~/components/GrubSuggest'
import { Http } from '~/common'
import moment from 'moment'

const { RangePicker } = DatePicker
const { Option } = Select
export type ReportPageHeaderProps = {
  theme: Theme
  onSearchChange: Function
  from: Moment
  to: Moment
  search: string
  data: any[]
  locations: any[]
}

class ReportPageHeader extends React.PureComponent<ReportPageHeaderProps> {
  lastFetchId = 0
  state = {
    pallets: [],
    fetching: false,
  }

  constructor(props: ReportPageHeaderProps) {
    super(props);
    this.fetchPallets = debounce(this.fetchPallets, 800);
  }

  fetchPallets = (value: string) => {
    this.lastFetchId += 1;
    const fetchId = this.lastFetchId;
    this.setState({
      pallets: [],
      fetching: true
    }, () => {
      const http = new Http()
      http.get(`/location/pallets?search=${value}&limit=5`)
        .subscribe((res: any) => {
          if (fetchId !== this.lastFetchId) {
            // for fetch callback order
            return;
          }
          this.setState({
            pallets: res.body.data,
            fetching: false
          });
        });
    });
  };

  onSearch = (keyword: string) => {
    this.props.onSearchChange('search', keyword)
  }

  onDateChange = (date: any, dateString: [string, string]) => {
    this.props.onSearchChange('date', { from: date[0], to: date[1] })
  }

  onSelectChange = (type: string, value: number|string) => {
    let data: any = value
    if (type == 'displayNumber' && value == undefined) {
      data = ''
    } else if (type == 'locationId' && value == undefined) {
      data = 0
    }
    this.props.onSearchChange(type, data)
    this.setState({
      fetching: false,
    })
  }

  onSuggest = (str: string) => {
    return []
  }

  onDownload = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('pallet-history-table'))
    var wscols = [
      { wpx: 100 },
      { wpx: 300 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 150 },
    ]

    wb.Sheets.Sheet1['!cols'] = wscols
    console.log(this.props.data)
    this.props.data.forEach((el, index)=> {
      wb.Sheets.Sheet1['D' + (index + 2)] = {v: el.displayNumber}
      wb.Sheets.Sheet1['G' + (index + 2)] = {v: moment(el.createdAt).format('MM/DD/YYYY LT')}
    })
    const today = moment().format('MM.DD.YYYY')
    const title = `Pallet History-${today}.xlsx`
    XLSX.writeFile(wb, title)
  }

  render() {
    const { from, to, locations } = this.props
    const { pallets, fetching } = this.state

    return (
      <HistoryHeaderWrapper>
        <Flex className="v-center">
          <HeaderTitle style={{ marginBottom: 20 }}>Pallet History</HeaderTitle>
          <ThemeButton onClick={this.onDownload} style={{float: 'right'}}>
            Download
          </ThemeButton>
        </Flex>
        <Row>
          <Col style={flexStyle} span={12}>
            <HeaderElement>
              <GrubSuggest theme={this.props.theme} onSearch={this.onSearch} onSuggest={this.onSuggest} />
            </HeaderElement>
            <HeaderElement>
              <RangePicker
                placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                format={'MM/DD/YYYY'}
                defaultValue={[from, to]}
                onChange={this.onDateChange}
                allowClear={false}
              />
            </HeaderElement>
          </Col>
          <Col style={{ ...flexStyle, justifyContent: 'flex-end' }} span={12}>
            <HeaderElement>
              <ThemeSelect
                labelInValue
                showSearch
                placeholder={'Please select the pallet...'}
                notFoundContent={fetching ? <Spin size="small" /> : null}
                defaultActiveFirstOption={false}
                filterOption={false}
                onSearch={this.fetchPallets}
                onChange={(v: any) => this.onSelectChange('displayNumber', v.label)}>
                {/* allowClear={true}
                onClear={this.onSelectChange.bind(this, 'displayNumber')}> */}

                {
                  pallets.map(el => {
                    return (<Option value={el}>{el}</Option>)
                  })
                }
              </ThemeSelect>
            </HeaderElement>
            <HeaderElement>
              <ThemeSelect
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                onChange={this.onSelectChange.bind(this, 'locationId')}
                placeholder={'Please select the location...'}
                allowClear={true}
                onClear={this.onSelectChange.bind(this, 'locationId')}
              >

                {
                  locations.map(el => {
                    return (<Option key={el.id} value={el.id}>{el.name}</Option>)
                  })
                }
              </ThemeSelect>
            </HeaderElement>
          </Col>
        </Row>
        <table id="pallet-history-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
        <thead>
          <tr>
            <th>Order Id</th>
            <th>Product</th>
            <th>Company Name</th>
            <th>Pallet Number</th>
            <th>Location</th>
            <th>Action Type</th>
            <th>Create Date</th>
          </tr>
        </thead>
        <tbody>
          {this.props.data.map((row: any) => {
            return (
              <tr key={`row-${row.wholesaleOrderId}`}>
                <td>{row['wholesaleOrderId']}</td>
                <td>{row['variety']}</td>
                <td>{row['companyName']}</td>
                <td></td>
                <td>{row['locationName']}</td>
                <td>{row['actionType']}</td>
                <td></td>
              </tr>
            )
          })}
        </tbody>
      </table>
      </HistoryHeaderWrapper>
    )
  }
}

export default ReportPageHeader
