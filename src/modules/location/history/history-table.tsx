import React from 'react'
import moment from 'moment'
import { ThemeTable } from '~/modules/customers/customers.style'

export type PalletHistoryTableProps = {
  total: number
  pageSize: number
  curPage: number
  onPageChange: Function
  data: any[]
  loading: boolean
}

const columns: any[] = [
  {
    title: '#',
    dataIndex: '',
    key: 'id',
    render: (text: any, record: any, index: number) => {
      return index + 1
    }
  },
  {
    title: 'Order Id',
    dataIndex: 'wholesaleOrderId',
    width: 200,
    align: 'center',
  },
  {
    title: 'Product',
    dataIndex: 'variety',
    align: 'center',
    width: 150,
  },
  {
    title: 'Company Name',
    dataIndex: 'companyName',
    align: 'center',
    width: 150,
  },
  {
    title: 'Pallet Number',
    dataIndex: 'displayNumber',
    align: 'center',
  },
  {
    title: 'Location',
    dataIndex: 'locationName',
    align: 'center',
  },
  {
    title: 'Action Type',
    dataIndex: 'actionType',
    align: 'center',
  },
  {
    title: 'Created Date',
    dataIndex: 'createdAt',
    align: 'center',
    render: (date: number) => {
      return moment(date).format('MM/DD/YYYY LT')
    }
  },
]
class PalletHistoryTable extends React.PureComponent<PalletHistoryTableProps> {


  onPageChange = (page: any) => {
    this.props.onPageChange('curPage', page - 1)
  }
  render() {
    const { total, pageSize, curPage, data, loading } = this.props
    return (
      <ThemeTable
        pagination={{
          hideOnSinglePage: true,
          pageSize: pageSize,
          onChange: this.onPageChange,
          current: curPage + 1,
          total: total,
          defaultCurrent: 1
        }}
        columns={columns}
        dataSource={data}
        loading={loading}
        rowKey="historyId"
      />
    )
  }
}

export default PalletHistoryTable