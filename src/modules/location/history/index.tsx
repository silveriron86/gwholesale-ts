import React from 'react'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import moment from 'moment'
import PageLayout from '~/components/PageLayout'
import { LocationDispatchProps, LocationModule, LocationStateProps } from './../location.module'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import ReportPageHeader from './history-header'
import { ReportPageContainer } from '../location.style'
import PalletHistoryTable from './history-table'

export type PalletHistoryProps = LocationDispatchProps &
  LocationStateProps & {
    theme: Theme
  }

class PalletHistory extends React.PureComponent<PalletHistoryProps> {
  state = {
    from: moment().subtract(30, 'days'),
    to: moment(),
    curPage: 0,
    pageSize: 12,
    search: '',
    displayNumber: '',
    locationId: 0
  }

  componentDidMount() {
    // this.props.getPalletDisplayNumbers()
    this.props.getLocations()
    this.getHistory(true)
  }

  getHistory = (needCallDownload: boolean = false) => {
    const seachObj = {
      ...this.state,
      from: this.state.from.format('MM/DD/YYYY'),
      to: this.state.to.format('MM/DD/YYYY'),
    }
    this.props.resetLoading()
    this.props.setSearchProps(seachObj)
    this.props.getPalletHistory(seachObj)
    if (needCallDownload) {
      this.props.getPalletDownloadData(seachObj)
    }
  }

  onChange = (type: string, data: any) => {
    if (type == 'date') {
      this.setState({ from: data.from, to: data.to })
    } else {
      let obj = this.state
      obj[type] = data
      this.setState({ ...obj })
    }

    setTimeout(() => {
      let needCallDownload = false;
      if (type != 'curPage') { // call download data unless it is page change request
        needCallDownload = true;
      }
      this.getHistory(needCallDownload)
    }, 100)
  }

  render() {
    const { from, to, curPage, pageSize, search, displayNumber, locationId } = this.state
    const { totalRows, locations, palletHistory, loading, palletDownloadData } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Pallet History'}>
        <ReportPageContainer>
          <ReportPageHeader
            onSearchChange={this.onChange}
            from={from}
            to={to}
            search={search}
            theme={this.props.theme}
            data={palletDownloadData}
            locations={locations}
          />
          <PalletHistoryTable
            curPage={curPage}
            pageSize={pageSize}
            total={totalRows}
            onPageChange={this.onChange}
            data={palletHistory}
            loading={loading}
          />
        </ReportPageContainer>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.location }
}

export default withTheme(connect(LocationModule)(mapStateToProps)(PalletHistory))
