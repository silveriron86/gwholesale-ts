import { ColumnProps } from 'antd/es/table'
import { withTheme } from 'emotion-theming'
import { Icon, Tabs, Input, Popconfirm } from 'antd'
import React from 'react'
import moment from 'moment'
import PageLayout from '~/components/PageLayout'
import { LocationHeader, LocationTable, WarehouseTabs, Relative, FullCenter } from './location.style'
import { ThemeButton, ThemeIconButton, ThemeInput, ThemeSpin, ThemeSwitch } from '../customers/customers.style'
import { Icon as IconSvg } from '~/components/icon/'
import ProductDetailModal from './product-detail-modal'
import jQuery from 'jquery'
import { connect } from 'redux-epics-decorator'
import { LocationDispatchProps, LocationModule, LocationStateProps } from './location.module'
import { GlobalState } from '~/store/reducer'
import { RouteComponentProps, RouteProps } from 'react-router'
import { Theme } from '~/common'
import { isMobile } from 'react-device-detect'
import { cloneDeep } from 'lodash'
import { LocationService } from './location.service'
import { of } from 'rxjs'
import { checkError, responseHandler, warehouseSyncParseAlgorithm } from '~/common/utils'

export type LocationPageProps = LocationDispatchProps &
  LocationStateProps &
  RouteProps &
  RouteComponentProps & {
    theme?: Theme
  }

const getUUID = () => {
  let d = new Date().getTime()
  if (window && window.performance && typeof window.performance.now === 'function') {
    d += performance.now()
  }
  return 'xxxxxxxx-xxxx-yxxx-xxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16)
  })
}

const { TabPane } = Tabs
const searchButton = (
  <React.Fragment>
    <IconSvg viewBox="0 0 16 16" width="16" height="16" type="search" />
    <span style={{ marginLeft: '9px', fontSize: '14px', lineHeight: '15px', letterSpacing: '0.05em' }}>SEARCH</span>
  </React.Fragment>
)

const defaultItem = {
  id: getUUID(),
  timestamp: '',
  type: '',
  input1: '',
  input2: '',
  locationId: 0,
  manualInput1: '',
  manualInput2: '',
}

class LocationPage extends React.PureComponent<LocationPageProps> {
  state: any
  constructor(props: LocationPageProps) {
    super(props)
    const items = localStorage.getItem('SYNC_ITEMS')
    this.state = {
      visibleModal: false,
      listItems: items ? cloneDeep(JSON.parse(items)) : [{ ...defaultItem }],
      manualEntry: false,
      assignmentsLoading: false,
      tabIndex: '1',
    }
  }

  onSearch = (value: string) => {
    this.props.initProducts()
    if (value !== '') {
      this.props.resetProductsLoading()
      this.props.searchProductsData(value)
    }
  }

  onCloseModal = () => {
    this.setState({
      visibleModal: false,
    })
  }



  // componentWillReceiveProps(nextProps: LocationPageProps) {
  //   if (this.props.synced !== nextProps.synced) {
  //     if (nextProps.synced !== null) {
  //       const { listItems } = this.state

  //       let failedRows: any[] = []
  //       nextProps.synced.forEach((row: any, index: number) => {
  //         if (row.type === 'fail') {
  //           failedRows.push({ ...listItems[index], type: `ERROR: ${row.message}` })
  //         }
  //       })
  //       console.log(failedRows)
  //       window.localStorage.removeItem('SYNC_ITEMS')
  //       this.setState(
  //         {
  //           listItems: failedRows.length ? failedRows : [defaultItem],
  //         },
  //         () => {
  //           setTimeout(() => {
  //             if (failedRows.length === 0) {
  //               jQuery('.location-input').val('')
  //             }
  //             jQuery('.location-input')
  //               .first()
  //               .focus()
  //           }, 10)
  //         },
  //       )
  //     }
  //   }
  // }

  onSyncData = () => {
    const _this = this
    const { listItems } = this.state
    this.setState({ assignmentsLoading: true })

    let data: any[] = []
    listItems.forEach((item: any, index: number) => {
      console.log(item)
      if (item.timestamp) {
        let row = {
          scanDate: moment(item.timestamp, 'M/DD/YYYY HH:mm:ss A').format('YYYY-MM-DD HH:mm:ss'),
          type: item.type,
        }
        if (item.type === 'CLEAR_LOCATION') {
          row['locationId'] = item.locationId
        } else if (item.type === 'CLEAR_PALLET') {
          row['palletNumber'] = item.palletNumber
        } else if (item.type === 'ASSIGN' || item.type.includes('ERROR')) {
          // when input1 != input2 and input 1 is location name ,input 2 is pallet  name Or reverse ，action is assign
          let found = this.props.locations.find((loc) => loc.name.toLowerCase() === item.input1.toLowerCase())
          if (found) {
            row['palletNumber'] = item.input2.trim()
            row['locationId'] = found.id
          } else {
            row['palletNumber'] = item.input1.trim()
            row['locationId'] = item.locationId
          }
        }
        data.push(row)
      }
    })
    LocationService.instance.syncData(data).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        console.log(res.body)

        const failedRows: any = []

        res.body.data.forEach((row: any, index: number) => {
          if (row.type === 'fail') {
            failedRows.push({ ..._this.state.listItems[index], type: `ERROR: ${row.message}` })
          }
        })

        if (failedRows.length) {
          _this.setState({
            listItems: failedRows,
          })
        } else {
          _this.setState({
            listItems: [defaultItem],
          })
        }
      },
      error(err) {
        checkError(err)
      },
      complete() {
        _this.setState({ assignmentsLoading: false })
        window.localStorage.removeItem('SYNC_ITEMS')
        // setTimeout(() => {
        //   jQuery('.location-input')
        //     .first()
        //     .focus()
        // }, 100)
      },
    })
  }

  componentDidMount() {
    const q = new URLSearchParams(this.props.location.search).get("q")
    if (q) {
      this.setState({
        tabIndex: '2',
      }, () => {
        this.onSearch(q)
      })
    } else {
      setTimeout(() => {
        jQuery('.location-input')
          .first()
          .focus()
      }, 150)
    }
    this.props.getLocations()
    this.props.getPalletDisplayNumbers()
    this.props.getCompanyUsers(null)
    this.initEvents()
    this.props.history.listen((location) => {
      if (this.props.location.pathname !== location.pathname) {
        window.localStorage.removeItem('SYNC_ITEMS')
        this.props.initProducts()
      }
    })
  }

  initEvents = () => {
    jQuery('.location-input')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        // e.preventDefault()
        if (e.target.value && e.keyCode == 13) {
          // Enter
          var index = jQuery(e.target).data('index')
          if (index % 2 === 1) {
            this.handleProcess(index)
            jQuery('#input' + (index + 1)).focus()
          } else {
            jQuery('#input' + (index + 1)).focus()
          }
        }
      })
  }

  parseInputType = (input1: any, input2: any) => {
    let locationId = 0
    if (input2.length && input1.length > 18 && !input1.startsWith('WSW', 4)) {
      return { type: 'ERROR: Wrong Code Scanned!', locationId }
    }

    if (input2.length && input2.length > 18 && !input2.startsWith('WSW', 4)) {
      return { type: 'ERROR: Wrong Code Scanned!', locationId }
    }

    if (input1.toLowerCase() === input2.toLowerCase()) {
      if (!input1 || !input2) {
        return { type: 'ERROR: No Data', locationId }
      }
      const found = this.props.locations.find((loc) => loc.name.toLowerCase() === input1.toLowerCase())
      const foundPallet = this.props.pallets.find(pallet => pallet.toLowerCase() == input1.toLowerCase())
      if (found) {
        // when input1 == input2  and input1 is location name. action is clear_location
        return { type: 'CLEAR_LOCATION', locationId: found.id }
      } else if (foundPallet) {
        // when input1 == input2 and input1 is pallet name, action is clear_pallet
        return { type: 'CLEAR_PALLET', palletNumber: foundPallet }
      } else {
        return { type: 'ERROR: Location not found!', locationId }
      }
    } else {
      if (!input1 || !input2) {
        return { type: 'ERROR: No Data', locationId }
      }
      // when input1 != input2 and input 1 is location name ,input 2 is pallet  name Or reverse ，action is assign
      let found = this.props.locations.find((loc) => loc.name.toLowerCase() === input1.toLowerCase())
      if (found) {
        return { type: 'ASSIGN', locationId: found.id }
      } else {
        found = this.props.locations.find((loc) => loc.name.toLowerCase() === input2.toLowerCase())
        if (found) {
          return { type: 'ASSIGN', locationId: found.id }
        } else {
          return { type: 'ERROR: Location not found!', locationId }
        }
      }
    }
  }

  handleProcess = (index: number) => {
    let items = [...this.state.listItems]

    const rowIndex = parseInt((index / 2).toString(), 10)
    const input1 = jQuery('#input' + (index - 1)).val()
    const input2 = jQuery('#input' + index).val()

    let { type, locationId, palletNumber } = this.parseInputType(input1, input2)

    if (!input1 || !input2) {
      type = 'ERROR: No Data'
    }

    items[rowIndex].timestamp = moment().format('M/DD/YYYY HH:mm:ss A')
    items[rowIndex].input1 = input1
    items[rowIndex].input2 = input2
    items[rowIndex].type = type
    items[rowIndex].locationId = locationId
    items[rowIndex].palletNumber = palletNumber

    if (items[items.length - 1].timestamp !== '') {
      items.push({
        id: getUUID(),
        timestamp: '',
        type: '',
        input1: '',
        input2: '',
        locationId: 0,
        manualInput1: '',
        manualInput2: '',
      })
    }
    localStorage.setItem('SYNC_ITEMS', JSON.stringify(items))
    this.setState(
      {
        listItems: items,
      },
      () => {
        this.initEvents()
        // setTimeout(() => {
        //   jQuery('#input' + (index + 1)).focus()
        // }, 10)
      },
    )
  }

  onDelete = (record) => {
    const listItems = this.state.listItems.filter((v) => v.id !== record.id)
    this.setState({
      listItems,
    })
    if (!listItems.length) return window.localStorage.removeItem('SYNC_ITEMS')
    window.localStorage.setItem('SYNC_ITEMS', JSON.stringify(listItems))
  }

  onSelectProduct = (itemId: string) => {
    this.props.resetProductItemLoading()
    this.setState({
      visibleModal: true,
    })
    this.props.getProductItem(itemId)
  }

  onTabClick = (tabIndex: string) => {
    this.setState({
      tabIndex,
    }, () => {
      if (tabIndex === '1') {
        setTimeout(() => {
          jQuery('.location-input')
            .first()
            .focus()
        }, 100)
      }
    })
  }

  onFocusInput1 = (record: any) => {
    this.setState({
      listItems: this.state.listItems.map((v: any) => (v.id === record.id ? { ...v, input1: record.manualInput1 } : v)),
    })
  }
  onFocusInput2 = (record: any) => {
    this.setState({
      listItems: this.state.listItems.map((v: any) => (v.id === record.id ? { ...v, input2: record.manualInput2 } : v)),
    })
  }

  onBlurInput1 = (record: any, value: string) => {
    if (this.state.manualEntry) return

    this.setState({
      listItems: this.state.listItems.map((v: any) =>
        v.id === record.id
          ? {
            ...v,
            manualInput1: value,
            input1: warehouseSyncParseAlgorithm(value),
            type: record.timestamp ? this.parseInputType(record.input1, record.input2).type : '',
          }
          : v,
      ),
    })
  }

  onBlurInput2 = (e: any, record: any, value: string) => {
    var index = e.currentTarget.getAttribute('data-index')
    if (!this.state.manualEntry) {
      this.setState(
        {
          listItems: this.state.listItems.map((v: any) =>
            v.id === record.id
              ? {
                ...v,
                manualInput2: value,
                input2: warehouseSyncParseAlgorithm(value),
                type: this.parseInputType(record.input1, record.input2).type,
              }
              : v,
          ),
        },
        () => {
          this.handleProcess(index)
        },
      )
    }
  }

  handleChangeManual = (checked: boolean) => {
    // if (checked) {}
    this.setState({
      manualEntry: checked,
      listItems: this.state.listItems.map((v) => ({
        ...v,
        input1: checked ? v.input1 : '',
        manualInput1: checked ? v.manualInput1 : '',
        input2: checked ? v.input2 : '',
        manualInput2: checked ? v.manualInpu2 : '',
      })),
    })
  }

  render() {
    const { visibleModal, listItems } = this.state

    const columns: ColumnProps<any>[] = [
      {
        title: '#',
        align: 'center',
        render: (id: string, r, i) => {
          return <div style={{ paddingLeft: isMobile ? 0 : 48 }}>{i + 1}</div>
        },
      },
      {
        title: 'TIMESTAMP',
        dataIndex: 'timestamp',
      },
      {
        title: 'TYPE',
        dataIndex: 'type',
        render: (type: string) => {
          return <div className={type !== 'IN PROGRESS' ? type.toLowerCase() : 'progress'}>{type}</div>
        },
      },
      {
        title: 'INPUT 1',
        dataIndex: 'input1',
        render: (value: string, record: any, index: number) => {
          return (
            <ThemeInput
              id={`input${index * 2}`}
              data-index={index * 2}
              className="location-input input1"
              value={value}
              data-manualInput={record.manualInput1}
              onChange={(e) =>
                this.setState({
                  listItems: listItems.map((v) => (v.id === record.id ? { ...v, input1: e.target.value } : v)),
                })
              }
              onFocus={() => this.onFocusInput1(record)}
              onBlur={() => this.onBlurInput1(record, value)}
            />
          )
        },
      },
      {
        title: 'INPUT 2',
        dataIndex: 'input2',
        render: (value: string, record: any, index: number) => {
          return (
            <FullCenter>
              <ThemeInput
                id={`input${index * 2 + 1}`}
                data-index={index * 2 + 1}
                className="location-input input2"
                value={value}
                data-manualInput={record.manualInput2}
                onChange={(e) => {
                  this.setState({
                    listItems: listItems.map((v) => (v.id === record.id ? { ...v, input2: e.target.value } : v)),
                  })
                }}
                onFocus={() => this.onFocusInput2(record)}
                onBlur={(e) => this.onBlurInput2(e, record, value)}
              />
            </FullCenter>
          )
        },
      },
      {
        title: '',
        dataIndex: 'action',
        key: 'action',
        width: 50,
        align: 'center',
        render: (value: string, record: any, index: number) => {
          return (
            <FullCenter>
              <Popconfirm
                title="Are you sure to delete?"
                onConfirm={() => this.onDelete(record)}
                okText="Yes"
                cancelText="No"
                okType="danger"
              >
                <ThemeIconButton type="link" className="close-btn" style={{ marginTop: isMobile ? -3 : 10 }}>
                  <Icon type="close" />
                </ThemeIconButton>
              </Popconfirm>
            </FullCenter>
          )
        },
      },
    ]

    const columns2: ColumnProps<any>[] = [
      {
        title: 'PRODUCT NAME',
        dataIndex: 'variety',
        render: (variety: string) => {
          return <div style={{ paddingLeft: 83 }}>{variety}</div>
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
      },
      {
        title: 'LOT ID',
        dataIndex: 'lotId',
      },
      {
        title: 'PALLET ID',
        dataIndex: 'displayNumber',
      },
      {
        title: 'INITIAL UNITS ON PALLET',
        dataIndex: 'palletUnits',
        render: (palletUnits, record) =>
          `${palletUnits} ${typeof record.overrideUOM !== 'undefined' ? record.overrideUOM : record.inventoryUOM}`,
      },
      {
        title: 'UPDATED',
        dataIndex: 'scanDate',
        render: (scanDate) => {
          return moment(scanDate).format('MM/DD/YY HH:mm:ss')
        }

      },
      {
        title: 'LOCATION',
        dataIndex: 'locationName',
      },
    ]

    const { loadingProducts, products, loadingProductItem, productItem, currentCompanyUsers } = this.props
    const { assignmentsLoading, tabIndex } = this.state
    const defaultSearch = new URLSearchParams(this.props.location.search).get("q")

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Warehouse Sync'}>
        <WarehouseTabs
          activeKey={tabIndex}
          className={isMobile ? 'mobile' : ''}
          onTabClick={this.onTabClick}
          tabBarExtraContent={
            <span>
              Manual Entry{' '}
              <ThemeSwitch checked={this.state.manualEntry} onChange={(checked) => this.handleChangeManual(checked)} />
            </span>
          }
        >
          <TabPane tab="Assignments" key="1">
            <ThemeSpin spinning={assignmentsLoading}>
              <Relative>
                <LocationHeader className={`first-tab ${isMobile ? 'mobile' : ''}`}>
                  SCAN ADDRESS TO ASSIGN OR PALLET ID AGAIN TO CLEAR
                  <ThemeButton
                    shape="round"
                    className={`sync-btn ${isMobile ? 'mobile' : ''}`}
                    style={{ position: 'absolute', right: 61 }}
                    onClick={this.onSyncData}
                    disabled={this.state.listItems.some((v) => v.type.includes('ERROR: No Data'))}
                  >
                    <IconSvg type="reload" viewBox="0 0 18 18" width={isMobile ? 12 : 18} height={isMobile ? 12 : 18} />
                    Sync Data
                  </ThemeButton>
                </LocationHeader>
                <LocationTable
                  className={`colorable ${isMobile ? 'mobile' : ''}`}
                  pagination={false}
                  rowClassName={(record: any, index) =>
                    record.type === 'ASSIGN'
                      ? 'assign-row'
                      : record.type === 'CLEAR_PALLET' || record.type === 'CLEAR_LOCATION'
                        ? 'clear-row'
                        : record.type.includes('ERROR')
                          ? 'error-row'
                          : ''
                  }
                  columns={columns}
                  dataSource={listItems}
                  rowKey="id"
                />
              </Relative>
            </ThemeSpin>
          </TabPane>
          <TabPane tab="Products" key="2">
            <ThemeSpin spinning={loadingProducts}>
              <Relative>
                <LocationHeader className={isMobile ? 'mobile' : ''}>
                  <Input.Search
                    defaultValue={defaultSearch}
                    placeholder="Search product name, SKU, or location"
                    size="large"
                    enterButton={searchButton}
                    onSearch={this.onSearch}
                    disabled={false}
                  />
                </LocationHeader>
                <LocationTable
                  className={`second ${isMobile ? 'mobile' : ''}`}
                  pagination={false}
                  columns={columns2}
                  dataSource={products}
                  rowKey="id"
                  onRow={(record: any, _rowIndex) => {
                    return {
                      onClick: (_event) => {
                        // this.onSelectProduct(record.wholesaleItemId)
                      },
                    }
                  }}
                />
              </Relative>
            </ThemeSpin>
          </TabPane>
        </WarehouseTabs>
        <ProductDetailModal
          visible={visibleModal}
          onClose={this.onCloseModal}
          item={productItem}
          loading={loadingProductItem}
          companyUsers={currentCompanyUsers}
        />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.location }
}

export default withTheme(connect(LocationModule)(mapStateToProps)(LocationPage))
