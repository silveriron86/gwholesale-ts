import { Module, EffectModule, ModuleDispatchProps, Effect, DefineAction } from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { map, switchMap, takeUntil, catchError } from 'rxjs/operators'
import { push, goBack } from 'connected-react-router'
import { Action } from 'redux-actions'

import { LocationService } from './location.service'
import { checkError, responseHandler } from '~/common/utils'
import { Order } from '~/schema'
import { ProductService } from '../product/product.service'
import { CustomerService } from '../customers/customers.service'

export interface LocationStateProps {
  locations: any[]
  pallets: any[]
  loading: boolean
  loadingProductItem: boolean
  productItem: any
  loadingProducts: boolean
  products: any[]
  currentCompanyUsers: any[]
  synced: any
  totalRows: number
  searchProps: any
  palletHistory: any[]
  assignmentsLoading: boolean
  palletDownloadData: any[]
}

@Module('location')
export class LocationModule extends EffectModule<LocationStateProps> {
  defaultState = {
    locations: [],
    pallets: [],
    loading: true,
    loadingProductItem: false,
    productItem: null,
    loadingProducts: false,
    products: [],
    currentCompanyUsers: [],
    synced: null,
    totalRows: 0,
    searchProps: {},
    palletHistory: [],
    palletDownloadData: []
  }

  @DefineAction() dispose$!: Observable<void>
  @DefineAction() disposeEditOrder$!: Observable<void>

  constructor(
    private readonly location: LocationService,
    private readonly product: ProductService,
    private readonly customer: CustomerService,
  ) {
    super()
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect()
  goto(action$: Observable<string>) {
    return action$.pipe(map((path) => push(path)))
  }

  @Effect({
    done: (state: LocationStateProps) => {
      return {
        ...state,
        message: '',
        type: null,
        loading: true,
        synced: null,
      }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: LocationStateProps, action: Action<any>) => {
      return {
        ...state,
        searchProps: action.payload,
      }
    },
  })
  setSearchProps(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: LocationStateProps) => {
      return {
        ...state,
        loadingProductItem: true,
        productItem: null,
      }
    },
  })
  resetProductItemLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: LocationStateProps) => {
      return {
        ...state,
        loadingProducts: true,
      }
    },
  })
  resetProductsLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: LocationStateProps) => {
      return {
        ...state,
        products: [],
      }
    },
  })
  initProducts(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: LocationStateProps, action: Action<any>) => {
      return {
        ...state,
        locations: action.payload,
        loading: false,
      }
    },
    error_message: (state: LocationStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
        loading: false,
      }
    },
  })
  getLocations(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.location.getLocations()),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: LocationStateProps, action: Action<any>) => {
      return {
        ...state,
        palletDownloadData: action.payload ? action.payload : [],
        loading: false,
      }
    },
    error_message: (state: LocationStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
        loading: false,
      }
    },
  })
  getPalletDownloadData(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) => this.location.getPalletDownload(data)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, productItem: payload, loadingProductItem: false }
    },
  })
  getProductItem(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getItems(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, currentCompanyUsers: payload.userList }
    },
  })
  getCompanyUsers(action$: Observable<any>) {
    return action$.pipe(
      switchMap(() => this.customer.getCompanyUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: LocationStateProps, action: Action<any>) => {
      return {
        ...state,
        products: action.payload,
        loadingProducts: false,
      }
    },
    error_message: (state: LocationStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
        loadingProducts: false,
      }
    },
  })
  searchProductsData(action$: Observable<string>) {
    return action$.pipe(
      switchMap((query: string) => this.location.searchProductsData(query)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: LocationStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        synced: action.payload,
      }
    },
    error_message: (state: LocationStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  syncData(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.location.syncData(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: LocationStateProps, action: Action<any>) => {
      return {
        ...state,
        pallets: action.payload,
      }
    },
    error_message: (state: LocationStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  getPalletDisplayNumbers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.location.getPalletDisplayNumbers().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: LocationStateProps, action: Action<any>) => {
      const { payload } = action
      return {
        ...state,
        loading: false,
        palletHistory: typeof payload === 'string' ? [] : payload && payload.dataList ? payload.dataList : [],
        totalRows: typeof payload === 'string' ? 0 : payload && payload.total ? payload.total : 0,
      }
    },
    error_message: (state: LocationStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  getPalletHistory(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.location.getPalletHistory(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }
}

export type LocationDispatchProps = ModuleDispatchProps<LocationModule>
