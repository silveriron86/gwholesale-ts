import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'

@Injectable()
export class LocationService {
  constructor(private readonly http: Http) { }

  static get instance() {
    return new LocationService(new Http())
  }

  getLocations() {
    return this.http.get(`/location`)
  }

  getPalletDownload (data: any) {
    return this.http.get(`/location/pallet-history-download?from=${data.from}&to=${data.to}&search=${data.search}&displayNumber=${data.displayNumber}&locationId=${data.locationId}`)
  }

  syncData(data: any) {
    return this.http.post(`/location/sync-data`, {
      body: JSON.stringify(data),
    })
  }

  searchProductsData(query: string) {
    return this.http.get(`/location/search?query=${query}`)
  }

  getPalletDisplayNumbers() {
    return this.http.get('/location/pallets')
  }

  getPalletHistory(data: any) {
    return this.http.get(`/location/pallet-history?from=${data.from}&to=${data.to}&search=${data.search}&curPage=${data.curPage}&pageSize=${data.pageSize}&displayNumber=${data.displayNumber}&locationId=${data.locationId}`)
  }
}
