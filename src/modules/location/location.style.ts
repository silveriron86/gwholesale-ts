import styled from '@emotion/styled'
import { Modal, Table, Tabs } from 'antd'
import { backgroundGreen, darkGrey, mediumGrey } from '~/common'

export const WarehouseTabs = styled(Tabs)((props) => ({
  '.ant-tabs-top-bar': {
    backgroundColor: backgroundGreen,
    height: 93,
    borderBottom: '1px solid #C4C4C4',
    marginBottom: 0,
  },
  '.ant-tabs-extra-content': {
    lineHeight: '90px',
    marginRight: '61px'
  },
  '.ant-tabs-nav': {
    display: 'flex',
    marginLeft: 115,
    '.ant-tabs-tab': {
      fontFamily: 'Museo Sans Rounded',
      fontStyle: 'normal',
      fontWeight: 600,
      fontSize: 24,
      height: 93,
      paddingTop: 30,
      marginRight: 95,
      color: '#888888',
      '&-active': {
        color: props.theme.primary,
      },
    },
    '.ant-tabs-ink-bar': {
      height: 5,
      backgroundColor: props.theme.primary,
    },
  },
  '&.mobile': {
    '.ant-tabs-top-bar': {
      height: 55,
    },
    '.ant-tabs-nav': {
      marginLeft: 70,
      '.ant-tabs-tab': {
        fontSize: 14,
        height: 55,
        paddingTop: 17,
        marginRight: 57,
      },
    },
  },
}))

export const FullCenter = styled('div')((props) => ({
  display: 'flex',
  width: '100%',
  height: '100%',
  alignItems: 'center',
  justifyContent: 'center',
}))

export const Relative = styled('div')((props) => ({
  position: 'relative',
}))

export const LocationHeader = styled('div')((props) => ({
  backgroundColor: 'white',
  width: '100%',
  height: 93,
  display: 'flex',
  alignItems: 'center',
  paddingLeft: 198,
  // paddingTop: 28,
  boxShadow: '0px 12px 14px -6px rgba(0,0,0,0.15)',
  fontFamily: 'Museo Sans Rounded',
  fontWeight: 600,
  fontSize: 36,
  color: props.theme.primary,
  position: 'absolute',
  top: -93,
  '&.first-tab': {
    paddingTop: 10,
  },
  '.ant-input-search': {
    borderBottom: '1px solid #A2A2A2 !important',
    paddingRight: 10,
    width: 971,
    marginLeft: -65,
    paddingBottom: 5,
    marginTop: 15,
    input: {
      border: 'none !important',
      outline: 'none !important',
      boxShadow: 'none !important',
      fontFamily: 'Museo Sans Rounded',
      fontWeight: 400,
      fontSize: 25,
      '&::placeholder': {
        fontFamily: 'Museo Sans Rounded',
        fontWeight: 400,
        fontSize: 25,
        color: mediumGrey,
      },
    },
    '.ant-input-group-addon': {
      '.ant-btn-primary': {
        backgroundColor: 'white',
        color: props.theme.primary,
        borderWidth: 0,
        border: 'none !important',
        outline: 'none !important',
        boxShadow: 'none !important',
        display: 'flex',
        alignItems: 'center',
        svg: {
          width: 22,
          height: 22,
        },
        span: {
          marginLeft: 17,
          fontSize: '24px !important',
        },
      },
    },
  },

  '&.mobile': {
    height: 55,
    paddingLeft: 70,
    fontSize: 16,
    '.ant-input-search': {
      paddingRight: 10,
      width: 550,
      marginLeft: 0,
      paddingBottom: 0,
      marginTop: 0,
      input: {
        fontSize: 15,
        '&::placeholder': {
          fontSize: 15,
        },
      },
      '.ant-input-group-addon': {
        '.ant-btn-primary': {
          svg: {
            width: 16,
            height: 16,
          },
          span: {
            marginLeft: 17,
            fontSize: '16px !important',
          },
        },
      },
    },
    '&.first-tab': {
      paddingTop: 3,
    },
  },
}))

export const LocationTable = styled(Table)((props) => ({
  '&.second': {
    '&.ant-table-wrapper': {
      '.ant-table-body': {
        '.ant-table-thead': {
          tr: {
            th: {
              '&:first-of-type': {
                paddingLeft: '95px !important',
              },
            },
          },
        },
      },
    },
  },
  '&.colorable': {
    'th, td': {
      '&:nth-child(5)': {
        borderRight: '0 !important',
      },
    },
    '.ant-table-body': {
      tr: {
        td: {
          '&:nth-child(4)': {
            padding: '0 21px',
          }
        }
      }
    },
  },
  '&.ant-table-wrapper': {
    marginTop: 93,
    backgroundColor: 'white',
    padding: 0,
    '.ant-table-body': {
      '.ant-table-thead': {
        tr: {
          th: {
            paddingLeft: 31,
            paddingTop: 35,
            paddingBottom: 17,
            background: 'transparent',
            borderBottom: '1px solid #C1C3C1',
            borderRight: '1px solid #C1C3C1',
            '.ant-table-column-title': {
              fontFamily: 'Museo Sans Rounded',
              fontWeight: 600,
              fontSize: 20,
              color: darkGrey,
            },
            '&:first-of-type': {
              paddingLeft: '58px !important',
            },
            '&:nth-child(7)': {
              borderRight: 0,
            },
            '&:last-child': {
              borderRight: 0,
            },
          },
        },
      },
      '.ant-table-tbody': {
        tr: {
          td: {
            background: 'transparent',
            fontFamily: 'Museo Sans Rounded',
            fontWeight: 400,
            fontSize: 24,
            color: 'black',
            borderBottom: '1px solid #C1C3C1',
            borderRight: '1px solid #C1C3C1',
            padding: '21px 16px',
            paddingLeft: 31,
            '.ant-input': {
              fontFamily: 'Museo Sans Rounded',
              fontWeight: 400,
              fontSize: 24,
              borderWidth: '0 !important',
              backgroundColor: 'transparent',
              height: 50,
              padding: '0 11px',
              lineHeight: '50px',
              color: 'black',
              '&:focus': {
                borderWidth: '1px !important',
              },
            },
            '.progress': {
              fontWeight: 600,
            },
            '.assign': {
              fontWeight: 600,
              color: '#2880B9',
            },
            '.clear': {
              fontWeight: 600,
              color: '#198333',
            },
            '.close-btn': {
              position: 'absolute',
              right: 18,
              fontSize: 21,
              marginTop: 3,
            },
            '&:last-child': {
              borderRight: 0,
            },
            '&:nth-child(7)': {
              borderRight: 0,
            },
            '&:first-of-type': {
              paddingLeft: 10,
            },
          },
          '&.assign-row': {
            backgroundColor: 'rgba(220, 244, 255, 0.66)',
          },
          '&.clear-row': {
            backgroundColor: 'rgba(233, 243, 235, 0.65)',
          },
          '&.error-row': {
            backgroundColor: '#fdc9ca65',
          },
        },
      },
    },
  },
  '&.mobile': {
    '.ant-table-content': {
      marginTop: -36,
    },
    '.ant-table-body': {
      '.ant-table-thead': {
        tr: {
          th: {
            paddingLeft: 31,
            paddingTop: 21,
            paddingBottom: 10,
            '.ant-table-column-title': {
              fontSize: 12,
            },
            '&:first-of-type': {
              paddingLeft: '16px !important',
            },
          },
        },
      },
      '.ant-table-tbody': {
        tr: {
          td: {
            fontSize: 15,
            padding: '12px 8px 6px',
            paddingLeft: 31,
            '.ant-input': {
              fontSize: 15,
              height: 30,
              padding: '0 11px',
              lineHeight: '30px',
            },
            '.close-btn': {
              fontSize: 12,
            },
            '&:nth-child(4)': {
              padding: '8px 21px',
            },
            '&:nth-child(5)': {
              padding: '8px 21px',
            },
          },
        },
      },
    },
    '&.colorable': {
      '.ant-table-body': {
        '.ant-table-thead tr th': {
          paddingLeft: 16,
        }
      },
      '.ant-table-tbody': {
        tr: {
          td: {
            paddingLeft: '16px !important',
            paddingTop: 6
          }
        }
      }
    },
    '&.second': {
      '.ant-table-body': {
        '.ant-table-thead tr th': {
          paddingLeft: 16,
          '&:first-of-type': {
            paddingLeft: '74px !important'
          }
        },
        '.ant-table-tbody': {
          tr: {
            td: {
              paddingLeft: '16px !important',
              '&:first-of-type': {
                div: {
                  paddingLeft: '58px !important',
                }
              },
            }
          }
        }
      }
    }
  },
}))

export const ModalWrapper = styled(Modal)((props) => ({
  '&': {
    '.ant-modal-body': {
      fontFamily: 'Museo Sans Rounded',
      padding: '38px 40px 27px',
      '.left-side': {
        width: '30%', //361,
        float: 'left',
        '.avatar-wrapper': {
          display: 'flex',
          justifyContent: 'center',
          marginTop: 10,
          marginBottom: 56,
        },
        '.ant-row': {
          paddingBottom: 10,
          paddingTop: 12,
          borderBottom: '1px solid #C4C4C4',
        },
      },
      '.right-side': {
        width: '70%', //1009,
        float: 'right',
        '.label': {
          marginLeft: 14,
          marginTop: 30,
        },
        '.value': {
          marginLeft: 20,
        },
      },
      '.label': {
        fontWeight: 600,
        fontSize: 18,
        lineHeight: '20.7px',
        color: '#888',
      },
      '.green-value': {
        fontWeight: 600,
        fontSize: 18,
        lineHeight: '20.7px',
        color: '#26AC5F',
      },
      '.value': {
        fontWeight: 400,
        fontSize: 24,
        lineHeight: '31px',
        color: mediumGrey,
        marginTop: 9,
      },
      '.specifications': {
        backgroundColor: props.theme.lighter,
        padding: '27px 25px',
        borderRadius: 10,
        '.ant-checkbox-wrapper': {
          fontSize: 20,
          fontWeight: 400,
          color: darkGrey,
          float: 'right',
          cursor: 'text',
          '.ant-checkbox': {
            cursor: 'text',
            input: {
              cursor: 'text',
            },
          },
        },
      },
      h1: {
        fontSize: 28,
        color: 'black',
        lineHeight: '32.2px',
        fontWeight: 600,
        height: 64,
      },
      h3: {
        color: props.theme.main,
        fontWeight: 400,
        fontSize: 28,
        marginBottom: 0,
      },
      '.details': {
        marginTop: 21,
        padding: '22px 25px',
        borderRadius: 10,
        border: '1px solid #C4C4C4',
      },
    },
  },
}))

export const ReportPageContainer = styled('div')({
  padding: '50px 20px 30px 50px'
})

export const HistoryHeaderWrapper = styled('div')({
  marginBottom: 30,
})

export const HeaderElement = styled('div')({
  margin: '0 10px',
  '& span.ant-calendar-picker-input': {
    height: 40
  },
  '&>div.ant-select': {
    width: 250,
    '&>div.ant-select-selection--single': {
      height: 40,
      '& div.ant-select-selection__rendered': {
        height: 40,
        '& div.ant-select-selection-selected-value': {
          padding: 4
        }
      }
    }
  }
})
