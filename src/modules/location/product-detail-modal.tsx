import React from 'react'
// import { withTheme } from 'emotion-theming'
import { Row, Col, Avatar } from 'antd'
import { ModalWrapper } from './location.style'
import { ThemeCheckbox, ThemeSpin } from '../customers/customers.style'
import { formatNumber, getFileUrl } from '~/common/utils'
import Moment from 'react-moment'
import { isMobile } from 'react-device-detect'

interface ProductDetailModalProps {
  visible: boolean
  onClose: Function
  loading: boolean
  item: any
  companyUsers: any[]
}

export default class ProductDetailModal extends React.PureComponent<ProductDetailModalProps> {
  onChangeRatio = (e) => {
    e.preventDefault()
  }
  render() {
    const { visible, onClose, loading, item, companyUsers } = this.props
    let productManager = ''

    if (item) {
      const user: any = companyUsers.find((u: any) => u.userId === item.managerId)
      if (user) {
        productManager = `${user.firstName} ${user.lastName}`
      }
    }

    return (
      <ModalWrapper
        wrapClassName={isMobile ? 'mobile' : ''}
        centered
        title=""
        visible={visible}
        footer={null}
        closable={false}
        width={1493}
        onCancel={onClose}
      >
        <ThemeSpin spinning={loading}>
          <div className="left left-side">
            <div className="avatar-wrapper">
              <Avatar size={isMobile ? 200 : 280} src={item ? getFileUrl(item.cover, false) : ''} />
            </div>
            <div className="small-label">PRODUCT NAME</div>
            <h1>{item ? item.variety : '--'}</h1>

            <Row style={{ marginTop: isMobile ? 10 : 33 }}>
              <Col md={14} className="label">
                SKU
              </Col>
              <Col md={10} className="green-value">
                {item ? item.sku : '--'}
              </Col>
            </Row>
            <Row>
              <Col md={14} className="label">
                units on hand
              </Col>
              <Col md={10} className="green-value">
                {item && item.onHandQty > 0 ? formatNumber(item.onHandQty, 2) : '--'}
              </Col>
            </Row>
            <Row>
              <Col md={14} className="label">
                UNITS COMMITTED
              </Col>
              <Col md={10} className="green-value">
                {item && item.commitedQty > 0 ? formatNumber(item.commitedQty, 2) : '--'}
              </Col>
            </Row>
            <Row>
              <Col md={14} className="label">
                UNITS INCOMING
              </Col>
              <Col md={10} className="green-value">
                {item && item.incomingQty > 0 ? formatNumber(item.incomingQty, 2) : '--'}
              </Col>
            </Row>
            <Row>
              <Col md={14} className="label">
                LAST ADJUSTED
              </Col>
              <Col md={10} className="green-value">
                {item && item.updatedDate ? <Moment format="MM/DD/YYYY" date={item.updatedDate} /> : '--'}
              </Col>
            </Row>
          </div>
          <div className="right right-side">
            <div className="specifications">
              <h3>Specifications</h3>
              <Row>
                <Col md={8}>
                  <div className="label">PRODUCT SKU</div>
                  <div className="value">{item ? item.sku : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">PRODUCT NAME</div>
                  <div className="value">{item ? item.variety : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">CATEGORY</div>
                  <div className="value">
                    {item && item.wholesaleCategory && item.wholesaleCategory.wholesaleSection
                      ? item.wholesaleCategory.wholesaleSection.name
                      : '--'}
                  </div>
                </Col>
                <Col md={8}>
                  <div className="label">PRICING UOM</div>
                  <div className="value">{item ? item.baseUOM : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">Inventory UOM</div>
                  <div className="value">{item ? item.inventoryUOM : '--'}</div>
                </Col>
                {/* <Col md={8}>
                  <div className="label">Pricing to inventory ratio</div>
                  <div className="value">
                    {item ? item.ratioUOM : '--'}{' '}
                    <ThemeCheckbox onChange={this.onChangeRatio} checked={item ? item.constantRatio : false}>
                      Constant Ratio
                    </ThemeCheckbox>
                  </div>
                </Col> */}
              </Row>
            </div>
            <div className="details">
              <h3>Details</h3>
              <Row>
                <Col md={8}>
                  <div className="label">ORIGIN</div>
                  <div className="value">{item ? item.origin : '--'}</div>
                </Col>
                <Col md={8}>
                  {/* <div className="label">BRAND</div>
                  <div className="value">{item ? item.brand : '--'}</div> */}
                </Col>
                <Col md={8}>
                  <div className="label">Family</div>
                  <div className="value">{item ? item.family : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">Temperature</div>
                  <div className="value">{item ? item.temperature : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">Shelf Life (days)</div>
                  <div className="value">{item ? item.shelfLife : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">PRODUCT MANAGER</div>
                  <div className="value">{productManager ? productManager : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">REORDER LEVEL</div>
                  <div className="value">{item && item.reorderLevel ? item.reorderLevel : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">RESTOCKING LEAD TIME (DAYS)</div>
                  <div className="value">{item ? item.restockingTime : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">PIECES / UNIT</div>
                  <div className="value">{item && item.unitPerLayer ? item.unitPerLayer : '--'}</div>
                </Col>
                <Col md={8}>
                  <div className="label">UNITS PER PALLET</div>
                  <div className="value">{item && item.layersPerPallet ? item.layersPerPallet : '--'}</div>
                </Col>
              </Row>
            </div>
          </div>
          <div className="clearfix" />
        </ThemeSpin>
      </ModalWrapper>
    )
  }
}

// export default withTheme(ProductDetailModal)
