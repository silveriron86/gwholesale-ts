import { Module, EffectModule, ModuleDispatchProps, Effect, StateObservable, DefineAction } from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { map, switchMap, takeUntil, catchError, endWith } from 'rxjs/operators'
import { Action } from 'redux-actions'
import { checkError, responseHandler, getFileUrl } from '~/common/utils'
import { ManufactureType, Station, Order, StationSetting } from '~/schema'
import { GlobalState } from '~/store/reducer'
import { DeliveryService } from '~/modules/delivery/delivery.service'
import { CustomerService } from '~/modules/customers/customers.service'
import { ManufacturingService } from './manufacturing.service'
import { MessageType } from '~/components'
import { ProductService } from '../product/product.service'
import { SettingService } from '../setting/setting.service'
import moment from 'moment'

export interface ManufacturingStateProps {
  loading: boolean
  workOrders: Order[]
  specialCutWorkOrders: Order[]
  unscheduleWorkOrders: Order[]
  incompletedWorkOrders: Order[]
  stations: Station[]
  manufactureTypes: ManufactureType[]
  error: string
  message: string
  description: string
  type: MessageType | null
  workers: any[]
  galleryDocuments: any[]
  newWorkOrderId: number
  pro_stationId: number
  pro_from: string
  pro_to: string
  drivers: any[]
  customers: any[]
  companyName: string
  logo: string
}

@Module('orders')
export class ManufacturingModule extends EffectModule<ManufacturingStateProps> {
  defaultState = {
    loading: false,
    workOrders: [],
    specialCutWorkOrders: [],
    incompletedWorkOrders: [],
    stations: [],
    manufactureTypes: [],
    workers: [],
    error: '',
    message: '',
    description: '',
    type: null,
    galleryDocuments: [],
    newWorkOrderId: -1,
    pro_stationId: -1,
    pro_from: moment().format('YYYY-MM-DD'),
    pro_to: moment().format('YYYY-MM-DD'),
    drivers: [],
    customers: [],
    companyName: '',
    logo: 'default',
  }

  @DefineAction() dispose$!: Observable<void>
  @DefineAction() disposeEditOrder$!: Observable<void>

  constructor(
    private readonly manufacturing: ManufacturingService,
    private readonly product: ProductService,
    private readonly driver: DeliveryService,
    private readonly customer: CustomerService,
    private readonly setting: SettingService,
  ) {
    super()
  }

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any[]>) => {
      return { ...state, customers: payload }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        customers: [],
        hasError: true,
      }
    },
  })
  getCustomerList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser.userId).pipe(
          switchMap((data: any) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps) => {
      return {
        ...state,
        message: '',
        type: null,
        loading: true,
      }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        ...action.payload,
      }
    },
  })
  setProcessingListHeader(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        manufactureTypes: action.payload.body.data,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        manufactureTypes: [],
        hasError: true,
      }
    },
  })
  getAllManufactureTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getAllManufactureTypes().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        drivers: action.payload.body.data,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        drivers: [],
        hasError: true,
      }
    },
  })
  getDrivers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.driver.getAllDrivers().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        stations: action.payload.body.data,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        stations: [],
        hasError: true,
      }
    },
  })
  getAllStations(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getAllStations().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps) => {
      return {
        ...state,
        message: 'Added a station with name successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  addStationByName(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((stationName: string) =>
        this.manufacturing.addStationByName(stationName).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        newWorkOrderId: payload.body.data,
        message: 'Created work order successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  createWorkOrderWithItemId(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((arg: any) =>
        this.manufacturing.createWorkOrderWithItemId(arg.orderId, arg.data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        newWorkOrderId: payload.body.data,
        message: 'Created work order successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  createWorkOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.createWorkOrder(data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps) => {
      return {
        ...state,
        message: 'Updated the station name successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateStationName(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.updateStationName(data.stationId, data.name).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        workers: action.payload.body.data,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        workers: [],
        hasError: true,
      }
    },
  })
  getAllWorkers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getAllWorkers().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps) => {
      return {
        ...state,
        message: 'Assigned the work to the station successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  assignWorkerToStation(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.assignWorkerToStation(data.stationId, data.workerId).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps) => {
      return {
        ...state,
        message: 'Unassigned the work to the station successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  unAssignWorkerToStation(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.unAssignWorkerToStation(data.stationId, data.workerId).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps) => {
      return {
        ...state,
        message: 'Assigned the manufacturing type to the station successfully',
        type: MessageType.SUCCESS,
        loading: false,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
        loading: false,
      }
    },
  })
  assignManufactureTypeToStation(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.assignManufactureTypeToStation(data.stationId, data.manufactureTypeId).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps) => {
      return {
        ...state,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  unassignManufactureTypeToStation(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.unassignManufactureTypeToStation(data.stationId, data.manufactureTypeId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllStations)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        workOrders: action.payload.body.data,
        hasError: false,
        loading: false,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        workOrders: [],
        hasError: true,
        loading: false,
      }
    },
  })
  getWorkOrderListByStationIdAndDate(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing
          .getWorkOrderListByStationIdAndDate(data.stationId, data.fromDate, data.toDate, data.filter)
          .pipe(
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        unscheduleWorkOrders: action.payload.body.data,
        hasError: false,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        unscheduleWorkOrders: [],
        hasError: true,
      }
    },
  })
  getUnscheduleWorkOrderList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getUnscheduleWorkOrderList().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        incompletedWorkOrders: action.payload.body.data,
        hasError: false,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        incompletedWorkOrders: [],
        hasError: true,
      }
    },
  })
  getIncompletedWorkOrderList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getIncompletedWorkOrderList().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        specialCutWorkOrders: action.payload.body.data,
        hasError: false,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        specialCutWorkOrders: [],
        hasError: true,
      }
    },
  })
  getWorkOrderListByDate(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.getWorkOrderListByDate(data.fromDate, '').pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, galleryDocuments: payload }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        galleryDocuments: [],
        hasError: true,
      }
    },
  })
  getGalleryDocuments(action$: Observable<any>) {
    return action$.pipe(
      switchMap((productId) => {
        return this.product.getGalleryDocuments(productId).pipe(
          switchMap((data) => of(this.formatProductDocuments(data))),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return { ...state }
    },
  })
  updateSettingsDisplayOrder(action$: Observable<StationSetting[]>) {
    return action$.pipe(
      switchMap((data) => {
        return this.manufacturing.updateStationSettingDisplayOrder(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
        )
      }),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateWO(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.updateWorkOrderInfo(data.orderId, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getWorkOrderListByStationIdAndDate)({
                  stationId: state$.value.manufacturing.pro_stationId,
                  fromDate: state$.value.manufacturing.pro_from,
                  toDate: state$.value.manufacturing.pro_to,
                }),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateWorkOrderDisplayNumber(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.updateWorkOrderDisplayNumber(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, action: Action<any>) => {
      if (action.payload.userSetting) {
        localStorage.setItem('isEnableCashSales', action.payload.userSetting.company.isEnableCashSales)
        return {
          ...state,
          companyName: action.payload.userSetting.companyName,
          logo: action.payload.userSetting.imagePath ? `${process.env.AWS_PUBLIC}/${action.payload.userSetting.imagePath}` : 'default',
        }
      } else {
        return {
          ...state,
        }
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getUserSetting(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  formatProductDocuments(datas: any[]): Document[] {
    return datas.map((data) => ({
      uid: data.id,
      url: getFileUrl(data.url, false),
      status: 'done',
    }))
  }
}

export type ManufacturingDispatchProps = ModuleDispatchProps<ManufacturingModule>
