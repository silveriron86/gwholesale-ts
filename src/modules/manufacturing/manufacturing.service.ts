import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import { ManufactureType, StationSetting } from '~/schema'

@Injectable()
export class ManufacturingService {
  constructor(private readonly http: Http) {}

  addManufactureType(name: string) {
    return this.http.post<any>(`/manufacture/manufacture-type`, {
      body: JSON.stringify({ name }),
    })
  }

  deleteManufactureType(id: number) {
    return this.http.delete<any>(`/manufacture/manufacture-type/${id}`)
  }

  getAllManufactureTypes() {
    return this.http.get<ManufactureType[]>(`/manufacture/manufacture-types`)
  }

  addStationByName(name: string) {
    return this.http.post<any>('/manufacture/station', {
      body: JSON.stringify({ name }),
    })
  }

  getManufactureTypesByStationId(stationId: number) {
    return this.http.get<any[]>(`/manufacture/station/${stationId}/manufacture-types`)
  }

  updateStationName(stationId: number, name: string) {
    return this.http.put(`/manufacture/station/${stationId}`, {
      body: JSON.stringify({ name }),
    })
  }

  assignManufactureTypeToStation(stationId: number, manufactureTypeId: number) {
    return this.http.post<any>(`/manufacture/station/${stationId}/manufacture-type/${manufactureTypeId}`)
  }

  unassignManufactureTypeToStation(stationId: number, manufactureTypeId: number) {
    return this.http.put<any>(`/manufacture/station/${stationId}/manufacture-type/${manufactureTypeId}`)
  }

  assignWorkerToStation(stationId: number, workerId: number) {
    return this.http.post<any>(`/manufacture/station/${stationId}/worker/${workerId}`)
  }

  unAssignWorkerToStation(stationId: number, workerId: number) {
    return this.http.put<any>(`/manufacture/station/${stationId}/worker/${workerId}`)
  }

  assignWorkerToWO(userId: number, orderId: number) {
    return this.http.post<any>(`/manufacture/order/${orderId}/worker/${userId}`)
  }

  unAssignWorkerFromWO(userId: number, orderId: number) {
    return this.http.put<any>(`/manufacture/order/${orderId}/worker/${userId}`)
  }

  getAllStations() {
    return this.http.get<any[]>(`/manufacture/stations`)
  }

  getAllWorkers() {
    return this.http.get<any[]>(`/manufacture/workers`)
  }

  // create work order in sale page cart view with order Item id
  createWorkOrderWithItemId(orderId: number, data: any) {
    return this.http.post<any>(`/manufacture/order/${orderId}`, {
      body: JSON.stringify(data),
    })
  }

  // create work order in process list
  createWorkOrder(data: any) {
    return this.http.post<any>('/manufacture/order', {
      body: JSON.stringify(data),
    })
  }

  // Update work order info by work order id
  updateWorkOrderInfo(workOrderId: number, data: any) {
    return this.http.put(`/manufacture/order/${workOrderId}`, {
      body: JSON.stringify(data),
    })
  }

  // Add work order item with item id  (type 1 in, 2 out)
  addWorkOrderItemByProductId(workOrderId: number, itemId: number, type: number) {
    return this.http.post<any>(`/manufacture/order/${workOrderId}/item/${itemId}`, {
      body: JSON.stringify({ type: type }),
    })
  }

  // Add work order item with order item id  (type 1 in, 2 out)
  addWorkOrderItemByOrderItemId(workOrderId: number, itemId: number, type: number) {
    return this.http.post<any>(`/manufacture/order/${workOrderId}/order-item/${itemId}`, {
      body: JSON.stringify({ type: type }),
    })
  }

  updateWorkOrderItem(workOrderItemId: number, data: any) {
    return this.http.put(`/manufacture/order-item/${workOrderItemId}?timestamp=${new Date().getTime()}`, {
      body: JSON.stringify(data),
    })
  }

  deleteWorkOrderItem(orderItemid: number) {
    return this.http.delete<any>(`/manufacture/order-item/${orderItemid}`)
  }

  getWorkOrderInfo(workOrderId: number) {
    return this.http.get<any[]>(`/manufacture/order/${workOrderId}`)
  }

  // Get work order list by staionId and request Date(process list menu)
  getWorkOrderListByStationIdAndDate(stationId: number, fromDate: string, toDate: string, filter: any = {}) {
    // return this.http.post<any[]>(`/manufacture/orders?stationId=${stationId}&from=2020-12-01&to=2021-04-20`, {
    //   body: JSON.stringify(filter),
    // })
    return this.http.post<any[]>(`/manufacture/orders?stationId=${stationId}&from=${fromDate}&to=${toDate}`, {
      body: JSON.stringify(filter),
    })
  }

  // Get work order list by request Date(special cut request menu)
  getWorkOrderListByDate(fromDate: string, toDate: string) {
    const to = toDate !== '' ? `&to=${toDate}` : ''
    return this.http.get<any[]>(`/manufacture/orders/special-cut-request?from=${fromDate}${to}`)
  }

  // Get unschedule work order list by request Date(special cut request menu)
  getUnscheduleWorkOrderList() {
    return this.http.get<any[]>('/manufacture/orders/special-cut-request/unschedule')
  }

  getIncompletedWorkOrderList() {
    return this.http.get<any[]>('/manufacture/orders/special-cut-request/incomplete')
  }

  duplicateWorkOrder(workOrderId: number) {
    return this.http.get<any[]>(`/manufacture/duplicate-order/${workOrderId}`)
  }

  getAllWorkOrderTemplates() {
    return this.http.get<any[]>('/manufacture/order/templates')
  }

  // Set work order as a wo template(1 set as template, 0 cancel )
  setWorkOrderasTemplate(workOrderId: number, isTemplate: number) {
    return this.http.put(`/manufacture/order/${workOrderId}/as-template`, {
      body: JSON.stringify({ isTemplate }),
    })
  }

  getAllAuditLog(orderId: number) {
    return this.http.get<any[]>(`/manufacture/order/${orderId}/logs`)
  }

  // Duplicate wo (apply template)
  applyTemplate(currentOrderId: number, templateOrderId: number) {
    return this.http.get<any[]>(`/manufacture/order/${currentOrderId}/apply-template/${templateOrderId}`)
  }

  getProcessingResourceMatrix(queryParam: number, fromDate: string, toDate: string) {
    return this.http.get<any[]>(`/manufacture/order-matrix/?queryParam=${queryParam}&from=${fromDate}&to=${toDate}`)
  }

  updateStationSettingDisplayOrder(settings: StationSetting[]) {
    return this.http.post(`/manufacture/update-settings-order`, {
      body: JSON.stringify(settings),
    })
  }
  updateWorkOrderDisplayNumber(displayOrderData: any) {
    return this.http.post(`/inventory/update-display-order`, {
      body: JSON.stringify(displayOrderData),
    })
  }
}
