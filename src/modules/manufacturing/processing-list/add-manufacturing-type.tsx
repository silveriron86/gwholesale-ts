import * as React from 'react'
import { ThemeIcon, ThemeOutlineButton } from '~/modules/customers/customers.style'
import { ManufactureType, StationSetting } from '~/schema'
import { Menu, Dropdown, Icon } from 'antd'
import { AddButtonWrap } from '~/modules/customers/components/customer-detail/customer-detail.style'
import { map } from 'lodash'

interface AddManufacturingTypeProps {
  manufactureTypes: ManufactureType[]
  wholesaleStationSetting: StationSetting[]
  onAssign: Function
}

export class AddManufacturingType extends React.PureComponent<AddManufacturingTypeProps> {
  onAssignType = (type: ManufactureType) => {
    this.props.onAssign(type)
  }

  render() {
    const { manufactureTypes, wholesaleStationSetting } = this.props
    const filtedTypes = manufactureTypes.filter((type) => {
      return map(wholesaleStationSetting, 'wholesaleManufactureType.id').indexOf(type.id) < 0
    })
    const AddToOverlay =
      filtedTypes.length > 0 ? (
        <Menu>
          {filtedTypes.map((p) => (
            <Menu.Item onClick={() => this.onAssignType(p)} key={`manufacture-type-${p.id}`}>
              {p.name}
            </Menu.Item>
          ))}
        </Menu>
      ) : (
        <Menu>
          {['N/A'].map((p) => (
            <Menu.Item key={p}>{p}</Menu.Item>
          ))}
        </Menu>
      )

    return (
      <Dropdown overlay={AddToOverlay}>
        <ThemeOutlineButton
          type="ghost"
          style={{
            width: '240px',
            height: 42,
          }}
        >
          <AddButtonWrap style={{ height: 42 }}>
            <div>
              <Icon type="plus" />
              <span>Add Manufacturing Type</span>
            </div>
            <ThemeIcon type="caret-down" />
          </AddButtonWrap>
        </ThemeOutlineButton>
      </Dropdown>
    )
  }
}

export default AddManufacturingType
