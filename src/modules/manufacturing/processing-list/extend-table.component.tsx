import React, { FC } from 'react'
import { Table } from 'antd'
import { StyleExpand } from './extend-table.style'

interface IProps {
  inData: any[]
  outData: any[]
  inColumns: any[]
  outColumns: any[]
  instructions?: string
}

const ExpandTable: FC<IProps> = (props) => {
  const { inData, outData, inColumns, outColumns, instructions } = props
  return (
    <StyleExpand className="expand">
      <h5>Instructions</h5>
      <p>{instructions ?? ''}</p>
      <h5 className="table-title">Materials In</h5>
      <Table className="details" dataSource={inData} columns={inColumns} pagination={false} />
      <h5 className="table-title">Materials Out</h5>
      <Table className="details" dataSource={outData} columns={outColumns} pagination={false} />
    </StyleExpand>
  )
}

export default ExpandTable
