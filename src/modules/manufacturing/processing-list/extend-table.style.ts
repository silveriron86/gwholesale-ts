import styled from '@emotion/styled'

export const StyleExpand = styled('div')({
  padding: '10px 20px 20px 200px',

  h5: {
    fontSize: '12px',
    fontWeight: 600,
    color: '#000000',

    '&.table-title': {
      marginTop: '20px',
    },
  },

  p: {
    fontSize: '12px',
    color: '#22282A',
  },
})
