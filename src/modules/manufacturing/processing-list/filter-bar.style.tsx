import styled from '@emotion/styled'
export const StyleFilterBar: any = styled.div((props) => ({
  textAlign: 'left',
  paddingLeft: '90px',
  marginBottom: '30px',

  '& .button-wrapper': {
    marginTop: '13px',
    marginBottom: '20px',
  },
}))
