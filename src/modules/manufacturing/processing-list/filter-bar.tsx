import React, { FC, useState, useEffect } from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { ManufacturingDispatchProps, ManufacturingModule, ManufacturingStateProps } from '~/modules/manufacturing'
import { FilterTags, FilterButtons, FilterOptions, FilterData } from '~/components/filter-bar'
import { StyleFilterBar } from './filter-bar.style'
import { produce } from 'immer'

type FilterBarProps = ManufacturingDispatchProps &
  ManufacturingStateProps & {
    setFilter: any
    filter: FilterData
  }

const FilterBar: FC<FilterBarProps> = (props) => {
  const [showModal, setShowModal] = useState<boolean>(false)
  const { getDrivers, drivers, getCustomerList, customers, setFilter, filter } = props

  useEffect(() => {
    getDrivers()
    getCustomerList()
  }, [])

  useEffect(() => {
    filterOptions = produce<FilterOptions>(filterOptions, (draft) => {
      draft.driver.filters = drivers.map((driver: any) => ({
        label: `${driver.firstName} ${driver.lastName}`,
        value: driver.userId,
      }))
    })
  }, [drivers])

  useEffect(() => {
    filterOptions = produce<FilterOptions>(filterOptions, (draft) => {
      draft.customer.filters = customers.map((c: any) => ({
        label: c?.clientCompany?.companyName,
        value: c.clientId,
      }))
    })
  }, [customers])

  const resetFilter = () => {
    setFilter(
      produce<FilterData>(filter, (draft) => {
        draft.status = initFiltersData.status
        draft.customer = initFiltersData.customer
        draft.driver = initFiltersData.driver
      }),
    )
    showModal && setShowModal(false)
  }

  const handleClearTag = ({ key, value }: { key: string; value: string | number }) => {
    setFilter(
      produce<FilterData>(filter, (draft) => {
        draft[key] = draft[key].filter((item: string | number) => item !== value)
      }),
    )
  }

  return (
    <StyleFilterBar>
      <div className="button-wrapper">
        <FilterButtons
          title="Filter Purchase Orders"
          open={showModal}
          setShowModal={setShowModal}
          filterOptions={filterOptions}
          filterData={filter}
          setFiltersData={setFilter}
          resetFilter={resetFilter}
        />
      </div>
      <div className="tags-wrapper">
        <FilterTags tags={formatTags(filter)} onClose={handleClearTag} />
      </div>
    </StyleFilterBar>
  )
}

const mapState = (state: GlobalState) => state.manufacturing

export default connect(ManufacturingModule)(mapState)(FilterBar)

interface ITag {
  label: string
  key: string
  value: string | number
}

const formatTags = (data: FilterData): ITag[] => {
  const result: ITag[] = []

  Object.keys(data).forEach((key: string) => {
    data[key].forEach((value: string | number) => {
      let label = filterOptions[key].filters.find((item) => item.value === value)?.label
      label = `${label} ${filterOptions[key].tagEnd}`
      result.push({
        label,
        key: key,
        value: value,
      })
    })
  })

  return result
}

export const initFiltersData: FilterData = {
  status: [],
  customer: [],
  driver: [],
}

let filterOptions: FilterOptions = {
  status: {
    title: 'STATUS',
    subTitle: 'Stock Status',
    tagEnd: 'status',
    filters: [
      { label: 'New Order', value: 'REQUEST' },
      { label: 'Scheduled', value: 'SCHEDULED' },
      { label: 'In Progress', value: 'WIP' },
      { label: 'Complete', value: 'COMPLETED' },
      { label: 'Shipped', value: 'SHIPPED' },
    ],
  },
  customer: {
    title: 'CUSTOMER',
    subTitle: 'Customers',
    tagEnd: 'Customer',
    filters: [],
  },
  driver: {
    title: 'DRIVER',
    subTitle: 'Drivers',
    tagEnd: 'Driver',
    filters: [],
  },
}
