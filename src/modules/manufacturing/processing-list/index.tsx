import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import { ManufacturingDispatchProps, ManufacturingModule, ManufacturingStateProps } from '~/modules/manufacturing'
import {
  ThemeButton,
  ThemeIconButton,
  InputLabel,
  ThemeSelect,
  floatRight,
  ThemeInput,
} from '~/modules/customers/customers.style'
import { Row, Col, Select, Icon, Dropdown, Button, Menu, Modal } from 'antd'
import { ClickParam } from 'antd/lib/menu'
import {
  ProcessingListHeader,
  ReactWrapper,
  ThemeRangePicker,
  ProcessingListItem,
  NavHeaderButtonsWrapper,
  TableTopBar,
} from './processing-list.style'
import ListTable from './table'
import { ValueSpan } from '../work-orders/work-orders.style'
import { Icon as IconSvg } from '~/components/icon/'
import moment from 'moment'
import WorkOrderModal from './modals/wo-modal'
import AssignEmployeeModal from './modals/assign-modal'
import AddStationModal from './modals/add-station-modal'
import AddManufacturingType from './add-manufacturing-type'
import { Station, ManufactureType, Order, StationSetting } from '~/schema'
import { RouteComponentProps } from 'react-router'
import { History } from 'history'
import { RouteProps } from 'react-router'
import { useState, useEffect, useCallback } from 'react'
import { cloneDeep, orderBy } from 'lodash'
import update from 'immutability-helper'
import { printWindow } from '~/common/utils'
import PrintProcessingModal from './print/print-modal'
import FilterBar, { initFiltersData } from './filter-bar'
import { FilterData } from '~/components/filter-bar'
import { StatusSpan, StyleModal } from '../work-orders/work-orders.style'
import qs from 'qs'
import { produce } from 'immer'
import { ThemeSpin } from '../../customers/customers.style'
import { fullButton } from '../../customers/sales/_style'
import { PrintPickSheet } from '../../customers/sales/cart/print'

type ProcessingListProps = ManufacturingDispatchProps &
  ManufacturingStateProps &
  RouteComponentProps &
  RouteProps & {
    theme: Theme
    history: History
  }

let timer: any
const interval = 30000

export const ProcessingList: React.FC<ProcessingListProps> = (props) => {
  const {
    getAllManufactureTypes,
    getAllStations,
    getAllWorkers,
    getWorkOrderListByStationIdAndDate,
    updateStationName,
    assignWorkerToStation,
    unAssignWorkerToStation,
    addStationByName,
    assignManufactureTypeToStation,
    updateSettingsDisplayOrder,
    setProcessingListHeader,
    updateWO,
    resetLoading,
    updateWorkOrderDisplayNumber,
    stations,
    workOrders,
    manufactureTypes,
    workers,
    history,
    location,
    loading,
    getSellerSetting,
    logo,
    companyName,
  } = props

  let prevStations = [...stations]
  const [initialState, setInitialState] = useState({
    visibleNew: false,
    visibleAssign: false,
    isEditStation: false,
    visibleAddStation: false,
    stationId: -1,
    stationName: '',
    fromDate: moment().format('YYYY-MM-DD'),
    toDate: moment().format('YYYY-MM-DD'),
    allCollapsed: false,
    visiblePrintModal: false,
    printProcessingRef: null,
    openStatusList: [],
  })
  const [currentStation, setCurrentStation] = useState<Station | null>(null)
  const [settingsList, setSettingsList] = useState<Array<StationSetting>>([])
  const [endDragging, setEndDragging] = useState(true)
  const [hoverIndex, setHoverIndex] = useState(-1)
  const [orderStatus, setOrderStatus] = useState({ ...initializeOrderState })
  const [filter, setFilter] = useState<FilterData>(initFiltersData)
  const [pickSheetModalVisible, setPickSheetModalVisible] = useState<boolean>(false)
  const printContentRef = React.useRef<any>(null)

  useEffect(() => {
    const newState = { ...initializeOrderState }
    workOrders.forEach((order: Order) => {
      newState[order.wholesaleOrderStatus]++
    })
    setOrderStatus(newState)
  }, [workOrders])

  useEffect(() => {
    history.replace(`${location.pathname}?${qs.stringify(filter)}`)
    getWorkOrderListByStationIdAndDate({
      stationId: initialState.stationId,
      fromDate: initialState.fromDate,
      toDate: initialState.toDate,
      filter,
    })
  }, [filter])

  useEffect(() => {
    setFilter(
      produce<FilterData>(filter, (draftState) => {
        draftState = Object.assign(draftState, qs.parse(location.search.slice(1)))
      }),
    )
  }, [])

  useEffect(() => {
    timer = setInterval(() => initValue(), interval)
    initValue()
    if (stations.length) {
      setInitialState((prevState) => {
        return {
          ...prevState,
          stationId: stations[0].id,
        }
      })
    }
    return () => {
      clearInterval(timer)
    }
  }, [])

  useEffect(() => {
    getSellerSetting()
  }, [])

  useEffect(() => {
    if (stations.length && initialState.stationId === -1) {
      onChangeStation(stations, stations[0].id)
    } else if (stations.length && prevStations.length !== stations.length) {
      const id = stations[stations.length - 1].id
      onChangeStation(stations, id)
    }
    prevStations = [...stations]
  }, [stations])

  useEffect(() => {
    //reset interval
    clearInterval(timer)
    timer = setInterval(() => initValue(), interval)
  }, [initialState.stationId, initialState.fromDate, filter])

  useEffect(() => {
    if (prevStations.length !== stations.length || initialState.stationId > -1) {
      _getCurrentStation()
    }
  }, [initialState.stationId])

  const _getCurrentStation = () => {
    const station = stations.find((station: any) => {
      return station.id === initialState.stationId
    })
    const curStation: Station | null = typeof station === 'undefined' ? null : station
    if (
      currentStation &&
      curStation &&
      currentStation.id === curStation.id &&
      currentStation.wholesaleStationSettingList.length === curStation.wholesaleStationSettingList.length
    ) {
      return
    }
    setCurrentStation(curStation)
    const settings = curStation != null ? curStation.wholesaleStationSettingList : []
    const displayOrders = settings.map((el) => {
      return el.displayOrder
    })
    if (displayOrders.indexOf(0) > -1) {
      settings.forEach((el, index) => {
        el.displayOrder = index + 1
      })
      // update StationgSettings with initial display orders - this will be called for old station data because old data have 0 displayOrder value at first

      updateSettingList(settings)
    }
    const ordered = orderBy(settings, ['displayOrder'], ['asc'])
    ordered.forEach((el) => {
      el.status = false
    })

    setSettingsList(ordered)
  }

  const initValue = () => {
    getAllManufactureTypes()
    getAllStations()
    getAllWorkers()
    if (initialState.stationId !== -1) {
      onChangeStation(stations, initialState.stationId)
    }
  }

  const onChangeStationName = (e: any) => {
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        stationName: e.target.value,
      }
    })
  }

  const onChangeStation = (stations: Station[], id: number) => {
    if (stations.length > 0) {
      setInitialState((prevState: any) => {
        return {
          ...prevState,
          stationId: id,
        }
      })
      setProcessingListHeader({
        pro_stationId: id,
      })
      getWorkOrderListByStationIdAndDate({
        stationId: id,
        fromDate: initialState.fromDate,
        toDate: initialState.toDate,
        filter,
      })
    }
  }

  const toggleNewModal = () => {
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        visibleNew: !initialState.visibleNew,
      }
    })
  }

  const handleNew = () => {
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        visibleNew: false,
      }
    })
  }

  const toggleAssignModal = () => {
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        visibleAssign: !initialState.visibleAssign,
      }
    })
  }

  const toggleAddStationModal = () => {
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        visibleAddStation: !initialState.visibleAddStation,
      }
    })
  }

  const toggleEditStation = () => {
    if (!currentStation) return
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        isEditStation: !initialState.isEditStation,
        stationName: currentStation.name,
      }
    })
  }

  const saveStationName = () => {
    toggleEditStation()
    updateStationName({
      stationId: initialState.stationId,
      name: initialState.stationName,
    })
  }

  const handleAssignWorker = (workerId: number) => {
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        visibleAssign: false,
      }
    })
    if (workerId >= 0) {
      assignWorkerToStation({
        stationId: initialState.stationId,
        workerId: workerId,
      })
    }
  }

  const handleUnAssignWorker = (workerId: number) => {
    unAssignWorkerToStation({
      stationId: initialState.stationId,
      workerId: workerId,
    })
  }

  const handleAdd = (stationName: string) => {
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        visibleAddStation: false,
      }
    })
    addStationByName(stationName)
  }

  const toggleCollapseAll = () => {
    if (!currentStation) return
    const opensList: any[] = []
    if (settingsList.length > 0) {
      const len = settingsList.length
      for (let i = 0; i < len; i++) {
        opensList.push({
          id: settingsList[i].wholesaleManufactureType.id,
          status: initialState.allCollapsed ? false : true,
        })
      }
    }
    setInitialState((prevState: any) => {
      return {
        ...prevState,
        allCollapsed: !initialState.allCollapsed,
        openStatusList: opensList,
      }
    })
    settingsList.forEach((el) => {
      el.status = initialState.allCollapsed ? false : true
    })

    setSettingsList(settingsList)
  }

  const handleAssignManufacturingType = (type: ManufactureType) => {
    assignManufactureTypeToStation({
      stationId: initialState.stationId,
      manufactureTypeId: type.id,
    })

    getWorkOrderListByStationIdAndDate({
      stationId: initialState.stationId,
      fromDate: initialState.fromDate,
      toDate: initialState.toDate,
      filter,
    })

    _getCurrentStation()
  }

  const onDateChange = (dates: [any, any], dateStrings: [string, string]) => {
    if (dateStrings.length > 0) {
      setInitialState((prevState: any) => {
        return {
          ...prevState,
          fromDate: moment(dateStrings[0]).format('YYYY-MM-DD'),
          toDate: moment(dateStrings[1]).format('YYYY-MM-DD'),
        }
      })

      setProcessingListHeader({
        pro_from: moment(dateStrings[0]).format('YYYY-MM-DD'),
        pro_to: moment(dateStrings[1]).format('YYYY-MM-DD'),
      })

      getWorkOrderListByStationIdAndDate({
        stationId: initialState.stationId,
        fromDate: moment(dateStrings[0]).format('YYYY-MM-DD'),
        toDate: moment(dateStrings[1]).format('YYYY-MM-DD'),
        filter,
      })
    }
  }

  const collapseHeader = (type: any) => {
    return (
      <ProcessingListItem onClick={() => onOpen(!type.status, type.wholesaleManufactureType.id)}>
        {type.status ? (
          <Icon type="caret-down" style={{ fontSize: '16px', marginRight: '10px' }} />
        ) : (
          <Icon type="caret-right" style={{ fontSize: '16px', marginRight: '10px' }} />
        )}
        {type.wholesaleManufactureType ? type.wholesaleManufactureType.name : ''}
      </ProcessingListItem>
    )
  }

  const {
    visibleNew,
    visibleAssign,
    visibleAddStation,
    isEditStation,
    stationName,
    allCollapsed,
    visiblePrintModal,
    printProcessingRef,
  } = initialState
  const calendarIcon = <IconSvg type="calendar" viewBox="0 0 22 22" width={17} height={18} />
  const dateFormat = 'MM/DD/YYYY'
  const dates = [moment(), moment()]

  const moveCard = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const draggedSetting = settingsList[dragIndex]

      setSettingsList(
        update(settingsList, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, draggedSetting],
          ],
        }),
      )
    },
    [settingsList],
  )

  const onOpen = (status: boolean, manufactureTypeId: number) => {
    const opensList: boolean[] = []
    settingsList.forEach((el) => {
      if (el.wholesaleManufactureType.id == manufactureTypeId) {
        el.status = status
      }
      opensList.push(el.status)
    })

    setSettingsList(settingsList)
    setInitialState((prevState) => {
      return {
        ...prevState,
        openStatusList: opensList,
      }
    })
  }

  const togglePrintModal = () => {
    setInitialState((prevState) => {
      return {
        ...prevState,
        visiblePrintModal: !initialState.visiblePrintModal,
      }
    })
  }

  const setProcessingRef = (el: any) => {
    setInitialState((prevState) => {
      return {
        ...prevState,
        printProcessingRef: el,
      }
    })
  }

  const onDropChange = () => {
    updateSettingList(settingsList)
  }

  const updateSettingList = (list: StationSetting[]) => {
    const setting = cloneDeep(list)
    setting.forEach((el, index) => {
      delete el.status
      el.displayOrder = index + 1
    })
    updateSettingsDisplayOrder(setting)
  }

  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1">Print List</Menu.Item>
      <Menu.Item key="2">Print Pick Sheets</Menu.Item>
    </Menu>
  )

  function handleMenuClick({ key }: ClickParam) {
    if (key === '1') {
      togglePrintModal()
    }
    if (key === '2') {
      setPickSheetModalVisible(true)
    }
  }

  return (
    <PageLayout currentTopMenu={'menu-Inventory-Processing List'} noSubMenu={true}>
      <NavHeaderButtonsWrapper>
        <Dropdown overlay={menu}>
          <Button>
            Actions <Icon type="down" />
          </Button>
        </Dropdown>
        <AddManufacturingType
          manufactureTypes={manufactureTypes}
          wholesaleStationSetting={currentStation != null ? currentStation.wholesaleStationSettingList : []}
          onAssign={handleAssignManufacturingType}
        />
        {workers.length > 0 && <Button onClick={toggleAssignModal}>Assign Worker</Button>}
        {stations.length > 0 && manufactureTypes.length > 0 && <Button onClick={toggleNewModal}>New Work Order</Button>}
      </NavHeaderButtonsWrapper>
      <ProcessingListHeader>
        <Row style={{ width: '100%' }}>
          <Col md={24} style={{ paddingLeft: 66, display: 'flex', alignItems: 'flex-end' }}>
            {isEditStation === false ? (
              <ReactWrapper style={{ border: 0, width: 250 }}>
                <InputLabel>Station</InputLabel>
                <ThemeSelect
                  className="pl-select"
                  style={{ width: 248, marginRight: 5, height: 42 }}
                  value={currentStation !== null ? currentStation.id : null}
                  onChange={(v: number) => onChangeStation(stations, v)}
                >
                  {stations.length > 0 &&
                    stations.map((station) => (
                      <Select.Option value={station.id} key={`station-${station.id}`}>
                        {station.name}
                      </Select.Option>
                    ))}
                </ThemeSelect>
              </ReactWrapper>
            ) : (
              <ReactWrapper style={{ border: 0, width: 270 }}>
                <InputLabel>Station</InputLabel>
                <ThemeInput
                  style={{ width: 248, marginRight: 5, height: 42 }}
                  value={stationName}
                  onChange={onChangeStationName}
                />
                <ThemeIconButton type="link" style={floatRight} onClick={toggleEditStation}>
                  <Icon type="close" />
                </ThemeIconButton>
                <ThemeIconButton disabled={stationName === ''} type="link" style={floatRight} onClick={saveStationName}>
                  <Icon type="save" />
                </ThemeIconButton>
              </ReactWrapper>
            )}

            <ReactWrapper style={{ width: 248, border: 0, marginLeft: '24px' }}>
              <InputLabel>Start Date</InputLabel>
              <ThemeRangePicker
                defaultValue={[dates[0], dates[1]]}
                format={dateFormat}
                suffixIcon={calendarIcon}
                allowClear={false}
                width={248}
                onChange={onDateChange}
              />
            </ReactWrapper>

            <ReactWrapper className="orders-info">
              <div>
                <InputLabel>Total Work Orders</InputLabel>
                <ValueSpan className="orders-number">{workOrders.length}</ValueSpan>
              </div>
              <ul className="orders-status-panel">
                {Object.keys(orderStatus).map((key: string) => {
                  return (
                    <li key={key}>
                      <StatusSpan className={`${key} capitalize`}>
                        <strong>{orderStatus[key]}</strong>
                        {statusMap[key]}
                      </StatusSpan>
                    </li>
                  )
                })}
              </ul>
            </ReactWrapper>
          </Col>
        </Row>
      </ProcessingListHeader>
      <FilterBar filter={filter} setFilter={setFilter} />
      <div style={{ padding: '0 47px 25px 94px' }}>
        <TableTopBar>
          <span>{settingsList.length} Manufacturing Types</span>
          <ThemeButton type="link" icon={!allCollapsed ? 'plus' : 'minus'} onClick={toggleCollapseAll}>
            {!allCollapsed ? 'Expand all' : 'Collapse all'}
          </ThemeButton>
        </TableTopBar>
        <div>
          <DndProvider backend={HTML5Backend}>
            <div className="processing-list-container">
              {settingsList.map((row: any, index: number) => {
                return (
                  <ListTable
                    moveCard={moveCard}
                    header={collapseHeader(row)}
                    key={index}
                    workOrders={workOrders}
                    title={row.wholesaleManufactureType.name}
                    id={row.wholesaleManufactureType.id}
                    stationSettingId={row.id}
                    index={index}
                    history={history}
                    allCollapsed={allCollapsed}
                    endDragging={endDragging}
                    setEndDragging={setEndDragging}
                    defaultHoverIndex={hoverIndex}
                    setDefaultHoverIndex={setHoverIndex}
                    openStatus={row.status}
                    onOpen={onOpen}
                    onDropChange={onDropChange}
                    reassignOrder={updateWO}
                    orderLoading={loading}
                    resetLoading={resetLoading}
                    updateWorkOrderDisplayNumber={updateWorkOrderDisplayNumber}
                  />
                )
              })}
            </div>
          </DndProvider>
        </div>
      </div>
      {stations.length > 0 && manufactureTypes.length > 0 && (
        <WorkOrderModal
          isSpecial={false}
          visible={visibleNew}
          stations={stations}
          manufactureTypes={manufactureTypes}
          history={history}
          handleOk={handleNew}
          handleCancel={toggleNewModal}
        />
      )}
      {currentStation && workers.length > 0 && (
        <AssignEmployeeModal
          workers={workers}
          visible={visibleAssign}
          station={currentStation}
          handleOk={handleAssignWorker}
          handleCancel={toggleAssignModal}
          handleUnAssign={handleUnAssignWorker}
        />
      )}
      <AddStationModal visible={visibleAddStation} handleOk={handleAdd} handleCancel={toggleAddStationModal} />
      <StyleModal width={1080} footer={null} visible={visiblePrintModal} onCancel={togglePrintModal}>
        <div id={'printProcessingModal'}>
          <PrintProcessingModal
            currentStation={currentStation}
            workOrders={workOrders}
            history={history}
            fromDate={initialState.fromDate}
            toDate={initialState.toDate}
            ref={(el: any) => {
              setProcessingRef.bind(el)
            }}
          />
        </div>
        <div style={{ padding: '10px 20px 0' }}>
          <ThemeButton
            shape="round"
            style={{ width: '100%' }}
            onClick={() => printWindow('printProcessingModal', printProcessingRef)}
          >
            <Icon type="printer" theme="filled" />
            Print List
          </ThemeButton>
        </div>
      </StyleModal>

      <Modal
        width={1080}
        footer={null}
        visible={pickSheetModalVisible}
        onCancel={() => setPickSheetModalVisible(false)}
      >
        <ThemeSpin spinning={false}>
          <div id={'printPickSheetModal'} style={{ minHeight: 500 }}>
            <div ref={printContentRef}>
              {workOrders.map((order, index) => {
                return (
                  <div key={`print-picksheet-${index}`}>
                    <PrintPickSheet
                      orderItems={order.orderItemList}
                      currentOrder={order}
                      companyName={companyName}
                      catchWeightValues={[]}
                      logo={logo}
                      multiple={true}
                    />
                    <div style={{ pageBreakAfter: 'always' }} />
                  </div>
                )
              })}
            </div>
          </div>
          <ThemeButton
            shape="round"
            style={fullButton}
            className="print-pick-sheet"
            onClick={() => printWindow('printPickSheetModal', () => printContentRef.current, 'portrait')}
          >
            <Icon type="printer" theme="filled" />
            Print Pick Sheets
          </ThemeButton>
        </ThemeSpin>
      </Modal>
    </PageLayout>
  )
}

const mapState = (state: GlobalState) => state.manufacturing
export default withTheme(connect(ManufacturingModule)(mapState)(ProcessingList))

const statusMap = {
  REQUEST: 'New Order',
  SCHEDULED: 'Scheduled',
  WIP: 'In Progress',
  COMPLETED: 'Completed',
  SHIPPED: 'Shipped',
}

const initializeOrderState = {
  REQUEST: 0,
  SCHEDULED: 0,
  WIP: 0,
  COMPLETED: 0,
  SHIPPED: 0,
}
