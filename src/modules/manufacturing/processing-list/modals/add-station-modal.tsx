import * as React from 'react'
import { ThemeModal, InputLabel, ThemeInput } from '~/modules/customers/customers.style';

interface AddStationModalProps {
  visible: boolean
  handleOk: any
  handleCancel: any
}

export class AddStationModal extends React.PureComponent<AddStationModalProps> {
  state: any
  constructor(props: AddStationModalProps) {    
    super(props)
    this.state = {
      stationName: '',
    }
  }

  onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      stationName : e.target.value
    })
  }


  render() {
    const {visible, handleOk, handleCancel} = this.props;
    const {stationName} = this.state

    return (
      <ThemeModal
        title="Add Station"
        visible={visible}
        onOk={()=> handleOk(stationName)}
        okButtonProps={{disabled: stationName === ''}}
        okText="Submit"
        onCancel={handleCancel}
        // footer={null}
      >
        <InputLabel>Station Name:</InputLabel>
        <ThemeInput value={stationName} onChange = {this.onChangeInput} />
      </ThemeModal>
    )
  }
}

export default AddStationModal
