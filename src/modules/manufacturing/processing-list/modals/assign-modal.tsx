import * as React from 'react'
import { ThemeModal, InputLabel } from '~/modules/customers/customers.style';
import { AutoComplete, Popconfirm, Table, Select } from 'antd';
import { AssignedClientWrapper } from '~/modules/pricesheet/pricesheet-detail.style';
import { PayloadIcon } from '~/components/elements/elements.style';
import { Icon as IconSvg } from '~/components/icon/'
import { Station } from '~/schema';

interface AssignEmployeeModalProps {
  workers: any[]
  visible: boolean
  handleOk: any
  handleCancel: any
  handleUnAssign: any
  station: Station
}

export class AssignEmployeeModal extends React.PureComponent<AssignEmployeeModalProps> {
  state: any
  constructor(props: AssignEmployeeModalProps) {
    super(props)
    this.state = {
      workerId: props.workers[0].userId,
      value: ''
    }
  }

  handleFilterOption = (input: string, option: any) => {
    return option.props.children.toUpperCase().indexOf(input.toUpperCase()) !== -1
  };

  onSelect = (workerId: number, option: any) => {
    this.setState({
      workerId
    })
  }

  onChange = (value: string) => {
    console.log(value);
    this.setState({ value });
  }

  clear = (value: string) => {
    this.setState({ value: '' });
  }  

  componentDidUpdate(prevProps: any) {
    if(prevProps.visible != this.props.visible) {
      this.clear();
    }
  }

  renderWorker = (worker: any, index) =>{
    return (
      <Select.Option key={worker.userId} text={worker.userId}>
        {`${worker.firstName} ${worker.lastName}`}
      </Select.Option>
    );
  }  

  render() {
    const {visible, workers, handleOk, handleCancel, station} = this.props;
    const {workerId} = this.state
    const column = [
      {
        dataIndex: 'user',
        render: (user: any) => `${user.firstName} ${user.lastName}`
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        width: '10%',
        render: (text: number, record: any) =>{
           return <Popconfirm title="Sure to unassign?" onConfirm={() => this.props.handleUnAssign(record.user.userId)}>
             <a>
               <PayloadIcon>
                 <IconSvg
                   type="close"
                   viewBox="0 0 20 28"
                   width={15}
                   height={14} />
               </PayloadIcon>
             </a>
           </Popconfirm>
        }
      }
    ]   
    return (
      <ThemeModal
        title="Assign Worker"
        visible={visible}
        onOk={() => handleOk(workerId)}
        okText="+ Assign"
        onCancel={handleCancel}
        okButtonProps={{disabled: workerId < 0}}
        // footer={null}
      >
        <InputLabel>Worker Name</InputLabel>
        <AutoComplete
          size="large"
          style={{ width: '100%' }}
          dataSource={workers}
          dataSource={workers.length > 0 ? workers.map(this.renderWorker) : []}
          filterOption={this.handleFilterOption}
          onSelect={this.onSelect}
          value={this.state.value}
          onChange={this.onChange}
          // onDropdownVisibleChange={this.onDropdownVisibleChange}
          placeholder={`Select a worker name`}
          allowClear={true}
        />
        <InputLabel style={{marginTop: 15}}>Assigned Workers</InputLabel>
        <AssignedClientWrapper>
          <Table columns={column} dataSource={station.wholesaleStationDetailList} rowKey="clientId" showHeader={false} />
        </AssignedClientWrapper>
      </ThemeModal>
    )
  }
}

export default AssignEmployeeModal
