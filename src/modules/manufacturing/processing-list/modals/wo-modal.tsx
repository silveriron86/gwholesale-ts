import * as React from 'react'
import { ThemeModal, InputLabel, ThemeInput, ThemeSelect, ThemeTextArea, ThemeRadio } from '~/modules/customers/customers.style';
import { Select, DatePicker, TimePicker, Row, Col, Radio } from 'antd';
import { Icon as IconSvg } from '~/components/icon/'
import moment from 'moment';
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { CustomDatePicker } from '../../work-orders/work-orders.style';
import { Station, ManufactureType, StationSetting } from '~/schema';
import { ManufacturingModule, ManufacturingStateProps, ManufacturingDispatchProps } from '../../manufacturing.module';
import { History } from 'history'
import { Theme } from '~/common';

type WorkOrderModalProps = ManufacturingDispatchProps & ManufacturingStateProps & {
  theme: Theme
} & {
  visible: boolean
  handleOk: any
  onWordOrderCreated?: any
  handleCancel: any
  stations: Station[]
  manufactureTypes: ManufactureType[]
  isSpecial: boolean
  orderId: string | undefined
  orderItemId: number | undefined
  quantity?: number
  history: History
}

export class WorkOrderModal extends React.PureComponent<WorkOrderModalProps> {
  state = {
    deliveryTime: moment(),
    orderName: '',
    notes: '',
    station: null,
    stationSettingId: null,
    itemType: 1
  }

  onDeliveryDateChange = (date: any, dateString: any) => {
    const time = this.state.deliveryTime.format("HH:mm")
    this.setState({
      deliveryTime: moment(dateString + " " + time)
    })
  }

  onDeliveryTimeChange = (time: any, timeString: any) => {
    const date = this.state.deliveryTime.format("MM/DD/YYYY")
    this.setState({
      deliveryTime: moment(date + " " + timeString)
    })
  }

  componentWillMount() {
    this.props.getAllStations();
  }

  componentWillReceiveProps(nextProps: any) {
    // Default today at 12pm if before 12pm. Otherwise tomorrow 12pm
    let deliveryTime = moment();
    if (moment().hours() >= 12) {
      deliveryTime = deliveryTime.add(1, 'days');
    }
    deliveryTime.set({ "hour": 12, "minute": 0 })

    this.setState({
      orderName: '',
      note: '',
      deliveryTime
    })

    const filteredStations = this._getFilteredStations();
    if (filteredStations.length > 0) {
      this.setState({
        station: filteredStations[0],
        stationSettingId: filteredStations[0].wholesaleStationSettingList[0].id
      })
    }

    if (nextProps.newWorkOrderId !== this.props.newWorkOrderId && nextProps.newWorkOrderId >= 0) {
      if (this.props.isSpecial === false) {
        //keep sales order page when creating work order from sales order item if isSpecial = true
        this.props.history.push(`/manufacturing/${nextProps.newWorkOrderId}/wo-overview`);
      }
      if (typeof this.props.onWordOrderCreated !== 'undefined') {
        this.props.onWordOrderCreated()
      }
    }

  }

  onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      orderName: e.target.value
    })
  }

  onChangeStation = (id: number) => {
    const station = this.props.stations.find(s => s.id === id);
    this.setState({
      station,
      stationSettingId: station!.wholesaleStationSettingList[0].id
    })
  }

  onChangeStationSetting = (stationSettingId: number) => {
    this.setState({
      stationSettingId
    })
  }

  onChangeNotes = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      notes: e.target.value
    })
  }

  onOk = () => {
    const { orderName, notes, deliveryTime, station, stationSettingId, itemType } = this.state
    const { isSpecial, orderItemId, orderId, quantity } = this.props
    let wholesaleStationSettingId = stationSettingId
    if (isSpecial === true && itemType == 1) { //if radio option is selected as Special Cuts Requests
      if (station && station.wholesaleStationSettingList.length) {
        const speicalCut = station.wholesaleStationSettingList.find((el: any) => { return el.wholesaleManufactureType.name.toLowerCase() == 'special cuts' })
        if (speicalCut) {
          wholesaleStationSettingId = speicalCut.id
        } else {
          return
        }
      } else {
        return
      }
    }
    this.props.handleOk();
    if (isSpecial === true) {
      let params = {
        orderId,
        data: {
          name: orderName,
          orderItemId: orderItemId,
          notes,
          requestedTime: moment.utc(deliveryTime).format('YYYY-MM-DD HH:mm:00'),
          wholesaleStationSettingId: wholesaleStationSettingId,
          wholesaleStationId: station != null ? station.id : null,
          type: itemType,
          quantity: quantity
        }
      }
      this.props.createWorkOrderWithItemId(params)
    } else {
      // processing list
      this.props.createWorkOrder({
        name: orderName,
        wholesaleStationSettingId: stationSettingId
      })
    }
  }

  _getFilteredStations = () => {
    const { stations, isSpecial } = this.props
    const { stationSettingId } = this.state
    return stations.length > 0 ? stations.filter(s => {
      let ret = false
      if (s.wholesaleStationSettingList.length > 0) {
        if (isSpecial === true) {
          const specialCut = s.wholesaleStationSettingList.filter((setting: any) => {
            // return (typeof setting.wholesaleManufactureType !== 'undefined' && setting.wholesaleManufactureType.name === 'Special Cuts')
            return typeof setting.wholesaleManufactureType !== 'undefined'
          })
          s.wholesaleStationSettingList = specialCut
          return (specialCut.length > 0)
        } else {
          ret = true
        }
      }
      return ret
    }) : [];
  }

  onChangeRadio = (e: any) => {
    this.setState({
      itemType: e.target.value,
    });
  }

  render() {
    const { deliveryTime, orderName, notes, station, stationSettingId, itemType } = this.state
    const { visible, stations, manufactureTypes, handleCancel, isSpecial } = this.props;
    // filter the stations with <wholesaleStationSettingList>
    const filteredStations = this._getFilteredStations()
    // console.log(filteredStations);

    return (
      <ThemeModal
        title="New Work Order Request"
        width={600}
        visible={visible}
        onOk={this.onOk}
        okText="Submit"
        onCancel={handleCancel}
        okButtonProps={{ disabled: (isSpecial === false && stationSettingId === null) }}
      // footer={null}
      >
        <Row>
          <Col md={8}>
            <InputLabel >Station</InputLabel>
          </Col>
          <Col md={16}>
            {filteredStations.length > 0 && (
              <ThemeSelect value={station ? station.id : ''} onChange={this.onChangeStation}>
                {filteredStations.length > 0 && (
                  filteredStations.map(station => <Select.Option value={station.id} key={`station-${station.id}`}>{station.name}</Select.Option>)
                )}
              </ThemeSelect>
            )}
          </Col>
        </Row>
        {/* <Row style={{ marginTop: 10 }}>
          <Col md={8}>
            <InputLabel>Working Order Name</InputLabel>
          </Col>
          <Col md={16}>
            <ThemeInput value={orderName} onChange={this.onChangeInput} />
          </Col>
        </Row> */}

        {isSpecial == true && (
          <Row style={{ marginTop: 10 }}>
            <Col md={8}>
              {/* <InputLabel>Item Type</InputLabel> */}
            </Col>
            <Col md={16} style={{ padding: '10px 0' }}>
              <Radio.Group value={itemType} onChange={this.onChangeRadio}>
                <ThemeRadio value={1}>Special Customer Request</ThemeRadio>
                <ThemeRadio value={2}>Regular Work Order</ThemeRadio>
              </Radio.Group>
            </Col>
          </Row>
        )}

        {((isSpecial == true && itemType != 1) || !isSpecial) && (
          <Row style={{ marginTop: 10 }}>
            <Col md={8}>
              <InputLabel>Manufacturing Type</InputLabel>
            </Col>
            <Col md={16}>
              <ThemeSelect value={stationSettingId} onChange={this.onChangeStationSetting}>
                {station && station.wholesaleStationSettingList.length > 0 && (
                  station.wholesaleStationSettingList.map((s: StationSetting) => <Select.Option value={s.id} key={`man-type-${s.id}`}>{s.wholesaleManufactureType.name}</Select.Option>)
                )}
              </ThemeSelect>
            </Col>
          </Row>
        )}

        {isSpecial === true && (
          <Row style={{ marginTop: 10 }}>
            <Col md={8}>
              <InputLabel>Request Date and Time</InputLabel>
            </Col>
            <Col md={16}>
              <CustomDatePicker
                placeholder={'MM/DD/YYYY'}
                format={'MM/DD/YYYY'}
                suffixIcon={<IconSvg type="calendar" viewBox="0 0 22 22" width={17} height={18} />}
                defaultValue={deliveryTime}
                onChange={this.onDeliveryDateChange.bind(this)}
                style={{ width: '60%' }}
                allowClear={false}
              />
              <TimePicker allowClear={false} defaultValue={deliveryTime} style={{ width: '40%', paddingLeft: 10 }} onChange={this.onDeliveryTimeChange.bind(this)} format={'HH:mm'} />
            </Col>
          </Row>
        )}

        {isSpecial === true && (
          <Row style={{ marginTop: 10 }}>
            <Col md={8}>
              <InputLabel>Instructions</InputLabel>
            </Col>
            <Col md={16}>
              <ThemeTextArea rows={5} value={notes} onChange={this.onChangeNotes} />
            </Col>
          </Row>
        )}
      </ThemeModal>
    )
  }
}

const mapState = (state: GlobalState) => state.manufacturing
export default withTheme(connect(ManufacturingModule)(mapState)(WorkOrderModal))
