import styled from '@emotion/styled'
export const StylePrintHeader: any = styled.div((props) => ({
  padding: '22px 40px',
  fontSize: '18px',
  lineHeight: '140%',
  fontWeight: 400,
  textAlign: 'center',
  color: '#000',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',

  p: {
    marginBottom: '0',
  },

  '.print-processing *': {
    fontSize: '12px !important',
  },

  '.page-info': {
    fontSize: '12px',
    alignSelf: 'flex-start',
  },
}))

export const StylePrintBody: any = styled.div(() => ({
  '&.print-processing *': {
    fontSize: '12px !important',
    color: '#22282A !important',
    fontWeight: 400,
  },

  '.ant-table-expanded-row > td > div': {
    padding: '10px 0 10px 140px !important',
  },

  '.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-thead .ant-table-column-title': {
    fontWeight: 400,
  },

  '.ant-table-tbody > tr > td': {
    padding: '15px',
  },

  '.ant-table-body .ant-table-thead > tr > th': {
    borderTop: '1px solid #D8DBDB',
    paddingBottom: 0,
  },

  '.details .ant-table-tbody > tr > td': {
    padding: 0,
  },

  '.ant-table-expanded-row': {
    '.ant-table-thead > tr > th, .ant-table-tbody > tr > td': {
      backgroundColor: '#fff !important',
    },
    '.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-thead > tr > th': {
      backgroundColor: '#fff',
    },
    '.ant-table-content .ant-table-body .ant-table-tbody > tr > td': {
      backgroundColor: '#fff',
    },
    '&.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr.ant-table-expanded-row > td': {
      backgroundColor: '#fff',
    },
  },

  '.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr.ant-table-expanded-row > td': {
    backgroundColor: '#fff !important',
  },

  '.ant-table-placeholder': {
    backgroundColor: '#fff',
  },

  'tr.ant-table-row.ant-table-row-level-0 > td': {
    borderTop: '1px solid #D8DBDB',
    borderBottom: '1px solid #D8DBDB',
  },

  'tr.ant-table-expanded-row tr.ant-table-row.ant-table-row-level-0 > td': {
    borderTop: 0,
    borderBottom: 0,
  },
}))

export const PageBreakAfter: any = styled.footer((props) => ({
  pageBreakAfter: 'always',
}))
