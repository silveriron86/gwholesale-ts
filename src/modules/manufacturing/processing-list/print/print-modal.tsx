import * as React from 'react'
import PrintListTable from './print-table'
import { Order, StationSchedule } from '~/schema'
import { History } from 'history'
import { FlexCenter } from '~/modules/customers/customers.style'
import { StylePrintHeader, StylePrintBody, PageBreakAfter } from './print-modal.style'
import moment from 'moment'

interface PrintProcessingModalProps {
  currentStation: null | any
  workOrders: Order[]
  history: History
  fromDate: string
  toDate: string
}

interface PrintProcessingModalState {
  activeKeyList: string[]
}

export class PrintProcessingModal extends React.PureComponent<PrintProcessingModalProps> {
  state: PrintProcessingModalState = {
    activeKeyList: [],
  }

  collapseHeader = (type: any) => {
    return <span>{type.wholesaleManufactureType ? type.wholesaleManufactureType.name : ''}</span>
  }

  render() {
    const { currentStation, workOrders, fromDate, toDate } = this.props
    const activeKeyList: any[] = []
    if (currentStation && currentStation.wholesaleStationSettingList.length > 0) {
      const len = currentStation.wholesaleStationSettingList.length
      for (let i = 0; i < len; i++) {
        activeKeyList.push(`${i + 1}`)
      }
    }
    const rows: any[] = []
    if (currentStation !== null && currentStation.wholesaleStationSettingList.length > 0) {
      currentStation.wholesaleStationSettingList.forEach((row: any, index: number) => {
        const myOrders = workOrders.filter((order: Order) => {
          let ret = false
          if (order.wholesaleStationSchedule && order.wholesaleStationSchedule.length > 0) {
            order.wholesaleStationSchedule.forEach((schedule: StationSchedule) => {
              if (
                schedule.wholesaleStationSetting &&
                schedule.wholesaleStationSetting.wholesaleManufactureType.id === row.wholesaleManufactureType.id
              ) {
                ret = true
              }
            })
          }

          return ret
        })

        row._myOrders = myOrders

        if (myOrders.length) {
          rows.push(row)
        }
      })
    }

    if (!rows.length) {
      return (
        <div style={{ minHeight: 300 }}>
          <StylePrintHeader>
            <p>Processing List</p>
            <p>
              {moment(fromDate).format('MM/DD/YYYY')} to {moment(toDate).format('MM/DD/YYYY')}
            </p>
          </StylePrintHeader>
          <StylePrintBody>
            <FlexCenter style={{ marginTop: 80, fontSize: 18, fontWeight: 400 }}>No data</FlexCenter>
          </StylePrintBody>
          <PageBreakAfter />
        </div>
      )
    }

    return (
      <div style={{ minHeight: 300 }}>
        {rows.map((row: any, index: number) => {
          const myOrders = row._myOrders
          return (
            <>
              <StylePrintHeader>
                <div className="name">
                  {this.collapseHeader(row)} ({myOrders.length})
                </div>
                <div>
                  <p>Processing List</p>
                  <p>
                    {moment(fromDate).format('MM/DD/YYYY')} to {moment(toDate).format('MM/DD/YYYY')}
                  </p>
                </div>
                <div className="page-info">
                  Page {index + 1} of {rows.length}
                </div>
              </StylePrintHeader>
              <StylePrintBody className="print-processing">
                {workOrders.length > 0 && activeKeyList.indexOf(`${index + 1}`) > -1 && (
                  <PrintListTable workOrders={myOrders} id={row.wholesaleManufactureType.id} history={history} />
                )}
              </StylePrintBody>
              <PageBreakAfter />
            </>
          )
        })}
      </div>
    )
  }
}

export default PrintProcessingModal
