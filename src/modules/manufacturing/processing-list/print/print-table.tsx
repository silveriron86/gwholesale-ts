import * as React from 'react'
import { Badge } from 'antd'
import { ExpandableTable } from '../processing-list.style'
import { Order, User, OrderItem } from '~/schema'
import { RouteProps } from 'react-router'
import { History } from 'history'
import moment from 'moment'
import { formatMaterialsInDataSource } from '../../work-orders/overview/tables/materials'
import { formatMaterialsOutDataSource } from '../../work-orders/overview/tables/products'
import ExpandTable from '../extend-table.component'

type PrintListTableProps = RouteProps & {
  id: number
  workOrders: Order[]
  history: History
}

const PrintListTable: React.SFC<PrintListTableProps> = (props) => {
  const { workOrders, history } = props

  const _filterOutTypes = (orderItemList: OrderItem[]) => {
    return orderItemList
      ? orderItemList.filter((oi) => {
          return oi.type === 2 || oi.type === 1
        })
      : []
  }

  const columns: any[] = [
    {
      title: 'WO#',
      dataIndex: 'wholesaleOrderId',
      key: 'orderId',
      width: 140,
      align: 'center',
    },
    {
      title: 'NAME',
      dataIndex: 'name',
      key: 'productName',
    },
    {
      title: 'START DATE',
      dataIndex: 'startDate',
      key: 'startDate',
      render: (value: number) => (value ? moment(value).format('YYYY-MM-DD') : ''),
    },
    {
      title: 'Customer / Driver',
      dataIndex: 'user',
      key: 'customer',
      render: (_: unknown, record: any) => {
        let driver = record?.overrideDriver ?? record?.defaultDriver ?? ''
        if (driver) {
          driver = `${driver.firstName} ${driver.lastName}`
        }
        const customerName = record?.wholesaleClient?.clientCompany?.companyName
        return (
          <>
            <div>{customerName}</div>
            {driver && (
              <div>
                <Badge status="default" text={driver} />
              </div>
            )}
          </>
        )
      },
    },
    {
      title: 'Units In',
      render: (_: string, record: any) => countUnitsInOrOut(record.orderItemList, 1),
    },
    {
      title: 'Units Out',
      render: (_: string, record: any) => countUnitsInOrOut(record.orderItemList, 2),
    },
    {
      title: 'STATUS',
      dataIndex: 'wholesaleOrderStatus',
      key: 'status',
    },
  ]

  const columnsInAndOut: any[] = [
    {
      title: 'Units Planned / Actual',
      align: 'left',
      render(_: any, record: any) {
        return `${record.unitsPlanned}/${record.unitsActual}  ${record.UOM}`
      },
    },
    {
      title: 'SKU',
      dataIndex: 'wholesaleItem.sku',
      align: 'center',
    },
    {
      title: 'Item',
      dataIndex: 'variety',
      align: 'center',
    },
  ]

  const materialsInColumns: any[] = [
    ...columnsInAndOut,
    {
      title: 'Lot (from Sales Order)',
      dataIndex: 'lotId',
      width: 250,
      align: 'center',
      key: 'lot',
    },
  ]
  const materialsOutColumn: any[] = [
    ...columnsInAndOut,
    {
      title: 'Weight Actual',
      dataIndex: 'weightActual',
      align: 'center',
      width: 250,
    },
  ]

  const onRow = (record: any) => {
    return {
      onClick: (_event: any) => {
        _event.preventDefault()
        if (_event.target.tagName !== 'BUTTON') {
          history.push(`/manufacturing/${record.wholesaleOrderId}/wo-overview`)
        }
      },
    }
  }

  return (
    <ExpandableTable
      dataSource={workOrders}
      columns={columns}
      pagination={false}
      expandIcon={() => null}
      expandIconColumnIndex={6}
      expandIconAsCell={false}
      defaultExpandAllRows={true}
      onRow={onRow}
      expandedRowRender={(record: any) => {
        const filtered = _filterOutTypes(record.orderItemList)
        return (
          !!filtered.length && (
            <ExpandTable
              inData={formatMaterialsInDataSource(record)}
              outData={formatMaterialsOutDataSource(record)}
              inColumns={materialsInColumns}
              outColumns={materialsOutColumn}
            />
          )
        )
      }}
    />
  )
}

export default PrintListTable

function countUnitsInOrOut(list: Array<any>, type: 1 | 2): number | '' {
  if (list && list.length) {
    return list.filter((el) => el.type === type).length
  }
  return ''
}
