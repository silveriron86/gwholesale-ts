import styled from '@emotion/styled'
import { Collapse, Table, DatePicker } from 'antd'
const { RangePicker } = DatePicker

export const ProcessingListHeader = styled.div`
  align-items: center;
  justify-content: center;
  display: flex;
  padding-bottom: 30px;
  padding-top: 19px;
  border-bottom: solid 1px #d8dbdb;
`

export const ThemeCollapse = styled(Collapse)((props) => ({
  '&': {
    border: 0,
    backgroundColor: 'white',
    '.ant-collapse-item': {
      marginBottom: 12,
      border: '1px solid #d9d9d9',
      borderRadius: 6,
      '.ant-collapse-header': {
        textAlign: 'left',
        color: props.theme.main,
        fontSize: 20,
        fontWeight: 'bold',
        padding: '19px 21px',
        '.ant-collapse-arrow': {
          textAlign: 'right',
          right: 21,
        },
      },
      '.ant-collapse-content': {
        borderTopWidth: 0,
        borderRadius: 6,
      },
    },
    '&.print-processing': {
      '.ant-collapse-item': {
        '.ant-collapse-header': {
          paddingBottom: 0,
        },
      },
    },
  },
}))

export const GreenLabel = styled('span')((props) => ({
  color: props.theme.primary,
  fontSize: 9,
  fontWeight: 'normal',
  textTransform: 'uppercase',
  marginRight: 25,
}))

export const InputSubLabel = styled('div')({
  color: '#373D3F',
  fontSize: 14,
  fontWeight: 'bold',
  marginTop: -12,
  marginBottom: 5,
})

export const ExpandableTable = styled(Table)((props) => ({
  '&': {
    '.ant-table-placeholder': {
      border: 0,
      padding: '5px 16px',
    },
    '.ant-empty-normal': {
      margin: 0,
    },
    '.ant-empty-image': {
      display: 'none',
    },
    '.ant-table-thead > tr > th, .ant-table-tbody > tr > td': {
      backgroundColor: '#ffffff',
      padding: '5px 16px',
      border: '0',
    },
    '.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-thead > tr > th': {
      backgroundColor: '#ffffff',
      paddingLeft: 0,
      paddingBottom: 0,
    },
  },
  '&.ant-table-wrapper .ant-table-content .ant-table-body tbody > .ant-table-row > td': {
    backgroundColor: '#ffffff',
  },

  '&.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr.ant-table-expanded-row > td': {
    backgroundColor: '#f7f7f7',
  },

  '.details .ant-table-tbody > tr > td': {
    padding: '16px 0',
  },

  '.ant-table-expanded-row': {
    '.ant-table-thead > tr > th, .ant-table-tbody > tr > td': {
      backgroundColor: '#f7f7f7',
    },
    '.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-thead > tr > th': {
      backgroundColor: '#f7f7f7',
    },
    '.ant-table-content .ant-table-body .ant-table-tbody > tr > td': {
      backgroundColor: '#f7f7f7',
    },
  },

  'ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr.ant-table-expanded-row > td': {
    backgroundColor: '#ffffff',
  },
}))

export const PadBottomDiv = styled('div')({
  backgroundColor: '#EDF1EE',
  height: 15,
  marginBottom: 10,
})

export const PadTopDiv = styled('div')({
  backgroundColor: '#EDF1EE',
  padding: '16px 0 0 16px',
})

export const SmallTable = styled(Table)((props) => ({
  '&': {
    margin: '0 !important',
    padding: 0,
    '& .ant-table-content': {
      '& .ant-table-body': {
        '& > table': {
          '& .ant-table-tbody': {
            '& > tr > td': {
              padding: '8px 8px',
            },
          },
        },
      },
    },
  },
}))

export const ReactWrapper = styled('div')({
  borderRadius: 6,
  border: '1px solid #DDDDDD',
  textAlign: 'left',

  '&.orders-info': {
    display: 'flex',
    width: '478px',
    height: '80px',
    backgroundColor: '#fff',
    marginLeft: '40px',
    padding: '6px 20px',
  },

  '& .orders-status-panel': {
    listStyle: 'none',
    display: 'flex',
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'column',
    marginLeft: '70px',
    marginBottom: 0,
    padding: '0',

    li: {
      margin: '3px 0',
    },
  },
})

export const ProcessingListItem = styled('div')((props: any) => ({
  cursor: 'pointer',
  fontSize: 12,
  fontWeight: 'bold',
  color: '#22282A',
  display: 'flex',
  alignItems: 'center',
  paddingLeft: '1rem',
  paddingBottom: '1rem',
}))

export const ThemeRangePicker = styled(RangePicker)((props: any) => ({
  '&': {
    '.ant-calendar-picker-icon': {
      color: props.theme.primary,
    },
    '.ant-input': {
      height: '42px',
    },
    '.ant-calendar-range-picker-separator': {
      lineHeight: '34px',
    },
  },
}))

export const DraggableTableWrapper = styled('div')((props: any) => ({
  '& tr.drop-over-downward td': {
    borderBottom: '2px dashed #1890ff',
  },
  '& tr.drop-over-upward td': {
    borderTop: '2px dashed #1890ff',
  },
}))

export const NavHeaderButtonsWrapper = styled('div')((props: any) => ({
  position: 'absolute',
  right: 0,
  top: 0,
  height: '60px',
  padding: '9px 0',
  zIndex: 1000,

  '.ant-btn': {
    height: '42px',
    color: props.theme.primary,
  },

  '.ant-dropdown-trigger, .ant-btn': {
    marginRight: '38px',
  },
}))

export const TableTopBar = styled('div')((props: any) => ({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: '16px 14px',

  '.ant-btn': {
    color: '#4A5355',
  },
}))
