import * as React from 'react'
import { useRef } from 'react'
import { Icon as AntIcon, Badge, Button } from 'antd'
import { DraggableTableWrapper } from './processing-list.style'
import { Order, StationSchedule, User, OrderItem, AvailableStatus } from '~/schema'
import { RouteProps } from 'react-router'
import { History } from 'history'
import moment from 'moment'
import { useDrag, useDrop, DropTargetMonitor } from 'react-dnd'
import { XYCoord } from 'dnd-core'
import { dragItemStyle, handlerStyle, StatusSpan } from '../work-orders/work-orders.style'
import { formatMaterialsInDataSource } from '../work-orders/overview/tables/materials'
import { formatMaterialsOutDataSource } from '../work-orders/overview/tables/products'
import DragSortingTable from '~/components/Table/draggable-table'
import ExpandTable from './extend-table.component'
import { orderBy } from 'lodash'

type ListTableProps = RouteProps & {
  stationSettingId: number
  id: number
  header: Element | string
  title: string
  index: number
  workOrders: Order[]
  history: History
  allCollapsed: boolean
  endDragging: boolean
  defaultHoverIndex: number
  openStatus: boolean
  onOpen: Function
  setEndDragging: Function
  setDefaultHoverIndex: Function
  onDropChange: Function
  reassignOrder: Function
  loading: boolean
  resetLoading: Function
  updateWorkOrderDisplayNumber: Function
  moveCard: (dragIndex: number, hoverIndex: number) => void
}

interface DragItem {
  index: number
  id: string
  type: string
}

const ListTable: React.SFC<ListTableProps> = (props: any) => {
  const {
    workOrders,
    stationSettingId,
    loading,
    id,
    index,
    history,
    header,
    allCollapsed,
    endDragging,
    defaultHoverIndex,
    openStatus,
    onOpen,
    setEndDragging,
    setDefaultHoverIndex,
    onDropChange,
    moveCard,
    reassignOrder,
    resetLoading,
    updateWorkOrderDisplayNumber,
  } = props

  const [filteredDate, setFilteredData] = React.useState([])

  React.useEffect(() => {
    setEndDragging(true)
    setDefaultHoverIndex(-1)
  }, [id])

  React.useEffect(() => {
    const myOrders = workOrders.filter((order: Order) => {
      let ret = false
      if (order.wholesaleStationSchedule && order.wholesaleStationSchedule.length > 0) {
        order.wholesaleStationSchedule.forEach((schedule: StationSchedule) => {
          if (schedule.wholesaleStationSetting) {
            if (schedule.wholesaleStationSetting.wholesaleManufactureType.id === id) {
              ret = true
            }
          }
        })
      }
      return ret
    })

    const displayOrders = myOrders.map((el, index) => {
      return el.displayOrder
    })
    if (displayOrders.indexOf(0) > -1) {
      const displayOrderData = myOrders.map((el: Order, index: number) => {
        return { wholesaleOrderId: el.wholesaleOrderId, displayOrder: index + 1 }
      })
      updateWorkOrderDisplayNumber({ data: displayOrderData })
      setFilteredData(myOrders)
    } else {
      const sortedOrders = orderBy(myOrders, ['displayOrder'], ['asc'])
      setFilteredData(sortedOrders)
    }
  }, [workOrders])

  const ref = useRef<HTMLDivElement>(null)
  const [, drop] = useDrop({
    accept: 'card',
    hover(item: DragItem, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return
      }

      const dragIndex = item.index
      const hoverIndex = index
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current.getBoundingClientRect()

      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

      // Determine mouse position
      const clientOffset = monitor.getClientOffset()

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards

      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return
      }

      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex)
      setEndDragging(false)
      setDefaultHoverIndex(hoverIndex)
      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex
    },
  })

  const [, drag, preview] = useDrag({
    item: { type: 'card', id, index },
    begin: (monitor) => {
      setEndDragging(false)
    },
    end: (dropResult, monitor) => {
      setDefaultHoverIndex(-1)
      setEndDragging(true)
      onDropChange()
    },
  })

  const _filterOutTypes = (orderItemList: OrderItem[]) => {
    return orderItemList
      ? orderItemList.filter((oi) => {
          return oi.type === 2 || oi.type === 1
        })
      : []
  }

  const onSortString = (key: string, a: any, b: any) => {
    const stringA: string = a[key] ? a[key] : ''
    const stringB: string = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }
  const columns: any[] = [
    {
      title: '',
      width: 15,
      render: (_: any, record: any) => {
        return <>{record.wholesaleOrderId && <AntIcon type="bars" style={{ ...handlerStyle, cursor: 'move' }} />}</>
      },
    },
    {
      title: 'WO #',
      dataIndex: 'wholesaleOrderId',
      key: 'orderId',
      sorter: (a: Order, b: Order) => a.wholesaleOrderId - b.wholesaleOrderId,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'productName',
      sorter: (a: Order, b: Order) => onSortString('name', a, b),
    },
    {
      title: 'Start Date',
      dataIndex: 'startDate',
      key: 'startDate',
      sorter: (a: any, b: any) => a.startDate - b.startDate,
      render: (value: number) => (value ? moment(value).format('YYYY-MM-DD HH:mm:ss') : ''),
    },

    {
      title: 'Customer / Driver',
      dataIndex: 'user',
      key: 'customer',
      render: (user: User, record: any) => {
        let driver = record?.overrideDriver ?? record?.defaultDriver ?? ''
        if (driver) {
          driver = `${driver.firstName} ${driver.lastName}`
        }
        const customerName = record?.wholesaleClient?.clientCompany?.companyName
        return (
          <>
            <div>{customerName}</div>
            {driver && (
              <div>
                <Badge status="default" text={driver} />
              </div>
            )}
          </>
        )
      },
      sorter: (a: Order, b: Order) => onSortString('status.user.firstName', a, b),
    },
    {
      title: 'Units In',
      render: (_: string, record: any) => countUnitsInOrOut(record.orderItemList, 1),
      sorter: (a: Order, b: Order) => {
        const countA = countUnitsInOrOut(a.orderItemList, 1) || 0
        const countB = countUnitsInOrOut(b.orderItemList, 1) || 0
        return countA - countB
      },
    },
    {
      title: 'Units Out',
      render: (_: string, record: any) => countUnitsInOrOut(record.orderItemList, 2),
      sorter: (a: Order, b: Order) => {
        const countA = countUnitsInOrOut(a.orderItemList, 2) || 0
        const countB = countUnitsInOrOut(b.orderItemList, 2) || 0
        return countA - countB
      },
    },
    {
      title: 'Status',
      dataIndex: 'wholesaleOrderStatus',
      key: 'status',
      sorter: (a: Order, b: Order) => onSortString('status', a, b),
      render: (value: keyof typeof AvailableStatus) => {
        return <StatusSpan className={value}>{value ? statusMap[value] : ''}</StatusSpan>
      },
    },
  ]

  const statusMap = {
    REQUEST: 'New Order',
    SCHEDULED: 'Scheduled',
    WIP: 'In Progress',
    COMPLETE: 'Complete',
    SHIPPED: 'Shipped',
  }

  const columnsInAndOut: any[] = [
    {
      title: 'Units Planned / Actual',
      align: 'left',
      render(_: any, record: any) {
        return `${record.unitsPlanned}/${record.unitsActual}  ${record.UOM}`
      },
    },
    {
      title: 'SKU',
      dataIndex: 'wholesaleItem.sku',
      align: 'center',
    },
    {
      title: 'Item',
      dataIndex: 'variety',
      align: 'center',
    },
  ]

  const materialsInColumns: any[] = [
    ...columnsInAndOut,
    {
      title: 'Lot (from Sales Order)',
      dataIndex: 'lotId',
      width: 250,
      align: 'center',
      key: 'lot',
    },
  ]
  const materialsOutColumn: any[] = [
    ...columnsInAndOut,
    {
      title: 'Weight Actual',
      dataIndex: 'weightActual',
      align: 'center',
      width: 250,
    },
  ]

  const onRow = (record: any, index: number) => {
    return {
      onClick: (_event: any) => {
        _event.preventDefault()
        if (_event.target.tagName !== 'BUTTON') {
          history.push(`/manufacturing/${record.wholesaleOrderId}/wo-overview`)
        }
      },
    }
  }

  const reassignStationSettingId = (dragInfo: any, hoverInfo: any) => {
    const orderId = dragInfo.orderId
    const stationSettingId = hoverInfo.stationSettingId
    const hoverIndex = hoverInfo.index
    const targetOrder = workOrders.find((order: Order) => {
      return order.wholesaleOrderId === orderId
    })
    const targetOrders = workOrders.filter((order: Order) => {
      let ret = false
      if (order.wholesaleStationSchedule && order.wholesaleStationSchedule.length > 0) {
        order.wholesaleStationSchedule.forEach((schedule: StationSchedule) => {
          if (schedule.wholesaleStationSetting) {
            if (schedule.wholesaleStationSetting.id === stationSettingId) {
              ret = true
            }
          }
        })
      }
      return ret
    })
    const newOrders = orderBy(targetOrders, ['displayOrder'], ['asc'])

    resetLoading()
    reassignOrder({ orderId: orderId, wholesaleStationSettingId: stationSettingId })

    //call update displayOrder api after 500ms to avoid updating at same time
    setTimeout(() => {
      if (targetOrder) {
        if (hoverInfo.orderId) {
          //if target is not bottom row---
          newOrders.splice(hoverIndex, 0, targetOrder)
        } else {
          newOrders.push(targetOrder)
        }
        const displayOrderData = newOrders.map((el: Order, index: number) => {
          return { wholesaleOrderId: el.wholesaleOrderId, displayOrder: index + 1 }
        })
        updateWorkOrderDisplayNumber(displayOrderData)
      }
    }, 500)
  }

  const expandButton = (props: any) => {
    return (
      props.record.wholesaleOrderId && (
        <Button type="link" icon={props.expanded ? 'caret-down' : 'caret-right'} onClick={props.onExpand} />
      )
    )
  }

  drag(drop(ref))

  const fixStyles = {
    border: '1px solid #D8DBDB',
    borderRadius: 3,
    paddingTop: '1rem',
    marginBottom: '24px',
    backgroundColor: 'white',
    position: 'relative',
  }

  return (
    <div
      ref={preview}
      style={{
        opacity: endDragging ? 1 : defaultHoverIndex === index ? 0.1 : 1,
        ...fixStyles,
      }}
    >
      <div style={{ ...dragItemStyle }}>
        <div>{header}</div>
      </div>
      <DraggableTableWrapper>
        {openStatus && (
          <DragSortingTable
            dataSource={filteredDate}
            columns={columns}
            pagination={false}
            expandIcon={expandButton}
            expandIconColumnIndex={1}
            expandIconAsCell={false}
            defaultExpandAllRows={allCollapsed}
            onRow={onRow}
            draggable={true}
            stationSettingId={stationSettingId}
            loading={loading}
            reassignStationSettingId={reassignStationSettingId}
            updateWorkOrderDisplayNumber={updateWorkOrderDisplayNumber}
            expandedRowRender={(record: Order) => {
              const filtered = _filterOutTypes(record.orderItemList)
              return (
                !!filtered.length && (
                  <ExpandTable
                    instructions={record?.manufactureNote ?? 'N/A'}
                    inData={formatMaterialsInDataSource(record)}
                    outData={formatMaterialsOutDataSource(record)}
                    inColumns={materialsInColumns}
                    outColumns={materialsOutColumn}
                  />
                )
              )
            }}
          />
        )}
      </DraggableTableWrapper>
    </div>
  )
}
export default ListTable

function countUnitsInOrOut(list: Array<any>, type: 1 | 2): number | '' {
  if (list && list.length) {
    return list.filter((el) => el.type === type).length
  }
  return ''
}
