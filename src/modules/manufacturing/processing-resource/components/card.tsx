import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Avatar, Icon } from 'antd'
import { ResourceCardContainer, CardHeader, NameSpan, StatusWrapper, HeaderItemWrapper, MatrixTableWrapper } from '../../work-orders/work-orders.style'
import { ThemeTable } from '~/modules/customers/customers.style'
import moment from 'moment'

interface CardProps {
  data: any,
  counts: number
}

export class ProcessingResourceCard extends React.PureComponent<CardProps> {
  state = {
    expand: true,
  }

  column = [
    {
      title: 'WORK ORDER#',
      dataIndex: 'wholesaleOrderId',
      key: 'wholesaleOrderId',
    },
    {
      title: 'WORK ORDER NAME #',
      dataIndex: 'name',
      key: 'name',      
    },
    {
      title: 'REQUESTED ETA',
      dataIndex: 'requestedTime',
      key: 'requestedTime',
      render: (time: number) => {
        return moment.utc(time).format('MM/DD/YYYY')
      }
    },   
    {
      title: 'STATUS',
      dataIndex: 'status',
      key: 'status',
      render: (status: string) => {
        if(status == 'PLANNING') {
          return <StatusWrapper><Icon type='unordered-list'/>{status}</StatusWrapper>
        } else if(status == 'SCHEDULED') {
          return <StatusWrapper><Icon type='schedule'/>{status}</StatusWrapper>
        } else if(status == 'COMPLETED') {
          return <StatusWrapper><Icon type='check-square'/>{status}</StatusWrapper>
        }
      }
    } 
  ]

  onClickExpand = () => {
    this.setState({expand: !this.state.expand})
  }

  render() {
    const { data: {user, orders}, counts } = this.props
    console.log('counts', counts)
    const { expand } = this.state
    return (
      <ResourceCardContainer>
        <CardHeader>
          <HeaderItemWrapper>
            <Avatar icon='user'/>
            <NameSpan>{user ? user.firstName + ' ' + user.lastName : 'N/A'}</NameSpan>
          </HeaderItemWrapper>
          <HeaderItemWrapper>
          <NameSpan>{counts == 0 ? '' : counts}</NameSpan>
            <Icon type={ expand ? 'down' : 'up'} onClick={this.onClickExpand}/>
          </HeaderItemWrapper>
        </CardHeader> 
        {expand &&
          <MatrixTableWrapper >
            <ThemeTable 
              columns={this.column}
              dataSource={orders}
              rowKey="wholesaleOrderId"
              pagination={false}
            />
          </MatrixTableWrapper>
        }
      </ResourceCardContainer>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
// export default withTheme(connect(OrdersModule)(mapState)(WorkOrderDetails))
export default withTheme(ProcessingResourceCard)
