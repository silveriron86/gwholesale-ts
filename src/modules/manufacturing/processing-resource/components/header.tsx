import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Row, Col, Input } from 'antd'
import { Icon as IconSvg } from '~/components'
import moment, {Moment} from 'moment'
import { ProcessingListHeader, ReactWrapper, ThemeRangePicker } from '../../processing-list/processing-list.style'
import { HeaderOptions, InputLabel } from '~/modules/customers/customers.style'
import { ValueSpan } from '../../work-orders/work-orders.style'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from '~/modules/orders'
import { Theme } from '~/common'
import _ from 'lodash'
import { searchButton } from '~/components/elements/search-button'

const calendarIcon = <IconSvg type="calendar" viewBox="0 0 22 22" width={17} height={18} />

type MatrixHeaderProps = OrdersDispatchProps &
OrdersStateProps & {
  theme: Theme,
  onSearch: Function
}

class ProcessingResourceHeader extends React.PureComponent<MatrixHeaderProps> {
  state = {
    searchStr: '',
    fromDate: moment().subtract(30, 'days'),
    toDate: moment()
  }
  componentDidMount() {
    const data = {
      from: this.state.fromDate.format('YYYY-MM-DD'),
      to: this.state.toDate.format('YYYY-MM-DD'),
      param: ''
    }
    this.props.getProcessingResourceMatrix(data)
  }

  onSearch = (val: string) => {
    if(this.state.searchStr != val) {
      this.setState({searchStr: val})
      this.props.onSearch(val)
    }
  }

  onDateChange = (dates: [Moment, Moment], dateStrings: [string, string]) => {
    if (dateStrings.length > 0) {
      let data = {
        param: '',
        from: moment(dateStrings[0]).format('YYYY-MM-DD'),
        to: moment(dateStrings[1]).format('YYYY-MM-DD'),
      }
      this.setState({fromDate: moment(dateStrings[0]), toDate: moment(dateStrings[1])})
      this.props.getProcessingResourceMatrix(data)
    }
  }

  getResourcesAndLastUpdated = () => {
    const { resourceMatrix } = this.props
    if(resourceMatrix.length == 0) return ['N/A', 'N/A']
    let totalResources = _.sum(_.map(resourceMatrix, elem=>{  return elem.orders.length }))
    let lastUpdatedOrder: any = _.maxBy(_.map(resourceMatrix, elem=>{
      return _.maxBy(elem.orders, el=>{return el ? el.updatedDate : null})}), function(o){return o ? o.updatedDate : null})
    let lastUpdated = lastUpdatedOrder ? moment(lastUpdatedOrder.updatedDate).format('MM/DD/YYYY HH:mm:ss') : 'N/A'
    return [totalResources, lastUpdated]
  }

  render() {
    const {fromDate, toDate} = this.state;
    const {resourceMatrix} = this.props

    return (
      <ProcessingListHeader>
        <Row style={{width: '100%'}}>
          <Col span={8} style={{paddingLeft: 66, paddingTop: 25}}>
            <HeaderOptions>
              <Input.Search
                placeholder="Search ..."
                size="large"
                style={{ border: '0' }}
                enterButton={searchButton}
                onSearch={this.onSearch}
              />
            </HeaderOptions>
          </Col>
          <Col span={14}>
            <Row type='flex' justify='space-around'>
              <Col span={4}>
                <ReactWrapper style={{minWidth: 250, border: 0}}>
                  <InputLabel style={{marginTop: -5}}>DEFAULT SCHEDULE DATE</InputLabel>
                    <ThemeRangePicker
                    // placeholder={dateFormat}
                    defaultValue={[fromDate, toDate]}
                    format={"MM/DD/YYYY"}
                    suffixIcon={calendarIcon}
                    allowClear={false}
                    onChange={this.onDateChange}
                    />
                  </ReactWrapper>
              </Col>
              <Col span={4}>
                <ReactWrapper style={{minWidth: 250}}>
                  <InputLabel>RESOURCES</InputLabel>
                  <ValueSpan>{this.getResourcesAndLastUpdated()[0]}</ValueSpan>
                </ReactWrapper>
              </Col>
              <Col span={4}>
                <ReactWrapper style={{minWidth: 250}}>
                  <InputLabel>LAST UPDATED</InputLabel>
                  <ValueSpan>{this.getResourcesAndLastUpdated()[1]}</ValueSpan>
                </ReactWrapper>
              </Col>
            </Row>
          </Col>
        </Row>
      </ProcessingListHeader>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapState)(ProcessingResourceHeader))

