import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Row, Col, Table } from 'antd'
import PageLayout from '~/components/PageLayout'
import ProcessingResourceHeader from './components/header'
import { ProcessingResourceCard } from './components/card'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from '~/modules/orders'
import { Theme } from '~/common'
import _, { cloneDeep } from 'lodash'


type MatrixProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
  }

export class ProcessingResource extends React.PureComponent<MatrixProps> {
  state = {
    data: this.props.resourceMatrix
  }
  componentWillReceiveProps(nextProps: MatrixProps) {
    this.setState({data: nextProps.resourceMatrix})
  }

  renderCards = () => {
    const { data } = this.state
    let result: any = []
    if(data && data.length > 0) {
      data.forEach((el, index) => {
        result.push(<Col span={12} key={index} >
          <ProcessingResourceCard data={el} counts={el.orders.length}></ProcessingResourceCard>
          </Col>)
      });
    } else {
      result = (
        <Table dataSource={[]} columns={[]} />
      )
    }
    return result
  }

  onSearch = (val: string) => {
    const {resourceMatrix} = this.props
    const clone = cloneDeep(resourceMatrix)
    const filtered = _.map(clone, elem=>{
      elem.orders = _.filter(elem.orders, function(el){
        return el.wholesaleOrderId.toString().includes(val) || el.name.toLowerCase().includes(val.toLowerCase()) || el.status.toLowerCase().includes(val.toLowerCase())
      })
      return elem
    })

    this.setState({data: filtered})
  }

  render() {
    return (
      <PageLayout noSubMenu={true}>
        <ProcessingResourceHeader onSearch={this.onSearch}/>

        <div style={{padding: '0 47px 25px 94px'}}>
          <Row style={{marginTop: -20}}>
            {this.renderCards()}
          </Row>
        </div>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapState)(ProcessingResource))
