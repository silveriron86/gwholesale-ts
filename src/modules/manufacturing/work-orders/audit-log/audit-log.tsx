import * as React from 'react'
import { format } from 'date-fns'
import { Tabs, Table } from 'antd'
import { AuditLogTabs, ModalTable, WorkOrdersHeader } from '../work-orders.style'
import PageLayout from '~/components/PageLayout'
import { GlobalState } from '~/store/reducer'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { OrdersDispatchProps, OrdersModule, OrdersStateProps } from '~/modules/orders'
import { RouteComponentProps } from 'react-router'
import { Theme } from '~/common'
import OverviewHeader from '~/modules/manufacturing/work-orders/overview/header'
import { CustomersInfoBody } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { CustomerDetailsWrapper as DetailsWrapper } from '~/modules/customers/customers.style'
const { TabPane } = Tabs

type AuditLogProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }


export class AuditLog extends React.PureComponent<AuditLogProps> {
  componentDidMount() {
    const orderId = this.props.match.params.orderId
    this.props.getAllAuditLogs(+orderId)
  }

  formatDataForLog = (data: any, type: number) => {
    let logs
    switch (type) {
      case 1:
        logs = data.in
        break
      case 2:
        logs = data.out
        break
      case 3:
        logs = data.wo
        break
    }
    // console.log(logs)
    return logs.map((el: any) => ({
      sku: el.wholesaleOrderItem ? el.wholesaleOrderItem.wholesaleItem.sku : '',
      description: el.wholesaleOrderItem ? el.wholesaleOrderItem.wholesaleItem.variety : '',
      date: format(el.createdAt, 'MM/DD/YY'),
      time: format(el.createdAt, 'h:mm a'),
      user_account: el.user.firstName + ' ' + el.user.lastName,
      field_name: el.fieldName,
      changed_to: el.changeTo ? el.changeTo : '',
      changed_from: el.changeFrom ? el.changeFrom : '',
    }))
  }
  render() {
    const columns1 = [
      {
        title: 'DATE',
        dataIndex: 'date',
        key: 'date',
      },
      {
        title: 'TIME',
        dataIndex: 'time',
        key: 'time',
      },
      {
        title: 'USER ACCOUNT',
        dataIndex: 'user_account',
        key: 'user_account',
      },
      {
        title: 'FIELD NAME',
        dataIndex: 'field_name',
        key: 'field_name',
      },
      {
        title: 'CHANGED FROM',
        dataIndex: 'changed_from',
        key: 'changed_from',
      },
      {
        title: 'CHANGED TO',
        dataIndex: 'changed_to',
        key: 'changed_to',
      },
    ]

    const columns2 = [
      {
        title: 'SKU',
        dataIndex: 'sku',
        key: 'sku',
      },
      {
        title: 'DESCRIPTION',
        dataIndex: 'description',
        key: 'description',
      },
      {
        title: 'DATE',
        dataIndex: 'date',
        key: 'date',
      },
      {
        title: 'TIME',
        dataIndex: 'time',
        key: 'time',
      },
      {
        title: 'USER ACCOUNT',
        dataIndex: 'user_account',
        key: 'user_account',
      },
      {
        title: 'FIELD NAME',
        dataIndex: 'field_name',
        key: 'field_name',
      },
      {
        title: 'CHANGED FROM',
        dataIndex: 'changed_from',
        key: 'changed_from',
      },
      {
        title: 'CHANGED TO',
        dataIndex: 'changed_to',
        key: 'changed_to',
      },
    ]

    const { auditLog, currentWorkOrder } = this.props
    return (
      <PageLayout currentTopMenu={'menu-Inventory-Work Orders'}>
        <WorkOrdersHeader>
          <OverviewHeader order={currentWorkOrder} />
        </WorkOrdersHeader>
        <CustomersInfoBody>
          <DetailsWrapper>
            <AuditLogTabs defaultActiveKey="1" type="card" style={{ marginBottom: 0 }}>
              <TabPane tab="Work Order" key="1">
                <ModalTable dataSource={this.formatDataForLog(auditLog, 3)} columns={columns1} pagination={false} />
              </TabPane>
              <TabPane tab="Materials In" key="2">
                <ModalTable dataSource={this.formatDataForLog(auditLog, 1)} columns={columns2} pagination={false} />
              </TabPane>
              <TabPane tab="Materials Out" key="3">
                <ModalTable dataSource={this.formatDataForLog(auditLog, 2)} columns={columns1} pagination={false} />
              </TabPane>
            </AuditLogTabs>
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapState)(AuditLog))

