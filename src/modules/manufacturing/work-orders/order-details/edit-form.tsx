import * as React from 'react'
import {
  pd20,
  InputLabel,
  ThemeSelect,
  ThemeButton,
} from '~/modules/customers/customers.style'
import { Layout, Row, Col, Select, Popconfirm, TimePicker } from 'antd'
import FormItem from 'antd/lib/form/FormItem'
import Form, { FormComponentProps } from 'antd/lib/form'
import moment from 'moment'
import { formLayoutStyle, editFormStyle, CustomDatePicker } from '../work-orders.style'
import { Icon as IconSvg } from '~/components/icon/'
import { Order, Station, StationSetting, StationSchedule } from '~/schema'

interface EditFormProps extends FormComponentProps {
  order: Order
  stations: Station[]
  onSubmit: Function
}

class EditForm extends React.PureComponent<EditFormProps> {
  state: any
  constructor(props: EditFormProps) {
    super(props)
    const { order } = props
    let stationId = -1
    let stationSettingId = -1
    if (order.wholesaleStationSchedule && order.wholesaleStationSchedule.length > 0) {
      order.wholesaleStationSchedule.forEach((schedule: StationSchedule) => {
        if (schedule.wholesaleStationSetting !== null) {
          stationSettingId = schedule.wholesaleStationSetting.id
          stationId = schedule.wholesaleStationSetting.wholesaleStation.id
        }
      })
    }
    const station = this.props.stations.find(s => s.id === stationId);
    console.log(station);
    this.state = {
      station,
      stationSettingId
    }
  }

  componentWillReceiveProps(nextProps: any) {
    // if(nextProps.stations.length > 0) {
    //   const filteredStations = nextProps.stations.filter((s: Station) => {
    //     return s.wholesaleStationSettingList.length > 0
    //   })
    //   if(filteredStations.length > 0) {
    //     if(this.state.station === null) {
    //       this.setState({
    //         station: filteredStations[0],
    //         stationSettingId: filteredStations[0].wholesaleStationSettingList[0].id
    //       })
    //     }
    //   }
    // }
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let v = { ...values }
        const revisedTime = (v.revisedETATime === null) ? null : `${moment.utc(v.revisedETATime).format('YYYY-MM-DD HH:mm:ss')}`
        this.props.onSubmit({
          status: v.status,
          completionDate: v.completionDate ? moment.utc(v.completionDate).format('YYYY-MM-DD 00:00:00') : null,
          revisedTime,
          wholesaleStationSettingId: v.stationSettingId
        });
        form.resetFields();
      }
    })
  }

  onChangeStation = (id: number) => {
    const station = this.props.stations.find(s => s.id === id);
    const stationSettingId = station!.wholesaleStationSettingList[0].id;
    this.setState({
      station,
      stationSettingId
    }, () => {
      this.props.form.setFieldsValue({
        stationSettingId
      });
    })
  }

  render() {
    const { station, stationSettingId } = this.state
    const { order, stations } = this.props
    const { getFieldDecorator } = this.props.form
    console.log(station)

    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical" style={editFormStyle}>
        <Layout style={formLayoutStyle}>
          <Row>
            <Col md={4} style={pd20}>
              <InputLabel>Status</InputLabel>
              <FormItem>
                {getFieldDecorator('status', {
                  // rules: [{ required: true, message: 'This is required!' }],
                  initialValue: order.wholesaleOrderStatus
                })(
                  <ThemeSelect style={{ width: '100%' }}>
                    <Select.Option value="PLANNING">PLANNING</Select.Option>
                    <Select.Option value="SCHEDULED">SCHEDULED</Select.Option>
                    <Select.Option value="COMPLETED">COMPLETED</Select.Option>
                  </ThemeSelect>
                )}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col md={4} style={pd20}>
              <InputLabel>Revised ETA</InputLabel>
              <FormItem>
                {getFieldDecorator('revisedETATime', {
                  // rules: [{ required: true, message: 'This is required!' }],
                  initialValue: order.revisedTime ? moment(order.revisedTime) : null
                })(
                  <CustomDatePicker
                    placeholder={'MM/DD/YYYY'}
                    format={'MM/DD/YYYY'}
                    suffixIcon={<IconSvg type="calendar" viewBox="0 0 22 22" width={17} height={18} />}
                    style={{ width: '100%' }}
                  />
                )}
              </FormItem>
            </Col>
            <Col md={4} style={pd20}>
              <InputLabel>Revised ETA Time</InputLabel>
              <FormItem>
                {getFieldDecorator('revisedETATime', {
                  // rules: [{ required: true, message: 'This is required!' }],
                  initialValue: order.revisedTime ? moment(order.revisedTime) : null
                })(
                  <TimePicker format='HH:mm' style={{ width: '100%' }} />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col md={4} style={pd20}>
              <InputLabel>Completion Date</InputLabel>
              <FormItem>
                {getFieldDecorator('completionDate', {
                  // rules: [{ required: true, message: 'This is required!' }],
                  initialValue: order.completionDate ? moment(order.completionDate) : null
                })(
                  <CustomDatePicker
                    placeholder={'MM/DD/YYYY'}
                    format={'MM/DD/YYYY'}
                    suffixIcon={<IconSvg type="calendar" viewBox="0 0 22 22" width={17} height={18} />}
                    style={{ width: '100%' }}
                  />
                )}
              </FormItem>
            </Col>
          </Row>
          {/* {station !== null && ( */}
          <Row>
            <Col md={4} style={pd20}>
              <InputLabel>Station</InputLabel>
              <FormItem>
                {getFieldDecorator('station', {
                  // rules: [{ required: true, message: 'This is required!' }],
                  initialValue: station ? station.id : '',
                  onChange: this.onChangeStation
                })(
                  <ThemeSelect style={{ width: '100%' }}>
                    {stations.length > 0 && (
                      stations.map(s => {
                        if (s.wholesaleStationSettingList.length > 0) {
                          return <Select.Option value={s.id} key={`station-${s.id}`}>{s.name}</Select.Option>
                        }
                      })
                    )}
                  </ThemeSelect>
                )}
              </FormItem>
            </Col>
            <Col md={4} style={pd20}>
              <InputLabel>Manufacture Type</InputLabel>
              <FormItem>
                {getFieldDecorator('stationSettingId', {
                  // rules: [{ required: true, message: 'This is required!' }],
                  initialValue: stationSettingId > 0 ? stationSettingId : ''
                })(
                  <ThemeSelect>
                    {station && typeof station.wholesaleStationSettingList !== 'undefined' && station.wholesaleStationSettingList.length > 0 && (
                      station.wholesaleStationSettingList.map((s: StationSetting) => <Select.Option value={s.id} key={`man-type-${station.id}-${s.id}`}>{s.wholesaleManufactureType.name}</Select.Option>)
                    )}
                  </ThemeSelect>
                )}
              </FormItem>
            </Col>
          </Row>
          {/* )} */}
          <Row style={{ paddingLeft: 20 }}>
            {/* <Col md={8} style={pd20}>
              <ThemeButton shape="round" type="primary" style={{ width: 200, marginBottom: 20 }}>SET TO SCHEDULED</ThemeButton>                  
            </Col> */}
            {/* <Col md={8} style={pd20}>
              <ThemeButton shape="round" type="primary" style={{ width: 200, marginBottom: 20 }}>BACK TO PLANNING</ThemeButton>                  
            </Col> */}
            <Col md={8} style={pd20}>
              {/* <Popconfirm
                title="Are you sure to save?"
                onConfirm={this.handleSubmit}
                okText="Save"
                cancelText="Cancel"
              > */}
              <ThemeButton shape="round" type="primary" htmlType="submit" style={{ width: 200, marginBottom: 20 }}>
                SAVE CHANGES
                </ThemeButton>
              {/* </Popconfirm> */}
            </Col>
          </Row>
        </Layout>
      </Form>
    )
  }
}

export default Form.create<EditFormProps>()(EditForm)