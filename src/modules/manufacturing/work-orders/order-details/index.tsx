import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Row, Col } from 'antd'
import { RouteComponentProps } from 'react-router'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders'
import { CustomersInfoBody } from '~/modules/customers/components/customers-detail/customers-detail.style'
import OverviewHeader from '~/modules/manufacturing/work-orders/overview/header';
import { WorkOrdersHeader } from '../work-orders.style'
import { CustomerDetailsTitle as DetailsTitle, CustomerDetailsWrapper as DetailsWrapper, floatLeft, ThemeSpin } from '~/modules/customers/customers.style'
import EditForm from './edit-form'

type OrderDetailsProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

export class WorkOrderDetails extends React.PureComponent<OrderDetailsProps> {
  componentDidMount() {
    const orderId = this.props.match.params.orderId
    this.props.getCurrentWorkOrder(orderId)
    this.props.getAllStationsInOrder()
  }

  handleSubmit = (data: any) => {
    this.props.resetLoading()
    this.props.updateWorkOrderInfo({
      orderId: this.props.match.params.orderId,
      data
    })
  }

  render() {
    const {currentWorkOrder, stations, loading} = this.props
    return (
      <PageLayout currentTopMenu={'menu-Inventory-Work Orders'}>
        <WorkOrdersHeader>
          <OverviewHeader order={currentWorkOrder} />
        </WorkOrdersHeader>
        <CustomersInfoBody>
          <DetailsWrapper>
            <Row>
              <Col md={24}>
                <DetailsTitle style={floatLeft}>Order Details</DetailsTitle>
              </Col>
            </Row>
            {currentWorkOrder !== null && stations.length > 0 &&
              <ThemeSpin tip="Loading..." spinning={loading}>
                <EditForm order={currentWorkOrder} stations={stations} onSubmit={this.handleSubmit} />
              </ThemeSpin>
            }
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapState)(WorkOrderDetails))
