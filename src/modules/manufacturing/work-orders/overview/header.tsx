import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { format } from 'date-fns'
import { Layout, Row, Col, Table, Form, DatePicker, TimePicker, Select } from 'antd'
import {
  Flex,
  layoutStyle,
  Header,
  Name,
} from '~/modules/customers/components/customers-detail/customers-detail.style'
import { small, ThemeCheckbox, ThemeInput, ThemeSelect } from '~/modules/customers/customers.style'
import { HeaderRow, HeaderCol, HeaderColWrapper, LabelSpan, ValueSpan, TemplateCheckBox, ThemeLabel } from '../work-orders.style'
import { Order, Station } from '~/schema'
import { Scheduler } from 'rxjs'
import { OrdersDispatchProps, OrdersStateProps, OrdersModule } from '~/modules/orders'
import { FormComponentProps } from 'antd/lib/form'
import { RouteTitleStyle } from '~/modules/delivery/routings/routing.style'
import { Icon } from '~/components'
import moment, { Moment } from 'moment'
import { getOrderPrefix } from '~/common/utils'
import { history } from '~/store/history'

type OrverViewHeaderProps = OrdersDispatchProps &
  OrdersStateProps & {
    order: Order
    onWorkOrderHeaderChange?: Function
  }

export const getStationAndManufactureTypeFromOrder = (order: Order) => {
  let schedule = order ? order.wholesaleStationSchedule : []
  let type = 'N/A'
  let station = '--'
  if (!schedule || schedule.length == 0)
    return [type, station]
  let stationSetting = null
  let wholesaleStation = null;
  schedule.forEach(element => {
    if (element.wholesaleStationSetting != null) {
      stationSetting = element.wholesaleStationSetting
    }
    if (element.wholesaleStation != null) {
      wholesaleStation = element.wholesaleStation
    }

  });
  if (stationSetting != null && stationSetting.wholesaleManufactureType) {
    type = stationSetting.wholesaleManufactureType.name
    station = stationSetting.wholesaleStation.name
  } else {
    if (wholesaleStation != null) {
      station = wholesaleStation.name
    }
  }
  return [type, station, stationSetting]
}

export class OverviewHeader extends React.PureComponent<FormComponentProps & OrverViewHeaderProps> {
  state = {
    editable: false,
    orderName: null,
    revisedTime: null,
    revisedDate: null
  }

  componentDidMount() {
    if (this.props.stations) {
      this.props.getAllStationsInOrder()
    }
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
  }

  componentDidUpdate(prevProps: any) {
    if (prevProps && this.props.order) {
      this.setState({
        orderName: this.props.order.name,
        revisedTime: this.props.order.revisedTime,
        revisedDate: this.props.order.revisedTime
      })
    }
  }

  onCheckTemplate = (evt: any) => {
    const { order } = this.props
    let checked = evt.target.checked
    this.props.setWorkOrderToTemplate({ orderId: order.wholesaleOrderId, isTemplate: checked ? 1 : 0 })
  }

  onOrderNameChange = (event) => {
    this.setState({ orderName: event.target.value })
  }

  saveOrderName = () => {
    if (this.state.orderName != this.props.order.name) {
      let data = {
        orderId: this.props.order.wholesaleOrderId,
        name: this.state.orderName
      }
      this.props.updateWorkOrder(data)
    }
    this.setState({ editable: false })
  }

  handleEnterKeyup = (event: any) => {
    if (this.state.orderName && event.keyCode == 13) {
      this.saveOrderName()
    }
  }

  handleBlur = () => {
    this.saveOrderName()
  }

  editOrderName = () => {
    if (this.props.order) {
      this.setState({ editable: true, orderName: this.props.order.name })
    }
  }

  onDateChange = (key: string, value: Moment) => {
    const { order } = this.props
    let type = key
    let dateString = ''
    if (key == 'revisedETATime') {
      type = 'revisedTime'
      if (order.revisedTime !== null) {
        dateString = moment(order.revisedTime).format('YYYY-MM-DD') + ' ' + moment(value).format('HH:mm')
      } else {
        dateString = moment(value).format('YYYY-MM-DD HH:mm')
      }
    } else if (key == 'revisedETADate') {
      type = 'revisedTime'
      if (order.revisedTime !== null) {
        dateString = moment(value).format('YYYY-MM-DD') + ' 00:00'
      } else {
        dateString = moment(value).format('YYYY-MM-DD') + ' 00:00'
      }
    } else {
      dateString = moment(value).format('YYYY-MM-DD')
    }

    if (this.props.onWorkOrderHeaderChange) {
      this.props.onWorkOrderHeaderChange(type, dateString)
    }
  }

  getManufactureTypes = () => {
    const { order, stations } = this.props
    let result: any[] = []
    const stationSetting = getStationAndManufactureTypeFromOrder(order)[2]
    if (stationSetting) {
      const station = stationSetting.wholesaleStation
      if (station) {
        const stationId = station.id
        let currentStation: Station | null = null
        if (stations.length) {
          stations.forEach(el => {
            if (el.id == stationId) {
              currentStation = el
            }
          });
        }

        const settings = currentStation && currentStation.wholesaleStationSettingList ? currentStation.wholesaleStationSettingList : []
        settings.forEach((el, index) => {
          result.push(
            <Select.Option value={el.id}>
              {el.wholesaleManufactureType.name.toLowerCase() === 'special cuts' ? "Special Customer Request" : el.wholesaleManufactureType.name}
            </Select.Option>
          )
        });
      } else {
        return result
      }
    }
    return result;
  }

  onManufactureTypeChange = (val: number) => {
    if (this.props.onWorkOrderHeaderChange) {
      this.props.onWorkOrderHeaderChange('wholesaleStationSettingId', val)
    }
  }

  getWOStatusString = (order: Order) => {
    if (!order) return '--'
    if (order.wholesaleOrderStatus == 'REQUEST') return 'NEW ORDER'
    if (order.wholesaleOrderStatus == 'RECEIVED') return 'SCHEDULED'
    if (order.wholesaleOrderStatus == 'WIP') return 'IN PROGRESS'
    if (order.wholesaleOrderStatus == 'SHIPPED') return 'APPROVED'
    return order.wholesaleOrderStatus
  }

  render() {
    const { order, sellerSetting } = this.props
    const { form: { getFieldDecorator } } = this.props
    const stationSetting = getStationAndManufactureTypeFromOrder(order)[2]
    const stationSettingId = stationSetting && stationSetting.wholesaleManufactureType ? stationSetting.id : ''

    return (
      <>
        <Layout style={layoutStyle}>
          <Row type="flex" justify="space-between" style={{ padding: '27px 27px 0' }}>
            <Col xs={20}>
              <Flex>
                <LabelSpan>WORK ORDER: </LabelSpan>
              </Flex>
              {/* {!editable && */}
              <Name style={{ marginLeft: 0 }} onClick={this.editOrderName}>#{order ? order.wholesaleOrderId : ''}</Name>
              {/* } */}
              {/* {editable &&
                <Form.Item>
                  {getFieldDecorator('orderName', {
                    initialValue: orderName ? orderName : 'N/A',
                    rules: [{ required: true, message: 'Please fill out the order name!' }],
                  })(
                    <ThemeInput style={{ ...RouteTitleStyle, width: '50%' }} onChange={this.onOrderNameChange} onBlur={this.handleBlur} onKeyUp={this.handleEnterKeyup} />
                  )}
                </Form.Item>
              } */}
            </Col>
            <Col xs={4}>
              <TemplateCheckBox>
                <ThemeCheckbox checked={order ? order.isTemplate : false} onChange={this.onCheckTemplate}>
                  <ThemeLabel style={{ marginLeft: 7 }}>TEMPLATE</ThemeLabel>
                </ThemeCheckbox>
              </TemplateCheckBox>
            </Col>
          </Row>
          <HeaderRow style={{ marginTop: 15 }}>
            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>START DATE</LabelSpan>
                <ValueSpan>{order && order.startDate ? format(order.startDate, 'MM/DD/YY') : '--'}</ValueSpan>
              </HeaderColWrapper>
            </HeaderCol>
            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>END DATE</LabelSpan>
                <ValueSpan>{order && order.endDate ? format(order.endDate, 'MM/DD/YY') : '--'}</ValueSpan>
              </HeaderColWrapper>
            </HeaderCol>
            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>CLIENT REQUESTED ETA</LabelSpan>
                <DatePicker
                  style={{
                    marginTop: 5, width: '100%'
                  }}
                  onChange={this.onDateChange.bind(this, 'requestedTime')}
                  placeholder="MM/DD/YYYY"
                  format="MM/DD/YYYY"
                  allowClear={false}
                  value={order && order.requestedTime ? moment.utc(order.requestedTime) : undefined}
                  suffixIcon={< Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                />
              </HeaderColWrapper>
            </HeaderCol>
            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>RELATED SALES ORDER</LabelSpan>
                {order && order.relateOrderId &&
                  <ValueSpan className='related-link' onClick={() => { history.push(`/sales-order/${order.relateOrderId}`) }}>{`#${getOrderPrefix(sellerSetting, 'sales') + order.relateOrderId}`}</ValueSpan>
                }
              </HeaderColWrapper>
            </HeaderCol>
            <HeaderCol xs={4}>
              <HeaderColWrapper>
                <LabelSpan>STATUS</LabelSpan>
                <ValueSpan>{this.getWOStatusString(order)}</ValueSpan>
              </HeaderColWrapper>
            </HeaderCol>
          </HeaderRow>
          <HeaderRow style={{ marginTop: -1 }}>
            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>MANUFACTURING TYPE</LabelSpan>
                <ThemeSelect
                  style={{ width: '100%' }}
                  value={stationSettingId}
                  onChange={this.onManufactureTypeChange}
                >
                  {this.getManufactureTypes()}
                </ThemeSelect>
                {/* <ValueSpan>{getStationAndManufactureTypeFromOrder(order)[0]}</ValueSpan> */}
              </HeaderColWrapper>
            </HeaderCol>
            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>PROCESSING ETA TIME</LabelSpan>
                <TimePicker
                  format='HH:mm'
                  style={{ width: '100%' }}
                  value={order && order.revisedTime ? moment.utc(order.revisedTime) : moment().utcOffset(0).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })}
                  onChange={this.onDateChange.bind(this, 'revisedETATime')}
                  allowClear={false} />
              </HeaderColWrapper>
            </HeaderCol>
            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>PROCESSING ETA</LabelSpan>
                <DatePicker
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={<Icon type="calendar" viewBox="0 0 22 22" width={17} height={18} />}
                  style={{ width: '100%' }}
                  value={order && order.revisedTime ? moment.utc(order.revisedTime) : undefined}
                  allowClear={false}
                  onChange={this.onDateChange.bind(this, 'revisedETADate')}
                />
              </HeaderColWrapper>
            </HeaderCol>

            <HeaderCol xs={5}>
              <HeaderColWrapper>
                <LabelSpan>COMPLETION DATE</LabelSpan>
                <DatePicker
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={<Icon type="calendar" viewBox="0 0 22 22" width={17} height={18} />}
                  style={{ width: '100%' }}
                  value={order && order.completionDate ? moment.utc(order.completionDate) : undefined}
                  onChange={this.onDateChange.bind(this, 'completionDate')}
                  allowClear={false}
                />
              </HeaderColWrapper>
            </HeaderCol>
            <HeaderCol xs={4}>
              <HeaderColWrapper>
                <LabelSpan>STATION</LabelSpan>
                <ValueSpan>{getStationAndManufactureTypeFromOrder(order)[1]}</ValueSpan>
              </HeaderColWrapper>
            </HeaderCol>
          </HeaderRow>
        </Layout>
      </>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapState)(Form.create()(OverviewHeader)))
