import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import { RouteComponentProps } from 'react-router'
import { CustomersInfoBody } from '~/modules/customers/components/customers-detail/customers-detail.style'
import OverviewHeader from '~/modules/manufacturing/work-orders/overview/header'
import AddOrderItemModal from '~/modules/customers/nav-sales/order-detail/modals/add-item'

import {
  WorkOrdersHeader,
  oneRowButtons,
  ButtonPad,
  SubLabelSpan,
  Rectangle,
  resourceRow,
  resNo,
} from '../work-orders.style'
import {
  CustomerDetailsTitle as DetailsTitle,
  CustomerDetailsWrapper as DetailsWrapper,
  ThemeOutlineButton,
  floatLeft,
  ThemeButton,
  ThemeTextArea,
  FlexCenter,
  floatRight,
  ThemeModal,
  ThemeTable,
} from '~/modules/customers/customers.style'
import ProductModal from './../../../customers/sales/cart/modals/product-modal'
import { Row, Col, Icon, Popconfirm, Divider, Button } from 'antd'
import Materials from './tables/materials'
import Products from './tables/products'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders'
import AssignWorkerModal from './../overview/mdoals/assign-worker'
import { AssignedClientWrapper } from '~/modules/pricesheet/pricesheet-detail.style'
import { PayloadIcon } from '~/components/elements/elements.style'
import { Icon as IconSvg } from '~/components/icon/'
import { printWindow } from '~/common/utils'
import { PrintModalHeader } from '~/modules/customers/sales/_style'
import { PrintPickSheet } from '~/modules/customers/sales/cart/print'

type OverviewProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

export class Overview extends React.PureComponent<OverviewProps> {
  assignModalColumn: (
    | {
      dataIndex: string
      render: (text: string, record: any) => string
      title?: undefined
      key?: undefined
      width?: undefined
    }
    | {
      title: string
      dataIndex: string
      key: string
      width: string
      render: (text: number, record: any) => JSX.Element
    }
  )[]
  constructor(props: OverviewProps) {
    super(props)

    this.state = {
      newModalVisible: false,
      newProductModalVisible: false,
      assignModalVisible: false,
      assignedWorkers: [],
      orderId: this.props.match.params.orderId,
      palletIndex: -1,
      palletOrderItemId: -1,
      orderNote: null,
      printPickSheetReviewShow: false,
      printPickSheetRef: null,
      searchInput: '',
      receiveMaterials: false,
      sendProductWarehouse: false,
    }

    this.assignModalColumn = [
      {
        dataIndex: 'firstName',
        render: (text: string, record: any) => {
          return record.firstName + ' ' + record.lastName
        },
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        width: '10%',
        render: (text: number, record: any) => {
          return (
            <Popconfirm title="Sure to unassign?" onConfirm={() => this.onUnassignWorker(record.userId)}>
              <a>
                <PayloadIcon>
                  <IconSvg type="close" viewBox="0 0 20 28" width={15} height={14} />
                </PayloadIcon>
              </a>
            </Popconfirm>
          )
        },
      },
    ]
  }

  componentDidMount() {
    const orderId = this.props.match.params.orderId
    this.props.getCurrentWorkOrder(orderId)
    this.props.getCompanyProductAllTypes()
    this.props.getWorkers()
    if (!this.props.vendors || this.props.vendors.length == 0) this.props.getVendors() //get vendors to show the company names in pallet table
    if (!this.props.companyName || !this.props.logo) this.props.getSellerSetting()
  }

  componentDidUpdate(prevProps: OverviewProps) {
    if (
      prevProps &&
      prevProps.currentWorkOrderId != 0 &&
      prevProps.currentWorkOrderId != this.props.currentWorkOrderId
    ) {
      this.props.history.push(`/manufacturing/${this.props.currentWorkOrderId}/wo-overview`)
    }
    if (this.props.currentWorkOrder) {
      const order = this.props.currentWorkOrder
      if (this.state.orderNote === null) {
        this.setState({ orderNote: order.manufactureNote })
      }
      let salesOrderStatus = ''
      let purchaseOrderStatus = ''
      if (order.childOrders) {
        order.childOrders.forEach((el) => {
          if (el.wholesaleClient && el.wholesaleClient.type == 'CUSTOMER') {
            salesOrderStatus = el.wholesaleOrderStatus
          }
          if (el.wholesaleClient && el.wholesaleClient.type == 'VENDOR') {
            purchaseOrderStatus = el.wholesaleOrderStatus
          }
        })
      }
      this.setState({
        receiveMaterials:
          salesOrderStatus === 'SHIPPED' ||
          order.wholesaleOrderStatus === 'WIP' ||
          order.wholesaleOrderStatus === 'COMPLETED' ||
          order.wholesaleOrderStatus === 'SHIPPED',
        sendProductWarehouse: purchaseOrderStatus === 'RECEIVED',
      })
    }
  }

  openAddItemToMaterial = () => {
    this.setState({
      newModalVisible: true,
    })
  }

  handleClose = () => {
    this.setState({
      newModalVisible: false,
      newProductModalVisible: false,
    })
  }

  handleAddOrderItem = (item: any) => {
    const orderId = this.props.match.params.orderId
    if (typeof item.itemId === 'string' && item.itemId.indexOf('lotItemId-') === 0) {
      item.itemId = item.itemId.replace('lotItemId-', '')
      this.props.addWorkOrderItemByOrderItemId({ orderId: +orderId, itemId: item.wholesaleOrderItemId, type: 1 })
      return
    }

    if (item != null && item.itemId && !item.wholesaleOrderItemId) {
      this.props.addWorkOrderItemByProductId({ orderId: +orderId, itemId: item.itemId, type: 1 })
    } else if (item != null && !item.itemId && item.wholesaleOrderItemId) {
      this.props.addWorkOrderItemByOrderItemId({ orderId: +orderId, itemId: item.wholesaleOrderItemId, type: 1 })
    }

    // this.handleClose()
  }

  openAddItemToProduct = () => {
    this.setState({
      newProductModalVisible: true,
    })
  }

  handleSelectProductOk = () => {
    this.setState({
      newProductModalVisible: false,
    })
  }

  handleProductItem = (item: any) => {
    const orderId = this.props.match.params.orderId
    console.log(item)
    let itemId = item.itemId

    if (itemId) {
      this.props.resetLoading()
      this.props.addWorkOrderItemByProductId({ orderId: +orderId, itemId: itemId, type: 2 })
    }
    // this.handleClose()
  }

  openAssignModal = () => {
    this.setState({ assignModalVisible: true })
  }

  onAssignWorker = (value: any) => {
    console.log('assigning customer', value)
    const { currentWorkOrder } = this.props
    let data = {
      orderId: currentWorkOrder.wholesaleOrderId,
      userId: value,
    }
    this.props.assignWokerToWO(data)
    this.setState({ assignModalVisible: false })
  }

  onUnassignWorker = (userId: number) => {
    console.log('unassign this client', userId)
    const { currentWorkOrder } = this.props
    let data = {
      orderId: currentWorkOrder.wholesaleOrderId,
      userId: userId,
    }
    this.props.unAssignWorkerFromWO(data)
  }

  onCloseAssignModal = () => {
    this.setState({ assignModalVisible: false })
  }

  getAvailableWorkers = () => {
    const { workers, currentWorkOrder } = this.props
    const stationSchedule = currentWorkOrder ? currentWorkOrder.wholesaleStationSchedule : []
    const assignedWorkers = stationSchedule
      .filter((el: any) => {
        return el.wholesaleStationSetting === null && el.user !== null
      })
      .map((el) => el.user)

    if (!assignedWorkers || !workers) return []
    const assignedIds: number[] = assignedWorkers.map((el) => el.userId)
    const availableWorkers: any[] = []
    workers.forEach((el) => {
      if (assignedIds.indexOf(el.userId) < 0) {
        availableWorkers.push(el)
      }
    })

    return [assignedWorkers, availableWorkers]
  }

  getUnitsPlannedAndActual = (order: Order, type: number) => {
    if (!order || order.orderItemList.length == 0) return [0, 0]
    let orderItemList = order.orderItemList.filter((el) => el.type == type)
    let total = 0
    let actual = 0
    orderItemList.forEach((el) => {
      total += el.unitsPlanned
      actual += el.unitsActual
    })
    return [total, actual]
  }

  duplicateWorkOrder = () => {
    const orderId = this.props.match.params.orderId
    if (orderId) {
      this.props.duplicateWorkOrder(+orderId)
    }
  }

  onPalletShow = (palletIndex: number, orderItemId: number) => {
    console.log('pallet index, order item id chnage', palletIndex, orderItemId)
    this.setState({
      palletIndex: palletIndex,
      palletOrderItemId: orderItemId,
    })
  }

  onSave = () => {
    //
  }

  updateOrderStatus = (status: string) => {
    const { orderNote } = this.state
    const { currentWorkOrder } = this.props
    let order: any = {
      orderId: currentWorkOrder.wholesaleOrderId,
      status: status,
      manufactureNote: orderNote,
    }
    this.props.updateWorkOrder(order)
  }

  selectOrderItemForPallet = () => {
    const { currentWorkOrder } = this.props
    const { palletIndex } = this.state
    if (currentWorkOrder && palletIndex >= 0) {
      let orderItemList = currentWorkOrder.orderItemList.filter((el) => el.type == 2)
      let orderItem = orderItemList[palletIndex]
      return orderItem
    }
    return null
  }
  onNoteChange = (evt: any) => {
    this.setState({ orderNote: evt.target.value })
  }

  saveNote = () => {
    const { currentWorkOrder } = this.props
    if (currentWorkOrder) {
      let data = {
        orderId: currentWorkOrder.wholesaleOrderId,
        manufactureNote: this.state.orderNote,
      }
      this.props.updateWorkOrder(data)
    }
  }

  onClickPrintPickSheet = () => {
    this.setState({
      printPickSheetReviewShow: true,
    })
  }

  closePrintModal = () => {
    this.setState({
      printPickSheetReviewShow: false,
    })
  }

  onSearch = (text: string) => {
    console.log(text)
    this.setState({
      searchInput: text,
    })
  }

  onHeaderChange = (type: string, value: string) => {
    const { currentWorkOrder } = this.props
    if (currentWorkOrder) {
      let data = {
        orderId: currentWorkOrder.wholesaleOrderId,
      }
      data[type] = value
      this.props.updateWorkOrder(data)
    }
  }

  toggleReceiveMaterials = (status: boolean, orderId: any) => {
    const { currentWorkOrder } = this.props
    const orderItemList = currentWorkOrder.orderItemList
    const itemIds = orderItemList
      .filter((el) => {
        return el.type == 1
      })
      .map((el) => {
        return { workOrderItemId: el.wholesaleOrderItemId }
      })
    this.setState({ receiveMaterials: status }, () => {
      this.props.receiveMaterials({ orderId, status, itemIds })
    })
  }

  toggleSendProductWarehouse = (status: boolean, orderId: any) => {
    this.setState({ sendProductWarehouse: status }, () => {
      this.props.sendProductWarehouse({ orderId: orderId, status: status })
    })
  }

  isStartWorkDisabled = () => {
    const { currentWorkOrder } = this.props
    const { receiveMaterials } = this.state
    if (!currentWorkOrder || !receiveMaterials) return true
    const orderItemList = currentWorkOrder.orderItemList
    const materialsIn = orderItemList.filter((el) => {
      return el.unitsPlanned > 0 && el.type == 1
    })
    const materialsOut = orderItemList.filter((el) => {
      return el.unitsPlanned > 0 && el.type == 2
    })
    if (materialsIn.length && materialsOut.length) return false
    else return true
  }

  render() {
    const {
      newModalVisible,
      newProductModalVisible,
      assignModalVisible,
      palletIndex,
      palletOrderItemId,
      orderNote,
      printPickSheetReviewShow,
      printPickSheetRef,
      searchInput,
    } = this.state
    const { currentWorkOrder } = this.props
    const workers = this.getAvailableWorkers()

    const printContent: any = (type: number) => {
      let content = printPickSheetRef
      //TODO: ask tao for status
      // this.updateOrderStatus('PICKING')

      return content
    }

    return (
      <PageLayout currentTopMenu={'menu-Inventory-Work Orders'}>
        <WorkOrdersHeader>
          <OverviewHeader order={currentWorkOrder} onWorkOrderHeaderChange={this.onHeaderChange} />
        </WorkOrdersHeader>
        <CustomersInfoBody>
          <DetailsWrapper style={{ border: 'none' }}>
            <Divider type="horizontal" style={{ marginTop: 0 }} />
            <Row>
              <Col md={24}>
                <DetailsTitle style={floatLeft}>Overview</DetailsTitle>
                {currentWorkOrder &&
                  (currentWorkOrder.wholesaleOrderStatus == 'REQUEST' ||
                    currentWorkOrder.wholesaleOrderStatus == 'PLANNING') && (
                    <ThemeButton style={oneRowButtons} onClick={this.updateOrderStatus.bind(this, 'SCHEDULED')}>
                      Schedule Order
                    </ThemeButton>
                  )}
                {currentWorkOrder &&
                  (currentWorkOrder.wholesaleOrderStatus == 'SCHEDULED' ||
                    currentWorkOrder.wholesaleOrderStatus == 'RECEIVED') && (
                    <>
                      <ThemeButton
                        style={oneRowButtons}
                        onClick={this.updateOrderStatus.bind(this, 'WIP')}
                        disabled={this.isStartWorkDisabled()}
                      >
                        Start Work
                      </ThemeButton>
                      <Button
                        style={oneRowButtons}
                        onClick={this.updateOrderStatus.bind(this, 'REQUEST')}
                        disabled={this.state.receiveMaterials}
                      >
                        Back to New Order
                      </Button>
                    </>
                  )}
                {currentWorkOrder && currentWorkOrder.wholesaleOrderStatus == 'WIP' && (
                  <>
                    <ThemeButton style={oneRowButtons} onClick={this.updateOrderStatus.bind(this, 'COMPLETED')}>
                      Mark complete
                    </ThemeButton>
                    <Button style={oneRowButtons} onClick={this.updateOrderStatus.bind(this, 'REQUEST')}>
                      Back to New Order
                    </Button>
                  </>
                )}
                {currentWorkOrder && currentWorkOrder.wholesaleOrderStatus == 'COMPLETED' && (
                  <>
                    <ThemeButton style={oneRowButtons} onClick={this.updateOrderStatus.bind(this, 'SHIPPED')}>
                      Approve work
                    </ThemeButton>
                    <Button style={oneRowButtons} onClick={this.updateOrderStatus.bind(this, 'WIP')}>
                      Back to In Progress
                    </Button>
                  </>
                )}
                {currentWorkOrder && currentWorkOrder.wholesaleOrderStatus == 'SHIPPED' && (
                  <>
                    <ThemeButton style={oneRowButtons} disabled={true}>
                      Approved
                    </ThemeButton>
                    <Button
                      style={oneRowButtons}
                      onClick={this.updateOrderStatus.bind(this, 'WIP')}
                      disabled={this.state.sendProductWarehouse}
                    >
                      Back to In Progress
                    </Button>
                  </>
                )}
                <ButtonPad />
              </Col>
            </Row>
            <Row type="flex" align="bottom">
              <Col md={12} style={{ paddingRight: 16 }}>
                <Row type="flex" justify="end">
                  <Col offset={21} xs={4} />
                </Row>
                <SubLabelSpan>Notes</SubLabelSpan>
                <Rectangle>
                  <ThemeTextArea
                    onChange={this.onNoteChange}
                    onBlur={this.saveNote}
                    style={{ height: 64 }}
                    value={orderNote}
                  />
                </Rectangle>
              </Col>
              <Col md={12} style={{ paddingLeft: 16 }}>
                <Row type="flex" justify="end">
                  <Col offset={21} xs={4}>
                    <ThemeOutlineButton
                      onClick={this.openAssignModal}
                      style={{ ...floatRight, ...{ marginTop: 10, marginRight: 10 } }}
                      shape="round"
                    >
                      <Icon type="plus-circle" />
                      Assign To
                    </ThemeOutlineButton>
                  </Col>
                </Row>
                <SubLabelSpan>Resouces / People</SubLabelSpan>
                <Rectangle>
                  {workers[0].map((user: any, i: number) => (
                    <Row style={resourceRow} key={`resource-${i}`}>
                      <Col md={2}>
                        <span style={resNo}>&nbsp;{i + 1}.</span>
                      </Col>
                      <Col md={21}>{user.firstName + ' ' + user.lastName}</Col>
                      <Col md={1}>
                        <Popconfirm
                          title="Sure to unassign?"
                          okText="Delete"
                          onConfirm={this.onUnassignWorker.bind(this, user.userId)}
                        >
                          <FlexCenter style={{ cursor: 'pointer' }}>
                            <Icon type="close-circle" theme="filled" style={{ color: 'black' }} />
                          </FlexCenter>
                        </Popconfirm>
                      </Col>
                    </Row>
                  ))}
                </Rectangle>
              </Col>
            </Row>
            {currentWorkOrder &&
              (currentWorkOrder.wholesaleOrderStatus == 'REQUEST' ||
                currentWorkOrder.wholesaleOrderStatus == 'PLANNING') && (
                <Row type="flex" justify="end">
                  <ThemeOutlineButton
                    style={{ ...floatRight, ...{ marginTop: 15, marginRight: 10 } }}
                    shape="round"
                    onClick={this.openAddItemToMaterial}
                  >
                    <Icon type="plus-circle" />
                    Add Item
                  </ThemeOutlineButton>
                </Row>
              )}
            <Materials
              companyProductTypes={this.props.companyProductTypes}
              getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
              setCompanyProductType={this.props.setCompanyProductType}
              deleteProductType={this.props.deleteProductType}
              updateProductType={this.props.updateProductType}
              getUnitsPlannedAndActual={this.getUnitsPlannedAndActual}
              handleSave={this.props.updateWorkOrderItem}
              handleRemove={this.props.deleteWorkOrderItem}
              order={currentWorkOrder}
              receiveMaterials={this.state.receiveMaterials}
              toggleReceiveMaterials={this.toggleReceiveMaterials}
            />
            {currentWorkOrder &&
              (currentWorkOrder.wholesaleOrderStatus == 'REQUEST' ||
                currentWorkOrder.wholesaleOrderStatus == 'PLANNING') && (
                <Row type="flex" justify="end">
                  <ThemeOutlineButton
                    style={{ ...floatRight, ...{ marginTop: 15, marginRight: 10 } }}
                    shape="round"
                    onClick={this.openAddItemToProduct}
                  >
                    <Icon type="plus-circle" />
                    Add Item
                  </ThemeOutlineButton>
                </Row>
              )}
            <Row>
              <Col>
                <Products
                  companyProductTypes={this.props.companyProductTypes}
                  getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
                  setCompanyProductType={this.props.setCompanyProductType}
                  deleteProductType={this.props.deleteProductType}
                  updateProductType={this.props.updateProductType}
                  getUnitsPlannedAndActual={this.getUnitsPlannedAndActual}
                  getCurrentWorkOrder={this.props.getCurrentWorkOrder}
                  handleSave={this.props.updateWorkOrderItem}
                  handlePalletShow={this.onPalletShow}
                  handleRemove={this.props.deleteWorkOrderItem}
                  order={currentWorkOrder}
                  palletIndex={palletIndex}
                  toggleSendProductWarehouse={this.toggleSendProductWarehouse}
                  sendProductWarehouse={this.state.sendProductWarehouse}
                />
              </Col>
            </Row>
          </DetailsWrapper>
          {newModalVisible && (
            <ThemeModal
              title="Add Item to Work Order"
              centered
              visible={newModalVisible}
              onCancel={this.handleClose}
              okButtonProps={{ style: { display: 'none' } }}
              cancelButtonProps={{ style: { display: 'none' } }}
              footer={null}
              width={'75%'}
              style={{ minWidth: 1000, maxHeight: '95vh' }}
              className="add-cart-item-modal"
            >
              {newModalVisible && (
                <AddOrderItemModal
                  wo={true}
                  onSelect={this.handleAddOrderItem}
                  visible={newModalVisible}
                  salesType="SELL"
                  onClose={this.handleClose}
                  addOffItem={null}
                  showProductModal={null}
                  insertItemPosition={-1}
                />
              )}
            </ThemeModal>
          )}
          {newProductModalVisible && (
            <ThemeModal
              closable={false}
              keyboard={true}
              okText="Close [esc]"
              okButtonProps={{ shape: 'round' }}
              cancelButtonProps={{ style: { display: 'none' } }}
              width={'75%'}
              visible={newProductModalVisible}
              onOk={this.handleSelectProductOk}
              onCancel={this.handleSelectProductOk}
            >
              <ProductModal
                isAdd={true}
                selected={searchInput}
                onSearch={this.onSearch}
                onSelect={this.handleProductItem}
                salesType="PURCHASE"
              />
            </ThemeModal>
          )}
          <AssignWorkerModal
            visible={assignModalVisible}
            onOk={this.onAssignWorker}
            onCancel={this.onCloseAssignModal}
            theme={this.props.theme}
            workers={workers[1]}
          >
            <div>
              <label>Assigned Workers</label>
              <br />
              <AssignedClientWrapper>
                <ThemeTable
                  columns={this.assignModalColumn}
                  dataSource={workers[0]}
                  rowKey="userId"
                  showHeader={false}
                />
              </AssignedClientWrapper>
            </div>
          </AssignWorkerModal>
          <ThemeModal
            width={1080}
            footer={null}
            visible={printPickSheetReviewShow}
            onCancel={this.closePrintModal.bind(this, 1)}
            wrapClassName="print-modal"
          >
            <PrintModalHeader>
              <ThemeButton
                size="large"
                onClick={() => printWindow('WOPrintPickSheetModal', printContent.bind(this, 1))}
              >
                <Icon type="printer" theme="filled" />
                Print Pick Sheet
              </ThemeButton>
              <div className="clearfix"></div>
            </PrintModalHeader>
            <div id={'WOPrintPickSheetModal'}>
              <PrintPickSheet
                isWorkOrder={true}
                orderItems={currentWorkOrder ? currentWorkOrder.orderItemList : []}
                currentOrder={currentWorkOrder}
                companyName={this.props.companyName}
                logo={this.props.logo}
                ref={(el) => {
                  this.setState({ printPickSheetRef: el })
                }}
              />
              {/* <WOPrintPickSheet
                currentOrder={currentWorkOrder}
                companyName={this.props.companyName}
                logo={this.props.logo}
                ref={(el) => {
                  this.setState({ printPickSheetRef: el })
                }}
              /> */}
            </div>
          </ThemeModal>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapState)(Overview))
