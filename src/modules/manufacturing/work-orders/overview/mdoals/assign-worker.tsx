/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Button, Modal, Select, message } from 'antd'
import Form, { FormComponentProps } from 'antd/lib/form';

import { Theme } from '~/common'
import AutoComplete from 'antd/es/auto-complete'
import { DialogFooter } from '~/modules/orders/components/orders-header.style';

type AssignWorkerModalProps = {
    visible: boolean
    onOk: (name: string) => void
    onCancel: () => void
    workers: any[]
    theme: Theme
}

class AssignWorkerModal extends React.PureComponent<FormComponentProps & AssignWorkerModalProps> {

    state = {
      onSelected: -1
    }
  
    componentDidUpdate(prevProps: any) {
      if(!this.props.visible) {
        this.props.form.resetFields()
      }
    }
  
    renderCompanyName = (worker: any, index: number) =>{
      return (
        <Select.Option key={worker.userId} text={worker.userid}>
          {worker.firstName + ' ' + worker.lastName}
        </Select.Option>
      );
    }
  
    handleFilterOption = (input: any, option: any) => {
      return option.props.children.toUpperCase().indexOf(input.toUpperCase()) !== -1
    };
  
    onSelect = (value: number, option: any) => {
      this.setState( {
        onSelected:value
        }
      )
    }
  
    render() {
      const {form: { getFieldDecorator }} = this.props
      
      return (
        <Modal
          visible={this.props.visible}
          onCancel={this.handleCancel}
          footer={[
            <DialogFooter key="plus">
              <Button
                type="primary"
                icon="plus"
                style={{
                  border: `1px solid ${this.props.theme.primary}`,
                  backgroundColor: this.props.theme.primary,
                }}
                onClick={this.handleOk}
              >
                Assign
              </Button>
              <Button onClick={this.handleCancel}>CANCEL</Button>
            </DialogFooter>,
          ]}
        >
          <label>Worker Name</label>
          <br />
          <Form.Item>
            {getFieldDecorator('name', {
              rules: [
                { required: true, message: `Worker name is required!` },
              ],
            })(
              <AutoComplete
              size="large"
              style={{ width: '100%' }}
              dataSource={this.props.workers && this.props.workers.map(this.renderCompanyName)}
              filterOption={this.handleFilterOption}
              onSelect={this.onSelect}
              placeholder={`Select a worker name`}
              />
            )}
          </Form.Item>
          {this.props.children} 
        </Modal>
      )
    }
    handleOk = (e: React.MouseEvent<HTMLElement>) => {
      e.preventDefault()
      const { form, onOk } = this.props
      form.validateFields((err, values) => {
        if (!err) {
          if(this.state.onSelected != -1 && values.name == this.state.onSelected){
            onOk(values.name)
          }else {
            message.error('Please select a client name at list');
          }
  
        }
      })
    }
  
    handleCancel = (e: React.MouseEvent<HTMLElement>) => {
      e.preventDefault()
      const { form, onCancel } = this.props
      onCancel()
      form.resetFields()
    }
  }
  
  
  
  export default Form.create<FormComponentProps & AssignWorkerModalProps>()(AssignWorkerModal)
