import * as React from 'react'
import { Carousel, Card, Col, Row } from 'antd'
import { ThemeModal, InputLabel, ThemeTextArea } from '~/modules/customers/customers.style'
import img1 from '../../../images/img1.png'
import img2 from '../../../images/img2.png'
import {
  ThemeLabel,
  ThemeCarousel,
  ScrollWrapper,
  DetailImage,
  ArrowWrapper,
  Instruction,
} from '../../work-orders.style'
import PropTypes from 'prop-types'
import ScrollMenu from 'react-horizontal-scrolling-menu'
import './scroll-menu.css'
import {
  ManufacturingStateProps,
  ManufacturingModule,
  ManufacturingDispatchProps,
} from '~/modules/manufacturing/manufacturing.module'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Order } from '~/schema'

// const list = [
//   'https://img.favpng.com/24/25/18/beefsteak-raw-meat-png-favpng-SXNB2dmjLFe0QjA6hCFZ5h36C.jpg',
//   'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSb7H0SLR8PlbfFPCLAs9kyWb9kZoJy_Xnlvo1ovCh5fGWKzNSw&usqp=CAU',
//   'https://pluspng.com/img-png/meat-png-beef-meat-800.png',
//   'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQQXRxWNV8cTQtURTjmkjqTfa52fWmcXMkLlpdwpOKsqh5xyDAj&usqp=CAU',
//   'https://img.favpng.com/24/25/18/beefsteak-raw-meat-png-favpng-SXNB2dmjLFe0QjA6hCFZ5h36C.jpg',
//   'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSb7H0SLR8PlbfFPCLAs9kyWb9kZoJy_Xnlvo1ovCh5fGWKzNSw&usqp=CAU',
//   'https://pluspng.com/img-png/meat-png-beef-meat-800.png',
//   'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQQXRxWNV8cTQtURTjmkjqTfa52fWmcXMkLlpdwpOKsqh5xyDAj&usqp=CAU'
// ]
const MenuItem = ({ url, selected }) => {
  return (
    <div className={`menu-item ${selected ? 'active' : ''}`}>
      <img src={url} width={'150px'} height={'130px'} />
    </div>
  )
}

export const Menu = (list: Array, selected: string) =>
  list.map((el, index) => {
    return <MenuItem url={el} key={index} selected={selected} />
  })

const Arrow = (text: string, className: string) => {
  return <ArrowWrapper className={className}>{text}</ArrowWrapper>
}
Arrow.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
}

export const ArrowLeft = Arrow('<', 'arrow-prev')
export const ArrowRight = Arrow('>', 'arrow-next')

type GalleryViewProps = ManufacturingDispatchProps &
  ManufacturingStateProps & {
    visible: boolean
    handleOk: any
    handleCancel: any
    itemId: number | null
    specification: string
    order: Order
  }

export class GalleryView extends React.PureComponent<GalleryViewProps> {
  menu: null
  list: any

  constructor(props: GalleryViewProps) {
    super(props)

    this.menu = null
    // this.menuItems = Menu(list, this.state.selected);
    this.list = []
  }

  state = {
    selected: null,
    hideSingleArrow: true,
  }

  componentDidUpdate(prevProps: GalleryViewProps) {
    // console.log("this.props", this.props.itemId)
    if (prevProps && prevProps.itemId != this.props.itemId) {
      if (this.props.itemId) {
        this.props.getGalleryDocuments(this.props.itemId)
      }
    }

    if (JSON.stringify(prevProps.galleryDocuments) !== JSON.stringify(this.props.galleryDocuments)) {
      this.setState({
        selected: this.props.galleryDocuments.length > 0 ? this.props.galleryDocuments[0].url : null,
      })
    }
  }

  onSelectImg = (url: string) => {
    this.setState({
      selected: url,
    })
  }

  handleOk = () => {
    this.setState({
      selected: null,
    })
    this.props.handleOk()
  }

  onSelect = (index: number) => {
    // console.log(`onSelect: ${index}`);
    if (this.list.length > index) this.setState({ selected: this.list[index] })
  }

  initMenuItems = () => {
    const { galleryDocuments } = this.props
    // console.log(galleryDocuments)
    let result = galleryDocuments.map((el) => el.url)
    if (result.length > 0) {
      this.menuItems = Menu(result, result[0])
      this.list = result
    }
  }

  render() {
    const { visible, order, galleryDocuments, specification } = this.props
    const { selected } = this.state
    this.initMenuItems()

    return (
      <ThemeModal
        title={
          <>
            <InputLabel>WORK ORDER NAME: </InputLabel>
            <ThemeLabel>{order ? `#${order.wholesaleOrderId}` : ''}</ThemeLabel>
          </>
        }
        visible={visible}
        width={1000}
        onOk={this.handleOk}
        onCancel={this.handleOk}
        footer={false}
      >
        <Row>
          <Instruction span={8}>
            <ThemeTextArea value={specification} />
          </Instruction>
          <Col span={16}>
            <DetailImage>
              <img src={selected} width={'100%'} />
            </DetailImage>
            <ScrollWrapper>
              {galleryDocuments.length > 0 && (
                <ScrollMenu
                  arrowLeft={ArrowLeft}
                  arrowRight={ArrowRight}
                  data={this.menuItems}
                  dragging={true}
                  hideArrows={true}
                  hideSingleArrow={true}
                  onSelect={this.onSelect}
                  ref={(el) => (this.menu = el)}
                  scrollToSelected={false}
                  selected={selected}
                  wheel={true}
                />
              )}
            </ScrollWrapper>
          </Col>
        </Row>
      </ThemeModal>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.manufacturing
export default withTheme(connect(ManufacturingModule)(mapStateToProps)(GalleryView))
