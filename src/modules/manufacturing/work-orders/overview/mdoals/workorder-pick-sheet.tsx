import React from 'react'
import { OrderDetail, OrderItem, Order } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode';
import {
  HeaderTextLeftBox,
  HeaderTextRightBox,
  HeaderTextCenterBox,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable
} from '~/modules/customers/sales/cart/print/_styles'
import Moment from 'react-moment'
import _moment from 'moment'
import { CompanyInfoWrapper, LogoWrapper } from '~/modules/orders/components/OrderTableHeader/styles';
import { defaultLogoStyle } from '~/modules/setting/theme.style';
import { formatAddress } from '~/common/utils';
import { Icon } from '~/components';

interface PrintPickSheetProps {
  currentOrder: Order
  companyName: string
  logo: string
}


export class WOPrintPickSheet extends React.PureComponent<PrintPickSheetProps> {
  state = {
    visibleAlert: false,
    selectedRowKeys: []
  }

  render() {
    const { currentOrder } = this.props;
    const columns: Array<any> = [
      {
        title: 'TYPE',
        dataIndex: 'wholesaleOrderItemStatus',
        key: 'wholesaleOrderItemStatus',
        align: 'center',
        edit_width: 30,
      },
      {
        title: 'UNIT',
        dataIndex: 'quantity',
        key: 'unit',
        align: 'center',
        edit_width: 30,
      },
          {
            title: 'UOM',
            dataIndex: 'uom',
            key: 'uom',
            align: 'center',
            edit_width: 30,
          },

          {
            title: 'LOT',
            dataIndex: 'lotId',
            key: 'lotId',
            align: 'center',
            edit_width: 120,
            render: (data: number) => {
              return data !== null ? (
                <Barcode value={data.toString()} width={1} height={30} margin={0} displayValue	={false}/>
              ) : (
               <></>
              )
            }
          },
      {
        title: 'LOCATION',
        dataIndex: 'location',
        align: 'center',
        key: 'location',
      },
      {
        title: 'PRODUCT DESCRIPTION',
        dataIndex: 'wholesaleItem.variety',
        key: 'wholesaleItem.variety',
        align: 'center',
      }
    ]
    return (
      <PrintPickSheetWrapper >
        <HeaderBox>
          <Row gutter={24} style={{marginBottom: 20}}>
            <Col span={6}>
              <CompanyInfoWrapper>
                {this.props.logo === 'default' ? (
                  <LogoWrapper>
                    <Icon type="logo4" viewBox={void 0} style={{width: 224, height: 37}} />
                  </LogoWrapper>
                ) : (
                  <img src={this.props.logo} />
                )}
                <span>{this.props.companyName}</span>
              </CompanyInfoWrapper>
            </Col>
            <Col offset={2} span={8} style={{textAlign: 'center'}}>
              <h1>PICK SHEET</h1>
            </Col>
            <Col span={7} style={{textAlign: 'right'}}>
              <Barcode value={ currentOrder ? currentOrder.wholesaleOrderId.toString() : ''} displayValue={false}/>
            </Col>
            <Col span={1}>

            </Col>
          </Row>

          <Row gutter={24}>
              <Col span={1}></Col>
              <Col  span={5}>
                <HeaderTextLeftBox>SHIP TO:</HeaderTextLeftBox>
              </Col>
              <Col  span={5}>
                <HeaderTextLeftBox></HeaderTextLeftBox>
              </Col>
              <Col span={2}></Col>
              <Col  span={6}>
                <HeaderTextLeftBox>WORK ORDER: </HeaderTextLeftBox>
              </Col>
              <Col  span={4}>
                <HeaderTextLeftBox># {currentOrder.wholesaleOrderId}</HeaderTextLeftBox>
              </Col>
          </Row>
          <Row gutter={24}>
            <Col span={1}></Col>
            <Col span={5}>
              <HeaderTextLeftBox>SHIPPING ADDRESS:</HeaderTextLeftBox>
            </Col>
            <Col span={5}>
              <HeaderTextLeftBox></HeaderTextLeftBox>
            </Col>
            <Col span={2}></Col>
            <Col  span={6}>
              <HeaderTextLeftBox>COMPLETION DATE/TIME: </HeaderTextLeftBox>
            </Col>
            <Col  span={4}>
              <HeaderTextLeftBox><Moment format="MM/DD/YYYY  h:mm A" date={currentOrder &&  currentOrder.completionDate ? currentOrder.completionDate : ''} /></HeaderTextLeftBox>
            </Col>
          </Row>
          <Row gutter={24}>
          <Col span={1}></Col>
              <Col  span={5}>
                <HeaderTextLeftBox>NOTES:</HeaderTextLeftBox>
              </Col>
              <Col  span={5}>
                <HeaderTextLeftBox>{currentOrder ? currentOrder.manufactureNote : ''}</HeaderTextLeftBox>
              </Col>
              <Col span={2}></Col>
              <Col  span={6}>
              <HeaderTextLeftBox>PRINT TIME: </HeaderTextLeftBox>
            </Col>
            <Col  span={4}>
              <HeaderTextLeftBox><Moment format="MM/DD/YYYY h:mm A" date={_moment().format()} /></HeaderTextLeftBox>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={13}/>
            <Col span={6}>
              <HeaderTextLeftBox>ACCOUNT MGR.: </HeaderTextLeftBox>
            </Col>
            <Col  span={4}>
              <HeaderTextLeftBox>{currentOrder ? currentOrder.user.firstName + ' ' + currentOrder.user.lastName : ''}</HeaderTextLeftBox>
            </Col>
          </Row>
        </HeaderBox>
        <PrintPickSheetTable>
        <Table rowKey="wholesaleOrderItemId" columns={columns} dataSource={currentOrder ? currentOrder.orderItemList : []} pagination={false}/>
        </PrintPickSheetTable>
      </PrintPickSheetWrapper>
    )
  }
}

export default WOPrintPickSheet
