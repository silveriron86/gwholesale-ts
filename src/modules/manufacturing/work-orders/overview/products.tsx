import * as React from 'react'
import { ThemeTable } from '~/modules/customers/customers.style'
import { TableWrapper, Totals, LabelSpan, ValueSpan, TotalsLabel } from '../work-orders.style'
import { Row, Col } from 'antd'

interface ProductsProps {}

export class Products extends React.PureComponent<ProductsProps> {
  render() {
    const columns: Array<any> = [
      {
        title: 'Products Manufactured (Out)',
        children: [          
          {
            title: '#',
            dataIndex: 'index',
            key: 'index',
          },
          {
            title: 'UNITS PLANNED',
            dataIndex: 'units_planned',
          },
          {
            title: 'WEIGHT TYPE',
            dataIndex: 'weight_type',
          },
          {
            title: 'UNITS ACTUAL',
            dataIndex: 'units',
          },
          {
            title: 'UNIT OF MEASURE',
            dataIndex: 'UOM',
          },
          {
            title: 'WEIGHT ACTUAL',
            dataIndex: 'weight',
          },
          {
            title: 'PRODUCT & LABEL NAME',
            dataIndex: 'label',
          },
          {
            title: 'INSTRUCTIONS',
            dataIndex: 'instructions',
          },
          {
            title: 'SKU',
            dataIndex: 'SKU',
          },
          {
            title: '',
            key: 'action',
          },
        ],
      },
    ]    
    const dataSource: any[] = [];

    return (
      <TableWrapper>
        <ThemeTable
          columns={columns}
          dataSource={dataSource}
          rowKey="id"
        />
      <Totals>
        <TotalsLabel>TOTALS</TotalsLabel>        
        <Row style={{marginTop: 9}}>
          <Col md={2}>
            <LabelSpan>Units Planned</LabelSpan>
            <ValueSpan>55</ValueSpan>
          </Col>
          <Col md={2}>
            <LabelSpan>Units Actual</LabelSpan>
            <ValueSpan>4</ValueSpan>
          </Col>
        </Row>
      </Totals>   
      </TableWrapper>
    )
  }
}

export default Products
