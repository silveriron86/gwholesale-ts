import * as React from 'react'
import { TableWrapper, Totals, LabelSpan, ValueSpan, TotalsLabel, MaterialsHeaderAction } from '../../work-orders.style'
import { Icon as IconSvg } from '~/components'
import { Row, Col, Dropdown, Icon, Popover, Popconfirm } from 'antd'
import GalleryView from '../mdoals/gallery-view'
import EditableTable from '~/components/Table/editable-table'
import TypeEditWrapper from '~/modules/settings/tabs/Product/TypeEditWrapper'
import _ from 'lodash'
import { VerticalMenuWrapper, NotePopoverWrapper, generatedOrdersStyle } from './../../work-orders.style'
import { Order, OrderItem } from '~/schema'
import { FlexCenter, ThemeModal, ThemeIcon, ThemeInput, ThemeSwitch } from '~/modules/customers/customers.style'
import LotsTable from '~/modules/customers/sales/cart/tables/lots-table'
import { Link } from 'react-router-dom'
import { combineUom } from '~/common/utils'

interface MaterialsProps {
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
  deleteProductType: Function
  updateProductType: Function
  getUnitsPlannedAndActual: Function
  companyProductTypes: any[]
  handleSave: Function
  handleRemove: Function
  order: Order
  receiveMaterials: boolean
  toggleReceiveMaterials: Function
}

export const formatMaterialsInDataSource = (order: Order) => {
  if (order && order.orderItemList.length > 0) {
    const items = order.orderItemList.filter((el) => el.type === 1)
    return items.map((el) => ({
      wholesaleOrderItemId: el.wholesaleOrderItemId,
      quantity: el.quantity ? el.quantity : 0, //same with units ordered in purchase order detail table
      UOM: el.uom ? el.uom : '',
      variety: el.wholesaleItem ? el.wholesaleItem.variety : '',
      itemId: el.wholesaleItem ? el.wholesaleItem.wholesaleItemId : undefined,
      unitsActual: el.unitsActual ? el.unitsActual : 0,
      unitsPlanned: el.unitsPlanned ? el.unitsPlanned : 0, // update By DENG, quantity fields need do inventory
      note: el.note ? el.note : '',
      lotId: el.lotId ? el.lotId : '',
      orderStatus: order.wholesaleOrderStatus,
      sku: el.wholesaleItem ? el.wholesaleItem.sku : '',
      baseUOM: el.wholesaleItem ? el.wholesaleItem.baseUOM : '',
      inventoryUOM: el.wholesaleItem ? el.wholesaleItem.inventoryUOM : '',
      constantRatio: el.wholesaleItem ? el.wholesaleItem.constantRatio : false,
      wholesaleProductUomList: el.wholesaleItem ? el.wholesaleItem.wholesaleProductUomList : [],
      wholesaleItem: el.wholesaleItem ? el.wholesaleItem : null,
    }))
  } else {
    return []
  }
}

export class Materials extends React.PureComponent<MaterialsProps> {
  state = {
    visibleGalleryView: false,
    initNotes: Array<any>(),
    modifyNotes: Array<any>(),
    selectedItemId: null,
    items: [],
    dataSource: [],
    visibleLotsModal: false,
    selectedItem: null,
  }

  componentDidMount = () => {}

  componentDidUpdate(prevProps: MaterialsProps) {}

  viewInstruction = (record: any, event: any) => {
    this.setState({
      visibleGalleryView: true,
      selectedItemId: record.itemId,
    })
  }

  onOkGalleryView = () => {
    this.setState({
      visibleGalleryView: false,
    })
  }

  setNewNote(value: string, index: number) {
    let { modifyNotes } = this.state
    modifyNotes[index] = value
    this.setState({ modifyNotes }, () => {
      // console.log(this.state.modifyNotes)
    })
  }

  showDotMenu = (record: any, index: number) => {
    // console.log(record)
    return (
      <VerticalMenuWrapper>
        <div style={{ borderBottom: '1px solid #CCCCCC' }} onClick={this.viewInstruction.bind(this, record)}>
          <Icon type="file-image" />
          <span>View Instructions</span>
        </div>
        {/* <NotePopoverWrapper>
          <Popover
            placement="bottomRight"
            content={<NotesModal record={record} setNewNote={this.setNewNote.bind(this)} index={index} />}
            trigger="click"
            onVisibleChange={this.onNotesVisbleChange.bind(this, record, index)}
          >
            <div>
              <Icon type="unordered-list" />
              <span>Notes to Picker</span>
            </div>
          </Popover>
        </NotePopoverWrapper> */}
      </VerticalMenuWrapper>
    )
  }

  onNotesVisbleChange = (record: any, index: number, visible: boolean) => {
    let { initNotes, modifyNotes } = this.state
    if (visible === false) {
      if (initNotes[index] != modifyNotes[index]) {
        initNotes[index] = modifyNotes[index]
        record.note = modifyNotes[index]
        this.props.handleSave(record)
        this.setState({ initNotes })
      }
    }
  }

  onToggleLotsModal = (record: OrderItem | null) => {
    // console.log("on toggle", record)

    this.setState({
      visibleLotsModal: !this.state.visibleLotsModal,
      selectedItem: record ? record : null,
    })
  }

  handleModalOk = () => {
    this.setState({
      visiblePriceModal: false,
    })
  }

  handleUpdate = (row: any) => {
    row.unitsPlanned == '' ? (row.unitsPlanned = 0) : ''
    row.unitsActual == '' ? (row.unitsActual = 0) : ''
    this.props.handleSave(row)
  }
  handleLotSave = (row: OrderItem) => {
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
    // console.log('handle save for lot item', row)
    this.props.handleSave(row)
  }

  handleSelectLot = _.debounce((record: any) => {
    let row = { ...this.state.selectedItem }
    if (record) {
      row['lotId'] = record.lotId
      row['price'] =
        record.cost * (record.lotMargin > 0 ? 1 + record.lotMargin / 100 : 1) + (record.freight ? record.freight : 0)
    } else {
      row['lotId'] = null
    }
    this.handleLotSave(row)
    this.onToggleLotsModal(null)
  }, 200)

  onDeleteRow = (id: number) => {
    this.props.handleRemove(id)
  }

  getHeaderRow = () => {
    const { order, toggleReceiveMaterials, receiveMaterials } = this.props
    const title = 'Materials (In)'
    let childOrderStatus = ''
    if (order) {
      const childOrders = order.childOrders
      let salesOrderId = null
      if (typeof childOrders !== 'undefined') {
        childOrders.forEach((el) => {
          if (el.wholesaleClient && el.wholesaleClient.type == 'CUSTOMER') {
            salesOrderId = el.wholesaleOrderId
            childOrderStatus = el.wholesaleOrderStatus
          }
        })
      }

      if (salesOrderId) {
        return (
          <div style={{ display: 'flex' }}>
            <div>{title}</div>
            {order.wholesaleOrderStatus != 'REQUEST' && (
              <MaterialsHeaderAction>
                <div>
                  <ThemeSwitch
                    onChange={() => toggleReceiveMaterials(!receiveMaterials, salesOrderId)}
                    checked={receiveMaterials}
                    style={{ marginRight: 4 }}
                    disabled={
                      order.wholesaleOrderStatus == 'WIP' ||
                      order.wholesaleOrderStatus == 'COMPLETED' ||
                      order.wholesaleOrderStatus == 'SHIPPED'
                    }
                  />
                  <span className="receive-materials">Receive Materials</span>
                </div>
                <div style={{ textAlign: 'right' }}>
                  <Link to={`/sales-order/${salesOrderId}`}>View Order</Link>
                </div>
              </MaterialsHeaderAction>
            )}
          </div>
        )
      } else {
        return title
      }
    } else {
      return title
    }
  }

  render() {
    const { order, getUnitsPlannedAndActual } = this.props
    const { visibleGalleryView, selectedItemId, visibleLotsModal, selectedItem } = this.state
    const dataSource = formatMaterialsInDataSource(order)
    const columns: Array<any> = [
      {
        title: this.getHeaderRow(),
        children: [
          {
            title: '#',
            dataIndex: 'index',
            key: 'index',
            render: (id: number, record: any, index: number) => {
              return index + 1
            },
          },
          {
            title: 'UNITS PLANNED',
            dataIndex: 'unitsPlanned',
            key: 'unitsPlanned',
            editable: true,
            edit_width: 100,
            align: 'center',
            render: (unitsPlanned: number, record: any) => {
              let productUom =
                record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                  ? record.wholesaleProductUomList[0]
                  : null
              return combineUom(unitsPlanned, record.UOM, record.wholesaleItem, productUom)
            },
          },
          {
            title: 'UNIT OF MEASURE',
            dataIndex: 'UOM',
            key: 'UOM',
            editable: true,
            edit_width: 80,
            align: 'center',
            width: 105,
          },
          {
            title: 'SKU',
            dataIndex: 'sku',
            key: 'SKU',
            edit_width: 80,
            width: 105,
            align: 'center',
          },
          {
            title: 'DESCRIPTION',
            dataIndex: 'variety',
            align: 'center',
          },
          {
            title: 'LOT',
            dataIndex: 'lotId',
            align: 'center',
          },
          {
            title: 'NOTE TO WORKER',
            editable: true,
            dataIndex: 'note',
            align: 'center',
          },
          /*
          {
            title: 'LOT ID',
            dataIndex: 'lotId',
            align: 'center',
            key: 'lot',
            // editable: true,
            // type: 'modal',
            // render: (lotId: string, record: any) => (
            //   <div>
            //     {record.orderStatus != 'REQUEST' && record.orderStatus != 'PLANNING' ? (
            //       <span style={{ color: 'rgba(0, 0, 0, 0.65)' }}>{lotId}</span>
            //     ) : (
            //         <ThemeIconButton
            //           type="link"
            //           onClick={this.onToggleLotsModal.bind(this, record)}
            //           style={{ width: '100%', textAlign: 'left' }}
            //         >
            //           <span style={{ color: 'rgba(0, 0, 0, 0.65)' }}>{lotId}</span>
            //           <Icon type="edit" />
            //         </ThemeIconButton>
            //       )}
            //   </div>
            // ),
          },
          */
          // {
          //   title: 'INSTRUCTIONS',
          //   dataIndex: 'instructions',
          //   editable: true,
          //   edit_width: 100,
          //   width: 125,
          // },
          {
            title: 'UNITS ACTUAL',
            dataIndex: 'unitsActual',
            key: 'unitsActual',
            editable: order && order.wholesaleOrderStatus == 'WIP' ? true : false,
            align: 'center',
            edit_width: 80,
            width: 105,
            render: (unitsActual: number, record: any, index: number) => {
              let productUom =
                record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                  ? record.wholesaleProductUomList[0]
                  : null
              return combineUom(unitsActual, record.UOM, record.wholesaleItem, productUom)
            },
          },
          {
            title: '',
            key: 'action',
            render: (id: number, record: any, index: number) => {
              return (
                <Dropdown trigger={['click']} overlay={this.showDotMenu(record, index)} placement="bottomRight">
                  <IconSvg viewBox="0 0 17 17" width="17" height="17" type="more-icon" />
                </Dropdown>
              )
            },
          },
          {
            title: '',
            key: 'delete',
            render: (id: number, record: any, index: number) => {
              return (
                (record.orderStatus == 'REQUEST' || record.orderStatus == 'PLANNING') && (
                  <Popconfirm
                    title="Permanently delete this one related record?"
                    okText="Delete"
                    onConfirm={this.onDeleteRow.bind(this, record.wholesaleOrderItemId)}
                  >
                    <FlexCenter style={{ cursor: 'pointer' }}>
                      <ThemeIcon type="close" style={{ fontWeight: 'bold' }} />
                    </FlexCenter>
                  </Popconfirm>
                )
              )
            },
          },
        ],
      },
    ]

    let allProps = { ...this.props }
    allProps['TypeEditWrapper'] = TypeEditWrapper
    return (
      <TableWrapper style={{ paddingBottom: 0 }}>
        <EditableTable
          columns={columns}
          dataSource={dataSource}
          pagination={false}
          rowKey={'wholesaleOrderItemId'}
          className="no-hr-border"
          handleSave={this.handleUpdate}
          allProps={allProps}
        />
        <Totals>
          <TotalsLabel>TOTALS</TotalsLabel>
          <Row style={{ marginTop: 9 }}>
            <Col md={4}>
              <LabelSpan>Units Planned</LabelSpan>
              <ValueSpan>{getUnitsPlannedAndActual(order, 1)[0]}</ValueSpan>
            </Col>
            <Col md={4}>
              <LabelSpan>Units Actual</LabelSpan>
              <ValueSpan>{getUnitsPlannedAndActual(order, 1)[1]}</ValueSpan>
            </Col>
          </Row>
        </Totals>

        <GalleryView
          visible={visibleGalleryView}
          itemId={selectedItemId}
          order={order}
          handleOk={this.onOkGalleryView}
          handleCancel={this.onOkGalleryView}
        />
        <ThemeModal
          title={'Lots'}
          keyboard={true}
          visible={visibleLotsModal}
          onOk={this.onToggleLotsModal.bind(this, null)}
          onCancel={this.onToggleLotsModal.bind(this, null)}
          okText="Close [esc]"
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={900}
        >
          <LotsTable
            itemId={selectedItem ? selectedItem.itemId : 0}
            lotId={selectedItem ? selectedItem.lotId : 0}
            onSelect={this.handleSelectLot}
          />
        </ThemeModal>
      </TableWrapper>
    )
  }
}

export default Materials
