import * as React from 'react'
import { FlexCenter, ThemeModal, ThemeIcon, ThemeSwitch } from '~/modules/customers/customers.style'
import {
  TableWrapper,
  Totals,
  LabelSpan,
  ValueSpan,
  TotalsLabel,
  VerticalMenuWrapper,
  MaterialsHeaderAction,
} from '../../work-orders.style'
import { Row, Col, Menu, Dropdown, Icon, Popconfirm } from 'antd'
import { Icon as IconSvg } from '~/components'
import GalleryView from '../mdoals/gallery-view'
import EditableTable from '~/components/Table/editable-table'
import TypeEditWrapper from '~/modules/settings/tabs/Product/TypeEditWrapper'
import QuantityModal from '~/modules/customers/sales/cart/modals/quantity-modal'
import { Order } from '~/schema'
import _ from 'lodash'
import { Link } from 'react-router-dom'
import { combineUom } from '~/common/utils'

interface ProductsProps {
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
  deleteProductType: Function
  updateProductType: Function
  getUnitsPlannedAndActual: Function
  getCurrentWorkOrder: Function
  handleSave: Function
  handlePalletShow: Function
  companyProductTypes: any[]
  handleRemove: Function
  order: Order
  palletIndex: number
  toggleSendProductWarehouse: Function
  sendProductWarehouse: boolean
}

export const formatMaterialsOutDataSource = (order: Order) => {
  if (order && order.orderItemList.length > 0) {
    const items = order.orderItemList.filter((el) => el.type === 2)
    return items.map((el) => {
      return {
        itemId: el.wholesaleItem ? el.wholesaleItem.wholesaleItemId : undefined,
        wholesaleOrderItemId: el.wholesaleOrderItemId,
        quantity: el.quantity ? el.quantity : 0, //same with units ordered in purchase order detail table
        unitsActual: el.unitsActual ? el.unitsActual : 0,
        unitsPlanned: el.unitsPlanned ? el.unitsPlanned : 0, // update By DENG, quantity fields need do inventory
        UOM: el.uom ? el.uom : '',
        weightType: el.weightType ? el.weightType : '',
        weightActual: el.weightActual ? el.weightActual : 0,
        variety: el.wholesaleItem ? el.wholesaleItem.variety : '',
        SKU: el.wholesaleItem ? el.wholesaleItem.sku : '',
        palletQty: el.palletQty ? el.palletQty : 0,
        note: el.note ? el.note : '',
        orderStatus: order.wholesaleOrderStatus,
        specification: el.wholesaleItem ? el.wholesaleItem.specification : '',
        wholesaleProductUomList: el.wholesaleItem ? el.wholesaleItem.wholesaleProductUomList : [],
        wholesaleItem: el.wholesaleItem ? el.wholesaleItem : null,
      }
    })
  }

  return []
}

export class Products extends React.PureComponent<ProductsProps> {
  state = {
    visibleGalleryView: false,
    visibleWeightModal: false,
    selectedItem: null,
    initNotes: [],
    modifyNotes: [],
    dataSource: [],
  }

  componentDidMount = () => {}

  showDotMenu = (record: any, index: number) => {
    return (
      <VerticalMenuWrapper>
        <div style={{ borderBottom: '1px solid #CCCCCC' }} onClick={this.viewInstruction.bind(this, record)}>
          <Icon type="file-image" />
          <span>View Instructions</span>
        </div>
      </VerticalMenuWrapper>
    )
  }

  viewInstruction = (record: any, event: any) => {
    this.setState({
      visibleGalleryView: true,
      selectedItem: record,
    })
  }

  onOkGalleryView = () => {
    this.setState({
      visibleGalleryView: false,
    })
  }

  setNewNote(value: string, index: number) {
    const { modifyNotes } = this.state
    modifyNotes[index] = value
    this.setState({ modifyNotes })
  }

  onNotesVisbleChange = (record: any, index: number, visible: boolean) => {
    const { initNotes, modifyNotes } = this.state
    if (visible === false && initNotes[index] !== modifyNotes[index]) {
      initNotes[index] = modifyNotes[index]
      record.note = modifyNotes[index]
      this.props.handleSave(record)
      this.setState({ initNotes })
    }
  }

  openGalleryView = () => {
    this.setState({
      visibleGalleryView: true,
    })
  }

  onSelectMenu = (e: any) => {
    if (e.key === 'Gallery') {
      this.openGalleryView()
    }
  }

  onToggleWeightModal = (record: any | null) => {
    const { order } = this.props
    this.setState({
      visibleWeightModal: !this.state.visibleWeightModal,
      selectedItem: record ? record : null,
    })
    this.props.getCurrentWorkOrder(order.wholesaleOrderId)
  }

  handleSave = (row: any) => {
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.itemId === item.itemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.props.handleSave(row)
  }

  onDeleteRow = (id: number) => {
    this.props.handleRemove(id)
  }

  getHeaderRow = () => {
    const { order } = this.props
    const title = 'Products Manufactured (Out)'
    if (order) {
      const childOrders = order.childOrders
      let purchaseOrderId = null
      if (typeof childOrders !== 'undefined') {
        childOrders.forEach((el) => {
          if (el.wholesaleClient && el.wholesaleClient.type == 'VENDOR') {
            purchaseOrderId = el.wholesaleOrderId
          }
        })
      }
      if (purchaseOrderId) {
        return (
          <div style={{ display: 'flex' }}>
            <div>{title}</div>
            {order.wholesaleOrderStatus == 'SHIPPED' && (
              <MaterialsHeaderAction>
                <div>
                  <ThemeSwitch
                    onChange={() =>
                      this.props.toggleSendProductWarehouse(!this.props.sendProductWarehouse, purchaseOrderId)
                    }
                    checked={this.props.sendProductWarehouse}
                    style={{ marginRight: 4 }}
                  />
                  <span className="receive-materials">Send product to warehouse</span>
                </div>
                <div style={{ textAlign: 'right' }}>
                  <Link to={`/order/${purchaseOrderId}/purchase-cart`}>View Order</Link>
                </div>
              </MaterialsHeaderAction>
            )}
          </div>
        )
      } else {
        return title
      }
    } else {
      return title
    }
  }

  render() {
    const { order, getUnitsPlannedAndActual } = this.props
    const { visibleGalleryView, visibleWeightModal, selectedItem } = this.state
    const dataSource = formatMaterialsOutDataSource(order)
    const menu = (
      <Menu onClick={this.onSelectMenu}>
        <Menu.Item key="Gallery">
          <Icon type="file-image" />
          View Instructions
        </Menu.Item>
        <Menu.Item key="Notes">
          <Icon type="unordered-list" />
          Notes to Picker
        </Menu.Item>
      </Menu>
    )

    const columns: Array<any> = [
      {
        title: this.getHeaderRow(),
        children: [
          {
            title: '#',
            dataIndex: 'index',
            key: 'index',
            render: (id: number, record: any, index: number) => {
              return index + 1
            },
          },
          {
            title: 'UNITS PLANNED',
            dataIndex: 'unitsPlanned',
            editable: true,
            align: 'center',
            edit_width: 100,
            render: (unitsPlanned: number, record: any) => {
              let productUom =
                record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                  ? record.wholesaleProductUomList[0]
                  : null
              return combineUom(unitsPlanned, record.UOM, record.wholesaleItem, productUom)
            },
          },
          {
            title: 'INVENTORY UOM',
            dataIndex: 'UOM',
            key: 'UOM',
            editable: true,
            edit_width: 80,
            align: 'center',
            width: 105,
          },
          {
            title: 'SKU',
            dataIndex: 'SKU',
            edit_width: 80,
            align: 'center',
            width: 105,
          },
          {
            title: 'DESCRIPTION',
            dataIndex: 'variety',
            align: 'center',
          },
          {
            title: 'NOTE TO WORKER',
            dataIndex: 'note',
            editable: true,
            align: 'center',
          },
          {
            title: 'WEIGHT ACTUAL',
            dataIndex: 'weightActual',
            editable: true,
            edit_width: 80,
            align: 'center',
            width: 105,
          },
          {
            title: 'UNITS ACTUAL',
            dataIndex: 'unitsActual',
            editable: order && order.wholesaleOrderStatus == 'WIP' ? true : false,
            align: 'center',
            edit_width: 80,
            render: (unitsActual: number, record: any) => {
              const productUom =
                record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                  ? record.wholesaleProductUomList[0]
                  : null
              return combineUom(unitsActual, record.UOM, record.wholesaleItem, productUom)
            },
          },
          {
            title: '',
            key: 'action',
            render: (id: number, record: any, index: number) => {
              return (
                <Dropdown overlay={this.showDotMenu(record, index)} placement="bottomRight">
                  <IconSvg viewBox="0 0 17 17" width="17" height="17" type="more-icon" />
                </Dropdown>
              )
            },
          },
          {
            title: '',
            key: 'delete',
            render: (id: number, record: any, index: number) => {
              return (
                (record.orderStatus == 'REQUEST' || record.orderStatus == 'PLANNING') && (
                  <Popconfirm
                    title="Permanently delete this one related record?"
                    okText="Delete"
                    onConfirm={this.onDeleteRow.bind(this, record.wholesaleOrderItemId)}
                  >
                    <FlexCenter style={{ cursor: 'pointer' }}>
                      <ThemeIcon type="close" style={{ fontWeight: 'bold' }} />
                    </FlexCenter>
                  </Popconfirm>
                )
              )
            },
          },
        ],
      },
    ]

    let allProps = { ...this.props }
    allProps['TypeEditWrapper'] = TypeEditWrapper
    return (
      <TableWrapper>
        <EditableTable
          columns={columns}
          dataSource={dataSource}
          pagination={false}
          rowKey={'wholesaleOrderItemId'}
          className="no-hr-border"
          handleSave={this.handleSave}
          allProps={allProps}
        />
        <Totals>
          <TotalsLabel>TOTALS</TotalsLabel>
          <Row style={{ marginTop: 9 }}>
            <Col md={4}>
              <LabelSpan>Units Planned</LabelSpan>
              <ValueSpan>{getUnitsPlannedAndActual(order, 2)[0]}</ValueSpan>
            </Col>
            <Col md={4}>
              <LabelSpan>Units Actual</LabelSpan>
              <ValueSpan>{getUnitsPlannedAndActual(order, 2)[1]}</ValueSpan>
            </Col>
          </Row>
        </Totals>
        <GalleryView
          visible={visibleGalleryView}
          order={order}
          itemId={selectedItem ? selectedItem.itemId : null}
          specification={selectedItem ? selectedItem.specification : ''}
          handleOk={this.onOkGalleryView}
          handleCancel={this.onOkGalleryView}
        />

        <ThemeModal
          title={'Quantity Details'}
          keyboard={true}
          visible={visibleWeightModal}
          onOk={this.onToggleWeightModal.bind(this, null)}
          onCancel={this.onToggleWeightModal.bind(this, null)}
          okText={'Submit'}
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={800}
        >
          <QuantityModal orderItem={selectedItem} isWO={true} updateSaleOrderItemForWO={this.props.handleSave} />
        </ThemeModal>
      </TableWrapper>
    )
  }
}

export default Products
