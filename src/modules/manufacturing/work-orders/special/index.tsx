import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import { RouteComponentProps, RouteProps } from 'react-router'
import { ManufacturingDispatchProps, ManufacturingModule, ManufacturingStateProps } from '~/modules/manufacturing'
import { CustomersInfoBody } from '~/modules/customers/components/customers-detail/customers-detail.style'
// import OverviewHeader from '~/modules/manufacturing/work-orders/overview/header';
import { AuditLogTabs, ModalTable, CustomDatePicker, tableBorderStyle } from '../work-orders.style'
import {
  CustomerDetailsTitle as DetailsTitle,
  CustomerDetailsWrapper as DetailsWrapper,
  floatLeft,
  InputLabel,
} from '~/modules/customers/customers.style'
import { Icon as IconSvg } from '~/components/icon/'
import { Row, Col, Tabs } from 'antd'
import moment from 'moment'
import { History } from 'history'
import { Order } from '~/schema'

const { TabPane } = Tabs
type SpecialProps = ManufacturingDispatchProps &
  ManufacturingStateProps &
  RouteComponentProps &
  RouteProps & {
    history: History
    theme: Theme
  }

export class Special extends React.PureComponent<SpecialProps> {
  state: any = {
    fromDate: moment(), // moment().subtract(10, 'days'),
  }

  componentDidMount() {
    this._loadData()
    this.props.getUnscheduleWorkOrderList()
    this.props.getIncompletedWorkOrderList()
  }

  onDateChange = (date: any, dateString: any) => {
    this.setState(
      {
        fromDate: moment(dateString),
      },
      () => {
        this._loadData()
      },
    )
  }

  _loadData = () => {
    const { fromDate } = this.state
    this.props.getWorkOrderListByDate({
      fromDate: fromDate.format('YYYY-MM-DD'),
    })
  }

  _formatDateTime = (value: number) => {
    return value ? moment(value).format('YYYY-MM-DD HH:mm:ss') : ''
  }

  _getStationAndManufactureTypeFromOrder = (schedule: any) => {
    let type = 'N/A'
    let station = '--'
    if (!schedule || schedule.length == 0) return [type, station]
    let stationSetting: any = null
    schedule.forEach((element: any) => {
      if (element.wholesaleStationSetting != null) {
        stationSetting = element.wholesaleStationSetting
      }
    })
    if (stationSetting != null && stationSetting.wholesaleManufactureType) {
      type = stationSetting.wholesaleManufactureType.name
      station = stationSetting.wholesaleStation.name
    }
    return [type, station]
  }

  render() {
    const { specialCutWorkOrders, unscheduleWorkOrders, incompletedWorkOrders } = this.props
    const { fromDate } = this.state
    const onSortString = (key: string, a: any, b: any) => {
      const stringA: string = a[key] ? a[key] : ''
      const stringB: string = b[key] ? b[key] : ''
      return stringA.localeCompare(stringB)
    }
    const columns = [
      {
        title: 'ID',
        dataIndex: 'wholesaleOrderId',
        key: 'id',
        sorter: (a: Order, b: Order) => a.wholesaleOrderId - b.wholesaleOrderId, //onSortString('wholesaleOrderId', a, b),
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a: Order, b: Order) => onSortString('name', a, b),
      },
      {
        title: 'Place Date',
        dataIndex: 'createdDate',
        key: 'createdDate',
        render: (value: number) => this._formatDateTime(value),
        sorter: (a: Order, b: Order) => a.createdDate - b.createdDate,
      },
      {
        title: ' Scheduled Date (Request ETA)',
        dataIndex: 'requestedTime',
        key: 'requestedTime',
        sorter: (a: Order, b: Order) => a.requestedTime - b.requestedTime,
        render: (value: number) => this._formatDateTime(value),
      },
      {
        title: ' Revised ETA',
        dataIndex: 'revisedTime',
        key: 'revisedTime',
        sorter: (a: Order, b: Order) => a.revisedTime - b.revisedTime,
        render: (value: number) => this._formatDateTime(value),
      },
      {
        title: ' End Date (Completion Date)',
        dataIndex: 'endDate',
        key: 'endDate',
        sorter: (a: Order, b: Order) => a.completionDate - b.completionDate,
        render: (value: number) => this._formatDateTime(value),
      },
      {
        title: 'Status',
        dataIndex: 'wholesaleOrderStatus',
        key: 'status',
        sorter: (a: Order, b: Order) => onSortString('wholesaleOrderStatus', a, b),
      },
      {
        title: 'Station',
        dataIndex: 'wholesaleStationSchedule',
        key: 'station',
        sorter: (a: Order, b: Order) =>
          this._getStationAndManufactureTypeFromOrder(a.wholesaleStationSchedule)[1].localeCompare(
            this._getStationAndManufactureTypeFromOrder(b.wholesaleStationSchedule)[1],
          ),
        render: (schedule: any) => this._getStationAndManufactureTypeFromOrder(schedule)[1],
      },
      {
        title: 'Manufacturing Type',
        dataIndex: 'wholesaleStationSchedule',
        key: 'type',
        sorter: (a: Order, b: Order) =>
          this._getStationAndManufactureTypeFromOrder(a.wholesaleStationSchedule)[0].localeCompare(
            this._getStationAndManufactureTypeFromOrder(b.wholesaleStationSchedule)[0],
          ),
        render: (schedule: any) => this._getStationAndManufactureTypeFromOrder(schedule)[0],
      },
      {
        title: 'Related Sales Order',
        dataIndex: 'relateOrderId',
        key: 'related',
        sorter: (a: Order, b: Order) => a.relateOrderId - b.relateOrderId,
        render: (relateOrderId: any) => (relateOrderId ? `#${relateOrderId}` : ''),
      },
    ]

    const onRow = (record: any, index: number) => {
      return {
        onClick: (_event: any) => {
          _event.preventDefault()
          this.props.history.push(`/manufacturing/${record.wholesaleOrderId}/wo-overview`)
        },
      }
    }

    return (
      <PageLayout currentTopMenu={'menu-Manufacturing-Special Customer Requests'} noSubMenu={true}>
        {/* <WorkOrdersHeader style={{marginLeft: 60}}>
          <OverviewHeader />
        </WorkOrdersHeader> */}
        <CustomersInfoBody style={{ paddingLeft: 50, paddingTop: 10 }}>
          <DetailsWrapper style={{ borderTopWidth: 0 }}>
            <Row>
              <Col md={24}>
                <DetailsTitle style={floatLeft}>Special Customer Request</DetailsTitle>
              </Col>
            </Row>

            <AuditLogTabs defaultActiveKey="1" type="card" style={{ marginBottom: 0 }}>
              <TabPane tab="By Date" key="1">
                <InputLabel>Request Date</InputLabel>
                <CustomDatePicker
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={<IconSvg type="calendar" viewBox="0 0 22 22" width={17} height={18} />}
                  defaultValue={fromDate}
                  onChange={this.onDateChange.bind(this)}
                />
                <ModalTable
                  onRow={onRow}
                  style={tableBorderStyle}
                  // showHeader={false}
                  dataSource={specialCutWorkOrders}
                  columns={columns}
                  // pagination={false}
                  rowClassName="high-row"
                />
              </TabPane>
              <TabPane tab="All Unscheduled" key="2">
                {/* <InputLabel>From</InputLabel>
                <CustomDatePicker
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={<IconSvg type="calendar" viewBox="0 0 22 22" width={17} height={18} />}
                  defaultValue={fromDate}
                  onChange={this.onDateChange.bind(this)}
                />*/}
                <ModalTable
                  onRow={onRow}
                  style={tableBorderStyle}
                  // showHeader={false}
                  dataSource={unscheduleWorkOrders}
                  columns={columns}
                  // pagination={false}
                  rowClassName="high-row"
                />
              </TabPane>
              <TabPane tab="Incomplete" key="3">
                <ModalTable
                  onRow={onRow}
                  style={tableBorderStyle}
                  // showHeader={false}
                  dataSource={incompletedWorkOrders}
                  columns={columns}
                  // pagination={false}
                  rowClassName="high-row"
                />
              </TabPane>
            </AuditLogTabs>
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.manufacturing
export default withTheme(connect(ManufacturingModule)(mapState)(Special))
