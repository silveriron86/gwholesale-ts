import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import { RouteComponentProps } from 'react-router'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders'
import { CustomersInfoBody } from '~/modules/customers/components/customers-detail/customers-detail.style'
import OverviewHeader from '~/modules/manufacturing/work-orders/overview/header';
import { WorkOrdersHeader, ModalTable, tableBorderStyle } from '../work-orders.style'
import { CustomerDetailsTitle as DetailsTitle, CustomerDetailsWrapper as DetailsWrapper, floatLeft, ThemeInput, InputLabel, ThemeButton, ThemeSpin } from '~/modules/customers/customers.style'
import { Row, Col, Popconfirm } from 'antd'
import moment from 'moment'
import { Order } from '~/schema'


type TemplatesProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

export class Templates extends React.PureComponent<TemplatesProps> {
  state = {
    search: ''
  }

  componentDidMount() {
    const orderId = this.props.match.params.orderId
    this.props.getCurrentWorkOrder(orderId)
    this.props.getAllWorkOrderTemplates()
  }

  _formatDateTime = (value: number) => {
    return value ? moment(value).format('YYYY-MM-DD HH:mm:ss') : '';
  }

  _getStationAndManufactureTypeFromOrder = (schedule: any) => {
    let type = 'N/A'
    let station = '--'
    if (!schedule || schedule.length == 0)
      return [type, station]
    let stationSetting: any = null
    schedule.forEach((element: any) => {
      if (element.wholesaleStationSetting != null) {
        stationSetting = element.wholesaleStationSetting

      }
    });
    if (stationSetting != null && stationSetting.wholesaleManufactureType) {
      type = stationSetting.wholesaleManufactureType.name
      station = stationSetting.wholesaleStation.name
    }
    return [type, station]
  }

  applyWorkOrder = (orderId: number) => {
    this.props.resetLoading();
    this.props.applyTemplate({
      currentOrderId: this.props.match.params.orderId,
      templateOrderId: orderId
    })
  }

  onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      search: e.target.value
    })
  }

  render() {
    const { search } = this.state
    const { currentWorkOrder, templates, loading } = this.props
    const onSortString = (key: string, a: any, b: any) => {
      const stringA: string = a[key] ? a[key] : ''
      const stringB: string = b[key] ? b[key] : ''
      return stringA.localeCompare(stringB)
    }
    const columns = [
      {
        title: 'ID',
        dataIndex: 'wholesaleOrderId',
        key: 'id',
        sorter: (a: Order, b: Order) => a.wholesaleOrderId - b.wholesaleOrderId //onSortString('wholesaleOrderId', a, b),
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a: Order, b: Order) => onSortString('name', a, b),
      },
      {
        title: 'Place Date',
        dataIndex: 'createdDate',
        key: 'createdDate',
        sorter: (a: Order, b: Order) => a.createdDate - b.createdDate,
        render: (value: number) => this._formatDateTime(value)
      },
      {
        title: ' Scheduled Date (Request ETA)',
        dataIndex: 'requestedTime',
        key: 'requestedTime',
        sorter: (a: Order, b: Order) => a.requestedTime - b.requestedTime,
        render: (value: number) => this._formatDateTime(value)
      },
      {
        title: ' Revised ETA',
        dataIndex: 'revisedTime',
        key: 'revisedTime',
        sorter: (a: Order, b: Order) => a.revisedTime - b.revisedTime,
        render: (value: number) => this._formatDateTime(value)
      },
      {
        title: ' End Date (Completion Date)',
        dataIndex: 'endDate',
        key: 'endDate',
        sorter: (a: Order, b: Order) => a.completionDate - b.completionDate,
        render: (value: number) => this._formatDateTime(value)
      },
      {
        title: 'Status',
        dataIndex: 'wholesaleOrderStatus',
        key: 'status',
        sorter: (a: Order, b: Order) => onSortString('wholesaleOrderStatus', a, b),
      },
      {
        title: 'Station',
        dataIndex: 'wholesaleStationSchedule',
        key: 'station',
        sorter: (a: Order, b: Order) => this._getStationAndManufactureTypeFromOrder(a.wholesaleStationSchedule)[1].localeCompare(this._getStationAndManufactureTypeFromOrder(b.wholesaleStationSchedule)[1]),
        render: (schedule: any) => this._getStationAndManufactureTypeFromOrder(schedule)[1]
      },
      {
        title: 'Manufacturing Type',
        dataIndex: 'wholesaleStationSchedule',
        key: 'type',
        sorter: (a: Order, b: Order) => this._getStationAndManufactureTypeFromOrder(a.wholesaleStationSchedule)[0].localeCompare(this._getStationAndManufactureTypeFromOrder(b.wholesaleStationSchedule)[0]),
        render: (schedule: any) => this._getStationAndManufactureTypeFromOrder(schedule)[0]
      },
      // {
      //   title: 'Related Sales Order',
      //   dataIndex: 'relateOrderId',
      //   key: 'related',
      //   render: (relateOrderId: any) => relateOrderId ? `#${relateOrderId}` : ''
      // }
      {
        title: '',
        dataIndex: 'wholesaleOrderId',
        key: 'action',
        width: 120,
        render: (orderId: number) => {
          return (
            <Popconfirm
              title="Are you sure you want to apply this template to this WO?"
              onConfirm={() => this.applyWorkOrder(orderId)}
              okText="Save"
              cancelText="Cancel"
              placement="left"
            >
              <ThemeButton shape="round" disabled={currentWorkOrder && currentWorkOrder.wholesaleOrderStatus != 'REQUEST' && currentWorkOrder.wholesaleOrderStatus != 'PLANNING'}>
                Apply to Work Order
              </ThemeButton>
            </Popconfirm>

          )
        }
      }
    ]

    let filteredTemplates = templates
    if (templates.length > 0) {
      filteredTemplates = templates.filter((wo: Order) => {
        return wo.wholesaleOrderId.toString().indexOf(search) >= 0
      })
    }

    return (
      <PageLayout currentTopMenu={'menu-Inventory-Work Orders'}>
        <WorkOrdersHeader>
          <OverviewHeader order={currentWorkOrder} />
        </WorkOrdersHeader>
        <CustomersInfoBody>
          <DetailsWrapper>
            <Row>
              <Col md={24}>
                <DetailsTitle style={floatLeft}>Templates</DetailsTitle>
              </Col>
            </Row>
            <InputLabel>WORK ORDER ID</InputLabel>
            <ThemeInput value={search} onChange={this.onChangeInput} style={{ width: 300 }} allowClear={true} />
            <ThemeSpin tip="Loading..." spinning={loading}>
              <ModalTable
                style={tableBorderStyle}
                // showHeader={false}
                dataSource={filteredTemplates}
                columns={columns}
                // pagination={false}
                rowClassName="high-row" />
            </ThemeSpin>
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapState)(Templates))
