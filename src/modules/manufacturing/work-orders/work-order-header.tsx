import React from 'react'
import { Table, Icon, DatePicker } from 'antd';
import { Link } from 'react-router-dom';
import { Order } from '~/schema';
import { DateEditorWrapper, DateTitle } from '~/modules/orders/components/orders-header.style';
import { Icon as IconSvg } from '~/components'
import { WorkOrderHeaderContainer } from './work-orders.style'

type HeaderProps {
    onDateChange: Function
}
class WorkOrderHeader extends React.PureComponent<HeaderProps> {

  render() {
    const calendarIcon = <IconSvg type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    const { dates } = this.props;
    const { RangePicker } = DatePicker

    return (
      <WorkOrderHeaderContainer>
        <DateEditorWrapper className="orders-delivery-date">
          <RangePicker
            placeholder={dateFormat}
            defaultValue={[dates[0], dates[1]]}
            format={dateFormat}
            suffixIcon={calendarIcon}
            onChange={this.props.onDateChange}
            allowClear={false}
          />
          <DateTitle>ORDERS DELIVERY DATE</DateTitle>
        </DateEditorWrapper>
      </WorkOrderHeaderContainer>
    )
  }
}

export default WorkOrderHeader
