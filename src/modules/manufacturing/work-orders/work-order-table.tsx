import React from 'react'
import { Icon } from 'antd';
import { format } from 'date-fns'
import { ThemeLink, ThemeTable } from '~/modules/customers/customers.style';
import { ManufacturingDispatchProps, ManufacturingStateProps, ManufacturingModule } from '../manufacturing.module';
import { connect } from 'redux-epics-decorator';
import { withTheme } from 'emotion-theming';
import { GlobalState } from '~/store/reducer';
import { getStationAndManufactureTypeFromOrder } from './overview/header';
import { History } from 'history'

type WorkOrderTableProps = ManufacturingDispatchProps & ManufacturingStateProps & {
  history: History
}

class WorkOrderTable extends React.PureComponent<WorkOrderTableProps> {
  column = [
    {
      title: 'ID #',
      dataIndex: 'id',
      key: 'id',
      width: 50,
      render: (d) => `# ${d}`,
    },
    {
      title: 'DATE PLACED',
      dataIndex: 'createdDate',
      key: 'createdDate',
      width: 60,
      sorter: (a, b) => a.createdDate - b.createdDate,
      render: (createdDate) => format(createdDate, 'MM/DD/YY'),
    },
    {
      title: 'SCHEDULED DATE',
      dataIndex: 'requestedTime',
      key: 'requestedTime',
      width: 60,
      sorter: (a, b) => a.requestedTime - b.requestedTime,
      render: (requestedTime) => requestedTime ? format(requestedTime, 'MM/DD/YY') : '',
    },
    {
      title: 'REVISED ETA',
      dataIndex: 'revisedTime',
      key: 'revisedTime',
      width: 60,
      sorter: (a, b) => a.revisedTime - b.revisedTime,
      render: (revisedTime) => revisedTime ? format(revisedTime, 'MM/DD/YY') : '',
    },
    {
      title: 'END DATE',
      dataIndex: 'completionDate',
      key: 'completionDate',
      width: 60,
      sorter: (a, b) => a.completionDate - b.completionDate,
      render: (completionDate) => completionDate ? format(completionDate, 'MM/DD/YY') : '',
    },
    {
      title: 'STATUS',
      dataIndex: 'status',
      key: 'status',
      width: 50,
      sorter: (a, b) => a.status.localeCompare(b.status),
    },
    {
      title: 'STATION',
      dataIndex: 'station',
      key: 'station',
      width: 50,
      sorter: (a, b) => a.station.localeCompare(b.station),
    },
    {
      title: 'MANUFACTUREING TYPE',
      dataIndex: 'type',
      key: 'type',
      width: 100,
      sorter: (a, b) => a.type.localeCompare(b.type),
    },
    {
      key: 'view',
      width: 50,
      render: (order) => (
        <ThemeLink
          // tslint:disable-next-line:jsx-no-lambda
          to={"/manufacturing/" + order.id + "/wo-overview"
          }>
          VIEW <Icon type="arrow-right" />
        </ThemeLink>
      ),
    },
  ]

  getTestWorkOrder = () => {
    let a = [{
      id: 970,
      createdDate: 1592496532145,
      deliveryDate: 1592496532145,
      totalPrice: 120,
      status: 'PLANNING',

    }]
    return a;
  }

  formatDataSource = () => {
    const { workOrders } = this.props
    let result = Array<any>()
    workOrders.forEach(el => {
      result.push({
        id: el.wholesaleOrderId,
        createdDate: el.createdDate,
        requestedTime: el.requestedTime,
        revisedTime: el.revisedTime,
        completionDate: el.completionDate,
        station: getStationAndManufactureTypeFromOrder(el)[1],
        type: getStationAndManufactureTypeFromOrder(el)[0],
        status: el.wholesaleOrderStatus,
      })
    });
    return result
  }

  render() {
    const data = this.formatDataSource()
    const onRow = (record: any, index: number) => {
      return {
        onClick: (_event: any) => {
          _event.preventDefault()
          if (_event.target.tagName !== 'A') {
            this.props.history.push(`manufacturing/${record.id}/wo-overview`)
          }
        }
      }
    }

    return (
      <ThemeTable columns={this.column} dataSource={data} rowKey='id' onRow={onRow}
          pagination={{current: this.props.page + 1, defaultCurrent: 0, onChange: this.props.onChangePage}}/>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.manufacturing
export default withTheme(connect(ManufacturingModule)(mapStateToProps)(WorkOrderTable))

