import React from 'react'

import PageLayout from '~/components/PageLayout';
import moment, { Moment } from 'moment';
import WorkOrderTable from './work-order-table';
import WorkOrderHeader from './work-order-header';
import { Row, Col } from 'antd';
import {
  WorkOrdersContainer
} from './work-orders.style'
import { HeaderTitle, ThemeButton } from '~/modules/customers/customers.style';
import { ManufacturingDispatchProps, ManufacturingStateProps, ManufacturingModule } from '../manufacturing.module';
import { RouteComponentProps } from 'react-router';
import { Theme } from '~/common';
import { connect } from 'redux-epics-decorator';
import { GlobalState } from '~/store/reducer';
import { withTheme } from 'emotion-theming';
import { History } from 'history'

type WorkOrdersProps = ManufacturingDispatchProps & ManufacturingStateProps & RouteComponentProps<{ orderId: string }> & {
  theme: Theme,
  history: History
}

class WorkOrdersList extends React.PureComponent<WorkOrdersProps> {
  dateFormat = 'YYYY-MM-DD';
  state: any
  constructor(props: WorkOrdersProps) {
    super(props)
    const sessionSearch = localStorage.getItem('WORK_ORDERS_SEARCH')
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search);
    const startDate = urlParams.get('datestart');
    const endDate = urlParams.get('dateend');
    const page = urlParams.get('page');
    const fromDate = startDate ? moment(startDate, 'MMDDYY') : moment()
    const toDate = endDate ? moment(endDate, 'MMDDYY') : moment().add(7, 'days')

    this.state = {
      fromDate,
      toDate,
      page: page ? parseInt(page, 10) : 0,
    }
  }

  componentDidMount() {
    const { fromDate, toDate } = this.state
    let from = fromDate.format(this.dateFormat)
    let to = toDate.format(this.dateFormat)
    this.onDateChange([fromDate, toDate], [from, to]);
    let date = {
      stationId: '',
      fromDate: from,
      toDate: to
    }
    this.props.getWorkOrderListByStationIdAndDate(date)
  }

  componentWillReceiveProps() {


  }


  onDateChange = (dates: [Moment, Moment], dateStrings: [string, string]) => {
    let date = {
      stationId: '',
      toDate: '',
      fromDate: '',
    }
    if (dateStrings.length > 0) {
      date = {
        stationId: '',
        fromDate: moment(dateStrings[0]).format('YYYY-MM-DD'),
        toDate: moment(dateStrings[1]).format('YYYY-MM-DD'),
      }
      this.setState({ fromDate: date.fromDate, toDate: date.toDate }, () => {
        this.updateURL();
      })
      this.props.getWorkOrderListByStationIdAndDate(date)
    }
  }

  updateURL = () => {
    var url = new URL(window.location.href);
    var queryParams = url.searchParams;
    const { fromDate, toDate, page } = this.state
    if (fromDate) {
      queryParams.set('datestart', moment(fromDate).format('MMDDYY'));
    }
    if (toDate) {
      queryParams.set('dateend', moment(toDate).format('MMDDYY'));
    }
    if (page) {
      queryParams.set('page', page);
    }
    url.search = queryParams.toString();
    localStorage.setItem('WORK_ORDERS_SEARCH', url.search)
    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString());
  }

  onChangePage = (page, pageSize) => {
    this.setState({
      page: page-1
    }, () => {
      this.updateURL();
    })
  }

  onCreate = (clientNumber: string) => {

  }

  onClickShow = () => {

  }

  render() {
    const { fromDate, toDate, page } = this.state
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Work Orders'}>
        <WorkOrdersContainer>
          <HeaderTitle>Work Orders</HeaderTitle>
          <Row >
            <Col span={8}>
              <WorkOrderHeader onDateChange={this.onDateChange} dates={[fromDate, toDate]} />
            </Col>
            {/* <Col offset={12}>
              <ThemeButton size='large' shape='round' onClick={this.onClickShow} style={{margin: 20}}>New Order</ThemeButton>
            </Col> */}
          </Row>
          <WorkOrderTable history={this.props.history} page={page} onChangePage={this.onChangePage} />
        </WorkOrdersContainer>
      </PageLayout>
    )
  }
}


const mapStateToProps = (state: GlobalState) => state.manufacturing
export default withTheme(connect(ManufacturingModule)(mapStateToProps)(WorkOrdersList))
