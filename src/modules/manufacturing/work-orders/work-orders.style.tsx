import styled from '@emotion/styled'
import { Icon, Tabs, Table, DatePicker, Carousel, Row, Col, Modal } from 'antd'
import { mediumGrey } from '~/common'

export const WorkOrdersHeader = styled.div`
  margin-bottom: 30px;
  
`

export const LabelSpan = styled('div')({
  fontSize: '12px',
  color: mediumGrey,
  textAlign: 'left',
  fontWeight: 'bold',
})

export const SubLabelSpan = styled('div')({
  fontSize: '15px',
  fontWeight: 'bold',
  color: mediumGrey,
  textAlign: 'left',
  lineHeight: '17px'
})

export const ValueSpan = styled('div')({
  fontSize: '17px',
  color: mediumGrey,
  textAlign: 'left',
  lineHeight: '144%',
  fontWeight: 'bold',

  '&.orders-number': {
    fontWeight: 'normal',
    color: '#22282A',
    fontSize: '12px',
    lineHeight: '115%',
  }
})

export const HeaderColWrapper = styled('div')((props: any) => ({
  width: '100%',
  padding: '11px 30px',
  '& .related-link': {
    color: props.theme.dark,
    cursor: 'pointer',
  }, '& .related-link:hover': {
    textDecoration: 'underline',
  }
})

export const HeaderRow = styled(Row)({
  border: `1px solid #cccccc`,
  borderLeft: 0,
  display: 'flex'
})

export const HeaderCol = styled(Col)({
  borderLeft: `1px solid #cccccc`,
  '& .ant-form-item': {
    margin: 0
  }
})

export const TemplateCheckBox = styled('div')((props) => ({
  border: `1px solid ${props.theme.primary}`,
  borderRadius: 5,
  paddingTop: 13,
  width: 139,
  height: 41,
  float: 'right',
  marginTop: 5
}))

export const ThemeLabel = styled('span')((props) => ({
  color: props.theme.main,
}))

export const oneRowButtons: React.CSSProperties = {
  float: 'right',
  marginTop: -5,
  marginRight: 10
}

export const ButtonPad = styled('div')((props) => ({
  width: 10,
  height: 10,
  float: 'right',
}))

export const Rectangle = styled('div')((props) => ({
  height: 102,
  borderRadius: 6,
  backgroundColor: props.theme.lighter,
  marginTop: 5,
  padding: '18px 11px'
}))

export const MoreIcon: any = styled(Icon)((props) => ({
  color: props.theme.main,
  borderColor: props.theme.main,
  fontSize: 20,
}))


export const resourceRow: React.CSSProperties = {
  marginBottom: 12,
  fontSize: 13
}

export const resNo: React.CSSProperties = {
  fontWeight: 'bold',
  color: 'black'
}

export const TableWrapper = styled('div')({
  padding: '33px 0',
  '& .no-hr-border td .editable-cell-value-wrap': {
    justifyContent: 'center'
  }
})

export const Totals = styled('div')({
  marginTop: 13,
  marginLeft: 7
})

export const TotalsLabel = styled('div')({
  fontSize: 12,
  fontWeight: 'bold',
  color: 'black',
  lineHeight: '108%',
})

export const AuditLogTabs = styled(Tabs)((props: any) => ({
  '&': {
    '&.ant-tabs.ant-tabs-card': {
      '.ant-tabs-nav': {
        display: 'flex',
        marginTop: 3,
        '>div': {
          flex: 1,
          display: 'flex',
          '.ant-tabs-tab': {
            flex: 1,
            fontSize: 18,
            height: 37,
            borderWidth: 0,
            textAlign: 'center',
            backgroundColor: '#F3F3F3',
            marginRight: 5
          },
          '.ant-tabs-tab-active': {
            backgroundColor: props.theme.main,
            color: 'white',
            fontWeight: 'bold',
            height: 40,
          },
          '.ant-tabs-tab:hover': {
            color: props.theme.primary,
          },
        },
      },
    },
    '.ant-tabs-bar': {
      borderBottom: `5px solid ${props.theme.main}`,
      paddingBottom: 1,
      marginRight: -5
    },
  },
}))

export const ModalTable = styled(Table)((props) => ({
  '&': {
    backgroundColor: 'white',
    padding: 0,
    '.ant-table-pagination.ant-pagination': {
      marginRight: 16,
      '.ant-pagination-item-active': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-item:focus, .ant-pagination-item:hover': {
        borderColor: props.theme.light,
        'a': {
          color: props.theme.light
        }
      },
      '.ant-pagination-prev:focus .ant-pagination-item-link, .ant-pagination-next:focus .ant-pagination-item-link, .ant-pagination-prev:hover .ant-pagination-item-link, .ant-pagination-next:hover .ant-pagination-item-link': {
        borderColor: props.theme.light,
        color: props.theme.light
      }
    },
    '& .ant-table-content': {
      '.ant-table-thead > tr > th': {
        textAlign: 'left',
      },
      '& .ant-table-body': {
        '& > table': {
          '& .ant-table-thead': {
            '& > tr > th': {
              backgroundColor: 'white',
              color: '#555F61',
              fontSize: 12
            },
          },
          '& .ant-table-tbody': {
            '& > tr > td': {
              fontSize: 12,
              fontWeight: 'normal',
              color: '#373D3F',
              paddingLeft: 10
            },
          },
        },
      },
    },
  },
}))

export const formLayoutStyle: React.CSSProperties = {
  backgroundColor: 'white',
  paddingTop: 25,
  paddingLeft: 10,
  paddingRight: 10,
  paddingBottom: 15,
  borderRadius: 6,
}

export const editFormStyle: React.CSSProperties = {
  border: '1px solid #C1C3C1',
  borderRadius: 6
}

export const tableBorderStyle: React.CSSProperties = {
  border: '1px solid #C1C3C1',
  borderRadius: 6,
  marginTop: 15,
  overflow: 'hidden'
}

export const BorderedDatePicker = styled(DatePicker)((props) => ({
  '&': {
    backgroundColor: 'white',
    'input': {
      border: '1px solid #C1C3C1',
      padding: '0 6px',
      borderRadius: 5
    }
  }
}))

export const ScrollWrapper = styled(Row)({
  height: 150,
  padding: '20px 10px',
  width: '95%',
  margin: 'auto'
})

export const DetailImage = styled(Row)({
  height: 400,
  padding: 20,
  width: '95%',
  margin: 'auto',

  '& img': {
    border: '1px solid black',
    height: '100%'
  }
})

export const ArrowWrapper = styled('div')({

})

export const Instruction = styled(Col)({
  '& textarea': {
    height: '500px !important',
    margin: '20px 10px'
  }
})

export const ThemeCarousel = styled(Carousel)((props) => ({
  '&': {
    marginTop: -10,
    paddingBottom: 15,

    '.slick-dots li': {
      'button': {
        backgroundColor: '#C4C4C4',
        width: 12,
        height: 12,
        margin: '0 4px',
        borderRadius: 6
      },
      '&.slick-active': {
        'button': {
          width: 12,
          backgroundColor: props.theme.primary
        }
      }
    },
    '.ant-card-grid': {
      padding: 12,
      'img': {
        width: '100%',
        cursor: 'zoom-in'
      }
    }
  }
}))

export const CustomDatePicker = styled(DatePicker)((props) => ({
  '&': {
    '.ant-calendar-picker-icon': {
      width: 17,
      height: 18,
      marginTop: -9,
      color: props.theme.primary
    }
  }
}))

export const VerticalMenuWrapper = styled('div')({
  background: 'white',
  boxShadow: '0px 0px 6px 4px rgba(180, 180, 180, 0.3)',
  borderRadius: 2,

  '&>div': {
    padding: '10px 18px',
    cursor: 'pointer',
    '& span': {
      marginLeft: 20
    }
  }
})

export const WorkOrdersContainer = styled('div')({
  padding: '20px 40px 0 60px'
})

export const WorkOrderHeaderContainer = styled('div')({
  padding: '20px'
})

export const CustomDropDownWrapper = styled('div')({
  position: 'relative'
})

export const NotePopoverWrapper = styled('div')({
  position: 'relative'
})

export const ResourceCardContainer = styled('div')({
  margin: 10,
  padding: 12,
  border: '1px solid lightgrey',
  borderRadius: 4,
})

export const CardHeader = styled('div')({
  display: 'flex',
  justifyContent: 'space-between'
})

export const NameSpan = styled('span')({
  fontSize: 16,
  fontWeight: 'bold',
  padding: '0 10px'
})

export const HeaderItemWrapper = styled('div')((props) => ({
  '& svg path': {
    fill: props.theme.primary
  }
}))

export const StatusWrapper = styled('div')((props) => ({
  '& svg': {
    marginRight: 4
  },
  '& svg path': {
    fill: props.theme.primary
  }
}))

export const MatrixTableWrapper = styled('div')((props) => ({
  marginTop: 8,
  '& .ant-table tbody': {
    maxHeight: 200,
    overflowY: 'auto',
    display: 'block'
  },
  '& .ant-table thead': {
    display: 'table',
    width: '100%',
    tableLayout: 'fixed'
  },
  '& .ant-table tbody tr': {
    display: 'table',
    width: '100%',
    tableLayout: 'fixed'
  },

}))

export const dragItemWrapperStyle: React.CSSProperties = {
  border: '1px solid gray',
  borderRadius: 4,
  padding: '1rem',
  marginBottom: '.5rem',
  backgroundColor: 'white',
  position: 'relative'
}

export const dragItemStyle: React.CSSProperties = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}

export const handlerStyle: React.CSSProperties = {
  cursor: 'move',
}

export const expandStyle: React.CSSProperties = {
  cursor: 'pointer',
  marginRight: 10
}

export const flexStyle: React.CSSProperties = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}

export const MaterialsHeaderAction = styled('div')((props: any) => ({
  position: 'absolute',
  right: 0,
  top: 3,
  marginRight: 10,
  fontSize: 14,
  'a:hover': {
    textDecoration: 'underline'
  },
  '.receive-materials': {
    color: mediumGrey
  }
}))

export const StatusSpan = styled.span((_: any) => ({
  display: 'flex',
  alignItems: 'baseline',
  textTransform: 'uppercase',

  '&.capitalize': {
    textTransform: 'capitalize',
  },

  strong: {
    marginRight: '3px',
  },
  
  '&:before': {
    display: 'inline-block',
    content: '""',
    width: '12px',
    height: '12px',
    borderRadius: '12px',
    marginRight: '10px',
  },

  '&.REQUEST:before': {
    border: '1px solid #B83D16',
    backgroundColor: '#B83D16',
  },

  '&.SCHEDULED:before': {
    border: '1px solid #FEAC0E',
    backgroundColor: '#FEAC0E',
  },

  '&.WIP:before': {
    border: '1px solid #DADADA',
    backgroundColor: '#FFFFFF',
  },

  '&.COMPLETED:before': {
    border: '1px solid #2F80ED',
    backgroundColor: '#2F80ED',
  },

  '&.SHIPPED:before': {
    border: '1px solid #2BC46C',
    backgroundColor: '#2BC46C',
  },

}))

export const StyleModal = styled(Modal)((props: any) => ({
  '.ant-modal-body': {
    padding: '0 0 20px'
  }
}))