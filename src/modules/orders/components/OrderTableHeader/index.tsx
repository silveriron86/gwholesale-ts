/**@jsx jsx */
/** @jsxFrag Preact.Fragment */
import React from 'react'
import { jsx } from '@emotion/core'
import { Select, DatePicker, Button, Form, Spin, Row, Col } from 'antd'
import { FormComponentProps } from 'antd/es/form'
import moment from 'moment'
import { NewOrderFormModal } from '~/modules/orders/components'
import { getFulfillmentDate } from '~/common/utils'

import {
  Header,
  BackButton,
  backButtonCss,
  OrderWrapper,
  OrderTitle,
  OrderId,
  OrderEditor,
  CustomerSelector,
  CustomerSelectorTitle,
  selectStyles,
  DateTitle,
  DateEditorWrapper,
  ItemWrapper,
  ItemTitle,
  ItemCount,
  PriceWrapper,
  PriceTitle,
  PriceTotal,
  ButtonWrapper,
  buttonStyles,
  buttonStyles2,
  TokenOrderHeader,
  overrideFormCss,
  CompanyInfoWrapper,
  LogoWrapper,
  TokenOrderHeaderTitle,
} from './styles'
import { Icon } from '~/components'
import { OrderLoading, OrdersDispatchProps } from '../../orders.module'
import { Moment } from 'moment'
import { formatNumber, formatAddress } from '~/common/utils'
import {
  ThemeButton,
  ThemeIcon,
  ThemeSelect,
  ThemeTextArea,
  ThemeTextButton,
} from '~/modules/customers/customers.style'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import _ from 'lodash'
export interface CustomerFormFields {
  deliveryDate: Moment
  clientId: string
}

interface OrderTableHeaderProps {
  customers: any
  loadingIndex: string
  loadIndex: OrdersDispatchProps['loadIndex']
  itemCount: number
  totalPrice: number
  onCreateOrder: (payload: CustomerFormFields) => void
  orderMeta?: {
    title: string
    id: string
    selector: string
    clientCompany: string
    logoUrl: string
  }
  selectedCustomer?: any
  onClickBack: () => ReduxActions.Action<void>
  isRemovedCats: boolean
  onUpdateCatsView: Function
  isTokenOrder: boolean
  tokenPriceSheet: any
  noBack?: boolean
}

export class OrderTableHeader extends React.PureComponent<
  FormComponentProps<CustomerFormFields> & OrderTableHeaderProps
> {
  state = {
    createShow: false,
  }
  getState = (): Promise<CustomerFormFields | null> => {
    const { selectedCustomer } = this.props
    return new Promise((resolve) => {
      this.props.form.validateFields((err, values) => {
        if (!err) {
          values.clientId = selectedCustomer
          resolve(values)
        }
        resolve()
      })
    })
  }

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    // this.props.loadIndex(OrderLoading.OrderPlacing)
    // const payload = await this.getState()
    // if (payload) {
    //   this.props.onCreateOrder(payload)
    // } else {
    //   this.props.loadIndex('')
    // }
    const { selectedCustomer, isTokenOrder } = this.props
    if (isTokenOrder) {
      if (selectedCustomer) {
        this.onClickCreate(selectedCustomer)
      }
    } else {
      if (selectedCustomer) {
        this.onClickCreate(selectedCustomer)
      } else {
        this.setState({
          createShow: true,
        })
      }
    }
  }

  onCloseNewOrder = () => {
    this.setState({
      createShow: false,
    })
  }

  onClickCreate = (selectedCustomer: any) => {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.loadIndex(OrderLoading.OrderPlacing)
        values.clientId = selectedCustomer
        this.props.onCreateOrder(values)
        this.setState({
          createShow: false,
        })
      }
    })
  }

  disabledDate = (current: any) => {
    // Can not select days before today and today
    return (
      current &&
      current <
        moment()
          .subtract(1, 'days')
          .local(true)
          .endOf('day')
    )
  }

  judgeOrderQuantity = () => {
    const { tokenPriceSheet } = this.props
    let result = true
    tokenPriceSheet.priceSheetItemList.map((priceSheetItem: any) => {
      if (priceSheetItem.orderQuantity) {
        result = false
      }
    })
    return result
  }

  formatUserSetting = (data: any) => {
    if (data) {
      return {
        ...data.seller,
        company: data.wholesaleCompany,
      }
    } else {
      return null
    }
  }

  render() {
    const {
      customers,
      loadingIndex,
      orderMeta = {} as any,
      form: { getFieldDecorator },
      selectedCustomer,
      noBack
    } = this.props
    const disableButton = this.judgeOrderQuantity()
    const logoImage =
      !orderMeta.logoUrl || orderMeta.logoUrl === 'default' ? (
        <LogoWrapper>
          <Icon type="logo" viewBox={void 0} style={defaultLogoStyle} />
        </LogoWrapper>
      ) : (
        <img src={orderMeta.logoUrl} />
      )
    const userSetting = this.formatUserSetting(selectedCustomer)
    return (
      <>
        <NewOrderFormModal
          visible={this.state.createShow}
          onOk={this.onClickCreate}
          onCancel={this.onCloseNewOrder}
          clients={customers}
          okButtonName="CREATE ORDER"
          modalType="order"
          isNewOrderFormModal={true}
        />
        <Form onSubmit={this.handleSubmit} css={overrideFormCss}>
          <TokenOrderHeader>
            {noBack !== true && (
              <Row type="flex" justify="space-between">
                <BackButton onClick={this.props.onClickBack} style={{ float: 'left' }}>
                  <Icon viewBox="-3 -3 16 16" width="24" height="24" type="arrow-left" css={backButtonCss} />
                  BACK
                </BackButton>
                {/* <CompanyInfoWrapper style={{ marginRight: 20 }}>
                  {logoImage}
                  <span>{orderMeta.selector}</span>
                </CompanyInfoWrapper> */}
              </Row>
            )}
            <Row>
              <Col span={10}>
                <Row className="mb20 mt30">
                  <OrderId>{orderMeta.clientCompany || '#'}</OrderId>
                </Row>
                <Row>
                  <Col span={10}>
                    <TokenOrderHeaderTitle>Delivery address</TokenOrderHeaderTitle>
                    <Form.Item style={{ flex: 1 }}>
                      {getFieldDecorator('shippingAddressId', {
                        // rules: [{ required: true }],
                        initialValue:
                          selectedCustomer && selectedCustomer.mainShippingAddress
                            ? selectedCustomer.mainShippingAddress.wholesaleAddressId
                            : '',
                      })(
                        <ThemeSelect
                          style={{ flex: 1 }}
                          showSearch
                          // optionFilterProp="children"
                          // onChange={this.onDeliveryAddressChange}
                          onSearch={() => {}}
                          // filterOption={(input: any, option: any) =>
                          //   option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          // }
                        >
                          {selectedCustomer &&
                            _.isArray(selectedCustomer.wholesaleAddressList) &&
                            selectedCustomer.wholesaleAddressList.map((wholesaleAddress: any, index: number) => {
                              if (wholesaleAddress.address && wholesaleAddress.addressType == 'SHIPPING') {
                                return (
                                  <Select.Option key={`sa-${index}`} value={wholesaleAddress.wholesaleAddressId}>
                                    {formatAddress(wholesaleAddress.address)}
                                  </Select.Option>
                                )
                              }
                            })}
                        </ThemeSelect>,
                      )}
                    </Form.Item>
                  </Col>
                  <Col span={10} offset={1}>
                    <TokenOrderHeaderTitle>Requested delivery date</TokenOrderHeaderTitle>
                    <Form.Item>
                      {getFieldDecorator('deliveryDate', {
                        initialValue: moment(getFulfillmentDate(userSetting)),
                        rules: [{ type: 'object', required: true, message: 'Please select delivery date!' }],
                      })(
                        <DatePicker
                          style={{ width: 300 }}
                          placeholder="MM/DD/YYYY"
                          format="MM/DD/YYYY"
                          disabledDate={this.disabledDate}
                          allowClear={false}
                          suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                        />,
                      )}
                    </Form.Item>
                    {/* <DateEditorWrapper className="removeMarginLeft">
                      <Form.Item>
                        {getFieldDecorator('deliveryDate', {
                          initialValue: moment(),
                          rules: [{ type: 'object', required: true, message: 'Please select delivery date!' }],
                        })(
                          <DatePicker
                            style={{ width: 300 }}
                            placeholder="MM/DD/YYYY"
                            format="MM/DD/YYYY"
                            disabledDate={this.disabledDate}
                            allowClear={false}
                            suffixIcon={<Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                          />,
                        )}
                      </Form.Item>
                      <TokenOrderHeaderTitle>Requested delivery date</TokenOrderHeaderTitle>
                    </DateEditorWrapper> */}
                  </Col>
                </Row>
                <Row className="mt20">
                  <Col span={10}>
                    <TokenOrderHeaderTitle>Orders notes</TokenOrderHeaderTitle>
                    <Form.Item style={{ flex: 1 }}>
                      {getFieldDecorator('pickerNote', {
                        initialValue: '',
                      })(
                        <ThemeTextArea
                          rows={4}
                          //  onChange={this.handleChange.bind(this)}
                        />,
                      )}
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={4}></Col>
              <Col span={10}>
                <Row>
                  <CompanyInfoWrapper style={{ marginRight: 20 }}>
                    {logoImage}
                    <span>{orderMeta.selector}</span>
                  </CompanyInfoWrapper>
                </Row>
                <Row className="mt30">
                  <Col offset={4} span={16}>
                    <Row type="flex" justify="space-between">
                      <ItemWrapper>
                        <ItemCount>{this.props.itemCount}</ItemCount>
                        <ItemTitle>ITEM(S)</ItemTitle>
                      </ItemWrapper>
                      <PriceWrapper>
                        <PriceTotal>
                          {!!selectedCustomer?.displayPriceSheetPrice
                            ? `$ ${formatNumber(this.props.totalPrice, 2)}`
                            : 'Pending'}
                        </PriceTotal>
                        <PriceTitle>TOTAL PRICE EST.</PriceTitle>
                      </PriceWrapper>
                      <ButtonWrapper style={{ paddingBottom: 10 }}>
                        <ThemeButton
                          htmlType="submit"
                          type="primary"
                          size="large"
                          css={buttonStyles2}
                          disabled={loadingIndex.length > 0 || disableButton}
                        >
                          {loadingIndex === OrderLoading.OrderPlacing ? (
                            <span>
                              <Spin /> Placing Order
                            </span>
                          ) : (
                            <span>Place Order</span>
                          )}
                        </ThemeButton>
                      </ButtonWrapper>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="mt30">
              <Col span={10}>
                <ThemeTextButton className="bold" onClick={this.props.onUpdateCatsView}>
                  {this.props.isRemovedCats === true ? 'Group by category' : 'Remove category grouping'}
                </ThemeTextButton>
              </Col>
            </Row>
          </TokenOrderHeader>
        </Form>
      </>
    )
  }
}

const EnhancedOrderTableHeader = Form.create<FormComponentProps<CustomerFormFields> & OrderTableHeaderProps>()(
  OrderTableHeader,
)

export default EnhancedOrderTableHeader
