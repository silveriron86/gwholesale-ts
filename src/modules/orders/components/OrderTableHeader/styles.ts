import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { mediumGrey, black, grey, darkGreen, white, topLightGrey, darkGrey } from '~/common'
import { Col, Row } from 'antd'

export const Header = styled('div')({
  display: 'flex',
  width: '100%',
  padding: '30px 0 30px 30px',
})

export const TokenOrderHeader = styled('div')({
  padding: '30px 50px',
  '& .bold': {
    padding: '0 !important',
  },
})

export const BackButton = styled('div')((props) => ({
  color: props.theme.primary,
  display: 'flex',
  justifyContent: 'center',
  userSelect: 'none',
  cursor: 'pointer',
}))

export const backButtonCss = css({
  marginRight: '5px',
})

export const OrderWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  marginLeft: '5%',
})

export const OrderTitle = styled('span')({
  color: mediumGrey,
})

export const OrderId = styled('div')({
  fontSize: '36px',
  lineHeight: '50px',
  color: black,
  fontWeight: 'bold',
})

export const OrderEditor = styled('div')({
  display: 'flex',
})

export const CustomerSelector = styled('div')({
  display: 'flex',
})

export const CustomerSelectorTitle = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  color: grey,
  fontSize: '14px',
  lineHeight: '33px',
})

export const overrideFormCss = css({
  '& .ant-form-explain': {
    position: 'absolute',
    left: 0,
    top: '100%',
  },
  '& .ant-form-item': {
    marginBottom: 0,
  },
})

export const selectStyles = css({
  width: '275px',
  '& .ant-select-selection': {
    border: 'none',
    borderBottom: '1px solid black',
    boxShadow: 'none',
    borderRadius: 0,
    '& > div': {
      margin: 0,
    },
    '&:hover': {
      border: 'none',
      borderBottom: '1px solid black',
    },
    '&:active': {
      boxShadow: 'none',
    },
  },
})

export const DateEditorWrapper = styled('div')((props) => ({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
  '& .ant-calendar-picker': {
    '& .ant-calendar-picker-input': {
      border: 'none',
      padding: '4px 0',
      borderBottom: 'solid 1px black',
      borderRadius: 0,
      '&:focus': {
        boxShadow: 'none',
      },
    },
    '& .ant-calendar-picker-icon': {
      width: '20px',
      height: '21px',
      marginTop: '-10px',
      color: props.theme.primary,
    },
  },
}))

export const DateTitle = styled('span')({
  color: grey,
  marginBottom: '10px',
})

export const TokenOrderHeaderTitle = styled('span')({
  color: black,
  marginBottom: '10px',
})

export const ItemWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
})

export const ItemTitle = styled('span')({
  color: black,
  fontSize: '14px',
  marginBottom: '8px',
})

export const ItemCount = styled('span')({
  color: black,
  lineHeight: '35px',
  fontSize: '30px',
})

export const PriceWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
})

export const PriceTitle = styled('span')({
  color: black,
  fontSize: '14px',
  marginBottom: '9px',
})

export const PriceTotal = styled('span')({
  color: black,
  fontSize: '30px',
  lineHeight: '35px',
})

export const ButtonWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
})

export const buttonStyles = css({
  width: '180px',
  borderRadius: '20px',
})

export const buttonStyles2 = css({
  width: '180px',
  borderRadius: '4px',
})

export const CompanyInfoWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  textAlign: 'center',

  '& img': {
    maxWidth: 180,
    margin: 'auto',
  },

  '& span': {
    fontSize: 20,
    fontWeight: 'bold',
  },
  '.left': {
    textAlign: 'left',
  },
})

export const CompanyInfoHorizontalWrapper = styled('div')({
  display: 'flex',

  '& img': {
    maxHeight: 40,
  },
})

export const PaddingWrapper = styled(Col)({
  padding: '10px 50px',
})

export const SmallPaddingWrapper = styled(Col)({
  padding: '10px 20px',
})

export const HeaderRow = styled(Row)((props) => ({
  backgroundColor: props.theme.light,
  marginTop: 10,
  color: white,
  fontWeight: 'bold',
  fontSize: 16,
}))

export const GreyBody = styled(Row)({
  backgroundColor: topLightGrey,
})

export const BoldTitle = styled('h1')({
  fontWeight: 'bold',
})

export const BoldTextWrapper = styled('span')({
  color: black,
  fontWeight: 'bold',
})

export const Space = styled('div')({
  width: '100%',
  height: 2,
  background: 'grey',
  margin: '20px 0',
})

export const Notes = styled('div')({
  fontSize: 12,
  fontWeight: 300,
  letterSpacing: 0,
})

export const BiggerText = styled(Col)({
  fontSize: 16,
})

export const BackColorText = styled('span')((props) => ({
  backgroundColor: props.theme.light,
  color: white,
  fontWeight: 'bold',
  fontSize: 16,
  padding: 10,
}))

export const PurchaseOrderPrintTable = styled('div')((props) => ({
  '& .ant-table table thead tr th': {
    backgroundColor: props.theme.light,

    '& span': {
      color: white,
      fontWeight: 'bold !important',
      fontSize: '14px !important',
    },
  },
  '&.jana-purchase-print': {
    'th.hidden-in-jana, td.hidden-in-jana': {
      display: 'none',
    },
  },
}))

export const LogoWrapper = styled('div')({
  textAlign: 'center',
  '& svg path': {
    fill: `${darkGreen} !important`,
  },
})

export const PurchaseItem = styled('div')({
  color: darkGrey,
  padding: '30px 50px',
})
