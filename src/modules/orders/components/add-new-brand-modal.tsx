/**@jsx jsx */
import { ClassNames, jsx } from '@emotion/core'
import { Select, Button } from 'antd'
import React from 'react'
import {
  InputLabel,
  ThemeButton,
  ThemeModal,
  ThemeSelect
} from '~/modules/customers/customers.style'

import { OrderHeaderProps } from './orders-header.component'
import { DialogBodyDiv, DialogSubContainer, noPaddingFooter } from '~/modules/customers/nav-sales/styles'
import _ from 'lodash'

export interface AddNewBrandModalProps {
  visible: boolean
  // currentSuppliers: string[]
  companyProductTypes: any[]
  itemName: string
  itemId: any
  oldBrands: string[]
  updateProduct: Function
  setCompanyProductTypes: Function
  onVisibleChange: Function
  onChangeBrandList: Function
  updateDatasource: Function
}

export default class AddNewBrandModal extends React.PureComponent<AddNewBrandModalProps> {
  state = {
    currentSuppliers: [],
    dropdownVisible: false,
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.visible && !nextProps.visible) {
      this.setState({ currentSuppliers: [] })
    }
  }

  addNewBrand = () => {
    const { companyProductTypes, itemId, oldBrands } = this.props
    const { currentSuppliers } = this.state
    const all = companyProductTypes ? companyProductTypes.suppliers.map((el: any) => el.name) : []
    const addedBrands = _.differenceWith(currentSuppliers, all)
    const isSame = _.isEqual(oldBrands.sort(), currentSuppliers.sort())
    const saved = _.union(oldBrands, currentSuppliers)
    if (addedBrands.length) {
      const updateProductTypes = {
        type: 'suppliers',
        name: addedBrands.join(';')
      }
      this.props.setCompanyProductTypes(updateProductTypes)
    }
    if (!isSame) {
      if (itemId) {
        const updateData = {
          wholesaleItemId: itemId,
          suppliers: saved.join(', '),
        }
        this.props.updateProduct(updateData)
      }
    }
    this.props.updateDatasource(saved)
  }

  getAvailableBrands = () => {
    const { companyProductTypes, oldBrands } = this.props
    const { currentSuppliers } = this.state
    const cur = _.union(currentSuppliers, oldBrands)
    const all = companyProductTypes ? companyProductTypes.suppliers.map((el: any) => el.name) : []
    const available: string[] = _.differenceWith(all, cur)
    return available.length ? available.sort((a: string, b: string) => a.localeCompare(b)) : []
  }

  onDropdownChange = (val: string[]) => {
    this.setState({ currentSuppliers: val.length > 0 ? [val[val.length - 1]] : val })
  }

  onInputKeyDown = (e: any) => {
    const { currentSuppliers, dropdownVisible } = this.state
    if (!dropdownVisible && currentSuppliers.length >= 1) {
      // if a brand already is selected from dropdown list, do not allow selecting more or typing in the field
      if (e.keyCode !== 8) {
        // No allow except for when pressing "backspace" key
        e.preventDefault();
      }
    }
  }

  onDropdownVisibleChange = (dropdownVisible: boolean) => {
    this.setState({
      dropdownVisible,
    })
  }

  render() {
    const {
      itemName,
      visible,
      onVisibleChange,
      // onChangeBrandList,
    } = this.props

    const { currentSuppliers } = this.state
    const availableOptions = this.getAvailableBrands()
    return (
      <ClassNames>
        {({ css, cx }) => (
          <ThemeModal
            title={`Add new brand`}
            visible={visible}
            onCancel={onVisibleChange.bind(this, -1)}
            bodyStyle={{ padding: 0 }}
            className={`${cx(css(noPaddingFooter))}`}
            footer={
              <DialogSubContainer>
                <ThemeButton type="primary" onClick={this.addNewBrand}>
                  Add Brand
              </ThemeButton>
                <Button onClick={() => onVisibleChange(-1)}>CANCEL</Button>
              </DialogSubContainer>
            }
            width={600}
          >
            <DialogBodyDiv>
              <InputLabel style={{ fontSize: 18 }}>{itemName}</InputLabel>
              <ThemeSelect
                mode="tags"
                key
                className={'quick-enter-po-add-brand'}
                placeholder="Select or type a brand name..."
                style={{ width: '90%' }}
                value={currentSuppliers}
                onInputKeyDown={this.onInputKeyDown}
                showSearch
                onChange={(val: string[]) => this.onDropdownChange(val)}
                onDropdownVisibleChange={this.onDropdownVisibleChange}
                filterOption={(input: any, option: any) => {
                  if (option.props.children) {
                    return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  } else {
                    return true
                  }
                }}
                dropdownRender={(el: any) => {
                  return (
                    <div className='enter-purchase-add-brand-list'>
                      {el}
                    </div>
                  )
                }}
              >
                <Select.Option value="" />
                {availableOptions.map((p, i) => {
                  return (
                    <Select.Option key={`brand-${i}`} value={p}>
                      {p}
                    </Select.Option>
                  )
                })}
              </ThemeSelect>
            </DialogBodyDiv>

          </ThemeModal>
        )}
      </ClassNames>
    )
  }
}
