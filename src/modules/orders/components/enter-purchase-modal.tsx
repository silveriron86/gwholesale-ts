/**@jsx jsx */
import { ClassNames, jsx } from '@emotion/core'
import { AutoComplete, Select, Icon, Button, Popconfirm, DatePicker, notification, Modal, Divider } from 'antd'
import { of } from 'rxjs'
import React from 'react'
import moment from 'moment'
import jQuery from 'jquery'
import {
  InputLabel,
  ThemeButton,
  ThemeDatePicker,
  ThemeIcon,
  ThemeIconButton,
  ThemeInput,
  ThemeModal,
  ThemeOutlineButton,
  ThemeSelect,
  ThemeSpin,
  ThemeTable,
} from '~/modules/customers/customers.style'
import {
  formatNumber,
  formatAddress,
  initHighlightEvent,
  ratioQtyToBaseQty,
  baseQtyToRatioQty,
  ratioPriceToBasePrice,
  basePriceToRatioPrice,
  inventoryPriceToRatioPrice,
  ratioQtyToInventoryQty,
  mathRoundFun,
  judgeConstantRatio,
  formatItemDescription,
} from '~/common/utils'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import { OrderHeaderProps } from './orders-header.component'
import { EnterPurchaseWrapper } from './orders-header.style'
import UomSelect from '~/modules/customers/nav-sales/order-detail/components/uom-select'
import {
  DialogBodyDiv,
  DialogSubContainer,
  FlexDiv,
  noPaddingFooter,
  ValueLabel,
} from '~/modules/customers/nav-sales/styles'
import { ENGINE_METHOD_CIPHERS } from 'constants'
import { cloneDeep, isArray, isEmpty } from 'lodash'
import { format } from 'highcharts'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, responseHandler, addVendorProductCode } from '~/common/utils'
import _ from 'lodash'
import AddNewBrandModal from './add-new-brand-modal'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import { Icon as IconSvg } from '~/components'

export interface EnterPurchaseModalProps {
  onStartPurchase: Function
  onToggle: Function
  visible: boolean
  companyProductTypes?: any
  loadingSaleItems: boolean
  getAllItemsForAddPurchase: Function
  setOrResetLoadingForGetItems: Function
  updateProduct: Function
  setCompanyProductType: Function
  settingCompanyName: string
  sellerSetting: any
  getCompanyProductAllTypes: Function
  loadingSimplifyVendors: boolean
  loadingPurchaseProducts: boolean
  noText?: boolean
  hideSaveConfirm?: boolean
}

export default class EnterPurchaseModal extends React.PureComponent<EnterPurchaseModalProps & OrderHeaderProps> {
  focusedIndex: number = 0
  isKeyboardNavigate: boolean = false
  timeoutHandler: any = null
  state = {
    productSKUs: [],
    vendors: [],
    // productBrands: [],
    vendorId: null,
    items: [],
    deliveryDate: moment(),
    referenceNo: '',
    pickerNote: '',
    currentPage: 1,
    defaultActive: false,
    showItems: false,
    defaultCostType: this.props.sellerSetting.company ? this.props.sellerSetting.company.defaultCostType : 1,
    loading: false,
    openAddNewBrandModal: false,
    addNewBrandItemIndex: -1,
    currentSuppliers: [],
    visibleCOOModal: false,
  }

  componentDidMount() {
    this.setColNavigateEvent()
    this.setArrowNavigateEvent()
  }

  onRefreshData = () => {
    const { vendorId } = this.state
    this.props.getSimplifyVendors()
    this.props.getItemList({ type: 'PURCHASE' })
    if (vendorId) {
      this.props.getVendorProductsForEnterPurchase(vendorId)
    }
  }

  // componentWillReceiveProps(nextProps: EnterPurchaseModalProps & OrderHeaderProps) {
  //   //when user change vendor and items is not empty
  //   if (
  //     // nextProps.loadingSaleItems == false &&
  //     nextProps.loadingSaleItems !== this.props.loadingSaleItems &&
  //     this.state.items.length > 0
  //   ) {
  //     this.setNewCostToItem(nextProps.saleItems)
  //   }
  // }

  setColNavigateEvent = () => {
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'
    const _this = this
    setTimeout(() => {
      jQuery('.select-vendor .ant-input.ant-select-search__field').focus()
      jQuery('body')
        .unbind('keydown')
        .bind('keydown', function(e: any) {
          let buttons = jQuery('.enter-purchase-modal .ant-btn:not(:disabled):not(.remove-purchase-item)')
          let editableFields = jQuery('.enter-purchase-modal').find(
            '.ant-input:not(.ant-select-search__field), .ant-calendar-picker-input, .ant-select-enabled .ant-select-selection, .ant-input-number-input',
          )
          if ((e.keyCode == 9 || e.keyCode == 13) && isFreshGreen) {
            const customFocusedSelect = jQuery('.enter-purchase-modal .custom-ant-select-focused')

            if (jQuery(editableFields).index(e.target) >= 0) {
              _this.focusedIndex = jQuery(editableFields).index(e.target)
              jQuery.each(customFocusedSelect, (index: number, els: any) => {
                jQuery(els).removeClass('custom-ant-select-focused')
              })
            } else if (customFocusedSelect.length) {
              let childOfCustomSelect = jQuery(customFocusedSelect).find('.ant-select-selection')[0]
              if (!childOfCustomSelect) {
                childOfCustomSelect = jQuery(customFocusedSelect).find('.ant-calendar-picker-input')[0]
              }
              _this.focusedIndex = jQuery(editableFields).index(childOfCustomSelect)
            }

            if (jQuery(e.target).hasClass('ant-select-search__field')) {
              const parent = jQuery(e.target).parents('.ant-select-selection')
              _this.focusedIndex = jQuery(editableFields).index(parent)
            }

            if (e.shiftKey) {
              _this.focusedIndex--
              if (_this.focusedIndex < 0) {
                _this.focusedIndex = buttons.length + editableFields.length - 1
                _this.setFocusEl(buttons[buttons.length - 1], editableFields, e.keyCode)
              } else if (_this.focusedIndex < editableFields.length) {
                _this.setFocusEl(editableFields[_this.focusedIndex], editableFields, e.keyCode)
              }
            } else {
              _this.focusedIndex++
              if (_this.focusedIndex < editableFields.length) {
                _this.setFocusEl(editableFields[_this.focusedIndex], editableFields, e.keyCode)
              } else if (_this.focusedIndex == buttons.length + editableFields.length) {
                _this.focusedIndex = 0
                _this.setFocusEl(editableFields[0], editableFields, e.keyCode)
              } else if (_this.focusedIndex == editableFields.length) {
                _this.setFocusEl(buttons[0], editableFields, e.keyCode)
              }
            }
          } else if (e.keyCode == 9) {
            if (jQuery(e.target).hasClass('ant-select-search__field')) {
              const activeItem = jQuery('.ant-select-dropdown-menu-item-active')
              if (activeItem.length > 0) {
                if (jQuery(e.target).parents('.select-vendor').length > 0) {
                  e.preventDefault()
                  jQuery('.vendor-autocomplete .ant-select-dropdown-menu-item-active').trigger('click')
                } else {
                  e.preventDefault()
                  jQuery('.enter-po-autocomplete .ant-select-dropdown-menu-item-active').trigger('click')
                }
              } else {
                if (jQuery(e.target).parents('.select-vendor').length > 0 && e.shiftKey === true) {
                  e.preventDefault()
                  jQuery('.enter-purchase-modal .cancel-btn').focus()
                }
              }
            } else if (jQuery(e.target).hasClass('cancel-btn')) {
              if (e.shiftKey === false) {
                e.preventDefault()
                jQuery('.select-vendor .ant-input.ant-select-search__field').focus()
              }
            }
          } else if (
            (e.keyCode == 32 && jQuery(e.target).parents('.select-container-parent').length) ||
            ((e.keyCode == 37 || e.keyCode == 39) &&
              isFreshGreen &&
              _this.isTriggableByLeftRightKeyboard(editableFields[_this.focusedIndex]))
          ) {
            _this.setFocusEl(editableFields[_this.focusedIndex], editableFields, 32)
          }
        })
    }, 50)
  }

  isTriggableByLeftRightKeyboard = (el: any) => {
    if (
      (jQuery(el).parents('.uom-select-container').length || jQuery(el).parents('.type-pricingUOM').length) &&
      (jQuery('body').find('.ant-select-dropdown').length == 0 ||
        jQuery('body').find('.ant-select-dropdown').length == jQuery('body').find('.ant-select-dropdown-hidden').length)
    ) {
      return true
    } else {
      return false
    }
  }

  setArrowNavigateEvent = () => {
    const _this = this
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'
    setTimeout(() => {
      jQuery('body')
        .bind('keydown', function(e: any) {
          const trObj = jQuery(e.target).parents('tr')
          const rowKey = jQuery(trObj).data('row-key')
          const parent = jQuery(e.target).parents('.ant-select')
          const classes = jQuery(parent)
            .first()
            .attr('class')
          const classArr = classes ? classes.split(' ') : []
          if (!classArr.length) return
          const typeClassName = classArr ? classArr.find((el) => el.indexOf('type-') > -1) : ''
          if (!typeClassName) return
          const type = typeClassName.substring(5)
          const options = jQuery(`.enter-purchase-selector-${type}-${rowKey} li.ant-select-dropdown-menu-item`)
          const active = jQuery(`.enter-purchase-selector-${type}-${rowKey} li.ant-select-dropdown-menu-item-active`)
          const selected = jQuery(
            `.enter-purchase-selector-${type}-${rowKey} li.ant-select-dropdown-menu-item-selected`,
          )
          if (options.length > 1) {
            const index = jQuery(options).index(active[0])
            let newIndex = 0
            if (e.keyCode == 39) {
              if (index != options.length - 1) {
                newIndex = index + 1
              } else {
                newIndex = 0
              }
              setTimeout(() => {
                jQuery.each(options, function(i: number, el: any) {
                  jQuery(el).removeClass('ant-select-dropdown-menu-item-active')
                })
                jQuery(options[newIndex]).addClass('ant-select-dropdown-menu-item-active')
              }, 50)
              _this.isKeyboardNavigate = true
            } else if (e.keyCode == 37) {
              if (index != 0) {
                newIndex = index - 1
              } else {
                newIndex = options.length - 1
              }
              setTimeout(() => {
                jQuery.each(options, function(i: number, el: any) {
                  jQuery(el).removeClass('ant-select-dropdown-menu-item-active')
                })
                jQuery(options[newIndex]).addClass('ant-select-dropdown-menu-item-active')
              }, 50)
              _this.isKeyboardNavigate = true
            }
            if ((e.keyCode == 13 || e.keyCode == 32) && active[0]) {
              if (!selected[0] || (selected[0] && jQuery(active[0]).text() != jQuery(selected[0]).text())) {
                const fullIndex = (_this.state.currentPage - 1) * 12 + rowKey
                const value = jQuery(active[0]).text()

                setTimeout(() => {
                  if (type == 'pricingUOM') {
                    _this.onChangePricingUom(0, value, fullIndex, true)
                  } else {
                    _this.onChange(value, type, fullIndex, true)
                  }
                }, 50)

                setTimeout(() => {
                  if (isFreshGreen && e.keyCode == 32) {
                    const curEl = e.target
                    if (jQuery(curEl).hasClass('ant-select-selection')) {
                      const parent = jQuery(curEl).parents('.select-container-parent')
                      jQuery(e.target).trigger('blur')
                      jQuery(parent).addClass('custom-ant-select-focused')
                    }
                  }
                }, 100)

                e.preventDefault()
              }
            }
          }
        })
        .bind('keyup', (e: any) => {
          if (_this.isKeyboardNavigate && e.keyCode == 13) {
            _this.isKeyboardNavigate = false
          }
        })
    }, 50)
  }

  setFocusEl = (el: any, editables: any[] = [], keycode: number = -1) => {
    jQuery('.select-container-parent').removeClass('custom-ant-select-focused')
    jQuery('.datepicker-container').removeClass('custom-ant-select-focused')
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'
    setTimeout(() => {
      if (jQuery(el).hasClass('ant-select-selection')) {
        if (isFreshGreen && (keycode == 13 || keycode == 9)) {
          const parent = jQuery(el)
            .parent()
            .parent()
          if (jQuery(parent).hasClass('select-container-parent') || jQuery(parent).hasClass('ant-checkbox')) {
            jQuery(parent).addClass('custom-ant-select-focused')
          }
          jQuery.each(editables, function(i: number, map: any) {
            jQuery(map).trigger('blur')
            if (jQuery(map).find('.ant-select-search__field').length) {
              jQuery(jQuery(map).find('.ant-select-search__field')[0]).trigger('blur')
            }
          })
          if (jQuery(el).find('.ant-select-search__field').length) {
            jQuery(jQuery(el).find('.ant-select-search__field')[0]).trigger('focus')
          }
        } else if (keycode == 32) {
          jQuery(el).trigger('click')
        } else {
          jQuery(el)
            .children()[0]
            .focus()
        }
      } else if (jQuery(el).hasClass('ant-calendar-picker-input')) {
        if (isFreshGreen && (keycode == 13 || keycode == 9)) {
          const parent = jQuery(el)
            .parent()
            .parent()
          if (jQuery(parent).hasClass('datepicker-container')) {
            jQuery(parent).addClass('custom-ant-select-focused')
          }
          jQuery.each(editables, function(i: number, map: any) {
            if (jQuery(map).find('.ant-select-search__field').length) {
              jQuery(jQuery(map).find('.ant-select-search__field')[0]).trigger('blur')
            } else {
              jQuery(map).trigger('blur')
            }
          })
        } else if (keycode == 32) {
          jQuery(el).trigger('click')
        }
      } else if (
        jQuery(el).hasClass('ant-btn') ||
        jQuery(el).hasClass('ant-input') ||
        jQuery(el).hasClass('ant-input-number-input')
      ) {
        jQuery(el).trigger('focus')
        jQuery(el).trigger('select')
      }
    }, 100)
  }

  setNewCostToItem = (saleItems: any) => {
    const { items, defaultCostType } = this.state
    items.map((addItem: any) => {
      saleItems.map((dbItem: any) => {
        if (addItem.wholesaleItemId == dbItem.wholesaleItem.wholesaleItemId) {
          if (defaultCostType == 1) {
            addItem.cost = basePriceToRatioPrice(
              dbItem.wholesaleItem.defaultPurchasingCostUOM
                ? dbItem.wholesaleItem.defaultPurchasingCostUOM
                : dbItem.wholesaleItem.inventoryUOM,
              dbItem.wholesaleItem.cost,
              dbItem.wholesaleItem,
            )
          } else if (defaultCostType == 2) {
            addItem.cost = basePriceToRatioPrice(
              dbItem.wholesaleItem.defaultPurchasingCostUOM
                ? dbItem.wholesaleItem.defaultPurchasingCostUOM
                : dbItem.wholesaleItem.inventoryUOM,
              dbItem.cost,
              dbItem.wholesaleItem,
            )
          } else {
            addItem.cost = basePriceToRatioPrice(
              dbItem.wholesaleItem.defaultPurchasingCostUOM
                ? dbItem.wholesaleItem.defaultPurchasingCostUOM
                : dbItem.wholesaleItem.inventoryUOM,
              dbItem.cost,
              dbItem.wholesaleItem,
            )
          }
        }
      })
    })
  }

  renderCompanyName = (client: any, index: number) => {
    return (
      <Select.Option key={client.clientId} text={client.clientId}>
        {client.clientCompanyName}
      </Select.Option>
    )
  }

  handleFilterOption = (input, option) => {
    return option.props.children.toUpperCase().indexOf(input.toUpperCase()) !== -1
  }

  onSelectVendor = (vendorId: number) => {
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'
    const { defaultCostType } = this.state
    // this.props.setOrResetLoadingForGetItems()
    // this.props.getAllItemsForAddPurchase({ type: 'PURCHASE', clientId: vendorId })

    this.setState(
      {
        vendorId,
        vendors: [],
        showItems: true,
      },
      () => {
        this.props.getVendorProductsForEnterPurchase(vendorId)
        if (!isFreshGreen) {
          setTimeout(() => {
            jQuery('.ant-modal-body .ant-calendar-picker-input').focus()
          }, 100)
        }
        if (defaultCostType == 2) {
          const { items } = this.state
          if (isArray(items) && items.length > 0) {
            let ids = items.map((item) => item.wholesaleItemId).join(',')
            this.getCostByItemIdAndClientId(ids, (orderItems: any) => {
              this.setNewCostToItem(orderItems)
            })
          }
        }
      },
    )
  }

  onOrderInfoTextFieldChange = (type: string, evt: any) => {
    const stateObj = { ...this.state }
    stateObj[type] = evt.target.value
    this.setState(stateObj)
  }

  onChangeDeliveryDate = (value) => {
    this.setState({
      deliveryDate: value,
    })
  }

  onSelectSKU = async (id, option) => {
    let items = JSON.parse(JSON.stringify(this.state.items))
    let wholesaleItemId = null
    let extraChargeId: number = null

    if (id.indexOf('extra_') === 0) {
      extraChargeId = parseInt(id.replace('extra_', ''), 10)
    } else {
      wholesaleItemId = id
    }

    if (wholesaleItemId !== null) {
      // to sales cart
      const { saleItems, sellerSetting } = this.props
      const { defaultCostType } = this.state

      const found = saleItems.find((item) => {
        return item.wholesaleItemId == wholesaleItemId
      })

      if (found) {
        const {
          variety,
          suppliers,
          constantRatio,
          quantity,
          cost,
          ratioUOM,
          inventoryUOM,
          baseUOM,
          wholesaleProductUomList,
          defaultPurchasingCostUOM,
          defaultPurchaseUnitUOM,
          SKU,
        } = found
        let newCost = basePriceToRatioPrice(
          defaultPurchasingCostUOM ? defaultPurchasingCostUOM : inventoryUOM,
          cost,
          found,
        )
        if (defaultCostType != 1) {
          await new Promise((resolve, reject) => {
            this.getCostByItemIdAndClientId(id, (orderItems: any) => {
              newCost = isArray(orderItems) && orderItems[0] ? orderItems[0].cost : 0
              newCost = basePriceToRatioPrice(
                defaultPurchasingCostUOM ? defaultPurchasingCostUOM : inventoryUOM,
                newCost,
                found,
              )
              resolve(newCost)
            })
          })
        }
        items.push({
          wholesaleItemId,
          wholesaleOrderItemId: wholesaleItemId,
          UOM: found.defaultPurchaseUnitUOM || found.inventoryUOM,
          variety: formatItemDescription(variety, SKU, sellerSetting),
          brand: '',
          extraOrigin: '',
          quantity: '',
          weight: 0,
          cost: newCost,
          inventoryUOM,
          baseUOM,
          constantRatio,
          ratioUOM,
          SKU,
          supplier: suppliers ? suppliers.split(', ') : [],
          wholesaleProductUomList,
          defaultPurchaseUnitUOM,
          pricingUOM: defaultPurchasingCostUOM ? defaultPurchasingCostUOM : inventoryUOM,
        })
        this.setState({ items, productSKUs: [] }, () => {
          // jQuery('.ant-modal-body .ant-select-search__field').focus()
          setTimeout(() => {
            jQuery('.ant-modal-body .ant-table-tbody .quantity-input')
              .last()
              .focus()
          }, 100)

          initHighlightEvent()
        })
      }
    } else {
      // to extra charges
      const extraCharges = this.props.companyProductTypes ? this.props.companyProductTypes.extraCharges : []
      const found = extraCharges.find((ec: any) => {
        return ec.id === extraChargeId
      })
      if (found) {
        const { id, name } = found
        items.push({
          wholesaleItemId: id,
          variety: `${name} [Extra charge]`,
          itemName: name,
          brand: '',
          extraOrigin: '',
          quantity: '',
          weight: 0,
          cost: 0,
          inventoryUOM: 'Each',
          baseUOM: 'Each',
          constantRatio: false,
          ratioUOM: 'Each',
          SKU: '',
          supplier: [],
          wholesaleProductUomList: [],
          pricingUOM: 'Each',
          type: 'extra',
        })
      }

      this.setState({ items, productSKUs: [] }, () => {
        // jQuery('.ant-modal-body .ant-select-search__field').focus()
        setTimeout(() => {
          jQuery('.ant-modal-body .ant-table-tbody .quantity-input')
            .last()
            .focus()
        }, 100)

        initHighlightEvent()
      })
    }
  }

  matchesVendorCode = (v: string) => {
    let startMatchIds: string[] = []
    let includeMatchIds: string[] = []
    let idCodeMap = {}
    this.props.enterPurchaseProducts.forEach((item) => {
      let { vendorProductCode } = item

      if (!vendorProductCode) {
        return
      }
      vendorProductCode = vendorProductCode.toLowerCase()
      if (vendorProductCode.startsWith(v)) {
        startMatchIds.push(item.wholesaleItemId)
        idCodeMap[item.wholesaleItemId] = item.vendorProductCode
      } else if (vendorProductCode.includes(v)) {
        includeMatchIds.push(item.wholesaleItemId)
        idCodeMap[item.wholesaleItemId] = item.vendorProductCode
      }
    })
    const allMatches = startMatchIds.concat(includeMatchIds)

    return { startMatchIds, includeMatchIds, allMatches, idCodeMap }
  }

  debounceSearch = (v) => {
    clearTimeout(this.searchTimeout)
    this.searchTimeout = setTimeout(() => {
      this.onProductSearch(v, 'SKU')
    }, 200)
  }

  onProductSearch = (searchStr: string, type: string) => {
    if (searchStr.length < 2) {
      this.setState({
        productSKUs: [],
      })
      return
    }

    const searchText = searchStr.toLowerCase()
    const { saleItems } = this.props
    // tslint:disable-next-line:prefer-const
    let matches0: any[] = []
    let matches1: any[] = []
    let matches2: any[] = []
    let matches3: any[] = []
    let extraChargeMatches1: any[] = []
    let extraChargeMatches2: any[] = []

    const extraCharges = this.props.companyProductTypes ? this.props.companyProductTypes.extraCharges : []
    if (extraCharges.length > 0) {
      extraCharges.forEach((ec: any) => {
        if (ec.name.toLowerCase().indexOf(searchText) === 0) {
          extraChargeMatches1.push(this.formatExtraOption(ec))
        } else if (ec.name.toLowerCase().indexOf(searchText) > 0) {
          extraChargeMatches2.push(this.formatExtraOption(ec))
        }
      })

      if (extraChargeMatches1.length > 0) {
        extraChargeMatches1 = extraChargeMatches1.sort((a, b) => a.text.localeCompare(b.text))
      }
      if (extraChargeMatches2.length > 0) {
        extraChargeMatches2 = extraChargeMatches2.sort((a, b) => a.text.localeCompare(b.text))
      }
    }

    if (saleItems && saleItems.length > 0) {
      saleItems.forEach((item, index) => {
        let row = item
        if (typeof item.wholesaleItem !== 'undefined') {
          row = { ...item, ...item.wholesaleItem }
        }
        if (typeof row.SKU === 'undefined') {
          row.SKU = row.sku
        }

        const { allMatches, idCodeMap } = this.matchesVendorCode(searchText)

        if (row[type] && row[type].toLowerCase().indexOf(searchText) === 0) {
          // full match on 2nd filed
          matches2.push(this.formartOption(row, type))
        } else if (row.variety && row.variety.toLowerCase().indexOf(searchText) === 0) {
          // full match on variety
          matches1.push(this.formartOption(row, type))
        } else if (allMatches.includes(row.wholesaleItemId)) {
          matches0.push({ value: row.wholesaleItemId, text: `${row.variety}(${idCodeMap[row.wholesaleItemId]})` })
        } else if (
          (row[type] && row[type].toLowerCase().indexOf(searchText) > 0) ||
          (type === 'SKU' && row.variety && row.variety.toLowerCase().indexOf(searchText) > 0) ||
          (row.wholesaleCategory && row.wholesaleCategory.name.toLowerCase().indexOf(searchText) >= 0)
        ) {
          // others
          matches3.push(this.formartOption(row, type))
        }
      })
    }
    if (matches1.length > 0) {
      matches1 = matches1.sort((a, b) => a.text.localeCompare(b.text))
    }
    if (matches3.length > 0) {
      matches3 = matches3.sort((a, b) => a.text.localeCompare(b.text))
    }

    // When searches items, Autocomplete automatically focuses on the last 1st item.
    // Fix: It always should focus on the 1st row regardless of last one.
    this.setState({
      productSKUs: [],
      defaultActive: false,
    })

    setTimeout(() => {
      this.setState({
        productSKUs: [
          ...matches2,
          ...extraChargeMatches2,
          ...extraChargeMatches1,
          ...matches1,
          ...matches0,
          ...matches3,
        ],
      })
      if (this.timeoutHandler) {
        clearTimeout(this.timeoutHandler)
      }

      this.timeoutHandler = setTimeout(() => {
        this.setState({
          defaultActive: true,
        })
      }, 200)
    }, 50)
  }

  formartOption = (row: any, type: string) => {
    if (row.variety && row.variety.indexOf('BABY BELLA') > -1) {
      console.log(row)
    }
    return {
      value: row.wholesaleItemId,
      text: type === 'SKU' ? `${row.variety} ${row[type] ? '(' + row[type] + ')' : ''}` : row[type],
    }
  }

  formatExtraOption = (row: any) => {
    return {
      value: `extra_${row.id}`,
      text: `${row.name} [Extra charge]`,
    }
  }

  onVendorSearch = (searchStr: string) => {
    const { clients } = this.props

    // tslint:disable-next-line:prefer-const
    let options: any[] = []
    if (searchStr.length >= 2 && clients && clients.length > 0) {
      clients.forEach((client, index) => {
        if (client.clientCompanyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          options.push({
            value: client.clientId,
            text: client.clientCompanyName,
          })
        }
      })
    }

    this.setState({
      vendors: options,
    })
  }

  onChange = (value: any, type: string, index: number, fromKeyEvent: boolean = false) => {
    if (this.isKeyboardNavigate && !fromKeyEvent) return
    let items = JSON.parse(JSON.stringify(this.state.items))
    const record = this.state.items[index]
    if (record.type === 'extra') {
      // extra charge type
      if (type === 'quantity') {
        items[index].weight = value
      }
    } else if (type === 'quantity' && record.constantRatio === true) {
      items[index].weight = baseQtyToRatioQty(
        record.pricingUOM,
        ratioQtyToBaseQty(record.UOM, value, record, 12),
        record,
        12,
      )
    }
    items[index][type] = value
    if (fromKeyEvent) {
      items[index]['fromKeyEvent'] = true
    }
    this.setState({
      items,
    })
    // if (!fromKeyEvent) {
    //   this.isKeyboardNavigate = false
    // }
  }

  onDelete = (itemId: number, index: number) => {
    let items = JSON.parse(JSON.stringify(this.state.items))
    items.splice(index, 1)
    this.setState({
      items,
    })
  }

  onEnterPurchase = (status: string) => {
    const { vendorId, items, referenceNo, pickerNote, deliveryDate } = this.state
    let data = [],
      chargeItems = []
    let submitable = true
    const extraCOO = this.props.companyProductTypes ? this.props.companyProductTypes.extraCOO : []
    items.forEach((item, index) => {
      const {
        wholesaleItemId,
        quantity,
        cost,
        weight,
        brand,
        constantRatio,
        UOM,
        extraOrigin,
        pricingUOM,
        wholesaleProductUomList,
        inventoryUOM,
        SKU,
        fromKeyEvent,
        type,
      } = item
      const origin = extraCOO.find((el) => el.id == extraOrigin)
      let row = {
        wholesaleItemId: wholesaleItemId,
        quantity:
          quantity > 0
            ? baseQtyToRatioQty(item.inventoryUOM, ratioQtyToBaseQty(item.UOM, quantity, item, 12), item, 12)
            : 0,
        cost: constantRatio ? ratioPriceToBasePrice(item.pricingUOM, cost, item, 12) : cost,
        provider: brand,
        modifiers: brand,
        extraOrigin: fromKeyEvent ? extraOrigin : origin ? origin.name : '',
        UOM: UOM,
        pricingUOM,
        SKU,
      }
      let tempData = { ...row, wholesaleProductUomList: wholesaleProductUomList, inventoryUOM: inventoryUOM }

      if (!judgeConstantRatio(tempData)) {
        row.weight = ratioQtyToInventoryQty(item.pricingUOM, weight, item, 12)
      }
      if (row.quantity === '') {
        submitable = false
      }
      if (type == 'extra') {
        let chargeRow: any = {
          itemName: item.itemName,
          quantity: item.quantity,
          uom: item.UOM,
          price: item.cost,
        }
        chargeItems.push(chargeRow)
      } else {
        data.push(row)
      }
    })
    if (!submitable) {
      notification.error({
        message: 'Error',
        description: 'Please enter a quantity value',
      })
      return
    }
    const today = moment(new Date()).format('MM/DD/YYYY')
    const isPassed = moment(deliveryDate).diff(moment(today)) < 0
    this.props.onEnterPurchase(vendorId, {
      orderDate: isPassed ? deliveryDate.format('MM/DD/YYYY') : today,
      deliveryDate: deliveryDate.format('MM/DD/YYYY'),
      status,
      pickerNote,
      po: referenceNo,
      orderItemList: data,
      chargeItemList: chargeItems,
    })
    this.setState({
      items: [],
    })
  }

  onCloseModal = () => {
    this.setState({
      items: [],
      pickerNote: '',
      referenceNo: '',
    })
    this.props.onToggle()
  }

  onChangeUom = (wholesaleItemId: number, value: any, index: number, fromKeyEvent: boolean = false) => {
    const uomArrowFlag = localStorage.getItem('uom-arrow')
    if (uomArrowFlag == '1' && !fromKeyEvent) return
    const { items } = this.state
    let cloneData = cloneDeep(items)
    cloneData[index]['UOM'] = value
    if (cloneData[index].quantity > 0) {
      cloneData[index].weight = baseQtyToRatioQty(
        cloneData[index].pricingUOM,
        ratioQtyToBaseQty(value, cloneData[index].quantity, cloneData[index], 12),
        cloneData[index],
        12,
      )
    }
    console.log('check clone ', cloneData)
    // }
    this.setState({
      items: cloneDeep(cloneData),
    })
    // this.props.updatePOItemUom({
    //   id: orderItemId,
    //   UOM: value,
    //   cost: row ? ratioPriceToBasePrice(oldUom, row.cost, row, 4) : 0,
    // })
  }

  onChangePricingUom = (wholesaleItemId: number, value: any, index: number, fromKeyEvent: boolean = false) => {
    if (this.isKeyboardNavigate && !fromKeyEvent) return
    const { items } = this.state
    let cloneData = cloneDeep(items)
    cloneData[index]['pricingUOM'] = value
    if (cloneData[index].quantity > 0) {
      cloneData[index].weight = baseQtyToRatioQty(
        value,
        ratioQtyToBaseQty(cloneData[index].UOM, cloneData[index].quantity, cloneData[index], 12),
        cloneData[index],
        12,
      )
    }
    // }

    this.setState({
      items: cloneDeep(cloneData),
    })
  }

  onPageChange = (page: number) => {
    this.setState({ currentPage: page })
  }

  getCostByItemIdAndClientId = (itemId: any, cb: Function) => {
    const { vendorId } = this.state
    const _this = this
    this.changeLoadingStatus(true)
    OrderService.instance.getItemCostByItemIdAndClientId({ itemIds: itemId, clientId: vendorId }).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        cb(res.body.data)
      },
      error(err) {
        checkError(err)
      },
      complete() {
        _this.changeLoadingStatus(false)
      },
    })
  }

  changeLoadingStatus = (flag: boolean) => {
    this.setState({
      loading: flag,
    })
  }

  onChangeVendorName = () => {
    this.setState({
      vendorId: null,
    })
  }

  openAddNewBrandModal = (index: number) => {
    const { items } = this.state
    if (this.state.items.length) {
      this.setState({
        openAddNewBrandModal: !this.state.openAddNewBrandModal,
        addNewBrandItemIndex: index,
        currentSuppliers: items[index] ? items[index].supplier : [],
      })
    }
  }

  openAddNewCOOModal = (index: number) => {
    this.setState(
      {
        visibleCOOModal: !this.state.visibleCOOModal,
      },
      () => {
        if (!this.state.visibleCOOModal) {
          this.props.getCompanyProductAllTypes()
        }
      },
    )
  }

  updateDatasource = (data: string[]) => {
    const { items, addNewBrandItemIndex } = this.state
    const diff = data.filter((s: string) => !items[addNewBrandItemIndex].supplier.includes(s))
    items[addNewBrandItemIndex].supplier = data
    if (diff.length > 0) {
      items[addNewBrandItemIndex].brand = diff[0]
    }
    this.setState({
      items,
      openAddNewBrandModal: false,
    })
  }

  onChangeBrandList = (currentSuppliers: string[]) => {
    this.setState({ currentSuppliers })
  }

  render() {
    const {
      clients,
      loadingSaleItems,
      visible,
      saleItems,
      companyProductTypes,
      loadingSimplifyVendors,
      loadingPurchaseProducts,
      noText,
      hideSaveConfirm
    } = this.props
    const {
      productSKUs,
      vendorId,
      items,
      deliveryDate,
      vendors,
      currentPage,
      defaultActive,
      showItems,
      loading,
      openAddNewBrandModal,
      addNewBrandItemIndex,
      currentSuppliers,
    } = this.state
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'

    const dataSource = [
      ...addVendorProductCode(items, this.props.enterPurchaseProducts, 'wholesaleItemId'),
      ...[
        {
          wholesaleItemId: -1,
          vareity: '',
          brand: '',
          extraOrigin: '',
          quantity: '',
          cost: 0,
          inventoryUOM: '',
          baseUOM: '',
          weight: 0,
          constantRatio: false,
          ratioUOM: 1,
          supplier: [],
          SKU: '',
        },
      ],
    ]
    const selectedVendor = clients.find((el) => el.clientId == vendorId)
    const mainBillingAddress =
      selectedVendor && selectedVendor.mainBillingAddress ? selectedVendor.mainBillingAddress.address : null
    const paymentAddress = formatAddress(mainBillingAddress)
    const columns: any = [
      {
        title: '',
        dataIndex: 'index',
        width: 35,
        render: (v: string, record: any, index: number) => {
          return <div className="text-center">{(currentPage - 1) * 12 + (index + 1)}</div>
        },
      },
      {
        title: 'Item',
        dataIndex: 'variety',
        className: 'th-left',
        width: '32%',
        render: (variety: string, record: any, index: number) => {
          return (
            <Flex className="v-center">
              {record.wholesaleItemId === -1 ? (
                <AutoComplete
                  size="large"
                  style={{ width: '100%' }}
                  placeholder="Add Item..."
                  dataSource={productSKUs}
                  onSelect={this.onSelectSKU}
                  onFocus={() => this.onProductSearch('', 'SKU')}
                  onSearch={this.debounceSearch}
                  dropdownClassName="enter-po-autocomplete"
                  defaultActiveFirstOption={defaultActive}
                  disabled={!vendorId}
                />
              ) : (
                variety
              )}
            </Flex>
          )
        },
      },
      {
        title: 'Vendor Code',
        dataIndex: 'vendorProductCode',
        className: 'th-left',
        width: 150,
      },
      {
        title: 'Brand',
        dataIndex: 'brand',
        render: (brand: string, record: any, index) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return (
            <Flex className="v-center select-container-parent">
              <ThemeSelect
                className="type-brand"
                value={brand}
                placeholder="Select..."
                style={{ minWidth: 90, width: '100%' }}
                onChange={(e) => this.onChange(e, 'brand', (currentPage - 1) * 12 + index)}
                showSearch
                filterOption={(input: any, option: any) => {
                  if (option.props.children) {
                    return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  } else {
                    return true
                  }
                }}
                dropdownMatchSelectWidth={false}
                dropdownRender={(el: any) => {
                  return (
                    <div className={`enter-purchase-selector-brand-${(currentPage - 1) * 12 + index}`}>
                      {el}
                      <Divider style={{ margin: '4px 0' }} />
                      <div
                        style={{ padding: '4px 8px', cursor: 'pointer' }}
                        onMouseDown={(e) => e.preventDefault()}
                        onClick={(e) => this.openAddNewBrandModal((currentPage - 1) * 12 + index)}
                      >
                        <Icon type="plus" /> Add New
                      </div>
                    </div>
                  )
                }}
              >
                <Select.Option value="" />
                {record.supplier.map((p, i) => {
                  return (
                    <Select.Option key={`brand-${i}`} value={p}>
                      {p}
                    </Select.Option>
                  )
                })}
              </ThemeSelect>
            </Flex>
          )
        },
      },
      {
        title: 'Origin',
        dataIndex: 'extraOrigin',
        render: (extraOrigin: string, record: any, index) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return (
            <Flex className="v-center select-container-parent">
              <ThemeSelect
                className="type-extraOrigin"
                value={extraOrigin}
                style={{ minWidth: 70, width: '100%' }}
                onChange={(e) => this.onChange(e, 'extraOrigin', (currentPage - 1) * 12 + index)}
                showSearch
                filterOption={(input: any, option: any) => {
                  if (option.props.children) {
                    return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  } else {
                    return true
                  }
                }}
                dropdownMatchSelectWidth={false}
                dropdownRender={(el: any) => {
                  return (
                    <div className={`enter-purchase-selector-extraOrigin-${(currentPage - 1) * 12 + index}`}>
                      {el}
                      <Divider style={{ margin: '4px 0' }} />
                      <div
                        style={{ padding: '4px 8px', cursor: 'pointer' }}
                        onMouseDown={(e) => e.preventDefault()}
                        onClick={(e) => this.openAddNewCOOModal((currentPage - 1) * 12 + index)}
                      >
                        <Icon type="plus" /> Add New
                      </div>
                    </div>
                  )
                }}
              >
                <Select.Option value="" />
                {companyProductTypes &&
                  companyProductTypes.extraCOO.length &&
                  companyProductTypes.extraCOO.map((p, i) => {
                    return (
                      <Select.Option value={p.id} key={`${p.name}_${i}`}>
                        {p.name}
                      </Select.Option>
                    )
                  })}
              </ThemeSelect>
            </Flex>
          )
        },
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity',
        render: (quantity: number, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return (
            <Flex className="v-center">
              <ThemeInput
                className="quantity-input"
                value={quantity}
                style={{ width: 60, marginRight: 3 }}
                onChange={(e) =>
                  this.onChange(
                    e.target.value.match(/^\d*(\.?\d{0,2})/g)[0],
                    'quantity',
                    (currentPage - 1) * 12 + index,
                  )
                }
              />{' '}
              <ThemeSelect
                className="type-pricingUOM"
                disabled={record.type == 'extra' ? true : false}
                value={record.UOM}
                suffixIcon={<Icon type="caret-down" />}
                onChange={(val: any) =>
                  this.onChangeUom(record.wholesaleOrderItemId, val, (currentPage - 1) * 12 + index)
                }
                style={{ minWidth: 80, width: '100%' }}
                dropdownRender={(el: any) => {
                  return (
                    <div className={`enter-purchase-selector-pricingUOM-${(currentPage - 1) * 12 + index}`}>{el}</div>
                  )
                }}
              >
                <Select.Option value={record.inventoryUOM}>{record.inventoryUOM}</Select.Option>
                {record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                  ? record.wholesaleProductUomList.map((uom) => {
                      if (!uom.deletedAt && uom.useForPurchasing) {
                        return (
                          <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                            {uom.name}
                          </Select.Option>
                        )
                      }
                    })
                  : ''}
              </ThemeSelect>
            </Flex>
          )
        },
      },
      {
        title: 'Cost',
        dataIndex: 'cost',
        render: (cost: number, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return (
            <Flex className="v-center highlight-input">
              $
              <ThemeInput
                value={cost}
                style={{ width: 60, marginRight: 3 }}
                onChange={(e) =>
                  this.onChange(e.target.value.match(/^\d*(\.?\d{0,2})/g)[0], 'cost', (currentPage - 1) * 12 + index)
                }
              />{' '}
              /
              <div className="select-container-parent">
                <ThemeSelect
                  className="type-pricingUOM"
                  disabled={record.type == 'extra' ? true : false}
                  value={record.pricingUOM}
                  suffixIcon={<Icon type="caret-down" />}
                  onChange={(val: any) =>
                    this.onChangePricingUom(record.wholesaleOrderItemId, val, (currentPage - 1) * 12 + index)
                  }
                  style={{ minWidth: 80, width: '100%' }}
                  dropdownRender={(el: any) => {
                    return (
                      <div className={`enter-purchase-selector-pricingUOM-${(currentPage - 1) * 12 + index}`}>{el}</div>
                    )
                  }}
                >
                  {/* {record.baseUOM != record.inventoryUOM && (
                      <Select.Option value={record.baseUOM} title={record.baseUOM}>{record.baseUOM}</Select.Option>
                    )} */}
                  <Select.Option value={record.inventoryUOM}>{record.inventoryUOM}</Select.Option>
                  {record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                    ? record.wholesaleProductUomList.map((uom) => {
                        if (!uom.deletedAt && uom.useForPurchasing) {
                          return (
                            <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                              {uom.name}
                            </Select.Option>
                          )
                        }
                      })
                    : ''}
                </ThemeSelect>
              </div>
            </Flex>
          )
        },
      },
      {
        title: 'Billable Quantity',
        dataIndex: 'weight',
        className: 'th-left',
        render: (weight: number, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          let editBillable = false
          let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          let unitUOM = record.UOM

          let unitProductUOM = record.wholesaleProductUomList.filter((uom: any) => {
            return uom.name == unitUOM
          })
          let pricingProductUOM = record.wholesaleProductUomList.filter((uom: any) => {
            return uom.name == pricingUOM
          })
          if (
            (unitUOM == record.inventoryUOM || (unitProductUOM.length > 0 && unitProductUOM[0].constantRatio)) &&
            pricingProductUOM.length > 0 &&
            !pricingProductUOM[0].constantRatio
          ) {
            editBillable = true
          }
          return (
            <Flex className="v-center">
              {editBillable ? (
                <ThemeInput
                  style={{ width: 70, marginRight: 3 }}
                  value={weight}
                  onChange={(e) => this.onChange(e.target.value, 'weight', (currentPage - 1) * 12 + index)}
                />
              ) : (
                mathRoundFun(weight, 2)
              )}{' '}
              {record.pricingUOM ? record.pricingUOM : record.inventoryUOM}
            </Flex>
          )
        },
      },
      {
        title: 'Total Cost',
        dataIndex: 'total_price',
        className: 'th-right pdr-9',
        render: (_v: any, record: any) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return <div className="text-right">{`$${formatNumber(record.weight * record.cost, 2)}`}</div>
        },
      },
      {
        title: '',
        dataIndex: 'wholesaleItemId',
        render: (wholesaleItemId: number, record: any, index: number) => {
          if (record.wholesaleItemId === -1) {
            return null
          }
          return (
            <ThemeIconButton
              className="no-border remove-purchase-item"
              onClick={() => this.onDelete(wholesaleItemId, (currentPage - 1) * 12 + index)}
            >
              <Icon type="close" />
            </ThemeIconButton>
          )
        },
      },
    ]

    return (
      <ThemeModal
        title={`Enter Purchase`}
        // centered
        visible={visible}
        onCancel={this.onCloseModal}
        width={'85%'}
        className="enter-purchase-modal"
        maskClosable={false}
        footer={
          <Flex>
            <ThemeButton
              type="primary"
              onClick={this.onEnterPurchase.bind(this, 'RECEIVED')}
              disabled={vendorId === null || items.length === 0}
            >
              Save &amp; Receive
            </ThemeButton>
            {!hideSaveConfirm && (
              <ThemeButton
                type="primary"
                onClick={this.onEnterPurchase.bind(this, 'CONFIRMED')}
                disabled={vendorId === null || items.length === 0}
              >
                Save &amp; Confirm
              </ThemeButton>
            )}
            <Button type="link" className="cancel-btn" onClick={this.onCloseModal}>
              Cancel
            </Button>
          </Flex>
        }
        style={{ minWidth: 1000, maxHeight: '95vh' }}
      >
        <EnterPurchaseWrapper>
          <ThemeSpin spinning={loadingSimplifyVendors || loadingSaleItems || loadingPurchaseProducts}>
            {noText !== true && (
              <p>
                Enter the items received in the warehouse below. To start from a purchase order,{' '}
                <a href="javascript:void(0);" onClick={this.props.onStartPurchase}>
                  click here.
                </a>
              </p>
            )}
            <ThemeOutlineButton
              size="large"
              onClick={this.onRefreshData}
              style={{ position: 'absolute', right: 0, top: 0 }}
            >
              <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
              Refresh data
            </ThemeOutlineButton>

            <Flex>
              <div className="vendor select-vendor">
                <label>Vendor</label>
                <AutoComplete
                  size="large"
                  placeholder="Select a vendor"
                  dataSource={vendors}
                  style={{ marginBottom: 4 }}
                  onSelect={this.onSelectVendor}
                  onFocus={() => this.onVendorSearch('')}
                  onSearch={this.onVendorSearch}
                  onChange={(value, e) => this.onChangeVendorName(value, e)}
                  dropdownClassName="vendor-autocomplete"
                />
                {mainBillingAddress && (
                  <ValueLabel className="small black" style={{ marginBottom: 20 }}>
                    {paymentAddress}
                  </ValueLabel>
                )}
              </div>
              <div className="vendor" style={{ marginLeft: 20 }}>
                <label>Delivery Date</label>
                <DatePicker
                  className="datepicker-container"
                  allowClear={false}
                  onChange={this.onChangeDeliveryDate}
                  value={deliveryDate}
                  size="large"
                  placeholder="MM/DD/YYYY"
                  format="MM/DD/YYYY"
                  suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                />
              </div>
              <div className="vendor" style={{ marginLeft: 20 }}>
                <label>Vendor Reference No.</label>
                <ThemeInput
                  maxLength={25}
                  onChange={this.onOrderInfoTextFieldChange.bind(this, 'referenceNo')}
                  value={this.state.referenceNo}
                  className="reference-no"
                />
              </div>
              <div className="vendor" style={{ marginLeft: 20 }}>
                <label>Internal note.</label>
                <ThemeInput
                  maxLength={25}
                  onChange={this.onOrderInfoTextFieldChange.bind(this, 'pickerNote')}
                  value={this.state.pickerNote}
                  className="reference-no"
                />
              </div>
            </Flex>
            {showItems && (
              <ThemeSpin spinning={loading}>
                <ThemeTable
                  dataSource={dataSource}
                  columns={columns}
                  pagination={{
                    hideOnSinglePage: true,
                    pageSize: 12,
                    current: this.state.currentPage,
                    onChange: this.onPageChange,
                  }}
                />
              </ThemeSpin>
            )}
          </ThemeSpin>
        </EnterPurchaseWrapper>
        {addNewBrandItemIndex > -1 && (
          <AddNewBrandModal
            visible={openAddNewBrandModal}
            itemName={items[addNewBrandItemIndex].variety}
            itemId={items[addNewBrandItemIndex].wholesaleItemId}
            oldBrands={items[addNewBrandItemIndex].supplier}
            // currentSuppliers={currentSuppliers}
            companyProductTypes={companyProductTypes}
            onVisibleChange={this.openAddNewBrandModal}
            onChangeBrandList={this.onChangeBrandList}
            updateDatasource={this.updateDatasource}
            updateProduct={this.props.updateProduct}
            setCompanyProductTypes={this.props.setCompanyProductType}
          />
        )}
        <ThemeModal
          title={`Edit Value List "Origin"`}
          visible={this.state.visibleCOOModal}
          onCancel={this.openAddNewCOOModal}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="extraCOO" title="Origin" buttonTitle="Add Origin" />
        </ThemeModal>
      </ThemeModal>
    )
  }
}
