import React, { FC, useState } from 'react'
import { useHistory } from 'react-router-dom'
import styled from '@emotion/styled'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { withTheme } from 'emotion-theming'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { ColumnProps } from 'antd/es/table'
import { OrderItem, Order, AuthUser, UserRole } from '~/schema'
import { Table, Icon, Tooltip } from 'antd'
import { Link } from 'react-router-dom'
import { ThemeIcon, Flex } from '~/modules/customers/customers.style'
import {
  ratioQtyToBaseQty,
  baseQtyToRatioQty,
  inventoryPriceToRatioPrice,
  inventoryQtyToRatioQty,
  judgeConstantRatio,
  formatNumber
} from '~/common/utils'
import ItemHistoryModal from '../../vendors/purchase-orders/overview/item-history-modal'

import { cloneDeep, isEmpty } from 'lodash'
import { gray01 } from "~/common";

type IProps = OrdersDispatchProps &
  OrdersStateProps & {
    record: Order
    currentUser: AuthUser
    expandAll: boolean
  }

const MinProductTable: FC<IProps> = ({
  record,
  getPurchaseOrderItemHistory,
  purchaseOrderItemHistory,
  sellerSetting,
  currentUser,
  expandAll,
}) => {
  const [selectedItem, setSelectedItem] = useState<OrderItem | null>(null)
  const [showModal, setShowModal] = useState(false)
  let history = useHistory()
  const { orderItems } = record
  const option = !expandAll
    ? {
      loading: !record.fetched,
    }
    : {}

  const columns: ColumnProps<OrderItem>[] = [
    {
      title: <span style={{ paddingLeft: '42px' }}>LOT #</span>,
      dataIndex: 'lotId',
      key: 'lotId',
      render: (_, record) => {
        if (!record.wholesaleItem) {
          return '-'
        }
        return (
          <div style={{ paddingLeft: '42px' }}>
            <Link to={`/product/${record.wholesaleItem.wholesaleItemId}/lots`}>{record.lotId}</Link>
          </div>
        )
      },
    },
    {
      title: 'DESCRIPTION',
      render: (_, record) => {
        if (!record.wholesaleItem) {
          if (record.wholesaleClientId) {
            return record.chargeDesc
          } else {
            return record.itemName
          }
        }
        return (
          <Link to={`/product/${record.wholesaleItem.wholesaleItemId}/activity`}>{record.wholesaleItem.variety}</Link>
        )
      },
    },
    {
      title: 'BRAND',
      dataIndex: 'modifiers',
    },
    {
      title: 'UNITS ORDERED',
      key: 'quantity',
      dataIndex: 'quantity',
      align: 'center',
      render: (quantity: number, record: any) => {
        if (!record.wholesaleItem) {
          return `${record.quantity} ${record.UOM}`
        }
        const unitUOM = record.overrideUOM ? record.overrideUOM : record.UOM
        const units = inventoryQtyToRatioQty(unitUOM, quantity, record.wholesaleItem)
        return `${units} ${unitUOM}`
      },
    },
    {
      title: 'COST',
      dataIndex: 'cost',
      width: 160,
      render: (cost: number, record) => {
        if (!record.wholesaleItem) {
          return `${record.quantity} ${record.UOM}`
        }
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        // const formatString = record.pas
        //   ? 'PAS'
        //   : `$${inventoryPriceToRatioPrice(pricingUOM, cost, record.wholesaleItem).toFixed(2)}/${pricingUOM}`
        const formatString = `$${inventoryPriceToRatioPrice(pricingUOM, cost, record.wholesaleItem).toFixed(
          2,
        )}/${pricingUOM}`
        return (
          <Flex className="space-between">
            {formatString}
            <Tooltip title="View Cost History">
              <ThemeIcon onClick={() => openModal(record)} type="bar-chart" style={{ fontSize: 20, marginLeft: 8 }} />
            </Tooltip>
          </Flex>
        )
      },
    },
    {
      title: 'PAS',
      dataIndex: 'pas',
      render: (_, record) => record?.pas && <Icon type="check" />,
    },
    {
      title: 'UNITS CONFIRMED',
      render: (value, record) => {
        if (isEmpty(record.wholesaleItem)) {
          return `${record.quantity} ${record.overrideUOM || record.UOM}`
        }

        const unitUOM = record.overrideUOM ? record.overrideUOM : record.UOM
        return (
          <>
            {record && inventoryQtyToRatioQty(unitUOM, record.qtyConfirmed, record.wholesaleItem)} {unitUOM}
          </>
        )
      },
    },
    {
      title: 'UNITS RECEIVED',
      render: (_, record) => {
        if (isEmpty(record.wholesaleItem)) {
          return `${record.quantity} ${record.overrideUOM || record.UOM}`
        }

        if (record.status == 'CONFIRMED' && record.receivedQty === 0) {
          return ''
        }

        const unitUOM = record.overrideUOM ? record.overrideUOM : record.UOM
        return (
          <>
            {record && inventoryQtyToRatioQty(unitUOM, record.receivedQty, record.wholesaleItem)} {unitUOM}
            {record && formatUnit(record, 'quantity') !== formatUnit(record, 'receivedQty') && (
              <Tooltip placement="right" title="Receive discrepancy. Click for details">
                <Icon
                  onClick={() => {
                    history.push(`/product/${record.wholesaleItem.wholesaleItemId}/lots`)
                  }}
                  style={{ color: '#8D0C04', marginLeft: '8px' }}
                  type="warning"
                />
              </Tooltip>
            )}
          </>
        )
      },
    },
    {
      title: 'TEMPERATURE',
      render: (_, record) => record?.wholesaleItem?.temperature ?? record.temperature,
    },
    {
      title: 'SUBTOTAL',
      dataIndex: 'subTotal',
      render: (_, record) => {
        const constantRatio = judgeConstantRatio({
          ...record,
          inventoryUOM: typeof record.wholesaleItem !== 'undefined' ? record.wholesaleItem.inventoryUOM : null,
          wholesaleProductUomList: typeof record.wholesaleItem !== 'undefined' ? record.wholesaleItem.wholesaleProductUomList : undefined
        })
        if (!constantRatio) {
          if (record.status == 'RECEIVED') return (
            <Tooltip placement="bottom" title={'Approximate values used for catchweight not yet received, based on approximate ratio'}>
              <span>${formatNumber(Math.round(record.cost * record.orderWeight * 100) / 100, 2)}*</span>
            </Tooltip>
          )
        } else {
          if (record.status == 'RECEIVED') return (
             <span>${formatNumber(Math.round(record.cost * record.receivedQty * 100) / 100, 2)}</span>
          )
        }
      },
    },
    {
      title: 'NOTES',
      dataIndex: 'note',
      width: 80,
      render(_, record) {
        return (
          record.note && (
            <Tooltip title={record.note}>
              <ThemeIcon type="file-text" />
            </Tooltip>
          )
        )
      },
    },
  ]

  function openModal(orderItem: OrderItem) {
    const selectItem = {
      ...orderItem,
      wholesaleProductUomList: orderItem.wholesaleItem ? orderItem.wholesaleItem.wholesaleProductUomList : [],
      SKU: orderItem.wholesaleItem ? orderItem.wholesaleItem.sku : '',
      variety: orderItem.wholesaleItem ? orderItem.wholesaleItem.variety : '',
    }
    setSelectedItem(selectItem)
    setShowModal(true)
  }

  function closeModal() {
    setSelectedItem(null)
    setShowModal(false)
  }

  function getFormattedColumns() {
    if (currentUser.accountType == UserRole.WAREHOUSE) {
      const result = cloneDeep(columns)
      const costIndex = result.findIndex((el) => el.dataIndex == 'cost')
      result.splice(costIndex, 1)
      const subTotalIndex = result.findIndex((el) => el.dataIndex == 'subTotal')
      result.splice(subTotalIndex, 1)
      return result
    }
    return columns
  }

  return (
    <>
      <ThemeTable
        columns={getFormattedColumns()}
        dataSource={orderItems}
        pagination={false}
        rowKey="wholesaleOrderItemId"
        {...option}
      />
      <ItemHistoryModal
        selectedItem={selectedItem}
        showModal={showModal}
        onClose={closeModal}
        getPurchaseOrderItemHistory={getPurchaseOrderItemHistory}
        itemHistory={purchaseOrderItemHistory}
        sellerSetting={sellerSetting}
      />
    </>
  )
}

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.orders,
    currentUser: state.currentUser,
  }
}

export default withTheme(connect(OrdersModule)(mapStateToProps)(MinProductTable))

type unitType = 'receivedQty' | 'quantity' | 'qtyConfirmed'

function formatUOM(item: OrderItem, unit: unitType): number {
  if (!item) {
    return 0
  }
  return baseQtyToRatioQty(
    item.overrideUOM ?? item.inventoryUOM,
    ratioQtyToBaseQty(item.inventoryUOM, item[unit], item, 4),
    item,
  )
}

function formatUnit(item: OrderItem, unit: unitType): string {
  const start = unit === 'qtyConfirmed' ? item.qtyConfirmed : formatUOM(item, unit)
  const end = item.overrideUOM ?? item?.wholesaleItem?.inventoryUOM ?? ''
  return `${start.toFixed(2)}  ${end}`
}

const ThemeTable = styled(Table)((props) => ({
  '&.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-thead .ant-table-column-title': {
    color: gray01, //color: '#555F61',
    fontWeight: 400,
  },
  '&.ant-table-wrapper .ant-table-content .ant-table-body tbody > .ant-table-row > td': {
    color: gray01,  //color: '#555F61',
    fontWeight: 400,
  },
}))
