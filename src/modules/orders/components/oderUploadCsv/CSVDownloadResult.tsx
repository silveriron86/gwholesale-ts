import React from 'react'
// @ts-ignore
import CSV from 'comma-separated-values'
import { Button } from 'antd'

interface IProps {
  arr: string[][]
  headerArr: string[]
  fileName: string
  resultHeight: number
  isNew?: boolean
}

const CSVDownloadResult = ({ arr, headerArr, fileName, resultHeight, isNew }: IProps): JSX.Element => {
  const downloadCSVFn = () => {
    const data = new CSV(arr, {
      header: headerArr,
    }).encode()
    downloadCsv(fileName, data)
  }

  const has = (browser: string) => {
    const ua = navigator.userAgent
    if (browser === 'ie') {
      const isIE = ua.indexOf('compatible') > -1 && ua.indexOf('MSIE') > -1
      if (isIE) {
        const reIE = new RegExp('MSIE (\\d+\\.\\d+);')
        reIE.test(ua)
        return parseFloat(RegExp['$1'])
      } else {
        return false
      }
    } else {
      return ua.indexOf(browser) > -1
    }
  }

  const _getDownloadUrl = (text: any) => {
    const BOM = '\uFEFF'
    // Add BOM to text for open in excel correctly
    if (window.Blob && window.URL && (window as any).URL.createObjectURL) {
      const csvData = new Blob([BOM + text], { type: 'text/csv' })
      return URL.createObjectURL(csvData)
    } else {
      return 'data:attachment/csv;charset=utf-8,' + BOM + encodeURIComponent(text)
    }
  }

  const downloadCsv = (filename: string, text: any) => {
    if (has('ie') && has('ie') < 10) {
      // has module unable identify ie11 and Edge
      const oWin = (window as any).top.open('about:blank', '_blank')
      oWin.document.charset = 'utf-8'
      oWin.document.write(text)
      oWin.document.close()
      oWin.document.execCommand('SaveAs', filename)
      oWin.close()
    } else {
      const link = document.createElement('a')
      link.download = filename
      link.href = _getDownloadUrl(text)
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)
    }
  }

  return (
    <>
      <Button
        style={{
          borderRadius: '40px',
          top: `${resultHeight + (isNew === true ? 175 : 220)}px`,
          width: '166px',
          backgroundColor: 'white',
          color: '#1C6E31',
          position: 'absolute',
          border: 'white 1px solid',
          right: '250px',
        }}
        onClick={() => {
          downloadCSVFn()
        }}
      >
        <img
          style={{
            width: '20px',
            height: '20px',
            marginRight: '10px',
            marginLeft: '10px',
            marginTop: '-5px',
          }}
          src={require('./downloadP.png')}
        />
        Export results as CSV
      </Button>
    </>
  )
}

export default CSVDownloadResult
