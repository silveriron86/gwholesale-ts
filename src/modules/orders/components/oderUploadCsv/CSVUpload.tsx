import { Spin } from 'antd'
import React, { useState } from 'react'
import XLSX from 'xlsx'
import './orderUpload.css'
import moment from 'moment'

interface IProps {
  onWrongFormat: () => void
  setExcelOutput: (excelOutPut: unknown[]) => void
  setHeaders: (val: string[]) => void
  upLoadSuccess: () => void
  fileUploadMode: string
}

const CSVUpload = ({fileUploadMode, onWrongFormat, setExcelOutput, setHeaders, upLoadSuccess }: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false)

  const readExcel = async (e: any) => {
    setLoading(true)
    const files = e.target.files
    const fileType = files[0].name.toLowerCase()
    if (files.length <= 0) {
      setLoading(false)
      return false
    } else if (!/\.(xls|xlsx|csv)$/.test(fileType)) {
      onWrongFormat()
      setLoading(false)
      return false
    }

    const fileReader = new FileReader()
    fileReader.onload = (ev: any) => {
      try {
        // get xlsx file
        const data = ev.target.result
        const workbook = XLSX.read(data, {
          type: 'binary',
          cellText: false,
          cellDates: true,
          raw: true,
        })
        const wsname = workbook.SheetNames[0] //取第一张表
        const ws = XLSX.utils.sheet_to_json(workbook.Sheets[wsname],{raw: false,dateNF: 'yyyy-mm-dd',rawNumbers:true})

        if (fileUploadMode ==='PO')
        {
          for(let i = 0; i < ws.length; i++) {
            ws[i]['Delivery Date'] = moment(ws[i]['Delivery Date']).format("YYYY-MM-DD")
          }
        }

        const worksheet = workbook.Sheets[wsname]
        const header = get_header_row(worksheet)
        setHeaders(header)
        setExcelOutput(ws)
        upLoadSuccess()
        setLoading(false)
      } catch (e) {
        setLoading(false)
        return false
      }
    }
    fileReader.readAsBinaryString(files[0])

    //Using the XLSX(sheetJS) for xlsx and CSV files
    /*
    if (!/\.(csv)$/.test(fileType)) {
      const fileReader = new FileReader()
      fileReader.onload = (ev: any) => {
        try {
          // get xlsx file
          const data = ev.target.result
          const workbook = XLSX.read(data, {
            type: 'binary',
          })
          const wsname = workbook.SheetNames[0] //取第一张表
          const ws = XLSX.utils.sheet_to_json(workbook.Sheets[wsname])
          const worksheet = workbook.Sheets[wsname]
          const header = get_header_row(worksheet)
          setHeaders(header)
          setExcelOutput(ws)
          upLoadSuccess()
          setLoading(false)
        } catch (e) {
          setLoading(false)
          return false
        }
      }
      fileReader.readAsBinaryString(files[0])
    } else {
      const fileReader = new FileReader()
      fileReader.onload = (ev: any) => {
        try {
          // get csv file
          const textFiles = readCell(ev.target.result)
          const header = textFiles[0].map((item: any) => item.replace(/ï»¿/g, '').replace(/\r/g, ''))
          const ws: any[] = []
          textFiles.forEach((item: any, index: number) => {
            if (index != 0) {
              const wsItem = {}
              header.forEach((headerString: string, index: number) => {
                wsItem[headerString] = item[index].replace(/\r/g, '')
              })
              ws.push(wsItem)
            }
          })
          setHeaders(header)
          setExcelOutput(ws)
          upLoadSuccess()
          setLoading(false)
        } catch (e) {
          setLoading(false)
          return false
        }
      }
      setLoading(false)
      fileReader.readAsBinaryString(files[0])
    }
    */
  }

  const get_header_row = (sheet: XLSX.WorkSheet) => {
    const headers = []
    const range = XLSX.utils.decode_range(sheet['!ref'] as string)
    let C
    const R = range.s.r
    for (C = range.s.c; C <= range.e.c; ++C) {
      var cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })]
      var hdr = 'UNKNOWN ' + C
      if (cell && cell.t) hdr = XLSX.utils.format_cell(cell)
      headers.push(hdr)
    }
    return headers
  }

  //const readCell = (ex: any) => {
  //  return ex
  //    .trim()
  //    .split(/\n/gm)
  //    .map((v: any) => {
  //      var str = v.match(/"[^"]+"/gi)
  //      if (str) {
  //        var result = str.map((vv: any) => {
  //          console.log(vv.replace(/,/g, '<|>'))
  //          return vv.replace(/,/g, '<|>')
  //        })
  //        str.forEach((vv: any, i: any) => {
  //          console.log(vv)
  //          v = v.replace(vv, result[i])
  //        })
  //      }
  //      v = v.split(/\s*,\s*/g).map((v: any) => {
  //        return v
  //          .replace(/"/g, '')
  //          .replace(/<\|>/g, ',')
  //          .replace(/<\\r>/g, ',')
  //      })
  //      return v
  //    })
  //}

  return (
    <>
      <input
        type="file"
        onClick={(e) => {
          ;(e.target as any).value = ''
        }}
        onChange={(e) => readExcel(e)}
        accept=".xls,.xlsx,.csv"
      />
      {loading ? <Spin /> : <></>}
    </>
  )
}

export default CSVUpload
