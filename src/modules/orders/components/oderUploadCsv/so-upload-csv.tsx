/* eslint-disable react/jsx-key */
/* eslint-disable no-case-declarations */
import { Alert, Button, Modal, notification, Select, Spin } from 'antd'
import { SelectValue } from 'antd/lib/select'
import React, { useEffect, useState } from 'react'
import './orderUpload.css'
// @ts-ignore
import CSV from 'comma-separated-values'
import CSVDownload from './CSVDownload'
import CSVUpload from './CSVUpload'
import { OrderService } from '~/modules/orders/order.service'
import { Flex, flexStyle, ThemeButton, ThemeCheckbox, ThemeInput, ThemeSpin } from '~/modules/customers/customers.style'
import { useHistory } from 'react-router'
import { NavLink } from 'react-router-dom'
import CSVDownloadResult from './CSVDownloadResult'
import {cloneDeep, isNumber} from 'lodash'
import UploadValidation from './upload-validation'
import UploadPreview from './upload-preview'
import moment from 'moment'

const templateArrSales = [
  ['36778', 'APP100', '2', '10.00', 'CASE'],
  ['46998', 'BAN100', '5', '15.00', 'BAG'],
  ['1558E564', 'CAR100', '10', '20.00', 'LB'],
]
const templateHeadersSales = ['Customer Item Code', 'Product SKU', 'OrderQty', 'Price', 'Unit of measure']

const templateArrPO = [
  ['12/24/2027', 'Gregory Farms', 'APP100', '2', 'CASE', '10.00'],
  ['12/25/2027', 'Sunny Side Orchard', 'BAN100', '5', 'BAG', '15.00'],
  ['12/25/2027', 'Highlands Produce', 'CAR100', '10', 'LB', '20.00'],
]
const templateHeadersPO = ['Delivery Date', 'Vendor Name', 'Product SKU', 'Order Qty', 'Unit of measure', 'Cost']

const templateArrItemUpdate = [
  ['5202', '16', '32', '1001', 'Y', 'Frozen', '123456789012', '6.22', '12.99'],
  ['5204', '16', '32', '', 'Y', 'Frozen', '', '11.99', '17.99'],
  ['6199', '12', '24', '1004', '', '', '123544512313', '11.99', '17.99'],
]

const templateHeadersItemUpdate = [
  'Product SKU',
  'Par Level',
  'Reorder Quantity',
  'Location',
  'Active',
  'Temperature',
  'UPC',
  'Default Cost',
  'Default Price',
]

let templateArr: string[][]
let templateHeaders: string[]
let templateFileName: string

let fileUploadMode: string

let maxRecords: number
let requiredColumnCount: number

interface Customer {
  lientCompanyId: number
  clientCompanyName: string
  clientId: number
  wholesaleCompanyId: number
}

interface ErrorType {
  sku: string
  customerProductCode: string
  result: string
}
interface ItemErrorType {
  sku: string
  parLevel: string
  location: string
  active: string
  temperature: string
  UPC: string
  defaultPrice: string
  defaultCost: string
  wholesaleItemID: string
  result: string
}

interface OrderType {
  vendorName: string
  deliveryDate: string
  orderNumber: string
}

interface POHeaderErrorType {
  vendorName: string
  deliveryDate: string
  result: string
}

interface PODetailErrorType {
  vendorName: string
  deliveryDate: string
  sku: string
  result: string
}

interface IProps {
  visible: boolean
  onCancel: () => void
  propData: any
  uploadMode: string
}

const SalesOrderUploadCsv = ({ visible, onCancel, propData, uploadMode }: IProps): JSX.Element => {
  const history = useHistory()

  const [stage, setStage] = useState<number>(0)
  const [customers, setCustomers] = useState<Customer[]>([])
  const [customerId, setCustomerId] = useState<string>('')
  const [excelOutput, setExcelOutput] = useState<unknown[]>([])
  const [headers, setHeaders] = useState<string[]>([])
  const [selectHeaders, setSelectHeaders] = useState<string[]>([])
  const [importState, setImportState] = useState<boolean>(false)
  const [loading, setLoading] = useState<boolean>(false)
  const [checkedSplit, setCheckedSplit] = useState<boolean>(false)
  const [successNum, setSuccessNum] = useState<number>(0)
  const [createdIds, setCreatedIds] = useState<string[]>([])
  const [errorArr, setErrorArr] = useState<ErrorType[]>([])
  const [errorArrPOHeader, setErrorArrPOHeader] = useState<POHeaderErrorType[]>([])
  const [errorArrPODetail, setErrorArrPODetail] = useState<PODetailErrorType[]>([])
  const [orderList, setOrderList] = useState<OrderType[]>([])
  const [errorArrItem, setErrorArrItem] = useState<ItemErrorType[]>([])
  const [maxWeight, setMaxWeight] = useState<number>()
  const [maxVolume, setMaxVolume] = useState<number>()
  const [checkedCommodity, setCheckedCommodity] = useState<boolean>(false)
  const [validatedResult, setValidateResult] = useState<any>({ items: [], errors: [] })

  const [alert, setAlert] = useState<{
    alertState: boolean
    text: string
  }>({
    alertState: false,
    text: '',
  })
  const [nextButton, setNextButton] = useState<{
    uploadCSV: boolean
    selectCustomer: boolean
  }>({
    uploadCSV: false,
    selectCustomer: false,
  })
  const [resultHeight, setResultHeight] = useState<number>(200)

  fileUploadMode = uploadMode == '' ? 'PO' : uploadMode

  if (fileUploadMode == 'Sales') {
    templateArr = Array.from(templateArrSales)
    templateHeaders = Array.from(templateHeadersSales)
    maxRecords = 500
    requiredColumnCount = 5
    templateFileName = 'sales_order_template.csv'
  } else if (fileUploadMode == 'PO') {
    templateArr = Array.from(templateArrPO)
    templateHeaders = Array.from(templateHeadersPO)
    maxRecords = 2000
    requiredColumnCount = 6
    templateFileName = 'purchase_order_template.csv'
  } else if (fileUploadMode == 'ItemUpdate') {
    templateArr = Array.from(templateArrItemUpdate)
    templateHeaders = Array.from(templateHeadersItemUpdate)
    maxRecords = 200
    requiredColumnCount = 9
    templateFileName = 'item_update_template.csv'
  }

  const { Option } = Select

  useEffect(() => {
    setCustomers(propData.simplifyCustomers)
  }, [propData.simplifyCustomers])

  useEffect(() => {
    //console.log(excelOutput)
    //console.log(headers)
    //console.log(selectHeaders)
  }, [nextButton, stage, importState, selectHeaders, resultHeight])

  const titleArr = ['UPLOAD', 'MAP DATA', 'VALIDATION', 'PREVIEW', 'RESULT']

  const onChange = (value: SelectValue) => {
    setAlert({
      alertState: false,
      text: '',
    })
    setCustomerId(value as string)
    setNextButton({
      uploadCSV: nextButton.uploadCSV,
      selectCustomer: true,
    })
  }

  const onBack = () => {
    setStage(0)
    setExcelOutput([])
    setHeaders([])
    setNextButton({ uploadCSV: false, selectCustomer: false })
    setAlert({
      alertState: false,
      text: '',
    })
    onCancel()
  }

  //stage index and background color
  const getStage = () =>
    titleArr.map((title: string, index: number) => {
      if (index == stage) {
        return (
          <div key={index} className="stageHeader">
            <p className="herderText showCircle showHeaderIndex">{index + 1}</p>
            <p className="herderText showHeaderText">{title}</p>
          </div>
        )
      } else {
        return (
          <div key={index} className="stageHeader">
            <div>
              <p className="herderText hideCircle hideHeaderIndex">{index + 1}</p>
            </div>
            <p className="herderText hideHeaderIndex">{title}</p>
          </div>
        )
      }
    })

  // stage 0 functions
  // select customer
  const getOPtions = () =>
    customers.map((customer: Customer, index: number) => {
      return (
        <Option key={index} value={customer.clientId}>
          {customer.clientCompanyName}
        </Option>
      )
    })

  //select header for upload csv
  const getImportOptions = () =>
    headers.map((header: string, index: number) => {
      return (
        <Option key={index} style={{ fontWeight: 'normal' }}>
          {header}
        </Option>
      )
    })

  //show alert when user upload wrong type file
  const uploadWrong = () => {
    setAlert({
      alertState: true,
      text: 'Invalid file type, please select a CSV file',
    })
  }
  // to stage 2 page and save csv columns
  const toNext = () => {
    if (checkCSVfile()) {
      setSelectHeaders(headers)
      setStage(1)
    }
  }
  //basic checks for uploaded file
  const checkCSVfile = (): boolean => {
    if (excelOutput.length >= maxRecords) {
      setAlert({
        alertState: true,
        text: 'Uploads are limited to ' + maxRecords.toString() + ' rows!',
      })
      return false
    } else if (headers.length < requiredColumnCount) {
      setAlert({
        alertState: true,
        text:
          'Import must have ' +
          requiredColumnCount.toString() +
          ' columns, please ensure the file has all of the required columns!',
      })
      return false
    } else {
      setAlert({
        alertState: false,
        text: '',
      })
      return true
    }
  }
  //stage 0 functions end

  //stage 1 functions
  //those two functions used to set correspondence between csv column and WSW columns

  const onSelectChange = (e: SelectValue, index: number) => {
    const newSelectHeader = selectHeaders.map((item) => item)
    newSelectHeader[index] = headers[parseInt(e?.toString() as string)]
    setSelectHeaders(newSelectHeader)
  }
  const getImportTable = (index: number, arrLen: number) => {
    let maxIndex = 0
    arrLen == 0 ? (maxIndex = 0) : (maxIndex = arrLen - 1)
    return (
      <div key={index} className="importTableLine">
        <p
          className="importPLeft "
          style={{
            lineHeight: '40px',
            borderTop: '1px solid black',
            borderLeft: '1px solid black',
            borderBottom: index == maxIndex ? '1px solid black' : '',
            marginBottom: '0px',
            width: '250px',
          }}
        >
          <p
            style={{
              paddingLeft: '5px',
              fontWeight: 'normal',
            }}
          >
            {templateHeaders[index] == 'OrderQty'
              ? 'Order Quantity'
              : templateHeaders[index] == 'Unit of measure'
                ? 'Unit of Measure'
                : templateHeaders[index]}
          </p>
        </p>
        <Select
          style={{
            lineHeight: '40px',
            borderTop: '1px solid black',
            borderLeft: '1px solid black',
            borderBottom: index == maxIndex ? '1px solid black' : '',
            marginBottom: '0px',
            paddingTop: '5px',
            width: '230px',
            paddingLeft: '10px',
            paddingRight: '10px',
            fontWeight: 'normal',
          }}
          showSearch
          placeholder=""
          defaultValue={headers[index]}
          optionFilterProp="children"
          onSelect={(e: SelectValue) => onSelectChange(e, index)}
        >
          <Option key={'none'} style={{ fontWeight: 'normal', height: '20px' }}>
            {''}
          </Option>
          {getImportOptions()}
        </Select>
        <p
          className="importPRight"
          style={{
            lineHeight: '40px',
            width: '300px',
            borderTop: '1px solid black',
            borderLeft: '1px solid black',
            borderRight: '1px solid black',
            borderBottom: index == maxIndex ? '1px solid black' : '',
          }}
        >
          <p
            style={{
              paddingLeft: '5px',
              fontWeight: 'normal',
            }}
          >
            <div
              style={{
                textOverflow: 'ellipsis',
                inlineSize: '290px',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
              }}
            >
              {' '}
              {(excelOutput as any)[0][selectHeaders[index]]}
            </div>
          </p>
        </p>
      </div>
    )
  }

  // format data for backend call - will get stringified later
  const getImportFile = (saveResults: boolean) => {
    const date = new Date()
    const time = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`
    const itemList = excelOutput.map((item: any) => {
      switch (fileUploadMode) {
        case 'Sales':
          return {
            customerProductCode: item[selectHeaders[0]] ? item[selectHeaders[0]].toString() : '',
            SKU: item[selectHeaders[1]] ? item[selectHeaders[1]].toString() : '',
            UOM: item[selectHeaders[4]] ? item[selectHeaders[4]].toString() : '',
            quantity: item[selectHeaders[2]] ? item[selectHeaders[2]].toString() : '',
            price: item[selectHeaders[3]] ? item[selectHeaders[3]].toString() : '',
          }
        case 'PO':
          return {
            deliveryDate: item[selectHeaders[0]] ? item[selectHeaders[0]].toString() : '',
            vendorName: item[selectHeaders[1]] ? item[selectHeaders[1]].toString() : '',
            SKU: item[selectHeaders[2]] ? item[selectHeaders[2]].toString() : '',
            UOM: item[selectHeaders[4]] ? item[selectHeaders[4]].toString() : '',
            quantity: item[selectHeaders[3]] ? item[selectHeaders[3]].toString() : '',
            cost: item[selectHeaders[5]] ? item[selectHeaders[5]].toString() : '',
          }
        case 'ItemUpdate':
          return {
            SKU: item[selectHeaders[0]] ? item[selectHeaders[0]].toString() : '',
            parLevel: item[selectHeaders[1]] ? item[selectHeaders[1]].toString() : '',
            reorderQuantity: item[selectHeaders[2]] ? item[selectHeaders[2]].toString() : '',
            location: item[selectHeaders[3]] ? item[selectHeaders[3]].toString() : '',
            active: item[selectHeaders[4]] ? item[selectHeaders[4]].toString() : '',
            temperature: item[selectHeaders[5]] ? item[selectHeaders[5]].toString() : '',
            UPC: item[selectHeaders[6]] ? item[selectHeaders[6]].toString() : '',
            defaultCost: item[selectHeaders[7]] ? item[selectHeaders[7]].toString() : '',
            defaultPrice: item[selectHeaders[8]] ? item[selectHeaders[8]].toString() : '',
          }
      }
    })
    switch (fileUploadMode) {
      case 'Sales':
        return {
          validateOnly: !saveResults,
          deliveryDate: time,
          itemList: itemList,
        }
      case 'PO':
      case 'ItemUpdate':
        return {
          validateOnly: !saveResults,
          rows: itemList,
        }
    }
  }

  //if import success save success values and show stage2 success component
  const importSuccess = (result: any) => {
    switch (fileUploadMode) {
      case 'Sales':
        setSuccessNum(result.wholesaleOrderId)
        break
      case 'PO':
        const tempOrderList: OrderType[] = []
        result.orderList.forEach((orderElem: any) => {
          const resultObj = {
            vendorName: orderElem.vendorName,
            deliveryDate: orderElem.deliveryDate,
            orderNumber: orderElem.wholesaleOrderId,
          }
          tempOrderList.push(resultObj)
        })
        setOrderList(tempOrderList)
        setSuccessNum(tempOrderList.length)
        break
      case 'FileUpdate':
        setSuccessNum(result.itemList.length)
        break
    }
    setImportState(true)
  }

  //if import fail save wrong data and show stage2 fail component
  const importFail = (result: any) => {
    let errorString = ''
    switch (fileUploadMode) {
      case 'Sales':
        const errorArr: ErrorType[] = []
        getImportFile(false).itemList.forEach((item: any) => {
          const resultObj = {
            sku: item.SKU,
            customerProductCode: item.customerProductCode,
            result: 'Successfully imported',
          }
          errorArr.push(resultObj)
        })

        result.forEach((error: any, index: any) => {
          //errorNumObj[error] = errorNumObj[error] + 1
          if (error.SKU || error.customerProductCode) {
            errorString = ''
            error.errorTypes.forEach((item: any) => {
              errorString = errorString + `${item} invalid,`
            })
            errorArr[error.index].result = errorString
          } else {
            if (isNumber(error.index)) {
              errorArr[error.index].result = error.errorTypes.join(',')
            }
          }
        })
        setErrorArr(errorArr)
        setImportState(false)
        break
      case 'PO':
        const errorArrDetail: PODetailErrorType[] = []
        const errorArrHeader: POHeaderErrorType[] = []
        getImportFile(false).rows.forEach((item: any) => {
          const index = errorArrHeader.findIndex(
            (i: any) => i.vendorName === item.vendorName && i.deliveryDate === item.deliveryDate,
          )
          if (index === -1) {
            const resultObj = {
              vendorName: item.vendorName,
              deliveryDate: item.deliveryDate,
              result: 'Successfully imported',
            }
            errorArrHeader.push(resultObj)
          }
          const resultObjDetail = {
            vendorName: item.vendorName,
            deliveryDate: item.deliveryDate,
            sku: item.SKU,
            quantity: item.quantity,
            UOM: item.UOM,
            cost: item.cost,
            result: 'Successfully imported',
          }
          errorArrDetail.push(resultObjDetail)
        })

        result.errorList.forEach((error: any, index: any) => {
          error.orderErrors.forEach((orderError: any) => {
            errorArrDetail.forEach((item: any) => {
              if (
                ((item.vendorName === error.vendorName && orderError.field === 'vendorName') ||
                  (item.deliveryDate === error.deliveryDate && orderError.field === 'deliveryDate')) &&
                item.result.indexOf(orderError.msg) === -1
              ) {
                item.result = item.result === 'Successfully imported' ? '' : item.result + ' '
                item.result += orderError.msg + ','
              }
            })
          })

          error.orderItemErrors.forEach((itemError: any) => {
            errorArrDetail.forEach((item: any) => {
              errorString = ''
              if (
                item.vendorName === error.vendorName &&
                item.deliveryDate === error.deliveryDate &&
                item.sku === itemError.SKU
              ) {
                itemError.orderItemFieldErrors.forEach((itemDetailError: any) => {
                  if (
                    (itemDetailError.field === 'SKU' && item.sku === itemDetailError.value) ||
                    (itemDetailError.field === 'UOM' && item.UOM === itemDetailError.value) ||
                    (itemDetailError.field === 'quantity' && item.quantity === itemDetailError.value) ||
                    (itemDetailError.field === 'cost' && item.cost === itemDetailError.value)
                  ) {
                    if (
                      item.result.indexOf(itemDetailError.msg) === -1 &&
                      errorString.indexOf(itemDetailError.msg) === -1
                    ) {
                      errorString = errorString + `${itemDetailError.msg}, `
                    }
                  }
                })
                if (errorString.length > 0) {
                  item.result = item.result === 'Successfully imported' ? '' : item.result + ' '
                  item.result = (item.result + errorString).trimRight()
                }
              }
            })
          })
        })

        setErrorArrPOHeader(errorArrHeader)
        setErrorArrPODetail(errorArrDetail)
        setImportState(false)
        break
      case 'ItemUpdate':
        const errorArrItem: ItemErrorType[] = []

        result.itemList.forEach((itemErr: any) => {
          const resultObjItem = {
            sku: itemErr.SKU,
            parLevel: itemErr.parLevel,
            reorderQuantity: itemErr.reorderQuantity,
            location: itemErr.location,
            active: itemErr.active,
            temperature: itemErr.temperature,
            UPC: itemErr.UPC,
            defaultPrice: itemErr.defaultPrice,
            defaultCost: itemErr.defaultCost,
            wholesaleItemID: itemErr.wholesaleItemID,
            result: 'Successfully imported',
          }
          errorArrItem.push(resultObjItem)
        })

        result.errorList.forEach((error: any, index: any) => {
          errorArrItem.forEach((item: any, index: any) => {
            if (item.sku === error.SKU) {
              errorString = ''
              error.itemErrorList.forEach((itemDetailError: any) => {
                if (
                  (itemDetailError.field === 'SKU' && item.sku === itemDetailError.value) ||
                  (itemDetailError.field === 'parLevel' && item.parLevel === itemDetailError.value) ||
                  (itemDetailError.field === 'reorderQuantity' && item.reorderQuantity === itemDetailError.value) ||
                  (itemDetailError.field === 'location' && item.location === itemDetailError.value) ||
                  (itemDetailError.field === 'active' && item.active === itemDetailError.value) ||
                  (itemDetailError.field === 'temperature' && item.temperature === itemDetailError.value) ||
                  (itemDetailError.field === 'UPC' && item.UPC === itemDetailError.value) ||
                  (itemDetailError.field === 'defaultPrice' && item.defaultPrice === itemDetailError.value) ||
                  (itemDetailError.field === 'defaultCost' && item.defaultCost === itemDetailError.value) ||
                  (itemDetailError.field === 'cost' && item.cost === itemDetailError.value)
                ) {
                  if (
                    item.result.indexOf(itemDetailError.msg) === -1 &&
                    errorString.indexOf(itemDetailError.msg) === -1
                  ) {
                    errorString = errorString + `${itemDetailError.msg}, `
                  }
                }
              })
              if (errorString.length > 0) {
                item.result = item.result === 'Successfully imported' ? '' : item.result + ' '
                item.result = (item.result + errorString).trimRight()
              }
            }
          })
        })

        //console.log(errorArrItem);
        setErrorArrItem(errorArrItem)
        setImportState(false)
        break
    }
  }

  const onCheckSplit = (e: any) => {
    setCheckedSplit(e.target.checked)
  }

  const onCheckCommodity = (e: any) => {
    setCheckedCommodity(e.target.checked)
  }

  // reset function used to back stage0
  const resetHeader = () => {
    setSelectHeaders(headers)
    setStage(0)
  }

  const onContinue = () => {
    if (stage === 1) {
      const data = excelOutput.map((row: any) => {
        return {
          customerProductCode: row['Customer Item Code'],
          quantity: row['OrderQty'],
          price: row['Price'],
          SKU: row['Product SKU'],
          UOM: row['Unit of measure']
        }
      })
      setLoading(true)
      OrderService.instance.validateImportCSV(data).subscribe({
        next(res: any) {
          // if server error show error
          setLoading(false)
          if (res.statusCodeValue !== 200) {
            notification.error({
              message: 'ERROR',
              description: res.statusCode,
              // onClose: () => { },
            })
          } else {
            setValidateResult(res.body.data)
            setStage(stage + 1)
          }
        },
        error(err: any) {
          setLoading(false)
          console.log('error', err)
        },
        complete() {
          // console.log('complete')
        },
      })
    } else {
      if (stage === 0) {
        if (checkCSVfile()) {
          setSelectHeaders(headers)
        }
      }
      setStage(stage + 1)
    }
  }

  const prepareData = () => {
    const { items } = validatedResult;
    let ordersList = [];
    if (items.length > 0) {
      const orderItems = items.map((item: any, index: number) => {
        const excelItem = excelOutput[index]
        return {
          ...item,
          data: {
            SKU: excelItem['Product SKU'],
            UOM: excelItem['Unit of measure'],
            customerProductCode: excelItem['Customer Item Code'],
            price: excelItem['Price'],
            quantity: excelItem['OrderQty']
          }
        }
      })

      const maxWeightValue = maxWeight ? parseInt(maxWeight, 0) : 0
      const maxVolumeValue = maxVolume ? parseInt(maxVolume, 0) : 0

      let sumWeights = 0
      let sumVolumes = 0
      let prevCommondity = orderItems[0].commodityClass;
      if (checkedSplit) {
        let subOrders = []
        orderItems.forEach((item: any) => {
          sumWeights += item.grossWeight ? parseInt(item.grossWeight, 0) : 0
          sumVolumes += item.grossVolume ? parseInt(item.grossVolume, 0) : 0
          let addable = false
          if (checkedCommodity) {
            if (prevCommondity !== item.commodityClass) {
              prevCommondity = item.commodityClass
            } else {
              addable = true
            }
          } else {
            if (maxWeightValue === 0 || sumWeights <= maxWeightValue) {
              if (maxVolumeValue === 0 || sumVolumes <= maxVolumeValue) {
                addable = true
              }
            }
          }
          if (!addable) {
            ordersList.push(cloneDeep(subOrders))
            subOrders = []
            sumWeights = 0
            sumVolumes = 0
          }
          subOrders.push(item)
        })
        if (subOrders.length > 0) {
          ordersList.push(cloneDeep(subOrders))
        }
      } else {
        ordersList = [cloneDeep(orderItems)];
      }
    }
    return ordersList
  }

  //import function
  const importFile = async (saveResults: boolean, ordersList: any[]|null) => {
    //loading
    setStage(5)

    // import by api
    switch (fileUploadMode) {
      case 'Sales':
        // const ordersList = prepareData();
        const dataList = ordersList.map((items: any) => ({
          deliveryDate: moment().format('M/D/YYYY'),
          itemList: items.map((item: any) => ({
            ...item.data
          }))
        }))
        OrderService.instance.createCSVOrders({ dataList }, parseInt(customerId)).subscribe({
          next(res: any) {
            // if server error show error
            if (res.statusCodeValue !== 200) {
              notification.error({
                message: 'ERROR',
                description: res.statusCode,
                onClose: () => {},
              })
              // back stage0
              setStage(0)
            } else {
              // success
              // if (res.body.data.wholesaleOrderId == undefined) {
              //   importFail(res.body.data)
              // } else {
              const ids = res.body.data.map((so: any) => so.wholesaleOrderId.toString())
              setCreatedIds(ids)
              setCheckedSplit(false)
              setMaxWeight(undefined)
              setMaxVolume(undefined)
              setCheckedCommodity(false)
              setStage(4)
            }
          },
          error(err: any) {
            setStage(0)
          },
          complete() {
            //setStage(0)
          },
        })
        break
      case 'PO':
        const data = getImportFile(saveResults)
        OrderService.instance.createCSVPurchaseOrders(data).subscribe({
          next(res: any) {
            // if server error show error
            if (res.statusCodeValue !== 200) {
              notification.error({
                message: 'ERROR',
                description: res.statusCode,
                onClose: () => {},
              })
              // back stage0
              setStage(0)
            } else {
              // success
              const x = JSON.parse(res.body.data)
              x.orderList[0].wholesaleOrderId == undefined || x.orderList[0].wholesaleOrderId === 0
                ? importFail(x)
                : importSuccess(x)
              setStage(2)
            }
          },
          error(err: any) {
            setStage(0)
          },
          complete() {
            //setStage(0)
          },
        })
        break
      case 'ItemUpdate':
        const data = getImportFile(saveResults)
        OrderService.instance.updateItemsFromCSV(data).subscribe({
          next(res: any) {
            // if server error show error
            if (res.statusCodeValue !== 200) {
              notification.error({
                message: 'ERROR',
                description: res.statusCode,
                onClose: () => {},
              })
              // back stage0
              setStage(0)
            } else {
              // success
              const x = JSON.parse(res.body.data)
              x.errorList.length > 0 ? importFail(x) : importSuccess(x)
              setStage(2)
            }
          },
          error(err: any) {
            setStage(0)
          },
          complete() {
            //setStage(0)
          },
        })
        break
    }
  }

  // all components
  const getMainBody = () => {
    switch (stage) {
      case 0:
        switch (fileUploadMode) {
          case 'Sales':
            return (
              <>
                <div className="selectDiv">
                  <h6 className="stepText">Step 1: Select a customer</h6>
                  <Select
                    showSearch
                    className="uploadSelect"
                    placeholder="Select a customer"
                    optionFilterProp="children"
                    onChange={onChange}
                  >
                    {getOPtions()}
                  </Select>
                  <h6 className="stepText">Step 2: Select a CSV upload</h6>
                  <CSVUpload
                    fileUploadMode={fileUploadMode}
                    onWrongFormat={uploadWrong}
                    setExcelOutput={(value) => setExcelOutput(value)}
                    setHeaders={(value) => setHeaders(value)}
                    upLoadSuccess={(res) => {
                      setNextButton({
                        uploadCSV: true,
                        selectCustomer: nextButton.selectCustomer,
                      })
                    }}
                  />
                </div>
                <CSVDownload
                  arr={templateArr}
                  headerArr={templateHeaders}
                  className="downloadCss"
                  fileName={templateFileName}
                />
                {/* <Button
                  className={
                    nextButton.uploadCSV === true && nextButton.selectCustomer === true
                      ? 'nextButtonShow'
                      : 'nextButtonHide'
                  }
                  onClick={() => toNext()}
                >
                  NEXT
                </Button>
                <Button className="cancelButton" onClick={() => onBack()}>
                  CANCEL
                </Button> */}


                <Flex className="v-center mapdata-btns">
                  <ThemeButton onClick={onContinue} disabled={!(nextButton.uploadCSV === true && nextButton.selectCustomer === true)}>NEXT</ThemeButton>
                  <Button onClick={() => onBack()}><p style={{ color: 'green' }}>CANCEL</p></Button>
                </Flex>
              </>
            )
          case 'PO':
          case 'ItemUpdate':
            return (
              <>
                <div className="selectDiv">
                  <h6 className="stepText">Step 1: Select a CSV upload</h6>
                  <CSVUpload
                    fileUploadMode={fileUploadMode}
                    onWrongFormat={uploadWrong}
                    setExcelOutput={(value) => setExcelOutput(value)}
                    setHeaders={(value) => setHeaders(value)}
                    upLoadSuccess={() => {
                      setNextButton({
                        uploadCSV: true,
                        selectCustomer: nextButton.selectCustomer,
                      })
                    }}
                  />
                </div>
                <CSVDownload
                  arr={templateArr}
                  headerArr={templateHeaders}
                  className="downloadCss"
                  fileName={templateFileName}
                />
                <Button
                  className={nextButton.uploadCSV == true ? 'nextButtonShow' : 'nextButtonHide'}
                  onClick={() => toNext()}
                >
                  NEXT
                </Button>
                <Button className="cancelButton" onClick={() => onBack()}>
                  CANCEL
                </Button>
              </>
            )
        }
      case 1:
        return (
          <div className="importDiv" style={{marginBottom: 0}}>
            <p className="importTitle">Map your fields to WholeSaleWare fields </p>
            <span className="total-items">{excelOutput.length} total items</span>
            <div className="flexDiv">
              <p className="importPLeft" style={{ paddingLeft: '5px', width: '250px', fontWeight: 'normal', marginBottom: 0 }}>
                WHOLESALEWARE FIELD
              </p>
              <p className="importSelect" style={{ paddingLeft: '15px', width: '230px', fontWeight: 'normal', marginBottom: 0 }}>
                YOUR FIELD
              </p>
              <p className="importPRight" style={{ paddingLeft: '5px', width: '300px', fontWeight: 'normal', marginBottom: 0 }}>
                EXAMPLE VALUE (FIRST ROW OF DATA)
              </p>
              <Button
                className="resetButton"
                style={{
                  padding: '0px',
                  height: '20px',
                  lineHeight: '20px',
                  position: 'absolute',
                  color: 'green',
                  border: '1px solid green',
                  borderRadius: '10px',
                }}
                onClick={() => resetHeader()}
              >
                RESET
                <img style={{ width: '12px', height: '12px', marginTop: '-4px' }} src={require('./reset.jpg')} />
              </Button>
            </div>
            <div style={{ maxHeight: '245px', overflow: 'auto' }}>
              {templateHeaders.map((header, index) => {
                return getImportTable(index, templateHeaders.length)
              })}
            </div>
            <ThemeCheckbox style={{ marginTop: 15 }} value={checkedSplit} onChange={onCheckSplit}>Split into multiple sales orders</ThemeCheckbox>
            {checkedSplit && (
              <Flex className="split-params">
                <div style={{ width: 265, marginRight: 50 }}>
                  <h3>1. Set split parameters</h3>
                  <label>Maximum Weight (lbs) per Container</label>
                  <ThemeInput value={maxWeight} onChange={(e: any) => setMaxWeight(e.target.value)} />
                  <label>Maximum Volume (ft³) per Container</label>
                  <ThemeInput value={maxVolume} onChange={(e: any) => setMaxVolume(e.target.value)}/>
                </div>
                <div>
                  <h3>2. Choose split options</h3>
                  <ThemeCheckbox value={checkedCommodity} onChange={onCheckCommodity}>Split by commodity classes</ThemeCheckbox>
                </div>
              </Flex>
            )}
            <Flex className="v-center mapdata-btns">
              <ThemeButton onClick={onContinue}>Continue</ThemeButton>
              <Button onClick={() => onBack()}><p style={{ color: 'green' }}>CANCEL</p></Button>
            </Flex>
          </div>
        )
        break;
      case 2:
        // Validation
        return (
          <UploadValidation
            excelOutput={excelOutput}
            validatedResult={validatedResult}
            onContinue={ onContinue }
            onBack={ onBack }
          />)
        break;
      case 3:
        // Preview
        return (
          <UploadPreview onContinue={(dataList: any[]) => importFile(true, dataList)} onBack={onBack} ordersList={prepareData()}/>
        )
        break;
      case 4:
        // Import results
        return <div className="importDiv">{getImportBody()}</div>
      case 5:
        return (
          <>
            <div style={{ height: '170px', marginLeft: '420px', marginTop: '130px' }}>
              <Spin size="large" />
            </div>
            <p style={{ marginLeft: '330px' }}>Importing data, please wait....</p>
          </>
        )
      default:
        break
    }
  }

  //stage2 component
  const getImportBody = () => {
    switch (fileUploadMode) {
      case 'Sales':
        return (
          <div className="importDiv">
            <p className="importTitle">Import results</p>
            {getImportResultSO()}
          </div>
        )
      case 'PO':
        return (
          <div className="importDiv">
            <p className="importTitle">Import results</p>
            {getImportResultPO()}
          </div>
        )
      case 'ItemUpdate':
        return (
          <div className="importDiv">
            <p className="importTitle">Import results</p>
            {getImportResultItem()}
          </div>
        )
    }
  }

  const getImportResultItem = () =>
    !importState ? (
      <>
        <p className="" style={{ color: 'black' }}>
          No items updated, please correct any problems and re-upload.
        </p>
        <p className="" style={{ color: 'black' }}>
          {`${
            errorArrItem.filter((item: ItemErrorType) => {
              return item.result === 'Successfully imported'
            }).length
          } of ${errorArrItem.length} rows with valid data`}
        </p>
        <div className="flexDiv">
          <div style={{ width: '10%' }}>
            <p style={{ paddingLeft: '10px' }}>
              <span className="boldStyle" style={{ color: 'black' }}>
                ROW #
              </span>
            </p>
          </div>
          <div style={{ width: '90%' }}>
            <p>
              <span className="boldStyle" style={{ color: 'black', marginLeft: '13px' }}>
                MESSAGE
              </span>
            </p>
          </div>
        </div>
        <div className="flexDiv" style={{ height: `${resultHeight}px` }}>
          <table cellPadding={'0'} cellSpacing={'0'} width={'100%'}>
            {errorArrItem.map((item, index) => {
              return (
                <tr style={{ height: '20x' }}>
                  <td
                    style={{
                      width: '10%',
                      borderTop: '1px solid black',
                      borderLeft: '1px solid black',
                      borderRight: '1px solid black',
                      borderBottom: index === errorArrItem.length - 1 ? '1px solid black' : '',
                      marginBottom: '0px',
                      paddingLeft: '10px',
                      fontWeight: 'normal',
                    }}
                  >
                    <span key={index} style={{ fontStyle: 'normal' }}>
                      {index + 1}
                    </span>
                  </td>
                  <td
                    style={{
                      width: '90%',
                      borderTop: '1px solid black',
                      borderRight: '1px solid black',
                      borderBottom: index === errorArrItem.length - 1 ? '1px solid black' : '',
                      marginBottom: '0px',
                      fontWeight: 'normal',
                    }}
                  >
                    {item.result == 'Successfully imported' ? (
                      <img
                        style={{
                          width: '20px',
                          height: '20px',
                          marginRight: '10px',
                          marginLeft: '10px',
                          marginTop: '-5px',
                        }}
                        src={require('./right.png')}
                      />
                    ) : (
                      <img
                        style={{
                          width: '20px',
                          height: '20px',
                          marginRight: '10px',
                          marginLeft: '10px',
                          marginTop: '-5px',
                        }}
                        src={require('./alert.png')}
                      />
                    )}

                    <span key={index} style={{ fontStyle: 'normal' }}>
                      {item.result !== 'Successfully imported'
                        ? `Error: ${item.result.toString().substring(0, item.result.length - 1)}`
                        : 'formatted correctly'}
                    </span>
                  </td>
                </tr>
              )
            })}
          </table>
        </div>
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220 - 46}px`,
            width: '166px',
            backgroundColor: 'white',
            color: '#1C6E31',
            position: 'absolute',
            border: 'white 1px solid',
            left: '-20px',
          }}
          onClick={() => {
            resultHeight === 200 ? setResultHeight(500) : setResultHeight(200)
          }}
        >
          <img
            style={{
              width: '20px',
              height: '20px',
              marginRight: '10px',
              marginLeft: '10px',
              marginTop: '-5px',
            }}
            src={require('./more.jpg')}
          />
          View more/less results
        </Button>
        <CSVDownloadResult
          arr={errorArrItem.map((item, index) => [
            (index + 1).toString(),
            item.result !== 'Successfully imported'
              ? item.result.toString().substring(0, item.result.length - 1)
              : item.result,
          ])}
          headerArr={['Row#', 'Message']}
          fileName={'import result'}
          resultHeight={resultHeight - 46}
        />
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220 - 46}px`,
            width: '166px',
            right: '20px',
          }}
          className="nextButtonShow"
          onClick={() => onBack()}
        >
          Exit
        </Button>
      </>
    ) : (
      <>
        <p className="" style={{ color: 'black' }}>
          File successfully uploaded.
        </p>
        <p className="">{excelOutput.length} item(s) updated</p>
        <div className="flexDiv">
          <div style={{ width: '20%' }}>
            <p>
              <span className="boldStyle" style={{ color: 'black' }}>
                ROW #
              </span>
            </p>
          </div>
          <div style={{ width: '80%' }}>
            <p>
              <span className="boldStyle" style={{ color: 'black' }}>
                MESSAGE
              </span>
            </p>
          </div>
        </div>
        <div className="flexDiv" style={{ height: `${resultHeight}px` }}>
          <div style={{ width: '20%' }}>
            {excelOutput.map((item, index) => {
              return (
                <p
                  style={{
                    borderTop: '1px solid black',
                    borderLeft: '1px solid black',
                    borderRight: '1px solid black',
                    borderBottom: index === excelOutput.length - 1 ? '1px solid black' : '',
                    marginBottom: '0px',
                    lineHeight: '48px',
                    paddingLeft: '10px',
                    fontWeight: 'normal',
                  }}
                >
                  <span key={index} style={{ fontStyle: 'normal' }}>
                    {index + 1}
                  </span>
                </p>
              )
            })}
          </div>
          <div style={{ width: '80%' }}>
            {excelOutput.map((item, index) => {
              return (
                <p
                  style={{
                    borderTop: '1px solid black',
                    borderRight: '1px solid black',
                    borderBottom: index === excelOutput.length - 1 ? '1px solid black' : '',
                    marginBottom: '0px',
                    lineHeight: '48px',
                    fontWeight: 'normal',
                  }}
                >
                  <img
                    style={{
                      width: '20px',
                      height: '20px',
                      marginRight: '10px',
                      marginLeft: '10px',
                      marginTop: '-5px',
                    }}
                    src={require('./right.png')}
                  />
                  <span key={index} style={{ fontStyle: 'normal' }}>
                    Successfully imported
                  </span>
                </p>
              )
            })}
          </div>
        </div>
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220 - 46}px`,
            width: '166px',
            backgroundColor: 'white',
            color: '#1C6E31',
            position: 'absolute',
            border: 'white 1px solid',
            left: '-20px',
          }}
          onClick={() => {
            resultHeight === 200 ? setResultHeight(500) : setResultHeight(200)
          }}
        >
          <img
            style={{
              width: '20px',
              height: '20px',
              marginRight: '10px',
              marginLeft: '10px',
              marginTop: '-5px',
            }}
            src={require('./more.jpg')}
          />
          View more results
        </Button>
        <CSVDownloadResult
          arr={excelOutput.map((item, index) => [(index + 1).toString(), 'Successfully imported'])}
          headerArr={['Row#', 'Message']}
          fileName={'import result'}
          resultHeight={resultHeight - 46}
        />
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220 - 46}px`,
            width: '166px',
            right: '20px',
          }}
          className="nextButtonShow"
          onClick={() => onBack()}
        >
          Exit
        </Button>
      </>
    )

  const getImportResultPO = () =>
    !importState ? (
      <>
        <p className="" style={{ color: 'black' }}>
          No Purchase Orders created, please correct any problems and re-upload.
        </p>
        <p className="" style={{ color: 'black' }}>
          {`${
            errorArrPODetail.filter((item: PODetailErrorType) => {
              return item.result === 'Successfully imported'
            }).length
          } of ${errorArrPODetail.length} rows with valid data`}
        </p>
        <div className="flexDiv">
          <div style={{ width: '10%' }}>
            <p style={{ paddingLeft: '10px' }}>
              <span className="boldStyle" style={{ color: 'black' }}>
                ROW #
              </span>
            </p>
          </div>
          <div style={{ width: '90%' }}>
            <p>
              <span className="boldStyle" style={{ color: 'black', marginLeft: '13px' }}>
                MESSAGE
              </span>
            </p>
          </div>
        </div>
        <div className="flexDiv" style={{ height: `${resultHeight}px` }}>
          <table cellPadding={'0'} cellSpacing={'0'} width={'100%'}>
            {errorArrPODetail.map((item, index) => {
              return (
                <tr style={{ height: '20px' }}>
                  <td
                    style={{
                      width: '10%',
                      borderTop: '1px solid black',
                      borderLeft: '1px solid black',
                      borderRight: '1px solid black',
                      borderBottom: index === errorArrPODetail.length - 1 ? '1px solid black' : '',
                      marginBottom: '0px',
                      paddingLeft: '10px',
                      fontWeight: 'normal',
                    }}
                  >
                    <span key={index} style={{ fontStyle: 'normal' }}>
                      {index + 1}
                    </span>
                  </td>
                  <td
                    style={{
                      width: '90%',
                      borderTop: '1px solid black',
                      borderRight: '1px solid black',
                      borderBottom: index === errorArrPODetail.length - 1 ? '1px solid black' : '',
                      marginBottom: '0px',
                      fontWeight: 'normal',
                    }}
                  >
                    {item.result == 'Successfully imported' ? (
                      <img
                        style={{
                          width: '20px',
                          height: '20px',
                          marginRight: '10px',
                          marginLeft: '10px',
                          marginTop: '-5px',
                        }}
                        src={require('./right.png')}
                      />
                    ) : (
                      <img
                        style={{
                          width: '20px',
                          height: '20px',
                          marginRight: '10px',
                          marginLeft: '10px',
                          marginTop: '-5px',
                        }}
                        src={require('./alert.png')}
                      />
                    )}

                    <span key={index} style={{ fontStyle: 'normal' }}>
                      {item.result !== 'Successfully imported'
                        ? `Error: ${item.result.toString().substring(0, item.result.length - 1)}`
                        : 'formatted correctly'}
                    </span>
                  </td>
                </tr>
              )
            })}
          </table>
        </div>
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220}px`,
            width: '166px',
            backgroundColor: 'white',
            color: '#1C6E31',
            position: 'absolute',
            border: 'white 1px solid',
            left: '-20px',
          }}
          onClick={() => {
            resultHeight === 200 ? setResultHeight(500) : setResultHeight(200)
          }}
        >
          <img
            style={{
              width: '20px',
              height: '20px',
              marginRight: '10px',
              marginLeft: '10px',
              marginTop: '-5px',
            }}
            src={require('./more.jpg')}
          />
          View more/less results
        </Button>
        <CSVDownloadResult
          arr={errorArrPODetail.map((item, index) => [
            (index + 1).toString(),
            item.result !== 'Successfully imported'
              ? item.result.toString().substring(0, item.result.length - 1)
              : item.result,
          ])}
          headerArr={['Row#', 'Message']}
          fileName={'import result'}
          resultHeight={resultHeight}
        />
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220}px`,
            width: '166px',
            right: '20px',
          }}
          className="nextButtonShow"
          onClick={() => onBack()}
        >
          Exit
        </Button>
      </>
    ) : (
      <>
        <p className="" style={{ color: 'black' }}>
          {`${excelOutput.length} of ${excelOutput.length} rows imported successfully`}
        </p>
        <p className="">{orderList.length} Purchase Order(s) created.</p>

        <div className="flexDiv">
          <div style={{ width: '60%' }}>
            <p>
              <span className="boldStyle" style={{ color: 'black' }}>
                Vendor - Delivery Date
              </span>
            </p>
          </div>
          <div style={{ width: '40%' }}>
            <p>
              <span className="boldStyle" style={{ color: 'black' }}>
                Purchase Order
              </span>
            </p>
          </div>
        </div>
        <div className="flexDiv" style={{ height: `${resultHeight}px` }}>
          <div style={{ width: '60%' }}>
            {orderList.map((item, index) => {
              return (
                <p
                  style={{
                    borderTop: '1px solid black',
                    borderLeft: '1px solid black',
                    borderRight: '1px solid black',
                    borderBottom: index === orderList.length - 1 ? '1px solid black' : '',
                    marginBottom: '0px',
                    lineHeight: '48px',
                    paddingLeft: '10px',
                    fontWeight: 'normal',
                  }}
                >
                  <span key={index} style={{ fontStyle: 'normal' }}>
                    {item.vendorName} - {item.deliveryDate}
                  </span>
                </p>
              )
            })}
          </div>
          <div style={{ width: '40%' }}>
            {orderList.map((item, index) => {
              return (
                <p
                  style={{
                    borderTop: '1px solid black',
                    borderRight: '1px solid black',
                    borderBottom: index === orderList.length - 1 ? '1px solid black' : '',
                    marginBottom: '0px',
                    lineHeight: '48px',
                    fontWeight: 'normal',
                  }}
                >
                  <img
                    style={{
                      width: '20px',
                      height: '20px',
                      marginRight: '10px',
                      marginLeft: '10px',
                      marginTop: '-5px',
                    }}
                    src={require('./right.png')}
                  />
                  <span key={index} style={{ fontStyle: 'normal' }}>
                    <NavLink to={`/order/${item.orderNumber}/purchase-cart`}>PO {item.orderNumber}</NavLink>
                  </span>
                </p>
              )
            })}
          </div>
        </div>
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220}px`,
            width: '166px',
            backgroundColor: 'white',
            color: '#1C6E31',
            position: 'absolute',
            border: 'white 1px solid',
            left: '-20px',
          }}
          onClick={() => {
            resultHeight === 200 ? setResultHeight(500) : setResultHeight(200)
          }}
        >
          <img
            style={{
              width: '20px',
              height: '20px',
              marginRight: '10px',
              marginLeft: '10px',
              marginTop: '-5px',
            }}
            src={require('./more.jpg')}
          />
          View more/less results
        </Button>
        <CSVDownloadResult
          arr={orderList.map((item, index) => [item.vendorName, item.deliveryDate, item.orderNumber])}
          headerArr={['Vendor', 'Delivery Date', 'PO Number']}
          fileName={'PO import result'}
          resultHeight={resultHeight}
        />
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 220}px`,
            width: '166px',
            right: '20px',
          }}
          className="nextButtonShow"
          onClick={() => onBack()}
        >
          Exit
        </Button>
      </>
      )

  const onFinish = () => {
    window.location.reload()
  }

  const getImportResultSO = () => {
    const ordersLinks = createdIds.map((id: string, index: number) => {
      return <><a target="_blank" href={`#/sales-order/${id}`}>{id}</a>{index < createdIds.length-1 ? ',' : ''} </>
    })
    return (
      <>
        <p className="" style={{ color: 'black' }}>
          {`${excelOutput.length} total items imported.`}
        </p>
        <p style={{ marginBottom: 150 }}>Sales Orders {ordersLinks} created successfully</p>
        <Button
          style={{
            borderRadius: '40px',
            top: 298,
            width: '166px',
            right: '20px',
          }}
          className="nextButtonShow"
        >
          <a href='javascript:;' onClick={onFinish}>Finish</a>
        </Button>
      </>
    )
  }

  //alert component
  const onCloseAlert = () =>
    setAlert({
      alertState: false,
      text: '',
    })

  const getAlert = () => {
    return alert.alertState ? (
      <Alert message="" description={alert.text} type="error" closable onClose={onCloseAlert} />
    ) : (
      <></>
    )
  }

  return (
    <>
      <Modal
        width={1200}
        style={{ height: '1000px' }}
        visible={visible}
        onCancel={onBack}
        className="uploadModel"
        footer={
          <div style={{ height: '50px' }}>
            <p style={{ color: 'white' }}>asdqw</p>
          </div>
        }
      >
        <ThemeSpin spinning={loading}>
          <div className="uploadModelTitle">{getStage()}</div>
          {getAlert()}
          {getMainBody()}
        </ThemeSpin>
      </Modal>
    </>
  )
}

export default SalesOrderUploadCsv
