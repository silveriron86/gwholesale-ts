/* eslint-disable react/jsx-key */
import { Button, Select } from 'antd'
import React, { useState, useEffect } from 'react'
import { formatNumber } from '~/common/utils'
import { Flex, ThemeButton, ThemeModal, ThemeSelect, ThemeTextButton } from '~/modules/customers/customers.style'
import './orderUpload.css'
import { Icon as IconSvg } from '~/components/icon/'
import { Item, NoBorderButton } from '~/modules/customers/nav-sales/styles'
import { cloneDeep } from 'lodash'

interface IProps {
  onContinue: Function
  onBack: Function
  ordersList: any[]
}

const UploadPreview = ({ onContinue, onBack, ordersList }: IProps): JSX.Element => {
  const [visibleMoveModal, setVisibleMoveModal] = useState<boolean>(false)
  const [selectedOrderIndex, selectOrder] = useState<number>(-1)
  const [selectedItemIndex, selectItem] = useState<number>(-1)
  const [toOrderIndex, setToOrderIndex] = useState<number|string>('')
  const [dataList, setDataList] = useState<any[]>([])

  useEffect(() => {
    setDataList(cloneDeep(ordersList))
  }, [ordersList])

  const onCancelMove = () => {
    selectOrder(-1)
    selectItem(-1)
    setToOrderIndex('');
    setVisibleMoveModal(false)
  }

  const onSaveMove = () => {
    const data = cloneDeep(dataList);
    data[toOrderIndex].push(data[selectedOrderIndex][selectedItemIndex])
    data[selectedOrderIndex].splice(selectedItemIndex, 1)
    setDataList(data)
    onCancelMove()
  }

  const getTotals = (order: any[]) => {
    // Sum of all ordered units for all items split into Sales Order
    const totalOrderedUnits = order.reduce(function (prev, item) {
      return prev + parseInt(item.data.quantity, 10);
    }, 0);

    // Total lbs: Sum of [qty of each order item x unit weight (from product configuration)]
    const totalLbs = order.reduce(function (prev: number, item: any) {
      return prev + parseInt(item.data.quantity, 10) * item.grossWeight;
    }, 0);

    // Total cubic ft: Sum of [qty of each order item x unit volume (from product configuration)]
    const totalCubicFt = order.reduce(function (prev: number, item: any) {
      return prev + parseInt(item.data.quantity, 10) * item.grossVolume;
    }, 0);
    return { totalOrderedUnits, totalLbs, totalCubicFt }
  }

  const selectedOrderTotals = selectedOrderIndex >= 0 ? getTotals(dataList[selectedOrderIndex]) : null

  return (
    <div className="importDiv">
      <p className="importTitle">Preview</p>
      <div className='preview-container'>
        <div className="preview-orders">
          {dataList.map((order, orderIndex) => {
            const { totalOrderedUnits, totalLbs, totalCubicFt }  = getTotals(order)
            return (
              <div key={`so-${orderIndex}`} className={`preview-so ${orderIndex === dataList.length-1 ? 'last' : ''}`}>
                <h3>Sale Order {orderIndex+1}</h3>
                <div className="specs">
                  <span>{formatNumber(totalOrderedUnits, 0)} units</span>
                  <span>{formatNumber(totalLbs, 0)} lbs</span>
                  <span>{formatNumber(totalCubicFt, 0)} ft³</span>
                </div>
                {order.length > 0 && (<ThemeTextButton>Move all {order.length} items</ThemeTextButton>)}
                <div className="so-items">
                  {order.sort((a: any, b:any) => a.variety.localeCompare(b.variety)).map((item: any, itemIndex: number) => (
                    <Flex key={`so-item-${orderIndex}-${itemIndex}`} className='v-center space-between'>
                      <span>{item.variety}</span>
                      <span>{item.data.quantity}</span>
                      {dataList.length > 1 && (
                        <IconSvg
                          className="move"
                          type="edit"
                          viewBox="0 0 18 18"
                          width={16}
                          height={16}
                          onClick={() => {
                            selectOrder(orderIndex)
                            selectItem(itemIndex)
                            setVisibleMoveModal(true)
                          }}
                        />
                      )}
                    </Flex>
                  ))}
                </div>

              </div>);
          })}
          <div className="clear"></div>
        </div>
      </div>

      <Flex className="v-center mapdata-btns" style={{marginTop: 56}}>
        <ThemeButton onClick={() => onContinue(dataList)}>Import orders</ThemeButton>
        <Button onClick={onBack}><p style={{ color: 'green' }}>CANCEL</p></Button>
      </Flex>
      <ThemeModal
        title={`Move item`}
        visible={visibleMoveModal}
        onCancel={() => setVisibleMoveModal(false)}
        okButtonProps={{ style: { display: 'none' } }}
        cancelButtonProps={{ style: { display: 'none' } }}
        footer={
          <Flex className="v-center">
            <ThemeButton onClick={onSaveMove} disabled={toOrderIndex === ''}>Save</ThemeButton>
            <NoBorderButton onClick={onCancelMove}>Cancel</NoBorderButton>
          </Flex>
        }
      >
        <div className='move-item-wrapper'>
          <h3>Sale Order {selectedOrderIndex + 1}</h3>
          {selectedOrderTotals !== null && (
            <div className="specs">
              <span>{formatNumber(selectedOrderTotals.totalOrderedUnits, 0)} units</span>
              <span>{formatNumber(selectedOrderTotals.totalLbs, 0)} lbs</span>
              <span>{formatNumber(selectedOrderTotals.totalCubicFt, 0)} ft³</span>
            </div>
          )}
          <label>Move to sale order</label>
          <ThemeSelect value={toOrderIndex} onChange={setToOrderIndex}>
            {dataList.map((order, orderIndex) => {
              if (orderIndex === selectedOrderIndex) {
                return null;
              }
              return (
                <Select.Option key={`so-${orderIndex}`} value={orderIndex}>Sales Order {orderIndex+1}</Select.Option>
              );
            })}
          </ThemeSelect>
          </div>
      </ThemeModal>
    </div>
  )
}

export default UploadPreview
