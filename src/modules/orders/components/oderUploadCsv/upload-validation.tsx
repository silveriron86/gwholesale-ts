/* eslint-disable react/jsx-key */
import { Button, Tooltip } from 'antd'
import React, { useState } from 'react'
import { Flex, ThemeButton } from '~/modules/customers/customers.style'
import CSVDownloadResult from './CSVDownloadResult'
import './orderUpload.css'

interface IProps {
  excelOutput: any[]
  validatedResult: any
  onContinue: Function
  onBack: Function
}

const UploadValidation = ({ excelOutput, validatedResult, onContinue, onBack }: IProps): JSX.Element => {
  const { errors, items } = validatedResult
  // const validatedItems = [...errors, ...items];
  const [resultHeight, setResultHeight] = useState<number>(200)

  let validatedItems = new Array(excelOutput.length).fill(null)
  if (errors.length > 0) {
    errors.forEach((error: any) => {
      validatedItems[error.index] = error
    })
  }
  items.forEach((item: any) => {
    // get next available index
    const index = validatedItems.findIndex((vi: any) => vi === null)
    validatedItems[index] = item
  })

  return (
    <div className="importDiv">
      <p className="importTitle">Data Validation</p>
      <p className="" style={{ color: 'black' }}>
        { validatedItems.length } total items
      </p>
      <div className="flexDiv">
        <div style={{ width: '10%' }}>
          <p style={{ paddingLeft: '10px' }}>
            <span className="boldStyle" style={{ color: 'black' }}>
              ROW #
            </span>
          </p>
        </div>
        <div style={{ width: '90%' }}>
          <p>
            <span className="boldStyle" style={{ color: 'black', marginLeft: '13px' }}>
              MESSAGE
            </span>
          </p>
        </div>
      </div>
      <div className="flexDiv" style={{ height: `${resultHeight}px` }}>
        <table cellPadding={'0'} cellSpacing={'0'} width={'100%'} style={{height: 'fit-content'}}>
          {validatedItems.map((item, index) => {
            const isSuccess = typeof item.errorTypes === 'undefined'
            return (
              <tr style={{ height: 50 }}>
                <td
                  style={{
                    width: '10%',
                    borderTop: '1px solid black',
                    borderLeft: '1px solid black',
                    borderRight: '1px solid black',
                    borderBottom: index === validatedItems.length - 1 ? '1px solid black' : '',
                    marginBottom: '0px',
                    paddingLeft: '10px',
                    fontWeight: 'normal',
                  }}
                >
                  <span key={index} style={{ fontStyle: 'normal' }}>
                    {index + 1}
                  </span>
                </td>
                <td
                  style={{
                    width: '90%',
                    borderTop: '1px solid black',
                    borderRight: '1px solid black',
                    borderBottom: index === validatedItems.length - 1 ? '1px solid black' : '',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                  }}
                >
                  {isSuccess ? (
                    <img
                      style={{
                        width: '20px',
                        height: '20px',
                        marginRight: '10px',
                        marginLeft: '10px',
                        marginTop: '-5px',
                      }}
                      src={require('./right.png')}
                    />
                  ) : (
                    <img
                      style={{
                        width: '20px',
                        height: '20px',
                        marginRight: '10px',
                        marginLeft: '10px',
                        marginTop: '-5px',
                      }}
                      src={require('./alert.png')}
                    />
                  )}

                  <span key={index} style={{ fontStyle: 'normal' }}>
                    {isSuccess ? 'Data validated' : `Error: ${item.errorTypes[0]}`}
                  </span>
                </td>
              </tr>
            )
          })}
        </table>
      </div>
      {validatedItems.length > 4 && (
        <Button
          style={{
            borderRadius: '40px',
            top: `${resultHeight + 175}px`,
            width: '166px',
            backgroundColor: 'white',
            color: '#1C6E31',
            position: 'absolute',
            border: 'white 1px solid',
            left: '-20px',
          }}
          onClick={() => {
            resultHeight === 200 ? setResultHeight(500) : setResultHeight(200)
          }}
        >
          <img
            style={{
              width: '20px',
              height: '20px',
              marginRight: '10px',
              marginLeft: '10px',
              marginTop: '-5px',
            }}
            src={require('./more.jpg')}
          />
          View more/less results
        </Button>
      )}
      <CSVDownloadResult
        isNew={true}
        arr={validatedItems.map((item, index) => [
          (index + 1).toString(),
          typeof item.errorTypes === 'undefined' ? 'Data validated' : `Error: ${item.errorTypes[0]}`
        ])}
        headerArr={['Row#', 'Message']}
        fileName={'Data validation'}
        resultHeight={resultHeight}
      />
      <Flex className="v-center mapdata-btns" style={{ marginTop: 56 }}>
        {errors.length > 0 ?
          <Tooltip title="Please fix the errors listed above and try importing again.">
            <ThemeButton disabled={true}>Continue1</ThemeButton>
          </Tooltip>
          :
          <ThemeButton onClick={onContinue}>Continue</ThemeButton>
        }
        <Button onClick={onBack}><p style={{ color: 'green' }}>CANCEL</p></Button>
      </Flex>
    </div>
  )
}

export default UploadValidation
