import styled from '@emotion/styled'
import { Icon, Modal, Table } from 'antd'
import _ from 'lodash'
import moment from 'moment'
import React, { FC, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { of } from 'rxjs'
import { checkError, copyStyles, formatNumber, formatOrderStatus, judgeConstantRatio, responseHandler } from '~/common/utils'
import { Flex, ThemeButton } from '~/modules/customers/customers.style'
import { OrderService } from '../order.service'


const BatchPickModal: FC<any> = ({ status, onCancel, from, to, sellerSetting }) => {
    const DIRECTION_TYPES = {
      ASC: 'ASC',
      DESC: 'DESC',
    }

    const ANTD_ORDER_CONVER = {
      ascend: DIRECTION_TYPES.ASC,
      descend: DIRECTION_TYPES.DESC,
    }

    const defultQuery = {
      page: 0,
      pageSize: 10,
      from,
      to,
      status
    }

    const [selectedRows, setSelectedRows] = useState<any[]>([])
    const [total, setTotal] = useState(0)
    const [dataSource, setdataSource] = useState([])
    const [loading, setLoading] = useState(false)
    const [query, setQuery] = useState(defultQuery)
    const [printData, setPrintData] = useState([])
    const [printButtonLoading, setPrintButtonLoading] = useState(false)
    const [showSelectedTotal, setShowSelectedTotal] = useState(false)
    const [selectedTotalLock, setSelectedTotalLock] = useState(false)
    const [orderTotal, setOrderTotal] = useState(0)
    const [itemTotal, setItemTotal] = useState(0)

    const fetchData = () => {
      setLoading(true)

      OrderService.instance.getWarehouseSalesOrders(query).subscribe({
        next(res: any) {
          of(responseHandler(res, false).body.data)
          setdataSource(res.body.data.dataList.filter((order: { wholesaleOrderStatus: string }) => order.wholesaleOrderStatus !== 'CANCEL'))
          setTotal(res.body.data.total)
        },
        error(err) {
          checkError(err)
          setLoading(false)
        },
        complete() {
          setLoading(false)
        },
      })
    }

    useEffect(() => {
      if (selectedRows.length && !selectedTotalLock) {
        setShowSelectedTotal(true)
      }
    }, [selectedRows])

    useEffect(() => {
      fetchData()
    }, [query])

    const columns = [
      {
        title: 'ID #',
        width: 120,
        render: (r) => <span># {r.wholesaleOrderId}</span>,
      },
      {
        title: 'DATE PLACED',
        dataIndex: 'updatedDate',
        key: 'updatedDate',
        render: (value: string, record: any) => moment(value).format('MM/D/YY'),
        sorter: true,
      },
      {
        title: 'DELIVERY DATE',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        render: (value: string, record: any) => moment(value).format('MM/D/YY'),
        sorter: true,
      },

      {
        title: 'ORDER TOTAL',
        dataIndex: 'totalCostPlusTax',
        key: 'totalCostPlusTax',
        width: 80,
        render: (value: number, record: any) => {
          return `$${formatNumber(value, 2)}`
        },
        sorter: true,
      },
      {
        title: 'STATUS',
        dataIndex: 'wholesaleOrderStatus',
        key: 'wholesaleOrderStatus',
        render: (value: any) => formatOrderStatus(value, sellerSetting),
        sorter: true,
      },
      {
        title: 'COMPANY',
        width: 300,
        key: 'wholesaleClient.clientCompany.companyName',
        render: (r) => (
          <Link to={`/customer/${r.clientId}/orders`}>
            <span style={{ textDecoration: 'underline', color: 'rgb(40, 128, 185)' }}>
              {r.companyName}
            </span>
          </Link>
        ),
        sorter: true,
      },
      {
        title: 'BUSINESS TYPE',
        dataIndex: 'type',
        key: 'type',
        sorter: true,
      },
    ]

    const handleChangeTable = (pagination: any, _filters: any, sorter: any) => {
      if (sorter.order) {
        setQuery({
          ...query,
          page: pagination.current - 1,
          orderBy: sorter.columnKey,
          direction: ANTD_ORDER_CONVER[sorter.order],
        })
      } else {
        setQuery({ ...defultQuery, page: pagination.current - 1 })
      }
    }

    const rowSelection = {
      onSelect: (record: any, selected: boolean) => {
        if (selected) {
          setSelectedRows([...selectedRows, record])
        } else {
          setSelectedRows(selectedRows.filter((v) => v.wholesaleOrderId !== record.wholesaleOrderId))
        }
      },
      onSelectAll: (selected: any, selectedRow: any, changeRows: _.List<any> | null | undefined) => {
        if (selected) {
          setSelectedRows(_.uniqBy([...selectedRows, ...selectedRow], 'wholesaleOrderId'))
        } else {
          setSelectedRows(_.xorBy(selectedRows, changeRows, 'wholesaleOrderId'))
        }
      },
      selectedRowKeys: selectedRows.map((v: any) => v.wholesaleOrderId),
    }

    const handlePrint = () => {
      setPrintButtonLoading(true)
      OrderService.instance.getSalesOrderBatchPick(selectedRows.map(v => v.wholesaleOrderId).join(',')).subscribe({
        next(res: any) {
          of(responseHandler(res, false).body.data)

          setOrderTotal(Object.keys(_.groupBy(res.body.data, 'wholesaleOrderId')).length)
          setItemTotal(res.body.data.length)
          setPrintData(Object.values(_.groupBy(res.body.data.map(v => ({...v, inventoryUOM: v.wholesaleItem.inventoryUOM, wholesaleProductUomList: v.wholesaleItem.wholesaleProductUomList})), v => v.wholesaleItem.wholesaleItemId)))
        },
        error(err) {
          checkError(err)
          setPrintButtonLoading(false)
        },
        complete() {
          setPrintButtonLoading(false)
          handleShowPrintModal()
        },
      })
    }

    const handleShowPrintModal = () => {
      const style = document.createElement('style')
      var mywindow = window.open('', 'PRINT', 'height=1080')
      if (!mywindow) return
      mywindow!.document.write('<html><head>')
      mywindow!.document.write('</head><body >')
      mywindow!.document.write(document.getElementById('content')!.innerHTML)

      mywindow!.document.write('</body></html>')

      mywindow!.document.close() // necessary for IE >= 10
      copyStyles(document, mywindow!.document)

      style.type = 'text/css'
      style.media = 'print'

      const css = `
      #page-header, .page-header-space {
        height: 50px;
      }

      #page-footer, .page-footer-space {
        height: 50px;
      }

      #page-footer {
        position: fixed;
        bottom: 0;
        left: 2mm;
        width: 100%;
        height: 50px;
        font-size: 8pt;
        color: #000;
      }

      .page-footer:after {
        counter-increment: page;
        content: counter(page)
      }

      #page-header {
        position: fixed;
        top: 0mm;
        width: 100%;
      }

      @page {
        size: 11in 8.5in;
      }

      @media print {
         thead {display: table-header-group;}
         tfoot {display: table-footer-group;}
         button {display: none;}
         body {margin: 0;}
      }

      #headerLabel {
        position: fixed;
        width: 100%;
        display: flex;
        justify-content: space-between;
        border-bottom: 8px solid #000;
        font-size: 22px;
        font-weight: bold;
        color: #000;
        top: 0;
      }
      `

      if (style.styleSheet) {
        style.styleSheet.cssText = css
      } else {
        style.appendChild(mywindow!.document.createTextNode(css))
      }

      mywindow!.document.head.appendChild(style)

      mywindow!.focus() // necessary for IE >= 10*/

      mywindow!.print()
      mywindow!.onafterprint = function() {
        mywindow!.close()
      }
    }

    return (
      <Modal
        width={1080}
        visible
        onCancel={onCancel}
        closable={false}
        footer={false}
        title={
          <Flex className="space-between">
            <BatchPickTitle>Print Batch Pick Sheet</BatchPickTitle>
            <ThemeButton disabled={!selectedRows.length} loading={printButtonLoading} onClick={handlePrint}>Print Batch Pick Sheet</ThemeButton>
          </Flex>
        }
      >
        {showSelectedTotal && !selectedTotalLock && <SelectedTotal><span>All {selectedRows.length} sales orders selected</span><Icon onClick={() => setSelectedTotalLock(true)} type="close" /></SelectedTotal>}
        <Table
          rowSelection={{
            type: 'checkbox',
            ...rowSelection,
          }}
          onChange={handleChangeTable}
          pagination={{
            total,
            pageSize: query.pageSize,
          }}
          loading={loading}
          columns={columns}
          dataSource={dataSource}
          rowKey="wholesaleOrderId"
        />
        <div id="content" style={{display: 'none'}}>
        <div id='page-header'>
          <div id="headerLabel">
            <div>Batch Pick Sheet</div>
            <div>{orderTotal} Orders, {itemTotal} Items</div>
          </div>
        </div>
        <div id="page-footer">
          <div>Printed at {moment().format('DD/MM/YY h:mm A')}</div>
        </div>
        <table>
          <thead>
            <tr>
              <td>
                <div className="page-header-space"></div>
              </td>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td>
              {printData.map((v: any) => {
                  return (
                  <FixedRatioItem>
                    <VaribleRatioItemTable title={v[0].wholesaleItem.variety} sku={v[0].wholesaleItem.sku} dataSource={v} />
                  </FixedRatioItem>
                )
              })}
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td>
                <div className="page-footer-space"></div>
              </td>
            </tr>
          </tfoot>
        </table>
        </div>
      </Modal>
    )
  }

  const VaribleRatioItemTable: FC<any> = ({ dataSource, title, sku }) => {
    const itemData = _.flattenDeep(dataSource.map((v: any) => {
      if (!judgeConstantRatio(v)) {
        return [{...v, noBorder: true }, { ...v, isNotRatio: true, picked: 0, quantity: 0 }]
      }
      return v
    }))

    const lotTotal = Object.keys(_.groupBy(dataSource, 'lotId')).length
    const locationTotal = _.uniq(_.flattenDeep(_.compact(dataSource.map((v: any) => v.locationList)))).length
    const pickedTotal = dataSource.map((v: any) => v.picked).reduce((p: number, n: number) => p + n, 0)
    const orderedTotal = dataSource.map((v: any) => v.quantity).reduce((p: number, n: number) => p + n, 0)

    const renderContent = (value: any, row: any) => {
      const obj = {
        children: value,
        props: {},
      }
      if (row.isNotRatio) {
        obj.props.colSpan = 0
      }
      return obj
    }
    const columns = [
      {
        title: `Lot(${lotTotal})`,
        width: 145,
        dataIndex: 'lotId',
        render: (text: any, row: any) => {
          const obj = {
            children: text,
            props: {},
          }
          if (row.isNotRatio) {
            return {
              children: '',
              props: {
                colSpan: 1,
              },
            }
          }
          return obj
        },
      },
      {
        title: `Location(${locationTotal})`,
        width: 175,
        dataIndex: 'location',
        render: (text, row, index) => {
          const obj = {
            children: text,
            props: {},
          }
          if (row.isNotRatio) {
            return {
              children: (
                <div className="totalWeight">
                  <div>
                    <span>{row.wholesaleQuantityDetailList.map(v => v.unitWeight).reduce((p, n) => p + n, 0)} {row.pricingUOM}</span>
                    <div className="label">Total Weight</div>
                  </div>
                  <div style={{ display: 'flex', flexWrap: 'wrap', height: '100%' }}>
                    {row.wholesaleQuantityDetailList.map((v, index) => (
                      <div className="item" key={index}>
                        {v.unitWeight ? <i>{v.unitWeight} {row.pricingUOM}</i> : <i />}
                        <span>{v.displayNumber}</span>
                      </div>
                    ))}
                  </div>
                </div>
              ),
              props: {
                colSpan: 6,
              },
            }
          } else {
            return _.get(row, 'locationList', []).join(', ')
          }
        },
      },
      {
        title: `Picked(${pickedTotal})`,
        width: 125,
        dataIndex: 'picked',
        render: (value: any, row: any) => {
          const obj = {
            children: `${value} ${row.overrideUOM || row.UOM}`,
            props: {},
          }
          if (row.isNotRatio) {
            obj.props.colSpan = 0
          }
          return obj
        },
      },
      {
        title: `Ordered(${orderedTotal})`,
        width: 125,
        dataIndex: 'quantity',
        render: (value: any, row: any) => {
          const obj = {
            children: `${value} ${row.overrideUOM || row.UOM}`,
            props: {},
          }
          if (row.isNotRatio) {
            obj.props.colSpan = 0
          }
          return obj
        },
      },
      {
        title: 'Customer',
        width: 200,
        dataIndex: 'companyName',
        render: renderContent,
      },
      {
        title: 'Order No.',
        width: 120,
        dataIndex: 'wholesaleOrderId',
        render: (value, row) => {
          const obj = {
            children: `#${value}`,
            props: {},
          }
          if (row.isNotRatio) {
            obj.props.colSpan = 0
          }
          return obj
        },
      },
      {
        title: 'Route',
        dataIndex: 'route',
        render: (value, row) => {
          const obj = {
            children: `${row.overrideRouteName || row.defaultRouteName || ''}`,
            props: {},
          }
          if (row.isNotRatio) {
            obj.props.colSpan = 0
          }
          return obj
        },
      },
    ]
    return (
      <Table  rowClassName={(record) => record.noBorder ? 'noBorder' : ''} title={() => <Flex className="space-between"><span>{title}</span><span>{sku}</span></Flex>} size="small" columns={columns} dataSource={itemData} pagination={false} />
    )
  }

export default BatchPickModal

const FixedRatioItem = styled.div({
marginTop: '20px',
td: {
    padding: '2px 0px !important',
},
th: {
    padding: '2px 0px !important',
    fontSize: '14px',
    fontWeight: 'bold'
},
'.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-thead .ant-table-column-title': {
    fontSize: '14px',
    fontWeight: 'bold',
},
'.ant-table-small > .ant-table-title, .ant-table-small > .ant-table-content > .ant-table-footer': {
    padding: 0,
},
'.ant-table-small > .ant-table-content > .ant-table-body' {
    margin: 0
},
'.ant-table-tbody > tr > td': {
    borderBottom: '1px solid #000',
},
'.ant-table-title': {
    color: '#000',
    fontSize: '18px',
    fontWeight: 800,
    borderBottom: '3px solid #000',
},
'.ant-table-small': {
    border: '1px solid #ffffff',
    borderRadius: '4px',
},
'.totalWeight': {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    border: '1px dashed #000',
    padding: '0 0 0 14px',
    marginLeft: '-14px',
    height: '116px'
},
'.label': {
    width: '134px',
    marginRight: '40px',
    borderTop: '1px solid #000',
},
'.item': {
    display: 'flex',
    flexDirection: 'column',
    minWidth: '56px',
    marginRight: '14px',
    marginTop: '14px',
    marginBottom: '5px',
    i: {
    fontStyle: 'normal',
    height: '20.8px',
    display: 'inline-block',
    },
    span: {
    borderTop: '1px solid #000',
    fontWeight: 'normal',
    fontSize: '10px',
    },
},
'.noBorder': {
    td: {
			borderBottom: '1px solid #fff !important',
			},
    }
})

const SelectedTotal = styled.div`
background: #0d4b1c;
height: 56px;
line-height: 56px;
font-size: 16px;
color: #fff;
padding-left: 16px;
display: flex;
justify-content: space-between;
align-items: center;
i {
    margin-right: 16px ;
}
`

const BatchPickTitle = styled.h2`
margin-bottom: 0;
line-height: 32px;
`
