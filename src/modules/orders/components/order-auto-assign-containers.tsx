import styled from '@emotion/styled'
import { Checkbox, DatePicker, Icon, Input, notification, Row, Select, Table } from 'antd'
import _, { orderBy } from 'lodash'
import moment from 'moment'
import React, { CSSProperties, FC, useEffect, useState } from 'react'
import { checkError, responseHandler } from '~/common/utils'
import { ThemeButton, ThemeCheckbox, ThemeModal, ThemeSelect, ThemeSpin, ThemeTable } from '~/modules/customers/customers.style'
import { OrderService } from '../order.service'

const { RangePicker } = DatePicker
const { Option } = Select

const plainOptions = [
  { label: 'Split by commodity classes', value: '1' },
  { label: 'Do not split these sales orders', value: '2' },
]

const dateFormat = 'MM/DD/YYYY'
const presets = [
  {
    key: 0,
    label: 'Today',
    from: moment().startOf('day').format(dateFormat),
    to: moment().endOf('day').format(dateFormat),
  },
  {
    key: 1,
    label: 'Tomorrow',
    from: moment().add(1, 'days').startOf('day').format(dateFormat),
    to: moment().add(1, 'days').endOf('day').format(dateFormat),
  },
  {
    key: 2,
    label: 'Today and Tomorrow',
    from: moment().startOf('day').format(dateFormat),
    to: moment().add(1, 'days').endOf('day').format(dateFormat),
  },
  {
    key: 3,
    label: 'Next 7 Days',
    from: moment().startOf('day').format(dateFormat),
    to: moment().add(7, 'days').endOf('day').format(dateFormat),
  },
  {
    key: 4,
    label: 'Next 30 Days',
    from: moment().startOf('day').format(dateFormat),
    to: moment().add(30, 'days').endOf('day').format(dateFormat),
  },
  {
    key: 5,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days').startOf('day').format(dateFormat),
    to: moment().add(30, 'days').endOf('day').format(dateFormat),
  },
  {
    key: 6,
    label: 'Custom date range',
  },
]

const AutoAssignContainerModal: FC<any> = ({
  onCancel,
  simplifyCustomers,
  sellerSetting
}) => {
  const [showCustomDateRange, setshowCustomDateRange] = useState(false)
  const [loading, setLoading] = useState(false)
  const [saveLoading, setSaveLoading] = useState(false)
  const [selectedRows, setSelectedRows] = useState<any>([])
  const [selectedExpandedRowKeys, setSelectedExpandedRowKeys] = useState([])
  const [expandedRowKeys, setExpandedRowKeys] = useState([])
  const [chooseSplit, setChooseSplit] = useState('')
  const [splitOrders, setSplitOrders] = useState([])
  const [data, setData] = useState({})
  const [allDataList, setAllDataList] = useState([])
  const [maxWeight, setMaxWeight] = useState('')
  const [maxVolume, setMaxVolume] = useState('')
  const [search, setSearch] = useState('')
  const [query, setQuery] = useState({
    clientId: '',
    commodityClass: '',
    fromDate: presets[3].from,
    ftoDate: presets[3].to,
  })

  useEffect(() => {
    setLoading(true)
    OrderService.instance.getAutoContainer({ ...query, search}).subscribe({
      next(resp) {
        const groupByClient = _.groupBy(resp.body.data, 'wholesaleOrderId')
        const entities = {}

        Object.keys(groupByClient).forEach(wholesaleOrderId => {
          const items = _.groupBy(groupByClient[wholesaleOrderId].map(v => ({ ...v, itemId: `${v.wholesaleOrderId}-${v.wholesaleItemId}` })), 'itemId')

          entities[wholesaleOrderId] = {}
          entities[wholesaleOrderId].companyName = groupByClient[wholesaleOrderId][0].companyName
          entities[wholesaleOrderId].commodityClass = groupByClient[wholesaleOrderId].map(v => v.commodityClass)
          entities[wholesaleOrderId].itemList = Object.keys(items).map(v => ({
            list: items[v],
            itemName: items[v][0].itemName,
            itemId: items[v][0].itemId,
            allItemIdList: Object.keys(items),
            wholesaleOrderId,
            commodityClass: items[v][0].commodityClass,
            totalWeight: _.reduce(
              items[v],
              (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight),
              0,
            ),
            totalVolume: _.reduce(
              items[v],
              (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume),
              0,
            ),
            containerName: items[v].map(v => v.containerName)
          }))
          entities[wholesaleOrderId].list = groupByClient[wholesaleOrderId].map(i => ({ ...i, entityOrderIdList: groupByClient[wholesaleOrderId].map(v => v.wholesaleOrderItemId) }))
          entities[wholesaleOrderId].totalWeight = _.reduce(
            groupByClient[wholesaleOrderId],
            (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight),
            0,
          )
          entities[wholesaleOrderId].totalVolume = _.reduce(
            groupByClient[wholesaleOrderId],
            (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume),
            0,
          )
          entities[wholesaleOrderId].wholesaleOrderId = wholesaleOrderId
        })
        setAllDataList(resp.body.data ? resp.body.data.map(v => ({ ...v, itemId: `${v.wholesaleOrderId}-${v.wholesaleItemId}` })) : [])
        setData(entities)
      },
      error(err) { checkError(err) },
      complete() {
        setLoading(false)
      }
    })
  }, [query, search])

  const columns = [
    {
      title: 'Customer',
      width: 200,
      dataIndex: 'companyName',
    },
    {
      title: 'Order No',
      width: 100,
      dataIndex: 'wholesaleOrderId'
    },
    {
      title: 'Fulfillment date',
      width: 130,
      dataIndex: 'deliveryDate'
    },
    {
      title: 'Class',
      width: 100,
      dataIndex: 'commodityClass',
      render: (t, r) => r.list.length > 1 ? 'Many' : t
    },
    {
      title: 'Vol.',
      width: 80,
      dataIndex: 'totalVolume',
    },
    {
      title: 'Gr. Weight',
      width: 100,
      dataIndex: 'totalWeight',
    },
    {
      title: 'Container',
      width: 100,
      render: () => '-'
    },
  ]

  const expandedColumns = [
    {
      title: 'Customer',
      width: 200,
      dataIndex: 'itemName',
    },
    {
      title: 'Order No',
      width: 100,
      dataIndex: 'wholesaleOrderId'
    },
    {
      title: 'Fulfillment date',
      width: 130,
      dataIndex: 'deliveryDate'
    },
    {
      title: 'Class',
      width: 100,
      dataIndex: 'commodityClass'
    },
    {
      title: 'Vol.',
      width: 80,
      dataIndex: 'totalVolume',
    },
    {
      title: 'Gr. Weight',
      width: 100,
      dataIndex: 'totalWeight',
    },
    {
      title: 'Container',
      width: 100,
      dataIndex: 'containerName',
      render: (t) => _.compact(t).join(', ')
    },
  ]

  const handleChangeDateFilter = (key: number) => {
    if (key === 6) {
      return setshowCustomDateRange(true)
    }
    setshowCustomDateRange(false)
    return setQuery({
      ...query,
      fromDate: presets[key].from,
      ftoDate: presets[key].to,
    })
  }

  const handleChangeCustomer = (clientId) => {
    setQuery({ ...query, clientId })
  }

  const handleCommodityClass = (commodityClass) => {
    setQuery({ ...query, commodityClass })
  }

  const handleChangeSearch = React.useRef(_.debounce((search) => {
    setSearch(search)
  }, 500)).current

  const rowSelection = {
    onSelect: (record: any, selected: boolean) => {
      if (selected) {
        setSelectedRows([...selectedRows, record])
        setSelectedExpandedRowKeys([...selectedExpandedRowKeys, ...record.itemList.map(v => v.itemId)])
      } else {
        setSelectedRows(selectedRows.filter((v) => v.wholesaleOrderId !== record.wholesaleOrderId))
        setSelectedExpandedRowKeys(selectedExpandedRowKeys.filter(v => !record.itemList.map(r => r.itemId).includes(v)))
      }
    },
    onSelectAll: (selected: any, selectedRow: any, changeRows: _.List<any> | null | undefined) => {
      if (selected) {
        setSelectedRows(_.uniqBy([...selectedRows, ...selectedRow], 'wholesaleOrderId'))
        setSelectedExpandedRowKeys(allDataList.map(v => v.itemId))
      } else {
        setSelectedRows(_.xorBy(selectedRows, changeRows, 'wholesaleOrderId'))
        setSelectedExpandedRowKeys([])
      }
    },
    selectedRowKeys: selectedRows.map((v: any) => v.wholesaleOrderId),
  }

  const expandedRowSelection = {
    onSelect: (record: any, selected: boolean) => {
      if (selected) {
        if (record.allItemIdList.length === _.intersection(record.allItemIdList, [...selectedExpandedRowKeys, record.itemId]).length) {
          setSelectedRows([...selectedRows, data[record.wholesaleOrderId]])
        }
        setSelectedExpandedRowKeys([...selectedExpandedRowKeys, record.itemId])
      } else {
        if (record.allItemIdList.length !== _.intersection(record.allItemIdList, selectedExpandedRowKeys.filter(key => key !== record.itemId)).length) {
          setSelectedRows(selectedRows.filter(v => v.wholesaleOrderId !== record.wholesaleOrderId))
        }
        setSelectedExpandedRowKeys(selectedExpandedRowKeys.filter((key) => key !== record.itemId))
      }
    },
    selectedRowKeys: selectedExpandedRowKeys,
  }

  const expandedRowRender = (record: { list: unknown[] | undefined }) => {
    return <ExpandedRowTable columns={expandedColumns} dataSource={Object.values(record.itemList)} pagination={false} rowSelection={expandedRowSelection} rowKey='itemId' />
  }

  const onExpand = ({ expanded, record }: { expanded: boolean; record: any }): void => {
    if (expanded) {
      setExpandedRowKeys(expandedRowKeys.filter(id => id !== record.wholesaleOrderId))
    } else {
      setExpandedRowKeys([...expandedRowKeys, record.wholesaleOrderId])
    }
  }

  const getContainers = () => {
    const currentSelectedOrderItems = _.groupBy(allDataList, 'itemId')
    const currentOrderItems = _.flatten(selectedExpandedRowKeys.map(v => currentSelectedOrderItems[v]))
    
    let containers: any = []
    let containerCount = 0
    
    const totalVol = _.reduce(
      currentOrderItems,
      (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume),
      0,
    )
    const totalWeight = _.reduce(
      currentOrderItems,
      (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight),
      0,
    )

    if (!maxVolume || !maxWeight) {
      return {
        totalVol,
        totalWeight,
        containerCount,
        containers
      }
    }

    if (chooseSplit === '') {
      containerCount =  _.divide(totalVol, Number(maxVolume)) > _.divide(totalWeight, Number(maxWeight)) ? Math.ceil(_.divide(totalVol, Number(maxVolume))) : Math.ceil(_.divide(totalWeight, Number(maxWeight)))
      containers = _.range(containerCount || 1).map(v => ({
        maxWeight,
        maxVolume,
        orderItemList: []
      }))
      if (_.divide(totalVol, Number(maxVolume)) > _.divide(totalWeight, Number(maxWeight))) {
        let vol = 0
        currentOrderItems.forEach((v: any) => {
          vol += _.multiply(v.catchWeightQty, v.grossVolume)
          if (Math.floor(_.divide(vol, Number(maxVolume))) >= containerCount) return 
          containers[Math.floor(_.divide(vol, Number(maxVolume)))].orderItemList.push({
            wholesaleOrderId: v.wholesaleOrderId,
            wholesaleOrderItemId: v.wholesaleOrderItemId,
            quantity: v.quantity,
            uom: v.UOM
          })
        })
      } else {
        let wei = 0
        currentOrderItems.forEach((v: any) => {
          wei += _.multiply(v.catchWeightQty, v.grossWeight)
          if (Math.floor(_.divide(wei, Number(maxWeight))) >= containerCount) return 
          containers[Math.floor(_.divide(wei, Number(maxWeight)))].orderItemList.push({
            wholesaleOrderId: v.wholesaleOrderId,
            wholesaleOrderItemId: v.wholesaleOrderItemId,
            quantity: v.quantity,
            uom: v.UOM
          })
        })
      }
    }

    if (chooseSplit === '1') {
      const groupByItems = _.groupBy(currentOrderItems, 'commodityClass') // 根据class 分组
      containers = _.range(Object.keys(groupByItems).length) // 创建class类的空箱子

      // 遍历 class 
      Object.values(groupByItems).forEach((items, index) => {
        // 单个class 总vol
        const allClassItemvol = _.reduce(
          items,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume),
          0,
        )
        // 单个class 总wei
        const allClassItemWei = _.reduce(
          items,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight),
          0,
        )
        
        // vol分箱要比wei 多的情况
        if (_.divide(allClassItemvol, Number(maxVolume)) > _.divide(allClassItemWei, Number(maxWeight))) {
          // 创建一个多维数组箱子 箱子结构
          containers[index] = _.range(Math.floor(_.divide(allClassItemvol, Number(maxVolume))) || 1).map(v => ({
            maxWeight,
            maxVolume,
            orderItemList: []
          }))
          let vol = 0
          // 遍历class items
          items.forEach((v: any) => {
            vol += _.multiply(v.catchWeightQty, v.grossVolume)
            if (Math.floor(_.divide(vol, Number(maxVolume))) >= containers[index].length) return 
            // 空箱子的[第i个class集合][第vol/maxVolume] 的orderItemlist push
            containers[index][Math.floor(_.divide(vol, Number(maxVolume)))].orderItemList.push({
              wholesaleOrderId: v.wholesaleOrderId,
              wholesaleOrderItemId: v.wholesaleOrderItemId,
              quantity: v.quantity,
              uom: v.UOM
            })
          })
          containerCount = _.flatten(containers).length
          containers = _.flatten(containers)
        } else {
          containers[index] = _.range(Math.ceil(_.divide(allClassItemWei, Number(maxWeight))) || 1).map(v => ({
            maxWeight,
            maxVolume,
            orderItemList: []
          }))
          let wei = 0
          // 遍历class items
          items.forEach((v: any) => {
            wei += _.multiply(v.catchWeightQty, v.grossWeight)
            if (Math.floor(_.divide(wei, Number(maxWeight))) >= containers[index].length) return 
            // 空箱子的[第i个class集合][第vol/maxVolume] 的orderItemlist push
            containers[index][Math.floor(_.divide(wei, Number(maxWeight)))].orderItemList.push({
              wholesaleOrderId: v.wholesaleOrderId,
              wholesaleOrderItemId: v.wholesaleOrderItemId,
              quantity: v.quantity,
              uom: v.UOM
            })
          })
          containerCount = _.flatten(containers).length
          containers = _.flatten(containers)
        }
      })
    }

    if (chooseSplit === '2') { 
      const byOrderList = splitOrders.map(v => data[v].list)
      containers = _.range(byOrderList.length) // 创建class类的空箱子

      // 遍历 class 
      Object.values(byOrderList).forEach((items, index) => {
        // 单个class 总vol
        const allClassItemvol = _.reduce(
          items,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume),
          0,
        )
        // 单个class 总wei
        const allClassItemWei = _.reduce(
          items,
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight),
          0,
        )
        
        // vol分箱要比wei 多的情况
        if (_.divide(allClassItemvol, Number(maxVolume)) > _.divide(allClassItemWei, Number(maxWeight))) {
          // 创建一个多维数组箱子 箱子结构
          containers[index] = _.range(Math.floor(_.divide(allClassItemvol, Number(maxVolume))) || 1).map(v => ({
            maxWeight,
            maxVolume,
            orderItemList: []
          }))
          let vol = 0
          // 遍历class items
          items.forEach((v: any) => {
            vol += _.multiply(v.catchWeightQty, v.grossVolume)
            if (Math.floor(_.divide(vol, Number(maxVolume))) >= containers[index].length) return 
            // 空箱子的[第i个class集合][第vol/maxVolume] 的orderItemlist push
            containers[index][Math.floor(_.divide(vol, Number(maxVolume)))].orderItemList.push({
              wholesaleOrderId: v.wholesaleOrderId,
              wholesaleOrderItemId: v.wholesaleOrderItemId,
              quantity: v.quantity,
              uom: v.UOM
            })
          })
          containerCount = _.flatten(containers).length
          containers = _.flatten(containers)
        } else {
          containers[index] = _.range(Math.ceil(_.divide(allClassItemWei, Number(maxWeight))) || 1).map(v => ({
            maxWeight,
            maxVolume,
            orderItemList: []
          }))
          let wei = 0
          // 遍历class items
          items.forEach((v: any) => {
            wei += _.multiply(v.catchWeightQty, v.grossWeight)
            if (Math.floor(_.divide(wei, Number(maxWeight))) >= containers[index].length) return 
            // 空箱子的[第i个class集合][第vol/maxVolume] 的orderItemlist push
            containers[index][Math.floor(_.divide(wei, Number(maxWeight)))].orderItemList.push({
              wholesaleOrderId: v.wholesaleOrderId,
              wholesaleOrderItemId: v.wholesaleOrderItemId,
              quantity: v.quantity,
              uom: v.UOM
            })
          })
          containerCount = _.flatten(containers).length
          containers = _.flatten(containers)
        }
      })
    }

    return {
      items: currentOrderItems,
      totalVol,
      totalWeight,
      containerCount,
      containers
    }
  }

  const handleSave = () => {
    setSaveLoading(true)
    const { containers } = getContainers()

    const data = containers.map((v, i) => ({
      ...v,
      containerName: `Container ${i + 1}`,
      company: sellerSetting.company.source
    }))
    OrderService.instance.saveAutoAssignContainer(data).subscribe({
      next(resp) {
        if (resp.message === 'Successfully get order item list') {
          notification.success({
            message: 'Container(s) successfully created.',
            onClose: () => {},
          })
        } else {
          notification.error({
            message: 'Unable to create containers. Please try again.',
            onClose: () => {},
          })
        }
        onCancel() 
      },
      error(err) { checkError(err) },
      complete() {
        setSaveLoading(false)
      }
    })
  }

  return (
    <WrapModal
      title='Auto-Assign Items to Container(s)'
      onCancel={onCancel}
      width={1000}
      visible
      footer={[
        <div>
          <ThemeButton key={1} onClick={handleSave} loading={saveLoading}>
            Auto-assign items to containers
          </ThemeButton>
          <span key={2} style={{ marginLeft: 20 }} onClick={onCancel}>Cancel</span>
        </div>,
        <div>{getContainers().containerCount} New Open Containers Will Be Created</div>
      ]}
    >
      <ThemeSpin spinning={loading}>
        <Header>
          <div style={{ marginRight: 20 }}>
            <p>Customer</p>
            <ThemeSelect
              showSearch
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              style={ThemeSelectStyle()}
              onChange={handleChangeCustomer}
              placeholder={'Select'}
              defaultValue={''}
            >
              <Option value={''}>
                All
              </Option>
              {simplifyCustomers.map((v) => (
                <Option key={v.clientId} value={v.clientId}>
                  {v.clientCompanyName}
                </Option>
              ))}
            </ThemeSelect>
          </div>
          <div style={{ marginRight: 20 }}>
            <p>Commodity class</p>
            <ThemeSelect defaultValue={query.commodityClass} style={ThemeSelectStyle()} onChange={handleCommodityClass}>
              <Option key={1} value={''}>
                All
              </Option>
            </ThemeSelect>
          </div>
          <div style={{ marginRight: 20 }}>
            <p>Fulfillment Date</p>
            <Row type="flex">
              <ThemeSelect defaultValue={3} style={ThemeSelectStyle()} onChange={handleChangeDateFilter}>
                {presets.map((v) => (
                  <Option key={v.key} value={v.key}>
                    {v.label}
                  </Option>
                ))}
              </ThemeSelect>
            </Row>
          </div>
          <div style={{ marginRight: 20 }}>
            <Input placeholder='Search Customer, Items' onChange={e => handleChangeSearch(e.target.value)} />
          </div>
        </Header>
        {showCustomDateRange && (
          <RangePicker
            placeholder={['From', 'To']}
            style={{ marginTop: 10, marginLeft: 340 }}
            format={dateFormat}
            allowClear={false}
            onChange={(_, dateStrings) => {
              setQuery({ ...query, fromDate: moment(dateStrings[0]).startOf('day').format(dateFormat), ftoDate: moment(dateStrings[1]).endOf('day').format(dateFormat) })
            }}
          />
        )}
        <WrapTable
          columns={columns}
          footer={() => (
            <div>
              <span>{selectedExpandedRowKeys.length} Items Selected</span>
              <span>{getContainers().totalVol} Cubes</span>
              <span>{getContainers().totalWeight} LBs</span>
            </div>
          )}
          rowSelection={rowSelection}
          pagination={false}
          expandIconAsCell={false}
          expandIconColumnIndex={0}
          expandIcon={({ expanded, record }) => (
            <Icon type={`caret-${expanded ? 'down' : 'right'}`} onClick={() => onExpand({ expanded, record })} />
          )}
          expandedRowKeys={expandedRowKeys}
          dataSource={Object.values(data)}
          expandedRowRender={expandedRowRender}
          rowKey='wholesaleOrderId'
        />
        <Footer>
          <div className='parameters'>
            <p>1. Set container split parameters</p>
            <div>
              <span>Maximum Weight (lbs) per Container</span>
              <Input value={maxWeight} onChange={e => setMaxWeight(e.target.value)} />
            </div>
            <div>
              <span>Maximum Volume (ft³) per Container</span>
              <Input value={maxVolume} onChange={e => setMaxVolume(e.target.value)} />
            </div>
          </div>
          <div className='options'>
            <p>2. Choose split options</p>
            <Checkbox checked={chooseSplit === '1'} onChange={(e) => e.target.checked ? setChooseSplit('1') : setChooseSplit('')}>Split by commodity classes</Checkbox>
            <div className='selectedOrder'>
              <Checkbox checked={chooseSplit === '2'} onChange={(e) => e.target.checked ? setChooseSplit('2') : setChooseSplit('')}>Do not split these sales orders</Checkbox>
              <ThemeSelect
                style={{ width: 250 }}
                placeholder='Select Sales Orders'
                dropdownRender={() => {
                  return (
                    <>
                      <div />
                      {_.uniq(selectedExpandedRowKeys.map(itemId => itemId && itemId.split('-')[0])).map((orderId) => (
                        <div
                          key={orderId}
                          style={{ padding: '4px 8px', cursor: 'pointer' }}
                          onMouseDown={e => e.preventDefault()}
                        >
                          <ThemeCheckbox 
                            style={{ marginRight: 10 }}
                            onChange={e => e.target.checked ? setSplitOrders([...splitOrders, orderId]) : setSplitOrders(splitOrders.filter(id => id !== orderId))}
                          /> 
                          S{orderId} - {data[orderId].companyName}
                        </div>
                      ))} 
                    </>
                  )
                }}
              />
            </div>
          </div>
          <div>

          </div>
        </Footer>
      </ThemeSpin>
    </WrapModal>
  )
}

const WrapModal = styled(ThemeModal)`
  .ant-modal-footer {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`

const Header = styled.div`
  display: flex;
  align-items: end;
`
const ExpandedRowTable = styled(Table)`
  .ant-table-selection-column {
    padding-left: 40px;
  }
  .ant-table-thead {
    display: none;
  }
`
const Footer = styled.div`
  margin-top: 20px;
  display: flex;
  span {
    font-weight: normal;
  }
  .parameters {
    width: 268px;
  }
  .options {
    display: flex;
    flex-direction: column;
    margin-left: 40px;
    .ant-checkbox-wrapper {
      margin-bottom: 20px;
    }
    .ant-checkbox-wrapper + .ant-checkbox-wrapper {
      margin-left: 0;
    }
    .selectedOrder {
      display: flex;
      
    }
  }

`
const WrapTable = styled(Table)`
  margin-top: 10px;
  thead {
    .ant-table-selection-column {
      padding-left: 40px;
    }
  } 
  .ant-table-selection-column {
    align-items: center;
    i {
      margin-right: 10px;
    }
  }
  .ant-table-footer {
    text-align: right;
    font-weight: bold;
    span {
      margin: 0 10px;
    }
  }
`

const ThemeSelectStyle = (): CSSProperties => ({
  width: '150px',
  fontWeight: 500
})

export default AutoAssignContainerModal

