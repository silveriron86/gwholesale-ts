import { Avatar, Modal, Table } from 'antd'
import React, { FC, useEffect, useState } from 'react'
import { Input } from 'antd'
import { v4 as uuidv4 } from 'uuid'
const { Search } = Input
import _ from 'lodash'
import { EmptyResult } from '~/modules/customers/nav-sales/styles'
import { Icon as IconSVG } from '~/components/icon'
import { Flex, ThemeButton } from '~/modules/customers/customers.style'
import { gray01 } from "~/common";

const CustomerItemsModal: FC<any> = ({ onCancel, customerItems, handleAddPriceSheetItem }) => {
  const [data, setData] = useState(customerItems)
  const [searchValue, setSearchValue] = useState('')

  const columns = [
    {
      title: 'NAME',
      dataIndex: 'variety',
      render: (t, r) => {
        return (
          <Flex className="v-center">
            <Avatar src={r.cover} style={{ marginRight: 10 }} />
            <span>{t}</span>
          </Flex>
        )
      },
    },
    {
      title: 'SKU',
      dataIndex: 'SKU',
    },
  ]

  const handleSearch = (text: string) => {
    setSearchValue(text)
    setData(
      _.filter(customerItems, (v) => {
        if (v.variety) {
          const variety = v.variety.toLowerCase()
          if (variety.includes(_.trim(text.toLowerCase()))) {
            return v
          }
          return
        }
        return
      }),
    )
  }

  const handleAddToOrder = () => {
    handleAddPriceSheetItem({
      note: '',
      isRequest: true,
      variety: searchValue,
      itemDesc: searchValue,
      orderQuantity: 1,
      priceSheetItemId: uuidv4(),
      priceSheet: null,
      freight: 0,
      margin: 0,
      markup: 0,
      salePrice: 0,
      createdDate: null,
      updatedDate: null,
      defaultLogic: null,
      defaultGroup: null,
      priceCustomize: false,
      modifier: null,
      isDeleted: false,
      itemId: uuidv4(),
      category: '',
      size: '',
      weight: '',
      grade: '',
      origin: '',
      provider: '',
      packing: '',
      suppliers: null,
      wholesaleItem: {
        sku: '',
        cost: 0,
      },
      wholesaleCategory: {},
    })
    onCancel()
  }

  return (
    <Modal visible onCancel={onCancel} title="Request an item" width={1080} footer={null}>
      <Search placeholder="search" onSearch={handleSearch} style={{ width: 300, marginBottom: 20 }} />
      {data.length ? (
        <Table
          columns={columns}
          dataSource={data}
          onRow={(record: any) => {
            return {
              onClick: (event) => {
                handleAddPriceSheetItem({
                  isSelectedOrder: true,
                  note: '',
                  variety: record.variety,
                  orderQuantity: 1,
                  priceSheetItemId: uuidv4(),
                  priceSheet: null,
                  freight: 0,
                  margin: 0,
                  markup: 0,
                  salePrice: 0,
                  createdDate: null,
                  updatedDate: null,
                  defaultLogic: null,
                  defaultGroup: null,
                  priceCustomize: false,
                  modifier: null,
                  isDeleted: false,
                  itemId: record.wholesaleItemId,
                  category: '',
                  size: '',
                  weight: '',
                  grade: '',
                  origin: '',
                  provider: '',
                  packing: '',
                  suppliers: null,
                  wholesaleItem: {
                    sku: record.SKU,
                    cost: 0,
                  },
                  wholesaleCategory: {},
                })
                onCancel()
              },
            }
          }}
        />
      ) : (
        <EmptyResult>
          <div className="icon">
            <IconSVG type="no-search-result" viewBox="0 0 45 45" width="45" height="45" />
          </div>
          <div style={{ color: gray01 }}>No results for &quot;{searchValue}&quot; found</div>
          <div className="extra-charge">
            <ThemeButton style={{ border: 'none' }} onClick={handleAddToOrder}>
              Add to Order
            </ThemeButton>
          </div>
        </EmptyResult>
      )}
    </Modal>
  )
}

export default CustomerItemsModal
