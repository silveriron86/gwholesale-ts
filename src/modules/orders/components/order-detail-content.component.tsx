/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { ColumnProps } from 'antd/es/table'
import { OrderItem, OrderDetail } from '~/schema'
import { tableCss } from './order-detail.style'
import { MutiSortTable } from '~/components'
import { OrderService } from '../order.service'
import { of } from 'rxjs'
import { checkError, responseHandler } from '~/common/utils'

export interface OrderDetailContentProps {
  order: OrderDetail
  items: OrderItem[]
  hasPadding?: boolean
  editorMode?: boolean
  sellerSetting: any
  getSellerSetting: Function
}

/*
export function columns(): any {
  const dataIndexMap = TITLES.reduce(
    (acc, cur) => {
      if (!acc[cur]) {
        acc[cur] = `${cur}`
      }
      return acc
    },
    {
      sku: 'SKU',
      category: 'category',
      name: 'variety',
      margin: 'margin',
      quantity: 'quantity',
      freight: 'freight',
      price: 'price',
    },
  )
  return TITLES.map((key) => {
    const upper = key.toUpperCase()
    return {
      title: upper,
      key,
      dataIndex: dataIndexMap[key],
      className: key === 'stock' ? columnClass : null,
      render: OrderDetailContent.renderIndexMap[key],
    }
  })
}

*/

export class OrderDetailContent extends React.PureComponent<OrderDetailContentProps> {
  componentDidMount() {
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
  }

  getColumnsByCompanySetting = (columns: any[]) => {
    const { sellerSetting } = this.props
    if (sellerSetting && sellerSetting.company) {
      if (!sellerSetting.company.showPriceSheetSize) {
        const findSizeIndex = columns.findIndex(el => el.key == 'size')
        columns.splice(findSizeIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetWeight) {
        const findWeightIndex = columns.findIndex(el => el.key == 'weight')
        columns.splice(findWeightIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetPacking) {
        const findPackingIndex = columns.findIndex(el => el.key == 'packing')
        columns.splice(findPackingIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetBrand) {
        const findBrandIndex = columns.findIndex(el => el.key == 'modifier')
        columns.splice(findBrandIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetOrigin) {
        const findOriginIndex = columns.findIndex(el => el.key == 'origin')
        columns.splice(findOriginIndex, 1)
      }
    }
    return columns
  }

  private mutiSortTable = React.createRef<any>()

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a ? (a[key] ? a[key] : '') : ''
    const stringB = b ? (b[key] ? b[key] : '') : ''
    return stringA.localeCompare(stringB)
  }

  render() {
    const { order } = this.props
    const columns: ColumnProps<OrderItem>[] = [
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'SKU')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('SKU', b, a)
          return this.onSortString('SKU', a, b)
        },
      },
      {
        title: 'CATEGORY',
        dataIndex: 'category',
        key: 'category',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'category',
          )
          if (b.category) {
            if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('name', b.category, a.category)
            return this.onSortString('name', a.category, b.category)
          } else {
            return false
          }
        },
        render: (t: string, r: any) => {
          if (r.itemId) {
            return t
          } else {
            return r.itemName
          }
        },
      },
      {
        title: 'NAME',
        dataIndex: 'variety',
        key: 'variety',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'variety',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('variety', b, a)
          return this.onSortString('variety', a, b)
        },
        render(name: string, record: any) {
          if (!record.isInventory) {
            if (record.itemId) {
              return <b>{record.itemName}</b>
            } else {
              return <b>{record.chargeDesc}</b>
            }
          } else {
            return <b>{name}</b>
          }
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'size')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('size', b, a)
          return this.onSortString('size', a, b)
        },
      },
      {
        title: 'WEIGHT',
        dataIndex: 'weight',
        key: 'weight',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'weight')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('weight', b, a)
          return this.onSortString('weight', a, b)
        },
      },
      {
        title: 'GRADE',
        dataIndex: 'grade',
        key: 'grade',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'grade')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('grade', b, a)
          return this.onSortString('grade', a, b)
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'packing',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('packing', b, a)
          return this.onSortString('packing', a, b)
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'origin',
        key: 'origin',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'origin')
          if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('origin', b, a)
          return this.onSortString('origin', a, b)
        },
      },
      // {
      //   title: 'PROVIDER',
      //   dataIndex: 'provider',
      //   key: 'provider',
      //   sorter: (a, b) => {
      //     const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
      //       (meta: any) => meta.dataIndex === 'provider',
      //     )
      //     if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('provider', b, a)
      //     return this.onSortString('provider', a, b)
      //   },
      //   render: (provider: string) => {
      //     return provider ? provider : ''
      //   },
      // },
      {
        title: 'QUANTITY',
        dataIndex: 'quantity',
        key: 'quantity',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
            (meta: any) => meta.dataIndex === 'quantity',
          )
          if (sortIndex && sortIndex.sortOrder === 'descend') return b.quantity - a.quantity
          return a.quantity - b.quantity
        },
        render(text) {
          return <b>{text}</b>
        },
      },
      // {
      //   title: 'FREIGHT',
      //   dataIndex: 'freight',
      //   key: 'freight',
      //   sorter: (a, b) => {
      //     const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
      //       (meta: any) => meta.dataIndex === 'freight',
      //     )
      //     if (sortIndex && sortIndex.sortOrder === 'descend') return b.freight - a.freight
      //     return a.freight - b.freight
      //   },
      //   render(rowPrice) {
      //     return `${rowPrice ? '$' + rowPrice : ''}`
      //   },
      // },
      {
        title: 'SALE PRICE',
        dataIndex: 'price',
        key: 'price',
        sorter: (a, b) => {
          const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'price')
          if (sortIndex && sortIndex.sortOrder === 'descend') return b.price - a.price
          return a.price - b.price
        },
        render: (rowPrice, r) => {
          if (!r.itemId) return <b>REQUEST</b>
          if (order.priceSheetItemList.some((v) => v.wholesaleItem.wholesaleItemId === r.itemId)) {
            return (
              <b>
                {!!order?.wholesaleClient?.displayPriceSheetPrice
                  ? rowPrice
                    ? '$' + rowPrice.toFixed(2)
                    : ''
                  : 'Available'}
              </b>
            )
          } else {
            return <b>REQUEST</b>
          }
        },
      },
    ]

    const filteredColumns = this.getColumnsByCompanySetting(columns)

    if (!this.props.items) {
      return 'loading'
    }

    /*
    const components = {
      body: { row: this.row, table: 'table' },
    }
    */
    return (
      <MutiSortTable
        ref={this.mutiSortTable}
        css={tableCss(!!this.props.hasPadding)}
        pagination={false}
        //components={components}
        columns={filteredColumns}
        dataSource={this.props.items}
        bordered={true}
        rowKey="itemId"
        id={'customer-so-table-preview'}
      />
    )
  }
}
