import React from 'react'
import { Global } from '@emotion/core'

import { OrderDetailContent } from './order-detail-content.component'
import { OrderItem, PriceSheetItem, OrderDetail } from '~/schema'
import { Placeholder, globalStyle } from './order-editor.style'

export interface OrderEditorProps {
  order: OrderDetail
  priceSheetItems: PriceSheetItem[]
}

export class OrderEditor extends React.PureComponent<OrderEditorProps> {
  readonly state = {
    items: [
      Object.create({
        addNewItem: true,
        itemId: 'add-new-item',
        kind: Symbol.for('placeholder'),
      }),
      ...Array.from({ length: 20 }, (_, k) =>
        Object.create({
          itemId: `placeholder-${k}`,
          kind: Symbol.for('placeholder'),
        }),
      ),
    ] as OrderItem[],
  }

  render() {
    return (
      <>
        <Global styles={globalStyle} />
        <OrderDetailContent order={this.props.order} items={this.state.items} editorMode={true} />
        {this.renderPlaceholder()}
      </>
    )
  }

  private renderPlaceholder() {
    return <Placeholder>Your order is currently empty</Placeholder>
  }
}
