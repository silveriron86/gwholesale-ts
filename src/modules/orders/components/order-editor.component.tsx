import { Select, InputNumber, Button } from 'antd'
import { ColumnProps } from 'antd/es/table'
import lodash from 'lodash'
import React, { useState, useRef, useEffect } from 'react'
import Highlighter from 'react-highlight-words'
import isHotkey from 'is-hotkey'

import { OrdersDispatchProps } from '../orders.module'
import { MutiSortTable } from '~/components'
import { SaleItem } from '~/schema'
import { highLightStyle, overrideTableCss, SelectOptDiv, SelectOptChild, SelectOptChildTitle } from './order-editor.style'

// search by keywords
function search(keyword: string, datum: SaleItem[]) {
  return datum
    .filter(n => {
      return (n.variety && n.variety.toLowerCase().indexOf(keyword) > -1)
        || (n.provider && n.provider.toLowerCase().indexOf(keyword) > -1)
    })
    // get SIX items
    .slice(0, 6)
}

const isSearchHotkey = isHotkey('Control+i')

const SearchInput: React.SFC<{
  onSearch: (keyword: string) => SaleItem[];
  onChange: (itemId: string) => void
}> = props => {
  // state for search keywords
  const [query, setQuery] = useState<string>()
  // state for dataSource in suggestion menu
  const [dataSource, setDataSource] = useState<SaleItem[]>([])
  // selected items

  function handleSearch(value: string) {
    const datum = props.onSearch(value)
    setDataSource(datum)
  }

  function handleChange(itemId: string) {
    props.onChange(itemId);
    // clear query
    setQuery('');
    // clear searched data source
    setDataSource([]);
  }

  const $selectEl: any = useRef<Select>(null)

  useEffect(() => {
    window.addEventListener('keydown', handleTriggerOrder)
    return () => {
      window.removeEventListener('keydown', handleTriggerOrder)
    }
  }, [])

  function handleTriggerOrder(e: KeyboardEvent) {
    if (isSearchHotkey(e)) {
      if ($selectEl && $selectEl.current) {
        $selectEl.current.focus()
      }
    }
  }

  return (
    <Select
      showSearch
      ref={$selectEl}
      style={{width: 800}}
      value={query}
      placeholder="Search item name"
      defaultActiveFirstOption={false}
      showArrow={false}
      filterOption={false}
      onSearch={lodash.debounce(handleSearch, 150)}
      onChange={handleChange}
      notFoundContent={null}>
      {
        dataSource.map(item => (
          <Select.Option key={item.itemId} value={item.itemId}>
            <SelectOptDiv>
              <SelectOptChild>
                <SelectOptChildTitle>name: </SelectOptChildTitle>
                <Highlighter
                  caseSensitive
                  highlightStyle={highLightStyle}
                  searchWords={[query as string]}
                  autoEscape={true}
                  textToHighlight={item.variety}
                />
              </SelectOptChild>
              <SelectOptChild>
                <SelectOptChildTitle>provider: </SelectOptChildTitle>
                <Highlighter
                  caseSensitive
                  highlightStyle={highLightStyle}
                  searchWords={[query as string]}
                  autoEscape={true}
                  textToHighlight={item.provider}
                />
              </SelectOptChild>
            </SelectOptDiv>
          </Select.Option>
        ))
      }
    </Select>
  )
}

export interface OrderEditorProps {
  saleItems: SaleItem[]
  onItemUpdate: OrdersDispatchProps['updateSaleItem']
}

export const OrderEditor: React.SFC<OrderEditorProps> = props => {
  const { saleItems, onItemUpdate } = props
  // selected items
  const [selectedItemIds, setSelectedItemIds] = useState<string[]>([])

  function handleSearch(value: string) {
    // 过滤已选中的数据
    const filteredRawData = saleItems.filter(n => selectedItemIds.indexOf(n.itemId) < 0)
    return search(value, filteredRawData)
  }

  function handleChange(itemId: string) {
    if (selectedItemIds.indexOf(itemId) < 0) {
      setSelectedItemIds([...selectedItemIds, itemId]);
    }
  }

  function handleRemove(itemId: string) {
    setSelectedItemIds(selectedItemIds.filter(n => n !== itemId))
  }

  const handleSalePriceChange = lodash.debounce((itemId: string, newSalePrice?: number) => {
    if (newSalePrice) {
      const item = saleItems.find((item) => item.itemId === itemId)
      if (item) {
        onItemUpdate({
          itemId: item.itemId,
          data: {
            salePrice: newSalePrice,
          },
        })
      }
    }
  }, 200)

  const handleQuantityChange = lodash.debounce((itemId: string, newQuantity?: number) => {
    if (newQuantity != null) {
      const item = saleItems.find((item) => item.itemId === itemId)
      if (item) {
        onItemUpdate({
          itemId: item.itemId,
          data: {
            orderQuantity: newQuantity,
          },
        })
      }
    }
  }, 200)

  // const columns: ColumnProps<SaleItem>[] = [
  const columns: ColumnProps<any>[] = [
    {
      title: 'SKU',
      dataIndex: 'SKU',
      key: 'SKU',
      render: (text, _item, index) => {
        if (index === 0) {
          return {
            children: <SearchInput onSearch={handleSearch} onChange={handleChange} />,
            props: {
              colSpan: 15,
              align: 'center'
            },
          }
        }
        return {
          children: text,
          props: {},
        }
      }
    },
    {
      title: 'CATEGORY',
      dataIndex: 'wholesaleCategory.name',
      key: 'wholesaleCategory.name',
    },
    {
      title: 'NAME',
      dataIndex: 'variety',
      key: 'variety',
    },
    {
      title: 'SIZE',
      dataIndex: 'size',
      key: 'size',
    },
    {
      title: 'WEIGHT',
      dataIndex: 'weight',
      key: 'weight',
    },
    {
      title: 'GRADE',
      dataIndex: 'grade',
      key: 'grade',
    },
    {
      title: 'PACKING',
      dataIndex: 'packing',
      key: 'packing',
    },
    {
      title: 'ORIGIN',
      dataIndex: 'origin',
      key: 'origin',
    },
    {
      title: 'STOCK',
      dataIndex: 'quantity',
      key: 'quantity',
    },
    {
      title: 'COST',
      dataIndex: 'cost',
      key: 'cost',
      render: (text) => text ? '$' + text : null
    },
    {
      title: 'MARGIN',
      dataIndex: 'margin',
      key: 'margin',
      sorter: (a, b) => a.margin - b.margin,
      render: (text) => text ? text + '%' : ''
    },
    {
      title: 'FREIGHT',
      dataIndex: 'freight',
      key: 'freight',
      sorter: (a, b) => a.freight - b.freight,
      render: (text) => text ? '$' + text : ''
    },
    {
      title: 'SALE PRICE',
      dataIndex: 'salePrice',
      key: 'salePrice',
      render: (rowPrice, item, index) => (
        index === 0
          ? null
          : <InputNumber
            value={rowPrice}
            placeholder="$0.00"
            step="0.25"
            formatter={value => value ? `$${value}` : ''}
            onChange={handleSalePriceChange.bind(null, item.itemId)}
          />
      )
    },
    {
      dataIndex: 'orderQuantity',
      key: 'orderQuantity',
      title: 'QUANTITY',
      render: (rowPrice, item, index) => (
        index === 0
          ? null
          : <InputNumber
            value={rowPrice}
            placeholder="0"
            step="1"
            onChange={handleQuantityChange.bind(null, item.itemId)}
          />
      )
    },
    {
      width: 110,
      title: 'operation',
      dataIndex: 'itemId',
      render: (_item, saleItem, index) => index === 0
        ? null
        : <Button icon="delete" onClick={handleRemove.bind(null, saleItem.itemId)} />
    },
  ]

  // keep one item in the table
  const dataSource = [{}].concat(
    saleItems.filter(n => selectedItemIds.indexOf(n.itemId) > -1)
  )

  return (
    <div>
      <MutiSortTable
        css={overrideTableCss}
        pagination={false}
        columns={columns}
        dataSource={dataSource}
        rowKey="itemId"
      />
    </div>
  )
}
