import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { lightGrey } from '~/common'

export const Placeholder = styled('div')({
  position: 'absolute',
  color: lightGrey,
  lineHeight: '26px',
  fontSize: '24px',
  bottom: '190px',
  left: '260px',
})

export const globalStyle = css({
  '#root': {
    minWidth: '1280px',
  },
})

export const highLightStyle: React.CSSProperties = {
  fontWeight: 'bold',
}

export const overrideTableCss = css({
  '& .ant-table-body tbody tr:first-of-type td:not(:first-of-type)': {
    padding: 0,
  },
})

export const SelectOptDiv = styled.div({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
})

export const SelectOptChild = styled.div({
  flex: 1,
  display: 'flex',
  alignItems: 'center',
})

export const SelectOptChildTitle = styled.div((props) => ({
  color: props.theme.primary,
  textTransform: 'capitalize',
}))
