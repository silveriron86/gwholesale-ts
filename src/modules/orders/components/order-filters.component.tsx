import React, { FC, useState, useMemo, useEffect } from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Icon, Modal, Tabs, Button, Input, Checkbox, Radio, Select, DatePicker, Form } from 'antd'
import { ThemeOutlineButton, ThemeCheckbox, ThemeRadio, ThemeSelect } from '~/modules/customers/customers.style'
import { StyleTabs, StyleTag, StyleTagSpan } from './order-filters.style'
import { useImmer } from 'use-immer'
import moment from 'moment'
import { FormComponentProps } from 'antd/es/form'
import useForm from 'rc-form-hooks'
import { gray01 } from '~/common'

const { TabPane } = Tabs
interface FormFields {
  status: string
  pas: number
  orderDate: String
  buyer: string
  vendor: string
  productSku: string
  productName: string
  businessType: string
}
type IProps = OrdersStateProps &
  OrdersDispatchProps & {
    setFilters: (filters: any) => void
    filters: any
  } & FormComponentProps<FormFields>

const OrderFilterField: FC<IProps> = (props: IProps) => {
  const {
    getCompanyUsers,
    currentCompanyUsers = [],
    getVendors,
    vendors = [],
    setFilters: setF,
    filters: F,
    // form: { getFieldDecorator },
  } = props
  const { getFieldDecorator, validateFields, resetFields } = useForm<FormFields>()
  const [filters, setFilters] = useImmer<any>({ ...props.filters })
  const [open, setOpen] = useState<boolean>(false)
  useEffect(() => {
    getCompanyUsers()
    getVendors()
  }, [])

  const setFilerByKey = (key: string, value: any) => {
    setFilters((draft: any) => {
      draft[key] = value
    })
  }

  const selectedBuyers = useMemo(() => {
    return filters.buyer.map((id: string) => {
      const user = currentCompanyUsers.find((item: any) => id == item.userId)
      return {
        label: `${user?.firstName}${user?.lastName}`,
        value: id,
      }
    })
  }, [currentCompanyUsers, filters.buyer])

  const selectedVendors = useMemo(() => {
    return filters.vendor.map((id: string) => {
      const client = vendors.find((item: any) => id == item.clientId)
      return {
        label: client?.clientCompany?.companyName,
        value: id,
      }
    })
  }, [filters.vendor, vendors])

  const selectedBusinessTypes = useMemo(() => {
    return filters.businessType.map((value: string) => {
      return {
        label: value,
        value,
      }
    })
  }, [filters.businessType])

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }

  const renderStatus = () => {
    return (
      <>
        <h3>Status</h3>
        <Form.Item>
          {getFieldDecorator('status', {
            initialValue: filters.status,
          })(
            <Checkbox.Group
              style={{ width: '100%' }}
              onChange={(values) => {
                setFilerByKey('status', values)
              }}
            >
              {Object.keys(statusMap).map((key) => (
                <div key={key} style={{ padding: '11px 0' }}>
                  <ThemeCheckbox value={key}>{capitalizeFirstLetter(statusMap[key])}</ThemeCheckbox>
                </div>
              ))}
            </Checkbox.Group>,
          )}
        </Form.Item>
      </>
    )
  }

  const renderPas = () => {
    return (
      <>
        <h3>PAS</h3>
        <Form.Item>
          {getFieldDecorator('pas', {
            initialValue: filters.pas,
          })(
            <Radio.Group
              onChange={(e) => {
                setFilerByKey('pas', e.target.value)
              }}
              value={filters.pas}
            >
              <div style={{ padding: '11px 0' }}>
                <ThemeRadio value={'1'}>True</ThemeRadio>
              </div>
              <div style={{ padding: '11px 0' }}>
                <ThemeRadio value={'0'}>False</ThemeRadio>
              </div>
              <div style={{ padding: '11px 0' }}>
                <ThemeRadio value={'all'}>All</ThemeRadio>
              </div>
            </Radio.Group>,
          )}
        </Form.Item>
      </>
    )
  }

  const renderOrderDate = () => {
    const values = filters.orderDate ? moment(filters.orderDate) : undefined
    return (
      <>
        <h3>Order date</h3>
        <DatePicker
          placeholder="Select Order Date"
          value={values}
          onChange={(_, dateString: string) => {
            setFilerByKey('orderDate', dateString)
          }}
        />
      </>
    )
  }

  const renderBuyer = () => {
    return (
      <>
        <h3>Buyer</h3>
        {currentCompanyUsers.length <= 8 ? (
          <Form.Item>
            {getFieldDecorator('buyer', {
              initialValue: filters.buyer,
            })(
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={(values) => {
                  setFilerByKey('buyer', values)
                }}
              >
                {currentCompanyUsers.map((user) => (
                  <div key={user.userId} style={{ padding: '11px 0' }}>
                    <ThemeCheckbox value={user.userId + ''}>{`${user.firstName}${user.lastName}`}</ThemeCheckbox>
                  </div>
                ))}
              </Checkbox.Group>,
            )}
          </Form.Item>
        ) : (
          <Form.Item>
            {getFieldDecorator('buyer', {
              initialValue: filters.buyer,
            })(
              <Select
                mode="multiple"
                style={{ width: '100%' }}
                placeholder="Please select buyer"
                defaultValue={[]}
                onChange={(values: any) => {
                  setFilerByKey('buyer', values)
                }}
                filterOption={(inputValue, option) =>
                  option.props?.children && option.props.children.includes(inputValue)
                }
              >
                {currentCompanyUsers.map((user) => (
                  <Select.Option key={user.userId} value={user.userId + ''}>
                    {`${user.firstName}${user.lastName}`}
                  </Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        )}
      </>
    )
  }

  const renderBusinessType = () => {
    return (
      <>
        <h3>Vendor business type</h3>
        {businessTypes.length <= 8 ? (
          <Form.Item>
            {getFieldDecorator('businessType', {
              initialValue: filters.businessType,
            })(
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={(values) => {
                  setFilerByKey('businessType', values)
                }}
              >
                {businessTypes.map((item: { name: {} | null | undefined }) => (
                  <div key={item.name} style={{ padding: '11px 0' }}>
                    <ThemeCheckbox value={item.name}>{item.name}</ThemeCheckbox>
                  </div>
                ))}
              </Checkbox.Group>,
            )}
          </Form.Item>
        ) : (
          <Form.Item>
            {getFieldDecorator('businessType', {
              initialValue: filters.businessType,
            })(
              <Select
                mode="multiple"
                style={{ width: '100%' }}
                // defaultValue={[]}
                onChange={(values: any) => {
                  setFilerByKey('businessType', values)
                }}
                filterOption={(inputValue, option) => {
                  const listItem: string = (option.props?.children as string) ?? ''
                  return listItem.toLowerCase().indexOf(inputValue.toLowerCase()) > -1
                }}
              >
                {businessTypes.map((item: { name: {} | null | undefined }) => (
                  <Select.Option key={item.name} value={item.name}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        )}
      </>
    )
  }

  const renderVendors = () => {
    return (
      <>
        <h3>Vendor</h3>
        {vendors.length <= 8 ? (
          <Form.Item>
            {getFieldDecorator('vendor', {
              initialValue: filters.vendor,
            })(
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={(values) => {
                  setFilerByKey('vendor', values)
                }}
              >
                {vendors.map((v) => (
                  <div key={v.clientId} style={{ padding: '11px 0' }}>
                    <ThemeCheckbox value={v.clientId + ''}>{v.clientCompany.companyName}</ThemeCheckbox>
                  </div>
                ))}
              </Checkbox.Group>,
            )}
          </Form.Item>
        ) : (
          <Form.Item>
            {getFieldDecorator('vendor', {
              initialValue: filters.vendor,
            })(
              <Select
                mode="multiple"
                style={{ width: '100%' }}
                placeholder="Please select vendor"
                // defaultValue={[]}
                onChange={(values: any) => {
                  setFilerByKey('vendor', values)
                }}
                filterOption={(inputValue, option) => {
                  const listItem: string = (option.props?.children as string) ?? ''
                  return listItem.toLowerCase().indexOf(inputValue.toLowerCase()) > -1
                }}
              >
                {vendors.map((v) => (
                  <Select.Option key={v.clientId} value={v.clientId + ''}>
                    {v.clientCompany.companyName}
                  </Select.Option>
                ))}
              </Select>,
            )}
          </Form.Item>
        )}
      </>
    )
  }

  const resetFilter = () => {
    setFilters(initFilters)
    setF({ ...initFilters })
    resetFields()
  }

  const { companyProductTypes } = props
  const businessTypes = companyProductTypes != null ? companyProductTypes.vendorTypes : []

  return (
    <>
      <div style={{ textAlign: 'right' }}>
        <ThemeOutlineButton
          onClick={() => {
            setOpen(!open)
          }}
        >
          Add a Filter <Icon type="filter" />
        </ThemeOutlineButton>
        <Button type="link" icon="close" onClick={resetFilter} style={{ color: gray01, marginLeft: '7px' }}>
          Clear all filters
        </Button>
      </div>

      <Modal
        title="Filter Purchase Orders"
        visible={open}
        onCancel={() => {
          setOpen(false)
        }}
        onOk={() => {
          setF(filters)
          setOpen(false)
        }}
      >
        <StyleTabs defaultActiveKey="0" tabPosition="left">
          <TabPane tab={<StyleTagSpan>Status {!!filters.status.length && <strong />}</StyleTagSpan>} key="0">
            {renderStatus()}
          </TabPane>
          <TabPane tab={<StyleTagSpan>PAS {filters.pas !== 'all' && <strong />}</StyleTagSpan>} key="1">
            {renderPas()}
          </TabPane>
          <TabPane tab={<StyleTagSpan>Order date {!!filters.orderDate && <strong />}</StyleTagSpan>} key="2">
            {renderOrderDate()}
          </TabPane>

          {currentCompanyUsers.length ? (
            <TabPane tab={<StyleTagSpan>Buyer {!!filters.buyer.length && <strong />}</StyleTagSpan>} key="3">
              {renderBuyer()}
            </TabPane>
          ) : null}

          {vendors.length ? (
            <TabPane tab={<StyleTagSpan>Vendor {!!filters.vendor.length && <strong />}</StyleTagSpan>} key="4">
              {renderVendors()}
            </TabPane>
          ) : null}

          <TabPane
            tab={<StyleTagSpan>Vendor business type {!!filters.businessType.length && <strong />}</StyleTagSpan>}
            key="7"
          >
            {renderBusinessType()}
          </TabPane>

          <TabPane tab={<StyleTagSpan>Product SKU {!!filters.productSku && <strong />}</StyleTagSpan>} key="5">
            <h3>Product SKU</h3>
            <Input
              placeholder="Product SKU"
              value={filters.productSku}
              onChange={(e) => {
                setFilerByKey('productSku', e.target.value)
              }}
            />
          </TabPane>

          <TabPane tab={<StyleTagSpan>Product Name {!!filters.productName && <strong />}</StyleTagSpan>} key="6">
            <h3>Product Name</h3>
            <Input
              placeholder="Product Name"
              value={filters.productName}
              onChange={(e) => {
                setFilerByKey('productName', e.target.value)
              }}
            />
          </TabPane>
        </StyleTabs>
      </Modal>

      <div style={{ textAlign: 'right', marginTop: '20px' }}>
        {!!F.status.length &&
          F.status.map((status: string) => (
            <StyleTag
              closable
              key={status}
              onClose={() => {
                setFilters((draft: any) => {
                  draft.status = filters.status.filter((s: any) => s !== status)
                })
                const FStatus = F.status.filter((s: any) => s !== status)
                setF({ ...F, status: FStatus })
                resetFields(['status'])
              }}
            >
              Status: {capitalizeFirstLetter(statusMap[status])}
            </StyleTag>
          ))}

        {F.pas !== 'all' && (
          <StyleTag
            closable
            onClose={() => {
              setFilters((draft: any) => {
                draft.pas = 'all'
              })
              setF({ ...F, pas: 'all' })
              resetFields(['pas'])
            }}
          >
            PAS: {F.pas === '1' ? 'True' : 'False'}
          </StyleTag>
        )}

        {!!F.orderDate && (
          <StyleTag
            closable
            onClose={() => {
              setFilters((draft: any) => {
                draft.orderDate = ''
              })
              setF({ ...F, orderDate: '' })
              resetFields(['orderDate'])
            }}
          >
            Order Date: {F.orderDate}
          </StyleTag>
        )}

        {!!selectedBuyers.length &&
          selectedBuyers.map((buyer: Option) => (
            <StyleTag
              closable
              key={buyer.value}
              onClose={() => {
                setFilters((draft: any) => {
                  draft.buyer = filters.buyer.filter((s: any) => s !== buyer.value)
                })
                const FBuyer = F.buyer.filter((s: any) => s !== buyer.value)
                setF({ ...F, buyer: FBuyer })
                resetFields(['buyer'])
              }}
            >
              Buyer: {buyer.label}
            </StyleTag>
          ))}

        {!!selectedVendors.length &&
          selectedVendors.map((client: Option) => (
            <StyleTag
              closable
              key={client.value}
              onClose={() => {
                setFilters((draft: any) => {
                  draft.vendor = filters.vendor.filter((s: any) => s !== client.value)
                })
                const FVendor = F.vendor.filter((s: any) => s !== client.value)
                setF({ ...F, vendor: FVendor })
                resetFields(['vendor'])
              }}
            >
              Vendor: {client.label}
            </StyleTag>
          ))}

        {!!selectedBusinessTypes.length &&
          selectedBusinessTypes.map((type: Option) => (
            <StyleTag
              closable
              key={type.value}
              onClose={() => {
                setFilters((draft: any) => {
                  draft.businessType = filters.businessType.filter((s: any) => s !== type.value)
                })
                const FBusinessType = F.businessType.filter((s: any) => s !== type.value)
                setF({ ...F, businessType: FBusinessType })
                resetFields(['businessType'])
              }}
            >
              Business Type: {type.value}
            </StyleTag>
          ))}

        {!!F.productSku && (
          <StyleTag
            closable
            onClose={() => {
              setFilters((draft: any) => {
                draft.productSku = ''
              })
              setF({ ...F, productSku: '' })
              resetFields(['productSku'])
            }}
          >
            Product SKU: {F.productSku}
          </StyleTag>
        )}
        {!!F.productName && (
          <StyleTag
            closable
            onClose={() => {
              setFilters((draft: any) => {
                draft.productName = ''
              })
              setF({ ...F, productName: '' })
              resetFields(['productName'])
            }}
          >
            Product Name: {F.productName}
          </StyleTag>
        )}
      </div>
    </>
  )
}

// const OrderFilterForm = Form.create()(OrderFilterField)
const mapStateToProps = (state: GlobalState) => state.orders

export default connect(OrdersModule)(mapStateToProps)(OrderFilterField)

const statusMap = {
  // PLACED: 'New',
  // CONFIRMED: 'Confirmed',
  // RECEIVED: 'Received',
  OPEN: 'OPEN',
  CLOSED: 'CLOSED',
  CANCEL: 'CANCEL',
}

interface Option {
  label: string
  value: number | string
}

const initFilters = {
  status: [],
  pas: 'all',
  orderDate: '',
  buyer: [],
  vendor: [],
  productSku: '',
  productName: '',
  businessType: [],
}
