import styled from '@emotion/styled'
import { Tabs, Tag } from 'antd'

export const StyleTabs = styled(Tabs)({
  '&.ant-tabs .ant-tabs-left-bar .ant-tabs-tab': {
    textAlign: 'left',
    fontWeight: 'normal',
    minWidth: 216,
    '&.ant-tabs-tab-active': {
      fontWeight: 'bold'
    }
  },
})

export const StyleTag: any = styled(Tag)((props) => ({
  borderRadius: '32px',
  marginBottom: '10px',
  color: props.theme.dark,
  boxShadow: '0px 1px 2px rgba(0, 0, 0, 0.25)',
  fontWeight: 600,
  fontSize: '14px',
  lineHeight: '140%',
  padding: '4px 14px',
}))

export const StyleTagSpan: any = styled.span((props) => ({
  display: 'flex',
  alignItems: 'center',
  strong: {
    display: 'inline-block',
    backgroundColor: props.theme.dark,
    width: '6px',
    height: '6px',
    borderRadius: '6px',
    marginLeft: '10px',
  },
}))
