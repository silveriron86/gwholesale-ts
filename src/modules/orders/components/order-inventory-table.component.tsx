/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Input } from 'antd'
import { ColumnProps } from 'antd/es/table'
import organicImg from '~/modules/inventory/components/organic.png'
import update from 'immutability-helper'

import { SaleItem } from '~/schema'
import { QBOImage, tableCss,
  inputCss,
  inputIcon,
  InputWrapper } from './order-pricesheet-table.style'
import { MutiSortTable } from '~/components'

import { cloneDeep } from 'lodash'

export interface OrderInventoryTableProps {
  saleItems: SaleItem[]
  updateOrderTotal: (newItemCount: number, newTotalPrice: number) => void
}


export class OrderInventoryTable extends React.PureComponent<OrderInventoryTableProps> {
  state = {
    initItems: cloneDeep(this.props.saleItems),
  }

  componentWillReceiveProps(nextProps: OrderInventoryTableProps)
  {
    if (this.state.initItems.length != nextProps.saleItems.length)
      this.setState({
        initItems: cloneDeep(nextProps.saleItems),
      })
  }


  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onSortVariety = (a: any, b: any) => {
    let stringA = a.variety ? a.variety : ''
    let stringB = b.variety ? b.variety : ''
    stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }

  render() {

    const { initItems } = this.state

    const checkItems = (newItems: SaleItem[]) => {
      let itemCount = 0
      let totalPrice = 0
      for (const priceSheetItem of newItems)
      {
        if (priceSheetItem.orderQuantity! > 0)
        {
          itemCount += priceSheetItem.orderQuantity!
          totalPrice += priceSheetItem.cost * priceSheetItem.orderQuantity!
        }
      }
      this.props.updateOrderTotal(itemCount, totalPrice)
    }

    const onQuantityChange = (itemId: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
      const newQty = e.target.valueAsNumber
      if (newQty < 0)
        return true

      const items = initItems
      const index = items.findIndex(i=> i.wholesaleItemId === itemId)
      const updatedItem = update(items[index], {orderQuantity: {$set: newQty}})
      const newItems = update(items, {
        $splice: [[index, 1, updatedItem]]
      })

      checkItems(newItems)
      this.setState({
        initItems: newItems
      })
    }


    const onCostChange = (itemId: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
      const newCost = e.target.valueAsNumber

      const items = initItems
      const index = items.findIndex(i=> i.wholesaleItemId === itemId)
      const updatedItem = update(items[index], {cost: {$set: newCost}})
      const newItems = update(items, {
        $splice: [[index, 1, updatedItem]]
      })

      checkItems(newItems)
      this.setState({
        initItems: newItems
      })
    }

    const columns: ColumnProps<SaleItem>[] = [
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        sorter: (a, b) => this.onSortString('SKU', a, b),
      },
      {
        title: 'CATEGORY',
        dataIndex: 'wholesaleCategory.name',
        key: 'wholesaleCategory.name',
        sorter: (a, b) => this.onSortString('name', a.wholesaleCategory, b.wholesaleCategory),
      },
      {
        title: 'NAME',
        dataIndex: 'variety',
        key: 'variety',
        sorter: (a, b) => this.onSortVariety(a, b),
        render(text, item) {
            return item.organic == true ?
              ( <span><QBOImage src={organicImg} /> {text}</span> )
              :
              ( <span>{text}</span> )
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        sorter: (a, b) => this.onSortString('size', a, b),
      },
      {
        title: 'WEIGHT',
        dataIndex: 'weight',
        key: 'weight',
        sorter: (a, b) => this.onSortString('weight', a, b),
      },
      {
        title: 'GRADE',
        dataIndex: 'grade',
        key: 'grade',
        sorter: (a, b) => this.onSortString('grade', a, b),
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        sorter: (a, b) => this.onSortString('packing', a, b),
      },
      {
        title: 'ORIGIN',
        dataIndex: 'origin',
        key: 'origin',
        sorter: (a, b) => this.onSortString('origin', a, b),
      },
      {
        title: 'PROVIDER',
        dataIndex: 'provider',
        key: 'provider',
        sorter: (a, b) => this.onSortString('provider', a, b),
      },
      {
        dataIndex: 'orderQuantity',
        key: 'orderQuantity',
        title: 'QUANTITY',
        render(rowPrice, item, _index) {
          return (
              <Input
              value={rowPrice}
              placeholder="0" step="1" type="number" css={inputCss} onChange={onQuantityChange(item.wholesaleItemId)} />
          )
        },
      },
    ]

    columns.push({
      title: 'COST',
      dataIndex: 'cost',
      key: 'cost',
      sorter: (a, b) => a.cost - b.cost,
      render(rowPrice, item, _index) {
        return (
            <InputWrapper>
              <Input
              value={rowPrice}
              placeholder="$0.00" step="0.25" type="number" css={inputCss} onChange={onCostChange(item.wholesaleItemId)} />
              <b css={inputIcon}>$</b>
            </InputWrapper>
          )
      },
    })

    return <MutiSortTable pagination={false} columns={columns} dataSource={initItems} rowKey="wholesaleItemId" css={tableCss} />
  }
}
