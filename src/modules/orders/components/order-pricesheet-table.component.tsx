/**@jsx jsx */
import React, { useEffect, useState } from 'react'
import { jsx } from '@emotion/core'
import { Dropdown, Input, InputNumber, Menu, Modal, Tooltip } from 'antd'
import { ColumnProps } from 'antd/es/table'
import lodash, { findIndex } from 'lodash'
import { Icon as IconSVG } from '~/components'

import organicImg from '~/modules/inventory/components/organic.png'
import { PriceSheetItem, SaleSection } from '~/schema'
import { tableCss, inputCss, InputWrapper, tableWrapperStyle } from './order-pricesheet-table.style'
import { MutiSortTable } from '~/components'

import CategoryHeader from '~/components/CategoryHeader'
import { OrdersDispatchProps } from '../orders.module'
import { QBOImage } from '~/modules/inventory/components/inventory-table.style'
import { Flex } from '~/modules/customers/customers.style'
import { FloatMenuItem, ItemHMargin4 } from '~/modules/customers/nav-sales/styles'
import styled from '@emotion/styled'
import { formatNumber } from '~/common/utils'

export const IconWrap = styled('span')((props: any) => ({
  cursor: 'pointer',
  height: 28,
  width: 36,
  lineHeight: '28px',
  textAlign: 'center',
  backgroundColor: 'white',
  border: '1px solid grey',
  boxShadow: '0px 1px 1px 1px rgba(0,0,0,0.45);',
  borderRadius: 4,
  '&:hover': {
    // boxShadow: '0px 1px 1px 1px rgba(0,0,0,0.25);',
    backgroundColor: '#F3F3F3',
  },
}))

export interface OrderPriceSheetTableProps {
  priceSheetItems: PriceSheetItem[]
  isSeller: boolean
  onPriceSheetItemUpdate: OrdersDispatchProps['updatePriceSheetItem']
  isRemovedCats: boolean
  selectedCustomer: any
  sellerSetting: any
}

export class OrderPriceSheetTable extends React.PureComponent<OrderPriceSheetTableProps> {
  state = {
    currentId: 0,
    currentNote: '',
    showAddNoteModalVisible: false,
  }

  handleSalePriceChange = lodash.debounce((priceSheetItemId: string, newSalePrice?: number) => {
    if (newSalePrice) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if (item) {
        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            salePrice: newSalePrice,
          },
        })
      }
    }
  }, 200)

  handleFreightChange = lodash.debounce((priceSheetItemId: string, newFreight?: number) => {
    if (newFreight) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if (item) {
        const newSalePrice = Math.round((item.cost / (1.0 - item.margin / 100.0) + newFreight) * 4) / 4
        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            salePrice: newSalePrice,
            freight: newFreight,
          },
        })
      }
    }
  }, 200)

  handleMarginChange = lodash.debounce((priceSheetItemId: string, newMargin?: number) => {
    if (newMargin) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props

      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if (item) {
        const newSalePrice = Math.round((item.cost / (1.0 - newMargin / 100.0) + item.freight) * 4) / 4
        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            margin: newMargin,
            salePrice: newSalePrice,
          },
        })
      }
    }
  }, 200)

  handleQuantityChange = lodash.debounce((priceSheetItemId: string, newQuantity?: number) => {
    if (newQuantity != null) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if (item) {
        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            orderQuantity: newQuantity,
          },
        })
      }
    }
  }, 200)

  getColumnsByCompanySetting = (columns: any[]) => {
    const { sellerSetting } = this.props
    if (sellerSetting && sellerSetting.company) {
      if (!sellerSetting.company.showPriceSheetSize) {
        const findSizeIndex = columns.findIndex(el => el.key == 'size')
        columns.splice(findSizeIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetWeight) {
        const findWeightIndex = columns.findIndex(el => el.key == 'weight')
        columns.splice(findWeightIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetPacking) {
        const findPackingIndex = columns.findIndex(el => el.key == 'packing')
        columns.splice(findPackingIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetBrand) {
        const findBrandIndex = columns.findIndex(el => el.key == 'modifier')
        columns.splice(findBrandIndex, 1)
      }
      if (!sellerSetting.company.showPriceSheetOrigin) {
        const findOriginIndex = columns.findIndex(el => el.key == 'origin')
        columns.splice(findOriginIndex, 1)
      }
    }
    return columns
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  renderModifiersListUserToolTips = (record: any) => {
    if (!record.suppliers) return ''
    const currentSuppliers = record.suppliers.split(', ').map((el: string) => el.trim())
    if (currentSuppliers.length > 1) {
      return (
        <Tooltip
          title={
            <>
              {currentSuppliers.map((val: any, index: number) => {
                if (index != 0) {
                  return <div>{val}</div>
                }
              })}
            </>
          }
        >
          {currentSuppliers[0]} <span style={{ float: 'right' }}>+{currentSuppliers.length - 1}</span>
        </Tooltip>
      )
    } else if (currentSuppliers.length == 1) {
      return currentSuppliers[0]
    } else {
      return ''
    }
  }

  onSortVariety = (a: any, b: any) => {
    let stringA = a.variety ? a.variety : ''
    let stringB = b.variety ? b.variety : ''
    stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }

  handleSaveNote = (note: string) => {
    if (note != null) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const { currentId } = this.state
      const item = priceSheetItems.find((item) => item.priceSheetItemId == currentId)
      if (item) {
        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            note,
          },
        })
      }
      this.setState({
        currentId: 0,
        currentNote: '',
        showAddNoteModalVisible: false,
      })
    }
  }

  menuClick = ({ key }: { key: string }) => {
    if (key === '0') {
      this.setState({ showAddNoteModalVisible: true })
    }
  }

  renderMenuItems = () => {
    return (
      <Menu onClick={this.menuClick}>
        <Menu.Item key={0}>
          <FloatMenuItem>
            <IconSVG type="add-note" style={ItemHMargin4} viewBox="0 0 20 20" width={20} height={20} />
            Add a Note
          </FloatMenuItem>
        </Menu.Item>
      </Menu>
    )
  }

  render() {
    const { isSeller, priceSheetItems, selectedCustomer } = this.props
    const baseColumns: ColumnProps<PriceSheetItem>[] = [
      {
        title: 'SKU',
        dataIndex: 'wholesaleItem.sku',
        key: 'SKU',
        sorter: (a, b) => this.onSortString('wholesaleItem.sku', a, b),
      },
      {
        title: 'CATEGORY',
        dataIndex: 'wholesaleCategory.name',
        key: 'wholesaleCategory.name',
        sorter: (a, b) => this.onSortString('name', a.wholesaleCategory, b.wholesaleCategory),
      },
      {
        title: 'NAME',
        dataIndex: 'variety',
        key: 'variety',
        sorter: (a, b) => this.onSortVariety(a, b),
        render: (text, item) => {
          return (
            <Flex className="space-between v-center">
              <span>
                {item.organic ? (
                  <span>
                    <QBOImage src={organicImg} />
                    {text}
                  </span>
                ) : (
                  <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <span>{text}</span>
                    {item.isRequest && <span style={{ color: '#ccc' }}>Add details to order notes</span>}
                  </div>
                )}
              </span>
              {item.note != null && item.note != '' && (
                <span>
                  <IconSVG
                    onClick={() =>
                      this.setState({
                        showAddNoteModalVisible: true,
                        currentNote: item.note,
                        currentId: item.priceSheetItemId,
                      })
                    }
                    type="add-note"
                    viewBox="0 0 20 20"
                    width={20}
                    height={20}
                    style={{
                      ...ItemHMargin4,
                      marginTop: 2,
                      fill: 'none',
                      cursor: 'pointer',
                    }}
                  />
                </span>
              )}
              {this.state.currentId == item.priceSheetItemId && (item.note == null || item.note == '') && (
                <Dropdown overlay={this.renderMenuItems} placement="bottomRight" trigger={['click']}>
                  <IconWrap>
                    <IconSVG type="ellipsis" viewBox="0 0 16 4" width={16} height={8} />
                  </IconWrap>
                </Dropdown>
              )}
            </Flex>
          )
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        sorter: (a, b) => this.onSortString('size', a, b),
      },
      {
        title: 'WEIGHT',
        dataIndex: 'weight',
        key: 'weight',
        sorter: (a, b) => this.onSortString('weight', a, b),
      },
      {
        title: 'GRADE',
        dataIndex: 'grade',
        key: 'grade',
        sorter: (a, b) => this.onSortString('grade', a, b),
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        sorter: (a, b) => this.onSortString('packing', a, b),
      },
      {
        title: 'BRAND',
        dataIndex: 'modifier',
        key: 'modifier',
        sorter: (a, b) => this.onSortString('modifier', a, b),
        render: (modifier: string, record: any) => this.renderModifiersListUserToolTips(record),
      },
      {
        title: 'ORIGIN',
        dataIndex: 'origin',
        key: 'origin',
        sorter: (a, b) => this.onSortString('origin', a, b),
      },
      {
        dataIndex: 'orderQuantity',
        key: 'orderQuantity',
        title: 'QUANTITY',
        width: 200,
        render: (rowPrice, item) => {
          return (
            <>
              <InputNumber
                value={rowPrice}
                placeholder="0"
                step="1"
                min={0}
                css={inputCss}
                onChange={this.handleQuantityChange.bind(this, item.priceSheetItemId)}
              />
              &nbsp;
              {item.wholesaleItem.inventoryUOM}
            </>
          )
        },
      },
    ]

    let columns: ColumnProps<PriceSheetItem>[] = baseColumns.concat(
      !isSeller
        ? [
          {
            title: 'SALE PRICE',
            dataIndex: 'salePrice',
            key: 'salePrice',
            sorter: (a, b) => a.salePrice - b.salePrice,
            render: (rowPrice, item) => {
              if (item.isRequest || item.isSelectedOrder) return <div style={{ textAlign: 'center' }}>REQUEST</div>
              // We don't need to calculate the salePrice again because this values were saved in pricesheet when margin, markup are changned
              // let newSalePrice: any = 0
              // const cost = item.wholesaleItem ? item.wholesaleItem.cost : 0
              // if (item.defaultLogic === 'FOLLOW_GROUP') {
              //   if (item.wholesaleItem && item.wholesaleItem.priceGroupType == 1) {
              //     newSalePrice = formatNumber(rowPrice, 2)
              //   } else {
              //     newSalePrice = formatNumber(cost / (1.0 - item.margin / 100.0) + item.markup, 2)
              //   }
              // } else {
              //   newSalePrice = formatNumber(cost / (1.0 - item.margin / 100.0) + item.markup, 2)
              // }
              return (
                <div style={{ textAlign: 'center' }}>
                  {!!selectedCustomer?.displayPriceSheetPrice
                    ? `$${rowPrice}/${item.wholesaleItem.inventoryUOM || ''}`
                    : 'Available'}
                </div>
              )
            },
          },
        ]
        : [
          {
            title: 'COST',
            dataIndex: 'cost',
            key: 'cost',
            sorter: (a, b) => a.cost - b.cost,
            render(rowPrice) {
              return <div>${(Math.round(rowPrice * 100) / 100).toFixed(2)}</div>
            },
          },
          {
            title: 'MARGIN',
            dataIndex: 'margin',
            key: 'margin',
            sorter: (a, b) => a.margin - b.margin,
            render: (rowPrice, item) => (
              <InputWrapper>
                <InputNumber
                  value={rowPrice}
                  placeholder="0%"
                  step="1"
                  min={0}
                  formatter={(value) => `${value}%`}
                  css={inputCss}
                  onChange={this.handleMarginChange.bind(this, item.priceSheetItemId)}
                />
              </InputWrapper>
            ),
          },
          {
            title: 'FREIGHT',
            dataIndex: 'freight',
            key: 'freight',
            sorter: (a, b) => a.freight - b.freight,
            render: (rowPrice, item) => (
              <InputWrapper>
                <InputNumber
                  value={rowPrice}
                  placeholder="$0.00"
                  step="0.25"
                  min={0}
                  formatter={(value) => `$${value}`}
                  css={inputCss}
                  onChange={this.handleFreightChange.bind(this, item.priceSheetItemId)}
                />
              </InputWrapper>
            ),
          },
          {
            title: 'SALE PRICE',
            dataIndex: 'salePrice',
            key: 'salePrice',
            sorter: (a, b) => a.salePrice - b.salePrice,
            render: (rowPrice, item) => {
              let newSalePrice: any = 0
              const cost = item.wholesaleItem ? item.wholesaleItem.cost : 0
              if (item.defaultLogic === 'FOLLOW_GROUP') {
                if (item.wholesaleItem && item.wholesaleItem.priceGroupType == 1) {
                  newSalePrice = formatNumber(rowPrice, 2)
                } else {
                  newSalePrice = formatNumber(cost / (1.0 - item.margin / 100.0) + item.markup, 2)
                }
              } else {
                newSalePrice = formatNumber(cost / (1.0 - item.margin / 100.0) + item.markup, 2)
              }

              return (
                <InputWrapper>
                  <InputNumber
                    value={newSalePrice}
                    placeholder="$0.00"
                    step="0.25"
                    min={0}
                    formatter={(value) => `$${value}`}
                    css={inputCss}
                    onChange={this.handleSalePriceChange.bind(this, item.priceSheetItemId)}
                  />
                </InputWrapper>
              )
            }
          },
        ],
    )

    columns = this.getColumnsByCompanySetting(columns)

    return (
      <React.Fragment>
        <MutiSortTable
          pagination={false}
          columns={columns}
          onRow={(r) => {
            return {
              onMouseEnter: () => {
                this.setState({ currentId: r.priceSheetItemId, currentNote: r.note })
              },
              onMouseLeave: () => {
                if (!this.state.showAddNoteModalVisible) {
                  this.setState({ currentId: 0, currentNote: '' })
                }
              },
            }
          }}
          dataSource={priceSheetItems}
          rowKey="priceSheetItemId"
          css={tableCss}
        />
        <AddNoteModal
          visible={this.state.showAddNoteModalVisible}
          onCancel={() => this.setState({ showAddNoteModalVisible: false, currentNote: '' })}
          onOk={this.handleSaveNote}
          note={this.state.currentNote}
        />
      </React.Fragment>
    )
  }
}

const OrderPriceSheetTableSubtable: React.SFC<OrderPriceSheetTableProps & {
  category: SaleSection | null
}> = (props) => {
  const { category, priceSheetItems } = props
  let listItems = priceSheetItems
  if (category !== null) {
    listItems = priceSheetItems.filter((n) => n.wholesaleCategory.wholesaleSection.name === category.name)
  }

  const [collapsed, setCollpased] = useState(category != null && listItems.length === 0)

  const handleHeaderClick = () => {
    setCollpased(!collapsed)
  }
  return (
    <React.Fragment>
      <CategoryHeader
        value={category !== null ? category.name : 'All'}
        collapsed={collapsed}
        onCollapseChange={handleHeaderClick}
      />
      <div css={tableWrapperStyle(collapsed)}>
        <OrderPriceSheetTable {...props} priceSheetItems={listItems} />
      </div>
    </React.Fragment>
  )
}

const OrderPriceSheetTableContainer: React.SFC<OrderPriceSheetTableProps> = (props) => {
  // pick up all category
  const categoryList: SaleSection[] = Array.isArray(props.priceSheetItems)
    ? lodash.uniqWith(
      props.priceSheetItems.map((n) =>
        lodash.pick(n.wholesaleCategory.wholesaleSection, ['wholesaleSectionId', 'name', 'warehouse', 'qboId']),
      ),
      lodash.isEqual,
    )
    : []

  if (props.isRemovedCats === true) {
    return <OrderPriceSheetTableSubtable category={null} {...props} />
  }

  return (
    <div>
      {categoryList.map((item) => (
        <OrderPriceSheetTableSubtable key={item.wholesaleSectionId} category={item} {...props} />
      ))}
    </div>
  )
}

export default React.memo(OrderPriceSheetTableContainer)

const AddNoteModal = ({ visible, onCancel, onOk, note }: any) => {
  const [value, setValue] = useState(note)
  useEffect(() => {
    setValue(note)
  }, [note])
  return (
    <Modal title="Item Notes" visible={visible} onCancel={onCancel} onOk={() => onOk(value)} okText="Save">
      <Input.TextArea value={value} onChange={(e) => setValue(e.target.value)} autoSize={{ minRows: 5, maxRows: 8 }} />
    </Modal>
  )
}
