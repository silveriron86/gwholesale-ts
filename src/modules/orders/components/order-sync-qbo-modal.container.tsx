import React from 'react'
import { Modal, Input, Button, notification, Icon, Spin, Tooltip, Select } from 'antd'
import { cloneDeep, keys } from 'lodash'
import { ColumnProps } from 'antd/es/table'

import { Icon as IconSvg } from '~/components'
import { SaleItem, PriceSheetItem, Order } from '~/schema'
//import { QBOImage, tableCss, EmptyTr } from '~/pricesheet/components/pricesheet-detail-table.style'
//import { DialogHeader, DialogFooter } from '~/pricesheet/components/pricesheet-detail-header.style'

import qboImage from './qbo.png'

import organicImg from '~/modules/inventory/components/organic.png'
import { MutiSortTable } from '~/components'
import { Theme, blue, CACHED_QBO_LINKED, CACHED_NS_LINKED } from '~/common'
import { format } from 'date-fns';
import { formatNumber, displayStatus } from '~/common/utils';
import { Link } from 'react-router-dom';
import { ThemeLink, DialogFooter, DialogHeader, ThemeCheckbox } from '~/modules/customers/customers.style';
import moment from 'moment';
import { EmptyTr } from './order-pricesheet-table.style';
import { QBOImage, NSImage } from './orders-table.style';
import { tableCss } from './order-detail.style';
import { withTheme } from 'emotion-theming';
import { ThemeSelect } from '~/modules/customers/customers.style'

export interface OrderQBOSyncModalProps {
  theme: Theme
  orders: Order[]
  loading: boolean
  sellerSetting: any
  type: string
}

export class OrderQBOSyncModalComponent extends React.PureComponent<
  OrderQBOSyncModalProps & {
    visible: boolean
    onOk: (keys: string[]) => void
    onCancel: () => void
  },
  {
    selectedRowKeys: string[]
  }
> {
  state = {
    selectedRowKeys: [],
    status: this.props.type === 'CUSTOMER' ? "ALL" : "CLOSED",
  }

  private mutiSortTable = React.createRef<any>()


  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onSortVariety = (a: any, b: any) => {
    let stringA = a.variety ? a.variety : ''
    let stringB = b.variety ? b.variety : ''
    stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }

  wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }

  render() {
    const shippedDisplay = displayStatus('SHIPPED', this.props.sellerSetting)
    let { orders, type } = this.props
    orders = orders.filter(el => el.status != 'CANCEL')


    const { status, selectedRowKeys } = this.state
    const noCanceledOrders = status === 'ALL' ? [...orders] : orders.filter(el => {
      if (type === 'CUSTOMER') {
        return el.status === status
      } else {
        return status === 'CLOSED' ? el.isLocked : !el.isLocked
      }
    })

    let columns: ColumnProps<Order>[] = [
      {
        title: 'ID #',
        dataIndex: 'id',
        key: 'id',
        width: 70,
        render: (d) => `# ${d}`,
      },
      {
        title: 'DATE PLACED',
        dataIndex: 'createdDate',
        key: 'createdDate',
        width: 80,
        sorter: (a, b) => a.createdDate - b.createdDate,
        render: (createdDate) => format(createdDate, 'MM/D/YY'),
      },
      {
        title: 'DELIVERY DATE',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        width: 80,
        sorter: (a, b) => a.deliveryDate - b.deliveryDate,
        render: (deliveryDate) => moment.utc(deliveryDate).format('MM/D/YY'),
      },
      {
        title: 'ORDER TOTAL',
        dataIndex: 'totalCostPlusTax',
        key: 'totalCostPlusTax',
        width: 100,
        render(text) {
          return <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}</span>
        },
        sorter: (a, b) => a.totalCostPlusTax - b.totalCostPlusTax,
      },
      {
        title: 'STATUS',
        dataIndex: 'status',
        key: 'status',
        width: 100,
        sorter: (a, b) => a.status.localeCompare(b.status),
        render: (status: string, record: any) => {
          if (type === 'CUSTOMER') {
            return displayStatus(status, this.props.sellerSetting)
          }
          return record.status === 'CANCEL' ? 'CANCEL' : record.isLocked ? 'CLOSED' : 'OPEN'
        }
      }]

    if (localStorage.getItem(CACHED_QBO_LINKED) != 'null')
      columns.push(
        {
          title: 'QBO',
          dataIndex: 'qboId',
          key: 'qboId',
          width: 50,
          sorter: (a, b) => a.qboId - b.qboId,
          render: (text) => {
            if (text) {
              return <Tooltip placement='top' title={text}><QBOImage src={qboImage} /></Tooltip>
            } else {
              return ''
            }
          },
        })
    if (localStorage.getItem(CACHED_NS_LINKED) != 'null')
      columns.push(
        {
          title: 'NS',
          dataIndex: 'nsId',
          key: 'nsId',
          width: 50,
          sorter: (a, b) => a.nsId - b.nsId,
          render: (text) => {
            if (text) {
              return <Tooltip placement='top' title={text}><NSImage /></Tooltip>
            } else {
              return ''
            }
          },
        })
    columns.push(
      {
        title: 'LAST SYNC',
        dataIndex: 'lastQboUpdate',
        key: 'lastQboUpdate',
        width: 80,
        sorter: (a, b) => a.lastQboUpdate - b.lastQboUpdate,
        render: (lastQboUpdate) => moment.utc(lastQboUpdate).format('MM/D/YY'),
      })
    columns.push(
      {
        title: 'COMPANY',
        dataIndex: 'company',
        key: 'company',
        width: 350,
        render: (company) =>
          company && (
            <Link to={
              `${company.type === 'CUSTOMER' ? "/customer/" + company.id + "/orders" : "/vendor/" + company.id + "/orders"}`
            }>
              <span style={{ textDecoration: 'underline', color: blue }}>{company.name}</span>
            </Link>
          ),
        sorter: (a, b) => a.company.name.localeCompare(b.company.name),
      })

    const rowSelection = {
      selectedRowKeys: selectedRowKeys,
      onChange: (keys: any[])=>{
        this.setState({selectedRowKeys: keys})
      }
    }


    return (
      <Modal
        width={'92%'}
        bodyStyle={{ padding: '0' }}
        visible={this.props.visible}
        onCancel={this.props.onCancel}
        footer={[
          <DialogFooter key="footer">
            <Button
              type="primary"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              {this.props.loading ? (
                <span>
                  <Spin /> Loading..
                </span>
              ) : (
                <span>
                  <Icon type="cloud" theme="filled" /> Sync Orders
                </span>
              )}
            </Button>
            <Button onClick={this.props.onCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <DialogHeader style={{paddingTop: 60}}>
          {localStorage.getItem(CACHED_NS_LINKED) != 'null' ? "NS" : "QBO"} Order Sync
        </DialogHeader>
        {type !== 'CUSTOMER' && (
          <div style={{ padding: '0 0 30px 50px' }}>
            Order status
            <ThemeSelect value={status} onChange={(val) => this.setState({ status: val, selectedRowKeys: [] })} style={{ width: 200, marginLeft: 20 }}>
              <Select.Option value="ALL">ALL</Select.Option>
              <Select.Option value="OPEN">OPEN</Select.Option>
              <Select.Option value="CLOSED">CLOSED</Select.Option>
            </ThemeSelect>
          </div>
        )}

        <MutiSortTable
          ref={this.mutiSortTable}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={noCanceledOrders}
          rowKey="id"
          css={tableCss}
          pagination={{ pageSize: 20 }}
        />
      </Modal>
    )
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { selectedRowKeys } = this.state
    if (!selectedRowKeys.length) {
      return notification.error({
        message: 'Select one at least',
      })
    }
    this.props.onOk(selectedRowKeys)
    this.setState({
      selectedRowKeys: [],
    })
  }
}

export const OrderQBOSyncModal = withTheme(OrderQBOSyncModalComponent)
