/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Button, Menu, DatePicker, Modal, Select, message, Icon, Input, Dropdown, Tooltip } from 'antd'
import { Icon as IconSvg } from '~/components'
import { Order } from '~/schema'
import { withTheme } from 'emotion-theming'
import {
  HeaderContainer,
  HeaderTitle,
  HeaderOptions,
  DateEditorWrapper,
  DateTitle,
  DialogFooter,
} from './orders-header.style'
import {Theme, CACHED_QBO_LINKED, CACHED_NS_LINKED, CACHED_USER_ID, CACHED_COMPANY, gray01, red} from '~/common'
import { AuthUser, UserRole } from '~/schema'
import Form, { FormComponentProps } from 'antd/lib/form'
import AutoComplete from 'antd/es/auto-complete'
import { ThemeModal, ThemeButton, ThemeIconButton, ThemeOutlineButton, ThemeSelect } from '~/modules/customers/customers.style'
import jQuery from 'jquery'
import { searchButton } from '~/components/elements/search-button'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import EnterPurchaseModal from './enter-purchase-modal'
import OrderFilterField from './order-filters.component'
import moment from 'moment'
import _ from 'lodash'
import { isSeller, displayStatus } from '~/common/utils'

export interface OrderHeaderProps {
  theme: Theme
  dates: [any, any]
  onShowModal: () => void
  onShowUploadCSV: (mode: string) => void
  onShowAutoAssignContainer: () => void
  onDateChange: (moment: [any, any], dateString: [string, string]) => void
  onSearch: (criteria: string) => void
  currentUser: AuthUser
  onCreate: (client: string) => void
  clients: any[]
  fromPriceSheet?: boolean
  total: any
  onPrintPicks: Function
  onPickDelivery: Function
  onShowBatchPick: Function
  printSetting?: any
  companyProductTypes?: any
  settingCompanyName?: string
  sellerSetting: any
  orders?: Order[]
  setFilters: (filters: any) => void
  filters: any
  getCompanyProductAllTypes: Function
  downloadLotList: Function
  search?: string
  getVendorProductsForEnterPurchase: Function
  enterPurchaseProducts: any[]
}

interface OrdersState {
  createShow: boolean
  type: string
  visibleEnterModal?: boolean
  curPreset: any
  onPresetChange: Function
  searchStr: string
}

export interface NewOrderFormProps {
  visible: boolean
  onOk: (name: string) => void
  onCancel: () => void
  theme?: Theme
  clients: any[]
  okButtonName: string
  isNewOrderFormModal: boolean
  defaultClientId?: number | null
  modalType?: string
}

interface NewOrderFormFields {}

export const presets = [
  {
    key: 0,
    label: 'Past 7 Days',
    from: moment().subtract(7, 'days'),
    to: moment(),
  },
  {
    key: 1,
    label: 'Past 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment(),
  },
  {
    key: 9,
    label: 'Yesterday',
    from: moment().subtract(1, 'days'),
    to: moment().subtract(1, 'days'),
  },
  {
    key: 2,
    label: 'Today',
    from: moment(),
    to: moment(),
  },
  {
    key: 3,
    label: 'Tomorrow',
    from: moment().add(1, 'days'),
    to: moment().add(1, 'days'),
  },
  {
    key: 4,
    label: 'Today and Tomorrow',
    from: moment(),
    to: moment().add(1, 'days'),
  },
  {
    key: 5,
    label: 'Next 7 Days',
    from: moment(),
    to: moment().add(7, 'days'),
  },
  {
    key: 6,
    label: 'Next 30 Days',
    from: moment(),
    to: moment().add(30, 'days'),
  },
  {
    key: 7,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment().add(30, 'days'),
  },
  {
    key: 8,
    label: 'Custom Date Range',
  },
]
export class NewOrderForm extends React.PureComponent<FormComponentProps<NewOrderFormFields> & NewOrderFormProps> {
  state = {
    onSelected: -1,
    displayClients: this.props.clients,
  }

  componentDidUpdate(prevProps: any) {
    if (!this.props.visible) {
      this.props.form.resetFields()
    } else {
      setTimeout(() => {
        const inputCtrl = jQuery('.client-select-auto-complete .ant-select-search__field')
        inputCtrl.trigger('focus')
        // inputCtrl.trigger('click')

        this.focusedItemToTop()
      }, 500)
    }
  }

  focusedItemToTop = () => {
    const dropdown = jQuery('.customer-select-dropdown ul')
    const parent = jQuery(dropdown).parents('.ant-select-dropdown')
    if (!jQuery(parent).hasClass('ant-select-dropdown-hidden')) {
      setTimeout(() => {
        const selectedItem = jQuery('.customer-select-dropdown ul .ant-select-dropdown-menu-item-selected')
        const liEls = jQuery('.customer-select-dropdown ul li')
        const index = jQuery(liEls).index(selectedItem)

        jQuery(dropdown).scrollTop(index * 32) //32 is li height
      }, 100)
    }
  }

  renderCompanyName = (client: { clientId: React.Key | undefined; clientCompanyName: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined }, index: any) => {
    return (
      <Select.Option key={client.clientId} text={client.clientId}>
        {client.clientCompanyName}
      </Select.Option>
    )
  }

  handleFilterOption = (input: string, option: { props: { children: string } }) => {
    return input.length >= 2 && option.props.children.toUpperCase().indexOf(input.toUpperCase()) !== -1
  }

  onSelect = (value: any, option: any) => {
    this.setState(
      {
        onSelected: value,
      },
      () => {
        setTimeout(() => {
          jQuery('.ant-modal-footer .ant-btn-primary').focus()
        }, 500)
      },
    )
  }

  filterDisplayClients = (searchStr: string, defaultClientId: any) => {
    const { clients } = this.props
    const sortedClients = clients.sort((a, b) => a.clientCompanyName.localeCompare(b.clientCompanyName))
    if (defaultClientId) {
      const defaultClientIndex = sortedClients.findIndex((el) => el.clientId == defaultClientId)
      let startIndex = defaultClientIndex > 20 ? defaultClientIndex - 20 : 0
      let endIndex = defaultClientIndex + 40 > sortedClients.length ? sortedClients.length : startIndex + 40
      const result = sortedClients.slice(startIndex, endIndex)
      this.setState({ displayClients: result })
    } else {
      if (searchStr.length > 1) {
        const filtered = sortedClients.filter(
          (el) => el.clientCompanyName.toLowerCase().indexOf(searchStr.toLowerCase()) > -1,
        )
        this.setState({ displayClients: filtered })
      } else {
        this.setState({ displayClients: [] })
      }
    }
  }

  render() {
    const { modalType, fromPriceSheet } = this.props
    const type = fromPriceSheet === true ? 'Customer' : location.href.indexOf('sales-') >= 0 ? 'Customer' : 'Vendor'

    if (modalType === 'order') {
      return (
        <ThemeModal
          className='dark'
          title={`Create a ${type === 'Customer' ? 'sales' : 'purchase'} order`}
          visible={this.props.visible}
          onCancel={this.handleCancel}
          footer={[
            <Flex>
              <ThemeButton className="dark" type="primary" onClick={this.handleOk}>
                Create order
              </ThemeButton>
              <ThemeIconButton className="no-border dark" onClick={this.handleCancel} style={{marginLeft: 24}}>Cancel</ThemeIconButton>
            </Flex>,
          ]}
        >
          {this.getModalContent()}
        </ThemeModal>
      )
    }

    return (
      <Modal
        visible={this.props.visible}
        onCancel={this.handleCancel}
        footer={[
          <DialogFooter key="plus">
            <ThemeButton type="primary" icon="plus" onClick={this.handleOk}>
              {this.props.okButtonName}
            </ThemeButton>
            <Button onClick={this.handleCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        {this.getModalContent()}
      </Modal>
    )
  }

  getModalContent = () => {
    const {
      form: { getFieldDecorator },
      fromPriceSheet,
      clients,
      defaultClientId,
    } = this.props
    const { displayClients } = this.state
    const type = fromPriceSheet === true ? 'Customer' : location.href.indexOf('sales-') >= 0 ? 'Customer' : 'Vendor'
    let defaultClient = null
    if (this.props.defaultClientId) {
      defaultClient = clients.find((el) => {
        return el.clientId == defaultClientId
      })
    }

    return (
      <>
        <label>{type} account</label>
        <Form.Item>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: `Select a ${type.toLowerCase()} account` }],
            initialValue:
              fromPriceSheet === true || !defaultClientId
                ? defaultClient
                  ? defaultClient.clientCompanyName
                  : ''
                : defaultClientId,
          })(
            fromPriceSheet === true || !defaultClientId ? (
              <AutoComplete
                className="client-select-auto-complete input-account"
                style={{ width: 400 }}
                size="large"
                autoFocus={true}
                dataSource={displayClients && displayClients.map(this.renderCompanyName)}
                filterOption={this.handleFilterOption}
                onSelect={this.onSelect}
                onSearch={(v) => this.filterDisplayClients(v, null)}
                onFocus={() => this.filterDisplayClients('', defaultClientId)}
                placeholder={`Search a ${type.toLowerCase()} name`}
              />
            ) : (
              <ThemeSelect
                className="client-select-auto-complete input-account"
                style={{ width: 400 }}
                defaultOpen={true}
                autoFocus={true}
                defaultValue={defaultClientId}
                showSearch
                optionFilterProp="children"
                onChange={this.onSelect}
                onSearch={(v: string) => this.filterDisplayClients(v, null)}
                onFocus={() => this.filterDisplayClients('', defaultClientId)}
                placeholder={`Search a ${type.toLowerCase()} name`}
                filterOption={(input: string, option: any) => {
                  return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }}
                dropdownClassName="customer-select-dropdown"
              >
                {displayClients
                  ? displayClients.map((el) => {
                      return (
                        <Select.Option key={el.clientId} value={el.clientId}>
                          {el.clientCompanyName}
                        </Select.Option>
                      )
                    })
                  : ''}
              </ThemeSelect>
            ),
          )}
        </Form.Item>
        {!this.props.isNewOrderFormModal && this.props.children}
      </>
    )
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onOk } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        if ((this.state.onSelected! = -1 && values.name == this.state.onSelected)) {
          onOk(values.name)
        } else {
          message.error('Please select a client name at list')
        }
      }
    })
  }

  handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onCancel } = this.props
    onCancel()
    form.resetFields()
  }
}

export const NewOrderFormModal = Form.create<FormComponentProps<NewOrderFormFields> & NewOrderFormProps>()(NewOrderForm)

class OrderHeaderComponent extends React.PureComponent<OrderHeaderProps, OrdersState> {
  state = {
    createShow: false,
    type: '',
    visibleEnterModal: false,
    searchStr: this.props.search,
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.search != nextProps.search) {
      this.setState({ searchStr: nextProps.search })
    }
  }

  componentDidMount() {
    // Tab Order
    setTimeout(() => {
      if (jQuery('.tab-able-el').length > 0) {
        jQuery('.tab-able-el input')
          .first()
          .focus()
      }
    }, 1000)

    jQuery('.tab-able-el')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9) {
          let index = jQuery('.tab-able-el').index(e.currentTarget)
          if (e.shiftKey) {
            if (index === 1) {
              e.preventDefault()
              jQuery('.tab-able-el input')
                .first()
                .focus()
            }
          } else {
            if (index === 2) {
              e.preventDefault()
              if (jQuery('.tab-view-link').length > 0) {
                jQuery('.tab-view-link')
                  .first()
                  .focus()
              }
            }
          }
        }
      })
    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.ctrlKey === true /* && e.shiftKey === true*/ && e.keyCode === 78) {
          if (window.location.href.indexOf('/sales-orders') > 0) {
            this.setState({
              createShow: true,
            })
          } else {
            this.setState({
              visibleEnterModal: true,
            })
          }
          return
        }

        if (jQuery(e.target).hasClass('tab-view-link')) {
          e.preventDefault()
          let cards = jQuery('.tab-view-link')
          if (e.keyCode === 9) {
            let tabIndex = jQuery('.tab-view-link').index(e.target)
            if (e.shiftKey) {
              tabIndex--
            } else {
              tabIndex++
            }

            if (tabIndex < 0) {
              let lastBtn = jQuery('.tab-able-el')[2]
              jQuery(lastBtn).focus()
            } else if (tabIndex >= cards.length) {
              jQuery('.tab-able-el input')
                .first()
                .focus()
              jQuery('html, body').animate(
                {
                  scrollTop: 0,
                },
                100,
              )
            } else {
              jQuery(cards[tabIndex]).focus()
            }
          } else if (e.keyCode === 13) {
            window.location.href = e.target.href
          }
        }
      })

    location.href.indexOf('sales-') >= 0 ? this.setState({ type: 'sales' }) : this.setState({ type: 'purchase' })
  }

  renderQboSyncButton = () => {
    const qboRealmId = localStorage.getItem(CACHED_QBO_LINKED) !== 'null'
    if (qboRealmId)
      return (
        <div>
          <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
          QBO Sync
        </div>
      )
    return <span />
  }

  renderNsSyncButton = () => {
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    if (nsRealmId)
      return (
        <div>
          <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
          NS Sync
        </div>
      )
    return <span />
  }

  onToggleEnterModal = () => {
    this.setState({
      visibleEnterModal: !this.state.visibleEnterModal,
    })

    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.altKey === true && e.shiftKey === true && e.keyCode === 65) {
          this.setState({
            visibleEnterModal: true,
          })
        }
      })
  }

  onStartPurchase = () => {
    this.setState(
      {
        visibleEnterModal: false,
      },
      () => {
        this.setState({
          createShow: true,
        })
      },
    )
  }

  onEnterPurchase = (clientId: any, data: any) => {
    this.setState(
      {
        visibleEnterModal: false,
      },
      () => {
        this.props.finalizePurchaseOrder({
          clientId,
          values: data,
        })
      },
    )
    return <span />
  }

  onPresetChange = (val: number) => {
    const type = location.href.indexOf('sales-') >= 0 ? 'Sales' : 'Purchase'
    if(type == 'Sales'){
      localStorage.setItem("saleFilter","0")
    }
    if(type == 'Purchase'){
      localStorage.setItem("purchaseFilter","0")
    }
    const selected = presets.find((p) => p.key === val)
    this.props.onPresetChange(val, selected)
  }

  onSearchTextChange = (evt: any) => {
    this.setState({ searchStr: evt.target.value })
  }

  onSearch = (val: string) => {
    this.setState({ searchStr: val }, () => {
      this.props.onSearch(val)
    })
  }

  render() {
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED) !== 'null'
    const { visibleEnterModal, searchStr } = this.state
    const { currentUser, total, printSetting, clients, setFilters, filters } = this.props
    const userPrintSetting = JSON.parse(printSetting)
    const type = location.href.indexOf('sales-') >= 0 ? 'Sales' : 'Purchase'
    const isSellerRole = isSeller(currentUser.accountType)

    const userId = localStorage.getItem(CACHED_USER_ID)
    const companyId = localStorage.getItem(CACHED_COMPANY)
    const warningTooltip =
      total >= 30 ? (
        <Tooltip
          title="Due to the large number of records, this feature may not work as expected"
          placement="bottomLeft"
        >
          <Icon type="warning" />
        </Tooltip>
      ) : null

    return (
      <HeaderContainer>
        {type === 'Sales' && isSellerRole && (
          <div style={{ position: 'absolute', top: 10, zIndex: 1000, right: 25 }}>
            <Flex>
              <ThemeOutlineButton
                size="large"
                type="primary"
                style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }}
              >
                Actions
              </ThemeOutlineButton>
              <Dropdown
                overlay={
                  <Menu onClick={this.onActions}>
                    {currentUser.accountType !== UserRole.WAREHOUSE &&
                    <Menu.Item key="1" hidden={
                      ((companyId == 'Growers Produce' && !(userId == '105629' || userId == '116137' || userId == '110151' || userId ==  '97960' || userId == '110154' || userId == '110148' || userId == '110149')) ||
                      (companyId == 'PayLess Logistics' && !(userId == '105120' || userId == '105165' || userId == '105159' || userId == '105160')))
                    }>Sync to {nsRealmId ? 'NS' : 'QBO'} </Menu.Item>
                    }
                    {userPrintSetting &&
                      (typeof userPrintSetting.pick_enabled === 'undefined' || userPrintSetting.pick_enabled) && (
                        <Menu.Item key="2">
                          Print{' '}
                          {userPrintSetting && userPrintSetting.pick_title ? userPrintSetting.pick_title : 'Pick Sheet'}
                          s {warningTooltip}
                        </Menu.Item>
                      )}
                    <Menu.Item key="4" style={{ textTransform: 'capitalize' }}>
                      Print Batch Pick
                    </Menu.Item>
                    {userPrintSetting &&
                      (typeof userPrintSetting.invoice_enabled === 'undefined' || userPrintSetting.invoice_enabled) && (
                        <Menu.Item key="3" style={{ textTransform: 'capitalize' }}>
                          Print{' '}
                          {userPrintSetting && userPrintSetting.invoice_title
                            ? userPrintSetting.invoice_title
                            : 'Invoice'}
                          s {warningTooltip}
                        </Menu.Item>
                      )}
                    {currentUser.accountType !== UserRole.WAREHOUSE &&
                      <Menu.Item key="5" style={{ textTransform: 'capitalize' }}>
                        Import Sales Order
                      </Menu.Item>
                    }
                    <Menu.Item key="6" style={{ textTransform: 'capitalize' }}>
                      Auto-assign items to containers
                    </Menu.Item>
                  </Menu>
                }
                trigger={['click']}
                placement="bottomRight"
              >
                <ThemeOutlineButton
                  size="large"
                  type="primary"
                  style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
                >
                  <Icon type="down" />
                </ThemeOutlineButton>
              </Dropdown>
              {currentUser.accountType !== UserRole.WAREHOUSE &&
              <ThemeButton
                className="icon enter-purchase"
                size="large"
                type="primary"
                onClick={this.onClickShow}
              >
                <Icon type="plus-circle" style={{ fontSize: 20 }} />
                New Order
              </ThemeButton>
              }
            </Flex>
          </div>
        )}
        <HeaderTitle>
          {type === 'Sales' ? (isSellerRole ? 'Sales Orders' : 'Order History') : 'Purchase Orders'}
        </HeaderTitle>

        <HeaderOptions style={{ alignItems: 'end'}}>
          {isSellerRole && (
            <div style={{ width: '30%' }}>
              <DateTitle style={{ visibility: 'hidden' }}>SEARCH {type.toUpperCase()} ORDERS</DateTitle>
              <Input.Search
                placeholder={`Search order ID, ${type.toLowerCase()} name, status, ${
                  type == 'Sales' ? 'shipping' : 'delivery'
                } address`}
                size="large"
                defaultValue={searchStr}
                onChange={this.onSearchTextChange}
                value={searchStr}
                style={{ border: '0'}}
                enterButton={searchButton}
                onSearch={this.props.onSearch}
                allowClear
              />
            </div>
          )}
          <Flex style={{ flex: 1, justifyContent: 'flex-start' }}>
            {this.renderDateEditor()}
            <div style={{ marginTop: 34, marginLeft: 15, whiteSpace: 'nowrap', color: gray01}}>
              {total} record{total > 1 ? 's' : ''}
            </div>
          </Flex>

          {type !== 'Sales' && (
            <div style={{ width: '40%' }}>
              <OrderFilterField setFilters={setFilters} filters={filters} />
            </div>
          )}

          <NewOrderFormModal
            theme={this.props.theme}
            visible={this.state.createShow}
            onOk={this.onClickCreate}
            onCancel={this.onCloseNewOrder}
            clients={clients}
            okButtonName="CREATE"
            modalType="order"
            isNewOrderFormModal={true}
          />
        </HeaderOptions>
        <div style={{ position: 'absolute', top: 10, zIndex: 1000, right: 25 }}>
          {_.indexOf([UserRole.SALES, UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN], currentUser.accountType) >
          -1 ? (
            <span>
              {type === 'Purchase' && (
                <Flex style={{ justifyContent: 'flex-end' }}>
                  <Flex>
                    <ThemeOutlineButton
                      size="large"
                      type="primary"
                      style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }}
                    >
                      Actions
                    </ThemeOutlineButton>
                    <Dropdown
                      overlay={
                        <Menu onClick={this.onClickPOListMenuButton}>
                          <Menu.Item key="1" hidden={
                            ((companyId == 'Growers Produce' && !(userId == '105629' || userId == '116137' || userId == '110151' || userId ==  '97960' || userId == '110154' || userId == '110148' || userId == '110149')) ||
                            (companyId == 'PayLess Logistics' && !(userId == '105120' || userId == '105165' || userId == '105159' || userId == '105160')))
                          }>
                            {this.renderQboSyncButton()}
                            {this.renderNsSyncButton()}
                          </Menu.Item>
                          <Menu.Item key="2">Export Lots List</Menu.Item>
                          <Menu.Item key="3">Import Purchase Orders</Menu.Item>
                        </Menu>
                      }
                      trigger={['click']}
                      placement="bottomRight"
                    >
                      <ThemeOutlineButton
                        size="large"
                        type="primary"
                        style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
                      >
                        <Icon type="down" />
                      </ThemeOutlineButton>
                    </Dropdown>
                  </Flex>
                  <ThemeButton
                    className="icon enter-purchase"
                    size="large"
                    type="primary"
                    onClick={this.onToggleEnterModal}
                  >
                    <Icon type="plus-circle" style={{ fontSize: 20 }} />
                    Enter New Purchase
                  </ThemeButton>
                  <Dropdown
                    overlay={
                      <Menu onClick={this.onClickShow}>
                        <Menu.Item key="1">Create Purchase Order</Menu.Item>
                      </Menu>
                    }
                    trigger={['click']}
                    placement="bottomRight"
                  >
                    <ThemeButton className="icon border-noleft" size="large" type="primary">
                      <Icon type="caret-right" rotate={90} />
                    </ThemeButton>
                  </Dropdown>
                  {visibleEnterModal && (
                    <EnterPurchaseModal
                      {...this.props}
                      visible={visibleEnterModal}
                      onToggle={this.onToggleEnterModal}
                      onStartPurchase={this.onStartPurchase}
                      onEnterPurchase={this.onEnterPurchase}
                    />
                  )}
                </Flex>
              )}
            </span>
          ) : (
            <span />
          )}
        </div>
      </HeaderContainer>
    )
  }

  onChangeStatuses = (status: string[]) => {
    this.props.setSOStatus(status);
  };


  private renderDateEditor = () => {
    const calendarIcon = <IconSvg type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    const { dates, curPreset, status, sellerSetting, currentUser } = this.props
    const { RangePicker } = DatePicker
    const shippedDisplay = displayStatus('SHIPPED', sellerSetting)

    const type = location.href.indexOf('sales-') >= 0 ? 'Sales' : 'Purchase'
    return (
      <Flex style={{marginLeft: 30}}>
        <DateEditorWrapper className="orders-delivery-date" style={{ minWidth: 230 }}>
          {curPreset == 8 && (
            <RangePicker
              // placeholder={dateFormat}
              defaultValue={[dates[0], dates[1]]}
              format={dateFormat}
              suffixIcon={calendarIcon}
              onChange={this.props.onDateChange}
              className="tab-able-el"
              allowClear={false}
            />
          )}
          <Select
            defaultValue={3}
            value={curPreset}
            className="date-filter-select"
            onChange={this.onPresetChange}
            dropdownClassName="full-select"
          >
            {presets.map((el: any) => {
              return (
                <Select.Option value={el.key} key={el.key}>
                  {el.label}
                </Select.Option>
              )
            })}
          </Select>
          <DateTitle className='orders-list'>
            {type === 'Sales' ? 'Target fulfillment date' : 'Scheduled Delivery Date'}
          </DateTitle>
        </DateEditorWrapper>
        {type === 'Sales' && (
          <div style={{display: 'flex', flexDirection: 'column', marginLeft: 15}}>
            <DateTitle className='orders-list'>
              Order status
            </DateTitle>
            <Select
              placeholder="All order statuses"
              mode="multiple"
              className="multi-selector-32 order-status-filter"
              value={status}
              onChange={this.onChangeStatuses}
            >
              <Select.Option value='NEW'>NEW</Select.Option>
              {(currentUser.accountType == UserRole.WAREHOUSE ||
                (sellerSetting && sellerSetting.company && sellerSetting.company.warehousePickEnabled)) && (
                <Select.Option value='PICKING'>PICKING</Select.Option>
                )}
              <Select.Option value='SHIPPED'>{shippedDisplay.toUpperCase()}</Select.Option>
              <Select.Option value='CANCEL'>CANCELED</Select.Option>
            </Select>
          </div>
        )}
      </Flex>
    )
  }

  private onClickShow = () => {
    this.setState({
      createShow: true,
    })
  }

  private onClickPOListMenuButton = ({ key }: any) => {
    if (key == '1') {
      this.props.onShowModal()
    } else if (key == '2') {
      this.props.downloadLotList()
    } else if (key == '3') {
      this.props.onShowUploadCSV("PO")
    }
}

  private onCloseNewOrder = () => {
    this.setState({
      createShow: false,
    })
  }

  private onClickCreate = (clientName: string) => {
    if (clientName) {
      this.props.onCreate(clientName)
    }
    this.setState({
      createShow: false,
    })
  }

  private onActions = (item: any) => {
    const { key } = item
    if (key === '1') {
      this.props.onShowModal()
    } else if (key === '2') {
      this.props.onPrintPicks()
    } else if (key === '3') {
      this.props.onPickDelivery()
    } else if (key === '4') {
      this.props.onShowBatchPick()
    } else if (key === '5') {
      this.props.onShowUploadCSV("Sales")
    } else if (key === '6') {
      this.props.onShowAutoAssignContainer()
    }
  }
}

export const OrderHeader = withTheme(OrderHeaderComponent)
