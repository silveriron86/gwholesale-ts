/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Link } from 'react-router-dom'
import { Icon, Tooltip, Button, Badge } from 'antd'
import { ColumnProps } from 'antd/es/table'
import { format } from 'date-fns'
import moment from 'moment'
import { Order, AuthUser, UserRole } from '~/schema'
import { tableCss } from './order-detail.style'
import { QBOImage, NSImage, OrdersListTable } from './orders-table.style'
import { Icon as IconSVG } from '~/components/icon'
import qboImage from './qbo.png'
import nsImage from './netsuite.svg'
import { blue, CACHED_QBO_LINKED, CACHED_NS_LINKED, CACHED_ACCOUNT_TYPE, gray01 } from '~/common'
import {
  formatDate,
  formatNumber,
  getOrderPrefix,
  isSeller,
  formatOrderStatus,
  isCustomOrderNumberEnabled,
} from '~/common/utils'
import { ThemeLink, TextLink, Flex } from '~/modules/customers/customers.style'
import { History } from 'history'
import { MultiBackSortTable } from '~/components/multi-back-sortable'
import { produce } from 'immer'
import { remove } from 'lodash'
import MinProductTable from './mini-product-table'

export interface OrderTableProps {
  orders: Order[]
  tableMini: boolean
  currentUser?: AuthUser
  history: History
  currentPage: any
  pageSize: any
  total: any
  changePage: Function
  loading: boolean
  curSortKey: string
  sellerSetting?: any
  getOrderItemsByOrderIdForPOPage: Function
  getOrderItemsByOrderIdsForPOPage: Function
}

interface OrderTableState {
  expandAll: boolean
  expandedRowKeys: string[]
}

export class OrderTable extends React.PureComponent<OrderTableProps, OrderTableState> {
  columns: ColumnProps<Order>[] = [
    {
      title: 'ORDER NO.',
      dataIndex: 'id',
      key: 'wholesaleOrderId',
      width: 150,
      className: 'no-pad-td pl-90',
      sorter: true,
      render: (d, record) => {
        return <span className='bold'>{this.getTextLink(record, d, true)}</span>
      },
    },
    {
      title: 'ORDER DATE',
      dataIndex: 'orderDate',
      key: 'orderDate',
      width: 80,
      sorter: true,
      className: 'no-pad-td',
      render: (val: number, record: any) => {
        if (record.orderDate === null) {
          return '';
        }
        return this.getTextLink(record, moment(record.orderDate).format('M/D/YY'))
      }
    },
    {
      title: 'FULFILLMENT DATE',
      dataIndex: 'deliveryDate',
      key: 'deliveryDate',
      width: 80,
      sorter: true,
      className: 'no-pad-td',
      render: (val, record) => {
        return <span className='bold'>{this.getTextLink(record, formatDate(val, true))}</span>
      }
    },
    {
      title: 'ORDER TOTAL',
      dataIndex: !isPurchase() ? 'totalCostPlusTax' : 'totalCost',
      key: !isPurchase() ? 'totalCostPlusTax' : 'totalCost',
      width: 100,
      className: 'no-pad-td',
      render: (text, record: any) => {
        if (isPurchase()) {
          if (record.isApprox > 0) {
            return <Tooltip placement="bottom" title={'Approximate values used for catchweight items not yet received, based on approximate ratio'}>
              {this.getTextLink(record, <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}*</span>)}
            </Tooltip>
          }
        }
        return this.getTextLink(record, <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}</span>)
      },
      sorter: !isPurchase() ? (a, b) => a.totalCostPlusTax - b.totalCostPlusTax : (a, b) => a.totalCost - b.totalCost,
    },
    {
      title: 'STATUS',
      dataIndex: 'status',
      key: !isPurchase() ? 'wholesaleOrderStatus' : 'isLocked',
      width: 100,
      sorter: true,
      className: 'no-pad-td',
      render: (status, record: any) => {
        if (!isPurchase()) {
          return this.getTextLink(record, formatOrderStatus(status, this.props.sellerSetting).toUpperCase())
        }

        // const showWarning = record.orderItems.some((o: any) => o.wholesaleItem && o.receivedQty !== o.quantity)
        const statusLabel = record.status == 'CANCEL' ? 'CANCEL' : record.isLocked ? 'CLOSED' : 'OPEN'
        return (
          <>
            {this.getTextLink(record, statusLabel)}
            {/* {showWarning && (
              <Tooltip placement="right" title="Receive discrepancy. Click for details">
                <Icon
                  onClick={() => this.onExpand({ expanded: false, record })}
                  style={{ color: '#8D0C04', marginLeft: '8px' }}
                  type="warning"
                />
              </Tooltip>
            )} */}
          </>
        )
      },
    },
  ]
  customerColumns: ColumnProps<Order>[] = [
    {
      title: 'ID #',
      dataIndex: 'id',
      key: 'id',
      className: 'no-pad-td pl-90',
      render: (d, record: any) => this.getTextLink(record, d, true),
    },
    {
      title: 'ORDER DATE',
      dataIndex: 'orderDate',
      key: 'orderDate',
      sorter: (a, b) => a.updatedDate - b.updatedDate,
      className: 'no-pad-td',
      render: (orderDate, record: any) => this.getTextLink(record, moment(orderDate).format('M/D/YY')),
    },
    {
      title: 'DELIVERY DATE',
      dataIndex: 'deliveryDate',
      key: 'deliveryDate',
      sorter: true,
      className: 'no-pad-td',
      render: (deliveryDate, record: any) => this.getTextLink(record, moment.utc(deliveryDate).format('M/D/YY')),
    },
    {
      title: 'TOTAL SALE PRICE',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      className: 'no-pad-td',
      render: (text, record: any) => {
        return this.getTextLink(record, <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}</span>)
      },
      sorter: true,
    },
    {
      title: 'CUSTOMER',
      dataIndex: 'firstName',
      key: 'firstName',
      sorter: true,
      className: 'no-pad-td',
      render: (_, order, __) => {
        let content = null
        if (order.firstName) {
          content = (
            <span>
              {order.firstName} {order.lastName}
              <br />
              {order.company.name}
            </span>
          )
        } else {
          content = <span>{order.company.name}</span>
        }
        return this.getTextLink(order, content)
      },
    },
    {
      key: 'view',
      render: (order) => (
        <Link to={`/order/${order.linkToken}`}>
          VIEW <Icon type="arrow-right" />
        </Link>
      ),
    },
  ]

  miniColumns: ColumnProps<Order>[] = [
    {
      title: 'ID #',
      dataIndex: 'id',
      key: 'id',
      className: 'no-pad-td pl-90',
      render: (d, order) => this.getTextLink(order, d, true),
    },
    {
      title: 'ORDER DATE',
      dataIndex: 'orderDate',
      key: 'orderDate',
      sorter: true,
      className: 'no-pad-td',
      render: (orderDate, order) => this.getTextLink(order, moment(orderDate).format('M/D/YY')),
    },
    {
      title: 'TOTAL SALE PRICE',
      dataIndex: 'totalPrice',
      key: 'totalPrice',
      className: 'no-pad-td',
      render: (text, order) => {
        return this.getTextLink(order, <span style={{ fontWeight: 700 }}>{`$${formatNumber(text, 2)}`}</span>)
      },
      sorter: true,
    },
    {
      title: 'CUSTOMER',
      dataIndex: 'customer',
      key: 'customer',
      render: (customer) =>
        customer && (
          <Link to={`/customers/${customer.id}`}>
            <span style={{ textDecoration: 'underline', color: blue }}>{customer.name}</span>
          </Link>
        ),
      sorter: true,
    },
  ]

  constructor(props: OrderTableProps) {
    super(props)
    this.state = {
      expandAll: false,
      expandedRowKeys: [],
    }
  }

  UNSAFE_componentWillUpdate(nextProps: OrderTableProps) {
    if (nextProps.currentPage !== this.props.currentPage) {
      this.setState({ expandedRowKeys: !this.state.expandAll ? [] : nextProps.orders.map((o) => o.id) })
    }
  }

  handleTableChange = (page: any, filters: any, sorter: any) => {
    this.props.changePage(page, sorter)
  }

  getTextLink = (order: any, content: any, isIdColumn: boolean = false) => {
    const { sellerSetting, currentUser } = this.props
    const isSellerRole = isSeller(currentUser?.accountType)
    let prefix: any = ''
    let br: any = ''
    let resultContent: any = content
    let isCustomOrderNo = false
    if (sellerSetting && isIdColumn) {
      isCustomOrderNo =
        isCustomOrderNumberEnabled(sellerSetting) && order.customOrderNo && order.company.type === 'CUSTOMER'
      if (isCustomOrderNo) {
        prefix = (
          <span style={{ whiteSpace: 'nowrap' }}>{`${getOrderPrefix(sellerSetting, 'sales')}${
            order.customOrderNo
          }`}</span>
        )
        br = <br />
        resultContent = (
          <span
            style={{ fontSize: '0.9em', marginLeft: 10, color: 'grey', whiteSpace: 'nowrap' }}
          >{`(WSW-${content})`}</span>
        )
      } else {
        resultContent = content
        if (order.priceSheetId) {
          prefix = 'E-Commerce-'
        } else {
          if (order.company.type === 'CUSTOMER') {
            prefix = getOrderPrefix(sellerSetting, 'sales')
          } else if (order.company.type === 'VENDOR') {
            prefix = getOrderPrefix(sellerSetting, 'purchase')
          }
        }
      }
    }
    if (isSellerRole) {
      return (
        <TextLink
          className="text"
          to={`${
            order.company.type === 'CUSTOMER' ? '/sales-order/' + order.id : '/order/' + order.id + '/purchase-cart'
          }`}
        >
          <Badge
            title={
              order.company.type === 'CUSTOMER' && isIdColumn && order.creditMemoCount
                ? 'A credit memo has been added to this order'
                : ''
            }
            count={order.company.type === 'CUSTOMER' && isIdColumn ? order.creditMemoCount : 0}
            offset={[0, -5]}
          >
            <span>
              {prefix}
              {br}
              {resultContent}
            </span>
          </Badge>
        </TextLink>
      )
    } else {
      return (
        <span>
          {prefix}
          {br}
          {resultContent}
        </span>
      )
    }
  }

  addEndingColumns = () => {
    let hasEndingCols = false
    this.columns.forEach((element) => {
      if (element.dataIndex === 'company') {
        hasEndingCols = true
        return
      }
    })
    if (hasEndingCols) {
      return
    }
    if (localStorage.getItem(CACHED_QBO_LINKED) !== 'null')
      this.columns.push({
        title: 'QBO',
        dataIndex: 'qboId',
        key: 'qboId',
        width: 50,
        sorter: true,
        className: 'no-pad-td',
        render: (qboId: string, record: any) => {
          if (typeof qboId !== 'undefined' && qboId !== null) {
            const act = record.company.type === 'CUSTOMER' ? 'invoice' : 'bill'
            const syncedItems = Object.values(JSON.parse(qboId)).length
            if (act === 'bill') {
              return (
                <Flex>
                  <a href={`https://qbo.intuit.com/app/${act}?txnId=${qboId}`} target="_blank">
                    <QBOImage src={qboImage} />
                  </a>
                  {syncedItems != parseInt(record.itemsCount, 10) && (
                    <Tooltip title="This order is partically synced. Some items from this order have not yet been synced.">
                      <IconSVG className="qbo-warning" type="warning" viewBox="0 0 24 24" width={24} height={24} />
                    </Tooltip>
                  )}
                </Flex>
              )
            }
            return (
              <a href={`https://qbo.intuit.com/app/${act}?txnId=${qboId}`} target="_blank">
                <QBOImage src={qboImage} />
              </a>
            )
          } else {
            return this.getTextLink(record, '')
          }
        },
      })

    if (localStorage.getItem(CACHED_NS_LINKED) != 'null')
      this.columns.push({
        title: 'NS',
        dataIndex: 'nsId',
        key: 'nsId',
        width: 50,
        sorter: true,
        render: (text, record) => {
          const nsRealmId = localStorage.getItem(CACHED_NS_LINKED)
          if (text) {
            const act = record.company.type === 'CUSTOMER' ? 'custinvc.nl' : 'vendbill.nl'
            return (
              <a
                href={`https://${nsRealmId}.app.netsuite.com/app/accounting/transactions/${act}?id=${text}`}
                target="_blank"
              >
                <NSImage src={nsImage} />
              </a>
            )
          } else {
            return ''
          }
        },
      })

    if (!isPurchase()) {
      this.columns.push({
        title: 'SALES REP',
        key: 'salesRep',
        width: 100,
        sorter: true,
        render: (order) => {
          return order?.salesRep !=  null  ? (<span>{order.salesRep}</span>) :  ''
        },
      })
      this.columns.push({
        title: 'ROUTE',
        dataIndex: 'routeName',
        key: 'routeName',
        width: 100,
        sorter: true,
      })
    }
    this.columns.push({
      key: 'view',
      width: 100,
      render: (order) => (
        <ThemeLink
          className="tab-view-link"
          to={`${
            order.company.type === 'CUSTOMER' ? '/sales-order/' + order.id : '/order/' + order.id + '/purchase-cart'
          }`}
        >
          VIEW <Icon type="arrow-right" />
        </ThemeLink>
      ),
    })
  }

  onExpand = ({ expanded, record }: { expanded: boolean; record: any }): void => {
    if (expanded) {
      this.setState(
        produce<OrderTableState>((draft) => {
          remove(draft.expandedRowKeys, (id) => id === record.id)
        }),
      )
    } else {
      this.setState(
        produce<OrderTableState>((draft) => {
          draft.expandedRowKeys.push(record.id)
        }),
      )
      if (!record.fetched) {
        this.props.getOrderItemsByOrderIdForPOPage(record.id)
      }
    }
  }

  handleExpandAll = (): void => {
    const { orders } = this.props
    const { expandAll } = this.state
    const orderIds = expandAll ? [] : orders.map((o) => o.id)
    this.setState(
      produce<OrderTableState>((draft) => {
        draft.expandAll = !expandAll
        draft.expandedRowKeys = orderIds
      }),
    )
    if (!expandAll) {
      this.props.getOrderItemsByOrderIdsForPOPage(orderIds.join(','))
    }
  }

  render() {
    const { currentPage, pageSize, total, orders, tableMini, currentUser } = this.props
    const { expandedRowKeys, expandAll } = this.state
    const isSellerRole = isSeller(currentUser.accountType)
    const accountType = localStorage.getItem(CACHED_ACCOUNT_TYPE)
    this.addEndingColumns()

    const companyColumn = {
      title: 'COMPANY',
      dataIndex: 'company',
      key: 'wholesaleClient.clientCompany.companyName',
      width: 300,
      render: (company) =>
        company && (
          <span className='bold'>
            <Link
              to={`${
                company.type === 'CUSTOMER' ? '/customer/' + company.id + '/orders' : '/vendor/' + company.id + '/orders'
              }`}
            >
              <span style={{ textDecoration: 'underline', color: blue }}>{company.name}</span>
            </Link>
          </span>
        ),
      sorter: true,
    }

    if (isPurchase()) {
      if (this.columns[5].dataIndex === 'pas') {
        this.columns.splice(5, 1)
      }
      if (this.columns[1].dataIndex === 'po') {
        this.columns.splice(1, 1)
      }
      this.columns[2].title = 'DELIVERY DATE'
      this.columns.splice(1, 0, {
        title: 'VENDOR REF',
        dataIndex: 'po',
        width: 80,
        key: 'po',
        sorter: true,
        className: 'no-pad-td',
        render: (d, order) => this.getTextLink(order, `${d ? d : ''}`),
      })
    } else {
      if (this.columns[1].dataIndex === 'reference') {
        this.columns.splice(1, 1)
      }
      
      this.columns.splice(1, 0, {
        title: this.props.currentUser.company === 'Fresh Green' ? 'RECEIPT NO.' : 'CUST. PO',
        dataIndex: 'reference',
        width: 100,
        key: 'reference',
        sorter: true,
        className: 'no-pad-td',
        render: (v, order) => this.getTextLink(order, v),
      })      
    }
    if(this.columns[2].dataIndex !== 'company') {
      this.columns.splice(2, 0, companyColumn)
    }
    if (isPurchase() && this.columns[5].dataIndex != 'pasTotal') {
      this.columns.splice(5, 0, {
        title: <Tooltip title="Priced after sale">PAS</Tooltip>,
        dataIndex: 'pasTotal',
        key: 'pasTotal',
        width: 80,
        sorter: false,
        className: 'no-pad-td',
        render: (value: number, record: any) => {
          // const pasItems =
          //   record.orderItems && Array.isArray(record.orderItems)
          //     ? record.orderItems.filter((el: any) => {
          //         return el.pas === true
          //       })
          //     : []
          const pas = typeof value !== 'undefined' ? value : 0;
          return this.getTextLink(record, `${pas} ${pas <= 1 ? 'item' : 'items'}`)
        },
      })
    }

    if (accountType === UserRole.WAREHOUSE) {
      const found = this.columns.findIndex(col => {
        return col.dataIndex === 'totalCost' || col.dataIndex === 'totalCostPlusTax'
      });
      if (found >= 0) {
        this.columns.splice(found, 1);
      }
    }

    const orderColumns =
      this.props.currentUser != null && isSeller(this.props.currentUser.accountType)
        ? this.columns
        : this.customerColumns

    const options: any = {
      pagination: {
        total: total,
        pageSize: pageSize,
        current: currentPage,
        defaultCurrent: 1,
      },
      columns: orderColumns,
      dataSource: orders,
    }

    if (tableMini) {
      options.pagination = false
      options.columns = this.miniColumns
      options.scroll = { y: 627 }
    }

    if (isPurchase()) {
      options.expandedRowKeys = expandedRowKeys
      options.expandIconAsCell = false
      options.expandedRowRender = (record) => <MinProductTable record={record} expandAll={expandAll} />
      options.expandIcon = ({ expanded, record }) => (
        <Icon type={`caret-${expanded ? 'down' : 'right'}`} onClick={() => this.onExpand({ expanded, record })} />
      )
    } else {
      if (isSellerRole) {
        options.onRow = (record: any) => {
          return {
            onClick: () => {
              this.props.history.push(`/sales-order/${record.id}`)
            },
          }
        }
      }
    }

    return (
      <>
        {!!orders.length && isPurchase() && (
          <div style={{ textAlign: 'left', paddingLeft: '70px' }}>
            <Button
              type="link"
              style={{ color: gray01 }}
              icon={expandAll ? 'shrink' : 'arrows-alt'}
              onClick={this.handleExpandAll}
            >
              {expandAll ? 'Collapse' : 'Expand'} all
            </Button>
          </div>
        )}
        <OrdersListTable>
          <MultiBackSortTable
            {...options}
            rowKey="id"
            css={tableCss(false)}
            loading={this.props.loading}
            onChange={this.handleTableChange}
            onSort={this.handleTableChange}
          />
        </OrdersListTable>
      </>
    )
  }
}

const isPurchase = (): boolean => {
  return window.location.href.indexOf('/purchase-orders') > 0
}
