import styled from '@emotion/styled'
import { black,gray01 } from '~/common'
import nsImage from '~/modules/orders/components/netsuite.svg'

export const Price = styled('span')({
  fontFamily: '"Museo Sans Rounded"',
  color: gray01,
  fontSize: '18px',
})

export const NSImage: any = styled(nsImage)((props) => ({
  width: '24px',
  height: '24px',
}))

export const QBOImage: any = styled('img')((props) => ({
  width: '24px',
  height: '24px',
  backgroundColor: props.theme.primary,
  borderRadius: 12,
}))

export const OrdersListTable: any = styled('div')((props) => ({
  '.ant-table tbody tr td:first-of-type': {
    '.ant-badge-count': {
      minWidth: 14,
      height: 14,
      padding: '0 3px',
      fontSize: 10,
      lineHeight: '14px',
      cursor: 'default',
      '.ant-scroll-number-only': {
        height: 14,
        'p.ant-scroll-number-only-unit': {
          height: 14
        }
      }
    }
  }
}))

export const EmptyTr = styled('tr')({
  height: '15px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})
