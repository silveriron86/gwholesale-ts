import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { GlobalState } from '~/store/reducer'
import { Icon, notification } from 'antd'

import { OrdersDispatchProps, OrdersModule, OrdersStateProps } from './orders.module'
import {
  AddressWrapper,
  BackButton,
  Container,
  ExtraWrap,
  Flex,
  Header,
  Info,
  InfoItem,
  InfoItemLabel,
  InfoItemText,
  InfoWrap,
  MainInfoItem,
  StatusWrapper,
} from './order-detail.style'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import { OrderDetailContent } from './components'
import { Icon as IconSvg, MessageHeader, MessageType } from '~/components'
import { formatAddress, formatNumber } from '~/common/utils'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { history } from '~/store/history'
import { CompanyInfoWrapper, LogoWrapper } from './components/OrderTableHeader/styles'
import moment from 'moment'

export type OrderDetailProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string; success: string }> & {
    theme: Theme
  }

export class OrdersDetailComponent extends React.PureComponent<OrderDetailProps> {
  state = {
    message: '',
    type: null,
    isEditing: false,
    emailText: '',
    email: '',
    showEmailModal: false,
  }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Coming Soon!',
      onClick: () => {
        //console.log('Notification Clicked!')
      },
    })
  }

  startEditOrder = () => {
    this.setState({ isEditing: true })
  }

  componentDidMount() {
    const id = this.props.match.params.orderId
    if (this.props.match.params.success != null) {
      this.props.loadIndex('')
      this.setState({
        message: 'Order placement successful',
        type: MessageType.SUCCESS,
      })
    }
    this.props.getOrderTokenDetail(id)
    this.props.getOrderItems(id)
    //TODO: find a way to disable below request if not login
    // this.props.getCustomers()
    // this.props.getSystemCustomers()
  }

  handleTextChange = (e: any) => {
    this.setState({ emailText: e.target.value })
  }

  sendEmail = (data: any) => {
    const { currentOrder } = this.props
    const customer = currentOrder ? currentOrder.wholesaleClient : null
    console.log(customer)
    if (customer != null) {
      this.props.sendEmail({
        customer: {
          emailAddress: customer.mainContact.email,
        },
        priceSheetId: 9,
        message: data.message,
      })
    }
  }

  toggleEmailModal = () => {
    this.setState({
      showEmailModal: !this.state.showEmailModal,
    })
  }

  onPrint = () => {
    const id = this.props.match.params.orderId
    history.push(`/orders/${id}/print`)
  }

  backToPricesheetList = () => {
    history.push(`/pricesheets`)
  }

  render() {
    const { currentOrder, wholesaleClient } = this.props
    if (!currentOrder) {
      return 'loading'
    }
    return (
      <div>
        {this.renderHeader()}
        <OrderDetailContent
          items={this.props.orderItems}
          order={currentOrder}
          editorMode={this.state.isEditing}
          sellerSetting={this.props.sellerSetting}
          getSellerSetting={this.props.getSellerSetting}
        />
      </div>
    )
  }

  renderHeader = () => {
    const { showEmailModal } = this.state
    const { sellerSetting } = this.props
    const currentOrder = this.props.currentOrder!
    const orderClient = currentOrder.wholesaleClient
    let count = 0
    let totalPrice = 0
    for (const item of this.props.orderItems!) {
      count += item.quantity
      totalPrice += item.quantity * item.price
    }
    if (!currentOrder) {
      return null
    }
    // currentOrder.wholesaleClient.displayPriceSheetPrice
    return (
      <Container>
        <MessageHeader message={this.state.message} type={this.state.type} description={this.props.description} />
        <div>
          <BackButton onClick={this.backToPricesheetList} style={{ marginTop: '36px', float: 'left' }}>
            <Icon type="arrow-left" />
            <span>BACK</span>
          </BackButton>
          <BackButton onClick={this.onPrint} style={{ marginTop: '36px', float: 'right' }}>
            <Icon type="printer" />
            <span>PRINT</span>
          </BackButton>
          <CompanyInfoWrapper style={{ float: 'right', marginRight: '20px' }}>
            {!sellerSetting || sellerSetting.imagePath === 'default' ? (
              <LogoWrapper>
                <IconSvg type="logo" viewBox={void 0} style={defaultLogoStyle} />
              </LogoWrapper>
            ) : (
              <img
                src={`
              ${process.env.AWS_PUBLIC}/${sellerSetting.imagePath}`}
              />
            )}
            <span style={{ marginTop: 10 }}>{sellerSetting.companyName}</span>
          </CompanyInfoWrapper>
        </div>
        <Header style={{ marginTop: '36px', padding: '0 60px' }}>
          <Flex>
            <InfoWrap>
              <p style={{ fontWeight: 700 }}>
                Sales Order
                <span style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 18, color: '#000' }}>
                  #{currentOrder.wholesaleOrderId}
                </span>
              </p>
              <Info>
                <InfoItem>
                  <InfoItemLabel>SALESPERSON</InfoItemLabel>
                  <InfoItemText>
                    {currentOrder.user ? currentOrder.user.firstName + ' ' + currentOrder.user.lastName : 'N/A'}
                  </InfoItemText>
                </InfoItem>
                <InfoItem>
                  <InfoItemLabel>ORDER STATUS</InfoItemLabel>
                  <StatusWrapper>{currentOrder.wholesaleOrderStatus}</StatusWrapper>
                </InfoItem>
                <InfoItem>
                  <InfoItemLabel>ORDER DATE</InfoItemLabel>
                  <InfoItemText>{moment.utc(currentOrder.orderDate).format('MM/DD/YYYY')}</InfoItemText>
                </InfoItem>
                <InfoItem>
                  <InfoItemLabel>REQUESTED DELIVERY DATE</InfoItemLabel>
                  <InfoItemText>{moment.utc(currentOrder.deliveryDate).format('MM/DD/YYYY')}</InfoItemText>
                </InfoItem>
                <AddressWrapper>
                  <InfoItemLabel>SHIPPING ADDRESS</InfoItemLabel>
                  <InfoItemText>
                    {currentOrder.shippingAddress && currentOrder.shippingAddress.address
                      ? formatAddress(currentOrder.shippingAddress.address, true)
                      : 'N/A'}
                  </InfoItemText>
                </AddressWrapper>
                <MainInfoItem>
                  <InfoItemLabel>ITEMS</InfoItemLabel>
                  <p>{count}</p>
                </MainInfoItem>
                <MainInfoItem>
                  <InfoItemLabel>TOTAL PRICE</InfoItemLabel>
                  <p>
                    {!!currentOrder?.wholesaleClient?.displayPriceSheetPrice
                      ? `$${formatNumber(totalPrice, 2)}`
                      : 'Pending'}
                  </p>
                </MainInfoItem>
              </Info>
            </InfoWrap>
          </Flex>
          <ExtraWrap>
            {/* <ExtraItem onClick={this.toggleEmailModal}>
              <Icon type="mail" />
              <span>EMAIL</span>
            </ExtraItem> */}
            {/* <ExtraItem onClick={this.onPrint}>
              <Icon type="printer" />
              <span>PRINT</span>
            </ExtraItem> */}
          </ExtraWrap>
        </Header>
        {/* { orderClient &&
        <OrderEmail
          customer={orderClient}
          theme={this.props.theme}
          visible={showEmailModal}
          onToggle={this.toggleEmailModal}
          onSend={this.sendEmail}
        />
        } */}
      </Container>
    )
  }
}

const mapStateToProps = (state: GlobalState) => ({ ...state.orders })
export const OrderDetail = withTheme(connect(OrdersModule)(mapStateToProps)(OrdersDetailComponent))
