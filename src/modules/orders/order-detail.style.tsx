import styled from '@emotion/styled'
import { white, mediumGrey, black, backgroundGreen, darkGrey, filterGreen, primaryBlue } from '~/common'

export const HeaderContainer = styled('div')({
  width: '100%',
  height: '238px',
  backgroundColor: white,
  display: 'flex',
  padding: '32px',
  boxSizing: 'border-box',
  position: 'relative',
})

export const Header = styled('div')({
  width: '100%',
  height: '232px',
  boxSizing: 'border-box',
  display: 'flex',
  justifyContent: 'space-between',
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const Container = styled('div')({
  paddingLeft: '36px',
  paddingRight: '50px',
})

export const Flex = styled('div')({
  display: 'flex',
})

export const BackButton = styled('div')((props) => ({
  color: props.theme.primary,
  fontSize: '12px',
  cursor: 'pointer',
  marginRight: '30px',

  '& > span': {
    textTransform: 'uppercase',
    fontWeight: 700,
    marginLeft: '8px',
  },
}))

export const InfoWrap = styled('div')({
  boxSizing: 'border-box',
  marginRight: '112px',
  '& > span': {
    fontSize: '12px',
    color: mediumGrey,
  },
})

export const OderId = styled('div')({
  color: black,
  fontSize: '36px',
  fontWeight: 700,
  letterSpacing: '0.05em',
  lineHeight: '50px',
})

export const Info = styled('div')({
  display: 'flex',
  width: '100%',
  height: '100px',
  boxSizing: 'border-box',
  marginTop: '22px',
})

export const InfoItem = styled('div')({
  height: '100px',
  boxSizing: 'border-box',
  paddingLeft: '30px',
  paddingRight: '50px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  backgroundColor: backgroundGreen,

  '& > span': {
    fontSize: '14px',
    color: mediumGrey,
  },
  '& > p': {
    fontSize: '18px',
    margin: 0,
    marginTop: '10px',
    color: darkGrey,
  },
})

export const InfoItemLabel = styled('span')({
  fontWeight: 700,
  fontSize: '14px',
  letterSpacing: '0.05em',
  lineHeight: '17px',
})

export const InfoItemText = styled('span')({
  fontWeight: 700,
  fontSize: '14px',
  letterSpacing: '0.05em',
  lineHeight: '34px',
})

export const StatusWrapper = styled('span')({
  fontWeight: 700,
  fontSize: '14px',
  letterSpacing: '0.05em',
  lineHeight: '34px',
  backgroundColor: primaryBlue,
  color: `${white} !important`,
  borderRadius: 4,
  textAlign: 'center'
})

export const CustomerLogo = styled('div')({
  position: 'absolute',
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'column',
  left: 0
})

export const MainInfoItem = styled('div')({
  height: '100px',
  boxSizing: 'border-box',
  padding: '0 30px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  backgroundColor: filterGreen,

  '& > span': {
    fontSize: '14px',
    color: mediumGrey,
  },
  '& > p': {
    fontSize: '30px',
    lineHeight: '41px',
    margin: 0,
    color: mediumGrey,
  },
})

export const ExtraWrap = styled('div')({
  position: 'absolute',
  top: '84px',
  right: '30px',
  display: 'flex',
  alignItems: 'center',
})


export const ButtonWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
})


export const ExtraItem = styled('div')((props) => ({
  display: 'flex',
  alignItems: 'center',
  cursor: (props.disabled === true) ? 'default' : 'pointer',
  marginLeft: '20px',

  '& > i': {
    fontSize: '18px',
    marginRight: '6px',
    color: (props.disabled === true) ? 'lightGray' : props.theme.primary,
  },
  '& > span': {
    color: (props.disabled === true) ? 'lightGray' : props.theme.primary,
    fontSize: '14px',
    letterSpacing: '0.05em',
  },
}))

export const AddressWrapper = styled('div')({
  height: '100px',
  boxSizing: 'border-box',
  minWidth: 200,
  paddingLeft: '20px',
  paddingRight: '20px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  backgroundColor: backgroundGreen,

  '& > span': {
    fontSize: '14px',
    color: mediumGrey,
  },
  '& > p': {
    fontSize: '18px',
    margin: 0,
    marginTop: '10px',
    color: darkGrey,
  },
})
