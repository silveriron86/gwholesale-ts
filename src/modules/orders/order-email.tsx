import React from 'react'
import { Button, Modal, Select } from 'antd'
import Form, { FormComponentProps } from 'antd/lib/form'
import { DialogFooter } from '../pricesheet/components/pricesheet-detail-header.style'
import { DialogHeader, CustomerSelector, CustomerSelectorTitle, MessageBox, MessageBoxTitle, MessageBoxInput, selectStyles } from '../pricesheet/pricesheet-detail.style'
import { ThemeButton, ThemeSelect, ThemeInput } from '../customers/customers.style'
import { Theme } from '~/common'

const FormItem = Form.Item

interface OrderEmailProps extends FormComponentProps {
  customer: any
  onToggle: Function
  onSend: Function
  visible: boolean
  theme: Theme
}

export class OrderEmail extends React.PureComponent<OrderEmailProps> {
  state = {
    message: '',
    type: null,
    isEditing: false,
    emailText: '',
    email: '',
    showEmailModal: false,
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let data = {...values}
        console.log(data)
        this.props.onSend(data);
        this.props.onToggle();
        form.resetFields();
      }
    })
  }

  render() {
    const { visible, onToggle, customer } = this.props
    const { getFieldDecorator } = this.props.form
    
    return (
      <Modal
        visible={visible}
        onCancel={onToggle}
        footer={null}
      >
        <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
          <DialogHeader>Email Order</DialogHeader>
          <CustomerSelector>
            <CustomerSelectorTitle style={{width: '100%'}}>
              TO CUSTOMER
              <FormItem style={{marginBottom: 0, paddingBottom: 0}}>
                {getFieldDecorator('customerId', {
                    rules: [
                      { 
                        required: true,
                        message: 'customer is required!'
                      }
                    ],
                    initialValue: customer && customer.mainContact && customer.mainContact.name ? customer.mainContact.name : ''
                })(
                  <ThemeInput disabled/>
                )}                
              </FormItem>              
            </CustomerSelectorTitle>
          </CustomerSelector>
          <MessageBox>
            <MessageBoxTitle>MESSAGE</MessageBoxTitle>
            <FormItem style={{marginBottom: 0, paddingBottom: 0}}>
              {getFieldDecorator('message', {
                rules: [{ required: true, message: 'Message is required!' }],
              })(<MessageBoxInput autosize={{ minRows: 2, maxRows: 8 }} />)}
            </FormItem>
          </MessageBox>
          <DialogFooter key="footer" style={{marginTop: 15, justifyContent: 'center', display: 'flex', alignItems: 'center', paddingRight: 0}}>
            <ThemeButton key="plus" shape="round" type="primary" icon="plus" htmlType="submit">
              Send Email
            </ThemeButton>
            <Button onClick={onToggle}>CANCEL</Button>
          </DialogFooter>
        </Form>   
      </Modal>
    )
  }
}

export default Form.create<OrderEmailProps>()(OrderEmail)