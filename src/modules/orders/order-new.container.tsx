/**@jsx jsx */
import React, { useEffect, useMemo, useRef, useState } from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { jsx } from '@emotion/core'
import { withTheme } from 'emotion-theming'
import { isHotkey } from 'is-hotkey'

import { Theme } from '~/common'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from './orders.module'
import { OrderEditor as OrderEditorComponent } from './components'
import EnhancedOrderTableHeader, { OrderTableHeader } from './components/OrderTableHeader'
import { Moment } from 'moment'

export type NewOrderProps = OrdersStateProps &
  OrdersDispatchProps &
  RouteComponentProps & {
    theme: Theme
  }

const isNewOrderHotkey = isHotkey('Control+p')

const NewOrder: React.SFC<NewOrderProps> = (props) => {
  const [isRemovedCats, setRemovedCats] = useState<boolean>(false)

  useEffect(() => {
    props.getCustomers()
    props.getSaleItems()

    return () => {
      props.disposeEditOrder$()
    }
  }, [])

  const memoizedItemList = useMemo(() => {
    return props.saleItems
      .filter((n) => n.orderQuantity)
      .map((item) => ({
        wholesaleItemId: item.itemId,
        quantity: item.orderQuantity!,
        cost: item.cost,
        price: item.salePrice ? item.salePrice : 0,
        margin: item.margin ? item.margin : 0,
        freight: item.freight ? item.freight : 0,
        status: item.status,
      }))
  }, [props.saleItems])

  const memoizedOrderMeta = useMemo(() => {
    return memoizedItemList.reduce(
      (prev, cur) => {
        prev.itemCount += cur.quantity
        prev.totalPrice += cur.price * cur.quantity
        return prev
      },
      {
        itemCount: 0,
        totalPrice: 0,
      },
    )
  }, [memoizedItemList])

  const handleCreateOrder = (payload: { deliveryDate: Moment; clientId: string }) => {
    const delivery = payload.deliveryDate.format('MM/DD/YYYY').toString()

    const formData = {
      deliveryDate: delivery,
      totalPrice: memoizedOrderMeta.totalPrice,
      wholesaleCustomerClientId: payload.clientId,
      itemList: memoizedItemList,
      status: 'PLACED',
    }

    props.createOrder(formData)
  }

  const $tableHeader = useRef<OrderTableHeader>(null)

  const quickCreareOrder = () => {
    if ($tableHeader && $tableHeader.current) {
      const payload: any = $tableHeader.current.getState()
      if (payload) {
        handleCreateOrder(payload)
      }
    }
  }

  useEffect(() => {
    window.addEventListener('keydown', handleTriggerOrder)
    return () => {
      window.removeEventListener('keydown', handleTriggerOrder)
    }
  }, [])

  function handleTriggerOrder(e: KeyboardEvent) {
    if (isNewOrderHotkey(e)) {
      quickCreareOrder()
    }
  }
  
  function onUpdateCatsView() {
    setRemovedCats(!isRemovedCats)
  }

  return (
    <React.Fragment>
      <EnhancedOrderTableHeader
        wrappedComponentRef={$tableHeader}
        itemCount={memoizedOrderMeta.itemCount}
        totalPrice={memoizedOrderMeta.totalPrice}
        onCreateOrder={handleCreateOrder}
        customers={props.customers}
        loadingIndex={props.loadingIndex}
        loadIndex={props.loadIndex}
        onClickBack={props.goBack}
        isRemovedCats={isRemovedCats}
        onUpdateCatsView={onUpdateCatsView}
      />
      <OrderEditorComponent onItemUpdate={props.updateSaleItem} saleItems={props.saleItems} />
    </React.Fragment>
  )
}

export const NewOrderContainer = withTheme(connect(OrdersModule)(({ orders }: GlobalState) => orders)(NewOrder))
