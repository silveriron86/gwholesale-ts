import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { mediumGrey, black, grey } from '~/common'

export const Header = styled('div')({
  display: 'flex',
  width: '100%',
  padding: '30px 0 30px 30px',
})

export const BackButton = styled('div')((props) => ({
  color: props.theme.primary,
  display: 'flex',
  justifyContent: 'center',
  userSelect: 'none',
  cursor: 'pointer',
}))

export const backButtonCss = css({
  marginRight: '5px',
})

export const OrderWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  marginLeft: '5%',
})

export const OrderTitle = styled('span')({
  color: mediumGrey,
})

export const OrderId = styled('div')({
  fontSize: '36px',
  lineHeight: '50px',
  color: black,
  fontWeight: 'bold',
})

export const OrderEditor = styled('div')({
  display: 'flex',
})

export const CustomerSelector = styled('div')({
  display: 'flex',
})

export const CustomerSelectorTitle = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  color: grey,
  fontSize: '14px',
  lineHeight: '33px',
})

export const selectStyles = css({
  width: '335px',
  '& .ant-select-selection': {
    border: 'none',
    borderBottom: '1px solid black',
    boxShadow: 'none',
    borderRadius: 0,
    '& > div': {
      margin: 0,
    },
    '&:hover': {
      border: 'none',
      borderBottom: '1px solid black',
    },
    '&:active': {
      boxShadow: 'none',
    },
  },
})

export const DateEditorWrapper = styled('div')((props) => ({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
  '& .ant-calendar-picker': {
    '& .ant-calendar-picker-input': {
      border: 'none',
      padding: '4px 0',
      borderBottom: 'solid 1px black',
      borderRadius: 0,
      '&:focus': {
        boxShadow: 'none',
      },
    },
    '& .ant-calendar-picker-icon': {
      width: '20px',
      height: '21px',
      marginTop: '-10px',
      color: props.theme.primary,
    },
  },
}))

export const DateTitle = styled('span')({
  color: grey,
  marginBottom: '10px',
})

export const ItemWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
})

export const ItemTitle = styled('span')({
  color: black,
  fontSize: '14px',
  marginBottom: '8px',
})

export const ItemCount = styled('span')({
  color: black,
  lineHeight: '35px',
  fontSize: '30px',
})

export const PriceWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
})

export const PriceTitle = styled('span')({
  color: black,
  fontSize: '14px',
  marginBottom: '9px',
})

export const PriceTotal = styled('span')({
  color: black,
  fontSize: '30px',
  lineHeight: '35px',
})

export const ButtonWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column-reverse',
  marginLeft: '5%',
})

export const buttonStyles = css({
  width: '180px',
  borderRadius: '20px',
})
