/**@jsx jsx */
import React, { useEffect, useState, useMemo } from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { jsx } from '@emotion/core'

import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from './orders.module'
import OrderPriceSheetTable from './components/order-pricesheet-table.component'
import OrderTableHeader from './components/OrderTableHeader'
import { Moment } from 'moment'
import { notification } from 'antd'

export type NewOrderPriceSheetProps = RouteComponentProps<{
  id: string
  customerId: string
}> &
  OrdersStateProps &
  OrdersDispatchProps

class NewOrderPriceSheet extends React.PureComponent<NewOrderPriceSheetProps> {
  state: any
  constructor(props: NewOrderPriceSheetProps) {
    super(props)
    this.state = {
      selectedCustomer: '',
      message: '',
      type: '',
      isRemovedCats: false,
    }
  }

  componentDidMount() {
    this.props.getCustomers()
    this.props.getPriceSheetItems(this.props.match.params.id)
  }

  componentWillReceiveProps(nextProps: NewOrderPriceSheetProps) {
    const { selectedCustomer } = this.state
    const existedCustomerId = this.props.match.params.customerId
    if (!selectedCustomer && existedCustomerId !== '0' && Array.isArray(nextProps.customers) && nextProps.customers.length) {
      const targetCustomer = nextProps.customers.find((n) => n.clientId == existedCustomerId)
      if (targetCustomer) {
        this.setState({
          selectedCustomer: targetCustomer.clientId
        })
      }
    }

    if (nextProps.newOrderId > 0) {
      this.props.history.push(`/sales-order/${nextProps.newOrderId}`)
      this.props.resetNewOrderId();
    }
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  getMemoizedItemList = () => {
    return this.props.priceSheetItems
      .filter((n) => n.orderQuantity)
      .map((item) => ({
        price: item.salePrice,
        wholesaleItemId: item.itemId,
        quantity: item.orderQuantity,
        cost: item.cost,
        margin: item.margin,
        freight: item.freight,
        modifier: item.modifier
      }))
  }

  getMemoizedOrderMeta = () => {
    return this.getMemoizedItemList().reduce(
      (prev, cur) => {
        prev.itemCount += cur.quantity!
        prev.totalPrice += cur.price * cur.quantity!
        return prev
      },
      {
        itemCount: 0,
        totalPrice: 0,
      },
    )
  }

  handleCreateOrder = (payload: { deliveryDate: Moment; clientId: string; }) => {
    const delivery = payload.deliveryDate.format('MM/DD/YYYY').toString()
    this.props.createOrder({
      deliveryDate: delivery,
      id: this.props.match.params.id,
      itemList: this.getMemoizedItemList(),
      totalPrice: this.getMemoizedOrderMeta().totalPrice,
      wholesaleCustomerClientId: payload.clientId,
      priceSheetId: this.props.match.params.id
    })
  }

  onUpdateCatsView = () => {
    this.setState({
      isRemovedCats: !this.state.isRemovedCats
    });
  }

  render() {
    const memoizedOrderMeta = this.getMemoizedOrderMeta()
    const { selectedCustomer, isRemovedCats } = this.state
    const { customers, loadingIndex, loadIndex, goBack, updatePriceSheetItem, priceSheetItems } = this.props

    return (
      <div>
        <OrderTableHeader
          itemCount={memoizedOrderMeta.itemCount}
          totalPrice={memoizedOrderMeta.totalPrice}
          onCreateOrder={this.handleCreateOrder}
          customers={customers}
          loadingIndex={loadingIndex}
          loadIndex={loadIndex}
          onClickBack={goBack}
          selectedCustomer={selectedCustomer}
          isRemovedCats={isRemovedCats}
          onUpdateCatsView={this.onUpdateCatsView}
          isTokenOrder={false}
        />
        <OrderPriceSheetTable isSeller
          onPriceSheetItemUpdate={updatePriceSheetItem}
          priceSheetItems={priceSheetItems}
          isRemovedCats={isRemovedCats}
        />
      </div>
    )
  }
}

export const NewOrderPriceSheetContainer = connect(OrdersModule)(({ orders }: GlobalState) => orders)(
  NewOrderPriceSheet,
)
