import React, { useRef, useEffect } from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { GlobalState } from '~/store/reducer'
import { format } from 'date-fns'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from './orders.module'
import {
  Header,
  InfoWrap,
  OderId,
  Info,
  InfoItem,
  MainInfoItem,
  Flex,
  Container,
  InfoItemLabel,
  InfoItemText,
  CustomerLogo,
} from './order-detail.style'
import { OrderDetailContent } from './components'
import { formatNumber } from '~/common/utils'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { PrintView, Addon } from '../pricesheet/pricesheet-print.style'
import { ThemeButton } from '../customers/customers.style'
import { printWindow } from '~/common/utils'

type OrderPrintProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

const OrderPrintComponent: React.SFC<OrderPrintProps> = (props) => {
  const tableRef: any = useRef<HTMLDivElement>()
  useEffect(() => {
    console.log('----------------------')
    const id = props.match.params.orderId
    console.log(id)
    props.getOrderTokenDetail(id)
    props.getOrderItems(id)
  }, [])

  const printTrigger = () => {
    return (
      <ThemeButton type="primary" size="large">
        Print this table
      </ThemeButton>
    )
  }
  const printContent: any = () => {
    return tableRef && tableRef.current
  }

  const { currentOrder } = props
  if (!currentOrder) {
    return null
  }

  let count = 0
  let totalPrice = 0
  for (const item of props.orderItems!) {
    count += item.quantity
    totalPrice += item.quantity * item.price
  }

  return (
    <React.Fragment>
      <Addon>
        <ThemeButton
          type="primary"
          size="large"
          onClick={() => printWindow('PrintPreviewDiv', printContent, 'landscape')}
        >
          Print
        </ThemeButton>
      </Addon>
      <div id={'PrintPreviewDiv'}>
        <PrintView ref={tableRef}>
          <Container>
            <Header>
              <Flex style={{ marginTop: '36px' }}>
                <InfoWrap>
                  <CustomerLogo>
                    <span style={{ fontWeight: 700 }}>&nbsp;</span>
                    <OderId>{currentOrder.wholesaleClient.mainContact.name}</OderId>
                  </CustomerLogo>
                  <span style={{ fontWeight: 700 }}>CUSTOMER ORDER</span>
                  <OderId>#{currentOrder.wholesaleOrderId}</OderId>
                  <Info>
                    {/* <InfoItem>
                    <InfoItemLabel>CUSTOMER</InfoItemLabel>
                    <InfoItemText>{currentOrder.wholesaleClient.mainContact.name}</InfoItemText>
                  </InfoItem> */}
                    <InfoItem>
                      <InfoItemLabel>DATE PLACED</InfoItemLabel>
                      <InfoItemText>{format(currentOrder.createdDate, 'MM/D/YYYY')}</InfoItemText>
                    </InfoItem>
                    <InfoItem>
                      <InfoItemLabel>DELIVERY DATE</InfoItemLabel>
                      <InfoItemText>{format(currentOrder.deliveryDate, 'MM/D/YYYY')}</InfoItemText>
                    </InfoItem>
                    {/* <InfoItem>
                      <InfoItemLabel>QBO INVOICE ID</InfoItemLabel>
                      <InfoItemText>#{currentOrder.qboId}</InfoItemText>
                    </InfoItem> */}
                    <MainInfoItem>
                      <InfoItemLabel>ITEMS</InfoItemLabel>
                      <p>{count}</p>
                    </MainInfoItem>
                    <MainInfoItem>
                      <InfoItemLabel>TOTAL PRICE</InfoItemLabel>
                      <p>${formatNumber(totalPrice, 2)}</p>
                    </MainInfoItem>
                  </Info>
                </InfoWrap>
              </Flex>
            </Header>
          </Container>
          <OrderDetailContent
            items={props.orderItems}
            order={currentOrder}
            editorMode={false}
            sellerSetting={props.sellerSetting}
            getSellerSetting={props.getSellerSetting} />
        </PrintView>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = (state: GlobalState) => state.orders
const OrderPrint = withTheme(connect(OrdersModule)(mapStateToProps)(OrderPrintComponent))
export default OrderPrint
