/**@jsx jsx */
import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { jsx } from '@emotion/core'
import { Select, DatePicker, Button, message } from 'antd'
import moment from 'moment'

import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from './orders.module'
import {
  Header,
  BackButton,
  backButtonCss,
  OrderWrapper,
  OrderTitle,
  OrderEditor,
  CustomerSelector,
  CustomerSelectorTitle,
  DateTitle,
  DateEditorWrapper,
  ItemWrapper,
  ItemTitle,
  ItemCount,
  PriceWrapper,
  PriceTitle,
  PriceTotal,
  ButtonWrapper,
  buttonStyles,
} from './order-new.style'
import { Icon } from '~/components'
import { OrderInventoryTable } from './components/order-inventory-table.component'
import { formatNumber } from '~/common/utils'

export type NewOrderStockProps = OrdersStateProps & OrdersDispatchProps & RouteComponentProps

const Option = Select.Option

export class NewOrderStock extends React.PureComponent<NewOrderStockProps> {
  state = {
    itemCount: 0,
    totalPrice: 0,
    selectedDate: '',
    status: '',
  }
  private tableCmp = React.createRef<OrderInventoryTable>()

  updateOrderTotal = (newItemCount: number, newTotalPrice: number) => {
    this.setState({
      itemCount: newItemCount,
      totalPrice: newTotalPrice,
    })
  }

  componentDidMount() {
    this.props.getSaleItems()
  }

  componentWillUnmount() {
    //this.props.disposeEditOrder$()
  }

  render() {
    return (
      <div>
        {this.renderHeader()}
        {this.renderTable()}
      </div>
    )
  }

  onDateChange = (_dateMoment: any, dateString: string) => {
    if (dateString.length > 0)
      this.setState({
        selectedDate: dateString,
      })
  }

  onPlaceOrder = () => {
    if (this.state.selectedDate.length === 0) {
      message.error('Please select Delivery Date')
      return
    }
    const itemList = []
    const tableList = this.tableCmp.current!.state.initItems
    for (const item of tableList) {
      if (item.orderQuantity! > 0) {
        const itemParam = {
          wholesaleItemId: item.itemId,
          quantity: item.orderQuantity!,
          cost: item.cost,
        }
        itemList.push(itemParam)
      }
    }
    const params = {
      totalPrice: this.state.totalPrice,
      itemList: itemList,
      deliveryDate: this.state.selectedDate,
      status: this.state.status,
    }
    this.props.requestPurchaseOrders(params)
  }

  onSelectStatus = (value: any) => {
    this.setState({
      status: value,
    })
  }

  private renderHeader() {
    return (
      <Header>
        <BackButton onClick={this.props.goBack}>
          <Icon viewBox="-3 -3 16 16" width="24" height="24" type="arrow-left" css={backButtonCss} />
          BACK
        </BackButton>
        {this.renderEditor()}
        {this.renderDateEditor()}
        {this.renderItemsCount()}
        {this.renderPrice()}
        <ButtonWrapper>
          <Button type="primary" size="large" css={buttonStyles} onClick={this.onPlaceOrder}>
            Place Order
          </Button>
        </ButtonWrapper>
      </Header>
    )
  }

  private renderEditor() {
    return (
      <OrderWrapper>
        <OrderTitle>NEW PURCHASE ORDER</OrderTitle>
        <OrderEditor>
          <CustomerSelector>
            <CustomerSelectorTitle>
              STATUS
              <br />
              <Select defaultValue={this.state.status} style={{ width: 200 }} onChange={this.onSelectStatus}>
                <Option value="RECEIVED">RECEIVED</Option>
                <Option value="INCOMING">INCOMING</Option>
              </Select>
            </CustomerSelectorTitle>
          </CustomerSelector>
        </OrderEditor>
      </OrderWrapper>
    )
  }

  private renderDateEditor() {
    const calendarIcon = <Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    return (
      <DateEditorWrapper>
        <DatePicker
          onChange={this.onDateChange}
          placeholder={dateFormat}
          format={dateFormat}
          suffixIcon={calendarIcon}
          defaultValue={moment()}
          allowClear={false}
        />
        <DateTitle>DELIVERY DATE</DateTitle>
      </DateEditorWrapper>
    )
  }

  private renderItemsCount() {
    return (
      <ItemWrapper>
        <ItemCount>{this.state.itemCount}</ItemCount>
        <ItemTitle>ITEM(S)</ItemTitle>
      </ItemWrapper>
    )
  }

  private renderPrice() {
    return (
      <PriceWrapper>
        <PriceTotal>$ {formatNumber(this.state.totalPrice, 2)}</PriceTotal>
        <PriceTitle>TOTAL PRICE</PriceTitle>
      </PriceWrapper>
    )
  }

  private renderTable() {
    return (
      <OrderInventoryTable
        ref={this.tableCmp}
        saleItems={this.props.saleItems}
        updateOrderTotal={this.updateOrderTotal}
      />
    )
  }
}

export const NewOrderStockContainer = connect(OrdersModule)(({ orders }: GlobalState) => orders)(NewOrderStock)
