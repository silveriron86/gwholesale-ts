/**@jsx jsx */
import React, { FC, useEffect, useMemo, useState } from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { jsx } from '@emotion/core'

import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from './orders.module'
import OrderPriceSheetTable from './components/order-pricesheet-table.component'
import OrderTableHeader from './components/OrderTableHeader'
import moment from 'moment'
import { PurchaseItem } from './components/OrderTableHeader/styles'
import { ThemeTextButton } from '../customers/customers.style'
import { Modal } from 'antd'
import CustomerItemsModal from './components/order-customer-items'
import { OrderService } from './order.service'
import { of } from 'rxjs'
import { checkError, mathRoundFun, responseHandler } from '~/common/utils'
import _ from 'lodash'

export type NewOrderPriceSheetProps = RouteComponentProps<{
  orderId: string
}> &
  OrdersStateProps &
  OrdersDispatchProps

const NewTokenOrder: React.SFC<NewOrderPriceSheetProps> = (props) => {
  const [isRemovedCats, setRemovedCats] = useState<boolean>(true)
  const [modalVisible, setModalVisible] = useState(false)
  const [customerItems, setCustomerItems] = useState<any>([])
  const {
    tokenPriceSheet,
    updateTokenPriceSheetItem,
    match: {
      params: { orderId },
    },
  } = props

  const fetchCustomerItems = () => {
    OrderService.instance.getCustomerItems(props.tokenPriceSheet.sellerSetting.userId).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        setCustomerItems(
          _.xorBy(
            res.body.data,
            tokenPriceSheet.priceSheetItemList.map((v) => ({
              ...tokenPriceSheet.priceSheetItemList,
              wholesaleItemId: v.wholesaleItem.wholesaleItemId,
            })),
            'wholesaleItemId',
          ),
        )
      },
      error(err) {
        checkError(err)
      },
      complete() {},
    })
  }

  useEffect(() => {
    props.getTokenPriceSheet(orderId)
    // if (!props.sellerSetting) {
    //   props.getSellerSetting()
    // }
  }, [])

  useEffect(() => {
    if (props.tokenPriceSheet.wholesaleCustomerClient) {
      fetchCustomerItems()
    }
  }, [tokenPriceSheet])

  function onUpdateCatsView() {
    setRemovedCats(!isRemovedCats)
  }

  const memoizedItemList = useMemo(() => {
    return props.tokenPriceSheet.priceSheetItemList
      .filter((n) => n.orderQuantity)
      .map((item,index) => {
        let newSalePrice = item.salePrice || 0
        // const cost = item.wholesaleItem ? item.wholesaleItem.cost : 0
        // if (item.defaultLogic === 'FOLLOW_GROUP') {
        //   if (item.wholesaleItem && item.wholesaleItem.priceGroupType == 1) {
        //     newSalePrice = rowPrice
        //   } else {
        //     newSalePrice = cost / (1.0 - item.margin / 100.0) + item.markup
        //   }
        // } else {
        //   newSalePrice = cost / (1.0 - item.margin / 100.0) + item.markup
        // }
        return {
          price: mathRoundFun(newSalePrice, 2),
          priceSheetItemId: (item.isRequest || item.isSelectedOrder)  ? null : item.priceSheetItemId,
          wholesaleItemId: item.isRequest ? null : item.itemId,
          quantity: item.orderQuantity!,
          cost: item.wholesaleItem.cost,
          margin: 0,
          freight: 0,
          modifier: item.modifier,
          defaultLogic: item.defaultLogic,
          defaultGroup: item.defaultGroup,
          note: item.note ? item.note : '',
          uom: item.isRequest ? 'each' : undefined,
          itemDesc: item.isRequest ? item.itemDesc : undefined,
          displayOrder:index
        }
      })
  }, [props.tokenPriceSheet.priceSheetItemList])

  const memoizedOrderMeta = useMemo(() => {
    return memoizedItemList.reduce(
      (prev, cur) => {
        prev.itemCount += cur.quantity
        prev.totalPrice += cur.price * cur.quantity
        return prev
      },
      {
        itemCount: 0,
        totalPrice: 0,
      },
    )
  }, [memoizedItemList])

  const handleCreateOrder = (payload: {
    deliveryDate: moment.Moment
    clientId: string
    pickerNote: string
    shippingAddressId: number
  }) => {
    const orderDate = moment(new Date()).format('MM/DD/YYYY')
    const delivery = payload.deliveryDate.format('MM/DD/YYYY').toString()
    // console.log(tokenPriceSheet)
    // console.log({
    //   deliveryDate: delivery,
    //   priceSheetId: tokenPriceSheet.priceSheet.priceSheetId,
    //   items: memoizedItemList,
    //   totalPrice: memoizedOrderMeta.totalPrice,
    //   userId: tokenPriceSheet.sellerSetting.userId,
    //   wholesaleClientId: tokenPriceSheet.wholesaleCustomerClient.clientId,
    //   token: orderId,
    //   orderDate,
    //   pickerNote: payload.pickerNote,
    //   shippingAddressId: payload.shippingAddressId,
    // });
    props.createOrderByToken({
      deliveryDate: delivery,
      priceSheetId: tokenPriceSheet.priceSheet.priceSheetId,
      items: memoizedItemList,
      totalPrice: memoizedOrderMeta.totalPrice,
      userId: tokenPriceSheet.sellerSetting.userId,
      wholesaleClientId: tokenPriceSheet.wholesaleCustomerClient.clientId,
      token: orderId,
      orderDate,
      pickerNote: payload.pickerNote,
      shippingAddressId: payload.shippingAddressId,
    })
  }
  const priceSheetName = tokenPriceSheet.priceSheet ? tokenPriceSheet.priceSheet.name : ''
  const companyName = tokenPriceSheet.sellerSetting ? tokenPriceSheet.sellerSetting.companyName : ''

  const clientCompany = tokenPriceSheet.wholesaleCustomerClient
    ? tokenPriceSheet.wholesaleCustomerClient.clientCompany.companyName
    : ''
  const firstName = tokenPriceSheet.sellerSetting
    ? tokenPriceSheet.sellerSetting.firstName : ''
  const lastName = tokenPriceSheet.wholesaleCustomerClient
    ? tokenPriceSheet.sellerSetting.lastName : ''
  const logoUrl = tokenPriceSheet.sellerSetting ? `${process.env.AWS_PUBLIC}/${tokenPriceSheet.sellerSetting.imagePath}` : 'default'

  const backToPricesheetLsit = () => {
    props.history.push(`/pricesheets`)
  }

  return (
    <div>
      <OrderTableHeader
        orderMeta={{
          title: `${firstName} ${lastName}`,
          id: priceSheetName,
          selector: companyName,
          clientCompany: clientCompany,
          logoUrl: logoUrl,
        }}
        itemCount={memoizedOrderMeta.itemCount}
        totalPrice={memoizedOrderMeta.totalPrice}
        onCreateOrder={handleCreateOrder}
        customers={props.customers}
        loadingIndex={props.loadingIndex}
        loadIndex={props.loadIndex}
        onClickBack={backToPricesheetLsit}
        isRemovedCats={isRemovedCats}
        onUpdateCatsView={onUpdateCatsView}
        selectedCustomer={props.tokenPriceSheet.wholesaleCustomerClient}
        isTokenOrder={true}
        tokenPriceSheet={tokenPriceSheet}
        noBack={true}
      />
      <OrderPriceSheetTable
        isSeller={false}
        isRemovedCats={isRemovedCats}
        sellerSetting={tokenPriceSheet.sellerSetting}
        onPriceSheetItemUpdate={updateTokenPriceSheetItem}
        priceSheetItems={tokenPriceSheet.priceSheetItemList}
        selectedCustomer={props.tokenPriceSheet.wholesaleCustomerClient}
      />
      <PurchaseItem>
        {props.tokenPriceSheet.wholesaleCustomerClient
          ? props.tokenPriceSheet.wholesaleCustomerClient.wholesaleCompany.purchaseTerms
          : ''}
        <p>
          Not seeing the item you want?{' '}
          <ThemeTextButton className="bold" onClick={() => setModalVisible(true)}>
            Request an item!
          </ThemeTextButton>
        </p>
      </PurchaseItem>
      {modalVisible && (
        <CustomerItemsModal
          onCancel={() => setModalVisible(false)}
          customerItems={customerItems}
          handleAddPriceSheetItem={props.handleAddPriceSheetItem}
        />
      )}
    </div>
  )
}

export const NewTokenOrderContainer = connect(OrdersModule)(({ orders }: GlobalState) => orders)(NewTokenOrder)
