import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import {
  Address,
  Company,
  MainAddress,
  Order,
  OrderDetail,
  OrderItem,
  PriceSheetItem,
  TempOrder,
  TokenPriceSheet,
} from '~/schema'

/**
 *  in jsx
 *
 *  OrderService.instance.getRelatedOrderItemList(query).subscribe({
      next(resp) { console.log(resp); },
      error(err) { checkError(err) },
      complete() { setTableLoading(false) }
    })
 *
 */

@Injectable()
export class OrderService {
  constructor(private readonly http: Http) {}

  static get instance() {
    return new OrderService(new Http())
  }

  getOrderItems(token: string) {
    return this.http.get<OrderItem[]>(`/account/order/token/${token}/items/list`)
  }

  getSimplifyVendors(forQuickModal: boolean) {
    return this.http.get<any>(`/vendor/simplify-vendors?forQuickModal=${forQuickModal ? 1 : 0}`)
  }

  getSimplifyItems(warehouse: String, type: any) {
    console.log(warehouse, type)
    let url = `/inventory/simplify-items`
    return this.http.get<any>(url, { query: { warehouse, type } })
  }

  getSimplifyItemsByOrderId(warehouse: String, type: any, orderId: number) {
    let url = `/inventory/simplify-items`
    return this.http.get<any>(url, { query: { warehouse, type, orderId } })
  }

  getOrderItemsById(id: string) {
    return this.http.get<OrderItem[]>(`/account/order/${id}/items/list?timestamp=${new Date().getTime()}`)
  }

  getOrderItemsPagination(id: string, page: number, pageSize: number) {
    return this.http.get<OrderItem[]>(`/account/order/${id}/item-list?page=${page}&pageSize=${pageSize}`)
  }

  getOrderItemsByIdAndSort(orderObj: { id: string; sort: string }) {
    const { id, sort } = orderObj
    let sortNum = 0
    if (sort == 'by location') {
      sortNum = 1
    } else if (sort == 'by sku') {
      sortNum = 2
    } else {
      sortNum = 0
    }
    return this.http.get<OrderItem[]>(`/account/order/${id}/items/list?sort=${sortNum}`)
  }

  getOrderOneOffItemsById(id: string) {
    return this.http.get<OrderItem[]>(`/account/order/${id}/one-off-item/list`)
  }

  updateOrderStatus(id: string) {
    return this.http.put<OrderItem[]>(`/inventory/order/${id}/status`)
  }

  getOrders(dateString: string, companyName: string) {
    /*
    const dateString = moment(date)
      .format('09/10/2018')
      .toString()
      */
    return this.http.get<Order[]>(
      `/inventory/warehouse/wholesaleorders?warehouse=${companyName}&deliveryDate=${dateString}`,
    )
  }

  getRecentOrders(companyName: string) {
    /*
    const dateString = moment(date)
      .format('09/10/2018')
      .toString()
      */
    return this.http.get<Order[]>(`/inventory/warehouse/wholesaleorders?warehouse=${companyName}`)
  }

  getRecentPurchaseOrders(companyName: string) {
    /*
    const dateString = moment(date)
      .format('09/10/2018')
      .toString()
      */
    return this.http.get<[any]>(`/inventory/warehouse/wholesalePurchaseOrders?warehouse=${companyName}`)
  }

  getPriceSheetItems(priceSheetId: string) {
    return this.http.get<PriceSheetItem[]>(`/pricesheet/${priceSheetId}/items/list`)
  }

  getOrderTokenDetail(orderId: string) {
    return this.http.get<OrderDetail>(`/inventory/order/token/${orderId}`)
  }

  getOrderDetail(orderId: string) {
    return this.http.get<OrderDetail>(`/inventory/order/${orderId}`)
  }

  getPriceSheetFromToken(tokenId: string) {
    return this.http.get<TokenPriceSheet>(`/pricesheet/getPriceSheetFromToken/${tokenId}`)
  }

  getCustomersOrders(userId: string) {
    return this.http.get<Order[]>(`/account/user/${userId}/customer/orders/list`)
  }

  getWarehouseSalesOrders(query: any) {
    return this.http.get<Order[]>(`/inventory/warehouse/wholesaleSaleOrders`, { query })
  }

  getWarehouseSalesOrdersByDate(data: any) {
    let url = `/inventory/warehouse/wholesaleSaleOrders?page=${data.page}&pageSize=${data.pageSize}`
    if (data.fromDate.length > 0) url += `&from=` + data.fromDate
    if (data.toDate.length > 0) url += `&to=` + data.toDate
    if (data.searchStr != '') url += `&search=${data.searchStr}`
    if (data.status && data.status.length > 0) url += `&status=${data.status.join(',')}`
    if (data.orderByFilter != '') url += `&orderBy=${data.orderByFilter}&direction=${data.direction}`
    // return this.http.get<Order[]>(`/inventory/warehouse/wholesaleSaleOrders?to=${toDateString}&from=${fromDateString}`)
    return this.http.get<Order[]>(url)
  }

  getOrdersForCustomerUserId(data: any) {
    let url = `/account/customer/orders?page=${data.page}&pageSize=${data.pageSize}`
    if (data.fromDate.length > 0) url += `&from=` + data.fromDate
    if (data.toDate.length > 0) url += `&to=` + data.toDate
    return this.http.get<Order[]>(url)
  }

  getWarehousePurchaseOrders() {
    return this.http.get<Order[]>(`/inventory/warehouse/wholesalePurchaseOrders`)
  }

  getWarehousePurchaseOrdersByDate(data: any) {
    let url = `/inventory/warehouse/wholesalePurchaseOrders?page=${data.page}&pageSize=${data.pageSize}`
    if (data.fromDate.length > 0) url += `&from=` + data.fromDate
    if (data.toDate.length > 0) url += `&to=` + data.toDate
    if (data.searchStr !== '') url += `&search=${data.searchStr}`
    if (data.orderByFilter !== '') url += `&orderBy=${data.orderByFilter}&direction=${data.direction}`
    if (data.filtersQueryString) url += data.filtersQueryString
    return this.http.get<Order[]>(url)
  }

  getPurchaseLotItemsDownload(data: any) {
    let url = `/inventory/purchase-lot-items-download`
    let params = ''
    if (data.fromDate.length > 0) params += `?from=` + data.fromDate
    if (data.toDate.length > 0) params += `${params ? '&' : '?'}to=` + data.toDate
    if (data.searchStr !== '') params += `${params ? '&' : '?'}search=${data.searchStr}`
    url += params
    return this.http.get<Order[]>(url)
  }

  requestPurchaseOrders(purchaseOrder: any) {
    const params = { ...purchaseOrder }

    const date = new Date(purchaseOrder.deliveryDate)
    params.deliveryDate = date.toLocaleDateString()
    return this.http.post<any>(`/inventory/purchaseOrders`, {
      body: JSON.stringify(params),
    })
  }

  createOrder(data: any, userId: string) {
    if (data.userId == null) data.userId = userId
    return this.http.post<any>('/inventory/create/orders', {
      body: JSON.stringify(data),
    })
  }

  createCSVPurchaseOrders(data: any) {
    const str = JSON.stringify(data)
    return this.http.post<any>(`/inventory/create-purchase-orders-from-csv`, {
      body: str,
    })
  }

  updateItemsFromCSV(data: any) {
    const str = JSON.stringify(data)
    return this.http.post<any>(`/inventory/update/wholesaleitems-from-csv`, {
      body: str,
    })
  }

  createCSVOrders(data: any, customerId: number) {
    return this.http.post<any>(`/inventory/create-order-from-csv/${customerId}`, {
      body: JSON.stringify(data),
    })
  }

  createOrderByToken(data: any, userId: string) {
    if (data.userId == null) data.userId = userId
    return this.http.post<any>(`/inventory/create/order/${data.token}`, {
      body: JSON.stringify(data),
    })
  }

  updateOrder(data: any) {
    const info = {
      ...data,
      itemList: data.itemList.map((v: { quantity: any }) => ({ ...v, quantity: Number(v.quantity) })),
    }
    return this.http.post<any>('/inventory/update/orders', {
      body: JSON.stringify(info),
    })
  }

  shipLockOrder(data: any) {
    const info = {
      ...data,
      // itemList: data.itemList.map((v: { quantity: any }) => ({ ...v, quantity: Number(v.quantity) })),
    }
    return this.http.post<any>('/inventory/order/ship-lock', {
      body: JSON.stringify(info),
    })
  }

  shipLockCashOrder(data: any) {
    const info = {
      ...data,
      itemList: data.itemList.map((v: { quantity: any }) => ({ ...v, quantity: Number(v.quantity) })),
    }
    return this.http.post<any>('/inventory/cash-order/ship-lock', {
      body: JSON.stringify(info),
    })
  }

  updateOrderInfo(data: any) {
    return this.http.post<any>('/inventory/updateOrderInfo', {
      body: JSON.stringify(data),
    })
  }

  updateOrderItemsDeliveryDate(orderId: number, deliveryTime: string) {
    return this.http.post<any>(`/inventory/updateDeliveryDate/${orderId}?deliveryTime=${deliveryTime}`)
  }

  updateOrderItemNoCancel(data: any) {
    return this.http.post<any>(`/inventory/updateOrderItem?noCancel`, {
      body: JSON.stringify(data),
    })
  }

  updateOrderItem(data: any) {
    return this.http.post<any>(`/inventory/updateOrderItem`, {
      body: JSON.stringify(data),
    })
  }

  getOrderAdjustements(orderId: string) {
    return this.http.get<any>(`/inventory/orderAdjustement/list/${orderId}`)
  }

  getAllOrderItemAdjustementsByOrderId(orderId: string) {
    return this.http.get<any>(`/inventory/order-items-adjustements/list/${orderId}`)
  }

  createOrderAdjustement(orderId: string, data: any) {
    return this.http.post<any>(`/inventory/orderAdjustement/${orderId}/create/${data.orderItemId}`, {
      body: JSON.stringify(data),
    })
  }

  updateOrderItemAdjustement(data: any) {
    return this.http.post<any>(`/inventory/orderAdjustement/update/${data.adjustmentId}`, {
      body: JSON.stringify(data),
    })
  }

  deleteOrderItemAdjustement(adjustmentId: any) {
    return this.http.delete<any>(`/inventory/orderAdjustment/${adjustmentId}`)
  }

  getItemLot(wholesaleItemId: number) {
    return this.http.get(`/inventory/orderItemLot/${wholesaleItemId}?available=true`)
  }

  getAvailableItemLot(salesType: string) {
    return this.http.get(`/inventory/getAvailableItemLot?type=${salesType}`)
  }

  getDocuments(data: any) {
    const {orderId, ...rest} = data
    return this.http.get(
      `/account/user/getOrderDocuments/${orderId}`,{query: rest}
    )
  }

  createDocument(orderId: string, data: any) {
    return this.http.post(`/account/user/createOrderDocument/${orderId}`, {
      body: JSON.stringify(data),
    })
  }

  updateDocument(orderId: string, id: string, data: any) {
    return this.http.put(`/account/user/updateOrderDocument/${orderId.toString()}/${id}`, {
      body: JSON.stringify(data),
    })
  }

  getPurchaseOrderDocuments(data: any) {
    const {orderId, ...rest} = data
    return this.http.get(
      `/vendor/user/getOrderDocuments/${orderId}`,{query: rest}
    )
  }

  createPurchaseOrderDocument(orderId: string, data: any) {
    return this.http.post(`/vendor/user/createOrderDocument/${orderId}`, {
      body: JSON.stringify(data),
    })
  }

  updatePurchaseOrderDocument(orderId: string, id: string, data: any) {
    return this.http.put(`/vendor/user/updateOrderDocument/${orderId.toString()}/${id}`, {
      body: JSON.stringify(data),
    })
  }

  getQuantityCatchWeight(orderItemId: string) {
    return this.http.get(`/inventory/getQuantityCatchWeight/${orderItemId}?timestamp=${new Date().getTime()}`)
  }

  getWeightsByOrderItemIds(orderItemIds: any[]) {
    return this.http.post(`/inventory/getQuantityCatchWeight`, {
      body: JSON.stringify(orderItemIds),
    })
  }

  addQuantityCatchWeight(orderItemId: string, data: any) {
    return this.http.post(`/inventory/addQuantityCatchWeight/${orderItemId}?timestamp=${new Date().getTime()}`, {
      body: JSON.stringify(data),
    })
  }

  addCatchWeightByNumber(orderItemId: string, n: number) {
    return this.http.post(`/inventory/addCatchWeightByNumber/${orderItemId}?count=${n}`)
  }

  deleteCatchWeightByNumber(orderItemId: string, n: number) {
    return this.http.post(`/inventory/deleteCatchWeightByNumber/${orderItemId}?count=${n}`)
  }

  updateQuantityCatchWeight(qtyDetailId: number, data: any) {
    return this.http.post(`/inventory/updateQuantityCatchWeight/${qtyDetailId}`, {
      body: JSON.stringify(data),
    })
  }

  deleteQuantityCatchWeight(qtyDetailId: number) {
    return this.http.post(`/inventory/deleteQuantityCatchWeight/${qtyDetailId}`)
  }

  getPallets(orderItemId: number) {
    return this.http.get(`/inventory/getPalletDetail/${orderItemId}`)
  }

  updatePalletDetail(palletId: number, data: any) {
    return this.http.post(`/inventory/updatePalletDetail/${palletId}`, {
      body: JSON.stringify(data),
    })
  }

  updateAllPalletDetail(data: any) {
    return this.http.post(`/inventory/updateAllPalletDetail`, {
      body: JSON.stringify(data),
    })
  }

  getWeightSheets(palletId: any) {
    return this.http.get(`/inventory/getWeightSheets/${palletId}`)
  }

  getTotalUnitsByAllPallets(orderItemId: any) {
    return this.http.get(`/inventory/getTotalUnitsByAllPallets/${orderItemId}`)
  }

  saveWeightSheet(data: any) {
    return this.http.post(`/inventory/saveWeightSheet?noCancel`, {
      body: JSON.stringify(data),
    })
  }

  getRelatedOrders(vendorId: number) {
    return this.http.get<any[]>(`/inventory/${vendorId}/relatedorders/list`)
  }

  getAllSales(lotId: number) {
    return this.http.get<any[]>(`/inventory/allsales/${lotId}`)
  }

  getSalesPrice(itemId: number, clientId: string, from: string, to: string) {
    return this.http.get<any[]>(`/inventory/allsalesprice/${itemId}?from=${from}&to=${to}&clientId=${clientId}`)
  }

  getAllPurchases(lotId: number) {
    return this.http.get<any[]>(`/inventory/allpurchases/${lotId}`)
  }

  getAllVendors() {
    return this.http.get<any[]>(`/vendor/list`)
  }

  getShippingAddresses() {
    return this.http.get<any[]>(`/vendor/companyAddresses`)
  }

  duplicateOrder(data: any) {
    // if (data.userId == null) data.userId = userId
    return this.http.post<any>(`/inventory/duplicate-order/${data.wholesaleOrderId}`, {
      body: JSON.stringify(data),
    })
  }

  duplicateMostRecentOrder(data: any) {
    return this.http.post<any>(`/inventory/duplicate/${data.clientId}/latest-order`, {
      body: JSON.stringify(data.body),
    })
  }

  duplicatePurchaseOrder(data: any, userId: string) {
    if (data.userId == null) data.userId = userId
    return this.http.post<any>('/inventory/duplicate/purchase-order', {
      body: JSON.stringify(data),
    })
  }

  updateCompanyAddress(address: Address) {
    return this.http.put<any[]>(`/vendor/companyAddresses`, {
      body: JSON.stringify(address),
    })
  }

  syncQBOOrder(orderId: string) {
    return this.http.post<any>(`/inventory/user/sendQBOOrder/${orderId}`)
  }

  syncNSOrder(orderId: string) {
    return this.http.post<any>(`/netsuite/sendOrder/${orderId}`)
  }

  forceTokenRefresh() {
    return this.http.post<any>(`/netsuite/forceTokenRefresh`)
  }

  setShippingAddress(orderId: string, wholesaleAddressId: number) {
    return this.http.put<any[]>(`/inventory/updateShipping/${orderId}/${wholesaleAddressId}`)
  }

  sendPOEmail(data: any, to: string | null, cc: string | null) {
    return this.http.post(`/inventory/sendPurchaseEmail/${data.orderId}?to=${to}${cc ? `&cc=${cc}` : ''}`, {
      body: JSON.stringify({ itemList: data.itemList }),
    })
  }

  orderPrintVersion(orderId: number) {
    return this.http.get<any>(`/inventory/printVersion/${orderId}`)
  }

  createOffItem(data: any) {
    return this.http.post<any>(`/inventory/order/${data.orderId}/order-item`, {
      body: JSON.stringify(data),
    })
  }

  updateOffItem(data: any) {
    return this.http.put<any>(`/inventory/order-item/${data.wholesaleOrderItemId}`, {
      body: JSON.stringify(data),
    })
  }

  removeOffItem(data: any) {
    return this.http.delete<any>(`/inventory/order-item/${data.wholesaleOrderItemId}`, {
      body: JSON.stringify(data),
    })
  }

  getFrequentSalesOrderItems(data: any) {
    let url = `/inventory/${data.clientId}/frequent-so-items`
    if (data.search != '') {
      url += `?search=${data.search}`
    }
    return this.http.get<any>(url)
  }

  finalizePurchaseOrder(clientId: number, data: any) {
    return this.http.post<any>(`/inventory/simple-purchase-order/${clientId}`, {
      body: JSON.stringify(data),
    })
  }

  getPrintOrders(fromDate: string, toDate: string, searchStr: string, data: null) {
    if (data) {
      return this.http.post('/inventory/print-orders', {
        body: JSON.stringify(data),
      })
    }
    return this.http.post(`/inventory/print-orders?from=${fromDate}&to=${toDate}&search=${searchStr}`)
  }

  udpateOrderItemsDisplayOrder(data: any) {
    return this.http.post<any>(`/inventory/update-orderitems-order/`, {
      body: JSON.stringify(data),
    })
  }

  updateOrderItemPricingLogic(data: any) {
    return this.http.put<any>(`/inventory/orderItem-logic/${data.wholesaleOrderItemId}?noCancel`, {
      body: JSON.stringify(data),
    })
  }

  updatePOItemUom(data: any) {
    return this.http.put<any>(`/inventory/orderItem-uom/${data.id}?noCancel`, {
      body: JSON.stringify(data),
    })
  }

  addQuantityCatchWeightByList(orderItemId: string, data: any) {
    return this.http.post<any>(`/inventory/addQuantityCatchWeightByList/${orderItemId}`, {
      body: JSON.stringify(data),
    })
  }

  copyOrderQty(data: any) {
    return this.http.post<any>(`/inventory/copy-order-qty`, {
      body: JSON.stringify(data),
    })
  }

  getPurchaseOrderProfitability(orderId: number) {
    return this.http.get(`/vendor/purchase-order/profitability/${orderId}`)
  }

  getPurchaseOrderItemHistory(orderItemId: any) {
    return this.http.get(`/inventory/purchase-order-item-history/${orderItemId}`)
  }
  getPurchaseOrderItemCost(orderItemId: any) {
    return this.http.get(`/inventory/sales-items?wholesaleOrderItemId=${orderItemId}`)
  }

  receiveMaterials(salesOrderId: any, status: boolean, itemIds: any[]) {
    return this.http.post(`/manufacture/update-sales-order/${salesOrderId}?status=${status}`, {
      body: JSON.stringify({ itemIds: itemIds }),
    })
  }

  sendProductWarehouse(purchaseOrderId: any, status: boolean) {
    return this.http.post(`/manufacture/update-purchase-order/${purchaseOrderId}?status=${status}`)
  }

  getRelatedOrderItemList({ orderId, query }: any) {
    return this.http.get<any[]>(`/inventory/relatedOrderItems/list/${orderId}`, { query })
  }

  getPrintCashSales(fromDate: string, toDate: string, searchStr: string) {
    return this.http.get(`/inventory/print-cash-sales?from=${fromDate}&to=${toDate}&search=${searchStr}`)
  }

  getWholesaleCashSaleOrders(query: any) {
    return this.http.get<Order[]>('/inventory/warehouse/wholesaleCashSaleOrders', { query })
  }

  getCashCustomerClient({ userId }: { userId: string | number }) {
    return this.http.get(`/account/user/${userId}/customer/cashCustomer`)
  }

  getReferenceList() {
    return this.http.get('/inventory/get-references')
  }

  getCustomOrderNoList() {
    return this.http.get('/inventory/get-custom-order-numbers')
  }

  updateItemsPrice(data: any) {
    return this.http.put('/inventory/items-price', { body: JSON.stringify(data) })
  }

  updateSalesPrice(orderId: string, data: any) {
    return this.http.put(`/inventory/sales-price/${orderId}`, { body: JSON.stringify(data) })
  }

  getItemCostByItemIdAndClientId(data: any) {
    return this.http.get(`/inventory/order-items/client/${data.clientId}`, { query: { itemIds: data.itemIds } })
  }

  addOrderItemForOrder(data: any) {
    return this.http.post(`/account/${data.wholesaleOrderId}/order-item?noCancel`, {
      body: JSON.stringify(data),
    })
  }

  removeOrderItemForOrder(data: any) {
    return this.http.delete(`/account/order-item/${data}`)
  }

  getSalesOrderBatchPick(orderIds: string) {
    return this.http.get('/inventory/batch-pick', { query: { orderIds } })
  }

  getCustomerItems(sellerId: string | number) {
    return this.http.get('/inventory/customer-items', { query: { sellerId } })
  }

  saveEPickedValues(data: any, orderId: number) {
    return this.http.post(`/inventory/save-epicked-values/${orderId}`, { body: JSON.stringify(data) })
  }

  getLatestInvoiceEmail(clientId: number) {
    return this.http.get(`/inventory/latest-invoice-email?clientId=${clientId}`)
  }

  getPOAllocate(orderId: any) {
    return this.http.get(`/inventory/allocations?orderId=${orderId}`)
  }

  getAllExtraCharge(query: any) {
    return this.http.get(`/inventory/extra-charges`, { query })
  }

  getAllextraPOList(query: any) {
    return this.http.get(`/inventory/simplify-PO`, { query })
  }

  getPOPallets(orderId: any) {
    return this.http.get(`/inventory/pallets?orderId=${orderId}`)
  }

  addPOAllocateExtraCharges({ orderId, data }: any) {
    return this.http.post(`/inventory/allocation/${orderId}`, {
      body: data,
    })
  }

  saveSalesOrderPalletLabels(data: any, orderId: number) {
    return this.http.post(`/label/so-pallet-labels/${orderId}`, { body: data })
  }

  getSalesOrderPalletLabels(orderId: number) {
    return this.http.get(`/label/so-pallet-labels/${orderId}`)
  }

  setOrdersStatusToPicked(orderIds: any) {
    return this.http.put('/inventory/order-status', { body: JSON.stringify(orderIds) })
  }

  getOrderItemsByOrderIdForPOPage(type: number, data: any) {
    if (type == 1) {
      return this.http.get(`/inventory/order-items?orderId=${data}`)
    } else {
      return this.http.get(`/inventory/order-items?orderIds=${data}`)
    }
  }

  getOrderItemListByOrderId(orderId: number) {
    return this.http.get(`/inventory/order/${orderId}/order-items`)
  }

  getOrderItemByOrderItemId(id: number) {
    return this.http.get(`/inventory/order-item/${id}`)
  }

  updatePOIReceived({ wholesaleOrderItemId, data }: any) {
    return this.http.put(`/inventory/poi-received/${wholesaleOrderItemId}`, {
      body: data,
    })
  }

  getOrderLots({ productId, lotIds }: { productId: number; lotIds: string }) {
    return this.http.get(`/inventory/available-lots/${productId}?lotIds=${lotIds}`)
  }

  saveOrderLots({ orderId, data }: { orderId: string; data: any }) {
    return this.http.put(`/inventory/order/${orderId}/lot-assignment-method`, {
      body: data,
    })
  }

  updateOrderItemByProductId({
    orderId,
    productId,
    displayOrder,
    data,
  }: {
    orderId: string
    productId: string
    displayOrder: number
    data: any
  }) {
    return this.http.put(`/inventory/order/${orderId}/item/${productId}/display-order/${displayOrder}?noCancel`, {
      body: data,
    })
  }

  updateOrderItemPrice({
    orderId,
    productId,
    displayOrder,
    data,
  }: {
    orderId: string
    productId: string
    displayOrder: number
    data: any
  }) {
    return this.http.put(
      `/inventory/order/${orderId}/item/${productId}/display-order/${displayOrder}/orderItems-logic?noCancel`,
      {
        body: data,
      },
    )
  }

  deleteOrderItem({ orderId, productId, displayOrder }: { orderId: string; productId: string; displayOrder: number }) {
    return this.http.delete(`/account/order/${orderId}/item/${productId}/display-order/${displayOrder}`)
  }

  getOrderItemCatchWeights({
    orderId,
    productId,
    displayOrder,
  }: {
    orderId: string
    productId: number
    displayOrder: number
  }) {
    return this.http.get(`/inventory/order/${orderId}/item/${productId}/display-order/${displayOrder}/catch-weights`)
  }

  updateOrderItemCatchWeights({
    orderId,
    productId,
    displayOrder,
    data,
  }: {
    orderId: string
    productId: number
    displayOrder: number
    data: any
  }) {
    return this.http.put(`/inventory/order/${orderId}/item/${productId}/display-order/${displayOrder}/catch-weights`, {
      body: data,
    })
  }

  updateOrderItemProduct({
    orderId,
    productId,
    displayOrder,
    data,
  }: {
    orderId: string
    productId: number
    displayOrder: number
    data: any
  }) {
    return this.http.put(`/account/order/${orderId}/item/${productId}/display-order/${displayOrder}/change-item`, {
      body: data,
    })
  }

  adjustCost(orderId: number, data: any) {
    return this.http.put(`/account/order/${orderId}/adjust-cost`, {
      body: data,
    })
  }

  getItemAvailableItemLotById(wholesaleItemId: number) {
    return this.http.get(`/inventory/addLotItem/${wholesaleItemId.toString()}`)
  }

  updateSortOrderItemDisplayOrder(
    orderId: number,
    productId: number,
    oldDisplayOrder: number,
    newDisplayOrder: number,
  ) {
    return this.http.put(`/account/order/${orderId}/item/${productId}/display-order`, {
      body: {
        oldDisplayOrder,
        newDisplayOrder,
      },
    })
  }

  updateAutofillPicked(orderId: number) {
    return this.http.put(`/inventory/order/${orderId}/autofill-picked`)
  }

  getOrderEPickCatchWeight(orderId: string) {
    return this.http.get(`/inventory/order/${orderId}/quantity-details`)
  }

  saveOrderEPickCatchWeight(orderId: string, data: any) {
    return this.http.put(`/inventory/order/${orderId}/ePick-sheet`, {
      body: data,
    })
  }

  deleteOrderItems(orderId: number, data: any) {
    return this.http.delete(`/account/order/${orderId}/order-items`, {
      body: data,
    })
  }

  createContainer(data: any) {
    return this.http.post(`/container`, {
      body: data,
    })
  }

  getOrderContainerList(orderId: string) {
    return this.http.get(`/container/containers/order/${orderId}`)
  }

  getContainerDetail(containerId: string) {
    return this.http.get(`/container/${containerId}`)
  }

  getUnassignOrderItemsCount(orderId: string) {
    return this.http.get(`/inventory/order/${orderId}/unassign-order-item-count`)
  }

  getUnassignOrderItems(orderId: string) {
    return this.http.get(`/inventory/order/${orderId}/unassign-order-items`)
  }

  getAssignModalContainers() {
    return this.http.get(`/container/containers?status=1000`)
  }

  setTmsFlowItems(data: any, orderId: string) {
    return this.http.post(`/tms-flow-item/oms/${orderId}/tms-flow-items`, {
      body: data,
    })
  }

  updateContainerDetail(data: any) {
    return this.http.put(`/container`, {
      body: data,
    })
  }

  getLogisticOrder(containerId: string) {
    return this.http.get(`/logistic-order/${containerId}`)
  }
  updateLogisticOrder(data: any) {
    return this.http.put(`/logistic-order`, {
      body: data,
    })
  }

  getContainersSummary(query: any) {
    return this.http.get(`/container/containers-summary`, { query })
  }

  getAutoContainer(query: any) {
    return this.http.get(`/inventory/order-items/container-type`, { query })
  }

  saveAutoAssignContainer(data: any) {
    return this.http.post(`/container/auto-assign`, {
      body: data,
    })
  }

  updateOrderItemWeightVolume(orderId: string, data: any) {
    return this.http.put(`/inventory/order/${orderId}/order-item/weight-volume`, {
      body: data,
    })
  }

  getPrintContainer(containerId: string, orderId?: string) {
    if (orderId) return this.http.get(`/container/${containerId}/oms/${orderId}/print-container`)
    return this.http.get(`/container/${containerId}/print-container`)
  }

  deleteContainer(containerId: string) {
    return this.http.delete(`/container/${containerId}`)
  }

  containerReassignOrderItems(data: any) {
    return this.http.put(`/tms-flow-item/reassign-order-items`, {
      body: data,
    })
  }

  getTmsFlowItem(itemId: string) {
    return this.http.get(`/tms-flow-item/order-item/${itemId}/tms-flow-items`)
  }

  updateTmsFlowItem(data: any) {
    return this.http.put(`/tms-flow-item/split-order-item`, {
      body: data,
    })
  }

  getRepack(orderId: string) {
    return this.http.get(`/vendor/repack/${orderId}`)
  }

  deleteRepackItem(repackItemId: string) {
    return this.http.delete(`/vendor/repack/${repackItemId}`)
  }

  repackSalesOrder(data: any) {
    return this.http.post(`/inventory/sales-order-repack`, {
      body: data,
    })
  }

  saveRepack(orderItemId: string, data: any) {
    return this.http.post(`/vendor/repack/${orderItemId}`, {
      body: data,
    })
  }

  updateRepack(data: any) {
    return this.http.post(`/vendor/update-po-repacks`, {
      body: data,
    })
  }

  syncOrderItemTms(orderId: string, itemId: string, displayOrder: string) {
    return this.http.get(`/tms-flow-item/order/${orderId}/item/${itemId}/display-order/${displayOrder}/sync-to-container`)
  }

  getAllocations(orderId: string, type: number) {
    return this.http.get(`/inventory/allocation/orders?orderId=${orderId}&type=${type.toString()}`)
  }

  saveAllocations(orderId: string, data: any) {
    return this.http.post(`/inventory/allocation/chargeOrder/${orderId}`, {
      body: data,
    })
  }

  syncContainer(orderId: string) {
    return this.http.get(`/tms-flow-item/oms/${orderId}/sync-to-container`)
  }

  getActivityLog(orderId: string) {
    return this.http.get(`/report/customer-activity/${orderId}`)
  }

  recordPrintActivity(printType: string, orderId: string) {
    return this.http.get(`/report/log-print/${printType}/${orderId}`)
  }

  createSingleContainer(data: any) {
    return this.http.post('/single-container', {
      body: data
    })
  }

  getSingleContainerList() {
    return this.http.get(`/single-container/single-containers`)
  }

  getWholesaleContainer(wholesaleContainerId: string) {
    return this.http.get(`/single-container/${wholesaleContainerId}`)
  }

  updateWholesaleContainer(data: any) {
    return this.http.put(`/single-container`, {
      body: data
    })
  }

  getWholesaleSailing(sailingId: string) {
    return this.http.get(`/sailing-info/${sailingId}`)
  }

  updateWholesaleSailing(data: any) {
    return this.http.put(`/sailing-info`, {
      body: data
    })
  }

  getSingleContainerPrint(orderId: string) {
    return this.http.get(`/single-container/order/${orderId}/print-data`)
  }

  setOrderItemsReceived(orderId: string, qtyType: string) {
    return this.http.post(`/inventory/set-order-items-received/${orderId}?qty_type=${qtyType}`)
  }

  getSyncQboOrderDetails(orderId: string) {
    return this.http.get(`/quickbook/getSyncQboOrderDetails/${orderId}/`)
  }

  syncQboOrderItems(orderId: string, body: any) {
    return this.http.post(`/quickbook/syncQboOrderItems/${orderId}`, {
      body
    })
  }

  validateImportCSV(itemList: any) {
    // const params = new URLSearchParams({itemList}).toString();
    return this.http.post(`/inventory/import-csv-validation`, {
      body: JSON.stringify({ itemList })
    })
  }

  getSoldItemsById(body: any) {
    return this.http.post(`/inventory/sold-items`, {
      body
    })
  }
}
