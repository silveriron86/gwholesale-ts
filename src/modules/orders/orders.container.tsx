import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { RouteComponentProps, RouteProps } from 'react-router'

import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from './orders.module'

import { OrderTable, OrderHeader, presets } from './components'
import { AuthUser, Order, OrderItem, UserRole } from '~/schema'
import { ResizeCallbackData } from 'react-resizable'
import PageLayout from '~/components/PageLayout'
import moment from 'moment'
import { Modal, Icon } from 'antd'
import { CACHED_NS_LINKED, local } from '~/common'
import { OrderQBOSyncModal } from './components/order-sync-qbo-modal.container'
import OrderUploadCsv from './components/oderUploadCsv/order-upload-csv'
import { PrintPickSheet } from '../customers/sales/cart/print'
import { fullButton } from '../customers/sales/_style'
import { ThemeButton, ThemeSpin } from '../customers/customers.style'

import { printWindow, getFulfillmentDate, isSeller } from '~/common/utils'

import _ from 'lodash'
import { produce } from 'immer'
import BatchPickModal from './components/orde-batch-pick-modal'
import PrintInvoice from '../customers/sales/cart/print/print-invoice'
import AutoAssignContainerModal from './components/order-auto-assign-containers'
import SalesOrderUploadCsv from './components/oderUploadCsv/so-upload-csv'

export const presetsArr: any[] = [
  {
    key: 0,
    label: 'Past 7 Days',
    from: moment().subtract(7, 'days'),
    to: moment(),
  },
  {
    key: 1,
    label: 'Past 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment(),
  },
  {
    key: 9,
    label: 'Yesterday',
    from: moment().subtract(1, 'days'),
    to: moment().subtract(1, 'days'),
  },
  {
    key: 2,
    label: 'Today',
    from: moment(),
    to: moment(),
  },
  {
    key: 3,
    label: 'Tomorrow',
    from: moment().add(1, 'days'),
    to: moment().add(1, 'days'),
  },
  {
    key: 4,
    label: 'Today and Tomorrow',
    from: moment(),
    to: moment().add(1, 'days'),
  },
  {
    key: 5,
    label: 'Next 7 Days',
    from: moment(),
    to: moment().add(7, 'days'),
  },
  {
    key: 6,
    label: 'Next 30 Days',
    from: moment(),
    to: moment().add(30, 'days'),
  },
  {
    key: 7,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment().add(30, 'days'),
  },
  {
    key: 8,
    label: 'Custom Date Range',
  },
]

export type OrdersProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteProps &
  RouteComponentProps & {
    currentUser: AuthUser
    onResize?: (e: React.SyntheticEvent, data: ResizeCallbackData) => any
  }

export class OrdersComponent extends React.PureComponent<OrdersProps> {
  dateFormat = 'MM/DD/YYYY'
  state: any
  isTableChange: boolean = false
  constructor(props: OrdersProps) {
    super(props)
    const type = this.props.location.pathname.indexOf('/sales-orders') >= 0 ? 'CUSTOMER' : 'VENDOR'
    const sessionSearch = localStorage.getItem(`${type}_ORDERS_SEARCH`)
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search)
    const search = urlParams.get('search')
    const startDate = urlParams.get('datestart')
    const endDate = urlParams.get('dateend')
    const page = urlParams.get('page')
    const preset = urlParams.get('preset')

    // filters
    const status = urlParams.get('status')
    const pas = urlParams.get('pas')
    const orderDate = urlParams.get('orderDate')
    const buyer = urlParams.get('buyerId')
    const vendor = urlParams.get('clientId')
    const productSku = urlParams.get('productSku')
    const productName = urlParams.get('productName')
    const businessType = urlParams.get('businessType')

    let fromDate = moment()
    let toDate = moment()
    if (preset === '8') {
      if (startDate) {
        fromDate = moment(startDate, 'MMDDYY')
      }
      if (endDate) {
        toDate = moment(endDate, 'MMDDYY')
      }
    }

    let defaultPreset = 2
    if (!isSeller(props.currentUser.accountType)) {
      defaultPreset = 7
    }

    this.state = {
      searchStr: search ? search.trim() : '',
      curPreset: preset !== '' && preset !== null ? parseInt(preset, 10) : startDate || endDate ? 8 : defaultPreset,
      fromDate,
      toDate,
      fromDateString: fromDate.format(this.dateFormat),
      toDateString: toDate.format(this.dateFormat),
      showModal: false,
      showUploadCSV: false,
      showAutoAssignContainer: false,
      page: page ? parseInt(page, 10) : 0,
      pageSize: 20,
      orderByFilter: '',
      direction: '',
      printPickSheetReviewShow: false,
      batchPickModalVisible: false,
      PrintBillOfLadingReviewShow: false,
      printPickSheetRef: null,
      PrintBillOfLadingRef: null,
      finishLoadingPrintLogo: this.props.logo === 'default' ? true : false,
      filters: {
        status: status ? status.split(',') : [],
        pas: pas ? pas : 'all',
        orderDate,
        buyer: buyer ? buyer.split(',') : [],
        vendor: vendor ? vendor.split(',') : [],
        productSku,
        productName,
        businessType: businessType ? businessType.split(',') : [],
      },
      uploadMode: '',
      soStatuses: local.get('soStatuses') || [], // for sales orders
    }
  }

  componentDidMount() {
    this.props.resetOrder()
    const { curPreset, fromDate, toDate } = this.state
    const { currentUser } = this.props

    if (curPreset === 8) {
      this.onPresetChange(curPreset, { from: fromDate, to: toDate })
    } else {
      const selected = presets.find((p) => p.key === curPreset)
      this.onPresetChange(curPreset, selected)
    }
    if (currentUser.accountType != UserRole.CUSTOMER) {
      this.props.getPrintSetting()
      this.props.getSellerSetting()
      this.props.getCompanyProductAllTypes()
    }

    const type = this.props.location.pathname.indexOf('/sales-orders') >= 0 ? 'CUSTOMER' : 'VENDOR'
    if (isSeller(currentUser.accountType)) {
      if (type === 'CUSTOMER') this.props.getSimplifyCustomers()
      else this.props.getSimplifyVendors(true)
    }
    if (type == 'VENDOR') {
      this.props.getItemList({ type: 'PURCHASE' })
    }

    this.props.clearContainerList()

    // this.props.getAllRoutes()
  }

  setFilters = (data: any) => {
    this.setState(
      produce<any>((draft) => {
        draft.filters = data
      }),
      () => {
        this.getAllOrders()
        this.updateURL()
      },
    )
  }

  setSOStatus = (soStatuses: any) => {
    this.setState(
      {
        soStatuses
      },
      () => {
        this.getAllOrders()
      },
    )
  }

  getAllOrders() {
    const { filters, soStatuses } = this.state
    const searchObj = {
      ...this.state,
      fromDate: this.state.fromDate.format('MM/DD/YYYY'),
      toDate: this.state.toDate.format('MM/DD/YYYY'),
    }

    this.updateURL()

    const { currentUser } = this.props
    this.props.resetLoading()
    if (isSeller(currentUser.accountType)) {
      if (this.props.location.pathname === '/sales-orders') {
        // status=OPEN,CLOSED
        this.props.getWarehouseSalesOrders({
          ...searchObj,
          status: soStatuses.indexOf('NEW') >= 0 ? [...soStatuses, 'PLACED'] : soStatuses,
        })
        local.set('soStatuses',soStatuses)
      } else if (this.props.location.pathname === '/purchase-orders') {
        const filtersQueryString = filtersToQuery(filters)
        if (filtersQueryString) {
          searchObj.filtersQueryString = filtersQueryString
        }
        localStorage.setItem('GET_PO_SEARCH', JSON.stringify(searchObj))
        this.props.getWarehousePurchaseOrders(searchObj)
        if (!this.isTableChange) {
          this.props.getPurchaseLotItemsDownload(searchObj)
        }
        this.isTableChange = false
      }
    } else {
      this.props.getOrdersForCustomerUserId(searchObj)
    }
  }

  updateURL = () => {
    var url = new URL(window.location.href)
    var queryParams = url.searchParams
    const { searchStr, fromDate, toDate, page, curPreset, filters } = this.state
    if (searchStr) {
      queryParams.set('search', searchStr)
    }
    queryParams.set('preset', curPreset.toString())
    if (curPreset === 8) {
      if (fromDate) {
        queryParams.set('datestart', moment(fromDate).format('MMDDYY'))
      }
      if (toDate) {
        queryParams.set('dateend', moment(toDate).format('MMDDYY'))
      }
    }
    if (page > 0) {
      queryParams.set('page', page.toString())
    }

    const queryFilters = filtersToQuery(filters)

    url.search = queryParams.toString() + queryFilters
    const type = this.props.location.pathname.indexOf('/sales-orders') >= 0 ? 'CUSTOMER' : 'VENDOR'
    localStorage.setItem(`${type}_ORDERS_SEARCH`, url.search)


    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString())
  }

  componentWillReceiveProps(nextProps: OrdersProps) {
    if (nextProps.sellerSetting && nextProps.sellerSetting.company) {
      const db_dataListOptions = [
        'PAST_7_DAYS',
        'PAST_30_DAYS',
        'TODAY',
        'TOMORROW',
        'TODAY_AND_TOMORROW',
        'NEXT_7_DAYS',
        'NEXT_30_DAYS',
        'PAST_30_DAYS_AND_NEXT_30_DAYS',
        '',
        'YESTERDAY',
      ]
      const type = location.href.indexOf('sales-') >= 0 ? 'Sales' : 'Purchase'
      if (type == 'Sales') {
        if (localStorage.getItem('saleFilter') != '0') {
          const index = db_dataListOptions.indexOf(nextProps.sellerSetting.company.salesOrderDateFile)
          if (index !== this.state.curPreset) {
            const selected = presetsArr.find((p) => p.key === index)
            this.onPresetChange(index, selected)
          }
        }
      }
      if (type == 'Purchase') {
        if (localStorage.getItem('purchaseFilter') != '0') {
          const index = db_dataListOptions.indexOf(nextProps.sellerSetting.company.purchaseOrderDateFilter)
          if (index !== this.state.curPreset) {
            const selected = presetsArr.find((p) => p.key === index)
            this.onPresetChange(index, selected)
          }
        }
      }
    }
    if (nextProps.newOrderId > 0) {
      const type = this.props.location.pathname.indexOf('/sales-orders') >= 0 ? 'CUSTOMER' : 'VENDOR'
      if (type === 'CUSTOMER') {
        this.props.history.push(`/sales-order/${nextProps.newOrderId}/`)
      } else {
        this.props.history.push(`/order/${nextProps.newOrderId}/purchase-cart`)
      }
      this.props.resetNewOrderId()
    }
    if (JSON.stringify(this.props.sellerSetting) != JSON.stringify(nextProps.sellerSetting)) {
    }
    if (nextProps.loading == false && this.state.showModal == true) this.onCloseModal()

    if (this.props.loading !== nextProps.loading && nextProps.loading === false) {
      const { page, pageSize } = this.state
      const { orderTotal } = nextProps
      if (orderTotal <= page * pageSize) {
        this.setState(
          {
            page: 0,
          },
          () => {
            this.updateURL()
          },
        )
      }
    }
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  onDateChange = (dates: [any, any], dateStrings: [string, string]) => {
    const state = { ...this.state }
    state['fromDate'] = dates[0]
    state['toDate'] = dates[1]
    // state['page'] = 0
    this.setState(state, () => {
      this.getAllOrders()
    })
  }

  onSync = (orderIds: string[]) => {
    if (this.props.loading == true) return
    this.props.resetLoading()

    var isNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    if (isNS) {
      this.props.setBatchMax(orderIds.length)
      this.props.syncNSOrders(orderIds)
    } else this.props.syncQBOOrders(orderIds)
  }

  onOpenModal = () => {
    this.setState({
      showModal: true,
    })
  }

  onShowUploadCSV = (mode: string) => {
    this.setState({
      uploadMode: mode,
    })
    this.setState({
      showUploadCSV: true,
    })
  }

  onShowAutoAssignContainer = () => {
    this.setState({
      showAutoAssignContainer: true,
    })
  }

  onCloseUploadCSV = () => {
    this.setState({
      showUploadCSV: false,
    })
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
    })
  }

  OnTableChange = (pagination: any, sorter: any) => {
    const state = { ...this.state }
    if (pagination !== null) {
      state['page'] = pagination.current - 1
    }
    if (sorter && Array.isArray(sorter)) {
      const filterStr = sorter
        .map((el: any) => {
          return el.dataIndex
        })
        .join(',')
      const directionStr = sorter
        .map((el: any) => {
          return el.sortOrder === 'ascend' ? 'ASC' : 'DESC'
        })
        .join(',')
      state['orderByFilter'] = filterStr ? filterStr : ''
      state['direction'] = directionStr ? directionStr : ''
    }

    this.setState(state, () => {
      this.isTableChange = true
      this.getAllOrders()
    })
  }

  onCreate = (clientNumber: string) => {
    // const { routes } = this.props

    const quotedDate = moment(new Date()).format('MM/DD/YYYY')
    const { sellerSetting } = this.props
    let fulfillmentDate = getFulfillmentDate(sellerSetting)
    let data = {
      wholesaleClientId: clientNumber,
      orderDate: quotedDate,
      quotedDate: quotedDate,
      // deliveryDate: quotedDate, // default for Scheduled Delivery Date to be the same as what you are setting for Quoted Delivery Date
      deliveryDate: fulfillmentDate,
      scheduledDeliveryTime: moment().format('h:mm a'),
      itemList: [],
    }
    this.props.createEmptyOrder(data)
  }

  onSearch = (value: string) => {
    const state = { ...this.state }
    state['searchStr'] = value.trim()
    state['page'] = 0
    this.setState(state, () => {
      this.getAllOrders()
    })
  }

  onPrintPicks = () => {
    this.setState({
      printPickSheetReviewShow: true,
    })

    this._loadPrintData()
  }

  _loadPrintData = () => {
    const { searchStr, fromDate, toDate } = this.state
    this.props.startPrintOrders()
    this.props.getPrintOrders({
      fromDate: fromDate.format('MM/DD/YYYY'),
      toDate: toDate.format('MM/DD/YYYY'),
      searchStr,
      body: null,
    })
  }

  onPickDelivery = () => {
    this.setState({
      PrintBillOfLadingReviewShow: true,
    })

    this._loadPrintData()
  }

  onShowBatchPick = () => {
    this.setState({
      batchPickModalVisible: true,
    })
  }

  closePrintModal = (flag: number) => {
    if (flag === 1) {
      this.setState({
        printPickSheetReviewShow: false,
      })
    } else {
      this.setState({
        PrintBillOfLadingReviewShow: false,
      })
    }
  }

  changePrintLogoStatus = () => {
    this.setState({
      finishLoadingPrintLogo: true,
    })
  }

  onPresetChange = (curPreset: number, selected: any) => {
    this.setState(
      {
        page: 0,
        curPreset,
      },
      () => {
        const dateFormat = 'MM/DD/YYYY'
        if (selected && selected.from && selected.to) {
          this.onDateChange(
            [selected.from, selected.to],
            [selected.from.format(dateFormat), selected.to.format(dateFormat)],
          )
        }
      },
    )
  }

  downloadLotList = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('export_lot_list'))
    var wscols = [
      { wpx: 100 },
      { wpx: 300 },
      { wpx: 200 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols
    const today = moment().format('MM.DD.YYYY')
    const title = `Purchase Lots List-${today}.xlsx`
    XLSX.writeFile(wb, title)
  }

  getPrintOrderItems = (payload: any[]) => {
    const orderItems = payload.map((v) => ({
      ...v,
      displayOrderProduct: _.floor(v.displayOrder),
      displayOrderItem: String(v.displayOrder).substring(String(v.displayOrder).indexOf('.') + 1),
    }))

    const dataGroupByItemId = _.groupBy(orderItems, 'displayOrderProduct')
    const productIds = Object.keys(dataGroupByItemId)
    const orderItemByProduct = {}
    productIds.forEach((productId) => {
      orderItemByProduct[productId] = {}
      let quantity = 0
      let picked = 0
      orderItemByProduct[productId].locationNames = []
      dataGroupByItemId[productId].forEach((orderItem) => {
        quantity = quantity + Number(orderItem.quantity)
        picked = picked + Number(orderItem.picked)
        orderItemByProduct[productId].locationNames = orderItemByProduct[productId].locationNames.concat(
          orderItem.locationNames,
        )
      })
      const catchWeightQty = dataGroupByItemId[productId].map((v) => v.catchWeightQty)
      orderItemByProduct[productId].items = dataGroupByItemId[productId]

      orderItemByProduct[productId].quantity = quantity
      orderItemByProduct[productId].picked = picked
      orderItemByProduct[productId].lotIds = _.compact(
        _.orderBy(dataGroupByItemId[productId], 'wholesaleOrderItemId').map((v) => v.lotId),
      )
      orderItemByProduct[productId].displayOrderProduct = dataGroupByItemId[productId][0].displayOrderProduct
      orderItemByProduct[productId].variety = dataGroupByItemId[productId][0].variety
      orderItemByProduct[productId].SKU = dataGroupByItemId[productId][0].SKU
      orderItemByProduct[productId].itemId = dataGroupByItemId[productId][0].itemId
      orderItemByProduct[productId].price = dataGroupByItemId[productId][0].price
      orderItemByProduct[productId].lotAssignmentMethod = dataGroupByItemId[productId][0].lotAssignmentMethod
      orderItemByProduct[productId].enableLotOverflow = dataGroupByItemId[productId][0].enableLotOverflow
      orderItemByProduct[productId].wholesaleProductUomList = dataGroupByItemId[productId][0].wholesaleProductUomList
      orderItemByProduct[productId].UOM = dataGroupByItemId[productId][0].UOM
      orderItemByProduct[productId].pricingUOM = dataGroupByItemId[productId][0].pricingUOM
      orderItemByProduct[productId].inventoryUOM = dataGroupByItemId[productId][0].inventoryUOM
      orderItemByProduct[productId].pricingLogic = dataGroupByItemId[productId][0].pricingLogic
      orderItemByProduct[productId].overrideUOM = dataGroupByItemId[productId][0].overrideUOM
      orderItemByProduct[productId].useForSelling = dataGroupByItemId[productId][0].useForSelling
      orderItemByProduct[productId].useForPurchasing = dataGroupByItemId[productId][0].useForPurchasing
      orderItemByProduct[productId].manuallyAssignState = dataGroupByItemId[productId][0].manuallyAssignState
      orderItemByProduct[productId].tax = dataGroupByItemId[productId][0].tax
      orderItemByProduct[productId].taxRate = dataGroupByItemId[productId][0].taxRate
      orderItemByProduct[productId].taxEnabled = dataGroupByItemId[productId][0].taxEnabled

      orderItemByProduct[productId].pricingGroup = dataGroupByItemId[productId][0].pricingGroup
      orderItemByProduct[productId].pas = dataGroupByItemId[productId][0].pas
      orderItemByProduct[productId].modifiers = dataGroupByItemId[productId][0].modifiers
      orderItemByProduct[productId].extraOrigin = dataGroupByItemId[productId][0].extraOrigin
      orderItemByProduct[productId].labelSerial = dataGroupByItemId[productId][0].labelSerial
      orderItemByProduct[productId].status = dataGroupByItemId[productId][0].status
      orderItemByProduct[productId].catchWeightQty = catchWeightQty.reduce((n, p) => n + p, 0)
      orderItemByProduct[productId].workOrderStatus = dataGroupByItemId[productId][0].workOrderStatus
      orderItemByProduct[productId].editedItemName = dataGroupByItemId[productId][0].editedItemName
      orderItemByProduct[productId].defaultGrossWeight = dataGroupByItemId[productId][0].defaultGrossWeight
      orderItemByProduct[productId].grossWeight = dataGroupByItemId[productId][0].defaultGrossWeight
      orderItemByProduct[productId].grossWeightUnit = dataGroupByItemId[productId][0].grossWeightUnit
      orderItemByProduct[productId].grossVolume = dataGroupByItemId[productId][0].grossVolume
      orderItemByProduct[productId].grossVolumeUnit = dataGroupByItemId[productId][0].grossVolumeUnit
      orderItemByProduct[productId].cost = dataGroupByItemId[productId][0].cost
      orderItemByProduct[productId].lotCost = dataGroupByItemId[productId][0].lotCost
      orderItemByProduct[productId].perAllocateCost = dataGroupByItemId[productId][0].perAllocateCost
      orderItemByProduct[productId].plu = dataGroupByItemId[productId][0].plu
      orderItemByProduct[productId].packing = dataGroupByItemId[productId][0].packing
      orderItemByProduct[productId].size = dataGroupByItemId[productId][0].size
      orderItemByProduct[productId].note = dataGroupByItemId[productId][0].note
      orderItemByProduct[productId].customerProductCode = dataGroupByItemId[productId][0].customerProductCode
      orderItemByProduct[productId].wholesaleOrderItemId = _.orderBy(
        dataGroupByItemId[productId],
        'wholesaleOrderItemId',
      )[0].wholesaleOrderItemId
    })
    return Object.values(orderItemByProduct)
  }

  render() {
    const {
      searchStr,
      curPreset,
      fromDate,
      toDate,
      page,
      pageSize,
      printPickSheetReviewShow,
      PrintBillOfLadingReviewShow,
      filters,
      soStatuses,
    } = this.state
    const {
      orders,
      orderTotal,
      itemList,
      loadingSaleItems,
      printOrders,
      printSetting,
      loadingPrintOrders,
      setOrdersStatusToPicked,
      loadingPurchaseProducts,
      enterPurchaseProducts,
      getVendorProductsForEnterPurchase,
      getItemList,
      loadingPurchaseProducts,
      companyProductTypes,
      loadingSimplifyVendors,
    } = this.props
    // const filteredOrders = orders
    // console.log('printOrders = ', printOrders)
    const type = this.props.location.pathname.indexOf('/sales-orders') >= 0 ? 'CUSTOMER' : 'VENDOR'
    let lastOrders

    if (type === 'VENDOR') {
      /*
      lastOrders = orders.map((order) => {
        let totalData,
          totalCost = 0
        let shippingCost = order.shippingCost ? order.shippingCost : 0
        if (order.orderItems != null) {
          totalData = calPoTotalCost(order.orderItems)
        }
        totalCost = totalData.subTotal + shippingCost
        order.totalCost = totalCost
        return order
      })*/

      //based on updated api, we display order total from api directly
      lastOrders = orders
    } else {
      lastOrders = orders
    }

    const printContent: any = (type: number) => {
      let content
      if (type === 1) {
        content = this.state.printPickSheetRef
      } else {
        content = this.state.PrintBillOfLadingRef
      }
      return content
    }

    let pick_layout = 'portrait'
    let invoice_layout = 'portrait'
    if (printSetting) {
      const userPrintSetting = JSON.parse(printSetting)
      if (typeof userPrintSetting.pick_layout !== 'undefined') {
        pick_layout = userPrintSetting.pick_layout
      }
      if (typeof userPrintSetting.invoice_layout !== 'undefined') {
        invoice_layout = userPrintSetting.invoice_layout
      }
    }
    const notCanceledPrintOrders = printOrders.filter((el) => el.wholesaleOrderStatus != 'CANCEL')
    const company = this.props.sellerSetting ? this.props.sellerSetting.company : null

    //Get Invoice Template
    const customInvoiceTemplates = _.get(companyProductTypes, 'customInvoiceTemplate', [])
    let customInvoiceTemplateName = '';

    if(customInvoiceTemplates && customInvoiceTemplates.length === 1){
      customInvoiceTemplateName = customInvoiceTemplates[0].name
    }

    const printInvoiceName = customInvoiceTemplateName.replace(' ', '') + 'PrintInvoiceModal'
    const filterStatus = soStatuses.indexOf('NEW') >= 0 ? [...soStatuses, 'PLACED'] : soStatuses

    return (
      <PageLayout
        noSubMenu={true}
        currentTopMenu={
          this.props.location.pathname === '/sales-orders'
            ? 'menu-Selling & Shipping-Sales Orders'
            : 'menu-Purchasing-Purchase Orders'
        }
      >
        <OrderHeader
          loadingPurchaseProducts={loadingPurchaseProducts}
          enterPurchaseProducts={enterPurchaseProducts}
          getItemList={getItemList}
          getVendorProductsForEnterPurchase={getVendorProductsForEnterPurchase}
          getSimplifyVendors={this.props.getSimplifyVendors}
          search={searchStr}
          curPreset={curPreset}
          dates={[fromDate, toDate]}
          onPresetChange={this.onPresetChange}
          onDateChange={this.onDateChange}
          currentUser={this.props.currentUser}
          onCreate={this.onCreate}
          clients={
            this.props.location.pathname === '/sales-orders' ? this.props.simplifyCustomers : this.props.simplifyVendors
          }
          loadingSimplifyVendors={loadingSimplifyVendors}
          onSearch={this.onSearch}
          onShowModal={this.onOpenModal}
          loadingSaleItems={loadingSaleItems}
          saleItems={itemList}
          total={orderTotal}
          finalizePurchaseOrder={this.props.finalizePurchaseOrder}
          onPrintPicks={this.onPrintPicks}
          onShowUploadCSV={this.onShowUploadCSV}
          onShowAutoAssignContainer={this.onShowAutoAssignContainer}
          onPickDelivery={this.onPickDelivery}
          onShowBatchPick={this.onShowBatchPick}
          printSetting={this.props.printSetting}
          getAllItemsForAddPurchase={this.props.getItemList}
          updateProduct={this.props.updateProduct}
          setOrResetLoadingForGetItems={this.props.setOrResetLoadingForGetItems}
          company={company}
          companyProductTypes={this.props.companyProductTypes}
          setCompanyProductType={this.props.setCompanyProductType}
          settingCompanyName={this.props.settingCompanyName}
          sellerSetting={this.props.sellerSetting}
          orders={lastOrders}
          setFilters={this.setFilters}
          filters={filters}
          getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
          downloadLotList={this.downloadLotList}
          setSOStatus={this.setSOStatus}
          status={soStatuses}
        />
        <Modal
          width={1080}
          footer={null}
          visible={printPickSheetReviewShow}
          onCancel={() => this.closePrintModal(1)}
          wrapClassName="print-modal"
        >
          <ThemeSpin spinning={loadingPrintOrders}>
            <div id={'printPickSheetModal'} style={{ minHeight: 500 }}>
              <div
                ref={(el) => {
                  this.setState({ printPickSheetRef: el })
                }}
              >
                {printOrders.map((order, index) => {
                  if (order.wholesaleOrderStatus === 'CANCEL') {
                    return null
                  }

                  if (filterStatus.indexOf(order.wholesaleOrderStatus) >= 0) {
                    return (
                      <div key={`print-picksheet-${index}`}>
                        <PrintPickSheet
                          orderItems={order.printOrderItem}
                          currentOrder={order}
                          companyName={this.props.companyName}
                          catchWeightValues={[]}
                          logo={this.props.logo}
                          multiple={true}
                          changePrintLogoStatus={this.changePrintLogoStatus}
                          sellerSetting={this.props.sellerSetting}
                        />
                        <div style={{ pageBreakAfter: 'always' }} />
                      </div>
                    )
                  }
                  return null
                })}
              </div>
            </div>
            <ThemeButton
              shape="round"
              style={fullButton}
              className="print-pick-sheet"
              onClick={() => {
                printWindow('printPickSheetModal', printContent.bind(this, 1), pick_layout)
                let orderIds = _.map(
                  printOrders.filter((el) => el.wholesaleOrderStatus == 'NEW' || el.wholesaleOrderStatus == 'PLACED'),
                  'wholesaleOrderId',
                )
                setOrdersStatusToPicked(orderIds)
              }}
            >
              <Icon type="printer" theme="filled" />
              Print Pick Sheets
            </ThemeButton>
          </ThemeSpin>
        </Modal>
        <Modal
          width={1200}
          footer={null}
          visible={PrintBillOfLadingReviewShow}
          onCancel={() => this.closePrintModal(2)}
          wrapClassName="print-modal"
        >
          <ThemeSpin spinning={loadingPrintOrders}>
            <div id={printInvoiceName} style={{ minHeight: 500 }}>
              {/* 'PrintInvoiceModal' */}
              <div
                ref={(el) => {
                  this.setState({ PrintBillOfLadingRef: el })
                }}
              >
                {notCanceledPrintOrders.map((order, index) => {
                  if (filterStatus.indexOf(order.wholesaleOrderStatus) >= 0) {
                    const chargeItems = order.chargeItems.map((ci) => {
                      return {
                        ...ci,
                        variety: ci.itemName,
                        UOM: ci.uom,
                      }
                    })
                    const printOrderItem = this.getPrintOrderItems(order.printOrderItem)
                    return (
                      <div key={`print-picksheet-${index}`}>
                        <PrintInvoice
                          getWeightsByOrderItemIds={this.props.getWeightsByOrderItemIds}
                          catchWeightValues={[]}
                          orderItems={[...printOrderItem, ...chargeItems]}
                          currentOrder={order}
                          multiple={true}
                          companyName={this.props.companyName}
                          company={company}
                          logo={this.props.logo}
                          printSetting={printSetting}
                          type={'invoice'}
                          changePrintLogoStatus={this.changePrintLogoStatus}
                          sellerSetting={this.props.sellerSetting}
                          fulfillmentOptionType={order.fulfillmentType}
                          paymentTerms={[
                            ..._.get(companyProductTypes, 'paymentTerms', []),
                            ..._.get(companyProductTypes, 'paymentTermsFixedTypes', []),
                          ]}
                          companyProductTypes={companyProductTypes}
                          /*customInvoiceTemplate={[
                            ..._.get(companyProductTypes, 'customInvoiceTemplate', [])
                          ]}*/
                        />
                        <div style={{ pageBreakAfter: 'always' }} />
                      </div>
                    )
                  }
                  return null
                })}
              </div>
            </div>

            <ThemeButton
              shape="round"
              style={fullButton}
              className="print-bill-lading"
              onClick={() => printWindow(printInvoiceName, printContent.bind(this, 2), invoice_layout)}
            >
              {' '}
              {/* 'PrintInvoiceModal' */}
              <Icon type="printer" theme="filled" />
              Print Invoices
            </ThemeButton>
          </ThemeSpin>
        </Modal>
        {this.state.batchPickModalVisible && (
          <BatchPickModal
            status={filterStatus.join(',')}
            onCancel={() => this.setState({ batchPickModalVisible: false })}
            from={this.state.fromDate.format('MM/DD/YYYY')}
            to={this.state.toDate.format('MM/DD/YYYY')}
            sellerSetting={this.props.sellerSetting}
          />
        )}
        <OrderQBOSyncModal
          visible={this.state.showModal}
          onCancel={this.onCloseModal}
          onOk={this.onSync}
          orders={lastOrders}
          loading={this.props.loading}
          sellerSetting={this.props.sellerSetting}
          type={this.props.location.pathname.indexOf('/sales-orders') >= 0 ? 'CUSTOMER' : 'VENDOR'}
        />
        {this.props.location.pathname.indexOf('/sales-orders') >= 0 ?
          <SalesOrderUploadCsv
            visible={this.state.showUploadCSV}
            propData={this.props}
            onCancel={this.onCloseUploadCSV}
            uploadMode={this.state.uploadMode} />
          :
          <OrderUploadCsv
            visible={this.state.showUploadCSV}
            propData={this.props}
            onCancel={this.onCloseUploadCSV}
            uploadMode={this.state.uploadMode}
          />
        }
        {this.state.showAutoAssignContainer && (
          <AutoAssignContainerModal
            onCancel={() => this.setState({ showAutoAssignContainer: false })}
            simplifyCustomers={this.props.simplifyCustomers}
            sellerSetting={this.props.sellerSetting}
          />
        )}
        <OrderTable
          history={this.props.history}
          orders={lastOrders}
          tableMini={false}
          currentUser={this.props.currentUser}
          changePage={this.OnTableChange}
          currentPage={page + 1}
          pageSize={pageSize}
          total={orderTotal}
          loading={this.props.loading}
          curSortKey={this.state.orderByFilter}
          sellerSetting={this.props.sellerSetting}
          getOrderItemsByOrderIdForPOPage={this.props.getOrderItemsByOrderIdForPOPage}
          getOrderItemsByOrderIdsForPOPage={this.props.getOrderItemsByOrderIdsForPOPage}
        />
        {this.props.location.pathname === '/purchase-orders' && (
          <table style={{ display: 'none' }} id="export_lot_list" className="uk-report-table table table-striped">
            <thead>
              <tr>
                <th>Lot</th>
                <th>Item</th>
                <th>Brand</th>
                <th>Origin</th>
                <th>Notes</th>
                <th>Vendor</th>
                <th>Received Date</th>
                <th>Cost</th>
                <th>UOM</th>
                <th>Units On Hand</th>
              </tr>
            </thead>
            <tbody>
              {this.props.downloadLotItems?this.props.downloadLotItems.map((row: any, index: number) => {
                return (
                  <tr key={`row-${index}`}>
                    <td>{row['lotId']}</td>
                    <td>{row['variety']}</td>
                    <td>{row['modifiers']}</td>
                    <td>{row['extraOrigin']}</td>
                    <td>{row['note']}</td>
                    <td>{row['vendorName']}</td>
                    <td>{moment(row['deliveryDate']).format('MM/DD/YYYY')}</td>
                    <td>{row['cost']}</td>
                    <td>{row['pricingUOM']}</td>
                    <td>{row['onHandQty']}</td>
                  </tr>
                )
              }:<></>)}
            </tbody>
          </table>
        )}
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export const Orders = connect(OrdersModule)(mapStateToProps)(OrdersComponent)

const filtersToQuery: (filters: any) => string = (filters: any) => {
  const newFilters = { ...filters }
  let query = ''

  if (newFilters.status.length) {
    query += `&status=${newFilters.status.join()}`
  }

  if (newFilters.pas !== 'all') {
    query += `&pas=${newFilters.pas}`
  }

  if (newFilters.orderDate) {
    query += `&orderDate=${moment(newFilters.orderDate).format('MM/DD/YYYY')}`
  }

  if (newFilters.buyer.length) {
    query += `&buyerId=${newFilters.buyer.join()}`
  }

  if (newFilters.vendor.length) {
    query += `&clientId=${newFilters.vendor.join()}`
  }

  if (newFilters.productSku) {
    query += `&productSku=${newFilters.productSku}`
  }

  if (newFilters.productName) {
    query += `&productName=${newFilters.productName}`
  }

  if (newFilters.businessType.length) {
    query += `&businessType=${newFilters.businessType.join()}`
  }

  return query
}
