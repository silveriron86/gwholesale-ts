import {
  Address,
  MainAddress,
  MainContact,
  Order,
  OrderDetail,
  OrderItem,
  PriceSheetItem,
  SaleItem,
  Station,
  TempOrder,
  TokenPriceSheet,
  WholesaleRoute,
  WholesaleSalesOrderPalletLabel,
} from '~/schema'
import {
  DefineAction,
  Effect,
  EffectModule,
  Module,
  ModuleDispatchProps,
  Reducer,
  StateObservable,
} from 'redux-epics-decorator'
import { Observable, of, from, timer } from 'rxjs'

import {
  map,
  switchMap,
  takeUntil,
  catchError,
  mergeMap,
  endWith,
  retryWhen,
  tap,
  delayWhen,
  startWith,
  concatMap,
} from 'rxjs/operators'
import { push, goBack } from 'connected-react-router'
import { Action } from 'redux-actions'
import { format } from 'date-fns'
import { notification } from 'antd'

import {
  checkError,
  formatPriceSheetItem,
  judgeConstantRatio,
  ratioPriceToBasePrice,
  ratioQtyToInventoryQty,
  responseHandler,
} from '~/common/utils'
import { cloneDeep, get } from 'lodash'
import { CustomerService } from '../customers/customers.service'
import { DeliveryService } from '../delivery/delivery.service'
import { GlobalState } from '~/store/reducer'
import { InventoryService } from '../inventory/inventory.service'
import { ManufacturingService } from '../manufacturing/manufacturing.service'
import { ManufacturingStateProps } from '../manufacturing'
import { MessageType } from '~/components'
import { OrderService } from './order.service'
import { PriceSheetService } from '../pricesheet/pricesheet.service'
import { ProductService } from '../product/product.service'
import { SettingService } from '../setting/setting.service'
import { VendorService } from '../vendors/vendors.service'
import { SettingsProps } from '~/modules/settings'
import { PricingService } from '../pricing/pricing.service'
import _ from 'lodash'

export enum OrderLoading {
  OrderPlacing = 'order_placing',
}

export interface OrdersStateProps {
  orders: Order[]
  currentOrder: OrderDetail | null
  saleItems: SaleItem[]
  purchaseSimplifyItems: SaleItem[]
  itemList: SaleItem[]
  customers: any[] | null
  cashCustomer: any[]
  systemCustomers: any[] | null
  orderItems: OrderItem[]
  orderItemByProduct: any
  orderItem: OrderItem
  priceSheetItems: PriceSheetItem[]
  error: string
  tokenPriceSheet: TokenPriceSheet
  message: string
  description: string
  type: MessageType | null
  companyName: string
  settingCompanyName: string
  logo: string
  loadingIndex: string
  QBOerror: string
  adjustments: any[]
  documents: []
  documentsLoading: boolean
  vendors: any[]
  itemLots: any[]
  availableItemLots: any[]
  catchWeight: any[]
  addresses: MainAddress[]
  pallets: any[]
  routes: WholesaleRoute[]
  selectedRoute: WholesaleRoute | null
  relatedOrders: TempOrder[]
  loading: boolean
  allSales: OrderItem[]
  allPurchases: OrderItem[]
  salesPrice: OrderItem[]
  newOrderId: number
  shippingAddresses: any[]
  companyProductTypes: any
  currentCompanyUsers: any[]
  sellerSetting: any | null
  orderTotal: number
  resourceMatrix: any[]
  loadingAddress: boolean
  catchWeightValues: any
  oneOffItems: OrderItem[]
  relatedBills: OrderItem[]
  loadingCurrentOrder: boolean
  loadingSaleItems: boolean
  loadingAvailableItemLots: boolean
  finalizePurchaseOrder: any[]
  printOrders: any[]
  loadingPrintOrders: boolean
  printSetting: string
  customerContacts: MainContact[]
  purchaseOrderItemHistory: any[]
  purchaseOrderItemCost: any[]
  loadingOnEPick: boolean
  updateReceivedModalLoading: boolean
  printingQuick: boolean
  itemAvailableItemLots: any[]
  /**
   * for manufacturing
   */

  currentWorkOrder: Order
  auditLog: any
  currentWorkOrderId: number
  templates: Order[]
  stations: Station[]
  workers: any[]

  product: any
  documentsTotal: number
  documentSearchProps: any
  frequentItems: any[]

  /* sales cart */
  updating: boolean
  updatingField: boolean
  updatingUOMField: boolean
  profitability: any
  fulfillmentOptionType: number
  orderItemIdForCW: number // for catch weight
  catchWeights: any[]

  printCashSales: any[]
  cashSaleOrders: any[]
  cashSaleAllTotal: number
  cashSaleOrderTotal: number
  addItemDataRefreshedAt: Date
  lotDataRefreshedAt: Date
  simplifyItems: any
  simplifyItemsByOrderId: any
  weightSheet: any
  totalPalletsInfo: any[]
  simplifyCustomers: any
  simplifyVendors: any
  loadingSimplifyVendors: boolean
  isShippedSwitchLoading: boolean
  isShippedSwitchCheck: boolean
  addingCartItem: boolean
  batchCount: number
  batchMax: number
  cashReferences: any[]
  customOrderNumbers: any[]
  orderItemsImpacts: any[]
  salesOrderPalletLabels: WholesaleSalesOrderPalletLabel[]
  downloadLotItems: OrderItem[]
  getItemLotLoading: boolean
  allocationData: any[]
  allocationPalletData: any[]
  updateOrderQuantityLoading: any
  //
  currentProducts: any[]
  enterPurchaseProducts: any[]
  loadingPurchaseProducts: boolean
  duplicateOrderLoading: boolean
  cancelingOrder: boolean
  finalizing: boolean
  containerList: []
  currentPrintContainerId: string
  repacking: boolean
  repack: any
  isHealthBol: boolean
  gettingAllocationsToOrder: boolean
  allocationsToOrder: any[]
  gettingAllocationsFromOrder: boolean
  allocationsFromOrder: any[]
  enteringPO: boolean
  sendingPOEmail: boolean
  lastNsUpdate: string | null
  lastQboUpdate: string | null
  getOrderItemsLoading: boolean
  gettingOrder: boolean
  unassignOrderItemsCount: number
  updatingReceived: boolean
  syncingQboOrder: boolean
  syncedItemList: any[]
  syncedQboBills: any[]
  page: number
  pageSize: number
  total: number
  totalPicked: null | number,
  totalQuantity: null | number,
  pageTotalInfo: {
    totalPicked: null,
    totalQuantity: null,
    totalPrice: null,
  }
}

@Module('orders')
export class OrdersModule extends EffectModule<OrdersStateProps> {
  defaultState = {
    orders: [],
    templates: [],
    addresses: [],
    currentOrder: null,
    saleItems: [],
    purchaseSimplifyItems: [],
    customers: null,
    cashCustomer: [],
    systemCustomers: null,
    orderItems: [],
    orderItemByProduct: {},
    orderItem: null,
    priceSheetItems: [],
    error: '',
    tokenPriceSheet: {
      priceSheetItemList: [] as any,
    } as TokenPriceSheet,
    message: '',
    description: '',
    type: null,
    companyName: '',
    settingCompanyName: '',
    logo: 'default',
    loadingIndex: '',
    QBOerror: '',
    adjustments: [],
    documents: [],
    documentsLoading: false,
    vendors: [],
    itemLots: [],
    availableItemLots: [],
    catchWeight: [],
    pallets: [],
    routes: [],
    selectedRoute: null,
    relatedOrders: [],
    loading: false,
    allSales: [],
    salesPrice: [],
    allPurchases: [],
    newOrderId: -1,
    shippingAddresses: [],
    companyProductTypes: null,
    currentCompanyUsers: [],
    sellerSetting: null,
    loadingAddress: false,
    catchWeightValues: {},
    updateReceivedModalLoading: false,
    currentWorkOrder: null,
    auditLog: {
      in: [],
      out: [],
      wo: [],
    },
    currentWorkOrderId: 0,
    stations: [],
    workers: [],
    orderTotal: 0,
    resourceMatrix: [],
    product: null,
    documentsTotal: 0,
    documentSearchProps: {},
    frequentItems: [],

    loadingCurrentOrder: false,
    loadingSaleItems: true,
    loadingAvailableItemLots: false,
    oneOffItems: [],
    relatedBills: [],
    finalizePurchaseOrder: [],
    printOrders: [],
    loadingPrintOrders: false,
    printSetting: null,
    profitability: null,
    fulfillmentOptionType: 1,
    orderItemIdForCW: 0,
    catchWeights: [],
    customerContacts: [],
    purchaseOrderItemHistory: [],
    updatingField: false,
    updatingUOMField: false,
    printCashSales: [],
    cashSaleOrders: [],
    cashSaleAllTotal: 0,
    cashSaleOrderTotal: 0,
    addItemDataRefreshedAt: null,
    lotDataRefreshedAt: null,
    simplifyItems: [],
    simplifyItemsByOrderId: [],
    weightSheet: null,
    totalPalletsInfo: [],
    simplifyCustomers: [],
    simplifyVendors: [],
    loadingSimplifyVendors: false,
    isShippedSwitchLoading: false,
    isShippedSwitchCheck: false,
    addingCartItem: false,
    batchCount: 0,
    batchMax: 0,
    loadingOnEPick: false,
    cashReferences: [],
    customOrderNumbers: [],
    orderItemsImpacts: [],
    salesOrderPalletLabels: [],
    downloadLotItems: [],
    getItemLotLoading: false,
    allocationData: [],
    allocationPalletData: null,
    updateOrderQuantityLoading: { loading: false, fetching: false },
    currentProducts: [],
    enterPurchaseProducts: [],
    loadingPurchaseProducts: false,
    printingQuick: false,
    itemAvailableItemLots: [],
    duplicateOrderLoading: false,
    cancelingOrder: false,
    finalizing: false,
    containerList: [],
    currentPrintContainerId: '',
    repacking: false,
    repack: null,
    isHealthBol: false,
    gettingAllocationsToOrder: false,
    allocationsToOrder: [],
    gettingAllocationsFromOrder: false,
    allocationsFromOrder: [],
    enteringPO: false,
    sendingPOEmail: false,
    lastNsUpdate: null,
    lastQboUpdate: null,
    getOrderItemsLoading: false,
    gettingOrder: false,
    unassignOrderItemsCount: 0,
    updatingReceived: false,
    syncingQboOrder: false,
    syncedItemList: [],
    syncedQboBills: [],
    page: 0,
    pageSize: 20,
    total: 0,
    pageTotalInfo: {
      totalPicked: null,
      totalQuantity: null,
      totalPrice: null,
    }
  }

  @DefineAction() dispose$!: Observable<void>
  @DefineAction() disposeEditOrder$!: Observable<void>

  constructor(
    private readonly customer: CustomerService,
    private readonly inventory: InventoryService,
    private readonly setting: SettingService,
    private readonly order: OrderService,
    private readonly delivery: DeliveryService,
    private readonly priceSheet: PriceSheetService,
    private readonly manufacturing: ManufacturingService,
    private readonly product: ProductService,
    private readonly vendor: VendorService,
    private readonly pricing: PricingService,
  ) {
    super()
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect()
  goto(action$: Observable<string>) {
    return action$.pipe(map((path) => push(path)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: '',
        type: null,
        loading: true,
        loadingCurrentOrder: true,
        catchWeight: [],
        batchCount: 0,
        batchMax: 0,
      }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        loadingOnEPick: true,
      }
    },
  })
  resetOnEPickSheet(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        batchCount: 0,
        batchMax: payload,
      }
    },
  })
  setBatchMax(action$: Observable<number>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        fulfillmentOptionType: payload,
      }
    },
  })
  onChangeFulfillmentOptionType(action$: Observable<number>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        orderItems: [],
        oneOffItems: [],
        currentOrder: null,
        lastNsUpdate: null,
        lastQboUpdate: null,
        relatedBills: [],
        orderItemByProduct: {},
        adjustments: [],
      }
    },
  })
  resetOrder(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        printOrders: [],
        printCashSales: [],
        loadingPrintOrders: true,
      }
    },
  })
  startPrintOrders(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        loadingSaleItems: !state.loadingSaleItems,
      }
    },
  })
  setOrResetLoadingForGetItems(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        loadingSaleItems: true,
      }
    },
  })
  setLoadingForGetItems(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        addingCartItem: true,
      }
    },
  })
  setAddingCartItem(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return { ...state, documentSearchProps: payload }
    },
  })
  setDocumentSearchProps(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        newOrderId: -1,
      }
    },
  })
  resetNewOrderId(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        product: null,
      }
    },
  })
  resetProduct(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, product: payload }
    },
  })
  getProductItem(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getItems(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        orders: action.payload,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
      }
    },
  })
  getOrder(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((deliveryDate) =>
        this.order
          .getOrders(deliveryDate, state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET')
          .pipe(
            map((data) => data.map((order) => this.formatOrder(order))),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        loadingAddress: true,
      }
    },
  })
  setAddressLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        addresses: action.payload,
        loadingAddress: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
        loadingAddress: false,
      }
    },
  })
  getAddresses(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() => this.customer.getAddresses(state$.value.orders.currentOrder.wholesaleClient.clientId.toString())),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        orders: action.payload,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
      }
    },
  })
  getCustomersOrders(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((_deliveryDate) =>
        this.order.getCustomersOrders(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0').pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          switchMap((data: any) => of(this.formatOrder(data))),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        orders: action.payload.dataList,
        orderTotal: action.payload.total,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // error: action.payload,
        loading: false,
        hasError: true,
      }
    },
  })
  getWarehouseSalesOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getWarehouseSalesOrdersByDate(data).pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          map((data) => {
            const total = data.total
            const orderList = data.dataList
            const newList = orderList.map((order) => {
              return this.formatSimplifyOrder(order)
            })
            return { total, dataList: newList }
          }),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        orders: action.payload.dataList,
        orderTotal: action.payload.total,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // error: action.payload,
        loading: false,
        hasError: true,
      }
    },
  })
  getOrdersForCustomerUserId(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getOrdersForCustomerUserId(data).pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          map((data) => {
            const total = data.total
            const orderList = data.dataList
            const newList = orderList.map((order) => {
              return this.formatCustomerOrder(order)
            })
            return { total, dataList: newList }
          }),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        downloadLotItems: action.payload,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        hasError: true,
      }
    },
  })
  getPurchaseLotItemsDownload(action$: Observable<any>) {
    return action$.pipe(
      switchMap((params) =>
        this.order.getPurchaseLotItemsDownload(params).pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        orders: action.payload.dataList,
        orderTotal: action.payload.total,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // error: action.payload,
        loading: false,
        hasError: true,
      }
    },
  })
  getWarehousePurchaseOrders(action$: Observable<{ fromDate: string; toDate: string }>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getWarehousePurchaseOrdersByDate(data).pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          map((data) => {
            const total = data.total
            const orderList = data.dataList
            const newList = orderList.map((order) => {
              return this.formatSimplifyOrder(order)
            })
            return { total, dataList: newList }
          }),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        currentOrder: {
          ...action.payload.currentOrder,
          priceSheetItemList: action.payload.priceSheetItemList,
        },
        sellerSetting: action.payload.sellerSetting,
        loading: false,
      }
    },
  })
  getOrderTokenDetail(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) => this.order.getOrderTokenDetail(orderId)),
      switchMap((data: any) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        currentOrder: action.payload,
        lastNsUpdate: action.payload.lastNsUpdate,
        lastQboUpdate: action.payload.lastQboUpdate,
        fulfillmentOptionType: action.payload && action.payload.fulfillmentType ? action.payload.fulfillmentType : 1,
        loading: false,
        loadingCurrentOrder: false,
        updating: false,
        isShippedSwitchCheck: action.payload && action.payload.wholesaleOrderStatus == 'SHIPPED' ? true : false,
      }
    },
  })
  getOrderDetail(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) => this.order.getOrderDetail(orderId)),
      switchMap((data: any) =>
        of(responseHandler(data, false).body.data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getVendorProducts)(data.body.data.wholesaleClient.clientId)),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    before: (state: any) => {
      console.log('*before')
      return {
        ...state,
        gettingOrder: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      console.log('* after ', action.payload)
      return {
        ...state,
        gettingOrder: false,
        currentOrder: action.payload,
      }
    },
  })
  getOrderBalance(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.order.getOrderDetail(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        currentOrder: action.payload,
        fulfillmentOptionType: action.payload && action.payload.fulfillmentType ? action.payload.fulfillmentType : 1,
        loading: false,
        loadingCurrentOrder: false,
        updating: false,
        isShippedSwitchCheck: action.payload && action.payload.wholesaleOrderStatus == 'SHIPPED' ? true : false,
      }
    },
  })
  getOrderDetailAfterUpdateClient(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) => this.order.getOrderDetail(orderId)),
      switchMap((data: any) =>
        of(responseHandler(data, false).body.data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAddresses)()),
          // endWith() todoo
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loading: true,
        lastQboUpdate: null,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        lastQboUpdate: null,
      }
    },
  })
  syncQBOOrder(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.order.syncQBOOrder(orderId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          endWith(this.createActionFrom(this.getOrderDetail)(orderId)),
          endWith(this.createActionFrom(this.getOneOffItems)(orderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      window.location.reload()
      return {
        ...state,
        loading: false,
      }
    },
  })
  syncQBOOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orderIds: any) => from(orderIds)),
      mergeMap((orderId: any) => {
        return this.order.syncQBOOrder(orderId)
      }, 8),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loading: true,
        lastNsUpdate: null,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        batchCount: 0,
        batchMax: 0,
        loading: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        lastNsUpdate: null,
      }
    },
  })
  syncNSOrder(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.order.syncNSOrder(orderId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          endWith(this.createActionFrom(this.getOrderDetail)(orderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      let currentCount = state.batchCount + 1
      if (currentCount >= state.batchMax) {
        return {
          ...state,
          batchCount: 0,
          batchMax: 0,
          loading: false,
        }
      }
      return {
        ...state,
        batchCount: currentCount,
      }
    },
  })
  syncNSOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orderIds: any) => this.order.forceTokenRefresh().pipe(switchMap(() => from(orderIds)))),
      mergeMap((orderId: any) => {
        return this.order.syncNSOrder(orderId)
      }, 2),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        currentOrder: action.payload,
        loading: false,
      }
    },
  })
  setShippingAddress(action$: Observable<{ orderId: string; wholesaleAddressId: number }>) {
    return action$.pipe(
      switchMap((data) => this.order.setShippingAddress(data.orderId, data.wholesaleAddressId)),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Reducer()
  resetItemLotLoading(state: OrdersStateProps) {
    return {
      ...state,
      getItemLotLoading: true,
    }
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        itemLots: [],
        getItemLotLoading: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        itemLots: action.payload,
        lotDataRefreshedAt: new Date(),
        getItemLotLoading: false,
      }
    },
  })
  getItemLot(action$: Observable<number>) {
    return action$.pipe(
      switchMap((orderId) => this.order.getItemLot(orderId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      startWith(this.createAction('before')(true)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        availableItemLots: action.payload || [],
        loadingAvailableItemLots: false,
        addItemDataRefreshedAt: new Date(),
      }
    },
  })
  getAvailableItemLot(action$: Observable<string>) {
    return action$.pipe(
      switchMap((salesType) => this.order.getAvailableItemLot(salesType)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }
  /*
    @Effect({
      done: (state: OrdersStateProps, action: Action<any>) => {
        return {
          ...state,
          settingCompanyName: action.payload.userSetting.company.companyName,
          companyName: action.payload.userSetting.companyName,
          themeKey: action.payload.userSetting.colorTheme ? action.payload.userSetting.colorTheme : 'green',
          logo: action.payload.userSetting.imagePath ? action.payload.userSetting.imagePath : 'default',
          sellerSetting: action.payload.userSetting,
        }
      },
    })
    getSellerTheme(action$: Observable<void>, state$: StateObservable<GlobalState>) {
      return action$.pipe(
        switchMap(() =>
          this.setting
            .getUserSetting(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
            .pipe(map(this.createAction('done'))),
        ),
        catchError((error) => of(checkError(error))),
      )
    }
  */
  @Effect({
    done: (state: OrdersStateProps, action: Action<PriceSheetItem[]>) => {
      return {
        ...state,
        priceSheetItems: formatPriceSheetItem(action.payload),
        loadingIndex: '',
      }
    },
  })
  getPriceSheetItems(action$: Observable<string>) {
    return action$.pipe(
      switchMap((priceSheetId) =>
        this.order.getPriceSheetItems(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderItem[]>) => {
      console.log('is calling here?')
      return {
        ...state,
        orderItems: action.payload ? action.payload.map((orderItem) => ({ ...orderItem, rowLoading: false })) : [],
      }
    },
  })
  getOrderItems(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.order.getOrderItems(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        getOrderItemsLoading: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<OrderItem[]>) => {
      const orderItems = action.payload.map((v) => ({
        ...v,
        displayOrderProduct: _.floor(v.displayOrder),
        displayOrderItem: String(v.displayOrder).substring(String(v.displayOrder).indexOf('.') + 1),
      }))

      const dataGroupByItemId = _.groupBy(orderItems, 'displayOrderProduct')
      const productIds = Object.keys(dataGroupByItemId)
      const orderItemByProduct = {}

      productIds.forEach((productId) => {
        orderItemByProduct[productId] = {}
        let quantity = 0
        let picked = 0
        orderItemByProduct[productId].locationNames = []
        dataGroupByItemId[productId].forEach((orderItem) => {
          quantity = quantity + Number(orderItem.quantity)
          picked = picked + Number(orderItem.picked)
          orderItemByProduct[productId].locationNames = orderItemByProduct[productId].locationNames.concat(
            orderItem.locationNames,
          )
        })
        const catchWeightQty = dataGroupByItemId[productId].map((v) => v.catchWeightQty)
        orderItemByProduct[productId].items = dataGroupByItemId[productId].map((v) => ({
          ...v,
          grossVolume: v.grossVolume || null,
          grossWeight: v.grossWeight || null,
        }))

        orderItemByProduct[productId].quantity = quantity
        orderItemByProduct[productId].picked = picked
        orderItemByProduct[productId].lotIds = _.compact(
          _.orderBy(dataGroupByItemId[productId], 'wholesaleOrderItemId').map((v) => v.lotId),
        )
        orderItemByProduct[productId].defaultSellingPricingUOM = dataGroupByItemId[productId][0].defaultSellingPricingUOM
        orderItemByProduct[productId].displayOrderProduct = dataGroupByItemId[productId][0].displayOrderProduct
        orderItemByProduct[productId].variety = dataGroupByItemId[productId][0].variety
        orderItemByProduct[productId].SKU = dataGroupByItemId[productId][0].SKU
        orderItemByProduct[productId].itemId = dataGroupByItemId[productId][0].itemId
        orderItemByProduct[productId].price = dataGroupByItemId[productId][0].price
        orderItemByProduct[productId].lotAssignmentMethod = dataGroupByItemId[productId][0].lotAssignmentMethod
        orderItemByProduct[productId].enableLotOverflow = dataGroupByItemId[productId][0].enableLotOverflow
        orderItemByProduct[productId].wholesaleProductUomList = dataGroupByItemId[productId][0].wholesaleProductUomList
        orderItemByProduct[productId].UOM = dataGroupByItemId[productId][0].UOM
        orderItemByProduct[productId].pricingUOM = dataGroupByItemId[productId][0].pricingUOM
        orderItemByProduct[productId].inventoryUOM = dataGroupByItemId[productId][0].inventoryUOM
        orderItemByProduct[productId].pricingLogic = dataGroupByItemId[productId][0].pricingLogic
        orderItemByProduct[productId].overrideUOM = dataGroupByItemId[productId][0].overrideUOM
        orderItemByProduct[productId].useForSelling = dataGroupByItemId[productId][0].useForSelling
        orderItemByProduct[productId].useForPurchasing = dataGroupByItemId[productId][0].useForPurchasing
        orderItemByProduct[productId].manuallyAssignState = dataGroupByItemId[productId][0].manuallyAssignState

        orderItemByProduct[productId].pricingGroup = dataGroupByItemId[productId][0].pricingGroup
        orderItemByProduct[productId].pas = dataGroupByItemId[productId][0].pas
        orderItemByProduct[productId].modifiers = dataGroupByItemId[productId][0].modifiers
        orderItemByProduct[productId].extraOrigin = dataGroupByItemId[productId][0].extraOrigin
        orderItemByProduct[productId].labelSerial = dataGroupByItemId[productId][0].labelSerial
        orderItemByProduct[productId].status = dataGroupByItemId[productId][0].status
        orderItemByProduct[productId].catchWeightQty = catchWeightQty.reduce((n, p) => n + p, 0)
        orderItemByProduct[productId].workOrderStatus = dataGroupByItemId[productId][0].workOrderStatus
        orderItemByProduct[productId].editedItemName = dataGroupByItemId[productId][0].editedItemName
        orderItemByProduct[productId].tax = dataGroupByItemId[productId][0].tax
        orderItemByProduct[productId].taxRate = dataGroupByItemId[productId][0].taxRate
        orderItemByProduct[productId].taxEnabled = dataGroupByItemId[productId][0].taxEnabled

        orderItemByProduct[productId].grossWeight = _.reduce(
          dataGroupByItemId[productId],
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
          0,
        )
        orderItemByProduct[productId].UPC = dataGroupByItemId[productId][0].upc
        orderItemByProduct[productId].grossWeightUnit = dataGroupByItemId[productId][0].grossWeightUnit
        orderItemByProduct[productId].grossVolume = _.reduce(
          dataGroupByItemId[productId],
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
          0,
        )
        orderItemByProduct[productId].grossVolumeUnit = dataGroupByItemId[productId][0].grossVolumeUnit

        orderItemByProduct[productId].cost = dataGroupByItemId[productId][0].cost
        orderItemByProduct[productId].lotCost = dataGroupByItemId[productId][0].lotCost
        orderItemByProduct[productId].perAllocateCost = dataGroupByItemId[productId][0].perAllocateCost
        orderItemByProduct[productId].plu = dataGroupByItemId[productId][0].plu
        orderItemByProduct[productId].packing = dataGroupByItemId[productId][0].packing
        orderItemByProduct[productId].size = dataGroupByItemId[productId][0].size
        orderItemByProduct[productId].note = dataGroupByItemId[productId][0].note
        orderItemByProduct[productId].customerProductCode = dataGroupByItemId[productId][0].customerProductCode
        orderItemByProduct[productId].commodityClass = dataGroupByItemId[productId][0].commodityClass
        orderItemByProduct[productId].wholesaleOrderItemId = _.orderBy(
          dataGroupByItemId[productId],
          'wholesaleOrderItemId',
        )[0].wholesaleOrderItemId
      })

      return {
        ...state,
        orderItems,
        orderItemByProduct,
        updating: false,
        updatingField: false,
        getOrderItemsLoading: false,
        updateOrderQuantityLoading: { loading: false, fetching: false },
        total: productIds.length,
        pageTotalInfo: {
          totalPicked: null,
          totalQuantity: null,
          totalPrice: null,
        }
      }
    },
  })
  getOrderItemsById(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.order.getOrderItemsById(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        getOrderItemsLoading: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<OrderItem[]>) => {
      const orderItems = action.payload.data.map((v) => ({
        ...v,
        displayOrderProduct: _.floor(v.displayOrder),
        displayOrderItem: String(v.displayOrder).substring(String(v.displayOrder).indexOf('.') + 1),
      }))

      const dataGroupByItemId = _.groupBy(orderItems, 'displayOrderProduct')
      const productIds = Object.keys(dataGroupByItemId)
      const orderItemByProduct = {}

      productIds.forEach((productId) => {
        orderItemByProduct[productId] = {}
        let quantity = 0
        let picked = 0
        orderItemByProduct[productId].locationNames = []
        dataGroupByItemId[productId].forEach((orderItem) => {
          quantity = quantity + Number(orderItem.quantity)
          picked = picked + Number(orderItem.picked)
          orderItemByProduct[productId].locationNames = orderItemByProduct[productId].locationNames.concat(
            orderItem.locationNames,
          )
        })
        const catchWeightQty = dataGroupByItemId[productId].map((v) => v.catchWeightQty)
        orderItemByProduct[productId].items = dataGroupByItemId[productId].map((v) => ({
          ...v,
          grossVolume: v.grossVolume || null,
          grossWeight: v.grossWeight || null,
        }))

        orderItemByProduct[productId].quantity = quantity
        orderItemByProduct[productId].picked = picked
        orderItemByProduct[productId].lotIds = _.compact(
          _.orderBy(dataGroupByItemId[productId], 'wholesaleOrderItemId').map((v) => v.lotId),
        )
        orderItemByProduct[productId].defaultSellingPricingUOM = dataGroupByItemId[productId][0].defaultSellingPricingUOM
        orderItemByProduct[productId].displayOrderProduct = dataGroupByItemId[productId][0].displayOrderProduct
        orderItemByProduct[productId].variety = dataGroupByItemId[productId][0].variety
        orderItemByProduct[productId].SKU = dataGroupByItemId[productId][0].SKU
        orderItemByProduct[productId].itemId = dataGroupByItemId[productId][0].itemId
        orderItemByProduct[productId].price = dataGroupByItemId[productId][0].price
        orderItemByProduct[productId].lotAssignmentMethod = dataGroupByItemId[productId][0].lotAssignmentMethod
        orderItemByProduct[productId].enableLotOverflow = dataGroupByItemId[productId][0].enableLotOverflow
        orderItemByProduct[productId].wholesaleProductUomList = dataGroupByItemId[productId][0].wholesaleProductUomList
        orderItemByProduct[productId].UOM = dataGroupByItemId[productId][0].UOM
        orderItemByProduct[productId].pricingUOM = dataGroupByItemId[productId][0].pricingUOM
        orderItemByProduct[productId].inventoryUOM = dataGroupByItemId[productId][0].inventoryUOM
        orderItemByProduct[productId].pricingLogic = dataGroupByItemId[productId][0].pricingLogic
        orderItemByProduct[productId].overrideUOM = dataGroupByItemId[productId][0].overrideUOM
        orderItemByProduct[productId].useForSelling = dataGroupByItemId[productId][0].useForSelling
        orderItemByProduct[productId].useForPurchasing = dataGroupByItemId[productId][0].useForPurchasing
        orderItemByProduct[productId].manuallyAssignState = dataGroupByItemId[productId][0].manuallyAssignState

        orderItemByProduct[productId].pricingGroup = dataGroupByItemId[productId][0].pricingGroup
        orderItemByProduct[productId].pas = dataGroupByItemId[productId][0].pas
        orderItemByProduct[productId].modifiers = dataGroupByItemId[productId][0].modifiers
        orderItemByProduct[productId].extraOrigin = dataGroupByItemId[productId][0].extraOrigin
        orderItemByProduct[productId].labelSerial = dataGroupByItemId[productId][0].labelSerial
        orderItemByProduct[productId].status = dataGroupByItemId[productId][0].status
        orderItemByProduct[productId].catchWeightQty = catchWeightQty.reduce((n, p) => n + p, 0)
        orderItemByProduct[productId].workOrderStatus = dataGroupByItemId[productId][0].workOrderStatus
        orderItemByProduct[productId].editedItemName = dataGroupByItemId[productId][0].editedItemName
        orderItemByProduct[productId].tax = dataGroupByItemId[productId][0].tax
        orderItemByProduct[productId].taxRate = dataGroupByItemId[productId][0].taxRate
        orderItemByProduct[productId].taxEnabled = dataGroupByItemId[productId][0].taxEnabled

        orderItemByProduct[productId].grossWeight = _.reduce(
          dataGroupByItemId[productId],
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight || 0),
          0,
        )
        orderItemByProduct[productId].grossWeightUnit = dataGroupByItemId[productId][0].grossWeightUnit
        orderItemByProduct[productId].grossVolume = _.reduce(
          dataGroupByItemId[productId],
          (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume || 0),
          0,
        )
        orderItemByProduct[productId].grossVolumeUnit = dataGroupByItemId[productId][0].grossVolumeUnit

        orderItemByProduct[productId].cost = dataGroupByItemId[productId][0].cost
        orderItemByProduct[productId].lotCost = dataGroupByItemId[productId][0].lotCost
        orderItemByProduct[productId].perAllocateCost = dataGroupByItemId[productId][0].perAllocateCost
        orderItemByProduct[productId].plu = dataGroupByItemId[productId][0].plu
        orderItemByProduct[productId].packing = dataGroupByItemId[productId][0].packing
        orderItemByProduct[productId].size = dataGroupByItemId[productId][0].size
        orderItemByProduct[productId].note = dataGroupByItemId[productId][0].note
        orderItemByProduct[productId].customerProductCode = dataGroupByItemId[productId][0].customerProductCode
        orderItemByProduct[productId].commodityClass = dataGroupByItemId[productId][0].commodityClass
        orderItemByProduct[productId].wholesaleOrderItemId = _.orderBy(
          dataGroupByItemId[productId],
          'wholesaleOrderItemId',
        )[0].wholesaleOrderItemId
      })

      return {
        ...state,
        orderItems,
        orderItemByProduct,
        updating: false,
        updatingField: false,
        getOrderItemsLoading: false,
        updateOrderQuantityLoading: { loading: false, fetching: false },
        total: action.payload.total,
        pageTotalInfo: {
          totalPicked: action.payload.totalPicked,
          totalQuantity: action.payload.totalQuantity,
          totalPrice: action.payload.totalPrice
        }
      }
    },
  })
  getSalesOrderItems(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      concatMap((orderId) =>
        this.order.getOrderItemsPagination(orderId, state$.value.orders.page, state$.value.orders.pageSize).pipe(
          concatMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        page: action.payload
      }
    },
  })
  setSalesOrderPage(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        of(data).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }


  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderItem[]>) => {
      return {
        ...state,
        orderSortItems: action.payload,
        updating: false,
        updatingField: false,
        printingQuick: false,
        updateOrderQuantityLoading: { loading: false, fetching: false },
      }
    },
  })
  getOrderItemsByIdAndSort(action$: Observable<{ id: string; sort: string }>) {
    return action$.pipe(
      switchMap((orderObj: { id: string; sort: string }) =>
        this.order.getOrderItemsByIdAndSort(orderObj).pipe(
          switchMap((data) => {
            console.log(data)
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderItem[]>) => {
      return {
        ...state,
        tokenPriceSheet: action.payload,
      }
    },
  })
  getTokenPriceSheet(action$: Observable<string>) {
    const processData = (data: TokenPriceSheet) => {
      data.priceSheetItemList.forEach((d, _i) => {
        d.itemId = d.wholesaleItem!.wholesaleItemId
        d.variety = d.wholesaleItem!.variety
        d.category = d.wholesaleItem!.wholesaleCategory.name
        d.wholesaleCategory = d.wholesaleItem!.wholesaleCategory
        d.size = d.wholesaleItem!.size
        d.weight = d.wholesaleItem!.weight
        d.grade = d.wholesaleItem!.grade
        d.origin = d.wholesaleItem!.origin
        d.provider = d.wholesaleItem!.provider
        d.packing = d.wholesaleItem!.packing
        d.suppliers = d.wholesaleItem!.suppliers
      })
      return data
    }

    return action$.pipe(
      switchMap((tokenId) =>
        this.order.getPriceSheetFromToken(tokenId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(processData),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // message: 'Created Order Successfully',
        // type: MessageType.SUCCESS,
        loadingIndex: '',
        // QBOerror: action.payload.QBO,
        newOrderId: action.payload ? action.payload.wholesaleOrderId : -1,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // type: MessageType.ERROR,
        // error: action.payload,
        // QBOerror: action.payload.QBO,
        loadingIndex: '',
      }
    },
  })
  createOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) => of(responseHandler(data, true).body.data).pipe(map(this.createAction('done')))),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loadingIndex: '',
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loadingIndex: '',
      }
    },
  })
  createOrderByToken(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createOrderByToken(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map((data) => push(`/order/${data.linkToken}/success`)),
              endWith(this.createActionFrom(this.loadIndex)('')),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loadingIndex: '',
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loadingIndex: '',
      }
    },
  })
  createTokenOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map((data) => push(`/order/${data.linkToken}/success`)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      // const type = action.payload.order.wholesaleClient.type === 'VENDOR' ? 'Purchase' : 'Sales';
      return {
        ...state,
        // message: `Created ${type} Order Successfully`,
        // type: MessageType.SUCCESS,
        loadingIndex: '',
        // QBOerror: action.payload.QBO,
        newOrderId: action.payload.wholesaleOrderId,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // type: MessageType.ERROR,
        // error: action.payload,
        // QBOerror: action.payload.QBO,
        loadingIndex: '',
        newOrderId: -1,
      }
    },
  })
  createEmptyOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        updating: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        updating: false,
        // message: 'Order Updated Successfully',
        // type: MessageType.SUCCESS,
        isShippedSwitchLoading: false,
      }
    },
  })
  updateOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrder(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              // catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
          retryWhen((errors) =>
            errors.pipe(
              //log error message
              tap((val) => console.log(`${val}`)),
              //restart in 0.5 seconds
              delayWhen((val) => {
                if (val == 'CONFLICT') {
                  return timer(500)
                } else {
                  //Hack: Avoid other unknown exceptions
                  return timer(600000)
                }
              }),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        isShippedSwitchLoading: false,
      }
    },
  })
  shipLockOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.shipLockOrder(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getSalesOrderItems)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
            ),
          ),
          catchError((error) => of(checkError(error))),
          retryWhen((errors) =>
            errors.pipe(
              //log error message
              tap((val) => console.log(`${val}`)),
              //restart in 0.5 seconds
              delayWhen((val) => {
                if (val == 'CONFLICT') {
                  return timer(500)
                } else {
                  //Hack: Avoid other unknown exceptions
                  return timer(600000)
                }
              }),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  removeOrderItemForOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.removeOrderItemForOrder(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  addOrderItemForOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.addOrderItemForOrder(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                // this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
                this.createActionFrom(this.getSalesOrderItems)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  afterCreateOrderUpdateOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrder(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  createOffItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createOffItem(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getOneOffItems)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  updateOffItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOffItem(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getOneOffItems)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  removeOffItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.removeOffItem(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getOneOffItems)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      const oneOffItems = action.payload.filter((orderItem: any) => {
        return orderItem.wholesaleClientId == null
      })
      const relatedBills = action.payload.filter((orderItem: any) => {
        return orderItem.wholesaleClientId != null
      })
      return {
        ...state,
        oneOffItems,
        relatedBills,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  getOneOffItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.order.getOrderOneOffItemsById(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        cancelingOrder: true,
      }
    },
  })
  startCanceling(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Order Info Updated Successfully',
        // type: MessageType.SUCCESS,
        loading: false,
        cancelingOrder: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        // error: action.payload,
        // hasError: true,
        loading: false,
        loadingCurrentOrder: false,
        cancelingOrder: false,
      }
    },
  })
  updateOrderInfo(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((param: any) => {
        const showNotification = param.showNotif
        delete param.showNotif
        return this.order.updateOrderInfo(param).pipe(
          switchMap((data) =>
            of(responseHandler(data, showNotification === false ? false : true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId),
                // this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Order Info Updated Successfully',
        // type: MessageType.SUCCESS,
        loading: false,
        loadingAddress: true,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        // error: action.payload,
        // hasError: true,
        loading: false,
        loadingCurrentOrder: false,
      }
    },
  })
  updateOrderClient(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrderInfo(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderDetailAfterUpdateClient)(
                  state$.value.orders.currentOrder!.wholesaleOrderId,
                ),
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      let currentOrder = null
      if (state.currentOrder) {
        currentOrder = {
          ...state.currentOrder,
          isLocked: !state.currentOrder.isLocked,
        }
      }

      return {
        ...state,
        currentOrder: currentOrder,
        loading: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        loading: false,
      }
    },
  })
  updateOrderLocked(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrderInfo(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        loading: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        loading: false,
      }
    },
  })
  updateOrderItemsDeliveryDate(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrderItemsDeliveryDate(data.orderId, data.deliveryTime).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        //message: 'Order Item Updated Successfully',
        //type: MessageType.SUCCESS,
        //QBOerror: payload.qboResponse,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        //error: action.payload,
        //hasError: true,
      }
    },
  })
  updateOrderItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data: any) =>
        this.order.updateOrderItem(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
                this.createActionFrom(this.updateOrderStatus)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  //TODO: hack for update lot.  this action don't need update order status, so add new function
  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  updateOrderItemForLot(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data: any) =>
        this.order.updateOrderItem(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loading: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loading: false,
        // orderItems: state.orderItems.map((item) => {
        //   if (item.wholesaleOrderItemId === payload.wholesaleOrderItemId) {
        //     return { ...item, ...payload, status: payload.wholesaleOrderItemStatus }
        //   }
        //   return item
        // }),
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  updateOrderItemWithoutRefresh(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data: any) =>
        this.order.updateOrderItemNoCancel(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              startWith(this.createAction('before')(true)),
              // endWith(
              //   this.createActionFrom(this.updateOrderStatus)(state$.value.orders.currentOrder!.wholesaleOrderId),
              // ),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        updating: true,
      }
    },
  })
  startUpdating(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        updating: false,
      }
    },
  })
  endUpdating(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        isShippedSwitchLoading: true,
      }
    },
  })
  setIsShippedSwitchLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        updatingField: true,
        updatingUOMField: true,
      }
    },
  })
  startUpdatingField(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        printingQuick: true,
      }
    },
  })
  startQuickPrint(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      console.log('picked qty')
      console.log(state.orderItems)
      return {
        ...state,
        orderItems: state.orderItems.map((item) => {
          if (item.wholesaleOrderItemId == payload.wholesaleOrderItemId) {
            return { ...item, rowLoading: false }
          }
          return item
        }),
        // updating: false,
        // updatingField: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  // 只在QuantityModal用到
  updateSaleOrderItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data: any) =>
        this.order.updateOrderItemNoCancel(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        updatingUOMField: false,
        updateOrderQuantityLoading: { loading: false, fetching: false },
        orderItems: state.orderItems.map((item) => {
          if (item.wholesaleOrderItemId === payload.wholesaleOrderItemId) {
            return {
              ...item,
              cost: payload.cost,
              price: payload.price,
              UOM: payload.uom,
              pricingUOM: payload.pricingUOM,
              catchWeightQty: payload.catchWeightQty,
              lotAvailableQty: payload.lotAvailableQty,
            }
          }
          return item
        }),
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  updateSOItemForUOM(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrderItem(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(),
              // this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              // this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId),
              // this.createActionFrom(this.getQuantityCatchWeight)(data.body.data.wholesaleOrderItemId),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        orderItems: state.orderItems.map((item) => {
          if (item.wholesaleOrderItemId === payload.wholesaleOrderItemId) {
            return {
              ...item,
              lotAvailableQty: payload.lotAvailableQty,
              qtyUpdatedFlag: false,
            }
          }
          return item
        }),
        updating: false,
        updatingField: false,
        updateOrderQuantityLoading: { loading: false, fetching: false },
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  updateSaleOrderItemWithoutRefresh(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data: any) =>
        this.order.updateOrderItemNoCancel(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(),
              // this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              // this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId),

              // this.createActionFrom(this.getSaleItems)('SELL'),
              // this.createActionFrom(this.getAvailableItemLot)('SELL'),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        updatingReceived: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        updatingReceived: false,
      }
    },
  })
  updateOrderStatus(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrderStatus(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return { ...state, message: 'Update Items Stock Successful', type: MessageType.SUCCESS }
    },
  })
  requestPurchaseOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => this.order.requestPurchaseOrders(data).pipe(map(this.createAction('done')))),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return { ...state, customers: payload }
    },
  })
  getCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        // (state$.value.orders.customers
        //   ? of(state$.value.orders.customers) :
        this.customer
          .getAllCustomers(state$.value.currentUser ? state$.value.currentUser.userId! : '1')
          .pipe(switchMap((data: any) => of(responseHandler(data, false).body.data)))
          .pipe(
            // takeUntil(this.disposeEditOrder$),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return { ...state, simplifyCustomers: payload }
    },
  })
  getSimplifyCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getSimplifyCustomers().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
    // return action$.pipe(
    //   switchMap(() =>
    //     this.customer.getSimplifyCustomers().pipe(
    //       switchMap((data: any) => of(responseHandler(data, false).body.data))),
    //       // .pipe(// takeUntil(this.disposeEditOrder$),
    //         map(this.createAction('done')),
    //         catchError((error) => of(checkError(error))),
    //       ),
    //     )
    //   ),
    // )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return { ...state, systemCustomers: payload }
    },
  })
  getSystemCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        (state$.value.orders.systemCustomers
          ? of(state$.value.orders.systemCustomers)
          : this.customer.getSystemCustomers()
        ).pipe(
          // takeUntil(this.disposeEditOrder$),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any[]>) => {
      return {
        ...state,
        vendors: action.payload,
      }
    },
  })
  getVendors(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.order.getAllVendors().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loadingSimplifyVendors: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any[]>) => {
      return {
        ...state,
        simplifyVendors: action.payload,
        loadingSimplifyVendors: false,
      }
    },
  })
  getSimplifyVendors(action$: Observable<boolean>) {
    return action$.pipe(
      switchMap((forQuickModal) =>
        this.order.getSimplifyVendors(forQuickModal).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any[]>) => {
      return {
        ...state,
        relatedOrders: action.payload,
      }
    },
  })
  getRelatedOrders(action$: Observable<number>) {
    return action$.pipe(
      switchMap((vendorId) => this.order.getRelatedOrders(vendorId)),
      switchMap((data) => of(this.formatTempOrder(responseHandler(data, false).body.data))),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  formatTempOrder(datas: any[]): TempOrder[] {
    return datas.map((data) => {
      data.createdDate = format(data.createdDate, 'MM/D/YY')
      data.deliveryDate = format(data.deliveryDate, 'MM/D/YY')
      data.updatedDate = format(data.updatedDate, 'MM/D/YY')
      if (data.wholesaleOrderStatus != null) data.status = data.wholesaleOrderStatus
      return data
    })
  }

  formatSimplifyOrder(data: any): Order {
    const dt = new Date(data.deliveryDate)
    dt.setHours(dt.getHours() + 7)

    return {
      id: data.wholesaleOrderId,
      totalCost: data.totalCost,
      totalPrice: data.totalPrice,
      status: data.wholesaleOrderStatus,
      deliveryDate: +dt,
      updatedDate: +new Date(data.updatedDate),
      createdDate: +new Date(data.createdDate),
      priceSheetId: data.priceSheetId,
      qboId: data.qboId,
      nsId: data.nsId,
      reference: data.reference,
      po: data.po,
      customer: {
        //TODO: discuss for this customer field whether it is needed or not
        id: data.clientId,
        name: data.fullName,
      },
      linkToken: data.linkToken,
      firstName: data.firstName,
      lastName: data.lastName,
      company: {
        id: data.clientId,
        name: data.companyName,
        type: data.type,
      },
      orderItems: data.orderItems ? data.orderItems : [],
      orderDate: data.orderDate,
      shippingCost: data.shippingCost,
      customOrderNo: data.customOrderNo,
      isLocked: data.isLocked,
      defaultRoute: data.defaultRoute,
      overrideRoute: data.overrideRoute,
      creditMemoCount: data.creditMemoCount,
      routeName: data.routeName,
      pasTotal: data.pasTotal,
      salesRep: data.salesRep,
      totalTax: data.totalTax,
      totalCostPlusTax: data.totalCostPlusTax,
      isApprox: data.isApprox,
      itemsCount: data.itemsCount ?? 0
    }
  }

  formatOrder(data: any): Order {
    const dt = new Date(data.deliveryDate)
    dt.setHours(dt.getHours() + 7)
    return {
      id: data.wholesaleOrderId,
      totalCost: data.totalCost,
      totalPrice: data.totalPrice,
      status: data.wholesaleOrderStatus,
      deliveryDate: +dt,
      updatedDate: +new Date(data.updatedDate),
      createdDate: +new Date(data.createdDate),
      priceSheetId: data.priceSheetId,
      qboId: data.qboId,
      nsId: data.nsId,
      reference: data.reference,
      po: data.po,
      customer: {
        //TODO: discuss for this customer field whether it is needed or not
        id: data.wholesaleCustomerClientId,
        name: data.fullName,
      },
      linkToken: data.linkToken,
      firstName: data.firstName,
      lastName: data.lastName,
      company: {
        id: data.wholesaleClient.clientId,
        name: data.wholesaleClient.clientCompany.companyName,
        type: data.wholesaleClient.type,
      },
      orderItems: data.orderItems,
      orderDate: data.orderDate,
      shippingCost: data.shippingCost,
      customOrderNo: data.customOrderNo,
      isLocked: data.isLocked,
      defaultRoute: data.defaultRoute,
      overrideRoute: data.overrideRoute,
      creditMemoCount: data.creditMemoCount,
      routeName: data.routeName,
    }
  }

  formatCustomerOrder(data: any): Order {
    const dt = new Date(data.deliveryDate)
    dt.setHours(dt.getHours() + 7)
    return {
      id: data.wholesaleOrderId,
      totalCost: data.totalCost,
      totalPrice: data.totalPrice,
      deliveryDate: +dt,
      updatedDate: +new Date(data.updatedDate),
      linkToken: data.linkToken,
      company: {
        id: data.wholesaleClient.clientId,
        name: data.wholesaleClient.clientCompany.companyName,
        type: 'SELLER',
      },
    }
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        saleItems: action.payload.map((v) => {
          if (v.wholesaleItem) {
            return { ...v, ...v.wholesaleItem, SKU: v.wholesaleItem.sku }
          }
          return {
            ...v,
            SKU: v.sku,
          }
        }),
        loadingSaleItems: false,
      }
    },
  })
  getSaleItems(action$: Observable<string | null>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((type) =>
        this.inventory.getAllItems(
          state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
          type,
        ),
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        saleItems: action.payload.map((v) => ({
          ...v,
          ...v.wholesaleItem,
          SKU: v.wholesaleItem.sku,
        })),
        loadingSaleItems: false,
        addItemDataRefreshedAt: new Date(),
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingSaleItems: false,
      }
    },
  })
  getItemForAddItemModal(action$: Observable<string | null>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.inventory.getAllItemsForAddModal(
          state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
          data.type,
          null,
          data.clientId
        )
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        purchaseSimplifyItems: action.payload,
        loadingSaleItems: false,
        addItemDataRefreshedAt: new Date(),
      }
    },
    error_message: (state: OrdersStateProps) => {
      return {
        ...state,
        loadingSaleItems: false,
      }
    },
  })
  getPurchaseItemsForAddItemModal(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getPurchaseItemsForAddItemModal(
          state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
        ),
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loadingSaleItems: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        itemList: action.payload,
        loadingSaleItems: false,
      }
    },
  })
  getItemList(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.inventory.getItemList(
          state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
          data,
        ),
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      startWith(this.createAction('before')()),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        adjustments: action.payload,
      }
    },
  })
  getOrderAdjustments(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) => this.order.getOrderAdjustements(orderId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        orderItemsImpacts: action.payload,
      }
    },
  })
  getAllOrderItemAdjustementsByOrderId(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) => this.order.getAllOrderItemAdjustementsByOrderId(orderId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // adjustments: [
        //   ...state.adjustments,
        //   payload
        // ]
        // message: 'Create an adjustment successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createOrderAdjustment(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.createOrderAdjustement(data.orderId, data.requestData).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderAdjustments)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Update an adjustment successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateOrderAdjustment(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateOrderItemAdjustement(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderAdjustments)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Update an adjustment successfully',
        // type: MessageType.SUCCESS,
      }
    },
  })
  deleteOrderAdjustement(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.deleteOrderItemAdjustement(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderAdjustments)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    before: (state) => {
      return { ...state, documentsLoading: true }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      const { payload } = action

      if (typeof payload === 'object' && Array.isArray(payload)) {
        return {
          ...state,
          documentsLoading: false,
          documents: payload,
          documentsTotal: payload.length,
        }
      }

      return {
        ...state,
        documentsLoading: false,
        documents: typeof payload === 'string' ? [] : payload && payload.dataList ? payload.dataList : [],
        documentsTotal: typeof payload === 'string' ? 0 : payload && payload.total ? payload.total : 0,
      }
    },
  })
  getDocuments(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getDocuments(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Create a document successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createDocument(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createDocument(state$.value.orders.currentOrder!.wholesaleOrderId, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getDocuments)({
              ...state$.value.orders.documentSearchProps,
              orderId: state$.value.orders.currentOrder!.wholesaleOrderId,
            }),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Docuement Updated Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateDocument(action$: Observable<Document>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateDocument(state$.value.orders.currentOrder!.wholesaleOrderId.toString(), data.id, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getDocuments)({
              ...state$.value.orders.documentSearchProps,
              orderId: state$.value.orders.currentOrder!.wholesaleOrderId,
            }),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state, action) => {
      return { ...state, documentsLoading: true }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      const { payload } = action
      return {
        ...state,
        documentsLoading: false,
        documents: typeof payload === 'string' ? [] : payload && payload.dataList ? payload.dataList : [],
        documentsTotal: typeof payload === 'string' ? 0 : payload && payload.total ? payload.total : 0,
      }
    },
  })
  getPurchaseOrderDocuments(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getPurchaseOrderDocuments(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Create a document successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createPurchaseOrderDocument(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order
          .createPurchaseOrderDocument(state$.value.orders.currentOrder!.wholesaleOrderId.toString(), data)
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            endWith(
              this.createActionFrom(this.getPurchaseOrderDocuments)({
                ...state$.value.orders.documentSearchProps,
                orderId: state$.value.orders.currentOrder!.wholesaleOrderId,
              }),
            ),
          ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Docuement Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updatePurchaseOrderDocument(action$: Observable<Document>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order
          .updatePurchaseOrderDocument(state$.value.orders.currentOrder!.wholesaleOrderId.toString(), data.id, data)
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            endWith(
              this.createActionFrom(this.getPurchaseOrderDocuments)({
                ...state$.value.orders.documentSearchProps,
                orderId: state$.value.orders.currentOrder!.wholesaleOrderId,
              }),
            ),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        orderItemIdForCW: payload,
      }
    },
  })
  setOrderItemId(action$: Observable<number>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      let catchWeights = cloneDeep(state.catchWeights)
      console.log(state.orderItemIdForCW, action.payload)
      return {
        ...state,
        loading: false,
        loadingOnEPick: false,
        catchWeight: action.payload,
        catchWeights,
      }
    },
  })
  getQuantityCatchWeight(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderItemId) => this.order.getQuantityCatchWeight(orderItemId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: 'Catch Weight added Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        loading: false,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  addQuantityCatchWeight(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.addQuantityCatchWeight(data.orderItemId, data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getQuantityCatchWeight)(data.orderItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({})
  reloadQuantityCatchWeight(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        new Observable((s) => {
          s.complete()
        }).pipe(
          endWith(
            this.createActionFrom(this.getOrderItemsById)(
              state$.value.orders.currentOrder!.wholesaleOrderId.toString(),
            ),
          ),
          endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        catchWeightValues: action.payload,
      }
    },
  })
  getWeightsByOrderItemIds(action$: Observable<any[]>) {
    return action$.pipe(
      switchMap((ids) => this.order.getWeightsByOrderItemIds(ids)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Catch Weight added Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        loading: false,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  addQuantityCatchWeightForWO(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.addQuantityCatchWeight(data.orderItemId, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getQuantityCatchWeight)(data.orderItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Catch Weight Updated Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        loading: false,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateQuantityCatchWeight(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateQuantityCatchWeight(data.qtyDetailId, data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getQuantityCatchWeight)(data.orderItemId)),
          endWith(
            this.createActionFrom(this.getOrderItemsById)(
              state$.value.orders.currentOrder!.wholesaleOrderId.toString(),
            ),
          ),
          endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Catch Weight Updated Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        loading: false,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateQuantityCatchWeightForWO(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateQuantityCatchWeight(data.qtyDetailId, data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getQuantityCatchWeight)(data.orderItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Catch Weight deleted Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  deleteQuantityCatchWeight(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.deleteQuantityCatchWeight(data.qtyDetailId).pipe(
          // switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getQuantityCatchWeight)(data.orderItemId)),
          endWith(
            this.createActionFrom(this.getOrderItemsById)(
              state$.value.orders.currentOrder!.wholesaleOrderId.toString(),
            ),
          ),
          endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Catch Weight deleted Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  deleteQuantityCatchWeightForWO(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.deleteQuantityCatchWeight(data.qtyDetailId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getQuantityCatchWeight)(data.orderItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        pallets: action.payload,
      }
    },
  })
  getPallets(action$: Observable<number>) {
    return action$.pipe(
      switchMap((orderItemId) => this.order.getPallets(orderItemId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  getOrderLots(action$: Observable<any>) {
    return action$.pipe(
      switchMap(({ productId, id }) => this.order.getOrderLots({ productId: productId, lotIds: id })),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        // message: 'Pallet Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updatePalletDetail(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updatePalletDetail(data.palletId, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPallets)(data.orderItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  updateAllPalletDetail(action$: Observable<any[]>) {
    return action$.pipe(
      switchMap((data: any[]) =>
        this.order.updateAllPalletDetail(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        weightSheet: payload,
      }
    },
  })
  getWeightSheets(action$: Observable<any>) {
    return action$.pipe(
      switchMap((palletId: any) =>
        this.order.getWeightSheets(palletId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        totalPalletsInfo: payload,
      }
    },
  })
  getTotalUnitsByAllPallets(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orderItemId: any) =>
        this.order.getTotalUnitsByAllPallets(orderItemId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        weightSheet: payload,
      }
    },
  })
  saveWeightSheet(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.saveWeightSheet(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<WholesaleRoute[]>) => {
      return {
        ...state,
        routes: payload,
        selectedRoute: payload && payload.length > 0 ? payload[0] : null,
      }
    },
  })
  getAllRoutes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.delivery.getAllRoutes()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Reducer()
  onSearchOrder(state: OrdersStateProps) {
    return state
  }

  @Reducer()
  resetDocuments(state: OrdersStateProps) {
    return { ...state, documentsLoading: false, documents: [], documentsTotal: 0 }
  }

  @Reducer()
  updatePriceSheetItem(
    state: OrdersStateProps,
    action: Action<{ priceSheetItemId: string; data: Partial<PriceSheetItem> }>,
  ) {
    const { priceSheetItemId, data } = action.payload!

    return {
      ...state,
      priceSheetItems: state.priceSheetItems.map((priceSheetItem) => {
        if (priceSheetItem.itemId === priceSheetItemId) {
          return { ...priceSheetItem, ...data }
        }
        return priceSheetItem
      }),
    }
  }

  @Reducer()
  updateTokenPriceSheetItem(
    state: OrdersStateProps,
    action: Action<{ priceSheetItemId: string; data: Partial<PriceSheetItem> }>,
  ) {
    const { priceSheetItemId, data } = action.payload!

    return {
      ...state,
      tokenPriceSheet: {
        ...state.tokenPriceSheet,
        priceSheetItemList: state.tokenPriceSheet.priceSheetItemList.map((priceSheetItem) => {
          if (priceSheetItem.itemId === priceSheetItemId) {
            return { ...priceSheetItem, ...data }
          }
          return priceSheetItem
        }),
      },
    }
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return { ...state, message: '', description: '', type: null }
    },
  })
  resetNotif(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Reducer()
  loadIndex(state: OrdersStateProps, action: Action<string>) {
    return {
      ...state,
      loadingIndex: action.payload,
    }
  }

  @Reducer()
  updateSaleItem(state: OrdersStateProps, action: Action<{ itemId: string; data: Partial<SaleItem> }>) {
    const { itemId, data } = action.payload!

    return {
      ...state,
      saleItems: state.saleItems.map((item) => {
        if (item.itemId === itemId) {
          return { ...item, ...data }
        }
        return item
      }),
    }
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        currentCompanyUsers: payload.userList.sort((a: any, b: any) =>
          `${a.firstName} ${a.lastName}`.localeCompare(`${b.firstName} ${b.lastName}`),
        ),
      }
    },
  })
  getCompanyUsers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.customer.getCompanyUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state }
    },
  })
  updateCompanyAddress(action$: Observable<Address>) {
    return action$.pipe(
      switchMap((address) =>
        this.order.updateCompanyAddress(address).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getShippingAddresses)()),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        orderItems: state.orderItems.map((item) => {
          if (item.wholesaleOrderItemId === action.payload.wholesaleOrderItemId) {
            return { ...item, ...action.payload }
          }
          return item
        }),
      }
    },
  })
  updatePOItemUom(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data) =>
        this.order.updatePOItemUom(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              // endWith(this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId)),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingsProps, { payload }: Action<any>) => {
      return { ...state }
    },
  })
  deleteProductType(action$: Observable<string>) {
    return action$.pipe(
      switchMap((id: string) =>
        this.setting.deleteProductType(id).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCompanyProductAllTypes)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingsProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'Type Updated Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: SettingsProps, action: Action<any[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        error: action.payload,
      }
    },
  })
  updateProductType(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting.updateProductType(data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCompanyProductAllTypes)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingsProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        message: 'Type Create Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: SettingsProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  setCompanyProductType(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => this.setting.saveCompanyProductType(data)),
      switchMap((data) =>
        of(responseHandler(data, false)).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCompanyProductAllTypes)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        allSales: action.payload,
      }
    },
  })
  getAllSales(action$: Observable<number>) {
    return action$.pipe(
      switchMap((lotId) => this.order.getAllSales(lotId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        allPurchases: action.payload,
      }
    },
  })
  getAllPurchases(action$: Observable<number>) {
    return action$.pipe(
      switchMap((lotId) => this.order.getAllPurchases(lotId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Price Sheet Email Sent',
        type: MessageType.SUCCESS,
      }
    },
  })
  sendEmail(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet.sendEmail(data.customer, data.priceSheetId, data.message).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        salesPrice: action.payload,
      }
    },
  })
  getSalesPrice(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) => this.order.getSalesPrice(data.itemId, data.clientId, data.from, data.to)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  // @Effect({
  //   done: (state: OrdersStateProps, { payload }: Action<any>) => {
  //     return { ...state, shippingAddresses: payload ? payload : [] }
  //   },
  // })
  // getShippingAddresses(action$: Observable<void>, state$: StateObservable<GlobalState>) {
  //   return action$.pipe(
  //     switchMap((lotId) => this.order.getShippingAddresses()),
  //     switchMap((data) => of(responseHandler(data, false).body.data)),
  //     map(this.createAction('done')),
  //     catchError((error) => of(checkError(error))),
  //   )
  // }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return { ...state, shippingAddresses: payload ? payload : [] }
    },
  })
  getCompanyShippingAddresses(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyAddress('SHIPPING')),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      if (action.payload.userSetting) {
        localStorage.setItem('isEnableCashSales', action.payload.userSetting.company.isEnableCashSales)
        return {
          ...state,
          settingCompanyName: action.payload.userSetting.company.companyName,
          companyName: action.payload.userSetting.companyName,
          themeKey: action.payload.userSetting.colorTheme ? action.payload.userSetting.colorTheme : 'green',
          logo: action.payload.userSetting.imagePath ? `${process.env.AWS_PUBLIC}/${action.payload.userSetting.imagePath}` : 'default',
          sellerSetting: action.payload.userSetting,
        }
      } else {
        return {
          ...state,
        }
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getUserSetting(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        sendingPOEmail: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      notification.success({
        message: 'SUCCESS',
        description: `Purchase order email sent successfully`,
        onClose: () => {},
        duration: 5,
      })
      return {
        ...state,
        sendingPOEmail: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      notification.error({
        message: 'ERROR',
        description: action.payload.body.message,
        onClose: () => {},
      })
      return {
        ...state,
        sendingPOEmail: false,
        type: MessageType.ERROR,
        error: action.payload,
      }
    },
  })
  sendEmailToVendor(action$: Observable<any>) {
    return action$.pipe(
      switchMap(({ data, to, cc }: any) =>
        this.order.sendPOEmail(data, to, cc).pipe(
          // switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        profitability: payload,
      }
    },
  })
  getPurchaseOrderProfitability(action$: Observable<number>) {
    return action$.pipe(
      switchMap((orderId) => this.order.getPurchaseOrderProfitability(orderId)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  formatProductTypes = (datas: any[]) => {
    let result = {
      unitOfMeasure: [],
      returnReason: [],
      carrier: [],
      modifier: [],
      costStructure: [],
      modeOfTransportation: [],
      suppliers: [],
      freightTypes: [],
      shippingTerms: [],
      paymentTerms: [],
      paymentTermsFixedTypes: [],
      extraCOO: [],
      extraCharges: [],
      customerTypes: [],
      vendorTypes: [],
      reasonType: [],
      containerType: [],
      customInvoiceTemplate: [],
    }

    // UNIT_OF_MEASURE, RETURN_REASON, CARRIER,MODIFIER,COST_STRUCTURE,MODE_OF_TRANSPORTATION
    datas.map((type: any) => {
      switch (type.type) {
        case 'UNIT_OF_MEASURE':
          result.unitOfMeasure.push(type)
          break
        case 'RETURN_REASON':
          result.returnReason.push(type)
          break
        case 'CARRIER':
          result.carrier.push(type)
          break
        case 'MODIFIER':
          result.modifier.push(type)
          break
        case 'COST_STRUCTURE':
          result.costStructure.push(type)
          break
        case 'MODE_OF_TRANSPORTATION':
          result.modeOfTransportation.push(type)
          break
        case 'SUPPLIERS':
          result.suppliers.push(type)
          break
        case 'FREIGHT':
          result.freightTypes.push(type)
          break
        case 'SHIPPING_TERM':
          result.shippingTerms.push(type)
          break
        case 'FINANCIAL_TERM':
          result.paymentTerms.push(type)
          break
        case 'FINANCIAL_TERM_FIXED_DAYS':
          result.paymentTermsFixedTypes.push(type)
          break
        case 'EXTRA_COO':
          result.extraCOO.push(type)
          break
        case 'EXTRA_CHARGE':
          result.extraCharges.push(type)
          break
        case 'CUSTOMER_BUSINESS_TYPE':
          result.customerTypes.push(type)
          break
        case 'VENDOR_BUSINESS_TYPE':
          result.vendorTypes.push(type)
          break
        case 'REASON_TYPE':
          result.reasonType.push(type)
          break
        case 'CONTAINER_TYPE':
          result.containerType.push(type)
          break
        case 'CUSTOM_INVOICE_TEMPLATE':
          result.customInvoiceTemplate.push(type)
      }
    })
    return result
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        duplicateOrderLoading: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // message: 'Duplicate Order Successfully',
        // type: MessageType.SUCCESS,
        // loadingIndex: '',
        newOrderId: action.payload.wholesaleOrderId,
        message: 'DUPLICATED_SUCCESS',
        currentOrder: action.payload,
        duplicateOrderLoading: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        // type: MessageType.ERROR,
        // error: action.payload,
        // loadingIndex: '',
        newOrderId: -1,
      }
    },
  })
  duplicateOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.duplicateOrder(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        newOrderId: action.payload.wholesaleOrderId,
        message: 'DUPLICATED_SUCCESS',
        currentOrder: action.payload,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        newOrderId: -1,
      }
    },
  })
  duplicatePurchaseOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.duplicatePurchaseOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  /**
   * For Manufacturing
   */

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        currentWorkOrder: payload,
        currentWorkOrderId: payload.wholesaleOrderId,
        type: MessageType.SUCCESS,
        loading: false,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
        loading: false,
      }
    },
  })
  getCurrentWorkOrder(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.manufacturing.getWorkOrderInfo(orderId ? +orderId : 0).pipe(
          switchMap((data) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  addWorkOrderItemByProductId(
    action$: Observable<{ orderId: number; itemId: number; type: number }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) =>
        this.manufacturing.addWorkOrderItemByProductId(info.orderId, info.itemId, info.type).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
          ),
          endWith(this.createActionFrom(this.getAllAuditLogs)(state$.value.orders.currentWorkOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
    error_message: (state: ManufacturingStateProps, { payload }: Action<any>) => {
      return {
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  addWorkOrderItemByOrderItemId(
    action$: Observable<{ orderId: number; itemId: number; type: number }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) =>
        this.manufacturing.addWorkOrderItemByOrderItemId(info.orderId, info.itemId, info.type).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
          ),
          endWith(this.createActionFrom(this.getAllAuditLogs)(state$.value.orders.currentWorkOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      let auditLog = {
        in: [],
        out: [],
        wo: [],
      }
      if (payload.length > 0) {
        auditLog.in = payload.filter((el) => el.type == 1)
        auditLog.out = payload.filter((el) => el.type == 2)
        auditLog.wo = payload.filter((el) => el.type == 3)
      }
      return { ...state, auditLog: auditLog }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        hasError: true,
      }
    },
  })
  getAllAuditLogs(action$: Observable<number>) {
    return action$.pipe(
      switchMap((orderId) => {
        return this.manufacturing.getAllAuditLog(orderId).pipe(
          switchMap((data) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        currentWorkOrderId: payload,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  duplicateWorkOrder(action$: Observable<number>) {
    return action$.pipe(
      switchMap((orderId) => {
        return this.manufacturing.duplicateWorkOrder(orderId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        currentOrder: payload,
      }
    },
  })
  orderPrintVersion(action$: Observable<number>) {
    return action$.pipe(
      switchMap((orderId) => {
        return this.order.orderPrintVersion(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: ManufacturingStateProps, action: Action<any>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  setWorkOrderToTemplate(
    action$: Observable<{ orderId: number; isTemplate: number }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((info) => {
        return this.manufacturing.setWorkOrderasTemplate(info.orderId, info.isTemplate).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
          ),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateWorkOrderItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.updateWorkOrderItem(data.wholesaleOrderItemId, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              endWith(
                this.createActionFrom(this.getAllAuditLogs)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        templates: payload,
        type: MessageType.SUCCESS,
        loading: false,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
        loading: false,
      }
    },
  })
  getAllWorkOrderTemplates(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getAllWorkOrderTemplates().pipe(
          switchMap((data) => of(responseHandler(data).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
        loading: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
        loading: false,
      }
    },
  })
  applyTemplate(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.manufacturing.applyTemplate(data.currentOrderId, data.templateOrderId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        stations: action.payload.body.data,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        stations: [],
        hasError: true,
      }
    },
  })
  getAllStationsInOrder(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getAllStations().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
        loading: false,
      }
    },
  })
  updateWorkOrderInfo(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.updateWorkOrderInfo(data.orderId, data.data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              endWith(
                this.createActionFrom(this.getAllAuditLogs)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }
  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateWorkOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.manufacturing.updateWorkOrderInfo(data.orderId, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              endWith(
                this.createActionFrom(this.getAllAuditLogs)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        workers: payload,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        workers: [],
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  getWorkers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.manufacturing.getAllWorkers().pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  assignWokerToWO(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((info) =>
        this.manufacturing.assignWorkerToWO(info.userId, info.orderId).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  unAssignWorkerFromWO(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((info) =>
        this.manufacturing.unAssignWorkerFromWO(info.userId, info.orderId).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  deleteWorkOrderItem(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderItemId) =>
        this.manufacturing.deleteWorkOrderItem(orderItemId).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              endWith(
                this.createActionFrom(this.getAllAuditLogs)(state$.value.orders.currentWorkOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        type: MessageType.SUCCESS,
        resourceMatrix: payload,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        hasError: true,
        resourceMatrix: [],
      }
    },
  })
  getProcessingResourceMatrix(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.manufacturing.getProcessingResourceMatrix(data.param, data.from, data.to).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        catchWeight: payload,
        message: 'Catch Weights added Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        loading: false,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  addCatchWeightByNumber(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.addCatchWeightByNumber(data.orderItemId, data.count).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getOrderItemsById)(
              state$.value.orders.currentOrder!.wholesaleOrderId.toString(),
            ),
          ),
          endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        message: 'Catch Weights deleted Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        loading: false,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  deleteCatchWeightByNumber(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.deleteCatchWeightByNumber(data.orderItemId, data.count).pipe(
          // switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getOrderItemsById)(
              state$.value.orders.currentOrder!.wholesaleOrderId.toString(),
            ),
          ),
          endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        frequentItems: payload,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  getFrequentSalesOrderItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.getFrequentSalesOrderItems(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        finalizing: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        finalizing: false,
        type: MessageType.SUCCESS,
        finalizing: false,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
        finalizing: false,
      }
    },
  })
  finalizePurchaseOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.finalizePurchaseOrder(data.clientId, data.values).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          endWith(
            this.createActionFrom(this.getWarehousePurchaseOrders)(JSON.parse(localStorage.getItem('GET_PO_SEARCH'))),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        printOrders: action.payload.body.data.dataList,
        hasError: false,
        loadingPrintOrders: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
      }
    },
  })
  getPrintOrders(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getPrintOrders(data.fromDate, data.toDate, data.searchStr, data.body).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        printOrders: [],
      }
    },
  })
  clearPrintOrders(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        hasError: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        hasError: true,
      }
    },
  })
  updateOrderItemsDisplayOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.udpateOrderItemsDisplayOrder(data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        updating: false,
        updatingField: false,
      }
    },
  })
  updateOrderItemPricingLogic(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.updateOrderItemPricingLogic(data).pipe(
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
            this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId),
            this.createActionFrom(this.getQuantityCatchWeight)(data.wholesaleOrderItemId),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      const { sellerSetting } = state
      const companySetting = sellerSetting ? sellerSetting.company : null
      return {
        ...state,
        updateOrderQuantityLoading: { loading: false, fetching: false },
        orderItems: state.orderItems.map((item) => {
          if (item.wholesaleOrderItemId == action.payload.wholesaleOrderItemId) {
            const constantRaio = judgeConstantRatio(item)
            let catchWeightQty = item.catchWeightQty
            if (constantRaio) {
              if (!action.payload.picked) {
                catchWeightQty = 0
              } else {
                catchWeightQty = ratioQtyToInventoryQty(action.payload.UOM, action.payload.picked, action.payload, 12)
              }
            } else {
              //disabled picked step and uom is catchweight
              if (companySetting && companySetting.isDisablePickingStep) {
                // !'0' = false  and !0 is true
                catchWeightQty = action.payload.quantity == '0' ? 0 : action.payload.quantity
              }
            }

            return { ...item, ...action.payload, catchWeightQty, rowLoading: false }
          }
          return item
        }),
      }
    },
  })
  handlerOrderItemsRedux(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        of(data).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        orderItems: action.payload.orderItems,
        orderItemByProduct: action.payload.orderItemByProduct,
        total: state.total + 1
      }
    },
  })
  updateOrderItemsRedux(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        of(data).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      const data = { ...action.payload }
      const displayOrder = action.payload.displayOrder
      delete data.displayOrder

      return {
        ...state,
        updateOrderQuantityLoading: { loading: false, fetching: true },
        orderItems: state.orderItems.map(v => {
          if (v.wholesaleOrderItemId === action.payload.wholesaleOrderItemId) {
            return { ...v, ...action.payload }
          }
          return v
        }),
        orderItemByProduct:
          state.orderItemByProduct && state.orderItemByProduct[displayOrder]
            ? {
                ...state.orderItemByProduct,
                [displayOrder]: {
                  ...state.orderItemByProduct[displayOrder],
                  ...data,
                },
              }
            : state.orderItemByProduct,
      }
    },
  })
  handlerUpdateSOUOMRedux(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        of(data).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      console.log(action.payload)
      return {
        ...state,
        orderItems: state.orderItems.map((item) => {
          if (item.wholesaleOrderItemId === action.payload.id) {
            const cost = ratioPriceToBasePrice(action.payload.pricingUOM, action.payload.cost, action.payload)
            const overrideUOM = action.payload.UOM
            const newPayload = { ...action.payload }
            delete newPayload.UOM
            console.log(overrideUOM)
            return { ...item, ...newPayload, cost, overrideUOM }
          }
          return item
        }),
      }
    },
  })
  handlerUpdatePOUOMRedux(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        of(data).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        updateOrderQuantityLoading: { loading: false, fetching: false },
        orderItems: state.orderItems.map((item) => {
          if (action.payload) {
            if (item.wholesaleOrderItemId === action.payload.wholesaleOrderItemId) {
              return { ...item, ...action.payload, rowLoading: false }
            }
          }
          return item
        }),
      }
    },
  })
  updateOrderItemPricingLogicWithoutRefresh(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data) =>
        this.order.updateOrderItemPricingLogic(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(),
              // this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: 'Catch weights were saved successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  addQuantityCatchWeightByList(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((arg: any) =>
        this.order.addQuantityCatchWeightByList(arg.orderItemId, arg.data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getQuantityCatchWeight)(arg.orderItemId)),
          endWith(
            this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId),
            this.createActionFrom(this.getOrderItemsById)(
              state$.value.orders.currentOrder!.wholesaleOrderId.toString(),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: 'Order quantities were copied successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  copyOrderQty(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.copyOrderQty(data).pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getOrderItemsById)(
              state$.value.orders.currentOrder!.wholesaleOrderId.toString(),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        printSetting: payload,
      }
    },
  })
  getPrintSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getPrintSetting().pipe(
          switchMap((data) => of(get(data, 'body.data', null)).pipe(map(this.createAction('done')))),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        customerContacts: action.payload,
      }
    },
  })
  getContacts(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getContacts(customerId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      // switchMap((data) => of(this.formatCustomers(data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        purchaseOrderItemHistory: action.payload,
      }
    },
  })
  getPurchaseOrderItemHistory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderItemId) => this.order.getPurchaseOrderItemHistory(orderItemId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        purchaseOrderItemCost: action.payload,
      }
    },
  })
  getPurchaseOrderItemCost(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderItemId) => this.order.getPurchaseOrderItemCost(orderItemId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  receiveMaterials(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.receiveMaterials(data.orderId, data.status, data.itemIds).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCurrentWorkOrder)(state$.value.orders.currentWorkOrderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      const currentWorkOrder = state.currentWorkOrder
      let childOrders = currentWorkOrder ? currentWorkOrder.childOrders : []
      const findIndex = childOrders.findIndex((el) => el.wholesaleOrderId == payload.wholesaleOrderId)
      if (findIndex > -1) {
        childOrders[findIndex] = payload
      }
      return {
        ...state,
        currentWorkOrder: { ...currentWorkOrder, childOrders: childOrders },
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  sendProductWarehouse(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.sendProductWarehouse(data.orderId, data.status).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        printCashSales: action.payload.body.data.dataList,
        hasError: false,
        loadingPrintOrders: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
        loadingPrintOrders: false,
      }
    },
  })
  getPrintCashSales(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getPrintCashSales(data.fromDate, data.toDate, data.searchStr).pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        cashSaleOrders: action.payload.dataList,
        cashSaleAllTotal: action.payload.orderTotal,
        cashSaleOrderTotal: action.payload.total,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
        hasError: true,
      }
    },
  })
  getWholesaleCashSaleOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getWholesaleCashSaleOrders(data).pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          map((data) => {
            const total = data.total
            const orderList = data.result.dataList
            const orderTotal = data.result.orderTotal
            const newList = orderList.map((order: any) => {
              return this.formatOrder(order)
            })
            return { total, dataList: newList, orderTotal }
          }),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        cashCustomer: [
          {
            ...action.payload,
            clientId: String(action.payload.clientId),
          },
        ],
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  getCashCustomerClient(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((userId) =>
        this.order.getCashCustomerClient({ userId }).pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }
  @Effect({
    done: (state: OrdersStateProps, action: Action<any[]>) => {
      return {
        ...state,
        cashReferences: action.payload,
      }
    },
  })
  getReferenceList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.getReferenceList().pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any[]>) => {
      return {
        ...state,
        customOrderNumbers: action.payload,
      }
    },
  })
  getCustomOrderNoList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.getCustomOrderNoList().pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      let quantity = 0
      let picked = 0
      let lotIds = []
      _.orderBy(action.payload, 'wholesaleOrderItemId').forEach((orderItem: any) => {
        quantity = quantity + Number(orderItem.quantity)
        picked = picked + Number(orderItem.picked)
        lotIds.push(orderItem.lotId)
      })
      const catchWeightQty = action.payload.map((v) => v.catchWeightQty)
      return {
        ...state,
        addingCartItem: false,
        orderItems: state.orderItems.map((v) => {
          if (!v.wholesaleOrderItemId) return action.payload[0]
          return v
        }),
        orderItemByProduct: {
          ...state.orderItemByProduct,
          [_.floor(action.payload[0].displayOrder)]: {
            items: action.payload,
            ...action.payload[0],
            grossWeight: _.reduce(action.payload, (sum, n) => sum + _.multiply(catchWeightQty, n.grossWeight || 0), 0),
            grossWeightUnit: action.payload[0].grossWeightUnit,
            grossVolume: _.reduce(action.payload, (sum, n) => sum + _.multiply(catchWeightQty, n.grossVolume || 0), 0),
            grossVolumeUnit: action.payload[0].grossVolumeUnit,
            catchWeightQty: catchWeightQty.reduce((n, p) => n + p, 0),
            lotIds: _.orderBy(action.payload, 'wholesaleOrderItemId').map((v) => v.lotId),
            displayOrderProduct: _.floor(action.payload[0].displayOrder),
            quantity: quantity,
            picked: picked,
            wholesaleOrderItemId: _.orderBy(action.payload, 'wholesaleOrderItemId')[0].wholesaleOrderItemId,
          },
        },
      }
    },
  })
  addCartOrderItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.addOrderItemForOrder(data).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
          retryWhen((errors) =>
            errors.pipe(
              delayWhen((val, i) => {
                if (val == 'CONFLICT') {
                  return timer(100)
                } else {
                  return timer(600000)
                }
              }),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      // const res = delete action.payload.wholesaleOrderItemId
      return {
        ...state,
        addingCartItem: false,
        orderItems: state.orderItems.map((v) => {
          if (v.wholesaleOrderItemId) return v
          return {
            ...v,
            catchWeightQty: v.catchWeightQty || 0,
            ...action.payload[0],
            picked: Number(v.picked) || Number(v.quantity),
            status: v.status,
            quantity: Number(v.quantity),
            pas: get(v, 'pas', false),
            rowLoading: get(v, 'rowLoading', false),
          }
        }),
      }
    },
  })
  addCashSalesCartOrderItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.addOrderItemForOrder(data).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              // endWith(this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              // endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      notification.success({
        message: 'SUCCESS',
        description: 'Changes were successfully saved.',
        duration: 5,
      })
      return {
        ...state,
      }
    }
  })
  updateItemsPrice(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updateItemsPrice(data).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
      }
    },
  })
  updateSalesPrice(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((arg: any) =>
        this.order.updateSalesPrice(arg.orderId, arg.data).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, true).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any[]>) => {
      return {
        ...state,
      }
    },
  })
  updateProduct(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => this.product.updateItemSuppliers(data)),
      switchMap((res) => of(responseHandler(res, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        simplifyItems: payload,
        loadingSaleItems: false,
        addItemDataRefreshedAt: new Date(),
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingSaleItems: false,
      }
    },
  })
  getSimplifyItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((type: any) =>
        this.order
          .getSimplifyItems(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET', type)
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    before: (state, action) => {
      return { ...state, simplifyItemsByOrderId: [] }
    },
    done: (state, action) => {
      return { ...state, simplifyItemsByOrderId: action.payload }
    },
  })
  getSimplifyItemsByOrderId(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(({ type, orderId }) =>
        this.order
          .getSimplifyItemsByOrderId(
            state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
            type,
            orderId,
          )
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            startWith(this.createAction('before')()),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        tokenPriceSheet: {
          ...state.tokenPriceSheet,
          priceSheetItemList: [...state.tokenPriceSheet.priceSheetItemList, payload],
        },
      }
    },
  })
  handleAddPriceSheetItem(action$: Observable<number>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      let orderItemByProduct = { ...state.orderItemByProduct }
      if (payload && payload.length) {
        payload.forEach((el: any) => {
          orderItemByProduct[_.floor(el.displayOrder)].picked = el.picked
          orderItemByProduct[_.floor(el.displayOrder)].catchWeightQty = el.catchWeightQty
          orderItemByProduct[_.floor(el.displayOrder)].status = el.status
        })
      }
      return {
        ...state,
        orderItems: payload,
        orderItemByProduct,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return state
    },
  })
  saveEPickedValues(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.saveEPickedValues(data.itemsList, data.orderId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // orderItems: _.merge(state.orderItems, payload.wholesaleOrderItemList),
        currentOrder: {
          ...state.currentOrder,
          allocateType: payload.allocateType,
        },
      }
    },
  })
  updatePOItemAllocate(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return state
    },
  })
  saveSalesOrderPalletLabels(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.saveSalesOrderPalletLabels(data.itemsList, data.orderId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        salesOrderPalletLabels: payload,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return state
    },
  })
  getSalesOrderPalletLabels(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderId: number) =>
        this.order.getSalesOrderPalletLabels(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  setOrdersStatusToPicked(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderIds: any) =>
        this.order.setOrdersStatusToPicked(orderIds).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      let { orderId, dataList } = payload
      const { orders } = state
      return {
        ...state,
        orders: _.map(orders, (order: OrderDetail) => {
          if (order.id == orderId) {
            if (_.isArray(dataList) && dataList.length > 0) {
              return { ...order, orderItems: dataList, fetched: true }
            } else {
              return { ...order, orderItems: [], fetched: true }
            }
          } else {
            return order
          }
        }),
      }
    },
  })
  getOrderItemsByOrderIdForPOPage(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderId: any) =>
        this.order.getOrderItemsByOrderIdForPOPage(1, orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map((data) => {
            return { orderId, dataList: data }
          }),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loading: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<OrderItem>) => {
      const groupData = _.groupBy(payload, 'wholesaleOrderId')
      let orders = []
      if (_.isArray(payload) && payload.length > 0) {
        orders = _.map(state.orders, (order: OrderDetail) => {
          return { ...order, orderItems: groupData[order.id], fetched: true }
        })
      } else {
        orders = state.orders
      }
      return {
        ...state,
        orders,
        loading: false,
      }
    },
  })
  getOrderItemsByOrderIdsForPOPage(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderIds: any) =>
        this.order.getOrderItemsByOrderIdForPOPage(2, orderIds).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        updateReceivedModalLoading: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<OrderItem>) => {
      return {
        ...state,
        orderItem: payload,
        updateReceivedModalLoading: false,
      }
    },
  })
  getOrderItemByOrderItemId(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((id: number) =>
        this.order.getOrderItemByOrderItemId(id).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        updateReceivedModalLoading: true,
        orderItems: state.orderItems.map((orderItem: OrderItem) => {
          if (orderItem.wholesaleOrderItemId == payload.wholesaleOrderItemId) {
            return {
              ...orderItem,
              receivedQty: payload.data.type === 1 ? payload.data.value : orderItem.receivedQty,
              orderWeight: payload.data.type === 2 ? payload.data.value : orderItem.orderWeight,
              status: 'RECEIVED',
            }
          } else {
            return orderItem
          }
        }),
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<OrderItem>) => {
      return {
        ...state,
        updateReceivedModalLoading: false,
      }
    },
  })
  updatePOIReceived(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.updatePOIReceived(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(data)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        allocationData: action.payload ? action.payload : [],
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  getAlloationData(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderId: string) =>
        this.order.getPOAllocate(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      console.log('allocation pallet paylaod', action.payload)
      return {
        ...state,
        allocationPalletData: action.payload,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  getAllocationPallets(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderId: string) =>
        this.order.getPOPallets(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {},
    error_message: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  getPOOneoffItems(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((orderId: string) =>
        this.order.getOrderOneOffItemsById(orderId).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        updateOrderQuantityLoading: { loading: false, fetching: false, lotLoading: false },
        orderItems: _.sortBy(
          [
            ...state.orderItems.filter((v) => {
              return ~~v.displayOrder !== ~~action.payload[0].displayOrder
            }),
            ...action.payload,
          ],
          'displayOrder',
        ),
        orderItemByProduct: {
          ...state.orderItemByProduct,
          [_.floor(action.payload[0].displayOrder)]: {
            ...state.orderItemByProduct[_.floor(action.payload[0].displayOrder)],
            lotIds: _.orderBy(action.payload, 'wholesaleOrderItemId').map((v) => v.lotId),
            UOM: action.payload[0].UOM,
            pricingUOM: action.payload[0].pricingUOM,
            catchWeightQty: action.payload.map((v) => v.catchWeightQty).reduce((n, p) => n + p, 0),
            status: action.payload[0].status,
            items: action.payload,
            picked: action.payload.map((v) => v.picked).reduce((p, n) => p + n, 0),
            grossWeight: _.reduce(action.payload, (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossWeight), 0),
            grossVolume: _.reduce(action.payload, (sum, n) => sum + _.multiply(n.catchWeightQty, n.grossVolume), 0),
            price: action.payload[0].price,
            cost: action.payload[0].cost,
            wholesaleOrderItemId: _.orderBy(action.payload, 'wholesaleOrderItemId')[0].wholesaleOrderItemId,
            enableLotOverflow: action.payload[0].enableLotOverflow,
          },
        },
      }
    },
    error_message: (state: any) => {
      return {
        ...state,
        updateOrderQuantityLoading: { loading: false, fetching: false },
      }
    },
  })
  updateOrderItemByProductId(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data: any) =>
        this.order
          .updateOrderItemByProductId({
            orderId: data.orderId,
            displayOrder: data.displayOrderProduct,
            productId: data.productId,
            data: data.data,
          })
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data).pipe(map(this.createAction('done')))),
            catchError((error) => of(checkError(error))),
            retryWhen((errors) =>
              errors.pipe(
                delayWhen((val, i) => {
                  if (val == 'CONFLICT') {
                    return timer(100)
                  } else {
                    return timer(600000)
                  }
                }),
              ),
            ),
          ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        updateOrderQuantityLoading: { loading: false, fetching: false },
        orderItems: state.orderItems.map((v) =>
          v.itemId === action.payload.wholesaleItemId ? { ...v, ...action.payload } : v,
        ),
        orderItemByProduct: {
          ...state.orderItemByProduct,
          [_.floor(action.payload.displayOrder)]: {
            ...state.orderItemByProduct[_.floor(action.payload.displayOrder)],
            price: action.payload.price,
            pricingLogic: action.payload.pricingLogic,
            pricingGroup: action.payload.pricingGroup,
            pricingUOM: action.payload.pricingUOM,
            items: state.orderItemByProduct[_.floor(action.payload.displayOrder)].items.map((v) => ({
              ...v,
              price: action.payload.price,
              pricingUOM: action.payload.pricingUOM,
              pricingLogic: action.payload.pricingLogic,
              pricingGroup: action.payload.pricingGroup,
            })),
          },
        },
      }
    },
    error_message: (state: any) => {
      return {
        ...state,
        updateOrderQuantityLoading: { loading: false, fetching: false },
      }
    },
  })
  updateOrderItemPrice(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      concatMap((data: any) =>
        this.order
          .updateOrderItemPrice({
            orderId: data.orderId,
            productId: data.productId,
            displayOrder: data.displayOrderProduct,
            data: data.data,
          })
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data).pipe(map(this.createAction('done')))),
            catchError((error) => of(checkError(error))),
            retryWhen((errors) =>
              errors.pipe(
                delayWhen((val, i) => {
                  if (val == 'CONFLICT') {
                    return timer(100)
                  } else {
                    return timer(600000)
                  }
                }),
              ),
            ),
          ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        updating: false,
      }
    },
  })
  updateOrderItemProduct(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      concatMap((data: any) =>
        this.order
          .updateOrderItemProduct({
            orderId: data.orderId,
            productId: data.productId,
            displayOrder: data.displayOrderProduct,
            data: data.data,
          })
          .pipe(
            switchMap((resp) =>
              of(responseHandler(resp, false).body.data).pipe(
                map(this.createAction('done')),
                endWith(this.createActionFrom(this.getOrderItemsById)(data.orderId)),
                catchError((error) => of(checkError(error))),
              ),
            ),
          ),
      ),
    )
  }

  @Reducer()
  updateOrderItemCatchWeight(
    state: OrdersStateProps,
    action: Action<{ displayOrderProduct: number; catchWeightQty: number; picked: number }>,
  ) {
    return {
      ...state,
      orderItemByProduct: {
        ...state.orderItemByProduct,
        [action.payload.displayOrderProduct]: {
          ...state.orderItemByProduct[action.payload.displayOrderProduct],
          catchWeightQty: action.payload.catchWeightQty,
          picked: action.payload.picked,
        },
      },
    }
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      const currentTotal = state.total - 1
      let currentPage = state.page
      if (currentTotal%state.pageSize === 0) {
        currentPage = state.page - 1
      }
      return {
        ...state,
        page: currentPage
      }
    },
  })
  deleteOrderItem(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order
          .deleteOrderItem({ orderId: data.orderId, productId: data.productId, displayOrder: data.displayOrderProduct })
          .pipe(
            switchMap((resp) =>
              of(responseHandler(resp, true).body.data).pipe(
                map(this.createAction('done')),
                endWith(this.createActionFrom(this.getSalesOrderItems)(data.orderId)),
              ),
            ),
            catchError((error) => of(checkError(error))),
            retryWhen((errors) =>
              errors.pipe(
                //log error message
                tap((val) => console.log(`${val}`)),
                //restart in 0.5 seconds
                delayWhen((val) => {
                  if (val == 'CONFLICT') {
                    return timer(500)
                  } else {
                    //Hack: Avoid other unknown exceptions
                    return timer(600000)
                  }
                }),
              ),
            ),
          ),
      ),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
        currentProducts: [],
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        currentProducts: action.payload,
      }
    },
  })
  getVendorProducts(action$: Observable<any>) {
    return action$.pipe(
      switchMap((vendorId) => {
        return this.vendor.getVendorProducts(vendorId).pipe(
          switchMap((data) => {
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
        enterPurchaseProducts: [],
        loadingPurchaseProducts: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        enterPurchaseProducts: action.payload,
        loadingPurchaseProducts: false,
      }
    },
  })
  getVendorProductsForEnterPurchase(action$: Observable<any>) {
    return action$.pipe(
      switchMap((vendorId) => {
        return this.vendor.getVendorProducts(vendorId).pipe(
          switchMap((data) => {
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
      }
    },
  })
  updateSortOrderItemDisplayOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => {
        return this.order
          .updateSortOrderItemDisplayOrder(data.orderId, data.productId, data.oldDisplayOrder, data.newDisplayOrder)
          .pipe(
            switchMap((resp) => {
              return of(responseHandler(resp, false).body.data)
            }),
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId)),
            catchError((error) => of(checkError(error))),
          )
      }),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
      }
    },
  })
  updateAutofillPicked(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => {
        return this.order.updateAutofillPicked(data.orderId).pipe(
          switchMap((resp) => {
            return of(responseHandler(resp, false).body.data)
          }),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
        itemAvailableItemLots: [],
        loadingAvailableItemLots: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        itemAvailableItemLots: action.payload,
        loadingAvailableItemLots: false,
      }
    },
  })
  getItemAvailableItemLotById(action$: Observable<any>) {
    return action$.pipe(
      switchMap((wholesaleItemId) => {
        return this.order.getItemAvailableItemLotById(wholesaleItemId).pipe(
          switchMap((data) => {
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        )
      }),
    )
  }

  @Effect({
    done: (state: OrdersStateProps) => {
      return {
        ...state,
        containerList: [],
      }
    },
  })
  clearContainerList(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        containerList: action.payload,
      }
    },
  })
  getOrderContainerList(action$: Observable<any>) {
    return action$.pipe(
      switchMap((wholesaleOrderId) => {
        return this.order.getOrderContainerList(wholesaleOrderId).pipe(
          switchMap((data:any) => {
            return of(data.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        )
      }),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        currentPrintContainerId: payload,
      }
    },
  })
  setCurrentPrintContainerId(action$: Observable<number>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        currentPrintContainerId: '',
      }
    },
  })
  clearCurrentPrintContainerId(action$: Observable<number>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        isHealthBol: payload,
      }
    },
  })
  setIsHealthBol(action$: Observable<number>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
        gettingAllocationsFromOrder: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        allocationsFromOrder: action.payload,
        gettingAllocationsFromOrder: false,
      }
    },
  })
  getAllocationsFromOrder(action$: Observable<any>) {
    return action$.pipe(
      switchMap((wholesaleOrderId) => {
        return this.order.getAllocations(wholesaleOrderId, 2).pipe(
          switchMap((data) => {
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        )
      }),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
        gettingAllocationsToOrder: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        allocationsToOrder: action.payload,
        gettingAllocationsToOrder: false,
      }
    },
  })
  getAllocationsToOrder(action$: Observable<any>) {
    return action$.pipe(
      switchMap((wholesaleOrderId) => {
        return this.order.getAllocations(wholesaleOrderId, 1).pipe(
          switchMap((data) => {
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        )
      }),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
        gettingAllocationsToOrder: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  saveAllocationsTo(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.saveAllocations(data.orderId, data.data).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getAllocationsToOrder)(data.orderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
          startWith(this.createAction('before')()),
        ),
      ),
    )
  }

  @Effect({
    before: (state: OrdersStateProps) => {
      return {
        ...state,
        gettingAllocationsFromOrder: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  saveAllocationsFrom(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.saveAllocations(data.orderId, data.data).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getAllocationsFromOrder)(data.orderId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
          startWith(this.createAction('before')()),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        enteringPO: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        enteringPO: false,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
        enteringPO: false,
      }
    },
  })
  enterPurchaseOrderForCostAllocation(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.finalizePurchaseOrder(data.clientId, data.values).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        repacking: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        repacking: false,
        repack: payload,
      }
    },
  })
  getRepack(action$: Observable<any>) {
    return action$.pipe(
      switchMap((orderId) => {
        return this.order.getRepack(orderId).pipe(
          switchMap((data) => {
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        )
      }),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        repacking: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        repacking: false,
        repack: payload,
      }
    },
  })
  deleteRepackItem(action$: Observable<any>) {
    return action$.pipe(
      switchMap((repackItemId) => {
        return this.order.deleteRepackItem(repackItemId).pipe(
          switchMap((data) => {
            return of(responseHandler(data, true).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        )
      }),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        repacking: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        repack: payload,
        repacking: false,
      }
    },
  })
  saveRepack(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(({ orderItemId, data }) => {
        return this.order.saveRepack(orderItemId, data).pipe(
          switchMap((resp) => {
            return of(responseHandler(resp, false).body.data)
          }),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        repacking: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        repacking: false,
      }
    },
  })
  updateRepack(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => {
        return this.order.updateRepack(data).pipe(
          switchMap((resp) => {
            return of(responseHandler(resp, false).body.data)
          }),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          endWith(this.createActionFrom(this.getOrderDetail)(state$.value.orders.currentOrder!.wholesaleOrderId)),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        error: null,
        repack: null,
        repacking: true,
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        error: null,
        repack: payload,
        repacking: false,
      }
    },
    error_message: (state: OrdersStateProps, action: Action<Order[]>) => {
      return {
        ...state,
        error: action.payload,
        hasError: true,
      }
    },
  })
  repackSalesOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => {
        return this.order.repackSalesOrder(data).pipe(
          switchMap((resp) => {
            return of(responseHandler(resp, false).body)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      console.log('allocation pallet paylaod', action.payload)
      return {
        ...state,
        unassignOrderItemsCount: action.payload,
      }
    },
  })
  getUnassignOrderItemsCount(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId: string) =>
        this.order.getUnassignOrderItemsCount(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      console.log('allocation pallet paylaod', action.payload)
      return {
        ...state,
        containerList: [action.payload],
      }
    },
  })
  getWholesaleContainer(action$: Observable<string>) {
    return action$.pipe(
      switchMap((wholesaleContainerId: string) =>
        this.order.getWholesaleContainer(wholesaleContainerId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any) => {
      console.log('*before')
      return {
        ...state,
        updatingReceived: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  setOrderItemsReceived(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      mergeMap((data: any) =>
        this.order.setOrderItemsReceived(data.orderId, data.qtyType).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              startWith(this.createAction('before')(true)),
              endWith(
                this.createActionFrom(this.getOrderItemsById)(state$.value.orders.currentOrder!.wholesaleOrderId),
                this.createActionFrom(this.updateOrderStatus)(state$.value.orders.currentOrder!.wholesaleOrderId),
              ),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }


  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loadingSaleItems: true,
      }
    },
    done: (state: any) => {
      return {
        ...state,
        loadingSaleItems: false,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingSaleItems: false,
      }
    },
  })
  addPriceSheetItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet.addPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          endWith(this.createActionFrom(this.updatePriceSheetItemsIsCPL)(data.assignItemListId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        loadingSaleItems: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        loadingSaleItems: false,
      }
    },
    error_message: (state: OrdersStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingSaleItems: false,
      }
    },
  })
  removePriceSheetItems(action$: any, state$: any) {
    return action$.pipe(
      switchMap((data: any) => {
        const { priceSheetId, deleteItemListId } = data
        return this.pricing.removeProduct(priceSheetId, deleteItemListId).pipe(
          switchMap((data: any) => of(responseHandler(data, true))),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          endWith(this.createActionFrom(this.updatePriceSheetItemsIsCPL)(deleteItemListId)),
        )
      }),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<any>) => {
      return {
        ...state,
        saleItems: state.saleItems.map((item) => {
          if (action.payload.includes(item.itemId)) {
            return {
              ...item,
              isCpl: !item.isCpl
            }
          }
          return item
        }),
      }
    },
  })
  updatePriceSheetItemsIsCPL(action$: any, state$: any) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }


  @Effect({
    before: (state: any) => {
      console.log('*before')
      return {
        ...state,
        syncingQboOrder: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      console.log('* after ', action.payload)
      const { itemList, qboBills } = action.payload
      return {
        ...state,
        syncingQboOrder: false,
        syncedItemList: itemList,
        syncedQboBills: qboBills
      }
    },
  })
  getSyncQboOrderDetails(action$: Observable<string>) {
    return action$.pipe(
      switchMap((orderId) =>
        this.order.getSyncQboOrderDetails(orderId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any) => {
      return {
        ...state,
        syncingQboOrder: true,
      }
    },
    done: (state: OrdersStateProps, action: Action<OrderDetail>) => {
      return {
        ...state,
        // syncingQboOrder: false,
      }
    },
  })
  syncQboOrderItems(action$: Observable<string>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.syncQboOrderItems(data.orderId, data.body).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(true)),
          endWith(this.createActionFrom(this.getSyncQboOrderDetails)(data.orderId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }
}

export type OrdersDispatchProps = ModuleDispatchProps<OrdersModule>
