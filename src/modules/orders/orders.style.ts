import styled from '@emotion/styled'
import { white, black, brownGrey } from '~/common'

const headerHeight = '270px'

export const HeaderContainer = styled('div')({
  width: '100%',
  height: headerHeight,
  backgroundColor: white,
  padding: '50px 38px 98px 66px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const HeaderOptions = styled('div')({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
})

export const buttonStyle: React.CSSProperties = {
  borderRadius: '200px',
  height: '40px',
  border: `1px solid ${brownGrey}`,
  margin: '0 11px',
}
