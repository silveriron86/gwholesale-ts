import { Button } from 'antd'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton, ThemeTable } from '~/modules/customers/customers.style'
import { POSItem } from '../pointOfSale.module'
import {
  AmountDue,
  AmountPaymentButton,
  AmountPaymentText,
  BottomButtons,
  TableEle,
  TableEleNoButton,
  Total,
  TotalBody,
  TotalDiv,
} from '../pointOfSale.style'
import _ from 'lodash'
import { QtyModal } from './modals/QtyModal'
import { PriceModal } from './modals/PriceModal'
import { PointOfSaleService } from '../pointOfSale.service'
import { checkError, responseHandler } from '~/common/utils'
import { RemoveModal } from './modals/RemoveModal'
import { PaymentModal } from './modals/PaymentModal'
import { PrintModal } from './modals/PrintModal'
import { CustomerService } from '~/modules/customers/customers.service'
import { ConvertInvoiceModal } from './modals/ConvertToInvoice'
import { of } from 'rxjs'
import { formatBn } from '~/common/mathjs'

export interface ClientObj {
  clientCompanyId: number
  clientCompanyName: string
  clientId: number
  wholesaleCompanyId: number
}

export interface CompanyPrintInfo {
  imagePath: string
  name: string
  address1: string
  address2: string
  phone: string
}

interface IProps {
  orderId: string
  item: POSItem | null
  setTotalValue: (value: string) => void
  resetList: () => void
  companyInfo: CompanyPrintInfo | null
  cancelOrder: () => void
}

const POSBody = ({ orderId, item, setTotalValue, resetList, companyInfo, cancelOrder }: IProps): JSX.Element => {
  const [loading, setLoading] = useState(false)
  const [displayTableData, setDisplayTableData] = useState<POSItem[]>([])
  const [clientArr, setClientArr] = useState<ClientObj[]>([])

  const [qtyModalVisible, setQtyModalVisible] = useState(false)
  const [priceyModalVisible, setPriceModalVisible] = useState(false)
  const [removeVisible, setRemoveModalVisible] = useState(false)
  const [paymentVisible, setPaymentModalVisible] = useState(false)
  const [convertInvoiceVisible, setConvertInvoiceVisible] = useState(false)

  const [choosePOItem, setChoosePOItem] = useState<POSItem | null>(null)
  const [chooseType, setChooseType] = useState<string>('')
  const [chooseNote, setChooseNote] = useState<string>('')

  const [payment, setPayment] = useState<boolean>(false)
  const [print, setPrint] = useState<boolean>(false)

  const [propOrderId, setPropOrderId] = useState<string>('')

  useEffect(() => {
    CustomerService.instance.getSimplifyCustomers().subscribe({
      next(res: any) {
        //of(responseHandler(res, false).body.data)
        if (res.statusCodeValue == 200) {
          setClientArr(res.body.data)
        }
      },
      error(err) {
        checkError(err)
      },
    })

    const posObj = localStorage.getItem('posItemTable')
    if (posObj !== null && posObj !== 'null') {
      const obj = JSON.parse(posObj)
      setDisplayTableData(obj.tableData)
    }
  }, [])

  useEffect(() => {
    setPropOrderId(orderId)
  }, [orderId])

  useEffect(() => {
    setTotalValue((getTotal('tax') + getSubtotal()).toFixed(2))
    return function cleanup() {
      localStorage.setItem(
        'posItemTable',
        JSON.stringify({
          tableData: displayTableData,
        }),
      )
    }
  }, [displayTableData])

  useEffect(() => {
    const newArr = _.cloneDeep(displayTableData)
    if (item) {
      //if item already added then add 1 qty
      const itemIndex = newArr.map((a) => a.itemId).findIndex((b) => b == item.itemId)
      itemIndex == -1 ? newArr.push(item) : (newArr[itemIndex].orderQty = newArr[itemIndex].orderQty + 1)
      setDisplayTableData(newArr)
    }
  }, [item])

  useEffect(() => {
    setTotalValue((getTotal('tax') + getSubtotal()).toFixed(2))
  }, [qtyModalVisible, priceyModalVisible, displayTableData])

  useEffect(() => {
    //console.log(qtyModalVisible)
  }, [qtyModalVisible])

  const columns: {
    title?: string
    dataIndex: string
    align?: 'left' | 'right' | 'center' | undefined
    width?: number
    className?: undefined | string
    render?: any
  }[] = [
    {
      title: '#',
      dataIndex: '',
      align: 'left',
      width: 10,
      render: (value: string, record: POSItem, index: number) => {
        return <>{index + 1}</>
      },
    },
    {
      title: 'Item',
      dataIndex: 'item',
      align: 'left',
      width: 130,
      render: (value: string, record: POSItem, index: number) => {
        return <>{record.item}</>
      },
    },
    {
      title: 'Order QTY',
      dataIndex: 'orderQty',
      align: 'left',
      width: 70,
      render: (value: string, record: POSItem, index: number) => {
        return (
          <TableEle>
            <p>{`${record.orderQty} ${record.orderQtyUOM}`}</p>
            <Button
              onClick={() => {
                setChoosePOItem(record)
                setQtyModalVisible(true)
              }}
            >
              Qty/UOM
            </Button>
          </TableEle>
        )
      },
    },
    {
      title: 'Price',
      dataIndex: 'price',
      align: 'left',
      width: 70,
      render: (value: string, record: POSItem, index: number) => {
        return (
          <TableEle>
            <p>{`$${record.price} ${record.priceUOM == null ? record.orderQtyUOM : record.priceUOM}`}</p>
            <Button
              onClick={() => {
                setChoosePOItem(record)
                setPriceModalVisible(true)
              }}
            >
              Price
            </Button>
          </TableEle>
        )
      },
    },
    {
      title: 'Billable Qty',
      dataIndex: '',
      align: 'left',
      width: 70,
      render: (value: string, record: POSItem, index: number) => {
        return (
          <TableEleNoButton>
            <p>{`${record.orderQty} ${record.orderQtyUOM}`}</p>{' '}
          </TableEleNoButton>
        )
      },
    },
    {
      title: 'Tax',
      dataIndex: 'tax',
      align: 'left',
      width: 70,
      render: (value: string, record: POSItem, index: number) => {
        return (
          <TableEleNoButton>
            <p>{record.tax}</p>
          </TableEleNoButton>
        )
      },
    },
    {
      title: 'Total Price',
      dataIndex: '',
      align: 'left',
      width: 100,
      render: (value: string, record: POSItem, index: number) => {
        const totalPrice = record.orderQty * record.price + record.tax
        return (
          <TableEle>
            <p>
              {formatBn(totalPrice, {
                comma: true,
                fixed: 2,
                $: true,
              })}
            </p>
            <Button
              onClick={() => {
                setChoosePOItem(record)
                setRemoveModalVisible(true)
              }}
            >
              Remove
            </Button>
          </TableEle>
        )
      },
    },
  ]

  const resetAll = () => {
    setDisplayTableData([])

    setChoosePOItem(null)
    setChooseType('')
    setChooseNote('')

    setPayment(false)
    setPrint(false)
    resetList()
    localStorage.setItem('posItemTable', 'null')
  }

  const updatePosItemTax = (posItem: POSItem) => {
    let tax = 0
    PointOfSaleService.instance
      .GetTax(propOrderId, posItem.itemId, (posItem.price * posItem.orderQty).toString())
      .subscribe({
        next(res: any) {
          //of(responseHandler(res, false).body.data)
          tax = res.body.data
        },
        error(err) {
          checkError(err)
        },
      })
    //update item with new tax
    posItem.tax = tax
    updatePosItem(posItem, 'priceChange')
  }

  const updatePosItem = (posItem: POSItem, key: string) => {
    const newArr = _.cloneDeep(displayTableData)
    const index = newArr.findIndex((item) => item.itemId == posItem.itemId)
    newArr[index] = posItem
    posItem[key] = true
    setDisplayTableData(newArr)
  }

  const removeItem = (posItem: POSItem) => {
    const newArr = _.cloneDeep(displayTableData)
    const index = newArr.findIndex((item) => item.itemId == posItem.itemId)
    newArr.splice(index, 1)
    setDisplayTableData(newArr)
  }

  const saveAndPrint = () => {
    const list = displayTableData.map((item, index) => {
      return {
        wholesaleItemId: item.itemId,
        displayOrder: index,
        quantity: item.orderQty,
        uomId: item.uomChange ? 1 : undefined,
        price: item.priceChange ? item.price : undefined,
      }
    })
    const body = {
      paymentType: chooseType, // string
      posNote: chooseNote, // string
      amountPaid: parseFloat((getTotal('tax') + getSubtotal()).toFixed(2)),
      itemList: list,
    }
    PointOfSaleService.instance.SaveAndPrint(propOrderId, body).subscribe({
      next(res: any) {
        of(responseHandler(res, true).body.data)
        if (res.statusCodeValue == 200) {
          setPrint(true)
          //resetAll()
        }
      },
      error(err) {
        checkError(err)
      },
    })
  }

  const convertToInvoice = (clientId: string) => {
    const list = displayTableData.map((item, index) => {
      return {
        wholesaleItemId: item.itemId,
        displayOrder: index,
        quantity: item.orderQty,
        uomId: item.uomChange ? 1 : undefined,
        price: item.priceChange ? item.price : undefined,
      }
    })
    PointOfSaleService.instance.ConvertToInvoice(propOrderId, clientId, list).subscribe({
      next(res: any) {
        of(responseHandler(res, true).body.data)
        if (res.statusCodeValue == 200) {
          //setPrint(true)
          resetAll()
        }
      },
      error(err) {
        checkError(err)
      },
    })
  }

  const getTotal = (key: string) => {
    if (displayTableData.length > 0) {
      const arr = displayTableData.map((item) => item[key])
      return arr.reduce((accumulator, currentValue) => {
        return accumulator + currentValue
      })
    } else {
      return 0
    }
  }

  const getSubtotal = () => {
    if (displayTableData.length > 0) {
      const arr = displayTableData.map((item) => item['orderQty'] * item['price'])
      return arr.reduce((accumulator, currentValue) => {
        return accumulator + currentValue
      })
    } else {
      return 0
    }
  }

  return (
    <>
      <div>
        <ThemeTable
          style={{ marginLeft: '70px', marginRight: '50px' }}
          loading={loading}
          dataSource={displayTableData}
          columns={columns}
          rowKey="purchaseOrderItemId"
          pagination={false}
        />
        <TotalBody>
          <TotalDiv>
            <h6>Total Billable Qty</h6>
            <p>{getTotal('orderQty').toFixed(2)}</p>
          </TotalDiv>
          <TotalDiv>
            <h6>Subtotal</h6>
            <p>
              {formatBn(getSubtotal(), {
                comma: true,
                fixed: 2,
                $: true,
              })}
            </p>
          </TotalDiv>
          <TotalDiv>
            <h6>Tax</h6>
            <p>
              {formatBn(getTotal('tax'), {
                comma: true,
                fixed: 2,
                $: true,
              })}
            </p>
          </TotalDiv>
        </TotalBody>
        <Total>
          <h6>Total</h6>
          <p>
            {formatBn(getTotal('tax') + getSubtotal(), {
              comma: true,
              fixed: 2,
              $: true,
            })}
          </p>
        </Total>
        <AmountDue>
          {payment ? (
            <>
              <AmountPaymentButton disabled>Add Payment</AmountPaymentButton>
              <AmountPaymentText>{chooseNote}</AmountPaymentText>
            </>
          ) : (
            <></>
          )}
          <h6>Amount</h6>
          <p>
            {formatBn(payment ? 0 : getTotal('tax') + getSubtotal(), {
              comma: true,
              fixed: 2,
              $: true,
            })}
          </p>
        </AmountDue>
        <BottomButtons>
          <Button
            disabled={payment}
            onClick={() => {
              cancelOrder()
              resetAll()
            }}
          >
            cancel Order
          </Button>
          {!payment ? (
            <ThemeButton
              disabled={displayTableData.length == 0}
              onClick={() => {
                setPaymentModalVisible(true)
              }}
            >
              Add Payment
            </ThemeButton>
          ) : (
            <></>
          )}
          <ThemeButton
            disabled={payment || displayTableData.length == 0}
            onClick={() => {
              setConvertInvoiceVisible(true)
            }}
          >
            Convert to Invoice
          </ThemeButton>
          <ThemeButton
            disabled={!payment}
            style={{ position: 'absolute', right: '66px' }}
            onClick={() => saveAndPrint()}
          >
            Save and Print
          </ThemeButton>
        </BottomButtons>
      </div>
      {qtyModalVisible && (
        <QtyModal
          posItem={choosePOItem}
          hideModal={() => setQtyModalVisible(false)}
          saveQtyAndUOM={(posItem: POSItem) => {
            updatePosItem(posItem, 'uomChange')
          }}
        />
      )}
      {priceyModalVisible && (
        <PriceModal
          posItem={choosePOItem}
          hideModal={() => setPriceModalVisible(false)}
          savePrice={(posItem: POSItem) => {
            updatePosItemTax(posItem)
          }}
        />
      )}
      {removeVisible && (
        <RemoveModal
          posItem={choosePOItem}
          hideModal={() => setRemoveModalVisible(false)}
          removeItem={(posItem: POSItem) => {
            removeItem(posItem)
          }}
        />
      )}
      {paymentVisible && (
        <PaymentModal
          totalValue={getSubtotal().toFixed(2)}
          hideModal={() => setPaymentModalVisible(false)}
          save={(type: string, note: string) => {
            setChooseType(type)
            setChooseNote(note)
            setPayment(true)
          }}
        />
      )}
      {convertInvoiceVisible && (
        <ConvertInvoiceModal
          sendClientId={(id: string) => {
            convertToInvoice(id)
            //resetAll()
          }}
          hideModal={() => setConvertInvoiceVisible(false)}
          clientArr={clientArr}
        />
      )}
      {print && (
        <PrintModal
          hideModal={() => {
            setPrint(false)
            resetAll()
          }}
          orderId={propOrderId}
          companyInfo={companyInfo}
          dataList={displayTableData}
          widthUOM={true}
        />
      )}
    </>
  )
}

export default POSBody
