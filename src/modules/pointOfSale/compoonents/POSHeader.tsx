import * as React from 'react'
import {
  POSPageBody,
  POSPageBodyDatePicker,
  POSPageBodyInput,
  POSPageBodyInputIcon,
  POSPageBodyInputSwitch,
  POSPageBodyOrder,
  POSPageBodySelect,
  POSPageHeader,
} from '../pointOfSale.style'
import { Icon } from '~/components'
import { Button, DatePicker, Input, Select, Switch } from 'antd'
import { useKeyPress } from 'ahooks'
import { useEffect, useState } from 'react'
import moment from 'moment'
import { formatBn } from '~/common/mathjs'

interface IProps {
  addNewItemByUpc: (upcCode: string) => void
  addNewItemById: (Id: string) => void
  totalValue: string
  orderNo: string
  upcList: { id: string; upc: string; name: string }[]
}

const { Option } = Select

const POSHeader = ({ addNewItemByUpc, addNewItemById, totalValue, orderNo, upcList }: IProps): JSX.Element => {
  const [inputValue, setInputValue] = useState('')
  const [searchValue, setSearchValue] = useState('')
  const [iniArr, setIniArr] = useState<{ id: string; upc: string; name: string }[]>([])
  const [searchArr, setSearchArr] = useState<{ id: string; upc: string; name: string }[]>([])
  const [scan, setScan] = useState(false)

  useKeyPress('Enter', () => {
    search()
  })

  const search = (value?: string) => {
    const finItem = upcList.find((item) => {
      if (item && item.id == value ? value : searchValue) {
        return item
      }
    })
    if (finItem) {
      addNewItemById(finItem ? finItem.id : '')
    }
    setInputValue('')
    setSearchValue('')
  }

  useEffect(() => {
    setIniArr(upcList)
    setSearchArr(upcList)
  }, [upcList])

  const getToday = () => {
    const today = new Date()
    return `${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()}`
  }

  const handleSearch = (value: string) => {
    const arr: { id: string; upc: string; name: string }[] = []
    iniArr.forEach((item) => {
      if (
        item !== undefined &&
        ((item.upc && item.upc.indexOf(value) != -1) ||
          (item.name && item.name.toLowerCase().indexOf(value.toLowerCase()) != -1))
      ) {
        arr.push(item)
      }
    })
    setInputValue(value)
    setSearchArr(arr)
  }

  const getInput = () => {
    if (scan) {
      return (
        <POSPageBodyInput
          onChange={(e) => {
            addNewItemByUpc(e.target.value)
          }}
        />
      )
    } else {
      return (
        <>
          <POSPageBodySelect
            showSearch
            value={searchValue}
            defaultActiveFirstOption={false}
            showArrow={false}
            filterOption={false}
            notFoundContent={null}
            style={{ width: '50%' }}
            onSearch={(e) => handleSearch(e)}
            onChange={(e: any) => setSearchValue(e)}
            onSelect={(e: any) => {
              console.log(e)
              setSearchValue(e)
              search(e)
            }}
            placeholder="Search to Select"
          >
            {searchArr ? (
              searchArr.map((item, index) => {
                if (item != undefined) {
                  return (
                    <Option key={index} value={item.id}>
                      {`${item.upc ? item.upc + ',' : ''}${item.name}`}
                    </Option>
                  )
                }
              })
            ) : (
              <></>
            )}
          </POSPageBodySelect>
          <POSPageBodyInputIcon>
            <Icon
              onClick={(e) => {
                search()
              }}
              viewBox="-7 -7 32 32"
              width={32}
              height={32}
              type="search"
            />
          </POSPageBodyInputIcon>
        </>
      )
    }
  }

  return (
    <div>
      <POSPageHeader>
        <p>Cash Customer</p>
        <Icon type="edit" viewBox="10 -10 25 25" width={50} height={25} />
      </POSPageHeader>
      <POSPageBody>
        {getInput()}
        <POSPageBodyInputSwitch>
          <Switch
            checkedChildren="Upc Scan"
            style={{ width: '120px' }}
            unCheckedChildren="Text search"
            defaultChecked={false}
            onChange={(e) => setScan(e)}
          />
        </POSPageBodyInputSwitch>
        <POSPageBodyDatePicker>
          <p>Sale Date</p>
          <DatePicker defaultValue={moment(getToday(), 'MM/DD/YY')} />
        </POSPageBodyDatePicker>
        <POSPageBodyOrder>
          <p>Order No.</p>
          <h6>{orderNo}</h6>
        </POSPageBodyOrder>
        <POSPageBodyOrder>
          <p>Order Total</p>
          <h6>
            {formatBn(parseFloat(totalValue), {
              comma: true,
              fixed: 2,
              $: true,
            })}
          </h6>
        </POSPageBodyOrder>
      </POSPageBody>
    </div>
  )
}

export default POSHeader
