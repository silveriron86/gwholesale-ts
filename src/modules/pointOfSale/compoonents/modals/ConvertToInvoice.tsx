import { Button, Select } from 'antd'
import _ from 'lodash'
import { i } from 'mathjs'
import React, { FC, useEffect, useState } from 'react'
import { ConvertInvoiceSelect, ConvertInvoiceTitle, QuantityModal } from '../../pointOfSale.style'
import { ClientObj } from '../POSBody'

interface ConvertInvoiceModalProps {
  sendClientId: (id: string) => void
  hideModal: () => void
  clientArr: ClientObj[]
}

const { Option } = Select

export const ConvertInvoiceModal: FC<ConvertInvoiceModalProps> = ({ sendClientId, hideModal, clientArr }) => {
  const _hideModal = () => {
    hideModal && hideModal()
  }

  const [clientArray, setClientArray] = useState<ClientObj[]>([])
  const [chooseId, setChooseId] = useState<string>('')

  useEffect(() => {
    setClientArray(clientArr ? clientArr : [])
  }, [clientArr])

  useEffect(() => {
    //console.log(clientArray)
  }, [clientArray])

  return (
    <QuantityModal
      title={`Convert to Invoice`}
      visible={true}
      width={700}
      style={{ padding: '0px' }}
      onCancel={_hideModal}
      footer={[
        <div style={{ textAlign: 'left' }}>
          <Button
            type={'primary'}
            onClick={() => {
              if (chooseId && chooseId != '') {
                sendClientId(chooseId)
              }
              _hideModal()
            }}
          >
            Convert to Invoice
          </Button>
          <Button onClick={_hideModal}>Cancel</Button>
        </div>,
      ]}
    >
      <ConvertInvoiceTitle>
        This sales receipt will be converted to an invoice and cannot be reverted.
      </ConvertInvoiceTitle>
      <ConvertInvoiceTitle>Customer Account</ConvertInvoiceTitle>
      <ConvertInvoiceSelect
        showSearch
        style={{ width: 200 }}
        onSelect={(e: any) => {
          setChooseId(e.toString())
        }}
        placeholder="Search to Select"
        optionFilterProp="children"
      >
        {clientArray ? (
          clientArray.map((item, index) => {
            return (
              <Option key={index} value={item.clientId}>
                {item.clientCompanyName}
              </Option>
            )
          })
        ) : (
          <></>
        )}
      </ConvertInvoiceSelect>
    </QuantityModal>
  )
}
