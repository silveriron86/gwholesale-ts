import { Button, Input, Select } from 'antd'
import _ from 'lodash'
import React, { FC, useState } from 'react'
import {
  PaymentAmount,
  PaymentAmountPaid,
  PaymentBody,
  PaymentChangeDue,
  PaymentInput,
  PaymentSelect,
  QuantityModal,
} from '../../pointOfSale.style'
import { format, evaluate } from 'mathjs'
import { Big, formatBn } from '~/common/mathjs'

const toStringFixedTwo = (value: string) => {
  if (value) {
    const stringValue = value.toString()
    const dotIndex = stringValue.indexOf('.')
    if (dotIndex > -1) {
      if (dotIndex == stringValue.length - 1) {
        return stringValue.substring(0, dotIndex) + '.'
      } else {
        const dotString = `0${stringValue.substring(dotIndex, stringValue.length)}`
        const fixedString = dotString.length > 4 ? parseFloat(dotString).toFixed(2) : dotString
        return fixedString !== '0'
          ? stringValue.substring(0, dotIndex) + '.' + fixedString.substring(2, fixedString.length)
          : stringValue.substring(0, dotIndex)
      }
    } else {
      return stringValue
    }
  } else {
    return ''
  }
}

interface PaymentModalProps {
  totalValue: string
  hideModal: () => void
  save: (type: string, note: string) => void
}

const { Option } = Select

export const PaymentModal: FC<PaymentModalProps> = ({ totalValue, hideModal, save }) => {
  const _hideModal = () => {
    hideModal && hideModal()
  }

  const [paymentType, setPaymentType] = useState<string>('creditCard')
  const [cashNum, setCashNum] = useState<string>('')
  const [note, setNote] = useState<string>('')

  const getChangeDue = () => {
    const num = format(Big.evaluate(`${parseFloat(cashNum) - parseFloat(totalValue)}`), {
      precision: 14,
    })
    return num.toString() == 'NaN'
      ? '$0.00'
      : formatBn(num, {
          comma: true,
          fixed: 2,
          $: true,
        })
  }

  return (
    <QuantityModal
      title={`Add Payment`}
      visible={true}
      width={810}
      style={{ padding: '0px' }}
      onCancel={_hideModal}
      footer={[
        <div style={{ textAlign: 'left' }}>
          <Button
            type={'primary'}
            onClick={() => {
              save(paymentType, note)
              _hideModal()
            }}
          >
            Apply Payment
          </Button>
          <Button onClick={_hideModal}>Cancel</Button>
        </div>,
      ]}
    >
      <PaymentBody>
        <PaymentSelect>
          <p>Payment Type</p>
          <Select value={paymentType} style={{ width: 120 }} onChange={(e: any) => setPaymentType(e)}>
            <Option value="creditCard">Credit Card</Option>
            <Option value="cash">Cash</Option>
          </Select>
        </PaymentSelect>
        <PaymentInput>
          <p>Note/Reference</p>
          <Input
            value={note}
            onChange={(e) => {
              setNote(e.target.value)
            }}
          />
        </PaymentInput>
        <PaymentAmount>
          <p>Amount Due</p>
          <h6>
            {formatBn(parseFloat(totalValue), {
              comma: true,
              fixed: 2,
              $: true,
            })}
          </h6>
        </PaymentAmount>
      </PaymentBody>
      {paymentType == 'cash' ? (
        <PaymentBody>
          <PaymentAmountPaid>
            <p>Amount Paid</p>
            <Input
              value={cashNum}
              onChange={(e) => {
                const num = toStringFixedTwo(e.target.value)
                setCashNum(num)
              }}
            />
          </PaymentAmountPaid>
          <PaymentChangeDue>
            <p>Change Due</p>
            <h6>{getChangeDue()}</h6>
          </PaymentChangeDue>
        </PaymentBody>
      ) : (
        <></>
      )}
    </QuantityModal>
  )
}
