import { Button, Input, InputNumber } from 'antd'
import _ from 'lodash'
import React, { FC, useEffect, useState } from 'react'
import { POSItem } from '../../pointOfSale.module'
import {
  PriceSummary,
  QuantityModal,
  QuantityModalHeader,
  QuantityModalText,
  UnitPriceDiv,
} from '../../pointOfSale.style'

interface PriceModalModalProps {
  posItem: POSItem | null
  hideModal: () => void
  savePrice: (posItem: POSItem) => void
}

const toStringFixedTwo = (value: string) => {
  if (value) {
    const stringValue = value.toString()
    const dotIndex = stringValue.indexOf('.')
    if (dotIndex > -1) {
      if (dotIndex == stringValue.length - 1) {
        return stringValue.substring(0, dotIndex) + '.'
      } else {
        const dotString = `0${stringValue.substring(dotIndex, stringValue.length)}`
        const fixedString = dotString.length > 4 ? parseFloat(dotString).toFixed(2) : dotString
        return fixedString !== '0'
          ? stringValue.substring(0, dotIndex) + '.' + fixedString.substring(2, fixedString.length)
          : stringValue.substring(0, dotIndex)
      }
    } else {
      return stringValue
    }
  } else {
    return ''
  }
}

export const PriceModal: FC<PriceModalModalProps> = ({ posItem, hideModal, savePrice }) => {
  const _hideModal = () => {
    hideModal && hideModal()
  }

  const [priceNum, setPriceNum] = useState<string>('')

  useEffect(() => {
    setPriceNum(posItem ? posItem.price.toString() : '')
  }, [posItem])

  useEffect(() => {
    //
  }, [priceNum])

  return (
    <QuantityModal
      title={`Price`}
      visible={true}
      width={700}
      style={{ padding: '0px' }}
      onCancel={_hideModal}
      footer={[
        <div style={{ textAlign: 'left' }}>
          <Button
            type={'primary'}
            onClick={() => {
              if (posItem) {
                const newObj = _.cloneDeep(posItem)
                newObj.price = parseFloat(priceNum)
                savePrice(newObj)
              }
              _hideModal()
            }}
          >
            Save
          </Button>
          <Button onClick={_hideModal}>Cancel</Button>
        </div>,
      ]}
    >
      <QuantityModalHeader>{posItem ? posItem.item : ''}</QuantityModalHeader>
      <QuantityModalText>Unit Pricing</QuantityModalText>
      <UnitPriceDiv>
        <Input
          value={priceNum}
          onChange={(e) => {
            const num = toStringFixedTwo(e.target.value)
            setPriceNum(num)
          }}
        />
        <p>{posItem ? posItem.priceUOM : ''}</p>
      </UnitPriceDiv>
      <QuantityModalText>Item Summary</QuantityModalText>
      <div>
        <PriceSummary>
          <div>
            <h6>Total OrderQty</h6>
            <p>{posItem ? posItem.orderQty : ''}</p>
          </div>
          <div>
            <h6>Total Billable Qty</h6>
            <p>{posItem ? posItem.orderQty : ''}</p>
          </div>
        </PriceSummary>
        <PriceSummary>
          <div>
            <h6>Total Item Tax</h6>
            <p>{posItem ? posItem.tax : ''}</p>
          </div>
          <div>
            <h6>Item Subtotal</h6>
            <p>{posItem ? (posItem.orderQty * parseFloat(priceNum) + posItem.tax).toFixed(2) : ''}</p>
          </div>
        </PriceSummary>
      </div>
    </QuantityModal>
  )
}
