import { Button } from 'antd'
import _ from 'lodash'
import React, { FC, useEffect, useState } from 'react'
import { POSItem } from '../../pointOfSale.module'
import {
  CenterDiv,
  CenterText,
  LineDiv,
  Logo,
  PrintAddress,
  PrintForm,
  PrintFormHeader,
  PrintItem,
  PrintName,
  PrintPrice,
  PrintQty,
  PrintTitle,
  PrintTotal,
  PrintTotalLeft,
  PrintTotalRight,
  QuantityModal,
} from '../../pointOfSale.style'
import { CompanyPrintInfo } from '../POSBody'
import Barcode from 'react-barcode'
import { ThemeButton } from '~/modules/customers/customers.style'
import { printWindow } from '~/common/utils'

interface PrintModalProps {
  hideModal: () => void
  companyInfo: CompanyPrintInfo | null
  orderId: string
  dataList: POSItem[]
  widthUOM: boolean
}

export const PrintModal: FC<PrintModalProps> = ({ hideModal, companyInfo, orderId, dataList, widthUOM }) => {
  const _hideModal = () => {
    hideModal && hideModal()
  }

  const [companyInformation, setCompanyInformation] = useState(companyInfo)
  const [receiptId, setReceiptId] = useState(orderId)
  const [isUom, setIsUOM] = useState(false)

  useEffect(() => {
    setCompanyInformation(companyInformation)
  }, [companyInfo])

  useEffect(() => {
    setIsUOM(widthUOM)
  }, [widthUOM])

  useEffect(() => {
    setReceiptId(orderId)
  }, [orderId])

  const formatAMPM = () => {
    const date = new Date()
    let hours = date.getHours()
    let minutes: any = date.getMinutes()
    let ampm = hours >= 12 ? 'pm' : 'am'
    hours = hours % 12
    hours = hours ? hours : 12
    minutes = minutes < 10 ? '0' + minutes : minutes
    var strTime = hours + ':' + minutes + ' ' + ampm
    return strTime
  }

  const getTime = () => {
    const time = new Date()
    return `${time.getMonth() + 1}/${time.getDate()}/${time.getFullYear()} ${formatAMPM()}`
  }

  const getTotalTax = () => {
    if (dataList.length > 0) {
      const arr = dataList.map((item) => item.tax)
      return arr.reduce((accumulator, currentValue) => {
        return accumulator + currentValue
      })
    } else {
      return 0
    }
  }

  const getSubtotal = () => {
    if (dataList.length > 0) {
      const arr = dataList.map((item) => item['orderQty'] * item['price'])
      return arr.reduce((accumulator, currentValue) => {
        return accumulator + currentValue
      })
    } else {
      return 0
    }
  }

  return (
    <QuantityModal
      title={
        <ThemeButton
          style={{ marginLeft: '160px' }}
          onClick={() => {
            printWindow(`SalesReceiptLabel`)
            _hideModal()
          }}
        >
          Print
        </ThemeButton>
      }
      visible={true}
      width={302}
      style={{ padding: '0px' }}
      onCancel={_hideModal}
      footer={[]}
    >
      <div id={`SalesReceiptLabel`}>
        <Logo
          src={`
              ${process.env.AWS_PUBLIC}/${companyInformation ? companyInformation.imagePath : ''}`}
        />
        <PrintTitle>
          <p>{getTime()}</p>
          <h6>Receipt #{receiptId}</h6>
        </PrintTitle>
        <PrintAddress>
          <h6>{companyInfo ? companyInfo.name : ''}</h6>
          <p>{companyInfo ? companyInfo.address1 : ''}</p>
          <p>{companyInfo ? companyInfo.address2 : ''}</p>
          <p>{companyInfo ? companyInfo.phone : ''}</p>
        </PrintAddress>
        <PrintFormHeader>
          <PrintItem>
            <h6>Item</h6>
          </PrintItem>
          <PrintQty>
            <h6>Qty</h6>
          </PrintQty>
          <PrintPrice>
            <h6>Price</h6>
          </PrintPrice>
        </PrintFormHeader>
        {dataList.map((item) => {
          return (
            <>
              {isUom == true ? <PrintName>{item.item}</PrintName> : <></>}
              <PrintForm>
                <PrintItem>
                  <p>{widthUOM == true ? `${item.orderQty} ${item.orderQtyUOM}` : `${item.item}`}</p>
                </PrintItem>
                <PrintQty>
                  <p>{item.orderQty}</p>
                </PrintQty>
                <PrintPrice>
                  <p>${item.price}</p>
                </PrintPrice>
              </PrintForm>
            </>
          )
        })}
        <LineDiv></LineDiv>
        <PrintTotal>
          <PrintTotalLeft>
            <p>Subtotal</p>
          </PrintTotalLeft>
          <PrintTotalRight>
            <p>${getSubtotal()}</p>
          </PrintTotalRight>
        </PrintTotal>
        <PrintTotal>
          <PrintTotalLeft>
            <p>Tax</p>
          </PrintTotalLeft>
          <PrintTotalRight>
            <p>${getTotalTax()}</p>
          </PrintTotalRight>
        </PrintTotal>
        <PrintTotal>
          <PrintTotalLeft>
            <h6>Total</h6>
          </PrintTotalLeft>
          <PrintTotalRight>
            <h6>${getSubtotal() + getTotalTax()}</h6>
          </PrintTotalRight>
        </PrintTotal>
        <CenterText>Thank you for your business</CenterText>
        <CenterDiv>
          <Barcode value={receiptId} displayValue={false} width={2} height={30} />
          <p>{receiptId}</p>
        </CenterDiv>
      </div>
    </QuantityModal>
  )
}
