import { Button, Input, Modal } from 'antd'
import _ from 'lodash'
import React, { FC, useEffect, useState } from 'react'
import { POSItem } from '../../pointOfSale.module'
import {
  ChooseUOM,
  QuantityModal,
  QuantityModalHeader,
  QuantityModalInput,
  QuantityModalQuantity,
  QuantityModalQuantityMinus,
  QuantityModalQuantityPlus,
  QuantityModalText,
  QuantityModalUOM,
} from '../../pointOfSale.style'

interface QtyModalModalProps {
  posItem: POSItem | null
  hideModal: () => void
  saveQtyAndUOM: (posItem: POSItem) => void
}

const toStringFixedTwo = (value: string) => {
  if (value) {
    const stringValue = value.toString()
    const dotIndex = stringValue.indexOf('.')
    if (dotIndex > -1) {
      if (dotIndex == stringValue.length - 1) {
        return stringValue.substring(0, dotIndex) + '.'
      } else {
        const dotString = `0${stringValue.substring(dotIndex, stringValue.length)}`
        const fixedString = dotString.length > 4 ? parseFloat(dotString).toFixed(2) : dotString
        return fixedString !== '0'
          ? stringValue.substring(0, dotIndex) + '.' + fixedString.substring(2, fixedString.length)
          : stringValue.substring(0, dotIndex)
      }
    } else {
      return stringValue
    }
  } else {
    return ''
  }
}

export const QtyModal: FC<QtyModalModalProps> = ({ posItem, hideModal, saveQtyAndUOM }) => {
  const _hideModal = () => {
    hideModal && hideModal()
  }

  const [quantityNum, setQuantityNum] = useState<string>('')
  const [chooseUOM, setChooseUOM] = useState<string>('')

  useEffect(() => {
    setQuantityNum(posItem ? posItem.orderQty.toString() : '')
    setChooseUOM(posItem ? posItem.orderQtyUOM : '')
  }, [posItem])

  useEffect(() => {
    //
  }, [chooseUOM, quantityNum])

  const getUomButtons = () => {
    const arr1: string[] = []
    const arr2: string[] = []
    const arr3: string[] = []
    if (posItem) {
      posItem.uomList.forEach((item, index) => {
        if (index == 0 || index % 3 == 0) {
          arr1.push(item)
        }
        if (index == 1 || index % 3 == 1) {
          arr2.push(item)
        }
        if (index == 2 || index % 3 == 2) {
          arr3.push(item)
        }
      })
    }
    return (
      <>
        <div style={{ display: 'flex', width: '100%' }}>
          <div style={{ width: '33%' }}>
            {arr1.map((item) => {
              return chooseUOM == item ? (
                <>
                  <ChooseUOM style={{ marginBottom: '10px' }}>{item}</ChooseUOM>
                </>
              ) : (
                <>
                  <Button style={{ marginBottom: '10px' }} onClick={(e) => setChooseUOM(item)}>
                    {item}
                  </Button>
                </>
              )
            })}
          </div>
          <div style={{ width: '33%' }}>
            {arr2.map((item) => {
              return chooseUOM == item ? (
                <>
                  <ChooseUOM style={{ marginBottom: '10px' }}>{item}</ChooseUOM>
                </>
              ) : (
                <>
                  <Button style={{ marginBottom: '10px' }} onClick={(e) => setChooseUOM(item)}>
                    {item}
                  </Button>
                </>
              )
            })}
          </div>
          <div style={{ width: '33%' }}>
            {arr3.map((item) => {
              return chooseUOM == item ? (
                <>
                  <ChooseUOM style={{ marginBottom: '10px' }}>{item}</ChooseUOM>
                </>
              ) : (
                <>
                  <Button style={{ marginBottom: '10px' }} onClick={(e) => setChooseUOM(item)}>
                    {item}
                  </Button>
                </>
              )
            })}
          </div>
        </div>
      </>
    )
  }

  return (
    <QuantityModal
      title={`Quantity and Unit of Measure`}
      visible={true}
      width={700}
      style={{ padding: '0px' }}
      onCancel={_hideModal}
      footer={[
        <div style={{ textAlign: 'left' }}>
          <Button
            type={'primary'}
            onClick={() => {
              if (posItem) {
                const newObj = _.cloneDeep(posItem)
                newObj.orderQty = parseFloat(quantityNum)
                newObj.orderQtyUOM = chooseUOM
                newObj.priceUOM = chooseUOM
                saveQtyAndUOM(newObj)
              }
              _hideModal()
            }}
          >
            Save
          </Button>
          <Button onClick={_hideModal}>Cancel</Button>
        </div>,
      ]}
    >
      <QuantityModalHeader>{posItem ? posItem.item : ''}</QuantityModalHeader>
      <QuantityModalText>Quantity</QuantityModalText>
      <QuantityModalQuantity>
        <QuantityModalQuantityPlus
          onClick={(e) => {
            const num = parseFloat(quantityNum) + 1
            setQuantityNum(num.toString())
          }}
        >
          <p>+</p>
        </QuantityModalQuantityPlus>
        <QuantityModalInput
          value={quantityNum}
          onChange={(e) => {
            const num = toStringFixedTwo(e.target.value)
            setQuantityNum(num)
          }}
        />
        <QuantityModalQuantityMinus
          onClick={(e) => {
            const num = parseFloat(quantityNum) - 1
            setQuantityNum(num.toString())
          }}
        >
          <p>-</p>
        </QuantityModalQuantityMinus>
      </QuantityModalQuantity>
      <QuantityModalText>Unit of Measure</QuantityModalText>
      <QuantityModalUOM>{getUomButtons()}</QuantityModalUOM>
    </QuantityModal>
  )
}
