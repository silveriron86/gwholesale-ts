import { Button } from 'antd'
import _ from 'lodash'
import React, { FC } from 'react'
import { POSItem } from '../../pointOfSale.module'
import { QuantityModal, QuantityModalHeader } from '../../pointOfSale.style'

interface RemoveModalProps {
  posItem: POSItem | null
  hideModal: () => void
  removeItem: (posItem: POSItem) => void
}

export const RemoveModal: FC<RemoveModalProps> = ({ posItem, hideModal, removeItem }) => {
  const _hideModal = () => {
    hideModal && hideModal()
  }

  return (
    <QuantityModal
      title={`Confirm Remove Item`}
      visible={true}
      width={700}
      style={{ padding: '0px' }}
      onCancel={_hideModal}
      footer={[
        <div style={{ textAlign: 'left' }}>
          <Button
            type={'primary'}
            onClick={() => {
              if (posItem) {
                removeItem(posItem)
              }
              _hideModal()
            }}
          >
            Remove Item
          </Button>
          <Button onClick={_hideModal}>Keep Item</Button>
        </div>,
      ]}
    >
      <QuantityModalHeader>
        {`Are you sure you want to remove `}
        <span style={{ fontWeight: 'bold' }}>{posItem ? posItem.item : ''}</span>
        {` from the order`}
      </QuantityModalHeader>
    </QuantityModal>
  )
}
