import React from 'react'
import { OrderHeaderProps } from '~/modules/orders/components'
import { PointOfSaleDispatchProps, PointOfSaleModule, PointOfSaleStateProps, POSItem } from './pointOfSale.module'
import { RouteComponentProps } from 'react-router'
import { AuthUser } from '~/schema'
import { GlobalState } from '~/store/reducer'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import PageLayout from '~/components/PageLayout'
import { POSPageContainer } from './pointOfSale.style'
import POSHeader from './compoonents/POSHeader'
import { OrderService } from '../orders/order.service'
import { PointOfSaleService, POSCreateObj } from './pointOfSale.service'
import { of } from 'rxjs'
import { checkError, responseHandler } from '~/common/utils'
import POSBody, { CompanyPrintInfo } from './compoonents/POSBody'
import _ from 'lodash'
import { SettingService } from '../setting/setting.service'
import { InventoryService } from '../inventory/inventory.service'

type PointOfSaleContainerProps = OrderHeaderProps &
  PointOfSaleDispatchProps &
  PointOfSaleStateProps &
  RouteComponentProps & {
    currentUser: AuthUser
  }

interface IState {
  qtyAndUOMModalVisible: boolean
  removeModalVisible: boolean
  priceModalVisible: boolean
  paymentModalVisible: boolean
  listData: POSItem | null
  orderNo: string
  totalValue: string
  companyInfo: CompanyPrintInfo | null
  upcList: any[]
  clientId: string
  productList: any[]
}

class PointOfSaleContainer extends React.PureComponent<PointOfSaleContainerProps, IState> {
  state = {
    qtyAndUOMModalVisible: false,
    removeModalVisible: false,
    priceModalVisible: false,
    paymentModalVisible: false,
    listData: null,
    orderNo: '',
    totalValue: '',
    companyInfo: null,
    upcList: [],
    clientId: '',
    productList: [],
  }

  constructor(props: PointOfSaleContainerProps) {
    super(props)
  }

  componentDidMount() {
    this.handleCreatePointOfSale()
  }

  componentWillUnmount() {
    localStorage.setItem('posItemId', this.state.orderNo)
  }

  private handleCreatePointOfSale = () => {
    const { currentUser } = this.props

    this.setCompanyImagePath(currentUser)

    const that = this
    OrderService.instance.getCashCustomerClient({ userId: currentUser.userId }).subscribe({
      next(res: any) {
        of(responseHandler(res, true).body.data)
        //set company information
        const bodyData = res.body.data
        that.setCompanyAddress(bodyData, that)

        //set all products
        that.setProducts(bodyData.wholesaleCompany.companyName, that)

        //create new order

        const today = new Date()
        const data: POSCreateObj = {
          wholesaleClientId: res.body.data.clientId as string,
          orderDate: `${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()}`,
          quotedDate: `${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()}`,
          deliveryDate: `${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()}`,
          itemList: [],
          pos: true,
          userId: currentUser.userId,
        }

        that.setState({
          clientId: res.body.data.clientId as string,
        })

        const posObj = localStorage.getItem('posItemId')
        if (posObj !== null && posObj !== 'null') {
          that.setState({
            orderNo: posObj,
          })
        } else {
          PointOfSaleService.instance.CreateOrder(data).subscribe({
            next(res: any) {
              of(responseHandler(res, false).body.data)
              that.setState({
                orderNo: res.body.data.wholesaleOrderId,
              })
            },
            error(err) {
              checkError(err)
            },
          })
        }
      },
    })
  }

  CancelOrder = () => {
    const today = new Date()
    const body = {
      wholesaleOrderId: this.state.orderNo,
      deliveryDate: `${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()}`,
      wholesaleCustomerClientId: this.state.clientId,
      status: 'CANCEL',
      pos: true,
    }
    PointOfSaleService.instance.CancelOrder(body).subscribe({
      next(res: any) {
        of(responseHandler(res, true).body.data)
        if (res.statusCodeValue == 200) {
          //that.handleCreatePointOfSale()
        }
      },
      error(err) {
        checkError(err)
      },
    })
  }

  setCompanyImagePath = (currentUser: any) => {
    const that = this
    SettingService.instance.getUserSetting(currentUser.userId).subscribe({
      next(res: any) {
        const companyInfo: CompanyPrintInfo = {
          imagePath: res.userSetting.imagePath,
          name: '',
          address1: '',
          address2: '',
          phone: '',
        }
        that.setState({
          companyInfo: companyInfo,
        })
      },
      error(err) {
        checkError(err)
      },
    })
  }

  setCompanyAddress = (bodyData: any, that: any) => {
    const address = bodyData.wholesaleCompany.mainShippingAddress.address
    const companyInfo: CompanyPrintInfo = {
      imagePath: that.state.companyInfo ? (that.state.companyInfo as CompanyPrintInfo).imagePath : '',
      name: bodyData.wholesaleCompany.companyName,
      address1: `${address.street1} ${address.street2}`,
      address2: `${address.city},${address.state},${address.country},${address.zipcode} `,
      phone: address.phone,
    }
    that.setState({
      companyInfo: companyInfo,
    })
  }

  setProducts = (companyName: string, that: any) => {
    OrderService.instance.getSimplifyItems(companyName, 'SELL').subscribe({
      next(res: any) {
        if (res.statusCodeValue == 200) {
          const _upcList: any[] = []
          res.body.data.map((item: any) => {
            if (item.upc !== null || (item.variety !== '' && item.variety !== null)) {
              _upcList.push({
                id: item.itemId,
                upc: item.upc,
                name: item.variety,
              })
            }
          })
          that.setState({
            upcList: _upcList,
          })
          that.setState({
            productList: res.body.data,
          })
        }
      },
      error(err) {
        checkError(err)
      },
    })
  }

  addItemByUpc = (upcCode: string) => {
    const that = this
    PointOfSaleService.instance.GetPOSItemUpc(upcCode).subscribe({
      next(res: any) {
        if (res.statusCodeValue == 200) {
          const _uomList = res.body.data[0].uomList.map((item: any) => item.name)
          _uomList.push(res.body.data[0].uom)
          const newObj: POSItem = {
            itemId: res.body.data[0].itemId,
            item: res.body.data[0].itemName ? res.body.data[0].itemName : '',
            orderQty: 1,
            orderQtyUOM: res.body.data[0].uom,
            price: res.body.data[0].price,
            priceUOM: res.body.data[0].priceUOM,
            tax: 0,
            uomList: _uomList,
            uomChange: false,
            priceChange: false,
          }
          that.setState({
            listData: newObj,
          })
        }
      },
      error(err) {
        checkError(err)
      },
    })
  }

  addItemById = (id: string) => {
    const that = this
    PointOfSaleService.instance.GetPOSItemById(id).subscribe({
      next(res: any) {
        if (res.statusCodeValue == 200) {
          const _uomList = res.body.data[0].uomList.map((item: any) => item.name)
          _uomList.push(res.body.data[0].uom)
          const newObj: POSItem = {
            itemId: res.body.data[0].itemId,
            item: res.body.data[0].itemName ? res.body.data[0].itemName : '',
            orderQty: 1,
            orderQtyUOM: res.body.data[0].uom,
            price: res.body.data[0].price,
            priceUOM: res.body.data[0].priceUOM,
            tax: 0,
            uomList: _uomList,
            uomChange: false,
            priceChange: false,
          }
          that.setState({
            listData: newObj,
          })
        }
      },
      error(err) {
        checkError(err)
      },
    })
  }

  render() {
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-POS'}>
        <POSPageContainer>
          <POSHeader
            addNewItemByUpc={(upcCode) => this.addItemByUpc(upcCode)}
            addNewItemById={(id) => this.addItemById(id)}
            totalValue={this.state.totalValue}
            orderNo={this.state.orderNo}
            upcList={this.state.upcList}
          />
        </POSPageContainer>
        <POSBody
          orderId={this.state.orderNo}
          item={this.state.listData}
          setTotalValue={(v) =>
            this.setState({
              totalValue: v,
            })
          }
          resetList={() => {
            this.setState({
              totalValue: '',
              listData: null,
            })
            this.handleCreatePointOfSale()
            localStorage.setItem('posItemId', 'null')
          }}
          companyInfo={this.state.companyInfo}
          cancelOrder={() => {
            this.CancelOrder()
          }}
        />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => ({ ...state.pointOfSale, currentUser: state.currentUser })

export const PointOfSale = withTheme(connect(PointOfSaleModule)(mapStateToProps)(PointOfSaleContainer))
