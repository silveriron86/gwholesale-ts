import { Action } from 'redux-actions'
import { DefineAction, Effect, EffectModule, Module, ModuleDispatchProps } from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { responseHandler } from '~/common/utils'

import { PointOfSaleService } from './pointOfSale.service'
import { map, switchMap } from 'rxjs/operators'

export interface POSQty {
  quantity: number
  uom: string
}

export interface POSItem {
  itemId: string
  item: string
  orderQty: number
  orderQtyUOM: string
  price: number
  priceUOM: string
  tax: number
  uomList: string[]
  uomChange: boolean
  priceChange: boolean
}

export interface PointOfSaleStateProps {
  dataSource: POSItem[]
  loading: boolean
}

@Module('pointOfSale')
export class PointOfSaleModule extends EffectModule<PointOfSaleStateProps> {
  defaultState = {
    dataSource: [],
    loading: false,
  }

  @DefineAction() dispose$!: Observable<void>
  @DefineAction() disposeEditOrder$!: Observable<void>

  constructor(private readonly pointOfSale: PointOfSaleService) {
    super()
  }

  @Effect({
    done: (
      state: PointOfSaleStateProps,
      { payload }: Action<{ dataList: POSItem[]; total: number }>,
    ): PointOfSaleStateProps => {
      return {
        ...state,
        dataSource: payload?.dataList,
        loading: false,
      }
    },
  })
  getPOSItems(action$: Observable<string>) {
    return action$.pipe(
      switchMap((data) =>
        this.pointOfSale.GetPOSItem(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
    )
  }
}

export type PointOfSaleDispatchProps = ModuleDispatchProps<PointOfSaleModule>
