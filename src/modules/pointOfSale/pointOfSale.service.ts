import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import { POSItem } from './pointOfSale.module'

export interface POSSendItem {
  wholesaleItemId: number
  displayOrder: number
  quantity: number
  uomId: number
  price: number
}

export interface POSCreateObj {
  wholesaleClientId: string
  deliveryDate: string
  orderDate: string
  quotedDate: string
  itemList: POSSendItem[]
  pos: boolean
  userId: string
}

export interface POSCancelObj {
  wholesaleOrderId: string
  deliveryDate: string
  wholesaleCustomerClientId: string
  status: string
  pos: boolean
}

@Injectable()
export class PointOfSaleService {
  constructor(private readonly http: Http) {}

  static get instance() {
    return new PointOfSaleService(new Http())
  }

  GetPOSItemUpc(upc: string) {
    return this.http.get<{ dataList: POSItem[]; total: number }>(`/inventory/item?upc_code=${upc}`)
  }

  GetPOSItemById(itemId: string) {
    return this.http.get<{ dataList: POSItem[]; total: number }>(`/inventory/item?item_id=${itemId}`)
  }

  AddPayment(params: object) {
    //return this.http.get<{ dataList: RestockItem[]; total: number }>(`/inventory/item-stock?${search.toString()}`)
  }

  ConvertToInvoice(orderId: string, clientId: string, items: any[]) {
    return this.http.put(`/account/${orderId}/pos/convert-to-invoice/${clientId}`, {
      body: JSON.stringify({
        itemList: items,
      }),
    })
  }

  SaveAndPrint(orderId: string, obj: any) {
    return this.http.post(`/account/${orderId}/pos/items`, {
      body: JSON.stringify(obj),
    })
  }

  CreateOrder(item: POSCreateObj) {
    return this.http.post(`/inventory/create/orders`, {
      body: JSON.stringify(item),
    })
  }

  CancelOrder(item: POSCancelObj) {
    return this.http.post(`/inventory/updateOrderInfo`, {
      body: JSON.stringify(item),
    })
  }

  GetTax(orderId: string, itemId: string, price: string) {
    return this.http.get(`/inventory/calculate-tax?order_id=${orderId}&item_id=${itemId}&price=${price}`)
  }
}
