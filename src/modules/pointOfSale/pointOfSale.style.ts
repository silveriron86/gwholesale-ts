import styled from '@emotion/styled'
import { Button, Input, Modal, Select } from 'antd'

/**
 * container
 */
export const POSPageContainer = styled.div`
  width: 100%;
  text-align: left;
  box-sizing: border-box;
  padding: 5px 70px;
`

/**
 * header
 */
export const POSPageHeader = styled.div`
  width: 100%;
  display: flex;
  p {
    width: 258px;
    height: 50px;
    font-style: normal;
    font-weight: 400;
    font-size: 36px;
    line-height: 50px;
    color: #1c6e31;
    margin-bottom: 0px;
  }
  svg {
    width: 7%;
    height: 7%;
    margin-left: 57px;
  }
`

export const POSPageBody = styled.div`
  width: 100%;
  display: flex;
  height: 87px;
`

export const POSPageBodySelect = styled(Select)`
  width: 40%;
  height: 36px;
  margin-top: 51px;
  padding: 0px;
`

export const POSPageBodyInput = styled(Input)`
  width: 40%;
  height: 36px;
  margin-top: 51px;
  padding: 0px;
`

export const POSPageBodyInputIcon = styled.div`
  margin-top: 51px;
  padding: 0px;
  height: 32px;
  width: 32px;
  border: 1px solid #a9adaa;
  cursor: pointer;
`

export const POSPageBodyInputSwitch = styled.div`
  margin-top: 51px;
  margin-left: 4px;
  margin-right: 43px;
`

export const POSPageBodyDatePicker = styled.div`
  width: 200px;
  height: 36px;
  margin-top: 19px;
  p {
    height: 20px;
    line-height: 20px;
    font-size: 14px;
    margin-bottom: 12px;
  }
`

export const POSPageBodyOrder = styled.div`
  width: 150px;
  height: 36px;
  margin-top: 19px;
  margin-left: 77px;
  p {
    height: 20px;
    line-height: 20px;
    font-size: 14px;
    margin-bottom: 12px;
  }
  h6 {
    height: 36px;
    font-weight: 250;
    font-size: 36px;
    line-height: 36px;
    margin-bottom: 0px;
  }
`

/**
 * body
 */
export const TotalBody = styled.div`
  position: absolute;
  margin-top: 37px;
  right: 66px;
`

export const TotalDiv = styled.div`
  display: flex;
  width: 404px;
  height: 36px;
  h6 {
    width: 150px;
    text-align: right;
    line-height: 36px;
    font-size: 18px;
    margin-bottom: 0px;
  }
  p {
    width: 254px;
    text-align: right;
    line-height: 36px;
    font-size: 18px;
    margin-bottom: 0px;
  }
`

export const Total = styled.div`
  position: absolute;
  right: 66px;
  display: flex;
  width: 404px;
  margin-top: 150px;
  height: 36px;
  h6 {
    width: 150px;
    text-align: right;
    line-height: 36px;
    font-size: 36px;
    font-weight: 600;
    margin-bottom: 0px;
  }
  p {
    width: 254px;
    text-align: right;
    line-height: 36px;
    font-size: 36px;
    font-weight: 600;
    margin-bottom: 0px;
  }
`

export const AmountDue = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  margin-top: 200px;
  background-color: #f7f7f7;
  height: 36px;
  h6 {
    position: absolute;
    right: 320px;
    width: 300px;
    text-align: right;
    font-size: 36px;
    font-weight: 600;
    margin-bottom: 0px;
    line-height: 36px;
    color: #1c6e31;
  }
  p {
    position: absolute;
    right: 66px;
    width: 254px;
    text-align: right;
    font-size: 36px;
    font-weight: 600;
    margin-bottom: 0px;
    line-height: 36px;
    height: 36px;
    color: #1c6e31;
  }
`

export const AmountPaymentButton = styled(Button)`
  position: absolute;
  left: 0;
  width: 126px;
  text-align: right;
  margin-left: 24px;
`

export const AmountPaymentText = styled.p`
  position: absolute;
  left: 160px;
  width: 647px;
  text-align: left !important;
  font-size: 24px !important;
  font-weight: normal !important;
  margin-bottom: 0px !important;
  line-height: 24px !important;
  height: 36px !important;
  color: black !important;
`

export const TableEle = styled.div`
  display: inline;
  p {
    margin-top: 16px;
    text-align: left;
    margin-bottom: 0px;
    height: 50px;
    line-height: 50px;
  }
  button {
    border: 1px solid #1c6e31;
    color: #1c6e31;
    text-align: center;
    pxmargin-bottom: 10px;
  }
`

export const TableEleNoButton = styled.div`
  display: inline;
  p {
    text-align: left;
    margin-bottom: 0px;
    height: 50px;
    line-height: 50px;
    margin-top: -15px;
  }
`

export const BottomButtons = styled.div`
  position: absolute;
  right: 0px;
  display: flex;
  width: 809px;
  margin-top: 34px;
  button {
    margin-left: 65px;
  }
`
/**
 * quantity modal
 */

export const QuantityModal = styled(Modal)`
  .ant-modal-body {
    padding: 0px;
  }
`

export const QuantityModalHeader = styled.h6`
  padding: 16px 24px;
  font-size: 24px;
  line-height: 33px;
  margin-bottom: 0px;
  background-color: #f7f7f7;
`

export const QuantityModalQuantity = styled.div`
  display: flex;
  height: 110px;
  border-bottom: 1px solid #d8dbdb;
`

export const QuantityModalQuantityPlus = styled.div`
  background-color: #1c6e31;
  width: 82px;
  height: 59px;
  margin-left: 96px;
  margin-top: 8px;
  cursor: pointer;
  p {
    width: 82px;
    height: 59px;
    color: white;
    line-height: 59px;
    text-align: center;
    font-size: 36px;
  }
`

export const QuantityModalInput = styled(Input)`
  width: 156px;
  height: 75px;
  margin-left: 51px;
  margin-right: 51px;
  font-size: 36px;
`

export const QuantityModalQuantityMinus = styled.div`
  background-color: #1c6e31;
  width: 82px;
  height: 59px;
  margin-top: 8px;
  cursor: pointer;
  p {
    width: 82px;
    height: 59px;
    color: white;
    line-height: 59px;
    text-align: center;
    font-size: 36px;
  }
`

export const QuantityModalUOM = styled.div`
  display: flex;
  button {
    width: 166px;
    height: 59px;
    margin-left: 24px;
    margin-right: 24px;
    background-color: #198333;
    color: white;
  }
  height: auto;
`

export const QuantityModalText = styled.p`
  margin-left: 24px;
`

export const ChooseUOM = styled(Button)`
  border: 3px solid #ff715e;
`

/**
 * price modal
 */

export const UnitPriceDiv = styled.div`
  display: flex;
  border-bottom: 1px solid #d8dbdb;
  input {
    width: 236px;
    margin-left: 40px;
    height: 50px;
    font-size: 36px;
  }
  p {
    margin-left: 40px;
    height: 50px;
    font-weight: 400;
    font-size: 36px;
    line-height: 50px;
  }
`

export const PriceSummary = styled.div`
  display: flex;
  margin-left: 40px;
  div {
    width: 50%;
  }
  h6 {
    font-weight: 400;
    font-size: 18px;
    height: 25px;
    line-height: 25px;
  }
  p {
    font-weight: 300;
    font-size: 36px;
    height: 41px;
    line-height: 41px;
  }
`

/**
 * payment modal
 */

export const PaymentBody = styled.div`
  display: flex;
  margin-top: 33px;
  p {
    margin-bottom: 0px;
  }
  h6 {
    font-size: 32px;
  }
`

export const PaymentSelect = styled.div`
  width: 159px;
  margin-left: 24px;
  .ant-select,
  .ant-select-enabled {
    margin-top: 10px;
  }
`

export const PaymentInput = styled.div`
  width: 327px;
  margin-left: 58px;
  input {
    margin-top: 10px;
  }
`

export const PaymentAmount = styled.div`
  width: 177px;
  margin-left: 43px;
`

export const PaymentAmountPaid = styled.div`
  width: 217px;
  margin-left: 24px;
  input {
    margin-top: 10px;
  }
`

export const PaymentChangeDue = styled.div`
  width: 217px;
  margin-left: 43px;
`

/**
 * convert invoice modal
 */

export const ConvertInvoiceTitle = styled.p`
  width: 100%;
  margin-left: 24px;
  margin-top: 19px;
  font-size: 14px;
  line-height: 20px;
  height: 20px;
`

export const ConvertInvoiceSelect = styled(Select)`
  width: 100%;
  margin-left: 24px;
  margin-bottom: 36px;
`

/**
 * print modal
 */

export const Logo = styled.img`
  width: 173px;
  height: 100px;
  margin-left: 65px;
  margin-top: 18px;
`

export const PrintTitle = styled.div`
  display: flex;
  margin-left: 15px;
  margin-top: 15px;
  p {
    width: 50%;
    height: 18px;
    font-size: 14px;
    line-height: 18px;
    margin-bottom: 0px;
  }
  h6 {
    width: 50%;
    height: 18px;
    font-size: 14px;
    line-height: 18px;
    font-weight: bold;
    margin-bottom: 0px;
  }
`

export const PrintAddress = styled.div`
  margin-top: 15px;
  p {
    width: 100%;
    font-size: 14px;
    line-height: 18px;
    text-align: center;
    margin-bottom: 0px;
  }
  h6 {
    width: 100%;
    font-size: 14px;
    line-height: 18px;
    text-align: center;
    font-weight: bold;
    margin-bottom: 0px;
  }
`

export const PrintFormHeader = styled.div`
  margin-top: 15px;
  display: flex;
  border-bottom: 1px solid black;
  margin-left: 15px;
  margin-bottom: 15px;
`

export const PrintName = styled.p`
  font-size: 14px;
  margin-bottom: 0px;
  font-weight: normal;
  line-height: 20px;
  margin-left: 15px;
`

export const PrintItem = styled.div`
  width: 137px;
  h6 {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: bold;
    line-height: 20px;
  }
  p {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: normal;
    line-height: 20px;
  }
`

export const PrintQty = styled.div`
  width: 25%;
  height: 20px;
  h6 {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: bold;
    line-height: 20px;
  }
  p {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: normal;
    line-height: 20px;
  }
`

export const PrintPrice = styled.div`
  width: 25%;
  height: 20px;
  h6 {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: bold;
    line-height: 20px;
  }
  p {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: normal;
    line-height: 20px;
    text-align: right;
  }
`

export const PrintForm = styled.div`
  margin-top: 2px;
  display: flex;
  margin-left: 15px;
  margin-right: 15px;
`

export const PrintTotal = styled.div`
  display: flex;
`

export const PrintTotalLeft = styled.div`
  width: 50%;
  p {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: normal;
    line-height: 20px;
    text-align: right;
  }
  h6 {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: bold;
    line-height: 20px;
    text-align: right;
  }
`

export const PrintTotalRight = styled.div`
  width: 50%;
  margin-right: 15px;
  p {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: normal;
    line-height: 20px;
    text-align: right;
  }
  h6 {
    font-size: 14px;
    margin-bottom: 0px;
    font-weight: bold;
    line-height: 20px;
    text-align: right;
  }
`

export const LineDiv = styled.div`
  border-bottom: 1px solid black;
  margin-top: 15px;
`

export const CenterText = styled.p`
  font-size: 14px;
  margin-bottom: 0px;
  text-align: center;
  margin-top: 15px;
`

export const CenterDiv = styled.div`
  width: 100%;
  text-align: center;
`
