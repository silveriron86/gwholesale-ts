/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Dropdown, Icon, Row, Menu } from 'antd'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { CustomerPriceSheet, TempCustomer } from '~/schema'
import { Icon as IconSvg } from '~/components/icon'
import { DropdownText, ListItemRow } from '~/modules/pricing/pricing.style'

interface Props {
  isHeader: boolean
  currentPriceSheet?: CustomerPriceSheet
  sortByKeyAndDirection?: Function
  defaultUrl?: string
  history: any
}

export class CustomerPriceSheetListRow extends React.PureComponent<Props> {
  state = {
    isMenuOpen: false,
    sortKey: 'priceSheetId',
    sortDirection: 'asc',
  }

  onMenuVisibleChange = (visible: any) => {
    console.log(visible)
  }

  renderCustomers = () => {
    const { currentPriceSheet } = this.props
    const { isMenuOpen } = this.state

    if (!currentPriceSheet || !currentPriceSheet.assignedClients) return []
    const assignedClients: TempCustomer[] = currentPriceSheet.assignedClients
    let result: any[] = []
    assignedClients.forEach((el, index) => {
      result.push(
        <Menu.Item key={index}>
          <div>{el.clientCompany.companyName}</div>
        </Menu.Item>,
      )
    })
    let dropdown: any = []
    if (result.length > 0) {
      const menu = <Menu>{result}</Menu>
      dropdown = (
        <Dropdown overlay={menu} trigger={['click']} onVisibleChange={this.onMenuVisibleChange}>
          <DropdownText>
            <a className="dropdown-link" onClick={(e) => e.preventDefault()}>
              3 customers <Icon type={`${isMenuOpen ? 'caret-up' : 'caret-down'}`} />
            </a>
          </DropdownText>
        </Dropdown>
      )
    }
    return dropdown
  }

  onSortChange = (sortKey: string, event: any) => {
    event.stopPropagation()
    const { isHeader, currentPriceSheet, defaultUrl } = this.props
    if (isHeader) {
      let sortDirection = 'asc'
      if (this.state.sortKey == sortKey && this.state.sortDirection == 'asc') {
        sortDirection = 'desc'
      }
      this.setState({ sortKey, sortDirection })
      if (this.props.sortByKeyAndDirection) this.props.sortByKeyAndDirection(sortKey, sortDirection)
    } else {
      if (currentPriceSheet && currentPriceSheet.linkToken) {
        const url = `/order/${currentPriceSheet.linkToken}/token`
        this.props.history.push(`/order/${currentPriceSheet.linkToken}/token`)
      }
    }
  }

  render() {
    const { isHeader, currentPriceSheet, defaultUrl } = this.props
    const { sortKey, sortDirection } = this.state
    return (
      <ListItemRow>
        <div className="inner-wrapper">
          {/* <div className='small-col pricesheet-id-col'>
            {isHeader ? 'ID #' : `#${currentPriceSheet ? currentPriceSheet.priceSheetId : ''}`}
            {!isHeader && <div className='gray-marker'></div>}
            {isHeader && <IconSvg viewBox='0 0 20 20' type={`${sortDirection == 'desc' && sortKey == 'priceSheetId' ? 'arrow-up-product' : 'arrow-down-product'}`} className={`sort-arrow ${sortKey == 'priceSheetId' ? 'active' : 'inactive'}`} />}
          </div> */}
          <div className="flex-col" style={{ justifyContent: 'center' }} onClick={(e) => this.onSortChange('name', e)}>
            {isHeader ? 'PRICE SHEET NAME' : currentPriceSheet ? currentPriceSheet.name : ''}
            {isHeader && (
              <IconSvg
                viewBox="0 0 20 20"
                type={`${sortDirection == 'desc' && sortKey == 'name' ? 'arrow-up-product' : 'arrow-down-product'}`}
                className={`sort-arrow ${sortKey == 'name' ? 'active' : 'inactive'}`}
              />
            )}
          </div>
          <div className="med-col" onClick={(e) => this.onSortChange('firstName', e)}>
            {isHeader
              ? 'SELLER'
              : currentPriceSheet
              ? currentPriceSheet.firstName + ' ' + currentPriceSheet.lastName
              : ''}
            {isHeader && (
              <IconSvg
                viewBox="0 0 20 20"
                type={`${
                  sortDirection == 'desc' && sortKey == 'firstName' ? 'arrow-up-product' : 'arrow-down-product'
                }`}
                className={`sort-arrow ${sortKey == 'firstName' ? 'active' : 'inactive'}`}
              />
            )}
          </div>
          <div className="med-col" onClick={(e) => this.onSortChange('company', e)}>
            {isHeader ? 'COMPANY' : currentPriceSheet ? currentPriceSheet.company : ''}
            {isHeader && (
              <IconSvg
                viewBox="0 0 20 20"
                type={`${sortDirection == 'desc' && sortKey == 'company' ? 'arrow-up-product' : 'arrow-down-product'}`}
                className={`sort-arrow ${sortKey == 'company' ? 'active' : 'inactive'}`}
              />
            )}
          </div>
          {/* <div className='small-col'>{isHeader ? 'ASSIGNED' : this.renderCustomers()}</div> */}
          <div className="med-col" onClick={(e) => this.onSortChange('emailAddress', e)}>
            {isHeader ? (
              'CONTACT INFO'
            ) : currentPriceSheet ? (
              <div>
                <p>{currentPriceSheet.emailAddress}</p>
                <p>{currentPriceSheet.phone}</p>
              </div>
            ) : (
              ''
            )}
            {isHeader && (
              <IconSvg
                viewBox="0 0 20 20"
                type={`${
                  sortDirection == 'desc' && sortKey == 'emailAddress' ? 'arrow-up-product' : 'arrow-down-product'
                }`}
                className={`sort-arrow ${sortKey == 'emailAddress' ? 'active' : 'inactive'}`}
              />
            )}
          </div>

          {!isHeader && (
            <Link
              to={currentPriceSheet ? (!defaultUrl ? `/order/${currentPriceSheet.linkToken}/token` : defaultUrl) : ``}
            >
              <div className="btn-view">New Order</div>
            </Link>
          )}
        </div>
      </ListItemRow>
    )
  }
}
