/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Link } from 'react-router-dom'
import { Button } from 'antd'
import { ColumnProps } from 'antd/es/table'

import { withTheme } from 'emotion-theming'

import { CustomerPriceSheet } from '~/schema'
import { PriceSheetWrap, EmptyTr, ListBody } from './pricesheets-table.style'
import { cloneDeep } from 'lodash'
import { Theme } from '~/common'
import { CustomerPriceSheetListRow } from './customer-pricesheet-list-item'

export interface CustomerPSTableProps {
  priceSheets: CustomerPriceSheet[]
  search: string
  sortKey: string
  sortDirection: 'asc' | 'desc' | boolean
  history: any
}

type CustomerPriceSheetsTableProps = CustomerPSTableProps & { theme: Theme }

export class CustomerPriceSheetsTableComponent extends React.PureComponent<CustomerPriceSheetsTableProps> {
  renderPriceSheetList = () => {
    console.log('is in')
    const { priceSheets, search, theme, sortKey, sortDirection, history } = this.props

    let priceSheetList = cloneDeep(priceSheets)
    priceSheetList = priceSheetList.filter((item) => item.name.toUpperCase().includes(search.toUpperCase()))
    let key = sortKey
    if (sortKey == 'latestOrder') key = 'lastOrder.updatedDate'
    priceSheetList = _.orderBy(priceSheetList, [key], [sortDirection])
    let result: any[] = []

    priceSheetList.forEach((el, index) => {
      result.push(<CustomerPriceSheetListRow key={index} currentPriceSheet={el} isHeader={false} history={history} />)
    })
    return result
  }
  render() {
    // const columns: ColumnProps<CustomerPriceSheet>[] = [
    //     {
    //       title: 'ID #',
    //       dataIndex: 'priceSheetClientId',
    //       key: 'priceSheetClientId',
    //       sorter: (a, b) => a.priceSheetClientId.localeCompare(b.priceSheetClientId),
    //       render: (data) => (
    //         <PriceSheetId>
    //           <span>#{data}</span>
    //           <div />
    //         </PriceSheetId>
    //       ),
    //     },
    //     {
    //       title: 'SELLER',
    //       dataIndex: 'firstName',
    //       key: 'firstName',
    //       sorter: (a: CustomerPriceSheet, b: CustomerPriceSheet) => {
    //           const stringA = a.firstName + ' ' + a.lastName
    //           const stringB = b.firstName + ' ' + b.lastName
    //           return stringA.localeCompare(stringB)
    //       },
    //       render(_, priceSheet, __) {
    //         return <span>{priceSheet.firstName} {priceSheet.lastName}</span>
    //       },
    //     },
    //     {
    //       title: 'COMPANY',
    //       dataIndex: 'company',
    //       key: 'company',
    //       sorter: (a: CustomerPriceSheet, b: CustomerPriceSheet) => a.company.localeCompare(b.company),
    //       render(data) {
    //         return <span>{data}</span>
    //       },
    //     },
    //     {
    //       title: 'CONTACT INFO',
    //       dataIndex: 'emailAddress',
    //       key: 'emailAddress',
    //       render(_, priceSheet, __) {
    //         return (
    //           <span>{priceSheet.emailAddress}<br/>{priceSheet.phone}</span>
    //         )
    //       },
    //     },
    //     {
    //       title: 'PRICE SHEET NAME',
    //       dataIndex: 'name',
    //       key: 'name',
    //       sorter: (a: CustomerPriceSheet, b: CustomerPriceSheet) => a.name.localeCompare(b.name),
    //       render(data) {
    //         return <PriceSheetName>{data}</PriceSheetName>
    //       },
    //     },
    //     {
    //       render(_text, priceSheet, index) {
    //         return (
    //           <Link to={`/order/${priceSheet.linkToken}/token`} className="visible">
    //             <Button
    //               className="colorable"
    //               type="primary"
    //               style={{
    //                 ...disabledButtonStyle,
    //                 // border: `1px solid ${theme.primary}`,
    //                 // backgroundColor: theme.primary,
    //               }}
    //             >
    //               New Order
    //             </Button>
    //           </Link>
    //         )
    //       },
    //     }
    // ]

    // const components = {
    //   body: {
    //     wrapper: this.wrapper,
    //   },
    // }

    return (
      <PriceSheetWrap>
        {/* <ThemeTable pagination={false} columns={columns} components={components} dataSource={priceSheetList} rowKey="priceSheetClientId" css={tableCss(false)} /> */}
        <PriceSheetWrap className="pricing-list">
          <ListBody>{this.renderPriceSheetList()}</ListBody>
        </PriceSheetWrap>
      </PriceSheetWrap>
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody className="ant-table-tbody">
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}

export const CustomerPriceSheetsTable = withTheme(CustomerPriceSheetsTableComponent)
