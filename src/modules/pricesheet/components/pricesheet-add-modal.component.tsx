import React from 'react'
import { Modal, Input, Button, notification } from 'antd'
import { cloneDeep, debounce } from 'lodash'
import { ColumnProps } from 'antd/es/table'
import jQuery from 'jquery'
// import { Icon as IconSvg } from '~/components'
import { SaleItem, PriceSheetItem } from '~/schema'
import { QBOImage, tableCss, EmptyTr } from './pricesheet-detail-table.style'
import { DialogHeader, DialogFooter, DialogSearch, DialogSearchWithIn } from './pricesheet-detail-header.style'

import organicImg from '~/modules/inventory/components/organic.png'
import { MutiSortTable } from '~/components'
import { Theme } from '~/common'
import { searchButton } from '~/components/elements/search-button'
import SearchInput from '~/components/Table/search-input'

export interface PriceSheetAddModalProps {
  saleItems: SaleItem[]
  priceSheetItems: PriceSheetItem[]
}

interface ItemProps extends SaleItem {
  sku: string
}

export class PriceSheetAddModal extends React.PureComponent<
  PriceSheetAddModalProps & {
    visible: boolean
    onOk: (keys: string[]) => void
    onCancel: () => void
    theme: Theme
  },
  {
    search: string
    searchWithIn: string
    selectedRowKeys: string[]
    searchDOMKey: number
  }
  > {
  state = {
    search: '',
    searchWithIn: '',
    selectedRowKeys: [],
    searchDOMKey: new Date().getTime()
  }

  onSearchWithIn = debounce((searchWithIn: string) => {
    this.setState({
      searchWithIn
    })
  }, 500)

  // private mutiSortTable = React.createRef<any>()

  private columns: ColumnProps<SaleItem>[] = [
    {
      title: 'SKU',
      dataIndex: 'SKU',
      key: 'SKU',
      width: '15%',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'SKU')
        // if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('SKU', b, a)
        return this.onSortString('SKU', a, b)
      },
    },
    {
      title: 'CATEGORY',
      dataIndex: 'wholesaleCategory.name',
      key: 'wholesaleCategory.name',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
        //   (meta: any) => meta.dataIndex === 'wholesaleCategory.name',
        // )
        // if (sortIndex && sortIndex.sortOrder == 'descend')
        //   return this.onSortString('name', b.wholesaleCategory, a.wholesaleCategory)
        return this.onSortString('name', a.wholesaleCategory, b.wholesaleCategory)
      },
    },
    {
      title: 'NAME',
      dataIndex: 'variety',
      key: 'variety',
      width: '25%',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
        //   (meta: any) => meta.dataIndex === 'variety',
        // )
        // if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortVariety(b, a)
        return this.onSortVariety(a, b)
      },
      render(text, item) {
        return item.organic == true ? (
          <>
            <QBOImage src={organicImg} /> <span>{text}</span>
          </>
        ) : (
            <span>{text}</span>
          )
      },
    },
    {
      title: 'SIZE',
      dataIndex: 'size',
      key: 'size',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'size')
        // if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('size', b, a)
        return this.onSortString('size', a, b)
      },
    },
    {
      title: 'WEIGHT',
      dataIndex: 'weight',
      key: 'weight',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'weight')
        // if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('weight', b, a)
        return this.onSortString('weight', a, b)
      },
    },
    {
      title: 'GRADE',
      dataIndex: 'grade',
      key: 'grade',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'grade')
        // if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('grade', b, a)
        return this.onSortString('grade', a, b)
      },
    },
    {
      title: 'PACKING',
      dataIndex: 'packing',
      key: 'packing',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
        //   (meta: any) => meta.dataIndex === 'packing',
        // )
        // if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('packing', b, a)
        return this.onSortString('packing', a, b)
      },
    },
    {
      title: 'ORIGIN',
      dataIndex: 'origin',
      key: 'origin',
      sorter: (a, b) => {
        // const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'origin')
        // if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('origin', b, a)
        return this.onSortString('origin', a, b)
      },
    },
    // {
    //   title: 'PROVIDER',
    //   dataIndex: 'provider',
    //   key: 'provider',
    //   sorter: (a, b) => {
    //     const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
    //       (meta: any) => meta.dataIndex === 'provider',
    //     )
    //     if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('provider', b, a)
    //     return this.onSortString('provider', a, b)
    //   },
    // },
  ]

  onChangeRowSelection = (selectedRowKeys: any) => {
    this.setState({ selectedRowKeys })
  }

  onSearch = (search: string) => {
    this.setState({
      search: search,
    })
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onSortVariety = (a: any, b: any) => {
    const stringA = a.variety ? a.variety : ''
    const stringB = b.variety ? b.variety : ''
    // stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    // stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.visible === false && nextProps.visible === true) {
      setTimeout(() => {
        jQuery('#search-sku').trigger('focus')
      }, 100)
    }
  }

  // wrapper = (props: any) => {
  //   return (
  //     <tbody>
  //       <EmptyTr>
  //         <td />
  //       </EmptyTr>
  //       {props.children}
  //     </tbody>
  //   )
  // }

  filterListItems = (listItems: SaleItem[], search: string) => {
    let matches1: ItemProps[] = []
    let matches2: ItemProps[] = []
    const matches3: ItemProps[] = []

    listItems.forEach((row: ItemProps) => {
      const searchStr = search.toLowerCase()
      if(typeof row.SKU === 'undefined' && row.sku) {
        row.SKU = row.sku
      }
      if (row.SKU && row.SKU.toLowerCase().indexOf(searchStr) === 0) {
        // full match on 2nd filed
        matches2.push(row);
      } else if (row.variety && row.variety.toLowerCase().indexOf(searchStr) === 0) {
          // full match on variety
        matches1.push(row);
      } else if (
        (row.wholesaleCategory.name && row.wholesaleCategory.name.indexOf(searchStr) > 0) ||
        (row.variety && row.variety.toLowerCase().indexOf(searchStr) > 0) ||
        (row.SKU && row.SKU.toLowerCase().indexOf(searchStr) > 0) ||
        (row.upc && row.upc.toLowerCase().indexOf(searchStr) >= 0)
      ) {
        // others
        matches3.push(row);
      }
    })

    if(matches1) {
      matches1 = matches1.sort((a, b) => a.variety.localeCompare(b.variety))
    }
    if(matches2) {
      matches2 = matches2.sort((a, b) => a.SKU.localeCompare(b.SKU))
    }

    return [...matches2, ...matches1, ...matches3]
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { selectedRowKeys } = this.state
    if (!selectedRowKeys.length) {
      return notification.error({
        message: 'Select one at least',
      })
    }
    this.props.onOk(selectedRowKeys)
    this.setState({
      selectedRowKeys: [],
      search: '',
      searchWithIn: '',
      searchDOMKey: new Date().getTime()
    })
  }

  render() {
    const { search, searchWithIn, selectedRowKeys, searchDOMKey } = this.state

    let listItems = cloneDeep(this.props.saleItems)

    const itemIds = this.props.priceSheetItems.map(item => item.itemId)

    listItems = listItems.filter(item => {
      return itemIds.includes(item.itemId) === false
    })

    // if (search) {
    //   listItems = listItems.filter((item) =>
    //     (item.SKU + item.variety + item.wholesaleCategory.name + item.provider).toUpperCase().includes(search.toUpperCase()),
    //   )
    // }

    if (search && listItems.length > 0) {
      listItems = this.filterListItems(listItems, search)
    }

    const visibleSearchWithIn = listItems.length > 0

    if (searchWithIn && visibleSearchWithIn) {
      listItems = this.filterListItems(listItems, searchWithIn)
    }

    return (
      <Modal
        width={'92%'}
        bodyStyle={{ padding: '0' }}
        visible={this.props.visible}
        onCancel={this.props.onCancel}
        footer={[
          <DialogFooter key="footer">
            <Button
              type="primary"
              icon="plus"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              Add Items
            </Button>
            <Button onClick={this.props.onCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <DialogHeader style={{ alignItems: 'center' }}>
          Bulk Add Items to Price Sheet
          <div key={searchDOMKey}>
            <DialogSearch>
              <Input.Search
                id="search-sku"
                placeholder="Search SKU, item name, category, or UPC"
                size="large"
                style={{ width: '557px', border: '0' }}
                onSearch={this.onSearch}
                enterButton={searchButton}
              />
            </DialogSearch>

            {visibleSearchWithIn && (
              <DialogSearchWithIn>
                <span>Search within results:</span>
                <SearchInput
                  className="add-item-modal-input"
                  handleSearch={this.onSearchWithIn}
                  placeholder="Search text"
                  defaultValue={searchWithIn}
                  withApiCall={false}
                />
              </DialogSearchWithIn>
            )}
          </div>
        </DialogHeader>
        <MutiSortTable
          rowSelection={{
            selectedRowKeys,
            onChange: this.onChangeRowSelection
          }}
          columns={this.columns}
          dataSource={listItems}
          bordered={false}
          rowKey="itemId"
          css={tableCss}
        />
      </Modal>
    )
  }
}
