import React from 'react'
import { Input, Button, Icon, notification, Spin, Popconfirm } from 'antd'
import { Icon as IconSvg } from '~/components'
import {
  HeaderOptions,
  buttonStyle,
  Flex,
  Operation,
  BackButton,
  Name,
  InfoSpan,
  Header,
} from './pricesheet-detail-header.style'
import { Theme } from '~/common'
import { withTheme } from 'emotion-theming'
import { PriceSheet } from '~/schema'
import { PriceSheetDispatchProps } from '../pricesheet.module'
import { match } from 'react-router'
import { PSLoading } from '../pricesheet.module'
import { searchButton } from '~/components/elements/search-button'

export interface Props {
  onPrint: () => void
  onOpenModal: () => void
  onOpenEmailModal: () => void
  onOpenManageCustomerModal: () => void
  onSearch: (search: string) => void
  onClickCancelEditInfo: () => void
  onClickEditInfo: () => void
  onShowDuplicate: () => void
  theme: Theme
  priceSheetInfo: PriceSheet | null
  editingInfo: boolean
  onClickSaveInfo: () => void
  onChangeEditInput: (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => void
  onClickBack: () => void
  onSaveItems: PriceSheetDispatchProps['savePriceSheetItems']
  toggleCompanyShared: () => void
  loadIndex: PriceSheetDispatchProps['loadIndex']
  match: match<{ id: string }>
  loadingIndex: string
  isRemovedCats: boolean
  onUpdateCatsView: Function
}

class PriceSheetDetailHeaderComponent extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props)
  }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'Coming Soon',
      onClick: () => {
        console.log('Notification Clicked!')
      },
    })
  }

  componentDidMount() {
    console.log(this.props)
  }

  showManageCustomerModal = () => {

  }

  render() {
    const { editingInfo, priceSheetInfo } = this.props
    const HeaderButtons = editingInfo ? (
      <>
        <BackButton onClick={this.props.onClickSaveInfo}>
          <Icon type="save" />
          <span>SAVE</span>
        </BackButton>
        <BackButton onClick={this.props.onClickCancelEditInfo}>
          <Icon type="cancel" />
          <span>CANCEL</span>
        </BackButton>
      </>
    ) : (
        <BackButton onClick={this.props.onClickEditInfo}>
          <Icon type="edit" />
          <span>RENAME</span>
        </BackButton>
      )

    const PricesheetInfo = editingInfo ? (
      <span>
        {priceSheetInfo ? (
          <Input defaultValue={priceSheetInfo.name} size="large" onChange={this.props.onChangeEditInput('name')} />
        ) : (
            <Input defaultValue="N/A" size="large" onChange={this.props.onChangeEditInput('name')} />
          )}
      </span>
    ) : (
        <div>{priceSheetInfo ? <Name>{priceSheetInfo.name}</Name> : <Name>N/A</Name>}</div>
      )

    return (
      <Header>
        <Flex style={{ marginTop: '36px' }}>
          <BackButton onClick={this.props.onClickBack}>
            <Icon type="arrow-left" />
            <span>back</span>
          </BackButton>
          <div>
            <Flex>
              <InfoSpan>Price Sheet</InfoSpan>
              {priceSheetInfo!.owner == true ? HeaderButtons : (<span></span>)}
            </Flex>
            {PricesheetInfo}
            <Flex style={{ marginTop: '36px' }}>
              <HeaderOptions>
                <Input.Search
                  placeholder="Search item name, category, or provider"
                  size="large"
                  style={{ width: '557px', border: '0' }}
                  onSearch={this.props.onSearch}
                  enterButton={searchButton}
                />
              </HeaderOptions>
            </Flex>
          </div>
        </Flex>
        <Flex style={{ marginTop: '16px' }}>
          <div>
            <Flex>
              <Operation onClick={this.props.onOpenManageCustomerModal}>
                <Icon type="user" /> MANAGE CUSTOMERS
              </Operation>
              <Operation onClick={this.props.onOpenEmailModal}>
                <Icon type="mail" /> EMAIL
              </Operation>
              <Operation onClick={this.props.onPrint}>
                <Icon type="printer" /> PRINT
              </Operation>
              <Operation onClick={this.props.onShowDuplicate}>
                <Icon type="copy" /> DUPLICATE
              </Operation>
            </Flex>
            {priceSheetInfo!.owner == true ? (
              <HeaderOptions style={{ marginTop: '110px' }}>
                {/* <Popconfirm
                  title="Are you sure to save?"
                  onConfirm={this.handleSaveItems}
                  okText="Save"
                  cancelText="Cancel"
                > */}
                <Button
                  size="large"
                  type="primary"
                  style={{
                    ...buttonStyle,
                    border: `1px solid ${this.props.theme.primary}`,
                    backgroundColor: this.props.theme.primary,
                  }}
                  onClick={this.handleSaveItems}
                  disabled={this.props.loadingIndex.length > 0}
                >
                  {this.props.loadingIndex == PSLoading.PS_ITEMS_SAVE ? (<span><Spin /> Saving</span>) : (<span><Icon type="check" /> Save Price Sheet</span>)}
                </Button>
                {/* </Popconfirm> */}
                <Button
                  size="large"
                  icon="plus"
                  type="primary"
                  style={{
                    ...buttonStyle,
                    border: `1px solid ${this.props.theme.primary}`,
                    backgroundColor: this.props.theme.primary,
                  }}
                  onClick={this.props.onOpenModal}
                >
                  Add Item
              </Button>
                <Button
                  size="large"
                  type="primary"
                  style={{
                    ...buttonStyle,
                    border: `1px solid ${this.props.theme.primary}`,
                    backgroundColor: this.props.theme.primary,
                  }}
                  onClick={this.props.onUpdateCatsView}
                >
                  {this.props.isRemovedCats === true ? 'Category View' : 'Remove Categories'}
                </Button>
                {/* <Button
                size="large"
                icon={priceSheetInfo!.companyShared ? "check-square" : "border"}
                type="primary"
                style={{
                  ...buttonStyle,
                  border: `1px solid ${this.props.theme.primary}`,
                  backgroundColor: this.props.theme.primary,
                }}
                onClick={this.props.toggleCompanyShared}
              >
                Share to Company
              </Button> */}
              </HeaderOptions>) : (<span></span>)}
          </div>
        </Flex>
      </Header>
    )
  }

  handleSaveItems = () => {
    this.props.loadIndex(PSLoading.PS_ITEMS_SAVE)
    this.props.onSaveItems(this.props.match.params.id)
  }
}

export const PriceSheetDetailHeader = withTheme(PriceSheetDetailHeaderComponent)
