import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { black, brownGrey, transparent, mediumGrey, mediumGrey2 } from '~/common'
import { Modal } from 'antd'

export const Header = styled('div')({
  width: '100%',
  // height: '232px',
  boxSizing: 'border-box',
  display: 'flex',
  justifyContent: 'space-between',

  '&.pricing-detail-header': {
    display: 'block',
    justifyContent: 'normal',
  },
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .info-editor': {
    margin: '10px 10px 0 10px',
    width: '100%',
    minWidth: 700,
    textAlign: 'left',
    '.name-area': {
      justifyContent: 'space-between',
      alignItems: 'center',
      '.shared-control': {
        alignItems: 'center',
        marginRight: 68,
        '.sales-rep-dropdown': {
          width: 200,
          marginRight: 24
        }
      }
    }
  },
})

export const OptionsWrapper = styled('div')({
  width: 700,
  marginLeft: 60,
})

export const Container = styled('div')({
  paddingLeft: '36px',
  paddingRight: '22px',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const HeaderOptions = styled('div')((props: any) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'flex-end',
  '&.flex-start': {
    justifyContent: 'flex-start',
  },
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: props.theme.primary,
    '&::after': {
      display: 'none',
    },
  },
  '.clear-icon': {
    display: 'none'
  },
  '& .ant-input-search:hover .clear-icon.show': {
    display: 'block'
  }
}))

export const DialogHeader = styled('div')((props) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'flex-end',
  padding: '43px 128px 45px 67px',
  fontSize: '28px',
  color: mediumGrey,
  lineHeight: '30px',
  letterSpacing: '0.05em',
  fontWeight: 700,
  // '& .ant-input-group-addon': {
  //   backgroundColor: transparent,
  // },
  // '& .ant-input-search-button': {
  //   backgroundColor: transparent,
  //   border: 'none',
  //   borderBottom: '1px solid black',
  //   borderRadius: 0,
  //   color: props.theme.primary,
  //   '&::after': {
  //     display: 'none',
  //   },
  // },
}))

export const DialogSearch = styled('div')((props) => ({
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: props.theme.primary,
    '&::after': {
      display: 'none',
    },
  },
}))

export const DialogSearchWithIn = styled('div')(() => ({
  marginTop: '6px',
  paddingLeft: '11px',
  display: 'flex',
  alignItems: 'center',

  '& > span': {
    marginRight: '10px',
    fontWeight: 'normal',
    fontSize: '14px',
    color: 'rgba(0, 0, 0, .65)'
  },
  '& .add-item-modal-input': {
    width: 375,
  },
  '& .clear-icon': {
    display: 'none',
    marginRight: 6
  },
  '& .add-item-modal-input:hover .clear-icon.show': {
    display: 'block'
  }
})

export const Flex = styled('div')({
  display: 'flex',
  '&.default-pricing-header': {
    justifyContent: 'flex-end',
  },
})

export const Name = styled('div')({
  color: black,
  fontSize: '36px',
  lineHeight: '52px',
  letterSpacing: '0.05em',
  fontWeight: 700,
})

export const Operation = styled('div')((props: any) => ({
  display: 'flex',
  fontSize: '14px',
  fontWeight: 700,
  color: props.theme.primary,
  lineHeight: '15px',
  height: '15px',
  letterSpacing: '0.05em',
  cursor: 'pointer',
  '& + &': {
    marginLeft: '45px',
  },
  '& > i': {
    fontSize: '16px',
    margin: '0 10px',
  },
}))

export const BackButton = styled('div')((props: any) => ({
  color: props.theme.primary,
  cursor: 'pointer',
  fontSize: '12px',
  marginRight: '50px',
  minWidth: '54px',

  '& > span': {
    textTransform: 'uppercase',
    fontWeight: 700,
    marginLeft: '8px',
  },
}))

export const InfoSpan = styled('div')({
  fontSize: '12px',
  fontWeight: 700,
  textTransform: 'uppercase',
  color: mediumGrey,
  marginRight: '18px',
})

export const searchIconCss = css({
  marginRight: '10px',
})

export const buttonStyle: React.CSSProperties = {
  borderRadius: '200px',
  height: '40px',
  marginRight: '58px',
}

export const DialogFooter = styled('div')({
  paddingRight: '38px',
  '& > button:nth-of-type(1)': {
    height: '40px',
    fontSize: '16px',
    width: '200px',
    borderRadius: '200px',
  },

  '& > button:nth-of-type(2)': {
    color: brownGrey,
    marginLeft: '23px',
    cursor: 'pointer',
    border: '0px',
    '& > span': {
      textDecoration: 'underline',
      fontSize: '14px',
      lineHeight: '15px',
      fontWeight: 700,
    },
  },
  '@media (max-width: 651px)': {
    paddingRight: 0
  }
})

export const ModalHeader = styled('div')(() => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'flex-end',
  padding: '45px 0px 45px 0px',
  fontSize: '28px',
  color: '#888',
  lineHeight: '30px',
  letterSpacing: '0.05em',
  fontWeight: 600,
}))

export const ModalFormLabel = styled('div')(() => ({
  fontSize: '16px',
  fontWeight: 600,
}))

export const ModalFormRadio = styled('div')(() => ({
  margin: '20px',
  '.ant-radio-wrapper': {
    color: mediumGrey2
  },
  '.ant-radio-wrapper-checked': {
    color: 'rgba(0, 0, 0, 0.65)'
  }
}))

export const EmailPriceSheetModal = styled(Modal)`
  .ant-modal-footer {
    text-align: left;
    background-color: rgb(247,247,247);
    border-top: 1px solid #d9d9d9;
    padding: 14px 20px;
  }
`

