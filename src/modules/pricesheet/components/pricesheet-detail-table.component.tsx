/**@jsx jsx */
import React, { useState, useEffect } from 'react'
import { jsx } from '@emotion/core'
import { Icon, Button, Spin, InputNumber, Select } from 'antd'
import { ColumnProps } from 'antd/es/table'
import lodash from 'lodash'

import { PriceSheetItem, SaleSection, PriceSheet } from '~/schema'
import { MutiSortTable } from '~/components'
import CategoryHeader from '~/components/CategoryHeader'
import { PriceSheetDispatchProps } from '../pricesheet.module'
import organicImg from '~/modules/inventory/components/organic.png'
import { tableCss, Stock, polygonCss, inputCss, InputWrapper, tableWrapperStyle } from './pricesheet-detail-table.style'
import { QBOImage } from '~/modules/inventory/components/inventory-table.style'
import { ThemeSelect, ThemeTable } from '~/modules/customers/customers.style'
import { formatNumber } from '~/common/utils'

export interface PriceSheetDetailTableProps {
  priceSheetItems: PriceSheetItem[]
  priceSheetInfo: PriceSheet
  search: string
  onRemoveItem: (priceSheetItemId: string) => void
  onCatogoryChange: PriceSheetDispatchProps['updateCategory']
  onPriceSheetItemUpdate: PriceSheetDispatchProps['updatePriceSheetItem']
  loadingIndex: string
  isRemovedCats: boolean
}

export class PriceSheetDetailTable extends React.PureComponent<PriceSheetDetailTableProps> {
  multiSortTable = React.createRef<any>()

  handleSalePriceChange = lodash.debounce((priceSheetItemId: string, newSalePrice?: number) => {
    if (newSalePrice) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if (item) {
        let newMargin = 0
        if (newSalePrice <= 0) {
          newMargin = 100
        } else {
          newMargin =
            item.cost > 0 && newSalePrice - item.markup > item.cost
              ? Math.round(((newSalePrice - item.markup - item.cost) / newSalePrice) * 100 * 100) / 100
              : 0
        }

        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            salePrice: parseFloat(newSalePrice.toFixed(2)),
            margin: newMargin,
          },
        })
      }
    }
  }, 200)

  handleFreightChange = lodash.debounce((priceSheetItemId: string, newFreight?: number) => {
    if (true /*newFreight*/) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if (item) {
        let newSalesPrice = 0
        if (item.margin >= 100) {
          newSalesPrice = 0
        } else {
          newSalesPrice =
            item.margin === 0 && newFreight === 0
              ? item.cost
              : Math.round((item.cost / (1.0 - item.margin / 100.0) + newFreight) * 4) / 4
        }
        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            markup: newFreight,
            salePrice: parseFloat(newSalesPrice.toFixed(2)),
          },
        })
      }
    }
  }, 200)

  handleMarginChange = lodash.debounce((priceSheetItemId: string, newMargin?: number) => {
    if (true /*newMargin*/) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props

      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if (item) {
        let newSalePrice = 0
        if (newMargin >= 100) {
          newSalePrice = 0
        } else {
          newSalePrice =
            newMargin === 0 && item.markup === 0
              ? item.cost
              : Math.round((item.cost / (1.0 - newMargin / 100.0) + item.markup) * 4) / 4
        }

        onPriceSheetItemUpdate({
          priceSheetItemId: item.itemId,
          data: {
            margin: newMargin,
            salePrice: parseFloat(newSalePrice.toFixed(2)),
          },
        })
      }
    }
  }, 200)

  handleRemoveItem(priceSheetItemId: string) {
    this.props.onRemoveItem(priceSheetItemId)
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onSortVariety = (a: any, b: any) => {
    let stringA = a.variety ? a.variety : ''
    let stringB = b.variety ? b.variety : ''
    stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }

  onModifiersChange = (record: any, value: any) => {
    console.log(record, value)
    const { onPriceSheetItemUpdate } = this.props
    onPriceSheetItemUpdate({
      priceSheetItemId: record.itemId,
      data: {
        modifier: value,
      },
    })
  }

  renderModifiersList = (record: any) => {
    if (!record.suppliers) return []
    const currentSuppliers = record.suppliers.split(', ').map((el: string) => el.trim())
    let result: any[] = []
    currentSuppliers.forEach((el: any, index: number) => {
      result.push(
        <Select.Option key={index} value={el}>
          {el}
        </Select.Option>,
      )
    })
    return result
  }

  render() {
    const { priceSheetItems, priceSheetInfo } = this.props
    const currentPriceSheetIndex = 0
    const columns: ColumnProps<PriceSheetItem>[] = [
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        width: 100,
        sorter: (a, b) => {
          return this.onSortString('SKU', a, b)
        },
      },
      {
        title: 'CATEGORY',
        dataIndex: 'wholesaleCategory.name',
        key: 'wholesaleCategory.name',
        width: 150,
        sorter: (a, b) => {
          return this.onSortString('name', a.wholesaleCategory, b.wholesaleCategory)
        },
      },
      {
        title: 'NAME',
        dataIndex: 'variety',
        key: 'variety',
        width: 310,
        sorter: (a, b) => {
          return this.onSortVariety(a, b)
        },
        render(text, item) {
          return item.organic ? (
            <div>
              <QBOImage src={organicImg} />
              <span>{text}</span>
            </div>
          ) : (
            <span>{text}</span>
          )
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        width: 150,
        sorter: (a, b) => {
          return this.onSortString('size', a, b)
        },
      },
      {
        title: 'WEIGHT',
        dataIndex: 'weight',
        key: 'weight',
        width: 100,
        sorter: (a, b) => {
          return this.onSortString('weight', a, b)
        },
      },
      {
        title: 'GRADE',
        dataIndex: 'grade',
        key: 'grade',
        width: 100,
        sorter: (a, b) => {
          return this.onSortString('grade', a, b)
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        width: 130,
        sorter: (a, b) => {
          return this.onSortString('packing', a, b)
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'origin',
        key: 'origin',
        width: 130,
        sorter: (a, b) => {
          return this.onSortString('origin', a, b)
        },
      },
      {
        title: 'PROVIDER',
        dataIndex: 'modifier',
        key: 'modifier',
        width: 120,
        render: (modifier: string, record: any) => {
          return (
            <ThemeSelect
              style={{ width: '100%' }}
              onChange={this.onModifiersChange.bind(this, record)}
              value={modifier ? modifier : ''}
            >
              {this.renderModifiersList(record)}
            </ThemeSelect>
          )
        },
      },
      {
        title: 'PRICING UOM',
        dataIndex: 'baseUOM',
        key: 'baseUOM',
        width: 150,
      },
      {
        title: 'STOCK',
        dataIndex: 'availableQty',
        key: 'availableQty',
        width: 100,
        sorter: (a, b) => {
          return a.availableQty - b.availableQty
        },
        render(text: any, record: any) {
          return (
            <Stock stock={Number(text)}>
              <span style={{ fontWeight: 700 }}>{formatNumber(text, 2)}</span>
              <Icon type="polygon" css={polygonCss} />
            </Stock>
          )
        },
      },
      {
        title: 'COST',
        dataIndex: 'cost',
        key: 'cost',
        width: 100,
        sorter: (a, b) => {
          return a.cost - b.cost
        },
        render(rowPrice) {
          return `$${rowPrice.toFixed(2)}`
        },
      },
      {
        title: 'MARGIN',
        dataIndex: 'margin',
        key: 'margin',
        width: 100,
        sorter: (a, b) => {
          return a.margin - b.margin
        },
        render: (rowPrice, item, index) => {
          return (
            <>
              <InputWrapper className="block">
                <InputNumber
                  value={rowPrice ? rowPrice.toFixed(2) : ''}
                  placeholder="0%"
                  step="1"
                  min={0}
                  formatter={(value) => `${value}%`}
                  css={inputCss}
                  onChange={this.handleMarginChange.bind(this, item.priceSheetItemId)}
                />
              </InputWrapper>
              <span className="none">{rowPrice ? rowPrice.toFixed(2) : ''}%</span>
            </>
          )
        },
      },
      {
        title: 'FREIGHT',
        dataIndex: 'freight',
        key: 'freight',
        width: 110,
        sorter: (a, b) => {
          return a.freight - b.freight
        },
        render: (rowPrice, item, index) => {
          return (
            <>
              {priceSheetInfo.owner === true && (
                <InputWrapper className="block">
                  <InputNumber
                    value={rowPrice}
                    placeholder="$0.00"
                    step="0.25"
                    min={0}
                    formatter={(value) => `$${value}`}
                    css={inputCss}
                    onChange={this.handleFreightChange.bind(this, item.priceSheetItemId)}
                  />
                </InputWrapper>
              )}
              <span className="none">${rowPrice.toFixed(2)}</span>
            </>
          )
        },
      },
      {
        title: 'SALE PRICE',
        dataIndex: 'salePrice',
        key: 'salePrice',
        width: 120,
        sorter: (a, b) => {
          return a.salePrice - b.salePrice
        },
        render: (rowPrice, item, index) => {
          return (
            <>
              {priceSheetInfo.owner === true && (
                <InputWrapper className="block">
                  <InputNumber
                    value={rowPrice}
                    placeholder="$0.00"
                    step="0.25"
                    min={0}
                    formatter={(value) => `$${value}`}
                    css={inputCss}
                    onChange={this.handleSalePriceChange.bind(this, item.priceSheetItemId)}
                  />
                </InputWrapper>
              )}
              <span className="none">${rowPrice.toFixed(2)}</span>
            </>
          )
        },
      },
      {
        width: 110,
        className: 'buttonCell',
        render: (_item, saleItem, index) => {
          return (
            <>
              {priceSheetInfo.owner === true && (
                <Button
                  className="block"
                  type="default"
                  onClick={this.handleRemoveItem.bind(this, saleItem.priceSheetItemId)}
                  disabled={this.props.loadingIndex.length > 0}
                >
                  {this.props.loadingIndex === saleItem.priceSheetItemId ? <Spin /> : <Icon type="delete" />}
                </Button>
              )}
              {this.props.loadingIndex === saleItem.priceSheetItemId && <Spin />}
            </>
          )
        },
      },
    ]

    return (
      <ThemeTable
        ref={this.multiSortTable}
        style={{ paddingLeft: 0, paddingRight: 0 }}
        pagination={false}
        columns={columns}
        dataSource={priceSheetItems}
        rowKey="itemId"
      // css={tableCss}
      />
    )
  }
}

const PriceSheetDetailSubtable: React.SFC<
  PriceSheetDetailTableProps & {
    category: SaleSection | null
  }
> = (props) => {
  const { category, priceSheetItems, search } = props
  let data = priceSheetItems
  if (category !== null) {
    data = priceSheetItems.filter((n) => n.wholesaleCategory.wholesaleSection.name === category.name)
  }

  const listItems = search
    ? data.filter((item) =>
      (item.SKU + item.variety + item.wholesaleCategory.name + item.provider)
        .toUpperCase()
        .includes(search.toUpperCase()),
    )
    : data

  const [collapsed, setCollpased] = useState(listItems.length === 0)

  useEffect(() => {
    // set collapsed#true with empty search result
    setCollpased(listItems.length === 0)
  }, [props.search])

  const handleHeaderClick = () => {
    setCollpased(!collapsed)
  }

  const handleCategoryChange = (value: string) => {
    props.onCatogoryChange({
      name: value,
      wholesaleSectionId: props.category.wholesaleSectionId,
      warehouse: props.category.warehouse,
      qboId: props.category.qboId,
    })
  }

  return (
    <React.Fragment>
      <CategoryHeader
        editMode
        value={category !== null ? category.name : 'All'}
        onChange={handleCategoryChange}
        collapsed={collapsed}
        onCollapseChange={handleHeaderClick}
      />
      <div css={tableWrapperStyle(collapsed)}>
        <PriceSheetDetailTable {...props} priceSheetItems={listItems} />
      </div>
    </React.Fragment>
  )
}

const PriceSheetDetailTableContainer: React.SFC<PriceSheetDetailTableProps> = (props) => {
  // pick up all category
  const categoryList: SaleSection[] = lodash.uniqWith(
    props.priceSheetItems.map((n) =>
      lodash.pick(n.wholesaleCategory.wholesaleSection, ['wholesaleSectionId', 'name', 'warehouse', 'qboId']),
    ),
    lodash.isEqual,
  )
  console.log(props)

  if (props.isRemovedCats === true) {
    return <PriceSheetDetailSubtable category={null} {...props} />
  }

  return (
    <div>
      {categoryList.map((item) => (
        <PriceSheetDetailSubtable key={item.wholesaleSectionId} category={item} {...props} />
      ))}
    </div>
  )
}

export default React.memo(PriceSheetDetailTableContainer)
