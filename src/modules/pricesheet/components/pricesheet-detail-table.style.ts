import styled from '@emotion/styled'
import { black } from '~/common'
import { css } from '@emotion/core'
import { css as createCss } from 'emotion'
import { transparent, yellow, mutedGreen, red, brightGreen, white, lightGrey } from '~/common'
import { gap } from '~/common/styleVars'

export const TableTr = styled('tr')({
  height: '50px',
})

export const tableCss = () =>
  css({
    marginTop: gap,
    '.ant-pagination': {
      paddingRight: '30px',
    },
    '& .ant-table-content > .ant-table-body': {
      '& > table': {
        marginBottom: '0px',
        paddingBottom: '12px',
        background: white,
        '& > tbody': {
          '& .ant-table-row': {
            borderBottom: '1px solid #d9d9d9',
            '& td:first-of-type': {
              borderRight: '0px',
              textAlign: 'unset',
            },
            '& td:nth-of-type(2)': {
              padding: '7px 7px 8px 0px',
            },
            '& td': {
              color: black,
              fontSize: '14px',
              lineHeight: '17px',
              fontWeight: 300,
              padding: '7px 7px 8px 22px',
              borderRight: '1px solid #EDF1EE',
            },
          },
        },
        '& .ant-table-thead': {
          boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
          '& > tr > th:first-of-type': {
            padding: '1px 0px 15px 22px',
          },
          '& > tr > th:nth-of-type(2)': {
            padding: '0px 0px 15px 0px',
          },
          '& > tr > th': {
            backgroundColor: white,
            color: black,
            border: 'none',
            padding: '0px 0px 15px 27px',
            fontWeight: 700,
            fontSize: '12px',
            lineHeight: '14px',
            '& > div': {
              display: 'flex',
            },
          },
        },
      },
    },
  })

export const Category = styled('td')({
  position: 'absolute',
  width: '100vw',
  height: '50px',
  textAlign: 'center',
  fontSize: '24px',
  lineHeight: '20px',
  fontFamily: '"Museo Sans Rounded"',
})

export const CategoryTr = styled('tr')({
  height: '50px',
  [`&:hover:not(.ant-table-expanded-row) ${Category}`]: {
    background: transparent,
  },
})

export const columnClass = createCss({
  boxShadow: '6px 0 6px -4px rgba(0,0,0,0.15)',
})

export const Stock = styled('div')(({ stock }: { stock: number }) => ({
  borderLeft: `solid 5px ${stock === 0 ? yellow : stock > 0 ? mutedGreen : red}`,
  height: '44px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  paddingLeft: '10px',
  whiteSpace: 'nowrap'
}))

export const polygonCss = css({
  width: '12px',
  height: '12px',
  marginLeft: '5px',
})

export const Flex = styled('div')({
  display: 'flex',
})

export const Quantity = styled('div')({
  display: 'flex',
  justifyContent: 'space-evenly',
  lineHeight: '40px',
})

export const PolygonWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-around',
  alignContent: 'center',
  height: '40px',
})

export const quantityPolygonCss = css({
  width: '12px',
  height: '12px',
  color: brightGreen,
  '&:nth-child(1)': {
    transform: 'rotate(180deg)',
  },
})

export const TextWrapper = styled('span')(({ len }: { len: number }) => ({
  paddingRight: len < 10 ? '6px' : '0',
}))

export const AddNewItem = styled('td')({
  position: 'absolute',
  width: '100vw',
  height: '55px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
})

export const AddNewItemTr = styled('tr')(
  ({ isEditing }: { isEditing: boolean }) => ({
    [`& ${AddNewItem}`]: {
      backgroundColor: isEditing ? transparent : 'rgba(38, 172, 95, 0.2)',
    },
    [`&:hover:not(.ant-table-expanded-row) ${AddNewItem}`]: {
      backgroundColor: isEditing ? transparent : 'rgba(38, 172, 95, 0.2)',
    },
  }),
  {
    height: '55px',
  },
)

export const AddItemsWrapper = styled('div')(
  ({ isEditing, theme }: { isEditing: boolean; theme: any }) => ({
    backgroundColor: isEditing ? theme.primary : 'transparent',
    color: isEditing ? white : theme.primary,
  }),
  {
    display: 'flex',
    fontSize: '16px',
    justifyContent: 'center',
    alignItems: 'center',
    userSelect: 'none',
    cursor: 'pointer',
    height: '55px',
    padding: '0 10px',
  },
)

export const PlusButton = styled('span')((props) => ({
  borderRadius: '50%',
  width: '20px',
  height: '20px',
  backgroundColor: props.theme.primary,
  fontSize: '20px',
  lineHeight: '20px',
  textAlign: 'center',
  color: white,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginRight: '10px',
}))

export const RadioWrapper = styled('div')({
  display: 'flex',
  padding: '0 40px',
  borderRight: `1px solid ${lightGrey}`,
  marginRight: '5px',
})

export const autocompleteCss = css({
  flexGrow: 1,
  position: 'relative',
  '& .ant-select-selection': {
    padding: '0 60px',
    '& .ant-select-search': {
      '& .ant-input': {
        borderWidth: 0,
        borderRightWidth: '0 !important',
      },
    },
  },
})

export const EmptyTr = styled('tr')({
  height: '15px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})

export const InputWrapper = styled('div')({
  position: 'relative',
})

export const inputIcon = css({
  position: 'absolute',
  left: '10px',
  top: '0.4em',
})

export const inputCss = css({
  paddingRight: '10px',
  textAlign: 'right',
  width: '82px',
})

export const searchIconStyle = css({
  color: brightGreen,
  position: 'absolute',
  left: '340px',
})

export const Price = styled('span')({
  fontFamily: '"Museo Sans Rounded"',
  color: black,
  fontSize: '18px',
})

export const QBOImage = styled('img')({
  width: '24px',
  height: '24px',
})


export const PricingDetailWrapper = styled('div')<{ collapsed: Boolean, width: number }>((props: any) => ({
  display: props.collapsed ? 'none' : 'block',
  transition: 'all .3s, height 0s',
  position: 'relative',
  minWidth: 1200,
  // width: `${props.width}px`,
  width: '100%',
  // overflowX: 'auto',
  '& .ant-table tbody tr td:first-of-type': {
    color: `${props.theme.primary} !important`,
    minWidth: 150,
    whiteSpace: 'nowrap',
    // fontWeight: 'bold',
    '& svg': {
      fill: props.theme.theme,
    }
  },
  '& table tbody tr td': {
    background: '#FAFAFA'
  },
  '.product-name': {
    color: props.theme.dark,
    cursor: 'pointer'
  }
}))

export const tableWrapperStyle = (collapsed: boolean) =>
  css({
    display: collapsed ? 'none' : 'block',
    transition: 'all .3s, height 0s',
  })
