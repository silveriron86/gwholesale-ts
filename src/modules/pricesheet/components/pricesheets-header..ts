import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { white, black, brownGrey, transparent, brightGreen } from '~/common'

const headerHeight = '270px'

export const HeaderContainer = styled('div')({
  width: '100%',
  height: headerHeight,
  backgroundColor: white,
  padding: '50px 38px 98px 66px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const HeaderOptions = styled('div')((props) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: props.theme.primary,
    '&::after': {
      display: 'none',
    },
  },
}))

export const searchIconCss = css({
  marginRight: '10px',
})

export const buttonStyle = css({
  borderRadius: '200px',
  height: '40px',
  border: `1px solid ${brownGrey}`,
  margin: '0 11px',
  '&.ant-btn-primary': {
    backgroundColor: brightGreen,
    borderColor: brightGreen,
    width: '180px',
  },
  '&.ant-dropdown-trigger': {
    borderRadius: '5px',
    borderColor: brownGrey,
  },
  '& > a': {
    '&:visited': {
      color: white,
    },
  },
})
