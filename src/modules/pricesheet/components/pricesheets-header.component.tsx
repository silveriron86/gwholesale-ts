/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Input, Button, Icon, Dropdown, Menu } from 'antd'
import { Icon as IconSvg } from '~/components'

import {
  HeaderContainer,
  HeaderOptions,
  buttonStyle,
  ghostButtonStyle,
  SortByText,
  SortByIcon,
} from './pricesheets-header.style'
import { AuthUser, UserRole } from '~/schema'
import { ThemeButton, ThemeOutlineButton } from '~/modules/customers/customers.style'
import { searchButton } from '~/components/elements/search-button'
import { isSeller } from '~/common/utils'

export interface PriceSheetsHeaderProps {
  onSearch: (value: string) => void
  onShowModal: () => void
  currentUser: AuthUser
}

export class PriceSheetsHeader extends React.PureComponent<PriceSheetsHeaderProps> {
  private renderSortByMenu() {
    const sorts = ['Last Ordered', 'Name', 'Item', 'ID']
    return (
      <Menu style={{ paddingLeft: '8px' }}>
        {sorts.map((sort) => (
          <Menu.Item key={sort}>{sort}</Menu.Item>
        ))}
      </Menu>
    )
  }

  render() {
    const { currentUser } = this.props
    return (
      <HeaderContainer>
        <HeaderOptions>
          <Input.Search
            placeholder="Search Price Sheet"
            size="large"
            style={{ width: '479px', border: '0' }}
            onSearch={this.props.onSearch}
            enterButton={searchButton}
          />
          <div>
            {currentUser.accountType != null && isSeller(currentUser.accountType) ? (
              <ThemeButton size="large" icon="plus" type="primary" css={buttonStyle} onClick={this.props.onShowModal}>
                New Price Sheet
              </ThemeButton>
            ) : (
              <span></span>
            )}

            {currentUser.accountType != null && isSeller(currentUser.accountType) ? (
              <Dropdown overlay={this.renderSortByMenu()} placement="bottomCenter">
                <ThemeOutlineButton size="large" css={ghostButtonStyle}>
                  <SortByText>SORT BY</SortByText>
                  <Icon type="caret-down" style={SortByIcon} />
                </ThemeOutlineButton>
              </Dropdown>
            ) : (
              <span></span>
            )}
          </div>
        </HeaderOptions>
      </HeaderContainer>
    )
  }
}
