import styled from '@emotion/styled'
import { css } from '@emotion/core'
import { white, black, brownGrey, transparent, brightGreen } from '~/common'

export const HeaderContainer = styled('div')({
  width: '100%',
  backgroundColor: white,
  padding: '80px 80px 32px 89px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

export const HeaderTitle = styled('div')({
  width: '100%',
  color: black,
  fontSize: '36px',
  letterSpacing: '0.05em',
  lineHeight: '52px',
})

export const HeaderOptions = styled('div')((props) => ({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  '& .ant-input': {
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    '&:focus': {
      boxShadow: 'none',
    },
  },
  '& .ant-input-group-addon': {
    backgroundColor: transparent,
  },
  '& .ant-input-search-button': {
    backgroundColor: transparent,
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    color: props.theme.primary,
    '&::after': {
      display: 'none',
    },
  },
  '.clear-icon': {
    display: 'none'
  },
  '& .ant-input-search:hover .clear-icon.show': {
    display: 'block'
  }
}))

export const SortByText = styled('span')({
  color: `${brownGrey} !important`,
})

export const SortByIcon: React.CSSProperties = {
  color: `${brownGrey} !important`,
  width: '18px',
  height: '13px',
}

export const ghostButtonStyle = css({
  borderRadius: '5px',
  width: '130px',
  height: '35px',
  border: `1px solid ${brownGrey}`,
  backgroundColor: transparent,
  color: `${brownGrey} !important`,
  marginLeft: '39px',
})

export const buttonStyle = css({
  borderRadius: '200px',
  height: '40px',
  marginLeft: '160px',
  '&.ant-dropdown-trigger': {
    borderRadius: '5px',
  },
  '& > a': {
    '&:visited': {
      color: white,
    },
  },
})
