/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Link } from 'react-router-dom'
import { Icon, Button } from 'antd'
import { ColumnProps } from 'antd/es/table'

import { withTheme } from 'emotion-theming'

import { PriceSheet } from '~/schema'
import { tableCss,
  PriceSheetWrap, EmptyTr, PriceSheetId, PriceSheetName } from './pricesheets-table.style'
import { MutiSortTable, ResizeableTitle } from '~/components'
import { cloneDeep } from 'lodash'
import { white, Theme } from '~/common';
import { buttonStyle, ThemeLink, ThemeTable } from '~/modules/customers/customers.style';
import { rgba } from 'polished';
import { disabledButtonStyle } from '~/modules/customers/components/customer-detail/customer-detail.style';
import { format } from 'date-fns';

export interface PSTableProps {
  priceSheets: PriceSheet[]
  search: string
}

type PriceSheetsTableProps = PSTableProps & { theme: Theme }

export class PriceSheetsTableComponent extends React.PureComponent<PriceSheetsTableProps>
{
  render() {
    const { priceSheets, search, theme } = this.props

    let priceSheetList = cloneDeep(priceSheets)

    if (search) {
      priceSheetList = priceSheetList.filter((item) => (item.name).toUpperCase().includes(search.toUpperCase()))
    }

    const columns: ColumnProps<PriceSheet>[] = [
      {
        title: 'ID #',
        dataIndex: 'priceSheetId',
        key: 'priceSheetId',
        width: 120,
        sorter: (a, b) => a.priceSheetId - b.priceSheetId,
        render: (data) => (
          <PriceSheetId>
            <span>#{data}</span>
            <div />
          </PriceSheetId>
        ),
      },
      {
        title: 'NAME',
        dataIndex: 'name',
        key: 'name',
        width: 500,
        sorter: (a: PriceSheet, b: PriceSheet) => a.name.localeCompare(b.name),
        render(_text, priceSheet, _index) {
          return <PriceSheetName>{priceSheet.owner === true ? "": "Shared - "}{_text}</PriceSheetName>
        },
      },
      {
        title: 'LATEST ORDER',
        dataIndex: 'lastOrder.updatedDate',
        key: 'lastOrder.updatedDate',
        width: 150,
        render(text) {
          return text ? format(text, 'MM/D/YY') : ""
        }
      },
      {
        title: 'ASSIGNED',
        dataIndex: 'assignedCount',
        key: 'assignedCount',
        width: 100,
      },
      {
        title: 'ITEMS',
        dataIndex: 'itemCount',
        key: 'itemCount',
        width: 100,
      },
      {
        width: 30,
      },
      {
        width: 120,
        render(_text, priceSheet) {
          return (
            <Link to={`/order/pricesheet/${priceSheet.priceSheetId}/customer/0/new`}>
              <Button
                className="colorable"
                type="primary"
                style={{
                  // ...buttonStyle,
                  ...disabledButtonStyle,
                  backgroundColor: rgba(theme.primary, 0.25),
                  // width: 100,
                }}
              >
                New Order
              </Button>
            </Link>
          )
        },
      },
      {
        width: 90,
        // className: "view",
        render(priceSheet) {
          return (
            <Link to={`/pricesheet/${priceSheet.priceSheetId}`}>
              <Button
                className="colorable"
                type="primary"
                style={{
                  // ...buttonStyle,
                  ...disabledButtonStyle,
                  backgroundColor: rgba(theme.primary, 0.25),
                  // width: 100,
                }}
              >
                VIEW <Icon type="arrow-right" />
              </Button>
            </Link>
          )
        },
      }
    ]

    const components = {
      body: {
        wrapper: this.wrapper,
      },
      header: {
        cell: ResizeableTitle
      }
    }

    return (
      <PriceSheetWrap>
        <ThemeTable
          pagination={false}
          columns={columns}
          components={components}
          dataSource={priceSheetList}
          rowKey="priceSheetId"
          // css={tableCss(false)}
        />
      </PriceSheetWrap>
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody className="ant-table-tbody">
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}



export const PriceSheetsTable = withTheme(PriceSheetsTableComponent)
