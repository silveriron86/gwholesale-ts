import styled from '@emotion/styled'
import {css} from '@emotion/core'
import {css as createCss} from 'emotion'
import {
  transparent,
  yellow,
  mutedGreen,
  red,
  brightGreen,
  white,
  lightGrey,
  mediumGrey,
  black,
  darkGrey,
  mediumGrey2,
} from '~/common'
import {rgb} from 'polished'

export const TableTr = styled('tr')({
  height: '50px',
})

export const tableCss = (hasPadding: boolean) =>
  css({
    padding: hasPadding ? '0 50px' : '0',
    '.ant-pagination': {
      paddingRight: '30px',
    },
    '& .ant-table-content': {
      '& .ant-table-body': {
        '& > table': {
          borderTop: 'none',
          '& .ant-table-thead': {
            boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
            '& > tr > th:first-of-type': {},
            '& > tr > th:last-of-type': {},
            '& > tr > th': {
              backgroundColor: white,
              border: 'none',
            },
            '& > tr > th > div': {
              fontSize: '14px',
              color: mediumGrey,
            },
          },
          [`& ${TableTr} td`]: {
            padding: '3px 10px',
            textAlign: 'left',
            whiteSpace: 'pre-wrap',
            wordWrap: 'break-word',
          },
          '& .ant-table-row > td:first-of-type': {
            backgroundColor: white,
          },
          '& .ant-table-row > td:nth-of-type(7)': {
            borderTopRightRadius: '10px',
            borderBottomRightRadius: '10px',
          },
          '& .ant-table-row > td:nth-of-type(8)': {
            borderTopRightRadius: '15px',
            borderBottomRightRadius: '15px',
          },
          '& .ant-table-row > td:nth-of-type(9)': {
            backgroundColor: white,
          },
          '& .ant-table-row > td': {
            paddingLeft: '9px',
            borderBottom: '1px solid ${filterGreen}',
            paddingTop: '19px',
            paddingBottom: '14px',
          },
          '& .ant-table-row': {
            '& > .view': {
              display: 'none',
            },
          },
          '& .ant-table-row:hover': {
            '& > td:nth-of-type(7)': {
              borderTopRightRadius: '0px',
              borderBottomRightRadius: '0px',
            },
            '& > .view': {
              background: brightGreen,
              display: 'table-cell',
            },
          },
        },
      },
    },
  })

export const Category = styled('td')({
  position: 'absolute',
  width: '100vw',
  height: '50px',
  textAlign: 'center',
  fontSize: '24px',
  lineHeight: '20px',
  fontFamily: '"Museo Sans Rounded"',
})

export const CategoryTr = styled('tr')({
  height: '50px',
  [`&:hover:not(.ant-table-expanded-row) ${Category}`]: {
    background: transparent,
  },
})

export const columnClass = createCss({
  boxShadow: '6px 0 6px -4px rgba(0,0,0,0.15)',
})

export const Stock = styled('div')(({stock}: { stock: number }) => ({
  borderLeft: `solid 5px ${stock === 0 ? yellow : stock > 0 ? mutedGreen : red}`,
  height: '44px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  paddingLeft: '10px',
}))

export const polygonCss = css({
  width: '12px',
  height: '12px',
  marginLeft: '5px',
})

export const Quantity = styled('div')({
  display: 'flex',
  justifyContent: 'space-evenly',
  lineHeight: '40px',
})

export const PolygonWrapper = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-around',
  alignContent: 'center',
  height: '40px',
})

export const quantityPolygonCss = css({
  width: '12px',
  height: '12px',
  color: brightGreen,
  '&:nth-child(1)': {
    transform: 'rotate(180deg)',
  },
})

export const EmptyTr = styled('tr')({
  height: '15px',
  backgroundColor: 'transparent',
  td: {
    backgroundColor: 'transparent !important',
  },
})

export const TextWrapper = styled('span')(({len}: { len: number }) => ({
  paddingRight: len < 10 ? '6px' : '0',
}))

export const AddNewItem = styled('td')({
  position: 'absolute',
  width: '100vw',
  height: '55px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
})

export const AddNewItemTr = styled('tr')(
  ({isEditing}: { isEditing: boolean }) => ({
    [`& ${AddNewItem}`]: {
      backgroundColor: isEditing ? transparent : 'rgba(38, 172, 95, 0.2)',
    },
    [`&:hover:not(.ant-table-expanded-row) ${AddNewItem}`]: {
      backgroundColor: isEditing ? transparent : 'rgba(38, 172, 95, 0.2)',
    },
  }),
  {
    height: '55px',
  },
)

export const AddItemsWrapper = styled('div')(
  ({isEditing, theme}: { isEditing: boolean; theme: any }) => ({
    backgroundColor: isEditing ? theme.primary : 'transparent',
    color: isEditing ? white : theme.primary,
  }),
  {
    display: 'flex',
    fontSize: '16px',
    justifyContent: 'center',
    alignItems: 'center',
    userSelect: 'none',
    cursor: 'pointer',
    height: '55px',
    padding: '0 10px',
  },
)

export const PlusButton = styled('span')((props) => ({
  borderRadius: '50%',
  width: '20px',
  height: '20px',
  backgroundColor: props.theme.primary,
  fontSize: '20px',
  lineHeight: '20px',
  textAlign: 'center',
  color: white,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginRight: '10px',
}))

export const RadioWrapper = styled('div')({
  display: 'flex',
  padding: '0 40px',
  borderRight: `1px solid ${lightGrey}`,
  marginRight: '5px',
})

export const autocompleteCss = css({
  flexGrow: 1,
  position: 'relative',
  '& .ant-select-selection': {
    padding: '0 60px',
    '& .ant-select-search': {
      '& .ant-input': {
        borderWidth: 0,
        borderRightWidth: '0 !important',
      },
    },
  },
})

export const searchIconStyle = css({
  color: brightGreen,
  position: 'absolute',
  left: '340px',
})

export const Price = styled('span')({
  fontFamily: '"Museo Sans Rounded"',
  color: black,
  fontSize: '18px',
})

export const QBOImage = styled('img')({
  width: '24px',
  height: '24px',
})

export const ViewSpan = styled('div')((props) => ({
  color: props.theme.primary,
  fontSize: '14px',
  letterSpacing: '0.05em',
  cursor: 'pointer',

  '& > span': {
    marginRight: '9px',
  },
}))

export const PriceSheetId = styled('div')({
  fontSize: '18px',
  letterSpacing: '0.05em',
  color: black,
  position: 'relative',

  '& > div': {
    position: 'absolute',
    width: '112px',
    height: '112px',
    backgroundColor: mediumGrey2,
    zIndex: 0,
    opacity: 0.1,
    borderRadius: '500px',
    top: '-44px',
    left: '-70px',
  },

  '& > span': {
    zIndex: 10,
  },
})

export const PriceSheetWrap = styled('div')({
  '&.pricing-list': {
    paddingTop: 50,
    width: '80%',
    // margin: 'auto',
    minWidth: 1200,
  },
  '&.customer-pricing-list': {
    paddingTop: 50,
    width: '90%',
    // margin: 'auto',
    minWidth: 1200,
  },

  '& table': {
    overflow: 'hidden',
  },
  '& .ant-table-row-level-0': {
    height: '80px',
    borderTop: '10px solid #fff',
    borderBottom: '15px solid #fff',

    '& > td': {
      overflow: 'hidden',
      backgroundColor: '#fafafa',
    },

    '& > td:nth-of-type(2)': {
      overflow: 'hidden',
      borderTopLeftRadius: '20px',
      borderBottomLeftRadius: '20px',
    },
  },

  '& .ant-table-row-level-0:hover': {
    backgroundColor: transparent,
  },
})

export const ProductPriceWrapper = styled("div")(props => ({
  display: "flex",
  flexWrap: "wrap",
  paddingTop: "10px",
  textAlign: "left",
  '& .item': {
    fontSize: "12px",
    color: "#373d3f",
    "& .left": {
      width: "140px",
    },
    "& > div": {
      paddingBottom: "10px"
    }
  },
  "& .link": {
    width: "100%",
    textAlign: "left",
    paddingTop: "10px",
    "& a" : {
      color: props.theme.primary
    }
  }
}));

export const ListHeader = styled('div')((props: any) => ({
  width: '80%',
  margin: 'auto',
  minWidth: 1200,
  '& div': {
    fontWeight: 'bold',
    '&.small-col, &.med-col, &.flex-col': {
      cursor: 'pointer',
      display: 'flex',
      alignItems: 'flex-end',
      justifyContent: 'center',
      '&:hover': {
        // backgroundColor: 'rgba(120, 120, 120, 0.3)'
      }
    },
    '& .sort-arrow': {
      marginLeft: 6,
      '&.inactive path': {
        fill: lightGrey
      },

    },
    "& .inner-wrapper" : {
      height: "50px"
    }
  }
}))

export const Splitter = styled('div')((props: any) => ({
  height: 20,
  width: '100%',
  boxShadow: '0px 12px 14px -6px rgba(0,0,0,0.15)',
}))

export const ListBody = styled('div')((props: any) => ({
  '& div': {
    '&.small-col, &.med-col': {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 14,
      overflow: 'hidden',
      '&.pricesheet-id-col': {
        justifyContent: 'start',
        paddingLeft: 10
      },
      '& .gray-marker': {
        position: 'absolute',
        width: 112,
        height: 112,
        backgroundColor: 'rgb(162, 162, 162)',
        zIndex: 0,
        opacity: '0.1',
        borderRadius: 500,
        top: -24,
        left: -50,
      }
    },
    '&.flex-col': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'start',
      fontSize: 20,
    }
  },
  '& .inner-wrapper': {
    boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
    transition: '0.3s',
    borderRadius: 6,
    fontWeight: 'bold',
    position: 'relative',
    '& .btn-view': {
      position: 'absolute',
      right: -60,
      height: '100%',
      background: props.theme.primary,
      width: 100,
      borderTopRightRadius: 6,
      borderBottomRightRadius: 6,
      opacity: 0,
      visibility: 'hidden',
      transition: 'opacity 0.6s',
      justifyContent: 'center',
      alignItems: 'center',
      color: 'white',
      '& svg': {
        marginLeft: 10
      },

    },
    '&:hover': {
      boxShadow: '0 6px 12px 0 rgba(0,0,0,0.2)',
      cursor: 'pointer',
      '& .btn-view': {
        display: 'flex',
        opacity: 1,
        visibility: 'visible',
      }
    },
    '& .small-col': {},
    '& .flex-col': {}
  }
}))

export const PriceSheetName = styled('div')({
  fontSize: '16px',
  letterSpacing: '0.05em',
  color: darkGrey,
  fontWeight: 700,
  width: '900px',
})
