import React from 'react'
import { Modal, Button, Select, notification, Input, Popconfirm, Table } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'

import { GlobalState } from '~/store/reducer'
import { PriceSheetDispatchProps, PriceSheetStateProps, PriceSheetModule } from './pricesheet.module'
import { PriceSheetDetailHeader, PriceSheetAddModal } from './components'
import PriceSheetDetailTable from './components/pricesheet-detail-table.component'
import {
  CustomerSelector,
  CustomerSelectorTitle,
  selectStyles,
  DialogHeader,
  MessageBox,
  MessageBoxTitle,
  MessageBoxInput,
  AssignedClientWrapper
} from './pricesheet-detail.style'
import { Container, DialogFooter } from './components/pricesheet-detail-header.style'
import { Theme } from '~/common'
import { MessageType } from '~/components'
import { history } from '~/store/history'
import Form, { FormComponentProps } from 'antd/lib/form'
import { PriceItemFormProps } from './pricesheets.container'
import { NewOrderFormModal } from '../orders/components'
import { TempCustomer } from '~/schema'
import { PayloadIcon } from '~/components/elements/elements.style'
import { Icon as IconSvg } from '~/components/icon/'

export type PriceSheetDetailProps = PriceSheetDispatchProps &
  PriceSheetStateProps &
  RouteComponentProps<{ id: string }> & { theme: Theme }

export interface PriceSheetDetailState {
  showModal: boolean
  showDuplicate: boolean
  emailText: string
  search: string
  editingInfo: boolean
  editValues: {}
  selectedCustomer: string
  message: string
  type: MessageType | null
  showPrintModal: boolean
  isRemovedCats: boolean
}

interface FormFields {
  name: string
}

// page /pricesheet/:id
class PriceItemForm extends React.PureComponent<FormComponentProps<FormFields> & PriceItemFormProps> {
  render() {
    const {
      form: { getFieldDecorator },
    } = this.props
    return (
      <Modal
        visible={this.props.visible}
        onCancel={this.handleCancel}
        footer={[
          <DialogFooter key="plus">
            <Button
              type="primary"
              icon="plus"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              Duplicate
            </Button>
            <Button onClick={this.handleCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <label>Duplicated Price Sheet Name</label>
        <br />
        <Form.Item>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Price sheet name is required!' }],
          })(<Input placeholder="Enter duplicated price sheet name" style={{ width: '80%' }} />)}
        </Form.Item>
      </Modal>
    )
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onOk } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        onOk(values.name)
      }
    })
  }

  handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onCancel } = this.props
    onCancel()
    form.resetFields()
  }
}

export const PriceItemFormModal = Form.create<FormComponentProps<FormFields> & PriceItemFormProps>()(PriceItemForm)

export class PriceSheetDetailComponent extends React.PureComponent<PriceSheetDetailProps, PriceSheetDetailState> {
  column: ({ dataIndex: string; title?: undefined; key?: undefined; width?: undefined; render?: undefined } | { title: string; dataIndex: string; key: string; width: string; render: (text: number, record: any) => JSX.Element })[]
  constructor(props: any) {
    super(props)

    this.column = [
      {
        dataIndex: 'clientCompany.companyName',
      },
      {
        title: '',
        dataIndex: '',
        key: 'x',
        width: '10%',
        render: (text: number, record: any) =>{
           return <Popconfirm title="Sure to unassign?" onConfirm={() => this.onUnassignCustomer(record.clientId)}>
             <a>
               <PayloadIcon>
                 <IconSvg
                   type="close"
                   viewBox="0 0 20 28"
                   width={15}
                   height={14} />
               </PayloadIcon>
             </a>
           </Popconfirm>
        }
      }
    ]
  }
  addItemsCmp = React.createRef<PriceSheetAddModal>()

  state = {
    showModal: false,
    showDuplicate: false,
    emailText: '',
    search: '',
    editingInfo: false,
    editValues: {},
    selectedCustomer: '',
    message: '',
    type: null,
    showPrintModal: false,
    isRemovedCats: false,
  }

  componentDidMount() {
    const id = this.props.match.params.id
    this.props.getPriceSheetInfo(id)
    this.props.getPriceSheetItems(id)
    this.props.getSaleItems()
    this.props.getCustomers(id)
    if(this.props.clients.length == 0) {
      this.props.getAllCustomers()
    }
  }
  onUnassignCustomer = (clientId: number) => {
    console.log(clientId)
    this.props.unAssignCustomerFromPriceSheet(clientId)
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  onClickBack = () => {
    this.props.goBack()
  }

  onOpenModal = () => {
    this.setState({
      showModal: true,
    })
  }

  onOpenEmailModal = () => {
    this.props.onShowEmailModal()
    this.setState({
      emailText: '',
    })
  }

  onOpenManageCustomerModal = () => {
    console.log(this.props.clients)
    this.props.onShowManageCustomerModal()
  }

  onAssignClient = (clientId: number) => {
    const priceSheetId = +this.props.match.params.id
    this.props.assignPriceSheet({priceSheetId: priceSheetId, clientId: clientId})
  }

  onClickEditInfo = () => {
    this.setState({
      editingInfo: true,
    })
  }

  onClickCancelEditInfo = () => {
    this.setState({
      editingInfo: false,
      editValues: {},
    })
  }

  onChangeEditInput = (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    this.setState({
      editValues: {
        ...this.state.editValues,
        [channel]: value,
      },
    })
  }

  onClickSaveInfo = () => {
    this.props.updatePriceSheetInfo(this.state.editValues)
  }

  onSearch = (search: string) => {
    this.setState({
      search: search,
    })
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
    })
  }

  onShowDuplicate = () => {
    this.setState({
      showDuplicate: true,
    })
  }

  onCloseDuplicate = () => {
    this.setState({
      showDuplicate: false,
    })
  }

  duplicatePriceSheet = (priceSheetName: string) => {
    if (priceSheetName) {
      this.props.duplicatePriceSheet({priceSheetName: priceSheetName, priceSheetId: this.props.match.params.id})
    }
  }

  onCloseEmailModal = () => {
    this.props.closeEmailModal()
  }

  onCloseManageCustomerModal = () => {
    this.props.closeManageCustomerModal()
  }

  onAddItems = (addedItems: string[]) => {
    const params = {
      priceSheetId: this.props.match.params.id,
      assignItemListId: addedItems,
    }
    this.props.addPriceSheetItemsPriceSheet(params)
    this.onCloseModal()
  }

  onRemoveItem = (priceSheetItemId: string) => {
    if (this.props.loadIndex.length > 0) return
    const newList = []
    for (const item of this.props.priceSheetItems) {
      if (item.priceSheetItemId !== priceSheetItemId) newList.push(item.itemId)
    }
    const params = {
      priceSheetId: this.props.match.params.id,
      assignItemListId: newList
    }
    this.props.loadIndex(priceSheetItemId)
    this.props.assignPriceSheetItems(params)
  }

  onSelectCustomer = (value: string) => {
    this.setState({
      selectedCustomer: value,
    })
  }

  handleTextChange = (e: any) => {
    this.setState({ emailText: e.target.value })
  }

  sendEmail = () => {
    const customer = this.props.customers!.find((element) => element.customerId === this.state.selectedCustomer)
    this.props.setSending()
    this.props.sendEmail({
      customer: customer,
      priceSheetId: this.props.match.params.id,
      message: this.state.emailText,
    })
  }

  toggleCompanyShared = () => {
    this.props.toggleCompanyShared(this.props.match.params.id)
  }

  renderOptions() {
    const { currentPriceSheet } = this.props
    if (!currentPriceSheet || !currentPriceSheet.assignedClients) {
      return null
    }
    const customers = currentPriceSheet.assignedClients
    console.log(customers)
    return customers.map((customer: any) => {
      return (
        <Select.Option value={customer.clientId} key={customer.clientId}>
          {customer.clientCompany && customer.clientCompany.companyName ? customer.clientCompany.companyName : 'N/A'}
        </Select.Option>
      )
    })
  }

  getAvailableClients = () => {
    const { currentPriceSheet, clients } = this.props
    console.log(clients)
    const assignedClients = currentPriceSheet.assignedClients
    console.log(assignedClients)
    if (!assignedClients || !clients) return []
    const assignedIds: number[] = assignedClients.map(el=>el.clientId)
    console.log(assignedIds)
    const availableClients: TempCustomer[] = []
    clients.forEach(element => {
      if(assignedIds.indexOf(element.clientId) < 0) {
        console.log(element.clientId)
        availableClients.push(element)
      }
    });
    console.log(availableClients)
    return availableClients
  }

  onUpdateCatsView = () => {
    this.setState({
      isRemovedCats: !this.state.isRemovedCats
    });
  }

  render() {
    const availbleClients = this.getAvailableClients()
    const assignedClients = this.props.currentPriceSheet.assignedClients ? this.props.currentPriceSheet.assignedClients : []
    return (
      <div>
        <NewOrderFormModal
          visible={this.props.showManageCustomerModal}
          onOk={this.onAssignClient}
          onCancel={this.onCloseManageCustomerModal}
          theme={this.props.theme}
          clients={ availbleClients }
          fromPriceSheet={true}
          okButtonName="ASSIGN"
          isNewOrderFormModal={false}>
            <div>
              <label>Assigned Customers</label>
              <br/>
              <AssignedClientWrapper>
                <Table columns={this.column} dataSource={assignedClients} rowKey="clientId" showHeader={false}></Table>
              </AssignedClientWrapper>
            </div>
        </NewOrderFormModal>
        <PriceSheetAddModal
          ref={this.addItemsCmp}
          theme={this.props.theme}
          visible={this.state.showModal}
          onCancel={this.onCloseModal}
          onOk={this.onAddItems}
          saleItems={this.props.saleItems}
          priceSheetItems={this.props.priceSheetItems}
        />

        <PriceItemFormModal
          theme={this.props.theme}
          visible={this.state.showDuplicate}
          onOk={this.duplicatePriceSheet}
          onCancel={this.onCloseDuplicate}
        />
        <Modal
          visible={this.props.showEmailModal}
          onCancel={this.onCloseEmailModal}
          footer={[
            <DialogFooter key="footer">
              <Button
                key="plus"
                type="primary"
                icon="plus"
                style={{
                  border: `1px solid ${this.props.theme.primary}`,
                  backgroundColor: this.props.theme.primary,
                }}
                onClick={this.sendEmail}
                disabled={this.props.sendingEmail}
              >
                {!this.props.sendingEmail ? 'Send Email' : 'Sending..'}
              </Button>
              <Button onClick={this.onCloseEmailModal}>CANCEL</Button>
            </DialogFooter>,
          ]}
        >
          <DialogHeader>Email Price Sheet</DialogHeader>
          <CustomerSelector>
            <CustomerSelectorTitle>
              TO CUSTOMER
              <Select
                style={{ width: '335px' }}
                loading={!this.props.customers}
                placeholder="Select Customer..."
                css={selectStyles}
                onChange={this.onSelectCustomer}
              >
                {this.renderOptions()}
              </Select>
            </CustomerSelectorTitle>
          </CustomerSelector>
          <MessageBox>
            <MessageBoxTitle>MESSAGE</MessageBoxTitle>
            <MessageBoxInput autosize={{ minRows: 2, maxRows: 8 }} onChange={this.handleTextChange} />
          </MessageBox>
        </Modal>
        <Container>
          <PriceSheetDetailHeader
            match={this.props.match}
            onClickBack={this.onClickBack}
            editingInfo={this.state.editingInfo}
            onSaveItems={this.props.savePriceSheetItems}
            priceSheetInfo={this.props.currentPriceSheet}
            onChangeEditInput={this.onChangeEditInput}
            onClickCancelEditInfo={this.onClickCancelEditInfo}
            onClickEditInfo={this.onClickEditInfo}
            onClickSaveInfo={this.onClickSaveInfo}
            onOpenModal={this.onOpenModal}
            onOpenEmailModal={this.onOpenEmailModal}
            onOpenManageCustomerModal={this.onOpenManageCustomerModal}
            onShowDuplicate={this.onShowDuplicate}
            toggleCompanyShared={this.toggleCompanyShared}
            onSearch={this.onSearch}
            loadingIndex={this.props.loadingIndex}
            loadIndex={this.props.loadIndex}
            onPrint={this.handlePrint}
            isRemovedCats={this.state.isRemovedCats}
            onUpdateCatsView={this.onUpdateCatsView}
          />
        </Container>

        <PriceSheetDetailTable
          onCatogoryChange={this.props.updateCategory}
          priceSheetItems={this.props.priceSheetItems}
          priceSheetInfo={this.props.currentPriceSheet}
          search={this.state.search}
          onPriceSheetItemUpdate={this.props.updatePriceSheetItem}
          onRemoveItem={this.onRemoveItem}
          loadingIndex={this.props.loadingIndex}
          isRemovedCats={this.state.isRemovedCats}
        />
      </div>
    )
  }

  handlePrint = () => {
    const id = this.props.match.params.id
    history.push(`/pricesheet/${id}/print`)
  }
}

const mapStateToProps = (state: GlobalState) => state.priceSheet

export const PriceSheetDetail = withTheme(connect(PriceSheetModule)(mapStateToProps)(PriceSheetDetailComponent))
