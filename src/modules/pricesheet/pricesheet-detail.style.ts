import styled from '@emotion/styled'
import { css } from '@emotion/core'

import { white, lightGrey, grey, mediumGrey } from '~/common'
import TextArea from 'antd/lib/input/TextArea'

export const HeaderContainer = styled('div')({
  height: '448px',
  backgroundColor: white,
  width: '100%',
  boxSizing: 'border-box',
  position: 'relative',
})

export const HeaderTitle = styled('div')((props) => ({
  marginTop: '48px',
  marginLeft: '68px',
  color: props.theme.primary,
  fontSize: '24px',
  lineHeight: '26px',
  letterSpacing: '0.05em',
}))

export const OptionContainer = styled('div')({
  width: '100%',
  height: '87px',
  position: 'absolute',
  bottom: 0,
  left: 0,
  padding: '0 37px 0 27px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: white,
  boxSizing: 'border-box',
})

export const SearchInput = styled('input')({
  width: '394px',
  height: '50px',
  backgroundColor: white,
  border: `1px solid ${lightGrey}`,
  borderRadius: '5px',
  boxSizing: 'border-box',
})

export const FilterWrap = styled('div')({
  flexGrow: 1,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
})

export const ButtonWrap = styled('div')({
  display: 'flex',
})

export const FakeButton = styled('button')({
  height: '47px',
  padding: '0 18px',
  border: `1px solid ${lightGrey}`,
  display: 'flex',
  borderRadius: '5px',
  marginLeft: '18px',
  cursor: 'pointer',
})

export const BodyContainer = styled('div')({
  minHeight: '512px',
  width: '100%',
  backgroundColor: lightGrey,
  boxSizing: 'border-box',
})

export const CustomerSelector = styled('div')({
  display: 'flex',
})

export const CustomerSelectorTitle = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  color: grey,
  fontSize: '14px',
  lineHeight: '33px',
})

export const MessageBox = styled('div')({
  marginTop: '10px',
})

export const MessageBoxTitle = styled('div')({
  color: grey,
  fontSize: '14px',
  lineHeight: '33px',
})

export const MessageBoxInput = styled(TextArea)({})

export const DialogHeader = styled('div')({
  width: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'flex-end',
  //padding: '0px 0px 0px 22px',
  fontSize: '28px',
  color: mediumGrey,
  lineHeight: '30px',
  letterSpacing: '0.05em',
  fontWeight: 700,
})

export const AssignedClientWrapper = styled('div')({
  borderTop: '1px solid lightgrey',
  '& .ant-table-body td': {
    color: `${mediumGrey} !important`,
    fontWeight: 100
  }
})

export const selectStyles = css({
  width: '335px',
  '& .ant-select-selection': {
    border: 'none',
    borderBottom: '1px solid black',
    boxShadow: 'none',
    borderRadius: 0,
    '& > div': {
      margin: 0,
    },
    '&:hover': {
      border: 'none',
      borderBottom: '1px solid black',
    },
    '&:active': {
      boxShadow: 'none',
    },
  },
})

export const printModelCss = css({})
