import React, { useEffect, useMemo, useState } from 'react'
import { RouteComponentProps } from 'react-router'
import { ColumnProps } from 'antd/es/table'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'

import { PriceSheetItem } from '~/schema'
import { PriceSheetDispatchProps, PriceSheetStateProps, PriceSheetModule } from './pricesheet.module'
import { PrintView, Addon, PrintHeader } from './pricesheet-print.style'

import { Theme } from '~/common'
import { GlobalState } from '~/store/reducer'
import { Button, Table } from 'antd'
import { useLocation, useParams } from 'react-router'
import qs from 'qs'
import { printWindow } from '~/common/utils'
import { useLatest } from 'ahooks'
import { formatBn } from '~/common/mathjs'

const sorter = (a: string, b: string) => {
  a = a || ''
  b = b || ''
  return a.localeCompare(b)
}
const sorterNumber = (a: number, b: number) => {
  a = +a
  b = +b
  if (a < b) {
    return -1
  }
  if (a === b) {
    return 0
  }
  return 1
}

type PriceSheetDetailProps = PriceSheetDispatchProps &
  PriceSheetStateProps &
  RouteComponentProps<{ id: string }> & { theme: Theme; logo: string }

const PriceSheetDetailComponent: React.SFC<PriceSheetDetailProps> = (props) => {
  const { search } = useLocation()
  const { id } = useParams<{ id: string }>()
  const latestRef = useLatest(props)
  const { customerName } = qs.parse(search, { ignoreQueryPrefix: true })
  const { currentPriceSheet, sellerSetting, priceSheetItems } = props
  const [dataSource, setDataSource] = useState<PriceSheetItem[]>([])

  useEffect(() => {
    latestRef.current.resetPrintData()
    latestRef.current.getPriceSheetInfo(id) // currentPriceSheet
    latestRef.current.getPriceSheetItems(id) // priceSheetItems
    latestRef.current.getSellerSetting() // sellerSetting
  }, [id, latestRef])

  useEffect(() => {
    setDataSource(
      priceSheetItems.sort((a, b) => {
        const aCategoryName = a?.wholesaleCategory?.name || ''
        const bCategoryName = b?.wholesaleCategory?.name || ''
        const result = aCategoryName.localeCompare(bCategoryName)
        if (result !== 0) {
          return result
        }
        const aName = a?.variety || ''
        const bName = b?.variety || ''
        return aName.localeCompare(bName)
      }),
    )
  }, [priceSheetItems])

  const columns: ColumnProps<PriceSheetItem>[] = useMemo(() => {
    const {
      showPriceSheetBrand,
      showPriceSheetOrigin,
      showPriceSheetPacking,
      showPriceSheetSize,
      showPriceSheetWeight,
    } = sellerSetting?.company || {}
    const brandColumn = showPriceSheetBrand
      ? [
          {
            title: 'BRAND',
            key: 'brand',
            render: (record: any) => {
              if (!record.suppliers) return ''
              const currentSuppliers = record.suppliers.split(', ').map((el: string) => el.trim())
              if (currentSuppliers.length > 1) {
                return currentSuppliers.join(', ')
              } else if (currentSuppliers.length == 1) {
                return currentSuppliers[0]
              } else {
                return ''
              }
            },
          },
        ]
      : []

    const originColumn = showPriceSheetOrigin
      ? [
          {
            title: 'ORIGIN',
            dataIndex: 'origin',
            key: 'origin',
            sorter: (a: any, b: any) => sorter(a.origin, b.origin),
          },
        ]
      : []

    const packColumn = showPriceSheetPacking
      ? [
          {
            title: 'PACKING',
            dataIndex: 'packing',
            key: 'packing',
            sorter: (a: any, b: any) => sorter(a.packing, b.packing),
          },
        ]
      : []

    const sizeColumn = showPriceSheetSize
      ? [
          {
            title: 'SIZE',
            dataIndex: 'size',
            key: 'size',
          },
        ]
      : []

    const weightColumn = showPriceSheetWeight
      ? [
          {
            title: 'WEIGHT',
            dataIndex: 'weight',
            key: 'weight',
            sorter: (a: any, b: any) => sorterNumber(a.weight, b.weight),
          },
        ]
      : []
    return [
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        sorter: (a: any, b: any) => sorter(a.SKU, b.SKU),
      },
      {
        title: 'CATEGORY',
        dataIndex: 'wholesaleCategory.name',
        key: 'wholesaleCategory.name',
        sorter: (a: any, b: any) => sorter(a.wholesaleCategory?.name || '', b.wholesaleCategory?.name || ''),
      },
      {
        title: 'NAME',
        dataIndex: 'variety',
        key: 'variety',
        sorter: (a: any, b: any) => sorter(a.variety, b.variety),
      },
      ...sizeColumn,
      ...weightColumn,
      ...packColumn,
      ...brandColumn,
      ...originColumn,
      {
        title: 'SALE PRICE',
        dataIndex: 'salePrice',
        key: 'salePrice',
        sorter: (a: any, b: any) => sorterNumber(a.salePrice, b.salePrice),
        render: (text) => formatBn(text, { $: true }),
      },
    ]
  }, [sellerSetting?.company])

  return (
    <React.Fragment>
      <Addon>
        <Button type="primary" size="large" onClick={() => printWindow('PrintViewDiv')}>
          Print
        </Button>
      </Addon>
      <div id="PrintViewDiv">
        <PrintView>
          <h1 style={{ lineHeight: 'initial' }}>
            {currentPriceSheet?.isDefault
              ? `${customerName} - Customer Product List`
              : `Pricesheet: ${currentPriceSheet?.name || ''}`}
          </h1>
          {!currentPriceSheet?.isDefault && (
            <PrintHeader>
              <div>
                <p>
                  <i>{currentPriceSheet?.user?.company || ''}</i>
                </p>
              </div>
              <div>{props.logo && <img style={{ height: '100px' }} src={props.logo} />}</div>
            </PrintHeader>
          )}

          <Table pagination={false} rowKey="priceSheetItemId" dataSource={dataSource} columns={columns} />
        </PrintView>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.priceSheet,
    logo: state.setting.logo,
  }
}

const PriceSheetPrintView = withTheme(connect(PriceSheetModule)(mapStateToProps)(PriceSheetDetailComponent))

export default PriceSheetPrintView
