import styled from '@emotion/styled'
import { css } from '@emotion/core'

import { black } from '~/common'
import { white } from '~/common'
import { gap } from '~/common/styleVars'

export const tableWrapperStyle = (collapsed: boolean) =>
  css({
    display: collapsed ? 'none' : 'block',
    transition: 'all .3s, height 0s',
  })

export const tableCss = () =>
  css({
    marginTop: gap,
    '.ant-pagination': {
      paddingRight: '30px',
    },
    '& .ant-table-content > .ant-table-body': {
      '& > table': {
        marginBottom: '0px',
        paddingBottom: '12px',
        background: white,
        '& > tbody': {
          '& .ant-table-row': {
            borderBottom: '1px solid #d9d9d9',
            '& td:first-of-type': {
              borderRight: '0px',
              textAlign: 'unset',
            },
            '& td:nth-of-type(2)': {
              padding: '7px 7px 8px 0px',
            },
            '& td': {
              color: black,
              fontSize: '14px',
              lineHeight: '17px',
              fontWeight: 300,
              padding: '7px 7px 8px 22px',
              borderRight: '1px solid #EDF1EE',
            },
          },
        },
        '& .ant-table-thead': {
          boxShadow: '0px 7px 7px rgba(0, 0, 0, 0.15)',
          '& > tr > th:first-of-type': {
            padding: '1px 0px 15px 22px',
          },
          '& > tr > th:nth-of-type(2)': {
            padding: '0px 0px 15px 0px',
          },
          '& > tr > th': {
            backgroundColor: white,
            color: black,
            border: 'none',
            padding: '0px 0px 15px 27px',
            fontWeight: 700,
            fontSize: '12px',
            lineHeight: '14px',
            '& > div': {
              display: 'flex',
            },
          },
        },
      },
    },
  })

export const PrintView = styled.div({
  padding: gap * 3,
})

export const PrintHeader = styled.div({
  padding: '10px',
  marginBottom: '20px',
  marginTop: '10px',
  border: '1px solid #d9d9d9',
  color: black,
  display: 'flex',
  justifyContent: 'space-between',
})

export const Addon = styled.div({
  padding: gap * 1.5,
  display: 'flex',
  justifyContent: 'flex-end',
})
