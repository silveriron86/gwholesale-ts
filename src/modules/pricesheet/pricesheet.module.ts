import {
  Module,
  EffectModule,
  ModuleDispatchProps,
  Effect,
  StateObservable,
  Reducer,
  DefineAction,
} from 'redux-epics-decorator'
import produce from 'immer'
import { Observable, of } from 'rxjs'
import { map, switchMap, endWith, catchError, takeUntil } from 'rxjs/operators'
import { push, goBack } from 'connected-react-router'
import { Action } from 'redux-actions'
import { sortBy } from 'lodash'

import { PriceSheet, PriceSheetItem, SaleItem, SaleSection, CustomerPriceSheet } from '~/schema'
import { PriceSheetService } from './pricesheet.service'
import { GlobalState } from '~/store/reducer'
import { MessageType } from '~/components'
import { checkError, responseHandler, formatPriceSheetItem } from '~/common/utils'
import { CustomerService } from '../customers/customers.service'
import { SettingService } from '../setting/setting.service'

export enum PSLoading {
  PS_ITEMS_SAVE = 'ps_items_save',
  PS_SAVE = 'ps_save',
}

export interface PriceSheetStateProps {
  priceSheets: PriceSheet[]
  customerPriceSheets: CustomerPriceSheet[]
  priceSheetItems: PriceSheetItem[]
  saleItems: SaleItem[]
  currentPriceSheet: PriceSheet
  customers: any[] | null
  message: string
  description: string
  type: MessageType | null
  sendingEmail: boolean
  showEmailModal: boolean
  showManageCustomerModal: boolean
  loadingIndex: string
  clients: any[]
  sellerSetting: any
}

@Module('pricesheet')
export class PriceSheetModule extends EffectModule<PriceSheetStateProps> {
  defaultState = {
    priceSheets: [],
    priceSheetItems: [],
    customerPriceSheets: [],
    saleItems: [],
    currentPriceSheet: {} as PriceSheet,
    customers: [],
    message: '',
    description: '',
    type: null,
    sendingEmail: false,
    showEmailModal: false,
    showManageCustomerModal: false,
    loadingIndex: '',
    clients: [],
    sellerSetting: null
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(private readonly priceSheet: PriceSheetService, private readonly customer: CustomerService, private readonly setting: SettingService) {
    super()
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect()
  goto(action$: Observable<string>) {
    return action$.pipe(map((path) => push(path)))
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: '', description: '', type: null }
    },
  })
  resetNotif(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PriceSheetStateProps, action: Action<PriceSheet>) => {
      return {
        ...state,
        currentPriceSheet: action.payload,
      }
    },
  })
  getPriceSheetInfo(action$: Observable<string>) {
    return action$.pipe(
      switchMap((priceSheetId) =>
        this.priceSheet.getPriceSheetMainDetail(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: 'Updated Price Sheet Successfully', type: MessageType.SUCCESS }
    },
  })
  toggleCompanyShared(action$: Observable<string>) {
    return action$.pipe(
      switchMap((priceSheetId: string) =>
        this.priceSheet.toggleCompanyShared(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetInfo)(priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, { payload }: Action<any[]>) => {
      return { ...state, clients: payload }
    },
  })
  getAllCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser.userId).pipe(
          switchMap((data: any) => of(responseHandler(data).body.data)),
          takeUntil(this.dispose$),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, { payload }: Action<any>) => {
      let currentPriceSheet = { ...state.currentPriceSheet }
      console.log(payload)
      if (payload.id > 0 && payload.client) {
        currentPriceSheet.assignedClients.push(payload.client)
      }
      return {
        ...state,
        currentPriceSheet: currentPriceSheet,
        showManageCustomerModal: false,
        message: 'Assigned Customer Successfully',
        type: payload.id > 0 ? MessageType.SUCCESS : MessageType.ERROR,
      }
    },
    error_message: (state: PriceSheetStateProps) => {
      return {
        ...state,
        showManageCustomerModal: false,
        message: 'Assign PriceSheet Failed',
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  assignPriceSheet(
    action$: Observable<{ priceSheetId: number; clientId: number }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((data) =>
        this.customer.assignPriceSheet(data.priceSheetId, data.clientId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCustomers)(state$.value.priceSheet.currentPriceSheet.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        showManageCustomerModal: false,
        message: 'Assigned Customer Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: PriceSheetStateProps) => {
      return {
        ...state,
        showManageCustomerModal: false,
        message: 'Assign PriceSheet Failed',
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  unAssignCustomerFromPriceSheet(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((clientId) =>
        this.priceSheet
          .unAssignCustomerFromPriceSheet(state$.value.priceSheet.currentPriceSheet.priceSheetId, clientId)
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            endWith(
              this.createActionFrom(this.getPriceSheetInfo)(state$.value.priceSheet.currentPriceSheet.priceSheetId),
            ),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, { payload }: Action<any>) => {
      return { ...state, customers: payload }
    },
  })
  getCustomers(action$: Observable<string>) {
    return action$.pipe(
      switchMap((priceSheetId: string) =>
        this.priceSheet.getPriceSheetCustomers(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, action: Action<PriceSheetItem[]>) => {
      return {
        ...state,
        priceSheetItems: formatPriceSheetItem(action.payload),
      }
    },
  })
  getPriceSheetItems(action$: Observable<string>) {
    const processData = (data: PriceSheetItem[]) => {
      const sortedData = sortBy(data, 'wholesaleCategory.wholesaleSection.name')
      return sortedData
    }

    return action$.pipe(
      switchMap((priceSheetId) =>
        this.priceSheet.getPriceSheetItems(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(processData),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        saleItems: action.payload,
      }
    },
  })
  getSaleItems(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.priceSheet
          .getAllItems(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET')
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, action: Action<CustomerPriceSheet[]>) => {
      return {
        ...state,
        customerPriceSheets: action.payload,
      }
    },
  })
  getCustomersPriceSheets(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.priceSheet
          .getCustomersPricesheets(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, showEmailModal: true }
    },
  })
  onShowEmailModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, showManageCustomerModal: true }
    },
  })
  onShowManageCustomerModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, showManageCustomerModal: false }
    },
  })
  closeManageCustomerModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, showEmailModal: false }
    },
  })
  closeEmailModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, sendingEmail: true }
    },
  })
  setSending(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return {
        ...state,
        message: 'Price Sheet Email Sent',
        type: MessageType.SUCCESS,
        sendingEmail: false,
        showEmailModal: false,
      }
    },
  })
  sendEmail(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet.sendEmail(data.customer, data.priceSheetId, data.message).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, action: Action<{ [key: string]: string }>) => {
      const newValue = action.payload
      return {
        ...state,
        customer: {
          ...state.currentPriceSheet,
          ...newValue,
        },
        message: 'Price Sheet Info Saved',
        type: MessageType.SUCCESS,
      }
    },
  })
  updatePriceSheetInfo(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet
          .savePriceSheetInfo(
            state$.value.priceSheet.currentPriceSheet ? state$.value.priceSheet.currentPriceSheet.priceSheetId! : '0',
            data,
          )
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            endWith(
              this.createActionFrom(this.getPriceSheetInfo)(
                state$.value.priceSheet.currentPriceSheet
                  ? state$.value.priceSheet.currentPriceSheet.priceSheetId!
                  : '0',
              ),
            ),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: 'Create Price Sheet Successful', type: MessageType.SUCCESS }
    },
  })
  createPriceSheet(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((priceSheetName: string) =>
        this.priceSheet
          .createPriceSheet(priceSheetName, state$.value.currentUser ? state$.value.currentUser.userId! : '0')
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map((data) => push(`/pricesheet/${data}`)),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: 'Create Price Sheet Successful', type: MessageType.SUCCESS }
    },
  })
  createPriceSheetByName(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((priceSheetName: string) =>
        this.priceSheet
          .createPriceSheet(priceSheetName, state$.value.currentUser ? state$.value.currentUser.userId! : '0')
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map((data) => push(`/pricesheet/${data}`)),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: 'Duplicate Price Sheet Successful', type: MessageType.SUCCESS }
    },
  })
  duplicatePriceSheet(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet
          .duplicatePriceSheet(
            data.priceSheetName,
            state$.value.currentUser ? state$.value.currentUser.userId! : '0',
            data.priceSheetId,
          )
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(() => push(`/pricesheets`)),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: 'Saved Price Sheet Items Successfully', type: MessageType.SUCCESS, loadingIndex: '' }
    },
  })
  savePriceSheetItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((id: string) => {
        return this.priceSheet
          .savePriceSheetItems({
            priceSheetId: id,
            priceSheetItemList: state$.value.priceSheet.priceSheetItems,
          })
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          )
      }),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: 'Updated Price Sheet Successfully', type: MessageType.SUCCESS }
    },
  })
  assignPriceSheetItems(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet.assignPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetItems)(data.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps) => {
      return { ...state, message: 'Updated Price Sheet Successfully', type: MessageType.SUCCESS }
    },
  })
  addPriceSheetItemsPriceSheet(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet.addPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetItems)(data.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PriceSheetStateProps, action: Action<PriceSheet[]>) => {
      return {
        ...state,
        priceSheets: action.payload,
        loadingIndex: '',
      }
    },
  })
  getCompanyPriceSheets(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.priceSheet.getCompanyPriceSheets(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Reducer()
  resetPrintData(state: PriceSheetStateProps) {
    return {
      ...state,
      priceSheetItems: [],
      currentPriceSheet: {},
    }
  }

  @Reducer()
  loadIndex(state: PriceSheetStateProps, action: Action<string>) {
    return {
      ...state,
      loadingIndex: action.payload,
    }
  }

  @Reducer()
  updateCategory(state: PriceSheetStateProps, action: Action<SaleSection>) {
    const { name, wholesaleSectionId } = action.payload!
    return {
      ...state,
      priceSheetItems: state.priceSheetItems.map(
        produce((item) => {
          if (item.wholesaleCategory.wholesaleSection.wholesaleSectionId === wholesaleSectionId) {
            item.wholesaleCategory.wholesaleSection.name = name
          }
        }),
      ),
    }
  }

  @Reducer()
  updatePriceSheetItem(
    state: PriceSheetStateProps,
    action: Action<{ priceSheetItemId: string; data: Partial<PriceSheetItem> }>,
  ) {
    const { priceSheetItemId, data } = action.payload!

    return {
      ...state,
      priceSheetItems: state.priceSheetItems.map((priceSheetItem) => {
        if (priceSheetItem.itemId === priceSheetItemId) {
          return { ...priceSheetItem, ...data }
        }
        return priceSheetItem
      }),
    }
  }

  @Effect({
    done: (state: PriceSheetStateProps, action: Action<any>) => {
      return {
        ...state,
        sellerSetting: action.payload.userSetting,
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getUserSetting(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(map(this.createAction('done'))),
      ),
      catchError((error) => of(checkError(error))),
    )
  }
}

export type PriceSheetDispatchProps = ModuleDispatchProps<PriceSheetModule>
