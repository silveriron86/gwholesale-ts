import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import { PriceSheetItem, PriceSheet, SaleItem, PriceSheetClient, CustomerPriceSheet } from '~/schema'

@Injectable()
export class PriceSheetService {
  constructor(private readonly http: Http) {}

  getPriceSheetMainDetail(priceSheetId: string) {
    return this.http.get<PriceSheet>(`/pricesheet/${priceSheetId}`)
  }

  getPriceSheetItems(priceSheetId: string) {
    return this.http.get<PriceSheetItem[]>(`/pricesheet/${priceSheetId}/items/list`)
  }

  getAllPriceSheet(userId: string) {
    return this.http.get<PriceSheet[]>(`/pricesheet/user/${userId}`)
  }

  getCompanyPriceSheets(userId: string) {
    return this.http.get<PriceSheet[]>(`/pricesheet/userCompany/${userId}`)
  }

  getPriceSheetCustomers(priceSheetId: string) {
    return this.http.get<PriceSheetClient[]>(`/pricesheet/${priceSheetId}/users/list`)
  }

  getAllItems(companyName: string) {
    return this.http.get<SaleItem[]>(`/inventory/${companyName}/items/list?type=PRICESHEET`)
  }

  createPriceSheet(priceSheetName: string, userId: string) {
    return this.http.post<any>(`/pricesheet/user/${userId}/new/${priceSheetName}`)
  }

  duplicatePriceSheet(priceSheetName: string, userId: string, priceSheetId: string) {
    return this.http.post<any>(`/pricesheet/user/${userId}/duplicate/${priceSheetId}/${priceSheetName}`)
  }

  savePriceSheetInfo(priceSheetId: string, data: any) {
    return this.http.post<any>(`/pricesheet/${priceSheetId}/update/${data.name}`, {
      body: JSON.stringify(data),
    })
  }

  sendEmail(customer: any, priceSheetId: string, message: string = '') {
    const data = {
      recipientEmail: customer.emailAddress,
      message: message,
      clientId: customer.priceSheetClientId,
    }
    return this.http.post<any>(`/pricesheet/${priceSheetId}/sendEmail`, {
      body: JSON.stringify(data),
    })
  }

  savePriceSheetItems(data: any) {
    return this.http.post<any>('/pricesheet/update/item', {
      body: JSON.stringify(data),
    })
  }

  assignPriceSheetItems(data: any) {
    return this.http.post<any>('/pricesheet/assign/item', {
      body: JSON.stringify(data),
    })
  }

  addPriceSheetItems(data: any){
    return this.http.post<any>('/pricesheet/add/items', {
      body: JSON.stringify(data),
    })
  }

  getCustomersPricesheets(userId: string) {
    return this.http.get<CustomerPriceSheet>(`/account/user/${userId}/customer/pricesheets/list`)
  }

  toggleCompanyShared(priceSheetId: string) {
    return this.http.post<any>(`/pricesheet/toggleCompanyShared/${priceSheetId}`)
  }

  unAssignCustomerFromPriceSheet(priceSheetId: string, clientId: number) {
    return this.http.delete<any>(`/pricesheet/unassign/${priceSheetId}/client/${clientId}`)
  }
}
