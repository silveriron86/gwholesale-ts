import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { Form, Modal, Button, Input } from 'antd'
import { withTheme } from 'emotion-theming'
import { FormComponentProps } from 'antd/lib/form'

import { GlobalState } from '~/store/reducer'
import { PriceSheetDispatchProps, PriceSheetStateProps, PriceSheetModule } from './pricesheet.module'

import { PriceSheetsHeader, PriceSheetsTable, CustomerPriceSheetsTable } from './components'
import { DialogFooter } from './components/pricesheet-detail-header.style'
import { Theme } from '~/common'
import { AuthUser, UserRole } from '~/schema'
import PageLayout from '~/components/PageLayout'
import { ListHeader, Splitter } from './components/pricesheets-table.style'
import { CustomerPriceSheetListRow } from './components/customer-pricesheet-list-item'
import { isSeller } from '~/common/utils'

export type PriceSheetsProps = PriceSheetDispatchProps &
  PriceSheetStateProps &
  RouteComponentProps & { theme: Theme; currentUser: AuthUser }

export interface PriceSheetsState {
  search: string
}

interface FormFields {
  name: string
}

export interface PriceItemFormProps {
  visible: boolean
  onOk: (name: string) => void
  onCancel: () => void
  theme: Theme
}

class PriceItemForm extends React.PureComponent<FormComponentProps<FormFields> & PriceItemFormProps> {
  render() {
    const {
      form: { getFieldDecorator },
    } = this.props
    return (
      <Modal
        visible={this.props.visible}
        onCancel={this.handleCancel}
        footer={[
          <DialogFooter key="plus">
            <Button
              type="primary"
              icon="plus"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              Create
            </Button>
            <Button onClick={this.handleCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <label>Price Sheet Name</label>
        <br />
        <Form.Item>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Price sheet name is required!' }],
          })(<Input placeholder="Enter price sheet name" style={{ width: '80%' }} />)}
        </Form.Item>
      </Modal>
    )
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onOk } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        onOk(values.name)
      }
    })
  }

  handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onCancel } = this.props
    onCancel()
    form.resetFields()
  }
}

const PriceItemFormModal = Form.create<FormComponentProps<FormFields> & PriceItemFormProps>()(PriceItemForm)

export class PriceSheetsComponent extends React.PureComponent<
  PriceSheetsProps,
  {
    search: string
    showModal: boolean
    sortKey: string
    sortDirection: string
  }
> {
  state = {
    search: '',
    showModal: false,
    sortKey: 'priceSheetId',
    sortDirection: 'asc',
  }

  componentDidMount() {
    if (isSeller(this.props.currentUser.accountType)) {
      this.props.getAllCustomers()
      this.props.getCompanyPriceSheets()
    } else if (this.props.currentUser.accountType === UserRole.CUSTOMER) {
      this.props.getCustomersPriceSheets()
    }
  }

  onSearch = (search: string) => {
    this.setState({
      search: search,
    })
  }

  onShowModal = () => {
    this.setState({
      showModal: true,
    })
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
    })
  }

  onAddPriceSheet = (priceSheetName: string) => {
    if (priceSheetName) {
      this.props.createPriceSheetByName(priceSheetName)
    }
  }

  resortData = (sortKey: string, sortDirection: string) => {
    this.setState({ sortKey, sortDirection })
  }

  render() {
    const { currentUser } = this.props
    const { sortKey, sortDirection, search } = this.state
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Pricesheets'}>
        <PriceSheetsHeader onSearch={this.onSearch} onShowModal={this.onShowModal} currentUser={currentUser} />
        <div style={{ marginLeft: 89, paddingBottom: 20 }}>
          {isSeller(this.props.currentUser.accountType) ? (
            <PriceSheetsTable priceSheets={this.props.priceSheets} search={this.state.search} />
          ) : (
            <>
              <ListHeader style={{marginLeft: 0}}>
                <CustomerPriceSheetListRow
                  key={-1}
                  isHeader={true}
                  sortByKeyAndDirection={this.resortData}
                  history={this.props.history}
                />
              </ListHeader>
              <Splitter />
              <CustomerPriceSheetsTable
                priceSheets={this.props.customerPriceSheets}
                search={search}
                sortKey={sortKey}
                sortDirection={sortDirection}
                history={this.props.history}
              />
            </>
          )}
        </div>
        <PriceItemFormModal
          theme={this.props.theme}
          visible={this.state.showModal}
          onOk={this.onAddPriceSheet}
          onCancel={this.onCloseModal}
        />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.priceSheet, currentUser: state.currentUser }
}

export const PriceSheets = withTheme(connect(PriceSheetModule)(mapStateToProps)(PriceSheetsComponent))
