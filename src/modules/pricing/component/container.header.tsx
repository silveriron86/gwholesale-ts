/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Icon, Input, Menu } from 'antd'
import { Icon as IconSvg } from '~/components'


import { AuthUser, UserRole } from '~/schema';
import { ThemeButton } from '~/modules/customers/customers.style'
import { HeaderContainer, HeaderOptions, buttonStyle } from '~/modules/pricesheet/components/pricesheets-header.style';

export interface PricingContainerHeaderProps {
  onSearch: (value: string) => void
  onShowModal: () => void
  search?: string
}

export class PricingContainerHeader extends React.PureComponent<PricingContainerHeaderProps> {

  state = {
    search: this.props.search
  }

  render() {
    const searchButton = (
      <React.Fragment>
        <IconSvg viewBox="0 0 16 16" width="16" height="16" type="search" />
        <span style={{ marginLeft: '9px', fontSize: '14px', lineHeight: '15px', letterSpacing: '0.05em' }}>SEARCH</span>
      </React.Fragment>
    )
    return (
      <HeaderContainer>
        <HeaderOptions>
          <Input.Search
            placeholder="Search Pricing..."
            size="large"
            style={{ width: '479px', border: '0' }}
            onSearch={this.props.onSearch}
            enterButton={searchButton}
            value={this.state.search}
            onChange={(evt) => this.setState({ search: evt.target.value })}
            allowClear
          />
          <div>
            <ThemeButton size="large" icon="plus" type="primary" css={buttonStyle} onClick={this.props.onShowModal}>
              New Price Sheet
            </ThemeButton>
          </div>
        </HeaderOptions>

      </HeaderContainer>
    )
  }
}
