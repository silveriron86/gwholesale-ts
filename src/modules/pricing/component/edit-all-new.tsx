/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { EditAllWrapperNew } from './../pricing.style'
import { Popover } from 'antd'
import { ThemeInput, ThemeButton, ThemeOutlineButton } from '~/modules/customers/customers.style'

interface Props {
  onSaveAll: Function
  type: string
}

export class EditAll extends React.PureComponent<Props> {
  state = {
    visible: false,
    newValue: '',
  }

  handleVisibleChange = () => {
    this.setState({ visible: !this.state.visible })
  }

  newMarginChange = (evt: any) => {
    this.setState({ newValue: evt.target.value })
  }

  cancelPopover = () => {
    this.setState({ visible: false })
  }

  saveNewValue = () => {
    this.props.onSaveAll(this.props.type, this.state.newValue)
    this.setState({ visible: false })
  }

  getPopoverContent = () => {
    const { newValue } = this.state
    return (
      <div>
        <ThemeInput type="number" style={{ marginBottom: 20 }} value={newValue} onChange={this.newMarginChange} />
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <ThemeOutlineButton shape="round" style={{ marginRight: 10 }} onClick={this.cancelPopover}>
            Cancel
          </ThemeOutlineButton>
          <ThemeButton shape="round" onClick={this.saveNewValue}>
            Save
          </ThemeButton>
        </div>
      </div>
    )
  }

  render() {
    const { visible } = this.state
    const { type } = this.props

    return (
      <Popover
        placement="top"
        content={this.getPopoverContent()}
        trigger="click"
        title={`Please input the new ${type} value here`}
        visible={visible}
        onVisibleChange={this.handleVisibleChange}
      >
        <EditAllWrapperNew>Edit All</EditAllWrapperNew>
      </Popover>
    )
  }
}
