/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { EditAllWrapper } from './../pricing.style'
import jQuery from 'jquery'
import { Popover } from 'antd'
import { ThemeInput, ThemeButton, ThemeOutlineButton } from '~/modules/customers/customers.style'

interface Props {
  onSaveAll: Function
  type: string
}

export class EditAll extends React.PureComponent<Props> {
  state = {
    marginHeaderPos: [100, location.hash.includes('pricing') ? 250 : 414],
    markupHeaderPos: [100, 310],
    visible: false,
    newValue: ''
  }

  componentDidMount() {
    // console.log(location)
    // setTimeout(()=> {
    //   this.getMarginColumnWidthAndLeft()
    //   this.getMarkUpColumnWidthAndLeft()
    // }, 1000)
    // let that = this
    // jQuery(window).resize(function(){
    //   that.getMarginColumnWidthAndLeft()
    //   that.getMarkUpColumnWidthAndLeft()
    // })
  }

  getMarginColumnWidthAndLeft = () => {
    const marginHeader = jQuery('.ant-table th.pricesheet-detail-margin')
    //update the position info when resized change bigger than 20 to prevent the state maximum update error
    if (marginHeader && marginHeader.offset() && Math.abs(this.state.marginHeaderPos[1] - marginHeader.offset().left) > 20) {
      this.setState({ marginHeaderPos: [marginHeader.outerWidth(), marginHeader.offset().left] })
    }
  }

  getMarkUpColumnWidthAndLeft = () => {
    const markupHeader = jQuery('.ant-table th.pricesheet-detail-markup')
    //update the position info when resized change bigger than 20 to prevent the state maximum update error
    if (markupHeader && markupHeader.offset() && Math.abs(this.state.marginHeaderPos[1] - markupHeader.offset().left) > 20) {
      this.setState({ markupHeaderPos: [markupHeader.outerWidth(), markupHeader.offset().left] })
    }
  }

  handleVisibleChange = () => {
    this.setState({ visible: !this.state.visible })
  }

  newMarginChange = (evt: any) => {
    this.setState({ newValue: evt.target.value })
  }

  cancelPopover = () => {
    this.setState({ visible: false })
  }

  saveNewValue = () => {
    this.props.onSaveAll(this.props.type, this.state.newValue)
    this.setState({ visible: false })
  }

  getPopoverContent = () => {
    const { newValue } = this.state
    return <div>
      <ThemeInput type='number' style={{ marginBottom: 20 }} value={newValue} onChange={this.newMarginChange} />
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <ThemeOutlineButton shape='round' style={{ marginRight: 10 }} onClick={this.cancelPopover}>Cancel</ThemeOutlineButton>
        <ThemeButton shape='round' onClick={this.saveNewValue}>Save</ThemeButton>
      </div>
    </div>
  }

  render() {
    const { marginHeaderPos, markupHeaderPos, visible } = this.state
    const { type } = this.props
    let location = type == 'margin' ? marginHeaderPos : markupHeaderPos
    const style = { display: location[1] == 0 ? 'none' : 'block', right: location[1] - 40, minWidth: location[0] - 1 }

    return (
      <Popover
        placement="top"
        content={this.getPopoverContent()}
        trigger="click"
        title={`Please input the new ${type} value here`}
        visible={visible}
        onVisibleChange={this.handleVisibleChange}
      >
        <EditAllWrapper style={style}>
          Edit All
        </EditAllWrapper>
      </Popover>
    )
  }
}
