/**@jsx jsx */
import React from 'react'
import { jsx, css } from '@emotion/core'
import { BatchEditView, BatchEditModalContainer } from '../../pricing.style'
import {
  ThemeOutlineButton,
  ThemeButton,
  InputLabel,
  ThemeSelect,
  ThemeModal,
} from '~/modules/customers/customers.style'
import { Select, Row, Col } from 'antd'
import PricingOptions from '~/components/pricing-group-options'
import { Icon } from '~/components/icon'

const { Option } = Select

type Props = {
  onClickSave: Function
  onCancelEdit: Function
  isDefault?: boolean
}

class BatchEdit extends React.PureComponent<Props> {
  state = {
    pricingLogic: '',
    pricingGroup: '',
    confirmModal: false,
  }

  componentDidMount() {
    console.log('batch edit modal')
  }

  onClickSave = () => {
    if (this.state.pricingLogic != '') {
      this.setState({ confirmModal: true })
    }
    this.props.onCancelEdit()
  }

  onClickCancel = () => {
    this.props.onCancelEdit()
  }

  onChangePricingLogic = (val: string) => {
    this.setState({ pricingLogic: val })
  }

  onConfirmModalClose = () => {
    this.setState({ confirmModal: false })
  }

  onConfirmModalOk = () => {
    this.setState({ confirmModal: false })
    let logic = this.state.pricingLogic

    this.props.onClickSave({ defaultLogic: logic, defaultGroup: this.state.pricingGroup })
  }

  onGroupSelectionChanged = (val: string) => {
    this.setState({ pricingGroup: val })
  }

  render() {
    const { confirmModal, pricingLogic } = this.state
    return (
      <BatchEditView>
        <Row>
          <Col offset={1} md={23}>
            <div className="title">BATCH EDIT PRICING FOR ALL PRODUCTS</div>
          </Col>
        </Row>
        {/* <div className='logic-options'>
        <InputLabel style={{fontWeight: 'bold'}}>Default Sales Price</InputLabel>
          <ThemeSelect style={{ width: '100%' }} onChange={this.setPricingType} value={pricingLogic}>
            <Option value='LAST_SOLD'>Follow Last Sold Price</Option>
            <Option value='GROUP'>Follow Product Manager Price</Option>
            <Option value='DEFAULT_SALES'>Follow Default Sales Price</Option>
            <Option value='AVERAGE_SOLD'>Follow Average Sold Price</Option>
            <Option value='LOT_SUGGEST_SALE_PRICE'>Follow Lot Suggest Sales Price</Option>
          </ThemeSelect>
        </div> */}
        <PricingOptions
          logicTitle={'Product Pricing Strategy'}
          groupTitle={'Default Pricing Group'}
          isDefault={this.props.isDefault}
          defaultGroup={''}
          defaultLogic={''}
          onSelectedGroupChange={this.onGroupSelectionChanged}
          onSelectedLogicChange={this.onChangePricingLogic}
        />
        <Row>
          <Col offset={1} md={23}>
            <div>
              <ThemeButton shape="round" onClick={this.onClickSave}>
                SAVE CHANGES
              </ThemeButton>
              <ThemeOutlineButton shape="round" onClick={this.onClickCancel}>
                CANCEL
              </ThemeOutlineButton>
              {confirmModal && (
                <ThemeModal
                  visible={confirmModal}
                  onCancel={this.onConfirmModalClose}
                  onOk={this.onConfirmModalOk}
                  okButtonProps={{ shape: 'round', style: { marginRight: 10 } }}
                  cancelButtonProps={{ shape: 'round' }}
                  cancelText="NO, CANCEL"
                  okText="YES, SAVE CHANGES"
                  className="batch-edit-confirm"
                >
                  <BatchEditModalContainer>
                    <Row>
                      <Icon type="warning-circle" viewBox="0 0 31 31" width="31" height="31" />
                    </Row>
                    <Row>
                      <h2>Are you sure?</h2>
                      <p>Updaing this price logic will change the pricing for all products in this price sheet</p>
                    </Row>
                  </BatchEditModalContainer>
                </ThemeModal>
              )}
            </div>
          </Col>
        </Row>
      </BatchEditView>
    )
  }
}

export default BatchEdit
