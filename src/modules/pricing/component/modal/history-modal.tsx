/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { Input, Row, Col, Icon } from 'antd'
import { Icon as IconSvg } from '~/components'
import { PricesheetPricingTable, PriceHistoryTableWrapper } from '../../pricing.style';
import { PricingStateProps, PricingDispatchProps, PricingModule } from '../../pricing.module';
import { GlobalState } from '~/store/reducer';
import { withTheme } from 'emotion-theming';
import { connect } from 'redux-epics-decorator';
import moment from 'moment';
import { HeaderOptions } from '~/modules/pricesheet/components/pricesheet-detail-header.style';
import { ThemeTable } from '~/modules/customers/customers.style'

const searchButton = (
  <React.Fragment>
    <IconSvg viewBox="0 0 16 16" width="16" height="16" type="search" />
    <span style={{ marginLeft: '9px', fontSize: '14px', lineHeight: '15px', letterSpacing: '0.05em' }}>SEARCH</span>
  </React.Fragment>
)

type Props = {
  priceSheetId: string
  getPriceSheetHistory: Function
  priceSheetHistory: any[]
}

class PriceHistory extends React.PureComponent<Props> {
  multiSortTable = React.createRef<any>()

  state = {
    search: ''
  }

  column = [
    {
      title: 'SKU',
      dataIndex: 'sku',
      key: 'sku',
      align: 'center',
      sorter: (a: any, b: any) => {
        const sortIndex = this.multiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'sku')
        if (sortIndex && sortIndex.sortOrder == 'descend') return this.onSortString('sku', b, a)
        return this.onSortString('sku', a, b)
      },
    },
    {
      title: 'NAME',
      dataIndex: 'variety',
      key: 'variety',
      align: 'center',
      width: '40%',
      sorter: (a: any, b: any) => {
        const sortIndex = this.multiSortTable.current!.state.sortIndex.find(
          (meta: any) => meta.dataIndex === 'variety',
        )
        if (sortIndex && sortIndex.sortOrder == 'descend')
          return this.onSortVariety(a, b)
        return this.onSortVariety(b, a)
      },
    },
    {
      title: 'GROUP',
      dataIndex: 'group',
      key: 'group',
      align: 'center',
      sorter: (a: any, b: any) => {
        const sortIndex = this.multiSortTable.current!.state.sortIndex.find(
          (meta: any) => meta.dataIndex === 'group',
        )
        if (sortIndex && sortIndex.sortOrder == 'descend')
          return this.onSortString('group', a.wholesaleCategory, b.wholesaleCategory)
        return this.onSortString('group', b.wholesaleCategory, a.wholesaleCategory)
      },
    },
    {
      title: 'OLD PRICE',
      dataIndex: 'oldPrice',
      key: 'oldPrice',
      align: 'center',
      sorter: (a: any, b: any) => a.oldPrice - b.oldPrice,
    },
    {
      title: 'NEW PRICE',
      dataIndex: 'newPrice',
      key: 'newPrice',
      align: 'center',
      sorter: (a: any, b: any) => a.newPrice - b.newPrice,
    },
    {
      title: 'EDIT BY',
      dataIndex: 'editBy',
      key: 'editBy',
      align: 'center',
      sorter: (a: any, b: any) => {
        const sortIndex = this.multiSortTable.current!.state.sortIndex.find(
          (meta: any) => meta.dataIndex === 'editBy',
        )
        if (sortIndex && sortIndex.sortOrder == 'descend')
          return this.onSortString('editBy', a.editBy, b.editBy)
        return this.onSortString('editBy', b.editBy, a.editBy)
      },
    },
    {
      title: 'EDIT TIME',
      dataIndex: 'createdDate',
      key: 'createdDate',
      align: 'center',
      sorter: (a: any, b: any) => +new Date(a.createdDate) - +new Date(b.createdDate),
      render: (value: number) => {
        return moment(value).format('MM/DD/YYYY h:mm A')
      }
    }    
  ]

  componentDidMount() {
    console.log(this.props.priceSheetId)
    this.props.getPriceSheetHistory(this.props.priceSheetId)
  }

  onSortVariety = (a: any, b: any) => {
    let stringA = a.variety ? a.variety : ''
    let stringB = b.variety ? b.variety : ''
    stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }


  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
}

  onSearch = (value: string) => {
    if(this.state.search != value){
      this.setState({
        search: value,
      })
    }
  }

  render() {
    const { priceSheetHistory } = this.props
    console.log(priceSheetHistory)
    return (
      <PricesheetPricingTable>
        <Row type='flex' justify='space-between'>
          <Col span={14}>
            <HeaderOptions>
              <Input.Search
                placeholder="Search item name, category, or provider"
                size="large"
                style={{ border: '0' }}
                onSearch={this.onSearch}
                enterButton={searchButton}
              />
            </HeaderOptions>
          </Col>
        </Row>
        <Row>
          <PriceHistoryTableWrapper>
            <ThemeTable
              ref={this.multiSortTable}
              style={{ paddingLeft: 0, paddingRight: 0 }}
              pagination={false}
              columns={this.column}
              dataSource={priceSheetHistory}
              rowKey="priceSheetItemId"
            />
          </PriceHistoryTableWrapper>
        </Row>
      </PricesheetPricingTable>
    )
  }
}


export default PriceHistory

