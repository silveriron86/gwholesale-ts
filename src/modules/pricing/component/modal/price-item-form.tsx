import React from 'react'
import { Modal, Button, Input } from 'antd'

import Form, { FormComponentProps } from 'antd/lib/form'
import { DialogFooter } from '~/modules/pricesheet/components/pricesheet-detail-header.style'
import { PriceItemFormProps } from '~/modules/pricesheet/pricesheets.container'

interface FormFields {
  name: string
}
  
class PriceItemForm extends React.PureComponent<FormComponentProps<FormFields> & PriceItemFormProps> {
  render() {
    const {
      form: { getFieldDecorator },
    } = this.props
    return (
      <Modal
        visible={this.props.visible}
        onCancel={this.handleCancel}
        footer={[
          <DialogFooter key="plus">
            <Button
              type="primary"
              icon="plus"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              CREATE
            </Button>
            <Button onClick={this.handleCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <label>Price Sheet Name</label>
        <br />
        <Form.Item>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Price sheet name is required!' }],
          })(<Input placeholder="Enter duplicated price sheet name" style={{ width: '80%' }} />)}
        </Form.Item>
      </Modal>
    )
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onOk } = this.props
    form.validateFields((err: any, values: any) => {
      if (!err) {
        onOk(values.name)
      }
    })
  }

  handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { form, onCancel } = this.props
    onCancel()
    form.resetFields()
  }
}

export const PriceItemFormModal = Form.create<FormComponentProps<FormFields> & PriceItemFormProps>()(PriceItemForm)
  