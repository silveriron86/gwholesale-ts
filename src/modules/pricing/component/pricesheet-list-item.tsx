/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { DropdownText, ListItemRow } from './../pricing.style'
import { Dropdown, Icon, Row, Menu } from 'antd'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { PriceSheet, TempCustomer } from '~/schema'
import { Icon as IconSvg } from '~/components/icon'
import { history } from '~/store/history'


interface Props {
  isHeader: boolean
  currentPriceSheet?: PriceSheet
  sortByKeyAndDirection?: Function
  defaultUrl?: string
  clientId?: string
}

export class PriceSheetListRow extends React.PureComponent<Props> {
  state = {
    isMenuOpen: false,
    sortKey: 'priceSheetId',
    sortDirection: 'asc'
  }

  onMenuVisibleChange = (visible: any) => {
    console.log(visible)
  }

  renderCustomers = () => {
    const { currentPriceSheet } = this.props
    const { isMenuOpen } = this.state

    if (!currentPriceSheet || !currentPriceSheet.assignedClients) return []
    const assignedClients: TempCustomer[] = currentPriceSheet.assignedClients
    let result: any[] = []
    assignedClients.forEach((el, index) => {
      result.push(
        <Menu.Item key={index}>
          <div>{el.clientCompany.companyName}</div>
        </Menu.Item>
      )
    });
    let dropdown: any = []
    if (result.length > 0) {
      const menu = (<Menu>{result}</Menu>)
      dropdown = (
        <Dropdown overlay={menu} trigger={['click']} onVisibleChange={this.onMenuVisibleChange}>
          <DropdownText>
            <a className="dropdown-link" onClick={e => e.preventDefault()}>
              3 customers <Icon type={`${isMenuOpen ? 'caret-up' : 'caret-down'}`} />
            </a>
          </DropdownText>
        </Dropdown>
      )
    }
    return dropdown
  }

  onSortChange = (sortKey: string, event: any) => {
    event.stopPropagation()
    const { isHeader, currentPriceSheet, defaultUrl, clientId } = this.props
    if (isHeader) {
      let sortDirection = 'asc'
      if (this.state.sortKey == sortKey && this.state.sortDirection == 'asc') {
        sortDirection = 'desc'
      }
      this.setState({ sortKey, sortDirection })
      if (this.props.sortByKeyAndDirection)
        this.props.sortByKeyAndDirection(sortKey, sortDirection)
    } else {
      if (currentPriceSheet) {
        const url = !defaultUrl ? `${typeof clientId !== 'undefined' && clientId !== '' ? `/customer/${clientId}` : ''}/price/${currentPriceSheet.priceSheetId}` : defaultUrl
        //   window.location.href = '/#' + url;
        history.push(url);
      }

    }
  }

  render() {
    const { isHeader, currentPriceSheet, defaultUrl, clientId } = this.props
    const { sortKey, sortDirection } = this.state
    return (
      <ListItemRow>
        <div className='inner-wrapper' onClick={(e) => this.onSortChange('priceSheetId', e)} >
          <div className='small-col pricesheet-id-col' style={{ justifyContent: 'flex-start' }}>
            {isHeader ? 'ID #' : `#${currentPriceSheet ? currentPriceSheet.priceSheetId : ''}`}
            {!isHeader && <div className='gray-marker'></div>}
            {isHeader && <IconSvg viewBox='0 0 20 20' type={`${sortDirection == 'desc' && sortKey == 'priceSheetId' ? 'arrow-up-product' : 'arrow-down-product'}`} className={`sort-arrow ${sortKey == 'priceSheetId' ? 'active' : 'inactive'}`} />}
          </div>
          <div className='flex-col' style={{ justifyContent: 'flex-start' }} onClick={(e) => this.onSortChange('name', e)}>
            {isHeader ? 'NAME' : (currentPriceSheet ? currentPriceSheet.name : '')}
            {isHeader && <IconSvg viewBox='0 0 20 20' type={`${sortDirection == 'desc' && sortKey == 'name' ? 'arrow-up-product' : 'arrow-down-product'}`} className={`sort-arrow ${sortKey == 'name' ? 'active' : 'inactive'}`} />}
          </div>
          <div className='small-col'  style={{ justifyContent: 'flex-start', minWidth: 225 }} onClick={(e) => this.onSortChange('latestOrder', e)}>
            {isHeader ? 'LATEST ORDER' : (currentPriceSheet && currentPriceSheet.lastOrder ? moment.utc(currentPriceSheet.lastOrder.updatedDate).format('MM/DD/YYYY') : '')}
            {isHeader && <IconSvg viewBox='0 0 20 20' type={`${sortDirection == 'desc' && sortKey == 'latestOrder' ? 'arrow-up-product' : 'arrow-down-product'}`} className={`sort-arrow ${sortKey == 'latestOrder' ? 'active' : 'inactive'}`} />}
            <div style={{width: 10}}></div>
          </div>
          {/* <div className='small-col'>{isHeader ? 'ASSIGNED' : this.renderCustomers()}</div> */}
          <div className='small-col' style={{ justifyContent: 'flex-end' }} onClick={(e) => this.onSortChange('assignedCount', e)}>
            {isHeader ? 'ASSIGNED' : (currentPriceSheet ? currentPriceSheet.assignedCount : '')}
            {isHeader && <IconSvg viewBox='0 0 20 20' type={`${sortDirection == 'desc' && sortKey == 'assignedCount' ? 'arrow-up-product' : 'arrow-down-product'}`} className={`sort-arrow ${sortKey == 'assignedCount' ? 'active' : 'inactive'}`} />}
            <div style={{width: 20}}></div>
          </div>
          <div className='small-col' style={{ justifyContent: 'flex-end' }} onClick={(e) => this.onSortChange('itemCount', e)}>
            {isHeader ? 'ITEMS' : (currentPriceSheet ? currentPriceSheet.itemCount : '')}
            {isHeader && <IconSvg viewBox='0 0 20 20' type={`${sortDirection == 'desc' && sortKey == 'itemCount' ? 'arrow-up-product' : 'arrow-down-product'}`} className={`sort-arrow ${sortKey == 'itemCount' ? 'active' : 'inactive'}`} />}
            <div style={{width: 50}}></div>
          </div>
          {!isHeader && <Link onClick={(e) => { e.stopPropagation(); }} to={currentPriceSheet ? (!defaultUrl ? `${typeof clientId !== 'undefined' && clientId !== '' ? `/customer/${clientId}` : ''}/price/${currentPriceSheet.priceSheetId}` : defaultUrl) : ``}><div className='btn-view'>VIEW<Icon type="arrow-right" /></div></Link>}
        </div>
      </ListItemRow>
    )
  }
}
