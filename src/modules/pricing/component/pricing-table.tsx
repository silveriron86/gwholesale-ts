/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { withTheme } from 'emotion-theming'

import { PriceSheet } from '~/schema'

import _, { cloneDeep } from 'lodash'
import { Theme } from '~/common';

import { EmptyTr, PriceSheetWrap, ListBody } from '~/modules/pricesheet/components/pricesheets-table.style'
import { PriceSheetListRow } from './pricesheet-list-item'

export interface PSTableProps {
  priceSheets: PriceSheet[]
  search: string
  sortKey: string
  sortDirection: 'asc' | 'desc' | boolean
}

type PricingTableProps = PSTableProps & { theme: Theme }

export class PricingTableComponent extends React.PureComponent<PricingTableProps>
{

  renderPriceSheetList = () => {
    const { priceSheets, search, theme, sortKey, sortDirection } = this.props

    let priceSheetList = cloneDeep(priceSheets)
    priceSheetList = priceSheetList.filter((item) => (item.name).toUpperCase().includes(search.toUpperCase()))
    let key = sortKey
    if (sortKey == 'latestOrder')
      key = 'lastOrder.updatedDate'
    priceSheetList = _.orderBy(priceSheetList, [key], [sortDirection])
    let result: any[] = []

    priceSheetList.forEach((el, index) => {
      result.push(
        <PriceSheetListRow
          key={index}
          currentPriceSheet={el}
          isHeader={false}
        />
      )
    });
    return result
  }

  render() {
    // const { priceSheets, search, theme } = this.props
    return (
      <PriceSheetWrap className='pricing-list'>

        <ListBody>
          {this.renderPriceSheetList()}
        </ListBody>
      </PriceSheetWrap>
    )
  }

  private wrapper = (props: any) => {
    return (
      <tbody className="ant-table-tbody">
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }
}



export const PricingTable = withTheme(PricingTableComponent)
