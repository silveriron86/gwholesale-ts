import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import GrubDrawer from '~/components/GrubDrawer'
import { FilterContainer, ResetButton, Flex, ShowMoreButton, FormLabel } from '~/modules/inventory/inventory.style'
import SearchInput from '~/components/Table/search-input'
import { Icon, Checkbox, Row, Col, Input } from 'antd'
import { brightGreen } from '~/common'

export class PricingRightMenu extends React.PureComponent {
  state = {
    filterSku: [],
    moreSku: false,
    filterCategory: [],
    moreCategory: false,
    moreGroup: false,
    moreCost: false,
    moreMarkup: false,
    moreFreight: false,
    moreOrigin: false,
    moreSalesPrice: false,
    filterOther: {
      group: [],
      origin: [],
      cost: [],
      markup: [],
      freight: [],
      salesPrice: []
    },
  }

  componentDidMount() {
    // this.props.getPricingGroups()
  }

  onSelect = () => {

  }

  onSearch = () => {

  }

  onShowMore = (type: string) => {

  }

  onResetSku = () => {

  }

  onChangeSku = () => {

  }

  onResetFilter = (type: string) => {

  }

  renderSkuList = () => {
    const skuList = ['445', '6432']
    let result = []
    if (!this.state.moreSku) {
      result = skuList.slice(0, 20).map((category, index) => (
        <Col key={index} span={12}>
          <Checkbox value={category}>{category}</Checkbox>
        </Col>
      ))
    } else {
      result = skuList.map((category, index) => (
        <Col key={index} span={12}>
          <Checkbox value={category}>{category}</Checkbox>
        </Col>
      ))
    }
    return result
  }

  onResetCategory = () => {

  }

  onChangeCategory = () => {

  }

  renderFilterOptions = (type: string) => {
    return []
  }

  renderCategories = () => {
    const saleCategories: any[] = []
    let categoryList = []
    if (!this.state.moreCategory) {
      categoryList = saleCategories.slice(0, 20).map((category, index) => (
        <Col key={index} span={12}>
          <Checkbox value={category.wholesaleCategoryId}>{category.name}</Checkbox>
        </Col>
      ))
    } else {
      categoryList = saleCategories.map((category, index) => (
        <Col key={index} span={12}>
          <Checkbox value={category.wholesaleCategoryId}>{category.name}</Checkbox>
        </Col>
      ))
    }
    return categoryList
  }

  onChangeGroupFilter = () => {

  }

  onChangeOriginFilter = () => {

  }

  onChangeCostFilter = () => {

  }

  onChangeMarkupFilter = () => {

  }

  onChangeFreightFilter = () => {

  }

  onChangeSalesPriceFilter = () => {

  }

  render() {
    return (
      <GrubDrawer selected={''} onSelect={this.onSelect} placement={'right'} subDrawer={true} width={350}>
        <React.Fragment>
          <FilterContainer>
            <h3>POWER SEARCH</h3>
            <SearchInput
              handleSearch={this.onSearch}
              placeholder="Search by Keyword or SKU..."
            />
          </FilterContainer>
          <FilterContainer>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <h3>ORGANIZE BY SKU</h3>
              <ResetButton onClick={this.onResetSku}>
                <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
              </ResetButton>
            </div>
            <Flex>
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={this.onChangeSku}
                value={this.state.filterSku}
              >
                <Row>{this.renderSkuList()}</Row>
              </Checkbox.Group>
              <ShowMoreButton hidden={this.state.moreSku} onClick={this.onShowMore.bind('Sku')}>
                Show more...
              </ShowMoreButton>
            </Flex>
          </FilterContainer>
          <FilterContainer style={{ marginTop: '20px' }}>
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                <h3>ORGANIZE BY CATEGORY</h3>
                <ResetButton onClick={this.onResetCategory}>
                  <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
                </ResetButton>
              </div>
              <Flex>
                <Checkbox.Group
                  style={{ width: '100%' }}
                  onChange={this.onChangeCategory}
                  value={this.state.filterCategory}
                >
                  <Row>{this.renderCategories()}</Row>
                </Checkbox.Group>
                <ShowMoreButton hidden={this.state.moreCategory} onClick={this.onShowMore.bind('Category')}>
                  Show more...
                </ShowMoreButton>
              </Flex>
          </FilterContainer>
          <FilterContainer style={{ marginTop: '20px' }}>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <h3>ORGANIZE BY GROUP</h3>
              <ResetButton onClick={this.onResetFilter.bind('group')}>
                <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
              </ResetButton>
            </div>
            <Flex>
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={this.onChangeGroupFilter}
                value={this.state.filterOther.group}
              >
                <Row>{this.renderFilterOptions('group')}</Row>
              </Checkbox.Group>
              <ShowMoreButton hidden={this.state.moreGroup} onClick={this.onShowMore.bind('group')}>
                Show more...
              </ShowMoreButton>
            </Flex>
          </FilterContainer>
          <FilterContainer style={{ marginTop: '20px' }}>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <h3>ORGANIZE BY ORIGIN</h3>
              <ResetButton onClick={this.onResetFilter.bind('origin')}>
                <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
              </ResetButton>
            </div>
            <Flex>
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={this.onChangeOriginFilter}
                value={this.state.filterOther.origin}
              >
                <Row>{this.renderFilterOptions('origin')}</Row>
              </Checkbox.Group>
              <ShowMoreButton hidden={this.state.moreOrigin} onClick={this.onShowMore.bind('origin')}>
                Show more...
              </ShowMoreButton>
            </Flex>
          </FilterContainer>
          <FilterContainer style={{ marginTop: '20px' }}>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <h3>ORIGIN BY COST</h3>
              <ResetButton onClick={this.onResetFilter.bind('cost')}>
                <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
              </ResetButton>
            </div>
            <Flex>
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={this.onChangeCostFilter}
                value={this.state.filterOther.cost}
              >
                <Row>{this.renderFilterOptions('cost')}</Row>
              </Checkbox.Group>
              <ShowMoreButton hidden={this.state.moreCost} onClick={this.onShowMore.bind('cost')}>
                Show more...
              </ShowMoreButton>
            </Flex>
          </FilterContainer>
          <FilterContainer style={{ marginTop: '20px' }}>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <h3>ORIGIN BY MARKUP</h3>
              <ResetButton onClick={this.onResetFilter.bind('markup')}>
                <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
              </ResetButton>
            </div>
            <Flex>
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={this.onChangeMarkupFilter}
                value={this.state.filterOther.markup}
              >
                <Row>{this.renderFilterOptions('markup')}</Row>
              </Checkbox.Group>
              <ShowMoreButton hidden={this.state.moreMarkup} onClick={this.onShowMore.bind('markup')}>
                Show more...
              </ShowMoreButton>
            </Flex>
          </FilterContainer>
          <FilterContainer style={{ marginTop: '20px' }}>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <h3>ORGANIZE BY FREIGHT</h3>
              <ResetButton onClick={this.onResetFilter.bind('freight')}>
                <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
              </ResetButton>
            </div>
            <Flex>
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={this.onChangeFreightFilter}
                value={this.state.filterOther.freight}
              >
                <Row>{this.renderFilterOptions('freight')}</Row>
              </Checkbox.Group>
              <ShowMoreButton hidden={this.state.moreFreight} onClick={this.onShowMore.bind('freight')}>
                Show more...
              </ShowMoreButton>
            </Flex>
          </FilterContainer>
          <FilterContainer style={{ marginTop: '20px' }}>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
              <h3>ORGANIZE BY SALES PRICE</h3>
              <ResetButton onClick={this.onResetFilter.bind('salesPrice')}>
                <span>Reset</span> <Icon style={{ marginRight: '5px', color: brightGreen }} type="undo" />
              </ResetButton>
            </div>
            <Flex>
              <Checkbox.Group
                style={{ width: '100%' }}
                onChange={this.onChangeSalesPriceFilter}
                value={this.state.filterOther.salesPrice}
              >
                <Row>{this.renderFilterOptions('salesPrice')}</Row>
              </Checkbox.Group>
              <ShowMoreButton hidden={this.state.moreSalesPrice} onClick={this.onShowMore.bind('salesPrice')}>
                Show more...
              </ShowMoreButton>
            </Flex>
          </FilterContainer>
        </React.Fragment>
      </GrubDrawer>
    )
  }
}

export default PricingRightMenu
