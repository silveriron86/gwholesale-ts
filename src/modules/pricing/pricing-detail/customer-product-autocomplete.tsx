import React, { FC, useState, useRef } from 'react'
import { AutoComplete } from 'antd'
import { debounce } from 'lodash'
import { SaleItem } from '~/schema'
import { StyledAutoComplete, StyledAutoCompleteOption } from './style'

interface AuthCompleteProps {
  saleItems: SaleItem[];
  onAddItem: () => void;
}

interface OptionType {
  itemId: string;
  title: string;
  value: string;
}

const CustomerProductAuthComplete: FC<AuthCompleteProps> = (props) => {
  const [dataSource, setDataSource] = useState<OptionType[]>([])
  const [value, setValue] = useState<string>('')
  const autoCompleteRef = useRef()

  const handleClear = () => {
    setValue('')
    setDataSource([])
  }

  const handleSelect = (itemId: string) => {
    props.onAddItem(itemId);
    (autoCompleteRef.current as any).blur()
  }

  const handleSearch = debounce((val: string) => {
    const searchStr = val.toLowerCase()

    setDataSource(
      props.saleItems.reduce((_prev: OptionType[], row: SaleItem) => {
        if (
          (row.variety && row.variety.toLowerCase().indexOf(searchStr) >= 0) ||
          (row.SKU && row.SKU.toLowerCase().indexOf(searchStr) >= 0)
        ) {
          _prev.push({
            itemId: row.itemId,
            title: row.variety,
            value: row.SKU,
          })
        }

        return _prev
      }, [])
    )
  }, 500)

  return (
    <StyledAutoComplete>
      <AutoComplete
        ref={autoCompleteRef}
        value={value}
        onChange={setValue}
        onSearch={handleSearch}
        onSelect={handleSelect}
        onFocus={handleClear}
        optionLabelProp="title"
        className="add-product-auto-complete"
        placeholder="Add Item"
        allowClear
      >
        {dataSource.map(row => (
          <AutoComplete.Option key={row.itemId} title={row.title}>
            <StyledAutoCompleteOption>
              <span>{row.title}</span>
              <b>{row.value}</b>
            </StyledAutoCompleteOption>
          </AutoComplete.Option>
        ))}
      </AutoComplete>
    </StyledAutoComplete>
  )
}

export default CustomerProductAuthComplete
