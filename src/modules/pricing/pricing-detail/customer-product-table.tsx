import React from 'react'
import { getLogicAndGroup } from '~/common/utils'
import lodash from 'lodash'
import { Link } from 'react-router-dom'
import { getAllowedEndpoint, mathRoundFun, getPriceGroupPriceString } from '~/common/utils'
import { ThemeSelect } from '~/modules/customers/customers.style'
import { Select, InputNumber, Icon, Button } from 'antd'
import { InputWrapper } from '../../pricesheet/components/pricesheet-detail-table.style'
import { EditAll } from '../component/edit-all-new'
import { PayloadIcon } from '~/components/elements/elements.style'
import { PricingRecordDetail } from './pricing-record-detail'
import { useParams } from 'react-router'
import { css } from '@emotion/core'
import { PricingService } from '../pricing.service'
import { StyleTable } from './style'
import { bignumber, Big } from '~/common/mathjs'

const inputCss = css({
  paddingRight: '10px',
  textAlign: 'right',
  width: '90px',
})

const getLogic = (item: any) => {
  let combineLogic = item.defaultLogic
  if (combineLogic == 'FOLLOW_GROUP') {
    combineLogic = 'GROUP_' + item.defaultGroup
  }
  return combineLogic
}

const calcMargin = (price: number, cost: number) =>
  bignumber(Big.evaluate('(price - cost)*100/price', { price, cost }))
    .toDecimalPlaces(2)
    .toNumber()

const calcMarkup = (price: number, cost: number) =>
  bignumber(Big.evaluate('price - cost', { price, cost }))
    .toDecimalPlaces(2)
    .toNumber()

const calcPriceByMarkup = (markup: number, cost: number) =>
  bignumber(Big.evaluate('markup + cost', { markup, cost }))
    .toDecimalPlaces(2)
    .toNumber()

const calcPriceByMargin = (margin: number, cost: number) =>
  bignumber(Big.evaluate('cost / (1 - margin/100)', { margin, cost }))
    .toDecimalPlaces(2)
    .toNumber()

const toStringFixedTwo = (value: string) => {
  if (value) {
    const stringValue = value.toString()
    const dotIndex = stringValue.indexOf('.')
    if (dotIndex > -1) {
      if (dotIndex == stringValue.length - 1) {
        return stringValue.substring(0, dotIndex) + '.'
      } else {
        const dotString = `0${stringValue.substring(dotIndex, stringValue.length)}`
        const fixedString = dotString.length > 4 ? parseFloat(dotString).toString() : dotString
        return stringValue.substring(0, dotIndex) + '.' + fixedString.substring(2, fixedString.length)
      }
    } else {
      return stringValue
    }
  } else {
    return ''
  }
}

const FOLLOW_DEFAULT_SALES = 'FOLLOW_DEFAULT_SALES'
const GROUP_A = 'GROUP_A'
const GROUP_B = 'GROUP_B'
const GROUP_C = 'GROUP_C'
const GROUP_D = 'GROUP_D'
const GROUP_E = 'GROUP_E'
const strategyArr = [FOLLOW_DEFAULT_SALES, GROUP_A, GROUP_B, GROUP_C, GROUP_D, GROUP_E]

const { Option } = Select

type Props = {
  productList: any[]
  updateProductItem: any
  loading: any
  orderQty: any
  pagination: any
  dispatchParams: any
  dispatchOrderQty: any
  removeProduct: any
  updatePriceSheetItemPricing: any
  updateProductItemLogic: any
  getCustomerProductList: any
  getSalesOrderItemHistory: any
  salesOrderItemHistory: any
  setCurrentItemId: any
  customerProductListModified: boolean
  saveProductList: () => any
  tableFooter: JSX.Element
}

const CustomerProductTable: React.FC<Props> = (props) => {
  const {
    productList,
    updateProductItem,
    loading,
    orderQty,
    pagination,
    dispatchParams,
    customerProductListModified,
  } = props
  const { id, customerId } = useParams<{ customerId: string; id: string }>()

  const _handleSalePriceChange = lodash.debounce((priceSheetItemId: string, newSalePrice: any, ratio) => {
    const item = productList.find((item: any) => item.priceSheetItemId === priceSheetItemId)
    if (!item) {
      return
    }
    newSalePrice = newSalePrice || 0
    newSalePrice = newSalePrice < 0 ? item.cost : newSalePrice
    const basePrice = bignumber(newSalePrice)
      .mul(ratio)
      .toDecimalPlaces(2)
      .toNumber()

    const newData = {
      markup: calcMarkup(basePrice, item.cost),
      margin: calcMargin(basePrice, item.cost),
      salePrice: basePrice,
    }

    updateProductItem({
      priceSheetItemId: priceSheetItemId,
      data: newData,
    })
  }, 50)

  const _handleMarkupChange = lodash.debounce((priceSheetItemId: string, newMarkup: any, ratio: number) => {
    const item = productList.find((item: any) => item.priceSheetItemId === priceSheetItemId)
    if (!item) {
      return
    }
    const baseMarkup = bignumber(newMarkup)
      .mul(ratio)
      .toDecimalPlaces(2)
      .toNumber()
    const price = calcPriceByMarkup(baseMarkup, item.cost)

    const newData = {
      markup: baseMarkup,
      margin: calcMargin(price, item.cost),
      salePrice: price,
    }

    updateProductItem({
      priceSheetItemId: priceSheetItemId,
      data: newData,
    })
  }, 50)

  const _handleMarginChange = lodash.debounce((priceSheetItemId: string, newMargin?: number) => {
    const item = productList.find((item: any) => item.priceSheetItemId === priceSheetItemId)
    if (!item) {
      return
    }
    newMargin = Number(newMargin) || 0
    newMargin = newMargin > 100 ? 0 : newMargin
    newMargin = bignumber(newMargin)
      .toDecimalPlaces(2)
      .toNumber()
    const price = calcPriceByMargin(newMargin, item.cost)

    const newData = {
      markup: calcMarkup(price, item.cost),
      margin: newMargin,
      salePrice: price,
    }

    updateProductItem({
      priceSheetItemId: priceSheetItemId,
      data: newData,
    })
  }, 50)

  const _handleRemoveItem = (priceSheetItemId: string) => {
    const item = productList.find((item: any) => item.priceSheetItemId == priceSheetItemId)
    // remove from OrderQty
    props.dispatchOrderQty({ priceSheetItemId: item.priceSheetItemId, qty: 0 })
    props.removeProduct({ priceSheetId: id, deleteItemListId: [item.wholesaleItemId] })
  }

  const _handleUpdateDefaultPriceSheetItem = (priceSheetItemId: string) => {
    const item = productList.find((item: any) => item.priceSheetItemId === priceSheetItemId)
    if (!item) return
    const info = {
      markup: item.markup,
      margin: item.margin,
      salePrice: item.salePrice,
      costMethod: item.costMethod && item.costMethod !== '0' ? 1 : 0,
      defaultSellingUOM: item.defaultSellingUOM,
      defaultPricingUOM: item.defaultPricingUOM,
    }
    props.updatePriceSheetItemPricing({ id: priceSheetItemId, info })
  }

  const _onClickSave = () => {
    //
  }

  const _changePriceSheetItemLogic = (priceSheetItem: any, value: any) => {
    if (customerProductListModified) {
      props.saveProductList()
    }
    props.updateProductItemLogic({
      id: priceSheetItem.priceSheetItemId,
      info: getLogicAndGroup(value),
      priceSheetId: id,
      clientId: customerId,
    })
  }

  const _onSaveAll = (type: string, value: number) => {
    PricingService.instance.editAllMarginById(id, value || 0).subscribe({
      next: (res) => {
        props.getCustomerProductList()
      },
      error: (error) => {
        console.dir(error)
      },
    })
  }

  const columns = [
    {
      title: 'Order Qty',
      dataIndex: 'priceSheetItemId',
      key: 'priceSheetItemId',
      width: 100,
      render: (val: any, item: any) => {
        return (
          <InputNumber
            value={orderQty[item.priceSheetItemId]?.qty}
            style={{ width: 70 }}
            min={0}
            onChange={(val) => {
              props.dispatchOrderQty({
                priceSheetItemId: item.priceSheetItemId,
                qty: val,
                wholesaleItemId: item.wholesaleItemId,
              })
            }}
            onBlur={(e) => {
              props.dispatchOrderQty({
                priceSheetItemId: item.priceSheetItemId,
                qty: Number(e.target.value),
                wholesaleItemId: item.wholesaleItemId,
              })
            }}
          />
        )
      },
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      key: 'sku',
      className: 'th-left',
      sorter: true,
      render: (text: any, item: any) => {
        const accountType = localStorage.getItem('accountType') || ''
        return (
          <Link
            to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`}
            className="product-name"
          >
            {text}
          </Link>
        )
      },
    },
    {
      title: 'NAME',
      dataIndex: 'variety',
      key: 'variety',
      width: 300,
      className: 'th-left',
      sorter: true,
      render(text: any, item: any) {
        const accountType = localStorage.getItem('accountType') || ''
        return (
          <Link
            to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`}
            className="product-name"
          >
            {text}
          </Link>
        )
      },
    },
    {
      title: 'ORDER QTY UOM',
      dataIndex: 'defaultSellingUOM',
      key: 'defaultSellingUOM',
      width: 150,
      className: 'th-left',
      render: (val: any, item: any) => {
        return (
          <ThemeSelect
            value={val}
            style={{ width: 120 }}
            onChange={(val: any) => {
              updateProductItem({
                priceSheetItemId: item.priceSheetItemId,
                data: {
                  defaultSellingUOM: val,
                },
              })
            }}
          >
            {(item.productUOMS || []).map((uom: any) => (
              <Option key={uom.defaultSellingUOM} value={uom.defaultSellingUOM}>
                {uom.defaultSellingUOM}
              </Option>
            ))}
          </ThemeSelect>
        )
      },
    },
    {
      title: 'PRICING STRATEGY',
      dataIndex: 'defaultLogic',
      key: 'defaultLogic',
      width: 100,
      className: 'th-left',
      render: (defaultLogic: any, record: any) => {
        const price = mathRoundFun(record.price, 2)
        const lastSoldPrice = mathRoundFun(record.lastSoldPrice ? record.lastSoldPrice : 0, 2)
        return (
          <ThemeSelect
            style={{ width: '200px' }}
            value={getLogic(record)}
            onChange={(val: any) => {
              _changePriceSheetItemLogic(record, val)
            }}
          >
            <Option value={FOLLOW_DEFAULT_SALES} title={`Default Price(${'$' + price + '/' + record.inventoryUOM})`}>
              Default Price({'$' + price + '/' + record.inventoryUOM})
            </Option>
            {record.lastSoldChecked && (
              <Option
                value="FOLLOW_LAST_SOLD"
                title={`Last Sold Price(${'$' + lastSoldPrice + '/' + record.inventoryUOM})`}
              >
                Last Sold Price({'$' + lastSoldPrice + '/' + record.inventoryUOM})
              </Option>
            )}
            {record.groupChecked && (
              <Option
                className="logicOption"
                value="GROUP_A"
                title={`Group A${getPriceGroupPriceString(record.priceGroupType, record.marginA, record.descA)}`}
              >
                Group A{getPriceGroupPriceString(record.priceGroupType, record.marginA, record.descA)}
              </Option>
            )}
            {record.groupChecked && (
              <Option
                value="GROUP_B"
                title={`Group B(${getPriceGroupPriceString(record.priceGroupType, record.marginB, record.descB)})`}
              >
                Group B{getPriceGroupPriceString(record.priceGroupType, record.marginB, record.descB)}
              </Option>
            )}
            {record.groupChecked && (
              <Option
                value="GROUP_C"
                title={`Group C${getPriceGroupPriceString(record.priceGroupType, record.marginC, record.descC)}`}
              >
                Group C{getPriceGroupPriceString(record.priceGroupType, record.marginC, record.descC)}
              </Option>
            )}
            {record.groupChecked && (
              <Option
                value="GROUP_D"
                title={`Group D${getPriceGroupPriceString(record.priceGroupType, record.marginD, record.descD)}`}
              >
                Group D{getPriceGroupPriceString(record.priceGroupType, record.marginD, record.descD)}
              </Option>
            )}
            {record.groupChecked && (
              <Option
                value="GROUP_E"
                title={`Group E${getPriceGroupPriceString(record.priceGroupType, record.marginE, record.descE)}`}
              >
                Group E{getPriceGroupPriceString(record.priceGroupType, record.marginE, record.descE)}
              </Option>
            )}

            <Option value="CUSTOM_PRICE">Custom Price</Option>
            <Option value="CUSTOM_MARGIN">Custom Margin</Option>
            <Option value="CUSTOM_MARKUP">Custom Markup</Option>
          </ThemeSelect>
        )
      },
    },
    {
      title: 'COST',
      dataIndex: 'cost',
      key: 'cost',
      width: 150,
      className: 'th-left',
      render(cost: any, item: any) {
        const costMethod = item.costMethod ? 1 : 0
        const ratio = item.productUOMS?.find((u: any) => u.defaultSellingUOM === item.defaultPricingUOM)?.ratio || 1
        const costStr = `Default cost($${bignumber(cost)
          .div(ratio)
          .toFixed(2)}/${item.defaultPricingUOM})`
        return (
          <ThemeSelect
            style={{ width: 214 }}
            value={costMethod}
            disabled={getLogic(item) === FOLLOW_DEFAULT_SALES}
            onChange={(val: any) => {
              updateProductItem({
                priceSheetItemId: item.priceSheetItemId,
                data: {
                  costMethod: val,
                },
              })
            }}
          >
            <Option title={costStr} value={0}>
              {costStr}
            </Option>
            <Option title="Lot cost (w/allocated charges)" value={1}>
              Lot cost (w/allocated charges)
            </Option>
          </ThemeSelect>
        )
      },
    },
    {
      title: 'PRICE',
      dataIndex: 'salePrice',
      key: 'salePrice',
      width: 100,
      className: 'th-left',
      render: (rowPrice: any, item: any) => {
        if (item.costMethod && getLogic(item) !== 'CUSTOM_PRICE') {
          return 'Varies'
        }
        const uom = item.productUOMS?.find((u: any) => u.defaultSellingUOM === item.defaultPricingUOM) || {}
        let { ratio = 1 } = uom
        const { priceFactor = 0 } = uom
        if (strategyArr.includes(getLogic(item))) {
          ratio = ratio / (1 + priceFactor / 100)
        }
        rowPrice = bignumber(rowPrice)
          .div(ratio)
          .toFixed(2)

        return (
          <div>
            <InputWrapper>
              <InputNumber
                value={rowPrice}
                min={0}
                placeholder="$0.00"
                step="0.25"
                disabled={item.defaultLogic != 'CUSTOM_PRICE'}
                formatter={(value) => (value ? '$' + value : '')}
                css={inputCss}
                onChange={(val) => _handleSalePriceChange(item.priceSheetItemId, val, ratio)}
              />
            </InputWrapper>
          </div>
        )
      },
    },
    {
      title: 'PRICE UOM',
      dataIndex: 'defaultPricingUOM',
      key: 'defaultPricingUOM',
      width: 140,
      className: 'th-left',
      render: (val: any, item: any) => {
        return (
          <ThemeSelect
            value={val}
            style={{ width: 120 }}
            onChange={(val: any) => {
              updateProductItem({
                priceSheetItemId: item.priceSheetItemId,
                data: {
                  defaultPricingUOM: val,
                },
              })
            }}
          >
            {(item.productUOMS || []).map((uom: any) => (
              <Option key={uom.defaultSellingUOM} value={uom.defaultSellingUOM}>
                {uom.defaultSellingUOM}
              </Option>
            ))}
          </ThemeSelect>
        )
      },
    },
    {
      title: (
        <div style={{ position: 'relative' }}>
          MARGIN
          <EditAll type="margin" onSaveAll={_onSaveAll} />
        </div>
      ),
      dataIndex: 'margin',
      className: 'pricesheet-detail-margin th-left',
      key: 'margin',
      render: (margin: any, item: any) => {
        return (
          <div>
            <InputWrapper>
              <InputNumber
                value={bignumber(margin)
                  .toDecimalPlaces(2)
                  .toNumber()}
                placeholder="0%"
                step="1"
                disabled={item.defaultLogic != 'CUSTOM_MARGIN'}
                min={0}
                formatter={(value) => {
                  const valNum = toStringFixedTwo(value ? value.toString() : '')
                  return Number(valNum) < 100 ? `${valNum ? valNum + '%' : ''}` : ``
                }}
                style={{ width: 66 }}
                css={inputCss}
                onChange={(val) => {
                  const valNum = toStringFixedTwo(val ? val.toString() : '')
                  Number(valNum) < 100
                    ? _handleMarginChange(item.priceSheetItemId, val)
                    : _handleMarginChange(item.priceSheetItemId, 0)
                }}
              />
            </InputWrapper>
          </div>
        )
      },
    },
    {
      title: 'MARKUP',
      dataIndex: 'markup',
      key: 'markup',
      className: 'th-left',
      render: (markup: any, item: any) => {
        const ratio = item.productUOMS?.find((u: any) => u.defaultSellingUOM === item.defaultPricingUOM)?.ratio || 1
        const newMarkup = bignumber(markup)
          .div(ratio)
          .toDecimalPlaces(2)
          .toNumber()
        return (
          <div>
            <InputWrapper>
              <InputNumber
                value={newMarkup}
                placeholder="$0"
                disabled={item.defaultLogic != 'CUSTOM_MARKUP'}
                step="1"
                min={0}
                formatter={(value) => `$${value}`}
                css={inputCss}
                style={{ width: 74 }}
                onChange={(val) => _handleMarkupChange(item.priceSheetItemId, val, ratio)}
              />
            </InputWrapper>
          </div>
        )
      },
    },
    {
      width: 90,
      render: (_item: any, saleItem: any) => {
        return (
          <PayloadIcon>
            <Button
              type="default"
              shape="round"
              style={{ padding: '4px 8px' }}
              onClick={() => _handleRemoveItem(saleItem.priceSheetItemId)}
            >
              <Icon type="delete" />
            </Button>
          </PayloadIcon>
        )
      },
    },
  ]

  return (
    <StyleTable
      style={{ paddingLeft: 0, paddingRight: 0, overflowX: 'unset' }}
      size="middle"
      columns={columns}
      dataSource={productList}
      rowKey="priceSheetItemId"
      expandIconColumnIndex={1}
      expandedRowRender={(record) => (
        <p style={{ margin: 0 }}>
          <PricingRecordDetail
            onClickSave={_onClickSave}
            product={record}
            clientId={customerId}
            getSalesOrderItemHistory={props.getSalesOrderItemHistory}
            salesOrderItemHistory={props.salesOrderItemHistory}
            setCurrentItemId={props.setCurrentItemId}
            isDefault
          />
        </p>
      )}
      expandIconAsCell={false}
      expandIcon={Expand}
      onExpand={(isExpanded: boolean, record: any) => void 0}
      loading={loading}
      pagination={pagination}
      onChange={(pagi, filter, sorter) => {
        dispatchParams({ type: 'order', payload: sorter })
      }}
      footer={() => props.tableFooter}
    />
  )
}

export default CustomerProductTable

const Expand = (props: any) => {
  return (
    <a
      style={{ color: 'black' }}
      onClick={(e) => {
        props.onExpand(props.record, e)
      }}
    >
      <Icon type={props.expanded ? 'up' : 'down'} style={{ marginRight: 4 }} />
    </a>
  )
}
