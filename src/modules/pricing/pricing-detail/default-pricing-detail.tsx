import React, { useCallback, useState, useReducer, useEffect } from 'react'
import PricingOptions, { CUSTOM_MARGIN, MATCH_A_DIFFERENT_CUSTOMER } from './pricing-group-options'
import BatchEdit from '../component/modal/batch-edit'
import { Input, Button, Icon, Tooltip, Popover, notification, Select, Dropdown, Spin, Menu } from 'antd'
import { Icon as IconSvg } from '~/components'
import {
  HeaderOptions,
  buttonStyle,
  Flex,
  Operation,
  Header,
} from '../../pricesheet/components/pricesheet-detail-header.style'
import { Stylehead, StyleDiv } from './style'
import { responseHandler } from '~/common/utils'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps, useHistory, useParams } from 'react-router'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { PricingModule, PricingDispatchProps, PricingStateProps } from '../pricing.module'
import { Container } from '~/modules/pricesheet/components/pricesheet-detail-header.style'
import { PriceSheetAddModal } from '~/modules/pricesheet/components'
import PageLayout from '~/components/PageLayout'
import { Icon as IconLocal } from '~/components'

import { useMount, useUnmount, useLatest } from 'ahooks'
import { getLogicAndGroup, getFulfillmentDate } from '~/common/utils'
import CustomerProductTable from './customer-product-table'
import CustomerProductAuthComplete from './customer-product-autocomplete'
import { PricingService } from '../pricing.service'
import moment from 'moment'
import { ThemeCheckbox } from '~/modules/customers/customers.style'
import { CustomerService } from '~/modules/customers/customers.service'
import { StyledCustomerProductTable } from './style'

enum FormEnum {
  reset = 'reset',
  change = 'change',
  searchKey = 'searchKey',
  page = 'page',
}

type FormType = { logic: string; margin: string | number; sharedClientId: number; costMethod: number }

export type PricingDetailProps = PricingDispatchProps &
  PricingStateProps &
  RouteComponentProps<{ id: string; customerId: string }> & {
    theme: Theme
  }

// /pricing/:id/default-pricing/:customerId

export const DefaultPricingDetailComponent: React.FC<PricingDetailProps> = (props) => {
  const {
    defaultPriceSheetCustomer: customer,
    defaultPriceSheetObj,
    theme,
    saleItems,
    customerProductList,
    customerProductTotal,
    sellerSetting,
    getCustomerProductList,
  } = props
  const getCustomerProductListRef = useLatest(getCustomerProductList)
  const history = useHistory()

  const { saving, loadingList } = defaultPriceSheetObj
  const [showModal, setShowModal] = useState(false)
  const [enableAutoAdd, setEnableAutoAdd] = useState(customer?.autoSyncProducts)
  const [creatingOrder, setCreatingOrder] = useState(false)
  type StateType = { [key: string]: { qty: number; wholesaleItemId: any } }
  const [orderQty, dispatchOrderQty] = useReducer((state: StateType, action: any) => {
    const { priceSheetItemId, qty, wholesaleItemId } = action
    if (!priceSheetItemId) {
      return state
    }
    if (!qty || qty == 0) {
      delete state[priceSheetItemId]
      return { ...state }
    }
    return {
      ...state,
      [priceSheetItemId]: { qty, wholesaleItemId },
    }
  }, {})

  const [searchParams, dispatchParams] = useReducer(
    (state: any, action: any) => {
      const { type, payload } = action
      switch (type) {
        case FormEnum.searchKey:
          return { ...state, searchKey: payload, page: 1 }
        case FormEnum.page:
          return { ...state, page: payload }
        case 'order': {
          const { order, field } = payload
          if (!order) {
            return { ...state, orderBy: '', direction: '' }
          }
          const map = { ascend: 'asc', descend: 'desc' }
          return { ...state, orderBy: field, direction: map[order] }
        }
        default:
          return state
      }
    },
    {
      page: 1,
      pageSize: 10,
      orderBy: '',
      direction: '',
      searchKey: '',
    },
  )

  const { id, customerId } = useParams<{ customerId: string; id: string }>()
  const [visibleBatchEditDlg, setVisible]: any = useState(false)

  useEffect(() => {
    const params = { priceSheetId: id, clientId: customerId, ...searchParams }
    params.page -= 1 // 服务器page从0开始
    getCustomerProductListRef.current(params)
  }, [customerId, id, searchParams, getCustomerProductListRef])

  const [form, dispatch] = useReducer((state: any, action: any): FormType => {
    const { type, payload } = action
    switch (type) {
      case FormEnum.reset:
        return { ...payload }

      case FormEnum.change:
        return { ...state, ...payload }

      default:
        return state
    }
  }, {} as FormType)

  useMount(() => {
    if (!sellerSetting) {
      props.getSellerSetting()
    }
    props.getCustomer(customerId) // defaultPriceSheetCustomer
    props.getSaleItemsPricing() //saleItems
  })

  useUnmount(() => {
    props.resetDefaultPriceSheet()
  })

  useEffect(() => {
    if (!customer) {
      return
    }
    setEnableAutoAdd(customer.autoSyncProducts)
    dispatch({
      type: FormEnum.reset,
      payload: {
        logic: customer.defaultLogic == 'FOLLOW_GROUP' ? 'GROUP_' + customer.defaultGroup : customer.defaultLogic,
        margin: customer.margin ? Number(customer.margin).toFixed(1) : customer.margin,
        sharedClientId: customer.sharedClientId,
        costMethod: customer.costMethod,
      },
    })
  }, [customer])

  const _onAddItems = (addedItems: string[]) => {
    const params = {
      priceSheetId: id,
      assignItemListId: addedItems,
      clientId: customerId,
    }
    props.addProductItems(params)
    setShowModal(false)
  }

  const handleSelectedItem = (addedItemId: string): void => {
    if (customerProductList.some(item => item.itemId === addedItemId)) {
      notification.success({
        message: 'Success',
        description: 'Updated Product List Successfully',
      })
      return undefined
    }

    props.addProductItems({
      priceSheetId: id,
      assignItemListId: [addedItemId],
      clientId: customerId,
    })
  }

  const _handlePrint = () => {
    const customerName = customer?.clientCompany?.companyName || ''
    history.push(`/pricesheet/${id}/print?customerName=${customerName}`)
  }

  const _toggleModal = useCallback(() => {
    setShowModal((pre) => !pre)
  }, [])

  const _onClickBatchEdit = (data: any) => {
    const { defaultLogic, defaultGroup } = data
    props.saveBatchEditProduct({
      clientId: customerId,
      priceSheetId: id,
      info: getLogicAndGroup(defaultLogic, defaultGroup),
    })
  }

  const _createOrder = () => {
    const fulfillmentDate = getFulfillmentDate(sellerSetting)
    const items = Object.keys(orderQty).map((priceSheetItemId) => {
      const { qty, wholesaleItemId } = orderQty[priceSheetItemId]
      return { priceSheetItemId, wholesaleItemId, quantity: qty }
    })

    const data = {
      wholesaleClientId: customerId,
      deliveryDate: fulfillmentDate,
      orderDate: moment().format('MM/DD/YYYY'),
      items,
      priceSheetId: id,
      userId: sellerSetting?.userId,
      scheduledDeliveryTime: moment().format('h:mm a'),
      quotedDate: moment().format('MM/DD/YYYY'),
    }

    setCreatingOrder(true)
    PricingService.instance.createOrder(data).subscribe({
      next: (res: any) => {
        responseHandler(res)
        const wholesaleOrderId = res?.body?.data?.wholesaleOrderId
        if (wholesaleOrderId) {
          history.push(`/sales-order/${wholesaleOrderId}`)
        }
      },
      error: (error) => {
        notification.error({
          message: 'error',
          description: error?.body?.message || error?.statusCode,
        })
      },
      complete: () => {
        setCreatingOrder(false)
      },
    })
  }

  const _saveProductList = () => {
    return props.saveProductPricing({
      priceSheetId: id,
      clientId: customerId,
    })
  }

  const _handleSaveItems = () => {
    if (customer) {
      const customerId = customer.clientId
      const obj: any = getLogicAndGroup(form.logic, customer.defaultGroup)
      obj.costMethod = form.costMethod
      if (form.logic === CUSTOM_MARGIN) {
        obj.margin = form.margin || 0
      } else if (form.logic === MATCH_A_DIFFERENT_CUSTOMER) {
        if (!form.sharedClientId) {
          notification.warn({
            message: 'Please choose a customer to continue',
          })
          return
        }
        obj.sharedClientId = form.sharedClientId
      }
      props.setClientDefaultLogic({ id: customerId, info: obj })
    }

    setTimeout(() => {
      _saveProductList()
    }, 500)
  }

  const _handleMenuClick = (e: { key: string }) => {
    if (e.key === 'create-save') {
      // create and save
    }
  }

  const onAutoSyncChange = (e: any) => {
    CustomerService.instance.setCustomerProductListAutoSync(+customerId, !enableAutoAdd).subscribe({
      next: (res: any) => {
        if (res?.body?.data) {
        setEnableAutoAdd(!enableAutoAdd)
        }
      },
      complete: () => {
        setEnableAutoAdd(!enableAutoAdd)
      },
      error: (error) => {
        notification.error({
          message: 'error',
          description: error?.body?.message || error?.statusCode,
        })
      },
    })
  }

  return (
    <PageLayout noSubMenu={true}>
      <div style={{ paddingLeft: 40 }}>
        <PriceSheetAddModal
          theme={theme}
          visible={showModal}
          onCancel={_toggleModal}
          onOk={_onAddItems}
          saleItems={saleItems}
          priceSheetItems={customerProductList}
        />

        <Container style={{ paddingBottom: 25 }}>
          <Header className="pricing-detail-header">
            <Flex style={{ paddingTop: '20px', width: '100%', justifyContent: 'space-between' }}>
              <Stylehead>
                <div className="sub-title">ACCOUNT NAME</div>
                <div className="title">{customer?.clientCompany?.companyName}</div>
              </Stylehead>
              {form.logic !== MATCH_A_DIFFERENT_CUSTOMER && (
                <div style={{ minWidth: 800 }}>
                  <Flex style={{ alignItems: 'center' }} className="default-pricing-header">
                    <Popover
                      placement="bottomRight"
                      content={
                        <BatchEdit
                          onClickSave={_onClickBatchEdit}
                          onCancelEdit={() => {
                            setVisible(false)
                          }}
                          isDefault={true}
                        />
                      }
                      trigger="click"
                      visible={visibleBatchEditDlg}
                      onVisibleChange={(val) => setVisible(val)}
                    >
                      <Operation>
                        <Icon type="edit" /> BATCH EDIT
                      </Operation>
                    </Popover>
                    <Operation onClick={_handlePrint}>
                      <Icon type="printer" /> PRINT
                    </Operation>
                    <Dropdown.Button
                      type="primary"
                      icon={<Icon type="down" />}
                      disabled={Object.keys(orderQty).length <= 0 || creatingOrder}
                      style={{ color: theme.primary, marginLeft: 45 }}
                      onClick={_createOrder}
                      overlay={
                        <Menu onClick={_handleMenuClick}>
                          <Menu.Item key="create-save">Create order and save configuration</Menu.Item>
                        </Menu>
                      }
                    >
                      <Spin spinning={creatingOrder}>Create order from product list</Spin>
                    </Dropdown.Button>
                  </Flex>
                </div>
              )}
            </Flex>
            <Stylehead style={{ margin: '25px 0 10px' }}>Product pricing strategy</Stylehead>
            <StyleDiv>
              <PricingOptions
                logic={form.logic}
                margin={form.margin}
                onChange={(obj: any) => dispatch({ type: FormEnum.change, payload: obj })}
                sharedClientId={form.sharedClientId}
                logicTitle="Default pricing strategy"
              />
              {form.logic !== MATCH_A_DIFFERENT_CUSTOMER && (
                <div className="select-cost">
                  <div className="label">
                    Default cost method
                    <Tooltip
                      placement="bottom"
                      title="Default cost method will be applied to any items priced based on a margin or markup pricing strategy. For group pricing, the configured cost method for each product will apply."
                    >
                      <IconLocal type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
                    </Tooltip>
                  </div>
                  <Select
                    onChange={(val: any) => {
                      dispatch({ type: FormEnum.change, payload: { costMethod: val } })
                    }}
                    value={form.costMethod}
                    style={{ width: 300 }}
                  >
                    <Select.Option value={0}>Default cost</Select.Option>
                    <Select.Option value={1}>Lot cost(w/allocated charges)</Select.Option>
                  </Select>
                </div>
              )}
            </StyleDiv>

            {form.logic !== MATCH_A_DIFFERENT_CUSTOMER && (
              <Stylehead style={{ marginTop: 38 }}>
                Product list
                <Tooltip
                  placement="top"
                  title="Pricing startegies for items added to this list will override the customer default pricing strategy"
                >
                  &nbsp;
                  <Icon type="info-circle" />
                </Tooltip>
              </Stylehead>
            )}

            <Flex style={{ margin: '20px 0', justifyContent: 'space-between', alignItems: 'center' }}>
              {form.logic !== MATCH_A_DIFFERENT_CUSTOMER && (
                <HeaderOptions className="flex-start">
                  <Input.Search
                    allowClear
                    placeholder="Search SKU, item name"
                    size="large"
                    style={{ width: '557px', border: '0' }}
                    onSearch={(val) => dispatchParams({ type: FormEnum.searchKey, payload: val })}
                    enterButton={
                      <React.Fragment>
                        <IconSvg viewBox="0 0 16 16" width="16" height="16" type="search" />
                        <span
                          style={{ marginLeft: '9px', fontSize: '14px', lineHeight: '15px', letterSpacing: '0.05em' }}
                        >
                          SEARCH
                        </span>
                      </React.Fragment>
                    }
                  />
                </HeaderOptions>
              )}
              <HeaderOptions style={{justifyContent: 'center'}}>
                <ThemeCheckbox checked={enableAutoAdd} onChange={onAutoSyncChange}>Auto-Add from Sales orders
                  <Tooltip title="When enabled, items ordered and shipped to this customer within the past 12 months will be auto-added to this product list.  Items not sold within 12 months will be auto-removed.">
                    <Icon className="icon-info" type="info-circle" style={{marginLeft: 4}}/>
                  </Tooltip>
                </ThemeCheckbox>
              </HeaderOptions>
              <HeaderOptions>
                <Button
                  size="large"
                  type="primary"
                  style={{
                    ...buttonStyle,
                    border: `1px solid ${theme.primary}`,
                    backgroundColor: theme.primary,
                  }}
                  onClick={_handleSaveItems}
                  loading={saving}
                >
                  <span>
                    <Icon type="check" /> Save Changes
                  </span>
                </Button>
                {form.logic !== MATCH_A_DIFFERENT_CUSTOMER && (
                  <Button
                    size="large"
                    icon="plus"
                    type="primary"
                    style={{
                      ...buttonStyle,
                      border: `1px solid ${theme.primary}`,
                      backgroundColor: theme.primary,
                    }}
                    onClick={() => {
                      setShowModal((pre) => !pre)
                    }}
                  >
                    Add New Item
                  </Button>
                )}
              </HeaderOptions>
            </Flex>
          </Header>
        </Container>

        {form.logic !== MATCH_A_DIFFERENT_CUSTOMER && (
          <StyledCustomerProductTable>
            <CustomerProductTable
              pagination={{
                current: searchParams.page,
                pageSize: searchParams.pageSize,
                total: customerProductTotal,
                size: 'default',
                onChange: (page: number, pageSize: number) => {
                  dispatchParams({ type: FormEnum.page, payload: page })
                },
              }}
              dispatchParams={dispatchParams}
              productList={customerProductList}
              updateProductItemLogic={props.updateProductItemLogic}
              updateProductItem={props.updateProductItem}
              updatePriceSheetItemPricing={props.updatePriceSheetItemPricing}
              salesOrderItemHistory={props.salesOrderItemHistory}
              getSalesOrderItemHistory={props.getSalesOrderItemHistory}
              setCurrentItemId={props.setCurrentItemId}
              loading={loadingList}
              dispatchOrderQty={dispatchOrderQty}
              orderQty={orderQty}
              getCustomerProductList={props.getCustomerProductList}
              removeProduct={props.removeProduct}
              customerProductListModified={props.customerProductListModified}
              saveProductList={_saveProductList}
              tableFooter={(
                <CustomerProductAuthComplete
                  saleItems={saleItems}
                  onAddItem={handleSelectedItem}
                />
              )}
            />
          </StyledCustomerProductTable>

        )}
      </div>
    </PageLayout>
  )
}

const mapStateToProps = (state: GlobalState) => ({ ...state.pricing })

// /pricing/:id/default-pricing/:customerId
export const DefaultPricingDetail = withTheme(connect(PricingModule)(mapStateToProps)(DefaultPricingDetailComponent))
