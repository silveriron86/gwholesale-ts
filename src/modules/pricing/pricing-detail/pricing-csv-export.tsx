import * as React from 'react'
import { Icon } from 'antd'
import lodash from 'lodash'
import { Operation } from '../../pricesheet/components/pricesheet-detail-header.style'
import { formatNumber } from '~/common/utils'
interface PricingCsvExportProps {
  data: any[]
}

class PricingCsvExport extends React.PureComponent<PricingCsvExportProps> {
  state: any

  exportFile = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('reports-table'))
    var wscols = [{ wpx: 300 }, { wpx: 100 }]
    wb.Sheets.Sheet1['!cols'] = wscols
    XLSX.writeFile(wb, 'PriceSheet.csv')
    return false
  }

  renderBody = (data: any[]) => {
    const sortedItems = data.sort((a, b) => b.availableQty - a.availableQty)

    let rows = [
      <tr>
        <td align="right">SKU</td>
        <td align="right">Product Name</td>
        <td align="right">Sale Price</td>
      </tr>,
    ]
    sortedItems.forEach((item: any, index: number) => {
      let newSalePrice = 0
      if (item.margin >= 100) {
        newSalePrice = 0
      } else {
        newSalePrice =
          item.margin === 0 && item.markup === 0
            ? item.salePrice
            : formatNumber(item.cost / (1.0 - item.margin / 100.0) + item.markup, 2)
      }

      rows.push(
        <tr key={`row-${index}`}>
          <td>{item.SKU}</td>
          <td>{item.variety}</td>
          <td>
            ${newSalePrice}/{item.inventoryUOM}
          </td>
        </tr>,
      )
    })

    return rows
  }

  render() {
    const { data } = this.props
    const categoryList: SaleSection[] = lodash.uniqWith(
      data.map((n) =>
        lodash.pick(n.wholesaleCategory.wholesaleSection, ['wholesaleSectionId', 'name', 'warehouse', 'qboId']),
      ),
      lodash.isEqual,
    )

    let filtered = data
    let rows = []
    if (categoryList.length > 0) {
      categoryList.map((category) => {
        filtered = data.filter((n) => n.wholesaleCategory.wholesaleSection.name === category.name)
        rows.push(
          <tr>
            <td align="right" />
            <td align="right">{category.name}</td>
            <td align="right" />
          </tr>,
        )
        rows = rows.concat(this.renderBody(filtered))
        rows.push(
          <tr>
            <td colspan="3" />
          </tr>,
        )
      })
    } else {
      rows = this.renderBody(filtered)
    }

    return (
      <>
        <Operation onClick={this.exportFile}>
          <Icon type="file-text" /> EXPORT AS CSV
          <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            {/* <thead>
              <tr>
                <th align="right" />
                <th align="right" />
                <th align="right" />
              </tr>
            </thead> */}
            <tbody>{rows}</tbody>
          </table>
        </Operation>
      </>
    )
  }
}

export default PricingCsvExport
