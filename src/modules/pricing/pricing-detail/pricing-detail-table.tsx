/**@jsx jsx */
import React, { useState, useEffect, useRef, useCallback } from 'react'
import { jsx } from '@emotion/core'
import { Icon, Button, Spin, Select, Tooltip, InputNumber } from 'antd'
import { ColumnProps, ExpandIconProps } from 'antd/es/table'
import lodash, { get } from 'lodash'
import jQuery from 'jquery'

import { PriceSheetItem, SaleSection, PriceSheet } from '~/schema'
import { PricingDispatchProps } from '../pricing.module'
import organicImg from '~/modules/inventory/components/organic.png'
import {
  Stock,
  polygonCss,
  inputCss,
  InputWrapper,
  tableWrapperStyle,
  PricingDetailWrapper,
} from '../../pricesheet/components/pricesheet-detail-table.style'
import { QBOImage } from '~/modules/inventory/components/inventory-table.style'
import { ThemeTable, ThemeSelect } from '~/modules/customers/customers.style'
import CategoryHeader from './pricing-detail-category.header'
import { PricingRecordDetail } from './pricing-record-detail'
import { EditAll } from '../component/edit-all'
import { history } from '~/store/history'
import { PayloadIcon } from '~/components/elements/elements.style'
import { formatNumber, getLogicAndGroup, mathRoundFun, getPriceGroupPriceString, getAllowedEndpoint } from '~/common/utils'
import { Link } from 'react-router-dom'
import _ from 'lodash'

export interface PricingDetailTableProps {
  priceSheetItems: PriceSheetItem[]
  priceSheetInfo: PriceSheet
  search: string
  onRemoveItem: (priceSheetItemId: string) => void
  onCatogoryChange: PricingDispatchProps['updateCategory']
  onPriceSheetItemUpdate: PricingDispatchProps['updatePriceSheetItem']
  updatePriceSheetItemPricing: Function
  updatePriceSheetItemLogic: Function
  editAllMarginById: Function
  editAllMarkupById: Function
  loadingIndex: string
  isRemovedCats: boolean
  getSalesOrderItemHistory: Function
  salesOrderItemHistory: any[]
  onSaveItems: Function
  setCurrentItemId: Function
  waiting: boolean
  clientId: string
  sectionId: number
  setCollpasedKeys: Function
  sellerSetting: any
}

const { Option } = Select

export class PricingDetailTable extends React.PureComponent<PricingDetailTableProps> {
  multiSortTable = React.createRef<any>()
  expandedRecord: PriceSheetItem | null = null
  state = {
    isEditing: false,
    editingItemId: null,
  }

  handleSalePriceChange = lodash.debounce((priceSheetItemId: string, newSalePrice?: number) => {
    const { priceSheetItems, onPriceSheetItemUpdate } = this.props
    const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
    if (!item) {
      return
    }
    newSalePrice = Number(newSalePrice) || 0
    newSalePrice = +(newSalePrice.toFixed(2))
    newSalePrice = newSalePrice < 0 ? item.cost : newSalePrice
    let newMarkup = (+newSalePrice - item.cost).toFixed(2)
    let newMargin = ((+newSalePrice - item.cost) * 100 / +newSalePrice).toFixed(2)

    const newData = {
      markup: +newMarkup,
      margin: +newMargin,
      salePrice: +newSalePrice,
    }

    onPriceSheetItemUpdate({
      priceSheetItemId: priceSheetItemId,
      data: newData
    })
  }, 50)

  handleMarkupChange = lodash.debounce((priceSheetItemId: string, newMarkup?: number) => {
    if (true /*newFreight*/) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if(!item) {
        return
      }
      newMarkup = Number(newMarkup) || 0
      newMarkup = +(newMarkup.toFixed(2))
      let newSalesPrice = (item.cost + newMarkup).toFixed(2)
      let newMargin = ((+newSalesPrice - item.cost) * 100 / +newSalesPrice).toFixed(2)

      const newData = {
        markup: newMarkup,
        margin: +newMargin,
        salePrice: +newSalesPrice,
      }

      onPriceSheetItemUpdate({
        priceSheetItemId: priceSheetItemId,
        data: newData
      })
    }
  }, 50)

  handleMarginChange = lodash.debounce((priceSheetItemId: string, newMargin?: number) => {
    if (true /*newMargin*/) {
      const { priceSheetItems, onPriceSheetItemUpdate } = this.props
      const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
      if(!item) {
        return
      }
      newMargin = Number(newMargin) || 0
      newMargin = +newMargin.toFixed(2)
      newMargin = newMargin > 100 ? 0 : newMargin
      let newSalesPrice = (item.cost / (1 - newMargin/100)).toFixed(2)
      let newMarkup = (+newSalesPrice - item.cost).toFixed(2)

      const newData = {
        markup: +newMarkup,
        margin: +newMargin,
        salePrice: +newSalesPrice,
      }

      onPriceSheetItemUpdate({
        priceSheetItemId: priceSheetItemId,
        data: newData
      })
    }
  }, 50)

  handleRemoveItem(priceSheetItemId: string) {
    this.props.onRemoveItem(priceSheetItemId)
  }

  handleEditOrSaveItem = (priceSheetItemId: string) => {
    if (!this.state.isEditing || this.state.editingItemId != priceSheetItemId) {
      this.setState({ isEditing: true, editingItemId: priceSheetItemId })
      return
    }
    const { priceSheetItems } = this.props
    const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
    this.setState({ isEditing: false, editingItemId: null })

    if (item) {
      const info = {
        markup: item?.markup,
        margin: item?.margin,
        salePrice: item?.salePrice
      }
      this.props.updatePriceSheetItemPricing({ id: priceSheetItemId, info })
    }
  }

  handleUpdateDefaultPriceSheetItem = (priceSheetItemId: string) => {
    const { priceSheetItems } = this.props
    const item = priceSheetItems.find((item) => item.priceSheetItemId === priceSheetItemId)
    const info = {
      markup: item?.markup,
      margin: item?.margin,
      salePrice: item?.salePrice
    }
    this.props.updatePriceSheetItemPricing({ id: priceSheetItemId, info })
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onSortVariety = (a: any, b: any) => {
    let stringA = a.variety ? a.variety : ''
    let stringB = b.variety ? b.variety : ''
    stringA = a.organic ? 'ORGANIC ' + stringA : stringA
    stringB = b.organic ? 'ORGANIC ' + stringB : stringB
    return stringA.localeCompare(stringB)
  }

  onRow = (record: any, index: number) => {
    return {
      onClick: (_event: any) => {
        _event.preventDefault()
      },
    }
  }

  onExpand = (isExpanded: boolean, record: PriceSheetItem) => {
    if (isExpanded) {
      this.expandedRecord = record
    } else {
      this.expandedRecord = null
    }
  }

  onClickSave = (data: any) => {
    if (this.expandedRecord) {
      const pricesheetItemId = this.expandedRecord.priceSheetItemId
      this.props.updatePriceSheetItemLogic({ id: pricesheetItemId, info: data })
    }
  }

  useCustomIcon = (props: ExpandIconProps<T>) => {
    if (props.expanded) {
      return (
        <a
          style={{ color: 'black' }}
          onClick={(e) => {
            props.onExpand(props.record, e)
          }}
        >
          <Icon type="up" style={{ marginRight: 4 }} />
        </a>
      )
    } else {
      return (
        <a
          style={{ color: 'black' }}
          onClick={(e) => {
            props.onExpand(props.record, e)
          }}
        >
          <Icon type="down" style={{ marginRight: 4 }} />
        </a>
      )
    }
  }

  renderModifiersList = (record: any) => {
    if (!record.suppliers) return []
    const currentSuppliers = record.suppliers.split(', ').map((el: string) => el.trim())
    let result: any[] = []
    currentSuppliers.forEach((el: any, index: number) => {
      result.push(
        <Select.Option key={index} value={el}>
          {el}
        </Select.Option>,
      )
    })
    return result
  }

  renderModifiersListUserToolTips = (record: any) => {
    if (!record.suppliers) return ''
    const currentSuppliers = record.suppliers.split(', ').map((el: string) => el.trim())
    if (currentSuppliers.length > 1) {
      return (
        <Tooltip
          title={
            <>
              {currentSuppliers.map((val: any, index: number) => {
                if (index != 0) {
                  return <div>{val}</div>
                }
              })}
            </>
          }
        >
          {currentSuppliers[0]} <span style={{ float: 'right' }}>+{currentSuppliers.length - 1}</span>
        </Tooltip>
      )
    } else if (currentSuppliers.length == 1) {
      return currentSuppliers[0]
    } else {
      return ''
    }
  }

  changePriceSheetItemLogic = (priceSheetItem: any, value: any) => {
    this.props.updatePriceSheetItemLogic({
      id: priceSheetItem.priceSheetItemId,
      info: getLogicAndGroup(value),
    })
  }
  render() {
    const { priceSheetItems, priceSheetInfo, sellerSetting } = this.props
    const sortedItems = priceSheetItems.sort((a, b) => b.availableQty - a.availableQty)
    const { isEditing, editingItemId } = this.state
    let columns: ColumnProps<PriceSheetItem>[]

    if (priceSheetInfo.isDefault) {
      columns = [
        {
          title: 'SKU',
          dataIndex: 'SKU',
          key: 'SKU',
          className: 'th-left',
          sorter: (a, b) => {
            return this.onSortString('SKU', a, b)
          },
          render: (text, item) => {
            const accountType = localStorage.getItem("accountType") || ''
            return (
              <Link to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`} className="product-name">
                {text}
              </Link>
            )
          }
        },
        {
          title: 'NAME',
          dataIndex: 'variety',
          key: 'variety',
          width: 300,
          className: 'th-left',
          sorter: (a, b) => {
            return this.onSortVariety(a, b)
          },
          render(text, item) {
            const accountType = localStorage.getItem("accountType") || ''
            return item.organic ? (
              <div>
                <QBOImage src={organicImg} style={{ marginRight: 10 }} />
                <Link to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`} className="product-name">
                  {text}
                </Link>
              </div>
            ) : (
              <Link to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`} className="product-name">
                {text}
              </Link>
            )
          },
        },
        {
          title: 'PRICING STRATEGY',
          dataIndex: 'defaultLogic',
          key: 'defaultLogic',
          width: 100,
          className: 'th-left',
          render: (defaultLogic: any, record: any) => {
            let combineLogic = defaultLogic
            if (combineLogic == 'FOLLOW_GROUP') {
              combineLogic = 'GROUP_' + record.defaultGroup
            }
            const cost = mathRoundFun(record.cost, 2)
            const price = mathRoundFun(record.defaultPrice, 2) //when formatting api response, price is changed to defaultPrice, look at formatPriceSheetItem method
            const lastSoldPrice = mathRoundFun(record.lastSoldPrice ? record.lastSoldPrice : 0, 2)
            return (
              <div>
                {record.groupChecked && (
                  <ThemeSelect
                    style={{ width: '200px' }}
                    value={combineLogic}
                    onChange={this.changePriceSheetItemLogic.bind(this, record)}
                  >
                    <Option
                      value="FOLLOW_DEFAULT_SALES"
                      title={`Default Price(${'$' + price + '/' + record.inventoryUOM})`}
                    >
                      Default Price({'$' + price + '/' + record.inventoryUOM})
                    </Option>
                    {record.lastSoldChecked && (
                      <Option
                        value="FOLLOW_LAST_SOLD"
                        title={`Last Sold Price(${'$' + lastSoldPrice + '/' + record.inventoryUOM})`}
                      >
                        Last Sold Price({'$' + lastSoldPrice + '/' + record.inventoryUOM})
                      </Option>
                    )}
                    <Option
                      className="logicOption"
                      value="GROUP_A"
                      title={`Group A${getPriceGroupPriceString(record.priceGroupType, record.marginA, record.descA)}`}
                    >
                      Group A{getPriceGroupPriceString(record.priceGroupType, record.marginA, record.descA)}
                    </Option>
                    <Option
                      value="GROUP_B"
                      title={`Group B(${getPriceGroupPriceString(
                        record.priceGroupType,
                        record.marginB,
                        record.descB,
                      )})`}
                    >
                      Group B{getPriceGroupPriceString(record.priceGroupType, record.marginB, record.descB)}
                    </Option>
                    <Option
                      value="GROUP_C"
                      title={`Group C${getPriceGroupPriceString(record.priceGroupType, record.marginC, record.descC)}`}
                    >
                      Group C{getPriceGroupPriceString(record.priceGroupType, record.marginC, record.descC)}
                    </Option>
                    <Option
                      value="GROUP_D"
                      title={`Group D${getPriceGroupPriceString(record.priceGroupType, record.marginD, record.descC)}`}
                    >
                      Group D{getPriceGroupPriceString(record.priceGroupType, record.marginD, record.descD)}
                    </Option>
                    <Option
                      value="GROUP_E"
                      title={`Group E${getPriceGroupPriceString(record.priceGroupType, record.marginE, record.descC)}`}
                    >
                      Group E{getPriceGroupPriceString(record.priceGroupType, record.marginE, record.descC)}
                    </Option>
                    <Option value="CUSTOM_PRICE">Custom Price</Option>
                    <Option value="CUSTOM_MARGIN">Custom Margin</Option>
                    <Option value='CUSTOM_MARKUP'>Custom Markup</Option>
                  </ThemeSelect>
                )}

                {!record.groupChecked && (
                  <ThemeSelect
                    style={{ width: '200px' }}
                    value={combineLogic}
                    onChange={this.changePriceSheetItemLogic.bind(this, record)}
                  >
                    <Option
                      value="FOLLOW_DEFAULT_SALES"
                      title={`Default Price(${'$' + price + '/' + record.inventoryUOM})`}
                    >
                      Default Price({'$' + price + '/' + record.inventoryUOM})
                    </Option>
                    {record.lastSoldChecked && (
                      <Option
                        value="FOLLOW_LAST_SOLD"
                        title={`Last Sold Price(${'$' + lastSoldPrice + '/' + record.inventoryUOM})`}
                      >
                        Last Sold Price({'$' + lastSoldPrice + '/' + record.inventoryUOM})
                      </Option>
                    )}
                    <Option value="CUSTOM_PRICE">Custom Price</Option>
                    <Option value="CUSTOM_MARGIN">Custom Margin</Option>
                    <Option value='CUSTOM_MARKUP'>Custom Markup</Option>
                  </ThemeSelect>
                )}
              </div>
            )
          },
        },
        {
          title: 'COST',
          dataIndex: 'cost',
          key: 'cost',
          width: 150,
          className: 'th-left',
          sorter: (a, b) => {
            return a.cost - b.cost
          },
          render(rowPrice, item) {
            return `$${(+rowPrice).toFixed(2)}/${item.inventoryUOM}`
          },
        },
        {
          title: 'PRICE',
          dataIndex: 'salePrice',
          key: 'salePrice',
          width: 200,
          className: 'th-left',
          render: (rowPrice: any, item: any, index) => {
            let price = rowPrice
            return (
              <div>
                <InputWrapper>
                  <InputNumber
                    value={price}
                    placeholder="$0.00"
                    step="0.25"
                    disabled={item.defaultLogic != 'CUSTOM_PRICE'}
                    formatter={(value) => `${value ? '$' + value : ''}`}
                    css={inputCss}
                    // onChange={this.customerChange.bind(this, item.priceSheetItemId, 'price')}
                    onChange={this.handleSalePriceChange.bind(this, item.priceSheetItemId)}
                  />
                  /${item.inventoryUOM}
                </InputWrapper>
              </div>
            )
          },
        },
        {
          title: 'MARGIN',
          dataIndex: 'margin',
          className: 'pricesheet-detail-margin th-left',
          key: 'margin',
          width: 120,
          render: (margin: any, item: any, index) => {
            let newMargin = margin
            return (
              <div>
                <InputWrapper>
                  <InputNumber
                    value={newMargin}
                    placeholder="0%"
                    step="1"
                    disabled={item.defaultLogic != 'CUSTOM_MARGIN'}
                    min={0}
                    formatter={(value) => `${value ? value + '%' : ''}`}
                    css={inputCss}
                    // onChange={this.customerChange.bind(this, item.priceSheetItemId, 'margin')}
                    onChange={this.handleMarginChange.bind(this, item.priceSheetItemId)}
                  />
                </InputWrapper>
              </div>
            )
          },
        },
        {
          title: 'MARKUP',
          dataIndex: 'markup',
          key: 'markup',
          width: 120,
          className: 'th-left',
          render: (markup: any, item: any, index) => {
            let newMarkup = markup
            return (
              <div>
                <InputWrapper>
                  <InputNumber
                    value={newMarkup}
                    placeholder="$0"
                    disabled={item.defaultLogic != 'CUSTOM_MARKUP'}
                    step="1"
                    min={0}
                    formatter={(value) => `$${value}`}
                    css={inputCss}
                    // onChange={this.customerChange.bind(this, item.priceSheetItemId, 'markup')}
                    onChange={this.handleMarkupChange.bind(this, item.priceSheetItemId)}
                  />
                </InputWrapper>
              </div>
            )
          },
        },
        {
          width: 110,
          className: 'buttonCell',
          render: (_item, saleItem, index) => {
            return (
              <div>
                {(priceSheetInfo.owner === true || priceSheetInfo.isDefault === true) && (
                  <PayloadIcon>
                    <Button
                      type="default"
                      shape="round"
                      style={{ padding: '4px 8px' }}
                      onClick={this.handleUpdateDefaultPriceSheetItem.bind(this, saleItem.priceSheetItemId)}
                      disabled={this.props.loadingIndex.length > 0}
                    >
                      {/* {this.props.loadingIndex === saleItem.priceSheetItemId && <Icon type={`${isEditing && editingItemId==saleItem.priceSheetItemId ? 'save' : 'edit'}`}/>} */}
                      <Icon type={'save'} />
                    </Button>
                    <Button
                      type="default"
                      shape="round"
                      style={{ padding: '4px 8px' }}
                      onClick={this.handleRemoveItem.bind(this, saleItem.priceSheetItemId)}
                      disabled={this.props.loadingIndex.length > 0}
                    >
                      {this.props.loadingIndex === saleItem.priceSheetItemId ? <Spin /> : <Icon type="delete" />}
                    </Button>
                  </PayloadIcon>
                )}
                {/* {this.props.loadingIndex === saleItem.priceSheetItemId && (
                  <Spin />
                )} */}
              </div>
            )
          },
        },
      ]
    } else {
      columns = [
        {
          title: 'SKU',
          dataIndex: 'SKU',
          key: 'SKU',
          sorter: (a, b) => {
            return this.onSortString('SKU', a, b)
          },
          render: (text, item) => {
            const accountType = localStorage.getItem("accountType") || ''
            return (
              <Link to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`} className="product-name">
                {text}
              </Link>
            )
          }
        },
        {
          title: 'CATEGORY',
          dataIndex: 'wholesaleCategory.name',
          key: 'wholesaleCategory.name',
          width: 150,
          sorter: (a, b) => {
            return this.onSortString('name', a.wholesaleCategory, b.wholesaleCategory)
          },
        },
        {
          title: 'NAME',
          dataIndex: 'variety',
          key: 'variety',
          width: 310,
          sorter: (a, b) => {
            return this.onSortVariety(a, b)
          },
          render(text, item) {
            const accountType = localStorage.getItem("accountType") || ''
            return item.organic ? (
              <div>
                <QBOImage src={organicImg} style={{ marginRight: 10 }} />
                <Link to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`} className="product-name">
                  {text}
                </Link>
              </div>
            ) : (
              <Link to={`/product/${item.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`} className="product-name">
                {text}
              </Link>
            )
          },
        },
        {
          title: 'SIZE',
          dataIndex: 'size',
          key: 'size',
          width: 150,
          sorter: (a, b) => {
            return this.onSortString('size', a, b)
          },
        },
        {
          title: 'WEIGHT',
          dataIndex: 'weight',
          key: 'weight',
          width: 100,
          sorter: (a, b) => {
            return this.onSortString('weight', a, b)
          },
        },
        {
          title: 'PACKING',
          dataIndex: 'packing',
          key: 'packing',
          width: 130,
          sorter: (a, b) => {
            return this.onSortString('packing', a, b)
          },
        },
        {
          title: 'BRAND',
          dataIndex: 'modifier',
          key: 'modifier',
          width: 120,
          render: (modifier: string, record: any) => this.renderModifiersListUserToolTips(record),
        },
        {
          title: 'ORIGIN',
          dataIndex: 'origin',
          key: 'origin',
          width: 130,
          sorter: (a, b) => {
            return this.onSortString('origin', a, b)
          },
        },
        {
          title: 'STOCK',
          dataIndex: 'availableQty',
          key: 'availableQty',
          width: 100,
          sorter: (a, b) => {
            return a.availableQty - b.availableQty
          },
          render(text: any, record: any) {
            if (text == null) {
              text = 0
            }
            return (
              <Stock stock={Number(text)}>
                <span style={{ fontWeight: 700 }}>{text.toFixed(2)}</span>
                <Icon type="polygon" css={polygonCss} />
              </Stock>
            )
          },
        },
        {
          title: 'COST',
          dataIndex: 'cost',
          key: 'cost',
          width: 150,
          sorter: (a, b) => {
            return a.cost - b.cost
          },
          render(rowPrice, record) {
            return `$${(+rowPrice).toFixed(2)}/${record.inventoryUOM}`
          },
        },
        {
          title: 'PRICING STRATEGY',
          dataIndex: 'defaultLogic',
          key: 'defaultLogic',
          width: 150,
          render: (defaultLogic: any, record: any) => {
            let combineLogic = defaultLogic
            if (combineLogic == 'FOLLOW_GROUP') {
              combineLogic = 'GROUP_' + record.defaultGroup
            } else if (combineLogic == 'FOLLOW_DEFAULT_SALES') {
              combineLogic = 'Default Price'
            } else if (combineLogic == 'FOLLOW_LAST_SOLD') {
              combineLogic = 'Last Sold Price'
            }
            return combineLogic
          },
        },
        {
          title: 'MARGIN',
          dataIndex: 'margin',
          key: 'margin',
          className: 'pricesheet-detail-margin',
          width: 100,
          sorter: (a, b) => {
            return a.margin - b.margin
          },
          render: (rowPrice, item, index) => {
            return (
              <div>
                <InputWrapper className={`${isEditing && editingItemId == item.priceSheetItemId ? 'block' : 'none'}`}>
                  <InputNumber
                    value={rowPrice ? formatNumber(rowPrice, 2) : ''}
                    placeholder="0%"
                    step="0.01"
                    formatter={(value) => `${value}%`}
                    css={inputCss}
                    onChange={this.handleMarginChange.bind(this, item.priceSheetItemId)}
                  />
                </InputWrapper>
                <span className={`${isEditing && editingItemId == item.priceSheetItemId ? 'none' : 'block'}`}>
                  {rowPrice ? formatNumber(rowPrice, 2) : '0'}%
                </span>
              </div>
            )
          },
        },
        {
          title: 'MARKUP',
          dataIndex: 'markup',
          className: 'pricesheet-detail-markup',
          key: 'markup',
          width: 110,
          sorter: (a, b) => {
            return a.markup - b.markup
          },
          render: (rowPrice, item, index) => {
            return (
              <div>
                {priceSheetInfo.owner === true && (
                  <InputWrapper className={`${isEditing && editingItemId == item.priceSheetItemId ? 'block' : 'none'}`}>
                    <InputNumber
                      value={rowPrice}
                      placeholder="$0.00"
                      step="0.25"
                      formatter={(value) => `$${value}`}
                      css={inputCss}
                      onChange={this.handleMarkupChange.bind(this, item.priceSheetItemId)}
                    />
                  </InputWrapper>
                )}
                <span className={`${isEditing && editingItemId == item.priceSheetItemId ? 'none' : 'block'}`}>
                  ${(+rowPrice).toFixed(2)}
                </span>
              </div>
            )
          },
        },
        {
          title: 'SALE PRICE',
          dataIndex: 'salePrice',
          key: 'salePrice',
          width: 150,
          sorter: (a, b) => {
            return a.salePrice - b.salePrice
          },
          render: (rowPrice: any, item: any, index) => {
            return (
              <div>
                {(priceSheetInfo.owner === true || priceSheetInfo.isDefault === true) && (
                  <InputWrapper className={`${isEditing && editingItemId == item.priceSheetItemId ? 'block' : 'none'}`}>
                    <InputNumber
                      value={rowPrice}
                      placeholder="$0.00"
                      step="0.01"
                      min={0}
                      formatter={(value) => `$${value}`}
                      css={inputCss}
                      onChange={this.handleSalePriceChange.bind(this, item.priceSheetItemId)}
                    />
                    /{item.inventoryUOM}
                  </InputWrapper>
                )}
                <span className={`${isEditing && editingItemId == item.priceSheetItemId ? 'none' : 'block'}`}>
                  ${rowPrice}/{item.inventoryUOM}
                </span>
              </div>
            )
          },
        },
        {
          width: 110,
          className: 'buttonCell',
          render: (_item, saleItem, index) => {
            return (
              <div>
                {(priceSheetInfo.owner === true || priceSheetInfo.isDefault === true) && (
                  <PayloadIcon>
                    <Button
                      type="default"
                      shape="round"
                      style={{ padding: '4px 8px' }}
                      onClick={this.handleEditOrSaveItem.bind(this, saleItem.priceSheetItemId)}
                      disabled={this.props.loadingIndex.length > 0}
                    >
                      {/* {this.props.loadingIndex === saleItem.priceSheetItemId && <Icon type={`${isEditing && editingItemId==saleItem.priceSheetItemId ? 'save' : 'edit'}`}/>} */}
                      <Icon type={`${isEditing && editingItemId == saleItem.priceSheetItemId ? 'save' : 'edit'}`} />
                    </Button>
                    <Button
                      type="default"
                      shape="round"
                      style={{ padding: '4px 8px' }}
                      onClick={this.handleRemoveItem.bind(this, saleItem.priceSheetItemId)}
                      disabled={this.props.loadingIndex.length > 0}
                    >
                      {this.props.loadingIndex === saleItem.priceSheetItemId ? <Spin /> : <Icon type="delete" />}
                    </Button>
                  </PayloadIcon>
                )}
                {/* {this.props.loadingIndex === saleItem.priceSheetItemId && (
                  <Spin />
                )} */}
              </div>
            )
          },
        },
      ]
    }

    if (sellerSetting) {
      if (sellerSetting.company.showPriceSheetBrand !== true) {
        const found = columns.findIndex(col => col.title === 'BRAND')
        if (found >= 0) {
          columns.splice(found, 1);
        }
      }
      if (sellerSetting.company.showPriceSheetOrigin !== true) {
        const found = columns.findIndex(col => col.title === 'ORIGIN')
        if (found >= 0) {
          columns.splice(found, 1);
        }
      }
      if (sellerSetting.company.showPriceSheetPacking !== true) {
        const found = columns.findIndex(col => col.title === 'PACKING')
        if (found >= 0) {
          columns.splice(found, 1);
        }
      }
      if (sellerSetting.company.showPriceSheetSize !== true) {
        const found = columns.findIndex(col => col.title === 'SIZE')
        if (found >= 0) {
          columns.splice(found, 1);
        }
      }
      if (sellerSetting.company.showPriceSheetWeight !== true) {
        const found = columns.findIndex(col => col.title === 'WEIGHT')
        if (found >= 0) {
          columns.splice(found, 1);
        }
      }
    }

    if (!sellerSetting) {
      return null
    }

    return (
      <ThemeTable
        ref={this.multiSortTable}
        id={'table'}
        style={{ paddingLeft: 0, paddingRight: 0, overflowX: 'unset' }}
        pagination={priceSheetInfo?.isDefault}
        columns={columns}
        dataSource={sortedItems}
        rowKey="priceSheetItemId"
        onRow={this.onRow}
        expandedRowRender={(record) => (
          <p style={{ margin: 0 }}>
            <PricingRecordDetail
              product={record}
              onClickSave={this.onClickSave}
              clientId={this.props.clientId}
              getSalesOrderItemHistory={this.props.getSalesOrderItemHistory}
              salesOrderItemHistory={this.props.salesOrderItemHistory}
              setCurrentItemId={this.props.setCurrentItemId}
              isDefault={priceSheetInfo.isDefault}
            />
          </p>
        )}
        expandIconAsCell={false}
        expandIcon={this.useCustomIcon}
        onExpand={this.onExpand}
        loading={this.props.waiting}
      />
    )
  }
}

const PriceSheetDetailSubtable: React.SFC<PricingDetailTableProps & {
  category: SaleSection | null
}> = (props) => {
  const { category, priceSheetItems, search, waiting, collpasedKeys, setCollpasedKeys, sectionId } = props
  const [width, setWidth] = useState(0)
  let data = priceSheetItems
  if (category !== null) {
    data = priceSheetItems.filter((n) => n.wholesaleCategory.wholesaleSection.name === category.name)
  }
  const listItems = search
    ? data.filter((item) =>
      (item.SKU + ' ' + item.variety + ' ' + item.wholesaleCategory.name + ' ' + item.provider + ' ' + item.upc ?? '')
        .toUpperCase()
        .includes(search.toUpperCase()),
    )
    : data

  // const [collapsed, setCollpased] = useState(listItems.length === 0)
  const [collapsed, setCollpased] = useState(false)

  useEffect(() => {
    // set collapsed#true with empty search result
    if (props.priceSheetInfo.isDefault === false) {
      setCollpased(listItems.length === 0)
    }
  }, [search, priceSheetItems])

  const handleHeaderClick = () => {
    setCollpased(!collapsed)
    if (!collapsed) {
      setCollpasedKeys(collpasedKeys.filter(id => id !== sectionId))
    } else {
      setCollpasedKeys([...collpasedKeys, sectionId])
    }
  }

  const handleCategoryChange = (value: string) => {
    // props.onCatogoryChange({
    //   name: value,
    //   wholesaleSectionId: props.category.wholesaleSectionId,
    //   warehouse: props.category.warehouse,
    //   qboId: props.category.qboId,
    // })
  }

  const onSaveAll = (type: string, value: string) => {
    let data = null
    if (category) {
      data = {
        id: props.priceSheetInfo.priceSheetId,
        value: value || 0,
        categoryId: category.wholesaleSectionId,
      }
    } else {
      data = {
        id: props.priceSheetInfo.priceSheetId,
        value: value || 0,
      }
    }

    if (type == 'margin') {
      props.editAllMarginById(data)
    } else if (type == 'markup') {
      props.editAllMarkupById(data)
    }
  }

  let categoryTitle: any = 'All'
  if (!category && props.priceSheetInfo.isDefault === true) {
    categoryTitle = () => (
      <div>
        Customer Product List
        <Tooltip placement="top" title='Pricing startegies for items added to this list will override the customer default pricing strategy'>
          &nbsp;<Icon type="info-circle" />
        </Tooltip>
      </div>
    )
  } else if (category && category.name) {
    categoryTitle = category.name
  }

  // useEffect(() => {
  //   if (jQuery('.ant-table-tbody').width() === 0) return
  //   setWidth(jQuery('.ant-table-tbody').width())
  // }, [jQuery('.ant-table-tbody').width()])

  return (
    <React.Fragment>
      <CategoryHeader
        editMode
        value={categoryTitle}
        onChange={handleCategoryChange}
        collapsed={collapsed}
        onCollapseChange={handleHeaderClick}
        width={width}
      />
      <PricingDetailWrapper collapsed={collapsed} width={width}>
        <EditAll type="margin" onSaveAll={onSaveAll} />
        {!props.priceSheetInfo.isDefault && <EditAll type="markup" onSaveAll={onSaveAll} />}

        <PricingDetailTable {...props} priceSheetItems={listItems} />
      </PricingDetailWrapper>
    </React.Fragment>
  )
}

const PricingDetailTableContainer: React.SFC<PricingDetailTableProps> = (props) => {
  // pick up all category
  const categoryList: SaleSection[] = lodash.uniqWith(
    props.priceSheetItems.map((n) =>
      lodash.pick(n.wholesaleCategory.wholesaleSection, ['wholesaleSectionId', 'name', 'warehouse', 'qboId']),
    ),
    lodash.isEqual,
  )

  const [collpasedKeys, setCollpasedKeys] = useState<any>([])
  const [defaultKeys, setDefaultKeys] = useState<any>([])

  useEffect(() => {
    if (categoryList.length) {
      setCollpasedKeys(categoryList.map(v => v.wholesaleSectionId))
    }
  }, [props.priceSheetItems])
  if (props.isRemovedCats === true || props.priceSheetInfo.isDefault === true) {
    return (
      <div style={{ width: !!defaultKeys.length ? 'fit-content' : '100%' }}>
        <PriceSheetDetailSubtable sectionId={1} collpasedKeys={defaultKeys} setCollpasedKeys={setDefaultKeys} category={null} {...props} />
      </div>
    )
  }
  return (
    <div style={{ width: !!collpasedKeys.length ? 'fit-content' : '100%' }}>
      {categoryList.length > 0 ? (
        categoryList.map((item) => (
          <PriceSheetDetailSubtable sectionId={item.wholesaleSectionId} collpasedKeys={collpasedKeys} setCollpasedKeys={setCollpasedKeys} key={item.wholesaleSectionId} category={item} {...props} />
        ))
      ) : (
        <div style={{ marginTop: 50 }}>
          <PriceSheetDetailSubtable category={null} {...props} />
        </div>
      )}
    </div>
  )
}

const PricingDetailTableContainerMemo = React.memo(PricingDetailTableContainer)

export default PricingDetailTableContainerMemo
