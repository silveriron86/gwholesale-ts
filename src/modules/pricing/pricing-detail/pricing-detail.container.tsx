import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { history } from '~/store/history'
import { PricingModule, PricingDispatchProps, PricingStateProps } from './../pricing.module'
import { PricingDetailHeader } from './pricing-detail.header'
import {
  Container,
  ModalHeader,
  ModalFormLabel,
  ModalFormRadio,
  EmailPriceSheetModal,
} from '~/modules/pricesheet/components/pricesheet-detail-header.style'
import { NewOrderFormModal } from '~/modules/orders/components'
import { Select, Table, Popconfirm, Button, Radio } from 'antd'
import { ThemeButton, ThemeRadio } from '~/modules/customers/customers.style'

import { TempCustomer } from '~/schema'
import {
  AssignedClientWrapper,
  CustomerSelector,
  CustomerSelectorTitle,
  selectStyles,
  MessageBox,
  MessageBoxInput,
} from '~/modules/pricesheet/pricesheet-detail.style'
import { PayloadIcon } from '~/components/elements/elements.style'
import { Icon as IconSvg } from '~/components/icon/'
import { PriceSheetAddModal } from '~/modules/pricesheet/components'
import { PriceItemFormModal } from '~/modules/pricesheet/pricesheet-detail.container'
import PricingDetailTable from './pricing-detail-table'
import PageLayout from '~/components/PageLayout'
import { notify } from '~/common/utils'

export type PricingDetailProps = PricingDispatchProps &
  PricingStateProps &
  RouteComponentProps<{ customerId: string; id: string }> & {
    theme: Theme
  }

// price/:id  customer/:id/price/:id
export class PricingDetailComponent extends React.PureComponent<PricingDetailProps> {
  addItemsCmp = React.createRef<PriceSheetAddModal>()

  column = [
    {
      dataIndex: 'clientCompany.companyName',
      render: (companyName: string, record: any) => {
        return <a href={`#/customer/${record.clientId}/account`} style={{fontWeight: 400}}>{companyName}</a>
      }
    },
    {
      title: '',
      dataIndex: '',
      key: 'x',
      width: '10%',
      render: (text: number, record: any) => {
        return (
          <Popconfirm title="Sure to unassign?" onConfirm={() => this.onUnassignCustomer(record.clientId)}>
            <a>
              <PayloadIcon>
                <IconSvg type="close" viewBox="0 0 20 28" width={15} height={14} />
              </PayloadIcon>
            </a>
          </Popconfirm>
        )
      },
    },
  ]

  state = {
    showModal: false,
    showDuplicate: false,
    emailText: '',
    search: '',
    editingInfo: false,
    editValues: {},
    selectedCustomer: '',
    message: '',
    type: null,
    showPrintModal: false,
    isRemovedCats: false,
    radioType: 1,
  }

  componentDidMount() {
    const id = this.props.match.params.id
    const clientId = this.props.match.params.customerId
    console.log('here pricing ......', id)
    this.props.resetLoading()
    this.props.getPriceSheetInfoPricing(id)
    this.props.getPriceSheetItemsPricingWithClient({ id, clientId })
    this.props.getSaleItemsPricing()
    this.props.getCustomersPricing(id)
    this.props.getSellerSetting()
    if (this.props.simplifyCustomers.length == 0) {
      this.props.getSimplifyCustomersForPricing()
    }
  }

  componentWillReceiveProps(nextProps: PricingDetailProps) {
    if (this.props.currentPriceSheet !== nextProps.currentPriceSheet && nextProps.currentPriceSheet) {
      const { assignedClients } = nextProps.currentPriceSheet
      if (assignedClients && assignedClients.length > 0) {
        this.setState({
          selectedCustomer: assignedClients[0].clientId,
        })
      }
    }
  }

  onUnassignCustomer = (clientId: number) => {
    this.props.unAssignCustomerFromPricesheet(clientId)
  }

  onClickBack = () => {
    this.props.history.goBack()
    this.props.resetPriceSheet()
  }

  onOpenModal = () => {
    this.setState({
      showModal: true,
    })
  }

  onOpenEmailModal = () => {
    this.props.onShowEmailModal()
    this.setState({
      emailText: '',
    })
  }

  onOpenManageCustomerModal = () => {
    this.props.onShowManageCustomerModal()
  }

  onAssignClient = (clientId: number) => {
    const priceSheetId = +this.props.match.params.id
    this.props.assignPriceSheetPricing({ priceSheetId: priceSheetId, clientId: clientId })
  }

  onClickEditInfo = () => {
    this.setState({
      editingInfo: true,
    })
  }

  onClickCancelEditInfo = () => {
    this.setState({
      editingInfo: false,
      editValues: {},
    })
  }

  onChangeEditInput = (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    this.setState({
      editValues: {
        ...this.state.editValues,
        [channel]: value,
      },
    })
  }

  onClickSaveInfo = () => {
    this.props.updatePricingPriceSheetInfo(this.state.editValues)
  }

  onSearch = (search: string) => {
    this.setState({
      search: search,
    })
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
    })
  }

  onShowDuplicate = () => {
    this.setState({
      showDuplicate: true,
    })
  }

  onCloseDuplicate = () => {
    this.setState({
      showDuplicate: false,
    })
  }

  duplicatePriceSheet = (priceSheetName: string) => {
    if (priceSheetName) {
      this.props.duplicatePriceSheetPricing({
        priceSheetName: priceSheetName,
        priceSheetId: this.props.match.params.id,
      })
    }
  }

  onCloseEmailModal = () => {
    this.props.closeEmailModal()
  }

  onCloseManageCustomerModal = () => {
    this.props.closeManageCustomerModal()
  }

  onAddItems = (addedItems: string[]) => {
    const params = {
      priceSheetId: this.props.match.params.id,
      assignItemListId: addedItems,
    }
    this.props.addPriceSheetItems(params)
    this.onCloseModal()
  }

  onRemoveItem = (priceSheetItemId: string) => {
    if (this.props.loadIndex.length > 0) return
    const newList = []
    for (const item of this.props.priceSheetItems) {
      if (item.priceSheetItemId !== priceSheetItemId) newList.push(item.itemId)
    }
    const params = {
      priceSheetId: this.props.match.params.id,
      assignItemListId: newList,
    }
    this.props.loadIndex(priceSheetItemId)
    this.props.assignPriceSheetItemsPricing(params)
  }

  onSelectCustomer = (value: string) => {
    this.setState({
      selectedCustomer: value,
    })
  }

  handleTextChange = (e: any) => {
    this.setState({ emailText: e.target.value })
  }

  sendEmail = () => {
    const customer = this.props.customers!.find((element) => element.customerId === this.state.selectedCustomer)
    const { radioType } = this.state
    if (radioType == 1 && customer == null) {
      notify('warn', 'Warning', 'You have not selected customer')
      return
    }
    this.props.setSending()
    this.props.sendEmailPricing({
      customer: customer,
      priceSheetId: this.props.match.params.id,
      message: this.state.emailText,
      type: this.state.radioType,
    })
  }

  toggleCompanyShared = () => {
    this.props.toggleCompanyShared(this.props.match.params.id)
  }

  renderOptions() {
    const { currentPriceSheet } = this.props
    if (!currentPriceSheet || !currentPriceSheet.assignedClients) {
      return null
    }
    const customers = currentPriceSheet.assignedClients
    return customers.sort((a: any, b: any) => a.clientCompany.companyName.localeCompare(b.clientCompany.companyName)).map((customer: any) => {
      return (
        <Select.Option value={customer.clientId} key={customer.clientId}>
          {customer.clientCompany && customer.clientCompany.companyName ? customer.clientCompany.companyName : 'N/A'}
        </Select.Option>
      )
    })
  }

  getAvailableClients = () => {
    const { currentPriceSheet, simplifyCustomers } = this.props
    const assignedClients = currentPriceSheet.assignedClients
    if (!assignedClients || !simplifyCustomers) return []
    const assignedIds: number[] = assignedClients.map((el) => el.clientId)
    const availableClients: TempCustomer[] = []
    simplifyCustomers.forEach((element) => {
      if (assignedIds.indexOf(element.clientId) < 0) {
        availableClients.push(element)
      }
    })
    return availableClients
  }

  onUpdateCatsView = () => {
    this.setState({
      isRemovedCats: !this.state.isRemovedCats,
    })
  }

  onShowModal = () => {
    this.setState({
      showModal: true,
    })
  }

  handlePrint = () => {
    const id = this.props.match.params.id
    history.push(`/pricesheet/${id}/print`)
  }

  changeRadioType = (e: any) => {
    this.setState({
      radioType: e.target.value,
    })
  }

  render() {
    const clientId = this.props.match.params.customerId
    const availbleClients = this.getAvailableClients()
    const assignedClients = this.props.currentPriceSheet.assignedClients
      ? this.props.currentPriceSheet.assignedClients
      : []
    const { radioType, selectedCustomer } = this.state
    return (
      <PageLayout noSubMenu={true}>
        <div style={{ paddingLeft: 40 }}>
          <NewOrderFormModal
            visible={this.props.showManageCustomerModal}
            onOk={this.onAssignClient}
            onCancel={this.onCloseManageCustomerModal}
            theme={this.props.theme}
            clients={availbleClients}
            fromPriceSheet={true}
            okButtonName="ASSIGN"
            isNewOrderFormModal={false}
          >
            <div>
              <label>Assigned Customers</label>
              <br />
              <AssignedClientWrapper>
                <Table
                  columns={this.column}
                  dataSource={assignedClients.sort((a: any, b: any) => {
                    return a.clientCompany.companyName.localeCompare(b.clientCompany.companyName)
                  })}
                  rowKey="clientId"
                  showHeader={false} />
              </AssignedClientWrapper>
            </div>
          </NewOrderFormModal>
          <PriceSheetAddModal
            ref={this.addItemsCmp}
            theme={this.props.theme}
            visible={this.state.showModal}
            onCancel={this.onCloseModal}
            onOk={this.onAddItems}
            saleItems={this.props.saleItems}
            priceSheetItems={this.props.priceSheetItems}
          />

          <PriceItemFormModal
            theme={this.props.theme}
            visible={this.state.showDuplicate}
            onOk={this.duplicatePriceSheet}
            onCancel={this.onCloseDuplicate}
          />
          <EmailPriceSheetModal
            visible={this.props.showEmailModal}
            onCancel={this.onCloseEmailModal}
            width={720}
            footer={[
              <div key="footer">
                <ThemeButton key="plus" type="primary" onClick={this.sendEmail} loading={this.props.sendingEmail}>
                  {!this.props.sendingEmail ? 'Send Email' : 'Sending..'}
                </ThemeButton>
                <Button onClick={this.onCloseEmailModal}>CANCEL</Button>
              </div>,
            ]}
          >
            <ModalHeader>Email Price Sheet</ModalHeader>
            <div>
              <ModalFormLabel>Email price sheet to </ModalFormLabel>
              <ModalFormRadio>
                <Radio.Group onChange={this.changeRadioType} defaultValue={radioType}>
                  <div>
                    <ThemeRadio value={1}>Individual customer</ThemeRadio>
                  </div>
                  {radioType == 1 && (
                    <CustomerSelector className="mt10">
                      <CustomerSelectorTitle>
                        <Select
                          style={{ width: '300px', marginLeft: '23px' }}
                          loading={!this.props.customers}
                          placeholder="Select Customer..."
                          css={selectStyles}
                          onChange={this.onSelectCustomer}
                          value={selectedCustomer}
                        >
                          {this.renderOptions()}
                        </Select>
                      </CustomerSelectorTitle>
                    </CustomerSelector>
                  )}
                  <div className="mt10">
                    <ThemeRadio value={2}>All assigned customers({assignedClients.length})</ThemeRadio>
                  </div>
                </Radio.Group>
              </ModalFormRadio>
            </div>

            <MessageBox>
              <ModalFormLabel>Message</ModalFormLabel>
              <MessageBoxInput
                autosize={{ minRows: 8, maxRows: 12 }}
                style={{ marginTop: '5px' }}
                onChange={this.handleTextChange}
              />
            </MessageBox>
          </EmailPriceSheetModal>
          <Container style={{ paddingBottom: 25 }}>
            <PricingDetailHeader
              match={this.props.match}
              onClickBack={this.onClickBack}
              editingInfo={this.state.editingInfo}
              onSaveItems={this.props.savePriceSheetItems}
              priceSheetInfo={this.props.currentPriceSheet}
              onChangeEditInput={this.onChangeEditInput}
              onClickCancelEditInfo={this.onClickCancelEditInfo}
              onClickEditInfo={this.onClickEditInfo}
              onClickSaveInfo={this.onClickSaveInfo}
              onOpenModal={this.onOpenModal}
              onOpenEmailModal={this.onOpenEmailModal}
              onOpenManageCustomerModal={this.onOpenManageCustomerModal}
              onShowDuplicate={this.onShowDuplicate}
              toggleCompanyShared={this.toggleCompanyShared}
              onSearch={this.onSearch}
              loadingIndex={this.props.loadingIndex}
              loadIndex={this.props.loadIndex}
              onPrint={this.handlePrint}
              isRemovedCats={this.state.isRemovedCats}
              onUpdateCatsView={this.onUpdateCatsView}
              getPriceSheetHistory={this.props.getPriceSheetHistory}
              priceSheetHistory={this.props.priceSheetHistory}
              priceSheetItems={this.props.priceSheetItems}
              saveBatchEdit={this.props.saveBatchEdit}
              currentCompanyUsers={this.props.currentCompanyUsers}
              getAllSalesRep={this.props.getAllSalesReps}
              updatePriceSheetInfo={this.props.updatePricingPriceSheetInfo}
            />
          </Container>

          <PricingDetailTable
            onCatogoryChange={this.props.updateCategory}
            priceSheetItems={this.props.priceSheetItems}
            priceSheetInfo={this.props.currentPriceSheet}
            search={this.state.search}
            onPriceSheetItemUpdate={this.props.updatePriceSheetItem}
            updatePriceSheetItemPricing={this.props.updatePriceSheetItemPricing}
            updatePriceSheetItemLogic={this.props.updatePriceSheetItemLogic}
            editAllMarginById={this.props.editAllMarginById}
            editAllMarkupById={this.props.editAllMarkupById}
            onRemoveItem={this.onRemoveItem}
            loadingIndex={this.props.loadingIndex}
            isRemovedCats={this.state.isRemovedCats}
            salesOrderItemHistory={this.props.salesOrderItemHistory}
            getSalesOrderItemHistory={this.props.getSalesOrderItemHistory}
            onSaveItems={this.props.savePriceSheetItemsPricing}
            setCurrentItemId={this.props.setCurrentItemId}
            clientId={typeof clientId !== 'undefined' ? clientId : ''}
            waiting={this.props.waiting}
            sellerSetting={this.props.sellerSetting}
          />
          {/* <PricingRightMenu /> */}
        </div>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.pricing

export const PricingDetail = withTheme(connect(PricingModule)(mapStateToProps)(PricingDetailComponent))
