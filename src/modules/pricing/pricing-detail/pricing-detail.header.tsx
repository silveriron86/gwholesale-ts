import React from 'react'
import { Input, Button, Icon, Spin, Tabs, Modal, Popover, Select, Tooltip } from 'antd'
import { Icon as IconSvg } from '~/components'
import {
  HeaderOptions,
  buttonStyle,
  Flex,
  Operation,
  BackButton,
  Name,
  InfoSpan,
  Header,
} from '../../pricesheet/components/pricesheet-detail-header.style'
import { Theme } from '~/common'
import { withTheme } from 'emotion-theming'
import { PriceSheet } from '~/schema'
import { PriceSheetDispatchProps } from '../../pricesheet/pricesheet.module'
import { match } from 'react-router'
import { ThemeOutlineButton, ThemeSelect, ThemeSwitch } from '~/modules/customers/customers.style'
import PriceHistory from './../component/modal/history-modal'
import BatchEdit from '../component/modal/batch-edit'
import { getLogicAndGroup } from '~/common/utils'
import PricingCsvExport from './pricing-csv-export'
import { Item } from '~/modules/customers/nav-sales/styles'

export interface Props {
  onPrint: () => void
  onOpenModal: () => void
  onOpenEmailModal: () => void
  onOpenManageCustomerModal: () => void
  onSearch: (search: string) => void
  onClickCancelEditInfo: () => void
  onClickEditInfo: () => void
  onShowDuplicate: () => void
  theme: Theme
  priceSheetInfo: PriceSheet | null
  editingInfo: boolean
  onClickSaveInfo: () => void
  onChangeEditInput: (channel: string) => (e: React.ChangeEvent<HTMLInputElement>) => void
  onClickBack: () => void
  onSaveItems: PriceSheetDispatchProps['savePriceSheetItems']
  toggleCompanyShared: () => void
  loadIndex: PriceSheetDispatchProps['loadIndex']
  match: match<{ id: string }>
  loadingIndex: string
  isRemovedCats: boolean
  onUpdateCatsView: Function
  getPriceSheetHistory: Function
  priceSheetHistory: any[]
  priceSheetItems: any[]
  saveBatchEdit: Function
  getAllSalesRep: Function
  currentCompanyUsers: any[]
  updatePriceSheetInfo: Function
}

class PricingDetailHeaderComponent extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props)
  }

  state = {
    visibleHistoryDlg: false,
    visibleBatchEditDlg: false,
    search: this.props.search
  }

  componentDidMount = () => {
    if (!this.props.currentCompanyUsers.length) {
      this.props.getAllSalesRep()
    }
  }

  onViewPricingHistory = () => {
    this.setState({ visibleHistoryDlg: true })
  }

  handleCancelHistoryModal = () => {
    this.setState({ visibleHistoryDlg: false })
  }

  handleBatchEditVisibleChange = (e: any) => {
    this.setState({ visibleBatchEditDlg: !this.state.visibleBatchEditDlg })
  }

  onClickBatchEdit = (data: any) => {
    const id = this.props.match.params.id
    let { defaultLogic, defaultGroup } = data
    this.props.saveBatchEdit({ priceSheetId: id, info: getLogicAndGroup(defaultLogic, defaultGroup) })
  }

  onCancelBatchEdit = () => {
    this.setState({ visibleBatchEditDlg: false })
  }

  renderBatchEditModal = () => {
    return <BatchEdit onClickSave={this.onClickBatchEdit} onCancelEdit={this.onCancelBatchEdit} />
  }

  componentWillReceiveProps(nextProps: Props) {
    if (
      this.props.priceSheetInfo &&
      nextProps.priceSheetInfo &&
      this.props.priceSheetInfo.name !== nextProps.priceSheetInfo.name
    ) {
      this.props.onClickCancelEditInfo()
    }
  }

  updatePriceSheetInfo = (type: string, value: any) => {
    const { priceSheetInfo } = this.props
    if (!priceSheetInfo) return
    const currentUserId = localStorage.getItem("id") || ''
    if (type == 'companyShared' && (!priceSheetInfo.user || (currentUserId != priceSheetInfo.user.userId && priceSheetInfo.companyShared))) {
      return
    }
    let obj = {}
    obj[type] = value
    this.props.updatePriceSheetInfo(obj)
  }

  render() {
    const { editingInfo, priceSheetInfo, currentCompanyUsers } = this.props
    const currentUserId = localStorage.getItem("id") || ''
    const HeaderButtons = editingInfo ? (
      <>
        <BackButton onClick={this.props.onClickSaveInfo}>
          <Icon type="save" />
          <span>SAVE</span>
        </BackButton>
        <BackButton onClick={this.props.onClickCancelEditInfo}>
          <Icon type="cancel" />
          <span>CANCEL</span>
        </BackButton>
      </>
    ) : (
      <BackButton onClick={this.props.onClickEditInfo}>
        <Icon type="edit" />
        <span>RENAME</span>
      </BackButton>
    )

    let PricesheetInfo = editingInfo ? (
      <span>
        {priceSheetInfo ? (
          <Input defaultValue={priceSheetInfo.name} size="large" onChange={this.props.onChangeEditInput('name')} />
        ) : (
          <Input defaultValue="N/A" size="large" onChange={this.props.onChangeEditInput('name')} />
        )}
      </span>
    ) : (
      <div>{priceSheetInfo ? <Name>{priceSheetInfo.name}</Name> : <Name>N/A</Name>}</div>
    )

    if (priceSheetInfo && priceSheetInfo.isDefault === true) {
      PricesheetInfo = <Name>{ }</Name>
    }

    const searchButton = (
      <React.Fragment>
        <IconSvg viewBox="0 0 16 16" width="16" height="16" type="search" />
        <span style={{ marginLeft: '9px', fontSize: '14px', lineHeight: '15px', letterSpacing: '0.05em' }}>SEARCH</span>
      </React.Fragment>
    )


    return (
      <Header className="pricing-detail-header">
        <Flex style={{ paddingTop: '16px', width: '100%', justifyContent: 'space-between' }}>
          <Operation onClick={this.props.onClickBack} style={{ marginBottom: 35 }}>
            <Icon type="arrow-left" /> BACK
          </Operation>
          <div style={{ minWidth: 800 }}>
            <Flex className="default-pricing-header">
              <Popover
                placement="bottom"
                content={this.renderBatchEditModal()}
                trigger="click"
                visible={this.state.visibleBatchEditDlg}
                onVisibleChange={this.handleBatchEditVisibleChange.bind(this)}
              >
                <Operation>
                  <Icon type="edit" /> BATCH EDIT
                </Operation>
              </Popover>
              <Operation onClick={this.props.onOpenManageCustomerModal}>
                <Icon type="user" /> MANAGE CUSTOMERS
              </Operation>
              <PricingCsvExport data={this.props.priceSheetItems} />
              <Operation onClick={this.props.onOpenEmailModal}>
                <Icon type="mail" /> EMAIL
              </Operation>
              <Operation onClick={this.props.onPrint}>
                <Icon type="printer" /> PRINT
              </Operation>
              <Operation onClick={this.props.onShowDuplicate}>
                <Icon type="copy" /> DUPLICATE
              </Operation>
            </Flex>
          </div>
        </Flex>
        <div className="info-editor">
          <Flex>
            <InfoSpan>{priceSheetInfo && priceSheetInfo.isDefault ? 'Price Rule' : 'Price Sheet'}</InfoSpan>
            {priceSheetInfo!.owner == true ? HeaderButtons : <span></span>}
          </Flex>
          <Flex className="name-area">
            <>
              {PricesheetInfo}
            </>
            <Flex className="shared-control">
              <Tooltip title={priceSheetInfo && !priceSheetInfo.companyShared ? `Set this price sheet to "Shared" in order ot change the owner` : ''}>
                <ThemeSelect
                  disabled={!priceSheetInfo || (priceSheetInfo && !priceSheetInfo.companyShared)}
                  className="sales-rep-dropdown"
                  value={!currentCompanyUsers.length || !priceSheetInfo || (priceSheetInfo && !priceSheetInfo.user) ? '' : priceSheetInfo.user.userId}
                  onChange={(userId: number) => this.updatePriceSheetInfo('userId', userId)}
                >
                  {
                    currentCompanyUsers.map((el: any) => {
                      return (
                        <Select.Option key={el.userId} value={el.userId}>
                          {`${el.firstName} ${el.lastName}`}
                        </Select.Option>
                      )
                    })
                  }
                </ThemeSelect>
              </Tooltip>
              <Tooltip title={priceSheetInfo && priceSheetInfo.user && (currentUserId != priceSheetInfo.user.userId && priceSheetInfo.companyShared)
                ? 'Change owner to pricesheet creator to disable sharing' : ''}>
                <ThemeSwitch
                  checked={priceSheetInfo ? priceSheetInfo.companyShared : false}
                  onChange={(val: boolean) => this.updatePriceSheetInfo('companyShared', val)}
                />
              </Tooltip>
              <Item>{priceSheetInfo && priceSheetInfo.companyShared ? 'Shared' : 'Not Shared'}</Item>
              <Tooltip title={"Shared price sheets can be accessed/edited by other users in the company. Price sheets that are not shared can only be accessed by the designated sales representative."}>
                <Icon style={{ marginLeft: 4 }} type="info-circle" />
              </Tooltip>
            </Flex>
          </Flex>
        </div>
        <Flex style={{ marginTop: 20, justifyContent: 'space-between' }}>
          <HeaderOptions className="flex-start">
            <Input.Search
              placeholder="Search item name, category, or UPC"
              size="large"
              style={{ width: '557px', border: '0' }}
              onSearch={this.props.onSearch}
              enterButton={searchButton}
              value={this.state.search}
              onChange={(evt) => this.setState({ search: evt.target.value })}
              allowClear
            />
          </HeaderOptions>
          <>
            {priceSheetInfo!.owner == true && (
              <HeaderOptions>
                <ThemeOutlineButton
                  onClick={this.onViewPricingHistory}
                  shape="round"
                  style={{
                    ...buttonStyle,
                    border: `1px solid ${this.props.theme.primary}`,
                  }}
                >
                  <Icon type="check" />
                  View Pricing History
                </ThemeOutlineButton>

                <Button
                  size="large"
                  icon="plus"
                  type="primary"
                  style={{
                    ...buttonStyle,
                    border: `1px solid ${this.props.theme.primary}`,
                    backgroundColor: this.props.theme.primary,
                  }}
                  onClick={this.props.onOpenModal}
                >
                  Add New Item
                </Button>
              </HeaderOptions>
            )}
          </>
          <Modal
            onCancel={this.handleCancelHistoryModal}
            visible={this.state.visibleHistoryDlg}
            okButtonProps={{ style: { display: 'none' } }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={'75%'}
          >
            <PriceHistory
              priceSheetId={this.props.match.params.id}
              priceSheetHistory={this.props.priceSheetHistory}
              getPriceSheetHistory={this.props.getPriceSheetHistory}
            />
          </Modal>
        </Flex>
      </Header>
    )
  }
}

export const PricingDetailHeader = withTheme(PricingDetailHeaderComponent)
