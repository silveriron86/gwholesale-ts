import React, { FunctionComponent } from 'react'
import { PricingRecordDetailWrapper } from '../pricing.style'
import { Row, Col } from 'antd'
import { RecordPricingGroup } from './record-pricing-group'
import { RecordPricingHistory } from './record-pricing-history'
import moment, { Moment } from 'moment'

type RecordDetail = {
  product: any
  onClickSave: Function
  getSalesOrderItemHistory: Function
  salesOrderItemHistory: any[]
  setCurrentItemId: Function
  isDefault: boolean
  clientId: string
}
export class PricingRecordDetail extends React.PureComponent<RecordDetail> {
  state = {
    from: moment().subtract(1, 'month'),
    to: moment(),
  }
  componentDidMount() {

  }

  onSaveChanges = (data: any) => {
    this.props.onClickSave(...data)
  }

  onHistoryDateChange = (type: string, date: any) => {
    let val = { ...this.state }
    val[type] = date
    this.setState({ ...val })
  }

  render() {
    const { isDefault } = this.props
    return (
      <PricingRecordDetailWrapper style={{ paddingTop: 10 }}>
        <Row>
          {
            !isDefault && <Col span={12}>
              <RecordPricingGroup product={this.props.product} onClickSave={this.onSaveChanges}
                isDefault={isDefault} />
            </Col>
          }

          <Col span={12}>
            <RecordPricingHistory
              product={this.props.product}
              clientId={this.props.clientId}
              onHistoryDateChange={this.onHistoryDateChange}
              getSalesOrderItemHistory={this.props.getSalesOrderItemHistory}
              salesOrderItemHistory={this.props.salesOrderItemHistory}
              setCurrentItemId={this.props.setCurrentItemId}
            />

          </Col>
        </Row>
      </PricingRecordDetailWrapper>
    )
  }
}
