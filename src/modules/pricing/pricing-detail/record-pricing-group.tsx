import React from 'react'
import { PricingRecordDetailWrapper, PricesheetPricingTable, SubTitle, LevelSave } from '../pricing.style'
import { Row, Col, Icon, Table } from 'antd'
import { PricingGroupTableWrapper } from '~/modules/product/components/product.component.style'
import PricingOptions from '~/components/pricing-group-options'
import moment from 'moment'
import { PricingTableWrapper } from '~/modules/product/product.style'
import { getLogicAndGroup } from '~/common/utils'

type RecordProps = {
  product: any
  onClickSave: Function
  isDefault: boolean
}

export class RecordPricingGroup extends React.PureComponent<RecordProps> {
  columns = [
    {
      title: 'LEVEL',
      dataIndex: 'level',
      width: '10%',
      ellipsis: true,
    },
    {
      title: 'COST',
      dataIndex: 'cost',
      width: '10%',
    },

    // {
    //   title: 'LOW',
    //   dataIndex: 'low',
    //   width: '10%'
    // },
    // {
    //   title: 'HIGH',
    //   dataIndex: 'high',
    //   width: '10%'
    // },
    {
      title: 'LAST UPDATED',
      dataIndex: 'updatedDate',
    },
    {
      title: 'PRICING FORMULA',
      dataIndex: 'margin',

      render: (margin: number, record: any, index: number) => {
        // return margin != 0 ? `${margin}%, $${record.markup}` : `$${record.markup}`
        return `${margin}%`
      },
    },
  ]

  state = {
    pricingType: '',
    defaultGroup: this.props.product.defaultGroup,
    defaultLogic: this.props.product.defaultLogic,
    dataSource: [],
  }

  componentDidMount() {
    const { product } = this.props
    if (product) {
      console.log(product)
      const groups = ['A', 'B', 'C', 'D', 'E']
      let result: any[] = []
      groups.forEach((el) => {
        result.push({
          level: el,
          cost: product.cost,
          low: product[`low${el}`],
          high: product[`high${el}`],
          updatedDate: moment(product.updatedDate).format('MM/DD/YYYY h:mm A'),
          margin: product[`margin${el}`],
          markup: product[`markup${el}`],
        })
      })
      this.setState({ dataSource: result })
    }
  }

  setPricingType = (val: string) => {
    this.setState({ pricingType: val })
  }

  onSaveChanges = () => {
    const { defaultLogic, defaultGroup } = this.state
    if (defaultLogic != '') {
      this.props.onClickSave(getLogicAndGroup(defaultLogic, defaultGroup))
    }
  }

  onGroupSelectionChanged = (value: string) => {
    this.setState({ defaultGroup: value })
  }

  onChangePricingLogic = (value: string) => {
    this.setState({ defaultLogic: value, defaultGroup: '' })
  }

  render() {
    const { product, isDefault } = this.props
    // console.log(isDefault);
    return (
      <div style={{ padding: 4 }}>
        <Row type="flex" justify="space-between" style={{ alignItems: 'center' }}>
          <Col>
            <SubTitle>Pricing Levels</SubTitle>
          </Col>
          <Col>
            <LevelSave onClick={this.onSaveChanges}>
              <Icon type="save" />
              Save Changes
            </LevelSave>
          </Col>
        </Row>
        <Row>
          <PricingGroupTableWrapper>
            <PricingOptions
              logicTitle={'Product Pricing Strategy'}
              groupTitle={'Pricing Group'}
              isDefault={isDefault}
              defaultGroup={product.defaultGroup}
              defaultLogic={product.defaultLogic}
              onSelectedGroupChange={this.onGroupSelectionChanged}
              onSelectedLogicChange={this.onChangePricingLogic}
            />
            {/* <PricingOptions type='pricesheet' setPricingType={this.setPricingType}/> */}
            <PricesheetPricingTable>
              <PricingTableWrapper>
                <Table columns={this.columns} dataSource={this.state.dataSource} pagination={false} />
              </PricingTableWrapper>
            </PricesheetPricingTable>
          </PricingGroupTableWrapper>
        </Row>
      </div>
    )
  }
}
