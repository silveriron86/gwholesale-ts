import React from 'react'
import Highcharts from 'highcharts'
import { SubTitle } from '../pricing.style'
import { Row, Col, DatePicker } from 'antd'
import { PricingGroupTableWrapper } from '~/modules/product/components/product.component.style';
import moment from 'moment'
import { InputLabel } from '~/modules/customers/customers.style';
import HighchartsReact from 'highcharts-react-official';
import { Icon } from '~/components/icon';

type RecordProps = {
  product: any
  salesOrderItemHistory: any[]
  onHistoryDateChange: Function
  getSalesOrderItemHistory: Function
  setCurrentItemId: Function
  clientId: string
}

const calendarIcon = <Icon type="dark-calendar" viewBox="0 0 14 14" />

export class RecordPricingHistory extends React.PureComponent<RecordProps> {
  state = {
    from: moment().subtract(1, 'month'),
    to: moment(),
    itemId: this.props.product ? this.props.product.itemId : 0
    // history: []
  }

  componentDidMount() {
    this.loadPriceHistory()
  }

  componentWillReceiveProps(nextProps: RecordProps) {
    const { itemId } = this.state
    const { salesOrderItemHistory } = nextProps
    console.log(salesOrderItemHistory)
  }

  loadPriceHistory = () => {
    if (this.state.itemId) {
      this.props.setCurrentItemId(this.state.itemId)
      this.props.getSalesOrderItemHistory({ itemId: this.state.itemId, clientId: this.props.clientId, from: this.state.from.format('MM/DD/YYYY'), to: this.state.to.format('MM/DD/YYYY') })
    }
  }

  onFromDateChange = (_dateMoment: any, dateString: string) => {
    this.setState({
      from: _dateMoment
    }, () => {
      this.loadPriceHistory()
    })
    this.props.onHistoryDateChange('from', _dateMoment)
  }

  onToDateChange = (_dateMoment: any, dateString: string) => {
    this.setState({
      to: _dateMoment
    }, () => {
      this.loadPriceHistory()
    })
    this.props.onHistoryDateChange('to', _dateMoment)
  }

  render() {
    const { from, to, itemId } = this.state
    const { salesOrderItemHistory } = this.props
    const history = salesOrderItemHistory[itemId]
    console.log(itemId, history)
    let prices: number[] = []
    let dates: string[] = []
    if (history && history.length > 0) {
      history.sort((a: any, b: any) => {
        return moment(a.deliveryDate).unix() - moment(b.deliveryDate).unix()
      })
      history.forEach((item: any) => {
        prices.push(item.price)
        dates.push(moment(item.deliveryDate).format('MM/DD/YY'))
      })
    }
    return (
      <div style={{ padding: 4 }}>
        <div><SubTitle>Sales History</SubTitle></div>
        <PricingGroupTableWrapper>
          <Row>
            <Col span={8}>
              <InputLabel style={{ paddingBottom: 0 }}>From:</InputLabel>
              <DatePicker
                placeholder={'MM/DD/YYYY'}
                format={'MM/DD/YYYY'}
                suffixIcon={calendarIcon}
                defaultValue={from}
                allowClear={false}
                className={'pricing-history-picker'}
                onChange={this.onFromDateChange}
                style={{ marginTop: 5 }}
              />
            </Col>
            <Col span={8}>
              <InputLabel style={{ paddingBottom: 0 }}>To:</InputLabel>
              <DatePicker
                placeholder={'MM/DD/YYYY'}
                format={'MM/DD/YYYY'}
                suffixIcon={calendarIcon}
                defaultValue={to}
                allowClear={false}
                className={'pricing-history-picker'}
                onChange={this.onToDateChange}
                style={{ marginTop: 5 }}
              />
            </Col>
          </Row>
          <Row>
            <div style={{ marginTop: 20 }}>
              <HighchartsReact
                highcharts={Highcharts}
                allowChartUpdate={true}
                options={{
                  title: {
                    text: ''
                  },
                  credits: {
                    enabled: false
                  },
                  legend: {
                    enabled: false,
                  },
                  xAxis: {
                    categories: dates
                  },
                  yAxis: {
                    title: {
                      text: 'Item Price'
                    }
                  },
                  series: [{
                    data: prices,
                  }],
                  chart: {
                    height: 300,
                    type: 'area'
                  },
                  plotOptions: {
                    series: {
                      fillColor: {
                        linearGradient: [0, 0, 0, 300],
                        stops: [
                          [0, 'rgba(189, 219, 195, 1)'],
                          [1, 'rgba(189, 219, 195, 0)']
                        ]
                      },
                      lineColor: 'rgba(82, 158, 99, 1)',
                      marker: {
                        enabled: true,
                        fillColor: 'rgba(82, 158, 99, 1)',
                        lineWidth: 1,
                        radius: 3
                      }
                    }
                  },
                }}
              />
            </div>
          </Row>
        </PricingGroupTableWrapper>
      </div>
    )
  }
}
