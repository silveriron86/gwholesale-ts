import styled from '@emotion/styled'
import { Table } from 'antd'

export const Stylehead = styled.div`
  text-align: left;
  color: #373d3f;
  font-size: 20px;
  font-weight: 600;

  .sub-title {
    font-size: 12px;
    font-weight: 700;
    color: #555f61;
  }
  .title {
    color: #555f61;
    font-size: 24px;
    font-weight: 700;
    line-height: 144%;
    letter-spacing: 0.05em;
  }
`

export const StyleDiv = styled.div`
  display: flex;
  text-align: left;

  .select-cost {
    margin-left: 50px;

    .label {
      color: #4a5355;
      font-size: 12px;
      line-height: 12px;
      letter-spacing: 0.05em;
      align-items: center;
      padding: 10px 0;
      font-weight: bold;
    }
  }
`

export const StyleTable = styled(Table)`
  th,
  td {
    &:first-child {
      border-bottom: none;
      border-right: 25px solid #fff;
      position: relative;
      &::after {
        content: '';
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        height: 1px;
        background-color: #edf1ee;
      }
    }
  }
`

export const StyledCustomerProductTable =  styled.div`
  padding-bottom: 30px;

  .ant-table-footer {
    padding: 0 !important;
    margin: 0;
  }
`

export const StyledAutoComplete = styled.div`
  position: relative;
  height: 0;
  text-align: left;
  background-color: #fff;

  .add-product-auto-complete {
    position: absolute;
    top: 16px;
    width: 380px;
  }
`

export const StyledAutoCompleteOption = styled.p`
  margin: 0;
  display: flex;
  align-items: center;

  b {
    margin-left: auto;
    font-weight: 400;
  }

  span {
    padding-right: 10px;
    flex: 1;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
`
