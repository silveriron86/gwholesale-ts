import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { AuthUser, UserRole } from '~/schema';
import PageLayout from '~/components/PageLayout';
import { PricingModule, PricingDispatchProps, PricingStateProps } from './pricing.module'
import { PricingContainerHeader } from './component/container.header'
import { PricingTable } from './component/pricing-table'
import { PriceItemFormModal } from './component/modal/price-item-form'
import { ListHeader, Splitter } from '../pricesheet/components/pricesheets-table.style'
import { PriceSheetListRow } from './component/pricesheet-list-item'
import { isSeller } from '~/common/utils'
import { ThemeSpin } from '../customers/customers.style'

export type PricingContainerProps = PricingDispatchProps & PricingStateProps & RouteComponentProps & {
  theme: Theme,
  currentUser: AuthUser
}

export class PricingContainerComponent extends React.PureComponent<PricingContainerProps> {
  state = {
    showModal: false,
    search: '',
    sortKey: 'priceSheetId',
    sortDirection: 'asc'
  }

  componentDidMount() {
    this.props.getAllCustomersPricing()
    const { currentUser } = this.props
    if (isSeller(currentUser.accountType))
      this.props.getCompanyPriceSheetsPricing()
    else if (currentUser.accountType === UserRole.CUSTOMER)
      this.props.getCustomersPriceSheetsPricing()
  }

  onSearch = (value: string) => {
    this.setState({
      search: value,
    })
  }

  onShowModal = () => {
    this.setState({
      showModal: true,
    })
  }

  onAddPriceSheet = (priceSheetName: string) => {
    if (priceSheetName) {
      this.props.createPriceSheetPricing(priceSheetName)
    }
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
    })
  }

  resortData = (sortKey: string, sortDirection: string) => {
    this.setState({ sortKey, sortDirection })
  }

  render() {
    const { sortKey, sortDirection } = this.state
    const { loadingPricing } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu="menu-Inventory-Price Sheets">
        <PricingContainerHeader onSearch={this.onSearch} onShowModal={this.onShowModal} search={this.state.search} />
        <ThemeSpin spinning={loadingPricing} style={{height: 'calc(100vh - 300px)'}}>
          <div style={{ marginLeft: 89, paddingBottom: 20 }}>
            <ListHeader style={{marginLeft: 0}}>
              <PriceSheetListRow
                key={-1}
                isHeader={true}
                sortByKeyAndDirection={this.resortData}
              />
            </ListHeader>
            <Splitter />

            <PricingTable priceSheets={this.props.priceSheets} search={this.state.search} sortKey={sortKey} sortDirection={sortDirection} />
          </div>
        </ThemeSpin>
        <PriceItemFormModal
          theme={this.props.theme}
          visible={this.state.showModal}
          onOk={this.onAddPriceSheet}
          onCancel={this.onCloseModal}
        />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => { return { ...state.pricing, currentUser: state.currentUser } }

export const PriceContainer = withTheme(connect(PricingModule)(mapStateToProps)(PricingContainerComponent))
