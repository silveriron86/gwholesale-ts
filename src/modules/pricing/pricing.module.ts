import {
  Module,
  EffectModule,
  ModuleDispatchProps,
  Effect,
  StateObservable,
  Reducer,
  DefineAction,
} from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { map, switchMap, endWith, catchError, takeUntil, startWith } from 'rxjs/operators'
import { push } from 'connected-react-router'
import { Action } from 'redux-actions'
import { pickBy, sortBy } from 'lodash'
import produce from 'immer'

import { PriceSheet, PriceSheetItem, SaleItem, SaleSection, CustomerPriceSheet, TempCustomer } from '~/schema'
import { GlobalState } from '~/store/reducer'
import { MessageType } from '~/components'
import { checkError, responseHandler, formatPriceSheetItem } from '~/common/utils'
import { PricingService } from './pricing.service'
import { CustomerService } from '../customers/customers.service'
import { SettingService } from '../setting/setting.service'
import { PriceSheetService } from '../pricesheet/pricesheet.service'

export interface PricingStateProps {
  waiting: boolean
  loadingIndex: string
  priceSheets: PriceSheet[]
  customerPriceSheets: PriceSheet[]
  currentPriceSheet: PriceSheet
  showManageCustomerModal: boolean
  showEmailModal: boolean
  sendingEmail: boolean
  clients: any[]
  customers: any[]
  priceSheetItems: PriceSheetItem[]
  saleItems: SaleItem[]
  pricingGroup: any
  priceSheetItemHistory: any[]
  salesOrderItemHistory: any
  priceSheetHistory: any[]
  defaultPriceSheetCustomer: TempCustomer
  defaultPriceSheetObj: any
  currentItemId: number
  simplifyCustomers: any
  currentCompanyUsers: any[]
  sellerSetting: any
  loadingPricing: boolean
  customerProductList: any[]
  customerProductListModified: boolean
  customerProductTotal: number
}

@Module('pricesheet')
export class PricingModule extends EffectModule<PricingStateProps> {
  defaultState = {
    waiting: false,
    loadingIndex: '',
    priceSheets: [],
    customerPriceSheets: [],
    currentPriceSheet: {} as PriceSheet,
    showManageCustomerModal: false,
    showEmailModal: false,
    clients: [],
    customers: [],
    sendingEmail: false,
    priceSheetItems: [],
    saleItems: [],
    pricingGroup: null,
    priceSheetItemHistory: [],
    salesOrderItemHistory: {},
    priceSheetHistory: [],
    defaultPriceSheetCustomer: {} as TempCustomer,
    defaultPriceSheetObj: { saving: false, loadingList: false },
    currentItemId: -1,
    simplifyCustomers: [],
    currentCompanyUsers: [],
    sellerSetting: null,
    loadingPricing: true,
    customerProductList: [],
    customerProductListModified: false,
    customerProductTotal: 0,
  }

  lastSearchParams: any = {}

  @DefineAction() dispose$!: Observable<void>

  constructor(
    private readonly pricing: PricingService,
    private readonly customer: CustomerService,
    private readonly setting: SettingService,
    private readonly priceSheet: PriceSheetService,
  ) {
    super()
  }

  @Reducer()
  resetLoading(state: PricingStateProps) {
    return {
      ...state,
      waiting: true,
    }
  }

  @Reducer()
  resetDefaultPriceSheet(state: PricingStateProps) {
    this.lastSearchParams = {}
    return {
      ...state,
      defaultPriceSheetCustomer: {},
      defaultPriceSheetObj: { saving: false, loadingList: false },
      customerProductList: [],
      customerProductListModified: false,
      customerProductTotal: 0,
    }
  }

  @Reducer()
  loadIndex(state: PricingStateProps, action: Action<string>) {
    return {
      ...state,
      loadingIndex: action.payload,
    }
  }

  @Reducer()
  resetPriceSheet(state: PricingStateProps, action: Action<void>) {
    return {
      ...state,
      currentPriceSheet: {},
      priceSheetItems: [],
    }
  }

  @Reducer()
  updateCategory(state: PricingStateProps, action: Action<SaleSection>) {
    const { name, wholesaleSectionId } = action.payload!
    return {
      ...state,
      priceSheetItems: state.priceSheetItems.map(
        produce((item) => {
          if (item.wholesaleCategory.wholesaleSection.wholesaleSectionId === wholesaleSectionId) {
            item.wholesaleCategory.wholesaleSection.name = name
          }
        }),
      ),
    }
  }

  @Reducer()
  updatePriceSheetItem(
    state: PricingStateProps,
    action: Action<{ priceSheetItemId: string; data: Partial<PriceSheetItem> }>,
  ) {
    const { priceSheetItemId, data } = action.payload!

    return {
      ...state,
      priceSheetItems: state.priceSheetItems.map((priceSheetItem) => {
        if (priceSheetItem.priceSheetItemId === priceSheetItemId) {
          return { ...priceSheetItem, ...data }
        }
        return priceSheetItem
      }),
    }
  }

  @Reducer()
  updateProductItem(
    state: PricingStateProps,
    action: Action<{ priceSheetItemId: string; data: Partial<PriceSheetItem> }>,
  ) {
    const { priceSheetItemId, data } = action.payload!

    return {
      ...state,
      customerProductListModified: true,
      customerProductList: state.customerProductList.map((priceSheetItem) => {
        if (priceSheetItem.priceSheetItemId === priceSheetItemId) {
          return { ...priceSheetItem, ...data }
        }
        return priceSheetItem
      }),
    }
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, showManageCustomerModal: true }
    },
  })
  onShowManageCustomerModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, showManageCustomerModal: false }
    },
  })
  closeManageCustomerModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, sendingEmail: true }
    },
  })
  setSending(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, showEmailModal: true }
    },
  })
  onShowEmailModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, showEmailModal: false }
    },
  })
  closeEmailModal(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Saved Price Sheet Items Successfully', type: MessageType.SUCCESS, loadingIndex: '' }
    },
  })
  savePriceSheetItemsPricing(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((id: string) => {
        return this.pricing
          .savePriceSheetItems({
            priceSheetId: id,
            priceSheetItemList: state$.value.pricing.priceSheetItems,
          })
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          )
      }),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Save product list successfully', type: MessageType.SUCCESS }
    },
  })
  saveProductPricing(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => {
        return this.pricing
          .savePriceSheetItems({
            priceSheetId: data.priceSheetId,
            priceSheetItemList: state$.value.pricing.customerProductList,
          })
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getCustomerProductList)()),
            catchError((error) => of(checkError(error))),
          )
      }),
    )
  }

  @Effect({
    done: (state: PricingStateProps, { payload }: Action<any>) => {
      let currentPriceSheet = { ...state.currentPriceSheet }
      if (payload.id > 0 && payload.client) {
        currentPriceSheet.assignedClients.push(payload.client)
      }
      return {
        ...state,
        currentPriceSheet: currentPriceSheet,
        showManageCustomerModal: false,
        message: 'Assigned Customer Successfully',
        type: payload.id > 0 ? MessageType.SUCCESS : MessageType.ERROR,
      }
    },
    error_message: (state: PricingStateProps) => {
      return {
        ...state,
        showManageCustomerModal: false,
        message: 'Assign PriceSheet Failed',
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  assignPriceSheetPricing(
    action$: Observable<{ priceSheetId: number; clientId: number }>,
    state$: StateObservable<GlobalState>,
  ) {
    return action$.pipe(
      switchMap((data) =>
        this.customer.assignPriceSheet(data.priceSheetId, data.clientId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCustomersPricing)(state$.value.pricing.currentPriceSheet.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        showManageCustomerModal: false,
        message: 'Assigned Customer Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: PricingStateProps) => {
      return {
        ...state,
        showManageCustomerModal: false,
        message: 'Assign PriceSheet Failed',
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  unAssignCustomerFromPricesheet(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((clientId) =>
        this.pricing.unAssignCustomerFromPricesheet(state$.value.pricing.currentPriceSheet.priceSheetId, clientId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getPriceSheetInfoPricing)(state$.value.pricing.currentPriceSheet.priceSheetId),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<PriceSheet>) => {
      return {
        ...state,
        currentPriceSheet: action.payload,
      }
    },
  })
  getPriceSheetInfoPricing(action$: Observable<string>) {
    return action$.pipe(
      switchMap((priceSheetId) =>
        this.pricing.getPriceSheetMainDetail(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, { payload }: Action<any[]>) => {
      return { ...state, clients: payload }
    },
  })
  getAllCustomersPricing(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser.userId).pipe(
          switchMap((data: any) => of(responseHandler(data).body.data)),
          takeUntil(this.dispose$),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, { payload }: Action<any>) => {
      return { ...state, customers: payload }
    },
  })
  getCustomersPricing(action$: Observable<string>) {
    return action$.pipe(
      switchMap((priceSheetId: string) =>
        this.pricing.getPriceSheetCustomers(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, { payload }: Action<any>) => {
      return { ...state, simplifyCustomers: payload }
    },
  })
  getSimplifyCustomersForPricing(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getSimplifyCustomers().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return {
        ...state,
        message: 'Price Sheet Email Sent',
        type: MessageType.SUCCESS,
        sendingEmail: false,
        showEmailModal: false,
      }
    },
  })
  sendEmailPricing(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.pricing.sendEmail(data.customer, data.priceSheetId, data.message, data.type).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Updated Price Sheet Successfully', type: MessageType.SUCCESS }
    },
  })
  toggleCompanyShared(action$: Observable<string>) {
    return action$.pipe(
      switchMap((priceSheetId: string) =>
        this.pricing.toggleCompanyShared(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetInfoPricing)(priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<{ [key: string]: string }>) => {
      const newValue = action.payload
      return {
        ...state,
        currentPriceSheet: newValue,
        customer: {
          // ...state.currentPriceSheet,
          ...newValue,
        },
        message: 'Price Sheet Info Saved',
        type: MessageType.SUCCESS,
      }
    },
  })
  updatePricingPriceSheetInfo(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    console.log('here called')
    return action$.pipe(
      switchMap((data: any) =>
        this.pricing
          .savePriceSheetInfo(
            state$.value.pricing.currentPriceSheet ? state$.value.pricing.currentPriceSheet.priceSheetId! : '0',
            data,
          )
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            endWith(
              this.createActionFrom(this.getPriceSheetInfoPricing)(
                state$.value.pricing.currentPriceSheet ? state$.value.pricing.currentPriceSheet.priceSheetId! : '0',
              ),
            ),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Duplicate Price Sheet Successful', type: MessageType.SUCCESS }
    },
  })
  duplicatePriceSheetPricing(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.pricing
          .duplicatePriceSheet(
            data.priceSheetName,
            state$.value.currentUser ? state$.value.currentUser.userId! : '0',
            data.priceSheetId,
          )
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map((data) => push(`/price/${data}`)),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<PriceSheetItem[]>) => {
      return {
        ...state,
        priceSheetItems: action.payload,
        waiting: false,
      }
    },
    error_message: (state: PricingStateProps) => {
      return {
        ...state,
        waiting: false,
        hasError: true,
      }
    },
  })
  getPriceSheetItemsPricing(action$: Observable<string>) {
    const processData = (data: PriceSheetItem[]) => {
      const sortedData = sortBy(data, 'wholesaleCategory.wholesaleSection.name')
      formatPriceSheetItem(sortedData)
      return sortedData
    }

    return action$.pipe(
      switchMap((priceSheetId) =>
        this.pricing.getPriceSheetItems(priceSheetId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(processData),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<PriceSheetItem[]>) => {
      return {
        ...state,
        priceSheetItems: action.payload,
        waiting: false,
      }
    },
    error_message: (state: PricingStateProps) => {
      return {
        ...state,
        waiting: false,
        hasError: true,
      }
    },
  })
  getPriceSheetItemsPricingWithClient(action$: Observable<any>) {
    const processData = (data: PriceSheetItem[]) => {
      const sortedData = sortBy(data, 'wholesaleCategory.wholesaleSection.name')
      formatPriceSheetItem(sortedData)
      return sortedData
    }

    return action$.pipe(
      switchMap((data) =>
        this.pricing.getPriceSheetItems(data.id, data.clientId).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(processData),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Updated Price Sheet Successfully', type: MessageType.SUCCESS }
    },
  })
  assignPriceSheetItemsPricing(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.pricing.assignPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetItemsPricing)(data.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return {
        ...state,
        message: 'Updated Product List Successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  assignProductListPricing(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.pricing.assignPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCustomerProductList)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Updated Price Sheet Successfully', type: MessageType.SUCCESS }
    },
  })
  addPriceSheetItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet.addPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetItemsPricing)(data.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Updated Product List Successfully', type: MessageType.SUCCESS }
    },
  })
  addProductItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.priceSheet.addPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCustomerProductList)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        saleItems: action.payload,
      }
    },
  })
  getSaleItemsPricing(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.pricing
          .getAllItems(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET')
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  //pricing group api
  @Effect({
    done: (state: PricingStateProps, action: Action<any>) => {
      return {
        ...state,
        pricingGroup: action.payload,
      }
    },
  })
  getPricingGroups(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.pricing.getPricingGroup().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps) => {
      return { ...state, message: 'Create Price Sheet Successful', type: MessageType.SUCCESS }
    },
  })
  createPriceSheetPricing(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((priceSheetName: string) =>
        this.pricing
          .createPriceSheet(priceSheetName, state$.value.currentUser ? state$.value.currentUser.userId! : '0')
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map((data) => push(`/price/${data}`)),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    before: (state: PricingStateProps) => {
      return {
        ...state,
        loadingPricing: true,
      }
    },
    done: (state: PricingStateProps, action: Action<PriceSheet[]>) => {
      return {
        ...state,
        priceSheets: action.payload,
        loadingPricing: false,
        loadingIndex: '',
      }
    },
  })
  getCompanyPriceSheetsPricing(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.pricing.getCompanyPriceSheets(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: PricingStateProps) => {
      return {
        ...state,
        loadingPricing: true,
      }
    },
    done: (state: PricingStateProps, action: Action<CustomerPriceSheet[]>) => {
      return {
        ...state,
        customerPriceSheets: action.payload,
        loadingPricing: false,
      }
    },
  })
  getCustomersPriceSheetsPricing(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.pricing
          .getCustomersPricesheets(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            startWith(this.createAction('before')()),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        priceSheetItemHistory: action.payload,
      }
    },
  })
  getPriceSheetItemHistory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((param) =>
        this.pricing.getPriceSheetItemHistory(param.itemId, param.from, param.to).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Reducer()
  setCurrentItemId(state: PricingStateProps, action: Action<number>) {
    return {
      ...state,
      currentItemId: action.payload,
    }
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      let history = { ...state.salesOrderItemHistory }
      history[state.currentItemId] = action.payload
      return {
        ...state,
        salesOrderItemHistory: history,
      }
    },
  })
  getSalesOrderItemHistory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((param) =>
        this.pricing.getSalesOrderItemHistory(param.itemId, param.clientId, param.from, param.to).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        priceSheetHistory: action.payload,
      }
    },
  })
  getPriceSheetHistory(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((id) =>
        this.pricing.getPriceSheetHistory(+id).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          switchMap((data) => of(this.formatPriceSheetHistory(data))),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'Batch edit pricing successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  saveBatchEdit(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.pricing.saveBatchEdit(data.priceSheetId, data.info).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getPriceSheetItemsPricing)(data.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'Batch edit pricing successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  saveBatchEditProduct(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.pricing.saveBatchEdit(data.priceSheetId, data.info).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCustomerProductList)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      console.log(action.payload)
      return {
        ...state,
        message: 'Updated pricing successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  updatePriceSheetItemPricing(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.pricing.updatePriceSheetItemPricing(data.id, data.info).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'Updated pricing logic successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  updatePriceSheetItemLogic(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.pricing.updatePriceSheetItemLogic(data.id, data.info).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getPriceSheetItemsPricing)(state$.value.pricing.currentPriceSheet.priceSheetId),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'Updated pricing logic successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  updateProductItemLogic(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(({ id, info, priceSheetId, clientId }) =>
        this.pricing.updatePriceSheetItemLogic(id, info).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCustomerProductList)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'All margin values were updated successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  editAllMarginById(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.pricing.editAllMarginById(data.id, data.value, data.categoryId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getPriceSheetItemsPricing)(state$.value.pricing.currentPriceSheet.priceSheetId),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'All markup values were updated successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  editAllMarkupById(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.pricing.editAllMarkupById(data.id, data.value, data.categoryId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getPriceSheetItemsPricing)(state$.value.pricing.currentPriceSheet.priceSheetId),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<TempCustomer>) => {
      return {
        ...state,
        defaultPriceSheetCustomer: action.payload,
      }
    },
  })
  getCustomer(action$: Observable<string>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.getCustomerInfo(customerId)),
      switchMap((data: any) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: PricingStateProps, { payload }: Action<any>) => {
      return { ...state, currentCompanyUsers: payload.userList }
    },
  })
  getAllSalesReps(action$: Observable<any>) {
    return action$.pipe(
      switchMap(() => this.customer.getCompanyUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    before: (state: any, action: any) => {
      return { ...state, defaultPriceSheetObj: { ...state.defaultPriceSheetObj, saving: true } }
    },
    done: (state: PricingStateProps, action: Action<TempCustomer>) => {
      return {
        ...state,
        defaultPriceSheetObj: { ...state.defaultPriceSheetObj, saving: false },
      }
    },
  })
  setClientDefaultLogic(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.pricing.setClientDefaultLogic(data.id, data.info).pipe(
          switchMap((data: any) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: any, action: any) => {
      return { ...state, defaultPriceSheetObj: { ...state.defaultPriceSheetObj, loadingList: true } }
    },
    done: (state: any, action: any) => {
      const { dataList = [], total = 0 } = action.payload || {}
      const customerProductList = dataList.map((item: any) => ({
        ...item,
        itemId: item.wholesaleItemId,
        SKU: item.sku,
      }))
      return {
        ...state,
        customerProductListModified: false,
        customerProductList,
        customerProductTotal: total,
        defaultPriceSheetObj: { ...state.defaultPriceSheetObj, loadingList: false },
      }
    },
  })
  getCustomerProductList(action$: Observable<{ priceSheetId: any; clientId: any }>) {
    return action$.pipe(
      switchMap((data: any) => {
        if (!data?.priceSheetId && !data?.clientId) {
          data = this.lastSearchParams
        }
        data = pickBy(data, (val) => val !== '' && val !== null && val !== void 0)
        this.lastSearchParams = data
        return this.pricing.getCustomerProductList(data).pipe(
          switchMap((data: any) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  formatPriceSheetHistory(datas: any[]): any[] {
    return datas.map((data) => ({
      id: data.id,
      sku: data.priceSheetItem.wholesaleItem.sku,
      variety: data.priceSheetItem.wholesaleItem.variety,
      group: data.newDefaultGroup,
      oldPrice: data.oldSalePrice,
      newPrice: data.newSalePrice,
      createdDate: data.createdDate,
      editBy: data.operatorUser.firstName + ' ' + data.operatorUser.lastName,
    }))
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any>) => {
      return {
        ...state,
        sellerSetting: action.payload.userSetting,
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getUserSetting(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(map(this.createAction('done'))),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: PricingStateProps, action: Action<any[]>) => {
      return state
    },
  })
  removeProduct(action$: any, state$: any) {
    return action$.pipe(
      switchMap((data: any) => {
        const { priceSheetId, deleteItemListId } = data
        return this.pricing.removeProduct(priceSheetId, deleteItemListId).pipe(
          switchMap((data: any) => of(responseHandler(data, true))),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCustomerProductList)()),
        )
      }),
      catchError((error) => of(checkError(error))),
    )
  }
}

export type PricingDispatchProps = ModuleDispatchProps<PricingModule>
