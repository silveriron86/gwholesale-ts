import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import { PriceSheetItem, PriceSheet, SaleItem, PriceSheetClient, CustomerPriceSheet } from '~/schema'

@Injectable()
export class PricingService {
  constructor(private readonly http: Http) {}

  static get instance() {
    return new PricingService(new Http())
  }

  getPricingGroup() {
    return this.http.get<any>(`/priceGroup`)
  }

  getPriceSheetMainDetail(priceSheetId: string) {
    return this.http.get<PriceSheet>(`/pricesheet/${priceSheetId}`)
  }

  getPriceSheetItems(priceSheetId: string, clientId: string = '') {
    let url = `/pricesheet/${priceSheetId}/items/list`
    if (clientId) {
      url += `?clientId=${clientId}`
    }
    return this.http.get<PriceSheetItem[]>(url)
  }

  getCustomerProductList(params: {
    priceSheetId: string
    clientId: string
    page: number
    pageSize: number
    searchKey?: string
    orderBy?: string
    direction?: string
  }) {
    const { priceSheetId, ...rest } = params
    return this.http.get(`/pricesheet/${priceSheetId}/items/pageList`, { query: rest })
  }

  getAllPriceSheet(userId: string) {
    return this.http.get<PriceSheet[]>(`/pricesheet/user/${userId}`)
  }

  getCompanyPriceSheets(userId: string) {
    return this.http.get<PriceSheet[]>(`/pricesheet/userCompany/${userId}`)
  }

  getPriceSheetCustomers(priceSheetId: string) {
    return this.http.get<PriceSheetClient[]>(`/pricesheet/${priceSheetId}/users/list`)
  }

  getAllItems(companyName: string) {
    return this.http.get<SaleItem[]>(`/inventory/${companyName}/items/list?type=PRICESHEET`)
  }

  createPriceSheet(priceSheetName: string, userId: string) {
    return this.http.post<any>(`/pricesheet/user/${userId}/new/${priceSheetName}`)
  }

  duplicatePriceSheet(priceSheetName: string, userId: string, priceSheetId: string) {
    return this.http.post<any>(`/pricesheet/user/${userId}/duplicate/${priceSheetId}/${priceSheetName}`)
  }

  savePriceSheetInfo(priceSheetId: string, data: any) {
    return this.http.post<any>(`/pricesheet/${priceSheetId}/update`, {
      body: JSON.stringify(data),
    })
  }

  sendEmail(customer: any, priceSheetId: string, message: string = '', type: number) {
    const data = {
      message: message,
      clientId: type == 1 ? customer.priceSheetClientId : '',
      type: type,
    }
    return this.http.post<any>(`/pricesheet/${priceSheetId}/sendEmail`, {
      body: JSON.stringify(data),
    })
  }

  savePriceSheetItems(data: any) {
    return this.http.post<any>('/pricesheet/update/item', {
      body: JSON.stringify(data),
    })
  }

  assignPriceSheetItems(data: any) {
    return this.http.post<any>('/pricesheet/assign/item', {
      body: JSON.stringify(data),
    })
  }

  getCustomersPricesheets(userId: string) {
    return this.http.get<CustomerPriceSheet>(`/account/user/${userId}/customer/pricesheets/list`)
  }

  toggleCompanyShared(priceSheetId: string) {
    return this.http.post<any>(`/pricesheet/toggleCompanyShared/${priceSheetId}`)
  }

  unAssignCustomerFromPricesheet(priceSheetId: string, clientId: number) {
    return this.http.delete<any>(`/pricesheet/unassign/${priceSheetId}/client/${clientId}`)
  }

  getPriceSheetItemHistory(itemId: string, from: string, to: string) {
    return this.http.get<CustomerPriceSheet>(`/priceHistory/priceSheetItem/${itemId}?from=${from}&to=${to}`)
  }

  getSalesOrderItemHistory(itemId: string, clientId: string, from: string, to: string) {
    return this.http.get<any>(
      `/account/orderItem/item/${itemId}?from=${from}&to=${to}${clientId ? `&clientId=${clientId}` : ''}`,
    )
  }

  getPriceSheetHistory(priceSheetId: number) {
    return this.http.get<any>(`/priceHistory/priceSheet/${priceSheetId}`)
  }

  saveBatchEdit(priceSheetId: number, info: any) {
    return this.http.put<any>(`/pricesheet/${priceSheetId}/batch-edit`, {
      body: JSON.stringify({ ...info }),
    })
  }

  updatePriceSheetItemLogic(itemId: string, info: any) {
    return this.http.put<any>(`/pricesheet/priceSheetItem/${itemId}/logic`, {
      body: JSON.stringify({ ...info }),
    })
  }

  updatePriceSheetItemPricing(itemId: string, info: any) {
    return this.http.put<any>(`/pricesheet/priceSheetItem/${itemId}`, {
      body: JSON.stringify({ ...info }),
    })
  }

  editAllMarginById(priceSheetId: string, value: number, categoryId?: number) {
    return this.http.put<any>(`/pricesheet/${priceSheetId}/edit-all/margin`, {
      body: JSON.stringify({ value, categoryId }),
    })
  }

  editAllMarkupById(priceSheetId: string, value: number, categoryId: number) {
    return this.http.put<any>(`/pricesheet/${priceSheetId}/edit-all/markup`, {
      body: JSON.stringify({ value, categoryId }),
    })
  }

  setClientDefaultLogic(clientId: number, info: any) {
    return this.http.put<any>(`/account/client/${clientId}/default-logic`, {
      body: JSON.stringify({ ...info }),
    })
  }

  createOrder(data: any) {
    return this.http.post(`/inventory/pricesheet/createOrder`, {
      body: JSON.stringify(data),
    })
  }

  removeProduct(priceSheetId: number, deleteItemListId: number[]) {
    return this.http.delete('/pricesheet/delete/priceSheetItem', {
      body: JSON.stringify({
        priceSheetId,
        deleteItemListId,
      }),
    })
  }
}
