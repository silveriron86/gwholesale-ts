import styled from "@emotion/styled";
import { Button, Row } from "antd";
import { hex2rgba } from "~/common/utils";

export const PricingRecordDetailWrapper = styled('div')({

})

export const PricesheetPricingTable = styled('div')({
    //   padding: 12
})

export const SubTitle = styled('span')((props: any) => ({
    color: props.theme.dark,
    fontSize: 20,
    fontWeight: 'bold'
}))

export const LevelSave = styled('div')((props: any) => ({
    color: props.theme.primary,
    cursor: 'pointer',
    fontWeight: 'bold',
    fontSize: 14,
    '& svg': {
        marginRight: 8
    },
    '& path': {
        fill: props.theme.primary,
    },
}))

export const PriceHistoryTableWrapper = styled('div')((props: any) => ({
    paddingTop: 20,
    '& .ant-table tbody': {
        maxHeight: 400,
        overflowY: 'auto',
        display: 'block'
    },
    '& .ant-table thead': {
        display: 'table',
        width: '100%',
        tableLayout: 'fixed'
    },
    '& .ant-table tbody tr': {
        display: 'table',
        width: '100%',
        tableLayout: 'fixed'
    },

}))

export const BatchEditView = styled('div')((props: any) => ({
    minWidth: '600px',
    '& .title': {
        color: props.theme.dark,
        fontWeight: 'bold',
        marginBottom: 30
    },
    '& .logic-options': {
        margin: '12px 0 36px'
    },
    '& .ant-btn': {
        margin: 2
    }
}))

export const BatchEditModalContainer = styled('div')((props: any) => ({
    textAlign: 'center'
}))

export const EditAllWrapper = styled(Button)((props) => ({
    background: hex2rgba(props.theme.light, 0.4),
    color: props.theme.primary,
    fontWeight: 'bold',
    padding: '4px 8px',
    position: 'absolute',
    top: '-25px',
    right: '300px',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    border: 'none'
}))

export const EditAllWrapperNew = styled(Button)((props) => ({
  background: hex2rgba(props.theme.light, 0.4),
  color: props.theme.primary,
  fontWeight: 'bold',
  padding: '4px 8px',
  position: 'absolute',
  top: '-40px',
  left: '0px',
  borderTopRightRadius: 10,
  borderTopLeftRadius: 10,
  border: 'none'
}))

export const ListItemRow = styled('div')((props: any) => ({
    padding: '10px 4px',
    '& .inner-wrapper': {

        display: 'flex',
        height: 70,
        '& .small-col': {
            minWidth: '140px',
            width: '10%',
        },
        '& .med-col': {
            minWidth: '200px',
            width: '15%',
        },
        '& .flex-col': {
            width: '95%',
        }
    }
}))

export const DropdownText = styled('div')((props: any) => ({

}))
