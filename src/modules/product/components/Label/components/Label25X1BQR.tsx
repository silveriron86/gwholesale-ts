import { Button, Checkbox, DatePicker, Input, Radio } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas2 from './LabelCanvas2'
import LabelCanvas25X1BQR from './LabelCanvas25X1BQR'
import { label251BQRObj, label252Obj } from './type/type'

interface IProps {
  labelObj: label251BQRObj
  saveLabel: (label: label252Obj) => void
}

const Label25X1BQR = ({ labelObj, saveLabel }: IProps): JSX.Element => {
  const [name, setProductName] = useState(labelObj.productName)
  const [note, setNote] = useState(labelObj.note)
  const [companyInformation, setCompanyInformation] = useState(labelObj.companyInfo)
  const [active, setActive] = useState(labelObj.x1B)
  const [sellByDate, setSellByDate] = useState(labelObj.sellByDate)
  const [sellByDatShow, setSellByDatShow] = useState(labelObj.sellByDateShow)

  const lot = labelObj.lot
  const origin = labelObj.origin
  const bar_code1 = labelObj.barcode1

  useEffect(() => {
    setProductName(labelObj.productName)
    setSellByDate(labelObj.sellByDate)
    setSellByDatShow(labelObj.sellByDateShow)
    setActive(labelObj.x1B)
    setCompanyInformation(labelObj.companyInfo)
    setNote(labelObj.note)
  }, [labelObj])

  const getLabelObj = (): label252Obj => {
    return {
      x1B: active,
      productName: name,
      lot: lot,
      origin: origin,
      barcode1: bar_code1,
      note: note,
      companyInfo: companyInformation,
      sellByDate: sellByDatShow ? sellByDate : '',
      sellByDateShow: sellByDatShow,
    }
  }
  const setDate = (e: any) => {
    const date = new Date(e)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    setSellByDate(`${month}-${day}-${year}`)
  }

  const time = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const save2X1BLabel = () => saveLabel(getLabelObj())

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '579px' }}>
        <div>
          <p style={{ fontSize: '18px', lineHeight: '25.2px' }}>Active</p>
          <Radio.Group
            onChange={() => {
              active == true ? setActive(false) : setActive(true)
            }}
            value={active}
          >
            <Radio value={true} style={{ fontSize: '15px', lineHeight: '21px' }}>
              Yes
            </Radio>
            <Radio value={false} style={{ fontSize: '15px', lineHeight: '21px' }}>
              No
            </Radio>
          </Radio.Group>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Label Type</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>Receiving</p>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Product Name</p>
          <Input
            style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
            value={name}
            onChange={(e) => setProductName(e.target.value)}
            disabled
          />
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '25%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Lot #</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px' }}>{lot}</p>
          </div>
          <div style={{ width: '25%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Origin</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px' }}>{origin}</p>
          </div>
          <div style={{ width: '50%', display: 'flex' }}>
            <Checkbox
              style={{ marginTop: '30px', marginRight: '10px' }}
              onChange={(e) => setSellByDatShow(e.target.checked)}
              checked={sellByDatShow}
            />
            <div>
              <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
                Sell By Date
              </p>
              <DatePicker
                defaultValue={moment(time(parseInt(sellByDate)), 'MM-DD-YYYY')}
                onChange={(e) => setDate(e)}
              />
            </div>
          </div>
        </div>
        <div style={{ marginTop: '21px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px' }}>Barcodes</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'bold', marginTop: '3px' }}>
            Code-128 <span style={{ fontWeight: 'normal' }}>{bar_code1}</span>
          </p>
        </div>
        <div style={{ display: 'inline' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Notes</p>
          <Input
            style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
            value={note}
            onChange={(e) => setNote(e.target.value)}
          />
        </div>
        <div style={{ display: 'inline' }}>
          <p
            style={{ fontSize: '18px', lineHeight: '25.2px', marginTop: '20px', marginBottom: '0px', color: '#4A5355' }}
          >
            Company Information
          </p>
          <Input
            style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
            value={companyInformation}
            onChange={(e) => setCompanyInformation(e.target.value)}
          />
        </div>
        <ThemeButton
          style={{ marginTop: '81px', width: '198px', height: '40px', borderRadius: '40px' }}
          onClick={() => save2X1BLabel()}
        >
          Save Changes
        </ThemeButton>
      </div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px' }}>Preview</p>
        <LabelCanvas25X1BQR label252Obj={getLabelObj()} />
      </div>
    </div>
  )
}

export default Label25X1BQR
