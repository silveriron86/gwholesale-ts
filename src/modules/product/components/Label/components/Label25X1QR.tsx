import { Button, Checkbox, DatePicker, Input, Radio } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas25X1AQR from './LabelCanvas25X1QR'
import { label251AQRObj, label251Obj } from './type/type'

interface IProps {
  labelObj: label251Obj
  saveLabel: (label: label251AQRObj) => void
}

const Label25X1QR = ({ labelObj, saveLabel }: IProps): JSX.Element => {
  const [name, setProductName] = useState(labelObj.productName)
  const [sellByDate, setSellByDate] = useState(labelObj.sellByDate)
  const [sellByDatShow, setSellByDatShow] = useState(labelObj.sellByDateShow)
  const [companyInformation, setCompanyInformation] = useState(labelObj.companyInfo)
  const [upcCode, setUpcCode] = useState(labelObj.barcode2)
  const [active, setActive] = useState(labelObj.x1A)

  const lot = labelObj.lot
  const origin = labelObj.origin
  const bar_code1 = labelObj.barcode1

  useEffect(() => {
    setProductName(labelObj.productName)
    setSellByDate(labelObj.sellByDate)
    setSellByDatShow(labelObj.sellByDateShow)
    setCompanyInformation(labelObj.companyInfo)
    setActive(labelObj.x1A)
    setUpcCode(labelObj.barcode2)
  }, [labelObj])

  const getLabelObj = (): label251Obj => {
    return {
      x1A: active,
      productName: name,
      lot: lot,
      origin: origin,
      sellByDate: sellByDatShow ? sellByDate : '',
      sellByDateShow: sellByDatShow,
      barcode1: bar_code1,
      barcode2: upcCode,
      companyInfo: companyInformation,
    }
  }

  const setDate = (e: any) => {
    const date = new Date(e)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    setSellByDate(`${day}-${month}-${year}`)
  }

  const time = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${day}-${month}-${year}`
  }

  const save2X1ALabel = () => saveLabel(getLabelObj())

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '579px' }}>
        <div>
          <p style={{ fontSize: '18px', lineHeight: '25.2px' }}>Active</p>
          <Radio.Group
            onChange={() => {
              active == true ? setActive(false) : setActive(true)
            }}
            value={active}
          >
            <Radio value={true} style={{ fontSize: '15px', lineHeight: '21px' }}>
              Yes
            </Radio>
            <Radio value={false} style={{ fontSize: '15px', lineHeight: '21px' }}>
              No
            </Radio>
          </Radio.Group>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Label Type</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>Receiving</p>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Product Name</p>
          <Input
            style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
            value={name}
            onChange={(e) => setProductName(e.target.value)}
            disabled
          />
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '25%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Lot #</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px' }}>{lot}</p>
          </div>
          <div style={{ width: '25%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Origin</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px' }}>{origin}</p>
          </div>
          <div style={{ width: '50%', display: 'flex' }}>
            <Checkbox
              style={{ marginTop: '30px', marginRight: '10px' }}
              onChange={(e) => setSellByDatShow(e.target.checked)}
              checked={sellByDatShow}
            />
            <div>
              <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
                Sell By Date
              </p>
              <DatePicker
                defaultValue={moment(time(parseInt(sellByDate)), 'DD-MM-YYYY')}
                onChange={(e) => setDate(e)}
              />
            </div>
          </div>
        </div>
        <div style={{ marginTop: '21px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>QR Code</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'bold', marginTop: '3px' }}>
            <span style={{ fontWeight: 'normal' }}>{bar_code1}</span>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'bold', marginTop: '3px' }}>
            UPC-A{' '}
            <Input
              style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
              value={upcCode}
              onChange={(e) => setUpcCode(e.target.value)}
            />
          </p>
          </p>
        </div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
          Company Information
        </p>
        <div style={{ display: 'inline' }}>
          <Input
            style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
            value={companyInformation}
            onChange={(e) => setCompanyInformation(e.target.value)}
          />
        </div>
        <ThemeButton
          style={{ marginTop: '81px', width: '198px', height: '40px', borderRadius: '40px' }}
          onClick={() => save2X1ALabel()}
        >
          Save Changes
        </ThemeButton>
      </div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginLeft: '33px' }}>Preview</p>
        <LabelCanvas25X1AQR label251Obj={getLabelObj()} />
      </div>
    </div>
  )
}

export default Label25X1QR
