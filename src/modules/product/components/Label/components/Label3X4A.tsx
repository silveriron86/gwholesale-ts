import { Button, Checkbox, DatePicker, Input, Radio } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas from './LabelCanvas'
import LabelCanvas34A from './LabelCanvas34A'
import { label34AObj, label43Obj } from './type/type'

interface IProps {
  labelObj: label34AObj
  saveLabel: (label: label34AObj) => void
}

const Label3X4A = ({ labelObj, saveLabel }: IProps): JSX.Element => {
  const [contractNumber, setContractNumber] = useState(labelObj.contractNumber)
  const [nsn, setNSN] = useState(labelObj.NSN ? labelObj.NSN : '')
  const [packDate, setPackDate] = useState(labelObj.packDate ? labelObj.packDate : '')
  const [sellByDate, setSellByDate] = useState(labelObj.sellByDate ? labelObj.sellByDate : '')
  const [note, setNote] = useState(labelObj.note ? labelObj.note : '')
  const [originEnable, setOriginEnable] = useState(false)
  const [packDateEnable, setPackDateEnable] = useState(false)
  const [bestBySellDateEnable, setBestBySellDateEnable] = useState(false)

  const productName = labelObj.productName
  const po = labelObj.PO
  const brand = labelObj.brand
  const packSize = labelObj.packSize
  const storage = labelObj.storage
  const lot = labelObj.lot
  const manufacturer = labelObj.manufacturer
  const mfgSku = labelObj.mfgSku
  const origin = labelObj.origin

  useEffect(() => {
    setContractNumber(labelObj.productName)
    setNSN(labelObj.NSN)
    setPackDate(labelObj.packDate)
    setSellByDate(labelObj.sellByDate)
    setNote(labelObj.note)
  }, [labelObj])

  const getLabelObj = (): label34AObj => {
    return {
      productName: productName,
      contractNumber: contractNumber,
      PO: po,
      NSN: nsn,
      brand: brand,
      packSize: packSize,
      storage: storage,
      origin: origin,
      originEnable: originEnable,
      packDate: packDate,
      packDateEnable: packDateEnable,
      sellByDate: sellByDate,
      sellByDateEnable: bestBySellDateEnable,
      lot: lot,
      manufacturer: manufacturer,
      note: note,
      mfgSku: mfgSku,
    }
  }

  const save4x3Label = () => {
    const obj = getLabelObj()
    saveLabel(obj)
  }

  const time = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const setNewSellbyDate = (e: any) => {
    const date = new Date(e)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    setSellByDate(`${month}-${day}-${year}`)
  }

  const setNewPackDate = (e: any) => {
    const date = new Date(e)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    setPackDate(`${month}-${day}-${year}`)
  }

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '579px' }}>
        <div>
          <p style={{ fontSize: '18px', lineHeight: '25.2px' }}>Active</p>
          <Radio.Group value={'Yes'}>
            <Radio value={'Yes'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              Yes
            </Radio>
            <Radio disabled value={'No'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              No
            </Radio>
          </Radio.Group>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Label Type</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>Shipping</p>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Product Name</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{productName}</p>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
              Contract Number
            </p>
            <Input
              style={{ marginTop: '4px', width: '80%' }}
              value={contractNumber}
              onChange={(e) => setContractNumber(e.target.value)}
            />
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>PO#</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{po}</p>
          </div>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>NSN#</p>
          <Input
            style={{ marginTop: '4px', marginLeft: '2px', width: '80%' }}
            value={nsn}
            onChange={(e) => setNSN(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Brand</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{brand}</p>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Pack Size</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{packSize}</p>
          </div>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Storage</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{storage}</p>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Origin</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>
              <Checkbox checked={originEnable} onChange={(e) => setOriginEnable(e.target.checked)} /> {origin}
            </p>
          </div>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Pack Date</p>
            <div style={{ display: 'flex' }}>
              <Checkbox
                style={{ marginTop: '4px', marginRight: '4px' }}
                checked={packDateEnable}
                onChange={(e) => setPackDateEnable(e.target.checked)}
              />
              <DatePicker
                defaultValue={moment(time(parseInt(sellByDate)), 'MM-DD-YYYY')}
                onChange={(e) => setNewPackDate(e)}
              />
            </div>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
              Best By Date
            </p>
            <div style={{ display: 'flex' }}>
              <Checkbox
                style={{ marginTop: '4px', marginRight: '4px' }}
                checked={bestBySellDateEnable}
                onChange={(e) => setBestBySellDateEnable(e.target.checked)}
              />
              <DatePicker
                defaultValue={moment(time(parseInt(sellByDate)), 'MM-DD-YYYY')}
                onChange={(e) => setNewSellbyDate(e)}
              />
            </div>
          </div>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Lot #</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{lot}</p>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
              Manufacturer
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{manufacturer}</p>
          </div>
        </div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Note</p>
        <div style={{ display: 'inline' }}>
          <Input
            style={{ marginTop: '4px', marginLeft: '2px', width: '80%' }}
            value={note}
            onChange={(e) => setNote(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>MFG SKU</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{mfgSku}</p>
        </div>
        <ThemeButton
          style={{ marginTop: '31px', width: '198px', height: '40px', borderRadius: '40px' }}
          onClick={() => save4x3Label()}
        >
          Save Changes
        </ThemeButton>
      </div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginLeft: '33px' }}>Preview</p>
        <LabelCanvas34A labelObj={getLabelObj()} />
      </div>
    </div>
  )
}

export default Label3X4A
