import { Checkbox, Input, Radio } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas34B from './LabelCanvas34B'
import { label34BObj } from './type/type'

interface IProps {
  labelObj: label34BObj
  saveLabel: (label: label34BObj) => void
}

const Label3X4B = ({ labelObj, saveLabel }: IProps): JSX.Element => {
  const [contractNumber, setContractNumber] = useState(labelObj.contractNumber)
  const [mfgSkuEnable, setMfgSkuEnable] = useState(false)
  const [nsn, setNSN] = useState(labelObj.NSN ? labelObj.NSN : '')
  const [originEnable, setOriginEnable] = useState(false)

  const productName = labelObj.productName
  const po = labelObj.PO
  const brand = labelObj.brand
  const packSize = labelObj.packSize
  const storage = labelObj.storage
  const uom = labelObj.uom
  const grossWeight = labelObj.grossWeight
  const netWeight = labelObj.netWeight
  const mfgSku = labelObj.mfgSku
  const origin = labelObj.origin

  useEffect(() => {
    setContractNumber(labelObj.productName)
    setNSN(labelObj.NSN)
  }, [labelObj])

  const getLabelObj = (): label34BObj => {
    return {
      productName: productName,
      contractNumber: contractNumber,
      PO: po,
      NSN: nsn,
      brand: brand,
      packSize: packSize,
      storage: storage,
      uom: uom,
      origin: origin,
      originEnable: originEnable,
      grossWeight: grossWeight,
      netWeight: netWeight,
      mfgSku: mfgSku,
      mfgSkuEnable: mfgSkuEnable,
    }
  }

  const save4x3Label = () => {
    const obj = getLabelObj()
    saveLabel(obj)
  }

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '579px' }}>
        <div>
          <p style={{ fontSize: '18px', lineHeight: '25.2px' }}>Active</p>
          <Radio.Group value={'Yes'}>
            <Radio value={'Yes'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              Yes
            </Radio>
            <Radio disabled value={'No'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              No
            </Radio>
          </Radio.Group>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Label Type</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>Shipping</p>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Product Name</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{productName}</p>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
              Contract Number
            </p>
            <Input
              style={{ marginTop: '4px', width: '80%' }}
              value={contractNumber}
              onChange={(e) => setContractNumber(e.target.value)}
            />
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>PO#</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{po}</p>
          </div>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>NSN#</p>
          <Input
            style={{ marginTop: '4px', marginLeft: '2px', width: '80%' }}
            value={nsn}
            onChange={(e) => setNSN(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Brand</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{brand}</p>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Pack Size</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{packSize}</p>
          </div>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Storage</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{storage}</p>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>UOM</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{uom}</p>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Origin</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>
              <Checkbox checked={originEnable} onChange={(e) => setOriginEnable(e.target.checked)} /> {origin}
            </p>
          </div>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>
              Gross Weight
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{grossWeight}</p>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Net Weight</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{netWeight}</p>
          </div>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>MFG SKU</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>
            <Checkbox checked={mfgSkuEnable} onChange={(e) => setMfgSkuEnable(e.target.checked)} />
            {` ${mfgSku}`}
          </p>
        </div>
        <ThemeButton
          style={{ marginTop: '31px', width: '198px', height: '40px', borderRadius: '40px' }}
          onClick={() => save4x3Label()}
        >
          Save Changes
        </ThemeButton>
      </div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginLeft: '33px' }}>Preview</p>
        <LabelCanvas34B labelObj={getLabelObj()} />
      </div>
    </div>
  )
}

export default Label3X4B
