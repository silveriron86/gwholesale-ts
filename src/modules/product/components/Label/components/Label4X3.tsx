import { Button, Checkbox, Input, Radio } from 'antd'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas from './LabelCanvas'
import { label43Obj } from './type/type'

interface IProps {
  labelObj: label43Obj
  saveLabel: (label: label43Obj, active: string) => void
}

const Label4X3 = ({ labelObj, saveLabel }: IProps): JSX.Element => {
  const [active, setActive] = useState('Yes')
  const [name, setProductName] = useState(labelObj.productName)
  const [subTitle, setSubTitle] = useState(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
  const [poNum, setPoNum] = useState(labelObj.isPoNum)
  const [Origin, setOrigin] = useState(labelObj.isOrigin)
  const [companyInfo, setCompanyInfo] = useState(labelObj.isCompanyInfo)
  const [companyInfoString, setCompanyInfoString] = useState<String>(labelObj.companyInfo)

  const lot = labelObj.lot
  const origin = labelObj.origin
  const poNumber = labelObj.poNum
  const barCode = labelObj.barCode
  const qrCode = labelObj.qrCode

  useEffect(() => {
    setProductName(labelObj.productName)
    setSubTitle(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
    setPoNum(labelObj.isPoNum)
    setOrigin(labelObj.isOrigin)
    setCompanyInfo(labelObj.isCompanyInfo)
    setCompanyInfoString(labelObj.companyInfo)
  }, [labelObj])

  const getLabelObj = (): label43Obj => {
    return {
      productName: name,
      lot: lot,
      poNum: poNumber ? poNumber.toString() : '',
      isPoNum: poNum,
      origin: origin ? origin.toString() : '',
      isOrigin: Origin,
      companyInfo: companyInfo ? companyInfoString.toString() : '',
      isCompanyInfo: companyInfo,
      count: labelObj.count,
      barCode: barCode,
      qrCode: qrCode,
      logo: labelObj.logo,
      isLogo: subTitle == 'Logo' ? true : false,
      sku: '105003',
    }
  }

  const save4x3Label = () => {
    const obj = getLabelObj()
    obj.companyInfo = companyInfoString.toString()
    saveLabel(obj, active)
  }

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '579px' }}>
        <div>
          <p style={{ fontSize: '18px', lineHeight: '25.2px' }}>Active</p>
          <Radio.Group
            onChange={() => {
              active == 'Yes' ? setActive('No') : setActive('Yes')
            }}
            value={active}
          >
            <Radio value={'Yes'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              Yes
            </Radio>
            <Radio disabled value={'No'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              No
            </Radio>
          </Radio.Group>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Label Type</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>Receiving</p>
        </div>
        <div style={{ marginTop: '23px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Product Name</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{name}</p>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Lot #</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{lot}</p>
          </div>
          <div style={{ width: '50%' }}>
            <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Origin</p>
            <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '9px', color: 'black' }}>{origin}</p>
          </div>
        </div>
        <div style={{ marginTop: '21px' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Barcodes</p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '3px' }}>
            CODE-128 <span style={{ fontWeight: 'normal' }}>{barCode}</span>
          </p>
          <p style={{ fontSize: '15px', lineHeight: '21px', marginTop: '3px' }}>
            QR <span style={{ fontWeight: 'normal' }}>{qrCode}</span>
          </p>
        </div>
        <div style={{ display: 'inline' }}>
          <div style={{ marginTop: '38px' }}>
            <Radio
              checked={subTitle == 'Logo' ? true : false}
              onClick={() => {
                setSubTitle('Logo')
              }}
            >
              Logo
            </Radio>
          </div>
          <div style={{ marginTop: '16px' }}>
            <Radio
              checked={subTitle !== 'Logo' ? true : false}
              onClick={() => {
                setSubTitle('AdditionalInfo')
              }}
            >
              Additional Information
            </Radio>
          </div>
        </div>
        <div style={{ display: subTitle == 'Logo' ? 'none' : 'inline' }}>
          <div style={{ marginTop: '9px', marginLeft: '21px' }}>
            <Checkbox onChange={(e) => setPoNum(e.target.checked)} checked={poNum}>
              PO Number
              {poNumber == '' ? '' : <span style={{ fontWeight: 'normal', color: '#C2C2C2' }}>{` ${poNumber}`}</span>}
            </Checkbox>
          </div>
          <div style={{ marginTop: '7px', marginLeft: '21px' }}>
            <Checkbox onChange={(e) => setOrigin(e.target.checked)} checked={Origin}>
              Origin{origin == '' ? '' : <span style={{ fontWeight: 'normal', color: '#C2C2C2' }}>{` ${origin}`}</span>}
            </Checkbox>
          </div>
          <div style={{ marginTop: '7px', marginLeft: '21px' }}>
            <Checkbox onChange={(e) => setCompanyInfo(e.target.checked)} checked={companyInfo}>
              Company Information
            </Checkbox>
            <Input
              style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
              value={typeof companyInfoString == 'string' ? companyInfoString : ''}
              onChange={(e) => setCompanyInfoString(e.target.value)}
            />
          </div>
        </div>
        <ThemeButton
          style={{ marginTop: '81px', width: '198px', height: '40px', borderRadius: '40px' }}
          onClick={() => save4x3Label()}
        >
          Save Changes
        </ThemeButton>
      </div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginLeft: '33px' }}>Preview</p>
        <LabelCanvas labelObj={getLabelObj()} UOM="CASE" />
      </div>
    </div>
  )
}

export default Label4X3
