import { Checkbox, Input, Radio } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas34B from './LabelCanvas34B'
import LabelCanvas64BPallet from './LabelCanvas64BPallet'
import LabelCanvas64Pallet from './LabelCanvas64Pallet'
import { label34BObj, pallet4X6Obj, pallet4X6ObjSetting } from './type/type'

interface IProps {
  labelObj: pallet4X6Obj
  saveLabel: (label: pallet4X6ObjSetting) => void
}

const Label4X6PalletB = ({ labelObj, saveLabel }: IProps): JSX.Element => {
  const [lot, setLot] = useState(labelObj.lot)
  const [poNum, setPoNum] = useState(labelObj.poNum)
  const [field, setField] = useState(labelObj.field)
  const [comm, setComm] = useState(labelObj.comm)
  const [origin, setOrigin] = useState(labelObj.origin)
  const [sku, setSku] = useState(labelObj.sku)
  const [style, setStyle] = useState(labelObj.style)
  const [size, setSize] = useState(labelObj.size)
  const [quantity, setQuantity] = useState(labelObj.quantity)
  const [description, setDescription] = useState(labelObj.description)
  const [qrCode, setQrCode] = useState(labelObj.qrCode)

  useEffect(() => {
    setLot(labelObj.lot)
    setPoNum(labelObj.poNum)
    setField(labelObj.field)
    setComm(labelObj.comm)
    setOrigin(labelObj.origin)
    setSku(labelObj.sku)
    setStyle(labelObj.style)
    setSize(labelObj.size)
    setQuantity(labelObj.quantity)
    setDescription(labelObj.description)
    setQrCode(labelObj.qrCode)
  }, [labelObj])

  const getLabelObj = (): pallet4X6Obj => {
    return {
      lot: lot,
      poNum: poNum,
      field: field,
      comm: comm,
      origin: origin,
      sku: sku,
      style: style,
      size: size,
      quantity: quantity,
      description: description,
      qrCode: qrCode,
    }
  }

  const save6x4PalletLabel = () => {
    const obj = getLabelObj()
    saveLabel(obj)
  }

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '479px' }}>
        <div>
          <p style={{ fontSize: '18px', lineHeight: '25.2px' }}>Active</p>
          <Radio.Group value={'Yes'}>
            <Radio value={'Yes'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              Yes
            </Radio>
            <Radio disabled value={'No'} style={{ fontSize: '15px', lineHeight: '21px' }}>
              No
            </Radio>
          </Radio.Group>
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>Lot</p>
          <Input style={{ marginTop: '4px', width: '80%' }} value={lot} onChange={(e) => setLot(e.target.value)} />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>poNum</p>
          <Input style={{ marginTop: '4px', width: '80%' }} value={poNum} onChange={(e) => setPoNum(e.target.value)} />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>field</p>
          <Input style={{ marginTop: '4px', width: '80%' }} value={field} onChange={(e) => setField(e.target.value)} />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>comm</p>
          <Input style={{ marginTop: '4px', width: '80%' }} value={comm} onChange={(e) => setComm(e.target.value)} />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>origin</p>
          <Input
            style={{ marginTop: '4px', width: '80%' }}
            value={origin}
            onChange={(e) => setOrigin(e.target.value)}
          />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>sku</p>
          <Input style={{ marginTop: '4px', width: '80%' }} value={sku} onChange={(e) => setSku(e.target.value)} />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>style</p>
          <Input style={{ marginTop: '4px', width: '80%' }} value={style} onChange={(e) => setStyle(e.target.value)} />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>size</p>
          <Input style={{ marginTop: '4px', width: '80%' }} value={size} onChange={(e) => setSize(e.target.value)} />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>quantity</p>
          <Input
            style={{ marginTop: '4px', width: '80%' }}
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
          />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>description</p>
          <Input
            style={{ marginTop: '4px', width: '80%' }}
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </div>
        <div style={{ width: '50%' }}>
          <p style={{ fontSize: '18px', lineHeight: '25.2px', marginBottom: '0px', color: '#4A5355' }}>description</p>
          <Input
            style={{ marginTop: '4px', width: '80%' }}
            value={qrCode}
            onChange={(e) => setQrCode(e.target.value)}
          />
        </div>
        <ThemeButton
          style={{ marginTop: '31px', width: '198px', height: '40px', borderRadius: '40px' }}
          onClick={() => save6x4PalletLabel()}
        >
          Save Changes
        </ThemeButton>
      </div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginLeft: '33px' }}>Preview</p>
        <LabelCanvas64BPallet labelObj={getLabelObj()} radioNum={1} />
      </div>
    </div>
  )
}

export default Label4X6PalletB
