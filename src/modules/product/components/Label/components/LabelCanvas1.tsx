import React, { FC, useRef, useEffect, useState } from 'react'
import { fabric } from 'fabric'
import QRCode from 'qrcode-svg'
import JsBarcode from 'jsbarcode'
import { useImmer } from 'use-immer'
import moment from 'moment'
import { label251Obj } from './type/type'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  label251Obj: label251Obj
}

const LabelCanvas1 = ({ label251Obj }: IProps): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  const canvas = useRef<any>(null)
  const canvasObjRef = useRef<any>({})
  const svgObjRef = useRef<any>(null)
  const [data, setData] = useImmer<IOptions[]>([])

  useEffect(() => {
    const width = 250
    const height = 100
    const RATIO = 3

    const c = new fabric.Canvas(canvasRef.current, { width, height, backgroundColor: '#fff', isDrawingMode: false })
    canvas.current = c

    // init canvas for retina
    const canvasEl: HTMLCanvasElement = c.getElement() as HTMLCanvasElement
    canvasEl.width = width * RATIO
    canvasEl.height = height * RATIO
    canvasEl.style.width = width + 'px'
    canvasEl.style.height = height + 'px'
    canvasEl.getContext('2d')?.scale(RATIO, RATIO)

    canvasObjRef.current = new Map()

    addBarcode1(label251Obj.barcode1)
    addBarcode2(label251Obj.barcode2)
    const productName = label251Obj.productName
    //addProductName(label251Obj.productName, 'productName', 10, 20, 88, 'normal')
    if (productName.toString().length < 26) {
      addProductName(label251Obj.productName, 'productName', 85, 10, 40, 'bold')
    } else {
      const firstRow = productName.toString().substring(0, 26)
      const secondRow = productName.toString().substring(26, productName.toString().length)
      addProductName(firstRow, 'productName', 85, 10, 42, 'bold')
      addProductName(secondRow, 'productName', 85, 10, 54, 'bold')
    }
    //set product lot
    addLabel(`Lot: ${label251Obj.lot}`, 'lot', 2, 8, 40, 'normal')
    //set origin
    addLabel(`ORIGIN: ${label251Obj.origin ? label251Obj.origin.substring(0, 12) : ''}`, 'origin', 2, 8, 48, 'normal')
    //set sellBy
    const time =
      label251Obj.sellByDate.toString().indexOf('-') == -1
        ? timeFn(parseInt(label251Obj.sellByDate))
        : label251Obj.sellByDate
    addLabel(label251Obj.sellByDate !== '' ? `SELL BY: ${time}` : '', 'sellBy', 2, 8, 56, 'normal')
    //set company information
    addCompanyInfo(label251Obj.companyInfo, 'companyInfo', 125, 8, 65, 'bold')
  }, [label251Obj])

  const timeFn = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const textToSvgBarcode1 = (text: string) => {
    const svg = document.createElement('svg')
    JsBarcode(svg, text, {
      format: 'CODE128',
      width: 0.6,
      height: 18,
      fontSize: 8,
    })

    return svg.outerHTML
  }

  const textToSvgBarcode2 = (text: string) => {
    const svg = document.createElement('svg')
    JsBarcode(svg, text, {
      format: 'UPC',
      width: 0.7,
      height: 15,
      fontSize: 6,
    })

    return svg.outerHTML
  }

  const addBarcode1 = (label: string) => {
    const c = canvas.current
    const key = Symbol()
    const barcodeStr = textToSvgBarcode1(label)

    fabric.loadSVGFromString(barcodeStr, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)
      const barCodeWidth = loadedObjects.width ? loadedObjects.width : 0

      loadedObjects.set({
        left: (250 - barCodeWidth) / 2,
        top: 0,
      })

      const id = +new Date()

      addId(loadedObjects, id)

      setData((draft) => {
        draft.push({ key, label, type: 'barcode', id })
      })

      canvasObjRef.current.set(key, loadedObjects)
      c.on('mouse:down', (e: any) => {
        e.target.lockMovementX = true
        e.target.lockMovementY = true
        e.target.lockScalingX = e.target.lockScalingY = true
        e.target.lockUniScaling = true
      })

      c.add(loadedObjects).renderAll()
    })
  }

  const addBarcode2 = (label: string) => {
    const c = canvas.current
    const key = Symbol()

    if (label != '') {
      try {
        const barcodeStr = textToSvgBarcode2(label)

        fabric.loadSVGFromString(barcodeStr, (objects, options) => {
          const loadedObjects = fabric.util.groupSVGElements(objects, options)

          loadedObjects.set({
            left: 0,
            top: 75,
            height: 15,
          })

          const id = +new Date()

          addId(loadedObjects, id)

          setData((draft) => {
            draft.push({ key, label, type: 'barcode', id })
          })

          canvasObjRef.current.set(key, loadedObjects)
          c.on('mouse:down', (e: any) => {
            e.target.lockMovementX = true
            e.target.lockMovementY = true
            e.target.lockScalingX = e.target.lockScalingY = true
            e.target.lockUniScaling = true
          })

          c.add(loadedObjects).renderAll()
        })
      } catch {}
    }
  }

  const addLabel = (text: string, type: string, left: number, fontSize: number, top: number, fontWeight: string) => {
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize,
      fontWeight: fontWeight,
      left: left,
      top: top,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: text, type, id })
    })

    const textFabric = new fabric.Text(text, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)
    c.on('mouse:down', (e: any) => {
      e.target.lockMovementX = true
      e.target.lockMovementY = true
      e.target.lockScalingX = e.target.lockScalingY = true
      e.target.lockUniScaling = true
    })

    c.add(textFabric).renderAll()
  }

  const addProductName = (
    text: string,
    type: string,
    left: number,
    fontSize: number,
    top: number,
    fontWeight: string,
  ) => {
    const totalWidth = text.replace(/\s+/g, '').length * 11.4
    const emptyWidth = (text.length - text.replace(/\s+/g, '').length) * 6.1
    const leftMargin = left
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize,
      fontWeight: fontWeight,
      left: leftMargin,
      top: top,
      width: totalWidth,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: text, type, id })
    })

    const textFabric = new fabric.Text(text, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)

    c.add(textFabric).renderAll()
  }

  const addCompanyInfo = (
    text: string,
    type: string,
    left: number,
    fontSize: number,
    top: number,
    fontWeight: string,
  ) => {
    const totalWidth = text.replace(/\s+/g, '').length * 11.4
    const emptyWidth = (text.length - text.replace(/\s+/g, '').length) * 6.1
    if (totalWidth + emptyWidth <= 250) {
      const c = canvas.current
      const key = Symbol()
      const style = {
        fontSize: fontSize,
        fontWeight: fontWeight,
        left: left,
        top: top,
        fontFamily: 'sans-serif',
      }
      const id = +new Date()
      setData((draft) => {
        draft.push({ key, label: text, type, id })
      })

      const textFabric = new fabric.Text(text, { ...style })

      addId(textFabric, id)

      canvasObjRef.current.set(key, textFabric)

      c.add(textFabric).renderAll()
    } else {
      const firstLine = text.slice(0, 22)
      const secondLine = text.slice(22, text.length)
      const c = canvas.current
      const key = Symbol()
      const id = +new Date()
      setData((draft) => {
        draft.push({ key, label: text, type, id })
      })
      //first line
      const style = {
        fontSize: fontSize,
        fontWeight: fontWeight,
        left: left,
        top: top,
        fontFamily: 'sans-serif',
      }
      const textFabric = new fabric.Text(firstLine, { ...style })

      addId(textFabric, id)

      canvasObjRef.current.set(key, textFabric)

      c.add(textFabric).renderAll()

      const style1 = {
        fontSize: fontSize,
        fontWeight: fontWeight,
        left: left,
        top: top + 10,
        fontFamily: 'sans-serif',
      }
      const textFabric1 = new fabric.Text(secondLine, { ...style1 })
      //second line
      addId(textFabric, id)

      canvasObjRef.current.set(key.toString() + '1', textFabric1)

      c.add(textFabric1).renderAll()
    }
  }

  return (
    <div style={{ marginLeft: '33px' }}>
      <canvas id="canvas" ref={canvasRef} style={{ border: '1px solid #ccc', marginTop: '17px' }} />
    </div>
  )
}

export default LabelCanvas1

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
