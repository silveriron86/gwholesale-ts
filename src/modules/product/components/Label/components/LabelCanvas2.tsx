import React, { FC, useRef, useEffect, useState } from 'react'
import { fabric } from 'fabric'
import JsBarcode from 'jsbarcode'
import { useImmer } from 'use-immer'
import { label252Obj } from './type/type'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  label252Obj: label252Obj
}

const LabelCanvas2 = ({ label252Obj }: IProps): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  const canvas = useRef<any>(null)
  const canvasObjRef = useRef<any>({})
  const svgObjRef = useRef<any>(null)
  const [data, setData] = useImmer<IOptions[]>([])

  useEffect(() => {
    const width = 250
    const height = 100
    const RATIO = 3

    const c = new fabric.Canvas(canvasRef.current, { width, height, backgroundColor: '#fff', isDrawingMode: false })
    canvas.current = c

    // init canvas for retina
    const canvasEl: HTMLCanvasElement = c.getElement() as HTMLCanvasElement
    canvasEl.width = width * RATIO
    canvasEl.height = height * RATIO
    canvasEl.style.width = width + 'px'
    canvasEl.style.height = height + 'px'
    canvasEl.getContext('2d')?.scale(RATIO, RATIO)

    canvasObjRef.current = new Map()

    addBarcode1(label252Obj.barcode1)
    //addProductName(label252Obj.productName, 'productName', 10, 20, 88, 'normal')
    const productName = label252Obj.productName
    if (productName.toString().length < 26) {
      addProductName(label252Obj.productName, 'productName', 85, 10, 40, 'bold')
    } else {
      const firstRow = productName.toString().substring(0, 26)
      const secondRow = productName.toString().substring(26, productName.toString().length)
      addProductName(firstRow, 'productName', 85, 10, 42, 'bold')
      addProductName(secondRow, 'productName', 85, 10, 54, 'bold')
    }
    //set product lot
    addLabel(`Lot: ${label252Obj.lot}`, 'lot', 2, 8, 40, 'normal')
    //set origin
    addLabel(`ORIGIN: ${label252Obj.origin ? label252Obj.origin.substring(0, 12) : ''}`, 'origin', 2, 8, 48, 'normal')
    //set sellBy
    const time =
      label252Obj.sellByDate.toString().indexOf('-') == -1
        ? timeFn(parseInt(label252Obj.sellByDate))
        : label252Obj.sellByDate
    addLabel(label252Obj.sellByDateShow ? `SELL BY: ${time}` : '', 'sellBy', 2, 8, 56, 'normal')
    //set company information
    addMiddleLabel(label252Obj.companyInfo, 'companyInfo', 0, 10, 85, 'bold')
    //set note
    addMiddleLabel(label252Obj.note, 'note', 0, 10, 65, 'bold')
  }, [label252Obj])

  const timeFn = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const textToSvgBarcode1 = (text: string) => {
    const svg = document.createElement('svg')
    JsBarcode(svg, text, {
      format: 'CODE128',
      width: 0.6,
      height: 18,
      fontSize: 8,
    })

    return svg.outerHTML
  }

  const addBarcode1 = (label: string) => {
    const c = canvas.current
    const key = Symbol()
    const barcodeStr = textToSvgBarcode1(label)

    fabric.loadSVGFromString(barcodeStr, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)
      const barCodeWidth = loadedObjects.width ? loadedObjects.width : 0

      loadedObjects.set({
        left: (250 - barCodeWidth) / 2,
        top: 0,
      })

      const id = +new Date()

      addId(loadedObjects, id)

      setData((draft) => {
        draft.push({ key, label, type: 'barcode', id })
      })

      canvasObjRef.current.set(key, loadedObjects)
      c.on('mouse:down', (e: any) => {
        e.target.lockMovementX = true
        e.target.lockMovementY = true
        e.target.lockScalingX = e.target.lockScalingY = true
        e.target.lockUniScaling = true
      })

      c.add(loadedObjects).renderAll()
    })
  }

  const addLabel = (text: string, type: string, left: number, fontSize: number, top: number, fontWeight: string) => {
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize,
      fontWeight: fontWeight,
      left: left,
      top: top,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: text, type, id })
    })

    const textFabric = new fabric.Text(text, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)
    c.on('mouse:down', (e: any) => {
      e.target.lockMovementX = true
      e.target.lockMovementY = true
      e.target.lockScalingX = e.target.lockScalingY = true
      e.target.lockUniScaling = true
    })

    c.add(textFabric).renderAll()
  }

  const addProductName = (
    text: string,
    type: string,
    left: number,
    fontSize: number,
    top: number,
    fontWeight: string,
  ) => {
    const totalWidth = text.replace(/\s+/g, '').length * 11
    const emptyWidth = (text.length - text.replace(/\s+/g, '').length) * 6.1
    const leftMargin = left
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize,
      fontWeight: fontWeight,
      left: leftMargin,
      top: top,
      width: totalWidth,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: text, type, id })
    })

    const textFabric = new fabric.Text(text, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)
    c.on('mouse:down', (e: any) => {
      e.target.lockMovementX = true
      e.target.lockMovementY = true
      e.target.lockScalingX = e.target.lockScalingY = true
      e.target.lockUniScaling = true
    })

    c.add(textFabric).renderAll()
  }

  const addMiddleLabel = (
    text: string,
    type: string,
    left: number,
    fontSize: number,
    top: number,
    fontWeight: string,
  ) => {
    const totalWidth = text.replace(/\s+/g, '').length * 6
    const emptyWidth = (text.length - text.replace(/\s+/g, '').length) * 3
    const leftMargin = (250 - (totalWidth + emptyWidth)) / 2
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize,
      fontWeight: fontWeight,
      left: leftMargin,
      top: top,
      width: totalWidth,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: text, type, id })
    })

    const textFabric = new fabric.Text(text, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)
    c.on('mouse:down', (e: any) => {
      e.target.lockMovementX = true
      e.target.lockMovementY = true
      e.target.lockScalingX = e.target.lockScalingY = true
      e.target.lockUniScaling = true
    })

    c.add(textFabric).renderAll()
  }

  return (
    <div>
      <canvas id="canvas" ref={canvasRef} style={{ border: '1px solid #ccc', marginTop: '30px' }} />
    </div>
  )
}

export default LabelCanvas2

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
