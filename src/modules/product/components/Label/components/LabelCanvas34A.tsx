import React, { FC, useRef, useEffect, useState } from 'react'
import { fabric } from 'fabric'
import QRCode from 'qrcode-svg'
import JsBarcode from 'jsbarcode'
import { useImmer } from 'use-immer'
import { label34AObj, label43Obj } from './type/type'
import { Button } from 'antd'
import { ThemeButton } from '~/modules/customers/customers.style'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  labelObj: label34AObj
}

const LabelCanvas34A = ({ labelObj }: IProps): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  const canvas = useRef<any>(null)
  const canvasObjRef = useRef<any>({})
  const svgObjRef = useRef<any>(null)
  const [data, setData] = useImmer<IOptions[]>([])
  const [updateNum, setUpdateNum] = useImmer<number>(0)

  useEffect(() => {
    const width = 283
    const height = 401
    const RATIO = 1

    const c = new fabric.Canvas(canvasRef.current, { width, height, backgroundColor: '#fff', isDrawingMode: false })
    canvas.current = c

    // init canvas for retina
    const canvasEl: HTMLCanvasElement = c.getElement() as HTMLCanvasElement
    canvasEl.width = width * RATIO
    canvasEl.height = height * RATIO
    canvasEl.style.width = width + 'px'
    canvasEl.style.height = height + 'px'
    canvasEl.getContext('2d')?.scale(RATIO, RATIO)

    canvasObjRef.current = new Map()

    setUpdateNum(updateNum + 1)
  }, [labelObj])

  useEffect(() => {
    canvas.current.clear()
    addLabel(`Contract Number: ${labelObj.contractNumber}`, 'productName', 20, 14, 10, 'normal')
    addLabel(`PO#: ${labelObj.PO}`, 'productName', 20, 14, 30, 'normal')
    addLabel(`NSN#: ${labelObj.NSN}`, 'productName', 20, 14, 50, 'normal')
    addLabel(`${labelObj.productName}`, 'productName', 20, 14, 70, 'normal')
    addLabel(`Brand: ${labelObj.brand == '-1' ? '' : labelObj.brand}`, 'productName', 20, 14, 90, 'normal')
    addLabel(`Pack Size: ${labelObj.packSize}`, 'productName', 20, 14, 110, 'normal')
    addLabel(`${labelObj.storage}`, 'productName', 20, 14, 130, 'normal')
    const packTime =
      labelObj.packDate.toString().trim() !== ''
        ? labelObj.packDate.toString().indexOf('-') == -1
          ? timeFn(parseInt(labelObj.packDate))
          : labelObj.packDate
        : ''
    if (labelObj.packDateEnable) {
      addLabel(`Pack Date: ${packTime}`, 'productName', 20, 14, 150, 'normal')
    }
    const bestByTime =
      labelObj.sellByDate.toString().trim() !== ''
        ? labelObj.sellByDate.toString().indexOf('-') == -1
          ? timeFn(parseInt(labelObj.sellByDate))
          : labelObj.sellByDate
        : ''
    if (labelObj.sellByDateEnable) {
      addLabel(`Best By Date: ${bestByTime}`, 'productName', 20, 14, 170, 'normal')
    }
    addLabel(`Lot: ${labelObj.lot == '-1' ? '' : labelObj.lot}`, 'productName', 20, 14, 190, 'normal')
    addLabel(
      `Manufacturer: ${labelObj.manufacturer == '-1' ? '' : labelObj.manufacturer}`,
      'productName',
      20,
      14,
      210,
      'normal',
    )
    addLabel(`${labelObj.note}`, 'productName', 20, 14, 230, 'normal')
    if (labelObj.originEnable && labelObj.originEnable == true) {
      addLabel(
        `Origin: ${labelObj.origin == undefined || labelObj.origin == '-1' ? '' : labelObj.origin}`,
        'productName',
        20,
        14,
        270,
        'normal',
      )
    }
    addLabel(`MFG SKU: ${labelObj.mfgSku}`, 'productName', 20, 14, 250, 'normal')
    if (labelObj.mfgSku && labelObj.mfgSku != '') {
      addBarcode1(labelObj.mfgSku)
    }
  }, [updateNum, labelObj])

  const timeFn = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const addBarcode1 = (label: string) => {
    const c = canvas.current
    const key = Symbol()
    const barcodeStr = textToSvgBarcode1(label)

    fabric.loadSVGFromString(barcodeStr, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)

      loadedObjects.set({
        left: 10,
        top: 290,
      })

      const id = +new Date()

      addId(loadedObjects, id)

      setData((draft) => {
        draft.push({ key, label, type: 'barcode', id })
      })

      canvasObjRef.current.set(key, loadedObjects)
      c.on('mouse:down', (e: any) => {
        e.target.lockMovementX = true
        e.target.lockMovementY = true
        e.target.lockScalingX = e.target.lockScalingY = true
        e.target.lockUniScaling = true
      })

      c.add(loadedObjects).renderAll()
    })
  }

  const textToSvgBarcode1 = (text: string) => {
    const svg = document.createElement('svg')
    JsBarcode(svg, text, {
      format: 'CODE128',
      width: 2.0,
      height: 50,
      fontSize: 17,
    })

    return svg.outerHTML
  }

  const addLabel = (text: string, type: string, left: number, fontSize: number, top: number, fontWeight: string) => {
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize,
      fontWeight: fontWeight,
      left: left,
      top: top,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: text, type, id })
    })

    const textFabric = new fabric.Text(text, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)
    c.on('mouse:down', (e: any) => {
      e.target.lockMovementX = true
      e.target.lockMovementY = true
      e.target.lockScalingX = e.target.lockScalingY = true
      e.target.lockUniScaling = true
    })

    c.add(textFabric).renderAll()
  }

  return (
    <div
      style={{
        marginLeft: '33px',
      }}
    >
      <canvas id="canvas" ref={canvasRef} style={{ border: '1px solid #ccc', marginTop: '17px' }} />
    </div>
  )
}

export default LabelCanvas34A

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
