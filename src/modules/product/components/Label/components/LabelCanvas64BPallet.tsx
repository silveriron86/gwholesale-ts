import React, { useRef, useEffect } from 'react'
import { fabric } from 'fabric'
import QRCode from 'qrcode-svg'
import { useImmer } from 'use-immer'
import { pallet4X6Obj } from './type/type'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  labelObj: pallet4X6Obj
  radioNum: number
}

const LabelCanvas64BPallet = ({ labelObj, radioNum }: IProps): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  const canvas = useRef<any>(null)
  const canvasObjRef = useRef<any>({})
  const svgObjRef = useRef<any>(null)
  const [data, setData] = useImmer<IOptions[]>([])
  const [updateNum, setUpdateNum] = useImmer<number>(0)

  useEffect(() => {
    const width = 600 * radioNum
    const height = 400 * radioNum
    const RATIO = 1

    const c = new fabric.Canvas(canvasRef.current, { width, height, backgroundColor: '#fff', isDrawingMode: false })
    canvas.current = c

    // init canvas for retina
    const canvasEl: HTMLCanvasElement = c.getElement() as HTMLCanvasElement
    canvasEl.width = width * RATIO
    canvasEl.height = height * RATIO
    canvasEl.style.width = width + 'px'
    canvasEl.style.height = height + 'px'
    canvasEl.getContext('2d')?.scale(RATIO, RATIO)

    canvasObjRef.current = new Map()

    setUpdateNum(updateNum + 1)
  }, [labelObj])

  useEffect(() => {
    canvas.current.clear()
    //lines
    addLine(2, 2, 596, 2)
    addLine(2, 68, 596, 68)
    addLine(2, 134, 596, 134)
    addLine(2, 200, 596, 200)
    addLine(2, 300, 596, 300)
    addLine(2, 2, 2, 300)
    addLine(596, 2, 596, 300)
    addLine(596, 2, 596, 2)
    addDottedLine(200, 300, 200, 400)
    addDottedLine(400, 300, 400, 400)
    //titles
    addLabel(`Lot #`, '', 10, 11, 10, 'normal')
    addLabel(`PO #`, '', 220, 11, 10, 'normal')
    addLabel(`Field`, '', 380, 11, 10, 'normal')
    addLabel(`Comm`, '', 10, 11, 75, 'normal')
    addLabel(`Origin`, '', 220, 11, 75, 'normal')
    addLabel(`SKU`, '', 380, 11, 75, 'normal')
    addLabel(`Style`, '', 10, 11, 143.5, 'normal')
    addLabel(`Size`, '', 220, 11, 143.5, 'normal')
    addLabel(`Quantity`, '', 380, 11, 143.5, 'normal')
    addLabel(`Description`, '', 10, 11, 212.25, 'normal')
    //text
    addLabel(labelObj.lot, '', 10, 22, 30, 'normal')
    addLabel(labelObj.poNum, '', 220, 22, 30, 'normal')
    addLabel(labelObj.field, '', 380, 22, 30, 'bold')
    addLabel(labelObj.comm, '', 10, 22, 97, 'normal')
    addLabel(labelObj.origin, '', 220, 22, 97, 'normal')
    addLabel(labelObj.sku, '', 380, 22, 97, 'normal')
    addLabel(labelObj.style, '', 10, 22, 160, 'normal')
    addLabel(labelObj.size, '', 220, 22, 160, 'normal')
    addLabel(labelObj.quantity, '', 380, 22, 160, 'normal')
    const description = labelObj.description ? labelObj.description.toString() : ''
    if (description.toString().length < 47) {
      addLabel(description.toString(), '', 10, 22, 227, 'normal')
    } else {
      const firstRow = description.toString().substring(0, 47)
      const secondRow = description.toString().substring(47, description.toString().length)
      addLabel(firstRow, '', 10, 22, 227, 'normal')
      addLabel(secondRow, '', 10, 22, 260, 'normal')
    }
    // bottom labels
    // label 1
    if (description.toString().length < 27) {
      addLabel(description.toString(), '', 20, 12, 315, 'bold')
    } else {
      const firstRow = description.toString().substring(0, 27)
      const secondRow = description.toString().substring(27, description.toString().length)
      addLabel(firstRow, '', 20, 12, 315, 'bold')
      addLabel(secondRow, '', 20, 12, 326, 'bold')
    }
    addLabel(labelObj.field, '', 20, 12, 345, 'bold')
    addLabel(labelObj.lot, '', 20, 12, 360, 'bold')
    addLabel(labelObj.quantity, '', 20, 12, 375, 'bold')
    // bottom labels
    // label 2
    if (description.length < 27) {
      addLabel(description, '', 220, 12, 315, 'bold')
    } else {
      const firstRow = description.substring(0, 27)
      const secondRow = description.substring(27, description.toString().length)
      addLabel(firstRow, '', 220, 12, 315, 'bold')
      addLabel(secondRow, '', 220, 12, 326, 'bold')
    }
    addLabel(labelObj.field, '', 220, 12, 345, 'bold')
    addLabel(labelObj.lot, '', 220, 12, 360, 'bold')
    addLabel(labelObj.quantity, '', 220, 12, 375, 'bold')
    // bottom labels
    // label 3
    if (description.toString().length < 27) {
      addLabel(description.toString(), '', 420, 12, 315, 'bold')
    } else {
      const firstRow = labelObj.description.toString().substring(0, 27)
      const secondRow = labelObj.description.toString().substring(27, description.toString().length)
      addLabel(firstRow, '', 420, 12, 315, 'bold')
      addLabel(secondRow, '', 420, 12, 326, 'bold')
    }
    addLabel(labelObj.field, '', 420, 12, 345, 'bold')
    addLabel(labelObj.lot, '', 420, 12, 360, 'bold')
    addLabel(labelObj.quantity, '', 420, 12, 375, 'bold')
  }, [updateNum, labelObj])

  const addLine = (left: number, top: number, width: number, height: number) => {
    const c = canvas.current
    const textFabric = new fabric.Line([left * radioNum, top * radioNum, width * radioNum, height * radioNum], {
      stroke: 'black',
    })
    c.add(textFabric).renderAll()
  }
  const addDottedLine = (left: number, top: number, width: number, height: number) => {
    const c = canvas.current
    const textFabric = new fabric.Line([left * radioNum, top * radioNum, width * radioNum, height * radioNum], {
      strokeDashArray: [5, 5],
      stroke: 'black',
    })
    c.add(textFabric).renderAll()
  }

  const addLabel = (text: any, type: string, left: number, fontSize: number, top: number, fontWeight: string) => {
    const value = text ? text.toString() : ''
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize * radioNum,
      fontWeight: fontWeight,
      left: left * radioNum,
      top: top * radioNum,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: value, type, id })
    })

    const textFabric = new fabric.Text(value, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)
    c.on('mouse:down', (e: any) => {
      e.target.lockMovementX = true
      e.target.lockMovementY = true
      e.target.lockScalingX = e.target.lockScalingY = true
      e.target.lockUniScaling = true
    })

    c.add(textFabric).renderAll()
  }

  return (
    <div
      style={{
        marginLeft: '33px',
      }}
    >
      <canvas id="canvas" ref={canvasRef} style={{ border: '1px solid #ccc', marginTop: '17px' }} />
    </div>
  )
}

export default LabelCanvas64BPallet

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
