import { Button, Checkbox, Input, Radio } from 'antd'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas from './LabelCanvas'
import LotLabelCanvas from './LotLabelCanvas'
import { label43Obj, LotLabelObj } from './type/type'

interface IProps {
  labelObj: LotLabelObj
  //saveLabel: (label: label43Obj, active: string) => void
}

const LotLabel = ({ labelObj }: IProps): JSX.Element => {
  const [active, setActive] = useState('Yes')
  const [name, setProductName] = useState(labelObj.productName)
  const [subTitle, setSubTitle] = useState(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
  const [poNum, setPoNum] = useState(labelObj.isPoNum)
  const [Origin, setOrigin] = useState(labelObj.isOrigin)
  const [companyInfo, setCompanyInfo] = useState(labelObj.isCompanyInfo)
  const [companyInfoString, setCompanyInfoString] = useState<String>(labelObj.companyInfo)

  const lot = labelObj.lot
  const origin = labelObj.origin
  const poNumber = labelObj.poNum
  const barCode = labelObj.barCode
  const qrCode = labelObj.qrCode

  useEffect(() => {
    setProductName(labelObj.productName)
    setSubTitle(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
    setPoNum(labelObj.isPoNum)
    setOrigin(labelObj.isOrigin)
    setCompanyInfo(labelObj.isCompanyInfo)
    setCompanyInfoString(labelObj.companyInfo)
  }, [labelObj])

  const getLabelObj = (): LotLabelObj => {
    return {
      productName: name,
      lot: lot,
      poNum: poNumber ? poNumber.toString() : '',
      isPoNum: poNum,
      origin: origin ? origin.toString() : '',
      isOrigin: Origin,
      companyInfo: companyInfo ? companyInfoString.toString() : '',
      isCompanyInfo: companyInfo,
      barCode: barCode,
      qrCode: qrCode,
      logo: labelObj.logo,
      isLogo: subTitle == 'Logo' ? true : false,
      sku: '105003',
      constantRatio: true,
    }
  }

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '579px' }}>lot label</div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginLeft: '33px' }}>Preview</p>
        <LotLabelCanvas labelObj={getLabelObj()} UOM="CASE" />
      </div>
    </div>
  )
}

export default LotLabel
