import React, { FC, useRef, useEffect, useState } from 'react'
import { fabric } from 'fabric'
import QRCode from 'qrcode-svg'
import JsBarcode from 'jsbarcode'
import { useImmer } from 'use-immer'
import { label43Obj, LotLabelObj } from './type/type'
import { Button } from 'antd'
import { ThemeButton } from '~/modules/customers/customers.style'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  labelObj: LotLabelObj
  UOM: string
}

const LotLabelCanvas = ({ labelObj, UOM }: IProps): JSX.Element => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  const canvas = useRef<any>(null)
  const canvasObjRef = useRef<any>({})
  const svgObjRef = useRef<any>(null)
  const [data, setData] = useImmer<IOptions[]>([])
  const [updateNum, setUpdateNum] = useImmer<number>(0)

  useEffect(() => {
    const width = 401
    const height = 283
    const RATIO = 1

    const c = new fabric.Canvas(canvasRef.current, { width, height, backgroundColor: '#fff', isDrawingMode: false })
    canvas.current = c

    // init canvas for retina
    const canvasEl: HTMLCanvasElement = c.getElement() as HTMLCanvasElement
    canvasEl.width = width * RATIO
    canvasEl.height = height * RATIO
    canvasEl.style.width = width + 'px'
    canvasEl.style.height = height + 'px'
    canvasEl.getContext('2d')?.scale(RATIO, RATIO)

    canvasObjRef.current = new Map()

    setUpdateNum(updateNum + 1)
  }, [labelObj])

  useEffect(() => {
    canvas.current.clear()
    addBarcode(labelObj.barCode)
    addQrCode(labelObj.qrCode)
    const productName = labelObj.productName
    if (productName.toString().length < 40) {
      addLabel(labelObj.productName, 'productName', 10, 20, 60, 'normal')
    } else {
      const firstRow = productName.toString().substring(0, 38)
      const secondRow = productName.toString().substring(38, productName.toString().length)
      addLabel(firstRow, 'productName', 10, 20, 60, 'normal')
      addLabel(secondRow, 'productName', 10, 20, 80, 'normal')
    }
    //set product bold name
    addLabel(labelObj.sku?.substring(0, 8), 'productBoldName', 10, 56, 100, 'bold')
    //set product lot
    addLabel(`${labelObj.lot}`, 'lot', 90, 36, 240, 'bold')
    //if logo
    if (labelObj.isLogo == true) {
      getLogo(labelObj.logo, canvas.current)
    } else {
      //set po number
      addLabel(labelObj.isPoNum ? `PO: ${labelObj.poNum}` : '', 'poNum', 13, 16, 190, 'normal')
      //set origin
      addLabel(labelObj.isOrigin ? `ORIGIN: ${labelObj.origin}` : '', 'origin', 13, 16, 210, 'normal')
      //set company information
      addLabel(labelObj.companyInfo !== '' ? labelObj.companyInfo : '', 'companyInfo', 13, 16, 230, 'normal')
    }
    //set count
    //set Case
    const captialArr: string[] = []
    const lowercaseArr: string[] = []
    const LArr: string[] = []
    const IArr: string[] = []
    UOM.split('').forEach((letter) => {
      const char = letter.charCodeAt(0)
      if (char >= 65 && char <= 90) {
        captialArr.push(letter)
      }
      if (char >= 97 && char <= 122) {
        lowercaseArr.push(letter)
      }
      if (letter == 'L' || letter == 'l') {
        LArr.push(letter)
      }
      if (letter == 'I' || letter == 'i') {
        IArr.push(letter)
      }
    })
    addLabel(
      UOM,
      'case',
      384 + IArr.length * 5 + LArr.length * 3 - captialArr.length * 13 - lowercaseArr.length * 10,
      20,
      190,
      'normal',
    )
    addLabel(
      labelObj.constantRatio === true ? 'FIXED' : 'VARIABLE',
      'case',
      labelObj.constantRatio === true ? 328 : 288,
      20,
      210,
      'normal',
    )
  }, [updateNum, labelObj])

  const getLogo = (logonLink: string, c: fabric.Canvas) => {
    fabric.util.loadImage(
      logonLink,
      (object) => {
        const loadedObjects = new fabric.Image(object)
        loadedObjects.scaleToHeight(50)
        //loadedObjects.scaleToWidth(200)
        loadedObjects.set({
          left: 10,
          top: 180,
        })
        svgObjRef.current = loadedObjects
        c.add(loadedObjects).renderAll()
      },
      { crossOrigin: 'Anonymous' },
    )
  }

  const textToSvgBarcode = (text: string) => {
    const svg = document.createElement('svg')
    JsBarcode(svg, text, {
      format: 'CODE128',
      width: 1.04,
      height: 30,
      fontSize: 15,
    })

    return svg.outerHTML
  }

  const textToSVGQrCode = (text: string) => {
    return new QRCode({
      content: text,
      padding: 1,
      width: 80,
      height: 80,
      color: '#000000',
      ecl: 'M',
    }).svg()
  }

  const addBarcode = (label: string) => {
    const c = canvas.current
    const key = Symbol()
    const barcodeStr = textToSvgBarcode(label)

    fabric.loadSVGFromString(barcodeStr, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)
      const barCodeWidth = loadedObjects.width ? loadedObjects.width : 0
      loadedObjects.set({
        left: (401 - barCodeWidth) / 2,
        top: 0,
      })

      const id = +new Date()

      addId(loadedObjects, id)

      setData((draft) => {
        draft.push({ key, label, type: 'barcode', id })
      })

      canvasObjRef.current.set(key, loadedObjects)
      c.on('mouse:down', (e: any) => {
        e.target.lockMovementX = true
        e.target.lockMovementY = true
        e.target.lockScalingX = e.target.lockScalingY = true
        e.target.lockUniScaling = true
      })

      c.add(loadedObjects).renderAll()
    })
  }

  const addQrCode = (link: string) => {
    const c = canvas.current
    const key = Symbol()
    const label = link
    //'https://lnk.bio/grubmarket'
    const style = {
      left: 310,
      top: 105,
    }

    const qrCodeSVGStr = textToSVGQrCode(label)

    fabric.loadSVGFromString(qrCodeSVGStr, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)

      loadedObjects.set(style)

      const id = +new Date()

      addId(loadedObjects, id)
      setData((draft) => {
        draft.push({ key, label, type: 'qr-code', id })
      })
      canvasObjRef.current.set(key, loadedObjects)
      c.on('mouse:down', (e: any) => {
        e.target.lockMovementX = true
        e.target.lockMovementY = true
        e.target.lockScalingX = e.target.lockScalingY = true
        e.target.lockUniScaling = true
      })

      c.add(loadedObjects).renderAll()
    })
  }

  const addLabel = (text: string, type: string, left: number, fontSize: number, top: number, fontWeight: string) => {
    const c = canvas.current
    const key = Symbol()
    const style = {
      fontSize: fontSize,
      fontWeight: fontWeight,
      left: left,
      top: top,
      fontFamily: 'sans-serif',
    }
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label: text, type, id })
    })

    const textFabric = new fabric.Text(text, { ...style })

    addId(textFabric, id)

    canvasObjRef.current.set(key, textFabric)
    c.on('mouse:down', (e: any) => {
      e.target.lockMovementX = true
      e.target.lockMovementY = true
      e.target.lockScalingX = e.target.lockScalingY = true
      e.target.lockUniScaling = true
    })

    c.add(textFabric).renderAll()
  }

  return (
    <div
      style={{
        marginLeft: '33px',
      }}
    >
      <canvas id="canvas" ref={canvasRef} style={{ border: '1px solid #ccc', marginTop: '17px' }} />
    </div>
  )
}

export default LotLabelCanvas

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
