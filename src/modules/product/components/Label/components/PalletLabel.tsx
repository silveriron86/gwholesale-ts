import * as React from 'react'
import { useEffect, useState } from 'react'
import LotLabelCanvas from './LotLabelCanvas'
import PalletLabelCanvas from './PalletLabelCanvas'
import { PalletLabelObj } from './type/type'

interface IProps {
  labelObj: PalletLabelObj
  //saveLabel: (label: label43Obj, active: string) => void
}

const PalletLabel = ({ labelObj }: IProps): JSX.Element => {
  const [active, setActive] = useState('Yes')
  const [name, setProductName] = useState(labelObj.productName)
  const [subTitle, setSubTitle] = useState(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
  const [poNum, setPoNum] = useState(labelObj.isPoNum)
  const [Origin, setOrigin] = useState(labelObj.isOrigin)
  const [companyInfo, setCompanyInfo] = useState(labelObj.isCompanyInfo)
  const [companyInfoString, setCompanyInfoString] = useState<String>(labelObj.companyInfo)

  const lot = labelObj.lot
  const origin = labelObj.origin
  const poNumber = labelObj.poNum
  const barCode = labelObj.barCode
  const qrCode = labelObj.qrCode

  useEffect(() => {
    setProductName(labelObj.productName)
    setSubTitle(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
    setPoNum(labelObj.isPoNum)
    setOrigin(labelObj.isOrigin)
    setCompanyInfo(labelObj.isCompanyInfo)
    setCompanyInfoString(labelObj.companyInfo)
  }, [labelObj])

  const getLabelObj = (): PalletLabelObj => {
    return {
      productName: name,
      lot: lot,
      poNum: poNumber ? poNumber.toString() : '',
      isPoNum: poNum,
      origin: origin ? origin.toString() : '',
      isOrigin: Origin,
      companyInfo: companyInfo ? companyInfoString.toString() : '',
      isCompanyInfo: companyInfo,
      count: labelObj.count,
      barCode: barCode,
      qrCode: qrCode,
      logo: labelObj.logo,
      isLogo: subTitle == 'Logo' ? true : false,
      sku: '105003',
      palletId: labelObj.palletId,
      constantRatio: labelObj.constantRatio,
    }
  }

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '579px' }}>pallet label</div>
      <div>
        <p style={{ fontSize: '18px', lineHeight: '25.2px', marginLeft: '33px' }}>Preview</p>
        <PalletLabelCanvas labelObj={getLabelObj()} UOM="CASE" />
      </div>
    </div>
  )
}

export default PalletLabel
