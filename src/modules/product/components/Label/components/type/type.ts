export type label252Obj = {
  x1B: boolean
  productName: string
  lot: string
  origin: string
  barcode1: string
  note: string
  companyInfo: string
  sellByDateShow: boolean
  sellByDate: string
  itemName?: string | null
}

export type label252ObjSetting = {
  productName: string
  note: string
  companyInfoString: string
  sellByDateShow: boolean
  sellByDate: string
}

export type label251Obj = {
  x1A: boolean
  productName: string
  lot: string
  origin: string
  sellByDateShow: boolean
  sellByDate: string
  barcode1: string
  barcode2: string
  companyInfo: string
  itemName?: string | null
}

export type label251ObjSetting = {
  productName: string
  sellByDateShow: boolean
  sellByDate: string
  companyInfoString: string
  upcCode: string
}

export type label251AQRObj = {
  x1A: boolean
  productName: string
  lot: string
  origin: string
  sellByDateShow: boolean
  sellByDate: string
  barcode1: string
  barcode2: string
  companyInfo: string
  itemName?: string | null
}

export type label251BQRObj = {
  x1B: boolean
  productName: string
  lot: string
  origin: string
  barcode1: string
  note: string
  companyInfo: string
  sellByDateShow: boolean
  sellByDate: string
  itemName?: string | null
}

export type label43Obj = {
  productName: string
  lot: string
  poNum: string
  isPoNum: boolean
  origin: string
  isOrigin: boolean
  companyInfo: string
  isCompanyInfo: boolean
  count: string
  barCode: string
  qrCode: string
  logo: string
  isLogo: boolean
  sku: string
}

export type label43ObjSetting = {
  isPoNum: boolean
  isOrigin: boolean
  companyInfoString: string
  isLogo: boolean
  isCompanyInfo: boolean
  sku: string
}

export type labelSetting = {
  x1A: boolean
  x1B: boolean
  productName: string
  lot: string
  poNum: string
  isPoNum: boolean
  origin: string
  isOrigin: boolean
  companyInfo: string
  isCompanyInfo: boolean
  count: string
  qrCode: string
  logo: string
  isLogo: boolean
  barcode1: string
  barcode2: string
  sellByDate: string
  sellByDateShow: boolean
  note: string
  sku: string
  itemName?: string
  constantRatio?: boolean
}

//2931

export type label34AObj = {
  productName: string
  contractNumber: string
  PO: string
  NSN: string
  brand: string
  packSize: string
  storage: string
  origin: string
  originEnable?: boolean
  packDateEnable: boolean
  packDate: string
  sellByDateEnable: boolean
  sellByDate: string
  lot: string
  manufacturer: string
  note: string
  mfgSku: string
}

export type label34AObjSetting = {
  contractNumber: string
  NSN: string
  packDateEnable: boolean
  sellByDateEnable: boolean
  packDate: string
  sellByDate: string
  note: string
  mfgSku: string
  originEnable: boolean
  storage: string
  brand?: string
  origin?: string
  lot?: string
  manufacturer?: string
}

export type label34BObj = {
  productName: string
  contractNumber: string
  PO: string
  NSN: string
  brand: string
  packSize: string
  storage: string
  uom: string
  origin: string
  originEnable?: boolean
  grossWeight: string
  netWeight: string
  mfgSku: string
  mfgSkuEnable?: boolean
}

export type label34BObjSetting = {
  contractNumber: string
  NSN: string
  mfgSkuEnable: boolean
  originEnable: boolean
  storage: string
  brand?: string
  origin?: string
}

export type LotLabelObj = {
  productName: string
  lot: string
  poNum: string
  isPoNum: boolean
  origin: string
  isOrigin: boolean
  companyInfo: string
  isCompanyInfo: boolean
  barCode: string
  qrCode: string
  logo: string
  isLogo: boolean
  sku: string
  constantRatio: boolean
}

export type PalletLabelObj = {
  productName: string
  lot: string
  poNum: string
  isPoNum: boolean
  origin: string
  isOrigin: boolean
  companyInfo: string
  isCompanyInfo: boolean
  count: string
  barCode: string
  qrCode: string
  logo: string
  isLogo: boolean
  sku: string
  palletId: string
  constantRatio: boolean
}

// 4X6 pallet

export type pallet4X6Obj = {
  lot: string
  poNum: string
  field: string
  comm: string
  origin: string
  sku: string
  style: string
  size: string
  quantity: string
  description: string
  qrCode: string
}

export type pallet4X6ObjSetting = {
  lot: string
  poNum: string
  field: string
  comm: string
  origin: string
  sku: string
  style: string
  size: string
  quantity: string
  description: string
  qrCode: string
}
