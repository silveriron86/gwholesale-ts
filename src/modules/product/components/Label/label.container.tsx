import { withTheme } from 'emotion-theming'
import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { ProductDispatchProps, ProductModule, ProductProps } from '../../product.module'
import { RouteProps } from 'react-router'
import PageLayout from '~/components/PageLayout'
import './newlabel.css'
import Label25X1A from './components/Label25X1A'
import Label4X3 from './components/Label4X3'
import Label25X1B from './components/Label25X1B'
import {
  label251AQRObj,
  label251BQRObj,
  label251Obj,
  label252Obj,
  label34AObj,
  label34BObj,
  label43Obj,
  LotLabelObj,
  pallet4X6Obj,
  pallet4X6ObjSetting,
  PalletLabelObj,
} from './components/type/type'
import Label3X4A from './components/Label3X4A'
import Label3X4B from './components/Label3X4B'
import LotLabel from './components/LotLabel'
import PalletLabel from './components/PalletLabel'
import { Theme } from '~/common'
import Label25X1QR from './components/Label25X1QR'
import Label25X1BQR from './components/Label25X1BQR'
import Label4X6Pallet from './components/Label4X6Pallet'
import Label4X6PalletB from './components/Label4X6PalletB'

export type LabelProps = ProductDispatchProps &
  ProductProps &
  RouteProps & {
    theme: Theme
  }

interface LabelState {
  showPreferModal: Boolean
  unit: string
  update4X3: {
    id: number
    update: boolean
  }
  update2X1A: {
    id: number
    update: boolean
  }
  update2X1B: {
    id: number
    update: boolean
  }
  update2X1AQR: {
    id: number
    update: boolean
  }
  update2X1BQR: {
    id: number
    update: boolean
  }
  label4X3: label43Obj
  label2X1A: label251Obj
  label2X1AQR: label251AQRObj
  label2X1BQR: label251BQRObj
  label2X1B: label252Obj
  label3X4A: label34AObj
  label3X4B: label34BObj
  lotLabel: LotLabelObj
  palletLabel: PalletLabelObj
  palletLabel4X6: pallet4X6Obj
  palletLabel4X6B: pallet4X6Obj
  updatePage: boolean
  list: any[]
}

class NewLabelContainer extends React.PureComponent<LabelProps, LabelState> {
  state = {
    showPreferModal: false,
    unit: '4x3',
    update4X3: {
      id: -1,
      update: false,
    },
    update2X1A: {
      id: -1,
      update: false,
    },
    update2X1B: {
      id: -1,
      update: false,
    },
    update2X1AQR: {
      id: -1,
      update: false,
    },
    update2X1BQR: {
      id: -1,
      update: false,
    },
    label4X3: {
      productName: 'Example Name',
      lot: '00000-00000',
      poNum: '1234567',
      isPoNum: true,
      origin: 'origin',
      isOrigin: true,
      companyInfo: 'companyInfo',
      isCompanyInfo: true,
      count: '00.00',
      barCode: `0111WSWFRDvLxzW032020044231018112-001`,
      qrCode: '012226203370',
      logo: 'https://wholesaleware.com/images/gwholesale/logo/si_88571_30-11-2021-16-29-50-410.jpg',
      isLogo: true,
      sku: '',
    },
    label2X1A: {
      x1A: true,
      productName: 'Product Name',
      lot: '00000-00000',
      origin: 'origin',
      sellByDateShow: true,
      sellByDate: '1612483200000',
      barcode1: `0111WSWFRDvLxzW032020044231018112-001`,
      barcode2: '012226203370',
      companyInfo: 'companyInfo',
    },
    label2X1AQR: {
      x1A: true,
      productName: 'Product Name',
      lot: '00000-00000',
      origin: 'origin',
      sellByDateShow: true,
      sellByDate: '1612483200000',
      barcode1: `0111WSWFRDvLxzW032020044231018112-001`,
      barcode2: '012226203370',
      companyInfo: 'companyInfo',
    },
    label2X1BQR: {
      x1B: true,
      productName: 'Product Name',
      lot: '00000-00000',
      origin: 'origin',
      barcode1: `0111WSWFRDvLxzW032020044231018112-001`,
      note: '',
      companyInfo: 'companyInfo',
      sellByDateShow: true,
      sellByDate: '1612483200000',
    },
    label2X1B: {
      x1B: true,
      productName: 'Product Name',
      lot: '00000-00000',
      origin: 'origin',
      barcode1: `0111WSWFRDvLxzW032020044231018112-001`,
      note: '',
      companyInfo: 'companyInfo',
      sellByDateShow: true,
      sellByDate: '1612483200000',
    },
    label3X4A: {
      productName: 'product Name',
      contractNumber: 'SPE300-19-D-4045',
      PO: '56498',
      NSN: '891001e392381',
      brand: 'Chobani',
      packSize: '12/7oz',
      storage: 'Keep Chilled',
      origin: 'WA',
      originEnable: true,
      packDateEnable: true,
      packDate: '1612483200000',
      sellByDateEnable: true,
      sellByDate: '1612483200000',
      lot: '56498-164586',
      manufacturer: 'Chobani',
      note: 'Distributed: Costco Business Center',
      mfgSku: '1829001171',
    },
    label3X4B: {
      productName: 'product Name',
      contractNumber: 'SPE300-19-D-4045',
      PO: '56498',
      NSN: '891001e392381',
      brand: 'Chobani',
      packSize: '12/7oz',
      storage: 'Keep Chilled',
      uom: 'Case',
      origin: 'WA',
      originEnable: true,
      grossWeight: '42.00 LBs',
      netWeight: '42.00 LBs',
      mfgSku: '1829001171',
    },
    lotLabel: {
      productName: 'Example Name',
      lot: '00000-00000',
      poNum: '1234567',
      isPoNum: true,
      origin: 'origin',
      isOrigin: true,
      companyInfo: 'companyInfo',
      isCompanyInfo: true,
      count: '00.00',
      barCode: `0111WSWFRDvLxzW032020044231018112-001`,
      qrCode: '012226203370',
      logo: 'https://wholesaleware.com/images/gwholesale/logo/si_88571_30-11-2021-16-29-50-410.jpg',
      isLogo: true,
      sku: '',
      constantRatio: false,
    },
    palletLabel: {
      productName: 'Example Name',
      lot: '00000-00000',
      poNum: '1234567',
      isPoNum: true,
      origin: 'origin',
      isOrigin: true,
      companyInfo: 'companyInfo',
      isCompanyInfo: true,
      count: '00.00',
      barCode: `0111WSWFRDvLxzW032020044231018112-001`,
      qrCode: '012226203370',
      logo: 'https://wholesaleware.com/images/gwholesale/logo/si_88571_30-11-2021-16-29-50-410.jpg',
      isLogo: true,
      sku: '',
      palletId: '000000-001',
      constantRatio: false,
    },
    palletLabel4X6: {
      lot: '00000-00000',
      poNum: '1234567',
      field: 'XXXX111',
      comm: 'ONION',
      origin: 'origin',
      sku: '',
      style: 'Bag 12/2',
      size: 'Medium',
      quantity: '80',
      description: 'ONINOS Bag 12/2 Generic Organic Medium Produce of USA',
      qrCode: '012226203370',
    },
    palletLabel4X6B: {
      lot: '00000-00000',
      poNum: '1234567',
      field: 'XXXX111',
      comm: 'ONION',
      origin: 'origin',
      sku: '',
      style: 'Bag 12/2',
      size: 'Medium',
      quantity: '80',
      description: 'ONINOS Bag 12/2 Generic Organic Medium Produce of USA',
      qrCode: '012226203370',
    },
    updatePage: true,
    list: [],
  }

  componentDidMount() {
    //should get labelsetting from backend
    this.props.getLabelList()
    this.props.getUserSetting()
    const companyId =
      this.props.userSetting !== null
        ? this.props.userSetting.company
          ? this.props.userSetting.company.companyId
          : ''
        : ''
    //get 4X3 label
    const propLabel4X3 = this.props.labelList.find(
      (label) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 4X3',
    )
    if (propLabel4X3) {
      const label4X3 = Object.assign({}, this.state.label4X3)
      ;(label4X3.companyInfo = propLabel4X3.companyInformation ? propLabel4X3.companyInformation : ''),
        (label4X3.isCompanyInfo = propLabel4X3.companyInformationEnabled),
        (label4X3.isLogo = propLabel4X3.logoEnabled),
        (label4X3.isOrigin = propLabel4X3.originUsaEnabled),
        (label4X3.isPoNum = propLabel4X3.poNumberEnabled)
      this.setState({
        update4X3: {
          id: propLabel4X3.id,
          update: true,
        },
      })
      this.setState({
        label4X3: label4X3,
      })
    }
    //get 2.5X1 A label
    const propLabel2X1A = this.props.labelList.find(
      (label) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1A',
    )
    if (propLabel2X1A) {
      const label2X1A = this.state.label2X1A
      ;(label2X1A.companyInfo = propLabel2X1A.companyInformation ? propLabel2X1A.companyInformation : ''),
        (label2X1A.x1A = propLabel2X1A.isActive),
        (label2X1A.productName = propLabel2X1A.editedProductName ? propLabel2X1A.editedProductName : ''),
        (label2X1A.sellByDateShow = propLabel2X1A.sellByDateEnabled),
        (label2X1A.companyInfo = propLabel2X1A.companyInformation ? propLabel2X1A.companyInformation : '')
      this.setState({
        update2X1A: {
          id: propLabel2X1A.id,
          update: true,
        },
      })
      this.setState({
        label2X1A: label2X1A,
      })
    }
    //get 2.5X1 B label
    const propLabel2X1B = this.props.labelList.find(
      (label) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1B',
    )
    if (propLabel2X1B) {
      const label2X1B = this.state.label2X1B
      ;(label2X1B.companyInfo = propLabel2X1B.companyInformation ? propLabel2X1B.companyInformation : ''),
        (label2X1B.x1B = propLabel2X1B.isActive),
        (label2X1B.productName = propLabel2X1B.editedProductName ? propLabel2X1B.editedProductName : ''),
        (label2X1B.note = propLabel2X1B.notes ? propLabel2X1B.notes : ''),
        (label2X1B.sellByDateShow = propLabel2X1B.sellByDateEnabled),
        (label2X1B.companyInfo = propLabel2X1B.companyInformation ? propLabel2X1B.companyInformation : '')
      this.setState({
        update2X1B: {
          id: propLabel2X1B.id,
          update: true,
        },
      })
      this.setState({
        label2X1B: label2X1B,
      })
    }
    //get 2.5X1 A QR label
    const propLabel2X1AQR = this.props.labelList.find(
      (label) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1AQR',
    )
    if (propLabel2X1AQR) {
      const label2X1AQR = this.state.label2X1AQR
      ;(label2X1AQR.companyInfo = propLabel2X1AQR.companyInformation ? propLabel2X1AQR.companyInformation : ''),
        (label2X1AQR.x1A = propLabel2X1AQR.isActive),
        (label2X1AQR.productName = propLabel2X1AQR.editedProductName ? propLabel2X1AQR.editedProductName : ''),
        (label2X1AQR.sellByDateShow = propLabel2X1AQR.sellByDateEnabled),
        (label2X1AQR.companyInfo = propLabel2X1AQR.companyInformation ? propLabel2X1AQR.companyInformation : '')
      this.setState({
        update2X1AQR: {
          id: propLabel2X1AQR.id,
          update: true,
        },
      })
      this.setState({
        label2X1AQR: label2X1AQR,
      })
    }

    //get 2.5X1 B QR label
    const propLabel2X1BQR = this.props.labelList.find(
      (label) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1BQR',
    )
    if (propLabel2X1BQR) {
      const label2X1BQR = this.state.label2X1B
      ;(label2X1BQR.companyInfo = propLabel2X1BQR.companyInformation ? propLabel2X1BQR.companyInformation : ''),
        (label2X1BQR.x1B = propLabel2X1BQR.isActive),
        (label2X1BQR.productName = propLabel2X1BQR.editedProductName ? propLabel2X1BQR.editedProductName : ''),
        (label2X1BQR.note = propLabel2X1BQR.notes ? propLabel2X1BQR.notes : ''),
        (label2X1BQR.sellByDateShow = propLabel2X1BQR.sellByDateEnabled),
        (label2X1BQR.companyInfo = propLabel2X1BQR.companyInformation ? propLabel2X1BQR.companyInformation : '')
      this.setState({
        update2X1BQR: {
          id: propLabel2X1BQR.id,
          update: true,
        },
      })
      this.setState({
        label2X1BQR: label2X1BQR,
      })
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (
      (nextProps.labelList.length != 0 && this.state.updatePage !== false) ||
      nextProps.labelList != this.state.list
    ) {
      const companyId =
        this.props.userSetting !== null
          ? this.props.userSetting.company
            ? this.props.userSetting.company.companyId
            : ''
          : ''
      const propLabel4X3 = nextProps.labelList.find(
        (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 4X3',
      )
      if (propLabel4X3) {
        const label4X3 = Object.assign({}, this.state.label4X3)
        ;(label4X3.companyInfo = propLabel4X3.companyInformation ? propLabel4X3.companyInformation : ''),
          (label4X3.isCompanyInfo = propLabel4X3.companyInformationEnabled),
          (label4X3.isLogo = propLabel4X3.logoEnabled),
          (label4X3.isOrigin = propLabel4X3.originUsaEnabled),
          (label4X3.isPoNum = propLabel4X3.poNumberEnabled)
        this.setState({
          update4X3: {
            id: propLabel4X3.id,
            update: true,
          },
        })
        this.setState({
          label4X3: {
            productName: 'Example Name',
            lot: '00000-00000',
            poNum: '1234567',
            isPoNum: label4X3.isPoNum,
            origin: 'origin',
            isOrigin: label4X3.isOrigin,
            companyInfo: label4X3.companyInfo,
            isCompanyInfo: label4X3.isCompanyInfo,
            count: '00.00',
            barCode: `0111WSWFRDvLxzW032020044231018112-001`,
            qrCode: '012226203370',
            logo: 'https://wholesaleware.com/images/gwholesale/logo/si_88571_30-11-2021-16-29-50-410.jpg',
            isLogo: label4X3.isLogo,
            sku: '',
          },
        })
      }
      const propLabel2X1A = nextProps.labelList.find(
        (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1A',
      )
      if (propLabel2X1A) {
        const label2X1A = Object.assign({}, this.state.label2X1A)
        ;(label2X1A.companyInfo = propLabel2X1A.companyInformation ? propLabel2X1A.companyInformation : ''),
          (label2X1A.x1A = propLabel2X1A.isActive),
          (label2X1A.productName = propLabel2X1A.editedProductName ? propLabel2X1A.editedProductName : ''),
          (label2X1A.sellByDateShow = propLabel2X1A.sellByDateEnabled),
          (label2X1A.companyInfo = propLabel2X1A.companyInformation ? propLabel2X1A.companyInformation : '')
        this.setState({
          update2X1A: {
            id: propLabel2X1A.id,
            update: true,
          },
        })
        this.setState({
          label2X1A: label2X1A,
        })
      }
      const propLabel2X1AQR = nextProps.labelList.find(
        (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1AQR',
      )
      if (propLabel2X1AQR) {
        const label2X1AQR = this.state.label2X1AQR
        ;(label2X1AQR.companyInfo = propLabel2X1AQR.companyInformation ? propLabel2X1AQR.companyInformation : ''),
          (label2X1AQR.x1A = propLabel2X1AQR.isActive),
          (label2X1AQR.productName = propLabel2X1AQR.editedProductName ? propLabel2X1AQR.editedProductName : ''),
          (label2X1AQR.sellByDateShow = propLabel2X1AQR.sellByDateEnabled),
          (label2X1AQR.companyInfo = propLabel2X1AQR.companyInformation ? propLabel2X1AQR.companyInformation : '')
        this.setState({
          update2X1AQR: {
            id: propLabel2X1AQR.id,
            update: true,
          },
        })
        this.setState({
          label2X1AQR: label2X1AQR,
        })
      }
      const propLabel2X1B = nextProps.labelList.find(
        (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1B',
      )
      if (propLabel2X1B) {
        const label2X1B = Object.assign({}, this.state.label2X1B)
        ;(label2X1B.companyInfo = propLabel2X1B.companyInformation ? propLabel2X1B.companyInformation : ''),
          (label2X1B.x1B = propLabel2X1B.isActive),
          (label2X1B.productName = propLabel2X1B.editedProductName ? propLabel2X1B.editedProductName : ''),
          (label2X1B.note = propLabel2X1B.notes ? propLabel2X1B.notes : ''),
          (label2X1B.sellByDateShow = propLabel2X1B.sellByDateEnabled),
          (label2X1B.companyInfo = propLabel2X1B.companyInformation ? propLabel2X1B.companyInformation : '')
        this.setState({
          update2X1B: {
            id: propLabel2X1B.id,
            update: true,
          },
        })
        this.setState({
          label2X1B: label2X1B,
        })
      }
      const propLabel2X1BQR = this.props.labelList.find(
        (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1BQR',
      )
      if (propLabel2X1BQR) {
        const label2X1BQR = this.state.label2X1B
        ;(label2X1BQR.companyInfo = propLabel2X1BQR.companyInformation ? propLabel2X1BQR.companyInformation : ''),
          (label2X1BQR.x1B = propLabel2X1BQR.isActive),
          (label2X1BQR.productName = propLabel2X1BQR.editedProductName ? propLabel2X1BQR.editedProductName : ''),
          (label2X1BQR.note = propLabel2X1BQR.notes ? propLabel2X1BQR.notes : ''),
          (label2X1BQR.sellByDateShow = propLabel2X1BQR.sellByDateEnabled),
          (label2X1BQR.companyInfo = propLabel2X1BQR.companyInformation ? propLabel2X1BQR.companyInformation : '')
        this.setState({
          update2X1BQR: {
            id: propLabel2X1BQR.id,
            update: true,
          },
        })
        this.setState({
          label2X1BQR: label2X1BQR,
        })
      }
      this.setState({
        updatePage: false,
      })
    }
  }

  chooseUnit = (value: string): void => {
    this.setState({ unit: value })
  }

  save4X3label = (label: label43Obj, active: string) => {
    const obj = {
      labelType: 1,
      name: 'unit 4X3',
      isActive: active == 'Yes' ? true : false,
      logoEnabled: label.isLogo,
      additionalInformationEnabled: true,
      poNumberEnabled: label.isPoNum,
      originUsaEnabled: label.isOrigin,
      companyInformationEnabled: label.isCompanyInfo,
      companyInformation: label.companyInfo,
      sellByDateEnabled: true,
      editedProductName: '',
      notes: '',
      createdBy: {
        userId: localStorage.getItem('id'),
      },
    }
    if (this.state.update4X3.update == true) {
      obj['id'] = this.state.update4X3.id
      this.props.updateLabel(obj)
    } else {
      this.props.createLabel(obj)
    }
    const label4X3 = this.state.label4X3
    ;(label4X3.companyInfo = obj.companyInformation ? obj.companyInformation : ''),
      (label4X3.isCompanyInfo = obj.companyInformationEnabled),
      (label4X3.isLogo = obj.logoEnabled),
      (label4X3.isOrigin = obj.originUsaEnabled),
      (label4X3.isPoNum = obj.poNumberEnabled)
    this.setState({
      label4X3: label4X3,
    })
  }

  save2X1Label = (label: label251Obj) => {
    const obj = {
      labelType: 1,
      name: 'unit 2X1A',
      isActive: label.x1A,
      logoEnabled: true,
      additionalInformationEnabled: true,
      poNumberEnabled: true,
      originUsaEnabled: true,
      companyInformationEnabled: true,
      companyInformation: label.companyInfo,
      sellByDateEnabled: label.sellByDateShow,
      editedProductName: label.productName,
      notes: '',
      createdBy: {
        userId: localStorage.getItem('id'),
      },
    }
    if (this.state.update2X1A.update == true) {
      obj['id'] = this.state.update2X1A.id
      this.props.updateLabel(obj)
    } else {
      this.props.createLabel(obj)
    }
    const label2X1A = this.state.label2X1A
    ;(label2X1A.companyInfo = obj.companyInformation ? obj.companyInformation : ''),
      (label2X1A.x1A = obj.isActive),
      (label2X1A.productName = obj.editedProductName ? obj.editedProductName : ''),
      (label2X1A.sellByDateShow = obj.sellByDateEnabled),
      (label2X1A.companyInfo = obj.companyInformation ? obj.companyInformation : '')
    this.setState({
      label2X1A: label2X1A,
    })
  }

  save2X1AQRLabel = (label: label251AQRObj) => {
    const obj = {
      labelType: 1,
      name: 'unit 2X1AQR',
      isActive: label.x1A,
      logoEnabled: true,
      additionalInformationEnabled: true,
      poNumberEnabled: true,
      originUsaEnabled: true,
      companyInformationEnabled: true,
      companyInformation: label.companyInfo,
      sellByDateEnabled: label.sellByDateShow,
      editedProductName: label.productName,
      notes: '',
      createdBy: {
        userId: localStorage.getItem('id'),
      },
    }
    if (this.state.update2X1AQR.update == true) {
      obj['id'] = this.state.update2X1AQR.id
      this.props.updateLabel(obj)
    } else {
      this.props.createLabel(obj)
    }
    const label2X1AQR = this.state.label2X1AQR
    ;(label2X1AQR.companyInfo = obj.companyInformation ? obj.companyInformation : ''),
      (label2X1AQR.x1A = obj.isActive),
      (label2X1AQR.productName = obj.editedProductName ? obj.editedProductName : ''),
      (label2X1AQR.sellByDateShow = obj.sellByDateEnabled),
      (label2X1AQR.companyInfo = obj.companyInformation ? obj.companyInformation : '')
    this.setState({
      label2X1AQR: label2X1AQR,
    })
  }

  save2X1BLabel = (label: label252Obj) => {
    const obj = {
      labelType: 1,
      name: 'unit 2X1B',
      isActive: label.x1B,
      logoEnabled: true,
      additionalInformationEnabled: true,
      poNumberEnabled: true,
      originUsaEnabled: true,
      companyInformationEnabled: true,
      companyInformation: label.companyInfo,
      sellByDateEnabled: label.sellByDateShow,
      editedProductName: label.productName,
      notes: label.note,
      createdBy: {
        userId: localStorage.getItem('id'),
      },
    }
    if (this.state.update2X1B.update == true) {
      obj['id'] = this.state.update2X1B.id
      this.props.updateLabel(obj)
    } else {
      this.props.createLabel(obj)
    }
    const label2X1B = this.state.label2X1B
    ;(label2X1B.companyInfo = obj.companyInformation ? obj.companyInformation : ''),
      (label2X1B.x1B = obj.isActive),
      (label2X1B.productName = obj.editedProductName ? obj.editedProductName : ''),
      (label2X1B.note = obj.notes ? obj.notes : ''),
      (label2X1B.sellByDateShow = obj.sellByDateEnabled),
      (label2X1B.companyInfo = obj.companyInformation ? obj.companyInformation : '')
    this.setState({
      label2X1B: label2X1B,
    })
  }

  save2X1BQRLabel = (label: label251BQRObj) => {
    const obj = {
      labelType: 1,
      name: 'unit 2X1BQR',
      isActive: label.x1B,
      logoEnabled: true,
      additionalInformationEnabled: true,
      poNumberEnabled: true,
      originUsaEnabled: true,
      companyInformationEnabled: true,
      companyInformation: label.companyInfo,
      sellByDateEnabled: label.sellByDateShow,
      editedProductName: label.productName,
      notes: label.note,
      createdBy: {
        userId: localStorage.getItem('id'),
      },
    }
    console.log(obj)
    if (this.state.update2X1BQR.update == true) {
      obj['id'] = this.state.update2X1BQR.id
      this.props.updateLabel(obj)
    } else {
      this.props.createLabel(obj)
    }
    const label2X1BQR = this.state.label2X1BQR
    ;(label2X1BQR.companyInfo = obj.companyInformation ? obj.companyInformation : ''),
      (label2X1BQR.x1B = obj.isActive),
      (label2X1BQR.productName = obj.editedProductName ? obj.editedProductName : ''),
      (label2X1BQR.note = obj.notes ? obj.notes : ''),
      (label2X1BQR.sellByDateShow = obj.sellByDateEnabled),
      (label2X1BQR.companyInfo = obj.companyInformation ? obj.companyInformation : '')
    this.setState({
      label2X1BQR: label2X1BQR,
    })
  }

  save3X4ALabel = (label: label34AObj) => {
    this.setState({
      label3X4A: label,
    })
  }

  save3X4BLabel = (label: label34BObj) => {
    this.setState({
      label3X4B: label,
    })
  }

  savePallet4X6 = (label: pallet4X6ObjSetting) => {
    const obj = {
      lot: label.lot,
      poNum: label.poNum,
      field: label.field,
      comm: label.comm,
      origin: label.origin,
      sku: label.sku,
      style: label.style,
      size: label.size,
      quantity: label.quantity,
      description: label.description,
      qrCode: label.qrCode,
    }
    this.setState({
      palletLabel4X6: obj,
    })
  }

  savePallet4X6B = (label: pallet4X6ObjSetting) => {
    const obj = {
      lot: label.lot,
      poNum: label.poNum,
      field: label.field,
      comm: label.comm,
      origin: label.origin,
      sku: label.sku,
      style: label.style,
      size: label.size,
      quantity: label.quantity,
      description: label.description,
      qrCode: label.qrCode,
    }
    this.setState({
      palletLabel4X6B: obj,
    })
  }
  getRightPart = () => {
    switch (this.state.unit) {
      case '4x3':
        return (
          <div className="newLabelRightLeftPadding">
            <Label4X3
              labelObj={this.state.label4X3}
              saveLabel={(label: label43Obj, active: string) => this.save4X3label(label, active)}
            />
          </div>
        )
      case '2X1 A':
        return (
          <div className="newLabelRightLeftPadding">
            <Label25X1A labelObj={this.state.label2X1A} saveLabel={(label: label251Obj) => this.save2X1Label(label)} />
          </div>
        )
      case '2X1 A QR':
        return (
          <div className="newLabelRightLeftPadding">
            <Label25X1QR
              labelObj={this.state.label2X1AQR}
              saveLabel={(label: label251AQRObj) => this.save2X1AQRLabel(label)}
            />
          </div>
        )
      case '2X1 B':
        return (
          <div className="newLabelRightLeftPadding">
            <Label25X1B labelObj={this.state.label2X1B} saveLabel={(label: label252Obj) => this.save2X1BLabel(label)} />
          </div>
        )
      case '2X1 B QR':
        return (
          <div className="newLabelRightLeftPadding">
            <Label25X1BQR
              labelObj={this.state.label2X1BQR}
              saveLabel={(label: label251BQRObj) => this.save2X1BQRLabel(label)}
            />
          </div>
        )
      case '3X4 A':
        return (
          <div className="newLabelRightLeftPadding">
            <Label3X4A labelObj={this.state.label3X4A} saveLabel={(label: label34AObj) => this.save3X4ALabel(label)} />
          </div>
        )
      case '3X4 B':
        return (
          <div className="newLabelRightLeftPadding">
            <Label3X4B labelObj={this.state.label3X4B} saveLabel={(label: label34BObj) => this.save3X4BLabel(label)} />
          </div>
        )
      case 'lotLabel':
        return (
          <div className="newLabelRightLeftPadding">
            <LotLabel labelObj={this.state.lotLabel} />
          </div>
        )
      case 'palletLabel':
        return (
          <div className="newLabelRightLeftPadding">
            <PalletLabel labelObj={this.state.palletLabel} />
          </div>
        )
      case '4X6A Pallet Label':
        return (
          <div className="newLabelRightLeftPadding">
            <Label4X6Pallet
              labelObj={this.state.palletLabel4X6}
              saveLabel={(label: pallet4X6ObjSetting) => this.savePallet4X6(label)}
            />
          </div>
        )
      case '4X6B Pallet Label':
        return (
          <div className="newLabelRightLeftPadding">
            <Label4X6PalletB
              labelObj={this.state.palletLabel4X6B}
              saveLabel={(label: pallet4X6ObjSetting) => this.savePallet4X6B(label)}
            />
          </div>
        )
    }
  }

  render() {
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Label'}>
        <div style={{ display: 'flex', backgroundColor: 'white', height: '1100px' }}>
          <div
            style={{
              width: '263px',
              display: 'inline',
              paddingTop: '35px',
              textAlign: 'right',
              backgroundColor: '#F3F3F3',
            }}
          >
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('4x3')}>
              UNIT 4x3
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('2X1 A')}>
              UNIT 2X1 A
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('2X1 A QR')}>
              UNIT 2X1 A QR
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('2X1 B')}>
              UNIT 2X1 B
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('2X1 B QR')}>
              UNIT 2X1 B QR
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('3X4 A')}>
              UNIT 3X4 A
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('3X4 B')}>
              UNIT 3X4 B
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('lotLabel')}>
              Lot Label
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('palletLabel')}>
              Pallet Label
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('4X6A Pallet Label')}>
              4X6A Pallet Label
            </p>
            <p className="newLabelLeftText" onClick={() => this.chooseUnit('4X6B Pallet Label')}>
              4X6B Pallet Label
            </p>
          </div>
          <div
            style={{ textAlign: 'left', display: 'inline', backgroundColor: 'white', marginTop: '19px', width: '100%' }}
          >
            <p
              className="newLabelRightPartTitle"
              style={{ color: '#22282A', fontWeight: 'bold', fontStyle: '18px' }}
            >{`Labels - Unit ${this.state.unit}`}</p>
            {this.getRightPart()}
          </div>
        </div>
      </PageLayout>
    )
  }
}

export default NewLabelContainer

const mapStateToProps = (state: GlobalState) => state.product

export const NewLabel = withTheme(connect(ProductModule)(mapStateToProps)(NewLabelContainer))
