import React, { useState } from 'react'
import { ThemeModal } from '~/modules/customers/customers.style'
import { SettingsService } from '~/modules/settings/settings.service'
import { useRequest } from 'ahooks'
import { promisify } from '~/common/promisify'
import { Input, Button, notification } from 'antd'

type Props = {
  visible: boolean
  addUOMCallback: (name: string) => void
  close: () => void
  uoms: { name: string }[]
}

export default function CreateUOM(props: Props) {
  const { visible, addUOMCallback, close, uoms = [] } = props
  const [name, setName] = useState<string | undefined>()

  const { loading, run } = useRequest(
    (uomName) => {
      return promisify(SettingsService.instance.saveCompanyProductType)({
        name: uomName,
        type: 'unitOfMeasure',
      })
    },
    {
      manual: true,
      onSuccess: (res: any, params) => {
        addUOMCallback && addUOMCallback(res?.body?.data?.name)
        close()
      },
      onError: (error) => {
        console.error(error)
      },
    },
  )

  return (
    <ThemeModal
      visible={visible}
      title="Create new unit of measurement(UOM)"
      cancelText="Close"
      okButtonProps={{ style: { display: 'none' } }}
      onCancel={close}
      afterClose={() => setName('')}
    >
      <Input
        value={name}
        style={{ width: 250, marginRight: 15 }}
        onChange={(e) => setName(e.target.value)}
        placeholder="New Unit of Measurement"
      />
      <Button
        loading={loading}
        disabled={!name}
        style={{ width: 160 }}
        type="primary"
        shape="round"
        onClick={() => {
          const exist = uoms.find((u) => u.name === name)
          if (exist) {
            notification.warn({ message: name + ' already exists' })
            return
          }
          run(name)
        }}
      >
        Create New UOM
      </Button>
    </ThemeModal>
  )
}
