import { Button, Col, Input, Row, Select, Typography, Upload } from 'antd'
import React, { FC, useEffect, useRef, useState } from 'react'

import { CACHED_ACCESSTOKEN } from '~/common/const'
import JsBarcode from 'jsbarcode'
import QRCode from 'qrcode-svg'
import { UploadChangeParam } from 'antd/lib/upload'
import { fabric } from 'fabric'
import { getFileUrl } from '~/common/utils'
import moment from 'moment'
import { remove } from 'lodash'
import { useImmer } from 'use-immer'

const { Title, Paragraph } = Typography
const { Option } = Select
const ButtonGroup = Button.Group

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

const CustomLabel: FC = () => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null)
  const canvas = useRef<any>(null)
  const canvasObjRef = useRef<any>({})
  const [data, setData] = useImmer<IOptions[]>([])
  const [labelType, setLabelType] = useState<string>()
  const svgObjRef = useRef<any>(null)

  useEffect(() => {
    const width = 800
    const height = 600
    const RATIO = 3

    const c = new fabric.Canvas(canvasRef.current, { width, height, backgroundColor: '#fff', isDrawingMode: false })
    // c.setDimensions({ width, height })
    canvas.current = c

    // init canvas for retina
    const canvasEl: HTMLCanvasElement = c.getElement() as HTMLCanvasElement
    canvasEl.width = width * RATIO
    canvasEl.height = height * RATIO
    canvasEl.style.width = width + 'px'
    canvasEl.style.height = height + 'px'
    canvasEl.getContext('2d')?.scale(RATIO, RATIO)

    canvasObjRef.current = new Map()
    const store = localStorage.getItem('CUSTOMER_LABEL_CANVAS')
    const options = JSON.parse(localStorage.getItem('CUSTOMER_LABEL_OPTIONS') || '[]')

    if (store) {
      c.loadFromJSON(JSON.parse(store), function() {
        c.renderAll()
        c.forEachObject((obj) => {
          if (obj.id) {
            const key = Symbol()
            const thisOption = options.find((o: IOptions) => o.id === obj.id)
            canvasObjRef.current.set(key, obj)
            setData((draft) => {
              draft.push({ ...thisOption, key })
            })
          }
        })
      })
    }

    // fabric.loadSVGFromString(SVG_STRING, (objects, options) => {
    //   const loadedObjects = fabric.util.groupSVGElements(objects, options)
    //   // loadedObjects.scaleToWidth(400)
    //   loadedObjects.scaleToHeight(200)
    //   loadedObjects.set({
    //     left: 0,
    //     top: 0,
    //   })
    //   svgObjRef.current = loadedObjects
    //   c.add(loadedObjects).renderAll()
    // })
    fabric.loadSVGFromURL("https://tuangou-local.s3-us-west-2.amazonaws.com/safe_handling.svg", (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)
      loadedObjects.scaleToHeight(164)
      loadedObjects.set({
        left: 0,
        top: 0,
      })
      svgObjRef.current = loadedObjects
      c.add(loadedObjects).renderAll()
    })
    fabric.loadSVGFromURL("https://tuangou-local.s3-us-west-2.amazonaws.com/asset.svg", (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)
      loadedObjects.scaleToHeight(164)
      loadedObjects.set({
        left: 0,
        top: 200,
      })
      svgObjRef.current = loadedObjects
      c.add(loadedObjects).renderAll()
    })
    // fabric.loadSVGFromURL(url, (objects, options) => {
    //   const loadedObjects = fabric.util.groupSVGElements(objects, options)
    //
    //   loadedObjects.set(style)
    //
    //   const id = hasKey ? +new Date() : data.find((item) => item.key === key)!.id
    //   addId(loadedObjects, id)
    //
    //   setData((draft) => {
    //     const targetData = draft.find((item) => item.key === key)
    //     targetData!.url = url
    //   })
    //
    //   canvasObjRef.current.set(key, loadedObjects)
    //
    //   c.add(loadedObjects).renderAll()
    // })
  }, [])

  const addBarcode = () => {
    const c = canvas.current
    const key = Symbol()
    const { label, style } = labelDefaultSettings.barcode
    const barcodeStr = textToSvgBarcode(label)

    fabric.loadSVGFromString(barcodeStr, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)

      loadedObjects.set(style)

      const id = +new Date()

      addId(loadedObjects, id)

      setData((draft) => {
        draft.push({ key, label, type: 'barcode', id })
      })

      canvasObjRef.current.set(key, loadedObjects)

      c.add(loadedObjects).renderAll()
    })
  }

  const addQrCode = () => {
    const c = canvas.current
    const key = Symbol()
    const { label, style } = labelDefaultSettings['qr-code']

    const qrCodeSVGStr = textToSVGQrCode(label)

    fabric.loadSVGFromString(qrCodeSVGStr, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)

      loadedObjects.set(style)

      const id = +new Date()

      addId(loadedObjects, id)
      setData((draft) => {
        draft.push({ key, label, type: 'qr-code', id })
      })
      canvasObjRef.current.set(key, loadedObjects)

      c.add(loadedObjects).renderAll()
    })
  }

  const addImage = (type: string = 'image') => {
    const key = Symbol()
    const { label } = labelDefaultSettings.image
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label, type, id })
    })
  }

  const loadImage = (key: Symbol, url: string) => {
    const { style } = labelDefaultSettings.image
    const c = canvas.current
    const map = canvasObjRef.current

    if (canvasObjRef.current.has(key)) {
      changeImg(map.get(key), url, c)
      return
    }

    fabric.Image.fromURL(
      url,
      (img) => {
        img.set(style)
        img.scaleToWidth(140)
        c.add(img).renderAll()

        setData((draft) => {
          const targetData = draft.find((item) => item.key === key)
          targetData!.url = url
        })

        canvasObjRef.current.set(key, img)
      },
      { crossOrigin: 'Anonymous' },
    )
  }

  const loadSVG = (key: Symbol, url: string) => {
    const { style } = labelDefaultSettings.image
    const c = canvas.current
    const map = canvasObjRef.current
    const hasKey = canvasObjRef.current.has(key)

    if (hasKey) {
      c.remove(map.get(key))
    }

    fabric.loadSVGFromURL(url, (objects, options) => {
      const loadedObjects = fabric.util.groupSVGElements(objects, options)

      loadedObjects.set(style)

      const id = hasKey ? +new Date() : data.find((item) => item.key === key)!.id
      addId(loadedObjects, id)

      setData((draft) => {
        const targetData = draft.find((item) => item.key === key)
        targetData!.url = url
      })

      canvasObjRef.current.set(key, loadedObjects)

      c.add(loadedObjects).renderAll()
    })
  }

  const addLabel = (type: string) => {
    const c = canvas.current
    const key = Symbol()
    const { label, style } = labelDefaultSettings[type] || labelDefaultSettings.address
    const id = +new Date()
    setData((draft) => {
      draft.push({ key, label, type, id })
    })

    let text: any

    if (type === 'address') {
      text = new fabric.Textbox(label, { ...style })
    } else {
      text = new fabric.Text(label, { ...style })
    }

    addId(text, id)

    canvasObjRef.current.set(key, text)

    c.add(text).renderAll()
  }

  const removeObject = (key: Symbol) => {
    const c = canvas.current
    const map = canvasObjRef.current
    c.remove(map.get(key)).renderAll()
    setData((draft) => {
      remove(draft, (item) => item.key === key)
    })
    map.delete(key)
  }

  const handleChangeValue = (value: string, key: Symbol, type: string) => {
    if (!value) {
      return
    }

    const c = canvas.current
    const map = canvasObjRef.current
    setData((draft) => {
      const targetData = draft.find((item) => item.key === key)
      targetData && (targetData.label = value)
    })

    const svgTypes = ['barcode', 'qr-code']

    if (svgTypes.includes(type)) {
      changeSVG(map, key, value, c, type as 'barcode' | 'qr-code')
    } else {
      map.get(key).set('text', value)
    }

    c.renderAll()
  }

  const handleAddLabel = () => {
    if (!labelType) {
      return
    }

    if (labelType === 'barcode') {
      addBarcode()
    } else if (labelType === 'qr-code') {
      addQrCode()
    } else if (labelType === 'image' || labelType === 'svg') {
      addImage(labelType)
    } else {
      addLabel(labelType)
    }
  }

  const handleUpdateImage = (info: UploadChangeParam, key: Symbol, type: string = 'image') => {
    if (info.file.status === 'done') {
      const { link } = info.file.response
      const url = getFileUrl(link, true)
      type === 'image' ? loadImage(key, url) : loadSVG(key, url)
    }
  }

  const handleSave = () => {
    const c = canvas.current
    const json = c.toDatalessJSON()
    localStorage.setItem('CUSTOMER_LABEL_CANVAS', JSON.stringify(json))
    localStorage.setItem('CUSTOMER_LABEL_OPTIONS', JSON.stringify(data))
  }

  type styleChange = 'bold' | 'fontSizePlus' | 'fontSizeMinus'

  const handleLabelStyleChange = (key: Symbol, action: styleChange) => {
    const c = canvas.current
    const map = canvasObjRef.current
    const target = map.get(key)
    if (action === 'fontSizePlus') {
      target.set('fontSize', (target.fontSize += 2)).setCoords()
    }

    if (action === 'fontSizeMinus') {
      target.set('fontSize', (target.fontSize -= 2)).setCoords()
    }

    if (action === 'bold') {
      target.set('fontWeight', target.fontWeight === 'normal' ? 'bold' : 'normal').setCoords()
    }

    c.renderAll()
  }

  return (
    <>
      <Row>
        <Col span={24}>
          <canvas id="canvas" ref={canvasRef} style={{ border: '1px solid #ccc', marginTop: '30px' }} />
        </Col>
      </Row>
      <Row style={{ marginTop: '60px' }}>
        <Col span={24} style={{ textAlign: 'left' }}>
          <Typography>
            <Title level={3}>Add Custom Label</Title>
            <Paragraph>Add Custom Label</Paragraph>
            <Paragraph>
              <Button
                icon="delete"
                type="primary"
                onClick={() => {
                  canvas.current.remove(svgObjRef.current).renderAll()
                }}
              >
                Delete SVG
              </Button>
            </Paragraph>
          </Typography>
          <Row style={{ marginBottom: '24px' }}>
            <Col span={7}>
              <Select
                defaultValue={labelType}
                onChange={(value: string) => {
                  setLabelType(value)
                }}
                placeholder="Select type of addition"
                style={{ width: 200 }}
              >
                <Option value="barcode">Barcode</Option>
                <Option value="qr-code">QR Code</Option>
                <Option value="address">Address</Option>
                <Option value="lots">Lots</Option>
                <Option value="name">Name</Option>
                <Option value="date">Date</Option>
                <Option value="uom">UOM</Option>
                <Option value="image">Add Image</Option>
                {/* <Option value="svg">Add SVG</Option> */}
                <Option value="customLabel">Add Your Custom Label</Option>
              </Select>
            </Col>
            <Col span={6}>
              <Button icon="plus-circle" type="primary" onClick={handleAddLabel}>
                Add Element to Canvas
              </Button>
            </Col>
          </Row>

          {data.map((item, index) => {
            let left: any = (
              <Input
                style={{ width: 200 }}
                defaultValue={item.label}
                onPressEnter={(e) => {
                  handleChangeValue(e.target.value, item.key, item.type)
                }}
              />
            )

            if (item.type === 'image' || item.type === 'svg') {
              left = (
                <Upload
                  {...uploadConfig}
                  onChange={(info) => {
                    handleUpdateImage(info, item.key, item.type)
                  }}
                >
                  <Button icon="upload" style={{ width: 200 }}>
                    Click to Upload
                  </Button>
                </Upload>
              )
            }

            return (
              <Row style={{ marginBottom: '24px' }} key={index}>
                <Col span={7}>{left}</Col>

                <Col span={5}>
                  {!noLabelStyle.includes(item.type) && (
                    <ButtonGroup>
                      <Button type="link">Font:</Button>
                      <Button
                        icon="plus"
                        onClick={() => {
                          handleLabelStyleChange(item.key, 'fontSizePlus')
                        }}
                      />
                      <Button
                        icon="minus"
                        onClick={() => {
                          handleLabelStyleChange(item.key, 'fontSizeMinus')
                        }}
                      />
                      <Button
                        icon="bold"
                        onClick={() => {
                          handleLabelStyleChange(item.key, 'bold')
                        }}
                      />
                    </ButtonGroup>
                  )}
                </Col>

                <Col span={5}>
                  <Button
                    icon="delete"
                    type="primary"
                    onClick={() => {
                      removeObject(item.key)
                    }}
                  />
                </Col>
              </Row>
            )
          })}
        </Col>
      </Row>
      <Row style={{ marginTop: '60px', textAlign: 'left' }}>
        <Button
          icon="printer"
          type="primary"
          onClick={() => {
            printCanvas(canvas.current.toSVG() as string)
          }}
        >
          Print Label
        </Button>
        <Button icon="cloud-upload" type="primary" style={{ marginLeft: '16px' }} onClick={handleSave}>
          Save
        </Button>
      </Row>
    </>
  )
}

export default CustomLabel

function textToBase64Barcode(text: string) {
  const canvas = document.createElement('canvas')
  JsBarcode(canvas, text, { format: 'CODE39', width: 4, height: 160, fontSize: 36 })
  return canvas.toDataURL('image/png')
}

function textToSVGQrCode(text: string) {
  return new QRCode({
    content: text,
    padding: 1,
    width: 256,
    height: 256,
    color: '#000000',
    ecl: 'M',
  }).svg()
}

function textToSvgBarcode(text: string): string {
  const svg = document.createElement('svg')
  JsBarcode(svg, text, {
    format: 'CODE39',
    width: 3,
    height: 110,
    fontSize: 20,
  })

  return svg.outerHTML
}

function printCanvas(svgString: string) {
  const printWindow = window.open('', 'PRINT', 'height=600')
  if (!printWindow) {
    return
  }
  printWindow!.document.write(svgString)
  printWindow!.document.close()
  printWindow!.focus() // necessary for IE >= 10*/
  printWindow!.print()
  printWindow!.onafterprint = function() {
    printWindow!.close()
  }
}

const textStyle = {
  fontFamily: 'sans-serif',
}

const labelDefaultSettings = {
  barcode: {
    label: '24744VW1021-3',
    style: {
      left: 20,
      top: 150,
    },
  },

  'qr-code': {
    label: 'https://lnk.bio/grubmarket',
    style: {
      left: 370,
      top: 270,
    },
  },
  address: {
    label: 'Manufactured by Grand Food, Hayward, CA 94445',
    style: {
      ...textStyle,
      width: 800,
      top: 570,
      textAlign: 'center',
      fontSize: 16,
    },
  },
  lots: {
    label: '1201-3',
    style: {
      ...textStyle,
      fontSize: 20,
      fontWeight: 'bold',
      left: 30,
      top: 30,
    },
  },
  name: {
    label: 'Beef for Stew Meat VP',
    style: {
      ...textStyle,
      left: 30,
      top: 80,
      fontSize: 24,
    },
  },
  date: {
    label: `PACKED ON: ${moment().format('MM/DD/YYYY')}`,
    style: {
      ...textStyle,
      left: 30,
      top: 110,
      fontSize: 16,
    },
  },
  uom: {
    label: 'Net Wt.',
    style: {
      ...textStyle,
      top: 34,
      left: 450,
      fontSize: 14,
    },
  },
  image: {
    label: 'user image',
    style: {
      left: 430,
      top: 110,
    },
  },
  customLabel: {
    label: 'input your label',
    style: {
      ...textStyle,
      left: 30,
      top: 260,
      fontSize: 16,
    },
  },
}

const noLabelStyle = ['image', 'barcode', 'qr-code', 'svg']

type imageTypes = 'barcode' | 'image'

const changeImg = (target: any, value: string, canvas: any, type: imageTypes = 'image') => {
  const imgEl = fabric.util.createImage()
  const { style } = labelDefaultSettings.image
  imgEl.setAttribute('crossOrigin', 'Anonymous')
  imgEl.onload = () => {
    target.setElement(imgEl)
    if (type === 'image') {
      target.set(style)
      target.scaleToWidth(140)
    }
    canvas.renderAll()
  }
  imgEl.src = type === 'barcode' ? textToBase64Barcode(value) : value
}

const changeSVG = (map: any, key: Symbol, value: string, canvas: any, type: 'barcode' | 'qr-code') => {
  const svgStr = type === 'barcode' ? textToSvgBarcode(value) : textToSVGQrCode(value)
  const { style } = labelDefaultSettings[type]

  fabric.loadSVGFromString(svgStr, (objects, options) => {
    const loadedObjects = fabric.util.groupSVGElements(objects, options)

    loadedObjects.set(style)
    canvas.remove(map.get(key))

    //TODO:: 处理ID

    // const id = +new Date()

    // addId(loadedObjects, id)
    map.set(key, loadedObjects)

    canvas.add(loadedObjects).renderAll()
  })
}

const uploadConfig = {
  action: process.env.WSW2_HOST + '/api/v1/wholesale/file/froala-upload',
  headers: {
    Authorization: `Bearer ${
      localStorage.getItem(CACHED_ACCESSTOKEN) != null ? localStorage.getItem(CACHED_ACCESSTOKEN) : ''
    }`,
  },
}

function toObjectFn(target: any) {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
