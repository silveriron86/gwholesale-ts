import { css } from '@emotion/core'
import { lightGrey } from '~/common'

export const CanvasStyle = css({
  border: `1px solid ${lightGrey}`,
})
