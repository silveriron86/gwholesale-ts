import React from 'react'
import { DatePicker } from 'antd'
import { Icon as IconSvg } from '~/components'
import moment from 'moment'
import {
  Wrapper,
  TipWithChildren,
  TipText,
  TipDatePicker,
  TipDatePickerText,
  TipDatePickerWrapper,
  LotsSplit,
  LotsPanel,
  LotsPanelTip,
  LotsPanelValue,
  LotsPanelWrapper,
  LotsPanelList,
  LotsPanelListItem,
  LotsPanelListItemTip,
  LotsPanelListItemValue,
} from './product.component.style'

const mock = [
  [
    { title: 'QUOTED / SCHED. DATE', value: '--' },
    { title: 'REcieve date', value: '--' },
    { title: 'PO #', value: '--' },
    { title: 'Lot #', value: '--' },
    { title: 'units on hand', value: '--' },
  ],
  [
    { title: 'UOM', value: '--' },
    { title: '# of times sold', value: '--' },
    { title: 'units ordered', value: '--' },
    { title: 'Units received', value: '--' },
    { title: 'Units shipped', value: '--' },
  ],
  [
    { title: 'units committed', value: '--' },
    { title: 'Units Confirmed', value: '--' },
    { title: 'Available To Sell', value: '--' },
  ],
]

export class DeprecatedProductLots extends React.Component<T> {
  state = {}

  onDateStartChange = () => {}

  onDateEndChange = () => {}
  render() {
    return (
      <Wrapper>
        <TipWithChildren>
          <TipText>Lots</TipText>
          <TipDatePicker>
            <TipDatePickerText style={{ marginRight: '30px' }}>Records Form</TipDatePickerText>
            {this.renderDateEditor(this.onDateStartChange, moment())}
            <TipDatePickerText style={{ margin: '0 20px' }}>to</TipDatePickerText>
            {this.renderDateEditor(this.onDateEndChange, moment().add(1, 'month'))}
          </TipDatePicker>
        </TipWithChildren>
        <LotsSplit />
        <LotsPanelWrapper>
          <LotsPanel>
            <LotsPanelTip>ESTIMATED TOTAL COST</LotsPanelTip>
            <LotsPanelValue>--</LotsPanelValue>
          </LotsPanel>
          <LotsPanel>
            <LotsPanelTip>SET PRICE</LotsPanelTip>
            <LotsPanelValue>--</LotsPanelValue>
          </LotsPanel>
          <LotsPanel>
            <LotsPanelTip>VENDOR</LotsPanelTip>
            <LotsPanelValue>--</LotsPanelValue>
          </LotsPanel>
        </LotsPanelWrapper>
        <LotsPanelWrapper>
          <LotsPanelList style={{ paddingLeft: '0', border: 'none' }}>
            {mock[0].map((item, index) => (
              <LotsPanelListItem key={index}>
                <LotsPanelListItemTip>{item.title}</LotsPanelListItemTip>
                <LotsPanelListItemValue>{item.value}</LotsPanelListItemValue>
              </LotsPanelListItem>
            ))}
          </LotsPanelList>
          <LotsPanelList>
            {mock[1].map((item, index) => (
              <LotsPanelListItem key={index}>
                <LotsPanelListItemTip>{item.title}</LotsPanelListItemTip>
                <LotsPanelListItemValue>{item.value}</LotsPanelListItemValue>
              </LotsPanelListItem>
            ))}
          </LotsPanelList>
          <LotsPanelList>
            {mock[2].map((item, index) => (
              <LotsPanelListItem key={index}>
                <LotsPanelListItemTip>{item.title}</LotsPanelListItemTip>
                <LotsPanelListItemValue>{item.value}</LotsPanelListItemValue>
              </LotsPanelListItem>
            ))}
          </LotsPanelList>
        </LotsPanelWrapper>
      </Wrapper>
    )
  }

  private renderDateEditor(onChange: any, defaultValue: any) {
    const calendarIcon = <IconSvg type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    return (
      <TipDatePickerWrapper>
        <DatePicker
          placeholder={dateFormat}
          format={dateFormat}
          suffixIcon={calendarIcon}
          defaultValue={defaultValue}
          onChange={onChange}
          allowClear={false}
        />
      </TipDatePickerWrapper>
    )
  }
}
