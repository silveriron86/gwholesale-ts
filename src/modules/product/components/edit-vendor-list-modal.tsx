import { Popconfirm, Select } from 'antd'
import React from 'react'
import { ThemeModal, ThemeButton, ThemeSelect, ThemeTable } from '~/modules/customers/customers.style'
import { PayloadIcon } from '~/components/elements/elements.style'
import { Icon as IconSvg } from '~/components/icon/'
import styled from '@emotion/styled'

export type EditVendorListModalProps = {
  visible: boolean
  onCancel: () => void
  productVendors: any[]
  addRemoveVendor: Function
  productId: string | number
  briefVendors: any[]
}

class EditVendorListModal extends React.Component<EditVendorListModalProps> {
  state = {
    selectedVendors: [],
    operating: false,
  }

  componentDidUpdate(preProps) {
    if (preProps.productVendors !== this.props.productVendors) {
      this.setState({
        selectedVendors: [],
        operating: false,
      })
    }
  }

  columns = [
    { title: 'name', dataIndex: 'wholesaleClient.clientCompany.companyName' },
    {
      title: '',
      dataIndex: 'wholesaleClient.clientId',
      render: (val: number, record: any) => {
        return (
          <Popconfirm
            title="Are you sure to remove?"
            onConfirm={() => {
              this.removeVendor(val)
            }}
          >
            <a>
              <PayloadIcon>
                <IconSvg type="trash" viewBox="0 0 32 32" width={24} height={24} />
              </PayloadIcon>
            </a>
          </Popconfirm>
        )
      },
    },
  ]

  selectChange = (val) => {
    this.setState({
      selectedVendors: val,
    })
  }

  removeVendor = (vendorId) => {
    const { addRemoveVendor, productId } = this.props
    this.setState({ operating: true })
    addRemoveVendor({
      productId,
      vendorId,
    })
  }

  addVendor = () => {
    const { productId, addRemoveVendor } = this.props
    const { selectedVendors } = this.state
    this.setState({ operating: true })
    selectedVendors.forEach((vendorId) =>
      addRemoveVendor({
        productId,
        vendorId,
      }),
    )
  }

  onCancel = () => {
    this.props.onCancel()
    this.setState({
      selectedVendors: [],
    })
  }

  render() {
    const { selectedVendors, operating } = this.state
    const { visible, productVendors, briefVendors } = this.props
    const options = briefVendors.filter((item) =>
      productVendors.every((pItem) => pItem.wholesaleClient.clientId !== item.clientId),
    )

    return (
      <ThemeModal
        okButtonProps={{ style: { display: 'none' } }}
        onCancel={this.onCancel}
        visible={visible}
        title="Edit vendor list"
        cancelText="Close"
      >
        <div>This product is included in the vendor product list for the vendors listed below.</div>
        <StyleDiv>
          <ThemeSelect onChange={this.selectChange} value={selectedVendors} mode="multiple" optionFilterProp="children">
            {options.map((item) => (
              <Select.Option key={item.clientId} value={item.clientId}>
                {item.clientCompanyName}
              </Select.Option>
            ))}
          </ThemeSelect>
          <ThemeButton disabled={selectedVendors.length <= 0 || operating} onClick={this.addVendor}>
            Add vendor
          </ThemeButton>
        </StyleDiv>
        <ThemeTable
          loading={operating}
          pagination={false}
          columns={this.columns}
          dataSource={productVendors}
          showHeader={false}
        />
      </ThemeModal>
    )
  }
}

const StyleDiv = styled.div`
  display: flex;
  padding: 20px 0;
  .ant-select {
    width: 300px;
    margin-right: 20px;
  }
`

export default EditVendorListModal
