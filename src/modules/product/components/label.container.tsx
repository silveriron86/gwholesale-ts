import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteProps } from 'react-router'
import { Row, notification, Input } from 'antd'
import { withTheme } from 'emotion-theming'
import Form, { FormComponentProps } from 'antd/lib/form'
import { Froala } from '~/components/froala'
import { ButtonRow } from '~/components/froala/style'
import { ProductDispatchProps, ProductProps, ProductModule } from '../product.module'
import { GlobalState } from '~/store/reducer'
import { ThemeButton, ThemeModal, ThemeTable } from '~/modules/customers/customers.style'
import { isEmpty } from 'lodash'
import moment from 'moment'
import PageLayout from '~/components/PageLayout'
import CustomLabel from './custom-labels.component'

export type LabelProps = ProductDispatchProps &
  ProductProps &
  RouteProps & {
    theme: Theme
  } & FormComponentProps<LabelFormProps>

interface LabelState {
  content: string
  name: string
  labelModalVisible: Boolean
  currentLabel: any
}

interface LabelFormProps {
  name: String
}

export class LabelComponent extends React.PureComponent<LabelProps, LabelState> {
  constructor(props: any) {
    super(props)
    this.state = {
      content: '',
      name: '',
      labelModalVisible: false,
      currentLabel: null,
    }
  }

  componentDidMount() {
    this.props.getLabelList()
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  updateTemplate = (flag: number) => {
    const { name, content, currentLabel } = this.state
    const { form } = this.props
    if (flag == 1) {
      form.validateFields((err, values: any) => {
        if (!err) {
          this.props.createLabel({ name: values.name, content })
          this.resetData()
        }
      })
    } else if (flag == 2) {
      this.setState({
        labelModalVisible: true,
      })
    } else if (flag == 3) {
      if (isEmpty(currentLabel)) {
        notification['warn']({
          message: 'Warn',
          description: `Please load a label first`,
        })
      } else {
        form.validateFields((err, values) => {
          if (!err) {
            this.props.updateLabel({ name: values.name, content, id: currentLabel.id })
            this.resetData()
          }
        })
      }
    } else if (flag == 4) {
      if (isEmpty(currentLabel)) {
        notification['warn']({
          message: 'Warn',
          description: `Please load a tempalte first`,
        })
      } else {
        this.props.deleteLabel({ id: currentLabel.id })
        this.resetData()
      }
    }
  }

  resetData = () => {
    this.props.form.resetFields()
    this.setState({
      content: '',
      currentLabel: null,
    })
  }

  onHandleModelChange = (value: any) => {
    this.setState({
      content: value,
    })
  }

  changeVisible = () => {
    this.setState({
      labelModalVisible: !this.state.labelModalVisible,
    })
  }

  setTemplateToLocal = (data: any) => {
    this.setState({
      currentLabel: data,
      name: data.name,
      content: data.content,
      labelModalVisible: false,
    })
    this.props.form.setFieldsValue({
      name: data.name,
    })
  }

  render() {
    const {
      labelList,
      form: { getFieldDecorator },
    } = this.props
    const { content, labelModalVisible } = this.state
    const columns: any[] = [
      {
        title: 'NAME',
        dataIndex: 'name',
        align: 'center',
        key: 'name',
      },
      {
        title: 'createdBy',
        dataIndex: 'createdBy',
        align: 'center',
        key: 'createdBy',
        render: (value: any, record: any) => {
          return record.createdBy ? record.createdBy.firstName + ' ' + record.createdBy.lastName : ''
        },
      },
      {
        title: 'createdAt',
        dataIndex: 'createdAt',
        align: 'center',
        key: 'createdAt',
        render: (value: any, record: any) => {
          return moment.utc(value).format('MM/DD/YYYY')
        },
      },
    ]

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Label'}>
        <div style={{ padding: '60px 100px' }}>
          <ThemeModal
            closable={false}
            keyboard={true}
            okText="Close [esc]"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ style: { display: 'none' } }}
            width={700}
            visible={labelModalVisible}
            onCancel={this.changeVisible}
          >
            <ThemeTable
              pagination={{ hideOnSinglePage: true, pageSize: 10 }}
              columns={columns}
              dataSource={labelList}
              rowKey="id"
              onRow={(record, _rowIndex) => {
                return {
                  onClick: (_event) => {
                    this.setTemplateToLocal(record)
                  },
                }
              }}
            />
          </ThemeModal>
          <Row>
            <Form.Item>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: `Label name is required!` }],
              })(<Input placeholder={`Label name`} />)}
            </Form.Item>
          </Row>
          <Row>
            <Froala onHandleModelChange={this.onHandleModelChange} content={content} />
          </Row>
          <Row style={{ paddingTop: '20px' }}>
            <ButtonRow>
              <ThemeButton shape="round" onClick={this.updateTemplate.bind(this, 1)}>
                Save As New
              </ThemeButton>
              <ThemeButton shape="round" style={{ marginLeft: '20px' }} onClick={this.updateTemplate.bind(this, 2)}>
                Load
              </ThemeButton>
              <ThemeButton shape="round" style={{ marginLeft: '20px' }} onClick={this.updateTemplate.bind(this, 3)}>
                Save
              </ThemeButton>
              <ThemeButton shape="round" style={{ marginLeft: '20px' }} onClick={this.updateTemplate.bind(this, 4)}>
                Delete
              </ThemeButton>
            </ButtonRow>
          </Row>
          <Row>
            <CustomLabel />
          </Row>
        </div>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.product

export const Label = withTheme(connect(ProductModule)(mapStateToProps)(Form.create<LabelProps>()(LabelComponent)))
