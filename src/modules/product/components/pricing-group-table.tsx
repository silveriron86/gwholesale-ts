
import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import { Input, InputNumber, Popconfirm, Form } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { ThemeTable } from '~/modules/customers/customers.style';

// const formItemLayout = {
//   labelCol: { span: 2 },
//   wrapperCol: { span: 22 },
// };

const EditableContext = React.createContext({});
/*
class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;

    return (
      <td {...restProps}>

        {editing ? (
          <>
            <Form.Item {...formItemLayout} label={`${dataIndex == 'markup' ? '$' : ''}`} colon={false}>
              {getFieldDecorator(dataIndex, {
                rules: [
                  {
                    required: false,
                    message: `Please Input ${title}!`,
                  },
                ],
                initialValue: record[dataIndex],
              })(this.getInput())}
              <span style={{ marginLeft: 8 }}>{dataIndex === 'margin' ? '%' : ''}</span>
            </Form.Item>
          </>
        ) : (
            children
          )}
      </td>
    );
  };

  render() {
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
  }
}
*/
type FormFields = {

}

type EditableTableProps = FormComponentProps<FormFields> & {
  selected: string
  data: any[]
  editable: boolean
  onChange: Function
}
// type EditableTableState = {
//   editingKey: string
//   data: any
// }

const EditableTable: React.SFC<EditableTableProps> = (props) => {
  const { form, data, editable, onChange } = props
  const [dataSource, setDataSource] = useState<any[]>(data)
  // const [editingKey, setEditingKey] = useState<string>(selectedRow)

  // useEffect(() => {
  //   setEditingKey(selectedRow)
  // }, [selectedRow])

  useEffect(() => {
    setDataSource(data)
  }, [data])

  const onValueChange = (type: string, level: string, val: any) => {
    props.onChange(type, level, val)
  }
  /*
  const columns = [
    {
      title: 'LEVEL',
      dataIndex: 'level',
      width: '10%',
      editable: false,
    },
    {
      title: 'COST',
      dataIndex: 'cost',
      width: '10%',
      editable: false,
    },

    {
      title: 'LOW',
      dataIndex: 'low',
      width: '10%',
      editable: true,
      render: (value: string) => {
        return <span className='left-align-value'>{`$${value}`}</span>
      }
    },
    {
      title: 'HIGH',
      dataIndex: 'high',
      width: '10%',
      editable: true,
      render: (value: string) => {
        return <span className='left-align-value'>{`$${value}`}</span>
      }
    },
    {
      title: 'LAST UPDATED',
      dataIndex: 'updatedDate',
      width: '15%',
    },
    {
      title: 'PRICING FORMULA',
      dataIndex: 'margin',
      colSpan: 2,
      align: 'right',
      width: '15%',
      editable: true,
      render: (value: number, record: any) => {
        const obj = {
          children: value,
          props: {},
        };
        return value != 0 ? `${obj.children}%` : '';
      }
    },
    {
      title: 'Markup',
      colSpan: 0,
      editable: true,
      align: 'left',
      width: '15%',
      dataIndex: 'markup',
      render: (text: string, record: any) => {
        const obj = {
          children: '+ $' + text,
          props: {},
        };

        return <span className='left-align-value'>{obj.children}</span>;
      }
    },
  ];*/

  const columns = [
    {
      title: 'LEVEL',
      dataIndex: 'level',
      width: '10%',
      editable: false,
    },
    {
      title: 'COST',
      dataIndex: 'cost',
      width: '10%',
      editable: false,
    },

    {
      title: 'Price',
      dataIndex: 'customSalesPrice',
      width: '10%',
      editable: true,
      render: (value: number, record: any) => {
        const price = Math.round(record.cost / (1 - record.margin / 100) * 100) / 100
        console.log(price)
        return !editable ? <span className='left-align-value'>{`$${price}`}</span> :
          <>$<InputNumber value={price} onChange={(e) => onValueChange('customSalesPrice', record.level, e)} /></>
      }
    },
    // {
    //   title: 'HIGH',
    //   dataIndex: 'high',
    //   width: '10%',
    //   editable: true,
    //   render: (value: number, record: any) => {
    //     return !editable ? <span className='left-align-value'>{`$${value}`}</span> :
    //       <>$<InputNumber value={value} onChange={(e) => onValueChange('high', record.level, e)} /></>
    //   }
    // },
    {
      title: 'LAST UPDATED',
      dataIndex: 'updatedDate',
      width: '15%',
    },
    {
      title: 'PRICING FORMULA',
      dataIndex: 'margin',
      colSpan: 2,
      align: 'right',
      width: '15%',
      editable: true,
      render: (val: number, record: any) => {
        let value = val
        if (value < 0 || isNaN(value)) {
          value = 0;
        }
        return !editable ? `${value ? value.toFixed(2) : '0'}%` : <><InputNumber value={value ? value.toFixed(2) : ''} min={0} onChange={(e) => onValueChange('margin', record.level, e)} />%</>
      }
    },
    // {
    //   title: 'Markup',
    //   colSpan: 0,
    //   editable: true,
    //   align: 'left',
    //   width: '15%',
    //   dataIndex: 'markup',
    //   render: (value: number, record: any) => {
    //     return !editable ? <span className='left-align-value'>${value}</span> : <>$<InputNumber value={value} min={0} onChange={(e) => onValueChange('markup', record.level, e)} /></>
    //   }
    // },
  ];
  /*
    function isEditing(record: any) {
      return record.level === editingKey;
    }
  
    function save(form: any, key: any) {
      form.validateFields((error: any, row: any) => {
        if (error) {
          return;
        }
        const newData = [...dataSource];
        const index = newData.findIndex(item => key === item.key);
        if (index > -1) {
          const item = newData[index];
          newData.splice(index, 1, {
            ...item,
            ...row,
          });
          setDataSource(newData);
        } else {
          newData.push(row);
          setDataSource(newData);
        }
      });
    }
  
    function getTableColumns() {
      const result = columns.map((col: any) => {
        if (!col.editable) {
          return col;
        }
        return {
          ...col,
          onCell: (record: any) => ({
            record,
            inputType: 'number',//col.dataIndex === 'age' ? 'number' : 'text',
            dataIndex: col.dataIndex,
            title: col.title,
            // editing: isEditing(record),
            editing: editable,
          }),
        };
      });
  
      return result;
    }
  */
  function renderTable() {

    // const components = {
    //   body: {
    //     cell: EditableCell,
    //   },
    // };

    return (
      <ThemeTable
        // components={components}
        dataSource={dataSource}
        // columns={getTableColumns()}
        columns={columns}
        rowClassName="editable-row"
        pagination={false}
      />
    )
  }

  return (
    <EditableContext.Provider value={form}>
      {renderTable()}
    </EditableContext.Provider>
  )
}

const EditableFormTable = Form.create()(EditableTable);
export default EditableFormTable
