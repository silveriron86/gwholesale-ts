import React from 'react'
import { DatePicker, Table, Row, Col, Button, Popover,Tooltip } from 'antd'
import moment from 'moment'
import { Icon as IconSvg } from '~/components'
import { brightGreen } from '~/common'
import { ProductAdjustments } from './product-adjustments.component'
import {
  TipWithChildren,
  TipText,
  TipDatePicker,
  TipDatePickerText,
  TipDatePickerWrapper,
  TableTip,
  ExpandedWrapper,
  ExpandedInner,
  ExpandedClose,
  ReturnLabel
} from './product.component.style'
import {
  formatNumber,
  mathRoundFun,
  baseQtyToRatioQty,
  ratioQtyToBaseQty,
  basePriceToRatioPrice,
  ratioPriceToBasePrice,
  ratioQtyToInventoryQty,
} from '~/common/utils'
import GrubRangePicker, {CUSTOM} from '~/components/GrubRangePicker'

export class ProductActivityExpand extends React.Component<T> {
  state = {}

  onCloseExpand = () => {
    this.props.history.push(`/product/${this.props.match.params.id}/activity`)
  }
  render() {
    const {
      purchaseOrders = [],
      saleOrders = [],
      dateRange,
      pageSize,
      page1,
      page2,
      saleOrderItemTotal,
      purchaseOrderItemTotal,
      options,
      onRangeChange,
    } = this.props
    return (
      <ExpandedWrapper>
        <ExpandedInner>
          <ExpandedClose onClick={this.onCloseExpand}>+</ExpandedClose>
          <TipWithChildren style={{ border: 'none' }}>
            <TipText>Activity</TipText>
            <TipDatePicker>
              <TipDatePickerText style={{ marginRight: '30px' }}>Records Form</TipDatePickerText>
              <GrubRangePicker options={options}
                dateRange={dateRange}
                onChange={onRangeChange}
                pickerProps={{allowClear:false, format:'MM/DD/YYYY', style:{width:300}}}
                selectProps={{style: { width: 300 }}} />
            </TipDatePicker>

            <Popover
              zIndex={600}
              placement="bottomRight"
              content={<ProductAdjustments {...this.props} />}
              trigger="click"
              overlayStyle={{ width: '80%', marginRight: 100 }}
            >
              <Button
                shape="round"
                icon="fullscreen"
                size="default"
                type="primary"
                style={{ backgroundColor: brightGreen, borderColor: brightGreen }}
              >
                SHOW ADJUSTMENTS
              </Button>
            </Popover>
          </TipWithChildren>
          <Row gutter={0}>
            <Col span={12}>
              <TableTip>Purchases</TableTip>
              <Table
                style={{ margin: '0 28px', overflow: 'auto' }}
                size="small"
                pagination={{
                  pageSize: pageSize,
                  current: page1 + 1,
                  total: purchaseOrderItemTotal,
                  onChange: this.props.onChangePage1,
                }}
                dataSource={dateRange.from && dateRange.to ? purchaseOrders : []}
                columns={this.tableColumns()}
              />
            </Col>
            <Col span={12}>
              <TableTip>Sales</TableTip>
              <Table
                style={{ margin: '0 28px', overflow: 'auto' }}
                size="small"
                pagination={{
                  pageSize: pageSize,
                  current: page2 + 1,
                  total: saleOrderItemTotal,
                  onChange: this.props.onChangePage2,
                }}
                dataSource={dateRange.from && dateRange.to ? saleOrders : []}
                columns={this.tableColumns('sales')}
              />
            </Col>
          </Row>
        </ExpandedInner>
      </ExpandedWrapper>
    )
  }

  private tableColumns(which: string = 'purchases') {
    const width = 110
    const widthBig = 400
    const {items} = this.props
    if (which === 'purchases') {
      return [
        {
          title: (
            <Tooltip title="Scheduled delivery date(quoted delivery date is displayed if scheduled delivery date is not available).">
              DELIVERY DATE
            </Tooltip>
          ),
          dataIndex: 'deliveryDate',
          key: 'deliveryDate',
          width: 150,
          render: (value: number) => {
            const year = moment.utc(value).get('year')
            // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
            if (year != 1990) {
              return moment.utc(value).format('MM/DD/YYYY')
            }
          },
        },
        {
          title: 'VENDOR',
          dataIndex: 'vendorName',
          key: 'vendorName',
          width: 250,
          render: (vendorName: any) => {
            return vendorName
          },
        },
        // {
        //   title: 'UNITS ORDERED',
        //   dataIndex: 'quantity',
        //   key: 'quantity',
        //   width,
        // },
        // {
        //   title: 'UNITS CONFIRMED',
        //   dataIndex: 'qtyConfirmed',
        //   key: 'qtyConfirmed',
        //   width,
        // },
        // {
        //   title: 'UNITS RECEIVED',
        //   dataIndex: 'receivedQty',
        //   key: 'receivedQty',
        //   width,
        // },
        {
          title: <Tooltip title="Units Ordered/Commited/Received">UNITS</Tooltip>,
          dataIndex: 'quantity',
          key: 'quantity',
          width: 200,
          render: (quantity: any, record: any) => {
            // if (record.isTotal) {
            return (
              <div style={{ whiteSpace: 'nowrap' }}>{`${mathRoundFun(quantity, 2)}/${mathRoundFun(
                record.qtyConfirmed,
                2,
              )}/${mathRoundFun(record.receivedQty, 2)}`}</div>
            )
          },
        },
        {
          title: 'LOT #',
          dataIndex: 'lotId',
          key: 'lot',
          width: 150,
        },
        {
          title: 'UOM',
          dataIndex: 'UOM',
          key: 'uom',
          width: 100,
        },
      ]
    } else {
      return [
        {
          title: 'FULFILLMENT DATE',
          dataIndex: 'deliveryDate',
          key: 'deliveryDate',
          width: 150,
          render: (value: number) => {
            const year = moment.utc(value).get('year')
            // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
            if (year != 1990) {
              return moment.utc(value).format('MM/DD/YYYY')
            }
          },
        },
        {
          title: 'ACCOUNT',
          dataIndex: 'vendorName',
          key: 'vendorName',
          width: 250,
          render: (vendorName: any) => {
            return vendorName
          },
        },
        {
          title: <Tooltip title="Units Ordered/Commited/Shipped">UNITS</Tooltip>,
          dataIndex: 'quantity',
          key: 'quantity',
          width: 200,
          render: (quantity: any, record: any) => {
            if (!items) {
              return
            }
            if (record.returnToInventory != null && record.returnToInventory != 0) {
              if (record.isTotal) {
                return (
                  <div style={{ whiteSpace: 'nowrap' }}>
                    {`${quantity}/${record.pickedQty}/`}
                    {record.receivedQty}
                    <ReturnLabel>(-{record.returnToInventory})</ReturnLabel>
                  </div>
                )
              } else {
                const uom = record.UOM != null ? record.UOM : items.inventoryUOM
                const ordered = ratioQtyToBaseQty(uom, quantity, items)
                const picked = ratioQtyToBaseQty(uom, record.pickedQty, items)
                return (
                  <div style={{ whiteSpace: 'nowrap' }}>
                    {`${ordered}/${picked}/`}
                    {record.wholesaleOrderItemStatus == 'SHIPPED' ? picked : 0}
                    <ReturnLabel>(-{record.returnToInventory})</ReturnLabel>
                  </div>
                )
              }
            } else {
              if (record.isTotal) {
                return (
                  <div style={{ whiteSpace: 'nowrap' }}>{`${quantity}/${record.pickedQty}/${record.receivedQty}`}</div>
                )
              } else {
                const uom = record.UOM != null ? record.UOM : items
                const ordered = ratioQtyToBaseQty(uom, quantity, items)
                const picked = ratioQtyToBaseQty(uom, record.pickedQty, items)
                return (
                  <div style={{ whiteSpace: 'nowrap' }}>{`${ordered}/${picked}/${
                    record.wholesaleOrderItemStatus == 'SHIPPED' ? picked : 0
                  }`}</div>
                )
              }
            }
          },
        },
        // {
        //   title: 'UNITS COMMITED',
        //   dataIndex: 'quantity',
        //   key: 'quantity',
        //   width,
        // },
        // {
        //   title: 'UNITS SHIPPED',
        //   dataIndex: 'qtyConfirmed',
        //   key: 'qtyConfirmed',
        //   width,
        //   render: (value: any, record: any) => {
        //     if (value) {
        //       return <div style={{ whiteSpace: 'nowrap' }}>{value}</div>
        //     } else {
        //       return <div style={{ whiteSpace: 'nowrap' }}>No lot</div>
        //     }
        //   },
        // },
        // {
        //   title: 'UNITS RETURNED',
        //   dataIndex: 'returnedQty',
        //   key: 'returnedQty',
        //   width,
        // },
        {
          title: 'LOT #',
          dataIndex: 'lotId',
          key: 'lot',
          width: 150,
          render: (value: any, record: any) => {
            if (value) {
              return <div style={{ whiteSpace: 'nowrap' }}>{value}</div>
            } else {
              return <div style={{ whiteSpace: 'nowrap' }}>No lot</div>
            }
          },
        },
        {
          title: 'UOM',
          dataIndex: 'UOM',
          key: 'uom',
          width: 100,
          render: (value: any, record: any) => {
            if (record.isTotal) {
              return <div style={{ whiteSpace: 'nowrap' }}>{record.UOM}</div>
            } else {
              return <div style={{ whiteSpace: 'nowrap' }}>{items.inventoryUOM}</div>
            }
          },
        },
        {
          title: 'PRICE',
          dataIndex: 'price',
          key: 'price',
          width: 150,
          render: (value: any, record: any) => {
            if (value && items) {
              if (record.pricingUOM) {
                return `$${basePriceToRatioPrice(record.pricingUOM, value, items, 2)}/${record.pricingUOM}`
              } else {
                return `$${mathRoundFun(value, 2)}/${items ? items.inventoryUOM : ''}`
              }
            } else {
              return ''
            }
          },
        }
      ]
    }
  }

  private renderDateEditor(which: string, onChange: any, defaultValue: any) {
    const calendarIcon = <IconSvg type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    return (
      <TipDatePickerWrapper>
        <DatePicker
          placeholder={dateFormat}
          format={dateFormat}
          // value={moment(this.props.dateRange[which], 'MM/DD/YYYY')}
          suffixIcon={calendarIcon}
          defaultValue={defaultValue}
          onChange={onChange}
          allowClear={false}
        />
      </TipDatePickerWrapper>
    )
  }
}
