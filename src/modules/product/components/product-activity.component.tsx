import React from 'react'
import { DatePicker, Table, Row, Col, Tooltip } from 'antd'
import moment from 'moment'
import { Route } from 'react-router-dom'
import { Icon as IconSvg } from '~/components'
import {
  Wrapper,
  TipWithChildren,
  TipText,
  TipDatePicker,
  TipAction,
  TipActionText,
  TipDatePickerText,
  TipDatePickerWrapper,
  TableTip,
  ActivityTables,
  ReturnLabel,
} from './product.component.style'
import { ProductActivityExpand } from './product-activity-expand.component'
import _, { cloneDeep } from 'lodash'
import {
  formatNumber,
  mathRoundFun,
  baseQtyToRatioQty,
  ratioQtyToBaseQty,
  basePriceToRatioPrice,
  ratioPriceToBasePrice,
  ratioQtyToInventoryQty,
  formatOrderStatus,
} from '~/common/utils'
import jQuery from 'jquery'
import { ProductDetailsForm } from './product-details-form-details.component'
import { ItemStockTable } from '~/modules/inventory/components'
import GrubRangePicker, {OptionType, CUSTOM} from '~/components/GrubRangePicker'
import { PanelItem, PanelItemContent, PanelItemLabel } from '../product.style'
import { textCenter } from '~/modules/customers/customers.style'
import { ActivityItemAvailableLabel } from './utils'

const options: OptionType[] = [
  {
    label: 'Past 7 Days',
    value: [moment().subtract(7, 'days'), moment()],
  },
  {
    label: 'Past 30 Days',
    value: [moment().subtract(30, 'days'), moment()],
  },
  {
    label: 'Yesterday',
    value: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  },
  {
    label: 'Today',
    value:[moment(),moment(),]
  },
  {
    label: 'Tomorrow',
    value:[moment().add(1, 'days'), moment().add(1, 'days'),]
  },
  {
    label: 'Today and Tomorrow',
    value:[moment(), moment().add(1, 'days'),]
  },
  {
    label: 'Next 7 Days',
    value:[moment(), moment().add(7, 'days'),]
  },
  {
    label: 'Next 30 Days',
    value:[moment(),moment().add(30, 'days'),]
  },
  {
    label: 'Past 30 Days and Next 30 Days',
    value:[moment().subtract(30, 'days'),moment().add(30, 'days'),]
  },
]

export class ProductActivity extends React.Component<T> {
  state: any
  constructor(props) {
    super(props)
    const id = this.props.match.params.id
    const sessionSearch = localStorage.getItem(`PRODUCT_ACTIVITIES_${id}_SEARCH`)
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search)
    const startDate = urlParams.get('datestart')
    const endDate = urlParams.get('dateend')
    const page1 = urlParams.get('page1')
    const page2 = urlParams.get('page2')
    const from = startDate
      ? moment(startDate, 'MMDDYY').format('MM/DD/YYYY')
      : moment()
          .subtract(3, 'month')
          .format('MM/DD/YYYY')
    const to = endDate
      ? moment(endDate, 'MMDDYY').format('MM/DD/YYYY')
      : moment()
          .add(1, 'month')
          .format('MM/DD/YYYY')
    this.state = {
      page1: page1 ? parseInt(page1, 10) : 0,
      page2: page2 ? parseInt(page2, 10) : 0,
      pageSize: 10,
      dateRange: {
        from,
        to,
      },
    }
  }

  componentWillReceiveProps(nextProps: any, nextState: any) {
    if (
      (this.props.saleOrders && nextProps.saleOrders.length > 0) ||
      (this.props.purchaseOrders && nextProps.purchaseOrders.length > 0)
    ) {
      setTimeout(() => {
        this.handleResize()
      }, 1000)
    }
    // const prevItem = this.props.items
    // const nextItem = nextProps.items
    // if (JSON.stringify(prevItem) !== JSON.stringify(nextItem)) {
    //   this.getOrders()
    // }
  }

  componentDidMount() {
    this.getOrders()

    const urlParams = new URLSearchParams(this.props.location.search)
    const id = this.props.match.params.id
    const sessionSearch = localStorage.getItem(`PRODUCT_ACTIVITIES_${id}_SEARCH`)
    if (sessionSearch) {
      setTimeout(() => {
        this.updateURL()
      }, 100)
    }
  }

  getOrders = () => {
    const { dateRange, pageSize, page1, page2 } = this.state
    const { match } = this.props
    Promise.all([
      this.props.getSaleOrders({
        id: match.params.id,
        dateRange: dateRange,
        pageSize,
        currentPage: page2,
      }),
      this.props.getPurchaseOrders({
        id: match.params.id,
        dateRange: dateRange,
        pageSize,
        currentPage: page1,
      }),
      this.props.getOrderItemSummaryByItemId({ ...dateRange, id: this.props.match.params.id }),
      this.props.getAdjustments({ ...dateRange, id: this.props.match.params.id }),
      this.props.getItemInventoryQtyAtTwoDates({ ...dateRange, id: this.props.match.params.id })
    ])

    window.addEventListener('resize', this.handleResize)
  }

  getSalesOrderItems = () => {
    const { dateRange, pageSize, page2 } = this.state
    const { match } = this.props
    this.props.getSaleOrders({
      id: match.params.id,
      dateRange: dateRange,
      pageSize,
      currentPage: page2,
    })

    window.addEventListener('resize', this.handleResize)
  }

  getPurchaseOrderItems = () => {
    const { dateRange, pageSize, page1 } = this.state
    const { match } = this.props
    this.props.getPurchaseOrders({
      id: match.params.id,
      dateRange: dateRange,
      pageSize,
      currentPage: page1,
    })

    window.addEventListener('resize', this.handleResize)
  }

  handleResize = () => {
    const purchaseFooter = jQuery('.purchase-activity-table .ant-table-footer')[0]
    const purchaseFooterbody = jQuery('.purchase-activity-table .ant-table-footer .ant-table-tbody')[0]
    if (purchaseFooterbody) {
      const height = jQuery(purchaseFooterbody).outerHeight()
      console.log('css for purchase')
      jQuery(purchaseFooter).css('visibility', `visible`)
      jQuery(purchaseFooter).css('bottom', `-${height}px`)
      const paginator = jQuery('.purchase-activity-table ul.ant-pagination')[0]
      if (paginator) {
        jQuery(paginator).css('margin-top', `${height + 16}px`)
      }
    }

    const salesFooter = jQuery('.sales-activity-table .ant-table-footer')[0]
    const salesFooterbody = jQuery('.sales-activity-table .ant-table-footer .ant-table-tbody')[0]
    if (salesFooterbody) {
      const salesHeight = jQuery(salesFooterbody).outerHeight()
      jQuery(salesFooter).css('visibility', `visible`)
      jQuery(salesFooter).css('bottom', `-${salesHeight}px`)
      const salesPaginator = jQuery('.sales-activity-table ul.ant-pagination')[0]
      if (salesPaginator) {
        jQuery(salesPaginator).css('margin-top', `${salesHeight + 16}px`)
      }
    }
  }
  onExpand = () => {
    this.props.history.push(`/product/${this.props.match.params.id}/activity/expand`)
  }

  onDateStartChange = (_: any, dateString: string) => {
    const { dateRange } = this.state
    const newObj = { ...dateRange, from: dateString }
    this.setState({ dateRange: newObj, page1: 0, page2: 0 }, () => {
      this._loadData()
      this.updateURL()
    })
  }

  onDateEndChange = (_: any, dateString: string) => {
    const { dateRange } = this.state
    const newObj = { ...dateRange, to: dateString }
    this.setState({ dateRange: newObj, page1: 0, page2: 0 }, () => {
      this._loadData()
      this.updateURL()
    })
  }

  onRangeChange = (arr:any) => {
    let [from, to] = arr
    if(from){
      from = from.format('MM/DD/YYYY')
    }
    if(to){
      to = to.format('MM/DD/YYYY')
    }
    this.setState({ dateRange: {from, to}, page1: 0, page2: 0 }, () => {
      this._loadData()
      this.updateURL()
      this.props.getItemInventoryQtyAtTwoDates({ from, to, id: this.props.match.params.id })
    })
  }

  updateURL = () => {
    var url = new URL(window.location.href)
    var queryParams = url.searchParams
    const { dateRange, page1, page2 } = this.state
    queryParams.set('datestart', moment(dateRange.from).format('MMDDYY'))
    queryParams.set('dateend', moment(dateRange.to).format('MMDDYY'))
    if (page1) {
      queryParams.set('page1', page1)
    }
    if (page2) {
      queryParams.set('page2', page2)
    }
    url.search = queryParams.toString()
    const id = this.props.match.params.id
    localStorage.setItem(`PRODUCT_ACTIVITIES_${id}_SEARCH`, url.search)
    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString())
  }

  onChangePage1 = (page, pageSize) => {
    this.setState(
      {
        page1: page - 1,
      },
      () => {
        this.updateURL()
        this.getPurchaseOrderItems()
      },
    )
  }

  onChangePage2 = (page, pageSize) => {
    this.setState(
      {
        page2: page - 1,
      },
      () => {
        this.updateURL()
        this.getSalesOrderItems()
      },
    )
  }

  _loadData = () => {
    const { dateRange, pageSize, page1, page2 } = this.state
    const { getAdjustments, getPurchaseOrders, getSaleOrders, match } = this.props
    if (dateRange.from && dateRange.to) {
      getAdjustments({ ...dateRange, id: match.params.id })
      getPurchaseOrders({
        id: match.params.id,
        dateRange: dateRange,
        pageSize,
        currentPage: page1,
      })
      getSaleOrders({
        id: match.params.id,
        dateRange: dateRange,
        pageSize,
        currentPage: page2,
      })
      this.props.getOrderItemSummaryByItemId({ ...dateRange, id: match.params.id })
    }
  }

  renderTableFooter = (data: any, summaryData: any, type: string) => {
    if (data.length > 0 && summaryData.length == 1) {
      return () => (
        <Table columns={this.tableColumns(type)} dataSource={summaryData} pagination={false} showHeader={true} />
      )
    } else {
      return undefined
    }
  }

  getSaleOrdersSummary = (saleOrders: any) => {
    const { items } = this.props
    var quantityTotal = 0.0,
      pickedQtyTotal = 0.0,
      receivedTotal = 0.0
    if (items) {
      saleOrders.map((order: any) => {
        // if (order.lotId) {
        quantityTotal += ratioQtyToInventoryQty(order.UOM, order.quantity, items, 2)
        // }
        if (order.wholesaleOrderItemStatus == 'SHIPPED') {
          receivedTotal += ratioQtyToInventoryQty(order.UOM, order.pickedQty, items, 2)
        }
        pickedQtyTotal += ratioQtyToInventoryQty(order.UOM, order.pickedQty, items, 2)
      })
      return [
        {
          wholesaleOrderItemStatus: 'SHIPPED',
          quantity: mathRoundFun(quantityTotal, 2),
          pickedQty: mathRoundFun(pickedQtyTotal, 2),
          receivedQty: mathRoundFun(receivedTotal, 2),
          UOM: saleOrders && saleOrders.length > 0 ? items.inventoryUOM : '',
          isTotal: true,
        },
      ]
    } else {
      return [
        {
          wholesaleOrderItemStatus: 'SHIPPED',
          quantity: mathRoundFun(quantityTotal, 2),
          pickedQty: mathRoundFun(pickedQtyTotal, 2),
          receivedQty: mathRoundFun(receivedTotal, 2),
          UOM: '',
          isTotal: true,
        },
      ]
    }
  }

  onRowClick = (type: string, record: any) => {
    if(!record.wholesaleOrderId) return
    if (type == 'purchaseOrder') {
      this.props.history.push(`/order/${record.wholesaleOrderId}/purchase-cart`)
    } else if (record.vendorName && record.isCashCustomer) {
      this.props.history.push(`/cash-sales/${record.wholesaleOrderId}`)
    } else {
      this.props.history.push(`/sales-order/${record.wholesaleOrderId}`)
    }
  }

  getPurchaseSummaryData = (orderItems: any) => {
    let quantityTotal: number = 0,
      confirmedTotal: number = 0,
      receivedTotal: number = 0
    let uom
    const { items } = this.props
    if (items) {
      orderItems.map((orderItem: any) => {
        let unitUom = orderItem.overrideUOM ? orderItem.overrideUOM : items.inventoryUOM
        quantityTotal += baseQtyToRatioQty(unitUom, orderItem.quantity, items)
        confirmedTotal += baseQtyToRatioQty(unitUom, orderItem.qtyConfirmed, items)
        receivedTotal += baseQtyToRatioQty(unitUom, orderItem.receivedQty, items)
        uom = items.inventoryUOM
      })
    }
    return [
      {
        quantity: quantityTotal,
        qtyConfirmed: confirmedTotal,
        receivedQty: receivedTotal,
        UOM: uom,
        isTotal: true,
      },
    ]
  }

  render() {
    const {
      purchaseOrders = [],
      saleOrders = [],
      saleOrderItemTotal,
      purchaseOrderItemTotal,
      itemSummary,
      items,
      adjustments,
      fromAndToInventoryQtys
    } = this.props
    const { dateRange, pageSize, page1, page2 } = this.state

    let inAdjustments: any[] = [], outAdjustments: any[] = []
    let inAdjustmentTotal = 0, outAdjustmentTotal = 0
    adjustments.forEach((el: any) => {
      if(el.quantity > 0) {
        inAdjustments.push({
          deliveryDate: el.createdDate,
          vendorName: el.reason,
          quantity: el.quantity,
          UOM: el.uom,
          lotId: el.orderItem?.lotId,
          isAdjustment: true
        })
        inAdjustmentTotal += el.quantity;
      } else {
        outAdjustments.push(
          {
            deliveryDate: el.createdDate,
            vendorName: el.reason,
            quantity: Math.abs(el.quantity),
            UOM: el.uom,
            lotId: el.orderItem?.lotId,
            isAdjustment: true
          }
        )
        outAdjustmentTotal += Math.abs(el.quantity);
      }
    });
    const prevIn = page2 > 0 ? Array(page1 * pageSize).fill(0) : [];
    const productsIn = [...prevIn, ...purchaseOrders, ...inAdjustments]
    const prevOut = page2 > 0 ? Array(page2 * pageSize).fill(0) : [];
    const productsOut = [...prevOut,...saleOrders, ...outAdjustments]

    let purchaseSummaryData = [
      {
        quantity: mathRoundFun(itemSummary?.totalPurchaseOrdered ? itemSummary.totalPurchaseOrdered : 0, 2) + mathRoundFun(inAdjustmentTotal, 2),
        qtyConfirmed: mathRoundFun(itemSummary?.totalConfirmed ? itemSummary.totalConfirmed : 0, 2) + mathRoundFun(inAdjustmentTotal, 2),
        receivedQty: mathRoundFun(itemSummary?.totalReceived ? itemSummary.totalReceived : 0, 2) + mathRoundFun(inAdjustmentTotal, 2),
        wholesaleItem: items ? items : null,
        isTotal: true,
      },
    ]

    let saleSummaryData = [
      {
        wholesaleOrderItemStatus: 'SHIPPED',
        quantity: mathRoundFun(itemSummary?.totalSalesOrdered ? itemSummary.totalSalesOrdered : 0, 2) + mathRoundFun(outAdjustmentTotal, 2),
        pickedQty: mathRoundFun(itemSummary?.totalCommitted ? itemSummary.totalCommitted : 0, 2) + mathRoundFun(outAdjustmentTotal, 2),
        receivedQty: mathRoundFun(itemSummary?.totalShipped ? itemSummary.totalShipped : 0, 2) + mathRoundFun(outAdjustmentTotal, 2),
        UOM: items ? items.inventoryUOM : '',
        isTotal: true,
        returnToInventory: mathRoundFun(itemSummary?.totalReturned || 0, 2)
      },
    ]
    if (purchaseSummaryData.length > 1 || saleSummaryData.length > 1) {
      this.handleResize()
    }

    return (
      <Wrapper>
        <TipWithChildren>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <TipText>Activity</TipText>
            <div style={{ marginLeft: 50 }}>
              <ProductDetailsForm isActivity={true} {...this.props} />
            </div>
          </div>
          <TipDatePicker>
          <TipDatePickerText style={{ marginRight: '30px' }}>Records Form</TipDatePickerText>
          <GrubRangePicker options={options}
            dateRange={dateRange}
            onChange={this.onRangeChange}
            pickerProps={{allowClear:false, format:'MM/DD/YYYY', style:{width:300}}}
            selectProps={{style: { width: 300 }}} />
          </TipDatePicker>
          <TipAction onClick={this.onExpand}>
            <IconSvg type="expand2" viewBox="0 0 20 20" width={24} height={24} />
            <TipActionText>EXPAND TABLE</TipActionText>
          </TipAction>
        </TipWithChildren>
        <TipWithChildren style={{justifyContent: 'center', borderTop: 'none', paddingTop: 0}}>
          { dateRange.from != dateRange.to &&             
            <ActivityItemAvailableLabel date={dateRange.from} uom={items.inventoryUOM} availableToSell={fromAndToInventoryQtys?.from?.availableToSell} unitsOnHand={fromAndToInventoryQtys?.from?.onHandQty}></ActivityItemAvailableLabel>
          }
          <ActivityItemAvailableLabel date={dateRange.to} uom={items.inventoryUOM} availableToSell={fromAndToInventoryQtys?.to.availableToSell} unitsOnHand={fromAndToInventoryQtys?.to.onHandQty}></ActivityItemAvailableLabel>
        </TipWithChildren>
        <ActivityTables gutter={0}>
          <Col span={24}>
            <TableTip className="purchase">Purchases</TableTip>
            <Table
              pagination={{
                pageSize: pageSize,
                current: this.state.page1 + 1,
                total: purchaseOrderItemTotal + inAdjustments.length,
                onChange: this.onChangePage1,
              }}
              style={{ margin: '0 28px', overflow: 'auto' }}
              size="small"
              className="purchase-activity-table"
              onRow={(record, _rowIndex) => {
                return {
                  onClick: (_event) => {
                    this.onRowClick('purchaseOrder', record)
                  },
                }
              }}
              dataSource={dateRange.from && dateRange.to ? productsIn : []}
              columns={this.tableColumns('purchases')}
              footer={this.renderTableFooter(productsIn, purchaseSummaryData, 'purchases')}
            />
          </Col>
          <Col span={24}>
            <TableTip>Sales</TableTip>
            <Table
              style={{ margin: '0 28px', overflow: 'auto' }}
              size="small"
              pagination={{
                pageSize: pageSize,
                current: this.state.page2 + 1,
                total: saleOrderItemTotal + outAdjustments.length,
                onChange: this.onChangePage2,
              }}
              className="sales-activity-table"
              onRow={(record, _rowIndex) => {
                return {
                  onClick: (_event) => {
                    this.onRowClick('salesOrder', record)
                  },
                }
              }}
              dataSource={dateRange.from && dateRange.to ? productsOut : []}
              columns={this.tableColumns('sales')}
              footer={this.renderTableFooter(productsOut, saleSummaryData, 'sales')}
            />
          </Col>
        </ActivityTables>
        <Route
          path={`/product/${this.props.match.params.id}/activity/expand`}
          render={() => (
            <ProductActivityExpand
              {...this.props}
              {...this.state}
              options={options}
              onRangeChange={this.onRangeChange}
              onDateEndChange={this.onDateEndChange}
              onDateStartChange={this.onDateStartChange}
              onChangePage1={this.onChangePage1}
              onChangePage2={this.onChangePage2}
            />
          )}
        />
      </Wrapper>
    )
  }

  private tableColumns = (which: string = 'purchases') => {
    // const width = 120
    const widthBig = 280
    const { items } = this.props
    if (which === 'purchases') {
      return [
        {
          title: (
            <Tooltip title="Scheduled delivery date(quoted delivery date is displayed if scheduled delivery date is not available).">
              DELIVERY DATE
            </Tooltip>
          ),
          dataIndex: 'deliveryDate',
          key: 'deliveryDate',
          width: 100,
          render: (value: number, record: any) => {
            if (value) {
              // Date value should be "shceduled delivery date" if "scheduled delivery date" has a value.
              const year = moment.utc(value).get('year')
              // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
              if (year != 1990) {
                return <div style={{ whiteSpace: 'nowrap' }}>{moment.utc(value).format('MM/DD/YY')}</div>
              }
            } else if (record.quotedDate) {
              // If "scheduled delivery date" does not have a value, display "quoted delivery date"
              const year = moment.utc(record.quotedDate).get('year')
              if (year != 1990) {
                return <div style={{ whiteSpace: 'nowrap' }}>{moment.utc(record.quotedDate).format('MM/DD/YY')}</div>
              }
            }
            return null
          },
        },
        {
          title: 'PO #',
          dataIndex: 'wholesaleOrderId',
          key: 'po#',
          width: 80,
        },
        {
          title: 'STATUS',
          dataIndex: 'wholesaleOrderItemStatus',
          width: 90,
        },
        {
          title: 'VENDOR',
          dataIndex: 'vendorName',
          key: 'vendorName',
          width: widthBig,
          render: (vendorName: any) => {
            return vendorName
          },
        },
        {
          title: <Tooltip title="Units Ordered/Commited/Received">UNITS</Tooltip>,
          dataIndex: 'quantity',
          key: 'quantity',
          width: 160,
          render: (quantity: any, record: any) => {
            // if (record.isTotal) {
            if(!!record.isAdjustment) {
              return `${quantity}/${quantity}/${quantity}`
            }
            return (
              <div style={{ whiteSpace: 'nowrap', position: 'absolute', marginTop: -11 }}>{`${mathRoundFun(quantity, 2)}/${mathRoundFun(
                record.qtyConfirmed,
                2,
              )}/${mathRoundFun(record.receivedQty, 2)}`}</div>
            )
            // }else{
            //   if(record.wholesaleItem != null){
            //     const unitUom = record.overrideUOM ? record.overrideUOM : record.wholesaleItem.inventoryUOM
            //     return `${baseQtyToRatioQty(unitUom, quantity, record.wholesaleItem)}/${baseQtyToRatioQty(unitUom, record.qtyConfirmed, record.wholesaleItem)}/${baseQtyToRatioQty(unitUom, record.receivedQty, record.wholesaleItem)}
            //     `
            //   }
            // }
          },
        },
        // {
        //   title: 'UNITS CONFIRMED',
        //   dataIndex: 'qtyConfirmed',
        //   key: 'qtyConfirmed',
        //   width,
        // },
        // {
        //   title: 'UNITS RECEIVED',
        //   dataIndex: 'receivedQty',
        //   key: 'receivedQty',
        //   width,
        // },
        {
          title: 'UOM',
          dataIndex: 'UOM',
          key: 'uom',
          width: 90,
          render: (value: any, record: any) => {
            if (items) {
              return <div style={{ whiteSpace: 'nowrap' }}>{items.inventoryUOM}</div>
            }
          },
        },
        {
          title: 'LOT #',
          dataIndex: 'lotId',
          key: 'lotId',
          width: 100,
          render: (value: any) => {
            return <div style={{ whiteSpace: 'nowrap' }}>{value}</div>
          },
        },
        {
          title: 'VENDOR REF #',
          dataIndex: 'po',
          width: 80,
        },
        {
          title: 'COST',
          dataIndex: 'cost',
          key: 'cost',
          width: 130,
          render: (value: any, record: any) => {
            if (value == null) {
              //summary line
              return
            }
            if (items == null) {
              return
            }
            value = value ? value : 0
            value = value + record.perAllocateCost
            if (record.pricingUOM) {
              return `$${formatNumber(basePriceToRatioPrice(record.pricingUOM, value, items, 2), 2)}/${record.pricingUOM}`
            } else {
              return `$${formatNumber(mathRoundFun(value, 2), 2)}/${items ? items.inventoryUOM : ''}`
            }
          },
        },
      ]
    } else {
      return [
        {
          title: <Tooltip title="Target Fulfillment Date">FULFILLMENT DATE</Tooltip>,
          dataIndex: 'deliveryDate',
          key: 'deliveryDate',
          width: 100,
          render: (value: number) => {
            if (value) {
              const year = moment.utc(value).get('year')
              // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
              if (year != 1990) {
                return moment.utc(value).format('MM/DD/YY')
              }
            }
          },
        },
        {
          title: 'SO #',
          dataIndex: 'wholesaleOrderId',
          key: 'so#',
          width: 80,
        },
        {
          title: 'STATUS',
          dataIndex: 'wholesaleOrderItemStatus',
          width: 90,
          render: (status: any, record: any) => {
            if (record.isTotal) {
              return '';
            }
            return formatOrderStatus(status).toUpperCase()
          }
        },
        {
          title: 'ACCOUNT',
          dataIndex: 'vendorName',
          key: 'vendorName',
          width: widthBig,
          render: (vendorName: any) => {
            return vendorName
          },
        },
        {
          title: <Tooltip title="Units Ordered/Commited/Shipped">UNITS</Tooltip>,
          dataIndex: 'quantity',
          key: 'quantity',
          width: 160,
          render: (quantity: any, record: any) => {
            if (!items) {
              return
            }
            if(!!record.isAdjustment) {
              return `${quantity}/${quantity}/${quantity}`
            }
            if (record.returnToInventory != null && record.returnToInventory != 0) {
              if (record.isTotal) {
                return (
                  <div style={{ whiteSpace: 'nowrap', position: 'absolute', marginTop: -11 }}>
                    {`${quantity}/${record.pickedQty}/`}
                    {record.receivedQty}
                    <ReturnLabel>(-{record.returnToInventory})</ReturnLabel>
                  </div>
                )
              } else {
                const uom = record.UOM != null ? record.UOM : items.inventoryUOM
                const ordered = ratioQtyToBaseQty(uom, quantity, items)
                const picked = ratioQtyToBaseQty(uom, record.pickedQty, items)
                return (
                  <div style={{ whiteSpace: 'nowrap' }}>
                    {`${ordered}/${picked}/`}
                    {record.wholesaleOrderItemStatus == 'SHIPPED' ? picked : 0}
                    <ReturnLabel>(-{mathRoundFun(record.returnToInventory, 2)})</ReturnLabel>
                  </div>
                )
              }
            } else {
              if (record.isTotal) {
                return (
                  <div style={{ whiteSpace: 'nowrap' }}>{`${quantity}/${record.pickedQty}/${record.receivedQty}`}</div>
                )
              } else {
                const uom = record.UOM != null ? record.UOM : items
                const ordered = ratioQtyToBaseQty(uom, quantity, items)
                const picked = ratioQtyToBaseQty(uom, record.pickedQty, items)
                return (
                  <div style={{ whiteSpace: 'nowrap' }}>{`${ordered}/${picked}/${
                    record.wholesaleOrderItemStatus == 'SHIPPED' ? picked : 0
                  }`}</div>
                )
              }
            }
          },
        },
        // {
        //   title: 'UOM',
        //   dataIndex: 'UOM',
        //   key: 'uom',
        //   width: 80,
        // },
        // {
        //   title: 'UNITS COMMITED',
        //   dataIndex: 'receivedQty',
        //   key: 'receivedQty',
        //   width,
        //   render: (quantity: any, record: any) => {
        //     if (record.lotId) {
        //       return record.quantity
        //     }
        //     if (record.isTotal) {
        //       return record.receivedQty
        //     }
        //   },
        // },
        // {
        //   title: 'UNITS SHIPPED',
        //   dataIndex: 'pickedQty',
        //   key: 'pickedQty',
        //   width,
        //   render: (value: any, record: any) => {
        //     if (record.returnToInventory != null && record.returnToInventory != 0) {
        //       return (
        //         <div>
        //           {record.wholesaleOrderItemStatus == 'SHIPPED' ? value : 0}
        //           <ReturnLabel>(-{record.returnToInventory})</ReturnLabel>
        //         </div>
        //       )
        //     } else {
        //       return record.wholesaleOrderItemStatus == 'SHIPPED' ? value : 0
        //     }
        //   },
        // },
        // {
        //   title: 'UNITS RETURNED',
        //   dataIndex: 'returnedQty',
        //   key: 'returnedQty',
        //   width,
        // },
        {
          title: 'UOM',
          dataIndex: 'UOM',
          key: 'uom',
          width: 90,
          render: (value: any, record: any) => {
            if (record.isTotal) {
              return <div style={{ whiteSpace: 'nowrap' }}>{record.UOM}</div>
            } else {
              return <div style={{ whiteSpace: 'nowrap' }}>{items.inventoryUOM}</div>
            }
          },
        },
        {
          title: 'LOT #',
          dataIndex: 'lotId',
          key: 'lotId',
          width: 100,
          render: (value: any, record: any) => {
            if (value || record.isTotal) {
              return <div style={{ whiteSpace: 'nowrap' }}>{value}</div>
            } else {
              return <div style={{ whiteSpace: 'nowrap' }}>No lot</div>
            }
          },
        },
        {
          title: 'VENDOR REF #',
          dataIndex: 'reference',
          width: 80,
        },
        {
          title: 'PRICE',
          dataIndex: 'price',
          key: 'price',
          width: 130,
          render: (value: any, record: any) => {
            if (value && items) {
              if (record.pricingUOM) {
                return `$${formatNumber(basePriceToRatioPrice(record.pricingUOM, value, items, 2), 2)}/${record.pricingUOM}`
              } else {
                return `$${formatNumber(mathRoundFun(value, 2), 2)}/${items ? items.inventoryUOM : ''}`
              }
            } else {
              return ''
            }
          },
        },
      ]
    }
  }

  private renderDateEditor(which: string, onChange: any, defaultValue: any) {
    const calendarIcon = <IconSvg type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    return (
      <TipDatePickerWrapper>
        <DatePicker
          placeholder={dateFormat}
          format={dateFormat}
          suffixIcon={calendarIcon}
          // value={moment(this.state.dateRange[which], 'MM/DD/YYYY')}
          defaultValue={defaultValue}
          onChange={onChange}
          allowClear={false}
        />
      </TipDatePickerWrapper>
    )
  }
}
