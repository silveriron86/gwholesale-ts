import React from 'react'
import { connect } from 'redux-epics-decorator'
import {
  PopoverWrapper,
  ThemeTabs,
  ThemeIconButton,
  FlexCenter,
  // Incoming,
  ThemeModal,
  ThemeTable,
} from '~/modules/customers/customers.style'
import { Tabs, Icon, Tooltip } from 'antd'
import moment from 'moment'
import { GlobalState } from '~/store/reducer'
import { ProductModule, ProductProps } from '../product.module'
import SearchInput from '~/components/Table/search-input'
import { ChatAvatar, avatarImg } from '~/modules/customers/accounts/chat/_styles'
import { formatNumber, getFileUrl, combineUomForInventory, inventoryQtyToRatioQty, mathRoundFun } from '~/common/utils'
import productModal from '~/modules/customers/sales/cart/modals/product-modal'
import { ThumbnailImage } from '~/modules/inventory/components/inventory-table.style'
import { isArray } from 'lodash'

type ProductAdjustmentLotProps = {
  getOrderItemLots: Function
  onSelect: Function
  receivedLots: []
  searchStr: string
  onSearch: Function
} & ProductProps
export class LotTableContainer extends React.PureComponent<ProductAdjustmentLotProps> {
  state: any
  constructor(props: ProductAdjustmentLotProps) {
    super(props)
    this.state = {
      visibleOrderModal: false,
      currentItem: null,
    }
  }

  handleOrderModalOk = () => {
    this.setState({
      visibleOrderModal: false,
    })
  }

  componentDidMount() {
    this.props.getOrderItemLots()
  }

  componentWillReceiveProps(nextProps: ProductAdjustmentLotProps) {
    if (this.props.creating !== nextProps.creating && nextProps.creating === false) {
      this.props.getOrderItemLots()
    }
  }

  render() {
    const { receivedLots, searchStr } = this.props
    const filteredLots = receivedLots.filter((row) => {
      return (
        (row.wholesaleItem!.wholesaleCategory && row.wholesaleItem!.wholesaleCategory.name.indexOf(searchStr) >= 0) ||
        (row.wholesaleItem!.variety && row.wholesaleItem!.variety.toLowerCase().indexOf(searchStr) >= 0) ||
        (row.wholesaleItem!.sku && row.wholesaleItem!.sku.toLowerCase().indexOf(searchStr) >= 0) ||
        (row.lotId && row.lotId.indexOf(searchStr) >= 0)
      )
    })
    const columns: any[] = [
      {
        title: 'IMAGE',
        dataIndex: 'wholesaleItem.cover',
        align: 'center',
        width: 50,
        render: (src: string) => {
          if (src) {
            return <ThumbnailImage src={getFileUrl(src, false)} />
          } else {
            return ''
          }
        },
      },
      {
        title: 'ARRIVAL DATE',
        dataIndex: 'deliveryDate',
        width: 120,
        render: (value: number) => {
          const year = moment.utc(value).get('year')
          // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
          if (year != 1990) {
            return moment.utc(value).format('MM/DD/YYYY')
          }
        },
      },
      {
        title: 'LOT',
        dataIndex: 'lotId',
      },
      // {
      //   title: 'SKU',
      //   dataIndex: 'wholesaleItem.sku',
      // },
      // {
      //   title: 'PRODUCT NAME',
      //   dataIndex: 'wholesaleItem.variety',
      // },
      {
        title: 'VENDOR',
        dataIndex: 'vendor',
        align: 'left',
        key: 'vendor',
        render: (vendor: any, record: any) => {
          if (record.wholesaleClient && record.wholesaleClient.clientCompany) {
            return record.wholesaleClient.clientCompany.companyName
          } else {
            return ''
          }
        },
      },
      {
        title: 'AVAILABLE TO SELL',
        dataIndex: 'lotAvailableQty',
        key: 'available_to_sell',
        align: 'left',
        width: 200,
        render(_text: any, item: any, _index: any) {
          let lotAvailableQty = item.lotAvailableQty === null ? 0 : item.lotAvailableQty
          let itemInfo = item.wholesaleItem
          // if (lotAvailableQty <= 0) {
          //   return ''
          // }
          // if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio == true && itemInfo.ratioUOM == 1) {
          //   return `${lotAvailableQty} ${item.uom ? " " + item.uom : ''}`
          // } else if (itemInfo.inventoryUOM != itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return lotAvailableQty + (item.uom ? " " + item.uom : '')
          // } else if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return lotAvailableQty + (item.uom ? " " + item.uom : '')
          // } else {
          //   let ratioQty = formatNumber(lotAvailableQty * item.wholesaleItem.ratioUOM, 2)
          //   return <div>
          //     {lotAvailableQty} {item.wholesaleItem.inventoryUOM ? " " + item.wholesaleItem.inventoryUOM : ''}
          //     <br />
          //     {ratioQty} {item.wholesaleItem.baseUOM ? " " + item.wholesaleItem.baseUOM : ''}
          //   </div>
          // }

          return isArray(itemInfo.wholesaleProductUomList) &&
            itemInfo.wholesaleProductUomList.length > 0 &&
            itemInfo.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
            <Tooltip
              title={
                <>
                  {itemInfo.wholesaleProductUomList.map((uom: any) => {
                    if (uom.useForInventory) {
                      return (
                        <div>
                          {inventoryQtyToRatioQty(uom.name, lotAvailableQty, itemInfo, 2)} {uom.name}
                        </div>
                      )
                    }
                  })}
                </>
              }
            >
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(lotAvailableQty, 2)} {itemInfo.inventoryUOM}
              </span>
            </Tooltip>
          ) : (
            <span style={{ marginRight: '20px' }}>
              {mathRoundFun(lotAvailableQty, 2)} {itemInfo.inventoryUOM}
            </span>
          )
        },
      },
      {
        title: 'UNITS ON HAND',
        dataIndex: 'onHandQty',
        key: 'onHandQty',
        align: 'left',
        width: 200,
        render(_text: any, item: any, _index: any) {
          let onHandQty = item.onHandQty === null ? 0 : item.onHandQty
          let itemInfo = item.wholesaleItem
          // if (onHandQty <= 0) {
          //   return ''
          // }
          // if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio == true && itemInfo.ratioUOM == 1) {
          //   return `${onHandQty} ${item.uom ? " " + item.uom : ''}`
          // } else if (itemInfo.inventoryUOM != itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return onHandQty + (item.uom ? " " + item.uom : '')
          // } else if (itemInfo.inventoryUOM == itemInfo.baseUOM && itemInfo.constantRatio != true) {
          //   return onHandQty + (item.uom ? " " + item.uom : '')
          // } else {
          //   let ratioQty = formatNumber(onHandQty * item.wholesaleItem.ratioUOM, 2)
          //   return <div>
          //     {onHandQty} {item.wholesaleItem.inventoryUOM ? " " + item.wholesaleItem.inventoryUOM : ''}
          //     <br />
          //     {ratioQty} {item.wholesaleItem.baseUOM ? " " + item.wholesaleItem.baseUOM : ''}
          //   </div>
          // },
          return isArray(itemInfo.wholesaleProductUomList) &&
            itemInfo.wholesaleProductUomList.length > 0 &&
            itemInfo.wholesaleProductUomList.filter((uom) => uom.useForInventory).length > 0 ? (
            <Tooltip
              title={
                <>
                  {itemInfo.wholesaleProductUomList.map((uom: any) => {
                    if (uom.useForInventory) {
                      return (
                        <div>
                          {inventoryQtyToRatioQty(uom.name, onHandQty, itemInfo, 2)} {uom.name}
                        </div>
                      )
                    }
                  })}
                </>
              }
            >
              <span style={{ marginRight: '20px' }}>
                {mathRoundFun(onHandQty, 2)} {itemInfo.inventoryUOM}
              </span>
            </Tooltip>
          ) : (
            <span style={{ marginRight: '20px' }}>
              {mathRoundFun(onHandQty, 2)} {itemInfo.inventoryUOM}
            </span>
          )
        },
      },
      {
        title: 'COST',
        dataIndex: 'cost',
        key: 'cost',
        width: 100,
        render: (value: number, record: any) => {
          // const freight = record.freight ? record.freight : 0
          // if (record.uom == record.wholesaleItem.inventoryUOM && record.wholesaleItem.constantRatio == true) {
          //   return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? (value / record.wholesaleItem.ratioUOM) + freight : 0, 2)}</div>
          // } else {
          //   return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? value + freight : 0, 2)}</div>
          // }
          value = value ? value : 0
          value = value + record.perAllocateCost
          return <div style={{ textAlign: 'right', paddingRight: 20 }}>${formatNumber(value ? value : 0, 2)}</div>
        },
      },
      {
        title: 'NOTES FROM PO',
        className: 'th-left',
        dataIndex: 'note',
        width: 180,
        render: (note: string) => {
          return (
            <Tooltip title={note && note.length > 25 ? note : ''}>
              <div style={{ width: 160, overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>
                {note ? note : ''}
              </div>
            </Tooltip>
          )
        },
      },
      // {
      //   title: 'QUOTED PRICE',
      //   dataIndex: 'price',
      //   key: 'price',
      //   align: 'right',
      //   width: 100,
      //   render: (value: number, record: any) => {
      //     const markup = (record.margin ? record.margin : 0) / 100
      //     const freight = record.freight ? record.freight : 0
      //     if (record.uom == record.wholesaleItem.inventoryUOM && record.wholesaleItem.constantRatio == true) {
      //       return (
      //         <div style={{ textAlign: 'right', paddingRight: 20 }}>
      //           ${formatNumber(value ? (1 + markup) * (value / record.wholesaleItem.ratioUOM) + freight : 0, 2)}
      //         </div>
      //       )
      //     } else {
      //       return (
      //         <div style={{ textAlign: 'right', paddingRight: 20 }}>
      //           ${formatNumber(value ? (1 + markup) * (value + freight) : 0, 2)}
      //         </div>
      //       )
      //     }
      //     return (
      //       <div style={{ textAlign: 'right', paddingRight: 20 }}>
      //         ${formatNumber(value ? (1 + markup) * (value + freight) : 0, 2)}
      //       </div>
      //     )
      //   },
      // },
    ]

    return (
      <PopoverWrapper>
        <SearchInput
          handleSearch={this.props.onSearch}
          placeholder="Search by PRODUCT NAME, SKU, LOT#"
          defaultValue={searchStr}
        />
        <ThemeTable
          pagination={{ hideOnSinglePage: true, pageSize: 10 }}
          columns={columns}
          dataSource={filteredLots}
          rowKey="wholesaleOrderItemId"
          // tslint:disable-next-line:jsx-no-lambda
          onRow={(record, _rowIndex) => {
            return {
              onClick: (_event) => {
                this.props.onSelect(record)
              },
            }
          }}
        />
      </PopoverWrapper>
    )
  }
}

// export default ProductModal
const mapStateToProps = (state: GlobalState) => state.product
export default connect(ProductModule)(mapStateToProps)(LotTableContainer)
