import React from 'react'
import { Form, Input, Row, Col, Button, Icon, InputNumber, Popconfirm, Select } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import {
  SectionRadioWrapper,
  SectionRadioRow,
  SectionRadioColumn,
  SectionRadioLabel,
  SectionRadioText,
  SectionTableSplit,
  SetionTableLabel,
  DragSmallWrapper,
  ThemeModal,
  inputTitleStyles,
  buttonStyle,
  LotIdValue,
} from './product.component.style'

import { ThemeButton, ThemeSelect } from '~/modules/customers/customers.style'

import { brightGreen } from '~/common'
import { ProductModule, ProductProps, ProductDispatchProps } from '..'
import LotTableContainer from './product-adjustment-lot-table-components'
import { OrderItem, WholesaleProductUom } from '~/schema'

const FormItem = Form.Item

interface FormFields {
  quantity: number
  uom: String
  reason: string
  currentLotId: number
  wholesaleOrderItemId: number
  wholesaleItemId: number
  type: number
}

type AdjustFormProps = ProductProps & ProductDispatchProps & FormComponentProps<FormFields> & {}

class AdjustmentFromContainer extends React.PureComponent<AdjustFormProps> {
  state = {
    newModalVisible: false,
    wholesaleOrderItemId: 0,
    searchStr: '',
  }

  handleSelectOk = () => {
    this.setState({
      newModalVisible: false,
      searchStr: '',
    })
  }

  handleChooseLot = (data: OrderItem) => {
    this.setState({
      newModalVisible: false,
      wholesaleOrderItemId: data.wholesaleOrderItemId,
    })
    this.props.form.setFieldsValue({
      currentLotId: data.lotId,
    })
  }

  onSave = (e: any) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.wholesaleOrderItemId = this.state.wholesaleOrderItemId
        values.wholesaleItemId = this.props.match.params.id
        values.type = 2 // 1 ordeitem adjustment 2.item adjustment
        this.props.startCreating()
        this.props.createItemAdjustments(values)
        this.props.form.resetFields()
      }
    })
  }

  componentWillReceiveProps(nextProps: Readonly<AdjustFormProps>, nextContext: any) {
    if(this.props.creating !== nextProps.creating && nextProps.creating === false) {
      setTimeout(() => {
        // because availableToSell value is not real-time updates and events are run every 5 seconds on the backend side
        this.props.getItems(this.props.match.params.id)
      }, 5000)
    }
  }

  showLotModal = () => {
    this.setState({
      newModalVisible: true,
    })
  }

  getOrderItemLots = () => {
    console.log(this.props)
    this.props.getReceivedLots({ from: null, to: null, id: this.props.match.params.id })
  }

  onSearch = (text: string) => {
    this.setState({
      searchStr: text,
    })
  }

  render() {
    const { form, items } = this.props
    const { newModalVisible, searchStr } = this.state
    const { getFieldDecorator } = form
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 24, offset: 0 },
      },
    }
    return (
      <div>
        <ThemeModal
          closable={false}
          keyboard={true}
          okText="Close [esc]"
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={'75%'}
          style={{ minWidth: 1200 }}
          visible={newModalVisible}
          onOk={this.handleSelectOk}
          onCancel={this.handleSelectOk}
        >
          <LotTableContainer
            getOrderItemLots={this.getOrderItemLots}
            onSelect={this.handleChooseLot}
            searchStr={searchStr}
            onSearch={this.onSearch}
          />
        </ThemeModal>

        <Form style={{ margin: '0 20px' }} onSubmit={this.onSave}>
          <Row gutter={24}>
            <Col span={12}>
              <FormItem
                colon={false}
                css={inputTitleStyles}
                style={{ marginBottom: '34px' }}
                label={<SetionTableLabel className="form-label">Current LotID</SetionTableLabel>}
              >
                {getFieldDecorator('currentLotId', {
                  // initialValue: this.state.currentLotId,
                  rules: [{ required: true, message: 'Please choose LOT ', type: 'string' }],
                })(<Input type="text" disabled />)}
              </FormItem>
            </Col>
            {/* <Col span={4}>
            {currentLotId != '' &&
              <Form.Item>
                <LotIdValue className="ant-form-text">{currentLotId}</LotIdValue>
              </Form.Item>
            }
            </Col> */}
            <Col span={12} style={{ textAlign: 'left' }}>
              <Form.Item
                {...formItemLayoutWithOutLabel}
                colon={false}
                css={inputTitleStyles}
                label={<SetionTableLabel />}
              >
                <Button type="dashed" onClick={this.showLotModal} style={{ width: '60%' }}>
                  <Icon type="plus" /> Choose LotId
                </Button>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={12}>
              <FormItem
                colon={false}
                css={inputTitleStyles}
                style={{ marginBottom: '34px' }}
                label={<SetionTableLabel className="form-label">Adjust Units</SetionTableLabel>}
              >
                {getFieldDecorator('quantity', {
                  rules: [{ required: true, message: 'Please input adjust unit', type: 'number' }],
                })(<InputNumber style={{ width: '100%' }} />)}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem
                colon={false}
                css={inputTitleStyles}
                style={{ marginBottom: '34px' }}
                label={<SetionTableLabel className="form-label">Adjust UOM</SetionTableLabel>}
              >
                {getFieldDecorator('uom', {
                  initialValue: items.inventoryUOM,
                })(
                  <ThemeSelect
                    style={{ width: '100%' }}
                    disabled={
                      items.wholesaleProductUomList == null ||
                      (items.wholesaleProductUomList != null && items.wholesaleProductUomList.length == 0)
                    }
                  >
                    <Select.Option key={items.inventoryUOM} value={items.inventoryUOM}>
                      {items.inventoryUOM}
                    </Select.Option>
                    {items.wholesaleProductUomList != null && items.wholesaleProductUomList.length > 0
                      ? items.wholesaleProductUomList.map((uom: WholesaleProductUom) => {
                        if (!uom.deletedAt && uom.useForPurchasing) {
                          return (
                            <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                              {uom.name}
                            </Select.Option>
                          )
                        }
                      })
                      : ''}
                  </ThemeSelect>,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FormItem
                colon={false}
                css={inputTitleStyles}
                style={{ marginBottom: '34px' }}
                label={<SetionTableLabel className="form-label">Adjust Reason</SetionTableLabel>}
              >
                {getFieldDecorator('reason', {
                  rules: [{ required: true, message: 'Please input adjust reason', type: 'string' }],
                })(<Input.TextArea />)}
              </FormItem>
            </Col>
            <Col span={24}>
              {/* <Popconfirm
                title="Are you sure to save?"
                onConfirm={this.onSave}
                okText="Save"
                cancelText="Cancel"
              > */}
              <ThemeButton shape="round" size="large" type="primary" htmlType="submit">
                Apply Adjustment
              </ThemeButton>
              {/* </Popconfirm> */}
            </Col>
          </Row>
        </Form>
      </div>
    )
  }
}

const AdjustmentForm = Form.create()(AdjustmentFromContainer)
export { AdjustmentForm }
