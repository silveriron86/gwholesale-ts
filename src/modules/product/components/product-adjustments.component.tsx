import React from 'react'
import {
  AdjustmentsWrapper,
  TableTip,
  AdjustmentsForm,
  AdjustmentsTable,
  inputTitleStyles,
  buttonStyle,
  TipWithChildren,
  TipText,
  TipAction,
  Wrapper,
} from './product.component.style'
import getProductActivities from '~/mock/getProductActivities'
import { ThemeTable } from '~/modules/customers/customers.style'
import { WrapperTitle } from '~/modules/customers/accounts/addresses/addresses.style'
import { AdjustmentForm } from './product-adjustments-form.components'
import moment from 'moment'
import _ from 'lodash'

export class ProductAdjustments extends React.Component<T> {
  componentDidMount() {
    this.props.getAdjustments({ id: this.props.match.params.id })
  }

  render() {
    const { adjustments } = this.props
    const sortByDate = _.orderBy(adjustments, ['createdDate'], ['desc'])
    return (
      <Wrapper>
        <TipWithChildren>
          <TipText>Adjustments</TipText>
          <TipAction />
        </TipWithChildren>
        <AdjustmentsWrapper>
          <AdjustmentsForm>
            <AdjustmentForm {...this.props} />
          </AdjustmentsForm>
          <AdjustmentsTable>
            <WrapperTitle>Adjustments</WrapperTitle>
            <div style={{width: '100%', overflowX: 'auto'}}>
              <ThemeTable
                // style={{ margin: '0 5px' }}
                // size="small"
                className="no-top-border"
                pagination={{ pageSize: 10 }}
                dataSource={sortByDate}
                columns={this.tableColumns()}
              />
            </div>
          </AdjustmentsTable>
        </AdjustmentsWrapper>
      </Wrapper>
    )
  }

  private tableColumns() {
    const width = 110
    const widthBig = 220
    return [
      {
        title: '#',
        render: (text, record, index) => `${index + 1}`,
      },
      {
        title: 'Date',
        dataIndex: 'createdDate',
        key: 'createdDate',
        width: widthBig,
        render: (text, record) => {
          return moment(record.createdDate).format('lll')
        },
      },
      {
        title: 'LOTID',
        dataIndex: 'orderItem.lotId',
        key: 'lotId',
        width: widthBig,
      },
      ,
      {
        title: 'Adjust Reason',
        dataIndex: 'reason',
        key: 'reason',
        width: 400,
      },
      {
        title: 'Adjust Units',
        dataIndex: 'quantity',
        key: 'quantity',
        width,
      },
      {
        title: 'Adjust Uom',
        dataIndex: 'uom',
        key: 'uom',
        width,
      },
      {
        title: 'Adjuster',
        width,
        render: (text, record) => {
          return record.user.firstName + ' ' + record.user.lastName
        },
      },
    ]
  }
}
