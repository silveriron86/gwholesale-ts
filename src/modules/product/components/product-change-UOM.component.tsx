import React from 'react'
import {
  ThemeButton,
  ThemeOutlineButton,
  ThemeCheckbox,
  ThemeInput,
  ThemeTable,
  ThemeSelect,
  ThemeLink,
  ThemeModal,
  ThemeInputNumber,
} from '~/modules/customers/customers.style'
import { PricingHeader, PricingSubHeader, PricingDesc, ProductInfo, ThumbnailImg } from './product.component.style'
import { Icon, Popconfirm, Tooltip, Form, Select, Row, Col } from 'antd'
import { FormComponentProps } from 'antd/es/form'
import { WholesaleProductUom } from '~/schema'
import { basePriceToRatioPrice, mathRoundFun, ratioPriceToBasePrice, formatNumber } from '~/common/utils'
import { find, isArray, cloneDeep, isNumber } from 'lodash'
import { Icon as IconLocal } from '~/components'

interface ProductChangeUOMProps extends FormComponentProps {
  visibled: boolean
  items: any
  changeUOMModalStatus: Function
  onSaveChanges: Function
  updateProductPricing: Function
}

export class ProductChangeUOMFormComponent extends React.PureComponent<ProductChangeUOMProps> {
  state = {
    defaultSellingPricingUOM: this.props.items.defaultSellingPricingUOM
      ? cloneDeep(this.props.items.defaultSellingPricingUOM)
      : cloneDeep(this.props.items.inventoryUOM),
    wholesaleProductUomList: this.props.items.wholesaleProductUomList
      ? cloneDeep(this.props.items.wholesaleProductUomList)
      : [],
    price:
      this.props.items.price != null
        ? basePriceToRatioPrice(
            this.props.items.defaultSellingPricingUOM,
            this.props.items.price,
            this.props.items,
            6,
            true,
          )
        : 0,
    defaultPrice: this.props.items.price != null ? cloneDeep(this.props.items.price) : 0,
  }

  componentWillReceiveProps(nextProps: any) {
    if (
      Object.keys(nextProps.items).length > 0 &&
      isArray(nextProps.items.wholesaleProductUomList) &&
      JSON.stringify(nextProps.items.wholesaleProductUomList) !=
        JSON.stringify(this.props.items.wholesaleProductUomList)
    ) {
      this.setState({
        wholesaleProductUomList: cloneDeep(nextProps.items.wholesaleProductUomList),
      })
    }
    if (
      Object.keys(nextProps.items).length > 0 &&
      (this.props.items.defaultSellingPricingUOM != nextProps.items.defaultSellingPricingUOM ||
        !nextProps.items.defaultSellingPricingUOM)
    ) {
      this.setState({
        defaultSellingPricingUOM: nextProps.items.defaultSellingPricingUOM
          ? cloneDeep(nextProps.items.defaultSellingPricingUOM)
          : cloneDeep(nextProps.items.inventoryUOM),
      })
    }
    if (Object.keys(nextProps.items).length > 0 && this.props.items.price != nextProps.items.price) {
      this.setState({
        price: basePriceToRatioPrice(
          nextProps.items.defaultSellingPricingUOM,
          nextProps.items.price,
          nextProps.items,
          6,
          true,
        ),
      })
    }
    if (Object.keys(nextProps.items).length > 0 && this.props.items.price != nextProps.items.price) {
      this.setState({
        defaultPrice: cloneDeep(nextProps.items.price),
      })
    }
  }

  formatFactor = (factor: number) => {
    if (factor >= 0) {
      return `${factor}% Premium`
    } else {
      return `${factor}% Discount`
    }
  }

  formatDesc = (uom: any) => {
    const { items } = this.props
    return (
      <>
        $
        <span style={{ minWidth: 70, display: 'inline-block' }}>
          {formatNumber(basePriceToRatioPrice(uom.name, items.price, items, 2, true), 2)}
        </span>
        &nbsp;per&nbsp;
        {uom.name}
      </>
    )
  }

  changeModalStatus = () => {
    const { items } = this.props
    this.props.changeUOMModalStatus(false)
    this.setState({
      defaultSellingPricingUOM: items.defaultSellingPricingUOM
        ? cloneDeep(items.defaultSellingPricingUOM)
        : cloneDeep(items.inventoryUOM),
      wholesaleProductUomList: items.wholesaleProductUomList ? cloneDeep(items.wholesaleProductUomList) : [],
      price:
        items.price != null ? basePriceToRatioPrice(items.defaultSellingPricingUOM, items.price, items, 6, true) : 0,
      defaultPrice: items.price != null ? cloneDeep(items.price) : 0,
    })
  }

  saveChange = (e: any) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let wholesaleProductUomList = [...this.state.wholesaleProductUomList]
        let { cost, price, defaultSellingPricingUOM } = values
        const { items } = this.props

        const newItem = cloneDeep(items)
        newItem.wholesaleProductUomList = wholesaleProductUomList
        let dbPrice = ratioPriceToBasePrice(defaultSellingPricingUOM, price, newItem, 12)
        const info = {
          ...values,
          price: dbPrice,
          wholesaleProductUomList: wholesaleProductUomList,
        }
        this.props.updateProductPricing({
          id: this.props.items.wholesaleItemId,
          info: info,
        })
        this.props.changeUOMModalStatus(false)
        this.props.onSaveChanges({ ...info, price: price })
      }
    })
  }

  onChangeUom = (value: string) => {
    const { items } = this.props
    const { defaultPrice } = this.state
    if (value == items.inventoryUOM) {
      this.setState(
        {
          defaultSellingPricingUOM: value,
          price: basePriceToRatioPrice(value, defaultPrice, items, 12, true),
          wholesaleProductUomList: cloneDeep(items.wholesaleProductUomList),
        },
        () => {
          this.props.form.setFieldsValue({
            price: mathRoundFun(this.state.price, 2),
          })
        },
      )
    } else {
      let wholesaleProductUomList = cloneDeep(this.state.wholesaleProductUomList).map((uom: any) => {
        if (uom.priceFactor != 0) {
          uom.priceFactor = 0
          uom.isUpdate = true
        }
        return { ...uom }
      })
      let newItems = { ...items }
      newItems.wholesaleProductUomList = [...wholesaleProductUomList]
      this.setState(
        {
          defaultSellingPricingUOM: value,
          price: basePriceToRatioPrice(value, defaultPrice, newItems, 12, true),
          wholesaleProductUomList: wholesaleProductUomList,
        },
        () => {
          this.props.form.setFieldsValue({
            price: mathRoundFun(this.state.price, 2),
          })
        },
      )
    }
  }

  onChangeUomMargin = (index: number, value: string) => {
    // const { wholesaleProductUomList } = this.state
    let wholesaleProductUomList = [...this.state.wholesaleProductUomList]
    const { defaultSellingPricingUOM, defaultPrice } = this.state
    const { items } = this.props
    if (!isNumber(value)) {
      value = 0
    }
    wholesaleProductUomList[index]['priceFactor'] = value
    if (wholesaleProductUomList[index].name == defaultSellingPricingUOM) {
      items.wholesaleProductUomList = [...wholesaleProductUomList]
      this.setState(
        {
          price: basePriceToRatioPrice(defaultSellingPricingUOM, defaultPrice, items, 12, true),
        },
        () => {
          this.props.form.setFieldsValue({
            price: mathRoundFun(this.state.price, 2),
          })
        },
      )
    }
    wholesaleProductUomList[index]['isUpdate'] = true
    this.setState({
      wholesaleProductUomList: wholesaleProductUomList,
    })
  }

  onChangePrice = (value: number) => {
    const { defaultSellingPricingUOM } = this.state
    const { items } = this.props
    if (items.inventoryUOM == defaultSellingPricingUOM) {
      this.setState({
        defaultPrice: value,
      })
    } else {
      this.setState({
        defaultPrice: ratioPriceToBasePrice(defaultSellingPricingUOM, value, items),
      })
    }
  }

  render() {
    const { visibled, items } = this.props
    const { getFieldDecorator } = this.props.form

    const { price, defaultSellingPricingUOM, wholesaleProductUomList, defaultPrice } = this.state
    const salesUomList = []
    const newItems = cloneDeep(items)

    if (isArray(wholesaleProductUomList)) {
      for (const [index, uom] of wholesaleProductUomList.entries()) {
        if (!uom.deletedAt && uom.useForSelling && uom.name != defaultSellingPricingUOM) {
          newItems.wholesaleProductUomList = cloneDeep(wholesaleProductUomList)
          salesUomList.push(
            <Row className="product-specification-row" gutter={24}>
              <Col span={4} className="overFont" title={uom.name}>
                {uom.name}
              </Col>
              <Col span={6}>
                <ThemeInputNumber
                  key={uom.id}
                  placeholder="0.00%"
                  value={uom.priceFactor}
                  step="1"
                  style={{ width: '100%' }}
                  onChange={this.onChangeUomMargin.bind(this, index)}
                  formatter={(value) => `${value ? value + '%' : ''}`}
                />
              </Col>
              <Col span={14}>
                (Default price ${formatNumber(basePriceToRatioPrice(uom.name, defaultPrice, newItems, 2, true), 2)} per{' '}
                {uom.name})
              </Col>
            </Row>,
          )
        }
      }
    }
    return (
      <ThemeModal
        title={`Item Price and UOM Settings`}
        visible={visibled}
        onOk={this.saveChange}
        onCancel={this.changeModalStatus}
        cancelText="Cancel"
        okText="Save"
        // width={'50%'}
        style={{ minWidth: 660 }}
      >
        <Form onSubmit={this.saveChange}>
          <ProductInfo>
            <Row>
              {/* <Col span={4}>
                <ThumbnailImg src={getFileUrl(items.cover, false)} />
              </Col> */}
              <Col span={10}>
                <PricingDesc className="mt10">Item</PricingDesc>
                <PricingHeader className="mt5">{items.variety}</PricingHeader>
              </Col>
              {/* <Col span={10}>
                <PricingDesc className="mt10">Current Default Price</PricingDesc>
                <PricingHeader className="mt5">
                  ${items.price} per {items.defaultSellingPricingUOM}
                </PricingHeader>
              </Col> */}
            </Row>
          </ProductInfo>
          <PricingHeader className="mt20">Current Pricing</PricingHeader>
          <PricingDesc className="mt10">
            {items.inventoryUOM !== items.defaultSellingPricingUOM && (
              <Row>
                <Col span={6}>{items.defaultSellingPricingUOM}&nbsp;(Default)</Col>
                <Col span={6}>
                  {items.wholesaleProductUomList != null &&
                    items.wholesaleProductUomList.length > 0 &&
                    items.wholesaleProductUomList.some((u) => u.name === items.defaultSellingPricingUOM) &&
                    this.formatFactor(
                      items.wholesaleProductUomList.find((u) => u.name === items.defaultSellingPricingUOM).priceFactor,
                    )}
                </Col>
                <Col span={12}>
                  {items.wholesaleProductUomList != null &&
                    items.wholesaleProductUomList.length > 0 &&
                    items.wholesaleProductUomList.some((u) => u.name === items.defaultSellingPricingUOM) &&
                    this.formatDesc(
                      items.wholesaleProductUomList.find((u) => u.name === items.defaultSellingPricingUOM),
                    )}
                </Col>
              </Row>
            )}
            <Row className="mt10">
              <Col span={6}>
                {items.inventoryUOM}
                {items.inventoryUOM === items.defaultSellingPricingUOM && '(Default)'}
              </Col>
              <Col span={6}></Col>
              <Col span={12}>
                $<span style={{ minWidth: 70, display: 'inline-block' }}>{formatNumber(items.price, 2)}</span>
                &nbsp;per&nbsp;
                {items.inventoryUOM}
              </Col>
            </Row>
            {items.wholesaleProductUomList != null && items.wholesaleProductUomList.length > 0
              ? items.wholesaleProductUomList
                  .filter((u) => u.name !== items.defaultSellingPricingUOM)
                  .map((uom: WholesaleProductUom) => {
                    if (!uom.deletedAt) {
                      return (
                        <Row className="mt10">
                          <Col span={6}>{uom.name}</Col>
                          <Col span={6}>{this.formatFactor(uom.priceFactor)}</Col>
                          <Col span={12}>{this.formatDesc(uom)}</Col>
                        </Row>
                      )
                    }
                  })
              : ''}
          </PricingDesc>
          <PricingHeader className="mt20">
            Set New Default Price and UOM
            <Tooltip placement="top" title="This is how Sales Orders and Price Sheets will display pricing by default">
              <IconLocal type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
            </Tooltip>
          </PricingHeader>
          <PricingSubHeader className="mt10">
            <Row>
              <Col span={8} className="m">
                Default Price *
              </Col>
              <Col span={2} />
              <Col span={8}>UOM</Col>
            </Row>
          </PricingSubHeader>

          <PricingSubHeader className="mt10">
            <Row className="product-specification-row">
              <Col span={8}>
                <Form.Item>
                  {getFieldDecorator('price', {
                    initialValue: price,
                    rules: [
                      {
                        required: true,
                        message: 'Please input default price!',
                      },
                    ],
                  })(
                    <ThemeInputNumber
                      placeholder="$0.00"
                      step="0.25"
                      min={0}
                      formatter={(value) => `${value ? '$' + value : ''}`}
                      onChange={this.onChangePrice}
                    />,
                  )}
                </Form.Item>
              </Col>
              <Col span={2} className="text-center">
                per
              </Col>
              <Col span={8}>
                <Form.Item>
                  {getFieldDecorator('defaultSellingPricingUOM', {
                    initialValue: defaultSellingPricingUOM,
                    rules: [
                      {
                        required: true,
                        message: 'Please choose default price uom!',
                      },
                    ],
                  })(
                    <ThemeSelect
                      onChange={this.onChangeUom.bind(this)}
                      suffixIcon={<Icon type="caret-down" />}
                      style={{ minWidth: 80 }}
                    >
                      {items.useForSelling && (
                        <Select.Option value={items.inventoryUOM} title={items.inventoryUOM}>
                          {items.inventoryUOM}
                        </Select.Option>
                      )}

                      {items.wholesaleProductUomList != null && items.wholesaleProductUomList.length > 0
                        ? items.wholesaleProductUomList.map((uom: WholesaleProductUom) => {
                          if (!uom.deletedAt && uom.useForSelling) {
                            return (
                              <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                                {uom.name}
                              </Select.Option>
                            )
                          }
                        })
                        : ''}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </PricingSubHeader>
          <PricingHeader className="mt20">
            Optionally Set Pricing Premiums or Discounts
            <Tooltip placement="top" title="Applies Discounts or Premiums Phen Selling By Alternate UOMs">
              <IconLocal type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
            </Tooltip>
          </PricingHeader>
          <PricingDesc className="mt10">{salesUomList}</PricingDesc>
        </Form>
      </ThemeModal>
    )
  }
}

export const ProductChangeUOMForm = Form.create<ProductChangeUOMProps>()(ProductChangeUOMFormComponent)
