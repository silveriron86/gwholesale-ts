import React, { ReactNode } from 'react'
import { Form, Input, Row, Col, Select, Table, AutoComplete, Popconfirm, Divider } from 'antd'
import { SetionTableLabel, StyledForm } from './product.component.style'
import { ThemeOutlineButton, ThemeModal, ThemeSelect, ThemeButton } from '~/modules/customers/customers.style';
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor';
import { AssignedClientWrapper } from '~/modules/pricesheet/pricesheet-detail.style';
import { PayloadIcon } from '~/components/elements/elements.style';
import { Icon as IconSvg } from '~/components/icon/'
import _ from 'lodash'
import EditVendorListModal from './edit-vendor-list-modal';
class ProductDescriptionContainer extends React.Component<T> {

  state = {
    loading: false,
    openSupplier: false,
    openProductSupplierModal: false,
    hasSpecialChar: false,
    selectedSupplier: '',
    currentSuppliers: [],
    showEditVendor: false
  };

  column = [
    {
      dataIndex: 'name',
    },
    {
      title: '',
      dataIndex: '',
      key: 'x',
      width: '10%',
      render: (text: number, record: any) => {
        return <Popconfirm title="Are you sure to remove?" onConfirm={() => this.onRemoveSupplier(record.name)}>
          <a>
            <PayloadIcon>
              <IconSvg
                type="close"
                viewBox="0 0 20 28"
                width={15}
                height={14} />
            </PayloadIcon>
          </a>
        </Popconfirm>
      }
    }
  ]

  componentDidMount() {
    this.props.getProductVendors(this.props.match.params.id)
    this.props.getBriefVendors()
    if (!this.props.companyProductTypes) {
      this.props.getCompanyProductAllTypes()
    }
  }

  toggleEditVendorModal = () => {
    this.setState((pre:any) => ({ showEditVendor: !pre.showEditVendor }))
  }

  componentDidUpdate(prevProps: any) {
    if (this.props.items.suppliers != prevProps.items.suppliers || (this.state.currentSuppliers.length == 0 && this.props.items.suppliers)) {
      const suppliers = this.props.items.suppliers
      const trimText = suppliers.split(', ').map((el: string) => el.trim())
      this.setState({ currentSuppliers: suppliers === '' ? [] : trimText })
    }
    if (this.props.companyProductTypes && JSON.stringify(this.props.companyProductTypes) != JSON.stringify(prevProps.companyProductTypes)) {
      this.props.form.resetFields()
    }
  }

  onSave = async (e: any) => {
    e.preventDefault()
    const items = await this.props.form.validateFields()
    //because if quantity set, backend will add new lot
    items.quantity = null
    items.suppliers = items.suppliers.join(', ')
    const { preferredVendorId } = items
    if (!preferredVendorId) {
      items.preferredVendorId = null
    }
    items.customerProductCode = items.customerProductCode ? items.customerProductCode.replaceAll(", ",",").replaceAll("， ","，")
                                .replaceAll(" ,",",").replaceAll(" ，","，")
                                .replaceAll(" , ",",").replaceAll(" ， ","，") : null
    this.props.updateItems({data: { ...this.props.items, ...items }, queryParams: {ignoreSync: true}})
  }

  openAddSupplierModal = () => {
    this.setState({ openSupplier: true })
  }

  cancelSupplierModal = () => {
    this.setState({ openSupplier: false })
  }

  onAfterCloseSupplier = () => {
    setTimeout(() => {
      this.props.getCompanyProductAllTypes()
    }, 500)
  }

  onClickAddProductSupplier = () => {
    this.setState({ openProductSupplierModal: true })
  }

  onAddSupplierToProduct = () => {
    const { companyProductTypes } = this.props
    const { selectedSupplier, currentSuppliers } = this.state
    const all = companyProductTypes ? companyProductTypes.suppliers.map((el: any) => el.name) : []
    if (_.includes(all, selectedSupplier)) {
      const newSuppliers = [...currentSuppliers, selectedSupplier]
      const suppliers = newSuppliers.join(', ')
      //because if quantity set, backend will add new lot
      let items = this.props.items
      items.quantity = null
      this.props.updateItems({data: { ...items, suppliers: suppliers }, queryParams: {ignoreSync: true}})
    }
    this.setState({ openProductSupplierModal: false, selectedSupplier: '' })
  }

  onCloseAddProductSupplierModal = () => {
    this.setState({ openProductSupplierModal: false, selectedSupplier: '' })
  }

  handleFilterOption = (input: string, option: any) => {
    return option.props.children.toUpperCase().indexOf(input.toUpperCase()) !== -1
  };

  onSelectAddingSupplier = (value: any, option: any) => {
    this.setState({ selectedSupplier: value })
  }

  onRemoveSupplier = (value: string) => {
    const { currentSuppliers } = this.state
    const removed = _.pull([...currentSuppliers], value)
    console.log(removed)
    if (removed.length != currentSuppliers.length) {
      const suppliers = removed.join(', ')
      console.log('removed suppliers', suppliers)
      this.setState({ currentSuppliers: removed })
      //because if quantity set, backend will add new lot
      let items = this.props.items
      items.quantity = null
      this.props.updateItems({data: { ...items, suppliers: suppliers}, queryParams: {ignoreSync: true} })
    }
  }

  getAvailableSuppliers = () => {
    const { companyProductTypes } = this.props
    const { currentSuppliers } = this.state
    const all = companyProductTypes ? companyProductTypes.suppliers.map((el: any) => el.name) : []
    const available = _.differenceWith(all, currentSuppliers)
    return available.length ? available.sort((a: string, b: string) => a.localeCompare(b)) : []
  }

  getTableData = () => {
    const { currentSuppliers } = this.state
    return currentSuppliers.map((el: string) => { return { name: el } })
  }

  handleChangeSuppliers = (value: string[]) => {
    console.log(value)
    this.setState({ currentSuppliers: value })
  }

  onChangeSelectedSuppliers = (value: any) => {
    this.setState({ selectedSupplier: value })
  }

  render() {
    const { items, form, productVendors, addRemoveVendor, briefVendors } = this.props
    const productId = this.props.match.params.id
    const { showEditVendor } = this.state

    const { getFieldDecorator } = form
    const { openSupplier, openProductSupplierModal, selectedSupplier } = this.state;
    // const allSuppliers = companyProductTypes ? companyProductTypes.suppliers.map((el: any) => el.name) : []
    const availableSuppliers = this.getAvailableSuppliers()
    const tableData = this.getTableData()

    return (
      <StyledForm style={{ margin: '0 20px' }} onSubmit={this.onSave}>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Summary</SetionTableLabel>}>
              {getFieldDecorator('detail', {
                rules: [{ required: false, message: 'Please input summary!' }],
                initialValue: items.detail,
              })(<Input.TextArea placeholder="Please input summary" autosize={false} style={{ minHeight: '100px' }} />)}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Specification</SetionTableLabel>}>
              {getFieldDecorator('specification', {
                rules: [{ required: false, message: 'Please input specification!' }],
                initialValue: items.specification,
              })(<Input.TextArea placeholder="Please input specification" autosize={false} style={{ minHeight: '100px' }} />)}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item colon={false} label={
              <div style={{display:'flex', justifyContent:'space-between'}}>
                <SetionTableLabel className="form-label">Brands</SetionTableLabel>
                <ThemeOutlineButton ghost onClick={this.openAddSupplierModal}>Manage brands list</ThemeOutlineButton>
              </div>
            }>
              {getFieldDecorator('suppliers', {
                rules: [{ required: false, message: 'Please input suppliers!' }],
                initialValue: tableData ? tableData.map(el => { return el.name }) : [],
              })(<ThemeSelect
                mode="multiple"
                style={{ width: '100%' }}
                placeholder="Please select brand"
                onChange={this.handleChangeSuppliers}

              >
                {
                  availableSuppliers.map((el: any, index: number) => {
                    return (
                      <Select.Option value={el} key={`${el}-${index}`}>
                        {el}
                      </Select.Option>)
                  })
                }
              </ThemeSelect>
              )}
            </Form.Item>
            {/* <div style={{ textAlign: 'left' }}>
              <ThemeButton onClick={this.onClickAddProductSupplier}>ADD PRODUCT SUPPLIER</ThemeButton>
            </div> */}
          </Col>
          <Col span={12}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Preferred vendor</SetionTableLabel>}>
            {getFieldDecorator('preferredVendorId', {
                rules: [{ required: false, message: 'Please select vendor!' }],
                initialValue: items.preferredVendorId || '',
              })(
                <ThemeSelect
                  showSearch
                  optionFilterProp="children"
                  style={{ width: '100%' }}
                  placeholder="Please select vendor"
                  dropdownRender={(menu: ReactNode) => (
                    <div>
                      {menu}
                      <Divider style={{ margin: '2px 0' }} />
                      <div
                        style={{ padding: '10px 8px 12px', cursor: 'pointer', color: this.props.theme.primary }}
                        onMouseDown={(e) => e.preventDefault()}
                        onClick={this.toggleEditVendorModal}
                      >
                        Edit vendor list
                      </div>
                    </div>
                  )}
                >
                  <Select.Option key="" value="">
                    None selected
                  </Select.Option>
                  {productVendors.map((el: any, index: number) => {
                    return (
                      <Select.Option
                        value={el.wholesaleClient.clientId}
                        key={el.wholesaleClient.clientId}
                      >
                        {el.wholesaleClient.clientCompany.companyName}
                      </Select.Option>
                    )
                  })}
                </ThemeSelect>,
              )}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Customer Product codes</SetionTableLabel>}>
              {getFieldDecorator('customerProductCode', {
                rules: [{ required: false, message: 'Please list customer product codes by comma split !' }],
                initialValue: items.customerProductCode,
              })(<Input.TextArea placeholder="List of Comma-separated Customer Product Codes for this item.  These values are used as lookup values when batch importing Sales Orders." autosize={false} style={{ minHeight: '100px' }} />)}
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Manufacturer item codes</SetionTableLabel>}>
              {getFieldDecorator('supplierSkus', {
                rules: [{ required: false, message: 'Please list manufacturer item codes/SKUs!' }],
                initialValue: items.supplierSkus,
              })(<Input.TextArea placeholder="Please list manufacturer item codes/SKUs separated by commas. These will be used for item identification in barcode scanning." autosize={false} style={{ minHeight: '100px' }} />)}
            </Form.Item>
          </Col>
        </Row>
        {/* <Popconfirm
          title="Are you sure to save?"
          onConfirm={this.onSave}
          okText="Save"
          cancelText="Cancel"
        > */}
        <ThemeButton
          htmlType="submit"
          shape="round"
          size="large"
          style={{ width: '180px', marginBottom: '10px' }}
        >
          Save
        </ThemeButton>
        {/* </Popconfirm> */}
        <ThemeModal
          title={`Manage Brands List`}
          visible={openSupplier}
          onCancel={this.cancelSupplierModal}
          cancelText='Close'
          okButtonProps={{ style: { display: 'none' } }}
          afterClose={this.onAfterCloseSupplier}
        >
          <TypesEditor
            isModal={true}
            field="suppliers"
            title="Brand Name"
            buttonTitle="Add New Brand"
          />
        </ThemeModal>
        <ThemeModal
          visible={openProductSupplierModal}
          onOk={this.onAddSupplierToProduct}
          onCancel={this.onCloseAddProductSupplierModal}
          theme={this.props.theme}
          okText="Add">
          <div>
            <label>Suppliers</label>
            <br />
            <AutoComplete
              size="large"
              style={{ width: '100%', marginBottom: 20 }}
              dataSource={availableSuppliers}
              filterOption={this.handleFilterOption}
              onSelect={this.onSelectAddingSupplier}
              placeholder={`Select a supplier`}
              value={selectedSupplier}
              onChange={this.onChangeSelectedSuppliers}
            />
            <label>Added Suppliers</label>
            <br />
            <AssignedClientWrapper>
              <Table columns={this.column} dataSource={tableData} rowKey="name" showHeader={false}></Table>
            </AssignedClientWrapper>
          </div>
        </ThemeModal>
        <EditVendorListModal briefVendors={briefVendors} productId={productId} addRemoveVendor={addRemoveVendor} productVendors={productVendors} onCancel={this.toggleEditVendorModal} visible={showEditVendor} />
      </StyledForm>
    )
  }
}

const ProductDescriptionForm = Form.create()(ProductDescriptionContainer)
export { ProductDescriptionForm }
