import React from 'react'
import { ProductDescriptionForm } from './product-description-form.component'
import { Wrapper, Tip } from './product.component.style'

export class ProductDescription extends React.Component<T> {
  state = {}
  render() {
    return (
      <Wrapper>
        <Tip>Product Description</Tip>
        <ProductDescriptionForm {...this.props} />
      </Wrapper>
    )
  }
}
