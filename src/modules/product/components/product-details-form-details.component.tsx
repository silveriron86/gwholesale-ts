import {
  Button,
  Col,
  Icon as DefaultIcon,
  Form,
  Input,
  InputNumber,
  Modal,
  Popconfirm,
  Row,
  Select,
  Tooltip,
  Upload,
  message,
  notification,
} from 'antd'
import {
  DragSmallWrapper,
  InitialAvailableSell,
  SetionTableLabel,
  ThemeDatePicker,
  TrashIconWrapper,
} from './product.component.style'
import { InputLabel, ThemeForm, ThemeIcon, ThemeInputNumber, ThemeModal } from '~/modules/customers/customers.style'
import { ProductDispatchProps, ProductProps } from '..'

import { CACHED_ACCESSTOKEN } from '~/common'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import { FormComponentProps } from 'antd/lib/form'
import { Icon } from '~/components'
import React from 'react'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import _ from 'lodash'
import { brightGreen } from '~/common'
import { getFileUrl } from '~/common/utils'
import moment from 'moment'
import { onLoadFocusOnFirst } from '~/common/jqueryHelper'

interface FormFields {
  sku: string
  variety: string
  category: string
  wholesaleCategory: any
  family: string
  defaultMargin: number
  temperature: string
  standardWeight: boolean
  unitWeight: number
  unitPerLayer: string
  reorderLevel: string
  layersPerPallet: string
  shelfLife: number
  size: number
  weight: number
  grade: number
  packing: number
  restockingTime: number
  origin: string
  managerId: number
  baseUOM: string
  inventoryUOM: string
  constantRatio: boolean
  ratioUOM: number
  brand: string
  price: number
  isOverridePrice: boolean
  quantity: number
  reorderQuantity: number
}

type ProductDetailProps = ProductProps &
  ProductDispatchProps &
  FormComponentProps<FormFields> & {
    radioData: any
    isActivity?: boolean
  }

class ProductDetailsContainer extends React.PureComponent<ProductDetailProps> {
  state = {
    loading: false,
    currentUom: 'case',
    currentManager: 'manager',
    managerValue: '',
    showManagerModal: false,
    confirmVisible: false,
    virtualLotModal: false,
    newData: null,
    price: 0,
    deliveryDate: moment(),
    inventory: 0,
    defaultInventory: 0,
    purchasePrice: 0,
  }
  input: any

  componentDidMount() {
    this.props.getCompanyProductAllTypes()
    this.props.getCompanyPmUsers()
    onLoadFocusOnFirst('simple-input-wrapper')
  }

  componentWillReceiveProps(nextProps: ProductDetailProps) {
    if (this.props.items.price != nextProps.items.price) {
      this.setState({
        price: nextProps.items.price,
      })
    }
    if (this.props.items.cover != nextProps.items.cover) {
      this.setState({
        loading: false,
      })
    }
  }

  onUomChange = (value: string) => {
    this.setState({ currentUom: value })
  }

  onManagerChange = (value: string) => {}

  onSave = async (e: any) => {
    e.preventDefault()
    const { categories, form, items } = this.props
    const { wholesaleItemId, qboId } = items

    form.validateFields((err, values) => {
      console.log(values)
      if (!err) {
        // let category: any = null
        // categories.forEach(el => {
        //   if(el.wholesaleCategoryId === values.category[1]) {
        //     category = el
        //   }
        // })
        // console.log(values, category);

        // delete values.category
        // const newData = { ...values, wholesaleItemId: wholesaleItemId, wholesaleCategory: category, qboId };
        const newData = { ...items, ...values, wholesaleItemId: wholesaleItemId, qboId }

        if (values.quantity !== null && values.quantity > 0 && values.quantity !== this.props.items.quantity) {
          this.setState({
            confirmVisible: true,
            newData,
          })
        } else {
          // if(newData.quantity !== null && newData.quantity > 0) {
          newData.quantity = null
          newData.shelfLife = _.isEmpty(newData.shelfLife) ? 0 : newData.shelfLife
          newData.restockingTime = _.isEmpty(newData.restockingTime) ? 0 : newData.restockingTime
          newData.plu = parseInt(newData.plu)
          this.props.updateItems({data: newData, queryParams: {ignoreSync: true}})
        }
      }
    })
  }

  getBase64 = (img, callback) => {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }

  beforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!')
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!')
    }
    return isJpgOrPng && isLt2M
  }

  handleSuccess = (response) => {
    if (response.statusCodeValue == 200) {
      const { id } = this.props.match.params
      this.props.getItems(id)
    } else {
      notification['error']({
        message: 'Error',
        description: `File upload failed`,
      })
      this.setState({ loading: false })
    }
  }
  handleChange = (info) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true })
      return
    }
  }

  _displayRender = (label: any) => {
    return label[label.length - 1]
  }

  handleConfirmOk = () => {
    this.setState(
      {
        confirmVisible: false,
      },
      () => {
        this.props.updateItems({data: this.state.newData, queryParams: {ignoreSync: true}})
      },
    )
  }

  handleConfirmCancel = () => {
    this.setState({
      confirmVisible: false,
    })
  }
  changeSalesPrice = (value: number) => {
    this.setState({
      price: value,
    })
  }

  changeVirtualLotModalStatus = (v) => {
    this.setState({
      inventory: v,
      defaultInventory: v,
      virtualLotModal: true,
    })
  }

  saveVirtualLotModal = () => {
    this.setState({
      virtualLotModal: false,
    })
    const { deliveryDate, purchasePrice, inventory } = this.state
    const item = this.props.items

    let data = {
      ...item,
      deliveryDate: moment.utc(deliveryDate).format('YYYY-MM-DD'),
      quantity: inventory,
      virtualLotPrice: purchasePrice ? purchasePrice : 0,
    }
    this.props.updateItems({data, queryParams: {ignoreSync: true}})
  }

  onDeliveryDateChange = (oldDate: any, newDate: any) => {
    this.setState({
      deliveryDate: moment(newDate),
    })
  }

  onChangeInventory = (val: any) => {
    this.setState({
      inventory: val,
    })
  }

  onChangePurchasePrice = (val: any) => {
    this.setState({
      purchasePrice: val,
    })
  }

  handleCancel = () => {
    this.setState(
      {
        virtualLotModal: false,
        inventory: 0,
        purchasePrice: 0,
        deliveryDate: moment(),
      },
      () => {
        if (this.input) {
          this.input.blur()
        }
      },
    )
    this.props.form.setFieldsValue({
      quantity: 0,
    })
  }

  onRemoveCover = (e: any) => {
    e.preventDefault()
    const { items } = this.props
    const newData = { ...items, quantity: null, cover: null }
    this.props.updateItems({data: newData, queryParams: {ignoreSync: true}})
  }

  render() {
    const { confirmVisible, virtualLotModal, deliveryDate, inventory, defaultInventory, purchasePrice } = this.state
    const { radioData = [], items, form } = this.props
    const { getFieldDecorator } = form
    const { companyPmUsers, isActivity } = this.props

    const uploadButton = (
      <div>
        <p className="ant-upload-drag-icon">
          <DefaultIcon type={this.state.loading ? 'loading' : 'inbox'} />
        </p>
        <p className="ant-upload-text">
          {this.state.loading ? 'loading' : 'Click or drag file to this area to upload'}
        </p>
        <div className="ant-upload-hint" style={{ color: 'gray', fontSize: 12, fontWeight: 500 }}>
          {this.state.loading ? (
            ''
          ) : (
            <>
              <p>File formats: JPEG or PNG</p>
              <p>Minimum size: 200px X 200px</p>
            </>
          )}
        </div>
      </div>
    )

    if (isActivity === true) {
      return (
        <ThemeForm onSubmit={this.onSave} className="simple-input-wrapper">
          <ThemeModal
            visible={virtualLotModal}
            title="Initial Inventory"
            onOk={this.saveVirtualLotModal}
            onCancel={this.handleCancel}
            okButtonProps={{ disabled: deliveryDate === '' || inventory === 0 }}
          >
            <Row style={{ marginTop: 10 }}>
              <Col md={10}>
                <InputLabel>Initial Inventory</InputLabel>
              </Col>
              <Col md={14}>
                <Flex style={{ alignItems: 'center' }}>
                  <div style={{ width: 11 }} />
                  <ThemeInputNumber
                    style={{ width: 'auto !important', flex: 1 }}
                    placeholder="Initial Inventory"
                    value={inventory}
                    onChange={this.onChangeInventory}
                    disabled={defaultInventory !== 0}
                  />
                </Flex>
              </Col>
            </Row>
            <Row style={{ marginTop: 10 }}>
              <Col md={10}>
                <InputLabel>Initial Inventory Lot Date</InputLabel>
              </Col>
              <Col md={14}>
                <Flex style={{ alignItems: 'center' }}>
                  <div style={{ width: 11 }} />
                  <ThemeDatePicker
                    allowClear={false}
                    onChange={this.onDeliveryDateChange}
                    defaultValue={deliveryDate}
                    placeholder="MM/DD/YYYY"
                    format="MM/DD/YYYY"
                    suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                  />
                </Flex>
              </Col>
            </Row>
            <Row style={{ marginTop: 10 }}>
              <Col md={10}>
                <InputLabel>Average Purchase Price</InputLabel>
              </Col>
              <Col md={14}>
                <Flex style={{ alignItems: 'center' }}>
                  <div style={{ width: 11 }}>$</div>
                  <ThemeInputNumber
                    style={{ width: 'auto !important', flex: 1 }}
                    placeholder="Average Purchase Price"
                    value={purchasePrice}
                    min={0}
                    onChange={this.onChangePurchasePrice}
                  />
                </Flex>
              </Col>
            </Row>
          </ThemeModal>
          <InitialAvailableSell>
            <Form.Item
              colon={false}
              label={<SetionTableLabel className="form-label">Initial Inventory</SetionTableLabel>}
            >
              {getFieldDecorator('quantity', {
                rules: [{ required: false, message: 'Please input initial inventory!' }],
                initialValue: items.quantity ? items.quantity : '0',
              })(
                <Input
                  ref={(node) => (this.input = node)}
                  placeholder="Initial Inventory"
                  min={0}
                  disabled={items.quantity > 0}
                  onClick={() => this.changeVirtualLotModalStatus(items.quantity ? items.quantity : 0)}
                  readOnly={true}
                  style={{ cursor: 'unset', outline: 'none', boxShadow: 'none', borderColor: '#d9d9d9' }}
                />,
              )}
            </Form.Item>
          </InitialAvailableSell>
        </ThemeForm>
      )
    }

    return (
      <ThemeForm onSubmit={this.onSave} className="simple-input-wrapper">
        <ThemeModal
          visible={confirmVisible}
          title="Are you sure?"
          onCancel={this.handleConfirmCancel}
          closable={false}
          footer={[
            <Button key="Yes" onClick={this.handleConfirmOk}>
              Yes
            </Button>,
            <Button key="No" type="primary" onClick={this.handleConfirmCancel}>
              No
            </Button>,
          ]}
        >
          <p>
            Please confirm you want to set the initial physical quantity present in warehouse. This value cannot be
            edited directly once it is initially set.
          </p>
        </ThemeModal>

        <Row gutter={24}>
          <Col span={8}>
            <DragSmallWrapper>
              <div style={{ textAlign: 'left', lineHeight: '45px' }}>
                <SetionTableLabel className="form-label">Image</SetionTableLabel>
                <Tooltip title="This image will be saved as a public image. It will be displayed to users within your company on the products list and may also be displayed to customers using online ordering from price sheets.">
                  <DefaultIcon type="info-circle" style={{ fontSize: 12, marginLeft: 8 }} />
                </Tooltip>
              </div>
              <Upload.Dragger
                listType="picture-card"
                showUploadList={false}
                action={process.env.WSW2_HOST + `/api/v1/wholesale/file/document/product/cover/${this.props.match.params.id}?needPermission=false`}
                headers={{
                  Authorization: `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN)}`,
                }}
                beforeUpload={this.beforeUpload}
                onChange={this.handleChange}
                onSuccess={this.handleSuccess}
              >
                {items.cover ? (
                  <img
                    src={getFileUrl(items.cover, true)}
                    alt="avatar"
                    style={{ maxWidth: '100%', maxHeight: '100%' }}
                  />
                ) : (
                  uploadButton
                )}
              </Upload.Dragger>
              {items.cover && (
                <TrashIconWrapper>
                  <Popconfirm
                    trigger={'click'}
                    title={'Are you sure to delete cover?'}
                    onConfirm={this.onRemoveCover}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Icon type="trash" viewBox="0 0 24 24" width={20} height={20} />
                  </Popconfirm>
                </TrashIconWrapper>
              )}
            </DragSmallWrapper>
          </Col>
          <Col span={16}>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item colon={false} label={<SetionTableLabel className="form-label">Origin</SetionTableLabel>}>
                  {getFieldDecorator('origin', {
                    rules: [{ required: false, message: 'Please input origin!' }],
                    initialValue: items.origin,
                  })(<Input placeholder="origin" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item colon={false} label={<SetionTableLabel className="form-label">UPC</SetionTableLabel>}>
                  {getFieldDecorator('upc', {
                    rules: [{ required: false, message: 'Please input UPC!' }],
                    initialValue: items.upc,
                  })(<Input placeholder="UPC" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item colon={false} label={<SetionTableLabel className="form-label">Family</SetionTableLabel>}>
                  {getFieldDecorator('family', {
                    rules: [{ required: false, message: 'Please input family!' }],
                    initialValue: items.family,
                  })(<Input placeholder="product family" />)}
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  colon={false}
                  label={<SetionTableLabel className="form-label">Temperature</SetionTableLabel>}
                >
                  <Input.Group>
                    <Row gutter={24}>
                      <Col span={24}>
                        {getFieldDecorator('temperature', {
                          rules: [{ required: false, message: 'Please input temperature!' }],
                          initialValue: items.temperature,
                        })(<Input placeholder="temperature" />)}
                      </Col>
                    </Row>
                  </Input.Group>
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Size</SetionTableLabel>}>
              {getFieldDecorator('size', {
                rules: [{ required: false, message: 'Please input size' }],
                initialValue: items.size,
              })(<Input placeholder="size" />)}
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Weight</SetionTableLabel>}>
              {getFieldDecorator('weight', {
                rules: [{ required: false, message: 'Please input weight!' }],
                initialValue: items.weight,
              })(<Input placeholder="weight" />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Grade</SetionTableLabel>}>
              {getFieldDecorator('grade', {
                rules: [{ required: false, message: 'Please input grade!' }],
                initialValue: items.grade,
              })(<Input placeholder="grade" />)}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Form.Item colon={false} label={<SetionTableLabel className="form-label">Packing</SetionTableLabel>}>
              {getFieldDecorator('packing', {
                rules: [{ required: false, message: 'Please input packing!' }],
                initialValue: items.packing,
              })(<Input placeholder="packing" />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              colon={false}
              label={<SetionTableLabel className="form-label">Shelf Life (Days)</SetionTableLabel>}
            >
              {getFieldDecorator('shelfLife', {
                rules: [{ required: false, message: 'Please input shelf life!' }],
                initialValue: items.shelfLife,
              })(<Input placeholder="shelf life" type="number" />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              colon={false}
              label={<SetionTableLabel className="form-label">Restocking Lead Time (Days)</SetionTableLabel>}
            >
              {getFieldDecorator('restockingTime', {
                rules: [{ required: false, message: 'Please input restocking time!' }],
                initialValue: items.restockingTime,
              })(<Input placeholder="restocking time" type="number" />)}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Flex style={{ alignItems: 'center' }}>
              <Form.Item
                colon={false}
                style={{ flex: 2 }}
                label={
                  <>
                      <SetionTableLabel className="form-label">Par level / Reorder Level</SetionTableLabel>
                      <Tooltip
                      <Tooltip
                        title="The PAR level or reorder point represents the lowest stock level that your available inventory can reach before the item needs to be reordered.">
                        <DefaultIcon type="info-circle" style={{ fontSize: 12, marginLeft: 8, marginTop: 13 }} />
                      </Tooltip>
                  </>
                }
              >
                {getFieldDecorator('reorderLevel', {
                  rules: [{ required: false, message: 'Please input reorder level!' }],
                  initialValue: items.reorderLevel,
                })(<InputNumber placeholder="reorder level" min={0} style={{ width: '100%' }} />)}{' '}
              </Form.Item>
              <div style={{ paddingTop: 20, marginLeft: 4 }}>{items.inventoryUOM}</div>
            </Flex>
          </Col>
          <Col span={8}>
            <Form.Item
              colon={false}
              label={<SetionTableLabel className="form-label">Product Manager</SetionTableLabel>}
            >
              {getFieldDecorator('managerId', {
                rules: [{ required: false, message: 'Please select Product Manager' }],
                initialValue: items.managerId > 0 ? items.managerId : '',
              })(
                <Select style={{ width: '100%' }} onChange={this.onManagerChange}>
                  {companyPmUsers &&
                    companyPmUsers.filter(v => v.role !== 'SUPERADMIN').map((item, index) => {
                      return (
                        <Select.Option key={index} value={item.userId}>
                          {item.firstName + '  ' + item.lastName}
                        </Select.Option>
                      )
                    })}
                </Select>,
              )}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              colon={false}
              label={
                <>
                  <SetionTableLabel className="form-label">PLU</SetionTableLabel>
                  <Tooltip title="Produce Price Look-Up: Usually a 4 for 5 digit number.">
                    <DefaultIcon type="info-circle" style={{ fontSize: 12, marginLeft: 8, marginTop: 13 }} />
                  </Tooltip>
                </>
              }
            >
              {getFieldDecorator('plu', {
                rules: [{ required: false, message: 'Please input grade!' }],
                initialValue: items.plu,
                // normalize: (value) => {
                //   value = value && value.replace(/[^\d]+/g, '')
                //   return value
                // },
              })(<Input placeholder="PLU" type="number" />)}
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={8}>
            <Flex style={{ alignItems: 'center' }}>
                <Form.Item
                  colon={false}
                  style={{ flex: 2 }}
                  label={
                    <>
                      <SetionTableLabel className="form-label">Reorder Quantity</SetionTableLabel>
                      <Tooltip
                        title='The reorder quantity is how many units make a "full shelf" of this item. This is the quantity you want to get back to when the available inventory goes below the PAR level.'>
                        <DefaultIcon type="info-circle" style={{ fontSize: 12, marginLeft: 8, marginTop: 13 }} />
                      </Tooltip>
                    </>
                  }
                >
                  {getFieldDecorator('reorderQuantity', {
                    rules: [{ required: false, message: 'Please input reorder quantity!' }],
                    initialValue: items.reorderQuantity,
                  })(<InputNumber placeholder="reorder quantity" min={0} style={{ width: '100%' }} />)}{' '}
                </Form.Item>
                <div style={{ paddingTop: 20, marginLeft: 4 }}>{items.inventoryUOM}</div>
              </Flex>
          </Col>
        </Row>
        <Button
          htmlType="submit"
          shape="round"
          size="large"
          type="primary"
          style={{ height: '40px', width: '180px', backgroundColor: brightGreen, borderColor: brightGreen }}
        >
          Save
        </Button>
        {/* </Popconfirm> */}
      </ThemeForm>
    )
  }
}

const ProductDetailsForm = Form.create()(ProductDetailsContainer)
export { ProductDetailsForm }
