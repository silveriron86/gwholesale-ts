import React from 'react'
import { ProductSection } from './product-section.component'
import { ProductDetailsForm } from './product-details-form-details.component'
import { Wrapper, Tip } from './product.component.style'

export class ProductDetails extends React.Component<T> {
  state = {
    radioData: [
      {
        label: 'Active',
        key: 'active',
        tip: 'Activating a product enables it to be sold, purchased, and reported',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Sell',
        key: 'sell',
        tip:
          'Determines whether your organization chooses to sell the product (vs store only or manufacture, for example)',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Purchase',
        key: 'purchase',
        tip: 'Flags product for repurchasing',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Manufacture',
        key: 'manufacture',
        tip: 'Determines whether product is internally manufactured, processed, or packed',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Promote',
        key: 'push',
        tip:
          'Promote flags product for quick sale or to sell out in a short amount of time, oftentimes at a discounted price',
        optionValues: ['YES', 'NO'],
      },
      { label: 'Organic', key: 'isOrganic', tip: '', optionValues: ['YES', 'NO'] },
    ],
  }

  render() {
    const { radioData } = this.state
    return (
      // <Wrapper>
      //   <Tip>Product Details</Tip>
      //   <ProductSection sectionKey={0} defaultExpanded={true}>
      //     <ProductDetailsForm radioData={radioData} {...this.props} />
      //    </ProductSection>
      //   {/* <ProductSection sectionKey={1} title="Details" />
      //   <ProductSection sectionKey={2} title="Specifications" />
      //   <ProductSection sectionKey={3} title="Freezer Locations" />
      //   <ProductSection sectionKey={4} title="Cooler Locations" /> */}
      // </Wrapper>
      <Wrapper>
        <Tip>Product Details</Tip>
        <ProductDetailsForm {...this.props} />
      </Wrapper>
    )
  }
}
