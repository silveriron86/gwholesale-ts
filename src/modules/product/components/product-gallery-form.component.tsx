import { Icon, Upload, message, notification } from 'antd'

import { CACHED_ACCESSTOKEN } from '~/common'
import { Document } from '~/schema'
import Modal from 'antd/lib/modal/Modal'
import React from 'react'
import { UploadFile } from 'antd/lib/upload/interface'

interface ProductGalleryFormProps {
}

export class ProductGalleryForm extends React.PureComponent<ProductGalleryFormProps> {

  state = {
    previewVisible: false,
    previewImage: '',
    fileList:[],
  };

  componentDidMount(): void {

  }


  handleCancel = () => this.setState({ previewVisible: false });
  getBase64 =(file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }
  handlePreview = async file => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
    });
  };
  handleRemove = file => {
    if(file.status === "removed"){
      message.success(`file removed successfully.`)
      this.props.removeGalleryDocument({"productId":this.props.match.params.id,"documentId":file.uid})
    }

    // const { status } = file

  }
  handleSuccess = response =>{
    if (response.statusCodeValue == 200) {
      this.props.getGalleryDocuments(this.props.match.params.id);
    } else{
      notification['error']({
        message: 'Error',
        description: `File upload failed`,
      })
    }
  }

  render() {
    const { previewVisible, previewImage } = this.state;
    this.setState({
      fileList:this.props.documents
    })
    console.log(this.props.galleryDocuments)
    return (
      <div className="clearfix">
        <Upload

          action = {process.env.WSW2_HOST + "/api/v1/wholesale/file/document/product/"+this.props.match.params.id}
          headers= {{
            'Authorization': `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN)}`,
          }}
          listType="picture-card"
          fileList={this.props.galleryDocuments}
          onPreview={this.handlePreview}
          onChange={this.handleRemove}
          onSuccess={this.handleSuccess}
        >
          <div>
            <Icon type="plus" />
            <div className="ant-upload-text">Upload</div>
          </div>
        </Upload>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: '100%' }} src={`${previewImage}`} />
        </Modal>
      </div>
    );
  }
}
