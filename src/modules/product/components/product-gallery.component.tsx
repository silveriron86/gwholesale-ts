import React from 'react'
import { ProductGalleryForm } from './product-gallery-form.component'
import { Wrapper, Tip } from './product.component.style'
import { Document } from '~/schema'

export interface ProductGalleryProps {
}

export class ProductGallery extends React.PureComponent<ProductGalleryProps> {
  state = {}
  render() {
    return (
      <Wrapper>
        <Tip>Gallery</Tip>
        <ProductGalleryForm {...this.props}/>
      </Wrapper>
    )
  }
}
