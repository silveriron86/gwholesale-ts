import React from 'react'
import { Button, Col, Divider, Form, Icon, Row, Select } from 'antd'
import {
  Wrapper,
  TipText,
  SetionTableLabel,
} from './product.component.style'
import { ThemeForm, ThemeInput, ThemeInputNumber, ThemeModal, ThemeSelect } from '~/modules/customers/customers.style'
import { LogisticWrapper } from '../product.style'
import { brightGreen } from '~/common'
import { ProductDispatchProps, ProductProps } from '..'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import { getDefaultUnit } from '~/common/utils'
import _ from 'lodash'
type ProductLogisticsProps = ProductProps & ProductDispatchProps

class ProductLogistics extends React.Component<ProductLogisticsProps> {
  state: any = {
    visibleGrossWeightUnit: false,
    visibleGrossVolumeUnit: false
  }
  constructor(props: any) {
    super(props)
  }

  componentDidMount() {
    if (!this.props.companyProductTypes) {
      this.props.getCompanyProductAllTypes()
    }
    this.props.getItemLocations()
  }

  onSave = (e: any) => {
    e.preventDefault()
    const { categories, form, items, itemLocations } = this.props
    const { wholesaleItemId, qboId } = items

    form.validateFields((err, values) => {
      if (!err) {
        console.log('values, items', values, items)
        if (values.locationId) {
          const selectedLocation = itemLocations.find(el => el.id == values.locationId)
          values.itemLocation = selectedLocation
          delete values.locationId
        } else {
          values.itemLocation = null
        }
        const newData = { ...items, ...values, quantity: null, wholesaleItemId: wholesaleItemId, qboId }
        this.props.updateItems({data: newData, queryParams: {ignoreSync: true}})
      }
    })
  }

  openCompanySettingChange = (type: string) => {
    let obj = { ...this.state }
    obj[type] = !obj[type]
    console.log(obj)
    this.setState({ ...obj })
  }

  render() {
    const { items, companyProductTypes, userSetting, itemLocations, form: { getFieldDecorator } } = this.props
    const { visibleGrossVolumeUnit, visibleGrossWeightUnit } = this.state
    const sortedLocations = _.orderBy(itemLocations, "name", "asc")
    const defaultLocation = itemLocations.find(el => items.itemLocation && items.itemLocation.id == el.id)
    return (
      <Wrapper>
        <TipText>Logistics</TipText>
        <ThemeForm onSubmit={this.onSave} className="simple-input-wrapper">
          <LogisticWrapper style={{ width: '50%' }}>
            <TipText style={{ fontWeight: 500, textAlign: 'left' }}>Units Volume and Weights</TipText>
            <Row style={{ marginTop: 20 }}>
              <Col span={12}>
                <Form.Item className="form-item" label={<SetionTableLabel>Gross Weight</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('grossWeight', {
                    initialValue: items ? items.grossWeight : '',
                  })(
                    <ThemeInput />,
                  )}
                </Form.Item>
              </Col>
              <Col span={8} offset={1}>
                <Form.Item className="form-item" label={<SetionTableLabel>Weight units</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('grossWeightUnit', {
                    initialValue: items && items.grossWeightUnit ? items.grossWeightUnit : getDefaultUnit('WEIGHT', userSetting)
                  })(
                    <ThemeSelect
                      placeholder="Select an option"
                      disabled
                      filterOption={(input, option) => {
                        return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }}
                      dropdownRender={(menu) => (
                        <>
                          {menu}
                          {/* <Divider style={{ margin: '4px 0' }} />
                          <div
                            style={{ padding: '4px 8px', cursor: 'pointer' }}
                            onMouseDown={(e) => e.preventDefault()}
                            onClick={(e) => this.openCompanySettingChange('visibleGrossWeightUnit')}
                          >
                            <Icon type="plus" /> Add New ...
                          </div> */}
                        </>
                      )}
                    >
                      <Select.Option value="LB">LB</Select.Option>
                      <Select.Option value="KG">KG</Select.Option>
                      {
                        companyProductTypes && companyProductTypes.grossWeightUnit.length
                          ? companyProductTypes.grossWeightUnit.map((el: any, index: number) => {
                            return (<Select.Option key={`${el.name}-${index}`} value={el.name}>{el.name}</Select.Option>)
                          })
                          : []
                      }
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Form.Item className="form-item" label={<SetionTableLabel>Gross Volume</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('grossVolume', {
                    initialValue: items ? items.grossVolume : ''
                  })(
                    <ThemeInput />,
                  )}
                </Form.Item>
              </Col>
              <Col span={8} offset={1}>
                <Form.Item className="form-item" label={<SetionTableLabel>Volume units</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('grossVolumeUnit', {
                    initialValue: items && items.grossVolumeUnit ? items.grossVolumeUnit : getDefaultUnit('VOLUME', userSetting),
                  })(
                    <ThemeSelect
                      placeholder="Select an option"
                      disabled
                      filterOption={(input, option) => {
                        return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }}
                      dropdownRender={(menu) => (
                        <>
                          {menu}
                          {/* <Divider style={{ margin: '4px 0' }} />
                          <div
                            style={{ padding: '4px 8px', cursor: 'pointer' }}
                            onMouseDown={(e) => e.preventDefault()}
                            onClick={(e) => this.openCompanySettingChange('visibleGrossVolumeUnit')}
                          >
                            <Icon type="plus" /> Add New ...
                          </div> */}
                        </>
                      )}
                    >
                      <Select.Option value="Cubic Ft.">Cubic Ft.</Select.Option>
                      <Select.Option value="Cubic M.">Cubic M.</Select.Option>
                      {
                        companyProductTypes && companyProductTypes.grossVolumeUnit.length ? companyProductTypes.grossVolumeUnit.map((el: any, index: number) => {
                          return (<Select.Option key={`${el.name}-${index}`} value={el.name}>{el.name}</Select.Option>)
                        }) : []
                      }
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </LogisticWrapper>

          <LogisticWrapper style={{ width: '50%' }}>
            <TipText style={{ fontWeight: 500, textAlign: 'left' }}>Palletization</TipText>
            <Row style={{ marginTop: 20 }}>
              <Col span={12}>
                <Form.Item className="form-item" label={<SetionTableLabel>Units per Pallet</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('unitPerPallet', {
                    initialValue: items ? items.unitPerPallet : '',
                  })(
                    <ThemeInput />,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Form.Item className="form-item" label={<SetionTableLabel>Units per Layer</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('unitPerLayer', {
                    initialValue: items ? items.unitPerLayer : '',
                  })(
                    <ThemeInput />,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Form.Item className="form-item" label={<SetionTableLabel>Layers per Pallet</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('layersPerPallet', {
                    initialValue: items ? items.layersPerPallet : '',
                  })(
                    <ThemeInput />,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </LogisticWrapper>

          <LogisticWrapper style={{ width: '50%' }}>
            <TipText style={{ fontWeight: 500, textAlign: 'left' }}>Barcoding</TipText>
            <Row style={{ marginTop: 20 }}>
              <Col span={12}>
                <Form.Item className="form-item" label={<SetionTableLabel>GTIN</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('gtin', {
                    initialValue: items ? items.gtin : '',
                  })(
                    <ThemeInput />,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </LogisticWrapper>

          <LogisticWrapper style={{ width: '50%' }}>
            <TipText style={{ fontWeight: 500, textAlign: 'left' }}>Commodity</TipText>
            <Row style={{ marginTop: 20 }}>
              <Col span={12}>
                <Form.Item className="form-item" label={<SetionTableLabel>Commodity Class</SetionTableLabel>} colon={false}>
                  {getFieldDecorator('commodityClass', {
                    initialValue: items ? items.commodityClass : '',
                  })(
                    <ThemeInputNumber />,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </LogisticWrapper>
          {
            userSetting && userSetting.company && userSetting.company.locationMethod == 1 &&
            <LogisticWrapper style={{ width: '50%' }}>
              <TipText style={{ fontWeight: 500, textAlign: 'left' }}>Location</TipText>
              <Row style={{ marginTop: 20 }}>
                <Col span={12}>
                  <Form.Item className="form-item" label={<SetionTableLabel>Location</SetionTableLabel>} colon={false}>
                    {getFieldDecorator('locationId', {
                      initialValue: defaultLocation ? defaultLocation.id : undefined,
                    })(
                      <ThemeSelect
                        placeholder="Select location"
                        showSearch
                        allowClear
                        filterOption={(input: any, option: any) => {
                          return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }}
                        dropdownRender={(menu: any) => (
                          <>
                            {menu}
                          </>
                        )}
                      >
                        {
                          sortedLocations.map((el: any, index: number) => {
                            return (<Select.Option key={`${index}`} value={el.id}>{el.name}</Select.Option>)
                          })
                        }
                      </ThemeSelect>,
                    )}
                  </Form.Item>
                </Col>
              </Row>
            </LogisticWrapper>
          }
          <div style={{ textAlign: 'left', marginTop: 20 }}>
            <Button
              htmlType="submit"
              shape="round"
              size="large"
              type="primary"
              style={{ height: '40px', width: '180px', backgroundColor: brightGreen, borderColor: brightGreen }}
            >
              Save
            </Button>
          </div>
        </ThemeForm>
        <ThemeModal
          title={`Edit Value List "Gross Weight Units"`}
          visible={visibleGrossWeightUnit}
          onCancel={this.openCompanySettingChange.bind(this, 'visibleGrossWeightUnit')}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="grossWeightUnit" title="Gross Weight Unit" buttonTitle="Add New Gross Weight Unit" />
        </ThemeModal>
        <ThemeModal
          title={`Edit Value List "Gross Volume Units"`}
          visible={visibleGrossVolumeUnit}
          onCancel={this.openCompanySettingChange.bind(this, 'visibleGrossVolumeUnit')}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="grossVolumeUnit" title="Gross Volume Unit" buttonTitle="Add New Gross Volume Unit" />
        </ThemeModal>
      </Wrapper>
    )
  }
}

export default Form.create<any>()(ProductLogistics)
