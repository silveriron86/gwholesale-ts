import React from 'react'
import { DatePicker, Table, Tooltip } from 'antd'
import moment from 'moment'
import { Icon as IconSvg } from '~/components'
import {
  Wrapper,
  TipWithChildren,
  TipText,
  TipDatePicker,
  TipAction,
  TipDatePickerText,
  TipDatePickerWrapper,
  TableTip,
} from './product.component.style'
import { formatDate, formatNumber, mathRoundFun, basePriceToRatioPrice, ratioPriceToBasePrice } from '~/common/utils'
export class ProductLots extends React.Component<T> {
  state: any
  constructor(props) {
    super(props)
    const id = this.props.match.params.id
    const sessionSearch = localStorage.getItem(`PRODUCT_LOTS_${id}_SEARCH`)
    const urlParams = new URLSearchParams(sessionSearch ? sessionSearch : props.location.search)
    const startDate = urlParams.get('datestart')
    const endDate = urlParams.get('dateend')
    const page = urlParams.get('page')
    const from = startDate ? moment(startDate, 'MMDDYY').format('MM/DD/YYYY') : `01/01/${new Date().getFullYear()}`
    const to = endDate
      ? moment(endDate, 'MMDDYY').format('MM/DD/YYYY')
      : moment()
        .add(1, 'month')
        .format('MM/DD/YYYY')
    this.state = {
      page: page ? parseInt(page, 10) : 0,
      dateRange: {
        from,
        to,
      },
    }
  }

  componentDidMount() {
    const id = this.props.match.params.id
    this.props.getLots({ ...this.state.dateRange, id })
    const sessionSearch = localStorage.getItem(`PRODUCT_LOTS_${id}_SEARCH`)
    if (sessionSearch) {
      setTimeout(() => {
        this.updateURL()
      }, 100)
    }
  }

  onDateStartChange = (_: any, dateString: string) => {
    const { dateRange } = this.state
    const { getLots, match } = this.props
    const newObj = { ...dateRange, from: dateString }
    this.setState({ dateRange: newObj }, () => {
      this.updateURL()
    })
    getLots({ ...newObj, id: match.params.id })
  }

  onDateEndChange = (_: any, dateString: string) => {
    const { dateRange } = this.state
    const { getLots, match } = this.props
    const newObj = { ...dateRange, to: dateString }
    this.setState({ dateRange: newObj }, () => {
      this.updateURL()
    })
    getLots({ ...newObj, id: match.params.id })
  }

  updateURL = () => {
    var url = new URL(window.location.href)
    var queryParams = url.searchParams
    const { dateRange, page } = this.state
    queryParams.set('datestart', moment(dateRange.from).format('MMDDYY'))
    queryParams.set('dateend', moment(dateRange.to).format('MMDDYY'))
    if (page) {
      queryParams.set('page', page)
    }
    url.search = queryParams.toString()
    const id = this.props.match.params.id
    localStorage.setItem(`PRODUCT_LOTS_${id}_SEARCH`, url.search)
    window.history.replaceState({}, '', `#${this.props.location.pathname}?` + queryParams.toString())
  }

  onChangePage = (page, pageSize) => {
    this.setState(
      {
        page: page - 1,
      },
      () => {
        this.updateURL()
      },
    )
  }

  moveToPurchaseOrder = (record: any) => {
    this.props.history.push(`/order/${record.wholesaleOrderId}/purchase-cart`)
  }

  render() {
    const { lots = [] } = this.props

    const sortedLots = lots.sort((a, b) => b.deliveryDate - a.deliveryDate)

    return (
      <Wrapper style={{minWidth: '100%', width: 'fit-content'}}>
        <TipWithChildren>
          <TipText>Lots</TipText>
          <TipDatePicker>
            <TipDatePickerText style={{ marginRight: '30px' }}>Records Form</TipDatePickerText>
            {this.renderDateEditor('from', this.onDateStartChange)}
            <TipDatePickerText style={{ margin: '0 20px' }}>to</TipDatePickerText>
            {this.renderDateEditor('to', this.onDateEndChange)}
          </TipDatePicker>
          <TipAction />
        </TipWithChildren>
        <TableTip style={{ margin: '0 20px' }}>Lots</TableTip>
        <Table
          style={{ margin: '0 20px' }}
          size="small"
          className="no-top-border"
          pagination={{ pageSize: 10 }}
          dataSource={sortedLots}
          columns={this.tableColumns()}
          onRowClick={(record: T, index: number, event: Event) => this.moveToPurchaseOrder(record)}
          pagination={{ current: this.state.page + 1, defaultCurrent: 0, onChange: this.onChangePage }}
        />
      </Wrapper>
    )
  }

  private tableColumns() {
    const width = 110
    const widthBig = 220
    return [
      {
        title: 'DELIVERY DATE',
        dataIndex: 'quotedDate',
        key: 'quotedDate',
        width,
        render: (value: number, record: any) => {
          return value ? formatDate(value) : formatDate(record.deliveryDate)
        },
      },
      {
        title: 'RECEIVED DATE',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        width,
        render: (value: number) => {
          const year = moment.utc(value).get('year')
          // product Initial quantity default po deliverydate is 01/01/1990  don't need show this time
          if (year != 1990) {
            return formatDate(value)
          }
        },
      },
      {
        title: 'VENDOR',
        dataIndex: 'wholesaleClient.clientCompany.companyName',
        key: 'wholesaleClient.clientCompany.companyName',
        width: widthBig,
      },
      {
        title: 'BRAND',
        className: 'th-left',
        dataIndex: 'modifiers',
        width
      },
      {
        title: 'ORIGIN',
        className: 'th-left',
        dataIndex: 'extraOrigin',
        width
      },
      {
        title: 'LOT #',
        dataIndex: 'lotId',
        key: 'lotId',
        width,
      },
      {
        title: 'UNITS ON HAND',
        dataIndex: 'onHandQty',
        key: 'onHandQty',
        width,
        render: (value: number) => {
          return mathRoundFun(value, 2)
        },
      },
      {
        title: 'UNITS RECEIVED',
        dataIndex: 'receivedQty',
        key: 'receivedQty',
        width,
        render: (value: number, record: any) => {
          return `${mathRoundFun(value, 2)} ${record.inventoryUOM}`
        },
      },
      {
        title: 'COST',
        dataIndex: 'cost',
        key: 'cost',
        width,
        render: (value: number, record: any) => {
          let item = record.wholesaleItem
          value = value ? value : 0
          value = value + record.perAllocateCost
          if (record.pricingUOM) {
            return `$${basePriceToRatioPrice(record.pricingUOM, value, item, 2)}/${record.pricingUOM}`
          } else {
            return `$${mathRoundFun(value, 2)}/${item ? item.baseUOM : ''}`
          }
        },
      },
      {
        title: 'NOTES FROM PO',
        dataIndex: 'note',
        key: 'note',
        width: widthBig,
        render: (note: string) => {
          return (
            <Tooltip title={note && note.length > 16 ? note.substr(0, 60) : ''}>
              <span>{note && note.length > 16 ? note.substr(0, 16) + '...' : note}</span>
            </Tooltip>
          )
        },
      },
      {
        title: 'AVAILABLE TO SELL',
        dataIndex: 'lotAvailableQty',
        key: 'lotAvailableQty',
        width: 50,
        render: (value: number, record: any) => {
          return `${mathRoundFun(value, 2)}`
        },
      },
      {
        title: 'UNITS INCOMING',
        dataIndex: 'qtyConfirmed',
        key: 'qtyConfirmed',
        width: 50,
        render: (t, r) => r.wholesaleOrderItemStatus === 'CONFIRMED' ? t : 0,
      },
      {
        title: 'UNITS COMMITTED',
        dataIndex: 'lotCommitedQty',
        key: 'lotCommitedQty',
        width: 50,
        render: (value: number, record: any) => {
          return `${mathRoundFun(value, 2)}`
        },
      },
    ]
  }

  private renderDateEditor(which: string, onChange: any) {
    const { dateRange } = this.state
    const calendarIcon = <IconSvg type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    return (
      <TipDatePickerWrapper>
        <DatePicker
          placeholder={dateFormat}
          format={dateFormat}
          suffixIcon={calendarIcon}
          defaultValue={which === 'from' ? moment(dateRange.from, 'MM/DD/YYYY') : moment(dateRange.to, 'MM/DD/YYYY')}
          onChange={onChange}
          allowClear={false}
        />
      </TipDatePickerWrapper>
    )
  }
}
