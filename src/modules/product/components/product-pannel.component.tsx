import React from 'react'
import { Icon } from '~/components'
import { Tooltip } from 'antd'
import {
  PanelWrapper,
  PanelLabel,
  PanelTitle,
  PanelContent,
  PanelItem,
  PanelItemLabel,
  PanelItemContent,
} from '../product.style'
import Moment from 'react-moment'
import {
  formatNumber,
  combineUomForInventory,
  inventoryQtyToRatioQty,
  mathRoundFun,
  handlerNoLotNumber,
} from '~/common/utils'
import { textCenter } from '~/modules/customers/customers.style'
import InventoryAsterisk from '~/components/inventory/inventoryAsterisk'
import { isArray } from 'lodash'

export class ProductPanel extends React.PureComponent<T> {
  state = {
    userSetting: this.props.userSetting
  }
  componentDidMount() {
    this.props.getItemLocations()

    const { id } = this.props.match.params
    this.props.getPalletLocations(id)
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.userSetting) != JSON.stringify(nextProps.userSetting)) {
      this.setState({
        userSetting: nextProps.userSetting
      })
    }
  }

  getNoLotCommittedDom = (showUOM?: any) => {
    const { items } = this.props
    return items.noLotCommittedQty > 0 ? (
      <>
        <Tooltip title={'Total units committed without Lot # assigned.'} placement="bottom">
          &nbsp;({items.noLotCommittedQty})&nbsp;
        </Tooltip>
        {showUOM ? items.inventoryUOM : ''}
      </>
    ) : (
      ''
    )
  }
  getNoLotLotOnHandDom = (showUOM?: any) => {
    const { items } = this.props
    return items.noLotOnHandQty > 0 ? (
      <>
        <Tooltip title={'Total units committed without Lot # assigned.'} placement="bottom">
          &nbsp;({items.noLotOnHandQty})&nbsp;
        </Tooltip>
        {showUOM ? items.inventoryUOM : ''}
      </>
    ) : (
      ''
    )
  }

  getItemLocation = () => {
    const { items, palletLocations } = this.props
    const { userSetting } = this.state
    if (userSetting && userSetting.company) {
      if (userSetting.company.locationMethod == 1) {
        return items.itemLocation ? items.itemLocation.name : ''
      } else {
        const itemNames = palletLocations.map(el => el.name).join(', ')
        return itemNames
      }
    } else {
      return ''
    }
  }

  render() {
    const { items } = this.props
    const location = this.getItemLocation()
    return (
      <PanelWrapper>
        <PanelLabel>PRODUCT NAME</PanelLabel>
        <PanelTitle>{items.variety}</PanelTitle>
        <PanelContent>
          <PanelItem>
            <PanelItemLabel>
              {/* <Tooltip title="this is tooltip"> */}
              SKU
              {/* <Icon type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} /> */}
              {/* </Tooltip> */}
            </PanelItemLabel>
            <PanelItemContent style={textCenter}>{items.sku}&nbsp;</PanelItemContent>
          </PanelItem>

          <PanelItem>
            <PanelItemLabel>
              <Tooltip title="Sum of quantity present in warehouse that are being used for delivery and of incoming from supplier in purchase order that has not yet been received.">
                Available To Sell
                <Icon type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
              </Tooltip>
            </PanelItemLabel>
            <PanelItemContent style={textCenter}>
              {isArray(items.wholesaleProductUomList) &&
                items.wholesaleProductUomList.filter((uom) => uom.useForInventory && !uom.deletedAt).length > 0 ? (
                <>
                  <Tooltip
                    title={
                      <>
                        {items.wholesaleProductUomList.map((uom: any) => {
                          if (uom.useForInventory && !uom.deletedAt) {
                            return (
                              <div>
                                {inventoryQtyToRatioQty(uom.name, handlerNoLotNumber(items, 1), items, 2)}&nbsp;
                                {uom.name}
                              </div>
                            )
                          }
                        })}
                      </>
                    }
                  >
                    <span>
                      {mathRoundFun(handlerNoLotNumber(items, 1), 2)}&nbsp;{items.inventoryUOM}
                    </span>
                  </Tooltip>
                  <InventoryAsterisk item={items} flag={1}></InventoryAsterisk>
                </>
              ) : (
                <span>
                  {mathRoundFun(handlerNoLotNumber(items, 1), 2)}&nbsp;{items.inventoryUOM}
                  <InventoryAsterisk item={items} flag={1}></InventoryAsterisk>
                </span>
              )}
            </PanelItemContent>
          </PanelItem>

          <PanelItem>
            <PanelItemLabel>
              <Tooltip title="Quantity physically present in warehouse.  Useful for sales allocations, inventory counting, product traceability">
                Units On Hand
                <Icon type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
              </Tooltip>
            </PanelItemLabel>
            <PanelItemContent style={textCenter}>
              {isArray(items.wholesaleProductUomList) &&
                items.wholesaleProductUomList.filter((uom) => uom.useForInventory && !uom.deletedAt).length > 0 ? (
                <>
                  <Tooltip
                    title={
                      <>
                        {items.wholesaleProductUomList.map((uom: any) => {
                          if (uom.useForInventory && !uom.deletedAt) {
                            return (
                              <div>
                                {inventoryQtyToRatioQty(uom.name, handlerNoLotNumber(items, 2), items, 2)}&nbsp;
                                {uom.name}
                              </div>
                            )
                          }
                        })}
                      </>
                    }
                  >
                    <span>
                      {mathRoundFun(handlerNoLotNumber(items, 2), 2)}&nbsp;
                      {items.inventoryUOM}
                    </span>
                  </Tooltip>
                  <InventoryAsterisk item={items} flag={2}></InventoryAsterisk>
                </>
              ) : (
                <span>
                  {mathRoundFun(handlerNoLotNumber(items, 2), 2)}&nbsp;
                  {items.inventoryUOM}
                  <InventoryAsterisk item={items} flag={2}></InventoryAsterisk>
                </span>
              )}
            </PanelItemContent>
          </PanelItem>

          <PanelItem>
            <PanelItemLabel>
              <Tooltip title="Quantity that has been confirmed from a supplier in a purchase order that has not yet been received.  Useful for both sales and warehouse to determine what quantities are expected to arrive in the near future">
                Units Incoming
                <Icon type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
              </Tooltip>
            </PanelItemLabel>
            <PanelItemContent style={textCenter}>
              {items.incomingQty > 0 ? (
                items.wholesaleProductUomList.filter((uom) => uom.useForInventory && !uom.deletedAt).length > 0 ? (
                  <Tooltip
                    title={
                      <>
                        {items.wholesaleProductUomList.map((uom: any) => {
                          if (uom.useForInventory && !uom.deletedAt) {
                            return (
                              <div>
                                {inventoryQtyToRatioQty(uom.name, items.incomingQty, items, 2)}&nbsp;{uom.name}
                              </div>
                            )
                          }
                        })}
                      </>
                    }
                  >
                    <span style={{ marginRight: '20px' }}>
                      {mathRoundFun(items.incomingQty, 2)}&nbsp;{items.inventoryUOM}
                    </span>
                  </Tooltip>
                ) : (
                  <span style={{ marginRight: '20px' }}>
                    {mathRoundFun(items.incomingQty, 2)}&nbsp;{items.inventoryUOM}
                  </span>
                )
              ) : (
                '--'
              )}
            </PanelItemContent>
          </PanelItem>

          <PanelItem>
            <PanelItemLabel>
              <Tooltip title="Quantity that has been ordered but not yet shipped. Useful for sales to determine how many units are already designated for customers.">
                Units committed
                <Icon type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
              </Tooltip>
            </PanelItemLabel>
            <PanelItemContent style={textCenter}>
              {isArray(items.wholesaleProductUomList) &&
                items.wholesaleProductUomList.filter((uom) => uom.useForInventory && !uom.deletedAt).length > 0 ? (
                <>
                  <Tooltip
                    title={
                      <>
                        {items.wholesaleProductUomList.map((uom: any) => {
                          if (uom.useForInventory && !uom.deletedAt) {
                            return (
                              <div>
                                {inventoryQtyToRatioQty(uom.name, handlerNoLotNumber(items, 3), items, 2)}&nbsp;
                                {uom.name}
                              </div>
                            )
                          }
                        })}
                      </>
                    }
                  >
                    <span>
                      {mathRoundFun(handlerNoLotNumber(items, 3), 2)}&nbsp;
                      {items.inventoryUOM}
                    </span>
                  </Tooltip>
                  <InventoryAsterisk item={items} flag={3}></InventoryAsterisk>
                </>
              ) : (
                <span>
                  {mathRoundFun(handlerNoLotNumber(items, 3), 2)}&nbsp;
                  {items.inventoryUOM}
                  <InventoryAsterisk item={items} flag={3}></InventoryAsterisk>
                </span>
              )}
            </PanelItemContent>
          </PanelItem>

          <PanelItem>
            <PanelItemLabel>Location</PanelItemLabel>
            <PanelItemContent style={textCenter}>
              <Tooltip title={location.length > 20 ? location : ''}>
                <span>{location.length > 20 ? location.substring(0, 20) + '...' : location ? location : '--'}</span>
              </Tooltip>
            </PanelItemContent>
          </PanelItem>

          {/* <PanelItem >
            <PanelItemLabel>
              <Tooltip title="Cost that is picked for sales orders.  Useful for sales to determine how much it will take for units that are already picked and designated for customers">
                Unit Cost
                <Icon type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
              </Tooltip>
            </PanelItemLabel>
            <PanelItemContent style={textCenter}>{items.cost>0?items.cost:'--'}</PanelItemContent>
          </PanelItem> */}
        </PanelContent>
      </PanelWrapper>
    )
  }
}
