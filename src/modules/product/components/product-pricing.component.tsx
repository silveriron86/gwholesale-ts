import React from 'react'
import {
  Wrapper,
  Tip,
  TipWithChildren,
  TipText,
  TipAction,
  PricingGroupTableWrapper,
  ProductBlock,
  ProductTitle,
  ProductTitleTips,
  TipSmallText,
  ProductStrategy,
  ProductGroupBlock,
  ThemeInputNumber,
} from './product.component.style'
import {
  ThemeButton,
  ThemeOutlineButton,
  ThemeCheckbox,
  ThemeInput,
  ThemeTable,
  ThemeSelect,
  ThemeLink,
  ThemeRadio,
} from '~/modules/customers/customers.style'
import { Icon, Popconfirm, Tooltip, Form, Select, Row, Col, Radio } from 'antd'
import RowEditableTable from './pricing-group-table'
import moment, { Moment } from 'moment'
import { PricingTableWrapper } from '../product.style'
import { cloneDeep, isArray, isNumber } from 'lodash'
import { Icon as IconLocal } from '~/components'
import { formatNumber, formatDate, mathRoundFun, basePriceToRatioPrice } from '~/common/utils'
import { FormComponentProps } from 'antd/es/form'
import GrubSuggest from '~/components/GrubSuggest'
import { WholesaleProductUom } from '~/schema'
import { ProductChangeUOMForm } from './product-change-UOM.component'
import { CACHED_NS_LINKED } from '~/common'

interface ProductPricingProps extends FormComponentProps {
  items: any
  savePricingLoading: Boolean
  updateProductPricing: Function
}

export class ProductPricing extends React.Component<T> {
  state = {
    selectedLogic: '',
    selectedGroup: '',
    dataSource: [],
    defaultLogicAndGroup: {},
    editable: false,
    price: null,
    cost: null,
    page: 0,
    pageSize: 5,
    searchKey: '',
    total: 11,
  }

  componentDidMount() {
    const { items } = this.props
    const { id } = this.props.match.params
    if (Object.keys(items).length == 0) {
      this.props.getItems(id)
    } else {
      this.initTable(items)
    }
    this.props.getProductPurchasePriceList({
      itemId: id,
      page: this.state.page,
      pageSize: this.state.pageSize,
    })
  }

  componentWillReceiveProps(nextProps: any) {
    // if(Object.keys(this.props.items).length == 0 && Object.keys(nextProps.items).length > 0) {
    if (
      (Object.keys(nextProps.items).length > 0 && this.state.dataSource.length == 0) ||
      (Object.keys(nextProps.items).length > 0 && nextProps.items.cost != this.props.items.cost)
    ) {
      this.initTable(nextProps.items)
    }
  }

  initTable = (items: any) => {
    const groups = ['A', 'B', 'C', 'D', 'E']
    let result: any[] = []
    groups.forEach((el) => {
      result.push({
        level: el,
        cost: items.cost,
        low: items[`low${el}`],
        high: items[`high${el}`],
        updatedDate: moment(items.updatedDate).format('MM/DD/YYYY h:mm A'),
        margin: items[`margin${el}`],
        markup: items[`markup${el}`],
        customSalesPrice: items['cost'], //define customSalesPrice field to only show price in the table
      })
    })
    console.log(result)

    this.setState({
      dataSource: result,
      price: items['price'],
      cost: items['cost'],
      selectedLogic: items['defaultLogic'],
      selectedGroup: items['defaultGroup'],
    })
  }

  onGroupSelectionChanged = (value: string) => {
    const { items } = this.props
    // let data = {
    //   id: items.wholesaleItemId,
    //   info: {
    //     defaultLogic: 'FOLLOW_GROUP',
    //     defaultGroup: value
    //   }
    // }
    let data = {}
    if (this.state.selectedLogic) {
      data = {
        id: items.wholesaleItemId,
        info: {
          defaultLogic: this.state.selectedLogic,
          defaultGroup: value,
        },
      }
    }

    this.setState({
      selectedGroup: value,
      defaultLogicAndGroup: data,
    })
    // this.props.setDefaultLogicAndGroup(data)
  }

  onLogicSelectionChanged = (value: string) => {
    const { items } = this.props
    let data = {
      id: items.wholesaleItemId,
      info: {
        defaultLogic: value,
        defaultGroup: this.state.selectedGroup,
      },
    }
    // if (value != 'GROUP') {
    //   data = {
    //     id: items.wholesaleItemId,
    //     info: {
    //       defaultLogic: 'FOLLOW_' + value,
    //     }
    //   }
    // }

    this.setState({
      selectedLogic: value,
      defaultLogicAndGroup: data,
    })
  }

  onClickEdit = () => {
    this.setState({ editable: !this.state.editable })
  }

  onSaveChanges = () => {
    const { items } = this.props
    const { defaultLogicAndGroup, dataSource, price, cost, selectedLogic } = this.state

    let item: any = { ...items }
    item = { ...item, cost: cost }
    if (defaultLogicAndGroup) {
      let data: any = defaultLogicAndGroup
      if (!data.id) {
        data = { ...data, id: items.wholesaleItemId }
      }
      if (!data.info) {
        data = { ...data, info: { defaultGroup: items.defaultGroup, defaultLogic: items.defaultLogic } }
      }
      if (data.info) {
        item = { ...item, ...data.info }
      }
      this.props.setDefaultLogicAndGroup(data)
    }

    if (this.isEdited()) {
      let data: any = {
        id: items.wholesaleItemId,
        info: {},
      }

      let pricing = {}
      dataSource.forEach((el: any) => {
        pricing[`low${el.level}`] = el.low
        pricing[`high${el.level}`] = el.high
        pricing[`margin${el.level}`] = el.margin
        pricing[`markup${el.level}`] = el.markup
      })
      data = { ...data, info: pricing }
      console.log(data)
      item = { ...item, ...data.info }
      setTimeout(() => {
        this.props.updateItemPricingGroup(data)
      }, 500)
    }

    if ((selectedLogic === 'DEFAULT_SALES' || selectedLogic === 'FOLLOW_DEFAULT_SALES') && price !== null) {
      item = { ...item, price: price }
    }
    //because if quantity set, backend will add new lot
    item.quantity = null
    console.log('updating item', item)
    this.props.updateItems({data: item, queryParams: {}})
    this.setState({ editable: false })
  }

  isEdited = () => {
    const { items } = this.props
    const { dataSource } = this.state
    let result = false
    dataSource.forEach((el: any, i: number) => {
      if (
        items[`low${el.level}`] != el.low ||
        items[`high${el.level}`] != el.high ||
        items[`margin${el.level}`] != el.margin ||
        items[`markup${el.level}`] != el.markup
      ) {
        result = true
      }
    })
    return result
  }

  onChangeValues = (key: string, level: string, value: number) => {
    const { dataSource } = this.state
    let cloneData: any[] = cloneDeep(dataSource)
    let item: any = dataSource.find((el: any) => el.level == level)
    if (item) {
      item[key] = value
      if (key == 'customSalesPrice') {
        item['margin'] =
          value > 0 && item['cost'] > 0 && value > item['cost']
            ? Math.round(((value - item['cost']) / value) * 100 * 100) / 100
            : 0
      } else if (key == 'margin') {
        item['customSalesPrice'] =
          value > 0 && item['cost'] > 0 ? Math.round((item['cost'] / (1 - value / 100)) * 100) / 100 : 0
      }
      let index = 0
      cloneData.forEach((el: any, i: number) => {
        if (el.level == level) {
          index = i
          return
        }
      })
      cloneData[index] = item
    }

    this.setState({ dataSource: cloneData })
  }

  onSalesPriceChange = (key: string, value: number) => {
    const currentObj = { ...this.state }
    currentObj[key] = value
    // console.log(key, value)
    this.setState(currentObj)
    // this.setState({ price: value });
  }

  changeCost = (value: number) => {
    console.log(value)
  }

  renderLotsPricing = (lotList: any) => {
    let column = []
    if (lotList && isArray(lotList.onHandList)) {
      lotList.forEach((element: any) => {
        column.push({
          title: `Lot ${element.lotId}`,
          dataIndex: 'onHandList.cost',
          width: 100,
          key: 'cost',
          render(cost: any, record: any) {
            return `${formatNumber(cost, 2)}/${record.baseUOM}`
          },
        })
      })
    }

    return column
  }

  expandedRowRender = (record: any) => {
    const { items } = this.props
    let onHandColumns = [],
      expectColumns = []
    if (record.onHandList && isArray(record.onHandList) && record.onHandList.length > 0) {
      onHandColumns = [
        {
          title: '',
          width: 100,
          align: 'center',
          render() {
            return `On Hand Lots Pricing`
          },
        },
      ]
      record.onHandList.forEach((element: any, index: number) => {
        onHandColumns.push({
          title: (
            <div>
              <div>Lot {element.lotId}</div>
              <div>{formatDate(element.deliveryDate)}</div>
            </div>
          ),
          dataIndex: 'lotId',
          width: 100,
          align: 'center',
          key: index,
          render(cost: any, data: any) {
            return `$${basePriceToRatioPrice(element.uom, element.cost, items, 2)}/${element.uom}`
          },
        })
      })
    }

    if (record.expectList && isArray(record.expectList) && record.expectList.length > 0) {
      expectColumns = [
        {
          title: '',
          width: 300,
          align: 'center',
          render() {
            return `Expected Lots Pricing`
          },
        },
      ]
      record.expectList.forEach((element: any, index: number) => {
        expectColumns.push({
          title: (
            <div>
              <div>Lot {element.lotId}</div>
              <div>{formatDate(element.deliveryDate)}</div>
            </div>
          ),
          dataIndex: 'lotId',
          width: 300,
          align: 'center',
          key: index,
          render(cost: any, data: any) {
            return `$${basePriceToRatioPrice(element.uom, element.cost, items, 2)}/${element.uom}`
          },
        })
      })
    }
    return (
      <>
        <ThemeTable
          columns={onHandColumns}
          dataSource={[{}]}
          pagination={false}
          scroll={{ x: 1500 }}
          style={{ margin: '0 0 0' }}
        />
        <ThemeTable
          columns={expectColumns}
          dataSource={[{}]}
          pagination={false}
          scroll={{ x: 1500 }}
          style={{ margin: '0 0 0' }}
        />
      </>
    )
  }

  onChangeSearch = (value: string) => {
    const { id } = this.props.match.params
    this.props.getProductPurchasePriceList({
      itemId: id,
      page: this.state.page,
      pageSize: this.state.pageSize,
      companyName: value,
    })
  }

  onSuggest = (str: string) => {
    return []
  }

  changePage = (page: any) => {
    const { id } = this.props.match.params
    console.log('change page number', page)
    this.setState({ page }, () => {
      this.props.getProductPurchasePriceList({
        itemId: id,
        page: page - 1,
        pageSize: this.state.pageSize,
      })
    })
  }

  render() {
    const { dataSource, price, cost, page, pageSize, total, visibled } = this.state
    const { items, purchaseItemPricingList, purchaseItemPricingTotal } = this.props

    const columns: ColumnProps<any>[] = [
      {
        title: 'Customer Account',
        dataIndex: 'clientCompanyName',
        key: 'clientCompanyName',
        align: 'center',
      },
      {
        title: 'Last Sold Price',
        dataIndex: 'lastSoldDate',
        key: 'lastSoldDate',
        align: 'center',
        // sorter: (a, b) => a.updatedDate - b.updatedDate,
        // sorter: true,
        render(lastSoldDate: any, record: any) {
          if (record.lastSoldUOM) {
            return `$${basePriceToRatioPrice(record.lastSoldUOM, record.lastSoldPrice, items, 2)}/${
              record.lastSoldUOM
            } (on ${formatDate(lastSoldDate)})`
          } else {
            return `--`
          }
        },
      },
      {
        title: 'Current Pricing Strategy',
        dataIndex: 'defaultLogic',
        key: 'defaultLogic',
        align: 'center',
        render(defualtLogic: any, record: any) {
          return `${defualtLogic ? defualtLogic : ''} ${defualtLogic == 'FOLLOW_GROUP' ? record.defaultGroup : ''}`
        },
      },
    ]

    return (
      <Wrapper>
        <TipWithChildren>
          <TipText>Pricing Insights</TipText>
          <GrubSuggest onSuggest={this.onSuggest} onSearch={this.onChangeSearch} theme={this.props.theme} />
        </TipWithChildren>
        <ThemeTable
          pagination={{
            total: purchaseItemPricingTotal,
            pageSize: pageSize,
            current: page,
            defaultCurrent: 1,
            onChange: this.changePage,
          }}
          // components={components}
          columns={columns}
          dataSource={purchaseItemPricingList}
          expandedRowRender={this.expandedRowRender}
          scroll={{ x: 1500 }}
          // rowKey="id"
          // css={tableCss(false)}
          // onRow={onRow}
          // loading={this.props.loading}
          // onChange={this.handleTableChange}
          // onSort={this.handleTableChange}
        />
        <ProductPricingForm
          items={items}
          savePricingLoading={this.props.savePricingLoading}
          updateProductPricing={this.props.updateItemPricingGroup}
        />
        {/* <div className='pricing-group-table-wrapper'>
          <PricingOptions
            logicTitle={'Default Pricing Logic'}
            groupTitle={'Default Pricing Group'}
            type='product'
            defaultGroup={items.defaultGroup}
            defaultLogic={items.defaultLogic}
            price={price}
            cost={cost}
            onSelectedGroupChange={this.onGroupSelectionChanged}
            onSelectedLogicChange={this.onLogicSelectionChanged}
            changeSalesPrice={this.onSalesPriceChange} />
          <PricingTableWrapper>
            <RowEditableTable data={dataSource} editable={this.state.editable} onChange={this.onChangeValues} />
          </PricingTableWrapper>
        </div> */}
      </Wrapper>
    )
  }
}

export class ProductPricingFormComponent extends React.PureComponent<ProductPricingProps> {
  state = {
    groupChecked: this.props.items.groupChecked ? this.props.items.groupChecked : false,
    lastSoldChecked: this.props.items.lastSoldChecked ? this.props.items.lastSoldChecked : false,
    defaultPurchasingCostUOM: this.props.items.defaultPurchasingCostUOM
      ? this.props.items.defaultPurchasingCostUOM
      : this.props.items.inventoryUOM,
    defaultSellingPricingUOM: this.props.items.defaultSellingPricingUOM
      ? this.props.items.defaultSellingPricingUOM
      : this.props.items.inventoryUOM,
    wholesaleProductUomList: this.props.items.wholesaleProductUomList ? this.props.items.wholesaleProductUomList : [],
    cost:
      this.props.items.cost != null
        ? basePriceToRatioPrice(this.props.items.defaultPurchasingCostUOM, this.props.items.cost, this.props.items)
        : 0,

    price:
      this.props.items.price != null
        ? basePriceToRatioPrice(
            this.props.items.defaultSellingPricingUOM,
            this.props.items.price,
            this.props.items,
            12,
            true,
          )
        : 0,
    defaultPrice: this.props.items.price != null ? this.props.items.price : 0,
    visibled: false,
    priceGroupType: this.props.items.priceGroupType != null ? this.props.items.priceGroupType : 2,
    costMethod: this.props.items.costMethod ? this.props.items.costMethod : 1,
  }

  componentWillReceiveProps(nextProps: any) {
    if (
      Object.keys(nextProps.items).length > 0 &&
      (nextProps.items.groupChecked != this.props.items.groupChecked ||
        nextProps.items.lastSoldChecked != this.props.items.lastSoldChecked)
    ) {
      this.setState({
        groupChecked: nextProps.items.groupChecked ? nextProps.items.groupChecked : false,
        lastSoldChecked: nextProps.items.lastSoldChecked ? nextProps.items.lastSoldChecked : false,
      })
    }
    if (Object.keys(nextProps.items).length > 0 && isArray(nextProps.items.wholesaleProductUomList)) {
      this.setState({
        wholesaleProductUomList: nextProps.items.wholesaleProductUomList,
      })
    }
    if (
      Object.keys(nextProps.items).length > 0 &&
      (this.props.items.defaultPurchasingCostUOM != nextProps.items.defaultPurchasingCostUOM ||
        !nextProps.items.defaultPurchasingCostUOM)
    ) {
      this.setState({
        defaultPurchasingCostUOM: nextProps.items.defaultPurchasingCostUOM
          ? nextProps.items.defaultPurchasingCostUOM
          : nextProps.items.inventoryUOM,
      })
    }
    if (
      Object.keys(nextProps.items).length > 0 &&
      (this.props.items.defaultSellingPricingUOM != nextProps.items.defaultSellingPricingUOM ||
        !nextProps.items.defaultSellingPricingUOM)
    ) {
      this.setState({
        defaultSellingPricingUOM: nextProps.items.defaultSellingPricingUOM
          ? nextProps.items.defaultSellingPricingUOM
          : nextProps.items.inventoryUOM,
      })
    }
    if (Object.keys(nextProps.items).length > 0 && this.props.items.cost != nextProps.items.cost) {
      this.setState({
        cost: basePriceToRatioPrice(nextProps.items.defaultPurchasingCostUOM, nextProps.items.cost, nextProps.items),
      })
    }
    if (Object.keys(nextProps.items).length > 0 && this.props.items.price != nextProps.items.price) {
      this.setState({
        price: basePriceToRatioPrice(
          nextProps.items.defaultSellingPricingUOM,
          nextProps.items.price,
          nextProps.items,
          12,
          true,
        ),
      })
    }
    if (Object.keys(nextProps.items).length > 0 && this.props.items.price != nextProps.items.price) {
      this.setState({
        defaultPrice: nextProps.items.price,
      })
    }
    if (Object.keys(nextProps.items).length > 0 && this.props.items.priceGroupType != nextProps.items.priceGroupType) {
      this.setState({
        priceGroupType: nextProps.items.priceGroupType,
      })
    }
    if (Object.keys(nextProps.items).length > 0 && this.props.items.costMethod != nextProps.items.costMethod) {
      this.setState({
        costMethod: nextProps.items.costMethod,
      })
    }
  }

  setStateFromEditModal = (info: any) => {
    this.setState(info)
    this.props.form.setFieldsValue({ price: info.price })
  }

  changeCost = (value: number) => {
    console.log(value)
  }

  onSaveChanges = (e: any) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // console.log('Received values of form: ', {
        //   ...values,
        //   groupChecked: this.state.groupChecked,
        //   lastSoldChecked: this.state.lastSoldChecked,
        //   wholesaleProductUomList: this.state.wholesaleProductUomList,
        // })
        const { items } = this.props
        const { defaultPrice, priceGroupType, costMethod } = this.state
        let wholesaleProductUomList = [...this.state.wholesaleProductUomList]
        let { cost, price, defaultPurchasingCostUOM, defaultSellingPricingUOM } = values
        for (let i = 0; i < wholesaleProductUomList.length; i++) {
          if (defaultPurchasingCostUOM == wholesaleProductUomList[i].name) {
            // if (items.cost == 0) {
            // wholesaleProductUomList[i].costFactor = 0
            cost = mathRoundFun(cost * wholesaleProductUomList[i].ratio, 12)
            // } else {
            // wholesaleProductUomList[i].costFactor = mathRoundFun(
            //   (1 - items.cost / (cost * wholesaleProductUomList[i].ratio)) * 100,
            //   6,
            // )
            //   cost = items.cost
            // }
            // wholesaleProductUomList[i]['isUpdate'] = true
          }
          if (defaultSellingPricingUOM == wholesaleProductUomList[i].name) {
            // if (items.price == 0) {
            //   wholesaleProductUomList[i].priceFactor = 0
            //   price = mathRoundFun(price * wholesaleProductUomList[i].ratio, 6)
            // } else {
            //   wholesaleProductUomList[i].priceFactor = mathRoundFun(
            //     (1 - items.price / (price * wholesaleProductUomList[i].ratio)) * 100,
            //     6,
            //   )
            //   price = items.price
            // }
            // wholesaleProductUomList[i]['isUpdate'] = true
            // if (wholesaleProductUomList[i].priceFactor > 0) {
            //   price = mathRoundFun(
            //     (price * wholesaleProductUomList[i].ratio) / (1 + wholesaleProductUomList[i].priceFactor / 100),
            //     6,
            //   )
            // } else {
            //   price = mathRoundFun(price * wholesaleProductUomList[i].ratio, 6)
            // }
          }
        }

        // console.log(cost, price)
        // console.log(wholesaleProductUomList)
        this.props.updateProductPricing({
          id: this.props.items.wholesaleItemId,
          info: {
            ...values,
            cost: cost,
            price: defaultPrice,
            groupChecked: this.state.groupChecked,
            lastSoldChecked: this.state.lastSoldChecked,
            wholesaleProductUomList: wholesaleProductUomList,
            priceGroupType,
            costMethod,
          },
        })
      }
    })
  }

  onChangeGroupCheck = (e: any) => {
    this.setState({
      groupChecked: e.target.checked,
    })
  }

  onChangeLastSoldCheck = (e: any) => {
    this.setState({
      lastSoldChecked: e.target.checked,
    })
  }

  onChangeUomMargin = (index: number, type: String, value: string) => {
    // const { wholesaleProductUomList } = this.state
    let wholesaleProductUomList = [...this.state.wholesaleProductUomList]
    const { defaultSellingPricingUOM, defaultPrice } = this.state
    const { items } = this.props
    if (!isNumber(value)) {
      value = 0
    }
    if (type == 'price') {
      wholesaleProductUomList[index]['priceFactor'] = value
      if (wholesaleProductUomList[index].name == defaultSellingPricingUOM) {
        items.wholesaleProductUomList = wholesaleProductUomList
        this.setState(
          {
            price: basePriceToRatioPrice(defaultSellingPricingUOM, defaultPrice, items, 12, true),
          },
          () => {
            this.props.form.setFieldsValue({
              price: mathRoundFun(this.state.price, 2),
            })
          },
        )
      }
    } else if (type == 'cost') {
      wholesaleProductUomList[index]['costFactor'] = value
    }
    wholesaleProductUomList[index]['isUpdate'] = true
    this.setState({
      wholesaleProductUomList: wholesaleProductUomList,
    })
  }

  onChangeUom = (type: string, value: string) => {
    const { items } = this.props
    const { cost, defaultPrice } = this.state
    if (type == 'defaultPurchasingCostUOM') {
      this.setState(
        {
          defaultPurchasingCostUOM: value,
          cost: basePriceToRatioPrice(value, items.cost, items),
        },
        () => {
          this.props.form.setFieldsValue({
            cost: this.state.cost,
          })
        },
      )
    } else if (type == 'defaultSellingPricingUOM') {
      this.setState(
        {
          defaultSellingPricingUOM: value,
          price: basePriceToRatioPrice(value, defaultPrice, items, 12, true),
        },
        () => {
          this.props.form.setFieldsValue({
            price: mathRoundFun(this.state.price, 2),
          })
        },
      )
    }
  }

  onChangePrice = (value: number) => {
    const { defaultSellingPricingUOM } = this.state
    const { items } = this.props
    if (items.inventoryUOM == defaultSellingPricingUOM) {
      this.setState({
        defaultPrice: value,
      })
    }
  }

  onChangePriceGroupType = (e: any) => {
    this.setState({
      priceGroupType: e.target.value,
    })
  }

  openChangeUOMModal = () => {
    this.changeUOMModalStatus(true)
  }

  changeUOMModalStatus = (flag: boolean) => {
    this.setState({
      visibled: flag,
    })
  }

  onChangeCostMethod = (value: any) => {
    console.log(value)
    this.setState({
      costMethod: value,
    })
  }

  getPricingGroupDom = () => {
    const { items } = this.props
    const { getFieldDecorator } = this.props.form
    const { defaultSellingPricingUOM, priceGroupType } = this.state
    let groupA = items.priceGroupType == priceGroupType ? mathRoundFun(items.marginA, 2) : 0,
      groupB = items.priceGroupType == priceGroupType ? mathRoundFun(items.marginB, 2) : 0,
      groupC = items.priceGroupType == priceGroupType ? mathRoundFun(items.marginC, 2) : 0,
      groupD = items.priceGroupType == priceGroupType ? mathRoundFun(items.marginD, 2) : 0,
      groupE = items.priceGroupType == priceGroupType ? mathRoundFun(items.marginE, 2) : 0,
      descA = items.priceGroupType == priceGroupType ? items.descA : '',
      descB = items.priceGroupType == priceGroupType ? items.descB : '',
      descC = items.priceGroupType == priceGroupType ? items.descC : '',
      descD = items.priceGroupType == priceGroupType ? items.descD : '',
      descE = items.priceGroupType == priceGroupType ? items.descE : ''
    let inputProps = priceGroupType == 1 ? { min: 0 } : {}
    return (
      <>
        <ProductGroupBlock style={{ marginTop: '10px' }}>
          <div className="group-name" />
          <div className="group-desc">Short Description</div>
          <div className="group-margin">
            {' '}
            {priceGroupType == 1 ? 'Price' : priceGroupType == 2 ? 'Margin' : 'markup'}
          </div>
          <div className="group-uom" />
        </ProductGroupBlock>
        <ProductGroupBlock>
          <div className="group-name">Group A</div>
          <div className="group-desc">
            {getFieldDecorator('descA', {
              initialValue: descA,
            })(<ThemeInput maxLength={35} placeholder="price Group A" />)}
          </div>
          <div className="group-margin">
            {getFieldDecorator('marginA', {
              initialValue: groupA,
            })(
              <ThemeInputNumber
                placeholder={priceGroupType == 2 ? `0%` : ''}
                {...inputProps}
                formatter={(value) => {
                  return priceGroupType == 2 ? `${value}%` : `$${value}`
                }}
                style={{ marginTop: '3px' }}
              />,
            )}
          </div>
          <div className="group-uom"> {priceGroupType == 1 ? `/${defaultSellingPricingUOM}` : ''}</div>
        </ProductGroupBlock>
        <ProductGroupBlock>
          <div className="group-name">Group B</div>
          <div className="group-desc">
            {getFieldDecorator('descB', {
              initialValue: descB,
            })(<ThemeInput maxLength={35} placeholder="price Group B" />)}
          </div>
          <div className="group-margin">
            {getFieldDecorator('marginB', {
              initialValue: groupB,
            })(
              <ThemeInputNumber
                placeholder={priceGroupType == 2 ? `0%` : ''}
                {...inputProps}
                formatter={(value) => {
                  return priceGroupType == 2 ? `${value}%` : `$${value}`
                }}
                style={{ marginTop: '3px' }}
              />,
            )}
          </div>
          <div className="group-uom"> {priceGroupType == 1 ? `/${defaultSellingPricingUOM}` : ''}</div>
        </ProductGroupBlock>
        <ProductGroupBlock>
          <div className="group-name">Group C</div>
          <div className="group-desc">
            {getFieldDecorator('descC', {
              initialValue: descC,
            })(<ThemeInput maxLength={35} placeholder="price Group C" />)}
          </div>
          <div className="group-margin">
            {getFieldDecorator('marginC', {
              initialValue: groupC,
            })(
              <ThemeInputNumber
                placeholder={priceGroupType == 2 ? `0%` : ''}
                {...inputProps}
                formatter={(value) => {
                  return priceGroupType == 2 ? `${value}%` : `$${value}`
                }}
                style={{ marginTop: '3px' }}
              />,
            )}
          </div>
          <div className="group-uom"> {priceGroupType == 1 ? `/${defaultSellingPricingUOM}` : ''}</div>
        </ProductGroupBlock>
        <ProductGroupBlock>
          <div className="group-name">Group D</div>
          <div className="group-desc">
            {getFieldDecorator('descD', {
              initialValue: descD,
            })(<ThemeInput maxLength={35} placeholder="price Group D" />)}
          </div>
          <div className="group-margin">
            {getFieldDecorator('marginD', {
              initialValue: groupD,
            })(
              <ThemeInputNumber
                placeholder={priceGroupType == 2 ? `0%` : ''}
                {...inputProps}
                formatter={(value) => {
                  return priceGroupType == 2 ? `${value}%` : `$${value}`
                }}
                style={{ marginTop: '3px' }}
              />,
            )}
          </div>
          <div className="group-uom"> {priceGroupType == 1 ? `/${defaultSellingPricingUOM}` : ''}</div>
        </ProductGroupBlock>
        {localStorage.getItem(CACHED_NS_LINKED) == 'null' && (
          <ProductGroupBlock>
            <div className="group-name">Group E</div>
            <div className="group-desc">
              {getFieldDecorator('descE', {
                initialValue: descE,
              })(<ThemeInput maxLength={35} placeholder="price Group E" />)}
            </div>
            <div className="group-margin">
              {getFieldDecorator('marginE', {
                initialValue: groupE,
              })(
                <ThemeInputNumber
                  placeholder={priceGroupType == 2 ? `0%` : ''}
                  {...inputProps}
                  formatter={(value) => {
                    return priceGroupType == 2 ? `${value}%` : `$${value}`
                  }}
                  style={{ marginTop: '3px' }}
                />,
              )}
            </div>
            <div className="group-uom"> {priceGroupType == 1 ? `/${defaultSellingPricingUOM}` : ''}</div>
          </ProductGroupBlock>
        )}
      </>
    )
  }
  render() {
    const { getFieldDecorator } = this.props.form
    const { items, savePricingLoading } = this.props
    const {
      groupChecked,
      lastSoldChecked,
      wholesaleProductUomList,
      defaultPurchasingCostUOM,
      defaultSellingPricingUOM,
      cost,
      price,
      defaultPrice,
      visibled,
      priceGroupType,
      costMethod,
    } = this.state
    // const purchaseUomList = []
    const salesUomList = []
    if (isArray(wholesaleProductUomList)) {
      let tempItem = cloneDeep(items)
      tempItem.wholesaleProductUomList = wholesaleProductUomList
      for (const [index, uom] of wholesaleProductUomList.entries()) {
        if (!uom.deletedAt && uom.useForSelling && defaultSellingPricingUOM != uom.name) {
          // if (!uom.deletedAt && uom.useForSelling) {
          salesUomList.push(
            <Row className="product-specification-row" gutter={24}>
              <Col span={4} className="overFont" title={uom.name}>
                {uom.name}
              </Col>
              <Col span={6}>
                <ThemeInputNumber
                  key={uom.id}
                  placeholder="0.00%"
                  value={uom.priceFactor}
                  step="1"
                  style={{ width: '100%' }}
                  onChange={this.onChangeUomMargin.bind(this, index, 'price')}
                  formatter={(value) => `${value ? value + '%' : ''}`}
                />
              </Col>
              <Col span={14}>
                (Default price ${formatNumber(basePriceToRatioPrice(uom.name, defaultPrice, tempItem, 2, true), 2)} per {uom.name})
              </Col>
            </Row>,
          )
        }
        //     if (uom.useForPurchasing && defaultPurchasingCostUOM != uom.name) {
        //       purchaseUomList.push(
        //         <Row className="product-specification-row" gutter={24}>
        //           <Col span={4} className="overFont" title={uom.name}>
        //             {uom.name}
        //           </Col>
        //           <Col span={6}>
        //             <ThemeInputNumber
        //               key={uom.id}
        //               placeholder="0.00%"
        //               defaultValue={uom.costFactor}
        //               step="1"
        //               style={{ width: '100%' }}
        //               onChange={this.onChangeUomMargin.bind(this, index, 'cost')}
        //               formatter={(value) => `${value ? value + '%' : ''}`}
        //             />
        //           </Col>
        //           <Col span={14}>
        //             (Default cost ${basePriceToRatioPrice(uom.name, items.cost, items)} per {uom.name})
        //           </Col>
        //         </Row>,
        //       )
        //     }
      }
    }
    // console.log(cost, price)
    return (
      <>
      {visibled && <ProductChangeUOMForm
          visibled={visibled}
          items={items}
          changeUOMModalStatus={this.changeUOMModalStatus}
          onSaveChanges={this.setStateFromEditModal}
          updateProductPricing={this.props.updateProductPricing}
        ></ProductChangeUOMForm>}

        <Form onSubmit={this.onSaveChanges}>
          <ProductBlock>
            <TipText>Cost & Pricing Configuration</TipText>
            <ProductTitleTips>
              Last Updated: {items.lastUpdatedDate ? formatDate(items.lastUpdatedDate) : ''}{' '}
              {items.lastUpdatedBy ? 'by ' + items.lastUpdatedBy.firstName + ' ' + items.lastUpdatedBy.lastName : ''}{' '}
            </ProductTitleTips>
          </ProductBlock>
          <ProductBlock>
            <TipSmallText>Cost</TipSmallText>
            <ProductTitle className="mt10">
              <Row>
                <Col span={8}>Default Cost *</Col>
                <Col span={2} />
                <Col span={8}>Default Cost UOM</Col>
              </Row>
              <Row className="product-specification-row">
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator('cost', {
                      // initialValue: basePriceToRatioPrice(defaultPurchasingCostUOM, items.cost, items),
                      initialValue: cost,
                      rules: [
                        {
                          required: true,
                          message: 'Please input default cost!',
                        },
                      ],
                    })(
                      <ThemeInputNumber
                        placeholder="$0.00"
                        step="0.25"
                        min={0}
                        formatter={(value) => `${value ? '$' + value : ''}`}
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col span={2} className="text-center">
                  per
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator('defaultPurchasingCostUOM', {
                      initialValue: defaultPurchasingCostUOM,
                      rules: [
                        {
                          required: true,
                          message: 'Please choose default cost uom!',
                        },
                      ],
                    })(
                      <ThemeSelect
                        onChange={this.onChangeUom.bind(this, 'defaultPurchasingCostUOM')}
                        suffixIcon={<Icon type="caret-down" />}
                        style={{ minWidth: 80 }}
                      >
                        {items.useForPurchasing && (
                          <Select.Option value={items.inventoryUOM} title={items.inventoryUOM}>
                            {items.inventoryUOM}
                          </Select.Option>
                        )}

                        {items.wholesaleProductUomList != null && items.wholesaleProductUomList.length > 0
                          ? items.wholesaleProductUomList.map((uom: WholesaleProductUom) => {
                              if (!uom.deletedAt && uom.useForPurchasing) {
                                return (
                                  <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                                    {uom.name}
                                  </Select.Option>
                                )
                              }
                            })
                          : ''}
                      </ThemeSelect>,
                    )}
                  </Form.Item>
                </Col>
              </Row>
              {/* <Row>Costing Premiums for Alternate UOMs</Row>
            {purchaseUomList} */}
            </ProductTitle>
          </ProductBlock>
          <ProductBlock>
            <TipSmallText>Pricing</TipSmallText>
            <ProductTitle className="mt10">
              <Row>
                <Col span={8}>Default Price *</Col>
                <Col span={2} />
                <Col span={8}></Col>
              </Row>
              <Row className="product-specification-row">
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator('price', {
                      // initialValue: basePriceToRatioPrice(defaultSellingPricingUOM, items.price, items),
                      initialValue: mathRoundFun(price, 2),
                      rules: [
                        {
                          required: true,
                          message: 'Please input default price!',
                        },
                      ],
                    })(
                      <ThemeInputNumber
                        placeholder="$0.00"
                        step="0.25"
                        min={0}
                        formatter={(value) => `${value ? '$' + value : ''}`}
                        style={{ marginTop: '3px' }}
                        disabled={defaultSellingPricingUOM != items.inventoryUOM}
                        onChange={this.onChangePrice}
                      />,
                    )}
                  </Form.Item>
                </Col>
                <Col span={2} className="text-center">
                  per
                </Col>
                <Col span={8}>
                  {defaultSellingPricingUOM}&nbsp;&nbsp;
                  <ThemeLink onClick={this.openChangeUOMModal}>Edit UOM</ThemeLink>
                  {/* <Form.Item>
                  {getFieldDecorator('defaultSellingPricingUOM', {
                    initialValue: defaultSellingPricingUOM,
                    rules: [
                      {
                        required: true,
                        message: 'Please choose default pricing uom!',
                      },
                    ],
                  })(
                    <ThemeSelect
                      suffixIcon={<Icon type="caret-down" />}
                      style={{ minWidth: 80 }}
                      onChange={this.onChangeUom.bind(this, 'defaultSellingPricingUOM')}
                    >
                      {items.useForSelling && (
                        <Select.Option value={items.inventoryUOM} title={items.inventoryUOM}>
                          {items.inventoryUOM}
                        </Select.Option>
                      )}

                      {items.wholesaleProductUomList != null && items.wholesaleProductUomList.length > 0
                        ? items.wholesaleProductUomList.map((uom: WholesaleProductUom) => {
                            if (!uom.deletedAt && uom.useForSelling) {
                              return (
                                <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                                  {uom.name}
                                </Select.Option>
                              )
                            }
                          })
                        : ''}
                    </ThemeSelect>,
                  )}
                </Form.Item> */}
                </Col>
              </Row>
              <Row>
                Pricing Premiums and Discounts
                <Tooltip placement="top" title="Applies Discounts or Premiums When Selling By Alternate UOMs">
                  <IconLocal type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
                </Tooltip>
              </Row>
              {salesUomList}
            </ProductTitle>
          </ProductBlock>
          <ProductBlock>
            <TipSmallText>Pricing Strategies</TipSmallText>
            <ProductTitleTips>
              Selected pricing strategies will be made available to a user who is adding this product to a sales order
              or price sheet.
            </ProductTitleTips>

            <ProductStrategy className="mt20">
              <ThemeCheckbox checked={groupChecked} onChange={this.onChangeGroupCheck} />
              <div className="strategy-content">
                <div className="strategy-name">Price Groups</div>
                {groupChecked && (
                  <div>
                    <ProductGroupBlock style={{ marginTop: '10px' }}>
                      <Radio.Group onChange={this.onChangePriceGroupType} value={priceGroupType}>
                        <ThemeRadio value={1}>Set prices</ThemeRadio>
                        <ThemeRadio value={2}>Set margins</ThemeRadio>
                        <ThemeRadio value={3}>Set markups</ThemeRadio>
                      </Radio.Group>
                    </ProductGroupBlock>
                    {priceGroupType != 1 && (
                      <>
                        <div className="mt20">Cost method</div>
                        <div>
                          <ThemeSelect
                            onChange={this.onChangeCostMethod.bind(this)}
                            style={{ width: 300 }}
                            value={costMethod}
                          >
                            <Select.Option value={1}>Lot cost (with allocated charges)</Select.Option>
                            <Select.Option value={2}>Default cost</Select.Option>
                          </ThemeSelect>
                        </div>
                      </>
                    )}

                    {this.getPricingGroupDom()}
                  </div>
                )}
              </div>
            </ProductStrategy>

            <ProductStrategy style={{ marginTop: '30px' }}>
              <ThemeCheckbox checked={lastSoldChecked} onChange={this.onChangeLastSoldCheck} />
              <div className="strategy-content">
                <div className="strategy-name">
                  Last Sold Price By Account
                  <Tooltip
                    placement="top"
                    title="This last sold price by account reflects the most recent price charged for this product in a shipped order.for a given account."
                  >
                    <IconLocal type="tip-icon" style={{ marginLeft: '8px', marginTop: '-3px' }} />
                  </Tooltip>
                </div>
              </div>
            </ProductStrategy>
          </ProductBlock>
          <ProductBlock>
            <ThemeButton shape="round" htmlType="submit" loading={savePricingLoading}>
              <Icon type="save" />
              Save Changes
            </ThemeButton>
          </ProductBlock>
        </Form>
      </>
    )
  }
}

export const ProductPricingForm = Form.create<ProductPricingProps>()(ProductPricingFormComponent)
