import React from 'react'
import { Icon } from '~/components'
import { Tooltip, Radio } from 'antd'
import {
  SectionWrapper,
  Section,
  SectionTitle,
  SectionOperation,
  SectionOperationText,
  SectionOperationSplit,
} from './product.component.style'

export class ProductSection extends React.Component<T> {
  state = {
    height: '',
    rawHight: '',
  }
  constructor(props: any) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }

  componentDidMount() {
    if (this.props.sectionKey === undefined) {
      throw new Error('Every ProductSection Component need a unique key!')
    }
    this.initHeight()
  }

  initHeight() {
    const { defaultExpanded } = this.props
    const { clientHeight }: any = document.querySelector(`#section-${this.props.sectionKey}`)
    if (defaultExpanded) {
      this.setState({ height: clientHeight, rawHight: clientHeight })
    } else {
      this.setState({ height: '46px', rawHight: clientHeight })
    }
  }

  onClick() {
    const { height, rawHight } = this.state
    this.setState({ height: height === '46px' ? rawHight : '46px' })
  }

  render() {
    const { height } = this.state
    const { title='Product Information', onSectionSave, sectionKey } = this.props

    return (
      <SectionWrapper style={{ /*height, */ marginBottom: height === '46px' ? '19px' : '62px' }}>
        <Section id={`section-${sectionKey}`} style={{ height }}>
          <SectionTitle>
            {title}
            {/* {height === '46px' ? (
              <SectionOperation>
                <Icon
                  style={{ cursor: 'pointer' }}
                  type="arrow-down-product"
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  onClick={this.onClick}
                />
              </SectionOperation>
            ) : (
              <SectionOperation>
                <Icon
                  style={{ cursor: 'pointer' }}
                  type="save"
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  onClick={onSectionSave}
                />
                <SectionOperationText>SAVE</SectionOperationText>
                <SectionOperationSplit />
                <Icon
                  style={{ cursor: 'pointer' }}
                  type="arrow-up-product"
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  onClick={this.onClick}
                />
              </SectionOperation>
            )} */}
          </SectionTitle>
          {this.props.children}
        </Section>
      </SectionWrapper>
    )
  }
}
