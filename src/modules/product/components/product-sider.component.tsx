import React from 'react'
import { Icon } from '~/components'
import { lightGrey3, lightGrey2, forestGreen } from '~/common/color'
import { SiderWrapper, SiderItemWrapper, SiderItem, SideItemSelectedBorder } from '../product.style'
import { SubMenuHandler } from '~/components/PageLayout'
import { isBrowser } from 'react-device-detect'
import { UserRole } from '~/schema'

interface Props {
  onExpand: Function
  currentUser: any
}
export class ProductSider extends React.Component<Props> {
  state: any
  constructor(props: any) {
    super(props)
    const { id } = props.match.params
    this.state = {
      selected: 0,
      siderItems: [
        // { title: 'Product Description', url: `/product/${id}/description` },
        { title: 'Specifications', url: `/product/${id}/specifications`, hideTags: [UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED] },
        { title: 'Pricing', url: `/product/${id}/pricing`, hideTags: [UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED] },
        { title: 'Details', url: `/product/${id}/details`, hideTags: [] },
        { title: 'Description', url: `/product/${id}/description`, hideTags: [] },
        { title: 'Gallery', url: `/product/${id}/gallery`, hideTags: [] },
        { title: 'Logistics', url: `/product/${id}/logistics`, hideTags: [UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED] },
        { title: 'Activity', url: `/product/${id}/activity`, hideTags: [UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED] },
        { title: 'Lots', url: `/product/${id}/lots`, hideTags: [UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED] },
        { title: 'Adjustments', url: `/product/${id}/adjustment`, hideTags: [UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED] },
      ],
      openSubMenu: false
    }
  }

  componentDidMount() {
    this.getCurrentTab()
  }

  getCurrentTab() {
    const items = this.state.siderItems
    const { pathname } = this.props.location
    let selected = 0
    for (let i = 0; i < items.length; i++) {
      if (items[i].url === pathname) {
        selected = i
        break
      }
    }
    this.setState({ selected })
  }

  onItemClick(index: number, url: string, _: any) {
    this.setState({ selected: index })
    this.props.history.push(url)
  }

  onClickSubMenu = () => {
    this.props.onExpand(!this.state.openSubMenu)
    this.setState({ openSubMenu: !this.state.openSubMenu })
  }

  render() {
    const { currentUser } = this.props
    const { siderItems, selected } = this.state
    return (
      <>
        <SubMenuHandler open={this.state.openSubMenu} onClick={this.onClickSubMenu} />
        <SiderWrapper style={{ width: this.state.openSubMenu || isBrowser ? 260 : 0 }}>
          {siderItems.map((item, index) => {
            if (item.hideTags.indexOf(currentUser.accountType) == -1) {
              return (
                <SiderItemWrapper key={index} style={{ backgroundColor: selected === index ? '#a3ada4' : 'transparent' }}>
                  <SiderItem onClick={this.onItemClick.bind(this, index, item.url)}>{item.title}{selected === index && <Icon type="arrow-right-product" viewBox='0 0 8 8' height='16' style={{ float: 'right', marginTop: 12, marginRight: 12 }} />}</SiderItem>
                  {selected === index && <SideItemSelectedBorder />}
                </SiderItemWrapper>
              )
            }
          })}
        </SiderWrapper>
      </>
    )
  }
}
