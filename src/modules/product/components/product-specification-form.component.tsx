import React, { CSSProperties } from 'react'
import {
  Form,
  Input,
  Row,
  Col,
  Radio,
  Tooltip,
  Checkbox,
  Button,
  Select,
  Upload,
  Icon as DefulatIcon,
  message,
  Cascader,
  Modal,
  DatePicker,
  Popconfirm, InputNumber,
  Icon as AntIcon,
  notification
} from 'antd'

const confirm = Modal.confirm
import { Icon } from '~/components'
import {
  SectionRadioWrapper,
  SectionRadioRow,
  SectionRadioColumn,
  SectionRadioLabel,
  SectionRadioText,
  SectionTableSplit,
  SetionTableLabel,
  DragSmallWrapper,
  ThemeDatePicker,
  RatioFormula
} from './product.component.style'
import { brightGreen, CACHED_NS_LINKED, CACHED_ACCOUNT_TYPE } from '~/common'
import { ProductModule, ProductProps, ProductDispatchProps } from '..'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { withTheme } from 'emotion-theming'
import { FormComponentProps } from 'antd/lib/form'
import { WrappedFormUtils } from 'antd/lib/form/Form'
import {
  EditA,
  ThemeSelect,
  ThemeModal,
  FullInputNumber,
  ThemeForm,
  InputLabel,
  ThemeInput,
  ThemeInputNumber,
  ThemeIcon,
  ThemeCheckbox,
  ThemeRadio, ThemeButton, ThemeOutlineButton, ThemeSwitch, Flex
} from '~/modules/customers/customers.style'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import NewUomEditor from '~/modules/settings/tabs/Product/NewUomEditor'
import { onLoadFocusOnFirst } from '~/common/jqueryHelper'
import moment from 'moment'
import { Icon as IconSvg } from '~/components/icon/'
import ChangeAddressModal from '~/modules/customers/sales/widgets/change-address-modal'
import { allowedNodeEnvironmentFlags } from 'process'
import _ from 'lodash'
import { ProductUOMModalForm } from './product-uom-modal.component'
import { TabHeaderBigSpan } from '~/modules/customers/nav-sales/styles'
import { baseQtyToRatioQty, mathRoundFun, ratioQtyToBaseQty } from '~/common/utils'
import ProductUOMSelect from '~/components/ProductUOMSelect'
import { UserRole } from '~/schema';

const tdStyle = (): CSSProperties => ({
  padding: '0px 25px 0px 0px',
  textAlign: 'right',
})

const radioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px',
  marginTop: '10px'
}

interface FormFields {
  sku: string
  variety: string
  category: string
  wholesaleCategory: any
  family: string
  defaultMargin: number
  temperature: string
  standardWeight: boolean
  unitWeight: number
  unitPerLayer: string
  reorderLevel: string
  layersPerPallet: string
  shelfLife: number
  size: number
  weight: number
  grade: number
  packing: number
  restockingTime: number
  origin: string
  managerId: number
  baseUOM: string
  inventoryUOM: string
  constantRatio: boolean
  ratioUOM: number
  brand: string
  price: number
  isOverridePrice: boolean
  quantity: number
}

type ProductDetailProps = ProductProps &
  ProductDispatchProps &
  FormComponentProps<FormFields> & {
    radioData: any
  }

class ProductSpecificationContainer extends React.PureComponent<ProductDetailProps> {
  state = {
    loading: false,
    currentUom: 'case',
    currentManager: 'manager',
    managerValue: '',
    showManagerModal: false,
    confirmVisible: false,
    virtualLotModal: false,
    uomModalVisiable: false,
    newData: null,
    price: 0,
    deliveryDate: moment(),
    inventory: 0,
    purchasePrice: 0,
    showConstantRatio: this.props.items ? this.props.items.constantRatio : false,
    defaultSalesUnitUOM: this.props.items.defaultSalesUnitUOM ? this.props.items.defaultSalesUnitUOM : '',
    defaultPurchaseUnitUOM: this.props.items.defaultPurchaseUnitUOM ? this.props.items.defaultPurchaseUnitUOM : '',
    useForSelling: this.props.items ? this.props.items.useForSelling : true,
    useForPurchasing: this.props.items ? this.props.items.useForPurchasing : false,
    wholesaleProductUomList: this.props.items ? this.props.items.wholesaleProductUomList : [],
    hasSpecialChars:false,
    lotAssignmentMethod: this.props.items ? this.props.items.lotAssignmentMethod : 1,
    enableLotOverflow: this.props.items ? this.props.items.enableLotOverflow : false,
    pricingUOM: this.props?.items?.pricingUOM || this.props?.items?.defaultSellingPricingUOM || this.props?.items?.inventoryUOM
  }
  input: any

  componentDidMount() {
    this.props.getCompanyProductAllTypes()
    this.props.getSaleCategories()
    onLoadFocusOnFirst('simple-input-wrapper')
  }

  componentWillReceiveProps(nextProps: ProductDetailProps) {
    if (this.props.items.price != nextProps.items.price) {
      this.setState({
        price: nextProps.items.price,
      })
    }
    if (this.props.items.constantRatio != nextProps.items.constantRatio) {
      this.setState({
        showConstantRatio: _.clone(nextProps.items.constantRatio),
      })
    }
    if (this.props.items.defaultSalesUnitUOM != nextProps.items.defaultSalesUnitUOM) {
      this.setState({
        defaultSalesUnitUOM: _.clone(nextProps.items.defaultSalesUnitUOM),
      })
    }
    if (this.props.items.defaultPurchaseUnitUOM != nextProps.items.defaultPurchaseUnitUOM) {
      this.setState({
        defaultPurchaseUnitUOM: _.clone(nextProps.items.defaultPurchaseUnitUOM),
      })
    }
    if (this.props.items.useForSelling != nextProps.items.useForSelling) {
      this.setState({
        useForSelling: _.clone(nextProps.items.useForSelling),
      })
    }
    if (this.props.items.useForPurchasing != nextProps.items.useForPurchasing) {
      this.setState({
        useForPurchasing: _.clone(nextProps.items.useForPurchasing),
      })
    }
    if (this.props.items.lotAssignmentMethod != nextProps.items.lotAssignmentMethod) {
      this.setState({
        lotAssignmentMethod: _.clone(nextProps.items.lotAssignmentMethod),
      })
    }
    if (this.props.items.enableLotOverflow != nextProps.items.enableLotOverflow) {
      this.setState({
        enableLotOverflow: _.clone(nextProps.items.enableLotOverflow),
      })
    }
    if (this.props.items.wholesaleProductUomList != nextProps.items.wholesaleProductUomList) {
      this.setState({
        wholesaleProductUomList: _.clone(nextProps.items.wholesaleProductUomList),
      })
    }

    if (this.props.items !== nextProps.items){
      this.setState({
        pricingUOM: nextProps?.items?.pricingUOM || nextProps?.items?.defaultSellingPricingUOM || nextProps?.items?.inventoryUOM
      })
    }
  }

  onUomChange = (index: number, value: string) => {
    this.setState({ currentUom: value })
  }

  onManagerChange = (value: string) => { }

  onSave = async (e: any) => {
    e.preventDefault()
    const { categories, form, items } = this.props
    const { wholesaleItemId, qboId } = items
    const { defaultSalesUnitUOM, defaultPurchaseUnitUOM, wholesaleProductUomList, useForSelling, useForPurchasing } = this.state

    form.validateFields((err, values) => {
      if (!err) {
        let category: any = null
        categories.forEach((el) => {
          if (el.wholesaleCategoryId === values.category[1]) {
            category = el
          }
        })

        // beacuse user can edit uom when product haven't transaction, so we need reset ratio
        // const ratioUOM = values.constantRatio ? values.ratioUOM : 1
        const newData = {
          ...items,
          ...values,
          wholesaleItemId: wholesaleItemId,
          wholesaleCategory: category,
          qboId,
          wholesaleProductUomList,
          useForSelling,
          useForPurchasing,
          defaultSalesUnitUOM,
          defaultPurchaseUnitUOM,
          lotAssignmentMethod: this.state.lotAssignmentMethod,
          enableLotOverflow: this.state.enableLotOverflow,
          pricingUOM: this.state.pricingUOM,
        }

        //because if quantity set, backend will add new lot
        newData.quantity = null
        delete newData.category
        delete newData.itemDescription

        //set produc uom  currently we need have one subUom
        // newData.wholesaleProductUomList = [
        //   {
        //     id:
        //       items.wholesaleProductUomList != null && items.wholesaleProductUomList.length > 0
        //         ? items.wholesaleProductUomList[0].id
        //         : null,
        //     name: values.subUom,
        //     type: 1,
        //     ratio: values.subRatio,
        //     constantRatio: values.subConstantRatio,
        //   },
        // ]
        // delete newData.subUom
        // delete newData.subRatio
        // delete newData.subConstantRatio

        // console.log(newData);
        this.props.updateItems({data: newData, queryParams: {}})
      }
    })
  }

  getAlert = () => (this.state.hasSpecialChars ? <p style={{ color: 'red', lineHeight: 'normal' fontWeight: 'normal' }}>{`Note: item name containing any of the following characters may cause unexpected behavior: \\ / " < > |`}</p> : '')

  _displayRender = (label: any) => {
    return label[label.length - 1]
  }

  handleConfirmOk = () => {
    this.setState(
      {
        confirmVisible: false,
      },
      () => {
        this.props.updateItems({data: this.state.newData, queryParams: {}})
      },
    )
  }

  handleConfirmCancel = () => {
    this.setState({
      confirmVisible: false,
    })
  }
  changeSalesPrice = (value: number) => {
    this.setState({
      price: value,
    })
  }

  onDeliveryDateChange = (oldDate: any, newDate: any) => {
    this.setState({
      deliveryDate: moment(newDate),
    })
  }

  onChangeInventory = (val: any) => {
    this.setState({
      inventory: val,
    })
  }

  onChangePurchasePrice = (val: any) => {
    this.setState({
      purchasePrice: val,
    })
  }

  handleCancel = () => {
    this.setState(
      {
        virtualLotModal: false,
        inventory: 0,
        purchasePrice: 0,
        deliveryDate: moment(),
      },
      () => {
        if (this.input) {
          this.input.blur()
        }
      },
    )
    this.props.form.setFieldsValue({
      quantity: 0,
    })
  }

  compareToInventoryUom = (rule, value, callback) => {
    const { form } = this.props
    if (value && value == form.getFieldValue('inventoryUOM')) {
      callback('SubUom cannot be equal to inventory uom')
    } else if (value && value == form.getFieldValue('baseUOM')) {
      callback('SubUom cannot be equal to Pricing uom')
    } else {
      if (form.getFieldValue('subRatio') == null || form.getFieldValue('subRatio') == '') {
        form.validateFields(['subRatio'], { force: true })
      }
      callback()
    }
    // if (value == 1 && form.getFieldValue('subUom') == form.getFieldValue('inventoryUOM')) {
    //   callback('SubUOM To Inventory Ratio cannot be 1 when inventory UOM equal SubUOM');
    // } else {
    //   callback();
    // }
  }

  subRatioHandler = (rule, value, callback) => {
    const { form } = this.props
    if (form.getFieldValue('subUom') != '' && !_.isNumber(value) && value != 0) {
      callback('SubUOM To Inventory Ratio must be greater than 0')
    } else {
      callback()
    }
  }

  toggleUOMModal = () => {
    const { uomModalVisiable } = this.state
    this.setState(
      {
        uomModalVisiable: !uomModalVisiable,
      },
      () => {
        this.props.form.resetFields()
      },
    )
  }

  onChangeCheckBox = (isSubUom: boolean, type: string, index: any, e: any) => {
    console.log(isSubUom, type, e.target.checked);
    let updateStatus = {}, value = e.target.checked
    const { useForSelling, useForPurchasing } = this.state

    if (!isSubUom) {
      if (type == 'useForSelling') {
        let hasSelling = this.state.wholesaleProductUomList.filter((uom: any) => !uom.deletedAt && uom.useForSelling).length > 0 ? true : false
        if (!hasSelling && !value) {
          notification.warn({
            message: 'Warn',
            description: 'Sorry, we need at least one of use for selling to be checked',
            onClose: () => { },
          })
          return
        } else {
          updateStatus = {
            useForSelling: value
          }
        }
      } else if (type == 'useForPurchasing') {
        let hasPurchasing = this.state.wholesaleProductUomList.filter((uom: any) => !uom.deletedAt && uom.useForPurchasing).length > 0 ? true : false
        if (!hasPurchasing && !value) {
          notification.warn({
            message: 'Warn',
            description: 'Sorry, we need at least one of use for purchasing to be checked',
            onClose: () => { },
          })
          return
        } else {
          updateStatus = {
            useForPurchasing: value
          }
        }
      }
    } else {
      let wholesaleProductUomList = [...this.state.wholesaleProductUomList]
      if (type == 'useForSelling') {
        let selectSelling = this.state.wholesaleProductUomList.filter((uom: any) => !uom.deletedAt && uom.useForSelling)
        if (selectSelling.length == 1 && !value && !useForSelling) {
          notification.warn({
            message: 'Warn',
            description: 'Sorry, we need at least one of use for selling to be checked',
            onClose: () => { },
          })
          return
        } else {
          wholesaleProductUomList[index]['useForSelling'] = value
        }
      } else if (type == 'useForPurchasing') {
        let selectPurchasing = this.state.wholesaleProductUomList.filter((uom: any) => !uom.deletedAt && uom.useForPurchasing)
        if (selectPurchasing.length == 1 && !value && !useForPurchasing) {
          notification.warn({
            message: 'Warn',
            description: 'Sorry, we need at least one of use for purchasing to be checked',
            onClose: () => { },
          })
          return
        } else {
          wholesaleProductUomList[index]['useForPurchasing'] = value
        }
      } else if (type == 'useForInventory') {
        wholesaleProductUomList[index]['useForInventory'] = value
      }
      updateStatus = {
        wholesaleProductUomList: wholesaleProductUomList
      }
    }

    this.setState(updateStatus)
  }

  onChangeSwitch = (index: any, value: any) => {
    console.log(value);
    let wholesaleProductUomList = [...this.state.wholesaleProductUomList]
    const { useForSelling, useForPurchasing } = this.state
    let selectPurchasing = wholesaleProductUomList.filter((uom: any) => !uom.deletedAt && uom.useForPurchasing)
    let selectSelling = wholesaleProductUomList.filter((uom: any) => !uom.deletedAt && uom.useForSelling)
    if (!value) {
      if ((selectPurchasing.length == 1 && !useForPurchasing && selectPurchasing[0].id == wholesaleProductUomList[index].id)
        || (selectSelling.length == 1 && !useForSelling && selectSelling[0].id == wholesaleProductUomList[index].id)) {
        notification.warn({
          message: 'Warn',
          description: 'Sorry, we need at least one of use for selling/purchasing to be checked',
          onClose: () => { },
        })
        return;
      } else {
        wholesaleProductUomList[index]['deletedAt'] = moment()
      }
    } else {
      wholesaleProductUomList[index]['deletedAt'] = ''
    }

    this.setState({
      wholesaleProductUomList: wholesaleProductUomList
    })
  }

  handleChangeLotMethod = (lotAssignmentMethod: number) => {
    if (lotAssignmentMethod === 2) {
      this.setState({ enableLotOverflow: false })
    }
    this.setState({ lotAssignmentMethod })
  }

  onChangeOverflowCheckBox = (enableLotOverflow: boolean) => {
    this.setState({ enableLotOverflow })
  }

  onChangeRadio = (type: string, value: string) => {
    let updateStatus = {}
    if (type == 'defaultSalesUnitUOM') {
      updateStatus = {
        defaultSalesUnitUOM: value
      }
    } else if (type == 'defaultPurchaseUnitUOM') {
      updateStatus = {
        defaultPurchaseUnitUOM: value
      }
    } else if (type == 'defaultSellingPricingUOM') {
      updateStatus = {
        defaultSellingPricingUOM: value
      }
    } else if (type == 'defaultPurchasingCostUOM') {
      updateStatus = {
        defaultPurchasingCostUOM: value
      }
    }
    this.setState(updateStatus)
  }

  checkDefault = (name: string): boolean =>(this.state.defaultPurchaseUnitUOM == name || this.state.defaultSalesUnitUOM == name)?true:false

  setPurchaseRadio = (name:string,index:number)=>{
    const trueBody = {
      target:{
        checked:true
      }
    }
    this.setState({ defaultPurchaseUnitUOM: name })
    this.onChangeCheckBox(true, 'useForPurchasing', index,trueBody)
  }

  setSellingRadio= (name:string,index:number)=>{
    const trueBody = {
      target:{
        checked:true
      }
    }
    this.setState({ defaultSalesUnitUOM: name })
    this.onChangeCheckBox(true, 'useForSelling', index,trueBody)
  }

  setInventoryPurchaseRadio= (obj:any)=>{
    const trueBody = {
      target:{
        checked:true
      }
    }
    this.onChangeCheckBox(false, 'useForPurchasing', -1,trueBody)
    this.setState(obj)
  }

  setInventorySellingRadio= (obj:any)=>{
    const trueBody = {
      target:{
        checked:true
      }
    }
    this.onChangeCheckBox(false, 'useForSelling', -1,trueBody)
    this.setState(obj)
  }

  _changeUOM  = (uomName: string, ratio: number, uomId:any) => {
    if(uomId ==='inventoryUOM'){
      this.setState({
        pricingUOM: uomName
      })
    }else{
      this.setState({
        wholesaleProductUomList:this.state.wholesaleProductUomList.map(item=>{
          if(item.id===uomId){
            return {...item, pricingUOM:uomName}
          }
          return item
        })
      })
    }
  }

  render() {
    const { confirmVisible, showConstantRatio, uomModalVisiable, defaultSalesUnitUOM,
      defaultPurchaseUnitUOM,
      useForPurchasing,
      useForSelling,
      enableLotOverflow,
      wholesaleProductUomList } = this.state

    const { radioData = [], items, form, userSetting, getCompanyProductAllTypes } = this.props
    const { getFieldDecorator } = form
    const { companyProductTypes, categories } = this.props
    const accountType =
      localStorage.getItem(CACHED_ACCOUNT_TYPE) && localStorage.getItem(CACHED_ACCOUNT_TYPE) != 'null'
        ? localStorage.getItem(CACHED_ACCOUNT_TYPE)
        : UserRole.ADMIN

    let itemCategory = [] as any[]
    const categoryOptions = []
    if (categories.length > 0) {
      const sections = {}
      for (const category of categories) {
        if (sections[category.wholesaleSection.wholesaleSectionId] == null)
          sections[category.wholesaleSection.wholesaleSectionId] = {
            value: category.wholesaleSection.wholesaleSectionId,
            label: category.wholesaleSection.name,
            children: [],
          }
        sections[category.wholesaleSection.wholesaleSectionId].children.push({
          value: category.wholesaleCategoryId,
          label: category.name,
        })
      }
      for (const section in sections) categoryOptions.push(sections[section])
      itemCategory = [categories[0].wholesaleSection.wholesaleSectionId, categories[0].wholesaleCategoryId]
    }

    if (items != null && items.wholesaleCategory != null)
      itemCategory = [
        items.wholesaleCategory.wholesaleSection.wholesaleSectionId,
        items.wholesaleCategory.wholesaleCategoryId,
      ]
    var isNSConnected = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const isNativeUOMDisabled = (userSetting && userSetting.nsCustomUom === false) && isNSConnected

    const RenderUomDomList = ({ items }: any) => {
      const RenderFirstRow = () => (
        <>
          <tr key="inventoryUOM">
            <td style={tdStyle()} />
            <td style={tdStyle()} />
            <td style={tdStyle()} />
            <td style={tdStyle()}>Default</td>
            <td style={tdStyle()}>Default</td>
            <td style={tdStyle()} />
          </tr>
          <tr>
            <td>{items.inventoryUOM} (Base)</td>
            <td>1 {items.inventoryUOM} per {items.inventoryUOM}</td>
            <td><ThemeCheckbox checked disabled /></td>
            <td>
              <Row type='flex' justify='space-around'>
                <ThemeCheckbox checked={useForSelling||defaultSalesUnitUOM === items.inventoryUOM} onChange={this.onChangeCheckBox.bind(this, false, 'useForSelling', -1)} />
                <ThemeRadio value={items.inventoryUOM} checked={defaultSalesUnitUOM === items.inventoryUOM} onChange={() => {this.setInventorySellingRadio({ defaultSalesUnitUOM: items.inventoryUOM })}} />
              </Row>
            </td>
            <td>
              <Row justify='space-around' type='flex'>
                <ThemeCheckbox checked={useForPurchasing||defaultPurchaseUnitUOM === items.inventoryUOM} onChange={this.onChangeCheckBox.bind(this, false, 'useForPurchasing', -1)} />
                <ThemeRadio value={items.inventoryUOM} checked={defaultPurchaseUnitUOM === items.inventoryUOM} onChange={() => this.setInventoryPurchaseRadio({ defaultPurchaseUnitUOM: items.inventoryUOM })} />
              </Row>
            </td>
            <td>
              <ProductUOMSelect value={this.state.pricingUOM} inventoryUOM={items.inventoryUOM} wholesaleProductUomList={items.wholesaleProductUomList} onChange={(val, ratio) => {this._changeUOM(val,ratio, 'inventoryUOM')}} />
            </td>
            <td><ThemeSwitch defaultChecked disabled /><span className="ml10">Available for use</span></td>
          </tr>
        </>
      )
      if (_.isArray(wholesaleProductUomList)) {
        return [
          RenderFirstRow(),
          ...wholesaleProductUomList.map((uom, index) => (
            <tr key={uom.id}>
              <td>{uom.name}</td>
              <td>{uom.ratio > 1 ? `${mathRoundFun(uom.ratio, 2)} ${uom.name} per ${items.inventoryUOM}` : `${mathRoundFun(1 / uom.ratio, 2)} ${items.inventoryUOM} per ${uom.name}`}{uom.type === 2 && (<span> (approx)</span>)}</td>
              <td><ThemeCheckbox disabled={uom.deletedAt ? true : false} checked={uom.useForInventory} onChange={this.onChangeCheckBox.bind(this, true, 'useForInventory', index)} /></td>
              <td>
                <Row justify='space-around' type='flex'>
                  <ThemeCheckbox disabled={uom.deletedAt||(this.state.defaultSalesUnitUOM ==uom.name ) ? true : false} checked={uom.useForSelling} onChange={this.onChangeCheckBox.bind(this, true, 'useForSelling', index)} />
                  <ThemeRadio disabled={uom.deletedAt ? true : false} value={uom.name} checked={defaultSalesUnitUOM === uom.name} onChange={() => this.setSellingRadio(uom.name,index)} />
                </Row>
              </td>
              <td>
                <Row justify='space-around' type='flex'>
                  <ThemeCheckbox disabled={uom.deletedAt||(this.state.defaultPurchaseUnitUOM ==uom.name )? true : false} checked={uom.useForPurchasing} onChange={this.onChangeCheckBox.bind(this, true, 'useForPurchasing', index)} />
                  <ThemeRadio disabled={uom.deletedAt ? true : false} value={uom.name} checked={defaultPurchaseUnitUOM === uom.name} onChange={() => this.setPurchaseRadio(uom.name,index)} />
                </Row>
              </td>
              <td>
                <ProductUOMSelect value={uom.pricingUOM} inventoryUOM={items.inventoryUOM} wholesaleProductUomList={items.wholesaleProductUomList} onChange={(val, ratio) => {this._changeUOM(val,ratio,uom.id)}} />
              </td>
              <td className="text-left"><ThemeSwitch disabled={this.checkDefault(uom.name)?true:false} checked={uom.deletedAt ? false : true} onChange={this.onChangeSwitch.bind(this, index)} /><span className="ml10">{uom.deletedAt ? 'Disabled' : 'Available for use'}</span></td>
            </tr>
          ))
        ]
      }
      return RenderFirstRow()
    }

    return (
      <>
        <ProductUOMModalForm getCompanyProductAllTypes={getCompanyProductAllTypes} uomModalVisiable={uomModalVisiable} items={items} toggleUOMModal={this.toggleUOMModal}
          companyProductTypes={companyProductTypes} addProductUom={this.props.addProductUom} />
        <ThemeForm onSubmit={this.onSave} className="simple-input-wrapper">
          <ThemeModal
            visible={confirmVisible}
            title="Are you sure?"
            onCancel={this.handleConfirmCancel}
            closable={false}
            footer={[
              <Button key="Yes" onClick={this.handleConfirmOk}>
                Yes
              </Button>,
              <Button key="No" type="primary" onClick={this.handleConfirmCancel}>
                No
              </Button>,
            ]}
          >
            <p>
              Please confirm you want to set the initial physical quantity present in warehouse. This value cannot be
              edited directly once it is initially set.
            </p>
          </ThemeModal>
          <SectionRadioWrapper>
            <SectionRadioRow>
              {radioData.map((item: any, index: number) => (
                <SectionRadioColumn key={index}>
                  <SectionRadioRow>
                    <SectionRadioLabel>{item.label}</SectionRadioLabel>
                    {item.tip && (
                      <SectionRadioRow>
                        <Tooltip title={item.tip}>
                          <Icon type="tip-icon" />
                        </Tooltip>
                      </SectionRadioRow>
                    )}
                  </SectionRadioRow>
                  <SectionRadioRow style={{ marginTop: '10px' }}>
                    {getFieldDecorator(item.key, {
                      initialValue: items[item.key],
                    })(
                      <Radio.Group>
                        <Radio value={true}>
                          <SectionRadioText>YES</SectionRadioText>
                        </Radio>
                        <Radio value={false}>
                          <SectionRadioText>NO</SectionRadioText>
                        </Radio>
                      </Radio.Group>,
                    )}
                  </SectionRadioRow>
                </SectionRadioColumn>
              ))}
            </SectionRadioRow>
          </SectionRadioWrapper>
          {/* <SectionTableSplit /> */}
          <Row gutter={24}>
            <Col span={6}>
              <Form.Item colon={false} label={<SetionTableLabel className="form-label">Item SKU</SetionTableLabel>}>
                {getFieldDecorator('sku', {
                  rules: [{ required: false }],
                  initialValue: items.sku,
                })(<Input placeholder="SKU" />)}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item colon={false} label={<SetionTableLabel className="form-label">Item Name</SetionTableLabel>}>
                {getFieldDecorator('variety', {
                  rules: [{ required: true, message: 'Please input name!' }],
                  initialValue: items.variety,
                  getValueFromEvent: (e) => {
                    if (!e || !e.target) {
                      return e;
                    }
                    const { value } = e.target;
                    let hasSpecialChars = false
                    if (value) {
                      const reg = new RegExp("[[<\>\/\\\\|\"]")
                      const r = reg.test(value)
                      if (r == true) {
                        hasSpecialChars = true
                      }
                    }
                    this.setState({
                      hasSpecialChars
                    })

                    return value;
                  }
                })(<Input placeholder="Item name" />)}
                {this.getAlert()}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item colon={false} label={<SetionTableLabel className="form-label">Item Description</SetionTableLabel>}>
                {getFieldDecorator('itemDescription', {
                  rules: [{ required: false }],
                  initialValue: (userSetting && userSetting.company.itemDescriptionFormat === 'B') ? `${items.sku ? items.sku + '-' : ''}${items.variety}` : items.variety,
                })(<Input disabled />)}
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item colon={false} label={<SetionTableLabel className="form-label">Category</SetionTableLabel>}>
                <Form.Item>
                  {getFieldDecorator('category', {
                    rules: [{ required: false, message: 'Please input category!' }],
                    initialValue: items.wholesaleCategory ? itemCategory : '',
                  })(<Cascader expandTrigger="hover" options={categoryOptions} displayRender={this._displayRender} />)}
                </Form.Item>
              </Form.Item>
            </Col>
          </Row>
          <h3 className="text-left">Units of Measure</h3>
          <table cellpadding="15" className="product-uom-table">
            <thead>
              <tr>
                <td>UOM</td>
                <td>Units per {items.inventoryUOM}</td>
                <td>Use for Inventory</td>
                <td>Use for Selling</td>
                <td>Use for Purchasing</td>
                <td><Tooltip title="Select the default pricing UOM to be used when this UOM is set as order quantity UOM">Default pricing UOM<Icon type="tip-icon" /></Tooltip></td>
              </tr>
            </thead>
            <tbody>
              <RenderUomDomList items={items} />
            </tbody>
          </table>
          <Row gutter={24} className="product-specification-row">
            <Tooltip
              title={isNativeUOMDisabled

                ? 'Adding new units of measure from WholesaleWare is currently not supported for the native NetSuite UOM configuration type'
                : ''}
            >
              <Col span={5} className="product-specification-span">
                <ThemeOutlineButton
                  // htmlType="button"
                  size="large"
                  onClick={this.toggleUOMModal}
                  disabled={isNativeUOMDisabled}
                >
                  Add unit of measure
                </ThemeOutlineButton>
              </Col>
            </Tooltip>
          </Row>
          <div style={{ marginTop: 40 }}>
            <h3 className="text-left">Default lot assignment method</h3>
            <div className='text-left'>
              <Radio.Group onChange={(e) => this.handleChangeLotMethod(e.target.value)} value={this.state.lotAssignmentMethod}>
              <ThemeRadio style={radioStyle} value={1}>
                Auto-assign lot(s) by FIFO
              </ThemeRadio>
              <ThemeCheckbox style={{ paddingLeft: 30 }} disabled={this.state.lotAssignmentMethod==2} checked={enableLotOverflow} onChange={(e) => this.onChangeOverflowCheckBox(e.target.checked)}>[BETA] Enable “overflow” to multiple lots </ThemeCheckbox>
              <ThemeRadio style={radioStyle} value={2}>
                No lot selected
              </ThemeRadio>
              </Radio.Group>
            </div>
          </div>
          <Row gutter={24}>
            <Col span={8} className="product-specification-span">
              <ThemeButton
                htmlType="submit"
                size="large"
              >
                Save changes
              </ThemeButton>
            </Col>
          </Row>
        </ThemeForm>
      </>
    )
  }
}

const ProductSpecificationsForm = Form.create()(ProductSpecificationContainer)
// const mapStateToProps = (state: GlobalState) => state.product
// export const ProductDetailsForm = withTheme(connect(ProductModule)(mapStateToProps)(ProductDetailsFormComp))
export { ProductSpecificationsForm }
