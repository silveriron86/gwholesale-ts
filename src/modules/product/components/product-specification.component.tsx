import React from 'react'
import { Wrapper, Tip } from './product.component.style'
import { ProductSpecificationsForm } from './product-specification-form.component'

export class ProductSpecifications extends React.Component<T> {
  state = {
    radioData: [
      {
        label: 'Active',
        key: 'active',
        tip: 'Activating a product enables it to be sold, purchased, and reported',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Sell',
        key: 'sell',
        tip:
          'Determines whether your organization chooses to sell the product (vs store only or manufacture, for example)',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Purchase',
        key: 'purchase',
        tip: 'Flags product for repurchasing',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Manufacture',
        key: 'manufacture',
        tip: 'Determines whether product is internally manufactured, processed, or packed',
        optionValues: ['YES', 'NO'],
      },
      {
        label: 'Promote',
        key: 'push',
        tip:
          'Promote flags product for quick sale or to sell out in a short amount of time, oftentimes at a discounted price',
        optionValues: ['YES', 'NO'],
      },
      { label: 'Organic', key: 'isOrganic', tip: '', optionValues: ['YES', 'NO'] },
      { label: 'Tax', key: 'taxEnabled', tip: '', optionValues: ['YES', 'NO'] },
    ],
  }

  render() {
    const { radioData } = this.state
    return (
      <Wrapper>
        <Tip>Product Specifications</Tip>
        <ProductSpecificationsForm radioData={radioData} {...this.props} />
      </Wrapper>
    )
  }
}
