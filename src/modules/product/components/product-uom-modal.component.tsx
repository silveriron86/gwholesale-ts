import React from 'react'
import { FormComponentProps } from 'antd/lib/form'
import { SetionTableLabel, RatioFormula } from './product.component.style'
import { Form, Input, Row, Col, Radio, Checkbox, Select, Icon } from 'antd'

import {
  ThemeModal,
  ThemeForm,
  ThemeInputNumber,
  ThemeCheckbox,
  ThemeRadio,
  ThemeButton,
  ThemeInput,
  ThemeSelect,
  ThemeOutlineButton,
} from '~/modules/customers/customers.style'
import { mathRoundFun, notify } from '~/common/utils'
import ProductUOMSelect from '~/components/ProductUOMSelect'
import { isArray } from 'lodash'
import CreateUOM from './create-uom'

interface UOMModalFormFields {
  ratioType: string
  type: number
  ratio: number
  name: string
  useFor: any
}

interface UOMModalFormProps {
  uomModalVisiable: boolean
  items: any
  toggleUOMModal: Function
  addProductUom: Function
  companyProductTypes: any
  getCompanyProductAllTypes: any
}

type ProductUOMModalProps = FormComponentProps<UOMModalFormFields> & UOMModalFormProps

class ProductUOMModalContainer extends React.PureComponent<ProductUOMModalProps> {
  state = {
    uomRatioType: 1,
    uomRatio: 1,
    fixedFormulaReverse: false,
    variableFormulaReverse: false,
    selectedUOM: void 0,
    selectedUOMRatio: 1,
    showCreateUOM: false,
  }

  static getDerivedStateFromProps(nextProps: any, curState: any) {
    if (!curState.selectedUOM && nextProps?.items?.inventoryUOM) {
      return { selectedUOM: nextProps?.items?.inventoryUOM }
    }
    return null
  }

  toggleUOMModal = () => {
    // const { uomModalVisiable } = this.props
    this.props.toggleUOMModal()
    this.props.form.resetFields()
    this.setState({
      uomRatioType: 1,
      uomRatio: 1,
      fixedFormulaReverse: false,
      variableFormulaReverse: false,
      selectedUOM: void 0,
      selectedUOMRatio: 1,
    })
  }

  onChangeRatioType = (e: any) => {
    this.setState({
      uomRatioType: e.target.value,
    })
  }

  addUom = async (e: any) => {
    e.preventDefault()
    const { form, items } = this.props
    const { selectedUOMRatio } = this.state

    form.validateFields((err, values) => {
      if (values.name.toLowerCase() == items.inventoryUOM.toLowerCase()) {
        notify('warn', 'Warn', 'Sorry, this uom already exists in the current product, we cannot add it again')
        return
      }
      if (isArray(items.wholesaleProductUomList) && items.wholesaleProductUomList.length > 0) {
        let filterData = items.wholesaleProductUomList.filter((uom) => {
          return uom.name.toLowerCase() == values.name.toLowerCase()
        })
        if (filterData.length > 0) {
          notify('warn', 'Warn', 'Sorry, this uom already exists in the current product, we cannot add it again')
          return
        }
      }
      let data = {
        name: values.name,
        type: values.type,
        useForInventory: values.useFor.filter((useFor) => useFor == 1).length > 0 ? true : false,
        useForSelling: values.useFor.filter((useFor) => useFor == 2).length > 0 ? true : false,
        useForPurchasing: values.useFor.filter((useFor) => useFor == 3).length > 0 ? true : false,
        constantRatio: values.type == 1 ? true : false,
        ratio: this.getRatio()
        itemId: items.wholesaleItemId,
        pricingUOM: items.defaultSellingPricingUOM || items.inventoryUOM,
      }
      // console.log(data)
      this.props.addProductUom(data)
      this.toggleUOMModal()
      form.resetFields()
    })
  }

  getRatio = () => {
    const { uomRatio, fixedFormulaReverse, variableFormulaReverse, uomRatioType } = this.state
    const uomReverseRatio = mathRoundFun(1 / uomRatio, 12)
    if (uomRatioType == 1) {
      if (fixedFormulaReverse) {
        return mathRoundFun(uomReverseRatio, 12)
      } else {
        return mathRoundFun(uomRatio, 12)
      }
    } else {
      if (variableFormulaReverse) {
        return mathRoundFun(uomReverseRatio, 12)
      } else {
        return mathRoundFun(uomRatio, 12)
      }
    }
  }

  changeUomModalRatio = (value: number) => {
    const { uomRatioType, fixedFormulaReverse, variableFormulaReverse } = this.state
    if (uomRatioType == 1) {
      this.setState({
        uomRatio: value,
      })
    } else {
      this.setState({
        uomRatio: value,
      })
    }
  }

  onCheckboxChange = (checkedValues: any) => {
    console.log(checkedValues)
  }

  onChangeFormulaReverse = (type: number) => {
    const { fixedFormulaReverse, variableFormulaReverse } = this.state
    if (type == 1) {
      this.setState({
        fixedFormulaReverse: !fixedFormulaReverse,
      })
    } else {
      this.setState({
        variableFormulaReverse: !variableFormulaReverse,
      })
    }
  }

  displayUomDom = () => {
    const { uomRatio, fixedFormulaReverse, variableFormulaReverse, uomRatioType, selectedUOM } = this.state
    const uomName = this.props.form.getFieldValue('name')
    const uomReverseRatio = uomRatio
    if (uomRatioType == 1) {
      if (fixedFormulaReverse) {
        return `${mathRoundFun(uomReverseRatio, 2)} ${selectedUOM} per ${uomName}`
      } else {
        return `${mathRoundFun(uomRatio, 2)} ${uomName} per ${selectedUOM}`
      }
    } else {
      if (variableFormulaReverse) {
        return `${mathRoundFun(uomReverseRatio, 2)} ${selectedUOM} per ${uomName}`
      } else {
        return `${mathRoundFun(uomRatio, 2)} ${uomName} per ${selectedUOM}`
      }
    }
  }

  _selectUOM = (val: string, ratio: number) => {
    this.setState({
      selectedUOM: val,
      selectedUOMRatio: ratio,
    })
  }

  addUOMCallback = (addedUOM:string) => {
    this.props.form.setFieldsValue({name:addedUOM})
    this.props.getCompanyProductAllTypes()
  }

  render() {
    const { uomRatioType, uomRatio, fixedFormulaReverse, variableFormulaReverse, selectedUOM, showCreateUOM } = this.state
    const uomReverseRatio = uomRatio
    const { items, form, uomModalVisiable, companyProductTypes } = this.props
    const uomName = form.getFieldValue('name')
    const { getFieldDecorator } = form

    return (
      <>
        <ThemeModal
          width={550}
          title={`Add unit of measurement`}
          visible={uomModalVisiable}
          onCancel={this.toggleUOMModal}
          cancelText="Cancel"
          okText="Add unit of measurement"
          onOk={this.addUom}
        >
          <ThemeForm onSubmit={this.addUom} className="product-specification-form">
            <Row gutter={24}>
              <Col span={16}>
                <Form.Item
                  colon={false}
                  label={<SetionTableLabel className="form-label">Unit of measurement</SetionTableLabel>}
                >
                  {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Unit of measurement is required' }],
                  })(
                    <ThemeSelect
                      style={{ width: '150' }}
                      showSearch
                      filterOption={(input: string, option: { props: { children: string } }) => {
                        console.log(option.props.children)
                        return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }}
                      dropdownRender={(menu: any) => (
                        <div>
                          {menu}
                          {/* <EditA onMouseDown={e => e.preventDefault()} onClick={this.toggleEditSelect}>Edit...</EditA> */}
                        </div>
                      )}
                    >
                      {companyProductTypes &&
                        companyProductTypes.unitOfMeasure.map(
                          (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                            return (
                              <Select.Option key={item.id} value={item.name}>
                                {item.name}
                              </Select.Option>
                            )
                          },
                        )}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item colon={false} label={<SetionTableLabel />}>
                  {getFieldDecorator('button', {
                    rules: [{ required: false }],
                  })(
                    <ThemeButton type="primary" onClick={()=>this.setState({showCreateUOM:true})}>
                      Create UOM
                    </ThemeButton>,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24} style={{ marginTop: '15px' }}>
              <Col span={24}>
                <Form.Item>
                  {getFieldDecorator('useFor', {
                    rules: [{ required: true, message: 'Use for is required' }],
                  })(
                    <Checkbox.Group style={{ width: '100%' }} value={['1', '2', '3']} onChange={this.onCheckboxChange}>
                      <Row>
                        <ThemeCheckbox value="1">Use for inventory</ThemeCheckbox>
                      </Row>
                      <Row style={{ marginTop: '10px' }}>
                        <ThemeCheckbox value="2">Use for selling</ThemeCheckbox>
                      </Row>
                      <Row style={{ marginTop: '10px' }}>
                        <ThemeCheckbox value="3">Use for purchasing</ThemeCheckbox>
                      </Row>
                    </Checkbox.Group>,
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={24}>
                <Form.Item
                  colon={false}
                  label={<SetionTableLabel className="form-label">{this.displayUomDom()}</SetionTableLabel>}
                >
                  {getFieldDecorator('type', {
                    initialValue: uomRatioType,
                    rules: [{ required: true, message: 'Ratio type is required' }],
                  })(
                    <Radio.Group onChange={this.onChangeRatioType}>
                      <Row>
                        <ThemeRadio value={1}>Fixed ratio</ThemeRadio>
                        {uomRatioType == 1 && (
                          <>
                            {!fixedFormulaReverse ? (
                              <RatioFormula>
                                1&nbsp;
                                <ProductUOMSelect
                                  value={selectedUOM}
                                  onChange={this._selectUOM}
                                  inventoryUOM={items.inventoryUOM}
                                  wholesaleProductUomList={items.wholesaleProductUomList}
                                  style={{ width: 160 }}
                                />
                                &nbsp;=&nbsp;
                                <ThemeInputNumber
                                  onChange={this.changeUomModalRatio}
                                  min={0}
                                  width={90}
                                  value={mathRoundFun(uomRatio, 6)}
                                />
                                <span className="ml10">{uomName}</span>
                                <ThemeOutlineButton onClick={this.onChangeFormulaReverse.bind(this, 1)}>
                                  <Icon type="sync" />
                                </ThemeOutlineButton>
                              </RatioFormula>
                            ) : (
                              <RatioFormula>
                                <span className="ml10">1&nbsp;{uomName}&nbsp;&nbsp;=&nbsp;&nbsp;</span>
                                <ThemeInputNumber
                                  onChange={this.changeUomModalRatio}
                                  min={0}
                                  width={90}
                                  value={mathRoundFun(uomReverseRatio, 6)}
                                />
                                &nbsp;
                                <ProductUOMSelect
                                  value={selectedUOM}
                                  onChange={this._selectUOM}
                                  inventoryUOM={items.inventoryUOM}
                                  wholesaleProductUomList={items.wholesaleProductUomList}
                                  style={{ width: 160 }}
                                />
                                <ThemeOutlineButton onClick={this.onChangeFormulaReverse.bind(this, 1)}>
                                  <Icon type="sync" />
                                </ThemeOutlineButton>
                              </RatioFormula>
                            )}
                          </>
                        )}
                      </Row>
                      <Row>
                        <ThemeRadio value={2}>Variable ratio</ThemeRadio>
                        {uomRatioType == 2 && (
                          <>
                            {!variableFormulaReverse ? (
                              <RatioFormula>
                                1&nbsp;
                                <ProductUOMSelect
                                  value={selectedUOM}
                                  onChange={this._selectUOM}
                                  inventoryUOM={items.inventoryUOM}
                                  wholesaleProductUomList={items.wholesaleProductUomList}
                                  style={{ width: 160 }}
                                />
                                &nbsp;=&nbsp;
                                <ThemeInputNumber
                                  onChange={this.changeUomModalRatio}
                                  min={0}
                                  width={90}
                                  value={mathRoundFun(uomRatio, 6)}
                                />
                                <span className="ml10">Approx&nbsp;&nbsp;{uomName}</span>
                                <ThemeOutlineButton
                                  onClick={this.onChangeFormulaReverse.bind(this, 2)}
                                  className="ml10"
                                >
                                  <Icon type="sync" />
                                </ThemeOutlineButton>
                              </RatioFormula>
                            ) : (
                              <RatioFormula>
                                <span className="ml10">1&nbsp;{uomName}&nbsp;&nbsp;=&nbsp;&nbsp;</span>
                                <ThemeInputNumber
                                  onChange={this.changeUomModalRatio}
                                  min={0}
                                  width={90}
                                  value={mathRoundFun(uomReverseRatio, 6)}
                                />
                                &nbsp;&nbsp;Approx&nbsp;&nbsp;
                                <ProductUOMSelect
                                  value={selectedUOM}
                                  onChange={this._selectUOM}
                                  inventoryUOM={items.inventoryUOM}
                                  wholesaleProductUomList={items.wholesaleProductUomList}
                                  style={{ width: 160 }}
                                />
                                <ThemeOutlineButton
                                  onClick={this.onChangeFormulaReverse.bind(this, 2)}
                                  className="ml10"
                                >
                                  <Icon type="sync" />
                                </ThemeOutlineButton>
                              </RatioFormula>
                            )}
                          </>
                        )}
                      </Row>
                    </Radio.Group>,
                  )}
                </Form.Item>
              </Col>
            </Row>
          </ThemeForm>
          <CreateUOM uoms={companyProductTypes?.unitOfMeasure} close={() => this.setState({showCreateUOM:false}) } visible={showCreateUOM} addUOMCallback={this.addUOMCallback} />
        </ThemeModal>
      </>
    )
  }
}

const ProductUOMModalForm = Form.create()(ProductUOMModalContainer)
export { ProductUOMModalForm }
