import styled from '@emotion/styled'
import { css } from '@emotion/core'
import {
  Row,
  Button,
  Icon,
  Input,
  Radio,
  Modal,
  Select,
  Checkbox,
  Tabs,
  Alert,
  Spin,
  DatePicker,
  InputNumber,
  Form,
} from 'antd'

import {
  mediumGrey,
  forestGreen,
  white,
  grey,
  brightGreen,
  lightGrey4,
  lightGrey2,
  lightGrey,
  black,
  lightGreen2,
  lightGrey5,
} from '~/common'

export const Wrapper = styled('div')((props: any) => ({
  position: 'relative',
  width: '100%',
  minHeight: '101vh',
  padding: '60px 19px',
  '.no-top-border': {
    '.ant-table': {
      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
    },
  },
}))

export const Tip = styled.div`
  position: relative;
  border-top: 5px solid ${forestGreen};
  padding: 25px 19px;
  font-family: Arial;
  font-size: 20px;
  font-weight: bold;
  line-height: 23px;
  color: ${mediumGrey};
`

export const TipWithChildren = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-top: 5px solid ${forestGreen};
  padding: 25px 19px;
`

export const ProductBlock = styled('div')((props: any) => ({
  width: '700px',
  textAlign: 'left',
  padding: '25px 19px',
  color: mediumGrey,
}))

export const ProductTitleTips = styled.div`
  font-family: Arial;
  font-size: 14px;
  line-height: 21px;
  color: ${lightGrey};
`

export const ProductTitle = styled.div`
  font-family: Arial;
  font-size: 14px;
  line-height: 21px;
  color: #777777;
`

export const ThemeInputNumber = styled(InputNumber)((props) => ({
  width: '200px',

  '&:hover': {
    borderColor: props.theme.primary,
  },
  '&:focus': {
    borderColor: props.theme.primary,
    boxShadow: 'none',
  },
  '&.ant-input-number-focused': {
    borderColor: props.theme.primary,
    boxShadow: 'none',
  },
  '&.ant-input-number-input-wrap:hover .ant-input-number-input-wrap:focus': {
    borderColor: props.theme.primary,
  },
}))

export const ProductStrategy = styled('div')((props: any) => ({
  display: 'flex',
  '.strategy-content': {
    flex: 1,
    marginLeft: '10px',
    color: mediumGrey,
  },
  '.strategy-name': {
    color: mediumGrey,
  },
}))

export const ProductGroupBlock = styled('div')((props: any) => ({
  display: 'flex',
  alignItems: 'center',
  color: mediumGrey,
  marginTop: '3px',
  '.group-name': {
    width: '100px',
  },
  '.group-desc': {
    width: '250px',
  },
  '.group-uom': {
    marginLeft: '10px',
    width: '100px',
    textAlign: 'left',
  },
  '.group-margin': {
    marginLeft: '40px',
    flex: 1,
  },
}))

export const TipText = styled.div`
  font-family: Arial;
  font-size: 20px;
  font-weight: bold;
  line-height: 30px;
  color: ${mediumGrey};
`

export const TipSmallText = styled.div`
  font-family: Arial;
  font-size: 16px;
  line-height: 24px;
  color: ${mediumGrey};
`

export const TipDatePickerWrapper = styled.div`
  & .ant-calendar-picker {
    & .ant-calendar-picker-input {
      border: none;
      padding: 4px 0;
      border-bottom: solid 1px black;
      border-radius: 0;
      &:focus {
        box-shadow: none;
      }
    }
    & .ant-calendar-picker-icon {
      width: 24px;
      height: 21px;
      margin-top: -10px;
      margin-right: -12px;
      color: ${brightGreen};
    }
  }
`

export const inputTitleStyles = css({
  color: mediumGrey,
  fontWeight: 700,
  fontSize: '14px',
  lineHeight: '24px',
  textTransform: 'uppercase',
  paddingBottom: '0px !important',

  '& .ant-form-item-required': {
    color: grey,
    fontSize: '14px',
    lineHeight: '33px',
  },
})

export const buttonStyle = css({
  borderRadius: '200px',
  width: '130px',
  height: '40px',
  border: `1px solid ${brightGreen}`,
  backgroundColor: `${brightGreen} !important`,
  marginTop: '19px',
  marginRight: '25px',
  fontWeight: 700,
})

export const TipDatePicker = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`

export const TipDatePickerText = styled.div`
  font-family: Arial;
  font-size: 17px;
  line-height: 24px;
  letter-spacing: 0.05em;
  color: ${black};
`

export const TipAction = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
`

export const ActivityTables = styled(Row)((props: any) => ({
  minWidth: 1100,
  '.ant-table-wrapper': {
    '.ant-table': {
      width: '100%',
    },
    'div.ant-table-body': {
      margin: '0 0 !important',
    },
    '.ant-table-footer': {
      '.ant-table-wrapper': {
        overflow: 'unset',
      },
    },
  },
  '& .purchase-activity-table, & .sales-activity-table': {
    minWidth: 500,
    '& .ant-table': {
      border: 'none',
      '& tbody tr td:last-child, & thead tr th:last-child': {
        borderRight: '1px solid #EDF1EE',
      },
      '& tbody tr td:first-of-type, & thead tr th:first-of-type': {
        borderLeft: '1px solid #EDF1EE',
      },
      '& tbody tr td': {
        borderBottom: '1px solid #EDF1EE !important',
      },
    },
    '& .ant-pagination': {
      // marginTop: '50px !important'
    },
    '& .ant-table-footer': {
      width: '100%',
      background: 'transparent !important',
      padding: '0 0 !important',
      visibility: 'hidden',
      // margin: '0 8px',
      borderTop: 'none !important',
      position: 'absolute',
      minHeight: 80,
      bottom: 0,
      '&:before': {
        background: 'transparent !important',
      },
      '& .ant-table-tbody tr:hover': {
        '& td': {
          background: 'white',
        },
      },
      '& .ant-table-thead': {
        visibility: 'hidden',
      },
      '& .ant-table-thead tr th': {
        padding: 8,
      },
      '& .ant-table-tbody tr td': {
        padding: 8,
        borderBottom: '1px solid #EDF1EE !important',
        '&:last-child': {
          borderBottomRightRadius: 4,
        },
        '&:first-of-type': {
          borderBottomLeftRadius: 4,
        },
      },
    },
    '.ant-table-body': {
      position: 'relative',
      zIndex: 1,
    },
    '.ant-table-footer': {
      zIndex: 0,
    },
  },
}))

export const TipActionText = styled.div`
  margin-top: -8px;
  margin-left: 4px;
  font-family: Arial;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  font-weight: bold;
  color: ${brightGreen};
`

export const SectionWrapper = styled.div`
  overflow: hidden;
  width: 100%;
  position: relative;
  padding: 0 19px;
  transition: all 0.2s ease-in-out;
  margin-bottom: 62px;
`

export const Section = styled.div`
  width: 100%;
  min-height: 46px;
  margin-bottom: 19px;
  padding: 14px 21px;
  background: ${white};
  border: 1px solid ${forestGreen};
  box-sizing: border-box;
  border-radius: 5px;

  transition: all 0.2s ease-in-out;
`

export const SectionTitle = styled.div`
  position: relative;
  font-family: Arial;
  font-size: 15px;
  font-weight: bold;
  line-height: 17px;
  text-transform: capitalize;
  color: ${brightGreen};
  margin-bottom: 30px;
`

export const SectionOperation = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  position: absolute;
  top: 3px;
  right: 0px;
  max-width: 120px;
`

export const SectionOperationText = styled.div`
  font-size: 14px;
  line-height: 16px;
  text-align: center;
`

export const SectionOperationSplit = styled.div`
  width: 1px;
  height: 17px;
  margin: 0 14px;
  background-color: ${lightGrey4};
`

export const SectionRadioWrapper = styled.div`
  width: 100%;
  padding: 20px 20px;
  background-color: ${lightGrey2};
  border-radius: 6px;
`

export const SectionRadioRow = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  width: 100%;
`

export const SectionRadioColumn = styled.div`
  margin-right: 80px;
`

export const SectionRadioLabel = styled.div`
  font-family: Arial;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.05em;
  margin-right: 8px;
  color: ${mediumGrey};
`

export const SectionRadioText = styled.span`
  font-family: Arial;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.05em;
  font-weight: bold;
  color: ${mediumGrey};
`

export const SupplierWrapper = styled('div')({
  display: 'flex',
  justifyContent: 'space-between',
  padding: '40px 0 20px',
  alignItems: 'center',
})

export const SectionTableSplit = styled.div`
  width: 100%;
  margin: 25px auto;
  border: 1px solid ${mediumGrey};
`

export const SetionTableLabel = styled('span')({
  fontFamily: 'Arial',
  fontSize: 12,
  fontWeight: 'bold',
  textTransform: 'capitalize',
  color: mediumGrey,
  '&.normal': {
    textTransform: 'none',
  }
})

export const TableTip = styled.div`
  height: 40px;
  margin: 0 28px;
  font-family: Arial;
  font-size: 17px;
  font-weight: bold;
  text-align: center;
  line-height: 40px;
  letter-spacing: 0.05em;
  color: ${forestGreen};
  background: ${lightGreen2};
`

export const LotsSplit = styled.div`
  height: 0px;
  margin: 0 20px;
  border: 1px solid ${lightGrey5};
`

export const LotsPanelWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  margin: 0 20px;
`

export const LotsPanel = styled.div`
  flex: 1;
  min-width: 304px;
  min-height: 70px;
  margin: 24px 30px 24px 0;
  padding: 13px;
  background: ${white};
  border: 1px solid ${lightGrey5};
  box-sizing: border-box;
  border-radius: 6px;
`

export const LotsPanelTip = styled.div`
  font-family: Arial;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.05em;
  color: ${mediumGrey};
`

export const LotsPanelValue = styled.div`
  margin-top: 5px;
  font-family: Arial;
  font-size: 20px;
  line-height: 29px;
  letter-spacing: 0.05em;
  color: ${mediumGrey};
`

export const LotsPanelList = styled.div`
  flex: 1;
  padding-left: 20px;
  border-left: 1px solid ${lightGrey5};
`

export const LotsPanelListItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 4px 0;
`

export const LotsPanelListItemTip = styled.div`
  font-family: Arial;
  font-size: 12px;
  line-height: 14px;
  text-transform: uppercase;
  color: ${mediumGrey};
`

export const LotsPanelListItemValue = styled.div`
  padding-right: 220px;
  font-family: Arial;
  font-size: 12px;
  line-height: 14px;
  text-transform: uppercase;
  color: ${mediumGrey};
`

export const DragWrapper = styled.div`
  & .ant-upload-drag-container {
    height: 280px;
  }
`

export const DragSmallWrapper = styled.div`
  & .ant-upload.ant-upload-drag {
    height: 190px;
    overflow: hidden;
    .ant-upload {
      padding: 0 0;
    }
  }
`

export const ExpandedWrapper = styled.div`
  z-index: 20;
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  .ant-table {
    width: fit-content;
  }
`
export const ExpandedInner = styled.div`
  position: relative;
  width: 90%;
  margin: 100px auto;
  border: 5px solid ${brightGreen};
  padding: 26px 0;
  background-color: ${white};
`

export const ExpandedClose = styled.div`
  z-index: 21;
  position: absolute;
  top: 0;
  right: 0;
  width: 50px;
  height: 50px;
  line-height: 50px;
  transform: rotate(45deg);
  font-size: 50px;
  font-weight: bold;
  text-align: center;
  color: ${brightGreen};
  cursor: pointer;
`

export const AdjustmentsWrapper = styled.div`
  width: 100%;
  /* min-height: 101vh; */
  display: flex;
  padding: 10px 0 0;
  position: relative;
`

export const AdjustmentsForm = styled.div``

export const AdjustmentsTable = styled.div`
  padding-right: 15px;
  overflow-x auto;
`

export const AdjustmentsDress = styled.div`
  z-index: 23;
  position: absolute;
  top: 45px;
  right: -10px;
  width: 18px;
  height: 18px;
  transform: rotate(45deg);
  border-top: 2px solid rgba(82, 169, 103, 0.4);
  border-right: 2px solid rgba(82, 169, 103, 0.4);
  background-color: ${white};
`

export const GalleryText = styled.div``

export const GalleryImage = styled.img`
  display: block;
  width: 100%;s
`

export const ThemeModal = styled(Modal)((props) => ({
  '&': {
    '.ant-btn': {
      color: props.theme.primary,
      borderColor: props.theme.primary,
      '&:hover': {
        borderColor: props.theme.main,
        color: props.theme.main,
      },
      '&:focus': {
        borderColor: props.theme.primary,
        color: props.theme.primary,
      },
      '&.ant-btn-primary': {
        backgroundColor: props.theme.primary,
        borderColor: props.theme.primary,
        color: 'white',
        '&:hover': {
          backgroundColor: props.theme.theme,
        },
        '&:focus': {
          backgroundColor: props.theme.primary,
          borderColor: props.theme.primary,
        },
      },
    },
  },
}))

export const LotIdValue = styled.span`
  font-family: Arial;
  font-size: 16px;
  font-weight: bold;
  text-transform: capitalize;
  color: ${mediumGrey};
  height: 40px;
  line-height: 40px;
`
export const PricingGroupTableWrapper = styled('div')((props: any) => ({
  border: `1px solid ${lightGrey4}`,
  borderRadius: 4,
  padding: 12,
  height: 450,
  '& svg': {
    left: '0px !important',
    marginRight: '12px !important',
    cursor: 'pointer',
    fill: 'black !important',
  },
  '& .ant-calendar-picker input': {
    borderRadius: 0,
    borderColor: 'black',
    borderTop: 'none',
    borderLeft: 'none',
    borderRight: 'none',
    paddingLeft: 25,
    width: 120,
    color: props.theme.dark,
    fontWeight: 'bold',
  },
}))

export const ThemeDatePicker = styled(DatePicker)((props) => ({
  '.ant-input:hover': {
    borderColor: `${props.theme.primary} !important`,
  },
  '.ant-input:focus': {
    borderColor: `${props.theme.primary} !important`,
  },
  '& .ant-calendar-picker input': {
    borderRadius: 0,
    borderColor: 'black',
    borderTop: 'none',
    borderLeft: 'none',
    borderRight: 'none',
    paddingLeft: 25,
    width: 120,
    color: props.theme.dark,
    fontWeight: 'bold',
  },

  '.anticon-calendar': {
    marginRight: 3,
  },
}))

export const InitialAvailableSell = styled('div')((props) => ({
  '.ant-form-item-label': {
    lineHeight: '12px',
  },
}))

export const ReturnLabel = styled.span`
  color: red;
`

export const PricingHeader = styled.div`
  font-family: 'Museo Sans';
  font-size: 18px;
  line-height: 22px;
  color: 'black';
  & .ant-col {
    font-weight: normal;
  }
`

export const PricingSubHeader = styled.div`
  font-family: 'Museo Sans';
  font-size: 15px;
  line-height: 18px;
  color: #777;
  & .ant-col {
    font-weight: normal;
  }
`

export const PricingDesc = styled.div`
  font-family: 'Museo Sans';
  font-size: 15px;
  line-height: 18px;
  color: #333;
  & .ant-col {
    font-weight: normal;
  }
`

export const ProductInfo = styled.div`
  background: #f7f7f7;
  margin: -20px -24px;
  padding: 10px 20px;
  & .ant-col {
    font-weight: normal;
  }
`

export const ThumbnailImg = styled.img`
  width: 64px;
  height: 64px;
  border-radius: 18;
`

export const RatioFormula = styled.div((props) => ({
  marginLeft: '22px',
  '.ant-btn': {
    border: 'none',
  },
}))

export const TrashIconWrapper = styled.div((props: any) => ({
  position: 'absolute',
  bottom: 15,
  right: 20,
  'svg': {
    cursor: 'pointer'
  },
  'svg:hover': {
    'path': {
      fill: props.theme.dark
    }
  }
}))

export const StyledForm = styled(Form)`
  .ant-col .ant-form-item-label{
    display: block;
    text-align: left;
    > label.ant-form-item-no-colon::after{
      display: none;
    }
  }
`
