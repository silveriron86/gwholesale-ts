import React from "react"
import { formatNumber } from "~/common/utils"
import { ActivityItemLavel } from "~/modules/product/product.style"

interface LabelProps {
  date: string
  availableToSell: number
  unitsOnHand: number
  uom: string
}

export const ActivityItemAvailableLabel = (props: LabelProps) => {
  const {date, availableToSell, unitsOnHand, uom} = props
  return (
    <ActivityItemLavel>
      <div className="date">{date}</div>  
      <div className="content">
        <div className='item'>
          <div>AVAILABLE TO SELL</div>
          <div className="amount">{formatNumber(availableToSell, 2)}&nbsp;{uom}</div>
        </div>
        <div className='item'>
          <div>UNITS ON HAND</div>
          <div className="amount">{formatNumber(unitsOnHand, 2)}&nbsp;{uom}</div>
        </div>
      </div>
    </ActivityItemLavel>
  )
}