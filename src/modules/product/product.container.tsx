import React, { Component } from 'react'
import { connect } from 'redux-epics-decorator'
import { Route, Switch, Router } from 'react-router-dom'
import { ProductModule } from './product.module'
import { GlobalState } from '~/store/reducer'
import { isBrowser } from 'react-device-detect'
import { withTheme } from 'emotion-theming'

import {
  ProductSider,
  ProductDescription,
  ProductDetails,
  ProductGallery,
  ProductPanel,
  ProductActivity,
  ProductLots,
  ProductAdjustments,
  ProductPricing,
} from './components'
import { Wrapper, ContentWrapper } from './product.style'
import PageLayout from '~/components/PageLayout'
import { ProductSpecifications } from './components/product-specification.component'
import { UserRole } from '~/schema'
import ProductLogistics from './components/product-logistics'

class Product extends Component<T> {
  state = {
    openSubMenu: false,
  }

  componentDidMount() {
    this.props.history.push(this.props.location.pathname)
    const { id } = this.props.match.params
    const { pathname } = this.props.location

    this.props.getItems(id)
    this.props.getUserSetting()
    if (pathname.indexOf('gallery') > -1) {
      this.props.getGalleryDocuments(id)
    }
    if (this.props.items && this.props.items.wholesaleItemId != id) {
      this.props.resetData()
      this.props.resetLoading()
    }
    window.scrollTo(0, 0)
  }

  onExpandSubMenu = (status: Boolean) => {
    this.setState({ openSubMenu: status })
  }

  render() {
    const { openSubMenu } = this.state
    const { currentUser } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Products'}>
        <Wrapper>
          <ProductSider {...this.props} onExpand={this.onExpandSubMenu} />
          <ContentWrapper style={{ paddingLeft: openSubMenu || isBrowser ? 260 : 0 }}>
            <ProductPanel {...this.props} />
            <Switch>
              {([UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(currentUser.accountType) == -1) &&
                <Route
                  path="/product/:id/specifications"
                  title="Product Specifications"
                  render={() => <ProductSpecifications {...this.props} />}
                />
              }
              {([UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(currentUser.accountType) == -1) && (
                <Route
                  path="/product/:id/pricing"
                  title="Product Pricing"
                  render={() => <ProductPricing {...this.props} />}
                />
              )}
              <Route
                path="/product/:id/details"
                title="Product Details"
                render={() => <ProductDetails {...this.props} />}
              />
              <Route path="/product/:id/description" render={() => <ProductDescription {...this.props} />} />
              {([UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(currentUser.accountType) == -1) && (
                <Route
                  path="/product/:id/activity"
                  title="Product Activity"
                  render={() => <ProductActivity {...this.props} />}
                />
              )}
              {([UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(currentUser.accountType) == -1) && (
                <Route path="/product/:id/lots" title="Product Lots" render={() => <ProductLots {...this.props} />} />
              )}
              <Route
                path="/product/:id/gallery"
                title="Product Gallery"
                render={() => <ProductGallery {...this.props} />}
              />
              {([UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(currentUser.accountType) == -1) && (
                <Route path="/product/:id/logistics" title="Logistics" render={() => <ProductLogistics {...this.props} />} />
              )}
              {([UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED].indexOf(currentUser.accountType) == -1) && (
                <Route
                  path="/product/:id/adjustment"
                  title="Product Adjustment"
                  render={() => <ProductAdjustments {...this.props} />}
                />
              )}
            </Switch>
          </ContentWrapper>
        </Wrapper>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return {
    currentUser: state.currentUser,
    ...state.product,
  }
}

export const ProductContainer = withTheme(connect(ProductModule)(mapStateToProps)(Product))
