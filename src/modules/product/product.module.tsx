import {
  Module,
  EffectModule,
  ModuleDispatchProps,
  Effect,
  StateObservable,
  DefineAction,
  Reducer,
} from 'redux-epics-decorator'
import { Action } from 'redux-actions'
import { switchMap, map, endWith, catchError, takeUntil, startWith } from 'rxjs/operators'
import { Observable, of, from } from 'rxjs'

import { ProductService } from './product.service'
import { VendorService } from '../vendors/vendors.service'
import { SettingService } from '../setting/setting.service'
import { CustomerPriceSheet } from '~/modules/customers'
import { any } from 'prop-types'
import { TempOrder, WholesaleLabel } from '~/schema'
import { checkError, responseHandler, getFileUrl } from '~/common/utils'
import { MessageType } from '~/components'
import { GlobalState } from '~/store/reducer'
import { OrdersStateProps } from '~/modules/orders'
import { InventoryService } from '../inventory/inventory.service'
import { action } from '@storybook/addon-actions'

export interface ProductProps {
  purchaseOrders: any[]
  purchaseOrderItemTotal: number
  saleOrders: any[]
  saleOrderItemTotal: number
  adjustments: any[]
  lots: any[]
  receivedLots: any[]
  items: any
  categories: any[]
  currentCompanyUsers: any[]
  companyPmUsers: any[]
  companyProductTypes: any
  currentItemId: number
  loadingItems: boolean
  creating: boolean
  labelList: WholesaleLabel[]
  currentLabel: WholesaleLabel
  purchaseItemPricingList: any[]
  productPricing: any
  loadingProductPrice: Boolean
  printSetting: any
  userSetting: any
  savePricingLoading: Boolean
  itemSummary: any
  itemLocations: any[]
  productVendors: any[]
  briefVendors: any[]
  palletLocations: any[]
  fromAndToInventoryQtys: any
}

@Module('product')
export class ProductModule extends EffectModule<ProductProps> {
  defaultState = {
    purchaseOrders: [],
    purchaseOrderItemTotal: 0,
    saleOrders: [],
    saleOrderItemTotal: 0,
    adjustments: [],
    lots: [],
    receivedLots: [],
    items: {},
    categories: [],
    currentCompanyUsers: [],
    companyPmUsers: [],
    companyProductTypes: null,
    currentItemId: 0,
    loadingItems: true,
    creating: false,
    labelList: [],
    currentLabel: null,
    purchaseItemPricingList: [],
    productPricing: {},
    loadingProductPrice: false,
    printSetting: null,
    userSetting: null,
    savePricingLoading: false,
    itemSummary: {},
    itemLocations: [],
    productVendors: [],
    briefVendors: [],
    palletLocations: [],
    fromAndToInventoryQtys: null
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(
    private readonly product: ProductService,
    private readonly inventory: InventoryService,
    private readonly setting: SettingService,
    private readonly vendor: VendorService,
  ) {
    super()
  }

  @Effect({
    done: (state: any) => {
      return {
        ...state,
        purchaseOrders: [],
        saleOrders: [],
        adjustments: [],
        lots: [],
        receivedLots: [],
      }
    },
  })
  resetData(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: any) => {
      return {
        ...state,
        loadingProductPrice: !state.loadingProductPrice,
      }
    },
  })
  resetProductPricingLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        items: payload,
        currentItemId: payload.wholesaleItemId,
        loadingItems: false,
        savePricingLoading: false,
      }
    },
  })
  getItems(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getItems(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        purchaseOrders: payload.dataList ? payload.dataList : [],
        purchaseOrderItemTotal: payload.total ? payload.total : 0,
      }
    },
  })
  getPurchaseOrders(action$: Observable<void>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getOrderByItemId(data, 'VENDOR').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        saleOrders: payload.dataList ? payload.dataList : [],
        saleOrderItemTotal: payload.total ? payload.total : 0,
      }
    },
  })
  getSaleOrders(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getOrderByItemId(data, 'CUSTOMER').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, adjustments: payload }
    },
  })
  getAdjustments(action$: Observable<void>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getAdjustments(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, lots: payload }
    },
  })
  getLots(action$: Observable<void>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getLots(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, receivedLots: payload }
    },
  })
  getReceivedLots(action$: Observable<void>) {
    return action$.pipe(
      switchMap((data: any) => {
        return this.product.getReceivedLots(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any[]>) => {
      let item = payload ? payload : state.items
      if (item.wholesaleProductUomList != null) {
        let uomList = item.wholesaleProductUomList.filter((uom: any) => {
          return uom.id != null
        })
        item.wholesaleProductUomList = uomList
      }
      return {
        ...state,
        items: item,
        message: 'Update Items Successful',
        type: MessageType.SUCCESS,
      }
    },
  })
  updateItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((param) => this.product.updateItems(param.data, param.queryParams)),
      switchMap((res) => of(responseHandler(res, true).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, galleryDocuments: payload }
    },
  })
  getGalleryDocuments(action$: Observable<any>) {
    return action$.pipe(
      switchMap((productId) => {
        return this.product.getGalleryDocuments(productId).pipe(
          switchMap((data) => of(this.formatProductDocuments(data))),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, currentCompanyUsers: payload.userList }
    },
  })
  getCompanyUsers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.product.getCompanyUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, companyPmUsers: payload.userList }
    },
  })
  getCompanyPmUsers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.product.getCompanyPmUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        message: 'remove Successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  removeGalleryDocument(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) => {
        return this.product.removeGalleryDocument(data.productId, data.documentId).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getGalleryDocuments)(data.productId)),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    done: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        categories: action.payload,
      }
    },
  })
  getSaleCategories(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getAllCategories(state$.value.currentUser.company || 'GRUBMARKET').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        creating: false,
      }
    },
  })
  createItemAdjustments(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.product.createItemAdjustments(data).pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done'), takeUntil(this.dispose$)),
          endWith(this.createActionFrom(this.getAdjustments)({ id: state$.value.product.currentItemId })),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any) => {
      return {
        ...state,
        creating: true,
      }
    },
  })
  startCreating(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    before: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        savePricingLoading: true,
      }
    },
    done: (state: any, action: Action<any[]>) => {
      return {
        ...state,
      }
    },
  })
  updateItemPricingGroup(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.product.updateItemPricingGroup(data.id, data.info).pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done'), takeUntil(this.dispose$)),
          endWith(this.createActionFrom(this.getItems)(state$.value.product.currentItemId)),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any[]>) => {
      return {
        ...state,
      }
    },
  })
  setDefaultLogicAndGroup(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.product.setDefaultLogicAndGroup(data.id, data.info).pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done'), takeUntil(this.dispose$)),
          // endWith(this.createActionFrom(this.getItems)(state$.value.product.currentItemId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Reducer()
  resetLoading(state: any) {
    return { ...state, loadingItems: true, items: {} }
  }

  @Reducer()
  selectMsg(state: any, { payload }: Action<string>) {
    return { ...state, currentMsgId: payload }
  }

  formatProductDocuments(datas: any[]): Document[] {
    return datas.map((data) => ({
      uid: data.id,
      url: getFileUrl(data.url, false),
      status: 'done',
    }))
  }

  formatProductTypes = (datas: any[]) => {
    let result = {
      unitOfMeasure: [],
      returnReason: [],
      carrier: [],
      modifier: [],
      costStructure: [],
      modeOfTransportation: [],
      suppliers: [],
      grossWeightUnit: [],
      grossVolumeUnit: [],
    }

    // UNIT_OF_MEASURE, RETURN_REASON, CARRIER,MODIFIER,COST_STRUCTURE,MODE_OF_TRANSPORTATION
    datas.map((type: any) => {
      switch (type.type) {
        case 'UNIT_OF_MEASURE':
          result.unitOfMeasure.push(type)
          break
        case 'RETURN_REASON':
          result.returnReason.push(type)
          break
        case 'CARRIER':
          result.carrier.push(type)
          break
        case 'MODIFIER':
          result.modifier.push(type)
          break
        case 'COST_STRUCTURE':
          result.costStructure.push(type)
          break
        case 'MODE_OF_TRANSPORTATION':
          result.modeOfTransportation.push(type)
          break
        case 'SUPPLIERS':
          result.suppliers.push(type)
          break
        case 'GROSS_WEIGHT_UNIT':
          result.grossWeightUnit.push(type)
          break
        case 'GROSS_VOLUME_UNIT':
          result.grossVolumeUnit.push(type)
          break
      }
    })
    return result
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        labelList: action.payload,
      }
    },
  })
  getLabelList(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.product.getLabelList().pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        currentLabel: action.payload,
      }
    },
  })
  getLabelById(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.getLabelById(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  createLabel(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.createLabel(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getLabelList)()),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        message: 'Update label successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  updateLabel(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.updateLabel(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getLabelList)()),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        message: 'Delete label successfully',
        type: MessageType.SUCCESS,
      }
    },
  })
  deleteLabel(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.deleteLabel(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getLabelList)()),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        purchaseItemPricingList: action?.payload?.data || [],
        purchaseItemPricingTotal: action?.payload?.total || 0,
      }
    },
  })
  getProductPurchasePriceList(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.getProductPurchasePriceList(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body?.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        productPricing: action.payload,
        loadingProductPrice: false,
      }
    },
  })
  getProductPricingData(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.getProductPricingData(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        items: {
          ...state.items,
          wholesaleProductUomList: [...state.items.wholesaleProductUomList, action.payload],
        },
      }
    },
  })
  addProductUom(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.addProductUom(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              // endWith(this.createActionFrom(this.getItems)(state$.value.product.currentItemId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        printSetting: payload,
      }
    },
  })
  getPrintSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getPrintSetting().pipe(
          switchMap((data) =>
            of(data.body.data).pipe(
              map(this.createAction('done')),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        userSetting: payload.userSetting,
      }
    },
  })
  getUserSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getUserSetting(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          switchMap((data) =>
            of(data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        itemSummary: payload,
      }
    },
  })
  getOrderItemSummaryByItemId(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.getOrderItemSummaryByItemId(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data).pipe(map(this.createAction('done')))),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        itemLocations: payload,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  getItemLocations(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getItemLocations().pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: any) => ({
      ...state,
      productVendors: action.payload,
    }),
  })
  getProductVendors(action$: Observable<any>) {
    return action$.pipe(
      switchMap((productId) =>
        this.product.getProductVendors(productId).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect()
  addRemoveVendor(action$: Observable<any>) {
    return action$.pipe(
      switchMap(({ productId, vendorId }) =>
        this.product.addRemoveVendor(productId, vendorId).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getProductVendors)(productId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: any) => ({
      ...state,
      briefVendors: action.payload,
    }),
  })
  getBriefVendors(action$: Observable<any>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getBriefVendors().pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

    @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        palletLocations: payload,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  getPalletLocations(action$: Observable<string>) {
    return action$.pipe(
      switchMap((itemId: string) =>
        this.product.getPalletLocations(itemId).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return {
        ...state,
        fromAndToInventoryQtys: payload,
      }
    },
  })
  getItemInventoryQtyAtTwoDates(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.product.getItemInventoryQtyAtTwoDates(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data).pipe(map(this.createAction('done')))),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }
}
export type ProductDispatchProps = ModuleDispatchProps<ProductModule>
