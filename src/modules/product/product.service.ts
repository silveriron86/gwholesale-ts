import { Injectable } from 'redux-epics-decorator'

import { Http } from '~/common'
import { DateRange } from '~/schema'

interface ItemDateRange {
  id: string
  from?: string
  to?: string
}

interface LotDateRange {
  id: string
  available: boolean
  from?: string
  to?: string
}
@Injectable()
export class ProductService {
  constructor(private readonly http: Http) {}
  getGalleryDocuments(productId: string) {
    return this.http.get<any[]>(`/file/document/product/${productId.toString()}`)
  }

  getCompanyUsers() {
    return this.http.get<any[]>(`/session/getCompanyUsers`)
  }

  getCompanyPmUsers() {
    return this.http.get<any[]>(`/session/getCompanyPmUsers`)
  }

  removeGalleryDocument(productId: string, documentId: string) {
    return this.http.put<any[]>(`/file/document/product/${productId.toString()}/${documentId.toString()}`)
  }

  getItems(data: any) {
    const itemId = typeof data === 'object' ? data.itemId : data
    const clientId = typeof data === 'object' ? data.clientId : ''
    return this.http.get<any>(`/inventory/getItem/${itemId}?clientId=${clientId}`)
  }

  updateItems(items: any = {}, queryParams: any = {}) {
    let queryString = '';
    if(queryParams.ignoreSync) {
      queryString = `ignoreSync=true`
    }
    return this.http.post<any>(`/inventory/update/wholesaleitem?${queryString}`, {
      body: JSON.stringify(items),
    })
  }

  updateItemSuppliers(item: any = {}) {
    return this.http.post<any>(`/inventory/update/item-suppliers`, {
      body: JSON.stringify(item),
    })
  }

  getAdjustments(data: ItemDateRange) {
    return this.http.get<any[]>(`/inventory/itemAdjustement/list/${data.id}`, {
      query: { from: data.from, to: data.to }
    })
  }

  createItemAdjustments(data: any) {
    return this.http.post<any[]>(`/inventory/itemAdjustement/create/${data.wholesaleItemId}`, {
      body: JSON.stringify(data),
    })
  }

  getLots(data: LotDateRange) {
    if (data.from) {
      return this.http.get<any[]>(`/inventory/orderItemLot/${data.id}`, {
        query: { from: data.from, to: data.to },
      })
    } else {
      return this.http.get<any[]>(`/inventory/orderItemLot/${data.id}`)
    }
  }

  getReceivedLots(data: LotDateRange) {
    if (data.from) {
      return this.http.get<any[]>(`/inventory/orderReceivedItemLot/${data.id}`, {
        query: { from: data.from, to: data.to },
      })
    } else {
      return this.http.get<any[]>(`/inventory/orderReceivedItemLot/${data.id}`)
    }
  }

  getOrderByItemId(data: any, type: string) {
    return this.http.get<any[]>(`/inventory/item/${data.id}/orders/list`, {
      query: {
        type: type,
        fromDate: data.dateRange.from,
        toDate: data.dateRange.to,
        pageSize: data.pageSize,
        currentPage: data.currentPage,
      },
    })
  }

  updateItemPricingGroup(wholesaleItemId: number, data: any) {
    return this.http.put<any>(`/inventory/item/${wholesaleItemId}/pricing`, {
      body: JSON.stringify(data),
    })
  }

  setDefaultLogicAndGroup(wholesaleItemId: number, data: any) {
    return this.http.put<any>(`/inventory/item/${wholesaleItemId}/logic`, {
      body: JSON.stringify(data),
    })
  }

  getLabelList() {
    return this.http.get(`/label`)
  }

  getLabelById(data: number) {
    return this.http.get(`/label/${data}`)
  }

  createLabel(data: any) {
    return this.http.post(`/label`, {
      body: data,
    })
  }

  updateLabel(data: any) {
    return this.http.put(`/label`, {
      body: data,
    })
  }

  deleteLabel(data: any) {
    return this.http.delete(`/label`, {
      body: data,
    })
  }

  getProductPurchasePriceList(data: any) {
    let url = `/inventory/item/${data.itemId}/client-pricing?page=${data.page}&pageSize=${data.pageSize}`
    if (data.companyName) {
      url += `&companyName=${data.companyName}`
    }
    return this.http.get(url)
  }

  getProductPricingData(data: any) {
    let url = `/inventory/order-item/${data.wholesaleOrderItemId}/client/${data.clientId}`
    return this.http.get(url)
  }

  addProductUom(data: any) {
    let url = `/inventory/${data.itemId}/product-uom`
    delete data.itemId
    return this.http.post(url, {
      body: data,
    })
  }

  getOrderItemSummaryByItemId(data: any) {
    let url = `/inventory/item-activity-summary/${data.id}`
    return this.http.get(url, {
      query: {
        fromDate: data.from,
        toDate: data.to,
      },
    })
  }

  getProductVendors(productId: number) {
    return this.http.get(`/inventory/preferred-vendors/${productId}`)
  }

  addRemoveVendor(productId: number, vendorId: number) {
    return this.http.post(`/inventory/add-remove-preferred-vendor/${vendorId}/item/${productId}?noCancel`)
  }

  getPalletLocations(itemId: string) {
    return this.http.get<any>(`/location/item-pallet-level-locations/${itemId}`)
  }

  getItemInventoryQtyAtTwoDates(data: any) {
    const url = `/inventory/item-qty-by-date/${data.id}`
    return this.http.get(url, {
      query: {
        fromDate: data.from,
        toDate: data.to,
      },
    })
  }
}
