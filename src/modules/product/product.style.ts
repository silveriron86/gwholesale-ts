import styled from '@emotion/styled'
import { rgb } from 'polished'
import { lightGrey2, darkGrey, white, mediumGrey, black } from '~/common/color'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row nowrap;
  position: relative;
  box-sizing: border-box;
  width: 100%;
`

export const SiderWrapper = styled.aside`
  overflow: auto;
  height: 100vh;
  position: fixed;
  left: 0px;
  background: rgb(243, 243, 243);

  flex: 0 0 260px;
  width: 260px;
  z-index: 1;
  padding: 24px 0;
`

export const SiderItemWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 40px;
  cursor: pointer;
  margin-top: 4px;
  margin-bottom: 8px;
`

export const SiderItem = styled.div`
  font-family: Arial;
  font-size: 14px;
  line-height: 41px;
  padding-left: 90px;
  letter-spacing: 0.05em;
  color: ${darkGrey};
  font-size: 14px;
  font-weight: bold;
  text-align: left;
`
export const SideItemSelectedBorder = styled('div')((props) => ({
  width: 3,
  backgroundColor: props.theme.main,
  height: '100%',
  position: 'absolute',
  right: 0,
  top: 0,
}))

export const ContentWrapper = styled.div`
  width: 100%;
  background-color: ${white};
`

export const PanelWrapper = styled.div`
  width: 100%;
  padding: 26px 31px;
  position: relative;
  box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.1);
`

export const PanelLabel = styled.div`
  font-family: Arial;
  font-size: 12px;
  line-height: 14px;
  text-transform: uppercase;
  color: ${mediumGrey};
`

export const PanelTitle = styled.div`
  font-family: Arial;
  font-size: 24px;
  line-height: 35px;
  letter-spacing: 0.05em;
  color: ${black};
`

export const PanelContent = styled.div`
  display: flex;
  flex-direction: row nowrap;
  justify-content: space-between;
  width: 100%;
  /* height: 81px; */
  padding-right: 29px;
  margin-top: 12px;
  margin-bottom: 34px;
  background: rgba(82, 158, 99, 0.1);
  border-radius: 6px;
`

export const PanelItem = styled.div`
  padding: 18px 20px;
`

export const PanelItemLabel = styled.div`
  font-family: Arial;
  font-size: 12px;
  line-height: 14px;
  text-transform: uppercase;
  color: ${mediumGrey};
`
export const PanelItemContent = styled.div`
  padding-top: 3px;
  padding-bottom: 3px;
  font-family: Arial;
  font-size: 17px;
  line-height: 24px;
  letter-spacing: 0.05em;
  text-transform: uppercase;
  border-bottom: 1px solid ${black};
  color: ${black};
`
export const PricingTableWrapper = styled('div')({
  '& .ant-table table': {
    border: 'none !important',

    '& .ant-form-item': {
      marginBottom: '0 !important'
    },
    '& thead': {
      boxShadow: 'none !important',
      '& th': {
        border: 'none !important'
      }
    },
    '& tbody tr td': {
      border: '3px solid white',
      backgroundColor: 'rgba(82, 158, 99, 0.1) !important',
      '& .ant-input-number': {
        width: 60
      },
      '& span.left-align-value': {
        paddingLeft: 8
      },
    },
    '& tbody tr td:nth-of-type(3)': {
      backgroundColor: 'rgba(26, 97, 163, 0.1) !important',
      color: '#1A61A3 !important',

      '& input': {
        color: '#1A61A3 !important'
      }
    },
    '& tbody tr td:nth-of-type(4)': {
      backgroundColor: 'rgba(255, 113, 94, 0.1) !important',
      color: '#C01700 !important',
      '& input': {
        color: '#C01700 !important'
      }
    },
    '& tbody tr td:nth-of-type(6)': {
      color: '#1C6E31 !important',
      '& .ant-form-item-control-wrapper': {
        width: '100% !important'
      },
      '& input': {
        color: '#1C6E31 !important',
        textAlign: 'right',
        paddingRight: 24
      }
    },
    '& tbody tr td:nth-of-type(7)': {
      color: '#1C6E31 !important',
      '& input': {
        color: '#1C6E31 !important'
      }
    }
  }
})

export const LogisticWrapper = styled('div')({
  '.form-item': {
    '.ant-form-item-label': {
      lineHeight: '10px'
    }
  }
})

export const ActivityItemLavel = styled('div')({
  margin: '0 24px',
  '.date': {
    fontSize: 22,
    fontWeight: 600,
    marginBottom: 6
  },
  '.content': {
    borderTop: '1px solid black',
    display: 'flex',
    '.item': {
      margin: '6px 12px',
      '.amount': {
        marginTop: 6,
        fontSize: 22,
        fontWeight: 600
      }
    }
  }
})