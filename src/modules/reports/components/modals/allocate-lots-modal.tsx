import { Select } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { Link } from 'react-router-dom'
import { formatNumber, getOrderPrefix } from '~/common/utils'
import { ThemeSelect, ThemeTable } from '~/modules/customers/customers.style'
import { DialogBodyDiv } from '~/modules/customers/nav-sales/styles'

type AllocateLotsModalProps = {
  allocateLotsData: any[]
  visible: boolean
  sellerSetting: any
  allocateLots: Function
}

export default class AllocateLotsModal extends React.PureComponent<AllocateLotsModalProps> {
  state = {
    dataSource: this.props.allocateLotsData,
    newAllocations: {}
  }

  componentWillReceiveProps(nextProps: AllocateLotsModalProps) {
    if(nextProps.visible) {
      if (JSON.stringify(this.props.allocateLotsData) != JSON.stringify(nextProps.allocateLotsData) && nextProps.allocateLotsData) {
        this.setState({
          dataSource: nextProps.allocateLotsData
        })
      }
    }
  }

  onClickAllocate = (filter: any) => {
    const { newAllocations } = this.state
    const orderItemIds = Object.keys(newAllocations)
    let data = {}
    orderItemIds.forEach(orderItemId => {
      if(newAllocations[orderItemId].lotOrderItemId) {
        data[orderItemId] = newAllocations[orderItemId]
      }
    });
    
    this.props.allocateLots({allocate: data, filter})
    this.setState({newAllocations: {}})
  }
  
  onLotChange = (wholesaleOrderItemId: number, lotOrderItemId: number, record: any) => {
    let newAllocations: any = {...this.state.newAllocations}
    const allocation = {
      [wholesaleOrderItemId]: {
        lotOrderItemId,
        oldQuantity: record.quantity,
        oldPicked: record.picked
      }
    }
    newAllocations = {...newAllocations, ...allocation}
    this.setState({newAllocations: JSON.parse(JSON.stringify(newAllocations))})
  }

  render() {   
    const { dataSource } = this.state
    console.log(this.state.newAllocations)
    const columns: any[] = [
      {
        title: 'Customer',
        dataIndex: 'companyName',
      },
      {
        title: 'Date',
        dataIndex: 'deliveryDate',
        align: 'left',
        render: (deliveryDate: string) => {
          return moment(deliveryDate).format('MM/DD/YYYY')
        }
      },
      {
        title: 'Order no.',
        dataIndex: 'wholesaleOrderId',
        align: 'center',
        render: (wholesaleOrderId: number, record: any) => {
          const orderPrefix = getOrderPrefix(this.props.sellerSetting, 'sales')
          return (
            <Link to={`/sales-order/${wholesaleOrderId}`}>{orderPrefix ? `${orderPrefix}` : ''}{wholesaleOrderId}</Link>
          )
        }
      },
      {
        title: 'Item(SKU)',
        dataIndex: 'sku',
        align: 'left',
        render: (sku: string, record: any) => {
          return `${record.variety}` + (sku ? `(${sku})` : '')
        }
      },
      {
        title: 'Order qty',
        dataIndex: 'quantity',
        align: 'right',
        render: (quantity: number, record: any) => {
          return `${formatNumber(quantity, 2)} ${record.inventoryUOM}`
        }
      },
      {
        title: 'Shipped qty',
        dataIndex: 'picked',
        align: 'right',
        render: (picked: number, record: any) => {
          return `${formatNumber(picked, 2)} ${record.inventoryUOM}`
        }
      },
      {
        title: 'Lot Allocation',
        dataIndex: 'lotId',
        width: 300,
        render: (lotId: string, record: any) => {
          let redColorStyle = {}
          if (record.availableLotItems.length ) {
            const selectedAllocation = this.state.newAllocations[record.wholesaleOrderItemId]
            const selectedLotItem = record.availableLotItems.find((el: any)=>selectedAllocation && el.purchaseOrderItemId == selectedAllocation.lotOrderItemId)
            if(selectedLotItem) {
              if(record.picked) {
                redColorStyle = {
                  color: selectedLotItem.lotAvailableQty < record.picked ? 'red' : 'inherit'
                }
              } else {
                redColorStyle = {
                  color: selectedLotItem.lotAvailableQty < record.quantity ? 'red' : 'inherit'
                }
              }
            }
            return (
              <ThemeSelect
                onChange={(lotOrderItemId: number)=>this.onLotChange(record.wholesaleOrderItemId, lotOrderItemId, record)}
                style={{ 
                  ...redColorStyle,
                  width: '100%',
                }}
                defaultValue={0}
                value={!!selectedAllocation ? selectedAllocation.lotOrderItemId : 0}
              >
                <Select.Option value={0}>Do not allocate</Select.Option>
                {record.availableLotItems.map((el: any) => {
                  return <Select.Option key={el.purchaseOrderItemId} value={el.purchaseOrderItemId}>
                    {`${el.lotId} (${formatNumber(el.lotAvailableQty, 2)} ${el.inventoryUOM}; ${el.wholesaleItemStatus == 'RECEIVED' ? 'Received' : moment(el.deliveryDate).format('MM/DD/YYYY') })`}
                  </Select.Option>
                })
                }
              </ThemeSelect>
            )
          }
          return 'No available lots'
        }
      },    
    ]

    return (
      <div>
        <DialogBodyDiv>
          <div style={{ marginBottom: 12 }}>Select the lots to which you want to allocate your sales order items which missing lots.</div>
          <ThemeTable 
            columns={columns}
            dataSource={dataSource}
          />
        </DialogBodyDiv>
      </div>
    )
  }
}