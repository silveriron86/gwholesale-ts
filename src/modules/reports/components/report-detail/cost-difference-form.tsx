import * as React from 'react'
import Form, { FormComponentProps } from 'antd/lib/form'
import { DatePicker, Select } from 'antd'
import { ThemeButton, ThemeSelect } from '~/modules/customers/customers.style'
import { Icon as IconSvg } from '~/components/icon'
import moment from 'moment'
import { getFromAndToByDateType } from '~/common/utils'

const FormItem = Form.Item
interface FiltersFormProps extends FormComponentProps {
  reportType: string
  onRun: (data: any) => void
  title: string
  filter: any
  isVertial: boolean
}

class CostDifferenceForm extends React.PureComponent<FiltersFormProps> {
  formRef = React.createRef<any>()
  state = {
    isReload: true,
    isAfterRun: false,
    selectedDateType: this.props.filter ? this.props.filter.dateType : 2
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.filter) != JSON.stringify(nextProps.filter)) {
      this.setState({ selectedDateType: nextProps.filter.dateType })
    }

  }

  handleSubmit = (e: any) => {
    const { form, reportType, filter } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        //define type to 10 for sales, 15 for purchase
        const data: any = { ...values, type: filter.type ? filter.type : reportType == 'sales' ? 10 : reportType == 'purchase' ? 15 : 20 }
        if (values.dateType == 6 && data.dateRange) {
          if (data.dateRange[0]) {
            data.from = moment(data.dateRange[0]).format('MM/DD/YYYY')
          }
          if (data.dateRange[1]) {
            data.to = moment(data.dateRange[1]).format('MM/DD/YYYY')
          }
        } else {
          const fromTo: any = getFromAndToByDateType(values.dateType)
          if (fromTo.from && fromTo.to) {
            data.from = fromTo.from
            data.to = fromTo.to
          }
        }
        delete data.dateRange


        this.setState(
          {
            isAfterRun: true,
          }, () => {
            this.props.onRun(data)
          })
      }
    })
  }

  onDateChange = (dateType: number) => {
    this.setState({ selectedDateType: dateType })
  }

  render() {
    const { filter } = this.props
    const { getFieldDecorator } = this.props.form
    const dateTypeSelect = (
      <ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateChange}>
        <Select.Option value={1}>All</Select.Option>
        <Select.Option value={2}>Today</Select.Option>
        <Select.Option value={3}>Tomorrow</Select.Option>
        <Select.Option value={4}>Yesterday</Select.Option>
        <Select.Option value={5}>Past Week</Select.Option>
        <Select.Option value={6}>Custom Date Range</Select.Option>
      </ThemeSelect>
    )
    return (
      <Form ref={this.formRef} onSubmit={this.handleSubmit} hideRequiredMark={true}>
        <div style={{ display: !this.props.isVertial ? 'flex' : 'block', flexWrap: 'wrap' }}>
          <FormItem label={filter.type == 10 || filter.type == 20 ? 'Fulfillment Date' : 'Scheduled Delivery Date'}>
            {getFieldDecorator('dateType', {
              rules: [{ required: false }],
              initialValue: filter && filter.dateType ? filter.dateType : 2,
            })(dateTypeSelect)}
          </FormItem>
          {this.state.selectedDateType === 6 && (
            <FormItem
              label={<span>&nbsp;</span>}
              colon={false}
              style={{ marginLeft: this.props.isVertial ? 0 : 20, width: this.props.isVertial ? '100%' : '' }}
            >
              {getFieldDecorator('dateRange', {
                rules: [{ required: false }],
                initialValue: filter ? [moment(filter.from), moment(filter.to)] : [,]
              })(<DatePicker.RangePicker
                placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                format={'MM/DD/YYYY'}
                className="report-dp" />)}
            </FormItem>
          )}
        </div>

        <ThemeButton type="primary" htmlType="submit" style={{ height: 42 }}>
          <IconSvg
            viewBox="0 0 24 24"
            width="24"
            height="24"
            type="run"
            style={{ fill: 'transparent', float: 'left' }}
          />
          <div
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              lineHeight: '21px',
              fontFamily: 'Museo Sans Rounded',
              display: 'block',
              float: 'left',
              marginTop: 2,
              marginLeft: 3,
            }}
          >
            Run Report
          </div>
        </ThemeButton>
      </Form>
    )
  }
}

export default Form.create<FiltersFormProps>()(CostDifferenceForm)
