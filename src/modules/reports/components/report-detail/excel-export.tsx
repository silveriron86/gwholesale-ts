import * as React from 'react'
import { Theme } from '~/common'
import { ThemeButton } from '~/modules/customers/customers.style'
import { formatDate, formatNumber, getOrderPrefix } from '~/common/utils'
import moment from 'moment'
import { getDays, getFullCost, getItemName } from '../../report-contents/inventory'
import { formatNumber, formatOrderStatus, getFromAndToByDateType, getOrderPrefix } from '~/common/utils'
interface ExcelExportProps {
  theme: Theme
  data: any[]
  filter: any
  sellerSetting: any
}

class ExcelExport extends React.PureComponent<ExcelExportProps> {
  state: any
  urlPath: string = ''
  componentDidMount() {
    const href = window.location.href
    const pathStr = "#/reports/"
    const index = href.indexOf(pathStr)
    const rootPath = href.substr(0, index)
    this.urlPath = rootPath
  }
  _buildItem = (record: any) => {
    return {
      'Sales Representatives':
        typeof record.firstName !== 'undefined'
          ? `${record.firstName} ${record.lastName}`
          : typeof record.companyName !== 'undefined'
            ? record.companyName
            : record.itemName,
      customerId:
        typeof record.clientId !== 'undefined'
          ? record.clientId
          : typeof record.wholesaleItemId !== 'undefined'
            ? record.wholesaleItemId
            : '',
      Units: record.totalUnits,
      Revenue: `$${formatNumber(record.totalRevenue, 2)}`,
      Cost: `$${formatNumber(record.totalCost, 2)}`,
      'Gross Profit': `$${formatNumber(this._getGrossProfit(record), 2)}`,
      Markup: `${formatNumber(this._getMarkup(record), 2)}%`,
      'Gross Margin': `${formatNumber(this._getGrossMargin(record), 2)}%`,
    }
  }

  _buildProductItem = (record: any) => {
    return {
      Category: typeof record.categoryName !== 'undefined' ? record.categoryName : record.variety,
      'Lot #': typeof record.lotId !== 'undefined' ? record.lotId : '',
      'Sale Date': typeof record.receiveDate !== 'undefined' ? formatDate(record.receiveDate) : '',
      Revenue: `$${formatNumber(record.totalRevenue, 2)}`,
      Cost: `$${formatNumber(record.totalCost, 2)}`,
      'Gross Profit': `$${formatNumber(this._getGrossProfit(record), 2)}`,
      Markup: `${formatNumber(this._getMarkup(record), 2)}%`,
      'Gross Margin': `${formatNumber(this._getGrossMargin(record), 2)}%`,
    }
  }

  _buildSaleProductOrderItem = (record: any) => {
    return {
      Category: !!record.categoryName ? record.categoryName : record.variety,
      'Customer': !!record.companyName ? record.companyName : '',
      'Order #': !!record.salesOrderId ? record.salesOrderId : '',
      'Sale Date': !!record.receiveDate ? formatDate(record.receiveDate) : '',
      Units: !!record.totalUnits ? record.totalUnits : '',
      Revenue: `$${formatNumber(record.totalRevenue, 2)}`,
      Cost: `$${formatNumber(record.totalCost, 2)}`,
      'Gross Profit': `$${formatNumber(this._getGrossProfit(record), 2)}`,
      Markup: `${formatNumber(this._getMarkup(record), 2)}%`,
      'Gross Margin': `${formatNumber(this._getGrossMargin(record), 2)}%`,
      UOM: record.UOM
    }
  }

  _getGrossProfit = (record: any) => {
    return record.totalRevenue - record.totalCost
  }

  _getMarkup = (record: any) => {
    return !record.totalCost ? 0 : (this._getGrossProfit(record) / record.totalCost) * 100
  }

  _getGrossMargin = (record: any) => {
    return !record.totalRevenue ? 0 : (this._getGrossProfit(record) / record.totalRevenue) * 100
  }

  exportFile = (title: string) => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('reports-table'))
    var wscols = [
      { wpx: 150 },
      { wpx: 150 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols
    // XLSX.writeFile(wb, 'Reports.xlsx')
    XLSX.writeFile(wb, title)
    return false
  }

  exportCostPriceData = () => {
    const { filter, data } = this.props
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('reports-table'))
    var wscols = [
      { wpx: 70 },
      { wpx: 100 },
      { wpx: filter.type == 20 ? 100 : 200 },
      { wpx: filter.type == 20 ? 200 : 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols
    data.forEach((el, index) => {
      let target = `${this.urlPath}#/order/${el.wholesaleOrderId}/purchase-cart`
      if (filter.type == 10 || filter.type == 20) {
        target = `${this.urlPath}#/sales-order/${el.wholesaleOrderId}`
      }
      wb.Sheets.Sheet1['A' + (index + 2)].l = { Target: target }  //set link to be redirected to matching order
      if (wb.Sheets.Sheet1['G' + (index + 2)]) {
        wb.Sheets.Sheet1['G' + (index + 2)].t = "s" //display Margin column as text to show the '%' symbol
        if (filter.type == 10) {
          wb.Sheets.Sheet1['G' + (index + 2)].v = el.price ? `${formatNumber(((el.price - el.lotCost) / el.price) * 100, 2)}%` : 'Error: 0 price'
        } else if (filter.type == 15) {
          wb.Sheets.Sheet1['G' + (index + 2)].v = el.itemPrice != 0 ? `${formatNumber(((el.itemPrice - el.cost) / el.itemPrice) * 100, 2)}%` : 'Error: 0 price'
        }
      }

    });

    const today = moment().format('MM.DD.YYYY')
    let title = `Sales Price Below Cost Report-${today}.xlsx`
    if (filter.type == 15) {
      title = `Purchase Cost Below Price Report-${today}.xlsx`
    } else if (filter.type == 20) {
      title = `Backorder Report-${today}.xlsx`
    }
    XLSX.writeFile(wb, title)
    return false
  }

  exportAllocationIssues = () => {
    const { filter, data } = this.props
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('reports-table'))
    var wscols = [
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols
    data.forEach((el, index) => {
      let orderTarget = `${this.urlPath}#/sales-order/${el.wholesaleOrderId}`
      wb.Sheets.Sheet1['A' + (index + 2)].l = { Target: orderTarget }  //set link to be redirected to matching order
      let activityTarget = `${this.urlPath}#/product/${el.itemId}/activity`
      wb.Sheets.Sheet1['B' + (index + 2)].l = { Target: activityTarget }  //set link to be redirected to matching order
    });

    const today = moment().format('YYYY/MM/DD')
    let title = `${today}-Allocations for Tomorrow.xlsx`

    XLSX.writeFile(wb, title)
    return false
  }

  exportUnallocationLots = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('reports-table'))
    var wscols = [
      { wpx: 200 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 200 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols

    const today = moment().format('YYYY/MM/DD')
    let title = `${today}-Lot errors by product.xlsx`

    XLSX.writeFile(wb, title)
    return false
  }

  exportInventoryReport = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('reports-table'))
    var wscols = [
      { wpx: 200 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 200 },
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols

    const today = moment().format('YYYY/MM/DD')
    let title = `Inventory Report-${today}.xlsx`

    XLSX.writeFile(wb, title)
    return false
  }

  formatLotErrorByOrderDownloadTable = () => {
    const { data } = this.props
    if (!data) return
    let allChildren: any[] = []
    data.forEach(el => {
      allChildren = [...allChildren, ...el.orderItemList]
    });
    return allChildren.map((row: any, index: number) => {
              return (
                <tr key={`row-${index}`}>
                  <td>{row['companyName']}</td>
                  <td>{row['orderId']}</td>
                  <td>{row['orderStatus']}</td>
                  <td>{row['salesRep']}</td>
                  <td>{row['sku']}</td>
                  <td>{row['lotId']}</td>
                  <td>{row['lotStatus']}</td>
                  <td>{row['lotAvailableQty']}</td>
                  <td>{row['quantity'] ? `${formatNumber(row['quantity'], 2)} ${row['UOM']}` : ''}</td>
                  <td>{row['picked'] ? `${formatNumber(row['picked'], 2)} ${row['UOM']}` : ''}</td>
                </tr>
              )
            })
  }

  formatUnallocatedLotsDownloadTable = () => {
    const { data } = this.props
    if (!data) return
    let allChildren: any[] = []
    data.forEach(el => {
      allChildren = [...allChildren, ...el.children]
    });
    return allChildren.map((row: any, index: number) => {
              return (
                <tr key={`row-${index}`}>
                  <td>{row['variety']}</td>
                  <td>{row['sku']}</td>
                  <td>{row['wholesaleOrderId']}</td>
                  <td>{moment(row['deliveryDate']).format('MM/DD/YYYY')}</td>
                  <td>{row['companyName']}</td>
                  <td>{row['salesRep']}</td>
                  <td>{row['quantity']} {row.inventoryUOM}</td>
                  <td>{row['catchWeightQty']} {row.inventoryUOM}</td>
                </tr>
              )
            })
  }

  formatInventoryExportData = () => {
    const { data, filter } = this.props
    let result: any[] = []
    let allChildren: any[] = []
    if (data) {
      data.forEach(el => {
        const d = {...el}
        allChildren = [...allChildren, {...d}]
        allChildren = [...allChildren, ...el.children]
      });
    }

    allChildren.forEach(record => {
      // calculate days on floor
      let daysOnFloor = ''
      let showAVG = false
      if(typeof record.footer !== 'undefined') {
        daysOnFloor = formatNumber(record.footer.days, 0)
      }
      if(record.children && record.children.length > 0) {
        let sum = 0
        record.children.forEach((child: any) => {
          sum += getDays(child.deliveryDate)
        })
        daysOnFloor = `${Math.floor(sum / record.children.length)}`
        showAVG = true
      }

      if(!record.deliveryDate || typeof record.deliveryDate === 'undefined') {
        daysOnFloor = ''
      }

      daysOnFloor = `${getDays(record.deliveryDate)}`


      // calculate received qty
      let receivedQty = ''
      let showTOT = typeof record.children !== 'undefined'
      if(typeof record.footer !== 'undefined') {
        receivedQty = formatNumber(record.footer.receivedQty, 0)
      }
      receivedQty =  record.receivedQty ? formatNumber(record.receivedQty, 0) : '--'

      // calculate qty sold
      let soldQty = ''      
      if(typeof record.footer !== 'undefined') {
        soldQty = formatNumber(record.footer.soldQty, 0)
      }
      soldQty = record.soldQty ? formatNumber(record.soldQty, 0) : '--'

      // calculate qty on hand
      let onHandQty = ''
      if(typeof record.footer !== 'undefined') {
        onHandQty = formatNumber(record.footer.onHandQty, 0)
      }
      if (typeof record.children === 'undefined' && !filter.includeLotsWithZero && !filter.includeLotsWithNegative) {
        // Certain lots do not show up unless ``Include lots with 0 on hand quantity`` and ``Include lots with negative on hand quantity`` are checked.
        onHandQty = ''
      }
      onHandQty = record.onHandQty ? formatNumber(record.onHandQty, 0) : '--'
      let perAllocateCost = typeof record.perAllocateCost === 'undefined' ? '' : `$${formatNumber(getFullCost(record), 2)}*`

      // calculate inventory value
      let inventoryValue = ''
      let showInventoryTOT = false
      if(typeof record.footer !== 'undefined') {
        inventoryValue = `$${formatNumber(record.footer.inventoryValue, 2)}*`
      }
      if(record.children && record.children.length > 0) {
        let sum = 0
        record.children.forEach((child: any) => {
          sum += getFullCost(child) * child.onHandQty
        })
        showInventoryTOT = true
        inventoryValue =formatNumber(sum, 2)
      }
      inventoryValue = `$${formatNumber(getFullCost(record) * record.onHandQty, 2)}*`
      let wholesaleOrderId = record.wholesaleOrderId ? getOrderPrefix(this.props.sellerSetting, 'purchase') + record.wholesaleOrderId : ''

      // item
      let item = ''
      if(typeof record.footer !== 'undefined') {
        item = 'TOTALS'
      }
      item = getItemName(record)

      // locations
      const locations = !record.locations || record.locations.length === 0 ? '' : record.locations.join(', ')
      
      result.push({
        vendor: record.children ? record.variety : record.companyName,
        sku: record.sku,
        deliveryDate: record.children || !record.deliveryDate ? '' : moment(record.deliveryDate).format('MM-DD-YY'),
        lotId: record.children ? '' : record.lotId,
        wholesaleOrderId: record.children ? '' : wholesaleOrderId,
        item: record.children ? '' : item,
        daysOnFloor,
        showAVG,
        receivedQty,
        showTOT,
        soldQty,
        onHandQty,
        perAllocateCost,
        inventoryValue,
        showInventoryTOT,
        locations,
        po: record.po,
        pickerNote: record.pickerNote
      })
    });


    return (
      result.map((el, index)=> {
        return (
          <tr key={index}>
            <td>{el.vendor}</td>
            <td>{el.sku}</td>
            <td>{el.deliveryDate}</td>
            <td>{el.wholesaleOrderId}</td>
            <td>{el.lotId}</td>
            <td>{el.po}</td>
            <td>{el.pickerNote}</td>
            <td>
              {el.showAVG ? <span>AVG &nbsp;</span> : ''}
              {el.daysOnFloor}
            </td>
            <td>
              {el.showTOT ? <span>TOT &nbsp;</span> : ''}
              {el.receivedQty}
            </td>
            <td>
              {el.showTOT ? <span>TOT &nbsp;</span> : ''}
              {el.soldQty}
            </td>
            <td>
              {el.showTOT ? <span>TOT &nbsp;</span> : ''}
              {el.onHandQty}
            </td>
            <td>{el.perAllocateCost}</td>
            <td>
              {el.showInventoryTOT ? <span>TOT &nbsp;</span> : ''}
              {el.inventoryValue}
            </td>
            <td>{el.locations}</td>
          </tr>
        )
      })
    )
  }

  exportExceptionReport = () => {
    const {filter} = this.props
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('reports-table'))
    var wscols = [
      { wpx: 250 },
      { wpx: 100 },
      { wpx: 250 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols

    const today = moment().format('YYYY/MM/DD')
    let title = `Unshipped Sales Order Report-${today}.xlsx`
    if (filter.type == 50) {
      title = `Unreceived Purchase Order Report-${today}.xlsx`
    }

    XLSX.writeFile(wb, title)
    return false
  }

  render() {
    const { data, filter } = this.props
    const { type } = filter
    const MDY = 'MM/DD/YYYY'
    
    let datesString = 'All Dates'
    if (typeof filter.from !== 'undefined') {
      datesString = `${filter.from} to ${filter.to}`
    } else {
      datesString = [
        'All',
        `${moment()
          .clone()
          .startOf('month')
          .format(MDY)} to ${moment().format(MDY)}`,
        `${moment()
          .clone()
          .startOf('year')
          .format(MDY)} to ${moment().format(MDY)}`,
        `${moment()
          .subtract(365, 'days')
          .format(MDY)} to ${moment().format(MDY)}`,
        `${moment()
          .subtract(30, 'days')
          .format(MDY)} to ${moment().format(MDY)}`,
        `${moment()
          .subtract(4, 'weeks')
          .format(MDY)} to ${moment().format(MDY)}`,
        `${moment()
          .subtract(2, 'weeks')
          .format(MDY)} to ${moment().format(MDY)}`,
        `${moment()
          .subtract(1, 'weeks')
          .format(MDY)} to ${moment().format(MDY)}`,
        `${moment().format(MDY)} to ${moment().format(MDY)}`,
      ][filter.dateType]
    }

    if (type === 2) {
      let mockData = [] as any[]
      if (data) {
        data.forEach((category, i) => {
          if (i > 0) {
            mockData.push({
              Category: 'Category',
              'Lot #': '',
              'Sale Date': '',
              Revenue: 'Sales',
              Cost: 'Costs',
              'Gross Profit': 'Profit',
              Markup: 'Markup',
              'Gross Margin': 'Gross Margin',
            })
          }
          mockData.push(this._buildProductItem(category))
          if (category.productList && category.productList.length > 0) {
            mockData.push({
              Category: 'Category',
              'Lot #': 'Lot #',
              'Sale Date': 'Sale Date',
              Revenue: 'Sales',
              Cost: 'Cost',
              'Gross Profit': 'Gross Profit',
              Markup: 'Markup',
              'Gross Margin': 'Gross Margin',
            })
          }
          if (category.productList) {
            category.productList.forEach((client: any, __i) => {
              mockData.push(this._buildProductItem(client))
              client.summary.forEach((summary: any, ___i) => {
                mockData.push(this._buildProductItem(summary))
              })
            })
          }
        })
      }

      return (
        <>
          <ThemeButton size="large" onClick={()=>this.exportFile(`Sales by Product Report-${moment().format('MM.DD.YYYY')}.xlsx`)}>
            Download
          </ThemeButton>
          <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            <thead>
              <tr>
                <th colSpan={8}>{datesString}</th>
              </tr>
              <tr>
                <th>Category</th>
                <th />
                <th align="right" />
                <th align="right">Sales</th>
                <th align="right">Costs</th>
                <th align="right">Profit</th>
                <th align="right">Markup</th>
                <th align="right">Gross Margin</th>
              </tr>
            </thead>
            <tbody>
              {mockData.map((row: any, index: number) => (
                <tr key={`row-${index}`}>
                  <td>{row['Category']}</td>
                  <td>{row['Lot #']}</td>
                  <td>{row['Sale Date']}</td>
                  <td>{row['Revenue']}</td>
                  <td>{row['Cost']}</td>
                  <td>{row['Gross Profit']}</td>
                  <td>{row['Markup']}</td>
                  <td>{row['Gross Margin']}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      )
    } else if (type == 10) {
      return (
        <>
          <ThemeButton size="large" onClick={this.exportCostPriceData}>
            Download
          </ThemeButton>
          <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            <thead>
              <tr>
                <th>Order No.</th>
                <th>SKU</th>
                <th>Product</th>
                <th>Lot No.</th>
                <th>Sales Price</th>
                <th>Cost</th>
                <th>Margin</th>
                <th>Fulfillment Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {data && data.map((row: any, index: number) => {
                return (
                  <tr key={`row-${index}`}>
                    <td>{getOrderPrefix(this.props.sellerSetting, 'sales') + row['wholesaleOrderId']}</td>
                    <td>{row['sku']}</td>
                    <td>{row['variety']}</td>
                    <td>{row['lotId']}</td>
                    <td>${formatNumber(row['price'], 2)}/{row.defaultSalesUnitUOM}</td>
                    <td>${formatNumber(row['lotCost'], 2)}/{row.defaultSalesUnitUOM}</td>
                    <td></td>
                    <td>{row['createdDate'] ? moment(row['createdDate']).format('MM/DD/YYYY') : ''}</td>
                    <td>{row['status']}</td>
                  </tr>
                )
              }
              )}
            </tbody>
          </table>
        </>
      )
    } else if (type == 15) {
      return (
        <>
          <ThemeButton size="large" onClick={this.exportCostPriceData}>
            Download
          </ThemeButton>
          <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            <thead>
              <tr>
                <th>Order No.</th>
                <th>Lot No.</th>
                <th>SKU</th>
                <th>Product</th>
                <th>Purchase Cost</th>
                <th>Sales Default Price</th>
                <th>Margin</th>
                <th>Delivery Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {data && data.map((row: any, index: number) => {
                return (
                  <tr key={`row-${index}`}>
                    <td>{getOrderPrefix(this.props.sellerSetting, 'purchase') + row['wholesaleOrderId']}</td>
                    <td>{row['lotId']}</td>
                    <td>{row['sku']}</td>
                    <td>{row['variety']}</td>
                    <td>{formatNumber(row['cost'], 2)}/{row.defaultPurchaseUnitUOM}</td>
                    <td>{formatNumber(row['itemPrice'], 2)}/{row.defaultPurchaseUnitUOM}</td>
                    <td></td>
                    <td>{row['orderDate'] ? moment(row['orderDate']).format('MM/DD/YYYY') : ''}</td>
                    <td>{row['status']}</td>
                  </tr>
                )
              }
              )}
            </tbody>
          </table>
        </>
      )
    } else if (type == 20) {
      return (
        <>
          <ThemeButton size="large" onClick={this.exportCostPriceData}>
            Download
          </ThemeButton>
          <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            <thead>
              <tr>
                <th>Order No.</th>
                <th>Customer Name</th>
                <th>SKU</th>
                <th>Product</th>
                <th>Order Qty</th>
                <th>Picked Qty</th>
                <th>Fulfillment Date</th>
                <th>Sales Rep</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {data && data.map((row: any, index: number) => {
                return (
                  <tr key={`row-${index}`}>
                    <td>{getOrderPrefix(this.props.sellerSetting, 'sales') + row['wholesaleOrderId']}</td>
                    <td>{row['customerName']}</td>
                    <td>{row['sku']}</td>
                    <td>{row['variety']}</td>
                    <td>{`${row.quantity} ${row.quantity <= 1 ? row.UOM : row.UOM + 's'}`}</td>
                    <td>{`${row.picked} ${row.picked <= 1 ? row.UOM : row.UOM + 's'}`}</td>
                    <td>{row['createdDate'] ? moment(row['createdDate']).format('MM/DD/YYYY') : ''}</td>
                    <td>{row['salesRep']}</td>
                    <td>{row['status']}</td>
                  </tr>
                )
              }
              )}
            </tbody>
          </table>
        </>
      )
    } else if (type == 25) {
      return (
        <>
          <ThemeButton size="large" onClick={this.exportAllocationIssues}>
            Download
          </ThemeButton>
          <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            <thead>
              <tr>
                <th>Customer Name</th>
                <th>SKU</th>
                <th>Product</th>
                <th>On Hand Qty</th>
                <th>Commited</th>
                <th>Overcommited</th>
              </tr>
            </thead>
            <tbody>
              {data && data.map((row: any, index: number) => {
                return (
                  <tr key={`row-${index}`}>
                    <td>{row['companyName']}</td>
                    <td>{row['sku']}</td>
                    <td>{row['variety']}</td>
                    <td>{row['onHandQty']} {row.inventoryUOM}*</td>
                    <td>{row['quantity']} {row.inventoryUOM}*</td>
                    <td>{`${-row.diff} ${row.inventoryUOM}*`}</td>
                  </tr>
                )
              }
              )}
            </tbody>
          </table>
        </>
      )
    } else if(type == 30)  {
    return (
      <>
        <ThemeButton size="large" onClick={this.exportUnallocationLots}>
          Download
        </ThemeButton>
        <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
          <thead>
            <tr>
              <th>Product</th>
              <th>SKU</th>
              <th>SO #</th>
              <th>Delivery Date</th>
              <th>Customer Name</th>
              <th>Sales Rep</th>
              <th>Qty Ordered</th>
              <th>Qty Shipped</th>
            </tr>
          </thead>
          <tbody>
            {this.formatUnallocatedLotsDownloadTable()}
          </tbody>
        </table>
      </>
    )
  } else if(type == 35) {
    return (
      <>
        <ThemeButton size="large" onClick={this.exportInventoryReport}>
          Download
        </ThemeButton>
        <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
          <thead>
            <tr>
              <th>Vendor</th>
              <th>SKU</th>
              <th>Delivery Date</th>
              <th>PO #</th>
              <th>Lot #</th>
              <th>Vendor Ref #</th>
              <th>PO Notes</th>
              <th>Days on Floor</th>
              <th>Qty Received</th>
              <th>Qty Sold</th>
              <th>Qty on Hand</th>
              <th>Full Cost</th>
              <th>Inventory Value</th>
              <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {this.formatInventoryExportData()}
          </tbody>
        </table>
      </>
    )
  } else if(type == 40) {
    let mockData = [] as any[]
      if (data) {
        data.forEach((category, i) => {
          if (i > 0) {
            mockData.push({
              Category: 'Category',
              'Customer': '',
              'Order #': '',
              'Sale Date': '',
              'Units': 'Units',
              Revenue: 'Sales',
              Cost: 'Costs',
              'Gross Profit': 'Profit',
              Markup: 'Markup',
              'Gross Margin': 'Gross Margin',
            })
          }
          mockData.push(this._buildSaleProductOrderItem(category))
          if (category.productList && category.productList.length > 0) {
            mockData.push({
              Category: 'Category',
              'Customer': 'Customer',
              'Order #': 'Order #',
              'Sale Date': 'Sale Date',
              'Units': 'Units',
              Revenue: 'Sales',
              Cost: 'Cost',
              'Gross Profit': 'Gross Profit',
              Markup: 'Markup',
              'Gross Margin': 'Gross Margin',
            })
          }
          if (category.productList) {
            category.productList.forEach((product: any) => {
              mockData.push(this._buildSaleProductOrderItem(product))
              product.summary.forEach((summary: any) => {
                mockData.push(this._buildSaleProductOrderItem(summary))
              })
            })
          }
        })
      }

      return (
        <>
          <ThemeButton size="large" onClick={()=>this.exportFile(`Sales By Product Order Report-${moment().format('MM.DD.YYYY')}.xlsx`)}>
            Download
          </ThemeButton>
          <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            <thead>
              <tr>
                <th colSpan={8}>{datesString}</th>
              </tr>
              <tr>
                <th>Category</th>
                <th />
                <th />
                <th align="right" />
                <th align="right">Units</th>
                <th align="right">Sales</th>
                <th align="right">Costs</th>
                <th align="right">Profit</th>
                <th align="right">Markup</th>
                <th align="right">Gross Margin</th>
              </tr>
            </thead>
            <tbody>
              {mockData.map((row: any, index: number) => (
                <tr key={`row-${index}`}>
                  <td>{row['Category']}</td>
                  <td>{row['Customer']}</td>
                  <td>{row['Order #']}</td>
                  <td>{row['Sale Date']}</td>
                  <td>{row['Units']} {row['UOM']}</td>
                  <td>{row['Revenue']}</td>
                  <td>{row['Cost']}</td>
                  <td>{row['Gross Profit']}</td>
                  <td>{row['Markup']}</td>
                  <td>{row['Gross Margin']}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      )
  } else if(type == 45) {
    return (
      <>
        <ThemeButton size="large" onClick={this.exportUnallocationLots}>
          Download
        </ThemeButton>
        <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
          <thead>
            <tr>
              <th>Customer</th>
              <th>SO #</th>
              <th>Order Status</th>
              <th>Sales Rep</th>
              <th>SKU</th>
              <th>Lot</th>
              <th>Lot Status</th>
              <th>Lot Avail to Sell</th>
              <th>Order Qty</th>
              <th>Picked Qty</th>
            </tr>
          </thead>
          <tbody>
            {this.formatLotErrorByOrderDownloadTable()}
          </tbody>
        </table>
      </>
    )
  } else if(type == 50 || type == 55) {
    const flattenData = data.filter(el=>el.wholesaleOrderId).reduce((prev, el)=>{
      if(el.children?.length) {
        return [...prev, el, ...el.children]
      } 
    }, [])
    return (
      <>
        <ThemeButton size="large" onClick={()=>this.exportExceptionReport()}>
          Download
        </ThemeButton>
        <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
          <thead>
            <tr>
              <th>{type == 55 ? 'Fulfillment Date' : 'Delivery Date'}</th>
              <th>Order #</th>
              <th>{type == 55 ? 'Customer' : 'Vendor'}</th>
              <th>Brand</th>
              <th>Origin</th>
              <th>SKU</th>
              <th>Lot #</th>
              <th>Status</th>
              <th>Total Billable Qty</th>
              <th>Order Total</th>
            </tr>
          </thead>
          <tbody>
            {flattenData.map((row: any, index: number) => (
              <tr key={`row-${index}`}>
                <td>{row['wholesaleOrderId'] ? moment(row['deliveryDate']).format('MM/DD/YYYY') : row['variety']}</td>
                <td>{row['wholesaleOrderId']}</td>
                <td>{row['companyName']}</td>
                <td>{row['modifiers']}</td>
                <td>{row['extraOrigin']}</td>
                <td>{row['SKU']}</td>
                <td>{row['lotId']}</td>
                <td>{row['wholesaleOrderStatus']}</td>
                <td>{row['catchWeightQty']}</td>
                <td>{row['itemTotal']}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </>
    )
  }


    let mockData = [] as any[]
    if (data) {
      data.forEach((seller, i) => {
        if (i > 0) {
          mockData.push({
            'Sales Representatives': 'Sales Representatives',
            // customerId: '',
            Units: 'Units',
            Revenue: 'Sales',
            Cost: 'Costs',
            'Gross Profit': 'Profit',
            Markup: 'Markup',
            'Gross Margin': 'Gross Margin',
          })
        }
        mockData.push(this._buildItem(seller))
        if (typeof seller.clientList !== 'undefined') {
          if (seller.clientList.length > 0) {
            mockData.push({
              'Sales Representatives': 'Customer',
              // customerId: 'Customer ID',
              Units: 'Units',
              Revenue: 'Sales',
              Cost: 'Cost',
              'Gross Profit': 'Gross Profit',
              Markup: 'Markup',
              'Gross Margin': 'Gross Margin',
            })
          }
          seller.clientList.forEach((client, __i) => {
            mockData.push(this._buildItem(client))
            client.summary.forEach((summary, ___i) => {
              mockData.push(this._buildItem(summary))
            })
          })
        }
      })
    }

    return (
      <>
        <ThemeButton size="large" onClick={()=>this.exportFile(`Sales By Account Report-${moment().format('MM.DD.YYYY')}.xlsx`)}>
          Download
        </ThemeButton>
        <table id="reports-table" className="uk-report-table table table-striped" style={{ display: 'none' }}>
          <thead>
            <tr>
              <th colSpan={7}>{datesString}</th>
            </tr>
            <tr>
              <th>Sales Representatives</th>
              {/* <th /> */}
              <th align="right">Units</th>
              <th align="right">Sales</th>
              <th align="right">Costs</th>
              <th align="right">Profit</th>
              <th align="right">Markup</th>
              <th align="right">Gross Margin</th>
            </tr>
          </thead>
          <tbody>
            {mockData.map((row: any, index: number) => (
              <tr key={`row-${index}`}>
                <td>{row['Sales Representatives']}</td>
                {/* <td>{row['customerId']}</td> */}
                <td>{row['Units']}</td>
                <td>{row['Revenue']}</td>
                <td>{row['Cost']}</td>
                <td>{row['Gross Profit']}</td>
                <td>{row['Markup']}</td>
                <td>{row['Gross Margin']}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </>
    )
  }
}

export default ExcelExport
