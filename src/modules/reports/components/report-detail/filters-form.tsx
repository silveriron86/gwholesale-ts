import * as React from 'react'
import Form, { FormComponentProps } from 'antd/lib/form'
import FormInstance from 'antd/lib/form'
import { FilterFormWrap } from '../../reports.style'
import { AutoComplete, DatePicker, Select } from 'antd'
import { ThemeButton, ThemeCheckbox, ThemeInput, ThemeSelect } from '~/modules/customers/customers.style'
import { SaleCategory, SaleItem, TempCustomer } from '~/schema'
import { Icon as IconSvg } from '~/components/icon'
import moment from 'moment'

const FormItem = Form.Item
interface FiltersFormProps extends FormComponentProps {
  vertical?: boolean
  sellers: any[]
  defaultFilter: any
  onRun: (data: any) => void
  // onChangeFilter: (data: any) => void
  customers: TempCustomer[]
  vendors: any[]
  items: SaleItem[]
  categories: SaleCategory[]
  title: string
}

class FiltersForm extends React.PureComponent<FiltersFormProps> {
  state: any
  formRef = React.createRef<FormInstance>()

  state = {
    selectedClientId: null,
    accountNames: [],
    vendorNames: [],
    productNames: [],
    productSKUs: [],
    selectedDateType: this.props.defaultFilter ? this.props.defaultFilter.dateType : 0
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.defaultFilter && nextProps.defaultFilter.clientId && !this.state.selectedClientId) {
      this.setState({ selectedClientId: nextProps.defaultFilter.clientId })
    }

    if (!this.props.customers.length && nextProps.customers.length) {
      let accountNames: any[] = []
      nextProps.customers.forEach((row) => {
        if (row.clientCompany && row.clientCompany.companyName) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
      this.setState({ accountNames: accountNames })
    }
    if (JSON.stringify(this.props.defaultFilter) != JSON.stringify(nextProps.defaultFilter)) {
      this.setState({ selectedDateType: nextProps.defaultFilter.dateType })
    }
  }

  handleSubmit = (e: any) => {
    const { form, title, defaultFilter } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let v = { ...values }
        if (defaultFilter && defaultFilter.type == 4) {
          v.type = 4
          if (values.clientId !== '') {
            v.clientId = this.state.selectedClientId
          }
          // if (v.deliveryDate) {
          //   v.from = moment(v.deliveryDate).format('MM/DD/YYYY')
          //   v.to = moment(v.deliveryDate).format('MM/DD/YYYY')
          // }
          delete v.customerNumber
          delete v.deliveryDate
        } else if (title === 'Profitability' || (defaultFilter && defaultFilter.type === 2)) {
          v.type = 2
        } else {
          v.type = 1
          if (values.clientId !== '') {
            v.clientId = this.state.selectedClientId
          }
        }
        if (v.dateRange) {
          v = {
            ...v,
            from: moment(v.dateRange[0]).format('MM/DD/YYYY'),
            to: moment(v.dateRange[1]).format('MM/DD/YYYY'),
          }
          delete v.dateRange
        }
        this.props.onRun(v)
      }
    })
  }

  onSearch = (searchStr: string) => {
    const { customers } = this.props
    // tslint:disable-next-line:prefer-const
    let accountNames: any[] = []
    if (customers && customers.length > 0 && searchStr) {
      customers.forEach((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      accountNames: accountNames,
    })
  }

  onSelect = (clientId: any) => {
    this.setState({ selectedClientId: clientId })
    if (this.props.defaultFilter && this.props.defaultFilter.type == 4) {
      this.props.form.setFieldsValue({ customerNumber: clientId })
    }
  }

  onProductSearch = (searchStr: string, type: string) => {
    const { items } = this.props
    // tslint:disable-next-line:prefer-const
    let options: any[] = []
    if (items && items.length > 0 && searchStr) {
      items.forEach((row) => {
        if (row[type] && row[type].toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          options.push({
            value: row[type],
            text: row[type],
          })
        }
      })
    }

    if (type === 'SKU') {
      this.setState({
        productSKUs: options,
      })
    } else {
      this.setState({
        productNames: options,
      })
    }
  }

  onVendorSearch = (searchStr: string) => {
    const { vendors } = this.props
    // tslint:disable-next-line:prefer-const
    let vendorNames: any[] = []
    if (vendors.length > 0 && searchStr) {
      vendors.filter((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          vendorNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      vendorNames,
    })
  }

  onAccountNumberChange = (evt: any) => {
    const val = evt.target.value
    const { customers } = this.props
    const customer = customers.find(el => { return el.clientId == val })
    const name = customer ? customer.clientCompany.companyName : ''
    this.props.form.setFieldsValue({ clientId: name })
    if (customer) {
      this.setState({ selectedClientId: customer.clientId })
    } else {
      this.setState({ selectedClientId: null })
    }
  }

  getDefaultCustomerName = () => {
    const { customers, defaultFilter } = this.props
    if (customers.length == 0 || !defaultFilter || (defaultFilter && !defaultFilter.clientId)) return ''
    const customer = customers.find((el) => {
      return el.clientId === defaultFilter.clientId
    })
    if (customer) {
      return customer.clientCompany ? customer.clientCompany.companyName : ''
    } else {
      return ''
    }
  }

  onDateChange = (dateType: number) => {
    this.setState({ selectedDateType: dateType })
  }

  render() {
    const { sellers, defaultFilter, vertical, title, categories } = this.props
    const { accountNames, productNames, vendorNames, productSKUs } = this.state
    const { getFieldDecorator } = this.props.form
    const defaultCompanyName = this.getDefaultCustomerName()
    const dateTypeSelect = (
      <ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateChange}>
        <Select.Option value={0}>All</Select.Option>
        <Select.Option value={1}>Month to Date</Select.Option>
        <Select.Option value={2}>Year to Date</Select.Option>
        <Select.Option value={3}>Past 365 Days</Select.Option>
        <Select.Option value={4}>Past 30 days</Select.Option>
        <Select.Option value={5}>Past 4 Weeks</Select.Option>
        <Select.Option value={6}>Past 2 Weeks</Select.Option>
        <Select.Option value={7}>Past 1 Week</Select.Option>
        <Select.Option value={8}>Today</Select.Option>
        <Select.Option value={9}>Custom Date Range</Select.Option>
      </ThemeSelect>
    )
    return (
      <Form ref={this.formRef} onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical" style={{ maxWidth: 'initial' }}>
        <FilterFormWrap style={{ display: vertical === true ? 'block' : 'flex', flexWrap: 'wrap' }}>
          {title === 'Profitability' || (defaultFilter && defaultFilter.type === 2) ? (
            <>
              <FormItem label="Product Categories">
                {getFieldDecorator('wholesaleCategoryId', {
                  rules: [{ required: false }],
                  initialValue: defaultFilter?.wholesaleCategoryId ?? '',
                })(
                  <ThemeSelect style={{ minWidth: 150 }}>
                    <Select.Option value="">All</Select.Option>
                    {categories.map((c: SaleCategory, _index: number) => {
                      return (
                        <Select.Option key={c.wholesaleCategoryId} value={c.wholesaleCategoryId}>
                          {c.name}
                        </Select.Option>
                      )
                    })}
                  </ThemeSelect>,
                )}
              </FormItem>
              <FormItem label="Sale Dates">
                {getFieldDecorator('dateType', {
                  rules: [{ required: false }],
                  initialValue: defaultFilter?.dateType ?? 0,
                })(dateTypeSelect)}
              </FormItem>
              {this.state.selectedDateType === 9 && (
                <FormItem label={vertical ? '' : <>&nbsp;</>}>
                  {getFieldDecorator('dateRange', {
                    rules: [{ required: false }],
                    initialValue: defaultFilter ? [moment(defaultFilter.from), moment(defaultFilter.to)] : [,]
                  })(<DatePicker.RangePicker
                    placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                    format={'MM/DD/YYYY'}
                    className="report-dp" />)}
                </FormItem>
              )}

              <FormItem label="PAS (Price-After-Sale)" className={`pas ${vertical === true ? '' : 'horizontal'}`}>
                {getFieldDecorator('priceAfterSale', {
                  rules: [{ required: false }],
                  initialValue: defaultFilter ? defaultFilter.priceAfterSale : false,
                  valuePropName: 'checked',
                })(<ThemeCheckbox />)}
              </FormItem>
              <FormItem label="Product SKU">
                {getFieldDecorator('sku', {
                  initialValue: defaultFilter ? defaultFilter.sku : '',
                  rules: [{ required: false }],
                })(
                  <AutoComplete
                    placeholder="Product SKU"
                    dataSource={productSKUs}
                    onSearch={(v) => this.onProductSearch(v, 'SKU')}
                  />,
                )}
              </FormItem>
              <FormItem label="Product Name">
                {getFieldDecorator('wholesaleItemName', {
                  rules: [{ required: false }],
                  initialValue: defaultFilter ? defaultFilter.wholesaleItemName : '',
                })(
                  <AutoComplete
                    placeholder="Product Name"
                    dataSource={productNames}
                    onSearch={(v) => this.onProductSearch(v, 'variety')}
                  />,
                )}
              </FormItem>
              <FormItem label="Lot #">
                {getFieldDecorator('lotId', {
                  rules: [{ required: false }],
                  initialValue: defaultFilter ? defaultFilter.lotId : '',
                })(<ThemeInput />)}
              </FormItem>
              <FormItem label="Vendor">
                {getFieldDecorator('clientId', {
                  initialValue: defaultFilter ? defaultFilter.wholesaleClientId : '',
                  rules: [{ required: false }],
                })(<AutoComplete dataSource={vendorNames} onSearch={this.onVendorSearch} />)}
              </FormItem>
            </>
          ) : (
            <>
              {/* {(!defaultFilter || (defaultFilter && defaultFilter.type != 4)) ? */}
              <FormItem label={(!defaultFilter || (defaultFilter && defaultFilter.type != 4)) ? "Report Dates" : "Fulfillment Date"}>
                {getFieldDecorator('dateType', {
                  rules: [{ required: false }],
                  initialValue: defaultFilter?.dateType ?? 0,
                })(dateTypeSelect)}
              </FormItem>
              {/*: <FormItem label="Fulfillment Date">
                  {getFieldDecorator('deliveryDate', {
                    rules: [{ required: false }],
                    initialValue: defaultFilter && defaultFilter.from ? moment(defaultFilter.from) : ''
                  })(
                    <DatePicker
                      placeholder="MM/DD/YYYY"
                      format="MM/DD/YYYY"
                      allowClear={true}
                      suffixIcon={<Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                    />,
                  )}
                </FormItem>
              } */}
              {this.state.selectedDateType === 9 && (
                <FormItem label={vertical ? '' : <>&nbsp;</>}>
                  {getFieldDecorator('dateRange', {
                    rules: [{ required: false }],
                    initialValue: defaultFilter ? [moment(defaultFilter.from), moment(defaultFilter.to)] : [,]
                  })(<DatePicker.RangePicker
                    placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                    format={'MM/DD/YYYY'}
                    className="report-dp" />)}
                </FormItem>
              )}

              <FormItem label="Sales Representatives">
                {getFieldDecorator('sellerId', {
                  initialValue: defaultFilter && defaultFilter.sellerId ? defaultFilter.sellerId : '',
                  rules: [{ required: false }],
                })(
                  <ThemeSelect>
                    <Select.Option value="">All</Select.Option>
                    {sellers.map((seller) => {
                      return (
                        <Select.Option value={seller.userId}>{`${seller.firstName} ${seller.lastName}`}</Select.Option>
                      )
                    })}
                  </ThemeSelect>,
                )}
              </FormItem>

              <FormItem label="Customer Account">
                {getFieldDecorator('clientId', {
                  initialValue: defaultCompanyName,
                  rules: [{ required: false }],
                })(
                  <AutoComplete
                    placeholder="Customer Account"
                    dataSource={accountNames}
                    onSearch={this.onSearch}
                    onSelect={this.onSelect}
                  />,
                  // <ThemeInput placeholder='Account Number' />
                )}
              </FormItem>
              {defaultFilter && defaultFilter.type == 4 &&
                <FormItem label="Customer Number">
                  {getFieldDecorator('customerNumber', {
                    initialValue: defaultFilter && defaultFilter.clientId ? defaultFilter.clientId : '',
                    rules: [{ required: false }],
                  })(<ThemeInput placeholder='' onChange={this.onAccountNumberChange} />
                  )}
                </FormItem>
              }
              {(!defaultFilter || (defaultFilter && defaultFilter.type != 4)) &&
                <FormItem label="Product">
                  {getFieldDecorator('sku', {
                    initialValue: defaultFilter ? defaultFilter.sku : '',
                    rules: [{ required: false }],
                  })(
                    <AutoComplete
                      placeholder="Product SKU"
                      dataSource={productSKUs}
                      onSearch={(v) => this.onProductSearch(v, 'SKU')}
                    />,
                    // <ThemeInput placeholder='Product SKU' />
                  )}
                </FormItem>
              }
              {defaultFilter && defaultFilter.type == 4 &&
                <FormItem label="Sales Order">
                  {getFieldDecorator('wholesaleOrderId', {
                    rules: [{ required: false }],
                    initialValue: defaultFilter && defaultFilter.wholesaleOrderId ? defaultFilter.wholesaleOrderId : ''
                  })(<ThemeInput />)}
                </FormItem>
              }
              {defaultFilter && (defaultFilter.type == 1 || defaultFilter.type == 4) &&
                <FormItem
                  label="Group By Sales Representative"
                  style={{
                    marginTop: vertical === true ? 0 : 30
                  }}
                  className='filter-checkbox'>
                  {getFieldDecorator('isGroupBySalesRep', {
                    valuePropName: 'checked',
                    initialValue: defaultFilter && typeof defaultFilter.isGroupBySalesRep != 'undefined' ? defaultFilter.isGroupBySalesRep : true
                  })(<ThemeCheckbox />)}
                </FormItem>
              }
              {/* <FormItem label="Product Name">
              {getFieldDecorator('productName', {
                rules: [{ required: false }],
              })(<ThemeInput />)}
            </FormItem> */}
            </>
          )}
        </FilterFormWrap>

        <ThemeButton type="primary" htmlType="submit" style={{ marginLeft: vertical ? 0 : 20, height: 42 }}>
          <IconSvg
            viewBox="0 0 24 24"
            width="24"
            height="24"
            type="run"
            style={{ fill: 'transparent', float: 'left' }}
          />
          <div
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              lineHeight: '21px',
              fontFamily: 'Museo Sans Rounded',
              display: 'block',
              float: 'left',
              marginTop: 2,
              marginLeft: 3,
            }}
          >
            Run Report
          </div>
        </ThemeButton>
      </Form>
    )
  }
}

export default Form.create<FiltersFormProps>()(FiltersForm)
