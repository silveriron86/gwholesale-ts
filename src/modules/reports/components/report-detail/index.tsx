import React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { Button, Collapse, Icon, Popover, Tooltip, Table } from 'antd'
import { Link } from 'react-router-dom'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { AuthUser } from '~/schema'
import PageLayout from '~/components/PageLayout'
import { ReportsDispatchProps, ReportsModule, ReportsStateProps } from '../../reports.module'
import {
  ReportsHeader,
  DetailWrapper,
  ExpandedTabelWrapper,
  PanelHeader,
  FlexWrap,
  InsightBlock,
  ReportFilter,
} from '../../reports.style'
import { ThemeButton, ThemeInput, ThemeSpin, ThemeTable } from '~/modules/customers/customers.style'
import { Icon as Icon1 } from '~/components'
import { ExpandIconProps } from 'antd/lib/table'
import { formatNumber, getOrderPrefix } from '~/common/utils'
import FiltersForm from './filters-form'
import ExcelExport from './excel-export'
import ProductReport from './product-report'
import moment from 'moment'
import { CostPriceDifference } from '../../report-contents/cost-price-difference'
import { AllocationIssue } from '../../report-contents/allocation-report'
import { UnallocatedLots } from '../../report-contents/unallocated-lots'
import LotErrorsByOrder from '../../report-contents/lot-error-by-order'
import { Inventory } from '../../report-contents/inventory'
import SalesProductOrderReport from './../../report-contents/sales-product-order/sales-product-order-report'
import { UnshippedSalesOrderReport } from '../../report-contents/unshipped-sales-order'
import { UnreceivedPurchaseOrderReport } from '../../report-contents/unreceived-purchase-order'
const { Panel } = Collapse

export type ReportDetailProps = ReportsDispatchProps &
  ReportsStateProps &
  RouteComponentProps<{ filterId: string }> & {
    theme: Theme
    currentUser: AuthUser
  }

class ReportDetail extends React.PureComponent<ReportDetailProps> {
  multiSortTable = React.createRef<any>()
  formRef = React.createRef<any>()
  state = {
    filterId: '',
    filter: {
      dateType: 0,
      clientId: '',
      sellerId: '',
      sku: '',
      isGroupBySalesRep: false
    },
    activeUserKeys: [],
    reportName: '',
    visiblePopover: false,
    isReload: true,
    isAfterRun: false,
  }
  componentDidMount() {
    this.props.getReportsHistory()
    this.props.getAllSellers()
    this.props.getCustomers()
    this.props.getVendors()
    this.props.getAllItems()
    this.props.getSaleCategories()
    this.props.getSellerSetting()
  }

  componentDidUpdate(prevProps: ReportDetailProps) {
    if (this.state.filterId !== this.props.match.params.filterId) {
      this.setState({ filterId: this.props.match.params.filterId }, () => {
        this._loadData(this.props)
      })
    }
  }

  componentWillReceiveProps(nextProps: ReportDetailProps) {
    if (
      this.state.isReload === true &&
      JSON.stringify(this.props.reportsHistory) !== JSON.stringify(nextProps.reportsHistory)
    ) {
      this._loadData(nextProps)
    }

    if (JSON.stringify(this.props.reportsHistory) !== JSON.stringify(nextProps.reportsHistory)) {
      if (this.state.isAfterRun === true && nextProps.reportsHistory.length > 0) {
        this.setState(
          {
            isAfterRun: false,
          },
          () => {
            window.location.href = `#/reports/${nextProps.reportsHistory[0].id}`
          },
        )
      }
    }
  }

  _loadData = (nextProps: ReportDetailProps) => {
    if (nextProps.reportsHistory && nextProps.reportsHistory.length > 0) {
      const filterId = this.state.filterId
      const found = nextProps.reportsHistory.find((f: any) => f.id == filterId)
      if (found) {
        let filter = {}
        const filterTypes = [2, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]
        if (filterTypes.indexOf(found.type) > -1) {
          filter = found
        } else {
          const { dateType, sellerId, sku, wholesaleClientId, isGroupBySalesRep } = found
          filter = {
            dateType,
            clientId: wholesaleClientId,
            sellerId,
            sku,
            isGroupBySalesRep,
            type: found.type == 4 ? 4 : 1,
          }
        }
        if (found.fromStrDate) {
          filter = { ...filter, from: moment(found.fromStrDate).format('MM/DD/YYYY') }
        }
        if (found.toStrDate) {
          filter = { ...filter, to: moment(found.toStrDate).format('MM/DD/YYYY') }
        }
        if (found.wholesaleOrderId) {
          filter = { ...filter, wholesaleOrderId: found.wholesaleOrderId }
        }
        this.setState(
          {
            filter,
          },
          () => {
            const option = this.getFilterOption()
            if (filter.type === 2) {
              this.props.getCategoryReport(option)
              this.props.getReportsToDownload(this.state.filter)
            } else if (filter.type == 10 || filter.type == 15 || filter.type == 20) {
              this.props.setLoading(true)
              this.props.getCostDifferenceData(filter)
            } else if (filter.type == 25) {
              this.props.setLoading(true)
              this.props.getAllocationIssues(filter)
            } else if(filter.type == 30) {
              this.props.setLoading(true)
              this.props.getUnallocatedLotsReport(filter)
            } else if(filter.type == 35) {
              this.props.setLoading(true)
              this.props.getInventoryReport(option)
            }  else if (filter.type == 40) {
              this.props.setLoading(true)
              this.props.getSalesProductOrderReportData(option)
              this.props.getReportsToDownload(option)
            } else if (filter.type == 45) {
              this.props.setLoading(true)
              this.props.getLotsErrorByOrderReport(filter)
            } else if (filter.type == 50 || filter.type == 55) {
              this.props.setLoading(true)
              this.props.getExceptionOrderReportData(option)
            } else {
              if (!filter.isGroupBySalesRep && (filter.type == 1 || filter.type == 4)) {
                this.props.getTotalReportsWithoutGroup(this.state.filter)
              } else {
                this.props.findReportByUser(this.state.filter)
              }
              this.props.getReportsToDownload(this.state.filter)
            }
          },
        )
      }
    }
  }

  getFilterOption = () => {
    const { filter } = this.state
    const { priceAfterSale } = filter
    let option: any = {
      priceAfterSale,
      type: filter.type
    }
    if (filter.wholesaleCategoryId) {
      option.categoryId = filter.wholesaleCategoryId
    }
    if (filter.dateType != 0) {
      option.dateType = filter.dateType
    }
    if (filter.wholesaleClientId) {
      option.clientId = filter.wholesaleClientId
    }
    if (filter.wholesaleItemName) {
      option.itemName = filter.wholesaleItemName
    }
    if (filter.lotId) {
      option.lotId = filter.lotId
    }
    if (filter.sku) {
      option.sku = filter.sku
    }
    if (filter.from) {
      option.from = filter.from
    }
    if (filter.to) {
      option.to = filter.to
    }
    if (filter.customOrderNumber) {
      option.customOrderNumber = filter.customOrderNumber
    }
    if (filter.search) {
      option.search = filter.search
    }
    if (!!filter.includeLotsWithZero) {
      option.includeLotsWithZero = filter.includeLotsWithZero
    }
    if (!!filter.includeLotsWithNegative) {
      option.includeLotsWithNegative = filter.includeLotsWithNegative
    }
    return option
  }

  useCustomIcon = (props: ExpandIconProps<T>) => {
    const aStyle = {
      color: 'black',
      display: 'flex',
      justifyContent: 'center',
      float: 'left',
      width: 40,
      zIndex: 1,
      position: 'relative',
      marginTop: 3,
    }
    if (props.expanded) {
      return (
        <a
          style={aStyle}
          onClick={(e) => {
            props.onExpand(props.record, e)
          }}
        >
          <Icon type="down" style={{ marginRight: 4 }} />
        </a>
      )
    } else {
      return (
        <a
          style={aStyle}
          onClick={(e) => {
            props.onExpand(props.record, e)
          }}
        >
          <Icon type="up" style={{ marginRight: 4, transform: 'rotate(90deg)' }} />
        </a>
      )
    }
  }

  onRun = (data: any) => {
    this.setState(
      {
        isReload: false,
        filter: data,
        isAfterRun: true,
      },
      () => {
        this.props.saveReportHistory(data)
        const { filter } = this.state
        const option = this.getFilterOption()
        if (filter.type === 2) {
          this.props.getReportsHistory(data)
          this.props.getReportsToDownload(data)
        } else if (filter.type == 10 || filter.type == 15 || filter.type == 20) {
          this.props.setLoading(true)
          this.props.getCostDifferenceData(data)
        } else if (filter.type == 25) {
          this.props.setLoading(true)
          this.props.getAllocationIssues(filter)
        } else if(filter.type == 30) {
          this.props.setLoading(true)
          this.props.getUnallocatedLotsReport(filter)
        } else if (filter.type == 40) {
          this.props.setLoading(true)
          this.props.getSalesProductOrderReportData(option)
          this.props.getReportsToDownload(option)
        } else if (filter.type == 50 || filter.type == 55) {
          this.props.setLoading(true)
          this.props.getExceptionOrderReportData(option)
        } else {
          this.props.findReportByUser(data)
          this.props.getReportsToDownload(data)
        }
      },
    )
  }

  onSaveCustomReport = () => {
    const { filter } = this.state
    const formRefCallableTypes = [10, 15, 30, 35, 45, 50, 55] // call save custom report method by form ref
    if (formRefCallableTypes.indexOf(filter.type)) {
      this.formRef.current.onSaveCustomReport()
    } else if (filter.type == 25 || filter.type == 30 || filter.type == 35 || filter.type == 45) {
      const { reportName } = this.state
      if (!reportName) {
        return
      }
      this.setState(
        {
          visiblePopover: false,
        },
        () => {
          this.props.saveCustomReportWithName({
            reportName,
            filter: {
              type: filter.type
            },
          })
        },
      )
    } else {
      this.formRef.current
        .validateFields()
        .then((values) => {
          const { reportName } = this.state
          if (!reportName) {
            return
          }
          this.setState(
            {
              visiblePopover: false,
            },
            () => {
              this.props.saveCustomReportWithName({
                reportName,
                filter: {
                  ...values,
                  ...{ type: filter.type }
                },
              })
            },
          )
        })
        .catch((errorInfo) => {
          console.log(errorInfo)
        })
    }
  }

  onSaveFilter = () => {
    const { filter } = this.state
    this.props.saveReportFilter(filter)
  }

  onChangeUserCollapse = (keys: number[]) => {
    const { activeUserKeys, filter } = this.state
    if (keys.length > activeUserKeys.length) {
      // expand
      const userId = keys[keys.length - 1]
      localStorage.setItem('reprot_seller_id', userId.toString())
      this.props.findReportByClient({
        ...filter,
        ...{ sellerId: userId },
      })
    } else {
      // collapse
    }
    this.setState({
      activeUserKeys: keys,
    })
  }

  onChangeReportName = (e: any) => {
    this.setState({
      reportName: e.target.value,
    })
  }

  handleVisibleChange = (visiblePopover: boolean) => {
    this.setState({ visiblePopover })
  }

  _getGrossProfit = (record: any) => {
    return record.totalRevenue - record.totalCost
  }

  _getMarkup = (record: any) => {
    return !record.totalCost ? 0 : (this._getGrossProfit(record) / record.totalCost) * 100
  }

  _getGrossMargin = (record: any) => {
    return !record.totalRevenue ? 0 : (this._getGrossProfit(record) / record.totalRevenue) * 100
  }

  render() {
    const { sellers, reports, totalReports, users, customers, items, reportsToDownload, categories, categoryReports, vendors } = this.props
    const { filter, activeUserKeys, reportName, visiblePopover } = this.state

    const totalColumns = [
      {
        title: 'Sales Representative',
        dataIndex: 'firstName',
        align: 'left',
        width: 330,
        render: (value: string, record: any) => {
          return `${record.firstName} ${record.lastName}`
        },
      },
      // {
      //   title: 'Customer ID', // shold be hidden
      //   align: 'left',
      //   width: 150,
      // },
      {
        title: 'Units', // should be default_sales_unit_uom
        dataIndex: 'totalUnits',
        align: 'right',
        width: 150,
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'right',
        width: 150,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Costs',
        dataIndex: 'totalCost',
        align: 'right',
        width: 150,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Profit',
        dataIndex: 'profit',
        align: 'right',
        width: 150,
        render: (value: number, record: any) => {
          const data = this._getGrossProfit(record)
          return `${data < 0 ? '-' : ''}$${formatNumber(Math.abs(data), 2)}`
        }
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'right',
        width: 150,
        render: (_value: number, record: any) => `${formatNumber(this._getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'right',
        width: 150,
        render: (_value: number, record: any) => `${formatNumber(this._getGrossMargin(record), 2)}%`,
      },
    ]

    const columns = [
      {
        title: 'Customer',
        dataIndex: 'companyName',
        align: 'left',
        width: 330,
        sorter: (a: any, b: any) => {
          return a.companyName.localeCompare(b.companyName)
        },
        render: (v: string, record: any) => {
          return <Link to={`/customer/${record.clientId}/orders`}>{v}</Link>
        }
      },
      // {
      //   title: 'Customer ID',
      //   dataIndex: 'clientId',
      //   align: 'left',
      //   width: 150,
      //   sorter: (a: any, b: any) => a.clientId - b.clientId,
      // },
      {
        title: 'Units', // should be default_sales_unit_uom
        dataIndex: 'totalUnits',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.units - b.units,
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalRevenue - b.totalRevenue,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Cost',
        dataIndex: 'totalCost',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalCost - b.totalCost,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Profit',
        dataIndex: 'profit',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this._getGrossProfit(a) - this._getGrossProfit(b),
        render: (value: number, record: any) => {
          const data = this._getGrossProfit(record)
          return `${data < 0 ? '-' : ''}$${formatNumber(Math.abs(data), 2)}`
        }
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this._getMarkup(a) - this._getMarkup(b),
        render: (_value: number, record: any) => `${formatNumber(this._getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this._getGrossMargin(a) - this._getGrossMargin(b),
        render: (_value: number, record: any) => `${formatNumber(this._getGrossMargin(record), 2)}%`,
      },
    ]

    const summaryColumns = [
      {
        title: 'Customer',
        dataIndex: 'itemName',
        align: 'left',
        width: 330,
        sorter: (a: any, b: any) => {
          return a.itemName.localeCompare(b.itemName)
        },
        render: (itemName: string, record: any) => {
          if (filter && filter.type == 4) {
            const prefix = getOrderPrefix(this.props.sellerSetting, 'sales')
            return <Link to={`/sales-order/${itemName}`}>{`${prefix} ${itemName}`}</Link>
          } else {
            return <Link to={`/product/${record.wholesaleItemId}/activity`}>{itemName}</Link>
          }
        }
      },
      // {
      //   title: 'Customer ID',
      //   dataIndex: 'wholesaleItemId',
      //   align: 'left',
      //   width: 150,
      //   sorter: (a: any, b: any) => a.wholesaleItemId - b.wholesaleItemId,
      // },
      {
        title: 'Units', // should be default_sales_unit_uom
        dataIndex: 'totalUnits',
        align: 'right',
        width: 150,
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalRevenue - b.totalRevenue,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Cost',
        dataIndex: 'totalCost',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalCost - b.totalCost,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Gross Profit',
        dataIndex: 'profit',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this._getGrossProfit(a) - this._getGrossProfit(b),
        render: (value: number, record: any) => {
          const data = this._getGrossProfit(record)
          return `${data < 0 ? '-' : ''}$${formatNumber(Math.abs(data), 2)}`
        }
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this._getMarkup(a) - this._getMarkup(b),
        render: (_value: number, record: any) => `${formatNumber(this._getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this._getGrossMargin(a) - this._getGrossMargin(b),
        render: (_value: number, record: any) => `${formatNumber(this._getGrossMargin(record), 2)}%`,
      },
    ]

    if (filter && filter.type == 4) {
      totalColumns.splice(1, 0, {
        title: '',
        key: 'fulfillment-date',
        width: 150,
        redner: () => ' ',
      })
      columns.splice(1, 0, {
        title: 'Fulfillment Date',
        dataIndex: 'deliveryDate',
        align: 'left',
        width: 150,
        render: (v: string) => moment(v).format('MM/DD/YYYY')
      })
      summaryColumns.splice(1, 0, {
        title: 'Fulfillment Date',
        dataIndex: 'deliveryDate',
        align: 'left',
        width: 150,
        render: (v: string) => moment(v).format('MM/DD/YYYY')
      })
    }

    let SumOfRevenue = 0
    let SumOfCost = 0
    let SumOfProfit = 0

    const data = (!filter.isGroupBySalesRep && (filter.type == 1 || filter.type == 4)) ? totalReports : filter.type === 2 ? categoryReports : users
    data.forEach((item: any, index: number) => {
      SumOfRevenue += item.totalRevenue
      SumOfCost += item.totalCost
      SumOfProfit += this._getGrossProfit(item)
    })


    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Reports'}>
        <ReportsHeader>
          <a className="back-reports" href="#/reports">
            <Icon1 viewBox="-3 -3 16 16" width="24" height="24" type="arrow-left" />
          </a>
          <h1 style={{marginBottom: 0}}>
            {
              filter.type == 1
                ? 'Sales by Account/Product'
                : filter.type == 4
                  ? 'Sales by Account/Order'
                  : filter.type == 2
                    ? 'Sales by Product' : ''}</h1>
          <div>
            <Popover
              trigger="click"
              visible={visiblePopover}
              content={
                <div>
                  <ThemeInput placeholder="Report Name" value={reportName} onChange={this.onChangeReportName} />
                  <div style={{ marginTop: 5, display: 'flex', justifyContent: 'flex-end' }}>
                    <ThemeButton onClick={this.onSaveCustomReport}>Save</ThemeButton>
                  </div>
                </div>
              }
              title={null}
              onVisibleChange={this.handleVisibleChange}
            >
              <Button shape="round" type="link" size="large">
                <Icon type="save" theme="filled" />
                Save Custom Reports
              </Button>
            </Popover>
            {/* <ThemeButton size="large">Download</ThemeButton> */}
            <ExcelExport theme={this.props.theme} filter={filter} data={reportsToDownload} sellerSetting={this.props.sellerSetting} />
          </div>
        </ReportsHeader>
        {typeof filter.type == 'undefined' ? (
          <>
            <ThemeSpin style={{ marginTop: 200 }} spinning={true} size={"small"} />
          </>
        ) : filter.type == 10 ? ( // report detail for sales order price under cost
          <>
            <CostPriceDifference
              isVertial={false}
              title={'Sales Price Under Cost'}
              filter={filter}
              reportType='sales'
              dataSource={this.props.costDifferenceData}
              sellerSetting={this.props.sellerSetting}
              onRun={this.onRun}
              loading={this.props.costDataLoading}
              saveCustomReportWithName={this.props.saveCustomReportWithName}
              reportName={reportName}
              handleReportNameVisibleChange={this.handleVisibleChange}
              ref={this.formRef}
            />
          </>
        ) : filter.type == 15 ? ( // report detail for purchase order cost under price
          <>
            <CostPriceDifference
              isVertial={false}
              title={'Purchase Cost Above Price'}
              filter={filter}
              reportType='purchase'
              dataSource={this.props.costDifferenceData}
              sellerSetting={this.props.sellerSetting}
              onRun={this.onRun}
              loading={this.props.costDataLoading}
              saveCustomReportWithName={this.props.saveCustomReportWithName}
              reportName={reportName}
              handleReportNameVisibleChange={this.handleVisibleChange}
              ref={this.formRef}
            />
          </>
        ) : filter.type == 20 ? ( // report detail for backorder report
          <>
            <CostPriceDifference
              isVertial={false}
              title={'Backorder'}
              filter={filter}
              reportType='backorder'
              dataSource={this.props.costDifferenceData}
              sellerSetting={this.props.sellerSetting}
              onRun={this.onRun}
              loading={this.props.costDataLoading}
              saveCustomReportWithName={this.props.saveCustomReportWithName}
              reportName={reportName}
              handleReportNameVisibleChange={this.handleVisibleChange}
              ref={this.formRef}
            />
          </>
        ) : filter.type == 25 ? ( // report detail for backorder report
          <>
            <AllocationIssue
              filter={filter}
              dataSource={this.props.allocationIssues}
              sellerSetting={this.props.sellerSetting}
              onRun={this.onRun}
              loading={this.props.costDataLoading}
              saveCustomReportWithName={this.props.saveCustomReportWithName}
              reportName={reportName}
              handleReportNameVisibleChange={this.handleVisibleChange}
              ref={this.formRef}
            />
          </>
        ) : filter.type == 30 ? ( // report detail for backorder report
          <>
            <UnallocatedLots
              filter={filter}
              isVertical={false}
              sellers={sellers}
              dataSource={this.props.unallocatedLots}
              sellerSetting={this.props.sellerSetting}
              onRun={this.onRun}
              loading={this.props.costDataLoading}
              saveCustomReportWithName={this.props.saveCustomReportWithName}
              reportName={reportName}
              getAllocateLots={this.props.getAllocateLotsData}
              allocateLotsData={this.props.allocateLotsData}
              handleReportNameVisibleChange={this.handleVisibleChange}
              allocateLots={this.props.allocateLots}
              setLoading={this.props.setLoading}
              ref={this.formRef}
            />
          </>
        ) : filter.type == 35 ? (
          <>
            <Inventory
              filter={filter}
              vertical={false}
              vendors={this.props.vendors}
              items={this.props.items}
              dataSource={this.props.inventoryReportItems}
              sellerSetting={this.props.sellerSetting}
              onRun={this.onRun}
              loading={this.props.costDataLoading}
              saveCustomReportWithName={this.props.saveCustomReportWithName}
              reportName={reportName}
              handleReportNameVisibleChange={this.handleVisibleChange}
              ref={this.formRef}
            />
          </>
        ) : filter.type == 40 ? (
          <SalesProductOrderReport
            {...this.props}
            ref={this.formRef}
            filter={filter}
            onRun={this.onRun}
            reportName={reportName}
            useCustomIcon={this.useCustomIcon}
            getGrossProfit={this._getGrossProfit}
            getMarkup={this._getMarkup}
            getGrossMargin={this._getGrossMargin}
            getFilterOption={this.getFilterOption}
            handleReportNameVisibleChange={this.handleVisibleChange}
          />
        ) : filter.type == 45 ? (
          <LotErrorsByOrder
            filter={filter}
            isVertical={false}
            sellers={sellers}
            sellerSetting={this.props.sellerSetting}
            onRun={this.onRun}
          />
        ) : filter.type == 55 ? (
          <UnshippedSalesOrderReport
            {...this.props}
            ref={this.formRef}
            filter={filter}
            onRun={this.onRun}
            reportName={reportName}
            useCustomIcon={this.useCustomIcon}
            saveCustomReportWithName={this.props.saveCustomReportWithName}
            handleReportNameVisibleChange={this.handleVisibleChange}
          />
        ) : filter.type == 50 ? (
          <UnreceivedPurchaseOrderReport
            {...this.props}
            ref={this.formRef}
            filter={filter}
            onRun={this.onRun}
            reportName={reportName}
            useCustomIcon={this.useCustomIcon}
            saveCustomReportWithName={this.props.saveCustomReportWithName}
            handleReportNameVisibleChange={this.handleVisibleChange}
          />

        ) : (
          <>
            <DetailWrapper className="white">
              <Collapse defaultActiveKey={['1']}>
                <Panel header="Hide Filters" key="1">
                  <ReportFilter>
                    <FiltersForm
                      // ref="filterForm"
                      ref={this.formRef}
                      onRun={this.onRun}
                      // onChangeFilter={this.onChangeFilter}
                      defaultFilter={filter}
                      sellers={sellers}
                      customers={customers}
                      vendors={vendors}
                      categories={categories}
                      items={items}
                    />
                  </ReportFilter>
                </Panel>
              </Collapse>
            </DetailWrapper>
            <DetailWrapper className="insight">
              <Collapse defaultActiveKey={['2']}>
                <Panel
                  header={
                    <FlexWrap className="between">
                      <div>Insights(5)</div>
                    </FlexWrap>
                  }
                  key="2"
                >
                  <FlexWrap>
                    <InsightBlock style={{ paddingBottom: 16 }}>
                      <div className="label">Total Sales</div>
                      <div className="value">${formatNumber(SumOfRevenue, 2)}</div>
                      {/* <div className="diff green">5.1%</div> */}
                    </InsightBlock>
                    <InsightBlock style={{ paddingBottom: 16 }}>
                      <div className="label">Total Costs</div>
                      <div className="value">${formatNumber(SumOfCost, 2)}</div>
                      {/* <div className="diff green">5.1%</div> */}
                    </InsightBlock>
                    <InsightBlock style={{ paddingBottom: 16 }}>
                      <div className="label">Total Profit</div>
                      <div className="value">${formatNumber(SumOfProfit, 2)}</div>
                      {/* <div className="diff green">5.1%</div> */}
                    </InsightBlock>
                    <InsightBlock style={{ paddingBottom: 16 }}>
                      <div className="label">
                        Avg. Markup
                        <Tooltip title="Markup is calculated as the difference between sales and costs, as a percentage of costs">
                          <Icon type="info-circle" className="info-icon" style={{ margin: '0 4px' }} />
                        </Tooltip>
                      </div>
                      {/* "Avg. Markup" should be "Total Profit" divided by "Total Costs" */}
                      <div className="value">{formatNumber(SumOfProfit / SumOfCost * 100, 2)}%</div>
                    </InsightBlock>
                    <InsightBlock style={{ paddingBottom: 16 }}>
                      <div className="label">
                        Avg. Gross Margin
                        <Tooltip title="Gross margin is calculated as profit divided by sales multipled by 100%">
                          <Icon type="info-circle" className="info-icon" style={{ margin: '0 4px' }} />
                        </Tooltip>
                      </div>
                      {/* "Avg. Gross Margin" should be "Total Profit" divided by "Total Sales" */}
                      <div className="value">{formatNumber(SumOfProfit / SumOfRevenue * 100, 2)}%</div>
                    </InsightBlock>
                  </FlexWrap>
                </Panel>
              </Collapse>
            </DetailWrapper>
            {filter.type === 2 ? (
              <ProductReport
                {...this.props}
                filter={filter}
                useCustomIcon={this.useCustomIcon}
                getGrossProfit={this._getGrossProfit}
                getMarkup={this._getMarkup}
                getGrossMargin={this._getGrossMargin}
                getFilterOption={this.getFilterOption}
              />
            ) : (
              <DetailWrapper className="last">
                <h3>
                  {
                    filter.isGroupBySalesRep === true
                    ? `${users.length} Sales Representative${users.length > 1 ? 's' : ''}`
                    : `${totalReports.length} Customer Account${totalReports.length > 1 ? 's' : ''}`}
                </h3>
                {
                  filter.isGroupBySalesRep === true ? (
                    <Collapse activeKey={activeUserKeys} onChange={this.onChangeUserCollapse}>
                      {users.map((item: any, index: number) => {
                        return (
                          <Panel
                            header={
                              <PanelHeader>
                                <ThemeTable pagination={false} columns={totalColumns} dataSource={item} rowKey="userId" />
                              </PanelHeader>
                            }
                            key={item.userId}
                          >
                            <Table
                              className="reports-table"
                              ref={this.multiSortTable}
                              style={{ paddingLeft: 0, paddingRight: 0 }}
                              pagination={false}
                              columns={columns}
                              dataSource={reports[item.userId]}
                              rowKey="clientId"
                              expandedRowRender={(record) => {
                                // let newCols = JSON.parse(JSON.stringify(columns))
                                // newCols.map((col: any, i: number) => {
                                //   delete col.sorter
                                //   return col
                                // })
                                return (
                                  <ExpandedTabelWrapper>
                                    <ThemeTable
                                      pagination={false}
                                      columns={summaryColumns}
                                      dataSource={record.summary}
                                      rowKey="itemId"
                                    />
                                  </ExpandedTabelWrapper>
                                )
                              }}
                              expandIconAsCell={false}
                              expandIcon={this.useCustomIcon}
                            // onExpand={this.onExpand}
                            />
                          </Panel>
                        )
                      })}
                    </Collapse>) : (
                    <Table
                      className="reports-table"
                      ref={this.multiSortTable}
                      style={{ paddingLeft: 0, paddingRight: 0 }}
                      pagination={false}
                      columns={columns}
                      dataSource={totalReports}
                      rowKey="totalReportIndex"
                      expandedRowRender={(record) => {
                        // let newCols = JSON.parse(JSON.stringify(columns))
                        // newCols.map((col: any, i: number) => {
                        //   delete col.sorter
                        //   return col
                        // })
                        return (
                          <ExpandedTabelWrapper>
                            <ThemeTable
                              pagination={false}
                              columns={summaryColumns}
                              dataSource={record.summary}
                              rowKey="wholesaleItemId"
                            />
                          </ExpandedTabelWrapper>
                        )
                      }}
                      expandIconAsCell={false}
                      expandIcon={this.useCustomIcon}
                    // onExpand={this.onExpand}
                    />
                  )}

              </DetailWrapper>
            )}
          </>
        )}
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.reports, currentUser: state.currentUser }
}

export default withTheme(connect(ReportsModule)(mapStateToProps)(ReportDetail))
