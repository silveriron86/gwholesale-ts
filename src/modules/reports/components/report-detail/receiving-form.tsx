import * as React from 'react'
import Form, { FormComponentProps } from 'antd/lib/form'
import FormInstance from 'antd/lib/form'
import { AutoComplete, DatePicker, Select } from 'antd'
import { ThemeButton, ThemeInput, ThemeSelect } from '~/modules/customers/customers.style'
import { SaleCategory, SaleItem, TempCustomer } from '~/schema'
import { Icon as IconSvg } from '~/components/icon'
import moment from 'moment'

const FormItem = Form.Item
interface ReceivingFormProps extends FormComponentProps {
  vertical?: boolean
  sellers: any[]
  defaultFilter: any
  onRun: (data: any) => void
  // onChangeFilter: (data: any) => void
  customers: TempCustomer[]
  vendors: any[]
  items: SaleItem[]
  categories: SaleCategory[]
  title: string
}

class ReceivingForm extends React.PureComponent<ReceivingFormProps> {
  state: any
  formRef = React.createRef<FormInstance>()

  state = {
    selectedClientId: null,
    accountNames: [],
    vendorNames: [],
    productNames: [],
    productSKUs: [],
  }

  componentWillReceiveProps(nextProps: any) {
    if (!this.props.customers.length && nextProps.customers.length) {
      let accountNames: any[] = []
      nextProps.customers.forEach((row) => {
        if (row.clientCompany && row.clientCompany.companyName) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
      this.setState({ accountNames: accountNames })
    }
    if (JSON.stringify(this.props.defaultFilter) != JSON.stringify(nextProps.defaultFilter)) {
      this.setState({ selectedDateType: nextProps.defaultFilter.dateType })
    }
  }

  handleSubmit = (e: any) => {
    const { form, defaultFilter } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let v = { ...values }
        if (defaultFilter && defaultFilter.type === 2) {
          v.type = 2
        } else {
          v.type = 1
          if (values.clientId !== '') {
            v.clientId = this.state.selectedClientId
          }
        }
        if (v.dateRange) {
          v = {
            ...v,
            from: moment(v.dateRange[0]).format('MM/DD/YYYY'),
            to: moment(v.dateRange[1]).format('MM/DD/YYYY'),
          }
          delete v.dateRange
        }
        this.props.onRun(v)
        // form.resetFields()
      }
    })
  }

  onSearch = (searchStr: string) => {
    const { customers } = this.props
    // tslint:disable-next-line:prefer-const
    let accountNames: any[] = []
    if (customers && customers.length > 0 && searchStr) {
      customers.forEach((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      accountNames: accountNames,
    })
  }

  onSelect = (clientId: any) => {
    this.setState({ selectedClientId: clientId })
  }

  onProductSearch = (searchStr: string, type: string) => {
    const { items } = this.props
    // tslint:disable-next-line:prefer-const
    let options: any[] = []
    if (items && items.length > 0 && searchStr) {
      items.forEach((row) => {
        if (row[type] && row[type].toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          options.push({
            value: row[type],
            text: row[type],
          })
        }
      })
    }

    if (type === 'SKU') {
      this.setState({
        productSKUs: options,
      })
    } else {
      this.setState({
        productNames: options,
      })
    }
  }

  onVendorSearch = (searchStr: string) => {
    const { vendors } = this.props
    // tslint:disable-next-line:prefer-const
    let vendorNames: any[] = []
    if (vendors.length > 0 && searchStr) {
      vendors.filter((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          vendorNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      vendorNames,
    })
  }

  getDefaultCustomerName = () => {
    const { customers, defaultFilter } = this.props
    if (customers.length == 0 || !defaultFilter || (defaultFilter && !defaultFilter.clientId)) return ''
    const customer = customers.find((el) => {
      return el.clientId === defaultFilter.clientId
    })
    if (customer) {
      return customer.clientCompany ? customer.clientCompany.companyName : ''
    } else {
      return ''
    }
  }

  onDateChange = (dateType: number) => {
    this.setState({ selectedDateType: dateType })
  }

  render() {
    const { sellers, vertical, categories } = this.props
    const { productNames, vendorNames, productSKUs } = this.state
    const { getFieldDecorator } = this.props.form

    const dateTypeSelect = (
      <ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateChange}>
        <Select.Option value={0}>All</Select.Option>
        <Select.Option value={1}>Month to Date</Select.Option>
        <Select.Option value={2}>Year to Date</Select.Option>
        <Select.Option value={3}>Past 365 Days</Select.Option>
        <Select.Option value={4}>Past 30 days</Select.Option>
        <Select.Option value={5}>Past 4 Weeks</Select.Option>
        <Select.Option value={6}>Past 2 Weeks</Select.Option>
        <Select.Option value={7}>Past 1 Week</Select.Option>
        <Select.Option value={8}>Today</Select.Option>
        <Select.Option value={9}>Custom Date Range</Select.Option>
      </ThemeSelect>
    )

    return (
      <Form
        ref={this.formRef}
        onSubmit={this.handleSubmit}
        hideRequiredMark={true}
        layout="vertical"
        style={{ maxWidth: 'initial' }}
      >
        <div style={{ display: vertical === true ? 'block' : 'flex', flexWrap: 'wrap' }}>
          <FormItem label="Receive Date">
            {getFieldDecorator('dateType', {
              rules: [{ required: false }],
              initialValue: 0,
            })(dateTypeSelect)}
          </FormItem>
          {this.state.selectedDateType === 9 && (
            <FormItem label="">
              {getFieldDecorator('receiveDate', {
                rules: [{ required: false }],
              })(
                <DatePicker.RangePicker
                  placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                  format={'MM/DD/YYYY'}
                  style={{ width: 260 }}
                />,
              )}
            </FormItem>
          )}

          <FormItem label="Purchase Order">
            {getFieldDecorator('po', {
              rules: [{ required: false }],
            })(<ThemeInput style={{ width: 160 }} />)}
          </FormItem>
          <FormItem label="Vendor Name">
            {getFieldDecorator('clientId', {
              rules: [{ required: false }],
            })(<AutoComplete dataSource={vendorNames} onSearch={this.onVendorSearch} />)}
          </FormItem>
          <FormItem label="Vendor Reference No.">
            {getFieldDecorator('clientId', {
              rules: [{ required: false }],
            })(
              <ThemeSelect>
                <Select.Option value="">All</Select.Option>
              </ThemeSelect>,
            )}
          </FormItem>
          <FormItem label="Product Name">
            {getFieldDecorator('wholesaleItemName', {
              rules: [{ required: false }],
            })(<AutoComplete dataSource={productNames} onSearch={(v) => this.onProductSearch(v, 'variety')} />)}
          </FormItem>
          <FormItem label="Product Category">
            {getFieldDecorator('wholesaleCategoryId', {
              rules: [{ required: false }],
              initialValue: '',
            })(
              <ThemeSelect style={{ minWidth: 150 }}>
                <Select.Option value="">All</Select.Option>
                {categories.map((c: SaleCategory, _index: number) => {
                  return (
                    <Select.Option key={c.wholesaleCategoryId} value={c.wholesaleCategoryId}>
                      {c.name}
                    </Select.Option>
                  )
                })}
              </ThemeSelect>,
            )}
          </FormItem>
          <FormItem label="SKU">
            {getFieldDecorator('sku', {
              rules: [{ required: false }],
            })(
              <AutoComplete
                placeholder="Product SKU"
                dataSource={productSKUs}
                onSearch={(v) => this.onProductSearch(v, 'SKU')}
              />,
            )}
          </FormItem>
          <FormItem label="Lot #">
            {getFieldDecorator('lotId', {
              rules: [{ required: false }],
            })(<ThemeInput />)}
          </FormItem>
          {this.state.selectedDateType === 9 && (
            <FormItem label={vertical ? '' : <>&nbsp;</>}>
              {getFieldDecorator('dateRange', {
                rules: [{ required: false }],
              })(
                <DatePicker.RangePicker
                  placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                  format={'MM/DD/YYYY'}
                  className="report-dp"
                />,
              )}
            </FormItem>
          )}

          <FormItem label="Carrier">
            {getFieldDecorator('carrier', {
              rules: [{ required: false }],
              initialValue: '',
            })(
              <ThemeSelect>
                <Select.Option value="">All</Select.Option>
                {sellers.map((seller) => {
                  return <Select.Option value={seller.userId}>{`${seller.firstName} ${seller.lastName}`}</Select.Option>
                })}
              </ThemeSelect>,
            )}
          </FormItem>

          <ThemeButton type="primary" htmlType="submit" style={{ marginLeft: vertical ? 0 : 20, height: 42 }}>
            <IconSvg
              viewBox="0 0 24 24"
              width="24"
              height="24"
              type="run"
              style={{ fill: 'transparent', float: 'left' }}
            />
            <div
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                lineHeight: '21px',
                fontFamily: 'Museo Sans Rounded',
                display: 'block',
                float: 'left',
                marginTop: 2,
                marginLeft: 3,
              }}
            >
              Run Report
            </div>
          </ThemeButton>
        </div>
      </Form>
    )
  }
}

export default Form.create<ReceivingFormProps>()(ReceivingForm)
