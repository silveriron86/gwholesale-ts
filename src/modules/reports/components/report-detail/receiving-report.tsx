import * as React from 'react'
import { Collapse } from 'antd'
import { DetailWrapper, ExpandedTabelWrapper, PanelHeader } from '../../reports.style'
import { ThemeTable } from '~/modules/customers/customers.style'
import { ReportsDispatchProps, ReportsStateProps } from '../../reports.module'
import { formatDate, formatNumber } from '~/common/utils'
import { Link } from 'react-router-dom'
const { Panel } = Collapse

export type ReceivingFoodReportProps = ReportsDispatchProps & ReportsStateProps

class ReceivingFoodReport extends React.PureComponent<ReceivingFoodReportProps> {
  multiSortTable = React.createRef<any>()
  state = {
    activeUserKeys: [],
  }

  onChangeUserCollapse = (keys: number[]) => {
    const { filter } = this.props
    const { activeUserKeys } = this.state
    if (keys.length > activeUserKeys.length) {
      // expand
      const categoryId = keys[keys.length - 1]
      localStorage.setItem('reprot_category_id', categoryId.toString())
      let opt = this.props.getFilterOption(filter)
      opt.categoryId = categoryId
      this.props.getReceivingFoodReport(opt)
    } else {
      // collapse
    }
    this.setState({
      activeUserKeys: keys,
    })
  }

  render() {
    const { activeUserKeys } = this.state
    const { categoryReports, productReports } = this.props

    const totalColumns = [
      {
        title: 'Category',
        dataIndex: 'categoryName',
        align: 'left',
        width: 330,
      },
      // {
      //   title: 'Customer ID', // shold be hidden
      //   align: 'left',
      //   width: 150,
      // },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'left',
        width: 150,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Costs',
        dataIndex: 'totalCost',
        align: 'left',
        width: 150,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Profit',
        dataIndex: 'profit',
        align: 'left',
        width: 150,
        render: (value: number, record: any) => `$${formatNumber(this.props.getGrossProfit(record), 2)}`,
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'left',
        width: 150,
        render: (_value: number, record: any) => `${formatNumber(this.props.getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'left',
        width: 150,
        render: (_value: number, record: any) => `${formatNumber(this.props.getGrossMargin(record), 2)}%`,
      },
    ]

    const columns = [
      {
        title: 'Category',
        dataIndex: 'variety',
        align: 'left',
        width: 300,
        sorter: (a: any, b: any) => {
          return a.variety.localeCompare(b.variety)
        },
      },
      {
        title: 'Lot #',
        dataIndex: 'summary',
        key: 'Lot#',
        align: 'left',
        width: 75,
        render: (summary: any[]) => {
          let ret = ''
          if (summary && summary.length > 0) {
            ret = summary[0].lotId ? summary[0].lotId : ''
            if (summary.length > 1) {
              ret += '...'
            }
          }
          return ret
        },
      },
      {
        title: 'Sale Date',
        dataIndex: 'summary',
        key: 'Receive Date',
        align: 'left',
        width: 105,
        render: (summary: any[]) => {
          let ret = ''
          if (summary && summary.length > 0) {
            ret = formatDate(summary[0].receiveDate)
            if (summary.length > 1) {
              ret += '...'
            }
          }
          return ret
        },
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => a.totalRevenue - b.totalRevenue,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Cost',
        dataIndex: 'totalCost',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => a.totalCost - b.totalCost,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Profit',
        dataIndex: 'profit',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossProfit(a) - this.props.getGrossProfit(b),
        render: (_value: number, record: any) => `$${formatNumber(this.props.getGrossProfit(record), 2)}`,
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => this.props.getMarkup(a) - this.props.getMarkup(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossMargin(a) - this.props.getGrossMargin(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getGrossMargin(record), 2)}%`,
      },
    ]

    const summaryColumns = [
      {
        title: 'Customer',
        dataIndex: 'variety',
        align: 'left',
        width: 300,
        sorter: (a: any, b: any) => {
          return a.variety.localeCompare(b.variety)
        },
      },
      {
        title: 'Lot #',
        dataIndex: 'lotId',
        align: 'left',
        width: 75,
        sorter: (a: any, b: any) => a.wholesaleItemId - b.wholesaleItemId,
        render: (lotId: string, record: any) => {
          if(!lotId) {
            return null
          }

          const poId = record.wholesaleOrderId//lotId.split('-')[0] - change this line according to new lotId format
          return <Link to={`/order/${poId}/purchase-cart?from=report`}>{lotId}</Link>
        }
      },
      {
        title: 'Sale Date',
        dataIndex: 'receiveDate',
        align: 'left',
        width: 105,
        sorter: (a: any, b: any) => a.receiveDate - b.receiveDate,
        render: (receiveDate: number) => {
          return formatDate(receiveDate)
        },
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => a.totalRevenue - b.totalRevenue,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Cost',
        dataIndex: 'totalCost',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => a.totalCost - b.totalCost,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Gross Profit',
        dataIndex: 'profit',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossProfit(a) - this.props.getGrossProfit(b),
        render: (_value: number, record: any) => `$${formatNumber(this.props.getGrossProfit(record), 2)}`,
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => this.props.getMarkup(a) - this.props.getMarkup(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossMargin(a) - this.props.getGrossMargin(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getGrossMargin(record), 2)}%`,
      },
    ]

    return (
      <DetailWrapper className="last">
        <h3>{categoryReports.length} Categories</h3>
        <Collapse activeKey={activeUserKeys} onChange={this.onChangeUserCollapse}>
          {categoryReports.map((item: any, index: number) => {
            return (
              <Panel
                header={
                  <PanelHeader>
                    <ThemeTable pagination={false} columns={totalColumns} dataSource={item} rowKey="categoryId" />
                  </PanelHeader>
                }
                key={item.categoryId}
              >
                <ThemeTable
                  className="reports-table"
                  ref={this.multiSortTable}
                  style={{ paddingLeft: 0, paddingRight: 0 }}
                  pagination={false}
                  columns={columns}
                  dataSource={productReports[item.categoryId]}
                  rowKey="clientId"
                  expandedRowRender={(record) => {
                    return (
                      <ExpandedTabelWrapper>
                        <ThemeTable
                          pagination={false}
                          columns={summaryColumns}
                          dataSource={record.summary}
                          rowKey="wholesaleItemId"
                        />
                      </ExpandedTabelWrapper>
                    )
                  }}
                  expandIconAsCell={false}
                  expandIcon={this.props.useCustomIcon}
                  // onExpand={this.onExpand}
                />
              </Panel>
            )
          })}
        </Collapse>
      </DetailWrapper>
    )
  }
}

export default ReceivingFoodReport
