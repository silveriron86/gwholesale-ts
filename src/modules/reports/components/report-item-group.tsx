import { notification } from 'antd'
import React from 'react'
import PageLayout from '~/components/PageLayout'
import { AuthUser, UserRole } from '~/schema'
import { ItemTitle, ReportItemGroupWrapper, ReportMenuItem } from '../reports.style'

export type ReportItemGroupProps = {
  title: string
  items: any[]
  initialShowItems?: number
  search: string
  selectedMenuItem: string
  onMenuItemChanged: Function
  currentUser?: AuthUser
}

export class ReportItemGroup extends React.PureComponent<ReportItemGroupProps> {
  state = {
    initialShowItems: this.props.initialShowItems ? this.props.initialShowItems : 3,
    expand: true,
  }

  comingSoon = () => () => {
    notification.open({
      message: 'Coming Soon',
      description: 'This report module is coming soon',
      onClick: () => { },
    })
  }

  renderItems = () => {
    let result: any[] = []
    const { items, search, selectedMenuItem } = this.props

    const { initialShowItems, expand } = this.state
    if (items.length == 0) return result

    const hideStyle = {
      display: 'none',
    }

    for (let index = 0; index < items.length; index++) {
      if (typeof items[index] === 'string') {
        if (!search || items[index].toLowerCase().indexOf(search.toLowerCase()) >= 0) {
          result.push(
            <ReportMenuItem
              className={items[index] === selectedMenuItem ? 'selected' : ''}
              onClick={this.onClickMenuItem.bind(this, items[index])}
              key={index}
              style={index >= initialShowItems && !expand ? hideStyle : {}}
            >
              {items[index]}
            </ReportMenuItem>,
          )
        }
      } else {
        if (!search || items[index].name.toLowerCase().indexOf(search.toLowerCase()) >= 0) {
          result.push(
            <ReportMenuItem
              className={items[index] === selectedMenuItem ? 'selected' : ''}
              onClick={this.onClickMenuItem.bind(this, items[index])}
              key={index}
              style={index >= initialShowItems && !expand ? hideStyle : {}}
            >
              {items[index].name}
            </ReportMenuItem>,
          )
        }
      }
    }
    return result
  }

  onClickMenuItem = (title: string) => {
    this.props.onMenuItemChanged(title)
    if (title != 'Margins') {
      this.comingSoon()
    }
  }

  onClickViewAll = () => {
    this.setState({ expand: !this.state.expand })
  }

  renderViewAllButton = () => {
    const { expand } = this.state
    return (
      <ReportMenuItem onClick={this.onClickViewAll} style={{ fontWeight: 'bold' }}>
        {!expand ? '+ View All' : '- Collapse All'}
      </ReportMenuItem>
    )
  }

  render() {
    const { title, items, search, currentUser } = this.props
    const { initialShowItems } = this.state
    return (
      <ReportItemGroupWrapper>
        <ItemTitle style={{ margin: [UserRole.SALES, UserRole.SELLER_RESTRICTED].indexOf(currentUser?.accountType || UserRole.SUPERADMIN) != -1 ? '20px 0' : '' }}>{title}</ItemTitle>
        {(title === 'custom reports' && items.length === 0) ? <p style={{ margin: '3px 0 1px 60px' }}>None created yet</p> : this.renderItems()}
        {/* {search === '' && items.length > initialShowItems ? this.renderViewAllButton() : ''} */}
      </ReportItemGroupWrapper>
    )
  }
}
