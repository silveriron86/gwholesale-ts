import { Col, Row, Icon, Select, DatePicker, Collapse } from 'antd'
import moment from 'moment'
import React from 'react'
import { formatNumber, getFromAndToByDateType, mathRoundFun } from '~/common/utils'
import { Icon as IconSVG } from '~/components'
import { Flex, ThemeButton, ThemeSelect, ThemeTable } from '~/modules/customers/customers.style'
import { history } from '~/store/history'
import * as _ from 'lodash'
import {
  ReportContent,
  ReportFilter,
  ReportTable,
  ReportTitle,
  SalesReportBody,
} from '../reports.style'
import { Link } from 'react-router-dom'
import { blue } from '~/common'

type AllocationIssueProps = {
  title: any
  filter: any
  reportsHistory: any[]
  onRun: Function
  dataSource?: any[]
  sellerSetting?: any
  loading?: boolean
  reportName?: string
  saveCustomReportWithName?: Function
  handleReportNameVisibleChange?: Function
}

export class AllocationIssue extends React.PureComponent<AllocationIssueProps> {
  formRef = React.createRef<any>()
  state = {
    currentPage: 1,
    pageSize: 10,
    expandedRowKeys: [],
    dateType: 3,
    from: null,
    to: null,
    dataSource: []
  }

  componentDidMount() {
    const { filter, dataSource } = this.props
    const data = dataSource ? _.orderBy(dataSource, "diff", "desc").map(el => { return { ...el, wholesaleOrderItemId: `${el.wholesaleOrderId}-${el.itemId}` } }) : []
    this.setState({
      dataSource: data,
      dateType: filter && filter.dateType ? filter.dateType : 3,
      from: filter.dateType != 1 && filter.from ? moment(filter.from).format('MM/DD/YYYY') : null,
      to: filter.dateType != 1 && filter.to ? moment(filter.to).format('MM/DD/YYYY') : null
    })
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.dataSource) != JSON.stringify(nextProps.dataSource)) {
      const data = nextProps.dataSource ? _.orderBy(nextProps.dataSource, "diff", "desc").map(el => { return { ...el, wholesaleOrderItemId: `${el.wholesaleOrderId}-${el.itemId}` } }) : []
      this.setState({
        dataSource: data,
      })
    }
  }

  salesColumns: any[] = [
    {
      title: 'Item',
      dataIndex: 'variety',
      align: 'left',
      width: '25%',
      sorter: (a: any, b: any) => {
        return a.variety && b.variety ? a.variety.localeCompare(b.variety) : false
      },
      render: (variety: string, record: any) => {
        return record.children && record.children.length ?
          <span>
            {variety}
          </span>
          :
          <div style={{ paddingLeft: 26 }}>
            <Link to={`/sales-order/${record.wholesaleOrderId}`}>
              <span style={{ color: blue }}>{variety}</span>
            </Link>
          </div>
      }
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.sku && b.sku ? a.sku.localeCompare(b.sku) : false
      },
      render: (sku: string, record: any) => {
        return (<span
          style={{ cursor: 'pointer' }}
          onClick={() => {
            history.push(`/product/${record.itemId}/activity`)
          }}
        >
          {sku}
        </span>)
      }
    },
    {
      title: 'Lot #',
      dataIndex: 'lotId',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.lotId.localeCompare(b.lotId)
      }
    },
    {
      title: 'Qty On-Hand',
      dataIndex: 'onHandQty',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.onHandQty - b.onHandQty
      },
      render: (onHandQty: number, record: any) => {
        return `${formatNumber(mathRoundFun(onHandQty, 2), 2)} ${record.inventoryUOM}*`
      }
    },
    {
      title: 'Qty Ordered',
      dataIndex: 'quantity',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.quantity - b.quantity
      },
      render: (quantity: number, record: any) => {
        return quantity ? `${quantity} ${record.inventoryUOM}*` : ''
      }
    },
    {
      title: 'Difference',
      dataIndex: 'diff',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.diff - b.diff
      },
      render: (diff: number, record: any) => {
        return diff ? `${formatNumber(-diff, 2)} ${record.inventoryUOM}*` : ''
      }
    },
  ]

  onSaveCustomReport = () => {
    const { filter } = this.props
    this.formRef.current
      .validateFields()
      .then((values) => {
        const { reportName } = this.props
        if (!reportName) {
          return
        }
        this.setState(
          {
            visiblePopover: false,
          },
          () => {
            this.props.saveCustomReportWithName({
              reportName,
              filter: {
                ...values,
                ...{ type: filter.type }
              },
            })
            this.props.handleReportNameVisibleChange(false)
          },
        )
      })
      .catch((errorInfo) => {
        console.log(errorInfo)
      })
  }

  onClickRunReport = () => {
    const { from, to, dateType } = this.state
    let data: any = {
      type: 25,
      dateType
    }
    const fromTo: any = getFromAndToByDateType(this.state.dateType)
    if(dateType == 1) {
      delete data.from
      delete data.to
    } else if (dateType != 6 && fromTo.from && fromTo.to) {
      data = { ...data, ...fromTo }
    } else if (from && to) {
      data = { ...data, from, to }
    }
    this.props.onRun(data)
  }

  onPageChange = (page: any, pageSize: number | undefined) => {
    this.setState({ currentPage: page })
  }

  expandColumn = (props: any) => {
    this.setState({ isExpandAll: false })
    return (
      props.record.children && props.record.children.length ? (
        <Icon
          className="icon"
          type={props.expanded ? 'caret-down' : 'caret-right'}
          style={{ marginLeft: -3, cursor: 'pointer' }}
          onClick={() => {
            window.event.preventDefault()
            props.onExpand()
          }}
        />
      ) : (
        ''
      )
    )
  }

  onClickExpand = () => {
    const { dataSource } = this.state
    let allKeys: string[] = []
    if (dataSource) {
      dataSource.forEach(el => {
        if (el.children && el.children.length > 1) {
          allKeys.push(el.wholesaleOrderItemId)
        }
      });
    }
    if (this.state.expandedRowKeys.length) {
      this.setState({ expandedRowKeys: [] })
    } else {
      this.setState({ expandedRowKeys: allKeys })
    }
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    if (this.state.expandedRowKeys.length > 0) {
      const row = []
      for (const value of expandedRowKeys) if (value !== this.state.expandedRowKeys[0]) row.push(value)
      this.setState({
        expandedRowKeys: row,
      })
    } else {
      this.setState({
        expandedRowKeys: expandedRowKeys,
      })
    }
  }

  onDateTypeChange = (dateType: number) => {
    this.setState({ dateType })
  }

  onDateRangeChange = (dates: any, dateStrings: any) => {
    this.setState({
      from: moment(dateStrings[0]).format('MM/DD/YYYY'),
      to: moment(dateStrings[1]).format('MM/DD/YYYY')
    })
  }

  renderFilterForm = () => {
    const { filter } = this.props
    const { dateType, from, to } = this.state
    return (
      <ReportFilter>
        <div style={{ marginBottom: 10 }}>Fulfillment Date:</div>
        <div style={{ display: filter && filter.id ? 'flex' : 'block' }}>
          <ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateTypeChange} value={dateType}>
            <Select.Option value={1}>All</Select.Option>
            <Select.Option value={2}>Today</Select.Option>
            <Select.Option value={3}>Tomorrow</Select.Option>
            <Select.Option value={4}>Yesterday</Select.Option>
            <Select.Option value={5}>Past Week</Select.Option>
            <Select.Option value={6}>Custom Date Range</Select.Option>
          </ThemeSelect>
          {dateType == 6 &&
            <div style={{ margin: filter && filter.id ? '0 0 0 20px' : '20px 0 0 0' }}>
              <DatePicker.RangePicker
                placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                format={'MM/DD/YYYY'}
                onChange={this.onDateRangeChange}
                value={from && to ? [moment(from), moment(to)] : [,]}
                className="report-dp" />
            </div>
          }
        </div>
        <div style={{ marginTop: 30 }}>
          <ThemeButton type="primary" onClick={this.onClickRunReport} style={{ height: 42 }}>
            <IconSVG
              viewBox="0 0 24 24"
              width="24"
              height="24"
              type="run"
              style={{ fill: 'transparent', float: 'left' }}
            />
            <div
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                lineHeight: '21px',
                fontFamily: 'Museo Sans Rounded',
                display: 'block',
                float: 'left',
                marginTop: 2,
                marginLeft: 3,
              }}
            >
              Run Report
            </div>
          </ThemeButton>
        </div>
      </ReportFilter>
    )
  }

  render() {
    const { filter, loading } = this.props
    const { dataSource } = this.state
    return (
      <SalesReportBody style={{ paddingLeft: 60 }} className='allocation-issues'>
        <ReportContent>
          <ReportTitle>
            <span className="bold">Allocation Issue </span><span className="bold" style={{ color: '#C4C4C4' }}>Report</span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              <ReportTable style={{ paddingTop: 0 }}>
                Find items that are oversold to determine how many to allocate for customers
              </ReportTable>

              {!filter || typeof filter.id == 'undefined' ?
                <>{this.renderFilterForm()} </> :
                <Collapse defaultActiveKey={['1']}>
                  <Collapse.Panel header="Hide Filters" key="1">
                    {this.renderFilterForm()}
                  </Collapse.Panel>
                </Collapse>
              }
            </Col>
          </Row>
          {filter && typeof filter.id != 'undefined' &&
            <>
              <Row style={{ marginTop: 24, padding: '0 24px' }} type='flex' justify='space-between'>
                <div>{dataSource ? dataSource.length : 0} Items</div>
                <Flex>
                  <div>
                    <Icon type='caret-down' style={{ marginRight: 12, cursor: 'pointer' }} onClick={this.onClickExpand} />
                    {this.state.expandedRowKeys.length ? 'Collapse' : 'Expand'} All
                  </div>
                </Flex>
              </Row>
              <Row style={{ marginTop: 24 }}>
                <ThemeTable
                  dataSource={dataSource}
                  columns={this.salesColumns}
                  pagination={{ current: this.state.currentPage, pageSize: this.state.pageSize, onChange: this.onPageChange }}
                  loading={loading}
                  rowKey="wholesaleOrderItemId"
                  expandIcon={this.expandColumn}
                  expandedRowKeys={this.state.expandedRowKeys}
                  onExpandedRowsChange={this.onExpendedRowsChange}
                />
              </Row>
            </>
          }
        </ReportContent>
      </SalesReportBody>
    )
  }
}
