import { Col, Row } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import _ from 'lodash'
import moment from 'moment'
import React from 'react'
import { formatNumber, getOrderPrefix, basePriceToRatioPrice } from '~/common/utils'
import { ThemeTable } from '~/modules/customers/customers.style'
import { history } from '~/store/history'
import CostDifferenceForm from '../components/report-detail/cost-difference-form'

import { ReportContent, ReportFilter, ReportTable, ReportTitle, SalesReportBody } from '../reports.style'

interface FormFields {
  fulfillmentDate: any
}

type CostPriceDifferenceProps = {
  title: any
  reportType: string
  filter: any
  reportsHistory: any[]
  onRun: Function
  dataSource?: any[]
  sellerSetting?: any
  loading?: boolean
  reportName?: string
  saveCustomReportWithName?: Function
  handleReportNameVisibleChange?: Function
  isVertial: boolean
}

export class CostPriceDifference extends React.PureComponent<
  FormComponentProps<FormFields> & CostPriceDifferenceProps
> {
  formRef = React.createRef<any>()
  state = {
    currentPage: 1,
    pageSize: 10,
  }

  salesColumns: any[] = [
    {
      title: '',
      dataIndex: 'x',
      align: 'left',
      render: (wholesaleOrderId: any, record: any, index: number) => {
        return (this.state.currentPage - 1) * this.state.pageSize + (index + 1)
      },
    },
    {
      title: 'Order No.',
      dataIndex: 'wholesaleOrderId',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.wholesaleOrderId - b.wholesaleOrderId
      },
      render: (wholesaleOrderId: any, record: any) => {
        let target = `/sales-order/${wholesaleOrderId}`
        if (record.clientType == 'VENDOR') {
          target = `/order/${wholesaleOrderId}/purchase-cart`
        }
        return (
          <span className="hoverable" onClick={() => history.push(`${target}`)}>
            {getOrderPrefix(this.props.sellerSetting, this.props.reportType)}
            {wholesaleOrderId}
          </span>
        )
      },
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.sku && b.sku ? a.sku.localeCompare(b.sku) : false
      },
    },
    {
      title: 'Product',
      dataIndex: 'variety',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.variety.localeCompare(b.variety)
      },
    },
    {
      title: 'Lot No.',
      dataIndex: 'lotId',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.lotId.localeCompare(b.lotId)
      },
    },
    {
      title: 'Sales Price',
      dataIndex: 'price',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.price - b.price
      },
      render: (price: number, record: any) => {
        return `${formatNumber(price, 2)}/${record.UOM}`
      },
    },
    {
      title: 'Cost',
      dataIndex: 'lotCost',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.lotCost - b.lotCost
      },
      render: (lotCost: number, record: any) => {
        return `${formatNumber(lotCost, 2)}/${record.UOM}`
      },
    },
    {
      title: 'Margin',
      dataIndex: 'margin',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.margin - b.margin
      },
      render: (margin: any, record) => {
        return record.price != 0
          ? `${formatNumber(((record.price - record.lotCost) / record.price) * 100, 2)}%`
          : 'Error: 0 price'
      },
    },
    {
      title: 'Fulfillment Date',
      dataIndex: 'deliveryDate',
      align: 'left',
      sorter: (a: any, b: any) => {
        return moment(a.deliveryDate).unix() - moment(b.deliveryDate).unix()
      },
      render: (deliveryDate: any) => {
        return deliveryDate ? moment(deliveryDate).format('MM/DD/YYYY') : ''
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.status.localeCompare(b.status)
      },
    },
  ]

  purchaseColumns: any[] = [
    {
      title: '',
      dataIndex: 'x',
      align: 'left',
      render: (wholesaleOrderId: any, record: any, index: number) => {
        return (this.state.currentPage - 1) * this.state.pageSize + (index + 1)
      },
    },
    {
      title: 'Order No.',
      dataIndex: 'wholesaleOrderId',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.wholesaleOrderId - b.wholesaleOrderId
      },
      render: (wholesaleOrderId: any, record: any) => {
        let target = `/sales-order/${wholesaleOrderId}`
        if (record.clientType == 'VENDOR') {
          target = `/order/${wholesaleOrderId}/purchase-cart`
        }
        return (
          <span className="hoverable" onClick={() => history.push(`${target}`)}>
            {getOrderPrefix(this.props.sellerSetting, this.props.reportType)}
            {wholesaleOrderId}
          </span>
        )
      },
    },
    {
      title: 'Lot No.',
      dataIndex: 'lotId',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.lotId.localeCompare(b.lotId)
      },
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.sku && b.sku ? a.sku.localeCompare(b.sku) : false
      },
    },
    {
      title: 'Product',
      dataIndex: 'variety',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.variety.localeCompare(b.variety)
      },
    },
    {
      title: 'Purchase Cost',
      dataIndex: 'lotCost',
      align: 'left',
      sorter: (a: any, b: any) => {
        return (a.lotCost + a.perAllocateCost) - (b.lotCost + b.perAllocateCost)
      },
      render: (lotCost: number, record: any) => {
        return `${formatNumber(
            basePriceToRatioPrice(record.defaultPurchaseUnitUOM, (lotCost + record.perAllocateCost), record.wholesaleItem),
          2,
        )}`
      },
    },
    {
      title: 'Default Sales Price',
      dataIndex: 'itemPrice',
      align: 'itemPrice',
      sorter: (a: any, b: any) => {
        return a.itemPrice - b.itemPrice
      },
      render: (itemPrice: number, record: any) => {
        return `${formatNumber(itemPrice, 2)}/${record.defaultPurchaseUnitUOM}`
      },
    },
    {
      title: 'Margin',
      dataIndex: 'margin',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.margin - b.margin
      },
      render: (margin: any, record: any) => {
        return record.itemPrice != 0
          ? `${formatNumber(((record.itemPrice - record.lotCost - record.perAllocateCost) / record.itemPrice) * 100, 2)}%`
          : 'Error: 0 price'
      },
    },
    {
      title: 'Delivery Date',
      dataIndex: 'orderDate',
      align: 'left',
      sorter: (a: any, b: any) => {
        return moment(a.orderDate).unix() - moment(b.orderDate).unix()
      },
      render: (orderDate: any) => {
        return orderDate ? moment(orderDate).format('MM/DD/YYYY') : ''
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.status.localeCompare(b.status)
      },
    },
  ]

  backorderColumns: any[] = [
    {
      title: '',
      dataIndex: 'x',
      align: 'left',
      render: (wholesaleOrderId: any, record: any, index: number) => {
        return (this.state.currentPage - 1) * this.state.pageSize + (index + 1)
      },
    },
    {
      title: 'Order No.',
      dataIndex: 'wholesaleOrderId',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.wholesaleOrderId - b.wholesaleOrderId
      },
      render: (wholesaleOrderId: any, record: any) => {
        const target = `/sales-order/${wholesaleOrderId}`
        return (
          <span className="hoverable" onClick={() => history.push(`${target}`)}>
            {getOrderPrefix(this.props.sellerSetting, 'sales')}
            {wholesaleOrderId}
          </span>
        )
      },
    },
    {
      title: 'Customer Name',
      dataIndex: 'customerName',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.customerName && b.customerName ? a.customerName.localeCompare(b.customerName) : false
      },
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.sku && b.sku ? a.sku.localeCompare(b.sku) : false
      },
    },
    {
      title: 'Product',
      dataIndex: 'variety',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.variety.localeCompare(b.variety)
      },
    },
    {
      title: 'Order Qty',
      dataIndex: 'quantity',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.quantity - b.quantity
      },
      render: (quantity: number, record: any) => {
        return `${quantity} ${quantity <= 1 ? record.UOM : record.UOM + 's'}`
      },
    },
    {
      title: 'Picked Qty',
      dataIndex: 'picked',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.picked - b.picked
      },
      render: (picked: number, record: any) => {
        return `${picked} ${picked <= 1 ? record.UOM : record.UOM + 's'}`
      },
    },
    {
      title: 'Fulfillment Date',
      dataIndex: 'createdDate',
      align: 'left',
      sorter: (a: any, b: any) => {
        return moment(a.createdDate).unix() - moment(b.createdDate).unix()
      },
      render: (createdDate: any) => {
        return createdDate ? moment(createdDate).format('MM/DD/YYYY') : ''
      },
    },
    {
      title: 'Sales Rep',
      dataIndex: 'salesRep',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.salesRep && b.salesRep ? a.salesRep.localeCompare(b.salesRep) : false
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.status.localeCompare(b.status)
      },
    },
  ]

  onSaveCustomReport = () => {
    const { filter } = this.props
    this.formRef.current
      .validateFields()
      .then((values) => {
        const { reportName } = this.props
        if (!reportName) {
          return
        }
        this.setState(
          {
            visiblePopover: false,
          },
          () => {
            this.props.saveCustomReportWithName({
              reportName,
              filter: {
                ...values,
                ...{ type: filter.type },
              },
            })
            this.props.handleReportNameVisibleChange(false)
          },
        )
      })
      .catch((errorInfo) => {
        console.log(errorInfo)
      })
  }

  onPageChange = (page: any, pageSize: number | undefined) => {
    this.setState({ currentPage: page })
  }
  render() {
    const { title, reportType, reportsHistory, onRun, filter, loading } = this.props
    return (
      <SalesReportBody style={{ paddingLeft: 60 }} className="cost-price-report">
        <ReportContent>
          <ReportTitle>
            <span className="bold">{title} </span>
            <span className="bold" style={{ color: '#C4C4C4' }}>
              Report
            </span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              {this.props.isVertial && (
                <ReportTable style={{ paddingTop: 0 }}>
                  {reportType == 'sales'
                    ? 'Find Sales Orders where product is priced under cost.'
                    : reportType == 'purchase'
                      ? 'Find Purchase Orders where the purchase cost of an item is above the default sales price.'
                      : 'Find Sales Orders with items where picked quantity is less than ordered quantity.'}
                </ReportTable>
              )}
              <ReportFilter style={{ width: this.props.isVertial ? 210 : '' }}>
                <CostDifferenceForm
                  isVertial={this.props.isVertial}
                  ref={this.formRef}
                  filter={filter}
                  reportType={reportType}
                  reportsHistory={reportsHistory}
                  onRun={onRun}
                />
              </ReportFilter>
            </Col>
          </Row>
          {filter && typeof filter.id != 'undefined' && (
            <Row style={{ marginTop: 24 }}>
              <ThemeTable
                dataSource={this.props.dataSource ? this.props.dataSource : []}
                columns={
                  reportType == 'sales'
                    ? this.salesColumns
                    : reportType == 'purchase'
                      ? this.purchaseColumns
                      : this.backorderColumns
                }
                pagination={{
                  current: this.state.currentPage,
                  pageSize: this.state.pageSize,
                  onChange: this.onPageChange,
                }}
                loading={loading}
              />
            </Row>
          )}
        </ReportContent>
      </SalesReportBody>
    )
  }
}
