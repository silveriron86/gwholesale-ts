import { Input, Col, Row, Icon, Select, DatePicker, Collapse, Tooltip, Table, AutoComplete, Spin } from 'antd'
import moment from 'moment'
import React from 'react'
import _, { cloneDeep } from 'lodash'
import { Link } from 'react-router-dom'
import { getFromAndToByDateType, getOrderPrefix, formatNumber, basePriceToRatioPrice } from '~/common/utils'
import { Icon as IconSVG } from '~/components'
import { Flex, ThemeButton, ThemeSelect, ThemeCheckbox } from '~/modules/customers/customers.style'
import {
  ReportContent,
  ReportFilter,
  ReportTable,
  ReportTitle,
  SalesReportBody,
  Insights,
  InsightsCollapse,
  InventoryLabel,
  Ellipsis
} from '../reports.style'

type InventoryProps = {
  title: any
  filter: any
  reportsHistory: any[]
  onRun: Function
  dataSource?: any[]
  sellerSetting?: any
  loading?: boolean
  reportName?: string
  vendors: any[]
  items: any[]
  saveCustomReportWithName?: Function
  handleReportNameVisibleChange?: Function
  vertical: boolean
}

export const getDays = (deliveryDate: string) => {
  const duration = moment.duration(moment().diff(moment(deliveryDate)))
  const days = Math.floor(duration.asDays())
  return days
}

export const getFullCost = (record: any) => {
  const ratioPerAllocateCost = basePriceToRatioPrice(record.pricingUOM, record.perAllocateCost, record, 2)
  return _.add(record.cost, ratioPerAllocateCost)
}

export const getItemName = (record: any) => {
  if (typeof record.children !== 'undefined') {
    return ''
  }

  let ret = record.variety
  if(record.extraOrigin) {
    ret += (ret ? ', ' : '') + record.extraOrigin
  }
  if(record.modifiers) {
    ret += (ret ? ', ' : '') + record.modifiers
  }
  return ret
}

export class Inventory extends React.PureComponent<InventoryProps> {
  formRef = React.createRef<any>()
  state = {
    currentPage: 1,
    pageSize: 10,
    expandedRowKeys: [],
    dateType: 1,
    from: null,
    to: null,
    dataSource: [],
    includeLotsWithZero: false,
    includeLotsWithNegative: false,
    isFilterCollapsed: false,
    isInsightsCollapsed: false,
    accountNames: [],
    productNames: [],
    selectedClientId: this.props.filter?.wholesaleClientId,
    searchStr: this.props.filter?.search,
    wholesaleItemName: this.props.filter?.wholesaleItemName
  }

  componentDidMount() {
    const { filter, dataSource, loading } = this.props
    let data = dataSource && !loading ? dataSource.map(this.formartRow) : []
    this.setState({
      dataSource: this.fillSKUs(data),
      dateType: filter && filter.dateType ? filter.dateType : 1,
      from: filter.dateType != 1 && filter.from ? moment(filter.from).format('MM/DD/YYYY') : null,
      to: filter.dateType != 1 && filter.to ? moment(filter.to).format('MM/DD/YYYY') : null,
      includeLotsWithZero: filter ? filter.includeLotsWithZero : false,
      includeLotsWithNegative: filter ? filter.includeLotsWithNegative : false
    })
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.dataSource) != JSON.stringify(nextProps.dataSource)) {
      const data = nextProps.dataSource ? nextProps.dataSource.map(this.formartRow) : []
      this.setState({
        dataSource: this.fillSKUs(data),
      })
    }
    if(nextProps.loading) {
      this.setState({dataSource: []})
    }
  }

  fillSKUs = (data: any[]) => {
    return data.map((row: any) => {
      if(row.children && row.children.length > 0) {
        const children = row.children.map((child: any) => {
          child.sku = row.children[0].sku
          return child
        })
        return {
          ...row,
          children
        }
      }
      return row
    })
  }

  formartRow = (el: { wholesaleItemId: any, children?: any[] }) => {
    let children = null
    if (typeof el.children !== 'undefined') {
      children = el.children.map((child: any, index: number) => {
        return {
          ...child,
          wholesaleItemId: `${child.wholesaleItemId}-${index}`
        }
      })
    }
    return {
      ...el,
      wholesaleItemId: `parent-${el.wholesaleItemId}`,
      children: children ? children : undefined
    }
  }



  getInventoryValue = (record: any) => {
    return getFullCost(record) * record.onHandQty
  }

  columns: any[] = [
    {
      title: 'Vendor',
      dataIndex: 'companyName',
      align: 'left',
      width: '15%',
      sorter: (a: any, b: any) => {
        return a.variety && b.variety ? a.variety.localeCompare(b.variety) : false
      },
      render: (variety: string, record: any) => {
        return record.children && record.children.length ?
          <span>
            {record.variety}
          </span>
          :
          <div style={{ paddingLeft: 26 }}>
            {variety}
          </div>
      }
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      align: 'left',
      width: 120,
      render: (sku: string, record: any) => {
        return sku
      }
    },
    {
      title: 'Delivery Date',
      dataIndex: 'deliveryDate',
      align: 'left',
      width: 120,
      render: (deliveryDate: string) => {
        return deliveryDate ? moment(deliveryDate).format('MM-DD-YY') : ''
      },
      sorter: (a: any, b: any) => {
        return a.deliveryDate && b.deliveryDate ? a.deliveryDate.localeCompare(b.deliveryDate) : false
      }
    },
    {
      title: 'PO #',
      dataIndex: 'wholesaleOrderId',
      align: 'left',
      width: 120,
      sorter: (a: any, b: any) => {
        return a.wholesaleOrderId - b.wholesaleOrderId
      },
      render:(wholesaleOrderId: number, record: any)=>{
        if(!wholesaleOrderId) return

        const orderPrefix = getOrderPrefix(this.props.sellerSetting, 'purchase')
        return (
          <Link to={`/order/${wholesaleOrderId}/purchase-cart`} target="_blank">{orderPrefix ? `${orderPrefix}` : ''}{wholesaleOrderId}</Link>
        )
      }
    },
    {
      title: 'Lot #',
      dataIndex: 'lotId',
      align: 'left',
      width: 120,
      sorter: (a: any, b: any) => {
        return a.lotId && b.lotId ? a.lotId.localeCompare(b.lotId) : false
      }
    },
    {
      title: 'Vendor Ref #',
      dataIndex: 'po',
      align: 'left',
      width: 120,
      sorter: (a: any, b: any) => {
        return a.po && b.po ? a.po.localeCompare(b.po) : false
      }
    },
    {
      title: 'PO Notes',
      dataIndex: 'pickerNote',
      align: 'left',
      width: 120,
      sorter: (a: any, b: any) => {
        return a.pickerNote && b.pickerNote ? a.pickerNote.localeCompare(b.pickerNote) : false
      }
    },
    {
      title: 'Days on floor',
      key: 'days-on-floor',
      dataIndex: 'deliveryDate',
      align: 'left',
      render: (deliveryDate: string, record: any) => {
        if(typeof record.footer !== 'undefined') {
          return formatNumber(record.footer.days, 0)
        }
        if(record.children && record.children.length > 0) {
          let sum = 0
          record.children.forEach((child: any) => {
            sum += getDays(child.deliveryDate)
          })
          return (
            <>
              <div>AVG</div>
              {Math.floor(sum / record.children.length)}
            </>
          )
        }

        if(!deliveryDate || typeof deliveryDate === 'undefined') {
          return ''
        }

        return getDays(deliveryDate)
      },
      sorter: (a: any, b: any) => {
        if(a.deliveryDate && b.deliveryDate) {
          const duration1 = moment.duration(moment().diff(moment(a.deliveryDate)))
          const days1 = Math.floor(duration1.asDays())
          const duration2 = moment.duration(moment().diff(moment(b.deliveryDate)))
          const days2 = Math.floor(duration2.asDays())
          return days1 - days2
        }
        return false
      }
    },
    {
      title: 'Qty Received',
      dataIndex: 'receivedQty',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.receivedQty - b.receivedQty
      },
      render: (receivedQty: number, record: any) => {
        if(typeof record.footer !== 'undefined') {
          return formatNumber(record.footer.receivedQty, 2)
        }
        return (
          <>
            {typeof record.children !== 'undefined' && (
              <div>TOT</div>
            )}
            {receivedQty ? formatNumber(receivedQty, 2) : '--'}
          </>
        )
      }
    },
    {
      title: 'Qty Sold',
      dataIndex: 'soldQty',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.soldQty - b.soldQty
      },
      render: (soldQty: number, record: any) => {
        if(typeof record.footer !== 'undefined') {
          return formatNumber(record.footer.soldQty, 2)
        }

        return (
          <>
            {typeof record.children !== 'undefined' && (
              <div>TOT</div>
            )}
            {soldQty ? formatNumber(soldQty, 2) : '--'}
          </>
        )
      }
    },
    {
      title: 'Qty On Hand',
      dataIndex: 'onHandQty',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.onHandQty - b.onHandQty
      },
      render: (onHandQty: number, record: any) => {
        if(typeof record.footer !== 'undefined') {
          return formatNumber(record.footer.onHandQty, 2)
        }
        return (
          <>
            {typeof record.children !== 'undefined' && (
              <div>TOT</div>
            )}
            {onHandQty ? formatNumber(onHandQty, 2) : '--'}
          </>
        )
      }
    },
    {
      title: 'Qty Adjustment',
      dataIndex: 'totalAdjustments',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.totalAdjustments - b.totalAdjustments
      },
      render:(adjustment: number)=> {
        return formatNumber(adjustment, 2)
      }
    },
    {
      title: 'Full Cost',
      dataIndex: 'perAllocateCost',
      align: 'left',
      sorter: (a: any, b: any) => {
        return getFullCost(a) - getFullCost(b)
      },
      render: (value: number, record: any) => {
        if (typeof value === 'undefined') {
          return ''
        }

        return `$${formatNumber(getFullCost(record), 2)}*`
      }
    },
    {
      title: 'Inventory Value',
      key: 'inventory-value',
      align: 'left',
      sorter: (a: any, b: any) => {
        return this.getInventoryValue(a) - this.getInventoryValue(b)
      },
      render: (record: any) => {
        if(typeof record.footer !== 'undefined') {
          return `$${formatNumber(record.footer.inventoryValue, 2)}*`
        }
        if(record.children && record.children.length > 0) {
          let sum = 0
          record.children.forEach((child: any) => {
            sum += this.getInventoryValue(child)
          })
          return (
            <>
              <div>TOT</div>
              ${formatNumber(sum, 2)}
            </>
          )
        }
        return (
          <Tooltip title={`$${formatNumber(record.cost, 2)}/${record.pricingUOM} x ${formatNumber(record.quantity, 0)} ${record.pricingUOM} Avg. per ${record.inventoryUOM}`}>
            ${formatNumber(this.getInventoryValue(record), 2)}*
          </Tooltip>
        )
      }
    },
    {
      title: 'Location',
      dataIndex: 'locations',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.locations - b.locations
      },
      render: (locations: string[], record: any) => {
        if(!locations || locations.length === 0) {
          return ''
        }
        return (
          <Tooltip title={locations.join(', ')}>
            <Ellipsis style={{maxWidth: 150}}>
              <Flex>
                {locations.map((loc: string, index: number) => (
                  <>
                    <Link to={`/location?q=${loc}`} target="_blank">{loc}</Link>
                    {index < locations.length -1 && (<span style={{width: 7}}>, </span>)}
                  </>
                ))}
              </Flex>
            </Ellipsis>
          </Tooltip>
        )
      }
    },
  ]

  onSaveCustomReport = () => {
    const { filter } = this.props
    this.formRef.current
      .validateFields()
      .then((values: any) => {
        const { reportName } = this.props
        if (!reportName) {
          return
        }
        this.setState(
          {
            visiblePopover: false,
          },
          () => {
            this.props.saveCustomReportWithName({
              reportName,
              filter: {
                ...values,
                ...{ type: filter.type }
              },
            })
            this.props.handleReportNameVisibleChange(false)
          },
        )
      })
  }

  onSearchVendor = (searchStr: string) => {
    const { vendors } = this.props
    let accountNames: any[] = []
    if (vendors?.length > 0 && searchStr) {
      vendors.forEach((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      accountNames: accountNames,
      selectedClientId: undefined
    })
  }

  onProductSearch = (searchStr: string, type: string) => {
    const { items } = this.props
    let options: any[] = []
    if (items && items.length > 0 && searchStr) {
      items.forEach((row) => {
        if (row[type] && row[type].toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          options.push({
            value: row[type],
            text: row[type],
          })
        }
      })
    }

    this.setState({
      productNames: options,
      wholesaleItemName: searchStr
    })
  }

  onSelectVendor = (clientId: any) => {
    this.setState({ selectedClientId: clientId })
  }

  onClickRunReport = () => {
    const { from, to, dateType, includeLotsWithZero, includeLotsWithNegative, selectedClientId, wholesaleItemName, searchStr } = this.state
    let data: any = {
      type: 35,
      includeLotsWithZero,
      includeLotsWithNegative,
      dateType,
      wholesaleItemName,
      search: searchStr,
      clientId: selectedClientId,
    }

    const fromTo: any = getFromAndToByDateType(this.state.dateType)
    if(dateType == 1) {
      delete data.from
      delete data.to
    } else if (dateType != 6 && fromTo.from && fromTo.to) {
      data = { ...data, ...fromTo }
    } else if (from && to) {
      data = { ...data, from, to }
    }
    this.props.onRun(data)
  }

  onPageChange = (page: any, pageSize: number | undefined) => {
    this.setState({ currentPage: page })
  }

  expandColumn = (props: any) => {
    this.setState({ isExpandAll: false })
    return (
      props.record.children && props.record.children.length ? (
        <Icon
          className="icon"
          type={props.expanded ? 'caret-down' : 'caret-right'}
          style={{ marginLeft: -3, cursor: 'pointer' }}
          onClick={() => {
            window.event.preventDefault()
            props.onExpand()
          }}
        />
      ) : (
        ''
      )
    )
  }

  onClickExpand = () => {
    const { dataSource } = this.state
    let allKeys: string[] = []
    if (dataSource) {
      dataSource.forEach((el: any) => {
        if (el.children && el.children.length > 0) {
          allKeys.push(el.wholesaleItemId)
        }
      });
    }
    if (this.state.expandedRowKeys.length) {
      this.setState({ expandedRowKeys: [] })
    } else {
      this.setState({ expandedRowKeys: allKeys })
    }
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    if (this.state.expandedRowKeys.length > 0) {
      const row = []
      for (const value of expandedRowKeys) if (value !== this.state.expandedRowKeys[0]) row.push(value)
      this.setState({
        expandedRowKeys: row,
      })
    } else {
      this.setState({
        expandedRowKeys: expandedRowKeys,
      })
    }
  }

  onDateTypeChange = (dateType: number) => {
    this.setState({ dateType })
  }

  onDateRangeChange = (dates: any, dateStrings: any) => {
    this.setState({
      from: moment(dateStrings[0]).format('MM/DD/YYYY'),
      to: moment(dateStrings[1]).format('MM/DD/YYYY')
    })
  }

  onCheck = (e: any, type: string) => {
    let state = cloneDeep(this.state)
    state[type] = e.target.checked
    this.setState(state)
  }

  onChangeSearchStr = (searchStr: string) => {
    this.setState({searchStr})
  }

  renderFilterForm = () => {
    const { filter, vertical } = this.props
    const { dateType, from, to, includeLotsWithZero, includeLotsWithNegative,
      accountNames, productNames, searchStr, selectedClientId, wholesaleItemName } = this.state

    return (
      <ReportFilter>
        <div style={{display: !vertical ? 'flex' : 'block'}}>
          <div>
            <div style={{ marginBottom: 10 }}>PO delivery date:</div>
            <div style={{ alignItems: 'center', display: !vertical ? 'flex' : 'block' }}>
              <ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateTypeChange} value={dateType}>
                <Select.Option value={1}>All</Select.Option>
                <Select.Option value={2}>Today</Select.Option>
                <Select.Option value={3}>Tomorrow</Select.Option>
                <Select.Option value={4}>Yesterday</Select.Option>
                <Select.Option value={5}>Past Week</Select.Option>
                <Select.Option value={6}>Custom Date Range</Select.Option>
              </ThemeSelect>

              {dateType == 6 &&
                <div style={{ margin: !vertical ? '0 0 0 20px' : '20px 0 0 0' }}>
                  <DatePicker.RangePicker
                    placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                    format={'MM/DD/YYYY'}
                    onChange={this.onDateRangeChange}
                    value={from && to ? [moment(from), moment(to)] : [,]}
                    className="report-dp" />
                </div>
              }
            </div>
          </div>
          <div style={{ margin: !vertical ? '0 0 0 20px' : '20px 0 0 0', width: !vertical ? '200px' : '30%' }}>
            <div style={{ marginBottom: 10 }}>Vendor:</div>
            <AutoComplete
              placeholder="Vendor"
              defaultValue={this.props.vendors.find(el=>el.clientId == selectedClientId)?.clientCompany.companyName}
              style={{width: '100%'}}
              dataSource={accountNames}
              onSearch={this.onSearchVendor}
              onSelect={this.onSelectVendor}
            />
          </div>
          <div style={{ margin: !vertical ? '0 0 0 20px' : '20px 0 0 0', width: !vertical ? '200px' : '30%' }}>
            <div style={{ marginBottom: 10 }}>Product Names:</div>
            <AutoComplete
              defaultValue={wholesaleItemName}
              placeholder="Product Name"
              style={{width: '100%'}}
              dataSource={productNames}
              onSearch={(v) => this.onProductSearch(v, 'variety')}
            />
          </div>
          <div style={{ margin: !vertical ? '0 0 0 20px' : '20px 0 0 0', width: !vertical ? '300px' : '30%' }}>
            <div style={{ marginBottom: 10 }}>&nbsp;</div>
            <Input.Search
              style={{width: '100%'}}
              value={searchStr}
              placeholder='Search PO, lot, or vendor ref.no'
              onChange={(evt)=> this.onChangeSearchStr(evt.target.value)}
              />
          </div>
        </div>
        <div>
          <div style={{marginTop: 30}}>
            <Tooltip title="Due to the large number of records, this option may not work.">
              <ThemeCheckbox id="includeLotsWithZero" checked={includeLotsWithZero} onChange={(e: any) => this.onCheck(e, 'includeLotsWithZero')}>
                Include lots with 0 on hand quantity
              </ThemeCheckbox>
            </Tooltip>
          </div>
          <div style={{marginTop: 10}}>
            <Tooltip title="Due to the large number of records, this option may not work.">
              <ThemeCheckbox checked={includeLotsWithNegative} onChange={(e: any) => this.onCheck(e, 'includeLotsWithNegative')}>
                Include lots with negative on hand quantity
              </ThemeCheckbox>
            </Tooltip>
          </div>
        </div>
        <div style={{ marginTop: 30 }}>
          <ThemeButton type="primary" onClick={this.onClickRunReport} style={{ height: 42 }}>
            <IconSVG
              viewBox="0 0 24 24"
              width="24"
              height="24"
              type="run"
              style={{ fill: 'transparent', float: 'left' }}
            />
            <div
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                lineHeight: '21px',
                fontFamily: 'Museo Sans Rounded',
                display: 'block',
                float: 'left',
                marginTop: 2,
                marginLeft: 3,
              }}
            >
              Run Report
            </div>
          </ThemeButton>
        </div>
      </ReportFilter>
    )
  }

  onChangeFilterCollapse = (keys: any[]) => {
    this.setState({
      isFilterCollapsed: keys.length == 0
    })
  }

  onChangeInsightsCollapse = (keys: any[]) => {
    this.setState({
      isInsightsCollapsed: keys.length == 0
    })
  }

  renderTableFooter = (summaryData: any[]) => {
    if(this.state.dataSource.length > 0) {
      return () => (
        <Table columns={this.columns} dataSource={summaryData} pagination={false} showHeader={true} />
      )
    }
    return undefined
  }

  render() {
    const { filter, loading } = this.props
    const { dataSource, isFilterCollapsed, isInsightsCollapsed } = this.state

    let totalQtyOnHand = 0
    let totalLots = 0
    let totalInventory = 0
    let totalSoldQty = 0
    let totalDays = 0
    let totalReceivedQty  = 0
    dataSource.forEach((row: any) => {
      if(row.children && row.children.length > 0) {
        row.children.forEach((child: any) => {
          totalDays += getDays(child.deliveryDate)
          totalInventory += this.getInventoryValue(child)
        })
      }
      totalLots += row.children.length
      totalQtyOnHand += row.onHandQty
      totalSoldQty += row.soldQty
      totalReceivedQty += row.receivedQty
    })
    return (
      <SalesReportBody style={{ paddingLeft: 60 }} className='inventory'>
        <ReportContent>
          <ReportTitle>
            <span className="bold">Inventory </span><span className="bold" style={{ color: '#C4C4C4' }}>Report</span>
          </ReportTitle>
          <Spin spinning={loading}>
          <Row>
            <Col span={24}>
              <ReportTable style={{ paddingTop: 0 }}>
                See your on-hand inventory, cost value, and age by lots.
              </ReportTable>

              {!filter || typeof filter.id == 'undefined' ?
                <>{this.renderFilterForm()} </> : (
                  <div>
                    <Collapse defaultActiveKey={['1']} onChange={this.onChangeFilterCollapse}>
                      <Collapse.Panel header={`${isFilterCollapsed ? 'Show': 'Hide'} Filters`} key="1">
                        {this.renderFilterForm()}
                      </Collapse.Panel>
                    </Collapse>
                    <InsightsCollapse defaultActiveKey={['1']} onChange={this.onChangeInsightsCollapse}>
                      <Collapse.Panel header={`${isInsightsCollapsed ? 'Show': 'Hide'} Insights`} key="1">
                      <Insights>
                        <div className="cl">
                          <div className="label">total Inventory Value</div>
                          <div className="value">${formatNumber(totalInventory, 2)}*</div>
                        </div>
                        <div className="cl" style={{marginLeft: 46, marginRight: 46}}>
                          <div className="label">total qty on hand</div>
                          <div className="value">{formatNumber(totalQtyOnHand, 0)}</div>
                        </div>
                        <div className="cl">
                          <div className="label">total # of lots</div>
                          <div className="value">{formatNumber(totalLots, 0)}</div>
                        </div>
                      </Insights>
                      </Collapse.Panel>
                    </InsightsCollapse>
                  </div>
                )}
            </Col>
          </Row>
          {filter && typeof filter.id != 'undefined' &&
            <>
              <Row style={{ marginTop: 0, padding: '0 24px' }} type='flex' justify='space-between'>
                <InventoryLabel>* Indicates approximate value</InventoryLabel>
                <Flex>
                  <div>
                    <Icon type='caret-down' style={{ marginRight: 12, cursor: 'pointer' }} onClick={this.onClickExpand} />
                    {this.state.expandedRowKeys.length ? 'Collapse' : 'Expand'} All
                  </div>
                </Flex>
              </Row>
              <Row style={{ marginTop: 24 }}>
                <Table
                  dataSource={dataSource}
                  columns={this.columns}
                  pagination={{ current: this.state.currentPage, pageSize: this.state.pageSize, onChange: this.onPageChange }}
                  // loading={loading}
                  rowKey="wholesaleItemId"
                  expandIcon={this.expandColumn}
                  expandedRowKeys={this.state.expandedRowKeys}
                  onExpandedRowsChange={this.onExpendedRowsChange}
                  footer={this.renderTableFooter([{
                    footer: {
                      item: 'Total',
                      days: Math.floor(totalDays / totalLots),
                      receivedQty: totalReceivedQty,
                      soldQty: totalSoldQty,
                      onHandQty: totalQtyOnHand,
                      inventoryValue: totalInventory
                    }
                  }])}
                />
              </Row>
            </>
          }
          </Spin>
        </ReportContent>
      </SalesReportBody>
    )
  }
}
