import React, { FC, useState, useRef, useEffect } from 'react'
import { Col, Row, Icon, Select, DatePicker, Collapse, Button } from 'antd'
import { ReportFilter, ReportTable, ReportTitle, SalesReportBody, ReportContent } from '../reports.style'
import {
  Flex,
  ThemeButton,
  ThemeCheckbox,
  ThemeModal,
  ThemeSelect,
  ThemeTable,
} from '~/modules/customers/customers.style'
import { Icon as IconSVG } from '~/components'
import moment from 'moment'
import { formatNumber, formatOrderStatus, getFromAndToByDateType, getOrderPrefix } from '~/common/utils'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { ReportsModule } from '../reports.module'
import { Link } from 'react-router-dom'
const lotErrorsByOrder: FC<any> = ({
  isVertical,
  filter,
  sellers,
  onRun,

  getLotsErrorByOrderReport, // action
  lotsErrorByOrderData, // reducer
  sellerSetting, // reducer
}) => {
  const [dateType, setDateType] = useState<any>('3')
  const [customDate, setCustomDate] = useState<any>([])
  const [customDateFormat, setCustomDateFormat] = useState<any>([])
  const [sellerId, setSellerId] = useState<any>(0)
  const [displayNotReceived, setDisplayNotReceived] = useState<any>(false)
  const [dataSource, setDataSource] = useState<any>([])
  const [expandedRowKeys, setExpandedRowKeys] = useState<any>([])

  const [tablePagination, setTablePagination] = useState<any>({
    page: 1,
    pageSize: 10,
  })

  const presets: any = [
    {
      key: '1',
      label: 'All',
    },
    {
      key: '2',
      label: 'Today',
      from: moment().format('MM/DD/YYYY'),
      to: moment().format('MM/DD/YYYY'),
    },
    {
      key: '3',
      label: 'Tomorrow',
      from: moment()
        .add(1, 'days')
        .format('MM/DD/YYYY'),
      to: moment()
        .add(1, 'days')
        .format('MM/DD/YYYY'),
    },
    {
      key: '4',
      label: 'Yesterday',
      from: moment()
        .subtract(1, 'days')
        .format('MM/DD/YYYY'),
      to: moment()
        .subtract(1, 'days')
        .format('MM/DD/YYYY'),
    },
    {
      key: '5',
      label: 'Past Week',
      from: moment()
        .subtract(7, 'days')
        .format('MM/DD/YYYY'),
      to: moment().format('MM/DD/YYYY'),
    },
    {
      key: '6',
      label: 'Custom Date Range',
      from: customDateFormat[0] || null,
      to: customDateFormat[1] || null,
    },
  ]

  const changeCustomDate = (dates: Array<any>, datesFormat: Array<any>) => {
    setCustomDate([...dates])
    setCustomDateFormat([...datesFormat])
  }

  const onClickRunReport = (isAllocationLots: boolean = false) => {
    const data = {
      type: filter?.type || 45,
      from: presets.find((item: any) => item.key === dateType).from || null,
      to: presets.find((item: any) => item.key === dateType).to || null,
      dateType,
      sellerId,
      displayNotReceived,
    }
    if (!isAllocationLots) {
      onRun(data) // save run report history
    }
    // get lot error by order data
    getLotsErrorByOrderReport(data)
  }

  const columns: any[] = [
    {
      title: 'Customer',
      dataIndex: 'companyName',
      align: 'left',
      width: '25%',
      sorter: (a: any, b: any) => {
        return a.variety && b.variety ? a.variety.localeCompare(b.variety) : false
      },
      render: (variety: string, record: any) => {
        return record.children && record.children.length ? (
          <span>{variety}</span>
        ) : (
          <div style={{ paddingLeft: 26 }}>{record.variety}</div>
        )
      },
    },
    {
      title: 'SO #',
      dataIndex: 'orderId',
      align: 'left',
      render: (orderId: number, record: any) => {
        if (!orderId) return
        const orderPrefix = getOrderPrefix(sellerSetting, 'sales')
        return (
          <Link to={`/sales-order/${orderId}`}>
            {orderPrefix ? `${orderPrefix}` : ''}
            {orderId}
          </Link>
        )
      },
    },
    {
      title: 'Order Status',
      dataIndex: 'orderStatus',
      align: 'left',
      render: (status: string, record: any) => {
        return formatOrderStatus(status, sellerSetting)
      },
    },
    {
      title: 'Sales Rep',
      dataIndex: 'salesRep',
      align: 'left',
    },
    {
      title: 'SKU',
      dataIndex: 'sku',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.parentRowSku && b.parentRowSku ? a.parentRowSku.localeCompare(b.parentRowSku) : false
      },
    },
    {
      title: 'Lot',
      dataIndex: 'lotId',
      align: 'left',
    },
    {
      title: 'Lot Status',
      dataIndex: 'lotStatus',
      align: 'left',
    },
    {
      title: 'Lot Avail to Sell',
      dataIndex: 'lotAvailableQty',
      align: 'left',
      render: (value: any, record: any) => {
        return value ? parseFloat(value).toFixed(2) : ''
      },
    },
    {
      title: 'Order Qty',
      dataIndex: 'quantity',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.quantity - b.quantity
      },
      render: (quantity: number, record: any) => {
        return quantity ? `${formatNumber(quantity, 2)} ${record.UOM}` : ''
      },
    },
    {
      title: 'Picked Qty',
      dataIndex: 'picked',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.picked - b.picked
      },
      render: (picked: number, record: any) => {
        return picked ? `${formatNumber(picked, 2)} ${record.UOM}` : ''
      },
    },
  ]

  const expandColumn = (expandProps: any) => {
    if (!expandProps?.record?.orderItemList?.length) return ''
    expandProps.record.children = [...expandProps?.record?.orderItemList]
    return (
      <Icon
        className="icon"
        type={expandProps.expanded ? 'caret-down' : 'caret-right'}
        style={{ marginLeft: -6, cursor: 'pointer' }}
        onClick={(e) => {
          e.preventDefault()
          expandProps.onExpand()
        }}
      />
    )
  }

  const onClickExpand = () => {
    let allKeys: string[] = []
    if (dataSource) {
      dataSource.forEach((el: any) => {
        if (el.orderItemList && el.orderItemList.length > 0) {
          allKeys.push(el.orderId)
        }
      })
    }

    if (expandedRowKeys.length) {
      setExpandedRowKeys([])
    } else {
      setExpandedRowKeys([...allKeys])
    }
  }

  const expandedRowsChange = (arr: any) => {
    setExpandedRowKeys([...arr])
  }

  useEffect(() => {
    setDataSource([...lotsErrorByOrderData])
  }, [lotsErrorByOrderData])

  const onPageChange = (page: number, pageSize: number | undefined) => {
    setTablePagination({
      page,
      pageSize,
    })
  }

  const renderFilterForm = () => (
    <ReportFilter>
      <div style={{ display: !isVertical ? 'flex' : 'block' }}>
        <div>
          <div style={{ marginBottom: 10 }}>Fulfillment Date:</div>
          <div style={{ display: !isVertical ? 'flex' : 'block' }}>
            <ThemeSelect style={{ minWidth: 150 }} onChange={setDateType} value={dateType}>
              {presets.map((item: any) => (
                <Select.Option value={item.key}>{item.label}</Select.Option>
              ))}
            </ThemeSelect>
            {/* 6: 'Custom Date Range' */}
            {dateType === '6' && (
              <div style={{ margin: !isVertical ? '0 0 0 20px' : '20px 0 0 0' }}>
                <DatePicker.RangePicker
                  placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                  format={'MM/DD/YYYY'}
                  className="report-dp"
                  onChange={changeCustomDate}
                  value={customDate}
                />
              </div>
            )}
          </div>
        </div>

        <div style={{ margin: !isVertical ? '0 0 0 20px' : '20px 0 0 0' }}>
          <div style={{ marginBottom: 10 }}>Sales Representative:</div>
          <ThemeSelect style={{ minWidth: 150 }} value={sellerId} onChange={setSellerId}>
            <Select.Option value={0}>All</Select.Option>
            {sellers &&
              sellers.map((item: any) => {
                return <Select.Option value={item.userId}>{`${item.firstName} ${item.lastName}`}</Select.Option>
              })}
          </ThemeSelect>
        </div>
        <div style={{ margin: !isVertical ? '35px 0 0 20px' : '30px 0 0 0', display: 'flex' }}>
          <ThemeCheckbox
            checked={displayNotReceived}
            onChange={(e: any) => setDisplayNotReceived(e.target.checked)}
            style={{ marginRight: 8 }}
          />
          <div style={{ marginBottom: 10 }}>Display items from lots not yet received</div>
        </div>
      </div>
      <div style={{ marginTop: 30 }}>
        <ThemeButton type="primary" onClick={() => onClickRunReport(false)} style={{ height: 42 }}>
          <IconSVG
            viewBox="0 0 24 24"
            width="24"
            height="24"
            type="run"
            style={{ fill: 'transparent', float: 'left' }}
          />
          <div
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              lineHeight: '21px',
              fontFamily: 'Museo Sans Rounded',
              display: 'block',
              float: 'left',
              marginTop: 2,
              marginLeft: 3,
            }}
          >
            Run Report
          </div>
        </ThemeButton>
      </div>
    </ReportFilter>
  )

  const renderItem = () => {
    if (filter && filter.id) {
      return (
        <>
          <Collapse defaultActiveKey={['1']}>
            <Collapse.Panel header="Hide Filters" key="1">
              {renderFilterForm()}
            </Collapse.Panel>
          </Collapse>
          <Row style={{ marginTop: 24, padding: '0 24px' }} type="flex" justify="space-between">
            <div>{dataSource ? dataSource.length : 0} Items</div>
            <Flex style={{ flexDirection: 'column' }}>
              <div>
                <Icon type="caret-down" style={{ marginRight: 12, cursor: 'pointer' }} onClick={onClickExpand} />
                {expandedRowKeys.length ? 'Collapse' : 'Expand'} All
              </div>
            </Flex>
          </Row>
          <Row style={{ marginTop: 24 }}>
            <ThemeTable
              dataSource={dataSource}
              columns={columns}
              pagination={{
                current: tablePagination.page,
                pageSize: tablePagination.pageSize,
                onChange: onPageChange,
              }}
              rowKey="orderId"
              expandIcon={expandColumn}
              expandedRowKeys={expandedRowKeys}
              onExpandedRowsChange={(arr: string[] | number[]) => expandedRowsChange(arr)}
            />
          </Row>
        </>
      )
    }
    return renderFilterForm()
  }

  return (
    <>
      <SalesReportBody style={{ paddingLeft: 60 }} className="allocation-issues">
        <ReportContent>
          <ReportTitle>
            <span className="bold">Lot Errors By Order </span>
            <span className="bold" style={{ color: '#C4C4C4' }}>
              Report
            </span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              <ReportTable style={{ paddingTop: 0 }}>
                Find items that are sold without lots or sold from lots with negative inventory
              </ReportTable>
              {renderItem()}
            </Col>
          </Row>
        </ReportContent>
      </SalesReportBody>
    </>
  )
}

const mapStateToProps = (state: GlobalState) => state.reports
export default connect(ReportsModule)(mapStateToProps)(lotErrorsByOrder)
