import { Col, Form, Row } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import moment from 'moment'
import React from 'react'
import Moment from 'react-moment'
import { Link } from 'react-router-dom'
import { Icon } from '~/components'
import { TempCustomer, SaleItem, SaleCategory } from '~/schema'
import ReceivingForm from '../components/report-detail/receiving-form'

import {
  ReportContent,
  ReportFilter,
  ReportTable,
  ReportTitle,
  RunHistoryItem,
  SalesReportBody,
} from '../reports.style'

interface FormFields {
  fulfillmentDate: any
  accountName: string
  accountNumber: number
  salesOrder: number
  salesRepresentative: number
  sku: string
  variety: string
  onRun: Function
  sellers: any[]
  filter: any
}

type ReceivingFoodReportProps = {
  reportsHistory: any[]
  category: number
  item: any
  customers: TempCustomer[]
  vendors: any[]
  items: SaleItem[]
  categories: SaleCategory[]
}

export class ReceivingFoodReport extends React.PureComponent<FormComponentProps<FormFields> & ReceivingFoodReportProps> {
  state = {
    searchMenuItem: '',
    selectedMenuItem: '',
    fromReportDate: moment().subtract(30, 'days'),
    toReportDate: moment(),
  }

  renderRunHistory = () => {
    const { reportsHistory } = this.props
    let result: any[] = []
    reportsHistory.forEach((el) => {
      result.push(
        <Link to={`/reports/${el.id}`}>
          <RunHistoryItem>
            <span className="date">
              <Moment format="MM/DD/YYYY" date={moment.utc(el.createdAt)} />
            </span>
            &nbsp;by {el.createdBy.firstName} {el.createdBy.lastName}
            <Icon type="arrow-right-product" viewBox="0 0 10 10" width={18} height={12} />
          </RunHistoryItem>
        </Link>,
      )
    })
    return result
  }
  render() {
    const {
      form: { getFieldDecorator },
      filter,
      category,
      item,
      customers,
      vendors,
      items,
      categories,
    } = this.props

    let title = ''
    switch (category) {
      case 0:
        title = 'Custom'
        break
      case 2:
        title = item
        break
    }
    return (
      <SalesReportBody>
        <ReportContent>
          <ReportTitle>
            <span className="bold">Receiving Food Safety Report</span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              <ReportTable>View Purchase Order Receiving Details</ReportTable>
              <ReportFilter style={{ width: 210 }}>
                <ReceivingForm
                  vertical={true}
                  onRun={this.props.onRun}
                  defaultFilter={filter}
                  sellers={this.props.sellers}
                  customers={customers}
                  vendors={vendors}
                  categories={categories}
                  items={items}
                />
              </ReportFilter>
            </Col>
            {/* <Col span={6} offset={2}>
              <SubLabelSpan style={{ marginTop: -5 }}>LAST RUN REPORTS</SubLabelSpan>
              {this.renderRunHistory()}
            </Col> */}
          </Row>
        </ReportContent>
      </ReceivingFootReportBody>
    )
  }
}

export default Form.create<FormComponentProps<FormFields> & ReceivingFootReportProps>()(ReceivingFoodReport)
