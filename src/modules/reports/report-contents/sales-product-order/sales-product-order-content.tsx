import React from 'react'
import { DetailWrapper, ExpandedTabelWrapper, FlexWrap, InsightBlock, PanelHeader } from '../../reports.style'
import { Collapse, Icon, Tooltip } from 'antd'
import { formatDate, formatNumber, getOrderPrefix } from '~/common/utils'
import { Link } from 'react-router-dom'
import { ThemeTable } from '~/modules/customers/customers.style'
const { Panel } = Collapse

interface SalesProductOrderReportContentProps {
  filter: any
  sellerSetting: any
  salesProductReport: any[]
  salesProductOrderReport: any[]

  getGrossProfit: Function
  getMarkup: Function
  getGrossMargin: Function
  getFilterOption: Function
  useCustomIcon: Function
  getSalesProductOrderReportData: Function
  getSalesProductOrderProductReportData: Function
}

export default class SalesProductOrderReportContent extends React.PureComponent<SalesProductOrderReportContentProps> {
  multiSortTable = React.createRef<any>()
  state = {
    activeKeys: []
  }

  onChangeCollapse = (keys: string[]) => {
    const { filter } = this.props
    const { activeKeys } = this.state
    if (keys.length > activeKeys.length) {
      const categoryId = keys[keys.length - 1]
      let opt = this.props.getFilterOption(filter)
      opt.categoryId = categoryId
      this.props.getSalesProductOrderProductReportData(opt)
    } 
    this.setState({
      activeKeys: keys,
    })
  }

  render () {
    const { activeKeys } = this.state
    const { salesProductOrderReport, salesProductReport, sellerSetting } = this.props

    const totalColumns = [
      {
        title: 'Category',
        dataIndex: 'categoryName',
        align: 'left',
        width: 330,
      },
      {
        title: 'Units',
        align: 'right',
        dataIndex: 'totalUnits',
        width: 150,
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'right',
        width: 150,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Costs',
        dataIndex: 'totalCost',
        align: 'right',
        width: 150,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Profit',
        dataIndex: 'profit',
        align: 'right',
        width: 150,
        render: (value: number, record: any) => {
          const val = this.props.getGrossProfit(record)
          return `${val < 0 ? '-' : ''}$${formatNumber(Math.abs(val), 2)}`
        },
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'right',
        width: 150,
        render: (_value: number, record: any) => `${formatNumber(this.props.getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'right',
        width: 150,
        render: (_value: number, record: any) => `${formatNumber(this.props.getGrossMargin(record), 2)}%`,
      },
    ]

    const columns = [
      {
        title: 'Category',
        dataIndex: 'variety',
        align: 'left',
        width: 300,
        sorter: (a: any, b: any) => {
          return a.variety.localeCompare(b.variety)
        },
      },
      {
        title: 'Customer',
        align: 'left',
        dataIndex: 'companyName',
        width: 200,
      },
      {
        title: 'Order #',
        align: 'right',
        dataIndex: 'salesOrderId',
        width: 150,
        render: (wholesaleOrderId: number, record: any) => {
          return wholesaleOrderId
        }
      },
      {
        title: 'Sale Date',
        dataIndex: 'receiveDate',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => a.receiveDate - b.receiveDate,
        render: (receiveDate: number) => {
          return formatDate(receiveDate)
        },
      },
      {
        title: 'Units',
        align: 'right',
        dataIndex: 'totalUnits',
        width: 150,
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalRevenue - b.totalRevenue,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Cost',
        dataIndex: 'totalCost',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalCost - b.totalCost,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Profit',
        dataIndex: 'profit',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossProfit(a) - this.props.getGrossProfit(b),
        render: (_value: number, record: any) => `$${formatNumber(this.props.getGrossProfit(record), 2)}`,
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this.props.getMarkup(a) - this.props.getMarkup(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossMargin(a) - this.props.getGrossMargin(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getGrossMargin(record), 2)}%`,
      },
    ]

    const summaryColumns = [
      {
        title: 'Variety',
        dataIndex: 'variety',
        align: 'left',
        width: 300,
        sorter: (a: any, b: any) => {
          return a.variety.localeCompare(b.variety)
        },
      },
      {
        title: 'Customer',
        align: 'left',
        dataIndex: 'companyName',
        width: 200,
      },
      {
        title: 'Order #',
        align: 'right',
        dataIndex: 'salesOrderId',
        width: 150,
        render: (wholesaleOrderId: number, record: any) => {
          return (
            <Link to={`/sales-order/${wholesaleOrderId}`}>{getOrderPrefix(sellerSetting, 'sales')}{wholesaleOrderId}</Link>
            )
        }
      },
      {
        title: 'Sale Date',
        dataIndex: 'receiveDate',
        align: 'left',
        width: 150,
        sorter: (a: any, b: any) => a.receiveDate - b.receiveDate,
        render: (receiveDate: number) => {
          return formatDate(receiveDate)
        },
      },
      {
        title: 'Units',
        align: 'right',
        dataIndex: 'totalUnits',
        width: 150,
        render: (totalUnits: string, record: any) => {
          return `${totalUnits} ${record.UOM}`
        }
      },
      {
        title: 'Sales',
        dataIndex: 'totalRevenue',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalRevenue - b.totalRevenue,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Cost',
        dataIndex: 'totalCost',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => a.totalCost - b.totalCost,
        render: (value: number) => `$${formatNumber(value, 2)}`,
      },
      {
        title: 'Gross Profit',
        dataIndex: 'profit',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossProfit(a) - this.props.getGrossProfit(b),
        render: (_value: number, record: any) => `$${formatNumber(this.props.getGrossProfit(record), 2)}`,
      },
      {
        title: 'Markup',
        dataIndex: 'markup',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this.props.getMarkup(a) - this.props.getMarkup(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getMarkup(record), 2)}%`,
      },
      {
        title: 'Gross Margin',
        dataIndex: 'margin',
        align: 'right',
        width: 150,
        sorter: (a: any, b: any) => this.props.getGrossMargin(a) - this.props.getGrossMargin(b),
        render: (_value: number, record: any) => `${formatNumber(this.props.getGrossMargin(record), 2)}%`,
      },
    ]

    let SumOfRevenue = 0
    let SumOfCost = 0
    let SumOfProfit = 0

    salesProductOrderReport.forEach((item: any, index: number) => {
      SumOfRevenue += item.totalRevenue
      SumOfCost += item.totalCost
      SumOfProfit += this.props.getGrossProfit(item)
    })

    return (
      <>
        <DetailWrapper className="insight">
          <Collapse defaultActiveKey={['2']}>
            <Panel
              header={
                <FlexWrap className="between">
                  <div>Insights(5)</div>
                </FlexWrap>
              }
              key="2"
            >
              <FlexWrap>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label">Total Sales</div>
                  <div className="value">${formatNumber(SumOfRevenue, 2)}</div>
                </InsightBlock>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label">Total Costs</div>
                  <div className="value">${formatNumber(SumOfCost, 2)}</div>
                </InsightBlock>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label">Total Profit</div>
                  <div className="value">${formatNumber(SumOfProfit, 2)}</div>
                </InsightBlock>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label">
                    Avg. Markup
                    <Tooltip title="Markup is calculated as the difference between sales and costs, as a percentage of costs">
                      <Icon type="info-circle" className="info-icon" style={{ margin: '0 4px' }} />
                    </Tooltip>
                  </div>
                  <div className="value">{formatNumber(SumOfProfit / SumOfCost * 100, 2)}%</div>
                </InsightBlock>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label">
                    Avg. Gross Margin
                    <Tooltip title="Gross margin is calculated as profit divided by sales multipled by 100%">
                      <Icon type="info-circle" className="info-icon" style={{ margin: '0 4px' }} />
                    </Tooltip>
                  </div>
                  <div className="value">{formatNumber(SumOfProfit / SumOfRevenue * 100, 2)}%</div>
                </InsightBlock>
              </FlexWrap>
            </Panel>
          </Collapse>
        </DetailWrapper>
        <DetailWrapper className="last">
        <h3>{salesProductOrderReport.length} Categories</h3>
        <Collapse activeKey={activeKeys} onChange={this.onChangeCollapse}>
          {salesProductOrderReport.map((item: any, index: number) => {
            return (
              <Panel
                header={
                  <PanelHeader>
                    <ThemeTable pagination={false} columns={totalColumns} dataSource={item} rowKey="categoryId" />
                  </PanelHeader>
                }
                key={item.categoryId}
              >
                <ThemeTable
                  className="reports-table"
                  ref={this.multiSortTable}
                  style={{ paddingLeft: 0, paddingRight: 0 }}
                  pagination={false}
                  columns={columns}
                  dataSource={salesProductReport[item.categoryId]}
                  rowKey="clientId"
                  expandedRowRender={(record: any) => {
                    return (
                      <ExpandedTabelWrapper>
                        <ThemeTable
                          pagination={false}
                          columns={summaryColumns}
                          dataSource={record.summary}
                          rowKey="wholesaleItemId"
                        />
                      </ExpandedTabelWrapper>
                    )
                  }}
                  expandIconAsCell={false}
                  expandIcon={this.props.useCustomIcon}
                />
              </Panel>
            )
          })}
        </Collapse>
      </DetailWrapper>
    </>
    )
  }
}
