import { DatePicker, Form, Select, AutoComplete } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import moment from 'moment'
import React from 'react'
import { getFromAndToByDateType } from '~/common/utils'
import { Icon } from '~/components'
import { ThemeButton, ThemeCheckbox, ThemeInput, ThemeSelect } from '~/modules/customers/customers.style'
import { SaleCategory } from '~/schema'
import {
  FilterFormWrap,
  ReportFilter,
} from './../../reports.style'

const FormItem = Form.Item

interface SalesProductReportProps extends FormComponentProps {
  onRun: Function
  filter: any
  vertical: boolean
  items: any[]
  customers: any[]
  categories: any[]
}

interface SalesProductReportPropsState {
  dateType: number
  selectedClientId: number
  productSKUs: any[]
  productNames: any[]
  accountNames: any[]
}

class SalesByProductOrderReportFilter extends React.PureComponent<SalesProductReportProps, SalesProductReportPropsState> {
  constructor(props: SalesProductReportProps) {
    super(props);
    this.state = {
      dateType: this.props.filter?.dateType,
      selectedClientId: this.props.filter?.wholesaleClientId,
      productSKUs: [],
      productNames: [],
      accountNames: []
    }
  }

  onProductSearch = (searchStr: string, type: string) => {
    const { items } = this.props
    let options: any[] = []
    if (items && items.length > 0 && searchStr) {
      items.forEach((row) => {
        if (row[type] && row[type].toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          options.push({
            value: row[type],
            text: row[type],
          })
        }
      })
    }

    if (type === 'SKU') {
      this.setState({
        productSKUs: options,
      })
    } else {
      this.setState({
        productNames: options,
      })
    }
  }

  onDateChange = (dateType: number) => {
    this.setState({ dateType })
  }

  onSearch = (searchStr: string) => {
    const { customers } = this.props
    let accountNames: any[] = []
    if (customers && customers.length > 0 && searchStr) {
      customers.forEach((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          accountNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      accountNames: accountNames,
    })
  }

  onSelect = (clientId: any) => {
    this.setState({ selectedClientId: clientId })
  }

  onRunReports = (evt: any) => { 
    const { form } = this.props
    evt.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let v = { ...values }
        v.type = 40 // define report type: 40
        const fromTo: any = getFromAndToByDateType(v.dateType)
        if(v.clientId) {
          v.clientId = this.state.selectedClientId
        }
        if(v.dateType == 1) {
          delete v.from
          delete v.to
        } else if (v.dateType != 6 && fromTo.from && fromTo.to) {
          v = { ...v, ...fromTo }
        } else if (v.dateRange) {
          v = {
            ...v,
            from: moment(v.dateRange[0]).format('MM/DD/YYYY'),
            to: moment(v.dateRange[1]).format('MM/DD/YYYY'),
          }
        }
        delete v.dateRange
        
        this.props.onRun(v)
      }
    })
  }
  

  render() {
    const { form: { getFieldDecorator }, filter, vertical, categories, customers } = this.props
    const { dateType, productSKUs, productNames, accountNames } = this.state

    return (
      <ReportFilter style={{ width: vertical ? 250 : '100%' }}>
        <Form onSubmit={this.onRunReports} hideRequiredMark={true} style={{ maxWidth: 'initial' }} colon={false}>
          <FilterFormWrap style={{ display: vertical === true ? 'block' : 'flex', flexWrap: 'wrap' }}>
            <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label="Product Categories">
              {getFieldDecorator('wholesaleCategoryId', {
                rules: [{ required: false }],
                initialValue: filter?.wholesaleCategoryId ? filter.wholesaleCategoryId : 0,
              })(
                <ThemeSelect style={{ minWidth: 150 }}>
                  <Select.Option value={0}>All</Select.Option>
                  {categories.map((c: SaleCategory, _index: number) => {
                    return (
                      <Select.Option key={c.wholesaleCategoryId} value={c.wholesaleCategoryId}>
                        {c.name}
                      </Select.Option>
                    )
                  })}
                </ThemeSelect>,
              )}
            </FormItem>
            <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label="Sale Dates">
              {getFieldDecorator('dateType', {
                rules: [{ required: false }],
                initialValue: filter?.dateType ? filter.dateType : 1,
              })(<ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateChange}>
                <Select.Option value={1}>All</Select.Option>
                <Select.Option value={2}>Today</Select.Option>
                <Select.Option value={3}>Tomorrow</Select.Option>
                <Select.Option value={4}>Yesterday</Select.Option>
                <Select.Option value={5}>Past Week</Select.Option>
                <Select.Option value={6}>Custom Date Range</Select.Option>
              </ThemeSelect>)}
            </FormItem>
            {dateType === 6 && (
              <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label={vertical ? '' : <>&nbsp;</>}>
                {getFieldDecorator('dateRange', {
                  rules: [{ required: false }],
                  initialValue: filter ? [moment(filter.from), moment(filter.to)] : [,]
                })(<DatePicker.RangePicker
                  placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                  format={'MM/DD/YYYY'}
                  className="report-dp" />)}
              </FormItem>
            )}
            <FormItem label="PAS (Price-After-Sale)" className={`pas ${vertical === true ? '' : 'horizontal mh20 mt10'}`}>
              {getFieldDecorator('priceAfterSale', {
                rules: [{ required: false }],
                initialValue: !!filter.priceAfterSale,
                valuePropName: 'checked',
              })(<ThemeCheckbox />)}
            </FormItem>
            <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label="Product SKU">
              {getFieldDecorator('sku', {
                initialValue: filter?.sku,
                rules: [{ required: false }],
              })(
                <AutoComplete
                  placeholder="Product SKU"
                  dataSource={productSKUs}
                  onSearch={(v) => this.onProductSearch(v, 'SKU')}
                />,
              )}
            </FormItem>
            <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label="Product Name">
              {getFieldDecorator('wholesaleItemName', {
                rules: [{ required: false }],
                initialValue: filter?.wholesaleItemName,
              })(
                <AutoComplete
                  placeholder="Product Name"
                  dataSource={productNames}
                  onSearch={(v) => this.onProductSearch(v, 'variety')}
                />,
              )}
            </FormItem>                   
            <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label="Customer Account">
              {getFieldDecorator('clientId', {
                initialValue: customers.find(el=> el.clientId == filter?.wholesaleClientId)?.clientCompany?.companyName,
                rules: [{ required: false }],
              })(
                <AutoComplete
                  placeholder="Customer Account"
                  dataSource={accountNames}
                  onSearch={this.onSearch}
                  onSelect={this.onSelect}
                />,
              )}
            </FormItem>
            <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label="Sales Order">
                {getFieldDecorator('customOrderNumber', {
                  rules: [{ required: false }],
                  initialValue: filter?.customOrderNumber,
                })(<ThemeInput />)}
            </FormItem>
          </FilterFormWrap>

          <ThemeButton 
            type="primary" 
            htmlType="submit" 
            style={{ 
              marginLeft: vertical ? 0 : 20, 
              marginTop: vertical ? 0 : 20,
              height: 42, 
            }}
              >
            <Icon
              viewBox="0 0 24 24"
              width="24"
              height="24"
              type="run"
              style={{ fill: 'transparent', float: 'left' }}
            />
            <div
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                lineHeight: '21px',
                fontFamily: 'Museo Sans Rounded',
                display: 'block',
                float: 'left',
                marginTop: 2,
                marginLeft: 3,
              }}
            >
              Run Report
            </div>
          </ThemeButton>
        </Form>
      </ReportFilter>
    )
  }
}

export default Form.create<SalesProductReportProps>()(SalesByProductOrderReportFilter)
