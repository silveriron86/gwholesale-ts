import React from 'react'
import { Row, Col } from 'antd'
import {
  ReportContent,
  ReportTitle,
  SalesReportBody,
} from '../../reports.style'
import SalesProductOrderFilter from './sales-product-order-filter'

interface SalesProductReportProps {
  onRun: Function
  filter: any
  vertical: boolean
  items: any[]
  customers: any[]
  categories: any[]
}

export class SalesByProductOrderReportFormVertical extends React.PureComponent<SalesProductReportProps> {
  constructor(props: SalesProductReportProps) {
    super(props);
  }
  render() {
    return (
      <SalesReportBody>
        <ReportContent>
          <ReportTitle>
            <span className="bold">Sales By Product/Order</span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              <SalesProductOrderFilter 
                {...this.props}
              />
            </Col>
          </Row>
        </ReportContent>
      </SalesReportBody>
    )
  }
}

export default SalesByProductOrderReportFormVertical
