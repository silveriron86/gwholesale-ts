import React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import SalesProductOrderReportContent from './sales-product-order-content'
import SalesByProductOrderReportFilter from './sales-product-order-filter'
import { Collapse } from 'antd'
import { ReportContent, ReportTitle, SalesReportBody } from '../../reports.style'

type SalesProductOrderReportProps = RouteComponentProps & {
  onRun: Function
  filter: any
  reportName: string
  getGrossProfit: Function
  getMarkup: Function
  getGrossMargin: Function
  getFilterOption: Function
  useCustomIcon: Function
  handleReportNameVisibleChange: Function
}

export class SalesProductOrderReport extends React.PureComponent<SalesProductOrderReportProps> {
  filterRef = React.createRef<any>()

  onSaveCustomReport = () => {
    const { filter } = this.props
    this.filterRef.current
      .validateFields()
      .then((values: any) => {
        const { reportName } = this.props
        if (!reportName) {
          return
        }
        this.setState(
          {
            visiblePopover: false,
          },
          () => {
            const clientId = this.props.customers.find(el=>el.clientCompany.companyName === values.clientId)?.clientId;
            this.props.saveCustomReportWithName({
              reportName,
              filter: {
                ...values,
                clientId,
                type: filter.type,
              },
            })
            this.props.handleReportNameVisibleChange(false)
          },
        )
      })
      .catch((errorInfo: any) => {
      })
  }

  render () {
    const { filter } = this.props
    return (
      <>
        <SalesReportBody>
          <ReportContent>
            <ReportTitle>
              <span className="bold">Sales By Product/Order</span>
            </ReportTitle>
            <Collapse defaultActiveKey={['1']}>
              <Collapse.Panel header="Hide Filters" key="1">
                <SalesByProductOrderReportFilter
                  ref={this.filterRef}
                  onRun={this.props.onRun}
                  filter={filter}
                  vertical={false}
                  customers={this.props.customers}
                  items={this.props.items}
                  categories={this.props.categories}
                />
              </Collapse.Panel>
            </Collapse>
          </ReportContent>
        </SalesReportBody>
        <SalesProductOrderReportContent
          filter={filter}
          salesProductOrderReport={this.props.salesProductOrderReport}
          salesProductReport={this.props.salesProductReport}
          getSalesProductOrderProductReportData={this.props.getSalesProductOrderProductReportData}
          getSalesProductOrderReportData={this.props.getSalesProductOrderReportData}
          useCustomIcon={this.props.useCustomIcon}
          getGrossProfit={this.props.getGrossProfit}
          getMarkup={this.props.getMarkup}
          getGrossMargin={this.props.getGrossMargin}
          getFilterOption={this.props.getFilterOption}
          sellerSetting={this.props.sellerSetting}
        />
      </>
    )
  }
}

export default SalesProductOrderReport
