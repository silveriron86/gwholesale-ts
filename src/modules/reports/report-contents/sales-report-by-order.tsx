import { Col, Row } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import moment from 'moment'
import React from 'react'
import { TempCustomer, SaleItem, SaleCategory } from '~/schema'
import FiltersForm from '../components/report-detail/filters-form'

import {
  ReportContent,
  ReportFilter,
  ReportTitle,
  SalesReportBody,
} from '../reports.style'

interface FormFields {
  fulfillmentDate: any
  accountName: string
  accountNumber: number
  salesOrder: number
  salesRepresentative: number
  sku: string
  variety: string
  onRun: Function
  sellers: any[]
  filter: any
}

type SalesReportProps = {
  reportsHistory: any[]
  category: number
  title: any
  customers: TempCustomer[]
  vendors: any[]
  items: SaleItem[]
  categories: SaleCategory[]
  wholesaleOrderId: string
  filter: any
  onRun: Function
  sellers: any[]
}

export class SalesReportByOrder extends React.PureComponent<FormComponentProps<FormFields> & SalesReportProps> {
  state = {
    searchMenuItem: '',
    selectedMenuItem: '',
    fromReportDate: moment().subtract(30, 'days'),
    toReportDate: moment(),
  }

  render() {
    const {
      filter,
      category,
      title,
      customers,
      vendors,
      items,
      categories,
    } = this.props
    return (
      <SalesReportBody>
        <ReportContent>
          <ReportTitle>
            <span className="bold">{title}&nbsp;</span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              <ReportFilter style={{ width: 250 }}>
                <FiltersForm
                  vertical={true}
                  title={title}
                  onRun={this.props.onRun}
                  defaultFilter={filter}
                  sellers={this.props.sellers}
                  customers={customers}
                  vendors={vendors}
                  categories={categories}
                  items={items}
                />
              </ReportFilter>
            </Col>
          </Row>
        </ReportContent>
      </SalesReportBody>
    )
  }
}
