import { Col, Form, Row } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import moment from 'moment'
import React from 'react'
import Moment from 'react-moment'
import { Link } from 'react-router-dom'
import { Icon } from '~/components'
import { TempCustomer, SaleItem, SaleCategory } from '~/schema'
import FiltersForm from '../components/report-detail/filters-form'

import {
  ReportContent,
  ReportFilter,
  ReportTable,
  ReportTitle,
  RunHistoryItem,
  SalesReportBody,
} from '../reports.style'

interface FormFields {
  fulfillmentDate: any
  accountName: string
  accountNumber: number
  salesOrder: number
  salesRepresentative: number
  sku: string
  variety: string
  onRun: Function
  sellers: any[]
  filter: any
}

type SalesReportProps = {
  reportsHistory: any[]
  category: number
  item: any
  customers: TempCustomer[]
  vendors: any[]
  items: SaleItem[]
  categories: SaleCategory[]
}

export class SalesReport extends React.PureComponent<FormComponentProps<FormFields> & SalesReportProps> {
  state = {
    searchMenuItem: '',
    selectedMenuItem: '',
    fromReportDate: moment().subtract(30, 'days'),
    toReportDate: moment(),
  }

  renderRunHistory = () => {
    const { reportsHistory } = this.props
    let result: any[] = []
    reportsHistory.forEach((el) => {
      result.push(
        <Link to={`/reports/${el.id}`}>
          <RunHistoryItem>
            <span className="date">
              <Moment format="MM/DD/YYYY" date={moment.utc(el.createdAt)} />
            </span>
            &nbsp;by {el.createdBy.firstName} {el.createdBy.lastName}
            <Icon type="arrow-right-product" viewBox="0 0 10 10" width={18} height={12} />
          </RunHistoryItem>
        </Link>,
      )
    })
    return result
  }

  render() {
    const {
      form: { getFieldDecorator },
      filter,
      category,
      item,
      customers,
      vendors,
      items,
      categories,
    } = this.props

    let title = ''
    switch (category) {
      case 0:
        title = 'Custom'
        break
      case 2:
        title = item
        break
    }
    return (
      <SalesReportBody>
        <ReportContent>
          <ReportTitle>
            <span className="bold">{title === 'Profitability' ? 'Product Sales & Profitability' : title}&nbsp;</span>
            {/* <span>Report</span> */}
          </ReportTitle>
          <Row>
            <Col span={24}>
              <ReportTable>
                {title === 'Profitability'
                  ? 'View margins by categories, vendor, products, and lots'
                  : 'Select the report dates and sales representative(s). Optionally filter by customer account and product.'}
              </ReportTable>
              <ReportFilter style={{ width: 250 }}>
                <FiltersForm
                  vertical={true}
                  title={item.type === 2 ? 'Profitability' : title}
                  onRun={this.props.onRun}
                  defaultFilter={filter}
                  sellers={this.props.sellers}
                  customers={customers}
                  vendors={vendors}
                  categories={categories}
                  items={items}
                />
              </ReportFilter>
            </Col>
            {/* <Col span={6} offset={2}>
              <SubLabelSpan style={{ marginTop: -5 }}>LAST RUN REPORTS</SubLabelSpan>
              {this.renderRunHistory()}
            </Col> */}
          </Row>
        </ReportContent>
      </SalesReportBody>
    )
  }
}

export default Form.create<FormComponentProps<FormFields> & SalesReportProps>()(SalesReport)
