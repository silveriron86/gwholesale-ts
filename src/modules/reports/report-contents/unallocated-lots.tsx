import { Col, Row, Icon, Select, DatePicker, Collapse, Button } from 'antd'
import moment from 'moment'
import React from 'react'
import { Link } from 'react-router-dom'
import { ClassNames } from '@emotion/core'
import { formatNumber, formatOrderStatus, getFromAndToByDateType, getOrderPrefix } from '~/common/utils'
import { Icon as IconSVG } from '~/components'
import {
  Flex,
  ThemeButton,
  ThemeCheckbox,
  ThemeModal,
  ThemeSelect,
  ThemeTable,
} from '~/modules/customers/customers.style'
import { DialogSubContainer, noPaddingFooter } from '~/modules/customers/nav-sales/styles'
import AllocateLotsModal from '../components/modals/allocate-lots-modal'
import { ReportContent, ReportFilter, ReportTable, ReportTitle, SalesReportBody } from '../reports.style'

type UnallocatedLotsProps = {
  title: any
  filter: any
  reportsHistory: any[]
  onRun: Function
  dataSource?: any[]
  sellers: any[]
  sellerSetting?: any
  loading?: boolean
  reportName?: string
  isVertical: boolean
  saveCustomReportWithName?: Function
  handleReportNameVisibleChange?: Function
  getAllocateLots: Function
  allocateLotsData: any[]
  allocateLots: Function
  setLoading: Function
}

export class UnallocatedLots extends React.PureComponent<UnallocatedLotsProps> {
  formRef = React.createRef<any>()
  allocateRef = React.createRef<any>()

  state = {
    currentPage: 1,
    pageSize: 10,
    expandedRowKeys: [],
    dateType: 3,
    from: null,
    to: null,
    dataSource: [],
    sellerId: 0,
    displayNotReceived: false,
    visibleAllocateLotsModal: false,
  }

  componentDidMount() {
    const { filter, dataSource } = this.props
    const data = dataSource
      ? dataSource.map((el) => {
          return { ...el, wholesaleItemId: `parent-${el.wholesaleItemId}` }
        })
      : []
    this.setState(
      {
        dataSource: data,
        dateType: filter && filter.dateType ? filter.dateType : 3,
        from: filter.dateType != 1 && filter.from ? moment(filter.from).format('MM/DD/YYYY') : null,
        to: filter.dateType != 1 && filter.to ? moment(filter.to).format('MM/DD/YYYY') : null,
        sellerId: filter.sellerId ? filter.sellerId : 0,
        displayNotReceived: filter.displayNotReceived,
        visibleAllocateLotsModal: false,
      },
      () => {
        this.onClickRunReport(true)
      },
    )
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.dataSource) != JSON.stringify(nextProps.dataSource)) {
      const data = nextProps.dataSource
        ? nextProps.dataSource.map((el) => {
            return { ...el, wholesaleItemId: `parent-${el.wholesaleItemId}` }
          })
        : []
      this.setState({
        dataSource: data,
      })
    }
  }

  columns: any[] = [
    {
      title: 'Item',
      dataIndex: 'companyName',
      align: 'left',
      width: '25%',
      sorter: (a: any, b: any) => {
        return a.variety && b.variety ? a.variety.localeCompare(b.variety) : false
      },
      render: (variety: string, record: any) => {
        return record.children && record.children.length ? (
          <span>{variety}</span>
        ) : (
          <div style={{ paddingLeft: 26 }}>{variety}</div>
        )
      },
    },
    {
      title: 'SKU',
      dataIndex: 'parentRowSku',
      align: 'left',
      render: (parentRowSku: string, record: any) => {
        if (parentRowSku) {
          const { wholesaleItemId } = record
          return <Link to={`/product/${wholesaleItemId.replace('parent-', '')}/specifications`}>{parentRowSku}</Link>
        }
        return ''
      },
      sorter: (a: any, b: any) => {
        return a.parentRowSku && b.parentRowSku ? a.parentRowSku.localeCompare(b.parentRowSku) : false
      },
    },
    {
      title: 'SO #',
      dataIndex: 'wholesaleOrderId',
      align: 'left',
      render: (wholesaleOrderId: number, record: any) => {
        if (!wholesaleOrderId) return

        const orderPrefix = getOrderPrefix(this.props.sellerSetting, 'sales')
        return (
          <Link to={`/sales-order/${wholesaleOrderId}`}>
            {orderPrefix ? `${orderPrefix}` : ''}
            {wholesaleOrderId}
          </Link>
        )
      },
    },
    {
      title: 'Order Status',
      dataIndex: 'status',
      align: 'left',
      render: (status: string, record: any) => {
        return formatOrderStatus(status, this.props.sellerSetting)
      },
    },
    {
      title: 'Sales Rep',
      dataIndex: 'salesRep',
      align: 'left',
    },
    {
      title: 'Lot',
      dataIndex: 'lotId',
      align: 'left',
      render: (value: any, record: any) => {
        if (record && record.children == undefined) {
          return value
        }
      },
    },
    {
      title: 'Lot Status',
      dataIndex: 'lotStatus',
      align: 'left',
    },
    {
      title: 'Lot Avail to Sell',
      dataIndex: 'lotAvailableQty',
      align: 'left',
      render: (value: any, record: any) => {
        if (record && record.children == undefined) {
          return value ? parseFloat(value).toFixed(2) : ''
        }
      },
    },
    {
      title: 'Order Qty',
      dataIndex: 'quantity',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.quantity - b.quantity
      },
      render: (quantity: number, record: any) => {
        return quantity ? `${formatNumber(quantity, 2)} ${record.UOM}` : ''
      },
    },
    {
      title: 'Picked Qty',
      dataIndex: 'picked',
      align: 'left',
      sorter: (a: any, b: any) => {
        return a.picked - b.picked
      },
      render: (picked: number, record: any) => {
        if (record && record.children == undefined) {
          return picked ? `${formatNumber(picked, 2)} ${record.UOM}` : ''
        } else {
          const pickeds = record.children.map((item) => (item.picked ? item.picked : 0))
          const pickTotal = pickeds.reduce((acr, cur) => acr + cur)
          return pickTotal ? `${formatNumber(pickTotal, 2)} ${record.UOM}` : ''
        }
      },
    },
  ]

  onSaveCustomReport = () => {
    const { filter } = this.props
    this.formRef.current.validateFields().then((values) => {
      const { reportName } = this.props
      if (!reportName) {
        return
      }
      this.setState(
        {
          visiblePopover: false,
        },
        () => {
          this.props.saveCustomReportWithName({
            reportName,
            filter: {
              ...values,
              ...{ type: filter.type },
            },
          })
          this.props.handleReportNameVisibleChange(false)
        },
      )
    })
  }

  onClickRunReport = (isAllocationLots: boolean = false) => {
    const data = this.getReportFilter()
    if (!isAllocationLots) {
      this.props.onRun(data)
    }
    this.props.getAllocateLots(data)
  }

  getReportFilter = () => {
    const { from, to, dateType, sellerId, displayNotReceived } = this.state
    let data: any = {
      type: 30,
      dateType,
      sellerId,
      displayNotReceived,
    }

    const fromTo: any = getFromAndToByDateType(this.state.dateType)
    if (dateType == 1) {
      delete data.from
      delete data.to
    } else if (dateType != 6 && fromTo.from && fromTo.to) {
      data = { ...data, ...fromTo }
    } else if (from && to) {
      data = { ...data, from, to }
    }
    return data
  }

  onPageChange = (page: any, pageSize: number | undefined) => {
    this.setState({ currentPage: page })
  }

  expandColumn = (props: any) => {
    this.setState({ isExpandAll: false })
    return props.record.children && props.record.children.length ? (
      <Icon
        className="icon"
        type={props.expanded ? 'caret-down' : 'caret-right'}
        style={{ marginLeft: -3, cursor: 'pointer' }}
        onClick={() => {
          window.event.preventDefault()
          props.onExpand()
        }}
      />
    ) : (
      ''
    )
  }

  onClickExpand = () => {
    const { dataSource } = this.state
    let allKeys: string[] = []
    if (dataSource) {
      dataSource.forEach((el: any) => {
        if (el.children && el.children.length > 0) {
          allKeys.push(el.wholesaleItemId)
        }
      })
    }

    if (this.state.expandedRowKeys.length) {
      this.setState({ expandedRowKeys: [] })
    } else {
      this.setState({ expandedRowKeys: allKeys })
    }
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    this.setState({
      expandedRowKeys,
    })
  }

  onDateTypeChange = (dateType: number) => {
    this.setState({ dateType })
  }

  onSellerChange = (sellerId: number) => {
    this.setState({ sellerId })
  }

  onDisplayNotReceivedChange = (evt: any) => {
    this.setState({ displayNotReceived: evt.target.checked })
  }

  onDateRangeChange = (dates: any, dateStrings: any) => {
    this.setState({
      from: moment(dateStrings[0]).format('MM/DD/YYYY'),
      to: moment(dateStrings[1]).format('MM/DD/YYYY'),
    })
  }

  renderFilterForm = () => {
    const { filter, sellers, isVertical } = this.props
    const { dateType, from, to, sellerId, displayNotReceived } = this.state
    return (
      <ReportFilter>
        <div style={{ display: !isVertical ? 'flex' : 'block' }}>
          <div>
            <div style={{ marginBottom: 10 }}>Fulfillment Date:</div>
            <div style={{ display: !isVertical ? 'flex' : 'block' }}>
              <ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateTypeChange} value={dateType}>
                <Select.Option value={1}>All</Select.Option>
                <Select.Option value={2}>Today</Select.Option>
                <Select.Option value={3}>Tomorrow</Select.Option>
                <Select.Option value={4}>Yesterday</Select.Option>
                <Select.Option value={5}>Past Week</Select.Option>
                <Select.Option value={6}>Custom Date Range</Select.Option>
              </ThemeSelect>
              {dateType == 6 && (
                <div style={{ margin: !isVertical ? '0 0 0 20px' : '20px 0 0 0' }}>
                  <DatePicker.RangePicker
                    placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                    format={'MM/DD/YYYY'}
                    onChange={this.onDateRangeChange}
                    value={from && to ? [moment(from), moment(to)] : [,]}
                    className="report-dp"
                  />
                </div>
              )}
            </div>
          </div>

          <div style={{ margin: !isVertical ? '0 0 0 20px' : '20px 0 0 0' }}>
            <div style={{ marginBottom: 10 }}>Sales Representative:</div>
            <ThemeSelect style={{ minWidth: 150 }} value={sellerId} onChange={this.onSellerChange}>
              <Select.Option value={0}>All</Select.Option>
              {sellers.map((seller) => {
                return <Select.Option value={seller.userId}>{`${seller.firstName} ${seller.lastName}`}</Select.Option>
              })}
            </ThemeSelect>
          </div>
          <div style={{ margin: !isVertical ? '35px 0 0 20px' : '30px 0 0 0', display: 'flex' }}>
            <ThemeCheckbox
              checked={displayNotReceived}
              onChange={this.onDisplayNotReceivedChange}
              style={{ marginRight: 8 }}
            />
            <div style={{ marginBottom: 10 }}>Display items from lots not yet received</div>
          </div>
        </div>
        <div style={{ marginTop: 30 }}>
          <ThemeButton type="primary" onClick={() => this.onClickRunReport(false)} style={{ height: 42 }}>
            <IconSVG
              viewBox="0 0 24 24"
              width="24"
              height="24"
              type="run"
              style={{ fill: 'transparent', float: 'left' }}
            />
            <div
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                lineHeight: '21px',
                fontFamily: 'Museo Sans Rounded',
                display: 'block',
                float: 'left',
                marginTop: 2,
                marginLeft: 3,
              }}
            >
              Run Report
            </div>
          </ThemeButton>
        </div>
      </ReportFilter>
    )
  }

  onClickAllocateLots = () => {
    const data = this.getReportFilter()
    this.allocateRef.current.onClickAllocate(data)
    this.setState({ visibleAllocateLotsModal: false })
    this.props.setLoading(true)
  }

  render() {
    const { filter, loading } = this.props
    const { dataSource, visibleAllocateLotsModal } = this.state
    return (
      <>
        <SalesReportBody style={{ paddingLeft: 60 }} className="allocation-issues">
          <ReportContent>
            <ReportTitle>
              <span className="bold">Lot Errors By Product </span>
              <span className="bold" style={{ color: '#C4C4C4' }}>
                Report
              </span>
            </ReportTitle>
            <Row>
              <Col span={24}>
                <ReportTable style={{ paddingTop: 0 }}>
                  Find items that are sold without lots or sold from lots with negative inventory
                </ReportTable>

                {!filter || typeof filter.id == 'undefined' ? (
                  <>{this.renderFilterForm()} </>
                ) : (
                  <Collapse defaultActiveKey={['1']}>
                    <Collapse.Panel header="Hide Filters" key="1">
                      {this.renderFilterForm()}
                    </Collapse.Panel>
                  </Collapse>
                )}
              </Col>
            </Row>
            {filter && typeof filter.id != 'undefined' && (
              <>
                <Row style={{ marginTop: 24, padding: '0 24px' }} type="flex" justify="space-between">
                  <div>{dataSource ? dataSource.length : 0} Items</div>
                  <Flex style={{ flexDirection: 'column' }}>
                    <div>
                      <Icon
                        type="caret-down"
                        style={{ marginRight: 12, cursor: 'pointer' }}
                        onClick={this.onClickExpand}
                      />
                      {this.state.expandedRowKeys.length ? 'Collapse' : 'Expand'} All
                    </div>
                    <ThemeButton
                      style={{ marginTop: 16 }}
                      onClick={() => this.setState({ visibleAllocateLotsModal: true })}
                    >
                      Allocate lots...
                    </ThemeButton>
                  </Flex>
                </Row>
                <Row style={{ marginTop: 24 }}>
                  <ThemeTable
                    dataSource={dataSource}
                    columns={this.columns}
                    pagination={{
                      current: this.state.currentPage,
                      pageSize: this.state.pageSize,
                      onChange: this.onPageChange,
                    }}
                    loading={loading}
                    rowKey="wholesaleItemId"
                    expandIcon={this.expandColumn}
                    expandedRowKeys={this.state.expandedRowKeys}
                    onExpandedRowsChange={this.onExpendedRowsChange}
                  />
                </Row>
              </>
            )}
          </ReportContent>
        </SalesReportBody>
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              title={`Allocate lots`}
              visible={visibleAllocateLotsModal}
              onCancel={() => this.setState({ visibleAllocateLotsModal: false })}
              okText={'Allocate lots'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              width={'85%'}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={this.onClickAllocateLots}>
                    Allocate
                  </ThemeButton>
                  <Button onClick={() => this.setState({ visibleAllocateLotsModal: false })}>CANCEL</Button>
                </DialogSubContainer>
              }
            >
              <AllocateLotsModal
                allocateLotsData={this.props.allocateLotsData}
                visible={visibleAllocateLotsModal}
                allocateLots={this.props.allocateLots}
                sellerSetting={this.props.sellerSetting}
                ref={this.allocateRef}
              />
            </ThemeModal>
          )}
        </ClassNames>
      </>
    )
  }
}
