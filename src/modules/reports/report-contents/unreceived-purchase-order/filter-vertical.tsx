import React from 'react'
import { Row, Col } from 'antd'
import {
  ReportContent,
  ReportTitle,
  SalesReportBody,
} from '../../reports.style'
import UnreceivedPurchaseOrderFilter from './filter'

interface UnreceivedPurchaseOrderVerticalProps {
  onRun: Function
  filter: any
  vertical: boolean
}

export class UnreceivedPurchaseOrderVertical extends React.PureComponent<UnreceivedPurchaseOrderVerticalProps> {
  constructor(props: UnreceivedPurchaseOrderVerticalProps) {
    super(props);
  }
  render() {
    return (
      <SalesReportBody>
        <ReportContent>
          <ReportTitle>
            <span className="bold">Unreceived Purchase Order</span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              <UnreceivedPurchaseOrderFilter 
                {...this.props}
              />
            </Col>
          </Row>
        </ReportContent>
      </SalesReportBody>
    )
  }
}

export default UnreceivedPurchaseOrderVertical
