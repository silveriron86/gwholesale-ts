import React from 'react'
import { Row, Col } from 'antd'
import {
  ReportContent,
  ReportTitle,
  SalesReportBody,
} from '../../reports.style'
import UnshippedSalesOrderFilter from './filter'

interface UnshippedSalesOrderVerticalProps {
  onRun: Function
  filter: any
  vertical: boolean
}

export class UnshippedSalesOrderVertical extends React.PureComponent<UnshippedSalesOrderVerticalProps> {
  constructor(props: UnshippedSalesOrderVerticalProps) {
    super(props);
  }
  render() {
    return (
      <SalesReportBody>
        <ReportContent>
          <ReportTitle>
            <span className="bold">Unshipped Sales Order</span>
          </ReportTitle>
          <Row>
            <Col span={24}>
              <UnshippedSalesOrderFilter 
                {...this.props}
              />
            </Col>
          </Row>
        </ReportContent>
      </SalesReportBody>
    )
  }
}

export default UnshippedSalesOrderVertical
