import { DatePicker, Form, Select } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import moment from 'moment'
import React from 'react'
import { getFromAndToByDateType } from '~/common/utils'
import { Icon } from '~/components'
import { ThemeButton, ThemeSelect } from '~/modules/customers/customers.style'
import {
  FilterFormWrap,
  ReportFilter,
} from './../../reports.style'

const FormItem = Form.Item

interface UnshippedSalesOrderFilterProps extends FormComponentProps {
  onRun: (filter: any)=> void
  filter: any
  vertical: boolean  
}

interface UnshippedSalesOrderFilterState {
  dateType: number

}

class UnshippedSalesOrderFilter extends React.PureComponent<UnshippedSalesOrderFilterProps, UnshippedSalesOrderFilterState> {
  constructor(props: UnshippedSalesOrderFilterProps) {
    super(props);
    this.state = {
      dateType: this.props.filter?.dateType,
    }
  }

  onRunReports = (evt: any) => { 
    const { form } = this.props
    evt.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let v = { ...values }
        v.type = 55 // define report type: 55
        const fromTo: any = getFromAndToByDateType(v.dateType)
       
        if(v.dateType == 1) {
          delete v.from
          delete v.to
        } else if (v.dateType != 6 && fromTo.from && fromTo.to) {
          v = { ...v, ...fromTo }
        } else if (v.dateRange) {
          v = {
            ...v,
            from: moment(v.dateRange[0]).format('MM/DD/YYYY'),
            to: moment(v.dateRange[1]).format('MM/DD/YYYY'),
          }
        }
        delete v.dateRange
        
        this.props.onRun(v)
      }
    })
  }
  
  onDateChange = (dateType: number) => {
    this.setState({ dateType })
  }

  render() {
    const { form: { getFieldDecorator }, filter, vertical } = this.props
    const { dateType } = this.state
    return (
      <ReportFilter style={{ width: vertical ? 250 : '100%' }}>
        <Form onSubmit={this.onRunReports} hideRequiredMark={true} style={{ maxWidth: 'initial' }} colon={false}>
          <FilterFormWrap style={{ display: vertical === true ? 'block' : 'flex', flexWrap: 'wrap' }}>
          <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label="Fulfillment Date">
              {getFieldDecorator('dateType', {
                rules: [{ required: false }],
                initialValue: filter?.dateType ? filter.dateType : 1,
              })(<ThemeSelect style={{ minWidth: 150 }} onChange={this.onDateChange}>
                <Select.Option value={1}>All</Select.Option>
                <Select.Option value={2}>Today</Select.Option>
                <Select.Option value={3}>Tomorrow</Select.Option>
                <Select.Option value={4}>Yesterday</Select.Option>
                <Select.Option value={5}>Past Week</Select.Option>
                <Select.Option value={6}>Custom Date Range</Select.Option>
              </ThemeSelect>)}
            </FormItem>
            {dateType === 6 && (
              <FormItem className={vertical ? '' : 'mh20 lineHeight15'} label={vertical ? '' : <>&nbsp;</>}>
                {getFieldDecorator('dateRange', {
                  rules: [{ required: false }],
                  initialValue: filter ? [moment(filter.from), moment(filter.to)] : [,]
                })(<DatePicker.RangePicker
                  placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                  format={'MM/DD/YYYY'}
                  className="report-dp" />)}
              </FormItem>
            )}
          </FilterFormWrap>

          <ThemeButton 
            type="primary" 
            htmlType="submit" 
            style={{ 
              marginLeft: vertical ? 0 : 20, 
              marginTop: vertical ? 0 : 20,
              height: 42, 
            }}
              >
            <Icon
              viewBox="0 0 24 24"
              width="24"
              height="24"
              type="run"
              style={{ fill: 'transparent', float: 'left' }}
            />
            <div
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                lineHeight: '21px',
                fontFamily: 'Museo Sans Rounded',
                display: 'block',
                float: 'left',
                marginTop: 2,
                marginLeft: 3,
              }}
            >
              Run Report
            </div>
          </ThemeButton>
        </Form>
      </ReportFilter>
    )
  }
}

export default Form.create<UnshippedSalesOrderFilterProps>()(UnshippedSalesOrderFilter)
