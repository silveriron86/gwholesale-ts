import React from 'react'
import { DetailWrapper, FlexWrap, InsightBlock } from '../../reports.style'
import { Collapse, Icon, Row} from 'antd'
import { formatNumber } from '~/common/utils'
import { Link } from 'react-router-dom'
import { ThemeTable } from '~/modules/customers/customers.style'
import moment from 'moment'
import { cloneDeep } from 'lodash'
const { Panel } = Collapse

interface UnshippedSalesOrderReportContentProps {
  filter: any
  sellerSetting: any
  exceptionOrderReportData: any[]
  useCustomIcon: Function
}

export default class UnshippedSalesOrderReportContent extends React.PureComponent<UnshippedSalesOrderReportContentProps> {
  multiSortTable = React.createRef<any>()
  state = {
    expandedRowKeys: []
  }

  onClickExpand = () => {
    const { exceptionOrderReportData } = this.props
    const allKeys: number[] = []
    exceptionOrderReportData.forEach(el => {
      if (el.children?.length) {
        allKeys.push(el.wholesaleOrderId)
      }
    });
    if (this.state.expandedRowKeys.length) {
      this.setState({ expandedRowKeys: [] })
    } else {
      this.setState({ expandedRowKeys: allKeys })
    }
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    if (this.state.expandedRowKeys.length > 0) {
      const row = []
      for (const value of expandedRowKeys) if (value !== this.state.expandedRowKeys[0]) row.push(value)
      this.setState({
        expandedRowKeys: row,
      })
    } else {
      this.setState({
        expandedRowKeys: expandedRowKeys,
      })
    }
  }

  makeChildColumns = (column: any[]) => {
    const cloneCols = cloneDeep(column)
    const customCols = cloneCols.filter(el=>el.key != 'companyName' && el.key != 'wholesaleOrderId').map(el=> {
      if(el.key == 'deliveryDate') {
        return {
          ...el,
          onCell: () => ({colSpan: 3})
        }
      } 
      return el
    })
    return customCols
  }

  expandColumn = (props: any) => {
    this.setState({ isExpandAll: false })
    return (
      props.record.children && props.record.children.length ? (
        <Icon
          className="icon"
          type={props.expanded ? 'caret-down' : 'caret-right'}
          style={{ marginLeft: -3, cursor: 'pointer' }}
          onClick={() => {
            window.event.preventDefault()
            props.onExpand()
          }}
        />
      ) : (
        ''
      )
    )
  }

  render () {
    const { exceptionOrderReportData } = this.props
    const dataSource = exceptionOrderReportData.map((el)=> {
      return {
        ...el,
        children: el.children?.map((child: any)=> ({...child, wholesaleOrderId: `${el.wholesaleOrderId}-${child.wholesaleOrderItemId}`}))  
      }
    })
    const columns = [
      {
        title: 'Fulfillment Date',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        align: 'left',
        width: 350,
        sorter: (a: any, b: any) => moment(a.deliveryDate).unix() - moment(b.deliveryDate).unix(),
        render: (deliveryDate: string, record: any) => {
          
          return !record.lotId ? moment(deliveryDate).format('MM/DD/YYYY') : record.variety
        }
      },
      {
        title: 'Order #',
        dataIndex: 'wholesaleOrderId',
        key: 'wholesaleOrderId',
        align: 'left',
        sorter: (a: any, b: any) => a.wholesaleOrderId - b.wholesaleOrderId,
        render: (wholesaleOrderId: number, record: any) => {
          if(!record.isParentRow) return ''
          return <Link to={`/sales-order/${wholesaleOrderId}`}>{wholesaleOrderId}</Link>
        }
      },
      {
        title: 'Customer',
        dataIndex: 'companyName',
        key: 'companyName',
        align: 'left',
        sorter: (a: any, b: any) => a.companyName.localeCompare(b.companyName),
      },
      {
        title: 'Brand',
        dataIndex: 'modifiers',
        key: 'modifiers',
        align: 'left',
        sorter: (a: any, b: any) => a.modifiers?.localeCompare(b.modifiers),
      },
      {
        title: 'Origin',
        dataIndex: 'extraOrigin',
        key: 'extraOrigin',
        align: 'left',
        sorter: (a: any, b: any) => a.extraOrigin?.localeCompare(b.extraOrigin),
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        align: 'left',
        sorter: (a: any, b: any) => a.SKU?.localeCompare(b.SKU),
      },
      {
        title: 'Lot #',
        dataIndex: 'lotId',
        key: 'lotId',
        align: 'left',
        sorter: (a: any, b: any) => a.lotId?.localeCompare(b.lotId),
      },
      {
        title: 'Status',
        dataIndex: 'wholesaleOrderStatus',
        key: 'wholesaleOrderStatus',
        align: 'left',
        sorter: (a: any, b: any) => a.wholesaleOrderStatus?.localeCompare(b.wholesaleOrderStatus),
      },
      {
        title: 'Total Billable Qty',
        dataIndex: 'catchWeightQty',
        key: 'catchWeightQty',
        align: 'right',
        sorter: (a: any, b: any) => a.catchWeightQty - b.catchWeightQty,
        render: (catchWeightQty: number) => {
          return formatNumber(catchWeightQty, 2)
        }
      },
      {
        title: 'Order Total',
        dataIndex: 'itemTotal',
        key: 'itemTotal',
        align: 'right',
        sorter: (a: any, b: any) => a.itemTotal - b.itemTotal,
        render: (itemTotal: number) => {
          return `$${formatNumber(itemTotal, 2)}`
        }
      },
    ]

    const orderRecords = exceptionOrderReportData.filter(el=>el.wholesaleOrderId)
    const totalBillableQty = orderRecords.reduce((prev, record)=>prev + record.catchWeightQty, 0)
    const totalSales = orderRecords.reduce((prev, record)=>prev + record.itemTotal, 0)
    return (
      <>
        <DetailWrapper className="insight">
          <Collapse defaultActiveKey={['2']}>
            <Panel
              header={
                <FlexWrap className="between">
                  <div>Insights</div>
                </FlexWrap>
              }
              key="2"
            >
              <FlexWrap>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label"># OF UNSHIPPED ORDERS</div>
                  <div className="value">${exceptionOrderReportData.length}</div>
                </InsightBlock>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label">TOTAL BILLABLE QTY</div>
                  <div className="value">${formatNumber(totalBillableQty, 2)}</div>
                </InsightBlock>
                <InsightBlock style={{ paddingBottom: 16 }}>
                  <div className="label">TOTAL SALES</div>
                  <div className="value">${formatNumber(totalSales, 2)}</div>
                </InsightBlock>
              </FlexWrap>
            </Panel>
          </Collapse>
            <Row style={{ marginTop: 24, padding: '0 24px' }} type='flex' justify='end'>
              <div>
                <Icon type='caret-down' style={{ marginRight: 12, cursor: 'pointer' }} onClick={this.onClickExpand} />
                {this.state.expandedRowKeys.length ? 'Collapse' : 'Expand'} All
              </div>
            </Row>
        </DetailWrapper>

        <DetailWrapper className="last">
          <ThemeTable
            className="simple-expandable-report-table"
            ref={this.multiSortTable}
            style={{ paddingLeft: 0, paddingRight: 0 }}
            columns={columns}
            dataSource={dataSource}
            rowKey="wholesaleOrderId"
            expandIconAsCell={true}
            expandIcon={this.expandColumn}
            expandedRowKeys={this.state.expandedRowKeys}
            onExpandedRowsChange={this.onExpendedRowsChange}
          />    
        </DetailWrapper>
      </>
    )
  }
}
