import React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import UnshippedSalesOrderReportContent from './report-content'
import UnshippedSalesOrderFilter from './filter'
import { Collapse } from 'antd'
import { ReportContent, ReportTitle, SalesReportBody } from '../../reports.style'

type UnshippedSalesOrderReportProps = RouteComponentProps & {
  onRun: Function
  filter: any
  reportName: string
  sellerSetting: any
  exceptionOrderReportData: any[]
  saveCustomReportWithName: Function
  handleReportNameVisibleChange: Function
  useCustomIcon: Function
}

export class UnshippedSalesOrderReport extends React.PureComponent<UnshippedSalesOrderReportProps> {
  filterRef = React.createRef<any>()

  onSaveCustomReport = () => {
    console.log('clicked on save custom report method')
    const { filter } = this.props
    this.filterRef.current
      .validateFields()
      .then((values: any) => {
        const { reportName } = this.props
        if (!reportName) {
          return
        }
        this.setState(
          {
            visiblePopover: false,
          },
          () => {
            this.props.saveCustomReportWithName({
              reportName,
              filter: {
                ...values,
                type: filter.type,
              },
            })
            this.props.handleReportNameVisibleChange(false)
          },
        )
      })
  }

  render () {
    const { filter, exceptionOrderReportData } = this.props
    return (
      <>
        <SalesReportBody>
          <ReportContent>
            <ReportTitle>
              <div className="bold">Unshipped Sales Order <span className="bold" style={{ color: '#C4C4C4' }}>Report</span></div>
              <div style={{fontSize: 16, marginTop: 12}}>Find sales orders that have not been shipped.</div>
            </ReportTitle>
            <Collapse defaultActiveKey={['1']}>
              <Collapse.Panel header="Hide Filters" key="1">
                <UnshippedSalesOrderFilter
                  ref={this.filterRef}
                  onRun={this.props.onRun}
                  filter={filter}
                  vertical={false}               
                />
              </Collapse.Panel>
            </Collapse>
          </ReportContent>
        </SalesReportBody>
        <UnshippedSalesOrderReportContent
          filter={filter}
          sellerSetting={this.props.sellerSetting}
          exceptionOrderReportData={exceptionOrderReportData}
          useCustomIcon={this.props.useCustomIcon}
        />
      </>
    )
  }
}