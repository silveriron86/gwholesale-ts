import React from 'react'

import { connect } from 'redux-epics-decorator'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { Theme } from '~/common'
import { AuthUser, UserRole } from '~/schema'
import PageLayout from '~/components/PageLayout'
import { ReportsDispatchProps, ReportsModule, ReportsStateProps } from './reports.module'
import {
  ReportContent,
  ReportsBody,
  ReportsContentWrapper,
  ReportsMenuWrapper,
  ReportTable,
  SalesReportBody,
} from './reports.style'
import { ReportItemGroup } from './components/report-item-group'
import SearchInput from '~/components/Table/search-input'
import SalesReport from './report-contents/sales-report'
import ReceivingFoodReport from './report-contents/receiving-report'
import DriverReports from '~/modules/delivery/drivers'
import { SalesReportByOrder } from './report-contents/sales-report-by-order'
import { CostPriceDifference } from './report-contents/cost-price-difference'
import { AllocationIssue } from './report-contents/allocation-report'
import { UnallocatedLots } from './report-contents/unallocated-lots'
import LotErrorsByOrder from './report-contents/lot-error-by-order'
import { Inventory } from './report-contents/inventory'
import SalesByProductOrderReportFormVertical from './report-contents/sales-product-order/sales-product-order-form-vertical'
import { UnshippedSalesOrderVertical } from './report-contents/unshipped-sales-order'
import { UnreceivedPurchaseOrderVertical } from './report-contents/unreceived-purchase-order'

export type ReportsContainerProps = ReportsDispatchProps &
  ReportsStateProps &
  RouteComponentProps & {
    theme: Theme
    currentUser: AuthUser
  }

class ReportsContainer extends React.PureComponent<ReportsContainerProps> {
  state = {
    searchMenuItem: '',
    selectedMenuItem: [UserRole.SALES, UserRole.SELLER_RESTRICTED].indexOf(this.props.currentUser?.accountType) != -1 ? 'Allocation Issue' : 'Sales by Account/Product',
    isAfterRun: false,
    category: 2
  }

  componentDidMount() {
    this.props.getAllCustomReports()
    this.props.getReportsHistory()
    this.props.getAllSellers()
    this.props.getCustomers()
    this.props.getVendors()
    this.props.getAllItems()
    this.props.getSaleCategories()
  }

  componentWillReceiveProps(nextProps: ReportsContainerProps) {
    if (JSON.stringify(this.props.reportsHistory) !== JSON.stringify(nextProps.reportsHistory)) {
      if (this.state.isAfterRun === true && nextProps.reportsHistory.length > 0) {
        this.setState(
          {
            isAfterRun: false,
          },
          () => {
            window.location.href = `#/reports/${nextProps.reportsHistory[0].id}`
          },
        )
      }
    }
  }

  onSearchReportsMenu = (search: string) => {
    this.setState({ searchMenuItem: search })
  }

  onMenuItemChanged = (title: string | any, category: number) => {
    this.setState({ selectedMenuItem: title, category })
  }

  onRun = (data: any) => {
    this.setState(
      {
        isAfterRun: true,
      },
      () => {
        this.props.saveReportHistory(data)
      },
    )
  }

  render() {
    const { reportsHistory, sellers, customReports, customers, vendors, items, categories, currentUser } = this.props
    const { searchMenuItem, selectedMenuItem, category } = this.state
    const customMenuItems = ['All Margins', 'California deliveries', 'Oregon deliveries(2020 Product ...)']
    const dailyReportsItems = ["Today's Sales", "Today's Deliveries", "Today's Purchase Orders", "Today's Inventory"]
    /**
     * define types
     * Sales Price Under Cost : 10
     * Purchase Cost Above Price: 15
     * Backorder: 20
     * Allocation Issue: 25
     * unshipped sales orders: 45
     */
    const operationsItems = [/*'Purchasing',*/ 'Sales by Account/Order', 'Sales by Account/Product', 'Sales by Product/Order', 'Sales by Product', 'Deliveries',
      'Sales Price Under Cost', 'Purchase Cost Above Price', 'Allocation Issue', 'Lot errors by order', 'Lot errors by product', 'Backorder', /*'Inventory',*/ 'Inventory']
    const exceptionItems = ['Unshipped Sales Orders', 'Unreceived Purchase Orders']
    const hrItems = ['Employees', 'Hours Worked', 'PTO']
    const externalReportItems = ['QuickBooks Online', 'NetSuite' /*'External Report 3', 'External Report 4'*/]

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Purchasing-Reports'}>
        <ReportsBody>
          <ReportsMenuWrapper>
            {
              [UserRole.SALES, UserRole.SELLER_RESTRICTED].indexOf(currentUser?.accountType) != -1 ?
                <ReportItemGroup
                  title={'operations'}
                  items={['Allocation Issue']}
                  initialShowItems={4}
                  search={searchMenuItem}
                  selectedMenuItem={selectedMenuItem}
                  onMenuItemChanged={(v) => this.onMenuItemChanged(v, 2)}
                  currentUser={currentUser}
                /> : <>
                  <div className="search-container">
                    <SearchInput placeholder={'Search Reports'} handleSearch={this.onSearchReportsMenu} />
                  </div>
                  <div className="item-container">
                    <ReportItemGroup
                      title={'custom reports'}
                      items={customReports}
                      search={searchMenuItem}
                      selectedMenuItem={selectedMenuItem}
                      onMenuItemChanged={(v) => this.onMenuItemChanged(v, 0)}
                    />
                    {/* <ReportItemGroup
                  title={'daily reports'}
                  items={dailyReportsItems}
                  initialShowItems={4}
                  search={searchMenuItem}
                  selectedMenuItem={selectedMenuItem}
                  onMenuItemChanged={(v) => this.onMenuItemChanged(v, 1)}
                /> */}
                    <ReportItemGroup
                      title={'operations'}
                      items={operationsItems}
                      initialShowItems={4}
                      search={searchMenuItem}
                      selectedMenuItem={selectedMenuItem}
                      onMenuItemChanged={(v) => this.onMenuItemChanged(v, 2)}
                    />
                    <ReportItemGroup
                      title={'exceptions'}
                      items={exceptionItems}
                      initialShowItems={4}
                      search={searchMenuItem}
                      selectedMenuItem={selectedMenuItem}
                      onMenuItemChanged={(v) => this.onMenuItemChanged(v, 5)}
                    />
                    <ReportItemGroup
                      title={'hr'}
                      items={hrItems}
                      search={searchMenuItem}
                      selectedMenuItem={selectedMenuItem}
                      onMenuItemChanged={(v) => this.onMenuItemChanged(v, 3)}
                    />
                    <ReportItemGroup
                      title={'external reports'}
                      items={externalReportItems}
                      search={searchMenuItem}
                      selectedMenuItem={selectedMenuItem}
                      onMenuItemChanged={(v) => this.onMenuItemChanged(v, 4)}
                    />
                  </div>
                </>

            }
          </ReportsMenuWrapper>
          <ReportsContentWrapper>
            {(category === 0 && typeof selectedMenuItem == 'string') ||
              (category === 2 && (selectedMenuItem == 'Sales by Account/Product' || selectedMenuItem === 'Sales by Product')) ? (
              <SalesReport
                reportsHistory={reportsHistory}
                filter={
                  typeof selectedMenuItem === 'string'
                    ? {
                      type: 1
                    } : (category === 0 && selectedMenuItem.type === 2) ? selectedMenuItem : {
                      sellerId: selectedMenuItem.sellerId,
                      dateType: selectedMenuItem.dateType,
                      sku: selectedMenuItem.sku,
                      clientId: selectedMenuItem.wholesaleClientId,
                      salesOrderId: selectedMenuItem.wholesaleOrderId
                    }
                }
                category={category}
                item={selectedMenuItem === 'Sales by Product' ? 'Profitability' : selectedMenuItem}
                onRun={this.onRun}
                sellers={sellers}
                customers={customers}
                vendors={vendors}
                categories={categories}
                items={items}
              />
            ) : category === 2 && selectedMenuItem === 'Deliveries' ? (
              <DriverReports />
            ) : ((category == 0 && typeof selectedMenuItem == 'object' && selectedMenuItem.type == 40) || (category === 2 && selectedMenuItem === 'Sales by Product/Order')) ? (
              <SalesByProductOrderReportFormVertical 
                filter={typeof selectedMenuItem === 'string' ?{
                  type: 40
                } : {
                  ...selectedMenuItem
                }}
                onRun={this.onRun}
                customers={customers}
                categories={categories}
                items={items}
                vertical={true}
              />
            ) : category === 2 && selectedMenuItem === 'Receiving Food Safety' ? (
              <ReceivingFoodReport
                reportsHistory={reportsHistory}
                filter={{
                  sellerId: selectedMenuItem.sellerId,
                  dateType: selectedMenuItem.dateType,
                  sku: selectedMenuItem.sku,
                  clientId: selectedMenuItem.wholesaleClientId,
                }}
                category={category}
                item={selectedMenuItem}
                onRun={this.onRun}
                sellers={sellers}
                customers={customers}
                vendors={vendors}
                categories={categories}
                items={items}
              />
            ) : category === 2 && selectedMenuItem === 'Sales by Account/Order' ? (
              <SalesReportByOrder
                reportsHistory={reportsHistory}
                filter={{
                  // sellerId: selectedMenuItem.sellerId,
                  // wholesaleOrderId: selectedMenuItem.wholesaleOrderId,
                  // clientId: selectedMenuItem.wholesaleClientId,
                  dateType: 0,
                  type: 4
                }}
                category={category}
                title={selectedMenuItem}
                onRun={this.onRun}
                sellers={sellers}
                customers={customers}
                vendors={vendors}
                categories={categories}
                items={items}
              />
            ) : ((category === 2 && selectedMenuItem === 'Inventory') || (category == 0 && typeof selectedMenuItem == 'object' && selectedMenuItem.type == 35)) ? (
              <Inventory
                title={typeof selectedMenuItem == 'object' ? selectedMenuItem.name : selectedMenuItem}
                reportsHistory={this.props.reportsHistory}
                vendors={vendors}
                items={items}
                onRun={this.onRun}
                vertical={true}
                loading={false}
                filter={{
                  type: 35
                }}
              />
            ) : ((category == 0 && typeof selectedMenuItem == 'object' && [10, 15, 20].indexOf(selectedMenuItem.type) > -1)
              || (category === 2 && (selectedMenuItem === 'Sales Price Under Cost' ||
                selectedMenuItem === 'Purchase Cost Above Price' ||
                selectedMenuItem === 'Backorder'))) ? (
              <CostPriceDifference
                isVertial={true}
                title={typeof selectedMenuItem == 'object' ? selectedMenuItem.name : selectedMenuItem}
                reportsHistory={this.props.reportsHistory}
                onRun={this.onRun}
                filter={typeof selectedMenuItem == 'object' ? {
                  type: selectedMenuItem.type,
                  dateType: selectedMenuItem.dateType,
                } : {
                  type: selectedMenuItem === 'Sales Price Under Cost'
                    ? 10 : selectedMenuItem === 'Purchase Cost Above Price'
                      ? 15 : 20
                }}
                reportType={selectedMenuItem === 'Sales Price Under Cost'
                  ? 'sales' : selectedMenuItem === 'Purchase Cost Above Price'
                    ? 'purchase' : 'backorder'}
              />
            ) : ((category == 0 && typeof selectedMenuItem == 'object' && selectedMenuItem.type == 25)
              || (category === 2 && selectedMenuItem === 'Allocation Issue')) ? (
              <AllocationIssue
                title={typeof selectedMenuItem == 'object' ? selectedMenuItem.name : selectedMenuItem}
                reportsHistory={this.props.reportsHistory}
                onRun={this.onRun}
                filter={{
                  type: 25
                }}
              />
            ) : ((category == 0 && typeof selectedMenuItem == 'object' && selectedMenuItem.type == 45)
            || (category === 2 && selectedMenuItem === 'Lot errors by order')) ? (
              <LotErrorsByOrder
                isVertical={true}
                onRun={this.onRun}
                filter={{
                  type: 45,
                }}
              />
          ) : ((category == 0 && typeof selectedMenuItem == 'object' && selectedMenuItem.type == 30)
          || (category === 2 && selectedMenuItem === 'Lot errors by product')) ? (
            <UnallocatedLots
              isVertical={true}
              title={typeof selectedMenuItem == 'object' ? selectedMenuItem.name : selectedMenuItem}
              reportsHistory={this.props.reportsHistory}
              sellers={sellers}
              onRun={this.onRun}
              getAllocateLots={this.props.getAllocateLotsData}
              allocateLotsData={this.props.allocateLotsData}
              allocateLots={this.props.allocateLots}
              setLoading={this.props.setLoading}
              filter={{
                type: 30,
              }}
            />
          ) : ((category == 0 && typeof selectedMenuItem == 'object' && selectedMenuItem.type == 55)
          || (category === 5 && selectedMenuItem === 'Unshipped Sales Orders')) ? (
            <UnshippedSalesOrderVertical
              filter={typeof selectedMenuItem === 'string' ?{
                type: 45
              } : {
                ...selectedMenuItem
              }}
              onRun={this.onRun}
              vertical={true}
            />
           ) : ((category == 0 && typeof selectedMenuItem == 'object' && selectedMenuItem.type == 50)
           || (category === 5 && selectedMenuItem === 'Unreceived Purchase Orders')) ? (
             <UnreceivedPurchaseOrderVertical
               filter={typeof selectedMenuItem === 'string' ?{
                 type: 50
               } : {
                 ...selectedMenuItem
               }}
               onRun={this.onRun}
               vertical={true}
             />
            ) : (
              <SalesReportBody>
                <ReportContent>
                  <ReportTable>
                    <span style={{ lineHeight: '25px' }}>
                      This area is currently under development.
                      <br />
                      Please contact your customer success representative for this report.
                    </span>
                  </ReportTable>
                </ReportContent>
              </SalesReportBody>
            )}
          </ReportsContentWrapper>
        </ReportsBody>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.reports, currentUser: state.currentUser }
}

export default withTheme(connect(ReportsModule)(mapStateToProps)(ReportsContainer))
