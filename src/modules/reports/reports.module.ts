import { Module, EffectModule, ModuleDispatchProps, Effect, DefineAction, StateObservable, Reducer } from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { map, switchMap, endWith, catchError } from 'rxjs/operators'
import { Action } from 'redux-actions'
import { checkError, responseHandler } from '~/common/utils'
import { ReportsService } from './reports.service'
import { CustomerService } from '../customers/customers.service'
import { GlobalState } from '~/store/reducer'
import { SaleCategory, SaleItem, TempCustomer } from '~/schema'
import { InventoryService } from '../inventory/inventory.service'
import { OrderService } from '../orders/order.service'
import { SettingService } from '../setting/setting.service'

export interface ReportsStateProps {
  loading: boolean
  costDataLoading: boolean
  reports: {}
  users: any[]
  sellers: any[]
  filters: any[]
  reportsHistory: any[]
  customReports: any[]
  reportsToDownload: any[]
  customers: TempCustomer[]
  items: SaleItem[]
  vendors: any[]
  categories: SaleCategory[]
  categoryReports: any[]
  productReports: any[]
  sellerSetting: any
  costDifferenceData: any[]
  allocationIssues: any[]
  unallocatedLots: any[]
  inventoryReportItems: any[]
  totalReports: any[]
  salesProductOrderReport: any[]
  salesProductReport: any
  allocateLotsData: any[]
  lotsErrorByOrderData: any[]
  exceptionOrderReportData: any[]
}

@Module('reports')
export class ReportsModule extends EffectModule<ReportsStateProps> {
  defaultState = {
    loading: false,
    costDataLoading: false,
    reports: {},
    users: [],
    sellers: [],
    filters: [],
    reportsHistory: [],
    customReports: [],
    reportsToDownload: [],
    customers: [],
    vendors: [],
    items: [],
    categories: [],
    categoryReports: [],
    productReports: [],
    sellerSetting: null,
    costDifferenceData: [],
    allocationIssues: [],
    unallocatedLots: [],
    inventoryReportItems: [],
    totalReports: [],
    salesProductOrderReport: [],
    salesProductReport: {},
    allocateLotsData: [],
    lotsErrorByOrderData: [],
    exceptionOrderReportData: []
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(
    private readonly reports: ReportsService,
    private readonly customer: CustomerService,
    private readonly inventory: InventoryService,
    private readonly order: OrderService,
    private readonly setting: SettingService
  ) {
    super()
  }

  @Reducer()
  setLoading(state: ReportsStateProps, action: Action<boolean>) {
    return {
      ...state,
      costDataLoading: action.payload
    }
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      const sellerId = localStorage.getItem('reprot_seller_id')
      let reports = JSON.parse(JSON.stringify(state.reports))
      reports[sellerId!] = action.payload
      return {
        ...state,
        reports,
        loading: false,
      }
    },
  })
  findReportByClient(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.findReportByClient(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        users: action.payload,
        loading: false,
      }
    },
  })
  findReportByUser(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.findReportByUser(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        reportsToDownload: action.payload,
        loading: false,
      }
    },
  })
  getReportsToDownload(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.getReportsToDownload(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        sellers: action.payload,
        loading: false,
      }
    },
  })
  getAllSellers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.reports.getAllSellerData().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        filters: action.payload,
        loading: false,
      }
    },
  })
  getAllFilters(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.reports.getAllReportFilter().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps) => {
      window.location.href = '#/reports'
      return {
        ...state,
        loading: false,
      }
    },
  })
  saveReportFilter(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.saveReportFilter(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllFilters)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        reportsHistory: action.payload,
        loading: false,
      }
    },
  })
  getReportsHistory(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.reports.getReportsHistory().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps) => {
      return {
        ...state,
        loading: false,
      }
    },
  })
  saveReportHistory(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.saveReportHistory(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getReportsHistory)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        customReports: action.payload,
        loading: false,
      }
    },
  })
  getAllCustomReports(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.reports.getAllCustomReports().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps) => {
      return {
        ...state,
        loading: false,
      }
    },
  })
  saveCustomReportWithName(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.saveCustomReportWithName(data.reportName, data.filter).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getAllCustomReports)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, { payload }: Action<any>) => {
      return { ...state, customers: payload }
    },
  })
  getCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.customer.getAllCustomers(state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        items: action.payload,
        loading: '',
      }
    },
  })
  getAllItems(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory
          .getAllItems(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET', null, null)
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<SaleCategory[]>) => {
      return {
        ...state,
        categories: action.payload,
      }
    },
  })
  getSaleCategories(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getAllCategories(state$.value.currentUser.company || 'GRUBMARKET').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any[]>) => {
      return {
        ...state,
        vendors: action.payload,
      }
    },
  })
  getVendors(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.order.getAllVendors().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        categoryReports: action.payload,
        loading: false,
      }
    },
  })
  getCategoryReport(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.reports.getCategoryReport(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      const categoryId = localStorage.getItem('reprot_category_id')
      let productReports = JSON.parse(JSON.stringify(state.productReports))
      productReports[categoryId!] = action.payload
      return {
        ...state,
        productReports,
        loading: false,
      }
    },
  })
  getProductReport(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.reports.getProductReport(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }
  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        sellerSetting: action.payload.userSetting,
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getUserSetting(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(map(this.createAction('done'))),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        costDifferenceData: action.payload,
        costDataLoading: false,
        reportsToDownload: action.payload
      }
    },
  })
  getCostDifferenceData(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((filter) =>
        this.reports.getCostReferenceData(filter)
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done'))
          ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        allocationIssues: action.payload,
        costDataLoading: false,
        reportsToDownload: action.payload
      }
    },
  })
  getAllocationIssues(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((filter: any) =>
        this.reports.getAllocationIssues(filter)
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done'))
          ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        unallocatedLots: action.payload,
        costDataLoading: false,
        reportsToDownload: action.payload
      }
    },
  })
  getUnallocatedLotsReport(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((filter: any) =>
        this.reports.getUnallocatedLotsReports(filter)
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done'))
          ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        lotsErrorByOrderData: action.payload,
        costDataLoading: false,
        reportsToDownload: action.payload,
      }
    },
  })
  getLotsErrorByOrderReport(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((filter: any) =>
        this.reports.getLotsErrorByOrder(filter)
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done'))
          ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }



  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        inventoryReportItems: action.payload,
        costDataLoading: false,
        reportsToDownload: action.payload
      }
    },
  })
  getInventoryReport(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((filter: any) =>
        this.reports.getInventoryReport(filter)
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done'))
          ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        totalReports: action.payload,
        loading: false,
      }
    },
  })
  getTotalReportsWithoutGroup(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.getTotalReportsWithoutGroup(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        salesProductOrderReport: action.payload,
        loading: false,
      }
    },
  })
  getSalesProductOrderReportData(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.getSalesProductOrderCategoryReportData(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        allocateLotsData: action.payload,
        loading: false,
      }
    },
  })
  getAllocateLotsData(action$: Observable<any>) {
    return action$.pipe(
      switchMap((filter) =>
        this.reports.getAllocateLotsData(filter).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        exceptionOrderReportData: action.payload,
        reportsToDownload: action.payload,
        loading: false,
      }
    },
  })
  getExceptionOrderReportData(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.getExceptionOrderReportData(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return {
        ...state,
        salesProductReport: {...state.salesProductReport, ...action.payload},
        loading: false,
      }
    },
  })
  getSalesProductOrderProductReportData(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.getSalesProductOrderProductReportData(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: ReportsStateProps, action: Action<any>) => {
      return state
    },
  })
  allocateLots(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.reports.allocateLots(data.allocate).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data).pipe(
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getUnallocatedLotsReport)(data.filter)),
            endWith(this.createActionFrom(this.getAllocateLotsData)(data.filter)),
            catchError((error) => of(checkError(error)))
          )),
        ),
      ),
    )
  }
}

export type ReportsDispatchProps = ModuleDispatchProps<ReportsModule>
