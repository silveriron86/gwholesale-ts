import moment from 'moment'
import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import { queryParams } from '~/common/utils'

@Injectable()
export class ReportsService {
  constructor(private readonly http: Http) { }

  getParams(data: any) {
    const { 
      clientId, 
      sellerId, 
      sku, 
      dateType, 
      type, 
      from, 
      to, 
      categoryId, 
      wholesaleOrderId, 
      includeLotsWithZero, 
      includeLotsWithNegative, 
      displayNotReceived,
      displayNegativeSell,
      customOrderNumber,
      itemName,
      priceAfterSale,
      search
     } = data
    let params = ''
    if (clientId) {
      params += `${params ? '&' : '?'}clientId=${clientId}`
    }
    if (sellerId) {
      params += `${params ? '&' : '?'}sellerId=${sellerId}`
    }
    if (sku) {
      params += `${params ? '&' : '?'}sku=${sku}`
    }
    if (dateType > 0) {
      params += `${params ? '&' : '?'}dateType=${dateType}`
    }
    if (from) {
      params += `${params ? '&' : '?'}from=${from}`
    }
    if (to) {
      params += `${params ? '&' : '?'}to=${to}`
    }
    if (typeof type !== 'undefined') {
      params += `${params ? '&' : '?'}type=${type}`
    }
    if (typeof includeLotsWithZero !== 'undefined') {
      params += `${params ? '&' : '?'}includeLotsWithZero=${includeLotsWithZero}`
    }
    if (typeof includeLotsWithNegative !== 'undefined') {
      params += `${params ? '&' : '?'}includeLotsWithNegative=${includeLotsWithNegative}`
    }
    if (typeof displayNotReceived !== 'undefined') {
      params += `${params ? '&' : '?'}displayNotReceived=${displayNotReceived}`
    }
    if (typeof displayNegativeSell !== 'undefined') {
      params += `${params ? '&' : '?'}displayNegativeSell=${displayNegativeSell}`
    }

    if (wholesaleOrderId) {
      params += `${params ? '&' : '?'}wholesaleOrderId=${wholesaleOrderId}`
    }
    if (categoryId) {
      params += `${params ? '&' : '?'}categoryId=${categoryId}`
    }
    if (customOrderNumber) {
      params += `${params ? '&' : '?'}customOrderNumber=${customOrderNumber}`
    }
    if (itemName) {
      params += `${params ? '&' : '?'}itemName=${itemName}`
    }
    if(priceAfterSale) {
      params += `${params ? '&' : '?'}priceAfterSale=${priceAfterSale}`
    }
    if(search) {
      params += `${params ? '&' : '?'}search=${search}`
    }
    return params
  }
  // find report summary group by client
  findReportByClient(data: any) {
    const params = this.getParams(data)
    if (data.type == 4) {
      return this.http.get<any>(`/report/client-report-by-order${params}`)
    } else {
      return this.http.get<any>(`/report/client-report${params}`)
    }
  }

  // find report summary group by user
  findReportByUser(data: any) {
    const params = this.getParams(data)
    return this.http.get<any>(`/report/seller-report${params}`)
  }

  // get data to download reports
  getReportsToDownload(data: any) {
    const params = this.getParams(data)
    return this.http.get<any>(`/report/download-report${params}`)
  }

  // get all report filter
  getAllReportFilter() {
    return this.http.get<any>(`/report`)
  }

  // save report filter
  saveReportFilter(data: any) {
    return this.http.post<any>(`/report`, {
      body: JSON.stringify(data),
    })
  }

  // get run report history
  getReportsHistory() {
    return this.http.get<any>(`/report/report-history`)
  }

  // save run report history
  saveReportHistory(data: any) {
    return this.http.post<any>(`/report/report-history`, {
      body: JSON.stringify(data),
    })
  }

  // get custom report list
  getAllCustomReports() {
    return this.http.get<any>(`/report/custom-report`)
  }

  // save custom report(with name)
  saveCustomReportWithName(reportName: string, data: any) {
    return this.http.post<any>(`/report/custom-report`, {
      body: JSON.stringify({
        ...data,
        ...{ name: reportName },
      }),
    })
  }

  // get all seller data
  getAllSellerData() {
    return this.http.get<any>(`/session/sale-user`)
  }

  // category report
  getCategoryReport(data: any) {
    const param = queryParams(data)
    return this.http.get<any>(`/report/category-report${param ? `?${param}` : ''}`)
  }

  // product report
  getProductReport(data: any) {
    const param = queryParams(data)
    return this.http.get<any>(`/report/product-report${param ? `?${param}` : ''}`)
  }

  // cost difference reports for stuffs that is added in Jana requirements
  getCostReferenceData(filter: any) {
    const param = this.getParams(filter)
    return this.http.get<any>(`/report/cost-price-report${param ? `${param}` : ''}`)
  }

  getAllocationIssues(filter: any) {
    const param = this.getParams(filter)
    return this.http.get<any>(`/report/allocation-issues${param ? `${param}` : ''}`)
  }

  getUnallocatedLotsReports(filter: any) {
    const param = this.getParams(filter)
    return this.http.get<any>(`/report/unallocated-lots${param ? `${param}` : ''}`)
  }

  getLotsErrorByOrder(filter: any) {
    const param = this.getParams(filter)
    return this.http.get<any>(`/report/lot-errors${param ? `${param}` : ''}`)

  }

  getInventoryReport(filter: any) {
    const param = this.getParams(filter)
    return this.http.get<any>(`/report/inventory-report${param ? `${param}` : ''}`)
  }

  getTotalReportsWithoutGroup(data: any) {
    const params = this.getParams(data)
    if (data.type == 4) {
      return this.http.get<any>(`/report/total-reports-without-group-by-sale-order${params ? `${params}` : ''}`)
    } else {
      return this.http.get<any>(`/report/total-reports-without-group${params ? `${params}` : ''}`)
    }
  }

  getSalesProductOrderCategoryReportData(data: any) {
    const params = this.getParams(data)
    return this.http.get<any>(`/report/sales-product-order-category-report${params ? `${params}` : ''}`)
  }

  getExceptionOrderReportData (data: any) { 
    const params = this.getParams(data)
    return this.http.get<any>(`/report/exception-order-report${params ? `${params}` : ''}`)
  }

  getSalesProductOrderProductReportData(data: any) {
    const params = this.getParams(data)
    return this.http.get<any>(`/report/sales-product-order-product-report${params ? `${params}` : ''}`)
  }

  getAllocateLotsData(filter: any) {
    const params = this.getParams(filter)
    return this.http.get<any>(`/report/allocate-lots${params ? `${params}` : ''}`)
  }

  allocateLots(data: any) {
    return this.http.post<any>(`/report/allocate-lots`, {
      body: JSON.stringify(data),
    })
  }
}
