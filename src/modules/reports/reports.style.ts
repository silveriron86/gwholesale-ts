import styled from '@emotion/styled'
import { Collapse } from 'antd'
import { gray01, lightGrey, topLightGrey} from '~/common'

export const ReportsHeader = styled('div')({
  height: 80,
  borderBottom: `1px solid ${lightGrey}`,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  paddingLeft: 70,
  paddingRight: 30,
  a: {
    display: 'flex',
    alignItems: 'center',
    svg: {
      marginRight: 15,
    },
    h3: {
      marginBottom: 0,
      fontSize: 20,
    },
  },
})

export const ReportsBody = styled('div')({
  display: 'flex',
  minHeight: 'calc(100vh - 80px - 60px)',
  fontFamily: 'Museo Sans Rounded',
})

export const ReportsMenuWrapper = styled('div')({
  width: 300,
  borderRight: `1px solid ${lightGrey}`,
  '& .search-container': {
    padding: '10px 4px 10px 50px',
    height: 55,
    '& input': {
      border: 'none',
      '&:focus': {
        border: 'none',
        boxShadow: 'none',
      },
    },
  },
  '& .item-container': {
    height: 'calc(100vh - 59px - 55px)',
    overflowY: 'auto',
  },
})

export const ReportsContentWrapper = styled('div')({
  flex: 'auto',
  '[class*="ReportTitle"]': {
    marginTop: 'auto',
    marginBottom: 20,
  }
})

export const ReportItemGroupWrapper = styled('div')({
  borderTop: `1px solid ${lightGrey}`,
  padding: 4,
  textAlign: 'left',
})

export const DetailWrapper = styled('div')({
  backgroundColor: '#f7f7f7',
  paddingTop: 35,
  paddingLeft: 70,
  paddingRight: 30,
  paddingBottom: 20,
  borderTop: '1px solid #ddd',
  textAlign: 'left',
  h1: {
    marginBottom: 25,
  },
  '.ant-collapse-header': {
    backgroundColor: 'white',
    '.anticon': {
      zIndex: 1
    }
  },
  '.ant-collapse-content': {
    backgroundColor: 'white',
    '.ant-collapse-content-box': {
      padding: 0,
      '.ant-table-wrapper .ant-table-content .ant-table-body .ant-table-tbody > tr > td': {
        background: 'white',
      },
      '.ant-table-expanded-row': {
        td: {
          backgroundColor: '#f7f7f7 !important',
        },
      },
    },
  },
  '&.white': {
    borderTop: 0,
    backgroundColor: 'white',
    '.ant-collapse-content-box': {
      paddingTop: 15,
      paddingBottom: 15,
    },
    '.ant-form-vertical': {
      padding: 10,
      maxWidth: 1050,
      '.ant-form-item': {
        paddingBottom: 8,
        margin: '0 20px 15px',
      },
    },
  },
  '&.last': {
    '.ant-collapse': {
      border: 0,
      '&> .ant-collapse-item': {
        marginBottom: 10,
        border: '1px solid #d9d9d9',
        borderRadius: 5,
        overflow: 'hidden',
      },
      '.ant-collapse-content': {
        table: {
          border: '0 !important',
        },
      },
    },
    '.ant-collapse > .ant-collapse-item > .ant-collapse-header': {
      paddingLeft: 0,
      paddingRight: 0
    }
  },
  '&.insight': {
    '.ant-collapse-header': {
      backgroundColor: '#f7f7f7',
    },
    '.ant-collapse-content-box': {
      backgroundColor: '#f7f7f7',
      padding: 15,
    },
    '.ant-collapse': {
      border: 'none',
      '.ant-collapse-item': {
        border: 'none',
        '.ant-collapse-content': {
          border: 'none'
        }
      }

    }
  },
  '.ant-table': {
    'td': {
      paddingLeft: '16px !important',
      paddingRight: '16px !important'
    }
  }
})

export const ExpandedTabelWrapper = styled('div')({
  marginLeft: -9,
  marginRight: -9,
  marginTop: -40,
  '.ant-table-wrapper': {
    backgroundColor: 'transparent !important',
    table: {
      borderLeft: 0,
      borderTop: 0,
      backgroundColor: 'transparent',
      '.ant-table-thead': {
        boxShadow: 'none !important',
        th: {
          backgroundColor: 'transparent !important',
          '.ant-table-header-column': {
            visibility: 'hidden',
          },
        },
      },
      tr: {
        td: {
          backgroundColor: '#f7f7f7 !important',
          '&:first-of-type': {
            paddingLeft: '49px !important'
          }
        },
      },
    },
  },
})

export const PanelHeader = styled('div')({
  backgroundColor: 'white',
  '.ant-table-wrapper': {
    backgroundColor: 'white !important',
    table: {
      border: '0 !important',
      boxShadow: 'none !important',
      '.ant-table-thead': {
        boxShadow: 'none !important',
        th: {
          border: '0 !important',
          '&:nth-of-type(1)': {
            paddingLeft: 49
          },
          '&:nth-of-type(2)': {
            '.ant-table-column-title': {
              // color: 'white !important'
            }
          }
        },
      },
      tr: {
        td: {
          backgroundColor: 'white !important',
          border: '0 !important',
          '&:nth-of-type(1)': {
            paddingLeft: '49px !important'
          },
        },
      },
    },
  },
})

export const FlexWrap = styled('div')({
  display: 'flex',
  flexWrap: 'wrap',
  '&.between': {
    justifyContent: 'space-between',
  },
})

export const FilterFormWrap = styled('div')({
  '.filter-checkbox': {
    display: 'flex',
    flexDirection: 'row-reverse',
    justifyContent: 'flex-end',
    '.ant-form-item-control-wrapper': {
      marginRight: 8
    },
    '.ant-form-item-label label': {
      whiteSpace: 'nowrap'
    }
  },
  '.mh20': {
    margin: '0 20px'
  },
  '.mt10': {
    marginTop: 10
  },
  '.lineHeight15 .ant-form-item-label': {
    lineHeight: '1.5 !important'
  }
})

export const InsightBlock = styled('div')({
  padding: '16px 20px 40px',
  margin: 8,
  backgroundColor: 'white',
  borderRadius: 5,
  border: '1px solid #dedede',
  '.label': {
    fontSize: 14,
    //color: '#61696a',
    color: gray01,
    // textTransform: 'uppercase',
  },
  '.value': {
    fontSize: 20,
    //color: '#222',
    color: gray01,
  },
  '.diff': {
    fontSize: 13,
    '&.green': {
      color: '#1c6e31',
    },
    '&.red': {
      color: '#b50f04',
    },
  },
  '.info-icon': {
    cursor: 'pointer'
  }
})

export const ItemTitle = styled('div')({
  color: lightGrey,
  fontWeight: 'bold',
  textTransform: 'uppercase',
  paddingLeft: 60,
})

export const ReportMenuItem = styled('div')((props: any) => ({
  textTransform: 'capitalize',
  padding: '10px 0',
  paddingLeft: 60,
  fontWeight: 'bold',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  '&.selected': {
    background: props.theme.lighter,
  },
  '&:hover': {
    cursor: 'pointer',
    background: topLightGrey,
  },
}))

export const SalesReportBody = styled('div')((props: any) => ({
  padding: 48,
  '&.cost-price-report': {
    'table': {
      'thead tr th:not(last-of-type)': {
        background: `rgb(250,250,250) !important`,
        borderRight: `1px solid ${topLightGrey}`
      },
      'tbody tr td:not(last-of-type)': {
        background: 'white !important',
        borderRight: `1px solid ${topLightGrey}`
      },
      'thead tr th:first-of-type, tbody tr td:first-of-type': {
        borderRight: `none`
      },
      '.hoverable': {
        color: props.theme.dark,
        cursor: 'pointer'
      },
      '.hoverable:hover': {
        textDecoration: 'underline',
      },
    }
  },
  '&.allocation-issues': {
    'tr td': {
      paddingLeft: '16px !important',
      paddingRight: '16px !important',
    },
    'tr.ant-table-row-level-1 td': {
      background: '#F7F7F7 !important',
      color: '#82898A !important'
    },
    'table tbody tr td:nth-of-type(2)': {
      color: `${props.theme.primary} !important`
    }
  },
  '&.inventory': {
    'tr td': {
      backgroundColor: 'white !important',
    },
    '.ant-table-pagination.ant-pagination': {
      marginTop: 64,
    },
    '.ant-table-body': {
      zIndex: 1,
      position: 'relative'
    },
    '.ant-table-footer': {
      zIndex: 0,
      padding: '0 0 !important',
      borderTop: 'none !important',
      position: 'absolute',
      minHeight: 80,
      bottom: -50,
      width: '100%',
      'td': {
        borderBottom: '0 !important',
        FontFamily: 'Arial',
        fontWeight: 700,
        fontstyle: 'normal',
        fontSize: 14,
        lineHeight: '19.6px',
      }
    }
  }
}))

export const ReportContent = styled('div')({
  textAlign: 'left',
})

export const ReportFilter = styled('div')({
  '.pas': {
    '.ant-form-item-control-wrapper': {
      float: 'left'
    },
    '.ant-form-item-label': {
      float: 'right',
      marginRight: 25
    },
    '&.horizontal': {
      marginTop: '25px !important',
      '.ant-form-item-label': {
        marginRight: 0,
        marginLeft: 5
      }
    }
  }
})

export const ReportFilterRow = styled('div')((props: any) => ({
  maxWidth: 350,
  textAlign: 'left',
  '& .run-report': {
    background: props.theme.dark,
  },
}))

export const ReportTitle = styled('div')({
  color: 'black',
  fontSize: 28,
  marginBottom: 45,
  marginTop: -95,
  '& span.bold': {
    fontWeight: 'bold',
  },
})

export const ReportTable = styled('div')({
  padding: '20px 0',
})

export const RunHistoryItem = styled('div')((props: any) => ({
  minWidth: 350,
  fontWeight: 'bold',
  padding: 10,
  paddingLeft: 0,
  '& span.date': {
    color: props.theme.primary,SalesReportBody
  },
}))

export const Insights = styled('div')({
  display: 'flex'
  '.cl': {
    minWidth: 288,
    height: 119,
    border: '1px solid #C4C4C4',
    borderRadius: 5,
    padding: '27px 29px',
    '.label': {
      color: '#82898A',
      fontFamily: 'Museo Sans Rounded',
      fontSize: 14,
      lineHeight: '14px',
      fontWeight: 600,
      textTransform: 'uppercase',
    },
    '.value': {
      marginTop: 14,
      color: '#4A5355',
      fontFamily: 'Museo Sans Rounded',
      fontSize: 32,
      lineHeight: '32px',
      fontWeight: 400
    }
  },
})

export const Ellipsis = styled('div')({
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
})

export const InventoryLabel = styled('div')({
  color: '#22282A',
  fontSize: 18,
  lineHeight: '25.2px',
  fontFamily: 'Arial',
  fontWeight: 400,
  marginTop: 42,
})

export const InsightsCollapse = styled(Collapse)({
  marginTop: 21,
  border: 0,
  '.ant-collapse-item.ant-collapse-item-active': {
    border: 0,
    '.ant-collapse-header': {
      background: 'white',
      borderBottom: 0,
    },
    '.ant-collapse-content.ant-collapse-content-active': {
      borderTop: 0,
      '.ant-collapse-content-box': {
        paddingBottom: 0
      }
    }
  }
})
