import React from 'react'
import { Button, Form, AutoComplete } from 'antd'
import { FormComponentProps } from 'antd/es/form'
import { ModalFooterContainer, NoBottomPaddingModal } from '~/modules/restock/restock.style'
import { SimplyVendor } from '~/modules/restock/restock.module'

interface CreateOrderModalProps extends FormComponentProps {
  visible: boolean
  vendors: SimplyVendor[]
  createEmptyOrder: (clientId: number) => void
  onVisibleChange: (visible: boolean) => void
  vendor?: number
  createPOLoading: boolean
  handleClearSelectedRowKeys: () => void
}

interface IState {
  vendorOptions: SimplyVendor[]
}

const { Option } = AutoComplete

export class CreateOrderModal extends React.PureComponent<CreateOrderModalProps, IState> {
  state = {
    vendorOptions: [] as SimplyVendor[],
  }

  onVendorSearch = (inputValue: string) => {
    const { vendors } = this.props
    if (inputValue.length < 2 || !vendors.length) {
      this.setState({
        vendorOptions: [],
      })
    } else {
      this.setState({
        vendorOptions: vendors.filter((item) =>
          item.clientCompanyName.toLowerCase().includes(inputValue.toLowerCase()),
        ),
      })
    }
  }

  createEmptyOrder = () => {
    const { form, createEmptyOrder, onVisibleChange } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        values.vendor && createEmptyOrder(values.vendor)
        this.props.onVisibleChange(false)
        this.props.handleClearSelectedRowKeys()
        // form.resetFields()
      }
    })
  }

  cancelCreateOrder = () => {
    const { form, onVisibleChange } = this.props
    onVisibleChange(false)
    form.resetFields()
  }

  render() {
    const {
      visible,
      form: { getFieldDecorator },
      createPOLoading,
    } = this.props
    const { vendorOptions } = this.state

    return (
      <NoBottomPaddingModal
        visible={visible}
        width={520}
        onCancel={this.cancelCreateOrder}
        footer={
          <ModalFooterContainer className={'create-order'}>
            <span data-desc={'placeholder'} />
            <Button
              type={'primary'}
              shape={'round'}
              icon={'plus'}
              loading={createPOLoading}
              onClick={this.createEmptyOrder}
            >
              CREATE
            </Button>
            <Button onClick={this.cancelCreateOrder}>CANCEL</Button>
          </ModalFooterContainer>
        }
      >
        <Form>
          <Form.Item label={'Vendor Account'}>
            {getFieldDecorator('vendor', {
              rules: [
                {
                  required: true,
                  message: "vendor can't be empty",
                },
              ],
            })(
              <AutoComplete
                size={'large'}
                onSearch={this.onVendorSearch}
                filterOption={() => true}
                placeholder={'Search a vendor name'}
              >
                {vendorOptions.map((item) => (
                  <Option value={item.clientId+''} key={item.clientId}>
                    {item.clientCompanyName}
                  </Option>
                ))}
              </AutoComplete>,
            )}
          </Form.Item>
        </Form>
      </NoBottomPaddingModal>
    )
  }
}

export const WrappedCreateOrderModal = Form.create<CreateOrderModalProps>()(CreateOrderModal)
