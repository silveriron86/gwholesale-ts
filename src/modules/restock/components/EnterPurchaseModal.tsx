import React from 'react'
import { AutoComplete, DatePicker, Divider, Form, Icon, notification, Select } from 'antd'
import { EnterPurchaseTip, ModalFooterContainer, NoBottomPaddingModal } from '~/modules/restock/restock.style'
import { FormComponentProps } from 'antd/lib/form/Form'
import { RestockItem, SimplyVendor } from '~/modules/restock/restock.module'
import moment, { Moment } from 'moment'
import { OptionProps, SelectValue } from 'antd/lib/select'
import { ColumnProps } from 'antd/es/table/interface'
import { SaleItem } from '~/schema'
import {
  basePriceToRatioPrice,
  baseQtyToRatioQty,
  checkError,
  formatAddress,
  formatItemDescription,
  formatNumber,
  initHighlightEvent,
  judgeConstantRatio,
  mathRoundFun,
  quantityToQuantity,
  ratioPriceToBasePrice,
  ratioQtyToBaseQty,
  ratioQtyToInventoryQty,
  responseHandler,
} from '~/common/utils'
import { OrderService } from '~/modules/orders/order.service'
import { of } from 'rxjs'
import { cloneDeep, debounce, isArray } from 'lodash'
import {
  ThemeIcon,
  ThemeIconButton,
  ThemeInput,
  ThemeModal,
  ThemeSelect,
  ThemeSpin,
  ThemeTable,
} from '~/modules/customers/customers.style'

import AddNewBrandModal from '~/modules/orders/components/add-new-brand-modal'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import UomSelect from '~/modules/customers/nav-sales/order-detail/components/uom-select'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import { EnterPurchaseWrapper } from '~/modules/orders/components/orders-header.style'
import { ValueLabel } from '~/modules/customers/nav-sales/styles'
import jQuery from 'jquery'
import { ThemeButton } from '~/modules/delivery/delivery.style'

interface EnterPurchaseModalProps extends FormComponentProps {
  visible: boolean
  vendors: SimplyVendor[]
  saleItems: SaleItem[]
  companyProductTypes: any
  sellerSetting: any
  onVisibleChange: (visible: boolean) => void
  fetchCompanyProductAllTypes: () => void
  updateProduct: Function
  setCompanyProductType: Function
  getCompanyProductAllTypes: Function
  settingCompanyName: string
  onEnterPurchase: (...arts: any) => void
  selectedRows: RestockItem[]
  createPOLoading: boolean
}

interface IState {
  vendorId: number | null
  dataSource: SaleItem[]
  productSKU: any[]
  loading: boolean
  currentPage: number
  openAddNewBrandModal: boolean
  addNewBrandItemIndex: number
  visibleCOOModal: boolean
  vendorOptions: any[]
  showItems: boolean
  defaultActive: boolean
  deliverDate: Moment
  referenceNo: string
  merged: boolean
  status: string
}

export class EnterPurchaseModal extends React.PureComponent<EnterPurchaseModalProps, IState> {
  timeoutHandler: any = null
  state = {
    vendorId: null,
    dataSource: [],
    productSKU: [],
    loading: false,
    currentPage: 1,
    openAddNewBrandModal: false,
    addNewBrandItemIndex: -1,
    visibleCOOModal: false,
    vendorOptions: [],
    showItems: false,
    defaultActive: false,
    deliverDate: moment(),
    referenceNo: '',
    merged: false,
    status: '',
  }

  columns: ColumnProps<SaleItem>[] = [
    {
      title: '',
      dataIndex: 'index',
      width: 35,
      render: (v: string, record: any, index: number) => {
        return <div className="text-center">{(this.state.currentPage - 1) * 12 + (index + 1)}</div>
      },
    },
    {
      title: 'Item',
      dataIndex: 'variety',
      className: 'th-left',
      render: (variety, record) => {
        return (
          <Flex className="v-center">
            {record.wholesaleItemId === -1 ? (
              <AutoComplete
                size="large"
                style={{ width: '100%' }}
                placeholder="Add Item..."
                dataSource={this.state.productSKU}
                onSelect={this.onItemSelect}
                onFocus={() => this.onItemSearch('')}
                onSearch={debounce(this.onItemSearch, 200)}
                dropdownClassName="enter-po-autocomplete"
                defaultActiveFirstOption={this.state.defaultActive}
                disabled={!this.state.vendorId}
              />
            ) : (
              formatItemDescription(variety, record.SKU, this.props.sellerSetting)
            )}
          </Flex>
        )
      },
    },
    {
      title: 'Brand',
      dataIndex: 'brand',
      render: (brand: string, record: any, index) => {
        if (record.wholesaleItemId === -1) {
          return null
        }
        return (
          <Flex className="v-center select-container-parent">
            <ThemeSelect
              className="type-brand"
              value={brand}
              placeholder="Select..."
              style={{ minWidth: 90, width: '100%' }}
              onChange={(e) => this.onChange(e, 'brand', (this.state.currentPage - 1) * 12 + index)}
              showSearch
              filterOption={(input: any, option: any) => {
                if (option.props.children) {
                  return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                } else {
                  return true
                }
              }}
              dropdownMatchSelectWidth={false}
              dropdownRender={(el: any) => {
                return (
                  <div className={`enter-purchase-selector-brand-${(this.state.currentPage - 1) * 12 + index}`}>
                    {el}
                    <Divider style={{ margin: '4px 0' }} />
                    <div
                      style={{ padding: '4px 8px', cursor: 'pointer' }}
                      onMouseDown={(e) => e.preventDefault()}
                      onClick={(e) => this.openAddNewBrandModal((this.state.currentPage - 1) * 12 + index)}
                    >
                      <Icon type="plus" /> Add New
                    </div>
                  </div>
                )
              }}
            >
              <Select.Option value="" />
              {record.suppliers?.split(',').map((p, i) => {
                return (
                  <Select.Option key={`brand-${i}`} value={p}>
                    {p}
                  </Select.Option>
                )
              })}
            </ThemeSelect>
          </Flex>
        )
      },
    },
    {
      title: 'Origin',
      dataIndex: 'extraOrigin',
      render: (extraOrigin: string, record: any, index) => {
        if (record.wholesaleItemId === -1) {
          return null
        }
        return (
          <Flex className="v-center select-container-parent">
            <ThemeSelect
              className="type-extraOrigin"
              value={extraOrigin}
              style={{ minWidth: 70, width: '100%' }}
              onChange={(e) => this.onChange(e, 'extraOrigin', (this.state.currentPage - 1) * 12 + index)}
              showSearch
              filterOption={(input: any, option: any) => {
                if (option.props.children) {
                  return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                } else {
                  return true
                }
              }}
              dropdownMatchSelectWidth={false}
              dropdownRender={(el: any) => {
                return (
                  <div className={`enter-purchase-selector-extraOrigin-${(this.state.currentPage - 1) * 12 + index}`}>
                    {el}
                    <Divider style={{ margin: '4px 0' }} />
                    <div
                      style={{ padding: '4px 8px', cursor: 'pointer' }}
                      onMouseDown={(e) => e.preventDefault()}
                      onClick={(e) => this.openAddNewCOOModal((this.state.currentPage - 1) * 12 + index)}
                    >
                      <Icon type="plus" /> Add New
                    </div>
                  </div>
                )
              }}
            >
              <Select.Option value="" />
              {this.props.companyProductTypes &&
                this.props.companyProductTypes.extraCOO.length &&
                this.props.companyProductTypes.extraCOO.map((p, i) => {
                  return (
                    <Select.Option value={p.id} key={`${p.name}_${i}`}>
                      {p.name}
                    </Select.Option>
                  )
                })}
            </ThemeSelect>
          </Flex>
        )
      },
    },
    {
      title: 'Quantity',
      dataIndex: 'quantity',
      render: (quantity: number, record: any, index: number) => {
        if (record.wholesaleItemId === -1) {
          return null
        }
        return (
          <Flex className="v-center">
            <ThemeInput
              className="quantity-input"
              value={quantity}
              style={{ width: 60, marginRight: 3 }}
              onChange={(e) => this.onChange(e.target.value, 'quantity', (this.state.currentPage - 1) * 12 + index)}
            />{' '}
            <UomSelect
              resock={true}
              disabled={record.type == 'extra'}
              orderItem={record}
              handlerChangeUom={this.onChangeUom}
              orderIndex={(this.state.currentPage - 1) * 12 + index}
              type={2}
              isFreshGreen={this.props.settingCompanyName === 'Fresh Green Inc'}
              defaultUom={record.UOM}
            />
          </Flex>
        )
      },
    },
    {
      title: 'Cost',
      dataIndex: 'cost',
      render: (cost: number, record: any, index: number) => {
        if (record.wholesaleItemId === -1) {
          return null
        }
        return (
          <Flex className="v-center highlight-input">
            $
            <ThemeInput
              value={cost}
              style={{ width: 60, marginRight: 3 }}
              onChange={(e) => this.onChange(e.target.value, 'cost', (this.state.currentPage - 1) * 12 + index)}
            />{' '}
            /
            <div className="select-container-parent">
              <ThemeSelect
                className="type-pricingUOM"
                disabled={record.type == 'extra'}
                value={record.pricingUOM}
                suffixIcon={<Icon type="caret-down" />}
                onChange={(val: any) =>
                  this.onChangePricingUom(record.wholesaleOrderItemId, val, (this.state.currentPage - 1) * 12 + index)
                }
                style={{ minWidth: 80, width: '100%' }}
                dropdownRender={(el: any) => {
                  return (
                    <div className={`enter-purchase-selector-pricingUOM-${(this.state.currentPage - 1) * 12 + index}`}>
                      {el}
                    </div>
                  )
                }}
              >
                {/* {record.baseUOM != record.inventoryUOM && (
                      <Select.Option value={record.baseUOM} title={record.baseUOM}>{record.baseUOM}</Select.Option>
                    )} */}
                <Select.Option value={record.inventoryUOM}>{record.inventoryUOM}</Select.Option>
                {record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                  ? record.wholesaleProductUomList.map((uom) => {
                      if (!uom.deletedAt && uom.useForPurchasing) {
                        return (
                          <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                            {uom.name}
                          </Select.Option>
                        )
                      }
                    })
                  : ''}
              </ThemeSelect>
            </div>
          </Flex>
        )
      },
    },
    {
      title: 'Billable Quantity',
      dataIndex: 'weight',
      render: (weight: number, record: any, index: number) => {
        if (record.wholesaleItemId === -1) {
          return null
        }
        let editBillable = false
        let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
        let unitUOM = record.UOM

        let unitProductUOM = record.wholesaleProductUomList.filter((uom: any) => {
          return uom.name == unitUOM
        })
        let pricingProductUOM = record.wholesaleProductUomList.filter((uom: any) => {
          return uom.name == pricingUOM
        })
        if (
          (unitUOM == record.inventoryUOM || (unitProductUOM.length > 0 && unitProductUOM[0].constantRatio)) &&
          pricingProductUOM.length > 0 &&
          !pricingProductUOM[0].constantRatio
        ) {
          editBillable = true
        }
        return (
          <Flex className="v-center">
            {editBillable ? (
              <ThemeInput
                style={{ width: 70, marginRight: 3 }}
                value={weight}
                onChange={(e) => this.onChange(e.target.value, 'weight', (this.state.currentPage - 1) * 12 + index)}
              />
            ) : (
              mathRoundFun(weight, 2)
            )}{' '}
            {record.pricingUOM ? record.pricingUOM : record.inventoryUOM}
          </Flex>
        )
      },
    },
    {
      title: 'Total Cost',
      dataIndex: 'total_price',
      render: (_v: any, record: any) => {
        if (record.wholesaleItemId === -1) {
          return null
        }
        return <div className="text-right">{`$${formatNumber(record.weight * record.cost, 2)}`}</div>
      },
    },
    {
      title: '',
      dataIndex: 'wholesaleItemId',
      render: (wholesaleItemId: number, record: any, index: number) => {
        if (record.wholesaleItemId === -1) {
          return null
        }
        return (
          <ThemeIconButton
            className="no-border remove-purchase-item"
            onClick={() => this.onDelete(wholesaleItemId, (this.state.currentPage - 1) * 12 + index)}
          >
            <Icon type="close" />
          </ThemeIconButton>
        )
      },
    },
  ]

  UNSAFE_componentWillReceiveProps(nextProps: EnterPurchaseModalProps) {
    if (!nextProps.createPOLoading) {
      nextProps.onVisibleChange(false)
    }
  }

  componentDidMount() {
    this.formattedItemForSelectItem()
  }

  filterOptions = (inputValue: string, option: React.ReactElement<OptionProps>, key: string = 'clientId') => {
    const current = this.props.vendors.find((item) => item[key] === option.props.value)
    if (!current) {
      return false
    }
    return current.clientCompanyName.toLowerCase().includes(inputValue?.toLowerCase())
  }

  onVendorSearch = (str: string) => {
    if (str.length >= 2) {
      const filtered = this.props.vendors
        .filter((item) => item.clientCompanyName.toLowerCase().includes(str.toLowerCase()))
        .map((item) => ({
          value: item.clientId,
          text: item.clientCompanyName,
        }))
      this.setState({
        vendorOptions: filtered,
      })
    } else {
      this.setState({
        vendorOptions: [],
      })
    }
  }

  onVendorChange = () => {
    this.setState({
      vendorId: null,
    })
  }

  onVendorSelect = (clientId: SelectValue) => {
    const { dataSource } = this.state
    const defaultCostType = this.props.sellerSetting.company?.defaultCostType || 1
    this.setState({ vendorId: clientId as number, vendorOptions: [], showItems: true }, () => {
      if (defaultCostType != 1) {
        if (dataSource.length) {
          const ids = dataSource.map((item) => item.wholesaleItemId).join(',')
          this.getCostByItemIdAndClientId(ids, (orderItems: any) => {
            this.setNewCostToItem(orderItems)
          })
        }
      }
    })
  }

  setNewCostToItem = (saleItems: any) => {
    const { dataSource } = this.state
    const defaultCostType = this.props.sellerSetting.company?.defaultCostType || 1
    dataSource.map((addItem: any) => {
      saleItems.map((dbItem: any) => {
        if (addItem.wholesaleItemId == dbItem.wholesaleItem.wholesaleItemId) {
          if (defaultCostType == 1) {
            addItem.cost = basePriceToRatioPrice(
              dbItem.wholesaleItem.defaultPurchasingCostUOM
                ? dbItem.wholesaleItem.defaultPurchasingCostUOM
                : dbItem.wholesaleItem.inventoryUOM,
              dbItem.wholesaleItem.cost,
              dbItem.wholesaleItem,
            )
          } else if (defaultCostType == 2) {
            addItem.cost = basePriceToRatioPrice(
              dbItem.wholesaleItem.defaultPurchasingCostUOM
                ? dbItem.wholesaleItem.defaultPurchasingCostUOM
                : dbItem.wholesaleItem.inventoryUOM,
              dbItem.cost,
              dbItem.wholesaleItem,
            )
          } else {
            addItem.cost = basePriceToRatioPrice(
              dbItem.wholesaleItem.defaultPurchasingCostUOM
                ? dbItem.wholesaleItem.defaultPurchasingCostUOM
                : dbItem.wholesaleItem.inventoryUOM,
              dbItem.cost,
              dbItem.wholesaleItem,
            )
          }
        }
      })
    })
  }

  formartOption = (row: any, type: string) => {
    return {
      value: row.wholesaleItemId,
      text: type === 'SKU' ? `${row.variety} ${row[type] ? '(' + row[type] + ')' : ''}` : row[type],
    }
  }

  formatExtraOption = (row: any) => {
    return {
      value: `extra_${row.id}`,
      text: `${row.name} [Extra charge]`,
    }
  }

  onItemSearch = (value: string) => {
    let searchText = value
    if (searchText == null || searchText.length < 2) {
      this.setState({
        productSKU: [],
      })
    } else {
      searchText = searchText.toLowerCase()
      const slice1 = this.props.saleItems.map((item) => {
        let copyRow = { ...item }
        if (copyRow.wholesaleItem != null) {
          copyRow = { ...item, ...item.wholesaleItem }
        }
        if (copyRow.SKU == null) {
          copyRow.SKU = copyRow.sku
        }
        return this.formartOption(copyRow, 'SKU')
      })
      const extraCharges = this.props.companyProductTypes ? this.props.companyProductTypes.extraCharges : []
      const slice2 = extraCharges.map((item) => {
        return this.formatExtraOption(item)
      })
      const skus = [...slice1, ...slice2]

      this.setState({
        productSKU: [],
        defaultActive: false,
      })

      setTimeout(() => {
        this.setState({
          productSKU: skus.filter((item) => {
            return item.text != null && item.text.toLowerCase().includes(searchText)
          }),
        })
        if (this.timeoutHandler) {
          clearTimeout(this.timeoutHandler)
        }
        this.timeoutHandler = setTimeout(() => {
          this.setState({
            defaultActive: true,
          })
        }, 200)
      }, 50)
    }
  }

  formattedItemForSelectItem = () => {
    const { saleItems, sellerSetting, selectedRows } = this.props
    const { dataSource } = this.state
    const newSelectRows = []
    if (selectedRows.length && saleItems.length) {
      // const selected = saleItems.filter((item) => selectedRowKeys.includes(parseInt(item.wholesaleItemId + '')))
      const selected = selectedRows.map((v) => saleItems.find((d) => d.wholesaleItemId == v.wholesaleItemId))
      selected.map((item) => {
        const {
          variety,
          suppliers,
          constantRatio,
          quantity,
          cost,
          ratioUOM,
          inventoryUOM,
          baseUOM,
          wholesaleProductUomList,
          defaultPurchasingCostUOM,
          defaultPurchaseUnitUOM,
          SKU,
        } = item

        const currentRow = selectedRows.find((v) => v.wholesaleItemId === item.wholesaleItemId)
        const UOM = defaultPurchaseUnitUOM || inventoryUOM // 采购数量单位
        const pricingUOM = defaultPurchasingCostUOM || inventoryUOM // 采购价格单位

        newSelectRows.push({
          UOM,
          wholesaleItemId: item.wholesaleItemId,
          variety,
          brand: '',
          extraOrigin: '',
          quantity: currentRow.quantity,
          weight: quantityToQuantity(currentRow.quantity, UOM, pricingUOM, item, 12),
          cost: basePriceToRatioPrice(pricingUOM, cost, item),
          inventoryUOM,
          baseUOM,
          constantRatio,
          ratioUOM,
          SKU,
          supplier: suppliers ? suppliers.split(', ') : [],
          wholesaleProductUomList,
          defaultPurchaseUnitUOM,
          pricingUOM,
        })
      })
    }
    this.setState({ dataSource: newSelectRows, showItems: true })
  }

  getFormattedItem = (value: SelectValue): Promise<SaleItem> => {
    return new Promise((resolve, reject) => {
      const regExp = /^[0-9]+$/
      if (regExp.test(value as string)) {
        const { saleItems, sellerSetting } = this.props
        const defaultCostType = sellerSetting.company ? sellerSetting.company.defaultCostType : 1
        const found = saleItems.find((item) => item.wholesaleItemId == value)
        if (found) {
          const {
            variety,
            suppliers,
            constantRatio,
            quantity,
            cost,
            ratioUOM,
            inventoryUOM,
            baseUOM,
            wholesaleProductUomList,
            defaultPurchasingCostUOM,
            defaultPurchaseUnitUOM,
            SKU,
          } = found

          const UOM = defaultPurchaseUnitUOM || inventoryUOM // 采购数量单位
          const pricingUOM = defaultPurchasingCostUOM || inventoryUOM // 采购价格单位
          const newItem = {
            UOM,
            wholesaleItemId: value,
            variety: formatItemDescription(variety, SKU, sellerSetting),
            brand: '',
            extraOrigin: '',
            quantity: '',
            weight: 0,
            cost: basePriceToRatioPrice(pricingUOM, cost, found),
            inventoryUOM,
            baseUOM,
            constantRatio,
            ratioUOM,
            SKU,
            supplier: suppliers ? suppliers.split(', ') : [],
            wholesaleProductUomList,
            defaultPurchaseUnitUOM,
            pricingUOM,
          }
          if (defaultCostType != 1) {
            this.getCostByItemIdAndClientId(value, (orderItems: any) => {
              const clientCost = isArray(orderItems) && orderItems[0] ? orderItems[0].cost : 0
              newItem.cost = basePriceToRatioPrice(pricingUOM, clientCost, found)
              resolve(newItem)
            })
          } else {
            resolve(newItem)
          }
        } else {
          reject()
        }
      } else if (typeof value === 'string' && value.startsWith('extra_')) {
        const valueStr = value + ''
        const extraChargeId = parseInt(valueStr.replace('extra_', ''), 10)
        const extraCharges = this.props.companyProductTypes?.extraCharges ?? []
        const found = extraCharges.find((item) => item.id === extraChargeId)
        if (found) {
          const { id, name } = found
          resolve({
            wholesaleItemId: id,
            variety: `${name} [Extra charge]`,
            itemName: name,
            brand: '',
            extraOrigin: '',
            quantity: '',
            weight: 0,
            cost: 0,
            inventoryUOM: 'Each',
            baseUOM: 'Each',
            constantRatio: false,
            ratioUOM: 'Each',
            SKU: '',
            supplier: [],
            wholesaleProductUomList: [],
            pricingUOM: 'Each',
            type: 'extra',
          })
        } else {
          reject()
        }
      } else {
        reject()
      }
    })
  }

  onItemSelect = (value: SelectValue) => {
    const { dataSource } = this.state
    this.getFormattedItem(value)
      .then((res) => {
        this.setState({ dataSource: [...dataSource, res], productSKU: [] }, () => {
          setTimeout(() => {
            jQuery('.ant-modal-body .ant-table-tbody .quantity-input')
              .last()
              .focus()
          }, 100)
          initHighlightEvent()
        })
      })
      .catch((e) => {
        console.log(e)
      })
  }

  onChange = (value: any, type: string, index: number, fromKeyEvent: boolean = false) => {
    const copy = cloneDeep(this.state.dataSource)
    const record = this.state.dataSource[index]
    if (record.type === 'extra' && type === 'quantity') {
      copy[index].weight = value
    } else if (type === 'quantity' && record.constantRatio === true) {
      copy[index].weight = baseQtyToRatioQty(
        record.pricingUOM,
        ratioQtyToBaseQty(record.UOM, value, record, 12),
        record,
        12,
      )
    }
    copy[index][type] = value
    this.setState({ dataSource: copy })
  }

  onChangePricingUom = (wholesaleItemId: number, value: any, index: number, fromKeyEvent: boolean = false) => {
    const copy = cloneDeep(this.state.dataSource)
    // @ts-ignore
    copy[index]['pricingUOM'] = value
    if (copy[index].quantity > 0) {
      copy[index].weight = baseQtyToRatioQty(
        value,
        ratioQtyToBaseQty(copy[index].UOM, copy[index].quantity, copy[index], 12),
        copy[index],
        12,
      )
    }
    this.setState({ dataSource: copy })
  }

  openAddNewBrandModal = (index: number) => {
    if (this.state.dataSource.length) {
      this.setState({
        openAddNewBrandModal: !this.state.openAddNewBrandModal,
        addNewBrandItemIndex: index,
      })
    }
  }

  // empty implement
  onChangeBrandList = () => {}

  updateDatasource = (data: string[]) => {
    const { dataSource, addNewBrandItemIndex } = this.state
    if (dataSource[addNewBrandItemIndex] != null) {
      dataSource[addNewBrandItemIndex].suppliers = data.join(',')
    }
    this.setState({
      dataSource,
      openAddNewBrandModal: false,
    })
  }

  getCostByItemIdAndClientId = (itemId: any, cb: Function) => {
    const { vendorId } = this.state
    const _this = this
    this.changeLoadingStatus(true)
    OrderService.instance.getItemCostByItemIdAndClientId({ itemIds: itemId, clientId: vendorId }).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        cb(res.body.data)
      },
      error(err) {
        checkError(err)
      },
      complete() {
        _this.changeLoadingStatus(false)
      },
    })
  }

  changeLoadingStatus = (flag: boolean) => {
    this.setState({
      loading: flag,
    })
  }

  openAddNewCOOModal = (index: number) => {
    this.setState(
      {
        visibleCOOModal: !this.state.visibleCOOModal,
      },
      () => {
        if (!this.state.visibleCOOModal) {
          this.props.getCompanyProductAllTypes()
        }
      },
    )
  }

  onChangeUom = (wholesaleItemId: number, value: any, index: number, fromKeyEvent: boolean = false) => {
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    cloneData[index]['UOM'] = value
    if (cloneData[index].quantity > 0) {
      cloneData[index].weight = baseQtyToRatioQty(
        cloneData[index].pricingUOM,
        ratioQtyToBaseQty(value, cloneData[index].quantity, cloneData[index], 12),
        cloneData[index],
        12,
      )
    }

    this.setState({
      dataSource: cloneDeep(cloneData),
    })
  }

  onDelete = (itemId: number, index: number) => {
    const copy = cloneDeep(this.state.dataSource)
    copy.splice(index, 1)
    this.setState({
      dataSource: copy,
    })
  }

  onPageChange = (page: number) => {
    this.setState({ currentPage: page })
  }

  onDateChange = (date: Moment | null) => {
    if (date != null) {
      this.setState({
        deliverDate: date,
      })
    }
  }

  onNoChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let { value } = event.target
    if (value == null) {
      value = ''
    }
    this.setState({
      referenceNo: value,
    })
  }

  onEnterPurchase = (status: string) => {
    this.setState({ status })
    const { dataSource, vendorId, deliverDate, referenceNo } = this.state
    let data: any[] = []
    let chargeItems: any[] = []
    let valid = true
    const extraCOO = this.props.companyProductTypes ? this.props.companyProductTypes.extraCOO : []
    dataSource.map((item) => {
      const {
        wholesaleItemId,
        quantity,
        cost,
        weight,
        brand,
        constantRatio,
        UOM,
        extraOrigin,
        pricingUOM,
        wholesaleProductUomList,
        inventoryUOM,
        SKU,
        fromKeyEvent,
        type,
      } = item
      const origin = extraCOO.find((el) => el.id == extraOrigin)
      let row = {
        wholesaleItemId: wholesaleItemId,
        quantity:
          quantity > 0
            ? baseQtyToRatioQty(item.inventoryUOM, ratioQtyToBaseQty(item.UOM, quantity, item, 12), item, 12)
            : 0,
        cost: constantRatio ? ratioPriceToBasePrice(item.pricingUOM, cost, item, 12) : cost,
        provider: brand,
        modifiers: brand,
        extraOrigin: fromKeyEvent ? extraOrigin : origin ? origin.name : '',
        UOM: UOM,
        pricingUOM,
        SKU,
      }
      let tempData = { ...row, wholesaleProductUomList: wholesaleProductUomList, inventoryUOM: inventoryUOM }

      if (!judgeConstantRatio(tempData)) {
        row.weight = ratioQtyToInventoryQty(item.pricingUOM, weight, item, 12)
      }
      if (row.quantity === '') {
        valid = false
      }
      if (type == 'extra') {
        let chargeRow: any = {
          itemName: item.itemName,
          quantity: item.quantity,
          uom: item.UOM,
          price: item.cost,
        }
        chargeItems.push(chargeRow)
      } else {
        data.push(row)
      }
    })
    if (!valid) {
      notification.error({
        message: 'Error',
        description: 'Please enter a quantity value',
      })
      return
    }
    this.props.onEnterPurchase(vendorId, {
      deliveryDate: deliverDate.format('MM/DD/YYYY'),
      status,
      po: referenceNo,
      orderItemList: data,
      chargeItemList: chargeItems,
    })
  }

  onReset = () => {
    this.setState({
      dataSource: [],
      vendorId: null,
      productSKU: [],
      vendorOptions: [],
      referenceNo: '',
      deliverDate: moment(),
      merged: false,
    })
  }

  render() {
    const { visible, onVisibleChange, companyProductTypes, vendors, createPOLoading } = this.props
    const { vendorId, dataSource, openAddNewBrandModal, addNewBrandItemIndex, vendorOptions, showItems } = this.state
    const selectedVendor = vendors.find((el) => el.clientId == vendorId)
    // @ts-ignore
    const mainBillingAddress =
      selectedVendor && selectedVendor.mainBillingAddress ? selectedVendor.mainBillingAddress.address : null
    const paymentAddress = formatAddress(mainBillingAddress)

    const actualDataSource = [
      ...dataSource,
      {
        wholesaleItemId: -1,
        vareity: '',
        brand: '',
        extraOrigin: '',
        quantity: '',
        cost: 0,
        inventoryUOM: '',
        baseUOM: '',
        weight: 0,
        constantRatio: false,
        ratioUOM: 1,
        supplier: [],
        SKU: '',
      },
    ]

    return (
      <>
        <NoBottomPaddingModal
          title={'Enter Purchase'}
          visible={visible}
          width={1440}
          onOk={() => onVisibleChange(false)}
          onCancel={() => onVisibleChange(false)}
          maskClosable={false}
          footer={
            <ModalFooterContainer>
              <ThemeButton
                type={'primary'}
                key={'enter-purchase'}
                loading={createPOLoading && this.state.status === 'RECEIVED'}
                onClick={() => this.onEnterPurchase('RECEIVED')}
                disabled={vendorId == null || dataSource.length === 0}
              >
                Save &amp; Receive
              </ThemeButton>
              <ThemeButton
                type={'primary'}
                key={'save'}
                loading={createPOLoading && this.state.status === 'CONFIRMED'}
                onClick={() => this.onEnterPurchase('CONFIRMED')}
                disabled={vendorId == null || dataSource.length === 0}
              >
                Save &amp; Confirm
              </ThemeButton>
              <ThemeButton
                type={'primary'}
                key={'cancel'}
                style={{ marginRight: 'auto' }}
                onClick={() => onVisibleChange(false)}
              >
                Cancel
              </ThemeButton>
            </ModalFooterContainer>
          }
        >
          <EnterPurchaseWrapper>
            <EnterPurchaseTip>
              Enter the items received in the warehouse below. To start from a purchase order,{' '}
              <a href="javascript:void 0;">click here</a>
            </EnterPurchaseTip>
            <Flex>
              <div className="vendor select-vendor" style={{ marginBottom: 20 }}>
                <label>Vendor</label>
                <AutoComplete
                  size="large"
                  placeholder="Select a vendor"
                  dataSource={vendorOptions}
                  style={{ marginBottom: 4 }}
                  onSelect={this.onVendorSelect}
                  onFocus={() => this.onVendorSearch('')}
                  onSearch={this.onVendorSearch}
                  onChange={this.onVendorChange}
                  dropdownClassName="vendor-autocomplete"
                />
                {mainBillingAddress && (
                  <ValueLabel className="small black" style={{ marginBottom: 20 }}>
                    {paymentAddress}
                  </ValueLabel>
                )}
              </div>
              <div className="vendor" style={{ marginLeft: 20 }}>
                <label>Delivery Date</label>
                <DatePicker
                  className="datepicker-container"
                  allowClear={false}
                  onChange={this.onDateChange}
                  value={this.state.deliverDate}
                  size="large"
                  placeholder="MM/DD/YYYY"
                  format="MM/DD/YYYY"
                  suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
                />
              </div>
              <div className={'vendor'} style={{ marginLeft: 20 }}>
                <label>Vendor Reference No.</label>
                <ThemeInput
                  maxLength={25}
                  className="reference-no"
                  value={this.state.referenceNo}
                  onChange={this.onNoChange}
                />
              </div>
            </Flex>
            {showItems ? (
              <ThemeSpin spinning={this.state.loading}>
                <ThemeTable
                  dataSource={actualDataSource}
                  // rowKey={"wholesaleItemId"}
                  columns={this.columns}
                  pagination={{
                    hideOnSinglePage: true,
                    pageSize: 12,
                    current: this.state.currentPage,
                    onChange: this.onPageChange,
                  }}
                />
              </ThemeSpin>
            ) : null}
          </EnterPurchaseWrapper>
        </NoBottomPaddingModal>
        {addNewBrandItemIndex !== -1 ? (
          <AddNewBrandModal
            visible={openAddNewBrandModal}
            companyProductTypes={companyProductTypes}
            itemName={dataSource[addNewBrandItemIndex].variety}
            itemId={dataSource[addNewBrandItemIndex].wholesaleItemId}
            oldBrands={dataSource[addNewBrandItemIndex].suppliers?.split(',') ?? []}
            updateProduct={this.props.updateProduct}
            setCompanyProductTypes={this.props.setCompanyProductType}
            onVisibleChange={this.openAddNewBrandModal}
            onChangeBrandList={this.onChangeBrandList}
            updateDatasource={this.updateDatasource}
          />
        ) : null}
        <ThemeModal
          title={`Edit Value List "Origin"`}
          visible={this.state.visibleCOOModal}
          onCancel={this.openAddNewCOOModal}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="extraCOO" title="Origin" buttonTitle="Add Origin" />
        </ThemeModal>
      </>
    )
  }
}

// only used in restock page
export const WrappedEnterPurchaseModal = Form.create<EnterPurchaseModalProps>()(EnterPurchaseModal)
