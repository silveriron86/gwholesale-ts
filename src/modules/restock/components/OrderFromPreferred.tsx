import { Modal } from 'antd'
import React, { useState } from 'react'
import styled from '@emotion/styled'
import { RestockService } from '../restock.service'
import { responseHandler, checkError } from '~/common/utils'
import { of } from 'rxjs'

export default function OrderFromPreferred(props: any) {
  const { visible, onCancel, selectedRows, deliveryDate, onRowSelected } = props
  const [loading, setLoading] = useState(false)

  const noPreferList: any = []
  const selectedMap = selectedRows.reduce((pre: any, cur: any) => {
    const { preferredVendorId } = cur
    if (!preferredVendorId) {
      noPreferList.push(cur)
      return pre
    }
    if (!pre[preferredVendorId]) {
      pre[preferredVendorId] = []
    }
    pre[preferredVendorId].push(cur)
    return pre
  }, {})

  const onOK = () => {
    const orderList = Object.keys(selectedMap).map((vendorId) => {
      return {
        wholesaleClientId: vendorId,
        itemList: selectedMap[vendorId].map((item: any) => ({
          wholesaleItemId: item.wholesaleItemId,
          quantity: item.quantity,
        })),
      }
    })

    setLoading(true)
    RestockService.instance.createPOByPreferred({ deliveryDate, orderList }).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        onRowSelected(noPreferList)
        onCancel()
        res.body.data.forEach((item) => {
          const href = window.location.href
          window.open(
            href.replace(
              href.substring(href.indexOf('#'), href.length),
              `#/order/${item.wholesaleOrderId}/purchase-cart`,
            ),
            '_blank',
          )
        })
      },
      error(err) {
        console.log(err)
        checkError(err)
      },
      complete() {
        setLoading(false)
      },
    })
  }

  return (
    <StyledModal
      title="Order from preferred vendors"
      visible={visible}
      onCancel={onCancel}
      okText="Create orders"
      onOk={onOK}
      okButtonProps={{
        disabled: Object.keys(selectedMap).length <= 0,
        loading,
      }}
    >
      <div className="modal-content">
        <div className="modal-title">Based on the selected items' preferred vendor settings:</div>
        <ul>
          {Object.keys(selectedMap).map((key) => {
            const arr = selectedMap[key]
            const preferredVendorName = arr[0]?.preferredVendorName
            return (
              <li>
                {arr.length} items will be ordered from <b>{preferredVendorName}</b>
              </li>
            )
          })}
          {noPreferList.length > 0 && (
            <li>
              {noPreferList.length} items <b>do not have preferred vendors</b> and will not be included in any purchase
              orders:&nbsp;
              {noPreferList.length < 6
                ? `${noPreferList
                    .map((item: any) => {
                      console.log(11, item, item.preferredVendorName)
                      return item.variety
                    })
                    .join(', ')}.`
                : `
                  ${noPreferList
                    .slice(0, 5)
                    .map((item: any) => item.variety)
                    .join(', ')} and ${noPreferList.length - 5} more items.
                `}
            </li>
          )}
        </ul>
      </div>
    </StyledModal>
  )
}

const StyledModal = styled(Modal)`
  .modal-content {
    font-weight: normal;
  }

  .modal-title {
    margin-bottom: 15px;
    font-weight: bold;
  }
`
