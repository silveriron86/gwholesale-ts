import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { RestockModule, RestockStateProps } from '../restock.module'
import { NoBottomPaddingModal } from '~/modules/restock/restock.style'
import { ThemeTable } from '~/modules/customers/customers.style'
import styled from '@emotion/styled'
import { mathRoundFun } from '~/common/utils'
import { notification } from 'antd'
import { RestockService } from '~/modules/restock/restock.service'
import { of } from 'rxjs'
import { checkError, responseHandler } from '~/common/utils'
import lodash from 'lodash'
import moment from 'moment'

interface PurhcaseOrderModalProps {
  visible: boolean
  addItemLoading: boolean
  openPOTotal: any
  openPOList: any
  selectedRows: any
  addItemsForPO: (params: any) => void
  getOpenPOList: (params: any) => void
  onVisibleChange: (visible: boolean) => void
  resetAddItemLoading: () => void
  handleClearSelectedRowKeys: Function
}

export class CreateOrderModal extends React.PureComponent<PurhcaseOrderModalProps> {
  state = {
    currentPage: 0,
    pageSize: 5,
  }

  componentDidMount() {
    this.props.getOpenPOList({ currentPage: this.state.currentPage, pageSize: this.state.pageSize })
  }

  cancelAddItemForPO = () => {
    const { onVisibleChange } = this.props
    onVisibleChange(false)
  }

  changePage = (value: any) => {
    this.setState({
      currentPage: value,
    })
    this.props.getOpenPOList({ currentPage: value - 1, pageSize: this.state.pageSize })
  }

  clickRow = lodash.debounce((row: any) => {
    const { selectedRows, onVisibleChange, resetAddItemLoading, handleClearSelectedRowKeys } = this.props
    let itemList = selectedRows.map((item: any) => {
      return {
        wholesaleItemId: item.wholesaleItemId,
        quantity: item.quantity || 0
      }
    })
    resetAddItemLoading()
    RestockService.instance.addItemsForPO(row.wholesaleOrderId, itemList).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        if (res.statusCodeValue == 200) {
          handleClearSelectedRowKeys()
          notification.success({
            message: 'Success',
            description: (
              <a href={`#/order/${row.wholesaleOrderId}/purchase-cart`}>
                Items successfully added. Go to [{row.wholesaleOrderId}]
              </a>
            ),
            duration: 5
          })
        }
      },
      error(err) {
        checkError(err)
      },
      complete() {
        resetAddItemLoading()
        onVisibleChange(false)
      },
    })
  }, 200)

  render() {
    const { visible, openPOTotal, openPOList, addItemLoading } = this.props
    const { currentPage, pageSize } = this.state

    const columns = [
      {
        title: 'ORDER NO.',
        align: 'left',
        dataIndex: 'wholesaleOrderId',
        render: (t: any) => `${t}`,
      },
      {
        title: 'VENDOR',
        align: 'left',
        dataIndex: 'vendorName',
      },
      {
        title: 'DELIVERY DATE',
        align: 'left',
        dataIndex: 'deliveryDate',
        render: (t: any) => {
          return moment(t).format('MM/DD/YYYY')
        },
      },
      {
        title: 'STATUS',
        align: 'left',
        dataIndex: 'isLocked',
        render: (t: any) => <Status isLocked={t}>{t ? 'Closed' : 'Open'}</Status>,
      },
      {
        title: 'CHARGE TOTAL',
        align: 'right',
        dataIndex: 'totalCost',
        render: (value: any) => `$${value ? mathRoundFun(value, 2) : 0}`,
      },
    ]

    return (
      <NoBottomPaddingModal
        visible={visible}
        width={1000}
        title={'Select purchase order'}
        onCancel={this.cancelAddItemForPO}
        footer={[]}
      >
        <Table
          dataSource={openPOList}
          // loading={loading}
          columns={columns}
          bordered={false}
          onRow={(record) => {
            return {
              onClick: (event) => this.clickRow(record),
            }
          }}
          pagination={{
            total: openPOTotal,
            current: currentPage,
            pageSize: pageSize,
            onChange: this.changePage,
          }}
          rowKey="wholesaleOrderId"
          loading={addItemLoading}
        />
      </NoBottomPaddingModal>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.restock
export const WrappedPurchaseOrderListModal = connect(RestockModule)(mapStateToProps)(CreateOrderModal)

const Status = styled.div<{ isLocked: boolean }>`
  border: 1px solid #ccc;
  border-radius: 15px;
  text-align: center;
  position: relative;
  padding: 4px;
  ::after {
    position: absolute;
    content: '';
    left: 8px;
    top: 12px;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    background: ${(props) => (props.isLocked ? '#1c6e31' : '#82898a')};
  }
`

const Table = styled(ThemeTable)`
  .ant-table-pagination {
    width: 100%;
    text-align: center;
    float: none;
    margin: 34px 0 10px 0;
  }
  .ant-table-thead > tr > th {
    border-bottom: 3px solid #d7d7d7;
    padding: 16px 9px;
  }
  .ant-table-tbody > tr > td {
    background: #fff !important;
    border-bottom: 1px solid #d7d7d7;
  }
`
