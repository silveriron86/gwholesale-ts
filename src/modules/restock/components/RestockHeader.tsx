import {
  ButtonGroup,
  CardAndButtonGroupContainer,
  CardContainer,
  CardOption,
  OptionsContainer,
  SelectBox,
  SelectContainer,
  StyledSearch,
} from '~/modules/restock/restock.style'
import { Button, Checkbox, Select, Dropdown, Menu, Icon } from 'antd'
import React, { ChangeEvent } from 'react'
import { QueryParams, Type } from '~/modules/restock/restock.service'
import { CheckboxChangeEvent } from 'antd/es/checkbox'
import { Category, SumInfo } from '~/modules/restock/restock.module'
import { CascaderOptionType } from 'antd/lib/cascader'
import { ThemeButton } from '~/modules/customers/customers.style'
import { cloneDeep, isEmpty } from 'lodash'
import { BriefClient } from '~/schema'

interface RestockHeaderProps {
  searchKey?: string
  active?: number
  onSearch?: () => void
  sumInfo?: SumInfo
  statuses?: string
  managerId?: number|string
  type?: Type
  onShowPreferModal: () => void
  onCreateOrder: () => void
  onEnterPurchase: () => void
  onQueryParamsChange?: (params: QueryParams) => void
  fetchSumInfo?: (params: any) => void
  onShowAddOrderItemModal: (visiable: boolean) => void
  categories: Category[]
  categoryIds?: string
  selectedRows: any
  vendorList: BriefClient[]
  companyPmUsers: any[]
  vendorId?: string
  dateChecked?: false
  fullfillDate?: number
  presets: any
}

interface IState {
  categoryOptions: CascaderOptionType[]
  statusOptions: { label: string; value: any }[]
}

const { Option, OptGroup } = Select

const options = [
  { label: 'Available', value: 4 },
  { label: 'Below par', value: 3 },
  { label: 'Oversold', value: 1 },
  { label: 'Out', value: 2 },
]

export class RestockHeader extends React.PureComponent<RestockHeaderProps, IState> {
  state = {
    categoryOptions: [] as CascaderOptionType[],
    statusOptions: [] as { label: string; value: any }[],
  }

  onSearchKeyChange = (event: ChangeEvent<HTMLInputElement>) => {
    this.props.onQueryParamsChange?.({ searchKey: event.target.value })
  }

  onActiveChange = (event: CheckboxChangeEvent) => {
    this.props.onQueryParamsChange?.({ active: Number(!event.target.checked) })
    this.props.fetchSumInfo?.({ active: Number(!event.target.checked) })
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    if (prevProps.categories !== this.props.categories) {
      this.onCategorySearch()
    }
    return null;
  }

  onCategorySearch = (value: string = '') => {
    const { categories } = this.props
    if (!categories.length) {
      this.setState({
        categoryOptions: [],
      })
      return
    }
    const record: Record<string, CascaderOptionType> = {}
    let sectionId: number = -1
    categories
      .filter((item) => item.name.toLowerCase().includes(value.toLowerCase()))
      .forEach((item) => {
        sectionId = item.wholesaleSection.wholesaleSectionId
        if (!record.hasOwnProperty(sectionId + '')) {
          record[sectionId] = {
            value: sectionId + '',
            label: item.wholesaleSection.name,
            children: [],
          }
        }
        record[sectionId].children!!.push({
          value: item.wholesaleCategoryId + '',
          label: item.name,
        })
      })
    const categoryOptions: CascaderOptionType[] = []
    Object.keys(record).forEach((key) => {
      categoryOptions.push(record[key])
    })
    this.setState({
      categoryOptions: cloneDeep(categoryOptions),
    })
  }

  onCategoryChange = (categoryIds: string[]) => {
    this.props.onQueryParamsChange?.({ categoryIds: categoryIds.length ? categoryIds.join(',') : undefined })
  }

  onSearch = () => {
    this.props.onSearch?.()
  }

  onTypeChange = (type: Type) => {
    this.props.onQueryParamsChange?.({ type })
  }

  onStatusChange = (statuses: number[]) => {
    this.props.onQueryParamsChange?.({ statuses: statuses.length ? statuses.join(',') : undefined })
  }

  onManagerChange = (managerId: any) => {
    this.props.onQueryParamsChange?.({managerId: managerId || undefined})
  }

  onVendorChange = (vendorId: number[]) => {
    this.props.onQueryParamsChange?.({ vendorId: vendorId.length ? vendorId.join(',') : undefined })
  }

  onDateCheckedChange = (e: any) => {
    const { checked } = e.target
    this.props.onQueryParamsChange?.({dateChecked: checked})
  }

  onFullfillDateChange = (val:number) => {
    this.props.onQueryParamsChange?.({fullfillDate: val})
  }

  onActions = (item: any) => {
    const { key } = item
    if (key === '1') {
      this.props.onShowAddOrderItemModal(true)
    } else if (key === '2') {
      this.props.onEnterPurchase()
    } else if (key === '3') {
      this.props.onShowPreferModal()
    }
  }

  onClearType = (e: React.MouseEvent) => {
    e.stopPropagation()
    this.props.onQueryParamsChange?.({ type: undefined })
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  render() {
    const { presets, onCreateOrder, searchKey, active, sumInfo, statuses, categoryIds, type, selectedRows, vendorList, companyPmUsers, managerId, vendorId, dateChecked, fullfillDate } = this.props
    const { categoryOptions } = this.state

    return (
      <OptionsContainer>
        <StyledSearch
          style={{ width: 320 }}
          allowClear
          enterButton
          value={searchKey}
          onChange={this.onSearchKeyChange}
          onSearch={this.onSearch}
          placeholder={'Search by product name or SKU'}
        />
        <Checkbox style={{ marginLeft: 30, width: 196 }} onChange={this.onActiveChange} checked={Boolean(!active)} value={Boolean(active)}>
          Show inactive
        </Checkbox>

        <SelectContainer>
          <SelectBox style={{ marginLeft: 0 }}>
            <div className={'label'}>Category</div>
            <Select
              style={{ width: 220 }}
              mode={'multiple'}
              value={categoryIds?.split(',') || []}
              placeholder={'All categories'}
              onChange={this.onCategoryChange}
              onFocus={() => this.onCategorySearch('')}
              onSearch={this.onCategorySearch}
              filterOption={() => true}
            >
              {categoryOptions.sort((a: any, b: any) => this.onSortString('label', a, b)).map((group) => (
                <OptGroup label={group.label} key={group.value}>
                  {group.children?.sort((a: any, b: any) => this.onSortString('label', a, b)).map((opt) => (
                    <Option value={opt.value} key={opt.value}>
                      {opt.label}
                    </Option>
                  ))}
                </OptGroup>
              ))}
            </Select>
          </SelectBox>
          <SelectBox>
            <div className={'label'}>Status</div>
            <Select
              style={{ width: 220 }}
              value={statuses?.split(',')?.map((item) => parseInt(item)) || []}
              mode={'multiple'}
              placeholder={'All statues'}
              onChange={this.onStatusChange}
              // onSearch={this.onStatusSearch}
              filterOption={(input: string, option: any) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {options.map((item) => (
                <Option value={item.value} key={item.value}>
                  {item.label}
                </Option>
              ))}
            </Select>
          </SelectBox>
          <SelectBox>
            <div className="label">
              Vendor
            </div>
            <Select
              optionFilterProp="children"
              mode="multiple"
              style={{ width: 220 }}
              placeholder="All vendors"
              value={vendorId?.split(',')?.map(item => parseInt(item)) || []}
              onChange={this.onVendorChange}
            >
              {vendorList.sort((a: any, b: any) => this.onSortString('clientCompanyName', a, b)).map(item => (
                <Option key={item.clientId} value={item.clientId}>
                  {item.clientCompanyName}
                </Option>
              ))}
            </Select>
          </SelectBox>
          <SelectBox>
            <div className={'label'}>Product Manager</div>
            <Select
              showSearch
              style={{ width: 220 }}
              value={managerId}
              placeholder={'All'}
              onChange={this.onManagerChange}
              filterOption={(input: string, option: any) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              <Option key="all" value="">All</Option>
              {(companyPmUsers || []).sort((a: any, b: any) => `${a.firstName} ${a.lastName}`.localeCompare(`${b.firstName} ${b.lastName}`)).map((item) => (
                <Option value={item.userId} key={item.userId}>
                  {item.firstName + '  ' + item.lastName}
                </Option>
              ))}
            </Select>
          </SelectBox>
        </SelectContainer>
        <div style={{margin: '20px 0 0 350px', display: 'flex', alignItems: 'center'}}>
          <Checkbox style={{width:196}} onChange={this.onDateCheckedChange} checked={dateChecked} >
            Show items ordered on
          </Checkbox>
          <SelectBox style={{marginLeft:0}}>
            <div className="label">
              Fulfillment date
            </div>
            <Select
              style={{ width: 220 }}
              value={fullfillDate}
              onChange={this.onFullfillDateChange}
            >
              {presets.map(item => (
                <Option key={item.key} value={item.key}>
                  {item.label}
                </Option>
              ))}
            </Select>
          </SelectBox>
        </div>
        <CardAndButtonGroupContainer>
          <CardContainer>
            <CardOption selected={type === 1} onClick={() => this.onTypeChange(1)}>
              <span className={'title'}>Out of stock</span>
              <div>
                <span className={'content'}>{sumInfo?.outOfStockCount ?? 0} items</span>
                <span className={'clear'} onClick={this.onClearType}>
                  Clear
                </span>
              </div>
            </CardOption>
            <CardOption selected={type === 2} onClick={() => this.onTypeChange(2)}>
              <span className={'title'}>Oversold</span>
              <div>
                <span className={'content'}>{sumInfo?.oversoldCount ?? 0} items</span>
                <span className={'clear'} onClick={this.onClearType}>
                  Clear
                </span>
              </div>
            </CardOption>
            <CardOption selected={type === 3} onClick={() => this.onTypeChange(3)}>
              <span className={'title'}>Need to be reordered</span>
              <div>
                <span className={'content'}>{sumInfo?.needToReorderedCount ?? 0} items</span>
                <span className={'clear'} onClick={this.onClearType}>
                  Clear
                </span>
              </div>
            </CardOption>
          </CardContainer>
          <ButtonGroup>
            {/* <Button type={'primary'} onClick={onCreateOrder}>
              Create order
            </Button>
            <Button type={'primary'} style={{ marginLeft: 20 }} onClick={onEnterPurchase}>
              Enter purchase
            </Button> */}
            <ThemeButton
              size="large"
              type="primary"
              style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }}
              onClick={onCreateOrder}
            >
              Create purchase order
            </ThemeButton>
            <Dropdown
              overlay={
                <Menu onClick={this.onActions}>
                  <Menu.Item key="1" disabled={isEmpty(selectedRows)}>Add to existing order</Menu.Item>
                  <Menu.Item key="2">Enter purchase</Menu.Item>
                  <Menu.Item key="3" disabled={isEmpty(selectedRows)}>Order from preferred vendor</Menu.Item>
                </Menu>
              }
              trigger="click"
              placement="bottomRight"
            >
              <ThemeButton
                size="large"
                type="primary"
                style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
              >
                <Icon type="down" />
              </ThemeButton>
            </Dropdown>
          </ButtonGroup>
        </CardAndButtonGroupContainer>
      </OptionsContainer>
    )
  }
}
