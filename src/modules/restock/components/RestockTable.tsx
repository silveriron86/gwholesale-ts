import React from 'react'
import { ColumnProps } from 'antd/es/table'
import { Badge, Icon, InputNumber, Select, Table, Tooltip } from 'antd'
import { ResTockTable, TableContainer, WeekTitleContainer } from '~/modules/restock/restock.style'
import { RestockItem } from '~/modules/restock/restock.module'
import { StatusButton } from '~/components/button'
import { Theme } from '~/common'
import { withTheme } from 'emotion-theming'
import { PaginationConfig } from 'antd/lib/pagination'
import { SorterResult, TableCurrentDataSource } from 'antd/lib/table/interface'
import { Direction, RowKey } from '~/modules/restock/restock.service'
import { ThemeLink, ThemeTable } from '~/modules/customers/customers.style'
import InventoryAsterisk from '~/components/inventory/inventoryAsterisk'
import { handlerNoLotNumber, mathRoundFun } from '~/common/utils'
import _ from 'lodash'
import moment from 'moment'
import styled from '@emotion/styled'

const StyledNameLink = styled(ThemeLink)`
  max-width: 100%;
  overflow: hidden;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
`

const StyledDiv = styled.div`
  max-width: 100%;
  overflow: hidden;
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
`

interface RestockTableProps {
  dataSource?: RestockItem[]
  total?: number
  page?: number
  pageSize?: number
  theme: Theme
  loading?: boolean
  changeItem?: (record: RestockItem) => void
  onTableChange?: (orderBy: RowKey | undefined, direction: Direction, page?: number, pageSize?: number) => void
  onRowSelected: (selectedRow: RestockItem[]) => void
  onOrder?: (wholesaleItemId: number) => void
  selectedRows: RestockItem[]
}

interface IState {}

const { Option } = Select

export class RestockTableContainer extends React.PureComponent<RestockTableProps, IState> {
  state = {}

  columns: ColumnProps<RestockItem>[] = [
    {
      title: 'Product',
      dataIndex: 'variety',
      sorter: true,
      width: 200,
      align: 'left',
      render: (text, record) => {
        const datestart = moment().format('MMDDYY')
        const dateend = moment()
          .add(30, 'days')
          .format('MMDDYY')
        return (
          <StyledNameLink to={`/product/${record.wholesaleItemId}/activity?datestart=${datestart}&dateend=${dateend}`}>
            <Tooltip title={text}>{text}</Tooltip>
          </StyledNameLink>
        )
      },
    },
    {
      title: 'SKU',
      dataIndex: 'SKU',
      width: 90,
      ellipsis: true,
      render: (val) => <Tooltip title={val}>{val}</Tooltip>,
    },
    {
      title: 'Category',
      dataIndex: 'categoryName',
      width: 80,
      ellipsis: true,
      render: (val) => <Tooltip title={val}>{val}</Tooltip>,
    },
    {
      title: 'Active',
      width: 115,
      render: (_, record) => (
        <Select
          value={String(record.active)}
          style={{ width: 100 }}
          onChange={(active: string) =>
            this.changeActive({
              ...record,
              active: active === 'true',
            })
          }
        >
          <Option value={'true'}>Active</Option>
          <Option value={'false'}>Inactive</Option>
        </Select>
      ),
    },
    {
      title: 'Product Manager',
      dataIndex: 'managerId',
      width: 148,
      render: (_, record) => `${record.firstName || ''} ${record.LastName || ''}`,
    },
    {
      title: 'UOM',
      dataIndex: 'inventoryUOM',
      width: 75,
    },
    {
      title: 'Status',
      sorter: true,
      width: 120,
      render: (_, record) => this.renderStatus(record),
    },
    {
      title: 'Weekly usage',
      dataIndex: 'weeklyUsage',
      sorter: true,
      width: 80,
    },
    {
      title: (
        <WeekTitleContainer>
          <span>Weeks</span>
          <Tooltip title={'Projected weeks of available inventory based on run rate from past 7 days'}>
            <Icon type={'info-circle'} />
          </Tooltip>
        </WeekTitleContainer>
      ),
      dataIndex: 'weeks',
      sorter: true,
      width: 90,
    },
    {
      title: 'Par / Reorder',
      dataIndex: 'reorderLevel',
      sorter: true,
      width: 80,
      render: (_, record) => {
        return (
          (record.reorderLevel == undefined ? 0 : record.reorderLevel).toString() +
          '/' +
          (record.reorderQuantity == undefined ? 0 : record.reorderQuantity).toString()
        )
      },
    },
    {
      title: 'Committed',
      dataIndex: 'committedQty',
      sorter: true,
      width: 100,
      render: (value, row) => {
        return (
          <>
            {handlerNoLotNumber(row, 3)}
            <InventoryAsterisk item={row} flag={3}></InventoryAsterisk>
          </>
        )
      },
    },
    {
      title: 'Available to Sell',
      dataIndex: 'availableToSell',
      sorter: true,
      width: 100,
      render: (value, row) => {
        return (
          <>
            {handlerNoLotNumber(row, 1)}
            <InventoryAsterisk item={row} flag={1}></InventoryAsterisk>
          </>
        )
      },
    },
    {
      title: 'On-hand',
      dataIndex: 'onHandQty',
      sorter: true,
      width: 100,
      render: (value, row) => {
        return (
          <>
            {handlerNoLotNumber(row, 2)}
            <InventoryAsterisk item={row} flag={2}></InventoryAsterisk>
          </>
        )
      },
    },
    {
      title: 'Incoming',
      dataIndex: 'incomingQty',
      sorter: true,
      width: 90,
      render: (val) => mathRoundFun(val, 2),
    },
    {
      title: (
        <Tooltip title="Display vendors that have the item on their vendor product list. The preferred vendor is displayed first, with an asterisk.">
          Vendor
          <Icon type={'info-circle'} />
        </Tooltip>
      ),
      dataIndex: 'preferredCompanyNames',
      sorter: true,
      width: 200,
      render: (val) => (
        <StyledDiv>
          <Tooltip title={val}>{val}</Tooltip>
        </StyledDiv>
      ),
    },
    {
      title: 'Sugg. Order qty',
      width: 80,
      render: (_, record) => this.renderSuggQty(record),
    },
    {
      title: 'Order qty',
      dataIndex: 'quantity',
      width: 80,
      render: (value, row) => {
        return (
          <InputNumber
            value={value}
            style={{ width: 70 }}
            min={0}
            onChange={(val) => {
              this.onQtyChange(val, row)
            }}
            onBlur={(e) => {
              this.onQtyChange(e.target.value, row)
            }}
          />
        )
      },
    },
    {
      title: 'Actions',
      align: 'left',
      width: 80,
      render: (_, record) => <ThemeLink onClick={() => this.onOrder(record.wholesaleItemId)}>Order</ThemeLink>,
    },
  ]

  onOrder = (wholesaleItemId: number) => {
    this.props.onOrder?.(wholesaleItemId)
  }

  onBlur = (e: React.FocusEvent<HTMLInputElement>, record: RestockItem) => {
    const input = e.target as HTMLInputElement
    const num = Number(input.value)
    if (!isNaN(num)) {
      this.props.changeItem?.({ ...record, reorderLevel: num })
    }
  }

  renderSuggQty = (record: RestockItem) => {
    const restockLevel = record.reorderLevel == undefined ? 0 : record.reorderLevel
    const restockQty = record.reorderQuantity == undefined ? 0 : record.reorderQuantity

    if (record.availToSellTotal >= restockLevel) {
      return 0
    } else {
      return mathRoundFun(restockQty - record.availToSellTotal, 2)
    }
  }

  changeActive = (record: RestockItem) => {
    this.props.changeItem?.(record)
  }

  renderStatus = (record: RestockItem) => {
    const map = {
      1: {
        color: '#b50f04',
        text: 'Oversold',
      },
      2: {
        color: '#b50f04',
        text: 'Out',
      },
      3: {
        color: '#f2994a',
        text: 'Below Par',
      },
      4: {
        color: this.props.theme.primary,
        text: 'Available',
      },
    }
    const item = map[record.restockStatus]
    return (
      <StatusButton shape="round" color={item.text === 'Available' ? undefined : item.color}>
        <Badge color={item.color} /> {item.text}
      </StatusButton>
    )
  }

  onQtyChange = (val, row) => {
    const { selectedRows, onRowSelected } = this.props
    const alreadyInList = selectedRows.find((item) => item.wholesaleItemId == row.wholesaleItemId)

    if (alreadyInList) {
      if (val && val > 0) {
        // change
        onRowSelected(
          selectedRows.map((item) => (item.wholesaleItemId == row.wholesaleItemId ? { ...item, quantity: val } : item)),
        )
        return
      }
      // remove
      onRowSelected(selectedRows.filter((item) => item.wholesaleItemId != row.wholesaleItemId))
      return
    }

    if (val && val > 0) {
      // add
      onRowSelected(selectedRows.concat({ ...row, quantity: val }))
    }
  }

  handleTableChange = (
    pagination: PaginationConfig,
    filters: Partial<Record<keyof RestockItem, string[]>>,
    sorter: SorterResult<RestockItem>,
    extra: TableCurrentDataSource<RestockItem>,
  ) => {
    let orderBy: RowKey | undefined
    let direction: Direction = Direction.DESC
    if (!sorter.column?.sorter) {
      orderBy = undefined
    } else {
      if (sorter.column.title === 'Status') {
        orderBy = 'status' as RowKey
      } else {
        orderBy = sorter.columnKey as RowKey
      }
      direction = sorter.order === 'ascend' ? Direction.ASC : Direction.DESC
    }
    this.props.onTableChange?.(
      orderBy,
      direction,
      pagination.current ? pagination.current - 1 : undefined,
      pagination.pageSize,
    )
  }

  render() {
    const { dataSource = [], total = 0, page = 0, pageSize = 10, loading = false, selectedRows } = this.props

    const data = dataSource.map((v) => {
      const currentSelectedRow = selectedRows.find((d: RestockItem) => d.wholesaleItemId === v.wholesaleItemId)
      if (currentSelectedRow)
        return {
          ...v,
          quantity: _.get(currentSelectedRow, 'quantity', ''),
        }
      return v
    })

    return (
      <TableContainer>
        <ResTockTable
          size="middle"
          columns={this.columns}
          bordered
          loading={loading}
          dataSource={data}
          rowKey={'wholesaleItemId'}
          onChange={this.handleTableChange}
          pagination={{
            size: 'default',
            current: page + 1,
            pageSize: pageSize,
            total: total,
          }}
        />
      </TableContainer>
    )
  }
}

export const RestockTable = withTheme(RestockTableContainer)
