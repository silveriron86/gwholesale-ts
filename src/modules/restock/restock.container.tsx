import React from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { RestockDispatchProps, RestockItem, RestockModule, RestockStateProps } from '~/modules/restock/restock.module'
import { RouteComponentProps } from 'react-router'
import PageLayout from '~/components/PageLayout'
import { RestockHeader } from '~/modules/restock/components/RestockHeader'
import { RestockTable } from '~/modules/restock/components/RestockTable'
import { RestockPageContainer } from '~/modules/restock/restock.style'
import { WrappedCreateOrderModal } from '~/modules/restock/components/CreateOrderModal'
import { Direction, QueryParams, RowKey } from '~/modules/restock/restock.service'
import { AuthUser } from '~/schema'
import moment from 'moment'
import { getFulfillmentDate } from '~/common/utils'
import { OrderHeaderProps } from '~/modules/orders/components'
import { WrappedEnterPurchaseModal } from '~/modules/restock/components/EnterPurchaseModal'
import { WrappedPurchaseOrderListModal } from '~/modules/restock/components/PurchaseOrderListModal'
import OrderFromPreferred from './components/OrderFromPreferred'
import _ from 'lodash'

type RestockContainerProps = OrderHeaderProps &
  RestockDispatchProps &
  RestockStateProps &
  RouteComponentProps & {
    currentUser: AuthUser
  }

interface IState {
  createOrderModalVisible: boolean
  enterPurchaseModalVisible: boolean
  addItemToPoModalVisible: boolean
  selectedRows: RestockItem[]
  singleOrderId: number | null
  showPreferModal: boolean
}

class RestockContainer extends React.PureComponent<RestockContainerProps, IState & QueryParams> {
  state = {
    showPreferModal: false,
    createOrderModalVisible: false,
    enterPurchaseModalVisible: false,
    addItemToPoModalVisible: false,
    selectedRows: [],
    searchKey: _.get(this.getListQueryStorage, 'searchKey', undefined),
    categoryIds: _.get(this.getListQueryStorage, 'categoryIds', undefined),
    page: _.get(this.getListQueryStorage, 'page', 0),
    pageSize: _.get(this.getListQueryStorage, 'pageSize', 10),
    active: _.get(this.getListQueryStorage, 'active', 1),
    direction: _.get(this.getListQueryStorage, 'direction', Direction.DESC),
    orderBy: _.get(this.getListQueryStorage, 'orderBy', undefined),
    statuses: _.get(this.getListQueryStorage, 'statuses', undefined),
    type: _.get(this.getListQueryStorage, 'type', undefined),
    vendorId: _.get(this.getListQueryStorage, 'vendorId', undefined),
    managerId: _.get(this.getListQueryStorage, 'managerId', undefined),
    dateChecked: _.get(this.getListQueryStorage,'dateChecked',undefined),
    fullfillDate:_.get(this.getListQueryStorage,'fullfillDate',4),
    singleOrderId: null as number | null,
  }

  componentDidMount() {
    this.props.fetchSumInfo({ active: 1 })
    this.props.getSimplyVendors(false)
    this.props.getSellerSetting()
    this.props.getItemList({ type: 'PURCHASE' })
    this.props.fetchCompanyProductAllTypes()
    this.props.getSaleCategories()
    this.props.getBriefVendors()
    this.props.getCompanyPmUsers()
    this.onSearch()
  }

  get getListQueryStorage() {
    return JSON.parse(localStorage.getItem('RESTOCK_QUERY'))
  }

  onCreateOrder = () => {
    this.setState({ createOrderModalVisible: true })
  }

  onShowPurchaseModal = () => {
    this.setState({ enterPurchaseModalVisible: true })
  }

  onShowAddOrderItemModal = () => {
    this.setState({ addItemToPoModalVisible: true })
  }
  onEnterPurchase = (vendorId: number, data: any) => {
    this.setState(
      {
        // enterPurchaseModalVisible: false,
        selectedRows: []
      },
      () => {
        this.props.finalizePurchaseOrder({
          clientId: vendorId,
          values: data,
        })
      },
    )
  }

  handleClearSelectedRowKeys = () => {
    this.setState({ selectedRows: [] })
  }

  onCreateOrderVisibleChange = (visible: boolean) => {
    const { singleOrderId } = this.state
    this.setState({ createOrderModalVisible: visible, singleOrderId: visible ? singleOrderId : null })
  }

  onPurchaseVisibleChange = (visible: boolean) => {
    this.setState({ enterPurchaseModalVisible: visible })
  }

  onPurchaseOrderVisibleChange = (visible: boolean) => {
    this.setState({ addItemToPoModalVisible: visible })
  }

  onQueryParamsChange = (params: QueryParams) => {
    this.setState(
      (state) => ({
        ...state,
        ...params,
        page: 0,
      }),
      () => {
        if(params.fullfillDate && !this.state.dateChecked){
          return
        }
        if (params.searchKey == null || params.searchKey.length === 0) {
          this.onSearch()
        }
      },
    )
  }

  onRowSelected = (selectedRows: RestockItem[]) => {
    this.setState({ selectedRows })
  }

  onSearch = () => {
    this.props.resetLoading()
    this.props.fetchDataSource(this.listParams)
    const {dateChecked, fullfillDate, statuses}  = this.state
    window.localStorage.setItem('RESTOCK_QUERY', JSON.stringify({
      ...this.listParams,
      dateChecked, fullfillDate, statuses
    }))
  }

  changeItem = (record: RestockItem) => {
    const currentItem = this.props.dataSource.find((v => v.wholesaleItemId === record.wholesaleItemId))
    if (currentItem?.active !== record.active) {
      this.props.resetLoading()
      this.props.modifyItem({
        wholesaleItemId: record.wholesaleItemId,
        active: record.active,
        reorderLevel: record.reorderLevel,
        callback: () => {
          this.onSearch()
        },
      })
    } else {
      this.props.modifyItem({
        wholesaleItemId: record.wholesaleItemId,
        active: record.active,
        reorderLevel: record.reorderLevel,
      })
    }
  }

  onTableChange = (orderBy: RowKey | undefined, direction: Direction, page?: number, pageSize?: number) => {
    this.setState(
      (state) => ({
        ...state,
        orderBy,
        direction,
        page,
        pageSize,
      }),
      () => {
        this.onSearch()
      },
    )
  }

  createEmptyOrder = (clientId: number) => {
    const quotedDate = moment(new Date()).format('MM/DD/YYYY')
    const { sellerSetting } = this.props
    const { selectedRows, singleOrderId } = this.state
    const rowKeys: number[] = singleOrderId ? [{ wholesaleItemId: singleOrderId, quantity: selectedRows.find(item=>item.wholesaleItemId==singleOrderId)?.quantity }] : selectedRows
    const fulfillmentDate = getFulfillmentDate(sellerSetting)
    const data = {
      wholesaleClientId: `${clientId}`,
      orderDate: quotedDate,
      quotedDate: quotedDate,
      deliveryDate: fulfillmentDate,
      totalPrice: 0,
      pickerNote: '',
      shippingAddressId: '',
      itemList: rowKeys.map((item: any) => ({
        price: 0,
        wholesaleItemId: item.wholesaleItemId,
        quantity: item.quantity || 0,
        cost: 0,
        margin: 0,
        freight: 0,
        modifier: null,
        defaultLogic: null,
        defaultGroup: null,
        note: '',
      })),
    }
    this.props.createEmptyOrder(data)
    this.handleClearSelectedRowKeys()
  }

  onOrder = (wholesaleItemId: number) => {
    this.setState({
      singleOrderId: wholesaleItemId,
      createOrderModalVisible: true,
    })
  }

  get listParams(): QueryParams {
    const {dateChecked, fullfillDate, statuses} = this.state
    let obj;
    if(dateChecked){
      const range = presets.find(item=>item.key === fullfillDate)
      obj = { from: range?.from.format('MM/DD/YYYY'), to: range?.to.format('MM/DD/YYYY')}
    } else{
      obj = {statuses}
    }
    return {
      searchKey: this.state.searchKey,
      categoryIds: this.state.categoryIds,
      page: this.state.page,
      pageSize: this.state.pageSize,
      active: this.state.active,
      vendorId: this.state.vendorId,
      managerId: this.state.managerId,
      orderBy: this.state.orderBy,
      direction: this.state.direction,
      type: this.state.type,
      ...obj
    }
  }

  render() {
    const {
      dataSource,
      total,
      sumInfo,
      loading,
      simplyVendors,
      saleItems,
      companyProductTypes,
      fetchCompanyProductAllTypes,
      sellerSetting,
      settingCompanyName,
      categories,
      createPOLoading,
      briefVendors,
      companyPmUsers,
    } = this.props
    const {
      searchKey,
      active,
      page,
      pageSize,
      createOrderModalVisible,
      statuses,
      enterPurchaseModalVisible,
      addItemToPoModalVisible,
      selectedRows,
      categoryIds,
      type,
      vendorId,
      managerId,
      dateChecked,
      fullfillDate,
      showPreferModal,
    } = this.state

    const fulfillmentDate = getFulfillmentDate(sellerSetting)

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Restock'}>
        <RestockPageContainer>
          <RestockHeader
            presets={presets}
            companyPmUsers={companyPmUsers}
            vendorList={briefVendors}
            searchKey={searchKey}
            active={active}
            sumInfo={sumInfo}
            statuses={statuses}
            vendorId={vendorId}
            managerId={managerId}
            dateChecked={dateChecked}
            fullfillDate={fullfillDate}
            type={type}
            categories={categories}
            categoryIds={categoryIds}
            onSearch={this.onSearch}
            onQueryParamsChange={this.onQueryParamsChange}
            onCreateOrder={this.onCreateOrder}
            onEnterPurchase={this.onShowPurchaseModal}
            onShowAddOrderItemModal={this.onShowAddOrderItemModal}
            onShowPreferModal={() => this.setState({ showPreferModal: true })}
            fetchSumInfo={this.props.fetchSumInfo}
            selectedRows={selectedRows}
          />
          <RestockTable
            dataSource={dataSource}
            page={page}
            pageSize={pageSize}
            total={total}
            loading={loading}
            onRowSelected={this.onRowSelected}
            selectedRows={selectedRows}
            onTableChange={this.onTableChange}
            onOrder={this.onOrder}
            changeItem={this.changeItem}
          />
        </RestockPageContainer>
        {createOrderModalVisible && (
          <WrappedCreateOrderModal
            visible={createOrderModalVisible}
            vendors={simplyVendors}
            createEmptyOrder={this.createEmptyOrder}
            onVisibleChange={this.onCreateOrderVisibleChange}
            createPOLoading={createPOLoading}
            handleClearSelectedRowKeys={this.handleClearSelectedRowKeys}
          />
        )}
        {enterPurchaseModalVisible && (
          <WrappedEnterPurchaseModal
            visible={enterPurchaseModalVisible}
            vendors={simplyVendors}
            saleItems={saleItems}
            selectedRows={selectedRows}
            companyProductTypes={companyProductTypes}
            sellerSetting={sellerSetting}
            onVisibleChange={this.onPurchaseVisibleChange}
            fetchCompanyProductAllTypes={fetchCompanyProductAllTypes}
            updateProduct={this.props.updateProduct}
            setCompanyProductType={this.props.setCompanyProductType}
            getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
            onEnterPurchase={this.onEnterPurchase}
            settingCompanyName={settingCompanyName}
            createPOLoading={createPOLoading}
          />
        )}
        {addItemToPoModalVisible && (
          <WrappedPurchaseOrderListModal
            visible={addItemToPoModalVisible}
            onVisibleChange={this.onPurchaseOrderVisibleChange}
            handleClearSelectedRowKeys={this.handleClearSelectedRowKeys}
            // addItemsForPO={this.addItemsForPo}
            // getOpenPOList={this.props.getOpenPOList}
            // openPOList={this.props.openPOList}
            // openPOTotal={this.props.openPOTotal}
            selectedRows={selectedRows}
          />
        )}
        <OrderFromPreferred
          onRowSelected={this.onRowSelected}
          selectedRows={selectedRows}
          visible={showPreferModal}
          deliveryDate={fulfillmentDate}
          onCancel={() => {
            this.setState({ showPreferModal: false })
          }}
        />
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => ({ ...state.restock, currentUser: state.currentUser })

export const Restock = withTheme(connect(RestockModule)(mapStateToProps)(RestockContainer))

const presets = [
  {
    key: 0,
    label: 'Past 7 Days',
    from: moment().subtract(7, 'days'),
    to: moment(),
  },
  {
    key: 1,
    label: 'Past 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment(),
  },
  {
    key: 9,
    label: 'Yesterday',
    from: moment().subtract(1, 'days'),
    to: moment().subtract(1, 'days'),
  },
  {
    key: 2,
    label: 'Today',
    from: moment(),
    to: moment(),
  },
  {
    key: 3,
    label: 'Tomorrow',
    from: moment().add(1, 'days'),
    to: moment().add(1, 'days'),
  },
  {
    key: 4,
    label: 'Today and Tomorrow',
    from: moment(),
    to: moment().add(1, 'days'),
  },
  {
    key: 5,
    label: 'Next 7 Days',
    from: moment(),
    to: moment().add(7, 'days'),
  },
  {
    key: 6,
    label: 'Next 30 Days',
    from: moment(),
    to: moment().add(30, 'days'),
  },
  {
    key: 7,
    label: 'Past 30 Days and Next 30 Days',
    from: moment().subtract(30, 'days'),
    to: moment().add(30, 'days'),
  },
]
