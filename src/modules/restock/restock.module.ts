import {
  DefineAction,
  Effect,
  EffectModule,
  Module,
  ModuleDispatchProps,
  Reducer,
  StateObservable,
} from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { ModifyItemParam, QueryParams, RestockService } from '~/modules/restock/restock.service'
import { catchError, endWith, finalize, map, startWith, switchMap, takeUntil } from 'rxjs/operators'
import { Action } from 'redux-actions'
import { checkError, responseHandler } from '~/common/utils'
import { OrderService } from '~/modules/orders/order.service'
import { GlobalState } from '~/store/reducer'
import { SettingService } from '~/modules/setting/setting.service'
import { push } from 'connected-react-router'
import { SaleItem } from '~/schema'
import { InventoryService } from '~/modules/inventory/inventory.service'
import { OrdersStateProps } from '~/modules/orders'
import { ProductService } from '~/modules/product/product.service'
import { VendorService } from '~/modules/vendors/vendors.service'
import { SettingsProps } from '~/modules/settings'
import { MessageType } from '~/components'
import { BriefClient } from '~/schema'

export interface RestockItem {
  SKU: string
  active: boolean
  availableToSell: number
  categoryName: string
  incomingQty: number
  inventoryUOM: string
  onHandQty: number
  reorderLevel?: number
  reorderQuantity?: number
  variety: string
  weeks: number | null
  wholesaleItemId: number
  availToSellTotal: number
  onHandTotal: number
  managerId: number
  firstName: string
  lastName: string
  restockStatus: number
}

export interface SumInfo {
  oversoldCount?: number
  needToReorderedCount?: number
  outOfStockCount?: number
}

export interface SimplyVendor {
  clientCompanyId: number
  clientCompanyName: string
  clientId: number
  wholesaleCompanyId: number
}

export interface Category {
  wholesaleCategoryId: number
  name: string
  warehouse: string
  wholesaleSection: {
    wholesaleSectionId: number
    name: string
    warehouse: string
    qboId: string
    // "nsId": null;
    isDefault: false
    wswVersion: number
  }
  isDefault: false
  // "nsId": null;
  wswVersion: number
}

export interface RestockStateProps {
  dataSource: RestockItem[]
  total: number
  loading: boolean
  simplyVendors: SimplyVendor[]
  sumInfo: SumInfo
  sellerSetting: any
  saleItems: SaleItem[]
  companyProductTypes: any
  settingCompanyName: string
  categories: Category[]
  openPOList: []
  openPOTotal: number
  addItemLoading: boolean
  createPOLoading: boolean
  briefVendors: BriefClient[]
  companyPmUsers: any[]
}

@Module('restock')
export class RestockModule extends EffectModule<RestockStateProps> {
  defaultState = {
    dataSource: [],
    total: 0,
    loading: false,
    simplyVendors: [],
    sellerSetting: undefined,
    saleItems: [],
    companyProductTypes: null,
    settingCompanyName: '',
    categories: [],
    sumInfo: {
      oversoldCount: 0,
      needToReorderedCount: 0,
      outOfStockCount: 0,
    },
    openPOList: [],
    openPOTotal: 0,
    addItemLoading: false,
    createPOLoading: false,
    briefVendors: [] as BriefClient[],
    companyPmUsers: [],
  }

  @DefineAction() dispose$!: Observable<void>
  @DefineAction() disposeEditOrder$!: Observable<void>

  constructor(
    private readonly restock: RestockService,
    private readonly order: OrderService,
    private readonly setting: SettingService,
    private readonly inventory: InventoryService,
    private readonly product: ProductService,
    private readonly vendor: VendorService,
  ) {
    super()
  }

  formatProductTypes = (datas: any[]) => {
    const result = {
      unitOfMeasure: [],
      returnReason: [],
      carrier: [],
      modifier: [],
      costStructure: [],
      modeOfTransportation: [],
      suppliers: [],
      freightTypes: [],
      shippingTerms: [],
      paymentTerms: [],
      extraCOO: [],
      extraCharges: [],
      customerTypes: [],
      vendorTypes: [],
      reasonType: []
    }

    // UNIT_OF_MEASURE, RETURN_REASON, CARRIER,MODIFIER,COST_STRUCTURE,MODE_OF_TRANSPORTATION
    datas.map((type: any) => {
      switch (type.type) {
        case 'UNIT_OF_MEASURE':
          result.unitOfMeasure.push(type)
          break
        case 'RETURN_REASON':
          result.returnReason.push(type)
          break
        case 'CARRIER':
          result.carrier.push(type)
          break
        case 'MODIFIER':
          result.modifier.push(type)
          break
        case 'COST_STRUCTURE':
          result.costStructure.push(type)
          break
        case 'MODE_OF_TRANSPORTATION':
          result.modeOfTransportation.push(type)
          break
        case 'SUPPLIERS':
          result.suppliers.push(type)
          break
        case 'FREIGHT':
          result.freightTypes.push(type)
          break
        case 'SHIPPING_TERM':
          result.shippingTerms.push(type)
          break
        case 'PAYMENT_TERM':
          result.paymentTerms.push(type)
          break
        case 'EXTRA_COO':
          result.extraCOO.push(type)
          break
        case 'EXTRA_CHARGE':
          result.extraCharges.push(type)
          break
        case 'CUSTOMER_BUSINESS_TYPE':
          result.customerTypes.push(type)
          break
        case 'VENDOR_BUSINESS_TYPE':
          result.vendorTypes.push(type)
          break
        case 'REASON_TYPE':
          result.reasonType.push(type)
          break
      }
    })
    return result
  }

  @Effect({
    done: (
      state: RestockStateProps,
      { payload }: Action<{ dataList: RestockItem[]; total: number }>,
    ): RestockStateProps => {
      return {
        ...state,
        dataSource: payload?.dataList,
        total: payload?.total,
        loading: false,
      }
    },
  })
  fetchDataSource(action$: Observable<QueryParams>) {
    return action$.pipe(
      switchMap((data) =>
        this.restock.fetchList(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  fetchCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: RestockStateProps, { payload }: Action<SumInfo[]>): RestockStateProps => {
      return {
        ...state,
        sumInfo: payload?.[0] || {},
      }
    },
  })
  fetchSumInfo(action$: Observable<any>, globalState: any) {
    return action$.pipe(
      switchMap((data) =>
        this.restock.fetchSumInfo(data).pipe(
          switchMap((res) => {
            return of(responseHandler(res, false).body.data)
          }),
          map(this.createAction('done')),
        ),
      ),
    )
  }

  @Effect({
    before: (state: RestockStateProps, action) => {
      return {
        ...state,
        dataSource: state.dataSource.map(v => {
          if (action.payload.wholesaleItemId === v.wholesaleItemId) return ({ ...v, reorderLevel: action.payload.reorderLevel })
          return v
        })
      }
    },
    done: (state: RestockStateProps) => {
      return {
        ...state,
        loading: false,
      }
    },
  })
  modifyItem(action$: Observable<ModifyItemParam>) {
    return action$.pipe(
      switchMap((resp) =>
        this.restock.modifyItem(resp).pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')(resp)),
          finalize(() => {
            if (resp.callback) {
              resp.callback?.()
            }
          }),
        ),
      ),
    )
  }

  @Reducer()
  resetLoading(state: RestockStateProps) {
    return {
      ...state,
      loading: true,
    }
  }

  @Reducer()
  resetAddItemLoading(state: RestockStateProps) {
    return {
      ...state,
      addItemLoading: !state.addItemLoading,
    }
  }

  @Effect({
    done: (state: RestockStateProps, { payload }: Action<SimplyVendor[]>) => {
      return {
        ...state,
        simplyVendors: payload,
      }
    },
  })
  getSimplyVendors(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.getSimplifyVendors(data).pipe(
          switchMap((res) => of(responseHandler(res, false).body.data)),
          map(this.createAction('done')),
        ),
      ),
    )
  }

  @Effect({
    done: (state: RestockStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        settingCompanyName: payload.company.companyName,
        sellerSetting: payload,
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getUserSetting(state$.value.currentUser?.userId || '0').pipe(
          switchMap((res) => of(res.userSetting)),
          map(this.createAction('done')),
        ),
      ),
    )
  }

  @Effect({
    before: (state: RestockStateProps, { payload }) => {
      return {
        ...state,
        createPOLoading: true
      }
    },
    done: (state: RestockStateProps, { payload }) => {
      const href = window.location.href
      window.open(href.replace(href.substring(href.indexOf('#'), href.length), `#/order/${payload.wholesaleOrderId}/purchase-cart`), '_blank')
      return {
        ...state,
        createPOLoading: false
      }
    },
  })
  createEmptyOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.order.createOrder(data, state$.value.currentUser?.userId || '1').pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
        ),
      ),
    )
  }

  @Effect({
    done: (state: OrdersStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        saleItems: action.payload,
      }
    },
  })
  getItemList(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.inventory.getItemList(
          state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
          data,
        ),
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any[]>) => {
      return {
        ...state,
      }
    },
  })
  updateProduct(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => this.product.updateItemSuppliers(data)),
      switchMap((res) => of(responseHandler(res, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingsProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        message: 'Type Create Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: SettingsProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  setCompanyProductType(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => this.setting.saveCompanyProductType(data)),
      switchMap((data) =>
        of(responseHandler(data, false)).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCompanyProductAllTypes)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    before: (state: RestockStateProps, { payload }) => {
      return {
        ...state,
        createPOLoading: true
      }
    },
    done: (state: OrdersStateProps, { payload }: Action<any>) => {
      const href = window.location.href
      window.open(href.replace(href.substring(href.indexOf('#'), href.length), `#/order/${payload.wholesaleOrderId}/purchase-cart`), '_blank')
      return {
        ...state,
        type: MessageType.SUCCESS,
        createPOLoading: false
      }
    },
  })
  finalizePurchaseOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.finalizePurchaseOrder(data.clientId, data.values).pipe(
          switchMap((res) => of(responseHandler(res, true).body.data)),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any[]>) => {
      return {
        ...state,
        categories: action.payload,
      }
    },
  })
  getSaleCategories(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getAllCategories(state$.value.currentUser.company || 'GRUBMARKET').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        openPOList: action.payload.dataList,
        openPOTotal: action.payload.total,
      }
    },
  })
  getOpenPOList(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.restock.getOpenPOList(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Reducer()
  updateItem(state: RestockStateProps, action: Action<{ quantity: string, wholesaleItemId: number }>) {
    return {
      ...state,
      dataSource: state.dataSource.map(v => {
        if (action.payload.wholesaleItemId === v.wholesaleItemId) return ({ ...v, quantity: action.payload.quantity })
        return v
      })
    }
  }

  @Effect({
    done: (state: any, action: Action<BriefClient[]>) => {
      return { ...state, briefVendors: action.payload }
    }
  })
  getBriefVendors(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getBriefVendors().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, companyPmUsers: payload.userList }
    },
  })
  getCompanyPmUsers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.product.getCompanyPmUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
    )
  }
}

export type RestockDispatchProps = ModuleDispatchProps<RestockModule>
