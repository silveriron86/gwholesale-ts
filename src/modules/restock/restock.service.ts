import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import { RestockItem, SumInfo } from '~/modules/restock/restock.module'

export enum Direction {
  DESC = 'desc',
  ASC = 'asc',
}

export type RowKey = keyof RestockItem
export type Type = 1 | 2 | 3 // 1 out of stock 2 oversold 3 need to reordered

export interface QueryParams {
  searchKey?: string
  categoryIds?: string
  page?: number // latest is 0
  pageSize?: number
  active?: number
  direction?: Direction
  statuses?: string
  orderBy?: RowKey
  type?: Type
  vendorId?: string
  managerId?: string|number
  fullfillDate?: number
  dateChecked?: boolean
}

export interface ModifyItemParam {
  wholesaleItemId: number
  active?: boolean
  reorderLevel?: number
  callback?: () => void
}

@Injectable()
export class RestockService {
  constructor(private readonly http: Http) {}

  static get instance() {
    return new RestockService(new Http())
  }

  fetchList(params: QueryParams) {
    const search = new URLSearchParams()
    Object.keys(params).forEach((key) => {
      if (key !== 'orderBy' && key !== 'direction') {
        params[key] != null && search.append(key, params[key])
      }
    })
    if (params.orderBy != null && params.direction != null) {
      search.append('orderBy', params.orderBy)
      search.append('direction', params.direction)
    }
    // /inventory/item-stock?page=0&pageSize=10&searchKey=test&categoryId=131&active=1
    return this.http.get<{ dataList: RestockItem[]; total: number }>(`/inventory/item-stock?${search.toString()}`)
  }

  fetchSumInfo(params: any) {
    return this.http.get<SumInfo[]>(`/inventory/item-stock-group?active=${params.active}`)
  }

  modifyItem(data: ModifyItemParam) {
    return this.http.put('/inventory/wholesaleItem', {
      body: JSON.stringify(data),
    })
  }

  getOpenPOList(query: any) {
    return this.http.get(`/inventory/open-POs`, { query })
  }

  addItemsForPO(orderId: number, itemList: any) {
    return this.http.post(`/inventory/order-items/${orderId}`, {
      body: JSON.stringify(itemList),
    })
  }

  createPOByPreferred({ deliveryDate, orderList }) {
    return this.http.post(`/inventory/create-po-by-preferred`, {
      body: JSON.stringify({
        deliveryDate,
        orderList,
      }),
    })
  }
}
