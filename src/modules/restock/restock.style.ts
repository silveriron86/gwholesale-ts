import styled from "@emotion/styled";
import { Input, Table } from "antd";
import { ThemeModal } from "~/modules/customers/customers.style";

/**
 * container
 */
export const RestockPageContainer = styled.div`
  width: 100%;
  text-align: left;
  box-sizing: border-box;
  padding: 30px 70px;
`;

/**
 * header
 */

export const OptionsContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;

export const SelectContainer = styled.div`
  display: flex;
`;

export const SelectBox = styled.div`
  flex: 1;

  &:not(:first-child) {
    margin-left: 20px;
  }

  .label {
    margin-bottom: 10px;
  }
`;


export const CardAndButtonGroupContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

export const CardContainer = styled.div`
  display: flex;
  margin-top: 30px;
`;

export const CardOption = styled.div<{ selected?: boolean }>`
  padding: 18px;
  border: ${props => props.selected ? props.theme.primary + " 2px solid" : "rgb(217, 217, 217) 1px solid"};
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  //cursor: pointer;
  //box-sizing: border-box;
  overflow: hidden;
  position: relative;

  &::after {
    content: "";
    display: block;
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    background-color: ${props => props.selected ? props.theme.primary : "transparent"};
    opacity: 0.1;
    pointer-events: none;
  }

  &:not(:first-child) {
    margin-left: 30px;
  }

  span.title {
    display: block;
    font-size: 16px;
  }

  & > div {
    width: 100%;
    padding-top: 10px;
    display: flex;
    align-items: flex-end;

    span.content {
      font-size: 20px;
    }

    span.clear {
      visibility: ${props => props.selected ? "visible" : "hidden"};
      margin-left: 30px;
      font-size: 16px;
      color: ${props => props.theme.primary};
      cursor: pointer;
    }
  }


`;

export const ButtonGroup = styled.div`
  margin-left: auto;
  display: flex;
  align-self: flex-end;
`;

/**
 * table
 */

export const TableContainer = styled.div`
  width: 100%;
  margin-top: 30px;
`;

/**
 * enter purchase modal
 */

export const NoBottomPaddingModal = styled(ThemeModal)`
  .ant-modal-body {
    padding-bottom: 0;
    padding-top: 0;
  }
`;

export const EnterPurchaseTip = styled.p`
  font-size: 14px;
  font-weight: normal;

  a:hover {
    text-decoration: underline;
  }
`;

export const EnterPurchaseFormContainer = styled.div`
  width: 100%;
  display: flex;

  .ant-form-item-label {
    font-weight: normal;
  }
`;

export const ModalFooterContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;

  &.create-order {
    & > button {
      &:nth-child(2) {
        width: 200px;
        height: 40px;
        font-size: 16px;
        margin-left: calc(50% - 100px);
      }

      &:nth-child(3) {
        color: #645D5C;
        border: 0;
        font-size: 14px;
        font-weight: 700;
        margin-left: 20px;

        & > span {
          text-decoration: underline;
        }
      }
    }
  }
`;

export const WeekTitleContainer = styled.div`
  display: flex;
  align-items: center;

  & > span:first-child {
    margin-right: 5px;
  }
`;

export const StyledSearch = styled(Input.Search)((props) => ({
  button: {
    paddingLeft: '9px',
    paddingRight: '9px',
    backgroundColor: props.theme.primary
  },
  '.ant-btn-primary:hover, .ant-btn-primary:focus': {
    backgroundColor: props.theme.primary,
    borderColor: props.theme.primary,
  },
  '.clear-icon': {
    display: 'none'
  },
  '&:hover .clear-icon.show': {
    display: 'block'
  }
}))

export const ResTockTable = styled(Table)((props) => ({
  '.ant-checkbox-checked .ant-checkbox-inner': {
    backgroundColor: props.theme.primary,
    borderColor: props.theme.primary,
  },
  '.ant-checkbox-wrapper:hover .ant-checkbox-inner, .ant-checkbox:hover .ant-checkbox-inner, .ant-checkbox-input:focus + .ant-checkbox-inner': {
    borderColor: props.theme.primary,
  },
  '.ant-checkbox-indeterminate .ant-checkbox-inner::after': {
    backgroundColor: props.theme.primary,
  },
  '.ant-pagination-item-active': {
    borderColor: props.theme.primary,
  },
  '.ant-pagination-item:focus, .ant-pagination-item:hover': {
    borderColor: props.theme.primary,
  }
}))
