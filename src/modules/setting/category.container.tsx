import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteProps } from 'react-router'
import { Icon, Modal, notification } from 'antd'
import { Droppable, DroppableProvided, DragDropContext, DropResult } from 'react-beautiful-dnd'

import { Icon as IconSvg, MessageType } from '~/components'
import { CategoryItem, NewCategoryItem } from './components/category-item.component'

import { GlobalState } from '~/store/reducer'
import { SettingModule, SettingDispatchProps, SettingStateProps } from './setting.module'
import { Category, Body, NewCategoryButton, ModalFooter, ModalContent } from './category.style'
import { SaleSection, SaleCategory } from '~/schema';
import { cloneDeep,orderBy } from 'lodash';
import { BackButton, Header, ThemeWrap } from './theme.style'
import PageLayout from '~/components/PageLayout'
import { CACHED_NS_LINKED } from '~/common'

export type CategoryProps = SettingDispatchProps & SettingStateProps & RouteProps

interface CategoryState {
  modalVisible: boolean
  message: string
  type: MessageType | null
}

export class CategoryComponent extends React.PureComponent<CategoryProps, CategoryState> {
  state = {
    modalVisible: false,
    message: '',
    type: null
  }

  componentDidMount() {
    this.props.getSections()
    this.props.getSaleCategories()
  }

  componentWillReceiveProps(nextProps: CategoryProps)
  {
    if (nextProps.message != this.state.message)
    {
      this.setState({
        message: nextProps.message,
        type: nextProps.type,
      })
      if (nextProps.type != null)
      {
        notification[nextProps.type!]({
          message: nextProps.type!.toLocaleUpperCase(),
          description: nextProps.message,
          onClose:this.onCloseNotification,
          duration: nextProps.type === 'success' ? 5: 4.5
        });
      }
    }
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  onCancelCategory = () => {
    this.props.cancelCategory()
  }

  render() {
    return (
      <PageLayout noSubMenu={true}>
        <ThemeWrap>
          <BackButton onClick={this.props.goBack}>
            <Icon type="arrow-left" />
            <span>back</span>
          </BackButton>
          <Header>
            <span>ACCOUNT</span>
            <div>
              Categories
              <IconSvg type="information" width={18} height={18} viewBox="0 0 18 18" onClick={this.onClickDesc} />
            </div>
          </Header>
          {this.renderBody()}
          {this.renderModal()}
        </ThemeWrap>
      </PageLayout>
    )
  }

  private renderBody() {
    const { categories, sections } = this.props

    let saleSections = [] as SaleSection[]
    if (sections.length > 0)
    {
      let sectionList = {}
      for (const section of sections)
      {
          const tmp = sectionList[section.wholesaleSectionId] = cloneDeep(section)
          tmp.categories = [] as SaleCategory[]
      }
      for (const category of categories)
      {
        let tmp = sectionList[category.wholesaleSection.wholesaleSectionId]
        if(tmp){
          tmp.categories!.push(category)
        }
      }
      for (const section in sectionList)
        saleSections.push(sectionList[section])

      saleSections = orderBy(saleSections,['isDefault'],['desc'])
      // console.log(saleSections)
    }


    const enabledNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const sectionItems = saleSections.map((section, index) => (
      <CategoryItem
        enabledNS={enabledNS}
        key={section.wholesaleSectionId}
        item={section}
        index={index}
        onAddSubCategory={this.onAddSubCategory}
        updateSubCategory={this.props.updateSubCategory}
        deleteSubCategory={this.props.deleteSubCategory}
        updateSection={this.props.updateSection}
        deleteSection={this.props.deleteSection}
      />
    ))

    return (
      <Body>
        {!enabledNS && (
          <NewCategoryButton onClick={this.onClickNewCategory}>
            <Icon type="plus" />
            <span>Add New Category</span>
          </NewCategoryButton>
        )}
        {this.props.newCategory != null ? (
          <NewCategoryItem
            key={this.props.newCategory!.wholesaleSectionId}
            item={this.props.newCategory!}
            addMainCategory={this.props.addMainCategory}
            onCancel={this.onCancelCategory}
          />
        ) : ''}
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable droppableId="0">
            {(provided: DroppableProvided) => (
              <div ref={provided.innerRef} {...provided.droppableProps}>
                {sectionItems}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </Body>
    )
  }

  private renderModal() {
    const { modalVisible } = this.state

    const Footer = (
      <ModalFooter onClick={this.onCloseModal}>
        <IconSvg type="check" width={15} height={14} viewBox="0 0 15 14" />
        <span>Okay</span>
      </ModalFooter>
    )

    return (
      <Modal visible={modalVisible} footer={Footer} closable={false}>
        <ModalContent>
          Categories can be assigned to any item in your inventory. Items displaying on price sheets are also organized
          and listed by category type. New categories can be created here or in the detail pane of an item.
        </ModalContent>
      </Modal>
    )
  }

  private onClickNewCategory = () => {
    this.props.addCategory()
  }

  private onClickDesc = () => {
    this.setState({
      modalVisible: true,
    })
  }

  private onCloseModal = () => {
    this.setState({
      modalVisible: false,
    })
  }

  onAddSubCategory = (data: any) => {
    this.props.addSubCategory(data)
  }
/*
  private onUpdateCategory = (id: string) => (data: Partial<CategoryType>) => {
    /
    this.props.updateCategory({
      id,
      value: data,
    })
  }

  private onDeleteCategory = (id: string) => () => {
    this.props.deleteCategory(id)
  }
*/
  private onDragEnd = (result: DropResult) => {
    const { categories } = this.props
    const { source, destination } = result
    if (!destination) return

    const startIndex = source.index
    const endIndex = destination.index

    const newList = [...categories]
    const [removed] = newList.splice(startIndex, 1)
    newList.splice(endIndex, 0, removed)
    //this.props.reorder(newList)
  }
}

const mapStateToProps = (state: GlobalState) => state.setting

export const CategorySetting = connect(SettingModule)(mapStateToProps)(CategoryComponent)
