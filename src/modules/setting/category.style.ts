import styled from '@emotion/styled'
import { white, mediumGrey, black, backgroundGreen, transparent } from '~/common'

export const Category = styled('div')({
  width: '100%',
  minHeight: '100vh',
  backgroundColor: white,
  boxSizing: 'border-box',
  '& *': {
    boxSizing: 'border-box',
  },
  '& .ant-modal-footer': {
    borderTop: 0,
  },
})

export const Header = styled('div')((props) => ({
  width: '100%',
  padding: '36px 130px',
  '& > span': {
    fontSize: '12px',
    color: mediumGrey,
    textTransform: 'uppercase',
    fontWeight: 500,
  },
  '& > div': {
    fontSize: '36px',
    color: black,
    letterSpacing: '.05em',
    lineHeight: '52px',
    fontWeight: 600,

    '& > svg': {
      color: props.theme.primary,
      marginLeft: '10px',
    },
  },
}))

export const Body = styled('div')({
  width: '100%',
  marginTop: '40px',
  padding: '0 220px',
})

export const NewCategoryButton = styled('div')((props) => ({
  width: '100%',
  height: '80px',
  border: `1px solid ${props.theme.primary}`,
  borderRadius: '10px',
  display: 'flex',
  alignItems: 'center',
  padding: '0 40px',
  cursor: 'pointer',

  '& > i': {
    color: props.theme.primary,
  },

  '& > span': {
    fontSize: '24px',
    color: props.theme.primary,
    letterSpacing: '.05em',
    fontWeight: 500,
    lineHeight: 35,
    marginLeft: '10px',
  },
}))

export const ModalFooter = styled('div')((props) => ({
  width: '100%',
  color: props.theme.primary,
  cursor: 'pointer',
  '& > span': {
    marginLeft: '10px',
    fontSize: '14px',
    letterSpacing: '.05em',
    lineHeight: '15px',
    fontWeight: 600,
    textDecoration: 'underline',
  },
}))

export const ModalContent = styled('div')({
  fontSize: '14px',
  lineHeight: '25px',
  letterSpacing: '.05em',
  color: black,
  fontWeight: 400,
})

export const CategoryWrap = styled('div')({
  width: '100%',
  backgroundColor: backgroundGreen,
  marginTop: '20px',
  padding: '24px 20px 24px 40px',
  borderRadius: '10px',
})

export const CategoryItemTop = styled('div')({
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
})

export const CategoryItemNameWrap = styled('div')((props) => ({
  display: 'flex',
  alignItems: 'center',

  '& > svg': {
    color: props.theme.primary,
    marginLeft: '10px',
    cursor: 'pointer',
  },
  '& .ant-input': {
    border: 'none',
    borderBottom: `1px solid ${props.theme.primary}`,
    borderRadius: 0,
    backgroundColor: transparent,
    fontSize: '16px',
    fontWeight: 600,
    '&:focus': {
      boxShadow: 'none',
    },
  },
}))

export const CategoryItemName = styled('div')({
  fontSize: '24px',
  lineHeight: '34px',
  color: black,
  fontWeight: 600,
  letterSpacing: '.05em',
})

export const CategoryInfoWrap = styled('div')({
  display: 'flex',
  alignItems: 'center',

  '& > i': {
    fontSize: '24px',
    marginLeft: '15px',
    color: mediumGrey,
    cursor: 'pointer',
  },
  '& > svg': {
    color: mediumGrey,
  },
})

export const CategoryItemSub = styled('div')((props) => ({
  display: 'flex',
  alignItems: 'center',
  marginRight: '50px',
  cursor: 'pointer',

  '& > span': {
    fontSize: '14px',
    color: props.theme.primary,
    lineHeight: '20px',
    letterSpacing: '.05em',
    fontWeight: 500,
    textTransform: 'uppercase',
  },
  '& > i': {
    color: props.theme.primary,
    fontSize: '12px',
    marginLeft: '10px',
  },
}))

export const CategoryItemSubWrap = styled('div')({
  width: '100%',
  padding: '30px 0 11px 0',
  borderTop: `1px solid rgb(193, 195, 193, .5)`,
  marginTop: '24px',
  display: 'flex',
  flexWrap: 'wrap',
})

export const CategorySubAddButton = styled('div')((props) => ({
  display: 'flex',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  color: props.theme.primary,

  '& > i': {
    fontSize: '12px',
  },

  '& > span': {
    fontSize: '14px',
    fontWeight: 500,
    letterSpacing: '.05em',
    marginLeft: '10px',
  },
}))

export const addToButtonStyle: React.CSSProperties = {
  borderRadius: '200px',
  height: '30px',
  width: '270px',
}

export const buttonStyle: React.CSSProperties = {
  borderRadius: '200px',
  width: '100px',
  height: '32px',
  marginRight: '31px',
  fontSize: '16px',
  marginTop: '10px'
}

export const CategorySubItem = styled('div')((props) => ({
  height: '30px',
  backgroundColor: props.theme.primary,
  borderRadius: '30px',
  display: 'flex',
  alignItems: 'center',
  padding: '0 20px',
  margin: '0 12px 10px 12px',
  position: 'relative',

  '& > span': {
    color: white,
    fontSize: '14px',
    fontWeight: 500,
    letterSpacing: '.05em',
    lineHeight: '20px',
  },

  '& > svg': {
    fontSize: '16px',
    position: 'absolute',
    right: '8px',
    color: white,
    cursor: 'pointer',
    display: 'none',
  },

  '&:hover > svg': {
    display: 'block',
  },
}))

export const CategorySubNewInputWrap = styled('div')((props) => ({
  height: '30px',
  backgroundColor: props.theme.primary,
  borderRadius: '30px',
  display: 'flex',
  alignItems: 'center',
  padding: '0 50px 0 30px',
  margin: '0 12px',
  position: 'relative',

  '& .ant-input': {
    border: 'none',
    borderRadius: 0,
    backgroundColor: transparent,
    fontSize: '14px',
    fontWeight: 500,
    color: white,

    '&:focus': {
      boxShadow: 'none',
    },
  },
}))

export const CategorySubNewInputIcons = styled('div')({
  position: 'absolute',
  right: '8px',
  display: 'flex',
  alignItems: 'center',

  '& > i': {
    fontSize: '14px',
    color: white,
  },
})
