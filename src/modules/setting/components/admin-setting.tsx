import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import Form, { FormComponentProps } from 'antd/lib/form'
import { ThemeButton, DetailsTitle, ThemeRadio } from '~/modules/customers/customers.style'
import { SettingModule, SettingDispatchProps, SettingStateProps } from './../setting.module'
import { GlobalState } from '~/store/reducer'
import { PrintSettingWrapper } from './../theme.style'
import { SetionTableLabel } from '~/modules/product/components/product.component.style'
import { Checkbox, Icon, Radio, Tooltip, Select } from 'antd'
import { Company } from '~/schema'
import { CACHED_NS_LINKED } from '~/common'

interface AdminSettingFields {
  isDisablePickingStep: boolean
  isEnableCashSales: boolean
  itemDescriptionFormat: string
  hideMenuLabel: boolean
  hideMenuRequisitionCat: boolean
  isEnableDebitMemo: boolean
}

type ThemeProps = SettingDispatchProps & SettingStateProps & FormComponentProps<AdminSettingFields>

class AdminSetting extends React.PureComponent<ThemeProps> {
  state = {
    isDisablePickingStep: this.props.companyInfo && this.props.companyInfo.isDisablePickingStep,
    isEnableCashSales: this.props.companyInfo && this.props.companyInfo.isEnableCashSales,
    isEnableDebitMemo: this.props.companyInfo && this.props.companyInfo.isEnableDebitMemo,
    itemDescriptionFormat:
      this.props.companyInfo && this.props.companyInfo.itemDescriptionFormat
        ? this.props.companyInfo.itemDescriptionFormat
        : 'A',
    hideMenuLabel: this.props.companyInfo && this.props.companyInfo.hideMenuLabel,
    hideMenuRequisitionCat: this.props.companyInfo && this.props.companyInfo.hideMenuRequisitionCat,
    updateRadio: false,
  }

  componentDidMount() {
    if (!this.props.printSetting) {
      this.props.getPrintSetting()
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.companyInfo && JSON.stringify(this.props.companyInfo) && JSON.stringify(nextProps.companyInfo)) {
      this.setState({
        isDisablePickingStep: nextProps.companyInfo && nextProps.companyInfo.isDisablePickingStep,
        isEnableCashSales: nextProps.companyInfo && nextProps.companyInfo.isEnableCashSales,
        isEnableDebitMemo: nextProps.companyInfo && nextProps.companyInfo.isEnableDebitMemo,
        hideMenuLabel: nextProps.companyInfo && nextProps.companyInfo.hideMenuLabel,
        hideMenuRequisitionCat: nextProps.companyInfo && nextProps.companyInfo.hideMenuRequisitionCat,
      })
      if (this.state.updateRadio == false) {
        this.setState({
          itemDescriptionFormat: nextProps.companyInfo && nextProps.companyInfo.itemDescriptionFormat,
        })
      }
    }
  }
  onSaveDateSetting = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let company: Company = this.props.companyInfo
        company.isDisablePickingStep = values.isDisablePickingStep
        company.isEnableCashSales = values.isEnableCashSales
        company.isEnableDebitMemo = values.isEnableDebitMemo
        company.itemDescriptionFormat = this.state.itemDescriptionFormat
        company.hideMenuLabel = values.hideMenuLabel
        company.hideMenuRequisitionCat = values.hideMenuRequisitionCat

        const data = { values: company, showNotification: true }
        this.props.updateCompanyInfo(data)
        if (values.isDisablePickingStep === true) {
          this.props.savePrintSetting({
            values: {
              ...JSON.parse(this.props.printSetting),
              bill_picked: false,
              invoice_picked: false,
              bill_catchWeightQty: false,
              invoice_catchWeightQty: false,
            },
            showNotification: false,
          })
        }
      }
    })
  }

  onCheckChange = (e: any, key: string) => {
    const checked = e.target.checked
    this.setState({ [key]: checked })
  }

  onItemDescriptionFromatChange = (e: any) => {
    this.setState({ itemDescriptionFormat: e.target.value })
    this.setState({ updateRadio: true })
  }

  render() {
    const { form, companyInfo } = this.props
    const { getFieldDecorator } = form
    const isNS = localStorage.getItem(CACHED_NS_LINKED) !== 'null'
    const {
      isDisablePickingStep,
      isEnableCashSales,
      isEnableDebitMemo,
      itemDescriptionFormat,
      hideMenuLabel,
      hideMenuRequisitionCat,
    } = this.state

    return (
      <PrintSettingWrapper>
        <Form onSubmit={this.onSaveDateSetting}>
          <DetailsTitle style={{ padding: '24px 0' }}>General</DetailsTitle>
          <div>
            Item description format
            <Tooltip title="Please contact support team if you would like to change your item description format.">
              <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
            </Tooltip>
          </div>
          <Radio.Group
            onChange={(e) => this.onItemDescriptionFromatChange(e)}
            value={itemDescriptionFormat}
            style={{ marginLeft: 24 }}
          >
            <ThemeRadio style={{ display: 'block' }} value={'A'}>
              Item name
            </ThemeRadio>
            <ThemeRadio style={{ display: 'block' }} value={'B'}>
              SKU-Item name
              <Tooltip
                title={`For example, for item with SKU "APPFUJ" and item name "Fuji Apples", the item description will display as
              "APPFUJ - Fuji Apples"`}
              >
                <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
              </Tooltip>
            </ThemeRadio>
          </Radio.Group>

          <DetailsTitle style={{ padding: '24px 0' }}>Sales Order</DetailsTitle>
          <Form.Item>
            {getFieldDecorator('isDisablePickingStep', {
              initialValue: isDisablePickingStep,
              valuePropName: 'checked',
            })(
              <Checkbox disabled onChange={(e) => this.onCheckChange(e, 'isDisablePickingStep')}>
                Disable picking step in sales order
                <Tooltip
                  placement="top"
                  title="This option removes the Picked Qty and Billable Qty columns from the Sales Order cart. Functionality for entering catch weights will not be available. For items with a variable ratio between order quantity UOM and price UOM, the configured approximate ratio will be used. Contact Customer Support if you would like to enable this option."
                >
                  <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
                </Tooltip>
              </Checkbox>,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('isEnableCashSales', {
              initialValue: isEnableCashSales,
              valuePropName: 'checked',
            })(<Checkbox onChange={(e) => this.onCheckChange(e, 'isEnableCashSales')}>Enable cash sales</Checkbox>)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('isEnableDebitMemo', {
              initialValue: isEnableDebitMemo,
              valuePropName: 'checked',
            })(<Checkbox disabled={isNS} onChange={(e) => this.onCheckChange(e, 'isEnableDebitMemo')}>
              Enable Debit Memo
                <Tooltip
                  placement="top"
                  title="We should keep it off for DBAS/NETSUITE."
                >
                  <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
                </Tooltip>
                </Checkbox>)}
          </Form.Item>

          <div style={{marginTop: 10}}>Display Logistic Volume and Weight using...</div>
          <Form.Item style={{width: 'fit-content'}}>
            {getFieldDecorator('logisticUnit', {
              initialValue: companyInfo ? companyInfo.logisticUnit : 'IMPERIAL',
              rules: [{ required: false }],
            })(
              <Select disabled>
                <Select.Option value={'IMPERIAL'}>Imperial (Cubic Feet and LBs)</Select.Option>
                <Select.Option value={'METRIC'}>Metric (Cubic Meters and KG)</Select.Option>
              </Select>,
            )}
          </Form.Item>

          <DetailsTitle style={{ padding: '24px 0' }}>Navigation</DetailsTitle>
          <Form.Item>
            {getFieldDecorator('hideMenuLabel', {
              initialValue: hideMenuLabel,
              valuePropName: 'checked',
            })(<Checkbox onChange={(e) => this.onCheckChange(e, 'hideMenuLabel')}>Hide Inventory &gt; Label</Checkbox>)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('hideMenuRequisitionCat', {
              initialValue: hideMenuRequisitionCat,
              valuePropName: 'checked',
            })(
              <Checkbox onChange={(e) => this.onCheckChange(e, 'hideMenuRequisitionCat')}>
                Hide Purchasing &gt; Requisition Catalog
              </Checkbox>,
            )}
          </Form.Item>
          <ThemeButton size="large" type="primary" htmlType="submit" style={{ marginTop: 12 }}>
            Save Changes
          </ThemeButton>
        </Form>
      </PrintSettingWrapper>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.setting

export const AdminSettingTab = withTheme(
  connect(SettingModule)(mapStateToProps)(Form.create<ThemeProps>()(AdminSetting)),
)
