import * as React from 'react'
import { Icon, Button, Input, message, Modal, Dropdown,Menu,Form } from 'antd'
import { uniqueId } from 'lodash'
import { Draggable, DraggableProvided } from 'react-beautiful-dnd'
import { withTheme } from 'emotion-theming'
import { ThemeModal,ThemeIcon } from '~/modules/customers/customers.style'

import { Theme } from '~/common'
import { Icon as IconSvg } from '~/components'

import {
  CategoryWrap,
  CategoryItemTop,
  CategoryItemNameWrap,
  CategoryItemName,
  CategoryInfoWrap,
  CategoryItemSub,
  CategoryItemSubWrap,
  CategorySubItem,
  CategorySubAddButton,
  addToButtonStyle,
  CategorySubNewInputWrap,
  CategorySubNewInputIcons,
  buttonStyle,
} from '../category.style'
import { SaleSection } from '~/schema';
import { SettingDispatchProps } from '../setting.module';


const { confirm } = Modal;

interface CategoryItemProps {
  item: SaleSection
  index: number
  onAddSubCategory: (data: any) => void
  updateSubCategory: (data: any) => void
  deleteSubCategory: (data: any) => void
  updateSection:(data: any) => void
  deleteSection:(data: any) => void
  enabledNS: boolean
}


interface NewCategoryItemProps {
  item: SaleSection
  onCancel: () => void
  addMainCategory: SettingDispatchProps['addMainCategory']

}

interface CategoryItemState {
  subOpen: boolean
  newSubList: SubCategoryInput[]
  updateSubCategoryModal: Boolean
  currentSubCategory: any
  newName: string
}

interface SubCategoryInput {
  id: string
  value: string
}

class NewCategoryItemComponent extends  React.PureComponent<NewCategoryItemProps & { theme: Theme }> {
  state = {
    tempName: ''
  }

  private onClickSaveName = () => {
    this.props.addMainCategory(this.state.tempName)
  }

  private onChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    this.setState({
      tempName: value,
    })
  }


  render () {
    const { item, onCancel } = this.props

    return (
      <CategoryWrap>
        <CategoryItemTop>
          <CategoryItemNameWrap>
            <CategoryItemName>
              <Input placeholder={item.name} onChange={this.onChangeName} />
              <Button
                icon="check"
                type="primary"
                style={{
                  ...buttonStyle,
                  border: `1px solid ${this.props.theme.primary}`,
                  backgroundColor: this.props.theme.primary,
                }}
                onClick={this.onClickSaveName}>SAVE</Button>
              &nbsp;
              <Button type="ghost" onClick={(_e: any) => {onCancel()}}>
                Cancel
              </Button>
            </CategoryItemName>
          </CategoryItemNameWrap>
        </CategoryItemTop>
      </CategoryWrap>
    )
  }
}

class CategoryItemComponent extends React.PureComponent<CategoryItemProps & { theme: Theme }, CategoryItemState> {
  state = {
    subOpen: false,
    newSubList: [],
    updateSubCategoryModal:false,
    updateCategoryModal:false,
    currentSubCategory:{
      name:''
    },
    currentSection:{
      name:''
    },
    newName:'',
    newSectionName:'',
    confirmVisableModal:false,
    operation:0,
    content:''
  }

  subNameNode:any;
  nameNode:any;

  toggleUpdateSubCateGory = (data:any) => {
    this.setState({
      updateSubCategoryModal : true
      currentSubCategory: data
    })
  }

  toggleUpdateCateGory = () => {
    this.setState({
      updateCategoryModal : true
    })
  }

  onChangeSubCategoryName = (e:any) => {
     this.setState({
       newName: e.target.value
     })
  }

  handleUpdate = () => {
    const { currentSubCategory, newName } = this.state
    if(newName != '' && newName != currentSubCategory.name){
      this.setState({
        confirmVisableModal: true,
        operation:1,
        content:`Are you sure you want to change sub-category ${currentSubCategory.name} to sub-category ${newName}? If you are sure, the products in the corresponding sub-category will be reset.`
      })
    }else{
      this.resetInputValue()
      this.setState({
        updateSubCategoryModal : false
      })
    }

  }

  handleCancel = () => {
    this.setState({
      updateSubCategoryModal : false
    })
  }

  handleRemoveCategory = (data:any) => {
    this.props.deleteSubCategory(data.wholesaleCategoryId)
  }

  handleVisibleChange = (data:any,flag:any) => {
    this.setState({
       currentSubCategory:data
    });
  }

  handleMenuClick = (e:any) => {
    const { currentSubCategory } = this.state
    const { item } = this.props
    if (e.key === '1') {
      this.toggleUpdateSubCateGory(currentSubCategory);
    }else if(e.key === '2'){
      this.setState({
        confirmVisableModal: true,
        operation:2,
        content:`Are you sure you want to delete this sub-category ${currentSubCategory.name}? If you are sure, the products in the corresponding sub-category will be reset.`
      })
    }else if(e.key === '3'){
      this.toggleUpdateCateGory();
    }else if(e.key === '4'){
      this.setState({
        confirmVisableModal: true,
        operation:4,
        content:`Are you sure you want to delete this category ${item.name}? If you are sure, all sub-categories will be deleted and the products in the corresponding sub-category will be reset.`
      })
    }
  }

  handleConfirmUpdate = () => {
    const { operation, currentSubCategory,newName ,newSectionName} = this.state
    const { item } = this.props
    if(operation == 1){
      this.props.updateSubCategory(
        {
          id:currentSubCategory.wholesaleCategoryId,
          categoryName:newName
        }
      );
      this.resetInputValue()
      this.setState({
        updateSubCategoryModal:false
      })
    }else if(operation == 2){
      this.props.deleteSubCategory(currentSubCategory.wholesaleCategoryId)
    }else if(operation == 3){
      this.props.updateSection(
        {
          id:item.wholesaleSectionId,
          categoryName:newSectionName
        }
      );
      this.resetInputValue()
      this.setState({
        updateCategoryModal:false
      })
    }
    else if(operation == 4){
      this.props.deleteSection(item.wholesaleSectionId)
    }
    this.setState({
      confirmVisableModal: false,
      operation:0,
      content:''
    })
  }

  handleConfirmCancel = () => {
    this.setState({
      confirmVisableModal: false,
      operation:0,
      content:''
    })
  }

  onChangeCategoryName = (e:any) => {
    this.setState({
      newSectionName: e.target.value
    })
  }

  handleCategoryUpdate = () => {
    const { newSectionName } = this.state
    const { item } = this.props
    if(newSectionName != '' && newSectionName != item.name){
      this.setState({
        confirmVisableModal: true,
        operation:3,
        content:`Are you sure you want to change category ${item.name} to category ${newSectionName}? If you are sure, all sub-categories will be deleted and the products in the corresponding sub-category will be reset.`
      })
    }else{
      this.resetInputValue()
      this.setState({
        updateCategoryModal : false
      })
    }

  }

  handleCategoryCancel = () => {
    this.setState({
      updateCategoryModal:false
    })
    this.resetInputValue()
  }

  resetInputValue = () => {
    if(this.nameNode){
      this.nameNode.state.value = ""
    }
    if(this.subNameNode){
      this.subNameNode.state.value = ""
    }
  }

  render() {
    const { item, index, enabledNS } = this.props
    const { subOpen,updateSubCategoryModal,currentSubCategory,content,confirmVisableModal,updateCategoryModal } = this.state
    //TODO: keeping this outside of component
    /*

    <IconSvg type="pencil" width={18} height={18} viewBox="0 0 18 18" onClick={this.onClickEditName} />
      <Icon type="delete" onClick={this.onClickDeleteCategory} />
    */

    return (
      <>
        <ThemeModal
            closable={false}
            keyboard={true}
            okText="Yes"
            cancelText="No"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ shape: 'round' }}
            // cancelButtonProps={{ style: { display: 'none' } }}
            width={500}
            zIndex={1001}
            visible={confirmVisableModal}
            onOk={this.handleConfirmUpdate}
            onCancel={this.handleConfirmCancel}>
           {content}
        </ThemeModal>
        <ThemeModal
            closable={false}
            keyboard={true}
            okText="Submit"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ shape: 'round' }}
            // cancelButtonProps={{ style: { display: 'none' } }}
            width={500}
            visible={updateSubCategoryModal}
            onOk={this.handleUpdate}
            onCancel={this.handleCancel}>
           <Input placeholder="Sub category name" onChange={this.onChangeSubCategoryName} placeholder={currentSubCategory.name} ref={input => this.subNameNode = input}/>
        </ThemeModal>
        <ThemeModal
            closable={false}
            keyboard={true}
            okText="Submit"
            okButtonProps={{ shape: 'round' }}
            cancelButtonProps={{ shape: 'round' }}
            // cancelButtonProps={{ style: { display: 'none' } }}
            width={500}
            visible={updateCategoryModal}
            onOk={this.handleCategoryUpdate}
            onCancel={this.handleCategoryCancel}>
           <Input placeholder="Category name" onChange={this.onChangeCategoryName} placeholder={item.name} ref={input => this.nameNode = input}/>
        </ThemeModal>
        <Draggable draggableId={item.wholesaleSectionId} index={index}>
          {(providedDraggable: DraggableProvided) => (
            <CategoryWrap ref={providedDraggable.innerRef} {...providedDraggable.draggableProps}>
              <CategoryItemTop>
                <CategoryItemNameWrap>
                  <CategoryItemName>
                    {item.name}
                    {/* <ThemeIcon type="edit" onClick=""/>
                    <ThemeIcon type="close" /> */}
                    {/* <IconSvg type="move" width={24} height={24} viewBox="0 0 24 24" /> */}
                  </CategoryItemName>
                </CategoryItemNameWrap>
                <CategoryInfoWrap>
                  <CategoryItemSub onClick={this.onToggleSub}>
                    <span>{item.categories!.length} sub-categories</span>
                    <Icon type={subOpen ? 'caret-up' : 'caret-down'} />
                  </CategoryItemSub>
                  {!enabledNS && (
                    <div style={{marginRight:'20px'}}>
                      {
                        !item.isDefault &&
                          <Dropdown
                          overlay={
                          <Menu onClick={this.handleMenuClick}>
                            <Menu.Item key="3">Edit</Menu.Item>
                            <Menu.Item key="4">Remove</Menu.Item>
                          </Menu>
                          }
                        >
                          <IconSvg type="move" width={24} height={24} viewBox="0 0 24 24" />
                        </Dropdown>
                      }

                    </div>
                  )}
                  <div {...providedDraggable.dragHandleProps}>
                    {!enabledNS && <Icon type="drag" />}
                  </div>

                </CategoryInfoWrap>
              </CategoryItemTop>
              {this.renderSubCategories()}
            </CategoryWrap>
          )}
        </Draggable>
    </>
    )
  }

  private onToggleSub = () => {
    this.setState({
      subOpen: !this.state.subOpen,
    })
  }


  private onClickNewSubCategory = () => {
    this.setState({
      newSubList: [
        ...this.state.newSubList,
        {
          id: uniqueId(),
          value: '',
        },
      ],
    })
  }

  private onChangeSubCategory = (id: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    this.setState({
      newSubList: this.state.newSubList.map((s) =>
        s.id === id
          ? {
              id,
              value,
            }
          : s,
      ),
    })
  }

  private onClickAddSubCategory = (id: string) => () => {
    const sub = this.state.newSubList.find((s) => s.id === id)!
    const value = sub.value.trim()

    if (!value) {
      message.error('Can not add empty sub category')
      return
    }

    const { item } = this.props
    if (item.categories!.findIndex((s) => s.name === value) !== -1) {
      message.error(`Sub category "${value}" already exist`)
      return
    }
    this.props.onAddSubCategory({subName: value, sectionId: item.wholesaleSectionId})
    this.onClickCancelSubCategory(id)()
  }

  private onClickCancelSubCategory = (id: string) => () => {
    this.setState({
      newSubList: this.state.newSubList.filter((s) => s.id !== id),
    })
  }


  private renderSubCategories() {
    const { subOpen, newSubList } = this.state
    const { item, enabledNS } = this.props

    if (!subOpen) return null

    const menu = (
      <Menu onClick={this.handleMenuClick}>
        <Menu.Item key="1">Edit</Menu.Item>
        <Menu.Item key="2">Remove</Menu.Item>
      </Menu>
    )
    const list = item.categories!.map((s,index) => (
      <CategorySubItem key={s.wholesaleCategoryId}>
        <span>
          {s.name}
          {
                !s.isDefault && !enabledNS &&
                  <Dropdown
                  overlay={menu}
                  onVisibleChange={this.handleVisibleChange.bind(this,s)}
                >
                  <Icon type="more" className="category-edit"/>
                </Dropdown>
          }

          {/* <Icon type="edit" onClick={this.toggleUpdateSubCateGory.bind(this,s)}/>
          <Icon type="edit" onClick={this.handleRemoveCategory.bind(this,s)}/> */}
        </span>

        {/*
          <IconSvg
            type="close-circle"
            width={16}
            height={16}
            viewBox="0 0 16 16"
            onClick={this.onClickDeleteSubCategory(s)}
          />
        */}
      </CategorySubItem>
    ))

    const newList = newSubList.map((s) => (
      <CategorySubNewInputWrap key={s.id}>
        <Input
          defaultValue={s.value}
          style={{ width: '220px' }}
          placeholder="Please enter new sub category"
          onChange={this.onChangeSubCategory(s.id)}
        />
        <CategorySubNewInputIcons>
          <Icon type="check" style={{ marginRight: '5px' }} onClick={this.onClickAddSubCategory(s.id)} />
          <Icon type="close" onClick={this.onClickCancelSubCategory(s.id)} />
        </CategorySubNewInputIcons>
      </CategorySubNewInputWrap>
    ))

    return (
      <CategoryItemSubWrap>
        {!item.isDefault && !enabledNS && (
          <Button
            type="ghost"
            style={{
              ...addToButtonStyle,
              border: `1px solid ${this.props.theme.primary}`,
            }}
            onClick={this.onClickNewSubCategory}
          >
            <CategorySubAddButton>
              <Icon type="plus" />
              <span>Add New Sub-Category</span>
            </CategorySubAddButton>
          </Button>
        )}

        {list}
        {newList}
      </CategoryItemSubWrap>
    )
  }
}


export const NewCategoryItem = withTheme(NewCategoryItemComponent)

export const CategoryItem = withTheme(CategoryItemComponent)
