import * as React from 'react'
import { Icon } from 'antd'
import { Theme } from '~/common'

import { ColorWrap, ColorThemeWrap, ColorThemeName, ColorThemeIcon } from '../theme.style'

interface ColorProps {
  theme: Theme
  name: string
  active: boolean
  onClick: () => void
}

export class ColorComponent extends React.PureComponent<ColorProps, {}> {
  render() {
    const { theme, name, active } = this.props
    return (
      <ColorWrap>
        <ColorThemeWrap active={active} onClick={this.props.onClick}>
          <span />
          <div style={{ backgroundColor: theme.theme }} />
        </ColorThemeWrap>
        <ColorThemeName active={active}>{name}</ColorThemeName>
        <ColorThemeIcon active={active}>
          <Icon type="caret-up" />
        </ColorThemeIcon>
      </ColorWrap>
    )
  }
}
