import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import Form, { FormComponentProps } from 'antd/lib/form'
import {
  ThemeButton,
  DetailsTitle,
  ThemeSelect,
  ThemeTextArea,
  ThemeUpload,
  ThemeRadio,
  ThemeOutlineButton,
  ThemeCheckbox,
  ThemeCheckBox,
  Flex,
} from '~/modules/customers/customers.style'
import { SettingModule, SettingDispatchProps, SettingStateProps } from './../setting.module'
import { GlobalState } from '~/store/reducer'
import { CustomFormItem, PrintSettingWrapper } from './../theme.style'
import { SetionTableLabel } from '~/modules/product/components/product.component.style'
import { Checkbox, Col, Icon, Input, InputNumber, Modal, Radio, Row, Select, Tooltip, notification, Table } from 'antd'
import moment from 'moment'
import styled from '@emotion/styled'
import { Icon as IconSvg } from '~/components'
import { ItemSyncModal } from '~/modules/inventory/components/item-sync-modal.container'
import { InventoryLoading } from '~/modules/inventory'
import { CACHED_NS_LINKED, Theme } from '~/common'
import { AddProductModal } from '~/modules/vendors/accounts/product-list/add-product-modal'
import { Company } from '~/schema'
import _ from 'lodash'

const CustomSelect = styled(Select)`
  .ant-select-selection__choice {
    border-radius: 20px;
    background: #dfdfdf;
  }
  .ant-select-selection__choice__content {
    line-height: 15px;
    overflow: initial;
  }
`

interface DateSettingFields {
  isAutomatically: any
  isForbidLotError: any
  enableAddItemFromProductList: any
  bestByDate: number
  targetDate: string
  hour: number
  minute: number
  ampm: string
  salesPrefix: string
  purchasePrefix: string
  defaultCostType: number
  customShippedLabel: string
  visibleCustomOrderNo: boolean
  purchaseTerms: string
  showPriceSheetSize: boolean
  showPriceSheetWeight: boolean
  showPriceSheetPacking: boolean
  showPriceSheetBrand: boolean
  showPriceSheetOrigin: boolean
  duplicatedOrderPriceStrategy: number
  warehousePickEnabled: boolean
  warehouseReceivingEnabled: boolean
  defaultLastUsedDateEnabled: boolean
  logisticUnit: string
  initialized: boolean
  additionalReceipients: string
  isCopyAssignedSalesRep: boolean

  quickprintPicksheetEnabled: boolean
  quickprintInvoiceEnabled: boolean
  quickprintInvoiceOfficeEnabled: boolean
  quickprintBolEnabled: boolean
  quickprintPicksheetCopies: number
  quickprintInvoiceCopies: number
  quickprintInvoiceOfficeCopies: number
  quickprintBolCopies: number
  visibleCreditMemoItemModal: boolean
  defaultCreditMemoItemIds: string
  defaultLotAssignMethod: number
  disableBetaOverflow: boolean
}

type paymentTermType = 'FINANCIAL_TERM' | 'FINANCIAL_TERM_FIXED_DAYS'
interface IPaymentTerm {
  companyId: number
  dueDays: number | undefined
  id: number
  name: string
  nsId: number | null
  type: paymentTermType
  useFor: number
}

type ThemeProps = SettingDispatchProps &
  SettingStateProps &
  FormComponentProps<DateSettingFields> & {
    theme: Theme
  }

class DateSetting extends React.PureComponent<ThemeProps> {
  state = {
    bestByDate: 0,
    targetDate: this.props.companyInfo ? this.props.companyInfo.targetFulfillmentDate : 'Today',
    hasPurchasePrefix: this.props.companyInfo && this.props.companyInfo.purchasePrefix ? true : false,
    hasSalesPrefix: this.props.companyInfo && this.props.companyInfo.salesPrefix ? true : false,
    defaultCostTypeSetting: [
      { key: 1, value: 'Pull from product default cost' },
      { key: 2, value: 'Pull from last product cost by vendor' },
      { key: 3, value: 'Pull from last product cost across all vendors' },
    ],
    isCustomLabelChecked: this.props.companyInfo && this.props.companyInfo.customShippedLabel ? true : false,
    isCustomComapnyChecked: this.props.companyInfo && this.props.companyInfo.customCompanyPrefix ? true : false,
    isAutomatically: this.props.companyInfo && this.props.companyInfo.isAutomatically ? true : false,
    isForbidLotError: this.props?.companyInfo?.isForbidLotError ? true : false,
    enableAddItemFromProductList: this.props?.companyInfo?.enableAddItemFromProductList ? true : false,
    customShippedLabel: '',
    visibleCustomOrderNo: this.props.companyInfo ? this.props.companyInfo.visibleCustomOrderNo : false,
    purchaseTerms:
      this.props.companyInfo && this.props.companyInfo.purchaseTerms ? this.props.companyInfo.purchaseTerms : '',
    showPriceSheetSize: this.props.companyInfo ? this.props.companyInfo.showPriceSheetSize : true,
    showPriceSheetWeight: this.props.companyInfo ? this.props.companyInfo.showPriceSheetWeight : true,
    showPriceSheetPacking: this.props.companyInfo ? this.props.companyInfo.showPriceSheetPacking : true,
    showPriceSheetBrand: this.props.companyInfo ? this.props.companyInfo.showPriceSheetBrand : true,
    showPriceSheetOrigin: this.props.companyInfo ? this.props.companyInfo.showPriceSheetOrigin : true,
    warehousePickEnabled: this.props.companyInfo ? this.props.companyInfo.warehousePickEnabled : false,
    warehouseReceivingEnabled: this.props.companyInfo ? this.props.companyInfo.warehouseReceivingEnabled : false,
    defaultLastUsedDateEnabled: this.props.companyInfo ? this.props.companyInfo.defaultLastUsedDateEnabled : false,
    companyProductTypes: [],
    paymentTermVisible: false,
    currentPaymentTerm: {},
    syncShow: false,
    selectedRows: [],
    initialized: false,
    additionalReceipients: this.props.companyInfo ? this.props.companyInfo.additionalReceipients : '',
    isCopyAssignedSalesRep: this.props.companyInfo ? this.props.companyInfo.isCopyAssignedSalesRep : false,

    quickprintPicksheetEnabled: this.props.companyInfo ? this.props.companyInfo.quickprintPicksheetEnabled : true,
    quickprintInvoiceEnabled: this.props.companyInfo ? this.props.companyInfo.quickprintInvoiceEnabled : true,
    quickprintInvoiceOfficeEnabled: this.props.companyInfo
      ? this.props.companyInfo.quickprintInvoiceOfficeEnabled
      : true,
    quickprintBolEnabled: this.props.companyInfo ? this.props.companyInfo.quickprintBolEnabled : false,
    quickprintPicksheetCopies: this.props.companyInfo ? this.props.companyInfo.quickprintPicksheetCopies : 1,
    quickprintInvoiceCopies: this.props.companyInfo ? this.props.companyInfo.quickprintInvoiceCopies : 1,
    quickprintInvoiceOfficeCopies: this.props.companyInfo ? this.props.companyInfo.quickprintInvoiceOfficeCopies : 1,
    quickprintBolCopies: this.props.companyInfo ? this.props.companyInfo.quickprintBolCopies : 1,
    defaultCreditMemoItemIds: !!this.props.companyInfo?.defaultCreditMemoItemIds
      ? this.props.companyInfo.defaultCreditMemoItemIds
      : '',
    visibleCreditMemoItemModal: false,
    defaultOverflowEnabled: this.props.companyInfo?.defaultOverflowEnabled,
    disableBetaOverflow: this.props.companyInfo?.defaultLotAssignMethod == 2,
    purchaseOrderDateFilter: this.props.companyInfo ? this.props.companyInfo.purchaseOrderDateFilter : 'Today',
    salesOrderDateFilter: this.props.companyInfo ? this.props.companyInfo.salesOrderDateFile : 'Today',
    receiveDiscrepancyNotify: this.props.companyInfo
      ? this.props.companyInfo.receiveDiscrepancyNotify
        ? this.props.companyInfo.receiveDiscrepancyNotify
        : false
      : false,
    receiveDiscrepancyNotifyReceivers: this.props.companyInfo
      ? this.props.companyInfo.receiveDiscrepancyNotifyReceivers
        ? this.props.companyInfo.receiveDiscrepancyNotifyReceivers
        : ''
      : '',
    defaultWholeNumberEnabled: this.props.companyInfo?.defaultOverflowEnabled,
  }

  creditMemoTableColumns: any[] = [
    {
      title: 'Item',
      dataIndex: 'variety',
      key: 'variety',
      width: 250,
      sorter: (a: any, b: any) => a.variety.localeCompare(b.variety),
    },
    {
      title: 'SKU',
      dataIndex: 'SKU',
      key: 'SKU',
      width: 150,
      sorter: (a: any, b: any) => a.sku.localeCompare(b.sku),
    },
    {
      title: '',
      dataIndex: 'x',
      width: 70,
      render: (value: any, record: any) => {
        return (
          <IconSvg
            className="icon"
            style={{ cursor: 'pointer' }}
            type="trash"
            viewBox="0 0 24 24"
            width={18}
            height={18}
            onClick={() => this.onRemoveCreditMemoItem(record.itemId)}
          />
        )
      },
    },
  ]

  componentDidMount() {
    if (!this.props.printSetting) {
      this.props.getPrintSetting()
    }
    if (!this.props.companyInfo) {
      this.props.getCompanySetting()
    }
    this.props.getCompanyProductAllTypes()
    this.props.getItemListByCondition({ active: 1 })
  }

  static getDerivedStateFromProps(nextProps: ThemeProps, prevState: any) {
    if (prevState.initialized === false && nextProps.companyInfo) {
      return {
        ...prevState,
        targetDate: nextProps.companyInfo.targetFulfillmentDate,
        hasPurchasePrefix: nextProps.companyInfo.purchasePrefix,
        hasSalesPrefix: nextProps.companyInfo.salesPrefix,
        isCustomLabelChecked: nextProps.companyInfo.customShippedLabel,
        isCustomComapnyChecked: nextProps.companyInfo.customCompanyPrefix,
        isAutomatically: nextProps.companyInfo.isAutomatically,
        isForbidLotError: nextProps.companyInfo.isForbidLotError,
        enableAddItemFromProductList: nextProps.companyInfo.enableAddItemFromProductList,
        visibleCustomOrderNo: nextProps.companyInfo.visibleCustomOrderNo,
        purchaseTerms: nextProps.companyInfo.purchaseTerms,
        showPriceSheetSize: nextProps.companyInfo.showPriceSheetSize,
        showPriceSheetWeight: nextProps.companyInfo.showPriceSheetWeight,
        showPriceSheetPacking: nextProps.companyInfo.showPriceSheetPacking,
        showPriceSheetBrand: nextProps.companyInfo.showPriceSheetBrand,
        showPriceSheetOrigin: nextProps.companyInfo.showPriceSheetOrigin,
        warehousePickEnabled: nextProps.companyInfo.warehousePickEnabled,
        warehouseReceivingEnabled: nextProps.companyInfo.warehouseReceivingEnabled,
        defaultLastUsedDateEnabled: nextProps.companyInfo.defaultLastUsedDateEnabled,
        additionalReceipients: nextProps.companyInfo.additionalReceipients,
        isCopyAssignedSalesRep: nextProps.companyInfo.isCopyAssignedSalesRep,
        initialized: true,
        quickprintPicksheetEnabled: nextProps.companyInfo.quickprintPicksheetEnabled ?? true,
        quickprintInvoiceEnabled: nextProps.companyInfo.quickprintInvoiceEnabled ?? true,
        quickprintInvoiceOfficeEnabled: nextProps.companyInfo.quickprintInvoiceOfficeEnabled ?? true,
        quickprintBolEnabled: nextProps.companyInfo.quickprintBolEnabled ?? false,
        quickprintPicksheetCopies: nextProps.companyInfo.quickprintPicksheetCopies ?? 1,
        quickprintInvoiceCopies: nextProps.companyInfo.quickprintInvoiceCopies ?? 1,
        quickprintInvoiceOfficeCopies: nextProps.companyInfo.quickprintInvoiceOfficeCopies ?? 1,
        quickprintBolCopies: nextProps.companyInfo.quickprintBolCopies ?? 1,
        defaultCreditMemoItemIds: nextProps.companyInfo.defaultCreditMemoItemIds ?? '',
        defaultLotAssignMethod: nextProps.companyInfo.defaultLotAssignMethod ?? 1,
        defaultOverflowEnabled: nextProps.companyInfo.defaultOverflowEnabled ?? false,
        disableBetaOverflow: nextProps.companyInfo.defaultLotAssignMethod == 2,
        purchaseOrderDateFilter: nextProps.companyInfo.purchaseOrderDateFilter,
        salesOrderDateFilter: nextProps.companyInfo.salesOrderDateFile,
        receiveDiscrepancyNotify: nextProps.companyInfo.receiveDiscrepancyNotify ?? false,,
        receiveDiscrepancyNotifyReceivers: nextProps.companyInfo.receiveDiscrepancyNotifyReceivers ?? false,,
        defaultWholeNumberEnabled:nextProps.companyInfo.isEnableWholeNumber ?? false,
      }
    }

    const companyProductTypes: IPaymentTerm[] = []
    nextProps.companyProductTypes.forEach((v: IPaymentTerm) => {
      if (v.type === 'FINANCIAL_TERM' || v.type === 'FINANCIAL_TERM_FIXED_DAYS') {
        companyProductTypes.push(v)
      }
    })
    if (JSON.stringify(companyProductTypes) !== JSON.stringify(prevState.companyProductTypes)) {
      return {
        companyProductTypes,
      }
    }
    if (nextProps.companyInfo && nextProps.companyInfo.defaultCreditMemoItemIds != prevState.defaultCreditMemoItemIds) {
      return {
        defaultCreditMemoItemIds: nextProps.companyInfo.defaultCreditMemoItemIds ?? '',
      }
    }

    return null
  }

  onSaveDateSetting = (e: any) => {
    const { form } = this.props
    e.preventDefault()
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        let company: Company = this.props.companyInfo
        company.bestByDate = values.bestByDate
        company.targetFulfillmentDate = values.targetDate
        company.costAllocationsTab = values.costAllocationsTab
        company.defaultCostType = values.defaultCostType
        company.customShippedLabel = values.customShippedLabel
        company.isAutomatically = values.isAutomatically
        company.isForbidLotError = values.isForbidLotError
        company.enableAddItemFromProductList = values.enableAddItemFromProductList
        company.visibleCustomOrderNo = values.visibleCustomOrderNo
        company.purchaseTerms = values.purchaseTerms
        company.customCompanyPrefix = values.customCompanyPrefix
        company.showPriceSheetSize = values.showPriceSheetSize
        company.showPriceSheetWeight = values.showPriceSheetWeight
        company.showPriceSheetPacking = values.showPriceSheetPacking
        company.showPriceSheetBrand = values.showPriceSheetBrand
        company.showPriceSheetOrigin = values.showPriceSheetOrigin
        company.duplicatedOrderPriceStrategy = values.duplicatedOrderPriceStrategy
        company.warehousePickEnabled = values.warehousePickEnabled
        company.warehouseReceivingEnabled = values.warehouseReceivingEnabled
        company.defaultLastUsedDateEnabled = values.defaultLastUsedDateEnabled
        company.useLogoInLotLabelPrint = values.useLogoInLotLabelPrint

        company.quickprintPicksheetEnabled = values.quickprintPicksheetEnabled
        company.quickprintInvoiceEnabled = values.quickprintInvoiceEnabled
        company.quickprintInvoiceOfficeEnabled = values.quickprintInvoiceOfficeEnabled
        company.quickprintBolEnabled = values.quickprintBolEnabled
        company.quickprintPicksheetCopies = values.quickprintPicksheetCopies
        company.quickprintInvoiceCopies = values.quickprintInvoiceCopies
        company.quickprintInvoiceOfficeCopies = values.quickprintInvoiceOfficeCopies
        company.quickprintBolCopies = values.quickprintBolCopies

        company.defaultLotAssignMethod = values.defaultLotAssignMethod
        company.defaultOverflowEnabled = this.state.defaultOverflowEnabled
        company.isEnableWholeNumber = this.state.defaultWholeNumberEnabled

        company.purchaseOrderDateFilter = values.purchaseOrderDateFilter
        company.salesOrderDateFile = values.salesOrderDateFilter

        company.receiveDiscrepancyNotify = values.receiveDiscrepancyNotify
        company.receiveDiscrepancyNotifyReceivers = values.receiveDiscrepancyNotifyReceivers
          ? values.receiveDiscrepancyNotifyReceivers.join(',')
          : ''

        let additionalReceipients = ''
        if (values.additionalReceipients.length > 0) {
          additionalReceipients = values.additionalReceipients.join(',')
        }
        company.additionalReceipients = additionalReceipients

        company.isCopyAssignedSalesRep = values.isCopyAssignedSalesRep
        company.logisticUnit = values.logisticUnit

        if (company.targetFulfillmentDate == 'Today with Cut-off Time') {
          company.cutOffTime = `${values.hour}:${values.minute}:${values.ampm}`
        } else {
          company.cutOffTime = null
        }
        company = {
          ...company,
          purchasePrefix: values.purchasePrefix && values.purchasePrefix.trim() ? values.purchasePrefix : null,
          salesPrefix: values.salesPrefix && values.salesPrefix.trim() ? values.salesPrefix : null,
        }
        const data = { values: company, showNotification: true }
        this.props.updateCompanyInfo(data)
      } else {
        const placement = 'topRight'
        notification.error({
          message: `Notification`,
          description: 'Changes were not saved. Please review the page for errors and try again.',
          placement,
        })
      }
    })
  }

  renderHours = () => {
    let result: any[] = []
    for (let i = 1; i <= 12; i++) {
      result.push(
        <Select.Option key={i} value={i}>
          {i}
        </Select.Option>,
      )
    }
    return result
  }

  renderMinutes = () => {
    const values = [0, 15, 30, 45]
    let result: any[] = []
    values.forEach((el) => {
      result.push(
        <Select.Option key={el} value={el}>
          {el == 0 ? '00' : el}
        </Select.Option>,
      )
    })
    return result
  }

  formatHour = (value: number) => {
    return value > 9 ? value : `0${value}`
  }

  onPoListDateChange = (val: string) => {
    this.setState({ purchaseOrderDateFilter: val })
  }

  onSoListDateChange = (val: string) => {
    this.setState({ salesOrderDateFilter: val })
  }

  onTargetDateChange = (val: string) => {
    this.setState({ targetDate: val })
  }

  onPrefixCheckChange = (e: any, key: string) => {
    const checked = e.target.checked
    this.setState({ [key]: checked })
  }

  onCustomShippedLabelChange = (e: any) => {
    this.setState({ isCustomLabelChecked: e.target.checked })
  }

  onCustomCompanyLabelChange = (e: any) => {
    this.setState({ isCustomComapnyChecked: e.target.checked })
  }

  handleSavePaymentTerm = (data: any) => {
    if (data.id) {
      this.props.updateProductType({
        ...data,
        type: data.dueDays ? 'FINANCIAL_TERM_FIXED_DAYS' : 'FINANCIAL_TERM',
      })
    } else {
      this.props.saveCompanyProductType({
        ...data,
        id: undefined,
        companyId: undefined,
        type: data.dueDays ? 'paymentTermsFixedDays' : 'paymentTerms',
      })
    }

    this.setState({ paymentTermVisible: false })
  }
  handleShowPaymentTermModal = (currentPaymentTerm: IPaymentTerm) => {
    this.setState({ currentPaymentTerm, paymentTermVisible: true })
  }

  getBase64 = (file: any, cb: any) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function() {
      cb(reader.result)
    }
    reader.onerror = function(error) {
      //console.log('Error: ', error);
    }
  }

  onChangeFile = (file: File) => {
    this.getBase64(file, (result: any) => {
      const details = {
        ext: file.name.split('.')[1],
        excel: result,
      }

      const formBody = []
      for (const property in details) {
        const encodedKey = encodeURIComponent(property)
        const encodedValue = encodeURIComponent(details[property])
        formBody.push(encodedKey + '=' + encodedValue)
      }
      this.props.uploadExcel(formBody.join('&'))
    })
    return false
  }

  onClickBatchSyncItem = () => {
    this.props.resetLoading()
    this.setState({
      syncShow: true,
      selectedRows: [],
    })
  }

  onCloseBatchSyncItem = () => {
    this.setState({
      syncShow: false,
      selectedRows: [],
    })
  }

  onRowSelection = (selectedRows: SaleItem[]) => {
    this.setState({
      selectedRows: selectedRows,
    })
  }

  onSyncItem = (itemIds: string[]) => {
    //if (this.props.loading == true) return
    this.props.resetLoading()
    this.props.loadIndex(InventoryLoading.INVENTORY_ITEM_SAVE)

    var isNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    if (!isNS) this.props.syncQBOItems(itemIds)
    else this.props.syncNSItems(itemIds)
  }

  openAddCreditMemoItemModal = () => {
    this.setState({ visibleCreditMemoItemModal: !this.state.visibleCreditMemoItemModal })
  }

  onAddCreditMemoItems = (addedItemIds: string[]) => {
    const currentIdArr = this.state.defaultCreditMemoItemIds.split(';')
    const allIds = [...currentIdArr, ...addedItemIds].filter((id) => !!id)
    let company: Company = this.props.companyInfo
    company.defaultCreditMemoItemIds = allIds.join(';')
    const data = { values: company, showNotification: false }
    this.props.updateCompanyInfo(data)
    this.setState({ visibleCreditMemoItemModal: false })
  }

  onRemoveCreditMemoItem = (itemId: number) => {
    const currentIdArr = this.state.defaultCreditMemoItemIds
      .split(';')
      .filter((id) => !!id)
      .map((x) => +x)
    _.remove(currentIdArr, (id: number) => id == itemId)

    let company: Company = this.props.companyInfo
    company.defaultCreditMemoItemIds = currentIdArr.join(';')
    const data = { values: company, showNotification: false }
    this.props.updateCompanyInfo(data)
  }

  onEnableOverflowChange = (evt: any) => {
    this.setState({ defaultOverflowEnabled: evt.target.checked })
  }

  onEnableWholeNumberChange = (evt: any) => {
    this.setState({ defaultWholeNumberEnabled: evt.target.checked })
  }

  onLotMethodFormFieldChange = (evt: any): number => {
    const curValue = evt.target.value
    if (curValue == 2) {
      this.setState({ defaultOverflowEnabled: false, disableBetaOverflow: true,defaultWholeNumberEnabled:false })
    } else {
      this.setState({ defaultOverflowEnabled: false, disableBetaOverflow: false,defaultWholeNumberEnabled:false })
    }

    return curValue
  }

  render() {
    const { form, companyInfo, printSetting, saleItems } = this.props
    const { getFieldDecorator } = form
    const {
      bestByDate,
      hasPurchasePrefix,
      enableAddItemFromProductList,
      hasSalesPrefix,
      defaultCostTypeSetting,
      customShippedLabel,
      isCustomLabelChecked,
      isAutomatically,
      isForbidLotError,
      visibleCustomOrderNo,
      purchaseTerms,
      showPriceSheetSize,
      showPriceSheetWeight,
      showPriceSheetPacking,
      showPriceSheetBrand,
      showPriceSheetOrigin,
      isCustomComapnyChecked,
      warehousePickEnabled,
      warehouseReceivingEnabled,
      defaultLastUsedDateEnabled,
      companyProductTypes,
      paymentTermVisible,
      currentPaymentTerm,
      additionalReceipients,
      isCopyAssignedSalesRep,

      quickprintPicksheetEnabled,
      quickprintInvoiceEnabled,
      quickprintInvoiceOfficeEnabled,
      quickprintBolEnabled,
      quickprintPicksheetCopies,
      quickprintInvoiceCopies,
      quickprintInvoiceOfficeCopies,
      quickprintBolCopies,
      defaultCreditMemoItemIds,
      defaultOverflowEnabled,
      disableBetaOverflow,
      receiveDiscrepancyNotify,
      receiveDiscrepancyNotifyReceivers,
      defaultWholeNumberEnabled
    } = this.state

    const creditItemIdArr = defaultCreditMemoItemIds
      .split(';')
      .filter((x) => !!x)
      .map((x) => +x)
    const defaultCreditMemoItems = saleItems.filter((el) => creditItemIdArr.indexOf(el.itemId) > -1)
    // console.log(creditItemIdArr, saleItems, defaultCreditMemoItems)
    const fulfillmentOptions = ['Today', 'Today with Cut-off Time', 'Tomorrow']
    //salesOrder data and purchase order list
    const dataListOptions = [
      'Past 7 Days',
      'Past 30 Days',
      'Yesterday',
      'Today',
      'Tomorrow',
      'Today and Tomorrow',
      'Next 7 Days',
      'Next 30 Days',
      'Past 30 Days and Next 30 Days',
    ]
    const db_dataListOptions = [
      'PAST_7_DAYS',
      'PAST_30_DAYS',
      'YESTERDAY',
      'TODAY',
      'TOMORROW',
      'TODAY_AND_TOMORROW',
      'NEXT_7_DAYS',
      'NEXT_30_DAYS',
      'PAST_30_DAYS_AND_NEXT_30_DAYS',
    ]
    let cutOff: string[] = []
    if (companyInfo && companyInfo.cutOffTime) {
      cutOff = companyInfo.cutOffTime.split(':')
    }
    const isNs = localStorage.getItem(CACHED_NS_LINKED) != 'null'

    let initialAdditionalReceipients = []
    if (additionalReceipients) {
      initialAdditionalReceipients = additionalReceipients.split(',')
    }

    const userPrintSetting = printSetting ? JSON.parse(printSetting) : null
    const pick_enabled =
      !userPrintSetting || typeof userPrintSetting.pick_enabled === 'undefined' ? true : userPrintSetting.pick_enabled
    const invoice_enabled =
      !userPrintSetting || typeof userPrintSetting.invoice_enabled === 'undefined'
        ? true
        : userPrintSetting.invoice_enabled
    const bill_enabled =
      !userPrintSetting || typeof userPrintSetting.bill_enabled === 'undefined' ? true : userPrintSetting.bill_enabled

    // console.log('* Company Info', this.props.companyInfo)
    return (
      <PrintSettingWrapper>
        <Form onSubmit={this.onSaveDateSetting}>
          <DetailsTitle style={{ padding: '24px 0 10px' }}>Purchase Order</DetailsTitle>
          {/* <div>
            <CustomFormItem>
              {getFieldDecorator('warehouseReceivingEnabled', {
                initialValue: warehouseReceivingEnabled,
                valuePropName: 'checked',
              })(<Checkbox className="non-bold">Enable Warehouse Receiving Role
                <Tooltip title="When enabled, deactivates certain financial information from being displayed in Receiving tab and PO list view.">
                  <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
                </Tooltip>
              </Checkbox>)}
            </CustomFormItem>
          </div> */}
          <CustomFormItem>
            {getFieldDecorator('hasPurchasePrefix', {
              initialValue: hasPurchasePrefix,
              valuePropName: 'checked',
            })(
              <Checkbox className="non-bold" onChange={(e) => this.onPrefixCheckChange(e, 'hasPurchasePrefix')}>
                Append custom prefix before Purchase Order Number
              </Checkbox>,
            )}
          </CustomFormItem>
          {hasPurchasePrefix && (
            <div style={{ paddingBottom: 16, width: '30%', marginLeft: 24 }}>
              <div>Custom Prefix</div>
              <Form.Item>
                {getFieldDecorator('purchasePrefix', {
                  initialValue: companyInfo && companyInfo.purchasePrefix ? companyInfo.purchasePrefix : '',
                  rules: [{ required: false }],
                })(<Input maxLength={6} />)}
              </Form.Item>
            </div>
          )}
          <div style={{ width: 353 }}>
            <div>Default purchase order list date filter</div>
            <Form.Item style={{ marginBottom: 24 }}>
              {getFieldDecorator('purchaseOrderDateFilter', {
                initialValue:
                  companyInfo && companyInfo.purchaseOrderDateFilter ? companyInfo.purchaseOrderDateFilter : 'Today',
                rules: [{ required: false }],
              })(
                <Select style={{ fontWeight: 'normal' }} onChange={this.onPoListDateChange}>
                  {db_dataListOptions.map((el, index) => {
                    return (
                      <Select.Option value={el} key={index}>
                        {dataListOptions[index]}
                      </Select.Option>
                    )
                  })}
                </Select>,
              )}
            </Form.Item>
          </div>
          <div style={{ minWidth: 320, width: 'fit-content' }}>
            Cost allocations tab
            <Form.Item>
              {getFieldDecorator('costAllocationsTab', {
                initialValue: companyInfo && companyInfo.costAllocationsTab ? companyInfo.costAllocationsTab : 1,
                rules: [{ required: false }],
              })(
                <Select style={{ fontWeight: 'normal' }} onChange={this.onTargetDateChange}>
                  {['Cost allocations tab', 'Related bills tab (Legacy - QBO systems only)'].map((el, index) => {
                    return (
                      <Select.Option value={index + 1} key={index + 1}>
                        {el}
                      </Select.Option>
                    )
                  })}
                </Select>,
              )}
            </Form.Item>
          </div>

          <Row style={{ marginTop: 10 }}>
            <Col span={10}>
              Item default cost
              <Tooltip title="This option sets the default cost values for items in a purchase order/purcahse entry.">
                <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
              </Tooltip>
              <Form.Item>
                {getFieldDecorator('defaultCostType', {
                  initialValue: companyInfo ? companyInfo.defaultCostType : 1,
                  rules: [{ required: false }],
                })(
                  <ThemeSelect style={{ fontWeight: 'normal' }} onChange={this.onTargetDateChange}>
                    {defaultCostTypeSetting.map((el: any) => {
                      return (
                        <Select.Option value={el.key} key={el.key}>
                          {el.value}
                        </Select.Option>
                      )
                    })}
                  </ThemeSelect>,
                )}
              </Form.Item>
            </Col>
          </Row>
          <div style={{ marginTop: 10 }}>Best By Date</div>
          <Form.Item>
            {getFieldDecorator('bestByDate', {
              initialValue: companyInfo ? companyInfo.bestByDate : bestByDate,
              rules: [{ required: true, message: 'Best By Date is required!' }],
            })(<Input type="number" placeholder="Best By Date" min={0} addonBefore="Received Date + " />)}
          </Form.Item>
          <CustomFormItem>
            {getFieldDecorator('receiveDiscrepancyNotify', {
              initialValue: receiveDiscrepancyNotify,
              valuePropName: 'checked',
            })(
              <Checkbox className="non-bold" onChange={(e) => this.onPrefixCheckChange(e, 'receiveDiscrepancyNotify')}>
                Enable email notification for receive discrepancies
              </Checkbox>,
            )}
          </CustomFormItem>
          {receiveDiscrepancyNotify ? (
            <Form.Item>
              {getFieldDecorator('receiveDiscrepancyNotifyReceivers', {
                initialValue: receiveDiscrepancyNotifyReceivers.split(','),
                rules: [{ required: false }],
              })(
                <>
                  <p style={{ marginBottom: '0px', height: '30px',fontWeight:"normal" }}>Send to:</p>
                  <Select
                    mode="tags"
                    value={this.state.receiveDiscrepancyNotifyReceivers.split(',')}
                    onChange={(e) => {
                      this.setState({ receiveDiscrepancyNotifyReceivers: e.join(',') })
                    }}
                  />
                </>,
              )}
            </Form.Item>
          ) : (
            <></>
          )}

          <DetailsTitle style={{ padding: '24px 0 10px' }}>Sales Order</DetailsTitle>
          <CustomFormItem>
            {getFieldDecorator('hasSalesPrefix', {
              initialValue: hasSalesPrefix,
              valuePropName: 'checked',
            })(
              <Checkbox className="non-bold" onChange={(e) => this.onPrefixCheckChange(e, 'hasSalesPrefix')}>
                Append custom prefix before Sales Order Number
              </Checkbox>,
            )}
          </CustomFormItem>
          {hasSalesPrefix && (
            <div style={{ paddingBottom: 16, width: '30%', marginLeft: 24 }}>
              <div>Custom Prefix</div>
              <Form.Item>
                {getFieldDecorator('salesPrefix', {
                  initialValue: companyInfo && companyInfo.salesPrefix ? companyInfo.salesPrefix : '',
                  rules: [{ required: false }],
                })(<Input maxLength={6} />)}
              </Form.Item>
            </div>
          )}
          <CustomFormItem style={{ margin: '10px 0 20px' }}>
            {getFieldDecorator('visibleCustomOrderNo', {
              initialValue: visibleCustomOrderNo,
              valuePropName: 'checked',
            })(
              <Checkbox className="non-bold" onChange={(e) => this.onPrefixCheckChange(e, 'visibleCustomOrderNo')}>
                Enable Custom Order Number
              </Checkbox>,
            )}
          </CustomFormItem>
          <Row style={{ marginTop: 10 }}>
            <Col span={14}>
              {/* <Form.Item colon={false} label={<SetionTableLabel className="form-label">Default Target Fulfillment Date</SetionTableLabel>}> */}
              <div>Default Sales order list date filter</div>
              <Form.Item style={{ marginBottom: 24 }}>
                {getFieldDecorator('salesOrderDateFilter', {
                  initialValue:
                    companyInfo && companyInfo.salesOrderDateFile ? companyInfo.salesOrderDateFile : 'Today',
                  rules: [{ required: false }],
                })(
                  <Select style={{ fontWeight: 'normal' }} onChange={this.onSoListDateChange}>
                    {db_dataListOptions.map((el, index) => {
                      return (
                        <Select.Option value={el} key={index}>
                          {dataListOptions[index]}
                        </Select.Option>
                      )
                    })}
                  </Select>,
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row style={{ marginTop: 0 }}>
            <Col span={14}>
              <div>Default Target Fulfillment Date</div>
              <Form.Item>
                {getFieldDecorator('targetDate', {
                  initialValue:
                    companyInfo && companyInfo.targetFulfillmentDate ? companyInfo.targetFulfillmentDate : 'Today',
                  rules: [{ required: false }],
                })(
                  <Select style={{ fontWeight: 'normal' }} onChange={this.onTargetDateChange}>
                    {fulfillmentOptions.map((el, index) => {
                      return (
                        <Select.Option value={el} key={index}>
                          {el}
                        </Select.Option>
                      )
                    })}
                  </Select>,
                )}
              </Form.Item>
              <div style={{ marginLeft: 24 }}>
                <CustomFormItem>
                  {getFieldDecorator('defaultLastUsedDateEnabled', {
                    initialValue: defaultLastUsedDateEnabled,
                    valuePropName: 'checked',
                  })(
                    <Checkbox className="non-bold">
                      Default to last used date within session
                      <Tooltip title='For the first order in the session, the "Default Target Fulfillment Date" setting will apply. Thereafter, the target fulfillment date will be set equal to the most recently created order by the user.'>
                        <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
                      </Tooltip>
                    </Checkbox>,
                  )}
                </CustomFormItem>
              </div>
            </Col>
            {this.state.targetDate == 'Today with Cut-off Time' && (
              <Col span={9} offset={1}>
                <div>&nbsp;</div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <div
                    style={{
                      flex: 2,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'flex-end',
                    }}
                  >
                    Cut-off time
                    <Tooltip title='All sales orders created after the cut-off time will have Target Fulfillment Date "Tomorrow"'>
                      <Icon type="info-circle" style={{ padding: '0 6px' }} />
                    </Tooltip>
                  </div>

                  <Form.Item style={{ flex: 1, fontWeight: 'normal' }}>
                    {getFieldDecorator('hour', {
                      initialValue:
                        cutOff.length && cutOff[0]
                          ? this.formatHour(Number.parseInt(cutOff[0]))
                          : moment().hour() > 12
                          ? this.formatHour(moment().hour() - 12)
                          : this.formatHour(moment().hour()),
                      rules: [{ required: false }],
                    })(<Select>{this.renderHours()}</Select>)}
                  </Form.Item>
                  <div style={{ padding: '0 3px' }}>:</div>
                  <Form.Item style={{ flex: 1, fontWeight: 'normal' }}>
                    {getFieldDecorator('minute', {
                      initialValue: cutOff.length && cutOff[1] ? this.formatHour(Number.parseInt(cutOff[1])) : 0,
                      rules: [{ required: false }],
                    })(<Select>{this.renderMinutes()}</Select>)}
                  </Form.Item>
                  <Form.Item style={{ flex: 1, paddingLeft: 10, fontWeight: 'normal' }}>
                    {getFieldDecorator('ampm', {
                      initialValue:
                        cutOff.length && cutOff[2] ? cutOff[2].toLowerCase() : moment().hour() > 12 ? 'pm' : 'am',
                      rules: [{ required: false }],
                    })(
                      <Select>
                        <Select.Option value="am">AM</Select.Option>
                        <Select.Option value="pm">PM</Select.Option>
                      </Select>,
                    )}
                  </Form.Item>
                </div>
              </Col>
            )}
          </Row>
          <Row>
            <Col span={14}>
              <div style={{ marginTop: 10 }}>
                <div className="text-left">Item lot assignment method default for new items</div>
                <Form.Item>
                  {getFieldDecorator('defaultLotAssignMethod', {
                    initialValue: companyInfo?.defaultLotAssignMethod ?? 1,
                    getValueFromEvent: this.onLotMethodFormFieldChange,
                  })(
                    <Radio.Group>
                      <div>
                        <ThemeRadio style={{ fontWeight: 'normal' }} value={1}>
                          Auto-assign lot(s) based on FIFO
                        </ThemeRadio>
                      </div>
                      <Checkbox
                        style={{ fontWeight: 'normal' }}
                        onChange={this.onEnableOverflowChange}
                        disabled={disableBetaOverflow}
                        checked={defaultOverflowEnabled}
                        style={{ fontWeight: 'normal', paddingLeft: 30 }}
                      >
                        [BETA] Enable “overflow” to multiple lots{' '}
                      </Checkbox>
                      <Checkbox
                        style={{ fontWeight: 'normal' }}
                        onChange={this.onEnableWholeNumberChange}
                        disabled={disableBetaOverflow}
                        checked={defaultWholeNumberEnabled}
                        style={{ fontWeight: 'normal', paddingLeft: 22 }}
                      >
                        Auto-assign to lots using whole number values only
                        <Tooltip
                          title="Auto-assign will not break units smaller than whole numbers (unless user has entered a
                            partial unit)"
                        >
                          <Icon type="info-circle" style={{ padding: '0 6px' }} />
                        </Tooltip>
                      </Checkbox>
                      <div>
                        <ThemeRadio style={{ fontWeight: 'normal' }} value={2}>
                          Manually assign to lot(s)
                        </ThemeRadio>
                      </div>
                    </Radio.Group>,
                  )}
                </Form.Item>
              </div>
            </Col>
          </Row>
          <Row style={{ marginTop: 10 }}>
            <Col span={14}>
              <Form.Item>
                {getFieldDecorator('enableAddItemFromProductList', {
                  initialValue: enableAddItemFromProductList,
                  valuePropName: 'checked',
                })(
                  <Checkbox className="non-bold" style={{ fontWeight: 'normal' }}>
                    Enable “add item from customer product list” behavior
                    <Tooltip title="Tooltip: This option will update the “add item” search typeahead in the sales order page to only search from the customer’s product list. The “add item” popup will still search the full products list, and the button text will be updated to reflect this.">
                      <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
                    </Tooltip>
                  </Checkbox>,
                )}
              </Form.Item>
            </Col>
          </Row>

          <DetailsTitle className="quick-print">
            Quick Print
            <Tooltip title='Set up the documents to be printed when the user clicks on "Quick Print" on the sales order page. "Invoice (office copy)" is the invoice with "Office Copy" printed at the top.'>
              <Icon
                style={{ marginLeft: '10px', fontSize: 14, position: 'absolute', marginTop: 1 }}
                type="info-circle"
              />
            </Tooltip>
          </DetailsTitle>
          <div style={{ marginLeft: 24 }}>
            <Flex>
              <CustomFormItem
                label={<span className="common-label">Pick sheet</span>}
                colon={false}
                className="checkbox"
                style={{ width: 200 }}
              >
                {getFieldDecorator('quickprintPicksheetEnabled', {
                  valuePropName: 'checked',
                  initialValue: quickprintPicksheetEnabled,
                })(<Checkbox />)}
              </CustomFormItem>

              {pick_enabled && (
                <CustomFormItem label={<span className="common-label">Copies</span>} colon={false} className="inline">
                  {getFieldDecorator('quickprintPicksheetCopies', {
                    initialValue: quickprintPicksheetCopies,
                  })(this.getCopiesSelector())}
                </CustomFormItem>
              )}
            </Flex>
            <Flex>
              <CustomFormItem
                label={<span className="common-label">Invoice</span>}
                colon={false}
                className="checkbox"
                style={{ width: 200 }}
              >
                {getFieldDecorator('quickprintInvoiceEnabled', {
                  valuePropName: 'checked',
                  initialValue: quickprintInvoiceEnabled,
                })(<Checkbox />)}
              </CustomFormItem>
              {invoice_enabled && (
                <CustomFormItem label={<span className="common-label">Copies</span>} colon={false} className="inline">
                  {getFieldDecorator('quickprintInvoiceCopies', {
                    initialValue: quickprintInvoiceCopies,
                  })(this.getCopiesSelector())}
                </CustomFormItem>
              )}
            </Flex>
            <Flex>
              <CustomFormItem
                label={<span className="common-label">Invoice (office copy)</span>}
                colon={false}
                className="checkbox"
                style={{ width: 200 }}
              >
                {getFieldDecorator('quickprintInvoiceOfficeEnabled', {
                  valuePropName: 'checked',
                  initialValue: quickprintInvoiceOfficeEnabled,
                })(<Checkbox />)}
              </CustomFormItem>
              {invoice_enabled && (
                <CustomFormItem label={<span className="common-label">Copies</span>} colon={false} className="inline">
                  {getFieldDecorator('quickprintInvoiceOfficeCopies', {
                    initialValue: quickprintInvoiceOfficeCopies,
                  })(this.getCopiesSelector())}
                </CustomFormItem>
              )}
            </Flex>
            <Flex>
              <CustomFormItem
                label={<span className="common-label">Bill of lading</span>}
                colon={false}
                className="checkbox"
                style={{ width: 200 }}
              >
                {getFieldDecorator('quickprintBolEnabled', {
                  valuePropName: 'checked',
                  initialValue: quickprintBolEnabled,
                })(<Checkbox />)}
              </CustomFormItem>
              {bill_enabled && (
                <CustomFormItem label={<span className="common-label">Copies</span>} colon={false} className="inline">
                  {getFieldDecorator('quickprintBolCopies', {
                    initialValue: quickprintBolCopies,
                  })(this.getCopiesSelector())}
                </CustomFormItem>
              )}
            </Flex>
          </div>

          <Row style={{ marginTop: 24 }}>
            <Col span={14}>
              {/* <Form.Item colon={false} label={<SetionTableLabel className="form-label">Default Target Fulfillment Date</SetionTableLabel>}> */}
              <div>Item pricing for duplicated orders</div>
              <Form.Item>
                {getFieldDecorator('duplicatedOrderPriceStrategy', {
                  initialValue: companyInfo ? companyInfo.duplicatedOrderPriceStrategy : 1,
                  rules: [{ required: false }],
                })(
                  <Select style={{ fontWeight: 'normal' }}>
                    <Select.Option value={1}>Copy item prices from original order</Select.Option>
                    <Select.Option value={2}>Set item prices based on customer and item pricing settings</Select.Option>
                  </Select>,
                )}
              </Form.Item>
            </Col>
          </Row>
          <div style={{ marginTop: 10 }}>
            <CustomFormItem>
              {getFieldDecorator('isAutomatically', {
                initialValue: isAutomatically,
                valuePropName: 'checked',
              })(<Checkbox className="non-bold">Automatically add items in sales order to a new credit memo</Checkbox>)}
            </CustomFormItem>
          </div>
          <div style={{ marginTop: 10 }}>
            <Checkbox checked={isCustomLabelChecked} onChange={this.onCustomShippedLabelChange}>
              Set custom label for &quot;Shipped&quot; order status
            </Checkbox>
          </div>
          <br />
          {isCustomLabelChecked && (
            <div>
              <div>&quot;Shipped&quot; order status label</div>
              <Form.Item>
                {getFieldDecorator('customShippedLabel', {
                  initialValue: companyInfo ? companyInfo.customShippedLabel : customShippedLabel,
                  rules: [{ required: false }],
                })(<Input placeholder="Custom Shipped Label" maxLength={10} />)}
              </Form.Item>
            </div>
          )}
          <div style={{ marginTop: 10 }}></div>
          <div style={{ marginTop: 10 }}>
            <CustomFormItem>
              {getFieldDecorator('isForbidLotError', {
                initialValue: isForbidLotError,
                valuePropName: 'checked',
              })(
                <Checkbox className="non-bold">
                  Do not allow shipping orders with lot errors (i.e. no lot selected, assigned lot inventory is less
                  than order/picked quantity)
                </Checkbox>,
              )}
            </CustomFormItem>
          </div>

          <div style={{ marginTop: 10 }}>
            <CustomFormItem>
              {getFieldDecorator('warehousePickEnabled', {
                initialValue: warehousePickEnabled,
                valuePropName: 'checked',
              })(
                <Checkbox className="non-bold">
                  Enable Warehouse Pick Tab for Warehouse Role
                  <Tooltip title="Enables a sales order pick tab and deactivates certain financial information form being displayed.">
                    <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
                  </Tooltip>
                </Checkbox>,
              )}
            </CustomFormItem>
          </div>

          <DetailsTitle style={{ padding: '24px 0 20px' }}>Price Sheets</DetailsTitle>
          <div>Purchase terms</div>
          <Form.Item>
            {getFieldDecorator('purchaseTerms', {
              initialValue: purchaseTerms,
            })(<ThemeTextArea />)}
          </Form.Item>

          <div style={{ marginTop: 10 }}>Display Fields</div>
          <Form.Item>
            {getFieldDecorator('showPriceSheetSize', {
              valuePropName: 'checked',
              initialValue: showPriceSheetSize,
            })(<Checkbox className="non-bold">Size</Checkbox>)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('showPriceSheetWeight', {
              valuePropName: 'checked',
              initialValue: showPriceSheetWeight,
            })(<Checkbox className="non-bold">Weight</Checkbox>)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('showPriceSheetPacking', {
              valuePropName: 'checked',
              initialValue: showPriceSheetPacking,
            })(<Checkbox className="non-bold">Packing</Checkbox>)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('showPriceSheetBrand', {
              valuePropName: 'checked',
              initialValue: showPriceSheetBrand,
            })(<Checkbox className="non-bold">Brand</Checkbox>)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('showPriceSheetOrigin', {
              valuePropName: 'checked',
              initialValue: showPriceSheetOrigin,
            })(<Checkbox className="non-bold">Origin</Checkbox>)}
          </Form.Item>

          <div style={{ paddingTop: 24 }}>
            <div>
              Additional recipients for order receipt & confirmation emails
              <Tooltip title="Designate additional email addresses to be copied on order receipt and confirmation emails, in addition to the email address for the individual placing the order.">
                <Icon style={{ marginLeft: '10px' }} type="info-circle" theme="filled" />
              </Tooltip>
            </div>
            <Form.Item label="Send to">
              {getFieldDecorator('additionalReceipients', {
                initialValue: initialAdditionalReceipients,
              })(<CustomSelect mode="tags" placeholder="" dropdownStyle={{ display: 'none' }} />)}
            </Form.Item>
          </div>
          <CustomFormItem>
            {getFieldDecorator('isCopyAssignedSalesRep', {
              initialValue: isCopyAssignedSalesRep,
              valuePropName: 'checked',
            })(<Checkbox className="non-bold">Copy the assigned sales representative</Checkbox>)}
          </CustomFormItem>

          <DetailsTitle style={{ padding: '24px 0 10px' }}>Labels</DetailsTitle>
          <div style={{ marginTop: 10 }}>
            <Checkbox checked={isCustomComapnyChecked} onChange={this.onCustomCompanyLabelChange}>
              Enable Custom Company Prefix
            </Checkbox>
          </div>
          {isCustomComapnyChecked && (
            <div style={{ paddingBottom: 16, width: '30%', marginLeft: 24 }}>
              <div style={{ marginTop: 5 }}>Custom Prefix</div>
              <Form.Item>
                {getFieldDecorator('customCompanyPrefix', {
                  initialValue: companyInfo && companyInfo.customCompanyPrefix ? companyInfo.customCompanyPrefix : '',
                  rules: [
                    { required: true, message: 'Required' },
                    {
                      len: 3,
                      message: 'Needs to be 3 characters long',
                    },
                  ],
                })(<Input maxLength={3} />)}
              </Form.Item>
            </div>
          )}
          <CustomFormItem colon={false} className="checkbox">
            {getFieldDecorator('useLogoInLotLabelPrint', {
              // valuePropName: 'checked',
              initialValue:
                companyInfo && typeof companyInfo.useLogoInLotLabelPrint !== 'undefined'
                  ? companyInfo.useLogoInLotLabelPrint
                  : true,
            })(
              <Radio.Group>
                <ThemeRadio value={true}>Use LOGO</ThemeRadio>
                <ThemeRadio value={false}>Use Company Prefix</ThemeRadio>
              </Radio.Group>,
            )}
          </CustomFormItem>
          <div>
            <DetailsTitle style={{ padding: '24px 0 10px' }}>
              Payment terms
              <Tooltip
                placement="top"
                title="Tool tip: For systems connected to QBO, QBO invoice sync will sync the calculated due date, not the payment term. For systems connected to NetSuite, please contact corporate accounting to add/edit payment terms. Due date is calculated in NetSuite."
              >
                <Icon type="info-circle" style={{ fontSize: 14, marginLeft: 10 }} />
              </Tooltip>
            </DetailsTitle>
            <PaymentTerms>
              {companyProductTypes.map((v: IPaymentTerm) => (
                <div className="item">
                  <span>
                    {v.name} <span style={{ color: '#a2a2a2' }}>{v.dueDays && `(due in ${v.dueDays} days)`}</span>
                  </span>
                  {localStorage.getItem('nsLinked') === 'null' ? (
                    <Icon className="edit" type="form" onClick={() => this.handleShowPaymentTermModal(v)} />
                  ) : null}
                </div>
              ))}
            </PaymentTerms>
            {paymentTermVisible && (
              <PaymentTermModal
                currentPaymentTerm={currentPaymentTerm}
                onCancel={() => this.setState({ paymentTermVisible: false, currentPaymentTerm: {} })}
                onSave={this.handleSavePaymentTerm}
              />
            )}
            <ThemeOutlineButton
              style={{ marginTop: 10 }}
              disabled={localStorage.getItem('nsLinked') !== 'null'}
              onClick={() =>
                this.handleShowPaymentTermModal({
                  companyId: 0,
                  dueDays: undefined,
                  id: 0,
                  name: '',
                  nsId: null,
                  type: 'FINANCIAL_TERM_FIXED_DAYS',
                  useFor: 1,
                })
              }
            >
              Add payment term
            </ThemeOutlineButton>
          </div>

          <DetailsTitle style={{ padding: '24px 0 10px' }}>Products</DetailsTitle>
          <div style={{ width: 130 }}>
            <ThemeUpload className="outline" showUploadList={false} beforeUpload={this.onChangeFile}>
              Import products
            </ThemeUpload>
          </div>
          <div style={{ width: 'fit-content', marginTop: 5 }}>
            <ThemeOutlineButton onClick={this.onClickBatchSyncItem} style={{ width: '100%' }}>
              <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
              Batch Sync to {isNs ? 'NS' : 'QBO'}
            </ThemeOutlineButton>
          </div>

          <div className="credit-memo-item-section">
            <DetailsTitle style={{ paddingTop: 24, marginBottom: 10 }}>Credit Memo</DetailsTitle>
            <div>Add items to be included in credit memos (you will also be able to credit any sold items).</div>
            <Table
              style={{ maxWidth: 500 }}
              columns={this.creditMemoTableColumns}
              dataSource={defaultCreditMemoItems}
              pagination={false}
            />
            <ThemeButton style={{ marginTop: 8 }} onClick={this.openAddCreditMemoItemModal}>
              Add Item
            </ThemeButton>
            <AddProductModal
              theme={this.props.theme}
              visible={this.state.visibleCreditMemoItemModal}
              onCancel={this.openAddCreditMemoItemModal}
              onOk={this.onAddCreditMemoItems}
              saleItems={this.props.saleItems}
              priceSheetItems={this.state.defaultCreditMemoItemIds.split(';')}
              module={'setting'} // add module name as props to identify priceSheetItems datasource
            />
          </div>

          <ThemeButton size="large" type="primary" htmlType="submit" style={{ marginTop: 40 }}>
            Save Changes
          </ThemeButton>
        </Form>

        <ItemSyncModal
          visible={this.state.syncShow}
          onCancel={this.onCloseBatchSyncItem}
          onOk={this.onSyncItem}
          items={this.props.saleItems}
          loading={this.props.loading.length == 0}
          onSelectionChange={this.onRowSelection}
        />
      </PrintSettingWrapper>
    )
  }

  getCopiesSelector = () => {
    return (
      <ThemeSelect style={{ width: 60 }}>
        {[...Array(10).keys()].map((i) => (
          <Select.Option value={i + 1}>{i + 1}</Select.Option>
        ))}
      </ThemeSelect>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.setting

export const DateSettingTab = withTheme(connect(SettingModule)(mapStateToProps)(Form.create<ThemeProps>()(DateSetting)))

const PaymentTermModal: React.FC<{
  currentPaymentTerm: IPaymentTerm
  onCancel: () => void
  onSave: (value: IPaymentTerm) => void
}> = ({ currentPaymentTerm, onCancel, onSave }) => {
  const [state, setState] = React.useState(currentPaymentTerm)
  const [checked, setChecked] = React.useState(!!state.dueDays)

  const handleChangeCheckbox = (e) => {
    setChecked(e.target.checked)
    if (!e.target.checked) {
      setState({ ...state, dueDays: undefined, type: 'FINANCIAL_TERM' })
    }
  }

  return (
    <ModalWrap
      visible
      title={!currentPaymentTerm.id ? 'Add Payment Term' : 'Edit Payment Term'}
      onCancel={onCancel}
      footer={[
        <ThemeButton key={1} onClick={() => onSave(state)}>
          Save
        </ThemeButton>,
        <span key={2} onClick={onCancel} style={{ cursor: 'pointer' }}>
          Cancel
        </span>,
      ]}
    >
      <Section>
        <span>Name</span>
        <Input className="input" value={state.name} onChange={(e) => setState({ ...state, name: e.target.value })} />
        <Checkbox checked={checked} onChange={(e) => handleChangeCheckbox(e)}>
          Due in fixed number of days
        </Checkbox>
        {checked && (
          <div className="inputNumber">
            <InputNumber value={state.dueDays} onChange={(dueDays) => setState({ ...state, dueDays })} />
            <span>days</span>
          </div>
        )}
      </Section>
    </ModalWrap>
  )
}

const ModalWrap = styled(Modal)`
  .ant-modal-footer {
    text-align: left;
    background: #f7f7f7;
    button {
      margin-right: 20px;
    }
  }
`

const Section = styled.div`
  span {
    font-weight: 600;
  }
  .input {
    margin: 5px 0 15px;
  }
  .inputNumber {
    margin-top: 10px;
    padding-left: 20px;
    span {
      margin-left: 5px;
    }
  }
`

const PaymentTerms = styled.div`
  .edit {
    display: none;
    cursor: pointer;
    margin-left: 20px;
  }
  .item {
    padding: 5px 5px;
    width: 300px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    :hover {
      background: #c4c4c44d;
      .edit {
        display: inline-block;
      }
    }
  }
`
