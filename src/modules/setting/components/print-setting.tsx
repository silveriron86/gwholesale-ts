import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Radio, Select, Icon, Collapse, Tooltip } from 'antd'
import Form, { FormComponentProps } from 'antd/lib/form'
import {
  ThemeButton,
  ThemeRadio,
  DetailsTitle,
  ThemeCheckbox,
  ThemeTextArea,
  ThemeInput,
  ThemeSelect,
  Flex,
} from '~/modules/customers/customers.style'
import { SettingModule, SettingDispatchProps, SettingStateProps } from './../setting.module'
import { GlobalState } from '~/store/reducer'
import { PrintSettingWrapper, CustomFormItem } from './../theme.style'
import _, { cloneDeep, cloneDeepWith } from 'lodash'

const { Panel } = Collapse;
const { Option } = Select

export type ThemeProps = SettingDispatchProps & SettingStateProps & FormComponentProps<PrintSettingFields>
interface PrintSettingFields {
  pick_enabled: boolean
  pick_alternateRow: number
  pick_quantity: number
  pick_UOM: string
  pick_variety: string
  pick_status: string
  pick_lotId: string
  pick_locationNames: string
  pick_weight: string,
  pick_volume: string,
  pick_note: string
  pick_title: string
  bill_enabled: boolean
  bill_barcode: number
  bill_catchWeightQty: number
  bill_variety: number
  bill_driver: number
  bill_alternateRow: number
  bill_index: number
  bill_logo: number
  bill_quantity: number
  bill_po: number
  bill_deliveryDate: number
  bill_fulfillmentLabel: string
  bill_price: number
  bill_pricingUOM: number
  bill_picked: number
  bill_SKU: number
  bill_so: number
  bill_total: number
  bill_terms: string
  bill_title: string
  bill_extraOrigin: number
  bill_packing: number
  bill_size: number
  bill_UOM: number
  bill_window: number
  bill_printTime: number
  invoice_enabled: boolean
  invoice_sales_confirmation_enabled: boolean
  invoice_barcode: number
  invoice_catchWeightQty: number
  invoice_variety: number
  invoice_driver: number
  invoice_alternateRow: number
  invoice_index: number
  invoice_logo: number
  invoice_quantity: number
  invoice_po: number
  invoice_price: number
  invoice_pricingUOM: number
  invoice_picked: number
  invoice_SKU: number
  invoice_so: number
  invoice_total: number
  invoice_terms: string
  invoice_title: string
  invoice_UOM: number
  invoice_window: number
  invoice_deliveryDate: number
  invoice_fulfillmentLabel: string
  invoice_extraOrigin: number
  invoice_packing: number
  invoice_size: number
  invoice_printTime: number
}

export class PrintSetting extends React.PureComponent<ThemeProps> {
  state = {
    options: null,
    lotType: true,
    pick_enabled: true,
    bill_enabled: true,
    invoice_enabled: true,
    invoice_sales_confirmation_enabled: false,
    bill_fulfillment_date_enabled: false,
    manifest_fulfillment_date_enabled: false,
    invoice_fulfillment_date_enabled: false,
    bill_carrierTerms: false,
    //purchase order
    purchase_enabled: true,
    manifest_enabled: true,
    creditmemo_enabled: true,
  }

  componentDidMount() {
    if (this.props.printSetting) {
      this.setState({ options: JSON.parse(this.props.printSetting) }, () => {
        this.setState({
          lotType: this.state.options.pick_lotId,
        })
        this._setEnabledOptions()
      })
    } else {
      this.props.getPrintSetting()
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.printSetting != nextProps.printSetting) {
      this.setState({ options: JSON.parse(nextProps.printSetting) }, () => {
        this.setState({
          lotType: this.state.options ? this.state.options.pick_lotId : true,
        })
        this._setEnabledOptions()
      })
    }
  }

  _setEnabledOptions = () => {
    const { options } = this.state
    this.setState({
      pick_enabled: !options || typeof options.pick_enabled === 'undefined' ? true : options.pick_enabled,
      bill_enabled: !options || typeof options.bill_enabled === 'undefined' ? true : options.bill_enabled,
      manifest_enabled: !options || typeof options.manifest_enabled === 'undefined' ? true : options.manifest_enabled,
      invoice_enabled: !options || typeof options.invoice_enabled === 'undefined' ? true : options.invoice_enabled,
      invoice_sales_confirmation_enabled: !options || typeof options.invoice_sales_confirmation_enabled === 'undefined' ? false : options.invoice_sales_confirmation_enabled,
      purchase_enabled: !options || typeof options.purchase_enabled === 'undefined' ? true : options.purchase_enabled,
      bill_fulfillment_date_enabled:
        !options || typeof options.bill_deliveryDate === 'undefined' ? true : options.bill_deliveryDate,
      manifest_fulfillment_date_enabled:
        !options || typeof options.manifest_deliveryDate === 'undefined' ? true : options.manifest_deliveryDate,
      invoice_fulfillment_date_enabled:
        !options || typeof options.invoice_deliveryDate === 'undefined' ? true : options.invoice_deliveryDate,
      bill_carrierTerms:
        !options || typeof options.bill_carrierTerms === 'undefined' ? true : options.bill_carrierTerms,
      purchase_enabled: !options || typeof options.purchase_enabled === 'undefined' ? true : options.purchase_enabled,
      creditmemo_enabled: !options || typeof options.creditmemo_enabled === 'undefined' ? true : options.creditmemo_enabled,
    })
  }

  handlePriceSettingChange = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      this.props.savePrintSetting({ values, showNotification: true })
    })
  }

  onChangeLot = (e: any) => {
    this.setState({
      lotType: e.target.checked,
    })
  }

  onValuesChange = (props: any, changedValues: any, allValues: any) => {
    console.log(changedValues)
  }

  onChangeEnabled = (field: string, e: any) => {
    let state = cloneDeep(this.state)
    state[field] = e.target.checked
    this.setState(state)
  }

  itemSelectChange = (e: string) => {
    let state = cloneDeep(this.state)
    this.state.options.item_sort = e
    this.setState(state)
  }

  render() {
    const { form, companyInfo } = this.props
    const { getFieldDecorator } = form
    const {
      options,
      lotType,
      pick_enabled,
      bill_enabled,
      invoice_enabled,
      invoice_sales_confirmation_enabled,
      manifest_enabled,
      bill_fulfillment_date_enabled,
      manifest_fulfillment_date_enabled,
      invoice_fulfillment_date_enabled,
      creditmemo_enabled,
      purchase_enabled,
    } = this.state

    const isFreshGreen =
      companyInfo && (companyInfo.companyName === 'Fresh Green' || companyInfo.companyName === 'Fresh Green Inc')

    return (
      <PrintSettingWrapper>
        <Form onSubmit={this.handlePriceSettingChange}>
          <DetailsTitle style={{ paddingTop: 24, marginBottom: 10 }}>Purchase order</DetailsTitle>
          <CustomFormItem label="Enable purchase order" colon={false} className="checkbox">
            {getFieldDecorator('purchase_enabled', {
              valuePropName: 'checked',
              initialValue: purchase_enabled,
            })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('purchase_enabled', e)} />)}
          </CustomFormItem>
          <Collapse
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 270 : 90} />}
            expandIconPosition="right"
            style={{ display: purchase_enabled ? 'block' : 'none' }}>
            <Panel key="1" header="Advanced settings">
              <div style={{ marginLeft: 23 }}>
                <div className="sub-title" style={{ marginTop: 0 }}>
                  Custom Document Title
                </div>
                <CustomFormItem>
                  {getFieldDecorator('purchase_title', {
                    initialValue: options ? options.purchase_title : 'Purchase Order',
                  })(<ThemeInput style={{ width: 250 }} maxLength={25} />)}
                </CustomFormItem>
                {/* <CustomFormItem label="Display Extended Cost" colon={false} className="checkbox">
                  {getFieldDecorator('extended_cost_enabled', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.extended_cost_enabled == 'undefined' ? false : options.extended_cost_enabled,
                  })(<ThemeCheckbox />)}
                </CustomFormItem> */}
                <CustomFormItem label="Display logo" colon={false} className="checkbox">
                  {getFieldDecorator('display_logo', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.display_logo == 'undefined' ? true : options.display_logo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display purchase order#" colon={false} className="checkbox">
                  {getFieldDecorator('display_purchase_order', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_purchase_order == 'undefined'
                        ? true
                        : options.display_purchase_order,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display barcode" colon={false} className="checkbox">
                  {getFieldDecorator('display_barcode', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_barcode == 'undefined' ? true : options.display_barcode,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display purchaser" colon={false} className="checkbox">
                  {getFieldDecorator('display_purchaser', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_purchaser == 'undefined' ? true : options.display_purchaser,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display order date" colon={false} className="checkbox">
                  {getFieldDecorator('display_order_date', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_order_date == 'undefined' ? true : options.display_order_date,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display delivery date" colon={false} className="checkbox">
                  {getFieldDecorator('delivery_date', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.delivery_date == 'undefined' ? true : options.delivery_date,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Vendor Ref. #" colon={false} className="checkbox">
                  {getFieldDecorator('display_reference', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_reference == 'undefined' ? true : options.display_reference,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Carrier Ref. #" colon={false} className="checkbox">
                  {getFieldDecorator('display_carrier_reference', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_carrier_reference == 'undefined' ? true : options.display_carrier_reference,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display payment terms" colon={false} className="checkbox">
                  {getFieldDecorator('display_payment_terms', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_payment_terms == 'undefined'
                        ? true
                        : options.display_payment_terms,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display carrier" colon={false} className="checkbox">
                  {getFieldDecorator('display_carrier', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_carrier == 'undefined'
                        ? true
                        : options.display_carrier,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display shipping terms" colon={false} className="checkbox">
                  {getFieldDecorator('display_shipping_terms', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_shipping_terms == 'undefined'
                        ? true
                        : options.display_shipping_terms,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display freight type" colon={false} className="checkbox">
                  {getFieldDecorator('display_freight_type', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_freight_type == 'undefined' ? true : options.display_freight_type,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display ship to" colon={false} className="checkbox">
                  {getFieldDecorator('display_ship_to', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_ship_to == 'undefined' ? true : options.display_ship_to,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display total cost" colon={false} className="checkbox">
                  {getFieldDecorator('display_totalAll_cost', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_totalAll_cost == 'undefined'
                        ? true
                        : options.display_totalAll_cost,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <div className="sub-title">Display columns</div>
                <CustomFormItem label="Display SKU" colon={false} className="checkbox">
                  {getFieldDecorator('display_sku', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.display_sku == 'undefined' ? true : options.display_sku,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display vendor item code" colon={false} className="checkbox">
                  {getFieldDecorator('display_vendor_item_code', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_vendor_item_code == 'undefined'
                        ? true
                        : options.display_vendor_item_code,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display product description" colon={false} className="checkbox">
                  {getFieldDecorator('display_product_description', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_product_description == 'undefined'
                        ? true
                        : options.display_product_description,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display order quantity" colon={false} className="checkbox">
                  {getFieldDecorator('display_order_quality', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_order_quality == 'undefined'
                        ? true
                        : options.display_order_quality,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display estimated billable quantity" colon={false} className="checkbox">
                  {getFieldDecorator('display_estimated_billable_quality', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_estimated_billable_quality == 'undefined'
                        ? true
                        : options.display_estimated_billable_quality,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display confirmed quantity" colon={false} className="checkbox">
                  {getFieldDecorator('display_confirmed_quality', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_confirmed_quality == 'undefined'
                        ? true
                        : options.display_confirmed_quality,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display quoted cost" colon={false} className="checkbox">
                  {getFieldDecorator('display_quoted_cost', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_quoted_cost == 'undefined' ? true : options.display_quoted_cost,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display extended cost" colon={false} className="checkbox">
                  {getFieldDecorator('display_total_cost', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.display_total_cost == 'undefined' ? true : options.display_total_cost,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <div className="sub-title">Terms and Conditions</div>
                <CustomFormItem>
                  {getFieldDecorator('po_terms', {
                    initialValue: options?.po_terms || ''
                  })(<ThemeTextArea maxLength={10000} rows={5} />)}
                </CustomFormItem>
              </div>
            </Panel>
          </Collapse>

          <DetailsTitle style={{ paddingTop: 24, marginBottom: 10 }}>Sales order</DetailsTitle>
          <CustomFormItem label="Enable pick sheet" colon={false} className="checkbox">
            {getFieldDecorator('pick_enabled', {
              valuePropName: 'checked',
              initialValue: pick_enabled,
            })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('pick_enabled', e)} />)}
          </CustomFormItem>
          <Collapse
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 270 : 90} />}
            expandIconPosition="right"
            style={{ display: pick_enabled ? 'block' : 'none' }}>
            <Panel key="1" header="Advanced settings">
              <div style={{ marginLeft: 23 }}>
                <div className="sub-title" style={{ marginTop: 0 }}>
                  Custom Document Title
                </div>
                <CustomFormItem>
                  {getFieldDecorator('pick_title', {
                    initialValue: options ? options.pick_title : '',
                  })(<ThemeInput style={{ width: 250 }} maxLength={25} />)}
                </CustomFormItem>
                <CustomFormItem label="Display logo" colon={false} className="checkbox">
                  {getFieldDecorator('pick_logo', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_logo == 'undefined' ? true : options.pick_logo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display barcode" colon={false} className="checkbox">
                  {getFieldDecorator('pick_barcode', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_barcode == 'undefined' ? true : options.pick_barcode,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Customer PO No." colon={false} className="checkbox">
                  {getFieldDecorator('pick_customerPONo', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_customerPONo == 'undefined' ? true : options.pick_customerPONo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver Name" colon={false} className="checkbox">
                  {getFieldDecorator('pick_driverName', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_driverName == 'undefined' ? true : options.pick_driverName,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Sales Rep" colon={false} className="checkbox">
                  {getFieldDecorator('pick_salesRep', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_salesRep == 'undefined' ? true : options.pick_salesRep,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <div className="sub-title">Sections</div>
                <CustomFormItem label="Catch Weight Values" colon={false} className="checkbox">
                  {getFieldDecorator('catchWeightValues', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.catchWeightValues == 'undefined' ? true : options.catchWeightValues,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem style={{height: 43}} label={(
                  <Flex>
                    Separate Loose Items Table
                    <Tooltip title='For the first order in the session, the "Default Target Fulfillment Date" setting will apply. Thereafter, the target fulfillment date will be set equal to the most recently created order by the user.'>
                      <Icon style={{ marginLeft: 10, marginTop: 10 }} type="info-circle" theme="filled" />
                    </Tooltip>
                  </Flex>)} colon={false} className="checkbox">
                  {getFieldDecorator('pick_separateLooseItemsTable', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_separateLooseItemsTable == 'undefined' ? false : options.pick_separateLooseItemsTable,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>


                <div className="sub-title">Display Columns</div>
                <CustomFormItem label="Display Alternate Row Shading" colon={false} className="checkbox">
                  {getFieldDecorator('pick_alternateRow', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_alternateRow == 'undefined' ? true : options.pick_alternateRow,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Ordered Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('pick_quantity', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_quantity == 'undefined' ? true : options.pick_quantity,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Unit Of Measure (UOM)" colon={false} className="checkbox">
                  {getFieldDecorator('pick_UOM', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_UOM == 'undefined' ? true : options.pick_UOM,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="SKU" colon={false} className="checkbox">
                  {getFieldDecorator('pick_SKU', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_SKU == 'undefined' ? true : options.pick_SKU,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Product Description" colon={false} className="checkbox">
                  {getFieldDecorator('pick_variety', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_variety == 'undefined' ? true : options.pick_variety,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Brand" colon={false} className="checkbox">
                  {getFieldDecorator('pick_modifiers', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_modifiers == 'undefined' ? true : options.pick_modifiers,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Origin" colon={false} className="checkbox">
                  {getFieldDecorator('pick_extraOrigin', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_extraOrigin == 'undefined' ? true : options.pick_extraOrigin,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem
                  colon={false}
                  className="checkbox"
                  style={{ position: 'absolute', marginLeft: 80, zIndex: 2 }}
                >
                  {getFieldDecorator('pick_lotType', {
                    // valuePropName: 'checked',
                    initialValue: options && typeof options.pick_lotType !== 'undefined' ? options.pick_lotType : 'number',
                  })(
                    <Radio.Group disabled={!lotType}>
                      <ThemeRadio value={'number'}>Number</ThemeRadio>
                      <ThemeRadio value={'barcode'}>Barcode</ThemeRadio>
                    </Radio.Group>,
                  )}
                </CustomFormItem>
                <CustomFormItem label="Lot" colon={false} className="checkbox">
                  {getFieldDecorator('pick_lotId', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_lotId == 'undefined' ? true : options.pick_lotId,
                  })(<ThemeCheckbox onChange={this.onChangeLot} />)}
                </CustomFormItem>
                <CustomFormItem label="Location" colon={false} className="checkbox">
                  {getFieldDecorator('pick_locationNames', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_locationNames == 'undefined' ? true : options.pick_locationNames,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Weight" colon={false} className="checkbox">
                  {getFieldDecorator('pick_weight', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_weight == 'undefined' ? true : options.pick_weight,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Volume" colon={false} className="checkbox">
                  {getFieldDecorator('pick_volume', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.pick_volume == 'undefined' ? true : options.pick_volume,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Units Picked" colon={false} className="checkbox">
                  {getFieldDecorator('pick_status', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_status == 'undefined' ? true : options.pick_status,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Note" colon={false} className="checkbox">
                  {getFieldDecorator('pick_note', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.pick_note == 'undefined' ? true : options.pick_note,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <div className="sub-title" style={{ marginTop: 10 }}>
                  Layout
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('pick_layout', {
                    // valuePropName: 'checked',
                    initialValue: options && typeof options.pick_layout !== 'undefined' ? options.pick_layout : 'portrait',
                  })(
                    <Radio.Group>
                      <ThemeRadio value={'portrait'}>Portrait</ThemeRadio>
                      <ThemeRadio value={'landscape'}>Landscape</ThemeRadio>
                    </Radio.Group>,
                  )}
                </CustomFormItem>
                <div className="sub-title" style={{ marginTop: 10 }}>
                  Item Sort
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('pick_item_sort', {
                    // valuePropName: 'checked',
                    initialValue:
                      options && typeof options.pick_item_sort !== 'undefined'
                        ? options.pick_item_sort
                        : 'follow display order in sales order',
                  })(
                    <Select
                      defaultValue={
                        options && typeof options.pick_item_sort !== 'undefined'
                          ? options.pick_item_sort
                          : 'follow display order in sales order'
                      }
                      style={{ width: 300 }}
                      onChange={(e: any) => this.itemSelectChange(e)}
                    >
                      <Option value="follow display order in sales order">Follow display order in sales order</Option>
                      <Option value="location">Sort by location</Option>
                      <Option value="sku">
                        Sort by SKU
                      </Option>
                    </Select>,
                  )}
                </CustomFormItem>
              </div>
            </Panel>
          </Collapse>

          <CustomFormItem label="Enable invoice" colon={false} className="checkbox">
            {getFieldDecorator('invoice_enabled', {
              valuePropName: 'checked',
              initialValue: invoice_enabled,
            })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('invoice_enabled', e)} />)}
          </CustomFormItem>
          <Collapse
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 270 : 90} />}
            expandIconPosition="right"
            style={{ display: invoice_enabled ? 'block' : 'none' }}>
            <Panel key="1" header="Advanced settings">
              <div style={{ marginLeft: 23 }}>
                <div className="sub-title" style={{ marginTop: 0 }}>
                  Custom Document Title
                </div>
                <CustomFormItem>
                  {getFieldDecorator('invoice_title', {
                    initialValue: options ? options.invoice_title : '',
                  })(<ThemeInput style={{ width: 250 }} maxLength={25} />)}
                </CustomFormItem>
                <div className="sub-title" style={{ marginTop: 10 }}>
                  Primary Number
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('invoice_primary_number', {
                    // valuePropName: 'checked',
                    initialValue:
                      options && typeof options.invoice_primary_number !== 'undefined'
                        ? options.invoice_primary_number
                        : 'salesOrder#',
                  })(
                    <Select
                      defaultValue={
                        options && typeof options.invoice_primary_number !== 'undefined'
                          ? options.invoice_primary_number
                          : 'salesOrder#'
                      }
                      style={{ width: 300 }}
                      onChange={(e: any) => this.itemSelectChange(e)}
                    >
                      <Option value="salesOrder#">Sales Order #</Option>
                      <Option value="customerPO#">Customer PO #</Option>
                    </Select>,
                  )}
                </CustomFormItem>
                <CustomFormItem label="Display logo" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_logo', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_logo == 'undefined' ? true : options.invoice_logo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display "Please submit payment to"' colon={false} className="checkbox">
                  {getFieldDecorator('invoice_submitPaymentWord', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_submitPaymentWord == 'undefined'
                        ? true
                        : options.invoice_submitPaymentWord,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Sales Order #" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_so', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_so == 'undefined' ? true : options.invoice_so,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Invoice #" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_num', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_num == 'undefined' ? true : options.invoice_num,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem
                  label={isFreshGreen ? 'Display Customer PO. (Receipt No. for Cash Sale orders)' : 'Display Customer PO #'}
                  colon={false}
                  className="checkbox"
                >
                  {getFieldDecorator('invoice_po', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_po == 'undefined' ? true : options.invoice_po,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Payment Term" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_paymentTerm', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_paymentTerm == 'undefined' ? true : options.invoice_paymentTerm,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Payment due date" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_paymentTermDueDate', {
                    valuePropName: 'checked',
                    initialValue: _.get(options, 'invoice_paymentTermDueDate', false),
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Order Date" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_orderDate', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_orderDate == 'undefined' ? true : options.invoice_orderDate,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Window" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_window', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_window == 'undefined' ? true : options.invoice_window,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Target Fulfillment Date" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_deliveryDate', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_deliveryDate == 'undefined' ? true : options.invoice_deliveryDate,
                  })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('invoice_fulfillment_date_enabled', e)} />)}
                </CustomFormItem>
                <div style={{ marginLeft: 24, display: invoice_fulfillment_date_enabled ? 'block' : 'none' }}>
                  <div className="sub-title" style={{ marginTop: 0 }}>
                    &quot;Target Fulfillment Date&quot; Label
                  </div>
                  <CustomFormItem>
                    {getFieldDecorator('invoice_fulfillmentLabel', {
                      initialValue:
                        options && options.invoice_fulfillmentLabel
                          ? options.invoice_fulfillmentLabel
                          : 'Target Fulfillment Date',
                    })(<ThemeInput style={{ width: 250 }} />)}
                  </CustomFormItem>
                </div>
                <CustomFormItem label="Display Sales Representative" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_salesRepresentative', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_salesRepresentative == 'undefined'
                        ? true
                        : options.invoice_salesRepresentative,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Delivery Driver" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_driver', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_driver == 'undefined' ? true : options.invoice_driver,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Pickup Reference No" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_pickupReferenceNo', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_pickupReferenceNo == 'undefined'
                        ? true
                        : options.invoice_pickupReferenceNo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display number of pallets" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_numberOfPallets', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_numberOfPallets == 'undefined'
                        ? true
                        : options.invoice_numberOfPallets,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display barcode" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_barcode', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_barcode == 'undefined' ? true : options.invoice_barcode,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Ship To" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_shipto', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_shipto == 'undefined' ? true : options.invoice_shipto,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Sold To" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_soldto', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_soldto == 'undefined' ? true : options.invoice_soldto,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Carrier and Reference No" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_carrier_reference', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_carrier_reference == 'undefined'
                        ? true
                        : options.invoice_carrier_reference,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Origin" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_origin', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_origin == 'undefined' ? true : options.invoice_origin,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Delivery Contact" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_deliveryContact', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_deliveryContact == 'undefined'
                        ? true
                        : options.invoice_deliveryContact,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Total Units" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_totalUnits', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_totalUnits == 'undefined' ? true : options.invoice_totalUnits,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Print Date/Time" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_printTime', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_printTime == 'undefined' ? true : options.invoice_printTime,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <div className="sub-title">Sections</div>
                <CustomFormItem label="Driver Payment Collection" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_driver_payment_collection', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_driver_payment_collection == 'undefined'
                        ? true
                        : options.invoice_driver_payment_collection,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Account Balance Forward" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_account_balance_forward', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_account_balance_forward == 'undefined'
                        ? true
                        : options.invoice_account_balance_forward,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display credit memo section" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_credit_memo', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_credit_memo == 'undefined' ? true : options.invoice_credit_memo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <CustomFormItem style={{height: 43}} label={(
                  <Flex>
                    Separate Loose Items Table
                    <Tooltip title='Useful to identify items that require special care to pick the correct UOM. When enabled, items sold with non-default selling UOM will be displayed in a separate list in the Invoice.'>
                      <Icon style={{ marginLeft: 10, marginTop: 10 }} type="info-circle" theme="filled" />
                    </Tooltip>
                  </Flex>)} colon={false} className="checkbox">
                  {getFieldDecorator('invoice_separateLooseItemsTable', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_separateLooseItemsTable == 'undefined' ? false : options.invoice_separateLooseItemsTable,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <CustomFormItem label="Display Warehouse Verification Signature" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_verificationSignature', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_verificationSignature == 'undefined'
                        ? true
                        : options.invoice_verificationSignature,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <div className="sub-title">Display Columns</div>
                <CustomFormItem label="Display Alternate Row Shading" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_alternateRow', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_alternateRow == 'undefined' ? true : options.invoice_alternateRow,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Item Numbering" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_index', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_index == 'undefined' ? true : options.invoice_index,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Ordered Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_quantity', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_quantity == 'undefined' ? true : options.invoice_quantity,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Shipped Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_picked', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_picked == 'undefined' ? true : options.invoice_picked,
                  })(<ThemeCheckbox disabled={_.get(companyInfo, 'isDisablePickingStep', false)} />)}
                </CustomFormItem>
                <CustomFormItem label="Unit of Measure for Ordered/Shipped Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_UOM', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_UOM == 'undefined' ? true : options.invoice_UOM,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Product Description" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_variety', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_variety == 'undefined' ? true : options.invoice_variety,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="SKU" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_SKU', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_SKU == 'undefined' ? true : options.invoice_SKU,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="UPC" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_UPC', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_UPC == 'undefined' ? false : options.invoice_UPC,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="PLU" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_PLU', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_PLU == 'undefined' ? true : options.invoice_PLU,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Billable Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_catchWeightQty', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_catchWeightQty == 'undefined'
                        ? true
                        : options.invoice_catchWeightQty,
                  })(<ThemeCheckbox disabled={_.get(companyInfo, 'isDisablePickingStep', false)} />)}
                </CustomFormItem>
                <CustomFormItem label="Unit of Measure for Pricing" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_pricingUOM', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_pricingUOM == 'undefined' ? true : options.invoice_pricingUOM,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Price/Unit" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_price', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_price == 'undefined' ? true : options.invoice_price,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Catch Weight Values" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_catchWeightValues', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_catchWeightValues == 'undefined'
                        ? true
                        : options.invoice_catchWeightValues,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Brand" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_modifiers', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_modifiers == 'undefined' ? true : options.invoice_modifiers,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Origin" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_extraOrigin', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_extraOrigin == 'undefined' ? true : options.invoice_extraOrigin,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="LOT" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_lot', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_lot == 'undefined'
                        ? true
                        : options.invoice_lot,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Packing" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_packing', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.invoice_packing == 'undefined' ? true : options.invoice_packing,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Size" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_size', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_size == 'undefined' ? true : options.invoice_size,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Subtotal" colon={false} className="checkbox">
                  {getFieldDecorator('invoice_total', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.invoice_total == 'undefined' ? true : options.invoice_total,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <div className="sub-title">Custom Message</div>
                <CustomFormItem>
                  {getFieldDecorator('invoice_customMsg', {
                    initialValue: !options ? '' : options.invoice_customMsg,
                  })(<ThemeTextArea rows={5} />)}
                </CustomFormItem>
                <div className="sub-title">Terms and Conditions</div>
                <CustomFormItem>
                  {getFieldDecorator('invoice_terms', {
                    initialValue: !options ? '' : options.invoice_terms,
                  })(<ThemeTextArea rows={5} />)}
                </CustomFormItem>
                <div className="sub-title" style={{ marginTop: 10 }}>
                  Layout
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('invoice_layout', {
                    // valuePropName: 'checked',
                    initialValue:
                      options && typeof options.invoice_layout !== 'undefined' ? options.invoice_layout : 'portrait',
                  })(
                    <Radio.Group>
                      <ThemeRadio value={'portrait'}>Portrait</ThemeRadio>
                      <ThemeRadio value={'landscape'}>Landscape</ThemeRadio>
                    </Radio.Group>,
                  )}
                </CustomFormItem>
                <div className="sub-title" style={{ marginTop: 10 }}>
                  Item Sort
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('invoice_item_sort', {
                    // valuePropName: 'checked',
                    initialValue:
                      options && typeof options.invoice_item_sort !== 'undefined'
                        ? options.invoice_item_sort
                        : 'follow display order in sales order',
                  })(
                    <Select
                      defaultValue={
                        options && typeof options.invoice_item_sort !== 'undefined'
                          ? options.invoice_item_sort
                          : 'follow display order in sales order'
                      }
                      style={{ width: 300 }}
                      onChange={(e: any) => this.itemSelectChange(e)}
                    >
                      <Option value="follow display order in sales order">Follow display order in sales order</Option>
                      <Option value="sku">
                        Sort by SKU
                      </Option>
                    </Select>,
                  )}
                </CustomFormItem>
              </div>
            </Panel>
          </Collapse>
          <div style={{marginLeft: 23}}>
            <CustomFormItem label="Enable sales confirmation" colon={false} className="checkbox">
              {getFieldDecorator('invoice_sales_confirmation_enabled', {
                valuePropName: 'checked',
                initialValue: invoice_sales_confirmation_enabled,
              })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('invoice_sales_confirmation_enabled', e)} />)}
            </CustomFormItem>
          </div>

          <CustomFormItem label="Enable bill of lading" colon={false} className="checkbox">
            {getFieldDecorator('bill_enabled', {
              valuePropName: 'checked',
              initialValue: bill_enabled,
            })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('bill_enabled', e)} />)}
          </CustomFormItem>
          <Collapse
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 270 : 90} />}
            expandIconPosition="right"
            style={{ display: bill_enabled ? 'block' : 'none' }}>
            <Panel key="1" header="Advanced settings">
              <div style={{ marginLeft: 23 }}>
                <div className="sub-title" style={{ marginTop: 0 }}>
                  Custom Document Title
                </div>
                <CustomFormItem>
                  {getFieldDecorator('bill_title', {
                    initialValue: options ? options.bill_title : '',
                  })(<ThemeInput style={{ width: 250 }} maxLength={25} />)}
                </CustomFormItem>
                <div className="sub-title" style={{ marginTop: 10 }}>
                  Primary Number
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('bill_primary_number', {
                    // valuePropName: 'checked',
                    initialValue:
                      options && typeof options.bill_primary_number !== 'undefined'
                        ? options.bill_primary_number
                        : 'salesOrder#',
                  })(
                    <Select
                      defaultValue={
                        options && typeof options.bill_primary_number !== 'undefined'
                          ? options.bill_primary_number
                          : 'salesOrder#'
                      }
                      style={{ width: 300 }}
                      onChange={(e: any) => this.itemSelectChange(e)}
                    >
                      <Option value="salesOrder#">Sales Order #</Option>
                      <Option value="customerPO#">Customer PO #</Option>
                    </Select>,
                  )}
                </CustomFormItem>
                <CustomFormItem label="Display logo" colon={false} className="checkbox">
                  {getFieldDecorator('bill_logo', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_logo == 'undefined' ? true : options.bill_logo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Sales Order #" colon={false} className="checkbox">
                  {getFieldDecorator('bill_so', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_so == 'undefined' ? true : options.bill_so,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Order Date" colon={false} className="checkbox">
                  {getFieldDecorator('bill_orderDate', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_orderDate == 'undefined' ? true : options.bill_orderDate,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Delivery Contact" colon={false} className="checkbox">
                  {getFieldDecorator('bill_deliveryContact', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_deliveryContact == 'undefined' ? true : options.bill_deliveryContact,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Total Units" colon={false} className="checkbox">
                  {getFieldDecorator('bill_totalUnits', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_totalUnits == 'undefined' ? true : options.bill_totalUnits,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem
                  label={isFreshGreen ? 'Display Customer PO. (Receipt No. for Cash Sale orders)' : 'Display Customer PO #'}
                  colon={false}
                  className="checkbox"
                >
                  {getFieldDecorator('bill_po', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_po == 'undefined' ? true : options.bill_po,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Target Fulfillment Date" colon={false} className="checkbox">
                  {getFieldDecorator('bill_deliveryDate', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_deliveryDate == 'undefined' ? true : options.bill_deliveryDate,
                  })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('bill_fulfillment_date_enabled', e)} />)}
                </CustomFormItem>
                <div style={{ marginLeft: 24, display: bill_fulfillment_date_enabled ? 'block' : 'none' }}>
                  <div className="sub-title" style={{ marginTop: 0 }}>
                    &quot;Target Fulfillment Date&quot; Label
                  </div>
                  <CustomFormItem>
                    {getFieldDecorator('bill_fulfillmentLabel', {
                      initialValue:
                        options && options.bill_fulfillmentLabel
                          ? options.bill_fulfillmentLabel
                          : 'Target Fulfillment Date',
                    })(<ThemeInput style={{ width: 250 }} />)}
                  </CustomFormItem>
                </div>
                <CustomFormItem label="Display Temperature" colon={false} className="checkbox">
                  {getFieldDecorator('bill_temperature', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_temperature == 'undefined' ? false : options.bill_temperature,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Pickup Reference No" colon={false} className="checkbox">
                  {getFieldDecorator('bill_pickupReferenceNo', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_pickupReferenceNo == 'undefined'
                        ? true
                        : options.bill_pickupReferenceNo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Window" colon={false} className="checkbox">
                  {getFieldDecorator('bill_window', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_window == 'undefined' ? true : options.bill_window,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Delivery Driver" colon={false} className="checkbox">
                  {getFieldDecorator('bill_driver', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_driver == 'undefined' ? true : options.bill_driver,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display number of pallets" colon={false} className="checkbox">
                  {getFieldDecorator('bill_numberOfPallets', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_numberOfPallets == 'undefined' ? true : options.bill_numberOfPallets,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Ship To" colon={false} className="checkbox">
                  {getFieldDecorator('bill_shipto', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_shipto == 'undefined' ? true : options.bill_shipto,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Sold To" colon={false} className="checkbox">
                  {getFieldDecorator('bill_soldto', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_soldto == 'undefined' ? true : options.bill_soldto,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Carrier and Reference No" colon={false} className="checkbox">
                  {getFieldDecorator('bill_carrier_reference', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_carrier_reference == 'undefined'
                        ? true
                        : options.bill_carrier_reference,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Origin" colon={false} className="checkbox">
                  {getFieldDecorator('bill_origin', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_origin == 'undefined' ? true : options.bill_origin,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Print Date/Time" colon={false} className="checkbox">
                  {getFieldDecorator('bill_printTime', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_printTime == 'undefined' ? true : options.bill_printTime,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Warehouse Verification Signature" colon={false} className="checkbox">
                  {getFieldDecorator('bill_verificationSignature', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_verificationSignature == 'undefined'
                        ? true
                        : options.bill_verificationSignature,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem
                  label="Display Customer Print Name and Signature Section"
                  colon={false}
                  className="checkbox"
                >
                  {getFieldDecorator('bill_nameAndSignature', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_nameAndSignature == 'undefined'
                        ? false
                        : options.bill_nameAndSignature,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <div className="sub-title">Freight Carrier Field Section</div>
                <CustomFormItem label="Display Carrier" colon={false} className="checkbox">
                  {getFieldDecorator('bill_carrier', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_carrier == 'undefined' ? true : options.bill_carrier,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Carrier Phone No." colon={false} className="checkbox">
                  {getFieldDecorator('bill_carrierPhoneNo', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_carrierPhoneNo == 'undefined' ? true : options.bill_carrierPhoneNo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver Name" colon={false} className="checkbox">
                  {getFieldDecorator('bill_driverName', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_driverName == 'undefined' ? true : options.bill_driverName,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver's License No." colon={false} className="checkbox">
                  {getFieldDecorator('bill_driverLicense', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_driverLicense == 'undefined' ? true : options.bill_driverLicense,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver's License Image Field" colon={false} className="checkbox">
                  {getFieldDecorator('bill_driverLicenseImageField', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_driverLicenseImageField == 'undefined'
                        ? true
                        : options.bill_driverLicenseImageField,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver Signature Field" colon={false} className="checkbox">
                  {getFieldDecorator('bill_driverSignatureField', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_driverSignatureField == 'undefined'
                        ? true
                        : options.bill_driverSignatureField,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Freight Carrier Terms & Conditions" colon={false} className="checkbox">
                  {getFieldDecorator('bill_carrierTerms', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_carrierTerms == 'undefined' ? true : options.bill_carrierTerms,
                  })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('bill_carrierTerms', e)} />)}
                </CustomFormItem>
                <CustomFormItem style={{ display: this.state.bill_carrierTerms ? 'block' : 'none' }}>
                  {getFieldDecorator('bill_carrierTermsText', {
                    initialValue: options && options.bill_carrierTermsText ? options.bill_carrierTermsText : '',
                  })(<ThemeTextArea rows={5} />)}
                </CustomFormItem>

                <div className="sub-title">Display Columns</div>
                <CustomFormItem label="Display Alternate Row Shading" colon={false} className="checkbox">
                  {getFieldDecorator('bill_alternateRow', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_alternateRow == 'undefined' ? true : options.bill_alternateRow,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Item Numbering" colon={false} className="checkbox">
                  {getFieldDecorator('bill_index', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_index == 'undefined' ? true : options.bill_index,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Ordered Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('bill_quantity', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_quantity == 'undefined' ? true : options.bill_quantity,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Shipped Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('bill_picked', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_picked == 'undefined' ? true : options.bill_picked,
                  })(<ThemeCheckbox disabled={_.get(companyInfo, 'isDisablePickingStep', false)} />)}
                </CustomFormItem>
                <CustomFormItem label="Unit of Measure for Ordered/Shipped Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('bill_UOM', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_UOM == 'undefined' ? true : options.bill_UOM,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Product Description" colon={false} className="checkbox">
                  {getFieldDecorator('bill_variety', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_variety == 'undefined' ? true : options.bill_variety,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="SKU" colon={false} className="checkbox">
                  {getFieldDecorator('bill_SKU', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_SKU == 'undefined' ? true : options.bill_SKU,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Billable Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('bill_catchWeightQty', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_catchWeightQty == 'undefined' ? true : options.bill_catchWeightQty,
                  })(<ThemeCheckbox disabled={_.get(companyInfo, 'isDisablePickingStep', false)} />)}
                </CustomFormItem>
                <CustomFormItem label="Unit of Measure for Pricing" colon={false} className="checkbox">
                  {getFieldDecorator('bill_pricingUOM', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_pricingUOM == 'undefined' ? true : options.bill_pricingUOM,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Price/Unit" colon={false} className="checkbox">
                  {getFieldDecorator('bill_price', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_price == 'undefined' ? true : options.bill_price,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Brand" colon={false} className="checkbox">
                  {getFieldDecorator('bill_modifiers', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_modifiers == 'undefined' ? true : options.bill_modifiers,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Origin" colon={false} className="checkbox">
                  {getFieldDecorator('bill_extraOrigin', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_extraOrigin == 'undefined' ? true : options.bill_extraOrigin,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="LOT" colon={false} className="checkbox">
                  {getFieldDecorator('bill_lot', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_lot == 'undefined'
                        ? true
                        : options.bill_lot,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Packing" colon={false} className="checkbox">
                  {getFieldDecorator('bill_packing', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_packing == 'undefined' ? true : options.bill_packing,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Size" colon={false} className="checkbox">
                  {getFieldDecorator('bill_size', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_size == 'undefined' ? true : options.bill_size,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Gross Weight" colon={false} className="checkbox">
                  {getFieldDecorator('bill_grossWeight', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_grossWeight == 'undefined' ? true : options.bill_grossWeight,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Gross Volume" colon={false} className="checkbox">
                  {getFieldDecorator('bill_grossVolume', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.bill_grossVolume == 'undefined' ? true : options.bill_grossVolume,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Subtotal" colon={false} className="checkbox">
                  {getFieldDecorator('bill_total', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.bill_total == 'undefined' ? true : options.bill_total,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <div className="sub-title">Terms and Conditions</div>
                <CustomFormItem>
                  {getFieldDecorator('bill_terms', {
                    initialValue: !options ? '' : options.bill_terms,
                  })(<ThemeTextArea rows={5} />)}
                </CustomFormItem>

                <div className="sub-title" style={{ marginTop: 10 }}>
                  Layout
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('bill_layout', {
                    // valuePropName: 'checked',
                    initialValue: options && typeof options.bill_layout !== 'undefined' ? options.bill_layout : 'portrait',
                  })(
                    <Radio.Group>
                      <ThemeRadio value={'portrait'}>Portrait</ThemeRadio>
                      <ThemeRadio value={'landscape'}>Landscape</ThemeRadio>
                    </Radio.Group>,
                  )}
                </CustomFormItem>
              </div>
            </Panel>
          </Collapse>

          <CustomFormItem label="Enable shipping manifest" colon={false} className="checkbox">
            {getFieldDecorator('manifest_enabled', {
              valuePropName: 'checked',
              initialValue: manifest_enabled,
            })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('manifest_enabled', e)} />)}
          </CustomFormItem>
          <Collapse
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 270 : 90} />}
            expandIconPosition="right"
            style={{ display: manifest_enabled ? 'block' : 'none' }}>
            <Panel key="1" header="Advanced settings">
              <div style={{ marginLeft: 23 }}>
                <div className="sub-title" style={{ marginTop: 0 }}>
                  Custom Document Title
                </div>
                <CustomFormItem>
                  {getFieldDecorator('manifest_title', {
                    initialValue: options ? options.manifest_title : '',
                  })(<ThemeInput style={{ width: 250 }} maxLength={25} />)}
                </CustomFormItem>
                <CustomFormItem label="Display logo" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_logo', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_logo == 'undefined' ? true : options.manifest_logo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Sales Order #" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_so', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_so == 'undefined' ? true : options.manifest_so,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Order Date" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_orderDate', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_orderDate == 'undefined' ? true : options.manifest_orderDate,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Delivery Contact" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_deliveryContact', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_deliveryContact == 'undefined' ? true : options.manifest_deliveryContact,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Total Units" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_totalUnits', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_totalUnits == 'undefined' ? true : options.manifest_totalUnits,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem
                  label={isFreshGreen ? 'Display Customer PO. (Receipt No. for Cash Sale orders)' : 'Display Customer PO #'}
                  colon={false}
                  className="checkbox"
                >
                  {getFieldDecorator('manifest_po', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_po == 'undefined' ? true : options.manifest_po,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Target Fulfillment Date" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_deliveryDate', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_deliveryDate == 'undefined' ? true : options.manifest_deliveryDate,
                  })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('manifest_fulfillment_date_enabled', e)} />)}
                </CustomFormItem>
                <div style={{ marginLeft: 24, display: manifest_fulfillment_date_enabled ? 'block' : 'none' }}>
                  <div className="sub-title" style={{ marginTop: 0 }}>
                    &quot;Target Fulfillment Date&quot; Label
                  </div>
                  <CustomFormItem>
                    {getFieldDecorator('manifest_fulfillmentLabel', {
                      initialValue:
                        options && options.manifest_fulfillmentLabel
                          ? options.manifest_fulfillmentLabel
                          : 'Target Fulfillment Date',
                    })(<ThemeInput style={{ width: 250 }} />)}
                  </CustomFormItem>
                </div>
                <CustomFormItem label="Display Temperature" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_temperature', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_temperature == 'undefined' ? false : options.manifest_temperature,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Pickup Reference No" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_pickupReferenceNo', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_pickupReferenceNo == 'undefined'
                        ? true
                        : options.manifest_pickupReferenceNo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Window" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_window', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_window == 'undefined' ? true : options.manifest_window,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Delivery Driver" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_driver', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_driver == 'undefined' ? true : options.manifest_driver,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display number of pallets" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_numberOfPallets', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_numberOfPallets == 'undefined' ? true : options.manifest_numberOfPallets,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Ship To" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_shipto', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_shipto == 'undefined' ? true : options.manifest_shipto,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Sold To" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_soldto', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_soldto == 'undefined' ? true : options.manifest_soldto,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Carrier and Reference No" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_carrier_reference', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_carrier_reference == 'undefined'
                        ? true
                        : options.manifest_carrier_reference,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Origin" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_origin', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_origin == 'undefined' ? true : options.manifest_origin,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Print Date/Time" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_printTime', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_printTime == 'undefined' ? true : options.manifest_printTime,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Warehouse Verification Signature" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_verificationSignature', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_verificationSignature == 'undefined'
                        ? true
                        : options.manifest_verificationSignature,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem
                  label="Display Customer Print Name and Signature Section"
                  colon={false}
                  className="checkbox"
                >
                  {getFieldDecorator('manifest_nameAndSignature', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_nameAndSignature == 'undefined'
                        ? false
                        : options.manifest_nameAndSignature,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <div className="sub-title">Freight Carrier Field Section</div>
                <CustomFormItem label="Display Carrier" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_carrier', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_carrier == 'undefined' ? true : options.manifest_carrier,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Carrier Phone No." colon={false} className="checkbox">
                  {getFieldDecorator('manifest_carrierPhoneNo', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_carrierPhoneNo == 'undefined' ? true : options.manifest_carrierPhoneNo,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver Name" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_driverName', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_driverName == 'undefined' ? true : options.manifest_driverName,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver's License No." colon={false} className="checkbox">
                  {getFieldDecorator('manifest_driverLicense', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_driverLicense == 'undefined' ? true : options.manifest_driverLicense,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver's License Image Field" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_driverLicenseImageField', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_driverLicenseImageField == 'undefined'
                        ? true
                        : options.manifest_driverLicenseImageField,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Driver Signature Field" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_driverSignatureField', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_driverSignatureField == 'undefined'
                        ? true
                        : options.manifest_driverSignatureField,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Display Freight Carrier Terms & Conditions" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_carrierTerms', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_carrierTerms == 'undefined' ? true : options.manifest_carrierTerms,
                  })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('manifest_carrierTerms', e)} />)}
                </CustomFormItem>
                <CustomFormItem style={{ display: this.state.manifest_carrierTerms ? 'block' : 'none' }}>
                  {getFieldDecorator('manifest_carrierTermsText', {
                    initialValue: options && options.manifest_carrierTermsText ? options.manifest_carrierTermsText : '',
                  })(<ThemeTextArea rows={5} />)}
                </CustomFormItem>

                <div className="sub-title">Display Columns</div>
                <CustomFormItem label="Display Alternate Row Shading" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_alternateRow', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_alternateRow == 'undefined' ? true : options.manifest_alternateRow,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Item Numbering" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_index', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_index == 'undefined' ? true : options.manifest_index,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Ordered Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_quantity', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_quantity == 'undefined' ? true : options.manifest_quantity,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Shipped Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_picked', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_picked == 'undefined' ? true : options.manifest_picked,
                  })(<ThemeCheckbox disabled={_.get(companyInfo, 'isDisablePickingStep', false)} />)}
                </CustomFormItem>
                <CustomFormItem label="Unit of Measure for Ordered/Shipped Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_UOM', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_UOM == 'undefined' ? true : options.manifest_UOM,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Product Description" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_variety', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_variety == 'undefined' ? true : options.manifest_variety,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="SKU" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_SKU', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_SKU == 'undefined' ? true : options.manifest_SKU,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Billable Quantity" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_catchWeightQty', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_catchWeightQty == 'undefined' ? true : options.manifest_catchWeightQty,
                  })(<ThemeCheckbox disabled={_.get(companyInfo, 'isDisablePickingStep', false)} />)}
                </CustomFormItem>
                <CustomFormItem label="Unit of Measure for Pricing" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_pricingUOM', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_pricingUOM == 'undefined' ? true : options.manifest_pricingUOM,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Gross Weight" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_grossWeight', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_grossWeight == 'undefined' ? true : options.manifest_grossWeight,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Price/Unit" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_price', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_price == 'undefined' ? true : options.manifest_price,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Brand" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_modifiers', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_modifiers == 'undefined' ? true : options.manifest_modifiers,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Origin" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_extraOrigin', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_extraOrigin == 'undefined' ? true : options.manifest_extraOrigin,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Packing" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_packing', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_packing == 'undefined' ? true : options.manifest_packing,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Size" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_size', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_size == 'undefined' ? true : options.manifest_size,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Gross Volume" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_grossVolume', {
                    valuePropName: 'checked',
                    initialValue:
                      !options || typeof options.manifest_grossVolume == 'undefined' ? true : options.manifest_grossVolume,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label="Subtotal" colon={false} className="checkbox">
                  {getFieldDecorator('manifest_total', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.manifest_total == 'undefined' ? true : options.manifest_total,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <div className="sub-title">Terms and Conditions</div>
                <CustomFormItem>
                  {getFieldDecorator('manifest_terms', {
                    initialValue: !options ? '' : options.manifest_terms,
                  })(<ThemeTextArea rows={5} />)}
                </CustomFormItem>

                <div className="sub-title" style={{ marginTop: 10 }}>
                  Layout
                </div>
                <CustomFormItem colon={false} className="checkbox">
                  {getFieldDecorator('manifest_layout', {
                    // valuePropName: 'checked',
                    initialValue: options && typeof options.manifest_layout !== 'undefined' ? options.manifest_layout : 'portrait',
                  })(
                    <Radio.Group>
                      <ThemeRadio value={'portrait'}>Portrait</ThemeRadio>
                      <ThemeRadio value={'landscape'}>Landscape</ThemeRadio>
                    </Radio.Group>,
                  )}
                </CustomFormItem>
              </div>
            </Panel>
          </Collapse>

          <CustomFormItem label="Enable credit memo" colon={false} className="checkbox">
            {getFieldDecorator('creditmemo_enabled', {
              valuePropName: 'checked',
              initialValue: creditmemo_enabled,
            })(<ThemeCheckbox onChange={(e: any) => this.onChangeEnabled('creditmemo_enabled', e)} />)}
          </CustomFormItem>
          <Collapse
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 270 : 90} />}
            expandIconPosition="right"
            style={{ display: creditmemo_enabled ? 'block' : 'none' }}>
            <Panel key="1" header="Advanced settings">
              <div style={{ marginLeft: 23 }}>
                <div className="sub-title" style={{ marginTop: 13, marginBottom: 8 }}>
                  Display sections
                </div>
                <CustomFormItem label='"Received by" signature' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_receivedby', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_receivedby == 'undefined' ? true : options.creditmemo_receivedby,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>

                <div className="sub-title" style={{ marginTop: 13, marginBottom: 8 }}>
                  Display columns
                </div>
                <CustomFormItem label='Display credit units' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_credited', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_credited == 'undefined' ? true : options.creditmemo_credited,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display order number' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_order', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_order == 'undefined' ? true : options.creditmemo_order,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display return/no return' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_return', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_return == 'undefined' ? true : options.creditmemo_return,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display product name' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_variety', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_variety == 'undefined' ? true : options.creditmemo_variety,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display SKU' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_sku', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_sku == 'undefined' ? true : options.creditmemo_sku,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display billable quantity' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_catchWeightQty', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_catchWeightQty == 'undefined' ? true : options.creditmemo_catchWeightQty,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display credit/unit' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_price', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_price == 'undefined' ? true : options.creditmemo_price,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
                <CustomFormItem label='Display subtotal' colon={false} className="checkbox">
                  {getFieldDecorator('creditmemo_totalPrice', {
                    valuePropName: 'checked',
                    initialValue: !options || typeof options.creditmemo_totalPrice == 'undefined' ? true : options.creditmemo_totalPrice,
                  })(<ThemeCheckbox />)}
                </CustomFormItem>
              </div>
            </Panel>
          </Collapse>

          <div style={{ marginLeft: 23 }}>
            <ThemeButton size="large" type="primary" htmlType="submit" style={{ marginTop: 12 }}>
              Save Changes
            </ThemeButton>
          </div>
        </Form>
      </PrintSettingWrapper>
    )
  }

  getCopiesSelector = () => {
    return (
      <ThemeSelect style={{width: 60}}>
        {[...Array(10).keys()].map(i => <Select.Option value={i+1}>{i+1}</Select.Option>)}
      </ThemeSelect>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.setting

export const PrintSettingTab = withTheme(
  connect(SettingModule)(mapStateToProps)(Form.create<ThemeProps>()(PrintSetting)),
)
