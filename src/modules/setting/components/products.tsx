import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { SettingModule, SettingDispatchProps, SettingStateProps } from './../setting.module'
import { GlobalState } from '~/store/reducer'
import _ from 'lodash'
import { PrintSettingWrapper, Locations } from './../theme.style'
import { Button, Icon, Radio } from 'antd'
import { Flex, ThemeButton, ThemeInput, ThemeModal, ThemeRadio } from '~/modules/customers/customers.style'
import { ClassNames } from '@emotion/core'
import { DialogSubContainer, noPaddingFooter } from '~/modules/customers/nav-sales/styles'
import { Icon as IconSVG } from '~/components'

interface ProductInSettingProps {

}

type ThemeProps = SettingDispatchProps & SettingStateProps & ProductInSettingProps

class ProductsInSetting extends React.PureComponent<ThemeProps> {
  state = {
    locationMethod: this.props.companyInfo ? this.props.companyInfo.locationMethod : 0,
    openModal: false,
    modalType: 0,
    name: '',
    handleItem: null
  }

  componentDidMount() {
    if (!this.props.companyInfo) {
      this.props.getCompanySetting()
    }
    this.props.getItemLocations()
  }

  componentWillReceiveProps(nextProps: any) {
    console.log(this.props.companyInfo)
    console.log(nextProps.companyInfo)
    if (nextProps.companyInfo && JSON.stringify(this.props.companyInfo) && JSON.stringify(nextProps.companyInfo)) {
      this.setState({ locationMethod: nextProps.companyInfo.locationMethod })
    }
  }

  onLocationMethodChange = (evt: any) => {
    const { companyInfo } = this.props
    if (!companyInfo) return
    const company = {
      ...companyInfo,
      locationMethod: evt.target.value
    }
    const data = { values: company, showNotification: true }
    this.props.updateCompanyInfo(data)
  }

  onOpenModal = (id: any, name: string, modalType: number) => {
    const { itemLocations } = this.props
    const handleItem = itemLocations.find(el => el.id == id)
    this.setState({
      openModal: true,
      modalType,
      name,
      handleItem: modalType == 0 ? null : handleItem
    })
  }

  onCancelModal = () => {
    this.setState({
      openModal: false,
      modalType: 0,
      handleItem: null
    })
  }

  handleLocation = () => {
    const { modalType, name, handleItem } = this.state
    if (modalType == 2) {
      if (!handleItem) return
      this.props.deleteItemLocation({ id: handleItem.id })
    } else if (modalType == 1 && handleItem) {
      this.props.saveItemLocation({ id: handleItem.id, name })
    } else {
      const data = { id: 0, name }
      this.props.saveItemLocation(data)
    }

    this.onCancelModal()
  }

  renderLocations = () => {
    const { itemLocations } = this.props
    const sortedData = _.orderBy(itemLocations, "name", "asc")
    let col: any[] = []
    let result: any[] = []
    sortedData.forEach((el: any, index: number) => {
      col.push(
        <Flex className="location v-center" key={el.id}>
          <div className="name">{el.name}</div>
          <IconSVG className="icon" type="edit" viewBox="-2 -2 18 18" width={21} height={21} onClick={() => this.onOpenModal(el.id, el.name, 1)} />
          <IconSVG className="icon" type="trash" viewBox="0 0 24 24" width={18} height={18} onClick={() => this.onOpenModal(el.id, el.name, 2)} />
        </Flex>
      )
      if (itemLocations.length >= 15 && index != 0 && index % 14 == 0) {
        result.push(
          <div>
            {col}
          </div>
        )
        col = []
      } else if (index == itemLocations.length - 1) {
        result.push(
          <div>
            {col}
          </div>
        )
      }
    });
    return result
  }

  render() {
    const { locationMethod, modalType, openModal, name } = this.state
    return (
      <PrintSettingWrapper className="location-method">
        <div>
          <div className="title">Location Method</div>
          <Radio.Group onChange={this.onLocationMethodChange} value={locationMethod} style={{ float: 'left' }}>
            <ThemeRadio className="radio-option" value={0}>Pallet-level location</ThemeRadio>
            <ThemeRadio className="radio-option" value={1}>Item-level location</ThemeRadio>
          </Radio.Group>
        </div>
        <div style={{ clear: 'both' }}>
          <Locations>
            <Flex>
              {this.renderLocations()}
            </Flex>
          </Locations>
          <ThemeButton style={{ marginTop: 24 }} onClick={() => this.onOpenModal(null, '', 0)}>Add Location</ThemeButton>
        </div>
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              title={`${modalType == 0 ? 'Add' : modalType == 1 ? 'Edit' : 'Delete'} Location`}
              visible={openModal}
              onCancel={this.onCancelModal}
              okText={modalType == 2 ? 'Delete Location' : 'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={this.handleLocation}>
                    {modalType == 2 ? 'Delete Location' : 'Save'}
                  </ThemeButton>
                  <Button onClick={this.onCancelModal}>CANCEL</Button>
                </DialogSubContainer>
              }
              width={400}
            >
              <div style={{ padding: 24 }}>
                {
                  modalType != 2 ?
                    (
                      <>
                        <div>Location name</div>
                        <ThemeInput value={name} style={{ width: '70%' }} onChange={(evt: any) => { this.setState({ name: evt.target.value }) }} />
                      </>
                    ) : (
                      <>
                        <div>Any products that have been assigned to this location will no longer have a location assigned.</div>
                        <div>Please confirm that you want to delete this location.</div>
                      </>
                    )
                }
              </div>
            </ThemeModal>
          )}
        </ClassNames>
      </PrintSettingWrapper>
    )
  }
}


export const ProductsInSettingTab = connect(SettingModule)(({ setting }: GlobalState) => setting)(ProductsInSetting)
