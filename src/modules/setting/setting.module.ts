import {
  EffectModule,
  Module,
  ModuleDispatchProps,
  Effect,
  Reducer,
  StateObservable,
  DefineAction,
} from 'redux-epics-decorator'
import { Action } from 'redux-actions'
import { Observable, of, from } from 'rxjs'
import { map, endWith, catchError, switchMap, takeUntil, mergeMap } from 'rxjs/operators'
import { CACHED_QBO_LINKED, CACHED_NS_LINKED } from '~/common'

import { SettingService } from './setting.service'
import { SaleCategory, SaleSection, Company, Address, MainAddress, SaleItem } from '~/schema'
import { GlobalState } from '~/store/reducer'

import { InventoryService } from '../inventory/inventory.service'
import { MessageType } from '~/components'

import { checkError, responseHandler, handlerNoLotNumber } from '~/common/utils'
import { goBack } from 'connected-react-router'

export interface SettingStateProps {
  categories: SaleCategory[]
  sections: SaleSection[]
  newCategory: SaleSection | null
  themeKey: string
  logo: string
  logoWidth: string
  message: string
  type: MessageType | null
  companyName: string
  companyInfo: Company
  companyAddress: Address
  addressList: MainAddress[]
  printSetting: string,
  userSetting: any
  companyProductTypes: any
  loading: string
  saleItems: SaleItem[]
  itemLocations: any[]
}

@Module('setting')
export class SettingModule extends EffectModule<SettingStateProps> {
  defaultState = {
    categories: [],
    sections: [],
    themeKey: localStorage.getItem('THEME') ? localStorage.getItem('THEME') : 'green',
    logo: 'default',
    logoWidth: 'square',
    message: '',
    type: null,
    newCategory: null,
    companyName: '',
    companyInfo: null,
    companyAddress: null,
    addressList: [],
    printSetting: null,
    userSetting: null,
    companyProductTypes: [],
    loading: '',
    saleItems: [],
    itemLocations: []
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(private readonly setting: SettingService, private readonly inventory: InventoryService) {
    super()
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state, message: '', description: '', type: null, waiting: true, loadingProducts: true }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Reducer()
  loadIndex(state: SettingStateProps, action: Action<string>) {
    return {
      ...state,
      loading: action.payload,
    }
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state, message: '', description: '', type: null }
    },
  })
  resetNotif(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<SaleSection[]>) => {
      return {
        ...state,
        sections: action.payload,
      }
    },
  })
  getSections(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getSections(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET')
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      localStorage.setItem(CACHED_QBO_LINKED, action.payload.userSetting.qboRealmId)
      localStorage.setItem(CACHED_NS_LINKED, action.payload.userSetting.nsRealmId)
      localStorage.setItem('isEnableCashSales', action.payload.userSetting.company.isEnableCashSales || false)
      return {
        ...state,
        userSetting: action.payload.userSetting,
        companyName: action.payload.userSetting.companyName,
        companyAddress: action.payload.userSetting.company ? action.payload.userSetting.company.address : null,
        companyInfo: action.payload.userSetting.company,
      }
    },
  })
  getCompanySetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getUserSetting(state$.value.currentUser ? state$.value.currentUser.userId! : '0').pipe(
          // switchMap((data) => of(this.checkConnector(data.body))),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  checkConnector(data: any) {
    localStorage.setItem(CACHED_QBO_LINKED, data.qboRealmId)
    localStorage.setItem(CACHED_NS_LINKED, data.nsRealmId)
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<SaleCategory[]>) => {
      return {
        ...state,
        categories: action.payload,
      }
    },
  })
  getSaleCategories(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.inventory.getAllCategories(state$.value.currentUser.company || 'GRUBMARKET').pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state }
    },
  })
  addSubCategory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting
          .addSubCategory(state$.value.currentUser.company || 'GRUBMARKET', data.subName, data.sectionId)
          .pipe(
            switchMap((data) => of(responseHandler(data, true).body.data)),
            map(this.createAction('done')),
            endWith(this.createActionFrom(this.getSaleCategories)()),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state, newCategory: null }
    },
  })
  addMainCategory(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((sectionName: string) =>
        this.setting.addMainCategory(state$.value.currentUser.company || 'GRUBMARKET', sectionName).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getSections)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state }
    },
  })
  updateSubCategory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.setting.updateSubCategory(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getSaleCategories)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state }
    },
  })
  deleteSubCategory(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.setting.deleteSubcategory(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getSaleCategories)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state }
    },
  })
  updateSection(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.setting.updateSection(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getSections)()),
          endWith(this.createActionFrom(this.getSaleCategories)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return { ...state }
    },
  })
  deleteSection(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.setting.deleteSection(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getSections)()),
          endWith(this.createActionFrom(this.getSaleCategories)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Reducer()
  addCategory(state: SettingStateProps) {
    return {
      ...state,
      newCategory: { name: 'New Category' } as SaleSection,
    }
  }

  @Reducer()
  cancelCategory(state: SettingStateProps) {
    return {
      ...state,
      newCategory: null,
    }
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      const theme = action.payload && action.payload.theme ? action.payload.theme : 'green'
      localStorage.setItem('THEME', theme)
      return {
        ...state,
        themeKey: theme,
      }
    },
  })
  getTheme(action$: Observable<void>) {
    return action$.pipe(
      // startWith(this.createAction('loading')),
      switchMap(() =>
        this.setting.getTheme().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      const theme = action.payload && action.payload.theme ? action.payload.theme : 'green'
      localStorage.setItem('THEME', theme)
      return {
        ...state,
        themeKey: theme,
      }
    },
  })
  updateTheme(action$: Observable<string>) {
    return action$.pipe(switchMap((theme) => this.setting.updateTheme(theme).pipe(map(this.createAction('done')))))
  }

  @Effect({
    done: (state: SettingStateProps) => {
      return {
        ...state,
        message: 'Successfully updated Company Name',
        type: MessageType.SUCCESS,
      }
    },
  })
  updateCompanyName(action$: Observable<string>) {
    return action$.pipe(
      switchMap((companyName) => this.setting.updateCompanyName(companyName).pipe(map(this.createAction('done')))),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
        logo: action.payload && action.payload.imagePath ? `${process.env.AWS_PUBLIC}/${action.payload.imagePath}` : 'default',
      }
    },
  })
  getLogo(action$: Observable<void>) {
    return action$.pipe(
      // startWith(this.createAction('loading')),
      switchMap(() =>
        this.setting.getLogo().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
        logo: action.payload && action.payload.imagePath ? `${process.env.AWS_PUBLIC}/${action.payload.imagePath}` : 'default',
      }
    },
  })
  updateLogo(action$: Observable<any>) {
    return action$.pipe(switchMap((logo) => this.setting.updateLogo(logo).pipe(map(this.createAction('done')))))
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      if (action.payload.company.isEnableCashSales) {
        localStorage.setItem('isEnableCashSales', 'true')
      } else {
        localStorage.setItem('isEnableCashSales', 'false')
      }
      return {
        ...state,
        companyInfo: action.payload.company,
      }
    },
  })
  getCompanyInfo(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getCompanyInfo().pipe(
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<{ [key: string]: string }>) => {
      if (action.payload && action.payload.isEnableCashSales) {
        localStorage.setItem('isEnableCashSales', 'true')
      } else {
        localStorage.setItem('isEnableCashSales', 'false')
      }
      return {
        ...state,
        companyInfo: action.payload,
        message: 'Company Info Updated Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: SettingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  updateCompanyInfo(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting.updateCompanyInfo(data.values).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, data.showNotification).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
    // return action$.pipe(
    //   switchMap((data) => this.setting.updateCompanyInfo(data)),
    //   switchMap((data) => of(responseHandler(data, true))),
    //   map(this.createAction('done')),
    //   catchError((error) => of(checkError(error)))
    // )
  }

  @Effect({
    done: (state: SettingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        itemLocations: payload,
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: SettingStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  getItemLocations(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getItemLocations().pipe(
          switchMap((resp) =>
            of(responseHandler(resp, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, { payload }: Action<any>) => {
      const itemLocations = [...state.itemLocations]
      const findIndex = itemLocations.findIndex(el => el.id == payload.id)
      if (findIndex == -1) {
        itemLocations.push(payload)
      } else {
        itemLocations.splice(findIndex, 1, payload)
      }
      return {
        ...state,
        itemLocations,
        type: MessageType.SUCCESS,
      }
    }
  })
  saveItemLocation(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting.saveItemLocation(data.id, data.name).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, true).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, { payload }: Action<any>) => {
      const itemLocations = [...state.itemLocations]
      const findIndex = itemLocations.findIndex(el => el.id == payload.id)
      if (findIndex != -1) {
        itemLocations.splice(findIndex, 1)
      }
      return {
        ...state,
        itemLocations,
        type: MessageType.SUCCESS,
      }
    }
  })
  deleteItemLocation(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting.deleteItemLocation(data.id).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, true).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  // TODO delete logo
  @Reducer()
  deleteLogo(state: SettingStateProps) {
    return {
      ...state,
      logo: 'default',
    }
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<string>) => {
      return {
        ...state,
        logoWidth: action.payload,
      }
    },
  })
  getLogoWidth($action: Observable<void>) {
    const MOCK_DATA = 'square'

    return $action.pipe(
      // startWith(this.createAction('loading')),
      switchMap(() => of(this.createAction('done')(MOCK_DATA))),
    )
  }

  // TODO update logo width
  @Reducer()
  updateLogoWidth(state: SettingStateProps, action: Action<string>) {
    return {
      ...state,
      logoWidth: action.payload,
    }
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
        addressList: action.payload ? action.payload : [],
      }
    },
  })
  getCompanyAddress(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyAddress()),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  createCompanyAddress(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting.createCompanyAddress(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getCompanyAddress)()),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  updateCompanyAddress(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting.updateCompanyAddress(data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getCompanyAddress)()),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
        printSetting: action.payload,
      }
    },
  })
  getPrintSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting.getPrintSetting().pipe(
          switchMap((data) =>
            of(data.body.data).pipe(
              map(this.createAction('done')),
            ),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
        printSetting: action.payload,
      }
    },
  })
  savePrintSetting(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.setting.savePrintSetting(data.values).pipe(
          switchMap((resp) =>
            of(responseHandler(resp, data.showNotification).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: state.companyProductTypes.map(v => {
          if (v.id === action.payload.id) {
            return action.payload
          }
          return v
        }),
      }
    },
  })
  updateProductType(action$: Observable<void>) {
    return action$.pipe(
      switchMap((data) => this.setting.updateProductType(data)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: [...state.companyProductTypes, action.payload],
      }
    },
  })
  saveCompanyProductType(action$: Observable<void>) {
    return action$.pipe(
      switchMap((data) => this.setting.saveCompanyProductType(data)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        // message: 'Successfully updated item(s) from excel file',
        // type: MessageType.SUCCESS,
      }
    },
  })
  uploadExcel(action$: Observable<any>) {
    return action$.pipe(
      switchMap((excel) =>
        this.inventory.uploadExcel(excel).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
      }
    },
  })
  syncQBOItems(action$: Observable<any>) {
    return action$.pipe(
      switchMap((itemIds: any) => from(itemIds)),
      mergeMap((itemId: any) => {
        return this.inventory.syncQBOItem(itemId)
      }, 8),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: Action<any>) => {
      return {
        ...state,
        loading: false,
      }
    },
  })
  syncNSItems(action$: Observable<any>) {
    return action$.pipe(
      switchMap((itemIds: any) => from(itemIds)),
      mergeMap((itemId: any) => {
        return this.inventory.syncNSItem(itemId)
      }, 4),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingStateProps, action: any) => {
      let items = action.payload.map((item: any) => {
        //Calculate the data that needs to be displayed. It can be used directly when downloading
        let onHandQtyForDownLoad = handlerNoLotNumber(item, 2)
        let wholesaleProductUomList = []
        // for performance,  backend only send defaultPurchaseCostUOM and defaultSellsPriceUOM to frontend
        let wholesaleCategory = null
        if (item.pricingUOM) {
          wholesaleProductUomList.push({
            name: item.pricingUOM,
            ratio: item.pricingUOMRatio,
            priceFactor: item.pricingUOMFactor,
          })
        }
        if (item.costUOM) {
          wholesaleProductUomList.push({
            name: item.costUOM,
            ratio: item.costUOMRatio,
            priceFactor: item.costUOMFactor,
          })
        }
        if (item.category) {
          wholesaleCategory = { name: item.category, wholesaleCategoryId: item.categoryId }
        }
        return {
          ...item,
          onHandQtyForDownLoad,
          wholesaleProductUomList,
          wholesaleCategory,
        }
      })
      return {
        ...state,
        saleItems: items,
        loading: '',
        // loadingProducts: false,
        // isDownloading: false,
        // downloadData: state.isDownloading ? items : [], // when api is called by download button, this value is initialized
      }
    },
  })
  getItemListByCondition(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.inventory.getItemListByCondition(
          state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET',
          data,
        ),
      ),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }
}

export type SettingDispatchProps = ModuleDispatchProps<SettingModule>
