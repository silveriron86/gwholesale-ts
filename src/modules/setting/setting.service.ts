import { Injectable } from 'redux-epics-decorator'
import { Http, CACHED_COMPANY } from '~/common'
import { SaleSection } from '~/schema';
import { CACHED_ACCESSTOKEN } from '~/common/const'


@Injectable()
export class SettingService {
  companyId: string | null;
  constructor(private readonly http: Http) {
    this.companyId = localStorage.getItem(CACHED_COMPANY)
  }

  static get instance() {
    return new SettingService(new Http())
  }

  getUserSetting(sellerId: string) {
    return this.http.get<any>(`/session/userSettings?sellerId=${sellerId}`)
  }

  getTheme() {
    return this.http.get<any>('/session/getTheme')
  }

  getLogo() {
    return this.http.get<any>('/session/getLogo')
  }

  deleteProductType(id: string) {
    return this.http.delete<any>(`/setting/company/product/type/${id}`)
  }

  updateProductType(data: any) {
    return this.http.put<any>('/setting/company/product/type', {
      body: JSON.stringify(data),
    })
  }

  getCompanyProductAllTypes() {
    return this.http.get<any>(`/setting/company/product/types`)
  }

  saveCompanyProductType(data: any) {
    return this.http.post<any>(`/setting/company/product/type`, {
      body: JSON.stringify(data),
    })
  }

  updateTheme(theme: string) {
    return this.http.post<any>(`/session/updateTheme?theme=${theme}`)
  }

  updateCompanyName(companyName: string) {
    return this.http.post<any>(`/session/updateCompanyName?companyName=${companyName}`)
  }

  updateLogo(data: any) {
    let header = new Headers()
    // @ts-ignore
    header.append('Authorization', `Bearer ${localStorage.getItem(CACHED_ACCESSTOKEN) != null ? localStorage.getItem(CACHED_ACCESSTOKEN) : ""}`)
    header.append('Content-Type', 'application/x-www-form-urlencoded')

    return this.http.post<any>(`/session/uploadLogo`, {
      body: data,
      headers: header,
    })
  }

  getSections(warehouse: string) {
    return this.http.get<SaleSection[]>(`/inventory/${warehouse}/sections/list`)
  }

  addSubCategory(warehouse: string, subName: string, sectionId: string) {
    return this.http.post<any>(`/inventory/${warehouse}/categories/create?categoryName=${subName}&sectionId=${sectionId}`)
  }

  addMainCategory(warehouse: string, sectionName: string) {
    return this.http.post<any>(`/inventory/${warehouse}/section/create?sectionName=${sectionName}`)
  }

  updateSubCategory(data: any) {
    return this.http.put<any>(`/inventory/${data.id}/category`, {
      body: JSON.stringify(data),
    })
  }

  deleteSubcategory(data: any) {
    return this.http.delete<any>(`/inventory/${data}/category`)
  }

  updateSection(data: any) {
    return this.http.put<any>(`/inventory/${data.id}/section`, {
      body: JSON.stringify(data),
    })
  }

  deleteSection(data: any) {
    return this.http.delete<any>(`/inventory/${data}/section`)
  }

  getCompanyInfo() {
    return this.http.get<any>(`/company/${this.companyId}/setting`)
  }

  updateCompanyInfo(data: any) {
    return this.http.post<any>(`/session/updatecompanyinfo`, {
      body: JSON.stringify(data),
    })
  }

  getItemLocations() {
    return this.http.get<any>(`/location/item-locations`)
  }

  saveItemLocation(id: number, name: string) {
    return this.http.post<any>(`/location/item-location/${id}?name=${name}`)
  }

  deleteItemLocation(id: number) {
    return this.http.delete<any>(`/location/item-location/${id}`)
  }

  getCompanyAddress(addressType: string) {
    if (addressType != null) {
      return this.http.get(`/session/company-address?addressType=${addressType}`)
    } else {
      return this.http.get(`/session/company-address`)
    }
  }

  createCompanyAddress(data: any) {
    return this.http.post(`/session/company-address`, {
      body: JSON.stringify(data),
    })
  }
  updateCompanyAddress(data: number) {
    return this.http.put(`/session/company-address`, {
      body: JSON.stringify(data),
    })
  }

  getPrintSetting() {
    return this.http.get(`/session/print-setting`)
  }

  savePrintSetting(data: any) {
    return this.http.post(`/session/print-setting`, {
      body: JSON.stringify(data),
    })
  }

}
