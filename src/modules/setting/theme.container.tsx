import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { RouteProps } from 'react-router'
import { Icon, Button, Radio, Upload, Input, notification, Row, Col, Popover, Tabs } from 'antd'
import { RadioChangeEvent } from 'antd/lib/radio'
import { map, omit } from 'lodash'
import { withTheme } from 'emotion-theming'
import Form, { FormComponentProps } from 'antd/lib/form'
import BillingModal from '~/modules/customers/accounts/addresses/modals/billing-modal'
import ShippingModal from '~/modules/customers/accounts/addresses/modals/shipping-modal'
import BillingTable from '~/modules/customers/accounts/addresses/tables/billing-table'
import ShippingTable from '~/modules/customers/accounts/addresses/tables/shipping-table'
import { Wrapper } from '~/modules/customers/accounts/addresses/addresses.style'
import {
  ThemeButton,
  DetailsTitle,
  ThemeCheckbox,
  ThemeTextArea,
  ThemeModal,
} from '~/modules/customers/customers.style'
import { CACHED_ACCOUNT_TYPE } from '~/common'

const { TabPane } = Tabs

import { SettingModule, SettingDispatchProps, SettingStateProps } from './setting.module'
import { SetionTableLabel } from '../product/components/product.component.style'
import { FullInputNumber, ThemeInput, ThemeInputNumber, ThemeIcon } from '../customers/customers.style'
import { GlobalState } from '~/store/reducer'
import { white, themes, Theme } from '~/common'

import { Icon as IconSvg, MessageType } from '~/components'
import { ColorComponent } from './components/color.component'
import { MainAddress, UserRole } from '~/schema'

import {
  ThemeWrap,
  BackButton,
  Header,
  Container,
  ColorContainer,
  ColorThemeContainer,
  CurrentColorWrap,
  ColorOperation,
  buttonStyle,
  LogoContainer,
  TypeName,
  LogoWrap,
  LogoOperation,
  LogoWidthWrap,
  uploadButtonStyle,
  defaultLogoStyle,
  FormItemStyle,
  BackButtonRelative,
  TabsWrapper,
  PrintSettingWrapper,
  GeneralSettingWrapper,
} from './theme.style'
import { AccountProfileContainer } from '../account'
import { Company } from '~/schema'
import PageLayout from '~/components/PageLayout'
import { dateFormats } from 'highcharts'
import { PrintSettingTab } from './components/print-setting'
import { DateSettingTab } from './components/date-setting'
import TypesEditor from '../settings/tabs/Product/TypesEditor'
import { AdminSettingTab } from './components/admin-setting'
import { ProductsInSettingTab } from './components/products'

export type ThemeProps = SettingDispatchProps &
  SettingStateProps &
  RouteProps & {
    theme: Theme
  } & FormComponentProps<AddressFormFields>

interface ThemeState {
  currentTheme: string
  companyValue: string
  bestByDate: number
  message: string
  type: MessageType | null
  newModalVisible: Boolean
  updateModalVisible: Boolean
  newShippingModalVisible: Boolean
  updateShippingModalVisible: Boolean
  currentIndex: number
  visibleExtraCoo: boolean
  selectedTabKey: string
}

interface AddressFormFields {
  addressId: number
  street1: string
  city: string
  state: string
  zip: string
  country: string
}

const tabHashs = ['company_info', 'branding', 'settings', 'integrations', 'printables', 'admin', 'products']

export class ThemeComponent extends React.PureComponent<ThemeProps, ThemeState> {
  constructor(props: any) {
    super(props)
    this.state = {
      currentTheme: '',
      companyValue: '',
      bestByDate: 0,
      message: '',
      type: null,
      newModalVisible: false,
      updateModalVisible: false,
      newShippingModalVisible: false,
      updateShippingModalVisible: false,
      currentIndex: 0,
      visibleExtraCoo: false,
      selectedTabKey: '1',
    }
  }

  componentDidMount() {
    const { currentUser } = this.props
    if (currentUser.accountType !== UserRole.CUSTOMER) {
      this.props.getCompanySetting()
      this.props.getCompanyAddress()
    }
    this.props.getLogo()

    const hash = window.location.hash.replace('#/myaccount/setting#', '');
    if (hash) {
      const found = tabHashs.indexOf(hash)
      if (found >= 0) {
        this.setState({
          selectedTabKey: (found + 1).toString()
        });
      }
    }
  }

  componentWillReceiveProps(nextProps: ThemeProps) {
    if (nextProps.message !== this.state.message) {
      this.setState({
        message: nextProps.message,
        type: nextProps.type,
      })
      if (nextProps.type != null) {
        notification[nextProps.type!]({
          message: nextProps.type!.toLocaleUpperCase(),
          description: nextProps.message,
          onClose: this.onCloseNotification,
          duration: nextProps.type === 'success' ? 5: 4.5
        })
      }
    }
    this.setState({
      currentTheme: nextProps.themeKey ? nextProps.themeKey : 'green',
    })
  }

  onCloseNotification = () => {
    this.props.resetNotif()
  }

  handleCompanyBaseInfo = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values)
        let company: Company = this.props.setting.companyInfo
        company.fax = values.fax
        company.phone = values.phone
        company.mobileNumber = values.mobileNumber
        company.email = values.email
        company.companyName = values.companyName
        const data = { values: company, showNotification: true }
        this.props.updateCompanyInfo(data)
      }
    })
  }

  onPaymentAddressSave = (data: MainAddress) => {
    // tslint:disable-next-line:no-console
    console.log(data)

    if (data.wholesaleAddressId) {
      // update
      this.props.updateCompanyAddress(data)
      const { currentIndex, updateModalVisible } = this.state
      const visibles: Array<boolean> = [...updateModalVisible]
      visibles[currentIndex] = false
      this.setState({
        updateModalVisible: visibles,
      })
    } else {
      // create
      this.props.createCompanyAddress(data)
      this.setState({
        newModalVisible: false,
      })
    }
  }

  onShippingAddressSave = (data: MainAddress, index: number) => {
    if (data.wholesaleAddressId) {
      // update
      this.props.updateCompanyAddress(data)
      const { currentIndex, updateShippingModalVisible } = this.state
      const visibles: Array<boolean> = [...updateShippingModalVisible]
      visibles[currentIndex] = false
      this.setState({
        updateShippingModalVisible: visibles,
      })
    } else {
      // create
      this.props.createCompanyAddress(data)
      this.setState({
        newShippingModalVisible: false,
      })
    }
  }

  getBillingModal = (address: MainAddress | null, index: number) => {
    const { addressList } = this.props.setting
    const { newModalVisible, updateModalVisible } = this.state
    const visible = index === -1 ? newModalVisible : updateModalVisible[index]

    const billingAddresses: MainAddress[] = []
    addressList.forEach((address: MainAddress) => {
      if (address.addressType === 'BILLING') {
        billingAddresses.push(address)
      }
    })

    return (
      <BillingModal
        data={address}
        save={this.onPaymentAddressSave}
        visible={visible}
        isFirst={billingAddresses.length === 0}
      />
    )
  }

  getShippingModal = (address: MainAddress | null, index: number) => {
    const { addressList } = this.props.setting
    const { newShippingModalVisible, updateShippingModalVisible } = this.state
    const from = 'SETTING'
    const visible = index === -1 ? newShippingModalVisible : updateShippingModalVisible[index]

    const shippingAddresses: MainAddress[] = []
    addressList.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        shippingAddresses.push(address)
      }
    })
    return (
      <ShippingModal
        data={address}
        visible={visible}
        routes={null}
        save={this.onShippingAddressSave}
        from={from}
        isFirst={shippingAddresses.length === 0}
      />
    )
  }

  handleVisibleChange = (item: MainAddress | null, index: number, visible: boolean) => {
    if (item) {
      const visibles: Array<boolean> = [...this.state.updateModalVisible]
      visibles[index] = visible
      this.setState({
        currentIndex: index,
        updateModalVisible: visibles,
      })
    } else {
      this.setState({
        newModalVisible: visible,
      })
    }
  }

  handleShippingVisibleChange = (item: MainAddress | null, index: number, visible: boolean) => {
    if (item) {
      const visibles: Array<boolean> = [...this.state.updateShippingModalVisible]
      visibles[index] = visible
      this.setState({
        currentIndex: index,
        updateShippingModalVisible: visibles,
      })
    } else {
      this.setState({
        newShippingModalVisible: visible,
      })
    }
  }

  onCompanySettingTypeModal = (type: string) => {
    let state = { ...this.state }
    state[type] = !this.state[type]
    this.setState(state)
  }

  onChangeTab = (activeKey: string) => {
    location.hash = `#/myaccount/setting#${tabHashs[parseInt(activeKey, 10) - 1]}`
    this.setState({
      selectedTabKey: activeKey,
    })
  }

  render() {
    const { currentUser } = this.props
    const { selectedTabKey } = this.state
    return (
      <PageLayout noSubMenu={true}>
        <ThemeWrap style={{ padding: '36px 120px', textAlign: 'left' }}>
          <BackButtonRelative onClick={this.props.goBack}>
            <Icon type="arrow-left" />
            <span>back</span>
          </BackButtonRelative>
          <Header style={{ padding: '32px 0' }}>
            <div>Settings</div>
          </Header>
          {currentUser.accountType == UserRole.CUSTOMER && (
            <TabsWrapper className="page-tab">
              <Tabs defaultActiveKey="1">
                <TabPane tab={'Company Info'} key="1">
                  {this.renderCompany()}
                </TabPane>
              </Tabs>
            </TabsWrapper>
          )}
          {currentUser.accountType != UserRole.CUSTOMER && (
            <TabsWrapper className="page-tab">
              <Tabs activeKey={selectedTabKey} onChange={this.onChangeTab}>
                <TabPane tab={'Company Info'} key="1">
                  {this.renderCompany()}
                </TabPane>
                <TabPane tab={'Branding'} key="2">
                  {this.renderLogo()}
                  {this.renderColors()}
                </TabPane>
                <TabPane tab={'Settings'} key="3">
                  <DateSettingTab />
                </TabPane>
                <TabPane tab={'Products'} key="7">
                  <ProductsInSettingTab />
                </TabPane>
                <TabPane tab={'Integrations'} key="4">
                  <AccountProfileContainer isCompanySetting={true} userSetting={this.props.setting.userSetting} />
                </TabPane>
                <TabPane tab={'Printables'} key="5">
                  <PrintSettingTab />
                </TabPane>
                {currentUser.accountType === UserRole.SUPERADMIN && (
                  <TabPane tab={'Admin'} key="6">
                    <AdminSettingTab />
                  </TabPane>
                )}
              </Tabs>
            </TabsWrapper>
          )}
        </ThemeWrap>
      </PageLayout>
    )
  }

  private renderCompany() {
    const {
      form,
      currentUser,
      setting: { companyInfo, addressList },
    } = this.props
    const {
      companyValue,
      newModalVisible,
      updateModalVisible,
      newShippingModalVisible,
      updateShippingModalVisible,
    } = this.state
    const { getFieldDecorator } = form
    const billingAddresses: MainAddress[] = []
    const shippingAddresses: MainAddress[] = []
    addressList.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        shippingAddresses.push(address)
      } else {
        billingAddresses.push(address)
      }
    })

    return (
      <GeneralSettingWrapper>
        <Form onSubmit={this.handleCompanyBaseInfo}>
          <Row>
            <Col span={8} className="mt-24">
              <div>Company Name</div>
              <Form.Item>
                {getFieldDecorator('companyName', {
                  initialValue: companyInfo ? companyInfo.companyName : companyValue,
                  rules: [{ required: true, message: `Company name is required!` }],
                })(<Input placeholder="Company Name" disabled />)}
              </Form.Item>
            </Col>
          </Row>
          <DetailsTitle style={{ padding: '4px 0', marginTop: 12 }}>Company Contact Information</DetailsTitle>
          <Row>
            <Col span={8} className="mt-24">
              <div>Phone Number</div>
              <Form.Item>
                {getFieldDecorator('phone', {
                  initialValue: companyInfo ? companyInfo.phone : '',
                })(<Input placeholder="Phone Number" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={8} offset={0}>
              <div>Fax</div>
              <Form.Item>
                {getFieldDecorator('fax', {
                  initialValue: companyInfo ? companyInfo.fax : '',
                })(<Input placeholder="Fax" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <div>Mobile number (for text/SMS)</div>
              <Form.Item>
                {getFieldDecorator('mobileNumber', {
                  initialValue: companyInfo ? companyInfo.mobileNumber : '',
                })(<Input placeholder="Mobile phone" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <div>Email address</div>
              <Form.Item>
                {getFieldDecorator('email', {
                  initialValue: companyInfo ? companyInfo.email : '',
                })(<Input placeholder="Email address" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={24}>
              <Button
                shape="round"
                size="large"
                type="primary"
                htmlType="submit"
                style={{
                  ...buttonStyle,
                  backgroundColor: this.props.theme.primary,
                }}
              >
                Save
              </Button>
            </Col>
          </Row>
        </Form>
        <hr style={{ marginTop: '40px', marginBottom: '40px' }}></hr>
        {currentUser.accountType != UserRole.CUSTOMER && (
          <>
            <Row>
              <DetailsTitle style={{ padding: '4px 0' }}>Company Addresses</DetailsTitle>
              <Col md={12}>
                <Wrapper>
                  <DetailsTitle>&nbsp;</DetailsTitle>
                  <Popover
                    placement="left"
                    content={this.getBillingModal(null, -1)}
                    trigger="click"
                    visible={newModalVisible}
                    onVisibleChange={this.handleVisibleChange.bind(this, null, 0)}
                  >
                    <ThemeButton shape="round" className="popover-with-buttons-billing">
                      <Icon type="plus-circle" theme="filled" />
                      New Payment Address
                    </ThemeButton>
                  </Popover>
                </Wrapper>

                <BillingTable
                  listItems={billingAddresses}
                  getModal={this.getBillingModal}
                  updateModalVisible={updateModalVisible}
                  handleVisibleChange={this.handleVisibleChange}
                />
              </Col>
              <Col md={12}>
                <Wrapper>
                  <DetailsTitle>&nbsp;</DetailsTitle>
                  <Popover
                    placement="left"
                    content={this.getShippingModal(null, -1)}
                    trigger="click"
                    visible={newShippingModalVisible}
                    onVisibleChange={this.handleShippingVisibleChange.bind(this, null, 0)}
                  >
                    <ThemeButton shape="round" className="popover-with-buttons-delivery">
                      <Icon type="plus-circle" theme="filled" />
                      New Delivery Address
                    </ThemeButton>
                  </Popover>
                </Wrapper>
                <ShippingTable
                  listItems={shippingAddresses}
                  getModal={this.getShippingModal}
                  updateModalVisible={updateShippingModalVisible}
                  handleVisibleChange={this.handleShippingVisibleChange}
                  isVendor={false}
                  fromCompanySetting={true}
                />
              </Col>
            </Row>

            <hr style={{ marginTop: '40px', marginBottom: '40px' }}></hr>
            <Row>
              <Col span={6}>
                <Wrapper>
                  <DetailsTitle style={{ padding: '4px 0' }}>Global Fields</DetailsTitle>
                  <ThemeButton shape="round" onClick={this.onCompanySettingTypeModal.bind(this, 'visibleExtraCoo')}>
                    <Icon type="plus-circle" theme="filled" />
                    Add New Origin
                  </ThemeButton>
                </Wrapper>
              </Col>
              <ThemeModal
                title={`Edit Value List "Origin"`}
                visible={this.state.visibleExtraCoo}
                onCancel={this.onCompanySettingTypeModal.bind(this, 'visibleExtraCoo')}
                cancelText="Close"
                okButtonProps={{ style: { display: 'none' } }}
              >
                <TypesEditor isModal={true} field="extraCOO" title="Origin" buttonTitle="Add Origin" />
              </ThemeModal>
            </Row>
          </>
        )}
      </GeneralSettingWrapper>
    )
  }

  private renderLogo() {
    const { theme } = this.props
    const { logo, logoWidth } = this.props.setting
    const logoImage =
      !logo || typeof logo === 'undefined' || logo === 'default' ? (
        <IconSvg type="logo" viewBox={void 0} color={white} style={defaultLogoStyle} />
      ) : (
        <img src={logo} />
      )
    return (
      <LogoContainer>
        <div style={{ marginRight: '78px' }}>
          <TypeName>logo image</TypeName>
          <LogoWrap>{logoImage}</LogoWrap>
          <LogoOperation>
            <Upload accept="images/*" showUploadList={false} beforeUpload={this.onChangeLogo}>
              <Button
                type="ghost"
                style={{
                  ...uploadButtonStyle,
                  color: theme.primary,
                  borderColor: theme.primary,
                }}
              >
                Upload New Image
              </Button>
            </Upload>
            <Icon type="delete" onClick={this.onDeleteLogo} />
          </LogoOperation>
        </div>
        <div>
          <TypeName>logo width</TypeName>
          <LogoWidthWrap>
            <Radio.Group value={logoWidth} onChange={this.onChangeLogoWidth}>
              <Radio value="square">square</Radio>
              <Radio value="wide">wide</Radio>
            </Radio.Group>
          </LogoWidthWrap>
        </div>
      </LogoContainer>
    )
  }

  private renderColors() {
    const { currentTheme } = this.state
    const currentColors = omit(themes[currentTheme], 'theme')

    return (
      <ColorContainer>
        <span>theme color</span>
        <div style={{ display: 'inline-block' }}>
          <ColorThemeContainer>
            {map(themes, (theme, name) => (
              <ColorComponent
                key={name}
                theme={theme}
                name={name}
                active={currentTheme === name}
                onClick={this.onClickCurrentTheme(name)}
              />
            ))}
          </ColorThemeContainer>
          <CurrentColorWrap>
            {map(currentColors, (color, name) => (
              <div style={{ backgroundColor: color }} key={name} />
            ))}
          </CurrentColorWrap>
        </div>
        <ColorOperation style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Button
            type="primary"
            style={{
              ...buttonStyle,
              backgroundColor: this.props.theme.primary,
            }}
            onClick={this.onClickUpdateTheme}
          >
            Update Theme
          </Button>
        </ColorOperation>
      </ColorContainer>
    )
  }

  private onClickCurrentTheme = (theme: string) => () => {
    this.setState({
      currentTheme: theme,
    })
  }

  private onClickUpdateTheme = () => {
    this.props.updateTheme(this.state.currentTheme)
  }

  private getBase64 = (file: any, cb: any) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
      cb(reader.result)
    }
    reader.onerror = function (error) {
      //console.log('Error: ', error);
    }
  }

  private onChangeLogo = (file: File) => {
    this.getBase64(file, (result: any) => {
      const details = {
        ext: file.name.split('.')[1],
        image: result,
      }

      const formBody = []
      for (const property in details) {
        const encodedKey = encodeURIComponent(property)
        const encodedValue = encodeURIComponent(details[property])
        formBody.push(encodedKey + '=' + encodedValue)
      }
      this.props.updateLogo(formBody.join('&'))
    })
    return false
  }

  private onDeleteLogo = () => {
    this.props.deleteLogo()
  }

  private onChangeLogoWidth = (e: RadioChangeEvent) => {
    const value = e.target.value
    this.props.updateLogoWidth(value)
  }
}

const mapStateToProps = (state: GlobalState) => {
  return {
    currentUser: state.currentUser,
    setting: state.setting,
  }
}

export const ThemeSetting = withTheme(
  connect(SettingModule)(mapStateToProps)(Form.create<ThemeProps>()(ThemeComponent)),
)
