import styled from '@emotion/styled'
import { Form } from 'antd'
import { white, mediumGrey, black, transparent, lightGrey, darkGrey, brownGrey, darkGreen } from '~/common'

export const Container = styled('div')({
  display: 'flex',

  '& .ant-input': {
    width: '671px',
    border: 'none',
    borderBottom: '1px solid black',
    borderRadius: 0,
    fontWeight: 700,
    flex: 1,
    '&:focus': {
      boxShadow: 'none',
    },
  },
})

export const ThemeWrap = styled('div')({
  width: '100%',
  minHeight: '100vh',
  backgroundColor: white,
  boxSizing: 'border-box',
  position: 'relative',
  padding: '36px 220px',

  '& *': {
    boxSizing: 'border-box',
  },
})

export const BackButton = styled('div')((props) => ({
  position: 'absolute',
  top: '36px',
  left: '120px',
  color: props.theme.primary,
  cursor: 'pointer',

  '& > span': {
    fontSize: '12px',
    textTransform: 'uppercase',
  },
  '& > i': {
    fontSize: '12px',
    marginRight: '8px',
  },
}))

export const BackButtonRelative = styled('div')((props: any) => ({
  color: props.theme.primary,
  cursor: 'pointer',

  '& > span': {
    fontSize: '12px',
    textTransform: 'uppercase',
  },
  '& > i': {
    fontSize: '12px',
    marginRight: '8px',
  },
}))

export const TabsWrapper = styled('div')((props: any) => ({
  textAlign: 'left',
  '.ant-tabs-bar': {
    marginBottom: 8,
  },
  '.ant-tabs-nav-wrap': {
    '.ant-tabs-nav': {
      '.ant-tabs-tab': {
        fontFamily: 'Museo Sans Rounded',
        fontWeight: 'normal',
        fontSize: 17,
        lineHeight: '22px',
        color: '#4A5355',
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 8,
        '&.ant-tabs-tab-active': {
          borderBottom: `4px solid ${props.theme.theme}`,
          color: props.theme.theme,
          fontWeight: 'bold',
        },
      },
      '.ant-tabs-ink-bar': {
        height: 3,
      },
    },
  },
  '.ant-tabs-top-content > .ant-tabs-tabpane': {
    '.tab-header': {
      // paddingLeft: 80,
      fontFamily: 'Museo Sans Rounded',
      // height: 50,
      paddingRight: 20,
      paddingBottom: 8,
      // borderBottom: '3px solid #DBDBDB',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      h3: {
        fontSize: 20,
        // lineHeight: '27.6px',
        color: '#22282A',
        marginBottom: 6,
        marginRight: 12,
      },
      label: {
        fontSize: 16,
        lineHeight: '22.4px',
        color: '#4A5355',
      },
    },
    '.ant-table-wrapper': {
      // paddingLeft: 80,
      '.ant-table-content .ant-table-body .ant-table-thead > tr > th': {
        paddingLeft: 16,
      },
      '.ant-table-content .ant-table-body .ant-table-tbody > tr > td ': {
        backgroundColor: 'transparent',
      },
    },
    '.ant-pagination': {
      marginRight: 20,
    },
  },

}))

export const Header = styled('div')({
  width: '100%',
  '& > span': {
    fontSize: '12px',
    color: mediumGrey,
    textTransform: 'uppercase',
    fontWeight: 500,
  },
  '& > div': {
    fontSize: '36px',
    color: black,
    letterSpacing: '.05em',
    lineHeight: '52px',
    fontWeight: 600,
  },
})

export const ColorContainer = styled('div')({
  margin: '56px 0 30px',

  '& > span': {
    fontSize: '14px',
    letterSpacing: '.05em',
    color: mediumGrey,
    textTransform: 'uppercase',
    display: 'block',
  },
})

export const ColorThemeContainer = styled('div')({
  marginTop: '18px',
  display: 'inline-flex',
  borderBottom: `2px solid ${darkGrey}`,
})

export const ColorWrap = styled('div')({
  width: '64px',
  height: '100px',
  textAlign: 'center',
  position: 'relative',

  '& + &': {
    marginLeft: '38px',
  },
})

export const ColorThemeWrap = styled('div')((props: any) => ({
  width: '64px',
  height: '64px',
  position: 'relative',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  cursor: 'pointer',
  '& > span': {
    width: props.active ? '64px' : '54px',
    height: props.active ? '64px' : '54px',
    backgroundColor: transparent,
    border: props.active ? `3px solid ${darkGrey}` : `1px solid ${lightGrey}`,
    borderRadius: '100px',
  },
  '& > div': {
    width: '48px',
    height: '48px',
    position: 'absolute',
    top: '8px',
    left: '8px',
    border: 0,
    borderRadius: '48px',
  },
}))

export const ColorThemeName = styled('div')((props: any) => ({
  fontSize: '16px',
  lineHeight: '16px',
  color: brownGrey,
  fontWeight: props.active ? 600 : 400,
  marginTop: '4px',
}))

export const ColorThemeIcon = styled('div')((props: any) => ({
  width: '100%',
  position: 'absolute',
  bottom: '-6px',
  left: 0,
  textAlign: 'center',
  display: props.active ? 'block' : 'none',
}))

export const CurrentColorWrap = styled('div')({
  width: '100%',
  marginTop: '6px',
  display: 'flex',

  '& > div': {
    width: '25%',
    height: '45px',
  },
})

export const ColorOperation = styled('div')({
  display: 'flex',
  marginTop: '60px',
})

export const buttonStyle: React.CSSProperties = {
  borderRadius: '200px',
  height: '40px',
  margin: '0 12px 0 0',
  border: 0,
}

export const LogoContainer = styled('div')({
  display: 'flex',
  marginTop: '20px',
})

export const TypeName = styled('span')({
  fontSize: '14px',
  letterSpacing: '.05em',
  color: mediumGrey,
  textTransform: 'uppercase',
  display: 'block',
})

export const LogoWrap = styled('div')({
  width: '230px',
  height: '230px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  marginTop: '10px',
  padding: '10px',
  backgroundImage: `url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAC1JREFUeNpiPHPmDAM2YGxsjFWciYFEMKqBGMD4//9/rBJnz54dDSX6aQAIMABCtQiAsDRF+wAAAABJRU5ErkJggg==')`,
  '& > img': {
    objectFit: 'contain',
    maxHeight: '210px',
    maxWidth: '210px',
  },
})

export const LogoOperation = styled('div')((props) => ({
  display: 'flex',
  marginTop: '14px',
  alignItems: 'center',

  '& > i': {
    color: props.theme.primary,
    fontSize: '20px',
    cursor: 'pointer',
  },
}))

export const uploadButtonStyle: React.CSSProperties = {
  height: '40px',
  marginRight: '18px',
  borderRadius: '200px',
}

export const defaultLogoStyle: React.CSSProperties = {
  width: '115px',
  height: '35px',
}

export const FormItemStyle: React.CSSProperties = {
  display: 'flex'
}

export const LogoWidthWrap = styled('div')({
  marginTop: '10px',
})

export const PrintSettingWrapper = styled('div')((props) => ({
  width: '60%',
  '.ant-form-item': {
    fontFamily: `Museo Sans Rounded`,
    fontWeight: 600,
    marginBottom: 0,
  },
  '.sub-title': {
    fontFamily: `Museo Sans Rounded`,
    fontWeight: 500,
    marginTop: 12,
  },
  '.ant-form-item.checkbox': {
    display: 'flex',
    flexDirection: 'row-reverse',
    justifyContent: 'flex-end',
    '.ant-form-item-control-wrapper': {
      marginRight: 8
    }
  },
  '&.location-method': {
    paddingTop: 24,
    '.title': {
      fontWeight: 'bold',
      marginBottom: 12
    },
    '.radio-option': {
      display: 'block',
      height: '34px',
      lineHeight: '30px',
    }
  },
  '.ant-collapse': {
    border: 0,
    background: 'transparent',
    '& > .ant-collapse-item': {
      borderBottom: 0
    },
    '.ant-collapse-header': {
      width: 164,
      paddingTop: 0,
      paddingRight: 0,
      paddingLeft: 23,
      color: props.theme.theme,
      '.ant-collapse-arrow': {
        marginTop: -5,
        right: 0,
        color: props.theme.theme,
      }
    },
    '.ant-collapse-content': {
      borderTop: 0,
      '& > .ant-collapse-content-box': {
        padding: 0
      }
    }
  }
}))

export const CustomFormItem = styled(Form.Item)({
  'label': {
    fontWeight: 500
  },
  '.non-bold': {
    fontWeight: 500,
    fontFamily: 'Arial'
  },
  '&.inline': {
    display: 'flex'
  }
})

export const GeneralSettingWrapper = styled('div')({
  '& table thead': {
    boxShadow: 'none !important'
  },
  '.mt-24': {
    marginTop: 24
  }
})

export const Locations = styled('div')((props: any) => ({
  marginTop: 24,
  paddingLeft: 22,
  '.location': {
    padding: '2px 0',
    '.name': {
      paddingTop: 2,
      color: props.theme.dark,
      marginRight: 28
    },
    '.icon': {
      marginLeft: 4,
      visibility: 'hidden',
      cursor: 'pointer',
      'path': {
        fill: props.theme.dark
      }
    }
  },
  '.location:hover': {
    '.icon': {
      visibility: 'visible'
    }
  }
}))
