import * as React from 'react'
import { Icon, Col, Layout, Row, Select } from 'antd'

import { InputLabel, PopoverWrapper, InputRow, ThemeSelect } from '../../../modules/customers/customers.style'
import { CustomButton } from '~/components'
import { transLayout } from '../../../modules/customers/components/customers-detail/customers-detail.style'
import { ThemeSelectStyle } from '../settings.style'
import { WEEKDAYS, WholesalePayload } from '~/schema'
import { RoutingProps } from '../tabs/Routing'
import { GlobalState } from '~/store/reducer'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { SettingsModule } from '../settings.module'
import Form, { FormComponentProps } from 'antd/lib/form'

const { Option } = Select

interface FormFields {
  deliveryWeekOfDay: string
  driverId: string
  wholesaleRouteId: string
}

export type PayloadModalProps = RoutingProps &
  FormFields &
  FormComponentProps<FormFields> & {
    data: WholesalePayload | null
    cancel: Function
    save: Function
  }

class PayloadModalComp extends React.PureComponent<PayloadModalProps> {
  state: any
  constructor(props: PayloadModalProps) {
    super(props)

    this.state = {
      data: {
        deliveryWeekOfDay: '',
        wholesaleRouteId: null,
        driverId: null,
      },
    }
  }

  onChangeDayOfWeek = (day: string) => {
    const data = { ...this.state.data }
    data.deliveryWeekOfDay = day
    this.setState({
      data: data,
    })
  }

  onChangeDriverName = (driver_id: number) => {
    const data = { ...this.state.data }
    data.driverId = driver_id
    this.setState({
      data: data,
    })
  }

  onChangeRouteName = (route_id: number) => {
    const data = { ...this.state.data }
    data.wholesaleRouteId = route_id
    this.setState({
      data: data,
    })
  }

  renderWeekDays = () => {
    const rows = []
    for (let i = 0; i < WEEKDAYS.length; i++) {
      rows.push(
        <Option key={i} value={WEEKDAYS[i]}>
          {WEEKDAYS[i]}
        </Option>,
      )
    }
    return rows
  }

  renderRoutes = () => {
    const rows = []
    const { deliveryWeekOfDay } = this.state.data
    const { payloads } = this.props
    let assignedRouteIds: number[] = []
    console.log(payloads)
    if (payloads && payloads.length > 0) {
      payloads.forEach((element) => {
        if (element.deliveryWeekOfDay == deliveryWeekOfDay) {
          assignedRouteIds.push(element.wholesaleRoute.id)
        }
      })
    }
    const { allRoutes } = this.props
    for (let i = 0; i < allRoutes.length; i++) {
      const route = allRoutes[i]
      if (
        deliveryWeekOfDay &&
        route.activeDays.indexOf(deliveryWeekOfDay) > -1 &&
        assignedRouteIds.indexOf(route.id) < 0
      ) {
        rows.push(
          <Option key={i} value={route.id}>
            {route.routeName}
          </Option>,
        )
      }
    }
    return rows
  }

  renderDrivers = () => {
    const rows = []
    const { drivers } = this.props
    for (let i = 0; i < drivers.length; i++) {
      const driver = drivers[i]
      rows.push(
        <Option key={i} value={driver.id}>
          {driver.firstName + ' ' + driver.lastName}
        </Option>,
      )
    }
    return rows
  }

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    const { form, save } = this.props
    form.validateFields((err, values) => {
      if (!err) {
        save(values)
      }
    })
    form.resetFields()
  }

  handleCancel = () => {
    const { form, cancel } = this.props
    form.resetFields()
    cancel()
  }

  render() {
    const {
      form: { getFieldDecorator },
    } = this.props
    return (
      <Form onSubmit={this.handleSubmit}>
        <PopoverWrapper style={{ width: 600 }}>
          <Layout style={transLayout}>
            <InputRow>
              <Col md={7}>
                <InputLabel>DAY OF WEEK</InputLabel>
              </Col>
              <Col md={17}>
                <Form.Item>
                  {getFieldDecorator('deliveryWeekOfDay', {
                    initialValue: undefined,
                    rules: [{ required: true, message: 'Please select delivery week of day!' }],
                  })(
                    <ThemeSelect
                      placeholder="Select Delivery Week Of Day"
                      onChange={this.onChangeDayOfWeek}
                      style={ThemeSelectStyle}
                    >
                      {this.renderWeekDays()}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>DRIVER NAME</InputLabel>
              </Col>
              <Col md={17}>
                <Form.Item>
                  {getFieldDecorator('driverId', {
                    initialValue: undefined,
                    rules: [{ required: true, message: 'Please select one of the drivers!' }],
                  })(
                    <ThemeSelect
                      placeholder="Select Driver"
                      onChange={this.onChangeDriverName}
                      style={ThemeSelectStyle}
                    >
                      {this.renderDrivers()}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            </InputRow>
            <InputRow>
              <Col md={7}>
                <InputLabel>ROUTES</InputLabel>
              </Col>
              <Col md={17}>
                <Form.Item>
                  {getFieldDecorator('wholesaleRouteId', {
                    initialValue: undefined,
                    rules: [{ required: true, message: 'Please select one of the routes!' }],
                  })(
                    <ThemeSelect placeholder="Select Route" onChange={this.onChangeRouteName} style={ThemeSelectStyle}>
                      {this.renderRoutes()}
                    </ThemeSelect>,
                  )}
                </Form.Item>
              </Col>
            </InputRow>
            <Row>
              <Col md={12} style={{ textAlign: 'center' }}>
                <CustomButton htmlType="submit">
                  <Icon type="save" />
                  Save New Payload
                </CustomButton>
              </Col>
              <Col md={12} style={{ textAlign: 'center' }}>
                <CustomButton onClick={this.handleCancel}>
                  <Icon type="close" />
                  Exit & Discard Changes
                </CustomButton>
              </Col>
            </Row>
          </Layout>
        </PopoverWrapper>
      </Form>
    )
  }
}
const mapStateToProps = (state: GlobalState) => state.settings
const ModalForm = Form.create()(PayloadModalComp)
export const PayloadModal = withTheme(connect(SettingsModule)(mapStateToProps)(ModalForm))
