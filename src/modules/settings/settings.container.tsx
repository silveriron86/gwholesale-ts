import * as React from 'react'
import PageLayout from '~/components/PageLayout'
import { Routing } from './tabs/Routing'

class Settings extends React.PureComponent {
  state = {
    tabKey: '1',
  }

  onChangeTab = (key: string) => {
    this.setState({
      tabKey: key,
    })
  }

  render() {
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Selling & Shipping-Drivers'}>
        <Routing />
      </PageLayout>
    )
  }
}

export default Settings
