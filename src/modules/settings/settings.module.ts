import { EffectModule, Module, ModuleDispatchProps, DefineAction, Effect } from 'redux-epics-decorator'
import { Observable, of } from 'rxjs'
import { switchMap, takeUntil, map, catchError, endWith } from 'rxjs/operators'
import { Action } from 'redux-actions'

import { SettingsService } from './settings.service'
import { AccountUser, WholesalePayload, WholesaleRoute } from '~/schema'
import { checkError, responseHandler } from '~/common/utils'
import { MessageType } from '~/components'

export interface SettingsProps {
  loading: boolean
  payloads: WholesalePayload[]
  drivers: AccountUser[]
  allRoutes: WholesaleRoute[]
  UOMs: string
  companyProductTypes: any
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
}

@Module('settings')
export class SettingsModule extends EffectModule<SettingsProps> {
  defaultState = {
    loading: true,
    payloads: [] as WholesalePayload[],
    drivers: [] as AccountUser[],
    allRoutes: [] as WholesaleRoute[],
    UOMs: '',
    companyProductTypes: null,
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(private settings: SettingsService) {
    super()
  }

  @Effect({
    done: (state: SettingsProps, { payload }: Action<WholesalePayload[]>) => {
      return { ...state, payloads: payload }
    },
  })
  getDriverSchedules(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.settings.getDriverSchedules()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingsProps, { payload }: Action<AccountUser[]>) => {
      return { ...state, drivers: payload }
    },
  })
  getAllDrivers(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.settings.getAllDrivers()),
      switchMap((data) => of(this.formatDriver(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingsProps, { payload }: Action<WholesaleRoute[]>) => {
      return { ...state, allRoutes: payload }
    },
  })
  getAllRoutes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.settings.getAllRoutes()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingsProps, { payload }: Action<any>) => {
      return {
        ...state,
        payloads: [...state.payloads, payload],
      }
    },
  })
  createPayload(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => this.settings.createPayload(data)),
      switchMap((data) => of(this.addNewPayload(responseHandler(data).body.data))),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: SettingsProps, { payload }: Action<number>) => {
      let { payloads } = state
      let index = 0
      payloads.forEach((el, i) => {
        if (el.id === payload) {
          index = i
        }
      })
      let result: any[] = []
      if (index !== 0) {
        result = [...payloads.slice(0, index), ...payloads.slice(index + 1)]

        return {
          ...state,
          payloads: result,
        }
      }
    },
  })
  deletePayload(action$: Observable<number>) {
    return action$.pipe(
      switchMap((schedule_id: any) => this.settings.deletePayload(schedule_id)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  formatDriver(arr: any[]): AccountUser[] {
    return arr.map((data) => ({
      id: data.userId,
      emailAddress: data.emailAddress,
      firstName: data.firstName,
      lastName: data.lastName,
      phone: data.phone,
      status: data.status,
      addressId: data.address ? data.address.addressId : null,
      company: data.company,
      password: '',
    }))
  }

  addNewPayload(obj: any) {
    const data: WholesalePayload = {
      id: obj.id,
      driver: obj.driver,
      deliveryWeekOfDay: obj.deliveryWeekOfDay,
      wholesaleRoute: obj.wholesaleRoute,
      scheduleOrder: obj.scheduleOrder,
    }
    return data
  }

  @Effect({
    done: (state: SettingsProps, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.settings.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
    )
  }

  @Effect({
    done: (state: SettingsProps, { payload }: Action<any>) => {
      return { ...state }
    },
  })
  deleteProductType(action$: Observable<string>) {
    return action$.pipe(
      switchMap((id: string) =>
        this.settings.deleteProductType(id).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCompanyProductAllTypes)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingsProps, action: Action<any[]>) => {
      return {
        ...state,
        message: 'Type Updated Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: SettingsProps, action: Action<any[]>) => {
      return {
        ...state,
        type: MessageType.ERROR,
        error: action.payload,
      }
    },
  })
  updateProductType(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.settings.updateProductType(data).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCompanyProductAllTypes)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: SettingsProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        message: 'Type Create Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: SettingsProps, { payload }: Action<any>) => {
      return {
        ...state,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  setCompanyProductType(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data: any) => this.settings.saveCompanyProductType(data)),
      switchMap((data) =>
        of(responseHandler(data, true)).pipe(
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getCompanyProductAllTypes)()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  formatProductTypes = (datas: any[]) => {
    let result = {
      unitOfMeasure: [],
      returnReason: [],
      carrier: [],
      modifier: [],
      costStructure: [],
      modeOfTransportation: [],
      suppliers: [],
      freightTypes: [],
      shippingTerms: [],
      paymentTerms: [],
      extraCOO: [],
      extraCharges: [],
      customerTypes: [],
      vendorTypes: [],
      grossWeightUnit: [],
      grossVolumeUnit: [],
      reasonType: []
    }

    // UNIT_OF_MEASURE, RETURN_REASON, CARRIER,MODIFIER,COST_STRUCTURE,MODE_OF_TRANSPORTATION
    datas.map((type: any) => {
      switch (type.type) {
        case 'UNIT_OF_MEASURE':
          result.unitOfMeasure.push(type)
          break
        case 'RETURN_REASON':
          result.returnReason.push(type)
          break
        case 'CARRIER':
          result.carrier.push(type)
          break
        case 'MODIFIER':
          result.modifier.push(type)
          break
        case 'COST_STRUCTURE':
          result.costStructure.push(type)
          break
        case 'MODE_OF_TRANSPORTATION':
          result.modeOfTransportation.push(type)
          break
        case 'SUPPLIERS':
          result.suppliers.push(type)
          break
        case 'FREIGHT':
          result.freightTypes.push(type)
          break
        case 'SHIPPING_TERM':
          result.shippingTerms.push(type)
          break
        case 'FINANCIAL_TERM':
          result.paymentTerms.push(type)
          break
        case 'EXTRA_COO':
          result.extraCOO.push(type)
          break
        case 'EXTRA_CHARGE':
          result.extraCharges.push(type)
          break
        case 'CUSTOMER_BUSINESS_TYPE':
          result.customerTypes.push(type)
          break
        case 'VENDOR_BUSINESS_TYPE':
          result.vendorTypes.push(type)
          break
        case 'GROSS_WEIGHT_UNIT':
          result.grossWeightUnit.push(type)
          break
        case 'GROSS_VOLUME_UNIT':
          result.grossVolumeUnit.push(type)
          break
        case 'REASON_TYPE':
          result.reasonType.push(type)
          break 
      }
    })
    return result
  }
}
export type SettingsDispatchProps = ModuleDispatchProps<SettingsModule>
