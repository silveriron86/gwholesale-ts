import { Http, CACHED_USER_ID } from '~/common'
import { Injectable } from 'redux-epics-decorator'

@Injectable()
export class SettingsService {
  constructor(private readonly http: Http) {}

  static get instance() {
    return new SettingsService(new Http())
  }

  getDriverSchedules() {
    return this.http.get<any>(`/delivery/payloads/allpayloads`)
  }

  getAllDrivers() {
    const user_id = localStorage.getItem(CACHED_USER_ID)
    return this.http.get<any>(`/delivery/drivers/${user_id}`)
  }

  getAllRoutes() {
    const user_id = localStorage.getItem(CACHED_USER_ID)
    return this.http.get<any>(`/delivery/routes/allroutes/${user_id}`)
  }

  createPayload(payload: any) {
    const user_id = localStorage.getItem(CACHED_USER_ID)
    return this.http.post<any>(`/delivery/${user_id}/driverschedule`, {
      body: JSON.stringify(payload),
    })
  }

  deletePayload(schedule_id: number) {
    const user_id = localStorage.getItem(CACHED_USER_ID)
    return this.http.delete<any>(`/delivery/${user_id}/driverschedule/${schedule_id}`)
  }

  deleteProductType(id: string) {
    return this.http.delete<any>(`/setting/company/product/type/${id}`)
  }

  updateProductType(data: any) {
    return this.http.put<any>('/setting/company/product/type', {
      body: JSON.stringify(data),
    })
  }

  getCompanyProductAllTypes() {
    return this.http.get<any>(`/setting/company/product/types`)
  }

  saveCompanyProductType = (data: any) => {
    return this.http.post<any>(`/setting/company/product/type`, {
      body: JSON.stringify(data),
    })
  }
}
