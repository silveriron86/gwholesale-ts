import styled from '@emotion/styled'
import { Tabs } from 'antd'
import { mediumGrey } from '~/common'

export const SettingsTabs = styled(Tabs)((props) => ({
  '&': {
    '&.ant-tabs.ant-tabs-card': {
      '.ant-tabs-nav': {
        display: 'flex',
        marginTop: 3,
        '>div': {
          flex: 1,
          display: 'flex',
          '.ant-tabs-tab': {
            flex: 1,
            fontSize: 18,
            height: 37,
            borderColor: '#c0c0c0',
          },
          '.ant-tabs-tab-active': {
            color: props.theme.primary,
            fontWeight: 'bold',
            height: 40,
          },
          '.ant-tabs-tab:hover': {
            color: props.theme.primary,
          },
        },
      },
    },
  },
}))

export const Label = styled('div')((props) => ({
  fontSize: 15,
  fontWeight: 'bold',
  marginBottom: 20,
  textAlign: 'center',
}))

export const settingsWrapper: React.CSSProperties = {
  padding: 30,
  paddingLeft: 70,
  backgroundColor: 'transparent',
}

export const mt5: React.CSSProperties = {
  marginTop: 5,
}

export const InputLabel = styled('div')({
  color: mediumGrey,
  fontSize: 12,
  lineHeight: '12px',
  fontWeight: 'bold',
  letterSpacing: '0.05em',
  textAlign: 'right',
  padding: '10px 10px 0',
})

export const ThemeSelectStyle: React.CSSProperties = {
  width: '100%',
}

export const productColStyle: React.CSSProperties = {
  border: '1px solid #eee',
  padding: 20,
  backgroundColor: 'transparent'
}