import * as React from 'react'
import { Layout, Row, Col, Icon } from 'antd'
import Form, { FormComponentProps } from 'antd/lib/form'
import { settingsWrapper, InputLabel, Label, mt5 } from './../settings.style'
import { ThemeInput, ThemeButton } from '../../customers/customers.style'
import { Company } from '~/schema'
import { mb0 } from '~/modules/customers/sales/_style'

const FormItem = Form.Item
interface GeneralProps extends FormComponentProps {
  companyInfo: Company
  onUpdate: Function
}

class General extends React.PureComponent<GeneralProps> {
  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const v = {...values}
        console.log(v)
        this.props.onUpdate(v)
      }
    })
  }

  render() {
    const { companyInfo } = this.props
    const { getFieldDecorator } = this.props.form
    
    if(!companyInfo) {
      return null;
    }
    
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark={true} layout="vertical">
        <Layout style={settingsWrapper}>
          <Row>
            <Col md={5}>
              <Label>Business Logo</Label>
              <div style={{ border: '1px solid #ccc', width: 200, height: 200 }} />
            </Col>
            <Col md={7}>
              <Label>Business Details</Label>
              <Row>
                <Col xs={7}>
                  <InputLabel>Business Name</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('companyName', {
                      rules: [{ required: true, message: 'Business name is required!' }],
                      initialValue: companyInfo.companyName
                    })(<ThemeInput />)}
                  </FormItem>
                </Col>
                <Col xs={2} />
              </Row>
              <Row style={mt5}>
                <Col xs={7}>
                  <InputLabel>Phone</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('phone', {
                      rules: [
                        {
                          required: true,
                          message: 'Phone is required!'
                        },
                        {
                          pattern: /(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})/,
                          message: 'Invalid international phone number!'
                        }
                      ],
                      initialValue: companyInfo.phone
                    })(<ThemeInput />)}
                  </FormItem>      
                </Col>
                <Col xs={2} />
              </Row>
              <Row style={mt5}>
                <Col xs={7}>
                  <InputLabel>Fax</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('fax', {
                      rules: [{ required: true, message: 'Fax is required!' }],
                      initialValue: companyInfo.fax
                    })(<ThemeInput />)}
                  </FormItem>
                </Col>
                <Col xs={2} />
              </Row>
              <Row style={mt5}>
                <Col xs={7}>
                  <InputLabel>Email</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('email', {
                      rules: [
                        {
                          type: 'email',
                          message: 'Email is not valid!'
                        },
                        { 
                          required: true,
                          message: 'Email is required!' 
                        }
                      ],
                      initialValue: companyInfo.email
                    })(<ThemeInput />)}
                  </FormItem>
                </Col>
                <Col xs={2} />
              </Row>
              <Row style={mt5}>
                <Col xs={7}>
                  <InputLabel>Website</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('website', {
                      rules: [
                        {
                          type: 'url',
                          message: 'Website is not valid!'
                        },
                        { 
                          required: true,
                          message: 'Website is required!' 
                        }
                      ],
                      initialValue: companyInfo.website
                    })(<ThemeInput />)}
                  </FormItem>
                </Col>
                <Col xs={2} />
              </Row>
            </Col>
            <Col md={7}>
              <Label>Business Address</Label>
              <Row>
                <Col xs={7}>
                  <InputLabel>Street</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('address.street1', {
                      rules: [{ required: true, message: 'Street is required!' }],
                      initialValue: companyInfo.address && companyInfo.address.street1
                    })(<ThemeInput />)}
                  </FormItem>                  
                </Col>
                <Col xs={2} />
              </Row>
              <Row style={mt5}>
                <Col xs={7}>
                  <InputLabel>City</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('address.city', {
                      rules: [{ required: true, message: 'City is required!' }],
                      initialValue: companyInfo.address && companyInfo.address.city
                    })(<ThemeInput />)}
                  </FormItem>
                </Col>
                <Col xs={2} />
              </Row>
              <Row style={mt5}>
                <Col xs={7}>
                  <InputLabel>State</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('address.state', {
                      rules: [{ required: true, message: 'State is required!' }],
                      initialValue: companyInfo.address && companyInfo.address.state
                    })(<ThemeInput />)}
                  </FormItem>
                </Col>
                <Col xs={2} />
              </Row>
              <Row style={mt5}>
                <Col xs={7}>
                  <InputLabel>Zip</InputLabel>
                </Col>
                <Col xs={15}>
                  <FormItem style={mb0}>
                    {getFieldDecorator('address.zipcode', {
                      rules: [{ required: true, message: 'Zip is required!' }],
                      initialValue: companyInfo.address && companyInfo.address.zipcode
                    })(<ThemeInput />)}
                  </FormItem>
                </Col>
                <Col xs={2} />
              </Row>
            </Col>
          </Row>
          <Row style={{marginTop: 20}}>
            <Col>
              <ThemeButton shape="round" type="primary" htmlType="submit" style={{ width: 200, marginBottom: 20 }}>
                <Icon type="save" theme="filled" />
                Save Company Info
              </ThemeButton>
            </Col>
          </Row>
        </Layout>
      </Form>
    )
  }
}

export default Form.create<GeneralProps>()(General)