import * as React from 'react'
import { Layout, Row, Col, Icon, Popconfirm } from 'antd'
import { Label, productColStyle } from '../../settings.style'
import { ThemeInput, ThemeButton, ThemeIconButton } from '../../../customers/customers.style'
import { SettingsDispatchProps, SettingsProps } from '~/modules/settings/settings.module'
import EditableTable from '~/components/Table/editable-table'
import { cloneDeep } from 'lodash'
import { hasSpecialCharacter } from '~/common/utils'

type newType = {
  type: string
  name: any
}

type NewUomEditWrapper = SettingsDispatchProps &
  SettingsProps & {
    isModal?: boolean
    field: string
    title: string
    buttonTitle: string
    setNewType: (obj: newType) => void
  }

export default class NewUomEditWrapper extends React.PureComponent<NewUomEditWrapper> {
  state: any
  constructor(props: NewUomEditWrapper) {
    super(props)
    this.state = {
      dataSource: [],
      value: '',
      hasSpecialChar: false,
    }
  }

  componentWillReceiveProps(nextProps: NewUomEditWrapper) {
    console.log(nextProps.companyProductTypes)
    if (
      (this.props.companyProductTypes === null && nextProps.companyProductTypes !== null) ||
      this.props.companyProductTypes[this.props.field] !== nextProps.companyProductTypes[nextProps.field]
    ) {
      let data = cloneDeep(nextProps.companyProductTypes[nextProps.field])
      console.log(data)
      data = data.length ? data.sort((a: any, b: any) => a.name.localeCompare(b.name)) : []
      this.setState({
        dataSource: data,
      })
    }
  }

  componentDidMount() {
    this.props.getCompanyProductAllTypes()
  }

  handleAdd = () => {
    const { field } = this.props
    const { value } = this.state
    if (value !== '') {
      this.props.setCompanyProductType({
        type: field,
        name: value,
      })
      this.setState({
        value: '',
      })
      //after new type add, this function will inform the prop component
      this.props.setNewType({
        type: field,
        name: value,
      })
    }
  }

  onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const hasError = hasSpecialCharacter(e.target.value)
    this.setState({
      value: e.target.value,
      hasSpecialChar: hasError,
    })
  }

  onDeleteType = (id: string) => {
    this.props.deleteProductType(id)
    const dataSource = [...this.state.dataSource]
    let data = dataSource.filter((item) => item.id !== id)
    data = data.length ? data.sort((a: any, b: any) => a.name.localeCompare(b.name)) : []
    this.setState({ dataSource: data })
  }

  handleSave = (row: any) => {
    if (row.name === '') {
      return
    }

    this.props.updateProductType(row)
    const { field, companyProductTypes } = this.props
    const newData = [...companyProductTypes[field]]
    const index = newData.findIndex((item) => row.id === item.id)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    const data = newData.length ? newData.sort((a: any, b: any) => a.name.localeCompare(b.name)) : []
    this.setState({ dataSource: data })
  }

  render() {
    const { title, buttonTitle, isModal } = this.props
    const { value, dataSource } = this.state
    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        editable: true,
        key: 'index',
      },
      {
        title: '',
        dataIndex: 'id',
        width: 40,
        render: (id: string) => {
          return (
            <Popconfirm
              title={`Are you to delete this "${title}?"`}
              onConfirm={() => this.onDeleteType(id)}
              okText="Delete"
              cancelText="Cancel"
              okType="danger"
              placement="bottomRight"
            >
              <ThemeIconButton type="link" block style={{ border: 0 }}>
                <Icon type="delete" style={{ fontSize: 20 }} />
              </ThemeIconButton>
            </Popconfirm>
          )
        },
      },
    ]

    return (
      <Layout style={isModal === true ? { backgroundColor: 'transparent', marginBottom: -20 } : productColStyle}>
        {isModal !== true && (
          <Row>
            <Col>
              <Label style={{ textAlign: 'left' }}>{title}</Label>
            </Col>
          </Row>
        )}
        <Row>
          <Col xs={14}>
            <ThemeInput placeholder={`New ${title}`} value={value} onChange={this.onChangeInput} />
          </Col>
          <Col xs={10}>
            <ThemeButton
              shape="round"
              type="primary"
              onClick={this.handleAdd}
              style={{ marginLeft: 10 }}
              disabled={this.props.field === 'suppliers' && this.state.hasSpecialChar === true}
            >
              {buttonTitle}
            </ThemeButton>
          </Col>
        </Row>
        {this.props.field === 'suppliers' && (
          <Row>
            {this.state.hasSpecialChar === true && (
              <span style={{ color: 'red' }}>Name doesn't allow to use special characters</span>
            )}
          </Row>
        )}
      </Layout>
    )
  }
}
