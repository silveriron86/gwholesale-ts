import * as React from 'react'
import { SettingsModule, SettingsDispatchProps, SettingsProps } from '~/modules/settings/settings.module'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import NewUomEditWrapper from './NewUomEditWrapper'

/*
this is a cody from TypeEditor component, in case the debug code will affect other component,
this new component aim to build a new ' Create new unit of measurement' component according customer's requirements
*/

type NewUomEditorProps = SettingsDispatchProps & SettingsProps & {}

class NewUomEditor extends React.PureComponent<NewUomEditorProps> {
  render() {
    return <NewUomEditWrapper {...this.props} />
  }
}

const mapState = (state: GlobalState) => state.settings
export default connect(SettingsModule)(mapState)(NewUomEditor)
