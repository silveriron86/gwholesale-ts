import * as React from 'react'
import { SettingsModule, SettingsDispatchProps, SettingsProps } from '~/modules/settings/settings.module'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import TypeEditWrapper from './TypeEditWrapper'

type TypesEditorProps = SettingsDispatchProps & SettingsProps & {}

class TypesEditor extends React.PureComponent<TypesEditorProps> {
  render() {
    return (      
      <TypeEditWrapper {...this.props} />
    )
  }
}

const mapState = (state: GlobalState) => state.settings
export default connect(SettingsModule)(mapState)(TypesEditor)
