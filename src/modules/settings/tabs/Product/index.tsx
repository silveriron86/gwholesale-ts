import * as React from 'react'
import { Layout, Row, Col } from 'antd'
import { settingsWrapper } from '../../settings.style'
import TypesEditor from './TypesEditor'

interface ProductProps { }

export default class Product extends React.PureComponent<ProductProps> {
  render() {
    return (
      <Layout style={settingsWrapper}>
        <Row>
          <Col md={8} style={{ padding: 10 }}>
            <TypesEditor
              field="unitOfMeasure"
              title="Unit of Measure"
              buttonTitle="Add New UOM"
            />
          </Col>

          <Col md={8} style={{ padding: 10 }}>
            <TypesEditor
              field="returnReason"
              title="Return reason"
              buttonTitle="Add New Return reason"
            />
          </Col>

          <Col md={8} style={{ padding: 10 }}>
            <TypesEditor
              field="carrier"
              title="Carrier"
              buttonTitle="Add New Carrier"
            />
          </Col>
        </Row>

        <Row>
          <Col md={8} style={{ padding: 10 }}>
            <TypesEditor
              field="modifier"
              title="Modifier (origin)"
              buttonTitle="Add New Provider"
            />
          </Col>

          <Col md={8} style={{ padding: 10 }}>
            <TypesEditor
              field="costStructure"
              title="Cost strcuture"
              buttonTitle="Add New Cost structure"
            />
          </Col>

          <Col md={8} style={{ padding: 10 }}>
            <TypesEditor
              field="modeOfTransportation"
              title="Mode of transaportation"
              buttonTitle="Add New Mode"
            />
          </Col>
        </Row>
      </Layout>
    )
  }
}
