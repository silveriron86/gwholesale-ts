import * as React from 'react'
import { Layout, Row, Col } from 'antd'
import { settingsWrapper, InputLabel, Label } from './../settings.style'
import { ThemeInput } from '../../customers/customers.style'

interface ReceivingProps {}

export class Receiving extends React.PureComponent<ReceivingProps> {
  render() {
    return (
      <Layout style={settingsWrapper}>
        <Row>
          <Col>
            <Label style={{ textAlign: 'left' }}>Best By Date</Label>
          </Col>
        </Row>
        <Row>
          <Col>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <InputLabel>Received Date+</InputLabel>
              <ThemeInput style={{ width: 50, fontWeight: 'bold', fontSize: 16 }} value="14" />
              <InputLabel>Days</InputLabel>
            </div>
          </Col>
        </Row>
      </Layout>
    )
  }
}

export default Receiving
