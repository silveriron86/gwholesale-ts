import * as React from 'react'
import { Layout, Row, Col, Popover, notification } from 'antd'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'

import { settingsWrapper } from './../settings.style'
import { Payload } from '~/components/elements/routing_payload'
import { CardWrappperStyle } from '../../delivery/deliveries/tableview/tableview.style'
import { WEEKDAYS, WholesalePayload } from './../../../schema'
import { SettingsDispatchProps, SettingsModule, SettingsProps } from '../settings.module'
import { GlobalState } from '~/store/reducer'
import { Icon as IconSvg } from '~/components/icon/'
import { Clear, HeaderTitle, ThemeButton } from '~/modules/customers/customers.style'
import { PayloadModal } from '../components/payload-modal'

export type RoutingProps = SettingsDispatchProps & SettingsProps & {}

export class RoutingComponent extends React.PureComponent<RoutingProps> {
  readonly state = {
    newModalVisible: false,
  }
  componentDidMount() {
    this.props.getDriverSchedules()
    this.props.getAllDrivers()
    this.props.getAllRoutes()
  }

  render() {
    const { newModalVisible } = this.state
    const { payloads } = this.props

    return (

      <Layout style={settingsWrapper}>
        <HeaderTitle>Driver-Route Assignment</HeaderTitle>
        <Row>
          <Col>
            <div style={{ textAlign: 'right' }}>
              <Popover
                placement="left"
                content={this.showPayloadModal(null, newModalVisible)}
                trigger="click"
                visible={newModalVisible}
                onVisibleChange={this.handleVisibleChange}
              >
                <ThemeButton shape="round">
                  <IconSvg
                    type="plus-circle"
                    viewBox="0 0 20 28"
                    width={15}
                    height={14}
                  />&nbsp;&nbsp;Add Payload
                  </ThemeButton>
              </Popover>
              <Clear />
            </div>
          </Col>
        </Row>
        <div>
          {payloads &&
            <div style={CardWrappperStyle}>
              {this.renderPayloads()}
            </div>
          }
        </div>
      </Layout>
    )
  }

  showPayloadModal = (item: any, visible: boolean) => {
    return <PayloadModal data={null} save={this.save} cancel={this.cancel} />
  }

  save = (payload: any | null) => {
    const { payloads } = this.props;
    const dup_data = payloads.filter(el => el.driver.userId == payload.driverId
      && el.wholesaleRoute.id == payload.wholesaleRouteId
      && el.deliveryWeekOfDay == payload.deliveryWeekOfDay)
    // payload.scheduleOrder = '#' + (dup_data.length + 1)          //for now, we don't care schedule_order
    console.log(dup_data)
    if (dup_data.length == 0) {
      this.props.createPayload(payload)
    } else {
      notification.open({
        message: 'Payload Duplication',
        description: 'This payload is already existing. Please select again',
        onClick: () => {
          //console.log('Notification Clicked!')
        },
      })
    }
    this.cancel()
  }

  cancel = () => {
    this.setState({
      newModalVisible: false
    })
  }

  onDelete = (schedule_id: number) => {
    this.props.deletePayload(schedule_id)
  }

  handleVisibleChange = (visible: boolean) => {
    this.setState({
      newModalVisible: visible
    })
  }

  renderPayloads() {
    const rows = []
    for (let i = 0; i < 7; i++) {
      const columnData = this.getPayloadDataForDay(WEEKDAYS[i]);
      rows.push(<Payload key={i} data={columnData} delete={this.onDelete}>{WEEKDAYS[i]}</Payload>)
    }
    return rows;
  }

  getPayloadDataForDay(day: string) {
    const { payloads } = this.props
    const data: WholesalePayload[] = payloads.filter(el => el.deliveryWeekOfDay.toLocaleLowerCase() == day.toLocaleLowerCase());
    const result: any[] = data.map((el) => ({
      id: el.id,
      driver: el.driver ? el.driver.firstName + ' ' + el.driver.lastName : '',
      routeName: el.wholesaleRoute ? el.wholesaleRoute.routeName : ''
    }))

    return result;
  }
}

const mapStateToProps = (state: GlobalState) => state.settings

export const Routing = withTheme(connect(SettingsModule)(mapStateToProps)(RoutingComponent))
// export default Routing
