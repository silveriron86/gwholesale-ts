import { Select, Spin, Skeleton, Icon, Collapse, Card, Row, Col, Statistic } from 'antd'
import React from 'react'
import { CascaderOptionType } from 'antd/lib/cascader'
import {
  OptionsContainer,
  SelectBox,
  CardAndButtonGroupContainer,
  CardContainer,
  CardOption
} from '~/modules/statistics/statistics.style'
import { StatisticsItem } from '../statistics.module'
import { ThemeTable } from '~/modules/customers/customers.style'
import _ from 'lodash'
const { Panel } = Collapse
interface StatisticsContentProps {
  presets: any
  intervalsDate?: number
  onQueryParamsChange?: (params: any) => void
  dataSource?: StatisticsItem[],
  loading: boolean,
}

interface PresetsItem {
  key: number,
  label: String,
  from: number,
  to: number
}
export class StatisticsContent extends React.PureComponent<StatisticsContentProps> {
  state = {
    type: 1,
  }

  onIntervalsDateChange = (val: number) => {
    this.props.onQueryParamsChange?.(val)
  }

  onTypeChange = (type: number) => {
    this.setState({
      type,
    })
  }

  render() {
    const { presets, intervalsDate, dataSource, loading } = this.props

    return (
      <>
        <OptionsContainer>
          <SelectBox style={{ marginLeft: 0 }}>
            <div className="label">
              Intervals date
            </div>
            <Select
              style={{ width: 220 }}
              value={intervalsDate}
              onChange={this.onIntervalsDateChange}
            >
              {presets.map((item: PresetsItem) => (
                <Select.Option key={item.key} value={item.key}>
                  {item.label}
                </Select.Option>
              ))}
            </Select>
          </SelectBox>
        </OptionsContainer>
        <Row gutter={[16, 16]}>
          {dataSource?.map(v => {
            const statisticData = _.groupBy(v.statisDataList, 'activity')
            return (
              <Col span={6} key={v.companyId}>
                <Card title={v.companyName} loading={loading} hoverable>
                  <Row gutter={16} justify='space-around'>
                    <Col span={12}>
                      <Statistic title="Unique logins" value={_.get(statisticData, 'login[0].uniqueLoginCount', 0)} />
                    </Col>
                    <Col span={12}>
                      <Statistic title="Total logins" value={_.get(statisticData, 'login[0].totalCount', 0)} />
                    </Col>
                    <Col span={12}>
                      <Statistic title="Average number of logins" value={_.get(statisticData, 'login[0].averageCount', 0)} />
                    </Col>
                    <Col span={12}>
                      <Statistic title="sales orders created" value={_.get(statisticData, 'salesOrderCreation[0].totalCount', 0)} />
                    </Col>
                    <Col span={12}>
                      <Statistic title="Average number of sales orders" value={_.get(statisticData, 'salesOrderCreation[0].averageCount', 0)} />
                    </Col>
                    <Col span={12}>
                      <Statistic title="purchase orders created" value={_.get(statisticData, 'purchaseOrderCreation[0].totalCount', 0)} />
                    </Col>
                    <Col span={12}>
                      <Statistic title="Average number of purchase orders" value={_.get(statisticData, 'purchaseOrderCreation[0].averageCount', 0)} />
                    </Col>
                  </Row>
                </Card>
              </Col>
            )
          })}
        </Row>
      </>
    )
  }
}
