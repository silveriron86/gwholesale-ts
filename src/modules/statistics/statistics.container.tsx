import React, { Component } from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import PageLayout from '~/components/PageLayout'
import { StatisticsPageContainer } from '~/modules/statistics/statistics.style'
import { StatisticsContent } from './components/StatisticsContent'
import moment from 'moment'
import { QueryParams } from './statistics.service'
import _ from 'lodash'
import { StatisticsModule, StatisticsDispatchProps, StatisticsStateProps } from './statistics.module'
import { AuthUser } from '~/schema'
import { RouteContentComponent } from '../delivery/deliveries/listview/route-content'
const presets = [
  {
    key: 0,
    label: 'Past 7 Days',
    from: moment().subtract(7, 'days').format('MM/DD/YYYY'),
    to: moment().format('MM/DD/YYYY'),
  },
  {
    key: 1,
    label: 'Past 30 Days',
    from: moment().subtract(30, 'days').format('MM/DD/YYYY'),
    to: moment().format('MM/DD/YYYY'),
  },
  {
    key: 9,
    label: 'Yesterday',
    from: moment().subtract(1, 'days').format('MM/DD/YYYY'),
    to: moment().subtract(1, 'days').format('MM/DD/YYYY'),
  },
  {
    key: 2,
    label: 'Today',
    from: moment().format('MM/DD/YYYY'),
    to: moment().format('MM/DD/YYYY'),
  },
]

type StatisticsContainerProps = StatisticsDispatchProps & StatisticsStateProps & RouteContentComponent & {
  currentUser: AuthUser
}

export class StatisticsContainer extends Component<StatisticsContainerProps> {
  state = {
    intervalsDate: 0,
  }
  
  onQueryParamsChange = (params:number) => {
    this.setState({
      intervalsDate: params,
    }, () => {
      this.onSearch()
    })
  }
  onSearch = () => {
    this.props.resetLoading()
    const selectedParam = presets.find(item => item.key === this.state.intervalsDate)
    const params: QueryParams = {
      to: selectedParam?.to.toString(),
      from: selectedParam?.from.toString(),
    }
    this.props.fetchDataSource(params)
  }
  componentDidMount() {
    this.onSearch()
  }
  render() {
    const { statisticsInfos, loading } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Inventory-Restock'}>
        <StatisticsPageContainer>
          <StatisticsContent
            presets={presets}
            onQueryParamsChange={this.onQueryParamsChange}
            intervalsDate={this.state.intervalsDate}
            dataSource={statisticsInfos}
            loading={loading}
          />
        </StatisticsPageContainer>
      </PageLayout>
    )
  }
}

export default Statistics

const mapStateToProps = (state: GlobalState) => ({ ...state.statistics, currentUser: state.currentUser })

export const Statistics = withTheme(connect(StatisticsModule)(mapStateToProps)(StatisticsContainer))

