import {
  DefineAction,
  Effect,
  EffectModule,
  Module,
  ModuleDispatchProps,
  Reducer
} from 'redux-epics-decorator'
import { Observable } from 'rxjs'
import { StatisticsService } from './statistics.service'
import { Action } from 'redux-actions'
import { map, switchMap } from 'rxjs/operators'


export interface StatisticsItem {
  companyId: number
  companyName: string,
  statisDataList: any[]
}

export interface StatisticsStateProps {
  statisticsInfos: StatisticsItem[],
  loading: boolean,
}

@Module('statistics')
export class StatisticsModule extends EffectModule<StatisticsStateProps> {
  defaultState = {
    statisticsInfos: [],
    loading: false
  }
  @DefineAction() dispose$!: Observable<void>

  constructor(
    private readonly statistics: StatisticsService,
  ) {
    super()
  }

  @Effect({
    done: (state: StatisticsStateProps, { payload }: Action<any>): StatisticsStateProps => {
      return {
        ...state,
        statisticsInfos: payload?.body || {},
        loading: false,
      }
    },
  })
  fetchDataSource(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) => this.statistics.fetchData(data)),
      map(this.createAction('done'))
    )
  }

  @Reducer()
  resetLoading(state: StatisticsStateProps) {
    return {
      ...state,
      loading: true,
    }
  }

}

export type StatisticsDispatchProps = ModuleDispatchProps<StatisticsModule>