import { Injectable } from 'redux-epics-decorator'
import { Http } from '~/common'
import { StatisticsItem } from '~/modules/statistics/statistics.module'

export interface QueryParams {
  from?: string
  to?: string
}

@Injectable()
export class StatisticsService {
  constructor(private readonly http: Http) {}

  static get instance() {
    return new StatisticsService(new Http())
  }

  fetchData(params: QueryParams) {
    const search = new URLSearchParams()
    Object.keys(params).forEach((key) => {
      if (key !== 'from' && key !== 'to') {
        params[key] != null && search.append(key, params[key])
      }
    })
    if (params.from != null && params.to != null) {
      search.append('from', params.from.toString())
      search.append('to', params.to.toString())
    }
    return this.http.get<{ statisticsInfo: StatisticsItem[] }>(`/report/customer-activity?${search.toString()}`)
  }
}