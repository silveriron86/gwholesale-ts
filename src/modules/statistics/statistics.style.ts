import styled from "@emotion/styled";
/**
 * container
 */
export const StatisticsPageContainer = styled.div`
  width: 100%;
  text-align: left;
  box-sizing: border-box;
  padding: 30px 70px;
`;

/**
 * header
 */

export const OptionsContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-bottom: 20px;
`;

export const SelectBox = styled.div`
  flex: 1;

  &:not(:first-child) {
    margin-left: 20px;
  }

  .label {
    margin-bottom: 10px;
  }
`;

export const CardAndButtonGroupContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

export const CardContainer = styled.div`
  display: flex;
  margin-top: 30px;
`;

export const CardOption = styled.div<{ selected?: boolean }>`
  padding: 18px;
  border: ${props => props.selected ? props.theme.primary + " 2px solid" : "rgb(217, 217, 217) 1px solid"};
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  //cursor: pointer;
  //box-sizing: border-box;
  overflow: hidden;
  position: relative;

  &::after {
    content: "";
    display: block;
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    background-color: ${props => props.selected ? props.theme.primary : "transparent"};
    opacity: 0.1;
    pointer-events: none;
  }

  &:not(:first-child) {
    margin-left: 30px;
  }

  span.title {
    display: block;
    font-size: 16px;
  }

  & > div {
    width: 100%;
    padding-top: 10px;
    display: flex;
    align-items: flex-end;

    span.content {
      font-size: 20px;
    }

    span.clear {
      visibility: ${props => props.selected ? "visible" : "hidden"};
      margin-left: 30px;
      font-size: 16px;
      color: ${props => props.theme.primary};
      cursor: pointer;
    }
  }

`;
