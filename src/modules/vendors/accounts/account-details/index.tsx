import React from 'react'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { Theme } from '~/common'
import {
  CustomersInfoHeader,
  CustomersInfoBody,
} from '~/modules/customers/components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import DetailsForm from '~/modules/customers/accounts/account-details/details-form'
import DetailsOverview from '~/modules/customers/components/details-overview'
import { VendorsModule, VendorsDispatchProps, VendorsStateProps } from '~/modules/vendors/vendors.module'
import { DetailsWrapper, DetailsTitle } from '~/modules/customers/customers.style'
import _ from 'lodash'
import { RouteComponentProps } from 'react-router'

type VendorAccountDetailsProps = VendorsDispatchProps &
  VendorsStateProps &
  RouteComponentProps<{ vendorId: string }> & {
    theme: Theme
  }

export class VendorAccountDetails extends React.PureComponent<VendorAccountDetailsProps> {
  state = {
    companyProductTypes: null
  }

  componentDidMount() {
    const vendorId = this.props.match.params.vendorId
    this.props.getVendor(vendorId)
    this.props.getCompanyUsers();
    this.props.getCompanyProductAllTypes()
    this.setState({ companyProductTypes: this.props.companyProductTypes })
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.companyProductTypes) != JSON.stringify(nextProps.companyProductTypes)) {
      this.setState({ companyProductTypes: nextProps.companyProductTypes })
    }
  }

  onSave = (data: any) => {
    // tslint:disable-next-line:no-console
    console.log(data)
    this.props.updateCurrentCustomer(data)
  }

  render() {
    const { vendor } = this.props
    const { companyProductTypes } = this.state
    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Vendors'}>
        <CustomersInfoHeader>
          <DetailsOverview customer={vendor} />
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <DetailsWrapper>
            <DetailsTitle>Account Details</DetailsTitle>
            {!_.isEmpty(vendor) && <DetailsForm customer={vendor} companyProductTypes={companyProductTypes} onSave={this.onSave} {...this.props} />}
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.vendors
export default withTheme(connect(VendorsModule)(mapState)(VendorAccountDetails))
