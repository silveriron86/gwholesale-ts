import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import {
  CustomersInfoHeader,
  CustomersInfoBody,
  Flex,
} from '~/modules/customers/components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import DetailsOverview from '~/modules/customers/components/details-overview'
import AddressesContainer from '~/modules/customers/accounts/addresses/addresses-container'
import { VendorsModule, VendorsDispatchProps, VendorsStateProps } from '~/modules/vendors/vendors.module'
import { MainAddress } from '~/schema'

type AddressesProps = VendorsDispatchProps &
  VendorsStateProps & {
    theme: Theme
  }

export class VendorAddresses extends React.PureComponent<AddressesProps> {
  componentDidMount() {
    const vendorId = this.props.match.params.vendorId
    this.props.getVendor(vendorId)
    this.props.getVendorAddresses(vendorId)
    this.props.getAllRoutes()
  }

  onSave = (data: MainAddress) => {
    // tslint:disable-next-line:no-console
    console.log(data)
    if (data.wholesaleAddressId) {
      // update
      this.props.updateVendorAddress(data)
    } else {
      // create
      this.props.createVendorAddress(data)
    }
  }

  render() {
    const { vendor, addresses, routes } = this.props
    const billingAddresses: MainAddress[] = []
    const shippingAddresses: MainAddress[] = []
    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        shippingAddresses.push(address)
      } else {
        billingAddresses.push(address)
      }
    })

    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Vendors'}>
        <Flex style={{ flexDirection: 'column' }}>
          <CustomersInfoHeader>
            <DetailsOverview customer={vendor} />
          </CustomersInfoHeader>
          <CustomersInfoBody style={{ flex: 1 }}>
            <AddressesContainer
              {...this.props}
              routes={routes}
              billingAddresses={billingAddresses}
              shippingAddresses={shippingAddresses}
              from="purchase"
              onSave={this.onSave}
            />
          </CustomersInfoBody>
        </Flex>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.vendors
export default withTheme(connect(VendorsModule)(mapState)(VendorAddresses))
