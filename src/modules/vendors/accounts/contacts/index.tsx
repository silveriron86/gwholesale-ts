import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import {
  CustomersInfoHeader,
  CustomersInfoBody,
} from '~/modules/customers/components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import DetailsOverview from '~/modules/customers/components/details-overview'
import ContactsContainer from '~/modules/customers/accounts/contacts/contacts-container'
import { VendorsModule, VendorsDispatchProps, VendorsStateProps } from '~/modules/vendors/vendors.module'
import { MainContact } from '~/schema'

type VendorContactsProps = VendorsDispatchProps &
  VendorsStateProps & {
    theme: Theme
  }

export class VendorContacts extends React.PureComponent<VendorContactsProps> {
  editItem = (item: MainContact) => {}

  componentDidMount() {
    const vendorId = this.props.match.params.vendorId
    this.props.getVendor(vendorId)
    this.props.getVendorContacts(vendorId)
  }

  onSave = (contact: MainContact) => {
    console.log(contact)
    if (contact.contactId) {
      // update
      this.props.updateVendorContact(contact)
    } else {
      // create
      const { vendor} = this.props
      // contact.wholesaleCompany = customer.wholesaleCompany
      // contact.user = null
      // contact.userRole = null
      contact.clientId = vendor.clientId
      this.props.createVendorContact(contact)
    }
  }

  onSaveAsMain = (contact: MainContact) => {
    this.onSave(contact)
    this.props.setVendorMainContact(contact.contactId)
  }

  render() {
    const { vendor, contacts } = this.props
    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Vendors'}>
        <CustomersInfoHeader>
          <DetailsOverview customer={vendor} />
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <ContactsContainer
            contacts={contacts}
            editItem={this.editItem}
            search={''}
            onSave={this.onSave}
            onSaveAsMain={this.onSaveAsMain}
          />
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.vendors
export default withTheme(connect(VendorsModule)(mapState)(VendorContacts))
