import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import {
  CustomersInfoHeader,
  CustomersInfoBody,
} from '~/modules/customers/components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import DetailsOverview from '~/modules/customers/components/details-overview'
import DocumentsContainer from '~/modules/customers/accounts/documents/documents-container'
import { VendorsModule, VendorsDispatchProps, VendorsStateProps } from '~/modules/vendors/vendors.module'
import { notification } from 'antd'
import moment from 'moment'

type VendorDocumentsProps = VendorsDispatchProps &
  VendorsStateProps & {
    theme: Theme
  }

export class VendorDocuments extends React.PureComponent<VendorDocumentsProps> {
  state = {
    from: moment().subtract(30, 'days'),
    to: moment(),
    curPage: 0,
    pageSize: 10,
    search: ''
  }

  componentDidMount() {
    const vendorId = this.props.match.params.vendorId
    this.props.getVendor(vendorId)
    this.props.resetDocuments()
    this.getVendorDocument()
  }

  getVendorDocument = () => {
    const vendorId = this.props.match.params.vendorId
    const searchObj = {
      // from: this.state.from.format('MM/DD/YYYY'),
      // to: this.state.to.format('MM/DD/YYYY'),
      search: this.state.search,
      // curPage: this.state.curPage,
      // pageSize: this.state.pageSize,
    }
    this.props.setDocumentSearchProps(searchObj)
    this.props.getDocuments({ ...searchObj, vendorId: vendorId })
  }


  onChange = (type: string, data: any) => {
    if (type == 'search') {
      this.setState({ search: data }, this.getVendorDocument)
    }
  }

  componentWillReceiveProps(nextProps: VendorDocumentsProps) {
    if (nextProps.message !== this.props.message) {
      if (nextProps.type != null) {
        notification[nextProps.type!]({
          message: nextProps.type!.toLocaleUpperCase(),
          description: nextProps.message,
          onClose: nextProps.resetNotif,
          duration: nextProps.type === 'success' ? 5: 4.5
        })
      }
    }
  }

  onSave = (data: Document) => {
    if (data.id) {
      this.props.updateDocument(data)
    } else {
      this.props.createDocument(data)
    }
  }

  render() {
    const { vendor, documents, documentsLoading } = this.props
    const { pageSize } = this.state
    // tslint:disable-next-line:no-console
    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Vendors'}>
        <CustomersInfoHeader>
          <DetailsOverview customer={vendor} />
        </CustomersInfoHeader>
        <CustomersInfoBody>
          <DocumentsContainer
            loading={documentsLoading}
            getDocuments={this.getVendorDocument}
            documents={documents}
            onSave={this.onSave}
            pageSize={pageSize}
            theme={this.props.theme} />
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.vendors
export default withTheme(connect(VendorsModule)(mapState)(VendorDocuments))
