import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { CACHED_NS_LINKED, Theme } from '~/common'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import {
  CustomersInfoHeader,
  CustomersInfoBody,
  Flex,
  layoutStyle,
} from '~/modules/customers/components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import DetailsOverview from '~/modules/customers/components/details-overview'
import { VendorsModule, VendorsDispatchProps, VendorsStateProps } from '~/modules/vendors/vendors.module'
import { MainAddress } from '~/schema'
import { DetailsTitle, DetailsWrapper, InputLabel, ThemeButton, ThemeInput, ThemeModal, ThemeSelect } from '~/modules/customers/customers.style'
import { Layout, Row, Col, Form, Icon, Select, Divider } from 'antd'
import { FormComponentProps } from 'antd/lib/form'
import { FINANCIAL_TERMS } from '~/common'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'

type Props = VendorsDispatchProps &
  VendorsStateProps & {
    theme: Theme
  }

interface FormFields {
  creditLimit: string
}


export class VendorFinancialDetail extends React.PureComponent<FormComponentProps<FormFields> & Props> {
  state = {
    visiblePaymentTermsModal: false,
    companyProductTypes: null
  }
  componentDidMount() {
    const vendorId = this.props.match.params.vendorId
    this.props.getVendor(vendorId)
    this.props.getCompanyProductAllTypes()
    this.setState({ companyProductTypes: this.props.companyProductTypes })
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.companyProductTypes) != JSON.stringify(nextProps.companyProductTypes)) {
      this.setState({ companyProductTypes: nextProps.companyProductTypes })
    }
  }

  handleSubmit = (e: any) => {
    const { form } = this.props
    e.preventDefault()

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values)
        this.props.updateCurrentCustomer(values)
      }
    })
  }

  openCompanySettingChange = (type: string) => {
    let state = { ...this.state }
    state[type] = true
    this.setState(state)
  }

  onCompanySettingTypeModal = (type: string) => {
    let state = { ...this.state }
    state[type] = !this.state[type]
    this.setState(state)
    this.props.getCompanyProductAllTypes()
  }
  render() {
    const { vendor, addresses, routes, form: { getFieldDecorator } } = this.props
    const { visiblePaymentTermsModal, companyProductTypes } = this.state

    const billingAddresses: MainAddress[] = []
    const shippingAddresses: MainAddress[] = []
    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        shippingAddresses.push(address)
      } else {
        billingAddresses.push(address)
      }
    })

    const nsEnabled = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const paymentTermsList = nsEnabled
      ? FINANCIAL_TERMS.map((name: string) => {
        return {name}
      })
    : companyProductTypes
    ? [...companyProductTypes.paymentTerms, ...companyProductTypes.paymentTermsFixedTypes]
    : []

    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Vendors'}>
        <Flex style={{ flexDirection: 'column' }}>
          <CustomersInfoHeader>
            <DetailsOverview customer={vendor} />
          </CustomersInfoHeader>
          <CustomersInfoBody style={{ flex: 1 }}>
            <DetailsWrapper>
              <DetailsTitle>Financial Details</DetailsTitle>
              <Layout style={layoutStyle}>
                <Form onSubmit={this.handleSubmit}>
                  <Row >
                    <Col lg={6} md={12}>
                      <InputLabel>PAYMENT TERMS</InputLabel>
                      <Form.Item>
                        {getFieldDecorator('paymentTerm', {
                          rules: [{ required: false }],
                          initialValue: vendor ? vendor.paymentTerm : ''
                        })(
                          <ThemeSelect
                            style={{ width: '100%' }}
                            placeholder="Payment Terms"
                            showSearch
                            optionFilterProp="children"
                            onSearch={() => {}}
                            filterOption={(input: any, option: any) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                            dropdownRender={(menu: any) => (
                              <div>
                                {menu}
                                {!nsEnabled && (
                                  <>
                                    <Divider style={{ margin: '4px 0' }} />
                                    <div
                                      style={{ padding: '4px 8px', cursor: 'pointer' }}
                                      onMouseDown={(e) => e.preventDefault()}
                                      onClick={(e) => this.openCompanySettingChange('visiblePaymentTermsModal')}
                                    >
                                      <Icon type="plus" /> Add New Payment Terms...
                                    </div>
                                  </>
                                )}
                              </div>
                            )}
                          >
                            {paymentTermsList.map((item) => (
                              <Select.Option key={`customer-term-${item.name}`} value={item.name}>
                                {item.name}
                              </Select.Option>
                            ))}
                          </ThemeSelect>
                        )}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <ThemeButton shape="round" type="primary" htmlType="submit" style={{ width: 200, marginBottom: 20 }}>
                        <Icon type="save" theme="filled" />
                        Save Financial Detail
                      </ThemeButton>
                    </Col>
                  </Row>
                </Form>
              </Layout>
              <ThemeModal
                title={`Edit Value List "Payment Terms"`}
                visible={visiblePaymentTermsModal}
                onCancel={this.onCompanySettingTypeModal.bind(this, 'visiblePaymentTermsModal')}
                cancelText='Close'
                okButtonProps={{ style: { display: 'none' } }}
              >
                <TypesEditor
                  isModal={true}
                  field="paymentTerms"
                  title="Payment Terms"
                  buttonTitle="Add New Payment Terms"
                />
              </ThemeModal>
            </DetailsWrapper>
          </CustomersInfoBody>
        </Flex>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.vendors
export default withTheme(connect(VendorsModule)(mapState)(Form.create()(VendorFinancialDetail)))
