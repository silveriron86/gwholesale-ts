import { Button, Input, Modal, notification } from 'antd'
import { DialogFooter, DialogHeader } from '~/modules/pricesheet/components/pricesheet-detail-header.style'
import { PriceSheetItem, PriceSheetItemNew, SaleItem } from '~/schema'
import { QBOImage, tableCss } from '~/modules/pricesheet/components/pricesheet-detail-table.style'

import { ColumnProps } from 'antd/es/table'
import { MutiSortTable } from '~/components'
import React from 'react'
import { Theme } from '~/common'
import { cloneDeep } from 'lodash'
import organicImg from '~/modules/inventory/components/organic.png'
import { searchButton } from '~/components/elements/search-button'

export interface AddProductModalProps {
  saleItems: SaleItem[]
  priceSheetItems: any[]
  module?: string
}

export class AddProductModal extends React.PureComponent<
  AddProductModalProps & {
    visible: boolean
    onOk: (keys: string[]) => void
    onCancel: () => void
    theme: Theme
  },
  {
    search: string
    selectedRowKeys: string[]
  }
> {
  state = {
    search: '',
    selectedRowKeys: [],
  }

  private mutiSortTable = React.createRef<any>()
  private searchRef = React.createRef<any>()

  onSearch = (search: string) => {
    this.setState({
      search: search,
    })
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onSortVariety = (a: any, b: any) => {
    const stringA = a.variety ? a.variety : ''
    const stringB = b.variety ? b.variety : ''
    return stringA.localeCompare(stringB)
  }

  handleOk = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault()
    const { selectedRowKeys } = this.state
    if (!selectedRowKeys.length) {
      return notification.warn({
        message: 'Select one at least',
      })
    }
    this.props.onOk(selectedRowKeys)
    this.searchRef.current.input.state.value = ''
    this.setState({
      selectedRowKeys: [],
      search: '',
    })
  }

  render() {
    const { search } = this.state
    const { saleItems, priceSheetItems, module } = this.props

    let listItems = cloneDeep(saleItems)

    listItems = listItems.filter((item) => {
      if(module === 'setting') {
        return priceSheetItems.every((id) => id != item.itemId)
      } else {
        return priceSheetItems.every((priceItem) => priceItem.wholesaleItem.wholesaleItemId !== item.itemId)
      }
    })

    if (search) {
      if (listItems) {
        let matches1: any[] = []
        let matches2: any[] = []
        let matches3: any[] = []
        listItems.forEach((row, index) => {
          const searchStr = search.toLowerCase()
          if (typeof row.SKU === 'undefined' && row.sku) {
            row.SKU = row.sku
          }
          if (row.SKU && row.SKU.toLowerCase().indexOf(searchStr) === 0) {
            // full match on 2nd filed
            matches2.push(row)
          } else if (row.variety && row.variety.toLowerCase().indexOf(searchStr) === 0) {
            // full match on variety
            matches1.push(row)
          } else if (
            (row.variety && row.variety.toLowerCase().indexOf(searchStr) > 0) ||
            (row.SKU && row.SKU.toLowerCase().indexOf(searchStr) > 0)
          ) {
            // others
            matches3.push(row)
          }
        })

        if (matches1) {
          matches1 = matches1.sort((a, b) => a.variety.localeCompare(b.variety))
        }
        if (matches2) {
          matches2 = matches2.sort((a, b) => a.SKU.localeCompare(b.SKU))
        }

        listItems = [...matches2, ...matches1, ...matches3]
      }
    }

    const columns: ColumnProps<SaleItem>[] = [
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        width: '15%',
        sorter: (a, b) => {
          return this.onSortString('SKU', a, b)
        },
      },
      {
        title: 'CATEGORY',
        dataIndex: 'wholesaleCategory.name',
        key: 'wholesaleCategory.name',
        sorter: (a, b) => {
          return this.onSortString('name', a.wholesaleCategory, b.wholesaleCategory)
        },
      },
      {
        title: 'NAME',
        dataIndex: 'variety',
        key: 'variety',
        width: '25%',
        sorter: (a, b) => {
          return this.onSortVariety(a, b)
        },
        render(text, item) {
          return item.organic == true ? (
            <>
              <QBOImage src={organicImg} /> <span>{text}</span>
            </>
          ) : (
            <span>{text}</span>
          )
        },
      },
      {
        title: 'SIZE',
        dataIndex: 'size',
        key: 'size',
        sorter: (a, b) => {
          return this.onSortString('size', a, b)
        },
      },
      {
        title: 'WEIGHT',
        dataIndex: 'weight',
        key: 'weight',
        sorter: (a, b) => {
          return this.onSortString('weight', a, b)
        },
      },
      {
        title: 'GRADE',
        dataIndex: 'grade',
        key: 'grade',
        sorter: (a, b) => {
          return this.onSortString('grade', a, b)
        },
      },
      {
        title: 'PACKING',
        dataIndex: 'packing',
        key: 'packing',
        sorter: (a, b) => {
          return this.onSortString('packing', a, b)
        },
      },
      {
        title: 'ORIGIN',
        dataIndex: 'origin',
        key: 'origin',
        sorter: (a, b) => {
          return this.onSortString('origin', a, b)
        },
      },
    ]

    const rowSelection = {
      selectedRowKeys: this.state.selectedRowKeys,
      onChange: (selectedRowKeys: any) => {
        this.setState({ selectedRowKeys })
      },
    }

    return (
      <Modal
        width={'92%'}
        bodyStyle={{ padding: '0' }}
        visible={this.props.visible}
        onCancel={this.props.onCancel}
        footer={[
          <DialogFooter key="footer">
            <Button
              type="primary"
              icon="plus"
              style={{
                border: `1px solid ${this.props.theme.primary}`,
                backgroundColor: this.props.theme.primary,
              }}
              onClick={this.handleOk}
            >
              Add Items
            </Button>
            <Button onClick={this.props.onCancel}>CANCEL</Button>
          </DialogFooter>,
        ]}
      >
        <DialogHeader style={{ alignItems: 'center' }}>
          Add items to product list
          <Input.Search
            ref={this.searchRef}
            placeholder="Search SKU or item name"
            size="large"
            style={{ width: '557px', border: '0' }}
            onSearch={this.onSearch}
            enterButton={searchButton}
          />
        </DialogHeader>
        <MutiSortTable
          ref={this.mutiSortTable}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={listItems}
          bordered={false}
          rowKey="itemId"
          css={tableCss}
        />
      </Modal>
    )
  }
}
