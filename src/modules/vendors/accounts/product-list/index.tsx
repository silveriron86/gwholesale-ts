import { Icon, Table, Input, notification } from 'antd'
import { Theme, mediumGrey } from '~/common'
import { VendorsDispatchProps, VendorsModule, VendorsStateProps } from '~/modules/vendors/vendors.module'

import { AddProductModal } from './add-product-modal'
import { GlobalState } from '~/store/reducer'
import PageLayout from '~/components/PageLayout'
import React from 'react'
import { RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import { ThemeButton } from '~/modules/customers/customers.style'
import { connect } from 'redux-epics-decorator'
import styled from '@emotion/styled'
import { withTheme } from 'emotion-theming'

const StyledWrap = styled.div`
  text-align: left;
  padding: 20px 24px;
`

const StyledDiv = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 25px 20px 16px;
  align-items: flex-end;
  color: ${(props) => props.theme.primary};
  cursor: pointer;

  .anticon-delete {
    margin-right: 6px;
    font-size: 1.2em;
  }
`

const InfoSpan = styled('div')({
  fontSize: '12px',
  fontWeight: 700,
  textTransform: 'uppercase',
  color: mediumGrey,
  marginRight: '18px',
})

const Name = styled('div')({
  color: mediumGrey,
  fontSize: '36px',
  fontWeight: 700,
  lineHeight: '52px',
  letterSpacing: '0.05em',
})

type ProductListProps = VendorsDispatchProps &
  VendorsStateProps &
  RouteComponentProps<{ vendorId: string }> & {
    theme: Theme
  }

type StateType = {
  showModal: boolean
  selectedRowKeys: string[]
}

class ProductList extends React.Component<ProductListProps, StateType> {
  vendorId = this.props.match.params.vendorId

  state = {
    showModal: false,
    selectedRowKeys: [] as string[],
  }

  columns = [
    {
      title: 'Item',
      dataIndex: 'wholesaleItem.variety',
      sorter: (a: any, b: any) => {
        const aValue: string = a?.wholesaleItem?.variety || ''
        const bValue: string = b?.wholesaleItem?.variety || ''
        return aValue.localeCompare(bValue)
      },
      width: 300,
      render: (val: string, row: any) => (
        <Link to={`/product/${row.wholesaleItem.wholesaleItemId}/specifications`}>{val}</Link>
      ),
    },
    {
      title: 'SKU',
      dataIndex: 'wholesaleItem.sku',
      width: 200,
      sorter: (a: any, b: any) => {
        const aValue: string = a?.wholesaleItem?.sku || ''
        const bValue: string = b?.wholesaleItem?.sku || ''
        return aValue.localeCompare(bValue)
      },
    },
    {
      title: 'Vendor product code',
      dataIndex: 'vendorProductCode',
      width: 200,
      render: (val, row) => (
        <Input
          style={{ width: 200 }}
          defaultValue={val}
          onBlur={(e) => {
            const newVal = e.target.value
            if (newVal === val) return
            this.updatePirceSheetItem(
              {
                priceSheetItemId: row.priceSheetItemId,
                vendorProductCode: newVal,
              },
              e.nativeEvent.target,
              val,
            )
          }}
          maxLength={100}
        />
      ),
    },
    {
      title: 'Custom field 1',
      dataIndex: 'customField1',
      // width: 200,
      render: (val, row) => (
        <Input
          style={{ width: 200 }}
          defaultValue={val}
          onBlur={(e) => {
            const newVal = e.target.value
            if (newVal === val) return
            this.updatePirceSheetItem(
              {
                priceSheetItemId: row.priceSheetItemId,
                customField1: newVal,
              },
              e.nativeEvent.target,
              val,
            )
          }}
          maxLength={100}
        />
      ),
    },
  ]

  componentDidMount() {
    this.props.getVendor(this.vendorId)
    this.props.getSaleItems()
    this.props.getProductListId(this.vendorId)
  }

  updatePirceSheetItem = (data: any, target: any, originVal: any) => {
    this.props.updatePirceSheetItem({
      ...data,
      callback: (res: any) => {
        if (!res || !res.body || res.statusCodeValue !== 200) {
          notification.error({
            message: 'ERROR',
            description: 'Fail to update product',
          })
          target.focus()
          setTimeout(() => {
            target.value = originVal
          }, 0)
          // setTimeout(() => {
          //   target.value = originVal
          // }, 100)
          // this.props.getProductList(this.props.productListId)
        }
      },
    })
  }

  onToggleModal = () => {
    this.setState((pre) => ({ ...pre, showModal: !pre.showModal }))
  }

  onChange = (selectedRowKeys: string[]) => {
    this.setState((pre) => ({ ...pre, selectedRowKeys }))
  }

  onAddItems = (addedItems: string[]) => {
    const { productListId } = this.props
    this.props.addPriceSheetItems({
      priceSheetId: productListId,
      assignItemListId: addedItems,
    })
    this.onToggleModal()
  }

  onRemoveItems = () => {
    const { productList, productListId } = this.props
    const { selectedRowKeys } = this.state
    const newList = productList
      .map((item) => item.wholesaleItem.wholesaleItemId)
      .filter((wholesaleItemId) => !selectedRowKeys.includes(wholesaleItemId))

    this.props.assignPriceSheetItems({
      priceSheetId: productListId,
      assignItemListId: newList,
    })
    this.setState((pre) => ({ ...pre, selectedRowKeys: [] }))
  }

  render() {
    const { saleItems, theme, vendor, productList, productListLoading } = this.props
    const { showModal, selectedRowKeys } = this.state

    return (
      <PageLayout currentTopMenu="">
        <StyledWrap>
          <div>
            <InfoSpan>VENDOR NAME</InfoSpan>
            <Name>{vendor?.clientCompany?.companyName}</Name>
          </div>
          <StyledDiv>
            {selectedRowKeys.length > 0 ? (
              <span onClick={this.onRemoveItems}>
                <Icon type="delete" />
                Remove item(s)
              </span>
            ) : (
              <span />
            )}
            <ThemeButton onClick={this.onToggleModal} type="primary">
              Add item
            </ThemeButton>
          </StyledDiv>
          <Table
            loading={productListLoading}
            rowSelection={{
              selectedRowKeys,
              onChange: this.onChange,
            }}
            pagination={{}}
            columns={this.columns}
            rowKey={(row) => row.wholesaleItem.wholesaleItemId}
            dataSource={productList}
          />
        </StyledWrap>
        <AddProductModal
          theme={theme}
          visible={showModal}
          onCancel={this.onToggleModal}
          onOk={this.onAddItems}
          saleItems={saleItems}
          priceSheetItems={productList}
        />
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => {
  const { vendors } = state
  const { vendor, saleItems, productListId, productList, productListLoading } = vendors
  return { vendor, saleItems, productListId, productList, productListLoading }
}
export default withTheme(connect(VendorsModule)(mapState)(ProductList))
