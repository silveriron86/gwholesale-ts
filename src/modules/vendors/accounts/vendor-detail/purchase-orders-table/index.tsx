import React from 'react'
import { ColumnProps } from 'antd/es/table'
import update from 'immutability-helper'
import { cloneDeep } from 'lodash'
import { RouteProps } from 'react-router'
import { MutiSortTable } from '~/components'
import { TempOrder } from '~/schema'
import { formatDate, formatNumber, getOrderPrefix } from '~/common/utils'
import { ThemeTable, ThemeModal, ThemeLink, ThemeIcon, ThemeIconButton } from '~/modules/customers/customers.style'
import { History } from 'history'
import { EmptyTr, tableCss } from '~/modules/customers/components/sales-orders-table/table.style'
import { MultiBackSortTable } from '~/components/multi-back-sortable'
import { Icon as IconSvg } from '~/components/icon';
import { Tooltip } from 'antd'
import moment from 'moment'
import { OrderService } from '~/modules/orders/order.service'
import { checkError } from '~/common/utils'


export type PurchaseOrdersTableProps = RouteProps & {
  columns: TempOrder[]
  search: string
  showDrawer: boolean
  loading: boolean
  history: History
  pageSize: any
  total: any
  changePage: (page: any, filter: any, sorter: any) => void
  currentPage: number
  sellerSetting?: any
  duplicateOrder: Function
}

const emtpyNode = () => null

export class PurchaseOrdersTable extends React.PureComponent<PurchaseOrdersTableProps> {
  state = {
    filteredCategory: [],
    initItems: cloneDeep(this.props.columns),
    saveItems: this.props.columns,
    selectedItemIndex: -1,
    expandedRowKeys: [],
  }

  private mutiSortTable = React.createRef<any>()

  constructor(props: PurchaseOrdersTableProps) {
    super(props)
  }

  componentWillReceiveProps(nextProps: PurchaseOrdersTableProps) {
    if (this.state.saveItems !== nextProps.columns) {
      this.setState({
        saveItems: nextProps.columns,
        initItems: cloneDeep(nextProps.columns),
      })
    }
  }

  wrapper = (props: any) => {
    return (
      <tbody>
        <EmptyTr>
          <td />
        </EmptyTr>
        {props.children}
      </tbody>
    )
  }

  updateArrayItem = (saleOrder: TempOrder, insert: boolean) => {
    const items = this.state.initItems
    const index = items.findIndex((i) => i.wholesaleOrderId === saleOrder.wholesaleOrderId)
    if (insert === false) {
      const newItems = update(items, {
        $splice: [[index, 1, saleOrder]],
      })
      this.setState({
        initItems: newItems,
      })
    } else {
      const newItems = update(items, { $push: [saleOrder] })
      this.setState({
        initItems: newItems,
      })
    }
  }

  onExpendedRowsChange = (expandedRowKeys: string[] | number[]) => {
    //this.props.clearRelatedOrders()
    if (this.state.expandedRowKeys.length > 0) {
      const row = []
      for (const value of expandedRowKeys) if (value !== this.state.expandedRowKeys[0]) row.push(value)
      this.setState({
        expandedRowKeys: row,
      })
    } else {
      this.setState({
        expandedRowKeys: expandedRowKeys,
      })
    }
  }

  onDuplicateOrder = (order: any) => {
    if (!order) return
    const _ = this
    var delivery = moment().format('MM/DD/YYYY')
    OrderService.instance.getOrderItemListByOrderId(order.wholesaleOrderId).subscribe({
      next(res: any) {
        let tempOrderItems = res.body.data
          ? res.body.data.map((obj: any) => {
            return {
              wholesaleItemId: obj.wholesaleItem ? obj.wholesaleItem.wholesaleItemId : undefined,
              modifiers: obj.modifiers,
              extraOrigin: obj.extraOrigin,
              cost: obj.cost,
              pricingUOM: obj.pricingUOM,
              inventoryUOM: obj.inventoryUOM,
              pas: obj.pas,
              note: obj.note,
              displayOrder: obj.displayOrder,
              pricingLogic: obj.pricingLogic,
              pricingGroup: obj.pricingGroup,
              //data for charge items
              itemName: obj.itemName,
              UOM: obj.UOM,
              price: obj.price,
              // cost: obj.cost,
              // pricingUOM: obj.pricingUOM,
              // ratio: obj.ratio,
            }
          }) : []
        const data = {
          deliveryDate: delivery,
          scheduledDeliveryTime: moment().format('h:mm a'),
          // orderDate: delivery,
          // quotedDate: delivery,
          // wholesaleCustomerClientId: order.wholesaleClient.clientId,
          userId: _.props.sellerSetting.userId,
          itemList: tempOrderItems,
          wholesaleOrderId: order.wholesaleOrderId,
        }
        _.props.duplicateOrder(data)
      },
      error(err) {
        checkError(err)
      },
    })
  }

  render() {
    const { initItems } = this.state
    const { history, currentPage, pageSize, total, sellerSetting } = this.props

    const columns: ColumnProps<TempOrder>[] = [
      {
        title: 'ORDER #',
        dataIndex: 'wholesaleOrderId',
        key: 'wholesaleOrderId',
        align: 'center',
        width: 150,
        sorter: true,
        render(text) {
          return <span>{getOrderPrefix(sellerSetting, 'purchase')}{text}</span>
        },
      },
      {
        title: 'TOTAL COST',
        dataIndex: 'totalCost',
        key: 'totalCost',
        sorter: true,
        render(val) {
          return <span>{val ? `$${formatNumber(val, 2)}` : ''}</span>
        },
      },
      {
        title: 'TOTAL PRICE',
        dataIndex: 'totalCost',
        key: 'totalCost',
        sorter: true,
        render(val) {
          return <span>{val ? `$${formatNumber(val, 2)}` : ''}</span>
        },
      },
      {
        title: 'STATUS',
        dataIndex: 'status',
        key: 'wholesaleOrderStatus',
        sorter: true,
      },
      {
        title: 'DATE PLACED',
        dataIndex: 'createdDate',
        key: 'createdDate',
        sorter: true,
        render: (val) => formatDate(val)
      },
      {
        title: 'DELIVERY DATE',
        dataIndex: 'deliveryDate',
        key: 'deliveryDate',
        sorter: true,
        render: (val) => formatDate(val)
      },
      {
        title: '',
        key: 'view',
        render: (_v: any, record: any) => {
          let url = `/order/${record.wholesaleOrderId}/purchase-cart`
          if (location.href.indexOf('/vendor') < 0) {
            url = `/sales-order/${record.wholesaleOrderId}`
          }
          return <ThemeLink to={url}>
            <div onClick={e => {
              e.stopPropagation();
            }}>
              View <ThemeIcon type="arrow-right" />
            </div>
          </ThemeLink>
        }
      },
      {
        title: '',
        key: 'duplicate',
        align: 'center',
        width: 100,
        render: (_v: any, record: any) => {
          // const url = location.href.indexOf('/vendor') >= 0 ? 'purchase-cart' : 'sales-cart'
          // return <ThemeLink to={`/order/${record.wholesaleOrderId}/${url}`}>View <ThemeIcon type="arrow-right" /></ThemeLink>
          return (
            <Tooltip title="Duplicate Order">
              <ThemeIconButton disabled={record.status == 'CANCEL'} className="no-border" onClick={() => this.onDuplicateOrder(record)}>
                <IconSvg
                  type="duplicate"
                  viewBox={void 0}
                  className={record.status == 'CANCEL' ? 'cancel-icon' : ''}
                  style={{ width: 24, height: 24, marginRight: 4, fill: 'transparent' }}
                />
              </ThemeIconButton>
            </Tooltip>
          )
        },
      },
    ]

    const onRow = (record: any, index: number) => {
      return {
        onClick: (_event: any) => {
          _event.preventDefault()
          const tagName = _event.target.tagName
          if (tagName !== 'button' && tagName !== 'path' && tagName !== 'svg') {
            history.push(`/order/${record.wholesaleOrderId}/purchase-cart`)
          }
        },
      }
    }

    return (
      <MultiBackSortTable
        ref={this.mutiSortTable}
        css={tableCss(false, false)}
        columns={columns}
        dataSource={initItems}
        rowKey="id"
        expandRowByClick
        expandIconAsCell={false}
        expandIcon={emtpyNode}
        expandedRowKeys={this.state.expandedRowKeys}
        onExpandedRowsChange={this.onExpendedRowsChange}
        onRow={onRow}
        pagination={{
          total: total,
          pageSize: pageSize,
          current: currentPage,
          defaultCurrent: 1
        }}
        onChange={this.props.changePage}
        onSort={this.props.changePage}
        loading={this.props.loading}
      />
    )
  }
}
