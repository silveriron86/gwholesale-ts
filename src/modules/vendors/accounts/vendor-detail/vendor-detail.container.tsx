import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { Layout, Row, Icon, notification } from 'antd'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'

import { Theme } from '~/common'
import { VendorsModule, VendorsDispatchProps, VendorsStateProps } from '~/modules/vendors/vendors.module'
import { SalesTableSearchComponent } from '~/modules/customers/components/searcher'
import { PurchaseOrdersTable } from '~/modules/vendors/accounts/vendor-detail/purchase-orders-table'

import { GlobalState } from '~/store/reducer'
import {
  layoutStyle,
  CustomersInfoHeader,
  CustomersInfoBody,
} from '~/modules/customers/components/customers-detail/customers-detail.style'

import { SaleItem, TempOrder, WholesaleRoute } from '~/schema'
import PageLayout from '~/components/PageLayout'
import DetailsOverview from '~/modules/customers/components/details-overview'
import moment, { Moment } from 'moment'
import { ThemeOutlineButton } from '~/modules/customers/customers.style'
import { Level2WithTable } from '~/common/jqueryHelper'
import { FlexDiv, Item } from '~/modules/customers/nav-sales/styles'

type CustomersDetailProps = VendorsDispatchProps &
  VendorsStateProps &
  RouteComponentProps<{ vendorId: string }> & {
    theme: Theme
  }

export class VendorDetail extends React.PureComponent<CustomersDetailProps> {
  readonly state = {
    curPreset: 8,
    currentOrderIndex: -1,
    currentPriceSheetIndex: -1,
    showOrder: true,
    showPriceSheet: true,
    editingInfo: false,
    editValues: {},
    message: '',
    type: null,
    showDrawer: false,
    showFilter: false,
    item: {} as SaleItem,
    saleCategories: [],
    search: '',
    loading: false,
    fromDate: moment().subtract(30, 'days'),
    toDate: moment().add(30, 'days'),
    currentPage: 1,
    pageSize: 10,
    vendorId: -1,
    orderByFilter: '',
    direction: '',
  }

  componentDidMount() {
    // const urlParams = new URLSearchParams(this.props.location.search)
    // const search = urlParams.get('duplicated')
    // if (search === 'success') {
    //   setTimeout(() => {
    //     window.history.replaceState({}, '', `#${this.props.location.pathname}`)
    //     setTimeout(() => {
    //       notification.success({
    //         message: 'SUCCESS',
    //         description: 'Order successfully duplicated.',
    //         onClose: () => { },
    //       })
    //     }, 1500)
    //   }, 100)
    // }

    this.props.resetLoading()
    const vendorId = this.props.match.params.vendorId
    this.props.getVendor(vendorId)
    this.setState({
      vendorId,
    })
    this.getAllPurchaseOrders()
    this.props.getVendorPriceSheet(vendorId)
    this.props.getVendorSellerPriceSheet()
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
    Level2WithTable('level-2-with-table')
  }

  componentWillReceiveProps(nextProps: CustomersDetailProps) {
    console.log('check change', nextProps.newItemId)
    if (nextProps.vendorOrders.length > 0) {
      if (nextProps.newItemId > 0) {
        setTimeout(() => {
          const query = nextProps.message === 'DUPLICATED_SUCCESS' ? '?duplicated=success' : ''
          console.log(this.props.history)
          this.props.history.push(`/order/${nextProps.newItemId}/purchase-cart${query}`)
          this.props.resetItemId()
        }, 300)
      }
    }
    if (this.state.loading && nextProps.newItemId > 0) {
      this.setState(
        {
          loading: false,
        },
        () => {
          if (nextProps.newItemId > 0) {
            setTimeout(() => {
              this.props.history.push(`/order/${nextProps.newItemId}/purchase-cart`)
              this.props.resetItemId()
            }, 300)
          }
        },
      )
    }
  }

  getAllPurchaseOrders = () => {
    const vendorId = this.props.match.params.vendorId
    const searchObj = {
      id: this.state.vendorId != -1 ? this.state.vendorId : vendorId,
      from: this.state.fromDate.format('MM/DD/YYYY'),
      to: this.state.toDate.format('MM/DD/YYYY'),
      page: this.state.currentPage - 1,
      pageSize: this.state.pageSize,
      queryParam: this.state.search,
      orderByFilter: this.state.orderByFilter,
      direction: this.state.direction,
    }
    this.props.resetLoading()
    this.props.getVendorOrder(searchObj)
  }

  onSearch = (text: string) => {
    this.setState(
      {
        search: text,
        currentPage: 1,
      },
      () => {
        this.getAllPurchaseOrders()
      },
    )
  }

  onNewOrder = () => {
    const { routes, match } = this.props
    let data = {
      wholesaleClientId: match.params.vendorId,
      deliveryDate: moment(new Date()).format('MM/DD/YYYY'),
      scheduledDeliveryTime: moment().format('h:mm a'),
      orderDate: moment().format('MM/DD/YYYY'),
      itemList: [],
    }

    let activeRoutes: WholesaleRoute[] = []
    if (routes.length > 0) {
      routes.forEach((route) => {
        const weekDay = moment()
          .format('dddd')
          .toUpperCase()
        if (route.activeDays.indexOf(weekDay) >= 0) {
          activeRoutes.push({ ...route })
        }
      })

      if (activeRoutes.length > 0) {
        data.defaultRoute = activeRoutes[0].id
      }
    }

    this.setState(
      {
        loading: true,
      },
      () => {
        this.props.createEmptyOrder(data)
      },
    )
  }

  onPageChange = (page: any, filter: any, sorter: any) => {
    const state = { ...this.state }
    if (page !== null) {
      state['currentPage'] = page.current
    }
    if (sorter && Array.isArray(sorter)) {
      const filterStr = sorter
        .map((el: any) => {
          return el.dataIndex
        })
        .join(',')
      const directionStr = sorter
        .map((el: any) => {
          return el.sortOrder === 'ascend' ? 'ASC' : 'DESC'
        })
        .join(',')
      state['orderByFilter'] = filterStr ? filterStr : ''
      state['direction'] = directionStr ? directionStr : ''
    }
    this.setState(state, () => {
      this.getAllPurchaseOrders()
    })
  }

  onDateChange = (dates: [Moment, Moment], dateStrings: [string, string]) => {
    if (dates.length < 2 || dateStrings.length < 2) return
    this.setState({ fromDate: dates[0], toDate: dates[1], currentPage: 1 }, () => {
      this.getAllPurchaseOrders()
    })
  }

  renderSalesOrders = () => {
    const { currentPage, pageSize, curPreset } = this.state
    const salesOrders: TempOrder[] = this.props.vendorOrders
    const { history, orderTotal } = this.props
    return (
      <Layout className="level-2-with-table">
        <SalesTableSearchComponent
          curPreset={curPreset}
          rawData={salesOrders}
          onShowDrawer={() => { }}
          onSearch={this.onSearch}
          theme={this.props.theme}
          handleNewOrder={this.onNewOrder}
          fromDateProps={this.state.fromDate}
          toDateProps={this.state.toDate}
          onDateChange={this.onDateChange}
        />
        <PurchaseOrdersTable
          columns={salesOrders}
          search={this.state.search}
          showDrawer={this.state.showDrawer}
          loading={this.props.loading}
          history={history}
          changePage={this.onPageChange}
          currentPage={currentPage}
          pageSize={pageSize}
          total={orderTotal}
          sellerSetting={this.props.sellerSetting}
          duplicateOrder={this.props.duplicateOrder}
        />
      </Layout>
    )
  }

  renderActionButtons = () => {
    return (
      <Layout style={layoutStyle}>
        <Row style={{ marginTop: '25px' }}>
          {/* <Col lg={6} style={{ textAlign: 'left' }}>
            <CustomButton>Add to QBO</CustomButton>
          </Col>
          <Col>
            <Row type="flex" justify="end" style={{ paddingBottom: 20 }}>
              <CustomButton>Update QBO</CustomButton>
              <CustomButton>View in QBO</CustomButton>
              <CustomButton>New Sales Order Form Template</CustomButton>
              <CustomButton>New Blank Sales Order</CustomButton>
            </Row>
          </Col>*/}
        </Row>
      </Layout>
    )
  }

  render() {
    const { vendor } = this.props
    const { loading } = this.state
    if (!vendor) return null
    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Vendors'}>
        <FlexDiv style={{ position: 'absolute', right: 11, marginTop: -48, zIndex: 1000 }}>
          <Item className="header-btns right">
            <ThemeOutlineButton size="large" onClick={this.onNewOrder}>
              <Icon type="plus" /> Create Blank Order
            </ThemeOutlineButton>
          </Item>
        </FlexDiv>
        <CustomersInfoHeader>
          <DetailsOverview customer={vendor} />
        </CustomersInfoHeader>
        <CustomersInfoBody>
          {/* <ThemeSpin tip="Loading..." spinning={loading}> */}
          {this.renderSalesOrders()}
          {this.renderActionButtons()}
          {/* </ThemeSpin> */}
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapState = (state: GlobalState) => state.vendors

export const VendorDetailContainer = withTheme(connect(VendorsModule)(mapState)(VendorDetail))
