/**@jsx jsx */
import React from 'react'
import { jsx } from '@emotion/core'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { GlobalState } from '~/store/reducer'

import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import moment from 'moment'

import { ReportPageContainer } from '~/modules/location/location.style'
import { VendorsDispatchProps, VendorsModule, VendorsStateProps } from '../vendors.module'
import OrderReportHeader from './order-report-header'
import PurchaseOrderReportBody from './order-report-body'

export type PurchaseOrderReportsProps = VendorsDispatchProps & VendorsStateProps & {
  theme: Theme,
}

class PurchaseOrderReports extends React.PureComponent<PurchaseOrderReportsProps> {
  state: any
  constructor(props: PurchaseOrderReportsProps) {
    super(props)
    this.state = {
      from: moment().subtract(30, 'days'),
      to: moment(),
      search: '',
      id: 0,
      type: 'VENDOR'
    }
  }
  componentDidMount() {
    this.props.getAllCustomersForUser()
    this.getReports()
  }

  getReports = () => {
    const searchObj = {
      ...this.state,
      from: this.state.from.format('MM/DD/YYYY'),
      to: this.state.to.format('MM/DD/YYYY'),
    }
    this.props.setPOReportsParams(searchObj)
    this.props.getPurchaseOrderStatistics(searchObj)
  }

  onChange = (type: string, data: any) => {
    if (type == 'date') {
      this.setState({ from: data.from, to: data.to })
    } else {
      let obj = this.state
      obj[type] = data
      this.setState({ ...obj })

    }

    setTimeout(() => {
      this.getReports()
    }, 100)
  }

  render() {
    const { from, to, search, vendorId } = this.state
    const { items, POReports } = this.props
    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Delivery Routing'}>
        <ReportPageContainer>
          <OrderReportHeader
            onSearchChange={this.onChange}
            from={from}
            to={to}
            search={search}
            theme={this.props.theme}
            title={'Purchase Order Reports'}
            placeholder={"Search by SKU"}
            clients={items ? items : []}
          />
          <PurchaseOrderReportBody
            data={POReports}
          />
        </ReportPageContainer>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.vendors

export default withTheme(connect(VendorsModule)(mapStateToProps)(PurchaseOrderReports))
