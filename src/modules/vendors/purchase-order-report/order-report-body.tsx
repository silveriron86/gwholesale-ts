import React from 'react'
import { Moment } from 'moment'
import { Theme } from '~/common'

import { Row, Col, DatePicker, Select } from 'antd'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
export type PurchaseOrderReportBodyProps = {
  data: any
}

class PurchaseOrderReportBody extends React.PureComponent<PurchaseOrderReportBodyProps> {

  render() {
    const { data } = this.props
    let seris = [0, 0, 0, 0]
    if (data) {
      const unitsOrdered = data.unitsOrdered ? data.unitsOrdered : 0
      const unitsConfirmed = data.unitsConfirmed ? data.unitsConfirmed : 0
      const totalUnits = data.totalUnits ? data.totalUnits : 0
      const totalPallets = data.totalPallets ? data.totalPallets : 0
      seris = [unitsOrdered, unitsConfirmed, totalUnits, totalPallets]
    }

    return (
      <div style={{ marginTop: 40, marginLeft: -12 }}>
        <h1>Order Total : ${data && data.orderTotal ? data.orderTotal : 0} </h1>
        <HighchartsReact
          highcharts={Highcharts}
          options={{
            chart: {
              type: 'column'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: ['Units Ordered', 'Units Confirmed', 'Total Units', 'Total Pallets'],
              title: {
                text: null
              }
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Units Count',
                align: 'high'
              },
              labels: {
                overflow: 'justify'
              }
            },
            tooltip: {
              // valueSuffix: ' millions'
            },
            plotOptions: {
              bar: {
                dataLabels: {
                  enabled: true
                }
              }
            },
            legend: {
              enabled: false
            },
            credits: {
              enabled: false
            },
            series: [{
              name: '',
              data: seris
            }
              // , {
              //   name: 'Year 1900',
              //   data: [133, 156, 947, 408, 6]
              // }, {
              //   name: 'Year 2000',
              //   data: [814, 841, 3714, 727, 31]
              // }, {
              //   name: 'Year 2016',
              //   data: [1216, 1001, 4436, 738, 40]
              // }
            ]
          }}
        />
      </div>
    )
  }
}

export default PurchaseOrderReportBody