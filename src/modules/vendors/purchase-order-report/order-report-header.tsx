import React from 'react'
import { Moment } from 'moment'
import { Theme } from '~/common'

import { Row, Col, DatePicker, Select } from 'antd'
import { HeaderTitle, ThemeSelect } from '~/modules/customers/customers.style'
import GrubSuggest from '~/components/GrubSuggest'
import { HeaderElement, HistoryHeaderWrapper } from '~/modules/location/location.style'
import { TempCustomer } from '~/schema'

const { RangePicker } = DatePicker
const { Option } = Select
export type OrderReportHeaderProps = {
  theme: Theme
  onSearchChange: Function
  from: Moment
  to: Moment
  search: string
  title: string
  clients: TempCustomer[]
  placeholder: string
}

class OrderReportHeader extends React.PureComponent<OrderReportHeaderProps> {

  onSearch = (keyword: string) => {
    this.props.onSearchChange('search', keyword)
  }

  onDateChange = (date: any, dateString: [string, string]) => {
    this.props.onSearchChange('date', { from: date[0], to: date[1] })
  }

  onSelectChange = (value: number) => {
    let clientId = value
    if (!value) {
      clientId = 0
    }
    this.props.onSearchChange('id', clientId)
  }

  onSuggest = (str: string) => {
    return []
  }

  render() {
    const { from, to, theme, clients, title, placeholder } = this.props
    return (
      <HistoryHeaderWrapper>
        <HeaderTitle style={{ marginBottom: 20 }}>{title}</HeaderTitle>
        <Row>
          <Col span={6}>
            <HeaderElement>
              <GrubSuggest theme={theme} onSuggest={this.onSuggest} onSearch={this.onSearch} placeholder={placeholder} />
            </HeaderElement>
          </Col>
          <Col offset={1} span={6}>
            <HeaderElement>
              <RangePicker
                placeholder={['MM/DD/YYYY', 'MM/DD/YYYY']}
                format={'MM/DD/YYYY'}
                defaultValue={[from, to]}
                onChange={this.onDateChange}
                allowClear={false}
              />
            </HeaderElement>
          </Col>
          <Col offset={5} span={6}>
            <HeaderElement>
              <ThemeSelect
                onChange={this.onSelectChange}
                placeholder={'Please select the vendors...'}
                allowClear={true}
                onClear={this.onSelectChange}
              >
                {
                  clients.map(el => {
                    return (<Option key={el.clientId} value={el.clientId}>{el.clientCompany ? el.clientCompany.companyName : ''}</Option>)
                  })
                }
              </ThemeSelect>
            </HeaderElement>
          </Col>
        </Row>
      </HistoryHeaderWrapper>
    )
  }
}

export default OrderReportHeader