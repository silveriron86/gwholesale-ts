import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { AccountLabel, InfoHeader } from '../../../customers/sales/_style'
import { Divider, Icon, Select, Tooltip } from 'antd'
import moment from 'moment'
import { ThemeInput, ThemeSelect, ThemeModal } from '~/modules/customers/customers.style'
import { getOrderPrefix, calcOneOffTotal, formatNumber, calPoTotalCost, formatAddress, notify, judgeConstantRatio } from '~/common/utils'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { CACHED_NS_LINKED, Theme, FINANCIAL_TERMS, gray01 } from '~/common'
import { AuthUser, OrderDetail, OrderItem, UserRole } from '~/schema'
import { CustomerName, FlexDiv, ValueLabel } from '~/modules/customers/nav-sales/styles'
import HeaderOrderDetail from '~/modules/customers/nav-sales/order-detail/components/header-order-detail'
import { NewOrderFormModal } from '~/modules/orders/components'
import { history } from '~/store/history'
import { Icon as IconSvg } from '~/components/icon/'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import _ from 'lodash'

type PurcahseHeaderProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
    order: OrderDetail
    orderItems: OrderItem[]
    oneOffItems: OrderItem[]
    errorMsg?: string
    setReceivedAndLock: Function
    currentUser: AuthUser
    propsValue: any
  }

export class PurcahseHeaderWrapper extends React.PureComponent<PurcahseHeaderProps> {
  state: any
  constructor(props: PurcahseHeaderProps) {
    super(props)
    // const { order, vendors } = this.props
    this.state = {
      accountName: props.order.wholesaleClient.clientCompany.companyName,
      accountId: props.order.wholesaleClient.clientId,
      showVendorModal: false,
      po: props.order.po,
      isClose: props.order.isLocked,
      statusList: [
        {
          key: 'New',
          value: 'New',
        },
        {
          key: 'Confirmed',
          value: 'Confirmed',
        },
        {
          key: 'Received',
          value: 'Received',
        },
        {
          key: 'Cancel',
          value: 'Canceled',
        },
      ],
      financialTerms: '',
      visiblePaymentTermsModal: false,
      companyProductTypes: null,
      editPo:false
    }
  }

  // goNextTab() {
  //   let els = jQuery('.tab-able')
  //   if (els.length > 0) {
  //     console.log(els[0])
  //     setTimeout(() => {
  //       jQuery(els[0]).trigger('click')
  //     }, 50)
  //   } else {
  //     setTimeout(() => {
  //       jQuery('.ant-select-search__field').focus()
  //     }, 50)
  //   }
  // }

  componentDidMount() {
    this.props.getSimplifyVendors()
    this.props.getCompanyShippingAddresses()
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
    this.props.getCompanyProductAllTypes()
    this.setState({ companyProductTypes: this.props.companyProductTypes })
    /*
    // Tab Order
    setTimeout(() => {
      jQuery('.ant-select-search__field').focus()
    }, 1000)
    jQuery('.external-id')
      .unbind()
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9 && !e.shiftKey) {
          e.preventDefault()
          setTimeout(() => {
            if (jQuery('.purchase-add-item').length) {
              jQuery('.purchase-add-item').focus()
            } else {
              console.log('here no add item')
              jQuery('.purchase-item-search input').focus()
            }
          }, 50)
        }
      })

    jQuery('.purchase-item-search')
      .unbind()
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9) {
          if (e.shiftKey) {
            e.preventDefault()
            setTimeout(() => {
              if (jQuery('.purchase-add-item').length) {
                jQuery('.purchase-add-item').focus()
              } else {
                jQuery('.external-id').focus()
              }
            }, 50)
          } else {
            this.goNextTab()
          }
        }
      })

    jQuery('.purchase-add-item')
      .unbind()
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9) {
          if (e.shiftKey) {
            e.preventDefault()
            setTimeout(() => {
              jQuery('.external-id').focus()
            }, 50)
          } else {
            jQuery('.purchase-item-search').focus()
          }
        }
      })

    jQuery('.account-name-first input')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode === 9) {
          if (e.shiftKey) {
            const editableEls = jQuery('.tab-able')
            if (editableEls.length > 0) {
              setTimeout(() => {
                jQuery(editableEls[editableEls.length - 1]).trigger('focus')
                if (jQuery(editableEls[editableEls.length - 1]).hasClass('editable-cell-value-wrap')) {
                  jQuery(editableEls[editableEls.length - 1]).trigger('click')
                }
              }, 50)
            } else {
              setTimeout(() => {
                jQuery('.ant-input')
                  .first()
                  .focus()
              }, 50)
            }
          }
        }
      })
      */
  }

  componentWillReceiveProps(nextProps: PurcahseHeaderProps) {
    const { order } = nextProps
    if(this.state.editPo == false){
      this.setState({
        po: order.po,
      })
    }

    if (
      this.props.order.wholesaleClient.clientCompany.companyId !==
      nextProps.order.wholesaleClient.clientCompany.companyId
    ) {
      this.setState({
        accountName: order.wholesaleClient.clientCompany.companyName,
        accountId: order.wholesaleClient.clientId
      })
    }

    if (order && !this.state.financialTerms) {
      const financialTerms = order.financialTerms ? order.financialTerms : order.wholesaleClient.paymentTerm ? order.wholesaleClient.paymentTerm : ''
      this.setState({
        financialTerms,
      })
    }

    if (JSON.stringify(this.props.companyProductTypes) != JSON.stringify(nextProps.companyProductTypes)) {
      this.setState({
        companyProductTypes: nextProps.companyProductTypes,
      })
    }
  }

  handleChangeAccount = (value: any) => {
    this.setState({
      accountName: value,
    })
  }

  handleSelectAccount = (value: any) => {
    this.setState(
      {
        accountId: value,
      },
      () => {
        const { order } = this.props
        this.props.updateOrderInfo({
          wholesaleOrderId: order.wholesaleOrderId,
          deliveryDate: moment(order.deliveryDate).format('MM/DD/YYYY'),
          wholesaleVendorClientId: value,
          status: order.wholesaleOrderStatus,
        })
      },
    )
  }

  disabledDate = (current) => {
    // Can not select days before today and today
    return (
      current &&
      current <
        moment()
          .subtract(1, 'days')
          .local(true)
          .endOf('day')
    )
  }

  onSearch = (searchStr: string) => {
    const { vendors } = this.props
    // tslint:disable-next-line:prefer-const
    let accountNames: any[] = []
    if (vendors.length > 0 && searchStr) {
      vendors.filter((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          accountNames.push({
            value: row.clientId,
            text: `${row.clientCompany.companyName}  #${row.clientId}`,
          })
        }
      })
    }
    this.setState({
      accountNames: accountNames,
    })
  }

  onReferenceChange = (e: any) => {
    this.setState({ editPo: true })
    this.setState({ po: e.target.value })
  }

  onFinancialTermsChange = (financialTerms: string) => {
    this.setState({
      financialTerms
    }, () => {
      this.props.updateOrderInfo({
        wholesaleOrderId: this.props.order.wholesaleOrderId,
        financialTerms
      })
    })
  }

  onCompanySettingTypeModal = (type: string) => {
    let state = { ...this.state }
    state[type] = !this.state[type]
    this.setState(state)
    this.props.getCompanyProductAllTypes()
  }

  onSaveReference = () => {
    const { po } = this.state
    const data = {
      wholesaleOrderId: this.props.order.wholesaleOrderId,
      po,
    }
    this.props.updateOrderInfo(data)
  }

  onChangeVendor = (selectedVendor: any, actionType: string) => {
    this.setState({ showVendorModal: false }, () => {
      if (selectedVendor && actionType == 'ok') {
        const data = {
          wholesaleOrderId: this.props.order.wholesaleOrderId,
          wholesaleClientId: selectedVendor,
        }
        this.props.updateOrderInfo(data)
      }
    })
  }

  onChangeSwitch = (v: string) => {
    const checked = v === 'CLOSED'
    // const { orderItems } = this.props
    // migration cann't handle this data,so we don't allow user to unshiped
    // const hasOldCatchWeight = orderItems.filter((orderItem: any) => orderItem.oldCatchWeight).length > 0
    // if (hasOldCatchWeight) {
    //   notify('warn', 'Warn', 'This data cannot be operated, please contact the system administrator')
    //   return
    // }

    // setTimeout(() => this.props.setReceivedAndLock(checked), 1000)
    // this.setState({
    //   isClose: checked,
    // })

    this.setState({
      isClose: checked
    }, () => {
      this.props.updateOrderInfo({
        wholesaleOrderId: this.props.order.wholesaleOrderId,
        isLocked: checked
      })
    })
  }

  openCompanySettingChange = (type: string) => {
    let state = { ...this.state }
    state[type] = true
    this.setState(state)
  }

  render() {
    const { accountNames, accountName, accountId, statusList, po, isClose, financialTerms, visiblePaymentTermsModal, companyProductTypes } = this.state
    const {
      errorMsg,
      order,
      orderItems,
      oneOffItems,
      sellerSetting,
      simplifyVendors,
      relatedBills,
      currentUser,
    } = this.props

    const isApprox = orderItems.map(v => {
      if (!judgeConstantRatio(v) && v.status !== 'RECEIVED') return false
      return true
    }).includes(false)

    let totalData = calPoTotalCost(orderItems,true)

    let totalUnit = totalData != null ? totalData.totalUnit : 0
    let subTotal = totalData != null ? totalData.subTotal : 0

    // const relatedBillTotal = calcOneOffTotal(relatedBills)
    const oneOffTotal = calcOneOffTotal(oneOffItems)
    const orderTotal = subTotal + oneOffTotal
    const orderDisabled = !order || order.isLocked || order.wholesaleOrderStatus == 'CANCEL'

    const mainBillingAddress =
      order.wholesaleClient && order.wholesaleClient.mainBillingAddress
        ? order.wholesaleClient.mainBillingAddress.address
        : null
    const paymentAddress = formatAddress(mainBillingAddress)

    const nsEnabled = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const paymentTermsList = nsEnabled
    ? FINANCIAL_TERMS
    : companyProductTypes
    ? [...companyProductTypes.paymentTerms, ...companyProductTypes.paymentTermsFixedTypes]
    : []

    return (
      <InfoHeader style={{ padding: 0, boxShadow: 'none', marginBottom: 0, color: gray01}}>
        <FlexDiv
          className="space-between"
          style={{ padding: '14px 24px 14px 90px', marginBottom: 14, borderBottom: '1px solid #D8DBDB' }}
        >
          <FlexDiv style={{ alignItems: 'center' }}>
            <div>
              <CustomerName style={{ paddingTop: 20, minWidth: 250, textAlign: 'left' }}>
                <ValueLabel className="link-text" onClick={() => history.push(`/vendor/${accountId}/orders`)}>
                  {order && order.wholesaleClient && order.wholesaleClient.clientCompany
                    ? order.wholesaleClient.clientCompany.companyName
                    : ''}
                </ValueLabel>
                <IconSvg
                  type="edit"
                  viewBox="0 0 18 18"
                  width={16}
                  height={16}
                  className={orderDisabled ? 'disabled' : 'enabled'}
                  onClick={() => {
                    this.setState({ showVendorModal: true })
                  }}
                />
                {this.state.showVendorModal && (
                  <NewOrderFormModal
                    visible={this.state.showVendorModal}
                    onOk={(e) => this.onChangeVendor(e, 'ok')}
                    onCancel={(e) => this.onChangeVendor(e, 'close')}
                    clients={simplifyVendors}
                    okButtonName="Change Vendor"
                    isNewOrderFormModal={false}
                    defaultClientId={order ? order.wholesaleClient.clientId : 'No Client'}
                  />
                )}
              </CustomerName>
              {mainBillingAddress && (
                <ValueLabel className="small black" style={{ textAlign: 'left' }}>
                  {paymentAddress}
                </ValueLabel>
              )}
              {/* <AccoutName
                className="bold-blink account-name-first"
                value={accountName}
                dataSource={accountNames}
                onSearch={this.onSearch}
                onChange={this.handleChangeAccount}
                onSelect={this.handleSelectAccount}
                style={{ width: 248 }}
              /> */}
            </div>
            <div style={{ marginLeft: 15 }} className="inputDisabled">
              <AccountLabel className="left-align">Vendor Reference No.</AccountLabel>
              <ThemeInput
                maxLength={25}
                value={po}
                style={{ width: 200 }}
                onChange={this.onReferenceChange}
                onBlur={this.onSaveReference}
                disabled={orderDisabled}
              />
            </div>
          </FlexDiv>
          <FlexDiv>
            <div>
              <AccountLabel className="left-align">Order No.</AccountLabel>
              <h3 style={{ marginTop: 10, textAlign: 'left' , color: gray01 }}>
                {getOrderPrefix(sellerSetting, 'purchase')}
                {order.wholesaleOrderId}
              </h3>
            </div>
            <div style={{ marginLeft: 24 }}>
              <AccountLabel className="left-align">Total Units</AccountLabel>
              <h3 className="text-left" style={{ marginTop: 10 , color: gray01 }}>
                {totalUnit}
              </h3>
            </div>
            {currentUser.accountType != UserRole.WAREHOUSE && (
              <div style={{ marginLeft: 24 }}>
                <AccountLabel className="left-align">Order Total</AccountLabel>
                <h3 className="text-left" style={{ marginTop: 10, color: gray01 }}>
                  {
                    isApprox ?
                    <>
                    <Tooltip placement="bottom" title={'Approximate values used for catchweight items not yet received, based on approximate ratio'}>
                      {`$${formatNumber(orderTotal, 2)}*`}
                    </Tooltip>
                    </>
                    : `$${formatNumber(orderTotal, 2)}`
                  }
                </h3>
              </div>
            )}
            {order !== null  && (
              <div style={{ marginLeft: 24 }}>
                <AccountLabel className="left-align">Order status</AccountLabel>
                <ThemeSelect
                  value={order.wholesaleOrderStatus == 'CANCEL' ? 'CANCEL' : order.isLocked ? 'CLOSED' : 'OPEN'}
                  onChange={this.onChangeSwitch}
                  style={{ minWidth: 100 }}
                  disabled={this.props.updateOrderQuantityLoading.fetching || orderDisabled}
                >
                  <Select.Option value="OPEN">
                    OPEN
                  </Select.Option>
                  <Select.Option value="CLOSED">
                    CLOSED
                  </Select.Option>
                </ThemeSelect>
              </div>
            )}

            <div style={{ marginLeft: 24 }}>
              <AccountLabel className="left-align">Terms</AccountLabel>
              <ThemeSelect
                disabled={orderDisabled}
                value={financialTerms}
                onChange={this.onFinancialTermsChange}
                style={{ minWidth: 120 }}
                showSearch
                optionFilterProp="children"
                onSearch={() => {}}
                filterOption={(input: any, option: any) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                dropdownRender={(menu: any) => (
                  <div>
                    {menu}
                    {!nsEnabled && (
                      <>
                        <Divider style={{ margin: '4px 0' }} />
                        <div
                          style={{ padding: '4px 8px', cursor: 'pointer' }}
                          onMouseDown={(e) => e.preventDefault()}
                          onClick={(e) => this.openCompanySettingChange('visiblePaymentTermsModal')}
                        >
                          <Icon type="plus" /> Add New Payment Terms...
                        </div>
                      </>
                    )}
                  </div>
                )}
              >
                {paymentTermsList.map(
                  (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                    return (
                      <Select.Option key={`payment-term-${index}`} value={nsEnabled ? item : item.name}>
                        {nsEnabled ? item : item.name}
                      </Select.Option>
                    )
                  },
                )}
              </ThemeSelect>
            </div>
          </FlexDiv>
        </FlexDiv>

        <HeaderOrderDetail propsValue={this.props} {...this.props} />

        <ThemeModal
          title={`Edit Value List "Payment Terms"`}
          visible={visiblePaymentTermsModal}
          onCancel={this.onCompanySettingTypeModal.bind(this, 'visiblePaymentTermsModal')}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor
            isModal={true}
            field="paymentTerms"
            title="Payment Terms"
            buttonTitle="Add New Payment Terms"
          />
        </ThemeModal>
      </InfoHeader>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.orders,
    currentUser: state.currentUser,
  }
}
export const PurcahseHeader = withTheme(connect(OrdersModule)(mapStateToProps)(PurcahseHeaderWrapper))
