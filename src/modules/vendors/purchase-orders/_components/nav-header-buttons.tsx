import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Icon, Menu, Dropdown, Popconfirm, Tooltip } from 'antd'
import { Flex, ThemeButton, ThemeOutlineButton, ThemeSpin } from '~/modules/customers/customers.style'
import { FlexDiv, Item, HeaderActionMenu, NavHeaderButtonsWrapper } from '~/modules/customers/nav-sales/styles'
import { Theme, CACHED_QBO_LINKED, CACHED_NS_LINKED, CACHED_USER_ID, CACHED_COMPANY } from '~/common'
import { OrderDetail } from '~/schema'
import { Icon as IconSvg } from '~/components'
import { formatTimeDifference, getFulfillmentDate } from '~/common/utils'
import EnterPurchaseModal from '~/modules/orders/components/enter-purchase-modal'
import { NewOrderFormModal } from '~/modules/orders/components'
import jQuery from 'jquery'
import moment from 'moment'

type PurchaseNavHeaderButtonsProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
    order: OrderDetail
    openPrintModal: Function
    handlerDropdownAction: Function
    syncQBOOrder: Function
    syncNSOrder: Function
    showPDFButton: boolean
    clients: any[]
    settings: any
    showSyncModal: Function
  }

export class PurchaseNavHeaderButtonsWrapper extends React.PureComponent<PurchaseNavHeaderButtonsProps> {
  state: any
  constructor(props: PurchaseNavHeaderButtonsProps) {
    super(props)
    this.state = {
      visibleEnterModal: false,
      createShow: false,
    }
  }

  componentWillReceiveProps(nextProps: Readonly<PurchaseNavHeaderButtonsProps>) {
    if (this.props.finalizing !== nextProps.finalizing && nextProps.finalizing === false) {
      window.location.href = '#/purchase-orders'
    }
  }

  componentDidMount() {
    this.props.getSimplifyVendors()
  }

  handleSyncQBOOrder = () => {
    const { order } = this.props
    if (order) {
      this.props.resetLoading()
      this.props.syncQBOOrder(order.wholesaleOrderId)
    }
  }

  handleSyncNSOrder = () => {
    const { order } = this.props
    if (order) {
      this.props.resetLoading()
      this.props.syncNSOrder(order.wholesaleOrderId)
    }
  }

  onActions = (item: any) => {
    const { key } = item
    console.log(key);
    if (key === '2') {
      this.props.showSyncModal()
    }
  }

  renderQboSyncButton = () => {
    const {
      loading,
      currentOrder: { qboId, lastQboUpdate },
      syncedQboBills
    } = this.props
    const qboRealmId = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    const userId = localStorage.getItem(CACHED_USER_ID)
    const companyId = localStorage.getItem(CACHED_COMPANY)
    if (qboRealmId) {
      const lastUpdated = new Date(lastQboUpdate)
      const style = { borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }
      const menuStyle = { textTransform: 'capitalize', fontSize: 15 }
      return (
        <Flex>
          <Tooltip
            placement={'top'}
            title={lastQboUpdate ? `Synced to Quickbooks ${formatTimeDifference(lastUpdated)} ago` : ''}
          >
            <ThemeOutlineButton
              size="large"
              onClick={() => this.handleSyncQBOOrder()}
              loading={loading}
              style={style}
              hidden={
                ((companyId == 'Growers Produce' && !(userId == '105629' || userId == '116137' || userId == '110151' || userId ==  '97960' || userId == '110154' || userId == '110148' || userId == '110149')) ||
                (companyId == 'PayLess Logistics' && !(userId == '105120' || userId == '105165' || userId == '105159' || userId == '105160')))
              }
            >
              <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
              {qboId ? 'Re-' : ''}Sync to QBO
            </ThemeOutlineButton>
          </Tooltip>
          <Dropdown
            overlay={
              <HeaderActionMenu onClick={this.onActions}>
                {/* {qboId && (
                  <Menu.Item key="1" style={menuStyle}>
                    <a href={`https://qbo.intuit.com/app/bill?txnId=${qboId}`} target="_blank" style={{ color: 'black' }}>
                      Open In Quickbooks
                    </a>
                  </Menu.Item>
                )} */}
                {syncedQboBills.map((item: any, index: number) => (
                  <Menu.Item key={`qbo-${qboId}`} style={menuStyle}>
                    <a href={`https://qbo.intuit.com/app/bill?txnId=${item.qboId}`} target="_blank" style={{ color: 'black' }}>
                      Open Bill #{index + 1} in QBO
                    </a>
                  </Menu.Item>
                ))}
                <Menu.Item key="2" style={menuStyle}>
                  Select items to sync
                </Menu.Item>
              </HeaderActionMenu>
            }
            trigger={['click']}
            placement="bottomRight"
          >
            <ThemeOutlineButton
              size="large"
              type="primary"
              style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
            >
              <Icon type="down" />
            </ThemeOutlineButton>
          </Dropdown>
        </Flex>
      )
    }
    return <span />
  }

  renderNsSyncButton = () => {
    const {
      loading,
      currentOrder: { nsId, lastNsUpdate }
    } = this.props
    const nsRealmId = localStorage.getItem(CACHED_NS_LINKED)
    const userId = localStorage.getItem(CACHED_USER_ID)
    const companyId = localStorage.getItem(CACHED_COMPANY)
    if (nsRealmId && nsRealmId != 'null') {
      const lastUpdated = new Date(lastNsUpdate)
      const style = { borderTopRightRadius: 0, borderBottomRightRadius: 0, marginRight: -1 }
      return (
        <Flex>
          <Tooltip
            placement={'top'}
            title={lastNsUpdate ? `Synced to NetSuite ${formatTimeDifference(lastUpdated)} ago` : ''}
          >
            <ThemeOutlineButton
              size="large"
              onClick={() => this.handleSyncNSOrder()}
              loading={loading}
              style={nsId ? style : {}}
              hidden={
                ((companyId == 'Growers Produce' && !(userId == '105629' || userId == '116137' || userId == '110151' || userId ==  '97960' || userId == '110154' || userId == '110148' || userId == '110149')) ||
                (companyId == 'PayLess Logistics' && !(userId == '105120' || userId == '105165' || userId == '105159' || userId == '105160')))
              }
            >
              <IconSvg type="menu_sync" viewBox={void 0} style={{ width: 24, height: 24, marginRight: 5 }} />
              {nsId ? 'Re-' : ''}Sync to NS
            </ThemeOutlineButton>
          </Tooltip>
          {nsId && (
            <Dropdown
              overlay={
                <HeaderActionMenu>
                  <Menu.Item key="1">
                    <a
                      href={`https://${nsRealmId}.app.netsuite.com/app/accounting/transactions/vendbill.nl?id=${nsId}`}
                      target="_blank"
                    >
                      Open In NetSuite
                    </a>
                  </Menu.Item>
                </HeaderActionMenu>
              }
              trigger={['click']}
              placement="bottomRight"
            >
              <ThemeOutlineButton
                size="large"
                type="primary"
                style={{ padding: '0 6px', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
              >
                <Icon type="down" />
              </ThemeOutlineButton>
            </Dropdown>
          )}
        </Flex>
      )
    }
    return <span />
  }

  onToggleEnterModal = () => {
    this.setState({
      visibleEnterModal: !this.state.visibleEnterModal,
    })

    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.altKey === true && e.shiftKey === true && e.keyCode === 65) {
          this.setState({
            visibleEnterModal: true,
          })
        }
      })
  }

  onStartPurchase = () => {
    this.setState(
      {
        visibleEnterModal: false,
      },
      () => {
        this.setState({
          createShow: true,
        })
      },
    )
  }

  onEnterPurchase = (clientId, data) => {
    this.setState(
      {
        visibleEnterModal: false,
      },
      () => {
        this.props.finalizePurchaseOrder({
          clientId,
          values: data,
        })
      },
    )
    return <span />
  }

  private onClickShow = () => {
    this.setState({
      createShow: true,
    })
  }

  private onCloseNewOrder = () => {
    this.setState({
      createShow: false,
    })
  }

  private onClickCreate = (clientName: string) => {
    if (clientName) {
      const quotedDate = moment(new Date()).format('MM/DD/YYYY')
      const { sellerSetting } = this.props
      let fulfillmentDate = getFulfillmentDate(sellerSetting)
      let data = {
        wholesaleClientId: clientName,
        orderDate: quotedDate,
        quotedDate: quotedDate,
        // deliveryDate: quotedDate, // default for Scheduled Delivery Date to be the same as what you are setting for Quoted Delivery Date
        deliveryDate: fulfillmentDate,
        scheduledDeliveryTime: moment().format('h:mm a'),
        itemList: [],
      }
      this.props.createEmptyOrder(data)
    }
    this.setState({
      createShow: false,
    })
  }

  render() {
    const { order, settings, simplifyVendors } = this.props
    const { visibleEnterModal, createShow } = this.state
    return (
      <>
        <NavHeaderButtonsWrapper>
          <FlexDiv>
            <Item className="header-btns right">
              {order && order.parentOrder == null ? (
                <span>
                  {this.renderQboSyncButton()}
                  {this.renderNsSyncButton()}
                </span>
              ) : (
                <span></span>
              )}
            </Item>
            <Item className="header-btns right">
              <ThemeOutlineButton
                size="large"
                disabled={!this.props.showPDFButton}
                onClick={() => this.props.openPrintModal()}
              >
                View {settings && settings.purchase_title ? settings.purchase_title : 'Purchase Order'}
              </ThemeOutlineButton>
            </Item>

            <ThemeButton
              className="icon enter-purchase"
              size="large"
              type="primary"
              onClick={this.onToggleEnterModal}
            >
              <Icon type="plus-circle" style={{ fontSize: 20 }} />
              Enter Purchase
            </ThemeButton>
            <Dropdown
              overlay={
                <Menu onClick={this.onClickShow}>
                  <Menu.Item key="1">Create Purchase Order</Menu.Item>
                </Menu>
              }
              trigger={['click']}
              placement="bottomRight"
            >
              <ThemeButton className="icon border-noleft" size="large" type="primary">
                <Icon type="plus" />
              </ThemeButton>
            </Dropdown>
            {visibleEnterModal && (
              <EnterPurchaseModal
                {...this.props}
                visible={visibleEnterModal}
                onToggle={this.onToggleEnterModal}
                onStartPurchase={this.onStartPurchase}
                onEnterPurchase={this.onEnterPurchase}
              />
            )}
            {order !== null && (
              <Dropdown
                overlay={
                  <HeaderActionMenu onClick={this.props.handlerDropdownAction}>
                    <Menu.Item key="1" disabled={order.wholesaleOrderStatus == 'CANCEL'}>
                      <IconSvg
                        type="duplicate"
                        viewBox={void 0}
                        style={{ width: 24, height: 24, marginRight: 4, fill: 'transparent' }}
                      />
                      Duplicate Order
                    </Menu.Item>
                    <Menu.Item key="3" disabled={order.wholesaleOrderStatus == 'CANCEL'}>
                      <Icon
                        type="export"
                        style={{ fontSize: 18, color: this.props.theme.theme, marginLeft: 3, marginRight: 7 }}
                      />
                      Export to CSV
                    </Menu.Item>
                    <Menu.Item key="2" disabled={order.wholesaleOrderStatus == 'CANCEL' || order.isLocked}>
                      <Icon
                        type="close"
                        style={{ fontSize: 18, color: this.props.theme.theme, marginLeft: 3, marginRight: 7 }}
                      />
                      Cancel Order
                    </Menu.Item>
                  </HeaderActionMenu>
                }
                placement="bottomLeft"
                trigger={['click']}
              >
                <IconSvg type="more" viewBox={void 0} style={{ width: 24, height: 24, margin: '9px 24px 5px 12px' }} />
              </Dropdown>
            )}
          </FlexDiv>
        </NavHeaderButtonsWrapper>

        {visibleEnterModal && (
          <EnterPurchaseModal
            {...this.props}
            saleItems={this.props.itemList}
            visible={visibleEnterModal}
            onToggle={this.onToggleEnterModal}
            onStartPurchase={this.onStartPurchase}
            onEnterPurchase={this.onEnterPurchase}
            clients={simplifyVendors}
          />
        )}

        <NewOrderFormModal
          theme={this.props.theme}
          visible={createShow}
          onOk={this.onClickCreate}
          onCancel={this.onCloseNewOrder}
          clients={simplifyVendors}
          okButtonName="CREATE"
          modalType="order"
          isNewOrderFormModal={true}
        />
      </>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export const PurchaseNavHeaderButtons = withTheme(
  connect(OrdersModule)(mapStateToProps)(PurchaseNavHeaderButtonsWrapper),
)
