import * as React from 'react'
import { useEffect, useState } from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Theme } from '~/common'
import { AuthUser, OrderDetail, OrderItem, UserRole } from '~/schema'
import { PurchaseNavHeaderButtons } from '../_components/nav-header-buttons'
import { PurcahseHeader } from '../_components/header'
import { Button, Icon, Modal, Form, Select } from 'antd'
import PrintOrderSummary from '../order-details/print-order-summary'
import {
  ratioQtyToBaseQty,
  baseQtyToRatioQty,
  printWindow,
  calPoTotalCost,
  needShowDuplicateConfirmModal,
  updateDisMatchDuplicateOrderItemsForGroup,
  getFulfillmentDate,
  addVendorProductCode,
  printWindowAsync,
  responseHandler,
} from '~/common/utils'
import { Icon as IconSvg } from '~/components'
import { AddEmails, fullButton, PrintModalHeader } from '~/modules/customers/sales/_style'
import { DialogSubContainer, DuplicateConfirmDiv, BoldSpan } from '~/modules/customers/nav-sales/styles'
import { ThemeButton, ThemeModal, ThemeSpin } from '~/modules/customers/customers.style'
import moment from 'moment'
import { OrderService } from '~/modules/orders/order.service'
import { of } from 'rxjs'
import styled from '@emotion/styled'
import _ from 'lodash'

type PurchaseNavHeaderProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
    order: OrderDetail
    orderItems: OrderItem[]
    errorMsg: string
    sendEmailPdf: Function
    syncQBOOrder: Function
    syncNSOrder: Function
    setReceivedAndLock: Function
    currentUser: AuthUser
    propsValue: any
  }

export class PurchaseNavHeaderWrapper extends React.PureComponent<PurchaseNavHeaderProps> {
  state: any
  constructor(props: PurchaseNavHeaderProps) {
    super(props)
    // const { order, vendors } = this.props
    this.state = {
      printOrderSummaryReviewShow: false,
      printOrderSummaryRef: null,
      finishLoadingPrintLogo: this.props.logo == 'default' ? true : false,
      duplicateConfirmModal: false,
      sendEmailModalVisible: false,
    }
  }

  printContent: any = (type: number) => {
    const { order } = this.props
    OrderService.instance.recordPrintActivity('purchaseOrder', order.wholesaleOrderId).subscribe()
    this.setState({
      printOrderSummaryReviewShow: false,
    })
    return this.state.printOrderSummaryRef
  }

  openPrintModal = () => {
    this.setState({
      printOrderSummaryReviewShow: true,
    })
  }

  closePrintModal = () => {
    this.setState({
      printOrderSummaryReviewShow: false,
    })
  }

  sendToVendor = (values: any) => {
    const { receiver, cc } = values
    const { order, orderItems, oneOffItems } = this.props
    const itemList = orderItems.concat(oneOffItems)
    this.props.sendEmailPdf({
      data: {
        orderId: order.wholesaleOrderId,
        itemList,
      },
      to: receiver,
      cc,
    })
  }

  handlerDropdownAction = (e: any) => {
    const { order, orderItems } = this.props
    if (e.key == 1) {
      // // lock order
      // this.props.updateOrderLocked({
      //   wholesaleOrderId: order.wholesaleOrderId,
      //   isLocked: !order.isLocked,
      // })

      //Add new menu item to duplicate order
      const showConfirmModal = needShowDuplicateConfirmModal(orderItems, 2)
      if (showConfirmModal) {
        this.setState({ duplicateConfirmModal: true })
      } else {
        this.duplicatePurchaseOrder()
      }
    } else if (e.key == 2) {
      // cancel order
      this.props.updateOrderInfo({
        wholesaleOrderId: order.wholesaleOrderId,
        status: 'CANCEL',
      })
    } else if (e.key == 3) {
      this.downloadLotExcelFile()
    }
  }

  duplicatePurchaseOrder = () => {
    const { orderItems, order, oneOffItems, relatedBills, sellerSetting } = this.props
    if (!order) return
    var delivery = moment().format('MM/DD/YYYY')

    let newOrderItems = orderItems.concat(oneOffItems)
    newOrderItems = newOrderItems.concat(relatedBills)
    let tempOrderItems = newOrderItems.map((obj) => {
      return {
        wholesaleItemId: obj.itemId,
        modifiers: obj.modifiers,
        extraOrigin: obj.extraOrigin,
        cost: obj.cost,
        pricingUOM: obj.pricingUOM,
        inventoryUOM: obj.inventoryUOM,
        pas: obj.pas,
        note: obj.note,
        displayOrder: obj.displayOrder,
        pricingLogic: obj.pricingLogic,
        pricingGroup: obj.pricingGroup,
        //data for charge and relatedBill items
        itemName: obj.itemName,
        UOM: obj.UOM,
        price: obj.price,
        grossWeight: obj.grossWeight,
        grossVolume: obj.grossVolume,

        //add wholesaleClientId for relatedBill items
        wholesaleClientId: obj.wholesaleClientId,
        billNumber: obj.billNumber,
        billDate: obj.billDate,
        // cost: obj.cost,
        // pricingUOM: obj.pricingUOM,
        // ratio: obj.ratio,
      }
    })
    const data = {
      deliveryDate: delivery,
      scheduledDeliveryTime: moment().format('h:mm a'),
      // orderDate: delivery,
      // quotedDate: delivery,
      // wholesaleCustomerClientId: order.wholesaleClient.clientId,
      userId: order.user ? order.user.userId : '',
      itemList: tempOrderItems,
      wholesaleOrderId: order.wholesaleOrderId,
    }
    this.props.duplicatePurchaseOrder(data)
  }

  changePrintLogoStatus = () => {
    this.setState({
      finishLoadingPrintLogo: true,
    })
  }

  changeDuplicateConfirmModalStatus = () => {
    this.setState({
      duplicateConfirmModal: !this.state.duplicateConfirmModal,
    })
  }

  renderDuplicateText = (orderItems: any) => {
    const dismatchOrderItems = updateDisMatchDuplicateOrderItemsForGroup(orderItems, 2)
    return (
      <DuplicateConfirmDiv>
        <p>The following updates have been made to your duplicated order:</p>
        <ul>
          {dismatchOrderItems.map((orderItem: any) => {
            if (orderItem.duplicateType == 1) {
              return (
                <li>
                  The item <BoldSpan>{orderItem.variety} </BoldSpan>is currently inactive. This item has not been
                  duplicated.
                </li>
              )
            } else if (orderItem.duplicateType == 2) {
              return (
                <li>
                  The original UOM for{' '}
                  <BoldSpan>
                    {orderItem.variety},{orderItem.overrideUOM ? orderItem.overrideUOM : orderItem.UOM},
                  </BoldSpan>
                  is currently not available for use.This item has been duplicated with the default selling UOM.
                  {orderItem.InventoryUOM}
                </li>
              )
            } else if (orderItem.duplicateType == 3) {
              return (
                <li>
                  The original UOM for{' '}
                  <BoldSpan>
                    {orderItem.variety},{orderItem.pricingUOM},
                  </BoldSpan>
                  is currently not available for use.This item has been duplicated with the default price UOM.
                  {orderItem.InventoryUOM}
                </li>
              )
            }
          })}
        </ul>
      </DuplicateConfirmDiv>
    )
  }

  downloadLotExcelFile = () => {
    const { order } = this.props
    if (!order) return
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('purchase-order-items-table'))
    var wscols = [
      { wpx: 200 },
      { wpx: 100 },
      { wpx: 300 },
      { wpx: 150 },
      { wpx: 300 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 300 },
      { wpx: 100 },
    ]

    wb.Sheets.Sheet1['!cols'] = wscols
    const today = moment().format('MM.DD.YYYY')
    const title = `${order.wholesaleOrderId}-${today}.csv`
    XLSX.writeFile(wb, title)
  }

  onCreate = (clientNumber: string) => {
    const quotedDate = moment(new Date()).format('MM/DD/YYYY')
    const { sellerSetting } = this.props
    const fulfillmentDate = getFulfillmentDate(sellerSetting)
    const data = {
      wholesaleClientId: clientNumber,
      orderDate: quotedDate,
      quotedDate: quotedDate,
      // deliveryDate: quotedDate, // default for Scheduled Delivery Date to be the same as what you are setting for Quoted Delivery Date
      deliveryDate: fulfillmentDate,
      scheduledDeliveryTime: moment().format('h:mm a'),
      itemList: [],
      allEmails: []
    }
    this.props.createEmptyOrder(data)
  }

  componentDidMount() {
    this.props.getContacts(this.props.currentOrder.wholesaleClient.clientId);
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.sendingPOEmail === true && nextProps.sendingPOEmail === false) {
      this.setState({
        sendEmailModalVisible: false,
      })
    }
    if (JSON.stringify(this.props.customerContacts) !== JSON.stringify(nextProps.customerContacts)) {
      let allEmails = []
      const emails = nextProps.customerContacts.map(e => {
        return e.email
      })
      if (this.props.currentOrder.wholesaleClient.email) {
        allEmails.push(this.props.currentOrder.wholesaleClient.email)
      }
      allEmails = [...allEmails, ...emails]

      this.setState({
        allEmails: [...new Set(allEmails)]
      })
    }
  }

  render() {
    const {
      order,
      errorMsg,
      orderItems,
      oneOffItems,
      currentUser,
      sellerSetting,
      printSetting,
      currentProducts,
      simplifyVendors,
    } = this.props
    const { sendEmailModalVisible, allEmails } = this.state
    //if printSetting exist and get purchase_enabled
    const _printSetting = printSetting ? JSON.parse(printSetting) : false
    const purchaseEnabled = _printSetting ? _printSetting.purchase_enabled : false

    const formattedOneOffItems = oneOffItems.map((el: any) => {
      return { ...el, receivedQty: el.catchWeightQty, orderWeight: el.catchWeightQty }
    })
    const orderItemsWithVendorProductInfo = addVendorProductCode(orderItems, currentProducts, 'itemId')
    const totalItems = orderItemsWithVendorProductInfo.concat(formattedOneOffItems)
    const { finishLoadingPrintLogo, duplicateConfirmModal } = this.state
    const companyName = order.wholesaleClient ? order.wholesaleClient.wholesaleCompany.companyName : ''
    const company = order.wholesaleClient ? order.wholesaleClient.wholesaleCompany : ''
    const companyAddress = ''
    let totalData = calPoTotalCost(totalItems)
    let totalUnit = totalData != null ? totalData.totalUnit : 0
    let subTotal = totalData != null ? totalData.subTotal : 0

    return (
      <>
        <ThemeModal
          title={`Duplicate order alert`}
          visible={duplicateConfirmModal}
          onCancel={this.changeDuplicateConfirmModalStatus}
          bodyStyle={{ padding: 0 }}
          footer={
            <DialogSubContainer>
              <ThemeButton type="primary" onClick={this.duplicatePurchaseOrder}>
                OK
              </ThemeButton>
            </DialogSubContainer>
          }
          width={600}
        >
          {this.renderDuplicateText(orderItems)}
        </ThemeModal>
        {currentUser.accountType != UserRole.WAREHOUSE && (
          <PurchaseNavHeaderButtons
            clients={simplifyVendors}
            order={order}
            openPrintModal={this.openPrintModal}
            handlerDropdownAction={this.handlerDropdownAction}
            syncedQboBills={this.props.syncedQboBills}
            syncQBOOrder={this.props.syncQBOOrder}
            syncNSOrder={this.props.syncNSOrder}
            settings={_printSetting}
            showPDFButton={_printSetting && purchaseEnabled}
            onCreate={this.onCreate}
            showSyncModal={this.props.showSyncModal}
          />
        )}
        <PurcahseHeader
          order={order}
          orderItems={orderItems}
          oneOffItems={oneOffItems}
          errorMsg={errorMsg}
          setReceivedAndLock={this.props.setReceivedAndLock}
          propsValue={this.props.propsValue}
        />

        {sendEmailModalVisible && (
          <WrappedSendEmail
            allEmails={ allEmails }
            onCancel={() => this.setState({ sendEmailModalVisible: false })}
            receiver={this.props.currentOrder.wholesaleClient.mainContact.email}
            sendEmailLoading={this.props.sendingPOEmail}
            clientId={this.props.currentOrder.wholesaleClient.clientId}
            handleSendEmail={(values: any) => {
              this.sendToVendor(values)
            }}
          />
        )}
        <Modal
          width={1120}
          footer={null}
          visible={this.state.printOrderSummaryReviewShow}
          onCancel={this.closePrintModal}
          wrapClassName="print-modal"
        >
          <PrintModalHeader>
            <ThemeButton
              size="large"
              onClick={() => printWindowAsync('PrintOrderSummaryModal', this.printContent.bind(this))}
            >
              <Icon type="printer" theme="filled" />
              Print Purchase Order
            </ThemeButton>

            <ThemeButton
              size="large"
              onClick={() => this.setState({ sendEmailModalVisible: true })}
              style={{ marginRight: 10 }}
            >
              <Icon type="mail" theme="filled" />
              Email Purchase Order
            </ThemeButton>
            <div className="clearfix"></div>
          </PrintModalHeader>
          {printSetting ? (
            <div id={'PrintOrderSummaryModal'}>
              <PrintOrderSummary
                totalUnit={totalUnit}
                subTotal={subTotal}
                shippingCost={order.shippingCost}
                orderItems={totalItems}
                order={order}
                companyName={companyName}
                companyAddress={companyAddress}
                sellerSetting={this.props.sellerSetting}
                printable={this.props.printSetting}
                company={company}
                logo={this.props.logo}
                changePrintLogoStatus={this.changePrintLogoStatus}
                ref={(el) => {
                  this.setState({ printOrderSummaryRef: el })
                }}
              />
            </div>
          ) : (
            <></>
          )}
          {/* <CustomButton
            style={fullButton}
            onClick={() => printWindow('PrintOrderSummaryModal', this.printContent.bind(this))}
            disabled={!finishLoadingPrintLogo}
          >
            <Icon type="printer" theme="filled" />
            Generate PDF / Print
          </CustomButton> */}
        </Modal>
        <table
          id="purchase-order-items-table"
          className="uk-report-table table table-striped"
          style={{ display: 'none' }}
        >
          <thead>
            <tr>
              <th>Delivery Date</th>
              <th>PO #</th>
              <th>Vendor</th>
              <th>SKU</th>
              <th>Item Name</th>
              <th>Units Ordered</th>
              <th>Brand</th>
              <th>Location</th>
              <th>Packing</th>
              <th>UOM</th>
              <th>UPC</th>
              <th>Vendor Location</th>
              <th>Temperature</th>
            </tr>
          </thead>
          <tbody>
            {orderItemsWithVendorProductInfo.map((row: any, index: number) => {
              return (
                <tr key={`row-${row.wholesaleOrderItemId}`}>
                  <td>{moment(row.deliveryDate).format('MM/DD/YYYY')}</td>
                  <td>{order ? order.wholesaleOrderId : ''}</td>
                  <td>{order && order.wholesaleClient ? order.wholesaleClient.clientCompany.companyName : ''}</td>
                  <td>{row['SKU']}</td>
                  <td>{row['variety']}</td>
                  <td>{row['quantity']}</td>
                  <td>{row['modifiers']}</td>
                  <td>{row['itemLocation'] ? row['itemLocation'] : ''}</td>
                  <td>{row['packing']}</td>
                  <td>{row['UOM']}</td>
                  <td>{row['upc']}</td>
                  <td>{row['customField1'] ? row['customField1'] : ''}</td>
                  <td>{row['temperature']}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    )
  }
}

const SendEmailModal: React.FC<any> = ({
  title,
  onCancel,
  receiver,
  form,
  handleSendEmail,
  sendEmailLoading,
  clientId,
  allEmails
}) => {
  const { getFieldDecorator, validateFields } = form
  const [loading, setLoading] = useState(false)
  const [emails, setEmails] = useState(allEmails ?? [])
  const [latestInvoiceEmail, setLatestInvoiceEmail] = useState({
    cc: [],
    receiver: [],
  })

  useEffect(() => {
    setLoading(true)
    OrderService.instance.getLatestInvoiceEmail(clientId).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        if (res.body.data) {
          setLatestInvoiceEmail({
            cc: res.body.data.cc ? _.compact(res.body.data.cc.split(',')) : [],
            receiver: _.compact(res.body.data.receiver.split(',')),
          })
        }
      },
      complete() {
        setLoading(false)
      },
    })
  }, [])

  useEffect(() => {
    const initialValue = latestInvoiceEmail.receiver.length
      ? latestInvoiceEmail.receiver
      : receiver
        ? [receiver]
        : [];
    let notAddedEmails = [];
    if (allEmails) {
      allEmails.forEach(e => {
        if (initialValue.indexOf(e) < 0) {
          notAddedEmails.push(e)
        }
      })
    }
    setEmails(notAddedEmails)
  }, [latestInvoiceEmail, receiver, allEmails])

  const handleSubmit = () => {
    validateFields((err, values) => {
      if (!err) {
        handleSendEmail(values)
      }
    })
  }

  const onAddEmail = (email: string) => {
    let receiver = form.getFieldValue('receiver')
    receiver.push(email);
    const found = emails.indexOf(email);
    emails.splice(found, 1);
    setEmails(emails)
    form.setFieldsValue({
      receiver,
    });
  }

  const onDeSelect = (receiver: string) => {
    emails.push(receiver)
    setEmails(emails)
  }

  const CustomSelect = styled(Select)`
    .ant-select-selection__choice {
      border-radius: 20px;
      background: #dfdfdf;
    }
    .ant-select-selection__choice__content {
      line-height: 15px;
      overflow: initial;
    }
  `

  return (
    <Modal
      width={800}
      visible
      title="Email Purchase Order"
      onCancel={onCancel}
      footer={[
        <Button key="back" onClick={onCancel}>
          Cancel
        </Button>,
        <ThemeButton key="submit" type="primary" loading={sendEmailLoading} onClick={handleSubmit}>
          {!sendEmailLoading && (
            <IconSvg
              type="send-email"
              viewBox="0 0 1024 1024"
              width="18"
              height="18"
              style={{ marginRight: 5, verticalAlign: 'sub' }}
            />
          )}
          Send {title}
        </ThemeButton>,
      ]}
      cancelText="Cancel"
    >
      <ThemeSpin spinning={loading}>
        <Form layout="vertical">
          <Form.Item label="Send to" className={emails.length ? 'no-error' : ''}>
            {getFieldDecorator('receiver', {
              initialValue: latestInvoiceEmail.receiver.length
                ? latestInvoiceEmail.receiver
                : receiver
                ? [receiver]
                : [],
              rules: [
                {
                  required: true,
                  message: 'Please input recipient',
                },
              ],
            })(<CustomSelect mode="tags" placeholder="" dropdownStyle={{ display: 'none' }} onDeselect={onDeSelect} />)}
          </Form.Item>
          {emails.length > 0 && (
            <AddEmails>
              Add:
              {emails.map((e: string, index: number) => {
                return (
                  <a key={`email-${index}`} onClick={() => onAddEmail(e)}>{e}</a>
                )
              })}
            </AddEmails>
          )}
          <Form.Item label="CC">
            {getFieldDecorator('cc', {
              initialValue: latestInvoiceEmail.cc,
              rules: [],
            })(<CustomSelect mode="tags" placeholder="" dropdownStyle={{ display: 'none' }} />)}
          </Form.Item>
        </Form>
      </ThemeSpin>
    </Modal>
  )
}
const WrappedSendEmail = Form.create()(SendEmailModal)

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.orders,
    currentUser: state.currentUser,
  }
}
export const PurchaseNavHeader = withTheme(connect(OrdersModule)(mapStateToProps)(PurchaseNavHeaderWrapper))
