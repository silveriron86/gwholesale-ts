import * as React from 'react'
import { withTheme } from 'emotion-theming'
import { connect } from 'redux-epics-decorator'
import { Popover, Icon } from 'antd'
import { ThemeButton, ThemeInput, ThemeOutlineButton, ThemeTable } from '~/modules/customers/customers.style'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Theme } from '~/common'
import { OrderDetail, Address } from '~/schema'
import EditableTable from '~/components/Table/editable-table'
import { history } from '~/store/history'

const bottomRow = {
  wholesaleAddressId: 0,
  address: null,
  name: '',
  street: '',
  city: '',
  state: '',
  zip: ''
}

type ShippingPopoverProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
    order: OrderDetail
    visible: boolean
    onVisibleChange?: ((visible: boolean) => void) | undefined
  }

export class ShippingPopover extends React.PureComponent<ShippingPopoverProps> {
  columns: ({ title: string; dataIndex: string; editable: boolean; edit_width: number; render?: undefined } | { title: string; dataIndex: string; editable?: undefined; edit_width?: undefined; render?: undefined } | { ... })[]
  constructor(props: ShippingPopoverProps) {
    super(props)

    this.columns = [
      {
        title: 'NAME',
        dataIndex: 'name',
        editable: true,
        edit_width: 100,
      },
      {
        title: 'STREET',
        dataIndex: 'street',
        editable: true,
        edit_width: 100,
      },
      {
        title: 'CITY',
        dataIndex: 'city',
        editable: true,
        edit_width: 100,
      },
      {
        title: 'STATE',
        dataIndex: 'state',
        editable: true,
        edit_width: 100,
      },
      {
        title: 'ZIP',
        dataIndex: 'zip',
        editable: true,
        edit_width: 100,
      },
      {
        title: '',
        dataIndex: '',
        render: (text: any, record: any) => {
          const { order } = this.props
          if (record.wholesaleAddressId > 0) {
            return (
              <ThemeOutlineButton shape="round" onClick={this.onSetDestinationClick.bind(this, record)}>
                {order && order.shippingAddress && order.shippingAddress.wholesaleAddressId == record.wholesaleAddressId && <Icon type="star" theme="filled" />}
              Set Destination</ThemeOutlineButton>
            )
          }
        }
      },
    ]
  }

  getAddressList = () => {
    const { shippingAddresses } = this.props
    let data: any[] = []
    shippingAddresses.forEach(el => {
      let street = ''
      if (el.address) {
        street += el.address.street1 ? el.address.street1 : ''
        street += el.address.street2 ? ' ' + el.address.street2 : ''
      }
      data.push({
        wholesaleAddressId: el.wholesaleAddressId,
        address: el.address ? el.address : null,
        name: el.address && el.address.name ? el.address.name : '',
        street: street,
        city: el.address ? el.address.city : '',
        state: el.address ? el.address.state : '',
        zip: el.address ? el.address.zipcode : ''
      })
    });
    // data.push(bottomRow)
    return (
      <div style={{ width: 800 }}>
        <ThemeTable columns={this.columns} dataSource={data} rowKey={`wholesaleAddressId`}></ThemeTable>
        <ThemeButton style={{ position: 'absolute', bottom: 13, zIndex: 1 }} onClick={this.moveToSetting}><Icon type='plus' />New Delivery Address</ThemeButton>
      </div>
    )
  }

  handleSave = (record: any) => {
    const address = {
      addressId: record.address ? record.address.addressId : 0,
      name: record.name,
      street1: record.street,
      city: record.city,
      state: record.state,
      zipcode: record.zip
    } as Address
    if (address.name != '' || address.street1 != '' || address.city != '' || address.state != '' || address.zipcode != '')
      this.props.updateCompanyAddress(address)

  }

  moveToSetting = () => {
    history.push("/myaccount/setting")
  }

  onSetDestinationClick = (record: any) => {
    const { order } = this.props
    if (order && record.wholesaleAddressId != 0) {
      this.props.setShippingAddress({ orderId: order.wholesaleOrderId, wholesaleAddressId: record.wholesaleAddressId });
    }
  }

  render() {
    const { visible } = this.props
    return (
      <Popover
        placement="bottom"
        content={this.getAddressList()}
        trigger="click"
        visible={visible}
        onVisibleChange={this.props.onVisibleChange}
      >
        {this.props.children}
      </Popover>

    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export const ShippingPopoverModal = withTheme(connect(OrdersModule)(mapStateToProps)(ShippingPopover))
