import styled from '@emotion/styled'
import { Tabs, Icon, Table, DatePicker, AutoComplete, Modal } from 'antd'
import { mediumGrey, topLightGrey, disabledGrey, lightGrey, mediumGrey2, gray01 } from '~/common'

export const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 8px;
  .ant-btn:nth-child(n+2){
    margin-top:10px;
  }
`

export const AccoutName = styled(AutoComplete)((props) => ({
  fontFamily: 'Arial',
  fontSize: 24,
  letterSpacing: '0.05em',
  color: 'black',
  width: '100%',
  '& input:focus': {
    borderColor: props.theme.primary
  },
  '& input': {
    padding: '0 8px',
  }
}))

export const accountLayout: React.CSSProperties = {
  backgroundColor: 'transparent',
  textAlign: 'left',
  margin: '0px 0px 15px',
}

export const OverviewTabs = styled(Tabs)((props) => ({
  '&': {
    '&.ant-tabs.ant-tabs-card': {
      '.ant-tabs-nav-container': {
        height: 51,
        overflow: 'visible',
        '&::after': {
          height: 5,
          width: 'calc(100% - 4px)',
          margin: '0 1px',
          backgroundColor: props.theme.main,
        },
      },
      '.ant-tabs-nav': {
        width: '100%',
        marginTop: 3,
        '>div': {
          display: 'flex',
          flex: 1,
          '.ant-tabs-tab': {
            flex: 1,
            backgroundColor: topLightGrey,
            color: mediumGrey,
            fontSize: 20,
            fontFamily: 'Arial',
            textAlign: 'center',
            lineHeight: '48px',
            height: 51,
            borderBottomWidth: 0,
            '&.ant-tabs-tab-active': {
              backgroundColor: props.theme.main,
              color: 'white',
            },
          },
        },
      },
    },
  },
}))

export const SubTabs = styled(Tabs)((props) => ({
  '&': {
    '&.ant-tabs.ant-tabs-line': {
      '.ant-tabs-bar': {
        borderBottom: 0,
      },
      '.ant-tabs-nav-container': {
        height: 25,
        background: 'none',
        overflow: 'hidden',
        '&::after': {
          height: 0,
          width: 0,
          margin: 0,
        },
      },
      '.ant-tabs-nav': {
        marginTop: 3,
        '>div': {
          display: 'block',
          '.ant-tabs-tab': {
            backgroundColor: 'transparent',
            color: disabledGrey,
            fontSize: 12,
            height: 25,
            lineHeight: '20px',
            padding: 0,
            borderBottomWidth: 0,
            '&.ant-tabs-tab-active': {
              color: props.theme.primary,
              background: 'transparent',
            },
          },
        },
      },
    },
  },
}))

export const PoDescription = styled('div')({
  fontFamily: 'Arial',
  fontSize: 12,
  lineHeight: '14px',
  textTransform: 'uppercase',
  color: mediumGrey,
})

export const PoDescriptionValue = styled('div')({
  fontFamily: 'Arial',
  fontSize: 15,
  lineHeight: '17px',
  color: 'black',
})

export const PoContent = styled('div')((props) => ({
  backgroundColor: props.theme.lighter,
  width: '100%',
  marginTop: 11,
  padding: '17px 27px',
  borderRadius: 6,
}))

export const poListRow: React.CSSProperties = {
  backgroundColor: 'transparent',
  padding: '16px 0 33px',
}

export const PoDollar = styled('div')((props) => ({
  color: props.theme.primary,
  letterSpacing: '0.05em',
  fontFamily: 'Arial',
  fontSize: 36,
  lineHeight: '108%',
  marginBottom: 9,
}))

export const PoHeaderLabel = styled('div')((props) => ({
  //color: mediumGrey,
  color: gray01,
  letterSpacing: '0.05em',
  fontFamily: 'Arial',
  fontSize: 18,
  lineHeight: '108%',
}))

export const PoLabel = styled('div')((props) => ({
  color: gray01, //color: mediumGrey,
  textTransform: 'capitalize',
  fontFamily: 'Arial',
  fontSize: 12,
  lineHeight: '14px',
  marginBottom: 6,
}))

export const poOverview: React.CSSProperties = {
  marginTop: 20,
  marginBottom: 20,
  padding: '18px 0',
  borderTop: '1px solid black',
  borderBottom: '1px solid black',
  backgroundColor: 'transparent',
}

export const poROverview: React.CSSProperties = {
  marginBottom: 20,
  paddingBottom: 18,
  backgroundColor: 'transparent',
}

export const PoOverviewRow = styled('div')((props) => ({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  height: 20,
  color: gray01,
}))

export const PooValue = styled('div')((props) => ({
  textTransform: 'uppercase',
  fontFamily: 'Arial',
  fontSize: 12,
  lineHeight: '14px',
  //color: mediumGrey,
  color: gray01,
}))

export const PomCol = styled('div')((props) => ({
  backgroundColor: 'transparent',
  borderRadius: 6,
  border: '1px solid #DDDDDD',
  padding: '13px 22px',
  margin: '0px 10px 24px',
}))

export const PomColLabel = styled('div')((props) => ({
  fontSize: 12,
  lineHeight: '12px',
  color: mediumGrey,
}))

export const PomColValue = styled('div')((props) => ({
  fontSize: 20,
  letterSpacing: '0.05em',
  color: mediumGrey,
}))

export const tabSearch: React.CSSProperties = {
  position: 'absolute',
  // width: 216,
  right: 10,
  display: 'flex'
}

export const dmFooter: React.CSSProperties = {
  display: 'flex',
  justifyContent: 'flex-end',
  borderTop: `1px solid ${lightGrey}`,
  marginTop: 20,
  padding: '15px 0 5px',
}

interface ActiveProps {
  active: boolean
}

export const PalletsTableWrapper = styled('div')<ActiveProps>((props) => ({
  marginLeft: 10,
  padding: 5,
  backgroundColor: props.active ? props.theme.main : mediumGrey2,
}))

export const PalltesTableFooter = styled('div')({
  marginTop: 8,
  padding: '0 2px',
})

export const SelectedArrow = styled(Icon)((props) => ({
  fontSize: 60,
  right: -30,
  position: 'absolute',
  display: 'block',
  marginTop: -28,
  color: props.theme.main,
}))

export const GreyTable = styled(Table)((props) => ({
  '&': {
    '.ant-table-column-title': {
      color: mediumGrey2,
    },
  },
}))

export const newDocumentButton: React.CSSProperties = {
  position: 'absolute',
  right: 10,
}

export const CompanyNameAC = styled(AutoComplete)({
  fontFamily: 'Arial',
  fontSize: 14,
  color: 'black',
  width: '100%',
})

export const SalesDatePicker = styled(DatePicker)((props) => ({
  marginTop: 1,
  // borderWidth: 1,
  borderColor: props.theme.primary,
  borderStyle: 'solid'

  '.ant-calendar-picker-input': {
    paddingLeft: 3,
    height: 36,
  },
  '.anticon-calendar': {
    marginRight: 3
  }
}))

export const QBOImage = styled('img')((props) => ({
  width: 38,
  height: 38,
  backgroundColor: props.theme.primary,
  borderRadius: 19,
  float: 'left',
  marginLeft: 40
}))

export const ReceivingTableWrapper = styled('div')((props) => ({
  '.ant-table': {
    'table': {
      width: '100%',
      // overflowX: 'auto',
      // display: 'block',
      // '.react-resizable-handle': {
      //   display: 'none'
      // }
    }
  }
}))

export const PalletLabelWrapper = styled('div')({
  width: '100%',
  fontFamily: 'Arial',
  color: 'black'
  // display: 'flex',
  // justifyContent: 'center'
})

export const PalletBarcodeWrapper = styled('div')({
  width: '25%',
})

export const PalletContentWrapper = styled('div')({
  width: '37%',
})

export const PalletContentItemLabel = styled('div')({
  width: '35%',
})

export const PalletContentItemValue = styled('div')({
  width: '65%',
})

export const LockText = styled('span')({
  color: mediumGrey,
  fontWeight: 'bold',
  marginLeft: '10px',
  paddingTop: '7px',
  fontSize: "16px"
})

export const PurchaseSearchWrapper = styled('div')({
  width: 200,
  '&.inline-block': {
    display: 'inline-block'
  },
  '.clear-icon': {
    display: 'none',
    marginRight: 6
  },
  '& .clear-icon.show': {
    display: 'block'
  }
})

export const ProfitabilityTable = styled('span')({
  '& table': {
    'thead': {
      boxShadow: 'none !important',
    },
    'tbody': {
      'td:last-of-type': {
        borderRight: '1px solid rgb(237, 241, 238)'
      }
    },
    '& .cursor-icon': {
      cursor: 'pointer',
      marginRight: 8
    }
  }
})

export const ProfitabilityItemWrapper = styled('div')({
  '.item-title': {
    fontWeight: 'bold',
    fontSize: 18,
    color: gray01,
    marginBottom: 24
  },
  '.statistic table tbody td': {
    color: `${gray01} !important`,
    fontWeight: 'bold'
  },
  '.ant-table-wrapper': {
    'table': {
      'thead': {
        'th': {
          border: 'none',
          'span.ant-table-column-title': {
            color: 'black !important',
            fontSize: '14px !important'
          }
        }
      },
      'tbody': {
        'td': {
          padding: '2px 16px 2px !important',
          height: `36px !important`,
          border: 'none !important'
        },
        'tr:last-child td': {
          color: `${gray01} !important`,
          fontWeight: 'bold'
        }
      }
    },
    '.ant-table-placeholder': {
      border: 'none'
    }
  }
})

export const PurchaseOrderPricing = styled('div')((props: any) => ({
  '.ant-table': {
    'thead': {
      'th': {
        '&.border-right': {
          borderRight: `1px solid #EDF1EE`
        },
        '&.pl50': {
          paddingLeft: '56px !important'
        }
      }
    },
    'tbody': {
      'td': {
        paddingLeft: '16px !important',
        paddingRight: '16px !important',
        '&.border-right': {
          borderRight: `1px solid #EDF1EE`
        },
        '&.pl50': {
          paddingLeft: '56px !important'
        },
        '&:nth-of-type(8)': {
          borderRight: `1px solid #EDF1EE`
        }
      }
    }

  }
}))

export const PricingAnalysisHeader = styled('div')((props: any) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  fontWeight: 'normal',
  fontSize: '16px',
  lineHeight: '21px',
  color: '#000',
  margin: '20px 0px',
  '& .icon-info': {
    marginLeft: 4,
    'path': {
      fill: props.theme.primary
    }
  }
}))

export const Item = styled('div')((props: any) => ({
  fontWeight: 500,
  fontSize: 15,
  '&.bold-big': {
    fontSize: 20,
    fontWeight: 'bold',
  },
  '&.warning-bold': {
    color: '#EB5757',
    fontWeight: 'bold',
  }
}))

export const ItemValue = styled('div')((props: any) => ({
  fontWeight: 500,
  fontSize: 13
}))

export const PalletTableWrapper = styled('div')((props: any) => ({
  maxHeight: 600,
  '.pallets-table': {
    'th': {
      textAlign: 'left !important',
      paddingLeft: 9
    },
    'table, th, td': {
      border: 'none !important'
    }
  }
}))

export const NewLabelModal = styled(Modal)((props: any) => ({
  '&': {
    '.ant-modal-body': {
      padding: 0
    },
  }
}))

export const CostAllocationsTab = styled('div')((props: any) => ({
  maxWidth: 869,
  paddingBottom: 100,
  hr: {
    marginBlockEnd: 17,
    marginBlockStart: 22,
  },
  h4: {
    fontFamily: 'Museo Sans Rounded',
    fontSize: 16,
    marginBottom: 0,
    color: 'black',
  },
  '.ant-table': {
    marginTop: 18,
    marginBottom: 18,
    '.ant-table-body': {
      svg: {
        path: {
          fill: props.theme.theme
        }
      }
    },
  }
}))
