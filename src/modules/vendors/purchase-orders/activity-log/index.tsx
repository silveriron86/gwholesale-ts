import React, { FC, useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { StyledActivityLogWrapper } from '~/modules/customers/nav-sales/styles'
import { OrderService } from '~/modules/orders/order.service'
import moment from 'moment'
import { checkError } from '~/common/utils'
import { ThemeSpin } from '~/modules/customers/customers.style'
interface NotesSpanProps {
  notes: string[]
}

const NotesSpan: FC<NotesSpanProps> = ({ notes }) => {
  return (
    <span className="bold">
      {notes.map((note, index) => (
        <span key={index}>
          {note}
          {index !== notes.length - 1 && ', '}
        </span>
      ))}
    </span>
  )
}

const ActivityLog: FC = () => {
  const { orderId } = useParams<{ orderId: string }>()

  const [dataSource, setDataSource] = useState<LogItem[]>([])

  const [loading, setLoading] = useState(true)

  useEffect(() => {
    OrderService.instance.getActivityLog(orderId).subscribe({
      next(resp: any) {
        if (resp.statusCode !== 'BAD_REQUEST') {
          setDataSource(resp.body.sort((item1: LogItem, item2: LogItem) => item2.time - item1.time))
        }
      },
      error(err) {
        checkError(err)
      },
      complete() {
        setLoading(false)
      },
    })
  }, [orderId])

  const getTimeString = (logItem: LogItem) => {
    const momentTime = moment(logItem.time)

    const date = momentTime.format('M/D/YY')
    const hour = momentTime.hour()
    const minute = momentTime.minute()

    const hourString = hour >= 12 ? hour - 12 : hour
    const minuteString = minute >= 10 ? minute : '0' + minute
    const meridiem = hour >= 12 ? 'PM' : 'AM'

    return `${date} at ${hourString}:${minuteString} ${meridiem}`
  }

  const getActivityType = (activity: string) => {
    let result = ''
    switch (activity) {
      case 'salesOrderCreation':
        result = 'Sales order created'
        break
      case 'purchaseOrderCreation':
        result = 'Purchase order created'
        break
      case 'pickSheetPrint':
        result = 'Pick sheet printed'
        break
      case 'invoicePrint':
        result = 'Invoice printed'
        break
      case 'BOLPrint':
        result = 'BOL printed'
        break
      case 'shippingManifestPrint':
        result = 'Shipping manifest printed'
        break
      case 'purchaseOrderPrint':
        result = 'Purchase order printed'
        break
      case 'invoiceEmailed':
        result = 'Invoice emailed'
        break
      case 'BOLEmailed':
        result = 'BOL emailed'
        break
      case 'purchaseOrderEmailed':
        result = 'Purchase order emailed'
        break
      case 'orderNSSynced':
        result = 'Order synced to NS'
        break
      case 'orderQBOSynced':
        result = 'Order synced to QuickBooks'
        break
      default:
        break
    }
    return result
  }
  return (
    <ThemeSpin spinning={loading}>
      <StyledActivityLogWrapper>
        {dataSource.map((logItem: LogItem) => (
          <div key={logItem.time} className="log-item">
            <div className="time">{getTimeString(logItem)}</div>
            <div className="content">
              <span className="bold">{getActivityType(logItem.activityType)}</span>
              {logItem.note && (
                <span>
                  {' '}
                  to <NotesSpan notes={logItem.note.split(',')} />
                </span>
              )}
              <span> by {logItem.operatorUserName}</span>
            </div>
          </div>
        ))}
      </StyledActivityLogWrapper>
    </ThemeSpin>
  )
}

export default ActivityLog

interface LogItem {
  time: number
  operatorUserName: string
  activityType: string
  note: string | null
}
