import React, { useEffect, useState } from 'react'
import { Input, Tooltip, Icon, Button } from 'antd'
import { Flex, ThemeModal, ThemeOutlineButton, ThemeTable } from '~/modules/customers/customers.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, formatNumber, responseHandler} from '~/common/utils'
import { of } from 'rxjs'
import styled from '@emotion/styled'
import _ from 'lodash'

const { Search } = Input

interface Iprops {
  type: number
  onCancel: () => void
  handleSave: (items: any[]) => void
  orderId: number
  selectedKeys: any
  onToggleEnterModal: Function
  currentOrder: any
  subTotal: number
}

const AllocateCostModal: React.FC<Iprops> = ({ type, onCancel, handleSave, orderId, selectedKeys, onToggleEnterModal, subTotal, enteringPO }) => {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState({})
  const [total, setTotal] = useState(0)
  const [selectedRows, setSelectedRows] = useState<any[]>(selectedKeys)
  const [expandedRowKeys, setExpandedRowKeys] = useState<any[]>([])
  const [query, setQuery] = useState({
    orderId,
    currentPage: 0,
    pageSize: 10,
    searchKey: '',
    greaterThenZero: type === 1 ? 1 : 0,
  })

  const fetchData = () => {
    setLoading(true)
    OrderService.instance.getAllextraPOList(query).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        const concatData = res.body.data.dataList.map((v) => {
          if (_.some(selectedRows, ['wholesaleOrderId', v.wholesaleOrderId])) {
            return { ...v, price: _.find(selectedRows, ['wholesaleOrderId', v.wholesaleOrderId]).price }
          }
          return v
        })
        setData(_.keyBy(concatData, 'wholesaleOrderId'))

        setTotal(res.body.data.total)
      },
      error(err) {
        checkError(err)
      },
      complete() {
        setLoading(false)
      },
    })
  }

  useEffect(() => {
    fetchData()
  }, [query])

  useEffect(() => {
    if (enteringPO === false) {
      fetchData();
    }
  }, [enteringPO])

  const handleChangeAmount = (item: any, price: any, target: any) => {
    if (_.toNumber(price) > item.totalCost) {
      setExpandedRowKeys([...expandedRowKeys, item.wholesaleOrderId])
    } else {
      setExpandedRowKeys(expandedRowKeys.filter((k) => k !== item.wholesaleOrderId))
    }
    setData({ ...data, [item.wholesaleOrderId]: { ...data[item.wholesaleOrderId], price } })

    const position = selectedRows.findIndex((r: any) => r.wholesaleOrderId === item.wholesaleOrderId)
    if (price === '' || price === 0) {
      selectedRows.splice(position, 1)
      if (position >= 0) {
        setSelectedRows(selectedRows)
      }
    } else {
      const newRow = { ...item, price, chargeFrom: item.wholesaleOrderId }
      if (position >= 0) {
        selectedRows[position] = newRow
        setSelectedRows(selectedRows)
      } else {
        setSelectedRows([...selectedRows, newRow])
      }
    }
  }

  const onSave = () => {
    const rows = selectedRows.filter((row: any) => {
      return row.price !== '' && parseFloat(row.price) > 0
    })
    handleSave(rows)
    onCancel()
  }

  const columns = [
    {
      title: 'ORDER NO.',
      align: 'left',
      dataIndex: 'wholesaleOrderId',
    },
    {
      title: 'VENDOR',
      align: 'left',
      dataIndex: 'vendorName',
    },
    {
      title: 'DELIVERY DATE',
      align: 'left',
      dataIndex: 'deliveryDate',
    },
    {
      title: 'STATUS',
      align: 'left',
      dataIndex: 'isLocked',
      render: (t: any) => <Status isLocked={t}>{t ? 'Closed' : 'Open'}</Status>,
    },
    {
      title: 'ORDER TOTAL',
      align: 'right',
      dataIndex: 'totalCost',
      render: (t: number) => `$${formatNumber(t, 2)}`,
    },
    {
      title: (
        <div>
          ALLOCATION
          <Tooltip placement="top" title="You can choose to allocate a portion of this charge, or all of it">
            <Icon type="info-circle" style={{ marginLeft: 10, color: 'rgb(86 131 96)' }} />
          </Tooltip>
        </div>
      ),
      align: 'right',
      dataIndex: 'price',
      render: (t: string | number | readonly string[] | undefined, r: { totalCost: string | undefined }) => {
        return (
          <Input
            value={t}
            placeholder={type === 1 ? r.totalCost : ''}
            prefix={'$'}
            style={{ textAlign: 'right', width: 100 }}
            onChange={(e) => handleChangeAmount(r, e.target.value.replace(/^\D*(\d*(?:\.\d{0,2})?).*$/g, '$1'), e.target)}
          />
        )
      },
    },
  ]

  const rowSelection = {
    onSelect: (record: any, selected: boolean) => {
      const newPrice = type === 1 ? record.totalCost : 0
      if (selected) {
        const oldPrice = record.price;
        const price = (typeof oldPrice === 'undefined' || oldPrice === '') ? newPrice : oldPrice
        setSelectedRows([...selectedRows, { ...record, price, chargeFrom: record.wholesaleOrderId }])
        setData({
          ...data,
          [record.wholesaleOrderId]: {
            ...data[record.wholesaleOrderId],
            price,
          },
        })
      } else {
        setSelectedRows(selectedRows.filter((v) => v.wholesaleOrderId !== record.wholesaleOrderId))
        setData({ ...data, [record.wholesaleOrderId]: { ...data[record.wholesaleOrderId], price: '' } })
      }
    },
    onSelectAll: (selected: any, selectedRow: any, changeRows: _.List<any> | null | undefined) => {
      if (selected) {
        setSelectedRows(_.uniqBy([...selectedRows, ...selectedRow], 'wholesaleOrderId'))
      } else {
        setSelectedRows(_.xorBy(selectedRows, changeRows, 'wholesaleOrderId'))
      }
    },
    selectedRowKeys: selectedRows.map((v: any) => v.wholesaleOrderId),
  }

  let totalAllocated = 0
  selectedRows.forEach((row: any) => {
    if (row.price !== '') {
      totalAllocated += parseFloat(row.price)
    }
  })

  return (
    <Modal
      title={`Allocate costs ${type === 1 ? 'to' : 'from'} this order`}
      width={1080}
      visible
      onCancel={onCancel}
      footer={[
        <span onClick={onCancel} style={{ cursor: 'pointer' }} key="1">
          Back
        </span>,
        <Button type="primary" disabled={selectedRows.length === 0 || totalAllocated === 0 || (type === 2 && totalAllocated > subTotal) || (type === 1 && expandedRowKeys.length > 0)} onClick={onSave} key="2">
          Save allocation
        </Button>,
      ]}
    >
      <Flex className="v-center space-between" style={{marginBottom: 10}}>
        <div style={{fontSize: 16, color: 'black', fontWeight: 400}}>
          {type === 1 ? 'Select at least one purchase order.' : `Total costs to allocate   $${formatNumber(subTotal, 2)}`}
        </div>
        <Flex className="v-center">
          <Search
            placeholder="Search purchases..."
            onSearch={(searchKey) => setQuery({ ...query, searchKey, currentPage: 0 })}
            style={{ width: 200, marginRight: 40 }}
          />
          <ThemeOutlineButton onClick={onToggleEnterModal}>
            <Icon type="plus" /> Enter purchase
          </ThemeOutlineButton>
        </Flex>
      </Flex>
      <Table
        dataSource={_.reverse(Object.values(data))}
        loading={enteringPO || loading}
        columns={columns}
        bordered={false}
        expandedRowKeys={type === 1 ? expandedRowKeys : []}
        expandIconColumnIndex={-1}
        expandIconAsCell={false}
        rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}
        expandedRowRender={(record: any) => (
          <p style={{ margin: 0, textAlign: 'right', color: '#ff0000', fontWeight: 500 }}>
            <Icon type="warning" theme="filled" style={{ marginRight: '5px' }} />
            Allocation amount cannot exceed ${formatNumber(record.totalCost, 2)}
          </p>
        )}
        pagination={{
          total,
          current: query.currentPage + 1,
          pageSize: query.pageSize,
          onChange: (current) => setQuery({ ...query, currentPage: current - 1 }),
        }}
        rowKey="wholesaleOrderId"
      />
      {selectedRows.length > 0 && (
        <div style={{
          position: 'absolute',
          fontSize: 16,
          color: 'black',
          fontWeight: 'bold',
          right: 41,
          marginTop: -70
        }}>
          Total allocated  ${formatNumber(totalAllocated, 2)}
        </div>
      )}
      {(totalAllocated > subTotal && type === 2) && (
        <p style={{ position: 'absolute', right: 20, margin: 0, textAlign: 'right', color: '#ff0000', fontWeight: 500 }}>
          <Icon type="warning" theme="filled" style={{ marginRight: '5px' }} />
          The maximum amount you can allocate is ${formatNumber(subTotal, 2)}.
        </p>
      )}
    </Modal>
  )
}

const ErrorText = styled.div`
  position: absolute;
  right: 20px;
  color: red;
  font-weight: normal;
`

const Status = styled.div<{ isLocked: boolean }>`
  border: 1px solid #ccc;
  border-radius: 15px;
  text-align: center;
  position: relative;
  padding: 4px;
  /* ::after {
    position: absolute;
    content: '';
    left: 8px;
    top: 12px;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    background: ${(props) => (props.isLocked ? '#1c6e31' : '#82898a')};
  } */
`
const Modal = styled(ThemeModal)`
  .ant-table-selection {
    display: none;
  }
  .ant-modal-footer {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 14px 22px;
    background: #f7f7f7;
    color: #418553;
    border-top: 1px solid #d7d7d7;
  }
`

const Table = styled(ThemeTable)`
  .ant-table-row-expand-icon-cell {
    padding: 0;
    width: 0;
  }
  .ant-table-pagination {
    width: 100%;
    text-align: center;
    float: none;
    margin: 34px 0 10px 0;
  }
  .ant-table-thead > tr > th {
    border-bottom: 3px solid #d7d7d7;
    padding: 16px 9px;
  }
  .ant-table-tbody > tr > td {
    background: #fff !important;
    border-bottom: 1px solid #d7d7d7;
  }
`

export default AllocateCostModal
