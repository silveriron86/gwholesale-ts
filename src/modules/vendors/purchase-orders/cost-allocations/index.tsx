import * as React from 'react'
import { Theme } from '~/common'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Flex } from '~/modules/customers/nav-sales/styles'
import { connect } from 'react-redux'
import _ from 'lodash'
import moment from 'moment'
import { ThemeButton, ThemeOutlineButton, ThemeSpin } from '~/modules/customers/customers.style'
import { CostAllocationsTab } from '../_style'
import CostAllocationSection from './section'
import AllocateCostModal from './allocate-modal'
import EnterPurchaseModal from '~/modules/orders/components/enter-purchase-modal'
import { calPoTotalCost, calcOneOffTotal, getFulfillmentDate } from "~/common/utils";
import AllocateExtraCharges from '../pricing-analysis/AllcateExtraChargesModal'
import { NewOrderFormModal } from '~/modules/orders/components'

type CostAllocationsProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
  }

class PurchaseOrderCostAllocations extends React.PureComponent<CostAllocationsProps> {
  state = {
    allocateModalVisible: false,
    visibleEnterModal: false,
    createShow: false,
    allocationSettingModalVisible: false,
    type: 1
  }

  componentDidMount() {
    this.props.getSimplifyVendors(true)
    // pls use url id
    this.props.getAllocationsToOrder(this.props.match.params.orderId)
    this.props.getAllocationsFromOrder(this.props.match.params.orderId)
  }

  componentWillReceiveProps(nextProps: CostAllocationsProps) {
    const orderId = this.props.currentOrder.wholesaleOrderId
    if (!this.props.gettingAllocationsToOrder&& nextProps.gettingAllocationsToOrder) {
      this.props.getAlloationData(orderId)
    }
  }

  toggleAllocateModal = (type: number) => {
    this.setState({
      type,
      allocateModalVisible: !this.state.allocateModalVisible
    })
  }

  onSaveAllocation = (selectedRows: any[], method: number = 1) => {
    const { type } = this.state
    const { currentOrder, allocationsToOrder, allocationsFromOrder } = this.props

    let orderList: any[] = []
    if (method === 1) {
      // add or update
      const allocations = type === 1 ? allocationsToOrder : allocationsFromOrder

      selectedRows.forEach((row: any) => {
        const { price, chargeFrom } = row
        const found = allocations.find((a: any) => a.orderNo === row.wholesaleOrderId)
        if (found !== undefined) {
          // update
          orderList.push({
            id: found.id,
            price,
            chargeFrom,
            method: 2
          })
        } else {
          // add
          orderList.push({
            price,
            chargeFrom,
            method: 1
          })
        }
      })
      allocations.forEach((a: any) => {
        const found = selectedRows.find((row: any) => a.orderNo === row.wholesaleOrderId)
        if (found === undefined) {
          // delete
          orderList.push({
            id: a.id,
            method: 3
          })
        }
      })
    } else {
      // delete
      selectedRows.forEach((row: any) => {
        const { id } = row
        orderList.push({
          id,
          method: 3
        })
      })
    }

    const arg = {
      orderId: currentOrder.wholesaleOrderId,
      data: {
        type,
        orderList
      }
    }

    if (type === 1) {
      this.props.saveAllocationsTo(arg)
    } else {
      this.props.saveAllocationsFrom(arg)
    }
  }

  onDeleteAllocation = (row: any, type: number) => {
    this.setState({
      type,
    }, () => {
      this.onSaveAllocation([row], 3)
    })
  }

  onToggleEnterModal = () => {
    this.setState({
      visibleEnterModal: !this.state.visibleEnterModal
    })
  }

  onStartPurchase = () => {
    this.setState(
      {
        visibleEnterModal: false,
      },
      () => {
        this.setState({
          createShow: true,
        })
      },
    )
  }

  onCloseNewOrder = () => {
    this.setState({
      createShow: false,
    })
  }

  onClickCreate = (clientName: string) => {
    if (clientName) {
      const quotedDate = moment(new Date()).format('MM/DD/YYYY')
      const { sellerSetting } = this.props
      let fulfillmentDate = getFulfillmentDate(sellerSetting)
      let data = {
        wholesaleClientId: clientName,
        orderDate: quotedDate,
        quotedDate: quotedDate,
        // deliveryDate: quotedDate, // default for Scheduled Delivery Date to be the same as what you are setting for Quoted Delivery Date
        deliveryDate: fulfillmentDate,
        scheduledDeliveryTime: moment().format('h:mm a'),
        itemList: [],
      }
      this.props.createEmptyOrder(data)
    }
    this.setState({
      createShow: false,
    })
  }

  onEnterPurchase = (clientId: any, data: any) => {
    this.setState(
      {
        visibleEnterModal: false,
      },
      () => {
        this.props.enterPurchaseOrderForCostAllocation({
          clientId,
          values: data,
        })
      },
    )
  }

  toggleAllocationSettings = () => {
    this.setState({
      allocationSettingModalVisible: !this.state.allocationSettingModalVisible
    })
  }

  render() {
    const {
      simplifyVendors,
      orderItems,
      oneOffItems,
      currentOrder,
      allocationsFromOrder,
      allocationsToOrder,
      gettingAllocationsFromOrder,
      gettingAllocationsToOrder,
      enteringPO
    } = this.props
    const { allocateModalVisible, visibleEnterModal, allocationSettingModalVisible, type, createShow } = this.state
    const totalData = calPoTotalCost(orderItems)
    const subTotal = totalData != null ? totalData.subTotal : 0
    const oneOffTotal = calcOneOffTotal(oneOffItems)
    const orderTotal = subTotal + oneOffTotal

    const selectedKeys = type === 1 ? allocationsToOrder : allocationsFromOrder

    return (
      <CostAllocationsTab style={{ padding: '0 40px 100px 25px' }} className="one-off-table">
        <div className="tab-header">
          <Flex className="v-center">
            <h3>Cost Allocations</h3>
          </Flex>
        </div>
        <Flex className="v-center space-between">
          <h4>Allocate costs to this order</h4>

          <Flex className="v-center">
            {/* <ThemeButton style={{marginRight: 20}} onClick={this.onToggleEnterModal}>Enter purchase</ThemeButton> */}
            <ThemeOutlineButton onClick={this.toggleAllocationSettings}>Allocation settings</ThemeOutlineButton>
          </Flex>
        </Flex>
        <ThemeSpin spinning={gettingAllocationsToOrder}>
          <CostAllocationSection
            data={allocationsToOrder}
            onDelete={(row: any) => this.onDeleteAllocation(row, 1)}
            selectPurcahseOrders={() => this.toggleAllocateModal(1)}
          />
        </ThemeSpin>

        <hr />

        <h4>Allocate costs from this order</h4>
        <ThemeSpin spinning={gettingAllocationsFromOrder}>
          <CostAllocationSection
            data={allocationsFromOrder}
            onDelete={(row: any) => this.onDeleteAllocation(row, 2)}
            selectPurcahseOrders={() => this.toggleAllocateModal(2)}
          />
        </ThemeSpin>

        {allocateModalVisible && (
          <AllocateCostModal
            type={type}
            enteringPO={enteringPO}
            onCancel={() => this.toggleAllocateModal(type)}
            handleSave={this.onSaveAllocation}
            onToggleEnterModal={this.onToggleEnterModal}
            orderId={currentOrder.wholesaleOrderId}
            selectedKeys={selectedKeys.map((v: any) => {
              return {
                ...v,
                wholesaleOrderId: v.orderNo
              }
            })}
            subTotal={orderTotal}
            currentOrder={currentOrder}
          />
        )}

        {allocationSettingModalVisible && (
          <AllocateExtraCharges
            onCancel={this.toggleAllocationSettings}
            orderId={currentOrder.wholesaleOrderId}
            currentOrder={currentOrder}
            currentOrderItems={orderItems}
            updatePOItemAllocate={(v: any) => {
              this.props.updatePOItemAllocate(v)
              this.props.getPurchaseOrderProfitability(currentOrder.wholesaleOrderId)
              this.props.getOrderItemsById(currentOrder.wholesaleOrderId)
              this.props.getAllocationPallets(currentOrder.wholesaleOrderId)
            }}
            oneOffItems={oneOffItems}
            relatedBills={[]}
            getAlloationData={this.props.getAlloationData}
          />
        )}

        {visibleEnterModal && (
          <EnterPurchaseModal
            {...this.props}
            saleItems={this.props.itemList}
            clients={simplifyVendors}
            visible={visibleEnterModal}
            onToggle={this.onToggleEnterModal}
            onStartPurchase={this.onStartPurchase}
            onEnterPurchase={this.onEnterPurchase}
            noText={true}
            hideSaveConfirm
          />
        )}

        <NewOrderFormModal
          theme={this.props.theme}
          visible={createShow}
          onOk={this.onClickCreate}
          onCancel={this.onCloseNewOrder}
          clients={simplifyVendors}
          okButtonName="CREATE"
          modalType="order"
          isNewOrderFormModal={true}
        />
      </CostAllocationsTab>
    )
  }
}

export default connect(({ orders }) => ({ sellerSetting: orders.sellerSetting }))(PurchaseOrderCostAllocations)
