import * as React from 'react'
import { Icon, Table, Popconfirm } from 'antd'
import { Link } from 'react-router-dom'
import { Icon as IconSVG } from '~/components'
import _ from 'lodash'
import { ThemeTextButton } from '~/modules/customers/customers.style'
import { formatNumber } from '~/common/utils'

type CostAllocationSectionProps = {
  data: any[]
  onDelete: Function
  selectPurcahseOrders: Function
}

export default class CostAllocationSection extends React.PureComponent<CostAllocationSectionProps> {
  render() {
    const { data } = this.props

    const columns = [
      {
        title: 'Vendor',
        dataIndex: 'companyName',
      },
      {
        title: 'Order no.',
        dataIndex: 'orderNo',
        render: (wholesaleOrderId: string) => {
          return <Link to={`/order/${wholesaleOrderId}/purchase-cart`}>{wholesaleOrderId}</Link>
        }
      },
      {
        title: 'Vendor reference no.',
        dataIndex: 'vendorReferenceNo',
      },
      {
        title: 'Allocated cost',
        dataIndex: 'price',
        align: 'right',
        render: (value: number) => {
          return `$${formatNumber(value, 2)}`
        }
      },
      {
        dataIndex: 'id',
        render: (id: number, row: any) => {
          return (
            <Popconfirm
              trigger={'click'}
              title={'Are you sure to delete?'}
              onConfirm={() => this.props.onDelete(row)}
              okText="Yes"
              cancelText="No"
            >
              <IconSVG type="trash" viewBox="0 0 24 24" width={20} height={20} style={{cursor: 'pointer'}} />
            </Popconfirm>
          )
        }
      }
    ];

    return (
      <>
        <Table dataSource={data} columns={columns} pagination={false} />
        <ThemeTextButton className="main" onClick={this.props.selectPurcahseOrders}>
          <Icon type="plus" /> Select purchase orders
        </ThemeTextButton>
      </>
    )
  }
}
