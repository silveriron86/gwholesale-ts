import React from 'react'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import PageLayout from '~/components/PageLayout'
import { PurcahseHeader } from '../_components/header'
import { RouteComponentProps } from 'react-router'
import { Row, Col } from 'antd'
import { CustomersInfoBody } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { CustomerDetailsTitle as DetailsTitle, CustomerDetailsWrapper as DetailsWrapper } from '~/modules/customers/customers.style'
import MemoTotalTable from '~/modules/customers/sales/credit-memo/memo-total-table'

type CreditMemoProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

export class CreditMemo extends React.PureComponent<CreditMemoProps> {
  componentDidMount() {
    const orderId = this.props.match.params.orderId
    this.props.getOrderDetail(orderId)
  }

  render() {
    const { currentOrder } = this.props
    if (!currentOrder) {
      return <PageLayout currentTopMenu={'menu-Purchasing-Purchase Orders'} />
    }
    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Purchase Orders'}>
        <PurcahseHeader order={currentOrder} />
        <CustomersInfoBody>
          <DetailsWrapper>
            <Row>
              <Col>
                <DetailsTitle>Credit Memo</DetailsTitle>
              </Col>
            </Row>
            <Row>
              <Col>
                <MemoTotalTable orderId={this.props.match.params.orderId} isPurchase={true} />
              </Col>
            </Row>
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(CreditMemo))
