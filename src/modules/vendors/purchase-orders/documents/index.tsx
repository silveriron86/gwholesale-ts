import * as React from 'react'
import { GlobalState } from '~/store/reducer'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { CustomersInfoBody } from '~/modules/customers/components/customers-detail/customers-detail.style'
import PageLayout from '~/components/PageLayout'
import DocumentsContainer from '~/modules/customers/accounts/documents/documents-container'
import { PurchaseNavHeader } from '../_components/nav-header'
import { RouteComponentProps } from 'react-router'
import { OrdersModule, OrdersDispatchProps, OrdersStateProps } from '~/modules/orders'
import moment from 'moment'
import { DetailsTitle } from '~/modules/customers/customers.style'

type PurchaseDocumentsProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

export default class PurchaseDocuments extends React.PureComponent<PurchaseDocumentsProps> {
  state = {
    from: moment().subtract(30, 'days'),
    to: moment(),
    curPage: 0,
    pageSize: 10,
    search: '',
  }
  componentDidMount() {
    // const orderId = this.props.match.params.orderId
    // this.props.getOrderDetail(orderId)
    // this.props.getOrderItemsById(orderId)
    this.props.resetDocuments()
    this.getPurchaseDocument()
    this.props.getSellerSetting()
  }

  // componentWillReceiveProps(nextProps: PurchaseDocumentsProps) {
  //   if (nextProps.message !== this.props.message) {
  //     if (nextProps.type != null) {
  //       notification[nextProps.type!]({
  //         message: nextProps.type!.toLocaleUpperCase(),
  //         description: nextProps.message,
  //         onClose: nextProps.resetNotif,
  //       })
  //     }
  //   }
  // }

  getPurchaseDocument = () => {
    const orderId = this.props.match.params.orderId
    const searchObj = {
      // from: this.state.from.format('MM/DD/YYYY'),
      // to: this.state.to.format('MM/DD/YYYY'),
      search: this.state.search,
      // curPage: this.state.curPage,
      // pageSize: this.state.pageSize,
    }
    this.props.setDocumentSearchProps(searchObj)
    this.props.getPurchaseOrderDocuments({ ...searchObj, orderId: orderId })
  }

  onSave = (data: Document) => {
    if (data.id) {
      this.props.updatePurchaseOrderDocument(data)
    } else {
      this.props.createPurchaseOrderDocument(data)
    }
  }

  onChange = (type: string, data: any) => {
    if (type == 'search') {
      this.setState({ search: data }, this.getPurchaseDocument)
    }
  }

  render() {
    const { currentOrder, documents, documentsLoading } = this.props
    const { pageSize } = this.state

    if (!currentOrder) {
      return null
    }

    return (
      <div style={{ padding: '15px 40px 0 25px' }}>
        {/* <DetailsTitle>Documents</DetailsTitle> */}
        <DocumentsContainer
          loading={documentsLoading}
          getDocuments={this.getPurchaseDocument}
          documents={documents}
          onSave={this.onSave}
          buttonStatus={currentOrder.wholesaleOrderStatus == 'CANCEL'}
          pageSize={pageSize}
          theme={this.props.theme}
          onlyContent={true}
        />
      </div>
    )
  }
}
