import * as React from 'react'
import { CACHED_NS_LINKED, Theme } from '~/common'
import { textCenter, ThemeIcon, ThemeOutlineButton, ThemeSelect } from '~/modules/customers/customers.style'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { OneOffItemTableWrapper } from '~/modules/customers/accounts/addresses/addresses.style'
import { Icon, Popconfirm, Select } from 'antd'
import { formatNumber, isFloat } from '~/common/utils'
import EditableTable from '~/components/Table/editable-table'
import { OrderItem } from '~/schema'
import lodash from 'lodash'
import { Flex } from '~/modules/customers/nav-sales/styles'
import jQuery from 'jquery'
import ExtraChargeTable from '../../../customers/nav-sales/order-detail/components/extra-charges-table.component'
import { connect } from 'react-redux'

type ExtraChargeProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
  }

class PurchaseOrderExtraCharge extends React.PureComponent<ExtraChargeProps> {
  state = {
    pageSize: 30,
    currentPage: 1,
    oneOffItemList: [],
    addItemClicked: false,
  }

  componentDidMount() {
    this.setOffItemsState(this.props)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.oneOffItems !== nextProps.oneOffItems) {
      this.setOffItemsState(nextProps)
    }
  }

  componentDidUpdate(prevProps: any) {
    if (
      this.state.addItemClicked &&
      prevProps.oneOffItems.length >= 0 &&
      prevProps.oneOffItems.length === this.props.oneOffItems.length - 1
    ) {
      const rows = jQuery('.one-off-table .ant-table-row')
      const allEls = jQuery('.one-off-table .ant-table-row .tab-able')
      const that = this
      if (rows.length > 0) {
        setTimeout(() => {
          const editableEls = jQuery(rows[rows.length - 1]).find('.tab-able')
          if (editableEls.length > 0) {
            setTimeout(() => {
              jQuery(editableEls[0]).trigger('click')
              const index = allEls.index(editableEls[0])
              that.setState({ addItemClicked: false })
              window.localStorage.setItem('CLICKED-INDEX', `${index}`)
            }, 50)
          }
        }, 200)
      }
    }
  }

  onDeleteRow = (id: number) => {
    const dataSource = [...this.state.oneOffItemList]
    this.setState({ dataSource: dataSource.filter((item) => item.wholesaleOrderItemId !== id) })
    this.props.removeOffItem({ wholesaleOrderItemId: id })
  }

  handleSave = (row: OrderItem, field?: string) => {
    this._saveOffItem(row, field)
  }

  _saveOffItem(row: OrderItem, field?: string) {
    const newData = [...this.state.oneOffItemList]
    const index = newData.findIndex((item: any) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })

    this.setState({ dataSource: newData })
    row.quantity = row.quantity ? row.quantity : 0
    row.picked = row.picked ? row.picked : 0
    row.uom = row.UOM

    // console.log(row)
    this.onUpsetOffItem(row)
  }

  onUpsetOffItem = (data: any) => {
    // update
    if (parseInt(data['wholesaleOrderItemId']) > 0) {
      this.setState({
        oneOffItemList: this.state.oneOffItemList.map((v) => {
          if (v.wholesaleOrderItemId === data.wholesaleOrderItemId) return data
          return v
        }),
      })
      this.props.updateOffItem(data)
    } else {
      this.props.createOffItem(data)
    }
  }

  setOffItemsState = (props: any) => {
    let oneOffItemList = props.oneOffItems.filter((obj: any) => {
      if (!obj.isInventory) {
        obj.uom = obj.UOM
        obj.variety = obj.itemName
        return true
      } else {
        return false
      }
    })
    this.setState({
      oneOffItemList,
    })
  }

  addNewOffItem = (name?: string) => {
    const itemList: OrderItem[] = lodash.cloneDeep(this.state.oneOffItemList)
    const newData = {
      wholesaleOrderItemId: '-' + (itemList.length + 1).toString(),
      constantRatio: true,
      cost: 0,
      picked: 0,
      price: 0,
      quantity: 0,
      status: 'SHIPPED',
      itemName: name ?? '',
      uom: 'each',
      orderId: this.props.match.params.orderId,
    }
    itemList.push(newData)
    this.setState({
      addItemClicked: true,
      oneOffItemList: itemList,
    })

    this.props.createOffItem(newData)
  }

  onAddWithType = (v: number) => {
    console.log('extra charge type = ', v)
  }

  changePage = (page: number) => {
    this.setState({ currentPage: page })
  }

  render() {
    const { currentOrder } = this.props
    const { pageSize, currentPage, oneOffItemList } = this.state
    const editable =
      currentOrder &&
      !currentOrder.isLocked &&
      currentOrder.wholesaleOrderStatus != 'CANCEL'
    const enabledNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const extraChargeColumn: Array<any> = [
      {
        title: 'Charge type',
        dataIndex: 'itemName',
        key: 'itemName',
        align: 'center',
        width: '15%',
        // width: '25%',
        // editable,
        sorter: (a: any, b: any) => a.itemName.localeCompare(b.itemName),
        render: (value: string) => {
          return typeof value !== 'undefined' && value.trim() !== '' ? (
            `${value}`
          ) : (
            <div style={{ height: 20, width: '100%' }} />
          )
        },
      },
      {
        title: 'Description',
        dataIndex: 'desc',
        key: 'desc',
        align: 'center',
        width: '25%',
        editable,
        sorter: (a: any, b: any) => a.desc.localeCompare(b.desc),
        render: (value: string) => {
          return typeof value !== 'undefined' && value.trim() !== '' && value.toLowerCase() != 'null' ? (
            `${value}`
          ) : (
            <div style={{ height: 20, width: '100%' }} />
          )
        },
      },
      {
        title: 'Quantity',
        dataIndex: 'quantity',
        align: 'center',
        key: 'unit',
        editable,
        width: 150,
        type: 'number',
        sorter: (a: any, b: any) => a.quantity - b.quantity,
        render: (value: number) => {
          return isFloat(value) ? formatNumber(value, 2) : value
        }
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        align: 'center',
        editable,
        width: 190,
        sorter: (a: any, b: any) => a.price - b.price,
        render: (value: number, record: any) => {
          return (
            <div style={textCenter}>
              ${formatNumber(value, 2)}/{record.UOM}
            </div>
          )
        },
      },
      {
        title: 'Total Price',
        dataIndex: 'item_total',
        align: 'center',
        key: 'item_total',
        sorter: (a: any, b: any) => a.item_total - b.item_total,
        render: (data: number, record: any) => {
          const extended = Number(record.price ? record.price : 0) + Number(record.freight ? record.freight : 0)
          let total = 0
          total = extended * record.quantity
          const prefix = total >= 0 ? '' : '-'
          return (
            <div style={textCenter}>
              {prefix}${formatNumber(Math.abs(total), 2)}
            </div>
          )
        },
      },
      {
        title: '',
        key: 'delete',
        render: (_text: any, record: any, index: any) => {
          return editable ? (
            <div>
              <Popconfirm
                title="Permanently delete this one related record?"
                okText="Delete"
                onConfirm={this.onDeleteRow.bind(this, record.wholesaleOrderItemId)}
              >
                <ThemeIcon type="close" style={{ marginLeft: '10px' }} />
              </Popconfirm>
            </div>
          ) : null
        },
      },
    ]

    const extraCharges = this.props.companyProductTypes ? this.props.companyProductTypes.extraCharges : []
    const footer = editable ? (
      <div>
        <div style={{ width: '15%', borderRight: '1px solid #D8DBDB', textAlign: 'center', padding: 12 }}>
          <ThemeSelect placeholder="Select type..." onChange={this.onAddWithType} style={{ width: '100%' }}>
            {extraCharges.map((ec: any) => (
              <Select.Option key={`extra-charge-type${ec.id}`} value={ec.id}>
                {ec.name}
              </Select.Option>
            ))}
          </ThemeSelect>
        </div>
      </div>
    ) : null

    return (
      <div style={{ padding: '0 40px 0 25px' }} className="one-off-table">
        <div className="tab-header">
          <Flex className="v-center">
            <h3>Extra Charges {`(${oneOffItemList.length})`}</h3>
          </Flex>
          {editable && (
            <div>
              <ThemeOutlineButton
                shape="round"
                onClick={() => this.addNewOffItem()}
                className="sales-cart-input add-item header-last-tab bold-blink"
                disabled={!currentOrder || currentOrder.wholesaleOrderStatus === 'CANCEL'}
              >
                <Icon type="plus-circle" />
                Add a Charge
              </ThemeOutlineButton>
            </div>
          )}
        </div>
        <OneOffItemTableWrapper>
          {/* <EditableTable
            className={'nested-head min-height-placeholder extra-charge'}
            columns={extraChargeColumn}
            dataSource={oneOffItemList}
            handleSave={this.handleSave}
            rowKey="wholesaleOrderItemId"
            pagination={{ pageSize: pageSize, current: currentPage, onChange: this.changePage, defaultCurrent: 1 }}
            footer={() => footer}
            allProps={this.props}
          /> */}
          <ExtraChargeTable
            dataSource={oneOffItemList}
            editable={!!editable}
            handleSave={this.handleSave}
            addOne={this.addNewOffItem}
            deleteRow={this.onDeleteRow}
            pagination={{ pageSize: pageSize, current: currentPage, onChange: this.changePage, defaultCurrent: 1 }}
            sellerSetting={this.props.sellerSetting}
          />
        </OneOffItemTableWrapper>
      </div>
    )
  }
}

export default connect(({ orders }) => ({ sellerSetting: orders.sellerSetting }))(PurchaseOrderExtraCharge)
