import * as React from 'react'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import LogisticsForm from './logistics-form'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'

type LogisticsProps = OrdersDispatchProps &
  OrdersStateProps &
{
  theme: Theme
}

export default class Logistics extends React.PureComponent<LogisticsProps> {
  componentDidMount() {
    this.props.getCompanyUsers()
    this.props.getSellerSetting()
    this.props.getAddresses()
    this.getOneOffItems()
  }

  getOneOffItems = () => {
    const orderId = this.props.match.params.orderId
    if (orderId) {
      this.props.getOneOffItems(orderId)
    }
  }

  onSave = (data: any) => {
    const { currentOrder } = this.props
    if (currentOrder) {
      data.wholesaleCustomerClientId = currentOrder.wholesaleClient.clientId
      this.props.updateOrderInfo(data)
    }
  }

  render() {
    const { currentOrder, vendors } = this.props
    if (!currentOrder) {
      return <PageLayout currentTopMenu={'menu-Purchasing-Purchase Orders'} />
    }

    return (
      <div style={{ padding: '15px 40px 0 25px' }}>
        {/* <DetailsTitle>Order Details</DetailsTitle> */}
        {currentOrder && (
          <LogisticsForm order={currentOrder} vendors={vendors} onSave={this.onSave} {...this.props} />
        )}
      </div>
    )
  }
}
