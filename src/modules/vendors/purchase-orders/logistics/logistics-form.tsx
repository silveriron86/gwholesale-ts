import * as React from 'react'

import {
  ThemeInput,
  ThemeSelect,
  ThemeIcon,
  EditA,
  ThemeModal,
  ThemeTextArea,
  ThemeButton,
  Flex,
} from '~/modules/customers/customers.style'
import { Select, DatePicker, TimePicker, AutoComplete, Icon, Form, Divider, Button } from 'antd'
import { LogisticRow, AccountLabel } from '~/modules/customers/sales/_style'
import moment from 'moment'
import { OrderDetail, MainAddress } from '~/schema'
import TypeEditWrapper from '~/modules/settings/tabs/Product/TypeEditWrapper'
import { OrdersDispatchProps, OrdersStateProps } from '~/modules/orders'
import { FormComponentProps } from 'antd/lib/form'
import { CACHED_NS_LINKED, FINANCIAL_TERMS } from '~/common'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import { formatAddress } from '~/common/utils'
import { Icon as IconSvg } from '~/components/icon'
import _ from 'lodash'

const FormItem = Form.Item

// import { formatNumber } from '~/common/utils'
const { Option } = Select
const folderIcon = <IconSvg viewBox="0 0 24 24" width="24" height="24" type="folder" style={{ fill: 'transparent' }} />

type LogisticsFormProps = OrdersDispatchProps &
  FormComponentProps<any> &
  OrdersStateProps & {
    order: OrderDetail
    vendors: any[]
    onSave: Function
  }

export class LogisticsForm extends React.PureComponent<LogisticsFormProps> {
  state: any
  constructor(props: LogisticsFormProps) {
    super(props)
    const { order } = this.props
    this.state = {
      visibleShippingTermsModal: false,
      visibleFreightTypeModal: false,
      visibleCarrierModal: false,
      modalVisible: false,
      editDlgVisible: false,
      editDlgTitle: '',
      editDlgButtonTitle: '',
      selectField: '',
      originAddresses: []
    }
  }

  componentDidMount() {
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
    if (this.props.shippingAddresses) {
      this.setState({
        originAddresses: this.props.shippingAddresses,
      })
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.shippingAddresses && nextProps.shippingAddresses) {
      if (this.props.shippingAddresses.length != nextProps.shippingAddresses.length) {
        this.setState({
          originAddresses: nextProps.shippingAddresses,
        })
      }
    }
  }

  handleVisibleChange = (v: boolean) => {
    this.setState({
      modalVisible: v,
    })
  }

  onEditSelect = (selectField: string) => {
    const { carrier } = this.state
    const { companyProductTypes } = this.props
    let editDlgTitle = ''
    let editDlgButtonTitle = ''

    switch (selectField) {
      case 'costStructure':
        editDlgTitle = 'Cost Structures'
        editDlgButtonTitle = 'Add New Cost structure'
        break
      case 'modeOfTransportation':
        editDlgTitle = 'Mode of transaportation'
        editDlgButtonTitle = 'Add New Mode'
        break
      case 'carrier':
        editDlgTitle = 'Carrier'
        editDlgButtonTitle = 'Add New Carrier'
        break
    }

    this.setState({
      editDlgVisible: true,
      editDlgTitle,
      editDlgButtonTitle,
      selectField,
    })
  }

  handleSelectOk = () => {
    this.setState({
      editDlgVisible: false,
    })
  }

  openCompanySettingChange = (type: string) => {
    let state = { ...this.state }
    state[type] = true
    this.setState(state)
  }

  onCompanySettingTypeModal = (type: string) => {
    let state = { ...this.state }
    state[type] = !this.state[type]
    this.setState(state)
    this.props.getCompanyProductAllTypes()
  }

  handleSearchAddress = (search: string, type: string) => {
    if (type == 'address') {
      const { order } = this.props
      const filtered = _.get(order, 'wholesaleClient.wholesaleAddressList', []).filter(v => v.addressType === 'SHIPPING').filter((el) => {
        return (
          formatAddress(el.address)
            .toLowerCase()
            .indexOf(search.toLowerCase()) > -1
        )
      })
      this.setState({ originAddresses: filtered })
    }
  }

  handleSubmit = (e: any) => {
    const { form, order } = this.props
    e.preventDefault()
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        const { shippingDate, deliveryTime, shippingAddressId } = values
        const data = {
          ...values,
          wholesaleOrderId: order.wholesaleOrderId,
          shippingDate: shippingDate ? moment(shippingDate).format('MM/DD/YYYY') : null,
          deliveryTime: deliveryTime ? moment(deliveryTime).format('MM/DD/YYYY HH:mm') : null,
        }
        if (isNaN(shippingAddressId)) {
          delete data.shippingAddressId
        }
        if (data.shippingTerm && data.shippingTerm.toLowerCase() == 'null') {
          data.shippingTerm = ''
        }
        this.props.onSave(data)
      }
    })
  }

  render() {
    const enabledNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const { order, companyProductTypes, currentCompanyUsers, sellerSetting, shippingAddresses, addresses } = this.props
    const {
      selectField,
      editDlgVisible,
      editDlgTitle,
      editDlgButtonTitle,
      visibleShippingTermsModal,
      visibleFreightTypeModal,
      visibleCarrierModal,
      originAddresses
    } = this.state

    const { getFieldDecorator } = this.props.form

    let defaultAddress = ''
    if (shippingAddresses && shippingAddresses.length > 0) {
      let record = shippingAddresses.find((a: any) => a.isMain === true)
      if (typeof record === 'undefined') {
        record = shippingAddresses[0]
      }
      defaultAddress = `${record.address.department ? record.address.department : ''} ${record.address.street1} ${record.address.city
        } ${record.address.state}, ${record.address.zipcode} ${record.address.country}`
    }

    let deliveryAddresses: MainAddress[] = []
    addresses.forEach((address: MainAddress) => {
      if (address.addressType === 'SHIPPING') {
        deliveryAddresses.push(address)
      }
    })
    const paymentTermsList = companyProductTypes ? companyProductTypes.freightTypes : []

    return (
      <div style={{ paddingBottom: 20 }}>
        <ThemeModal
          title={`Edit Value List "PO | ${editDlgTitle}"`}
          visible={editDlgVisible}
          onCancel={this.handleSelectOk}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          {/* <TypesEditor
            isModal={true}
            field={selectField}
            title={editDlgTitle}
            buttonTitle={editDlgButtonTitle}
          /> */}
          <TypeEditWrapper
            isModal={true}
            field={selectField}
            title={editDlgTitle}
            buttonTitle={editDlgButtonTitle}
            {...this.props}
          />
        </ThemeModal>

        <ThemeModal
          title={`Edit Value List "Shipping Terms"`}
          visible={visibleShippingTermsModal}
          onCancel={this.onCompanySettingTypeModal.bind(this, 'visibleShippingTermsModal')}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor
            isModal={true}
            field="shippingTerms"
            title="Shipping Terms"
            buttonTitle="Add New Shipping Terms"
          />
        </ThemeModal>
        <ThemeModal
          title={`Edit Value List "Payment Terms"`}
          visible={visibleFreightTypeModal}
          onCancel={this.onCompanySettingTypeModal.bind(this, 'visibleFreightTypeModal')}
          cancelText='Close'
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor
            isModal={true}
            field="freightTypes"
            title="Freight Type"
            buttonTitle="Add New Freight Type"
          />
        </ThemeModal>
        <ThemeModal
          title={`Edit Value List "Carrier"`}
          visible={visibleCarrierModal}
          onCancel={this.onCompanySettingTypeModal.bind(this, 'visibleCarrierModal')}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="carrier" title="Carrier" buttonTitle="Add New Carrier" />
        </ThemeModal>
        {/* <Col md={8} style={odPR}>
              <Layout style={{ ...odDestLayout, ...{ backgroundColor: 'white' } }}>
                <Row>
                  <Col md={16}>
                    <AccountLabel>DELIVERY ADDRESS</AccountLabel>
                  </Col>
                </Row>
                <Row>
                  <Col md={18} style={{ paddingRight: 15 }}>
                    <DestinationText>{destination}</DestinationText>
                  </Col>
                  <Col md={6}>
                    <ShippingPopoverModal
                      order={order}
                      visible={modalVisible}
                      onVisibleChange={this.handleVisibleChange}
                    >
                      <ThemeOutlineButton shape="round" style={changeButton}>
                        CHANGE <Icon type="arrow-right" />
                      </ThemeOutlineButton>
                    </ShippingPopoverModal>
                  </Col>
                </Row>
              </Layout>
            </Col> */}
        {/* <div>
          <AccountLabel>VENDOR REFERENCE NO.</AccountLabel>
          <ThemeInput value={po} onChange={this.handleChange.bind(this, 'po')} />
        </div> */}
        <Form onSubmit={this.handleSubmit}>
          {companyProductTypes && (
            <LogisticRow className="large">
              <AccountLabel>Carrier</AccountLabel>
              <Flex className="v-center">
                <FormItem label="" style={{ flex: 1 }}>
                  {getFieldDecorator('carrier', {
                    rules: [{ required: false }],
                    initialValue: order.carrier ? order.carrier : '',
                  })(
                    <ThemeSelect
                      style={{ position: 'relative', width: 250 }}
                      showSearch
                      optionFilterProp="children"
                      onSearch={() => { }}
                      filterOption={(input: any, option: any) =>
                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                      }

                      dropdownRender={(menu: any) => (
                        <div>
                          {menu}
                          <Divider style={{ margin: '4px 0' }} />
                          <div
                            style={{ padding: '4px 8px', cursor: 'pointer' }}
                            onMouseDown={(e) => e.preventDefault()}
                            onClick={(e) => this.openCompanySettingChange('visibleCarrierModal')}
                          >
                            <Icon type="plus" /> Add New Carrier...
                          </div>
                        </div>
                      )}
                    >
                      {companyProductTypes.carrier.sort((a: any, b: any) => a.name.localeCompare(b.name)).map(
                        (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                          return (
                            <Select.Option key={`freight-type-${index}`} value={item.name}>
                              {item.name}
                            </Select.Option>
                          )
                        },
                      )}
                    </ThemeSelect>
                    // <ThemeInput style={{ width: '90%' }} />
                  )}
                </FormItem>
                {/* <Button onClick={() => this.onEditSelect('carrier')} type="link" style={{ marginBottom: 24 }}>
                  {folderIcon}
                </Button> */}
              </Flex>
            </LogisticRow>
          )}
          {/* <Col md={6} style={{ ...odPR, ...odPL }}>
                <AccountLabel>SHIPPING COST</AccountLabel>
                <ThemeInput value={shippingCost} onChange={this.handleChange.bind(this, 'shippingCost')} />
              </Col> */}
          <LogisticRow className="large">
            <AccountLabel>Origin Address</AccountLabel>
            <Flex className="v-center">
              <FormItem style={{ flex: 1 }}>
                {getFieldDecorator('originAddress', {
                  rules: [{ required: false }],
                  initialValue: order.originAddress || formatAddress(_.get(order, 'wholesaleClient.mainShippingAddress.address', undefined)),
                })(
                  <AutoComplete
                    style={{ width: '100%' }}
                    onSearch={(val) => this.handleSearchAddress(val, 'address')}
                    dataSource={_.get(order, 'wholesaleClient.wholesaleAddressList', []).filter(v => v.addressType === 'SHIPPING').map((el: any) => (
                      <AutoComplete.Option key={el.wholesaleAddressId} value={formatAddress(el.address)}>
                        {formatAddress(el.address)}
                      </AutoComplete.Option>
                    ))}
                  />,
                )}
                <Icon type="down" className="custom-dropdown-arrow" viewBox="0 0 12 12" width={12} height={12} />
              </FormItem>
              <Button href={`#/vendor/${_.get(order, 'wholesaleClient.clientId')}/addresses`} type="link" style={{ marginBottom: 24 }}>
                {folderIcon}
              </Button>
            </Flex>
          </LogisticRow>
          <LogisticRow className="large">
            <AccountLabel>Delivery Address</AccountLabel>
            <Flex className="v-center">
              <FormItem style={{ flex: 1 }}>
                {getFieldDecorator('shippingAddressId', {
                  rules: [{ required: false }],
                  initialValue: _.get(order, 'shippingAddress.wholesaleAddressId', defaultAddress),
                })(
                  <ThemeSelect
                    style={{ width: '100%' }}
                    showSearch
                    optionFilterProp="children"
                    onSearch={() => { }}
                    filterOption={(input: any, option: any) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {originAddresses.map((el: MainAddress, index: number) => {
                      if (el.address) {
                        return (
                          <Select.Option key={`sa-${index}`} value={el.wholesaleAddressId}>
                            {formatAddress(el.address)}
                          </Select.Option>
                        )
                      }
                    })}
                  </ThemeSelect>,
                )}
              </FormItem>
              <Button href={`#/myaccount/setting`} type="link" style={{ marginBottom: 24 }}>
                {folderIcon}
              </Button>
            </Flex>
          </LogisticRow>

          <LogisticRow>
            <AccountLabel>Freight Type</AccountLabel>
            <FormItem>
              {getFieldDecorator('freightType', {
                rules: [{ required: false }],
                initialValue: order.freightType ? order.freightType : '',
              })(
                <ThemeSelect
                  style={{ flex: 1 }}
                  showSearch
                  optionFilterProp="children"
                  onSearch={() => { }}
                  filterOption={(input: any, option: any) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  dropdownRender={(menu: any) => (
                    <div>
                      {menu}
                      {!enabledNS &&
                        <>
                          <Divider style={{ margin: '4px 0' }} />
                          <div
                            style={{ padding: '4px 8px', cursor: 'pointer' }}
                            onMouseDown={e => e.preventDefault()}
                            onClick={(e) => this.openCompanySettingChange('visibleFreightTypeModal')}
                          >
                            <Icon type="plus" /> Add New Freight Type...
                          </div>
                        </>
                      }
                    </div>
                  )}
                >
                  {paymentTermsList.sort((a: any, b: any) => a.name.localeCompare(b.name)).map(
                    (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                      return (
                        <Select.Option key={`freight-type-${index}`} value={item.name}>
                          {item.name}
                        </Select.Option>
                      )
                    },
                  )}
                </ThemeSelect>
              )}
            </FormItem>
          </LogisticRow>
          <LogisticRow>
            <AccountLabel>Shipping Terms</AccountLabel>
            <FormItem>
              {getFieldDecorator('shippingTerm', {
                rules: [{ required: false }],
                initialValue: order.shippingTerm && order.shippingTerm.toLowerCase() != 'null' ? order.shippingTerm : '',
              })(
                <ThemeSelect
                  disabled={enabledNS}
                  style={{ width: '100%' }}
                  showSearch
                  optionFilterProp="children"
                  onSearch={() => { }}
                  filterOption={(input: any, option: any) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                  dropdownRender={(menu: any) => (
                    <div>
                      {menu}
                      <Divider style={{ margin: '4px 0' }} />
                      <div
                        style={{ padding: '4px 8px', cursor: 'pointer' }}
                        onMouseDown={(e) => e.preventDefault()}
                        onClick={(e) => this.openCompanySettingChange('visibleShippingTermsModal')}
                      >
                        <Icon type="plus" /> Add New Shipping Terms...
                      </div>
                    </div>
                  )}
                >
                  {companyProductTypes.shippingTerms.sort((a: any, b: any) => a.name.localeCompare(b.name)).map((item: any, index: number) => {
                    return (
                      <Select.Option key={`shipping-term-${index}`} value={item.name}>
                        {item.name}
                      </Select.Option>
                    )
                  })}
                </ThemeSelect>,
              )}
            </FormItem>
          </LogisticRow>

          <LogisticRow>
            <AccountLabel>Carrier Reference No.</AccountLabel>
            <FormItem label="">
              {getFieldDecorator('reference', {
                rules: [{ required: false }],
                initialValue: order.reference ? order.reference : '',
              })(<ThemeInput />)}
            </FormItem>
          </LogisticRow>
          <LogisticRow>
            <AccountLabel>Ship Date</AccountLabel>
            <FormItem label="">
              {getFieldDecorator('shippingDate', {
                rules: [{ required: false }],
                initialValue: order.shippingDate ? moment.utc(order.shippingDate) : '',
              })(
                <DatePicker
                  allowClear={false}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={<ThemeIcon type="calendar" />}
                  style={{ width: '100%' }}
                />,
              )}
            </FormItem>
          </LogisticRow>
          <LogisticRow>
            <AccountLabel>Delivery Time</AccountLabel>
            <FormItem label="">
              {getFieldDecorator('deliveryTime', {
                rules: [{ required: false }],
                initialValue: order.deliveryTime ? moment.utc(order.deliveryTime) : '',
              })(
                <DatePicker
                  showTime={true}
                  allowClear={false}
                  placeholder={'MM/DD/YYYY HH:mm'}
                  format={'MM/DD/YYYY HH:mm'}
                  suffixIcon={<ThemeIcon type="calendar" />}
                  style={{ width: '100%' }}
                />,
              )}
            </FormItem>
            {/* <DatePicker
              allowClear={false}
              placeholder={'MM/DD/YYYY'}
              format={'MM/DD/YYYY'}
              suffixIcon={<ThemeIcon type="calendar" />}
              defaultValue={deliveryTime ? moment.utc(deliveryTime) : ''}
              onChange={this.onDeliveryDateChange.bind(this)}
              style={{ width: '60%' }}
            />
            <TimePicker
              defaultValue={deliveryTime ? moment.utc(deliveryTime) : ''}
              style={{ width: '40%' }}
              onChange={this.onDeliveryTimeChange.bind(this)}
              format={'HH:mm'}
            /> */}
          </LogisticRow>
          {/* <LogisticRow>
            <AccountLabel>Cost Structure</AccountLabel>
            {companyProductTypes && (
              <ThemeSelect
                value={costStructure ? costStructure : ''}
                onChange={this.handleChangeValue.bind(this, 'costStructure')}
                dropdownRender={(menu: any) => (
                  <div>
                    {menu}
                    <EditA onMouseDown={(e) => e.preventDefault()} onClick={() => this.onEditSelect('costStructure')}>
                      Edit...
                    </EditA>
                  </div>
                )}
                style={{ width: '100%' }}
              >
                {companyProductTypes.costStructure.map(
                  (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                    return (
                      <Select.Option key={index} value={item.name}>
                        {item.name}
                      </Select.Option>
                    )
                  },
                )}
              </ThemeSelect>
            )}
          </LogisticRow>
          <LogisticRow>
            <AccountLabel>Mode Of Transporation</AccountLabel>
            {companyProductTypes && (
              <ThemeSelect
                value={modeOfTransportation ? modeOfTransportation : ''}
                onChange={this.handleChangeValue.bind(this, 'modeOfTransportation')}
                dropdownRender={(menu: any) => (
                  <div>
                    {menu}
                    <EditA
                      onMouseDown={(e) => e.preventDefault()}
                      onClick={() => this.onEditSelect('modeOfTransportation')}
                    >
                      Edit...
                    </EditA>
                  </div>
                )}
                style={{ width: '100%' }}
              >
                {companyProductTypes.modeOfTransportation &&
                  companyProductTypes.modeOfTransportation.map(
                    (item: { name: {} | null | undefined }, index: string | number | undefined) => {
                      return (
                        <Select.Option key={index} value={item.name}>
                          {item.name}
                        </Select.Option>
                      )
                    },
                  )}
              </ThemeSelect>
            )}
          </LogisticRow>
          <LogisticRow>
            <AccountLabel>Vendor Sales Rep</AccountLabel>
            <ThemeInput value={vendorSalesRep} onChange={this.handleChange.bind(this, 'vendorSalesRep')} />
          </LogisticRow> */}
          <Flex>
            <LogisticRow style={{ marginRight: 1 }}>
              <AccountLabel>Tractor Plate #</AccountLabel>
              <FormItem label="">
                {getFieldDecorator('tractorPlate', {
                  rules: [{ required: false }],
                  initialValue: order.tractorPlate ? order.tractorPlate : '',
                })(<ThemeInput />)}
              </FormItem>
            </LogisticRow>
            <LogisticRow>
              <AccountLabel>Trailer Plate #</AccountLabel>
              <FormItem label="">
                {getFieldDecorator('trailerPlate', {
                  rules: [{ required: false }],
                  initialValue: order.trailerPlate ? order.trailerPlate : '',
                })(<ThemeInput />)}
              </FormItem>
            </LogisticRow>
          </Flex>
          <LogisticRow>
            <AccountLabel>Driver’s License No.</AccountLabel>
            <FormItem label="">
              {getFieldDecorator('driverLicense', {
                rules: [{ required: false }],
                initialValue: order.driverLicense ? order.driverLicense : '',
              })(<ThemeInput />)}
            </FormItem>
          </LogisticRow>
          <LogisticRow>
            <AccountLabel>Fulfillment Instructions</AccountLabel>
            <FormItem label="">
              {getFieldDecorator('deliveryInstruction', {
                rules: [{ required: false }],
                initialValue: order.deliveryInstruction ? order.deliveryInstruction : '',
              })(<ThemeTextArea style={{ height: 96 }} />)}
            </FormItem>
          </LogisticRow>

          <ThemeButton shape="round" type="primary" htmlType="submit">
            <Icon type="save" theme="filled" />
            Save
          </ThemeButton>
        </Form>
      </div >
    )
  }
}

export default Form.create()(LogisticsForm)
