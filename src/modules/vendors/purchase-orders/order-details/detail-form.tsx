import * as React from 'react'
import { flexStyle, InputLabel, ThemeButton, ThemeSelect, ThemeTextArea } from '~/modules/customers/customers.style'
import { Layout, Row, Icon, Form, Tooltip } from 'antd'
import { OrderDetail } from '~/schema'
import { transLayout } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { OrdersDispatchProps, OrdersStateProps } from '~/modules/orders'
import { FormComponentProps } from 'antd/lib/form'
import { CACHED_NS_LINKED, FINANCIAL_TERMS } from '~/common'
import { FlexDiv, ItemHMargin4, ItemMarginTop8, OrderInfoItem } from '~/modules/customers/nav-sales/styles'

const FormItem = Form.Item

type DetailFormProps = OrdersDispatchProps &
  FormComponentProps<any> &
  OrdersStateProps & {
    order: OrderDetail
    vendors: any[]
    onSave: Function
    companyProductTypes: any
    getCompanyProductAllTypes: Function
  }

class DetailForm extends React.PureComponent<DetailFormProps> {
  state = {
    visiblePaymentTermsModal: false,
    companyProductTypes: null,
  }

  componentDidMount() {
    if (!this.props.sellerSetting) {
      this.props.getSellerSetting()
    }
    this.props.getCompanyProductAllTypes()
    this.setState({ companyProductTypes: this.props.companyProductTypes })
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.companyProductTypes) != JSON.stringify(nextProps.companyProductTypes)) {
      this.setState({ companyProductTypes: nextProps.companyProductTypes })
    }
  }

  openCompanySettingChange = (type: string) => {
    let state = { ...this.state }
    state[type] = true
    this.setState(state)
  }

  onCompanySettingTypeModal = (type: string) => {
    let state = { ...this.state }
    state[type] = !this.state[type]
    this.setState(state)
    this.props.getCompanyProductAllTypes()
  }

  handleSubmit = (e: any) => {
    const { form, order } = this.props
    e.preventDefault()
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values)
        const { pickerNote, customerNote } = values
        this.props.onSave({
          wholesaleOrderId: order.wholesaleOrderId,
          pickerNote,
          customerNote,
        })
      }
    })
  }

  render() {
    const { order } = this.props
    const orderDisabled = !order || order.isLocked || order.wholesaleOrderStatus == 'CANCEL'
    const { getFieldDecorator } = this.props.form
    const { visiblePaymentTermsModal, companyProductTypes } = this.state
    const nsEnabled = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const paymentTermsList = nsEnabled
      ? FINANCIAL_TERMS
      : companyProductTypes
      ? [...companyProductTypes.paymentTerms, ...companyProductTypes.paymentTermsFixedTypes]
      : []

    return (
      <Form onSubmit={this.handleSubmit}>
        <FlexDiv style={ItemMarginTop8}>
          <OrderInfoItem className='inputDisabled left'>
            <InputLabel style={flexStyle}>Internal notes<Tooltip placement="top" title="This note will be displayed on the Pick Sheet. It will not be displayed on the Delivery List."><Icon style={ItemHMargin4} type='info-circle' /></Tooltip></InputLabel>
            {getFieldDecorator('pickerNote', {
                rules: [{ required: false }],
                initialValue: order.pickerNote ? order.pickerNote : '',
              })(
                <ThemeTextArea
                  data-id={3}
                  className='note-text header-input-field'
                  rows={4}
                  disabled={orderDisabled}
                />
              )}
          </OrderInfoItem>
          <OrderInfoItem className="inputDisabled">
            <InputLabel style={flexStyle}>Note for Customer<Tooltip placement="top" title="This note will be displayed on the Delivery List. It will not be displayed on the Pick Sheet."><Icon style={ItemHMargin4} type='info-circle' /></Tooltip></InputLabel>
            {getFieldDecorator('customerNote', {
              rules: [{ required: false }],
              initialValue: order.customerNote ? order.customerNote : '',
            })(
              <ThemeTextArea
                data-id={4}
                className='note-text header-input-field'
                rows={4}
                disabled={orderDisabled}/>
            )}
          </OrderInfoItem>
        </FlexDiv>

        <Layout style={transLayout}>
          <Row>
            <ThemeButton disabled={orderDisabled} shape="round" htmlType="submit" type="primary" style={{ float: 'right' }}>
              <Icon type="save" theme="filled" />
              Save
            </ThemeButton>
          </Row>
        </Layout>
      </Form>
    )
  }
}

export default Form.create()(DetailForm)
