import * as React from 'react'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import DetailForm from './detail-form'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'

type OrderDetailsProps = OrdersDispatchProps &
  OrdersStateProps & /*RouteComponentProps<{ orderId: string }> & */ {
    theme: Theme
  }

export default class OrderDetails extends React.PureComponent<OrderDetailsProps> {
  componentDidMount() {
    // const orderId = this.props.match.params.orderId
    // this.props.getOrderDetail(orderId)
    // this.props.getOrderItemsById(orderId)
    // this.props.getCompanyProductAllTypes()
    this.props.getCompanyUsers()
    this.props.getSellerSetting()
    this.getOneOffItems() //get OneOffItems whenever OrderDetails tab is created because
    //oneOffItems variable might contain so items.
    //We are using same OrdersModule and redux variable for oneOffItems
  }

  getOneOffItems = () => {
    const orderId = this.props.match.params.orderId
    console.log(orderId)
    if (orderId) {
      this.props.getOneOffItems(orderId)
    }
  }

  onSave = (data: any) => {
    const { currentOrder } = this.props
    if (currentOrder) {
      data.wholesaleCustomerClientId = currentOrder.wholesaleClient.clientId
      this.props.updateOrderInfo(data)
    }
  }

  render() {
    const { currentOrder, vendors, sellerSetting } = this.props
    if (!currentOrder) {
      return <PageLayout currentTopMenu={'menu-Purchasing-Purchase Orders'} />
    }

    return (
      <div style={{ padding: '15px 40px 0 50px' }}>
        {/* <DetailsTitle>Order Details</DetailsTitle> */}
        {currentOrder && <DetailForm order={currentOrder} vendors={vendors} onSave={this.onSave} {...this.props} />}
        {/* <SummaryForm
          sendEmailPdf={this.props.sendEmailToVendor}
          order={currentOrder}
          orderItems={orderItems}
          companyName={this.props.companyName}
          companyAddress={sellerAddress}
          company={company}
          logo={this.props.logo}
          oneOffItems={this.props.oneOffItems}
        /> */}
      </div>
    )
  }
}

// const mapStateToProps = (state: GlobalState) => state.orders
// export default withTheme(connect(OrdersModule)(mapStateToProps)(OrderDetails))
