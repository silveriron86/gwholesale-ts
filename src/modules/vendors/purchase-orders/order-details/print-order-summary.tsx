import React from 'react'
import { OrderDetail, OrderItem, Address } from '~/schema'
import { Table, Row, Col } from 'antd'
import Barcode from 'react-barcode'
import {
  HeaderTextRightBox,
  HeaderTextLeftBox,
  HeaderBox,
  PrintPickSheetWrapper,
  PrintPickSheetTable,
} from '~/modules/customers/sales/cart/print/_styles'
import {
  formatNumber,
  formatAddress,
  baseQtyToRatioQty,
  ratioQtyToBaseQty,
  ratioPriceToBasePrice,
  basePriceToRatioPrice,
  getOrderPrefix,
  mathRoundFun,
  judgeConstantRatio,
  formatItemDescription,
  calculatePOPrintableSubtotal
} from '~/common/utils'
import Moment from 'react-moment'
import {
  CompanyInfoWrapper,
  CompanyInfoHorizontalWrapper,
  LogoWrapper,
  PaddingWrapper,
  SmallPaddingWrapper,
  HeaderRow,
  GreyBody,
  BoldTitle,
  BoldTextWrapper,
  BiggerText,
  BackColorText,
  PurchaseOrderPrintTable,
  Space,
  Notes,
} from '~/modules/orders/components/OrderTableHeader/styles'
import { Icon } from '~/components'
import { defaultLogoStyle } from '~/modules/setting/theme.style'
import '../order-details/purchasePDF.css'
import moment from 'moment'
import styled from '@emotion/styled'

interface PrintOrderSummaryProps {
  orderItems: OrderItem[]
  order: OrderDetail
  companyName: string
  companyAddress: Address
  logo: string
  totalUnit: number
  subTotal: number
  shippingCost: number
  company: any
  changePrintLogoStatus: Function
  sellerSetting?: any
  printable?: any
}

class PrintOrderSummary extends React.PureComponent<PrintOrderSummaryProps> {
  state = {
    totalQuantity: 0,
    totalAmount: 0,
    visibleAlert: false,
    selectedRowKeys: [],
  }

  onImageLoad = () => {
    this.props.changePrintLogoStatus()
  }

  printShow = (show: boolean, obj: any) => (show ? obj : <></>)

  filterColumns = (columns: any, setting: any) => {
    const settingArr = [
      setting?setting.display_sku:false,
      setting?setting.display_vendor_item_code:false,
      setting?setting.display_product_description:false,
      setting?setting.display_order_quality:false,
      setting?setting.display_estimated_billable_quality:false,
      setting?setting.display_confirmed_quality:false,
      setting?setting.display_quoted_cost:false,
      setting?setting.display_total_cost:false,
    ]
    const newColumns: any[] = []
    newColumns.push(columns[0])
    columns.forEach((item: any, index: number) => {
      if (index > 0 && settingArr[index - 1] == true) {
        newColumns.push(item)
      }
    })
    return newColumns
  }

  render() {
    const { orderItems, order, company, sellerSetting, printable } = this.props
    const _printable = printable?JSON.parse(printable):null
    const printableSetting = (_printable != null&&_printable.purchase_enabled)?_printable:null
    let columns: Array<any> = [
      {
        title: '',
        dataIndex: 'index',
        key: 'index',
        align: 'center',
        width: 20,
        margin: 0,
        className: 'PurchasePDFTable',
        render: (id: number, record: OrderItem, index: number) => {
          return <span style={{whiteSpace: 'nowrap', minWidth: 25, display: 'block'}}>{index + 1}</span>
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        key: 'SKU',
        align: 'center',
        width: 100,
        className: 'PurchasePDFTable',
      },
      {
        title: 'Vendor Item Code',
        dataIndex: 'vendorProductCode',
        key: 'vendorProductCode',
        align: 'center',
        width: 130,
        className: 'PurchasePDFTable',
      },
      {
        title: 'Product Description',
        dataIndex: 'variety',
        align: 'center',
        key: 'variety',
        width: 260,
        className: 'PurchasePDFTable',
        render: (_text: string, record: OrderItem) => {
          if (!record.lotId) {
            //one off item
            return record.itemName
          } else {
            return formatItemDescription(_text, record.SKU, sellerSetting)
          }
        },
      },
      {
        title: 'Order Qty',
        dataIndex: 'quantity',
        align: 'center',
        key: 'quantity',
        width: 150,
        className: 'PurchasePDFTable',
        render: (_text: string, record: OrderItem, index: number) => {
          if (!record.lotId) {
            //one off item
            return `${record.quantity} ${record.UOM}`
          } else {
            let quantity = record.quantity ? record.quantity : 0
            const overrideUOM = record.overrideUOM ? record.overrideUOM : record.UOM
            return (
              <span>
                {baseQtyToRatioQty(overrideUOM, ratioQtyToBaseQty(record.inventoryUOM, quantity, record, 4), record)}
                &nbsp;
                {overrideUOM}
              </span>
            )
          }
        },
      },
      {
        title: 'EST. Billable Qty',
        dataIndex: 'orderWeight',
        align: 'center',
        key: 'orderWeight',
        className: 'PurchasePDFTable',
        render: (_text: string, record: OrderItem) => {
          if (!record.lotId) {
            //one off item
            return `${record.quantity} ${record.UOM}`
          } else {
            let constantRatio = judgeConstantRatio(record)
            const uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
            if (!constantRatio) {
              return `${baseQtyToRatioQty(uom, record.orderWeight, record)} ${uom}`
            } else {
              return `${baseQtyToRatioQty(uom, record.quantity, record)} ${uom}`
            }
          }
        },
      },
      {
        title: 'Confirmed Qty',
        dataIndex: 'qtyConfirmed',
        align: 'center',
        className: 'PurchasePDFTable',
        render: (_text: number, record: OrderItem) => {
          if (!_text) return
          if (!record.lotId) {
            //one off item
            return `${record.qtyConfirmed} ${record.UOM}`
          } else {
            let quantity = _text ? _text : 0
            const overrideUOM = record.overrideUOM ? record.overrideUOM : record.UOM
            return (
              <span>
                {baseQtyToRatioQty(overrideUOM, ratioQtyToBaseQty(record.inventoryUOM, quantity, record, 4), record)}
                {overrideUOM}
              </span>
            )
          }
        },
      },
      {
        title: 'Quoted Cost',
        dataIndex: 'cost',
        className: 'hidden-in-jana',
        align: 'center',
        key: 'cost',
        className: 'PurchasePDFTable',
        render: (text: number, record: OrderItem) => {
          if (!record.lotId) {
            //one off item
            return `$${formatNumber(record.cost, 2)}/${record.UOM}`
          } else {
            const uom = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
            const price = basePriceToRatioPrice(uom, text, record)
            return (
              <span>
                ${formatNumber(price, 2)}/{uom}
              </span>
            )
          }
        },
      },
      {
        title:  'Extended Cost',
        dataIndex: 'subTotal',
        className: 'hidden-in-jana',
        key: 'subTotal',
        className: 'PurchasePDFTable th-right',
        render: (_text: string, record: OrderItem, index: number) => {
          // let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
          // let ratioCost = basePriceToRatioPrice(pricingUOM, record.cost, record)
          const constantRatio = judgeConstantRatio(record)
          if (!constantRatio) {
            return <div className="text-right">${formatNumber(Math.round(record.cost * record.orderWeight * 100) / 100, 2)}</div>
          } else {
            return <div className="text-right">${formatNumber(Math.round(record.cost * record.quantity * 100) / 100, 2)}</div>
          }
        },
      }
    ]
    // TODO: check why there are dupliated settings for this extended_cost_enabled and display_total_cost
    // if (printableSetting && printableSetting.extended_cost_enabled) {
    //   columns = [...columns, {
    //     title:  'Extended Cost',
    //     dataIndex: 'subTotal',
    //     className: 'hidden-in-jana',
    //     key: 'subTotal',
    //     className: 'PurchasePDFTable th-right',
    //     render: (_text: string, record: OrderItem, index: number) => {
    //       // let pricingUOM = record.pricingUOM ? record.pricingUOM : record.UOM
    //       // let ratioCost = basePriceToRatioPrice(pricingUOM, record.cost, record)
    //       const constantRatio = judgeConstantRatio(record)
    //       if (!constantRatio) {
    //         return <div className="text-right">${formatNumber(Math.round(record.cost * record.orderWeight * 100) / 100, 2)}</div>
    //       } else {
    //         return <div className="text-right">${formatNumber(Math.round(record.cost * record.quantity * 100) / 100, 2)}</div>
    //       }
    //     },
    //   }]
    // }

    const isJanaService =
      sellerSetting && sellerSetting.company && sellerSetting.company.companyName == 'Jana Food Services'
    const subTotal = calculatePOPrintableSubtotal(this.props.orderItems)
    // console.log(order)

    let logo = this.props.logo
    if (logo && logo !== 'default') {
      if (!location.origin.includes('www')) {
        logo = logo.replace('www.', '')
      }
    }

    return (
      <PrintPickSheetWrapper style={{ marginLeft: 20, marginRight: 20 }}>
        <HeaderBox style={{ fontWeight: 'normal' }}>
          <Row style={{ textAlign: 'center' }}>
            <h1>{(printableSetting && printableSetting.purchase_title ? printableSetting.purchase_title : 'Purchase Order').toUpperCase()}</h1>
          </Row>
          <Row gutter={24} style={{ marginBottom: 20 }}>
            <Col span={10}>
              <CompanyInfoWrapper>
                <div style={{ textAlign: 'left' }}>
                  {this.printShow(printableSetting?printableSetting.display_logo:false,logo === 'default' ? (
                    <LogoWrapper>
                      <Icon type="logo" viewBox={void 0} style={defaultLogoStyle} />
                    </LogoWrapper>
                  ) : (
                    <img src={logo} onLoad={this.onImageLoad} />
                  ))}
                  {/* <span>{this.props.companyName}</span> */}
                  <div>
                    <Row>{company && company.companyName ? company.companyName : ''}</Row>
                    <Row>
                      {company && company.mainBillingAddress
                        ? formatAddress(company.mainBillingAddress.address, false)
                        : ''}
                    </Row>
                    <Row>
                      <Col>Phone: {company && company.phone ? company.phone : ''}{company && company.fax ? ` / Fax: ${company.fax}` : ''}</Col>
                    </Row>
                  </div>
                </div>
              </CompanyInfoWrapper>
            </Col>
            <Col span={7} style={{ textAlign: 'left', marginTop: 20 }}>
              {/* <Row>{company && company.address ? formatAddress(company.address, true) : ''}</Row>
              <Row>
                <Col span={8}>Phone:</Col>
                <Col span={16}>{company && company.phone ? company.phone : ''}</Col>
              </Row>
              <Row>
                <Col span={8}>Fax:</Col>
                <Col span={16}>{company && company.fax ? company.fax : ''}</Col>
              </Row> */}
            </Col>
            <Col span={7} style={{ textAlign: 'right' }}>
              {this.printShow(printableSetting?printableSetting.display_barcode:false,(<Barcode value={order.wholesaleOrderId.toString()} displayValue={false} />))}
            </Col>
            <Col span={1} />
          </Row>
          <div style={{ display: 'flex', marginBottom: '10px' }}>
            <div style={{ width: '50%' }}>
            {this.printShow(printableSetting?printableSetting.display_purchase_order:false,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Purchase order #:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                    <span>
                        {getOrderPrefix(sellerSetting, 'purchase')}
                        {order.wholesaleOrderId}
                      </span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting?printableSetting.display_reference:false,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Vendor Ref. #:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                      <span>
                        {order.po}
                      </span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting?printableSetting.display_order_date:false,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Order date:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                    <span>
                        <Moment format="MM/DD/YYYY" date={moment.utc(order.orderDate)} />
                      </span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting?printableSetting.display_purchaser:false,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Purchaser:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                    <span>{order.purchaser ? order.purchaser.firstName + ' ' + order.purchaser.lastName : ''}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting?printableSetting.delivery_date:false,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Delivery date:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                    <span>
                        <Moment format="MM/DD/YYYY" date={moment.utc(order.deliveryDate)} />
                      </span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
            </div>
            <div style={{ width: '50%' }}>
            {this.printShow(printableSetting && printableSetting.display_payment_terms && order.financialTerms,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Terms:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                      <span>{order.financialTerms}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting && (printableSetting.display_carrier !== false) && order.carrier,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Carrier:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                      <span>{order.carrier}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting && printableSetting.display_shipping_terms && order.shippingTerm,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Shipping terms:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                      <span>{order.shippingTerm}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting && printableSetting.display_freight_type && order.freightType,(
              <Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Freight type:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                    <span>{order.freightType}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
              {this.printShow(printableSetting && printableSetting.display_carrier_reference && order.reference,(<Row>
                <Col>
                  <Row>
                    <Col span={8}>
                      <span>Carrier Ref. #:</span>
                    </Col>
                    <Col offset={1} style={{textAlign: 'left'}}>
                    <span>{order.reference}</span>
                    </Col>
                  </Row>
                </Col>
              </Row>))}
            </div>
          </div>
          <div style={{ display: 'flex', marginTop: '10px' }}>
            <span style={{ width: '50%', color: 'black' }} span={12}>
              Vendor:
            </span>
            <span style={{ width: '50%', color: 'black' }} span={12}>
              Ship to:
            </span>
          </div>
          <Row style={{}}>
            <Col span={12}>
              <Row>
                <BoldTextWrapper>{order.wholesaleClient.clientCompany.companyName}</BoldTextWrapper>
              </Row>
              {order && order.wholesaleClient && order.wholesaleClient.mainBillingAddress && (
                <Row style={{ whiteSpace: 'pre-wrap' }}>{formatAddress(order.wholesaleClient.mainBillingAddress.address, true)}</Row>
              )}
            </Col>
            <Col span={12}>
              {this.printShow(printableSetting?printableSetting.display_ship_to:false,(<Row style={{ whiteSpace: 'pre-wrap' }}>{order && order.shippingAddress ? formatAddress(order.shippingAddress.address, true) : ''}</Row>))}
            </Col>
          </Row>
          <Row>
            <Col style={{ marginTop: '10px' }} span={12}>
              <Row>
                <Col span={6}>
                  <span>Phone: </span>
                </Col>
                <Col span={18}>{order.wholesaleClient.mainShippingAddress && order.wholesaleClient.mainShippingAddress.phone
                    ? order.wholesaleClient.mainShippingAddress.phone
                    : order.wholesaleClient.mobilePhone}</Col>
              </Row>
              <Row>
                <Col span={6}>
                  <span>Email:</span>
                </Col>
                <Col span={18}>{order.wholesaleClient.mainContact?.email}</Col>
              </Row>
            </Col>
            <Col style={{ marginTop: '10px' }} span={12}>
              <Row>
                <Col span={6}>
                  <span>Phone: </span>
                </Col>
                {this.printShow(printableSetting?printableSetting.display_ship_to:false,(<Col span={18}>
                  {order.shippingAddress ? order.shippingAddress.phone : ''}
                </Col>))}
              </Row>
              {/* <Row>
                <Col span={6}>
                  <BoldTextWrapper>Attn.:</BoldTextWrapper>
                </Col>
                <Col span={18}>{order.wholesaleClient.seller ? order.wholesaleClient.seller.firstName : ''}</Col>
              </Row> */}
            </Col>
          </Row>
          <br/>
          <div>
            {order?order.customerNote?(<Col style={{width:"100%"}} span={14}>
              <span style={{fontWeight:"normal"}}>{`NOTES: ${order.customerNote}`}</span>
              </Col>):<></>:<></>}
            </div>
          <Table
            rowKey="wholesaleOrderItemId"
            columns={this.filterColumns(columns, printableSetting)}
            dataSource={orderItems}
            pagination={false}
          />
        </HeaderBox>

        <Row style={{ borderTop: '2px solid grey', paddingTop: 40, marginTop: '5px' }}>
          <Col span={14}></Col>
          {this.printShow(printableSetting?printableSetting.display_totalAll_cost:false,(<Col span={8} style={{ display: isJanaService ? 'none' : 'block' }}>
            <Row type="flex" justify="space-around">
              <Col span={10}>TOTAL</Col>
              <Col span={4} />
              <Col span={10} style={{ textAlign: 'right' }}>
                ${formatNumber(subTotal + this.props.shippingCost, 2)}
              </Col>
            </Row>
          </Col>))}
        </Row>
        { printableSetting?.po_terms && <StyledTerms>{printableSetting?.po_terms}</StyledTerms> }
      </PrintPickSheetWrapper>
    )
  }
}

export default PrintOrderSummary

const StyledTerms = styled.p`
  margin-top: 20px;
  font-size: 10px;
  font-weight: normal;
  letter-spacing: normal;
  color: black;
`
