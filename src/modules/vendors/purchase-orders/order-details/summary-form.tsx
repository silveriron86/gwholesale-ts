import * as React from 'react'

import { CustomerDetailsTitle as DetailsTitle, ThemeButton } from '~/modules/customers/customers.style'
import { Layout, Row, Icon, Col, Input, Modal, Popconfirm } from 'antd'
import {
  odOrderSummaryLayout,
  DetailsSection,
  DetailLabel,
  NoBorderText,
  OoOrderSummaryCol,
  fullButton,
} from '~/modules/customers/sales/_style'
import { OrderDetail, OrderItem, Address } from '~/schema'
import { formatNumber, printWindow, calPoTotalCost } from '~/common/utils'

import { CustomButton } from '~/components'

interface SummaryFormProps {
  sendEmailPdf: Function
  order: OrderDetail
  orderItems: OrderItem[]
  companyName: string
  companyAddress: Address
  company: any
  logo: string
  oneOffItems?: any[]
}

export class SummaryForm extends React.PureComponent<SummaryFormProps> {
  state = {
    printOrderSummaryReviewShow: false,
    printOrderSummaryRef: null,
  }

  openPrintModal = (type: number) => {
    this.setState({
      printOrderSummaryReviewShow: true,
    })
  }

  sendEmailWithPdf = (data: any) => {
    this.props.sendEmailPdf(data)
  }

  closePrintModal = (type: number) => {
    this.setState({
      printOrderSummaryReviewShow: false,
    })
  }

  printContent: any = (type: number) => {
    return this.state.printOrderSummaryRef
  }

  printOrderSummaryTrigger = () => {
    return (
      <CustomButton style={fullButton}>
        <Icon type="printer" theme="filled" />
        Generate PDF / Print
      </CustomButton>
    )
  }

  calcOneOffTotal = (data: any[] | undefined) => {
    let oneOffTotal = 0
    if (data && data.length) {
      data.forEach((el) => {
        oneOffTotal += Math.round(el.price * el.picked * 100) / 100
      })
    }
    return oneOffTotal
  }

  render() {
    const { order, orderItems, oneOffItems } = this.props
    let totalData = calPoTotalCost(orderItems)
    let totalUnit = totalData != null ? totalData.totalUnit : 0
    let subTotal = totalData != null ? totalData.subTotal : 0

    const oneOffTotal = this.calcOneOffTotal(oneOffItems)

    // Order total = Item(s) subtotal + shipping cost.
    const shippingCost = order.shippingCost !== null ? order.shippingCost : 0
    const orderTotal = Math.round((subTotal + shippingCost + oneOffTotal) * 100) / 100
    // const orderTotal = subTotal + oneOffTotal

    let emailData: any = {
      orderId: order.wholesaleOrderId,
      itemList: orderItems,
    }

    return (
      <Layout style={odOrderSummaryLayout}>
        <DetailsTitle>Order Summary</DetailsTitle>
        <DetailsSection>
          <Row>
            <Col md={4}>
              <OoOrderSummaryCol>
                <DetailLabel>TOTAL UNITS</DetailLabel>
                <Input value={formatNumber(totalUnit, 2)} style={NoBorderText} readOnly={true} />
              </OoOrderSummaryCol>
            </Col>
            <Col md={4}>
              <OoOrderSummaryCol>
                <DetailLabel>ITEM(S) SUBTOTAL</DetailLabel>
                <Input value={`$${formatNumber(subTotal, 2)}`} style={NoBorderText} readOnly={true} />
              </OoOrderSummaryCol>
            </Col>
            {/* <Col md={4}>
              <OoOrderSummaryCol>
                <DetailLabel>SHIPPING COST</DetailLabel>
                <Input value={`$${formatNumber(shippingCost, 2)}`} style={NoBorderText} readOnly={true} />
              </OoOrderSummaryCol>
            </Col> */}
            <Col md={4}>
              <OoOrderSummaryCol>
                <DetailLabel>ORDER TOTAL</DetailLabel>
                <Input value={`$${formatNumber(orderTotal, 2)}`} style={NoBorderText} readOnly={true} />
              </OoOrderSummaryCol>
            </Col>
            {/* <Col md={4}>
              <OoOrderSummaryCol>
                <ThemeButton shape="round" type="primary" onClick={this.openPrintModal.bind(this, 2)}>
                  <Icon type="printer" theme="filled" />
                  Print / Save PDF
                </ThemeButton>
              </OoOrderSummaryCol>
            </Col>
            <Col md={4}>
              {order.wholesaleClient.mainContact && (
                <OoOrderSummaryCol>
                  <Popconfirm
                    title={`Are you sure to email to ${order.wholesaleClient.mainContact.email}?`}
                    onConfirm={() => this.sendEmailWithPdf(emailData)}
                  >
                    <ThemeButton shape="round" type="primary" style={{ width: '100%' }}>
                      <Icon type="email" theme="filled" />
                      Email PDF
                    </ThemeButton>
                  </Popconfirm>
                </OoOrderSummaryCol>
              )}
            </Col> */}
          </Row>
          {/* <Modal
            width={1080}
            footer={null}
            visible={this.state.printOrderSummaryReviewShow}
            onCancel={this.closePrintModal.bind(this, 2)}
          >
            <div id={'PrintOrderSummaryModal'}>
              <PrintOrderSummary
                totalUnit={totalUnit}
                subTotal={subTotal}
                shippingCost={shippingCost}
                orderItems={orderItems}
                order={order}
                companyName={this.props.companyName}
                companyAddress={this.props.companyAddress}
                company={this.props.company}
                logo={this.props.logo}
                ref={(el) => {
                  this.setState({ printOrderSummaryRef: el })
                }}
              />
            </div>
            <CustomButton
              style={fullButton}
              onClick={() => printWindow('PrintOrderSummaryModal', this.printContent.bind(this))}
            >
              <Icon type="printer" theme="filled" />
              Generate PDF / Print
            </CustomButton>
          </Modal> */}
        </DetailsSection>
      </Layout>
    )
  }
}
// totalUnit += orderItems[i].quantity
// orderItems[i]["subTotal"] = orderItems[i].price * orderItems[i].quantity
// subTotal += orderItems[i]["subTotal"]
// shippingCost += orderItems[i].freight
export default SummaryForm
