import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { RouteComponentProps, RouteProps } from 'react-router'

import { OrderTable, OrderHeader } from '~/modules/orders/components'
import { AuthUser } from '~/schema'
import { ResizeCallbackData } from 'react-resizable'
import { VendorsModule, VendorsStateProps, VendorsDispatchProps } from '../vendors.module'

export type PurchaseOrdersProps = VendorsDispatchProps &
  VendorsStateProps &
  RouteProps &
  RouteComponentProps & {
    currentUser: AuthUser
    onResize?: (e: React.SyntheticEvent, data: ResizeCallbackData) => any
  }

// 该文件没用到 2021-11-01
export class PurchaseOrdersComponent extends React.PureComponent<PurchaseOrdersProps> {
  componentDidMount() {
    this.props.getVendorOrders()
  }

  onDateChange = (_dateMoment: any, dateString: string) => {
    if (dateString.length > 0 && this.props.currentUser.accountType === UserRole.SALES) {
      // this.props.getOrder(dateString);
    }
  }

  render() {
    return (
      <div>
        <OrderHeader
          sellerSetting={this.props.sellerSetting}
          onDateChange={this.onDateChange}
          currentUser={this.props.currentUser}
        />
        <OrderTable
          history={this.props.history}
          orders={this.props.orders}
          tableMini={false}
          currentUser={this.props.currentUser}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export default connect(VendorsModule)(mapStateToProps)(PurchaseOrdersComponent)
