import React, { FC, useEffect, useState } from 'react'

import { AutoComplete } from 'antd'
import { GlobalState } from '~/store/reducer'
import { OrdersModule } from '~/modules/orders/orders.module'
import _ from 'lodash'
import { addVendorProductCode, formatNumber } from '~/common/utils'
import { connect } from 'redux-epics-decorator'

const GeneralFooter: FC<any> = ({
  getPurchaseItemsForAddItemModal,
  currentProducts,
  purchaseSimplifyItems,
  handleAddOrderItem,
}) => {
  const [data, setData] = useState<any>([])
  const [searchValue, setSearchValue] = useState<any>('')
  const [originData, setOriginData] = useState<any>([])

  const onSelectItem = (_t: any, r: any) => {
    handleAddOrderItem(r.props.label)
    setSearchValue('')
  }

  const getData = _.debounce(() => {
    if (!searchValue) return setData([])
    const _selected = searchValue.length >= 2 ? searchValue : ''
    if (purchaseSimplifyItems) {
      let matches0: Array<any> = []
      let matches1: Array<any> = []
      let matches2: Array<any> = []
      let matches3: Array<any> = []

      originData.forEach((row: any, index: number) => {
        if (typeof row.SKU === 'undefined' && row.sku) {
          row.SKU = row.sku
        }
        if (row.SKU && row.SKU.toLowerCase().indexOf(_selected.toLowerCase()) === 0) {
          // full match on 2nd filed
          matches2.push(row)
        } else if (row.variety && row.variety.toLowerCase().indexOf(_selected.toLowerCase()) === 0) {
          // full match on variety
          matches1.push(row)
        } else if (
          row.vendorProductCode &&
          row.vendorProductCode.toLowerCase().indexOf(_selected.toLowerCase()) === 0
        ) {
          // full match on vendorProductCode
          matches0.push(row)
        } else if (
          (row.wholesaleCategory && row.wholesaleCategory.name.indexOf(_selected.toLowerCase()) > 0) ||
          (row.variety && row.variety.toLowerCase().indexOf(_selected.toLowerCase()) > 0) ||
          (row.SKU && row.SKU.toLowerCase().indexOf(_selected.toLowerCase()) > 0) ||
          (row.vendorProductCode && row.vendorProductCode.toLowerCase().indexOf(_selected.toLowerCase()) > 0)
        ) {
          // others
          matches3.push(row)
        }
      })

      if (matches0) {
        matches0 = matches0.sort((a, b) => a.vendorProductCode.localeCompare(b.vendorProductCode))
      }
      if (matches1) {
        matches1 = matches1.sort((a, b) => a.variety.localeCompare(b.variety))
      }
      if (matches2) {
        matches2 = matches2.sort((a, b) => a.SKU.localeCompare(b.SKU))
      }
      if (matches3) {
        matches3 = matches3.sort((a, b) => a.variety.localeCompare(b.variety))
      }
      const targetData = [...matches2, ...matches1, ...matches0, ...matches3]
      if (targetData.length > 50) return setData(targetData.slice(0, 50))
      setData(targetData)
    }
  }, 400)

  useEffect(() => {
    getPurchaseItemsForAddItemModal()
  }, [])

  useEffect(() => {
    setOriginData(addVendorProductCode(purchaseSimplifyItems, currentProducts))
  }, [purchaseSimplifyItems])

  useEffect(() => {
    getData()
  }, [searchValue])

  const children = (item: any) => {
    return (
      <AutoComplete.Option key={item.itemId} label={item}>
        <div style={{ display: 'flex' }}>
          <p style={{ width: '150px' }}>{item.SKU}</p>
          <p style={{ width: '400px' }}>{item.variety}</p>
          <p style={{ float: 'right' }}>{`${formatNumber(item.availableToSell, 2)} ${
            item.inventoryUOM
          }* available(${formatNumber(item.onHandQty, 2)} ${item.inventoryUOM} on hand)`}</p>
        </div>
      </AutoComplete.Option>
    )
  }

  return (
    <AutoComplete
      style={{ width: 400 }}
      dropdownStyle={{ minWidth: 800 }}
      placeholder="Add Item..."
      value={searchValue}
      onSearch={setSearchValue}
      onSelect={(_t, r: any) => onSelectItem(_t, r)}
      optionLabelProp="value"
      dataSource={data.map(children)}
    ></AutoComplete>
  )
}

const mapStateToProps = (state: GlobalState) => state.orders
export default connect(OrdersModule)(mapStateToProps)(GeneralFooter)
