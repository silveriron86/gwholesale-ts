import * as React from 'react'

import { CACHED_NS_LINKED, Theme } from '~/common'
import { CustomersInfoBody, layoutStyle } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { CustomerDetailsWrapper as DetailsWrapper, Flex, ThemeButton, ThemeSelect, ThemeCheckBox, ThemeIcon, ThemeIconButton, ThemeModal, ThemeSpin, ThemeRadio } from '~/modules/customers/customers.style'
import { Layout, Tabs, notification, Radio, Checkbox, Select } from 'antd'
import { OrdersDispatchProps, OrdersModule, OrdersStateProps } from '~/modules/orders/orders.module'
import { baseQtyToRatioQty, formatNumber, ratioQtyToBaseQty, mathRoundFun } from '~/common/utils'

import { GlobalState } from '~/store/reducer'
import Logistics from '../logistics'
import MemoTotalTable from '~/modules/customers/sales/credit-memo/memo-total-table'
import OrderDetails from '../order-details'
import OrderTab from './order-tab'
import PageLayout from '~/components/PageLayout'
import PurchaseDocuments from '../documents'
import { PurchaseNavHeader } from '../_components/nav-header'
import PurchaseOrderExtraCharge from '../extra-charge'
import PurchaseOrderRelatedBills from '../related-bills'
import PurchaseOrderCostAllocations from '../cost-allocations'
import PurchasePricingAnalysis from '../pricing-analysis'
import PurchaseProfitability from '../profitability'
import Receiving from '../receiving'
import { RouteComponentProps } from 'react-router'
import { TabsWrapper } from '~/modules/customers/nav-sales/styles'
import { AuthUser, UserRole } from '~/schema'
import { connect } from 'redux-epics-decorator'
import jQuery from 'jquery'
import moment from 'moment'
import { withTheme } from 'emotion-theming'
import RepackTab from '~/modules/vendors/purchase-orders/overview/repack/repack-tab'
import ActivityLog from '../activity-log'

const { TabPane } = Tabs

type PurchaseOverviewProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
    currentUser: AuthUser
  }

export class PurchaseOverview extends React.PureComponent<PurchaseOverviewProps> {
  focusedIndex: number = 0
  prevIndex: number = 0
  timer: any = null
  bodyClicked: boolean = false
  state = {
    tabKey: this.props.currentUser.accountType != UserRole.WAREHOUSE ? '1' : '4',
    syncModalVisible: false,
    billType: 1,
    checkedItems: [],
    selectedBill: null
  }

  editItem = () => {}

  onChangeTab = (key: string) => {
    const orderId = this.props.match.params.orderId
    if (key === '4' || key === '7') {
      // the item field value in redux here delay call 1s
      setTimeout(() => {
        this.props.getOrderItemsById(orderId)
      }, 1000)
    }
    this.setState({
      tabKey: key,
    })

    const urlParams = new URLSearchParams(this.props.location.search)
    const from = urlParams.get('from')
    if (from && from === 'report' && key !== '7') {
      window.history.replaceState({}, '', `#${this.props.location.pathname}`)
    }
  }

  onSave = (item: any, noRefresh?: boolean) => {
    if (item.status === 'AVAILABLE') {
      item.status = 'PLACED'
    }

    if (noRefresh === true) {
      this.props.handlerOrderItemsRedux(item)
      this.props.updateOrderItemWithoutRefresh(item)
    } else {
      this.props.updateOrderItem(item)
    }
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(this.props.location.search)
    const search = urlParams.get('duplicated')
    if (search === 'success') {
      setTimeout(() => {
        window.history.replaceState({}, '', `#${this.props.location.pathname}`)
        setTimeout(() => {
          notification.success({
            message: 'SUCCESS',
            description: 'Order successfully duplicated.',
            onClose: () => {},
            duration: 5,
          })
        }, 1500)
      }, 100)
    }

    const orderId = this.props.match.params.orderId
    this.props.getOrderDetail(orderId)
    this.props.getOrderItemsById(orderId)
    this.props.getOneOffItems(orderId)
    this.props.getCompanyProductAllTypes()
    this.props.getCompanyUsers()
    //get setting
    this.props.getSellerSetting()
    this.props.getPrintSetting()

    // repacks
    this.props.getRepack(orderId)

    // sync
    this.props.getSyncQboOrderDetails(orderId);

    // For enter purchase order
    this.props.getItemList({ type: 'PURCHASE' })

    const from = urlParams.get('from')
    if (from && from === 'report') {
      this.setState({
        tabKey: '7',
      })
    }
    setTimeout(() => {
      jQuery('.page-tab .ant-tabs-tabpane-active .purchase-add-item').trigger('focus')
      this.setTabOrder()
    }, 100)
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.newOrderId != -1 && nextProps.newOrderId !== this.props.newOrderId) {
      // window.location.href = `#/sales-order/${nextProps!.currentOrder!.wholesaleOrderId}`
      const query = nextProps.message === 'DUPLICATED_SUCCESS' ? '?duplicated=success' : ''
      window.location.href = `#/order/${nextProps.newOrderId}/purchase-cart${query}`
      window.location.reload()
    }
    if (
      (!this.props.currentOrder && nextProps.currentOrder) ||
      (this.props.currentOrder && this.props.currentOrder.wholesaleOrderId != nextProps.currentOrder.wholesaleOrderId)
    ) {
      setTimeout(() => {
        jQuery('.page-tab .ant-tabs-tabpane-active .purchase-add-item').trigger('focus')
        this.setTabOrder()
      }, 100)
    }

    if (this.props.syncingQboOrder !== nextProps.syncingQboOrder && nextProps.syncingQboOrder === false) {
      this.setState({
        checkedItems: []
      })
    }
  }

  setTabOrder = () => {
    const _this = this
    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (_this.isModalOrPopoverOpen()) {
          return
        }
        let actionFields = jQuery('.page-tab .ant-tabs-tabpane-active .purchase-cart-actions:not(:disabled)')
        let editableTableEls: any[] = []
        if (_this.state.tabKey == '1') {
          editableTableEls = jQuery('.purchase-order-overview-table').find(
            '.ant-input:not(.ant-select-search__field), .ant-btn, .pas .ant-checkbox-input, .ant-select-enabled .ant-select-selection, .ant-input-number-input, .tab-able',
          )
          const customFocusedSelect = jQuery('.purchase-order-overview-table .custom-ant-select-focused')
          if (customFocusedSelect.length) {
            let childOfCustomSelect = jQuery(customFocusedSelect).find('.ant-select-selection')[0]
            if (!childOfCustomSelect) {
              childOfCustomSelect = jQuery(customFocusedSelect).find('.ant-checkbox-input')[0]
            }
            this.focusedIndex = jQuery(editableTableEls).index(childOfCustomSelect) + actionFields.length
          } else {
            this.focusedIndex = jQuery(editableTableEls).index(e.target) + actionFields.length
          }
          if (jQuery(e.target).hasClass('ant-select-search__field')) {
            const parent = jQuery(e.target).parents('.ant-select-selection')
            this.focusedIndex = jQuery(editableTableEls).index(parent) + actionFields.length
          }
        } else {
          editableTableEls = jQuery('.page-tab .ant-tabs-tabpane-active .ant-table tbody tr .tab-able')
        }

        if (window.localStorage.getItem('CLICKED-INDEX')) {
          const clickedIndex = window.localStorage.getItem('CLICKED-INDEX')
          if (clickedIndex != '-1' && clickedIndex != 'no_value') {
            _this.focusedIndex = parseInt(clickedIndex, 10)
            window.localStorage.setItem('CLICKED-INDEX', '-1')
          }
        } else if (_this.bodyClicked) {
          _this.bodyClicked = false
          _this.focusedIndex = 0
          return
        }
        const isFreshGreen =
          _this.props.settingCompanyName === 'Fresh Green' || _this.props.settingCompanyName === 'Fresh Green Inc'
        if (e.keyCode == 9 || (isFreshGreen && e.keyCode == 13)) {
          if (jQuery('.ant-spin-nested-loading .ant-spin.ant-spin-spinning').length) {
            e.preventDefault()
            if (_this.timer === null) {
              _this.timer = setInterval(() => {
                if (jQuery('.ant-spin-nested-loading .ant-spin.ant-spin-spinning').length === 0) {
                  clearInterval(_this.timer)
                  _this.timer = null
                  _this.setFocusEl(editableTableEls[_this.prevIndex - 1])
                }
              }, 100)
            }
            return
          } else {
            _this.prevIndex = _this.focusedIndex
          }

          console.log('focused index', this.focusedIndex)
          if (e.shiftKey) {
            _this.focusedIndex--
            if (_this.focusedIndex < 0) {
              _this.focusedIndex = actionFields.length + editableTableEls.length - 1
              if (editableTableEls.length > 0) {
                _this.setFocusEl(editableTableEls[editableTableEls.length - 1], editableTableEls, e.keyCode)
              } else {
                _this.setFocusEl(actionFields[_this.focusedIndex], editableTableEls, e.keyCode)
              }
            } else if (_this.focusedIndex < actionFields.length) {
              _this.setFocusEl(actionFields[_this.focusedIndex], editableTableEls, e.keyCode)
            } else if (e.keyCode == 13) {
              _this.setFocusEl(editableTableEls[_this.focusedIndex - 1], editableTableEls, 13)
            }
          } else {
            _this.focusedIndex++
            if (_this.focusedIndex < actionFields.length) {
              _this.setFocusEl(actionFields[_this.focusedIndex], editableTableEls, e.keyCode)
            } else if (_this.focusedIndex == actionFields.length + editableTableEls.length) {
              _this.focusedIndex = 0
              _this.setFocusEl(actionFields[0], editableTableEls, e.keyCode)
            } else if (_this.focusedIndex == actionFields.length) {
              _this.setFocusEl(editableTableEls[0], editableTableEls, e.keyCode)
            } else if (e.keyCode == 13) {
              _this.setFocusEl(editableTableEls[_this.focusedIndex - 1], editableTableEls, 13)
            }
          }
        }
        //for now, disable this code because this code should be called when cell is focused
        //after testing and confirmation, will remove this code.
        // else if (e.keyCode == 32) {
        // _this.setFocusEl(editableTableEls[_this.focusedIndex - 1], editableTableEls, 32)
        // }
      })
  }

  setFocusEl = (el: any, editables: any[] = [], keycode: number = -1) => {
    console.log('current focused el', el)
    jQuery('.select-container-parent').removeClass('custom-ant-select-focused')
    jQuery('.ant-checkbox').removeClass('custom-ant-select-focused')
    const isFreshGreen =
      this.props.settingCompanyName === 'Fresh Green' || this.props.settingCompanyName === 'Fresh Green Inc'
    setTimeout(() => {
      if (jQuery(el).hasClass('ant-select-selection')) {
        console.log('isfresh, key', isFreshGreen, keycode)
        if (isFreshGreen && keycode == 13) {
          const parent = jQuery(el)
            .parent()
            .parent()

          console.log('parent class', parent)
          if (jQuery(parent).hasClass('select-container-parent') || jQuery(parent).hasClass('ant-checkbox')) {
            jQuery(parent).addClass('custom-ant-select-focused')
          }
          jQuery.each(editables, function(i: number, map: any) {
            jQuery(map).trigger('blur')
          })
          if (jQuery(el).find('.ant-select-search__field').length) {
            jQuery(jQuery(el).find('.ant-select-search__field')[0]).trigger('focus')
          }
        } else if (keycode == 32) {
          jQuery(el).trigger('click')
        } else {
          jQuery(el)
            .children()[0]
            .focus()
        }
      } else if (jQuery(el).hasClass('ant-checkbox-input')) {
        if (isFreshGreen && keycode == 13) {
          const parent = jQuery(el).parent()
          jQuery(parent).addClass('custom-ant-select-focused')
          jQuery.each(editables, function(i: number, map: any) {
            jQuery(map).trigger('blur')
          })
          jQuery(parent).trigger('focus')
        } else {
          jQuery(el).focus()
        }
      } else if (jQuery(el).hasClass('tab-able')) {
        if (!jQuery(el).hasClass('ant-btn')) {
          setTimeout(() => {
            jQuery(el)[0].click()
          }, 10)
        } else {
          setTimeout(() => {
            jQuery(el).trigger('focus')
          }, 10)
        }
      } else if (
        jQuery(el).hasClass('ant-btn') ||
        jQuery(el).hasClass('ant-input') ||
        jQuery(el).hasClass('ant-input-number-input')
      ) {
        jQuery(el).trigger('focus')
        jQuery(el).trigger('select')
      }
    }, 50)
  }

  isModalOrPopoverOpen = () => {
    if (
      (jQuery('body').find('.ant-modal-root').length &&
        jQuery('body').find('.ant-modal-mask').length != jQuery('body').find('.ant-modal-mask-hidden').length) ||
      (jQuery('body').find('.ant-popover').length && !jQuery('body').find('.ant-popover.ant-popover-hidden').length)
    ) {
      return true
    } else {
      return false
    }
  }

  handleAddItem = (newItem: any) => {
    const { orderItems } = this.props
    console.log(newItem)
    // tslint:disable-next-line:prefer-const
    let itemsList: any[] = []
    // orderItems.forEach((item) => {
    //   itemsList.push(this._formatItem(item))
    // })

    let cost = newItem.cost,
      palletQty = 0
    if (this.props.settingCompanyName == 'Jana Food Services') {
      palletQty = 1
    }

    itemsList.push({
      wholesaleItemId: newItem.itemId,
      wholesaleOrderItemId: null,
      quantity: 0,
      cost: cost,
      price: 0,
      margin: newItem.defaultMargin,
      freight: 0,
      // status: newItem.status,
      status: 'PLACED',
      UOM: newItem.inventoryUOM, // update by deng 11/01/2020   order item default uom need inventoryUOM and cost user pricinguom
      palletQty, // hardcode for jana food services
    })
    this._updateOrder(itemsList)
  }

  _updateOrder = (itemsList: any[], extra: any = null) => {
    const { currentOrder } = this.props
    if (currentOrder) {
      if (itemsList.length > 0) {
        itemsList.map((item: any) => {
          item.wholesaleOrderId = currentOrder.wholesaleOrderId
        })
      }
      let formData = {
        deliveryDate: moment.utc(currentOrder.deliveryDate).format('MM/DD/YYYY'),
        wholesaleOrderId: currentOrder.wholesaleOrderId,
        userId: currentOrder.user.userId,
        totalPrice: currentOrder.totalPrice,
        wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
        itemList: itemsList,
        status: currentOrder.wholesaleOrderStatus,
      }
      if (extra) {
        formData = { ...formData, ...extra }
      }
      this.props.updateOrder(formData)
    }
  }

  _formatItem = (item: any) => {
    return {
      wholesaleItemId: item.itemId,
      wholesaleOrderItemId: item.wholesaleOrderItemId,
      quantity: item.quantity,
      cost: item.cost,
      price: item.price,
      margin: item.margin,
      freight: item.freight,
      status: item.status,
    }
  }

  handleRemoveItem = (id: string) => {
    const { orderItems } = this.props
    // tslint:disable-next-line:prefer-const
    let itemsList: any[] = []
    orderItems.forEach((item) => {
      if (item.wholesaleOrderItemId === id) {
        let orderItem = this._formatItem(item)
        orderItem = { ...orderItem, deleted: true }
        itemsList.push(orderItem)
      }
    })
    this._updateOrder(itemsList)
  }

  setReceivedAndLock = (status: boolean) => {
    const { orderItems } = this.props
    const lockState = status == true ? true : false
    this._updateOrder(orderItems, { isLocked: lockState })
  }

  toggleSyncModal = () => {
    if (!this.state.syncModalVisible) {
      const orderId = this.props.match.params.orderId
      this.props.getSyncQboOrderDetails(orderId);
    }
    this.setState({
      syncModalVisible: !this.state.syncModalVisible
    })
  }

  onChangeBillType = (e: any) => {
    const { value } = e.target
    const { selectedBill } = this.state
    const { syncedQboBills } = this.props
    if (value === 2 && selectedBill === null && syncedQboBills.length) {
      this.setState({
        selectedBill: syncedQboBills[0].qboId
      })
    }
    this.setState({
      billType: value
    })
  }

  checkSyncItem = (e, id) => {
    let checkedItems = [...this.state.checkedItems];
    if (e.target.checked) {
      checkedItems.push(id);
    } else {
      checkedItems.splice(checkedItems.indexOf(id), 1);
    }
    this.setState({
      checkedItems
    })
  }

  syncItems = () => {
    const { syncingQboOrder } = this.props
    const { billType, selectedBill, checkedItems } = this.state
    if (syncingQboOrder || checkedItems.length === 0) {
      return;
    }
    // this.toggleSyncModal();
    const body = {
      // qboId(optional)
      orderItemList: checkedItems
    }
    if (billType === 2) {
      body.qboId = selectedBill
    }

    this.props.syncQboOrderItems({
      orderId: this.props.match.params.orderId,
      body
    })
  }

  onSelectBill = (selected) => {
    this.setState({
      selectedBill: selected
    })
  }

  render() {
    const {
      currentOrder,
      orderItems,
      oneOffItems,
      relatedBills,
      currentUser,
      currentProducts,
      finalizing,
      sellerSetting,
      repack,
      syncedItemList,
      syncedQboBills,
      syncingQboOrder
    } = this.props
    const { checkedItems, selectedBill, billType } = this.state
    const NSAccount = localStorage.getItem(CACHED_NS_LINKED)
    if (!currentOrder || currentOrder.wholesaleClient.type !== 'VENDOR') {
      return <PageLayout noSubMenu={true} currentTopMenu={'menu-Purchasing-Purchase Orders'} />
    }

    let error = ''
    if (orderItems && orderItems.length > 0) {
      orderItems.forEach((record) => {
        if (record.qtyConfirmed !== null && record.quantity !== record.qtyConfirmed) {
          if (record.quantity < record.qtyConfirmed) {
            error = 'Errors On Order'
          } else {
            error = 'Warning On Order'
          }
        }
      })
    }

    return (
      <PageLayout
        noSubMenu={true}
        currentTopMenu={'menu-Purchasing-Purchase Orders'}
        style={{
          // paddingLeft: 50,
          minHeight: 'calc(100vh - 59px)',
        }}
      >
        <a
          href="javascript:;"
          onClick={this.props.goBack}
          style={{ paddingLeft: 70, position: 'absolute', left: 0, top: 23, zIndex: 1000 }}
        >
          <ThemeIcon type="arrow-left" viewBox="0 0 20 20" width={13} height={11} style={{ color: 'white' }} />
        </a>
        <PurchaseNavHeader
          order={currentOrder}
          errorMsg={error}
          orderItems={orderItems}
          sendEmailPdf={this.props.sendEmailToVendor}
          syncQBOOrder={this.props.syncQBOOrder}
          syncNSOrder={this.props.syncNSOrder}
          setReceivedAndLock={this.setReceivedAndLock}
          propsValue={this.props}
          syncedQboBills={this.props.syncedQboBills}
          showSyncModal={this.toggleSyncModal}
        />

        <ThemeSpin spinning={finalizing}>
          <TabsWrapper className="page-tab purchase">
            <Tabs
              defaultActiveKey={currentUser.accountType != UserRole.WAREHOUSE ? '1' : '4'}
              onChange={this.onChangeTab}
              activeKey={this.state.tabKey}
            >
              {currentUser.accountType != UserRole.WAREHOUSE && (
                <TabPane
                  tab={`Order${
                    orderItems.length > 0 ? `${orderItems.length > 1 ? 's' : ''} (${orderItems.length})` : ''
                  }`}
                  key="1"
                >
                  <Layout style={{ backgroundColor: 'white', paddingRight: 30 }}>
                    <OrderTab
                      currentProducts={currentProducts}
                      orderId={this.props.match.params.orderId}
                      orderItems={orderItems}
                      currentOrder={currentOrder}
                      handleSave={this.onSave}
                      handleAddItem={this.handleAddItem}
                      handleRemove={this.handleRemoveItem}
                      updatePOItemUom={this.props.updatePOItemUom}
                      handlerUpdatePOUOMRedux={this.props.handlerUpdatePOUOMRedux}
                      updateProduct={this.props.updateProduct}
                      // For UOM
                      companyProductTypes={this.props.companyProductTypes}
                      getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
                      setCompanyProductType={this.props.setCompanyProductType}
                      deleteProductType={this.props.deleteProductType}
                      updateProductType={this.props.updateProductType}
                      setReceivedAndLock={this.setReceivedAndLock}
                      getPurchaseOrderItemHistory={this.props.getPurchaseOrderItemHistory}
                      itemHistory={this.props.purchaseOrderItemHistory}
                      getPurchaseOrderItemCost={this.props.getPurchaseOrderItemCost}
                      itemCost={this.props.purchaseOrderItemCost}
                      sellerSetting={sellerSetting}
                      updateSalesPrice={this.props.updateSalesPrice}
                      resetLoading={this.props.resetLoading}
                      printSetting={this.props.printSetting}
                      updateOrderQuantityLoading={this.props.updateOrderQuantityLoading}
                      tableLoading={this.props.loading}
                    />
                  </Layout>
                </TabPane>
              )}

              {currentUser.accountType != UserRole.WAREHOUSE && (
                <TabPane tab={`Repacks (${this.props.repack?.repackItems?.length || 0})`} key="11">
                  <RepackTab {...this.props} />
                </TabPane>
              )}

              {currentUser.accountType != UserRole.WAREHOUSE && (
                <TabPane
                  tab={`Extra Charge${
                    oneOffItems.length > 0 ? `${oneOffItems.length > 1 ? 's' : ''} (${oneOffItems.length})` : ''
                  }`}
                  key="2"
                >
                  <PurchaseOrderExtraCharge {...this.props} />
                </TabPane>
              )}

              {currentUser.accountType != UserRole.WAREHOUSE && sellerSetting && sellerSetting.company.costAllocationsTab === 1 && currentOrder && (
                <TabPane tab="Cost Allocations" key="10">
                  <PurchaseOrderCostAllocations {...this.props} />
                </TabPane>
              )}

              {currentUser.accountType != UserRole.WAREHOUSE &&
                NSAccount == 'null' &&
                sellerSetting &&
                sellerSetting.company.costAllocationsTab === 2 && (
                  <TabPane tab={`Related Bills${relatedBills.length > 0 ? ` (${relatedBills.length})` : ''}`} key="9">
                    <PurchaseOrderRelatedBills {...this.props} />
                  </TabPane>
                )}

              <TabPane tab={`Logistics`} key="8">
                <Logistics {...this.props} />
              </TabPane>

              <TabPane tab={`Receiving`} key="4">
                <Receiving {...this.props} />
              </TabPane>
              {currentUser.accountType != UserRole.WAREHOUSE && (
                <TabPane tab="Pricing/Costing" key="5">
                  <PurchasePricingAnalysis {...this.props} />
                </TabPane>
              )}
              <TabPane tab={`Documents`} key="6">
                <PurchaseDocuments {...this.props} />
              </TabPane>
              {currentUser.accountType !== UserRole.SALES &&
                currentUser.accountType != UserRole.BUYER &&
                currentUser.accountType != UserRole.WAREHOUSE && (
                  <TabPane tab={`Profitability`} key="7">
                    <PurchaseProfitability {...this.props} />
                  </TabPane>
                )}
              {/* <TabPane tab={`Credit Memo`} key="8">
                <MemoTotalTable orderId={this.props.match.params.orderId} isPurchase={true} />
              </TabPane> */}
          {(currentUser.accountType == UserRole.ADMIN || currentUser.accountType == UserRole.SUPERADMIN) && (
            <TabPane tab="Activity Log" key="12">
              <ActivityLog />
            </TabPane>
          )}
            </Tabs>
          </TabsWrapper>
        </ThemeSpin>


        <ThemeModal
          className='dark sync-qbo-modal'
          title='Sync items to QBO'
          visible={this.state.syncModalVisible}
          onCancel={this.toggleSyncModal}
          width={651}
          footer={[
            <Flex>
              <ThemeButton className={`dark ${syncingQboOrder || checkedItems.length === 0 ? 'disabled' : ''}`} type="primary" onClick={this.syncItems}>
                Sync items
              </ThemeButton>
              <ThemeIconButton className="no-border dark" onClick={this.toggleSyncModal} style={{marginLeft: 24}}>Cancel</ThemeIconButton>
            </Flex>,
          ]}
        >
          <ThemeSpin spinning={syncingQboOrder}>
            <p>Select items to sync to a QBO vendor bill.</p>
            <Flex className="content">
              <div className="items">
                {syncedItemList.map((oi: any) => (
                  <label className='new' key={`oi-${oi.wholesaleOrderItemId}`}>
                    <ThemeCheckBox checked={checkedItems.indexOf(oi.wholesaleOrderItemId) >= 0} onChange={e => this.checkSyncItem(e, oi.wholesaleOrderItemId)} disabled={oi.qboBillId} />
                    <span style={{ marginLeft: 7, color: oi.qboBillId ? '#82898A' : '#22282A' }}>{oi.itemName} (Received: ${mathRoundFun(oi.receivedQty * oi.cost, 2)})</span>
                  </label>
                ))}
              </div>
              <div className="bill">
                <Radio.Group onChange={this.onChangeBillType} value={billType}>
                  <ThemeRadio value={1}>Create new vendor bill</ThemeRadio>
                  <ThemeRadio value={2}>Add to existing vendor bill</ThemeRadio>
                </Radio.Group>
                {billType === 2 && (
                  <ThemeSelect
                    value={selectedBill}
                    onChange={this.onSelectBill}
                    style={{ minWidth: 100 }}
                    disabled={syncedQboBills.length === 0}
                  >
                    {syncedQboBills.map((bill) => (
                      <Select.Option key={`bill-${bill.qboId}`} value={bill.qboId}>{bill.docNumber}</Select.Option>
                    ))}
                  </ThemeSelect>
                )}
              </div>
            </Flex>
          </ThemeSpin>
        </ThemeModal>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return {
    ...state.orders,
    currentUser: state.currentUser,
  }
}
export default withTheme(connect(OrdersModule)(mapStateToProps)(PurchaseOverview))
