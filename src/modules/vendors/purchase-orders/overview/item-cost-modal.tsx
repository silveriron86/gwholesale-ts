import * as React from 'react'

import {
  ThemeButton,
  ThemeCheckbox,
  ThemeInputNumber,
  ThemeModal,
  ThemeTextButton,
} from '~/modules/customers/customers.style'
import { basePriceToRatioPrice, mathRoundFun, ratioPriceToBasePrice } from '~/common/utils'

import Divider from 'antd/lib/divider'
import EditableTable from '~/components/Table/editable-table'
import { EnterPurchaseWrapper } from '~/modules/orders/components/orders-header.style'
import { Flex, Flex1 } from '~/modules/inventory/components/inventory-header.style'
import { Title } from '~/modules/delivery/deliveries/route-card/card.style'
import _ from 'lodash'
import { cloneDeep } from 'lodash'

interface ItemCostModalProps {
  selectedItem: any
  showModal: boolean
  onClose: (e: any) => void
  onSave: (e: any, data: any) => void
  itemCost: any[]
  getPurchaseOrderItemCost: Function
  newCost: number
}

export class ItemCostModal extends React.PureComponent<ItemCostModalProps> {
  componentWillReceiveProps(nextProps: any) {
    if (!this.props.showModal && nextProps.showModal && nextProps.selectedItem) {
      this.props.getPurchaseOrderItemCost(nextProps.selectedItem.wholesaleOrderItemId)
    }
    this.setState({
      orders: this.props.itemCost,
      shippedOrder: false,
      newCost: this.props.newCost,
    })
  }
  state: { shippedOrder: boolean; orders: any[]; newCost: number }
  constructor(props: ItemCostModalProps) {
    super(props)
    this.state = {
      shippedOrder: false,
      orders: [],
      newCost: 0,
    }
  }
  changeNewPrice = (record: any, newPrice: number) => {
    const { orders } = this.state

    let cloneOrders = cloneDeep(orders)
    cloneOrders.forEach((order: any) => {
      if (order.wholesaleOrderItemId == record.wholesaleOrderItemId && _.isNumber(newPrice)) {
        order.newPrice = mathRoundFun(newPrice, 2)
        const pricingUOM = order.pricingUOM ? order.pricingUOM : order.inventoryUOM

        order.wholesaleProductUomList = this.props.selectedItem.wholesaleProductUomList
        const basePrice = ratioPriceToBasePrice(pricingUOM, order.newPrice, order, 12)
        const newMargin = mathRoundFun(((basePrice - this.props.newCost) / basePrice) * 100, 2)
        if (isFinite(newMargin)) {
          order.newMargin = newMargin
        }
      } else if (order.wholesaleOrderItemId == record.wholesaleOrderItemId) {
        order.newPrice = null
        order.newMargin = null
      }
    })

    this.setState({
      orders: cloneOrders,
    })
  }
  getAllChangedPrice = () => {
    const allChangedItems = this.state.orders
      .filter((item) => _.isNumber(item.newPrice))
      .map((item) => {
        return { wholesaleOrderItemId: item.wholesaleOrderItemId, price: item.newPrice, isCustom: true }
      })
    return allChangedItems
  }

  changeNewMargin = (record: any, newMargin: number) => {
    const { orders } = this.state
    let cloneOrders = cloneDeep(orders)
    cloneOrders.forEach((order: any) => {
      if (order.wholesaleOrderItemId == record.wholesaleOrderItemId && newMargin) {
        order.newMargin = mathRoundFun(newMargin, 2)

        //to basePrice

        //price = cost / ( 1- margin）
        const temp = 1 - order.newMargin / 100
        const basePrice = this.props.newCost / temp

        const pricingUOM = order.pricingUOM ? order.pricingUOM : order.inventoryUOM

        //to ratioPrice
        order.wholesaleProductUomList = this.props.selectedItem.wholesaleProductUomList

        const ratioPrice = basePriceToRatioPrice(pricingUOM, basePrice, order, 12)
        order.newPrice = mathRoundFun(ratioPrice, 2)
      } else if (order.wholesaleOrderItemId == record.wholesaleOrderItemId) {
        order.newPrice = null
        order.newMargin = null
      }
    })

    this.setState({
      orders: cloneOrders,
    })
  }

  shippedOrder = (e: any) => {
    this.setState({
      shippedOrder: e.target.checked,
    })
  }

  filterOrders = () => {
    return this.state.shippedOrder
      ? this.state.orders
      : this.state.orders?.filter((order) => order.wholesaleOrderItemStatus != 'SHIPPED')
  }

  calcPriceStrategy = (value: any, record: any) => {
    switch (value) {
      case 'CUSTOM_PRICE':
        return 'Custom'
      case 'FOLLOW_DEFAULT_SALES':
        return 'Default price'
      case 'FOLLOW_GROUP':
        return `${record['margin' + record.pricingGroup]}% Margin`
      case 'FOLLOW_LAST_SOLD':
        return 'FOLLOW_LAST_SOLD'
      default:
        return
    }
  }

  calcPrice = (record: any) => {
    const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
    return basePriceToRatioPrice(pricingUOM, record.price, this.props.selectedItem, 2)
  }
  render() {
    const columns = [
      {
        title: 'Order No.',
        key: 'wholesaleOrderItemId',
        dataIndex: 'wholesaleOrderItemId',
        align: 'center',
        render: (value: any, record: any) => {
          return <>{`#${value}`}</>
        },
      },
      {
        title: 'Customer',
        key: 'clientCompanyName',
        dataIndex: 'clientCompanyName',
        align: 'center',
      },
      {
        title: 'Status',
        key: 'wholesaleOrderItemStatus',
        dataIndex: 'wholesaleOrderItemStatus',
        align: 'center',
      },
      {
        title: 'Pricing strategy',
        key: 'pricingLogic',
        dataIndex: 'pricingLogic',
        align: 'center',
        render: (value: any, record: any) => {
          return <>{this.calcPriceStrategy(value, record)}</>
        },
      },
      {
        title: 'PAS?',
        key: 'pas',
        dataIndex: 'pas',
        align: 'center',
        render: (value: any, record: any) => {
          return <>{value ? 'Yes' : 'No'}</>
        },
      },
      {
        title: 'Current price',
        key: 'price',
        dataIndex: 'price',
        align: 'center',
        render: (value: any, record: any) => {
          return <>{`$${this.calcPrice(record)}/${record.pricingUOM ? record.pricingUOM : record.inventoryUOM}`}</>
        },
      },
      {
        title: 'New price',
        key: 'newPrice',
        dataIndex: 'newPrice',
        // width: 200,
        render: (value: any, record: any) => {
          return (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <div style={{ width: 100 }}>
                <ThemeInputNumber
                  formatter={(value) => `${value ? `$${value}` : ''}`}
                  value={value}
                  onChange={this.changeNewPrice.bind(this, record)}
                  style={{ marginRight: 4, textAlign: 'left' }}
                />
              </div>
              <Flex1>/ {`${record.pricingUOM ? record.pricingUOM : record.inventoryUOM}`}</Flex1>
            </div>
          )
        },
      },
      {
        title: 'New margin',
        key: 'newMargin',
        dataIndex: 'newMargin',
        align: 'center',
        // width: 150,
        render: (value: any, record: any) => {
          return (
            <div style={{ width: 100, display: 'block', marginLeft: 'auto', marginRight: 'auto' }}>
              <ThemeInputNumber
                formatter={(value) => `${value ? `${value}%` : ''}`}
                value={value}
                parser={(value) => value?.replace('%', '')}
                onChange={this.changeNewMargin.bind(this, record)}
                style={{ marginRight: 4, width: 100, textAlign: 'left' }}
              />
            </div>
          )
        },
      },
    ]
    return (
      <ThemeModal
        title={`Update item pricing`}
        width={'75%'}
        visible={this.props.showModal}
        onCancel={this.props.onClose}
        destroyOnClose={true}
        footer={
          <Flex>
            <ThemeButton
              className="cancel-btn"
              onClick={() => {
                const data = this.getAllChangedPrice()
                this.props.onSave(this.props.selectedItem, data)
              }}
              style={{ color: 'white' }}
            >
              Save pricing
            </ThemeButton>
            <ThemeTextButton onClick={this.props.onClose}>Cancel</ThemeTextButton>
          </Flex>
        }
        style={{ minWidth: 1000 }}
      >
        <EnterPurchaseWrapper>
          <Title>
            Units from this lot have been added to the following sales orders. Please confirm the item pricing for these
            orders, give the change to the lot cost.
          </Title>
          <Divider style={{ height: 0 }} />
          <ThemeCheckbox checked={this.state.shippedOrder} onChange={this.shippedOrder}>
            Show shipped orders
          </ThemeCheckbox>
          <Divider style={{ height: 0 }} />
          <EditableTable columns={columns} dataSource={this.filterOrders()} rowKey="wholesaleOrderItemId" />
        </EnterPurchaseWrapper>
      </ThemeModal>
    )
  }
}

export default ItemCostModal
