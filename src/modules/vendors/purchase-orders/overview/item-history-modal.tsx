import moment from 'moment'
import * as React from 'react'
import { getOrderPrefix, inventoryPriceToRatioPrice, inventoryQtyToRatioQty } from '~/common/utils'
import { ThemeButton, ThemeModal, ThemeTable } from '~/modules/customers/customers.style'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import { EnterPurchaseWrapper } from '~/modules/orders/components/orders-header.style'
import { history } from '~/store/history'

interface ItemHistoryModalProps {
  selectedItem: any
  showModal: boolean
  onClose: Function
  getPurchaseOrderItemHistory: Function
  itemHistory: any[]
  sellerSetting: any
}

export class ItemHistoryModal extends React.PureComponent<ItemHistoryModalProps> {
  componentWillReceiveProps(nextProps: any) {
    if (!this.props.showModal && nextProps.showModal && nextProps.selectedItem) {
      this.props.getPurchaseOrderItemHistory(nextProps.selectedItem.wholesaleOrderItemId)
    }
  }

  render() {
    const { itemHistory, sellerSetting, selectedItem } = this.props
    const columns = [
      {
        title: 'Order Date',
        key: 'orderDate',
        dataIndex: 'orderDate',
        align: 'center',
        render: (orderDate: number) => {
          return moment(orderDate).format('MM/DD/YYYY')
        }
      },
      {
        title: 'PO #',
        key: 'wholesaleOrderId',
        dataIndex: 'wholesaleOrderId',
        align: 'center',
        render: (orderId: number) => {
          return (
            <span className="hoverable" onClick={() => history.push(`/order/${orderId}/purchase-cart`)}>
              {getOrderPrefix(sellerSetting, 'purchase')}{orderId}
            </span>
          )
        },
      },
      {
        title: 'Vendor',
        key: 'vendor',
        dataIndex: 'vendor',
        align: 'center',
        render: (vendor: string, record: any) => {
          return (
            <span className="hoverable" onClick={() => history.push(`/vendor/${record.clientId}/orders`)}>
              {vendor}
            </span>
          )
        },
      },
      {
        title: 'Units',
        key: 'quantity',
        dataIndex: 'quantity',
        align: 'center',
        render: (quantity: number, record: any) => {
          if (selectedItem) {
            let units = 0
            const unitUOM = record.overrideUOM ? record.overrideUOM : record.inventoryUOM
            if (record.receivedQty) {
              units = record.receivedQty
            } else if (record.qtyConfirmed) {
              units = record.qtyConfirmed
            } else {
              units = quantity
            }
            units = inventoryQtyToRatioQty(unitUOM, units, selectedItem)
            return `${units} ${unitUOM}`
          }
        },
      },
      {
        title: 'Status',
        key: 'status',
        dataIndex: 'status',
        align: 'center',
        render: (status: string) => {
          let renderStatus = status === 'PLACED' ? 'Ordered' : status
          return renderStatus ? renderStatus.charAt(0).toUpperCase() + renderStatus.slice(1).toLowerCase() : ''
        },
      },
      {
        title: 'Cost',
        key: 'cost',
        dataIndex: 'cost',
        align: 'center',
        render: (cost: number, record: any) => {
          if (selectedItem) {
            const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
            return record.pas
              ? 'PAS'
              : `$${inventoryPriceToRatioPrice(pricingUOM, cost, selectedItem).toFixed(2)}/${pricingUOM}`
          }
        },
      },
    ]
    return (
      <ThemeModal
        title={`Cost History for ${selectedItem && selectedItem.SKU ? selectedItem.SKU : ''} - ${selectedItem && selectedItem.variety ? selectedItem.variety : ''
          } `}
        width={'75%'}
        visible={this.props.showModal}
        onCancel={this.props.onClose}
        footer={
          <Flex>
            <ThemeButton className="cancel-btn" onClick={this.props.onClose} style={{ color: 'white' }}>
              OK
            </ThemeButton>
          </Flex>
        }
        style={{ minWidth: 1000 }}
      >
        <EnterPurchaseWrapper>
          <ThemeTable
            columns={columns}
            dataSource={itemHistory}
            rowKey="wholesaleOrderItemId"
            className="enter-purchase-modal"
          />
        </EnterPurchaseWrapper>
      </ThemeModal>
    )
  }
}

export default ItemHistoryModal
