import { Divider, Icon, Select } from 'antd'
import * as React from 'react'
import { InputLabel, ThemeInput, ThemeSelect } from '~/modules/customers/customers.style'
import { DialogBodyDiv, DialogSubContainer, FlexDiv, Item, ValueLabel } from '~/modules/customers/nav-sales/styles'
import { OrderItem } from '~/schema'


type LotSpecificationModalProps = {
  record: OrderItem
  extraCOO: any[]
  openAddNewCOOModal: Function
  openAddNewBrandModal: Function
  handleSave: Function
}

class LotSpecificationModal extends React.PureComponent<LotSpecificationModalProps> {
  state = {
    record: this.props.record,
    addNewExtraOriginModal: false,
  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps: LotSpecificationModalProps) {
    const nextPropsRecord = JSON.stringify(nextProps.record);
    if(JSON.stringify(this.state.record) !== nextPropsRecord) {
      this.setState({
        record: JSON.parse(nextPropsRecord)
      })
    }
  }

  onTextChange = (type: string, value: string) => {
    let obj = { ...this.state.record }
    console.log(obj, type, value)
    obj[type] = value
    this.setState({ record: obj })
  }

  onSelectChange = (type: string, value: any) => {
    const { extraCOO } = this.props
    let row = { ...this.state.record }
    if (type == 'extraOrigin') {
      const found = extraCOO.find((el) => el.id == value)
      if (found) {
        row[type] = found.name
      } else {
        return
      }
    } else {
      row[type] = value
    }
    this.setState({ record: row })
  }

  saveLotSpecInfo = () => {
    this.props.handleSave(this.state.record, 'lotSpec')
  }

  render() {
    const { extraCOO } = this.props
    const { grossWeight, grossVolume, record } = this.state
    if(!record) {
      return null;
    }
    const currentSuppliers = record.suppliers ? record.suppliers.split(', ').map((el: string) => el.trim()) : []
    return (
      <>
        <DialogSubContainer className='bordered'>
          <FlexDiv className="space-between">
            <Item>
              <InputLabel>Item</InputLabel>
              <ValueLabel className='medium'>{record ? record.variety : ''}</ValueLabel>
            </Item>
            <Item>
              <InputLabel>Lot</InputLabel>
              <ValueLabel className='medium'>{record ? record.lotId : ''}</ValueLabel>
            </Item>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <InputLabel className="mt-12">Brand</InputLabel>
          <ThemeSelect
            style={{ width: '50%' }}
            onChange={(value: any) => this.onSelectChange('modifiers', value)}
            value={record.modifiers}
            showSearch
            filterOption={(input: any, option: any) => {
              if (option.props.children) {
                return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              } else {
                return true
              }
            }}
            dropdownMatchSelectWidth={false}
            dropdownRender={(el: any) => {
              return (
                <div>
                  {el}
                  <Divider style={{ margin: '4px 0' }} />
                  <div
                    style={{ padding: '4px 8px', cursor: 'pointer' }}
                    onMouseDown={(e) => e.preventDefault()}
                    onClick={(e) => this.props.openAddNewBrandModal(record)}
                  >
                    <Icon type="plus" /> Add New
                  </div>
                </div>
              )
            }}
          >
            <Select.Option value="" />
            {currentSuppliers.map((el: any, index: number) => {
              return (
                <Select.Option key={`${el}_${index}`} value={el} className="select-option">
                  {el}
                </Select.Option>
              )
            })}
          </ThemeSelect>

          <InputLabel className="mt-12">Origin</InputLabel>
          <ThemeSelect
            value={record.extraOrigin}
            onChange={(value: any) => this.onSelectChange('extraOrigin', value)}
            style={{ width: '50%' }}
            showSearch
            filterOption={(input: any, option: any) => {
              if (option.props.children) {
                return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              } else {
                return true
              }
            }}
            dropdownMatchSelectWidth={false}
            dropdownRender={(el: any) => {
              return (
                <div>
                  {el}
                  <Divider style={{ margin: '4px 0' }} />
                  <div
                    style={{ padding: '4px 8px', cursor: 'pointer' }}
                    onMouseDown={(e) => e.preventDefault()}
                    onClick={(e) => this.props.openAddNewCOOModal(record)}
                  >
                    <Icon type="plus" /> Add New
                  </div>
                </div>
              )
            }}
          >
            <Select.Option value="" />
            {extraCOO.length &&
              extraCOO.map((p, i) => {
                return (
                  <Select.Option value={p.id} key={`${p.name}_${i}`}>
                    {p.name}
                  </Select.Option>
                )
              })}
          </ThemeSelect>
          <InputLabel className="mt-12">Unit Gross Volume {record.grossVolumeUnit ? '- ' + record.grossVolumeUnit : ''}</InputLabel>
          <ThemeInput
            style={{ width: '50%' }}
            defaultValue={record.grossVolume || ''}
            value={record.grossVolume || ''}
            onChange={(evt: any) => this.onTextChange('grossVolume', evt.target.value)}
          />
          <InputLabel className="mt-12">Unit Gross Weight {record.grossWeightUnit ? '- ' + record.grossWeightUnit : ''}</InputLabel>
          <ThemeInput
            style={{ width: '50%' }}
            defaultValue={record.grossWeight || ''}
            value={record.grossWeight || ''}
            onChange={(evt: any) => this.onTextChange('grossWeight', evt.target.value)}
          />
        </DialogBodyDiv>
      </>
    )
  }
}

export default LotSpecificationModal
