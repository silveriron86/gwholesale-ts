import React from 'react'
import { transLayout } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { Layout, Row, Col } from 'antd'
import {
  PoDescription,
  PoDescriptionValue,
  poListRow,
  PoContent,
  PoDollar,
  PoHeaderLabel,
  PoLabel,
  poOverview,
  PoOverviewRow,
  PooValue,
} from '../_style'
import { ThemeTextArea } from '~/modules/customers/customers.style'
import { disabledGrey } from '~/common'
import { formatNumber } from '~/common/utils'

interface OrderRowProps {
  item: any
}

export class OrderRow extends React.PureComponent<OrderRowProps> {
  render() {
    const { item } = this.props
    let subtotal = 0
    if (item.constantRatio === true) {
      subtotal = Number.parseFloat(item.cost ? item.cost : 0) * Number.parseFloat(item.catchWeightQty ? item.catchWeightQty : 0)
    }
    return (
      <Layout style={poListRow}>
        <PoDescription>{item.variety}</PoDescription>
        <PoDescriptionValue>{item.variety}</PoDescriptionValue>
        <PoContent>
          <Layout style={transLayout}>
            <Row>
              <Col md={8}>
                <PoDollar>{`$${item.constantRatio === true ? subtotal.toFixed(2) : formatNumber((item.cost + item.freight) * item.qtyConfirmed, 2)}`}</PoDollar>
                <PoHeaderLabel>ESTIMATED COST SUBTOTAL</PoHeaderLabel>
              </Col>
              <Col md={8}>
                <PoDollar>{`$${item.constantRatio === true ? subtotal.toFixed(2) : formatNumber((item.cost + item.freight) * item.receivedQty, 2)}`}</PoDollar>
                <PoHeaderLabel>ACTUAL COST SUBTOTAL</PoHeaderLabel>
              </Col>
              <Col md={8}>
                <PoDollar>{item.modifiers ? item.modifiers : ''}</PoDollar>
                <PoHeaderLabel>PROVIDER</PoHeaderLabel>
              </Col>
            </Row>
          </Layout>
          <Layout style={poOverview}>
            <Row>
              <Col md={8} style={{ paddingRight: 23, borderRight: `1px solid ${disabledGrey}` }}>
                <PoOverviewRow>
                  <PooValue>SKU</PooValue>
                  <PooValue>{item.SKU}</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px', borderRight: `1px solid ${disabledGrey}` }}>
                  <PooValue>Units confirmed</PooValue>
                  <PooValue>{item.qtyConfirmed}</PooValue>
                </PoOverviewRow>
              </Col>
              {/* <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px' }}>
                  <PooValue>Price Formular</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col> */}
              <Col md={8} style={{ padding: '0 23px' }}>
                <PoOverviewRow>
                  <PooValue>UOM</PooValue>
                  <PooValue>{item.UOM}</PooValue>
                </PoOverviewRow>
              </Col>
            </Row>
            <Row>
              <Col md={8} style={{ paddingRight: 23, borderRight: `1px solid ${disabledGrey}` }}>
                <PoOverviewRow>
                  <PooValue>Units orderded</PooValue>
                  <PooValue>{item.quantity}</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px', borderRight: `1px solid ${disabledGrey}` }}>
                  <PooValue>Cost/Unit</PooValue>
                  <PooValue>{item.cost}</PooValue>
                </PoOverviewRow>
              </Col>
              {/* <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px' }}>
                  <PooValue>Carrier</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col> */}
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px' }}>
                  <PooValue>Units received/net weight</PooValue>
                  <PooValue>{item.onHandQty}</PooValue>
                </PoOverviewRow>
              </Col>
            </Row>
            {/* <Row>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px', borderRight: `1px solid ${disabledGrey}` }}>
                  <PooValue>Freight Cost</PooValue>
                  <PooValue>{item.freight}</PooValue>
                </PoOverviewRow>
              </Col>
            </Row> */}
          </Layout>
          <PoLabel>Notes</PoLabel>
          <ThemeTextArea rows={3} placeholder="Enter notes here..." value={item.note} />
        </PoContent>
      </Layout>
    )
  }
}

export default OrderRow
