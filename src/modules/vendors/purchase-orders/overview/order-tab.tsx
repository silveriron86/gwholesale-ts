import { Icon, Layout, Tabs } from 'antd'
import { OrderDetail, OrderItem } from '~/schema'
import { PurchaseSearchWrapper, SubTabs, tabSearch } from '../_style'
import { ThemeModal, ThemeOutlineButton, ThemeSwitch, floatRight } from '~/modules/customers/customers.style'

import { Icon as IconSVG } from '~/components'
import OrderRow from './order-row'
import ProductModal from '~/modules/customers/sales/cart/modals/product-modal'
import React from 'react'
import SearchInput from '~/components/Table/search-input'
import TableView from './table-view'
import jQuery from 'jquery'
import { margin } from 'polished'
import { notify } from '~/common/utils'
import { withTheme } from 'emotion-theming'
import { gray01 } from '~/common'

interface OrderTabProps {
  currentProducts?: any[]
  orderId: string
  orderItems: OrderItem[]
  currentOrder: OrderDetail
  handleSave: Function
  handlerUpdatePOUOMRedux: Function
  handleAddItem: Function
  handleRemove: Function
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
  deleteProductType: Function
  updateProductType: Function
  updatePOItemUom: Function
  setReceivedAndLock: Function
  companyProductTypes: any
  itemHistory: any[]
  getPurchaseOrderItemHistory: Function
  updateProduct: Function
  sellerSetting: any
  itemCost: any[]
  getPurchaseOrderItemCost: Function
  updateSalesPrice: Function
  resetLoading: Function
  printSetting: any
  updateOrderQuantityLoading: any
  tableLoading: boolean
}

const { TabPane } = Tabs

export class OrderTab extends React.PureComponent<OrderTabProps> {
  state = {
    searchStr: '',
    modalSearch: '',
    newModalVisible: false,
    isChecked: this.props.currentOrder.wholesaleOrderStatus == 'RECEIVED',
    footerSelected: '',
    addedItemId: false,
  }

  handleSelectOk = () => {
    this.setState({
      newModalVisible: false,
      modalSearch: '',
    })
  }

  componentDidMount() {
    let map = { 65: false, 18: false }
    jQuery('body')
      .unbind('keydown')
      .bind('keydown', (e: any) => {
        if (e.keyCode == 27) {
          if (this.state.newModalVisible) {
            jQuery('.purchase-add-item.ant-btn').trigger('focus')
          }
        }
        if (e.keyCode in map) {
          map[e.keyCode] = true
        }
        if (map[18] && map[65]) {
          setTimeout(function() {
            jQuery('.purchase-add-item.ant-btn').trigger('focus')
            jQuery('.purchase-add-item.ant-btn').trigger('click')
          }, 30)
        }
      })
      .bind('keyup', (e: any) => {
        if (e.keyCode in map) {
          map[e.keyCode] = false
        }
      })
  }

  componentWillReceiveProps(nextProps: any) {
    if (
      this.props.currentOrder &&
      nextProps.currentOrder &&
      this.props.currentOrder.wholesaleOrderStatus != nextProps.currentOrder.wholesaleOrderStatus
    ) {
      this.setState({ isChecked: nextProps.currentOrder.wholesaleOrderStatus == 'RECEIVED' })
    }
  }

  onModalSearch = (text: string) => {
    this.setState({
      modalSearch: text,
    })
  }

  onFooterSearch = (text: string) => {
    this.setState({
      footerSelected: text,
    })
  }

  handleAddOrderItem = (item: any) => {
    // tslint:disable-next-line:no-console
    this.props.resetLoading()
    this.props.handleAddItem(item)
    this.setState({
      addedItemId: true,
    })
    // this.handleSelectOk()
  }

  openAddModal = () => {
    this.setState({
      newModalVisible: true,
    })
    jQuery(`.modal-order-tab`)
      .find('.ant-table-row')
      .removeClass('focused-row')
    setTimeout(() => {
      jQuery(`.modal-order-tab`)
        .find('.ant-input')[0]
        .focus()
    }, 0)
  }

  onSearch = (text: string) => {
    this.setState({
      searchStr: text,
    })
  }

  render() {
    const { searchStr, newModalVisible, modalSearch, isChecked } = this.state
    const { orderId, orderItems, currentOrder, currentProducts, tableLoading } = this.props

    const filteredItems =
      orderItems.length > 0
        ? orderItems.filter((row) => {
            return (
              (row.variety && row.variety.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) ||
              (row.SKU && row.SKU.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) ||
              (row.UOM && row.UOM.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0)
            )
          })
        : []
    return (
      <Layout onClick = {()=>{
        this.setState({
          addedItemId: false,
        })
      }}style={{ padding: 10, background: 'transparent', position: 'relative' }}>
        <ThemeModal
          closable={false}
          keyboard={true}
          okText="Close [esc]"
          okButtonProps={{ shape: 'round' }}
          cancelButtonProps={{ style: { display: 'none' } }}
          width={'75%'}
          visible={newModalVisible}
          onOk={this.handleSelectOk}
          onCancel={this.handleSelectOk}
        >
          <ProductModal
            currentProducts={currentProducts}
            visible={newModalVisible}
            isAdd={true}
            selected={modalSearch}
            onSearch={this.onModalSearch}
            onSelect={this.handleAddOrderItem}
            salesType="PURCHASE"
          />
        </ThemeModal>
        <SubTabs defaultActiveKey="1" type="line" animated={false}>
          <TabPane
            tab={
              <span>
                <Icon type="table" />
                TABLE VIEW
              </span>
            }
            key="1"
          >
            <TableView
              currentProducts={currentProducts}
              orderId={orderId}
              orderStatus={currentOrder.wholesaleOrderStatus}
              searchStr={searchStr}
              items={orderItems}
              isLocked={currentOrder.isLocked}
              handleSave={this.props.handleSave}
              handleRemove={this.props.handleRemove}
              updatePOItemUom={this.props.updatePOItemUom}
              handlerUpdatePOUOMRedux={this.props.handlerUpdatePOUOMRedux}
              updateProduct={this.props.updateProduct}
              // For UOM
              companyProductTypes={this.props.companyProductTypes}
              getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
              setCompanyProductType={this.props.setCompanyProductType}
              deleteProductType={this.props.deleteProductType}
              updateProductType={this.props.updateProductType}
              itemHistory={this.props.itemHistory}
              getPurchaseOrderItemHistory={this.props.getPurchaseOrderItemHistory}
              sellerSetting={this.props.sellerSetting}
              newModalVisible={newModalVisible}
              getPurchaseOrderItemCost={this.props.getPurchaseOrderItemCost}
              itemCost={this.props.itemCost}
              updateSalesPrice={this.props.updateSalesPrice}
              printable={this.props.printSetting}
              updateOrderQuantityLoading={this.props.updateOrderQuantityLoading}
              // For Footer
              footerSelected={this.state.footerSelected}
              onSearch={this.onFooterSearch}
              handleAddOrderItem={this.handleAddOrderItem}
              tableLoading={tableLoading}
              addItemId={this.state.addedItemId}
              changeAddItem={() => {
                this.setState({
                  addedItemId: false,
                })
              }}
            />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="unordered-list" />
                LIST VIEW
              </span>
            }
            key="2"
          >
            {filteredItems.map((item, index) => (
              <OrderRow key={`purchase-order-${index}`} item={item} />
            ))}
          </TabPane>
        </SubTabs>
        <div style={tabSearch}>
          {!currentOrder.isLocked && currentOrder.wholesaleOrderStatus != 'CANCEL' ? (
            <ThemeOutlineButton
              style={{ marginTop: 0, marginRight: 20 }}
              shape="round"
              onClick={this.openAddModal}
              className="purchase-add-item bold-blink purchase-cart-actions"
            >
              <Icon type="plus-circle" />
              Add Item
            </ThemeOutlineButton>
          ) : (
            ''
          )}
          <PurchaseSearchWrapper>
            <SearchInput
              handleSearch={this.onSearch}
              placeholder="Search"
              className="purchase-item-search header-last-tab purchase-cart-actions add-item-modal-input"
            />
          </PurchaseSearchWrapper>
        </div>
      </Layout>
    )
  }
}

export default withTheme(OrderTab)
