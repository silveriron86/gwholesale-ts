import * as React from 'react'
import { Modal, Layout, Row, Col } from 'antd'
import { PomCol, PomColLabel, PomColValue } from '../_style'
import { transLayout } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { OrderItem } from '~/schema'
import { formatNumber } from '~/common/utils'

interface OverviewModalProps {
  orderId: string
  showModal: boolean
  onCloseRequest: () => void
  items: OrderItem[]
}

export class OverviewModal extends React.PureComponent<OverviewModalProps> {
  render() {
    const { orderId, items } = this.props
    const rows: any[] = []
    items.forEach((item, index) => {
      let subtotal = 0
      if (item.constantRatio === true) {
        subtotal = Number.parseFloat(item.cost ? item.cost : 0) * Number.parseFloat(item.catchWeightQty ? item.catchWeightQty : 0)
      }
      rows.push(
        <tr key={`poo-first-${index}`} style={{ borderTop: '1px solid #DDD', height: 75 }}>
          <td>{index + 1}</td>
          {/* <td>{orderId}{index + 1}</td> */}
          <td>{item.lotId}</td>
          <td>{item.variety}</td>
          <td>{item.provider}</td>
          <td>{item.SKU}</td>
          <td>{item.quantity}</td>
          <td>{item.UOM}</td>
          <td>{item.cost}</td>
          <td>{item.freight}</td>
          <td>--</td>
          <td>{`$${formatNumber(item.cost + item.freight, 2)}`}</td>
          <td>--</td>
        </tr>,
      )
      rows.push(
        <tr key={`poo-second-${index}`}>
          <td colSpan={11}>
            <Layout style={transLayout}>
              <Row>
                <Col md={8}>
                  <PomCol>
                    <PomColLabel>Estimated total cost</PomColLabel>
                    <PomColValue>--</PomColValue>
                  </PomCol>
                </Col>
                <Col md={8}>
                  <PomCol>
                    <PomColLabel>Units received / net weight</PomColLabel>
                    <PomColValue>{item.onHandQty}</PomColValue>
                  </PomCol>
                </Col>
                <Col md={8}>
                  <PomCol>
                    <PomColLabel>Cost subtotal</PomColLabel>
                    <PomColValue>{`$${item.constantRatio === true ? subtotal.toFixed(2) : formatNumber((item.cost + item.freight) * item.quantity, 2)}`}</PomColValue>
                  </PomCol>
                </Col>
              </Row>
            </Layout>
          </td>
        </tr>,
      )
    })

    return (
      <Modal width={1331} footer={null} visible={this.props.showModal} onCancel={this.props.onCloseRequest}>
        {/* <Table dataSource={dataSource} columns={columns} pagination={false} /> */}
        <table style={{ marginTop: 25, width: '100%' }}>
          <thead>
            <tr>
              <th style={{ width: 50 }}>#</th>
              <th>LOT #</th>
              <th>DESCRIPTION</th>
              <th>MODIFIERS</th>
              <th>SKU</th>
              <th>UNITS ORDERED</th>
              <th>UOM</th>
              <th>COST/UNIT</th>
              <th>FREIGHT COST</th>
              <th>PRICE FORMULA</th>
              <th>SALE PRICE</th>
              <th>CARRIER</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      </Modal>
    )
  }
}

export default OverviewModal
