import React from 'react'
import { Layout, Row, Col, Select } from 'antd'
import {
  PoDescription,
  PoDescriptionValue,
  poListRow,
  PoContent,
  PoLabel,
  poROverview,
  PoOverviewRow,
  PooValue,
} from '../_style'
import { ThemeTextArea, ThemeSelect } from '~/modules/customers/customers.style'
import { disabledGrey } from '~/common'

interface ReceivingRowProps {
  item: any
}

const { Option } = Select

export class ReceivingRow extends React.PureComponent<ReceivingRowProps> {
  render() {
    const { item } = this.props
    return (
      <Layout style={poListRow}>
        <PoDescription>{item.description}</PoDescription>
        <PoDescriptionValue>{item.description}</PoDescriptionValue>
        <PoContent>
          <Layout style={poROverview}>
            <Row>
              <Col md={8} style={{ paddingRight: 23, borderRight: `1px solid ${disabledGrey}` }}>
                <PoOverviewRow>
                  <PooValue>SKU</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px', borderRight: `1px solid ${disabledGrey}` }}>
                  <PooValue>NET WEIGHT</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px' }}>
                  <PooValue>PALLETS</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
            </Row>
            <Row>
              <Col md={8} style={{ paddingRight: 23, borderRight: `1px solid ${disabledGrey}` }}>
                <PoOverviewRow>
                  <PooValue>VENDOR SKU</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px', borderRight: `1px solid ${disabledGrey}` }}>
                  <PooValue>TEMP</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8} />
            </Row>
            <Row>
              <Col md={8} style={{ paddingRight: 23, borderRight: `1px solid ${disabledGrey}` }}>
                <PoOverviewRow>
                  <PooValue>UNITS ORDERED</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px', borderRight: `1px solid ${disabledGrey}` }}>
                  <PooValue>STORAGE LOCATION</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px' }}>
                  <ThemeSelect style={{ width: '100%', height: 30 }}>
                    <Option value="">&nbsp;</Option>
                  </ThemeSelect>
                </PoOverviewRow>
              </Col>
            </Row>
            <Row>
              <Col md={8} style={{ paddingRight: 23, borderRight: `1px solid ${disabledGrey}` }}>
                <PoOverviewRow>
                  <PooValue>Units recieved</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8}>
                <PoOverviewRow style={{ padding: '0 23px', borderRight: `1px solid ${disabledGrey}` }}>
                  <PooValue># of pallets</PooValue>
                  <PooValue>...</PooValue>
                </PoOverviewRow>
              </Col>
              <Col md={8} />
            </Row>
          </Layout>
          <PoLabel>Notes</PoLabel>
          <ThemeTextArea rows={3} placeholder="Enter notes here..." />
        </PoContent>
      </Layout>
    )
  }
}

export default ReceivingRow
