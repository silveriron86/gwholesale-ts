import React from 'react'
import { Layout, Tabs, Icon } from 'antd'
import { SubTabs, tabSearch } from '../_style'
import TableView from './table-view'
import ReceivingRow from './receiving-row'
import SearchInput from '~/components/Table/search-input'

interface ReceivingTabProps {}

const { TabPane } = Tabs

const ReceivingTabData = [
  {
    description: 'Ut enim ad minim veniam, quis nostrud exercitation. Magna pars studiorum, prodita quaerimus.',
  },
  {
    description: 'Ut enim ad minim veniam, quis nostrud exercitation. Magna pars studiorum, prodita quaerimus.',
  },
]

export class ReceivingTab extends React.PureComponent<ReceivingTabProps> {
  state = {
    searchStr: '',
  }

  onSearch = (text: string) => {
    this.setState({
      searchStr: text,
    })
  }

  render() {
    const { searchStr } = this.state
    const filteredItems = ReceivingTabData.filter((row) => {
      return row.description.indexOf(searchStr) >= 0
    })

    return (
      <Layout style={{ padding: 10, background: 'transparent' }}>
        <SubTabs defaultActiveKey="1" type="line" animated={false}>
          <TabPane
            tab={
              <span>
                <Icon type="table" />
                TABLE VIEW
              </span>
            }
            key="1"
          >
            <TableView searchStr={searchStr} />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="unordered-list" />
                LIST VIEW
              </span>
            }
            key="2"
          >
            {filteredItems.map((item, index) => (
              <ReceivingRow key={`purchase-order-${index}`} item={item} />
            ))}
          </TabPane>
        </SubTabs>
        <div style={tabSearch}>
          <SearchInput handleSearch={this.onSearch} placeholder="Search" />
        </div>
      </Layout>
    )
  }
}

export default ReceivingTab
