import * as React from 'react'
import { connect } from "redux-epics-decorator";
import { DatePicker, Select, notification, Radio } from 'antd'
import moment from 'moment'
import { OrdersModule } from "~/modules/orders";
import { GlobalState } from "~/store/reducer";
import { Flex, ThemeButton, ThemeModal, ThemeIconButton, ThemeInput, ThemeSelect, ThemeIcon, ThemeSpin } from '~/modules/customers/customers.style'
import { RepackModalWrapper } from '../repack.styles'
import RepackInputTable from "./input-table"
import RepackOutputTable from "./output-table"
import RepackYieldTable from "./yield-table"
import { getAvailableToSell } from '~/common/utils';
import { OrderDetail } from '~/schema';
type EnterRepackModalProps = {
  repack: any
  currentOrder: OrderDetail
  sellerSetting: any
  selectedItem: any
  visible: boolean
  repacking: boolean
  isCreating: boolean
  selectedRepackItem?: any
  onClose: (status?: boolean)=> void
  getSimplifyItems: (type: string) => any
  saveRepack: (data: any) => void
  getOrderItemsById: (orderId: string) => any
  updateSavingData?: (data: any) => any
}

type EnterRepackModalStates = {
  input: any[],
  output: any[],
  yieldLossPaidType: number,
  repackDate: any,
  repackReferenceNo: string
  isOneTimeRepack: boolean
  isCreating: boolean
}

class EnterRepackModal extends React.PureComponent<EnterRepackModalProps, EnterRepackModalStates> {
  constructor(props: EnterRepackModalProps) {
    super(props)
    this.state = {
      isOneTimeRepack: true,
      input: [],
      output: [],
      yieldLossPaidType: 1,
      repackDate: moment(),
      repackReferenceNo: ""
    }
  }
  componentDidMount() {
    this.props.getSimplifyItems('SELL')
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps: EnterRepackModalProps) {
    if ( (this.props.visible != nextProps.visible && nextProps.visible) ||
         (nextProps.selectedItem && JSON.stringify(this.props.selectedItem) !== JSON.stringify(nextProps.selectedItem))) {
      this.fillData(nextProps)
    }

    if (this.props.repacking !== nextProps.repacking && nextProps.repacking === false) {
      if (nextProps.repack && nextProps.repack.data === false) {
        notification.error({
          message: 'ERROR',
          description: nextProps.repack.message,
          onClose: () => { },
        })
      } else {
        this.props.onClose(true);
        this.props.getOrderItemsById(nextProps?.currentOrder.wholesaleOrderId);
      }
    }
  }

  fillData = (props: EnterRepackModalProps) => {
    const { selectedItem, selectedRepackItem, isCreating } = props
    const { overrideUOM, inventoryUOM } = selectedItem
    if (isCreating) {
      this.setState({
        input: {
          ...selectedItem,
          uom: overrideUOM ? overrideUOM : inventoryUOM,
          quantity: 0,
          netWeight: 0,
          repackType: 0,
        },
        output: [],
        yieldLossPaidType: 1,
        repackDate: moment(),
        repackReferenceNo: ""
      })
    } else {
      const inputRepack = selectedRepackItem?.repackDetails.find((el: any)=> el.repackType === 0)
      const outRepack = selectedRepackItem?.repackDetails.filter((el: any)=> el.repackType === 1)
     this.setState({
        input: {
          ...selectedItem,
          id: inputRepack.id,
          itemId: inputRepack?.item?.wholesaleItemId,
          variety: inputRepack?.item?.variety,
          uom: overrideUOM ? overrideUOM : inventoryUOM,
          quantity: inputRepack?.quantity ?? 0,
          netWeight: inputRepack?.netWeight ?? 0,
          repackType: 0,
        },
        output: outRepack?.map((el: any)=>{           
            return {
              ...el.wholesaleOrderItem,
              id: el.id,
              itemId: el.item?.wholesaleItemId,
              variety: el?.item?.variety,
              quantity: el.quantity, 
              netWeight: el.netWeight,
              repackType: 1
            }
          }) ?? [],
        yieldLossPaidType: selectedRepackItem?.yieldLossPaidType,
        repackDate: moment(selectedRepackItem?.repackDate),
        repackReferenceNo: selectedRepackItem?.repackReferenceNo,
        isOneTimeRepack: selectedRepackItem?.isOneTimeRepack,
      })
    }
  }

  onUpdateInput = (input: any) => {
    this.setState({ input })
  }

  onUpdateOutput = (output: any[]) => {
    this.setState({ output })
  }

  onChangeState = (value: any, type: string) => {
    let state = {...this.state}
    state[type] = value
    this.setState(state)
  }

  onSave = () => {
    const { input, output, yieldLossPaidType, repackDate, repackReferenceNo, isOneTimeRepack } = this.state
    const { selectedItem, isCreating, selectedRepackItem } = this.props
    const { wholesaleOrderItemId, status, quantity, qtyConfirmed, receivedQty } = selectedItem
    const statusQty = status === 'PLACED' ? quantity : status === 'CONFIRMED' ? qtyConfirmed : receivedQty
    if (input.quantity > statusQty) {
      const statusString = status === 'PLACED' ? 'ordered' : status.toLowerCase();
      notification.error({
        message: 'ERROR',
        description: `Please enter an input quantity that is equal to or less than the ${statusString} quantity`,
      })
      return;
    }

    const data = {
      orderItemId: wholesaleOrderItemId,
      repackItemId: selectedRepackItem?.id,
      data: {
        repackReferenceNo,
        repackDate: moment(repackDate).format('MM/DD/YYYY'),
        yieldLossPaidType,
        isOneTimeRepack,
        dataList: [...input, ...output],
        isCreating: isCreating,
        // repackItemId: selectedRepackItem?.id,
      }
    }
    if(isCreating) {
      this.props.saveRepack(data)
    } else if(this.props.updateSavingData) {
      this.props.updateSavingData(data)
    }
    
    this.props.onClose()    
  }

  render() {
    const { visible, onClose, sellerSetting, selectedItem, repacking, isCreating } = this.props
    const { input, output, yieldLossPaidType, repackDate, repackReferenceNo, isOneTimeRepack } = this.state
    const logisticUnit = sellerSetting ? sellerSetting.company.logisticUnit : "IMPERIAL"
    const unit = logisticUnit === "IMPERIAL" ? 'lb' : 'kg'

    const availableToSell = selectedItem ? getAvailableToSell(selectedItem) : 0
    const disabled = availableToSell < parseFloat(input.quantity)

    return (
      <ThemeModal
        title={`Enter repack`}
        visible={visible}
        onCancel={onClose}
        okText={'Save'}
        className='dark'
        footer={[
          <Flex>
            <ThemeButton className={`dark ${repacking || disabled && output.length === 0 ? 'disabled' : ''}`} type="primary" onClick={this.onSave} disabled={repacking}>
              {isCreating ? 'Enter ': 'Edit '} repack
            </ThemeButton>
            <ThemeIconButton className="no-border dark" onClick={onClose} style={{marginLeft: 24}}>Cancel</ThemeIconButton>
          </Flex>,
        ]}
        width={600}
      >
        <RepackModalWrapper>
          <ThemeSpin spinning={repacking}>
            <Flex>
              <div>
                <label>Repack reference no.</label>
                <ThemeInput style={{ width: 260 }} value={repackReferenceNo} onChange={(e: any) => this.onChangeState(e.target.value, 'repackReferenceNo')} />
              </div>
              <div style={{marginLeft: 20}}>
                <label>Repack date</label>
                <DatePicker
                  value={repackDate}
                  placeholder={'MM/DD/YYYY'}
                  format={'MM/DD/YYYY'}
                  suffixIcon={<ThemeIcon type="calendar" />}
                  style={{ width: 160, display: 'block' }}
                  allowClear={false}
                  onChange={(v: any) => this.onChangeState(v, 'repackDate')}
                />
              </div>
            </Flex>
            <div style={{marginTop: 15}}>
              <Radio.Group
                value={isOneTimeRepack}
                disabled={!isCreating}
                onChange={(e) => {
                  this.setState({ isOneTimeRepack: e.target.value })
                }}
              >
                <Radio value={true}>One-Time Repack</Radio>
                <Radio value={false}>Continuous Repack</Radio>
              </Radio.Group>
            </div>
            <div style={{marginTop: 15}}>
              <label>Yield loss paid by</label>
              <ThemeSelect style={{ width: 260 }} value={yieldLossPaidType} onChange={(v: any) => this.onChangeState(v, 'yieldLossPaidType')}>
                <Select.Option value={0}>Vendor (shipper)</Select.Option>
                <Select.Option value={1}>Wholesaler (broker)</Select.Option>
              </ThemeSelect>
            </div>
            <RepackInputTable dataSource={[input]} logisticUnit={unit} onUpdate={this.onUpdateInput} />
            <RepackOutputTable {...this.props} dataSource={output} logisticUnit={unit} onUpdate={this.onUpdateOutput} isOneTimeRepack={isOneTimeRepack} isCreating={isCreating}/>
            <RepackYieldTable {...this.props} input={[input]} output={output} logisticUnit={unit} />
            
          </ThemeSpin>
        </RepackModalWrapper>
      </ThemeModal>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export default connect(OrdersModule)(mapStateToProps)(EnterRepackModal)
