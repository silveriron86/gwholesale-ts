import * as React from "react";
import { Table, Tooltip } from 'antd';
import {Flex, ThemeInput} from "~/modules/customers/customers.style";
import { cloneDeep } from "lodash";
import { getAvailableToSell } from "~/common/utils";

interface RepackInputTableprops {
  dataSource: any
  logisticUnit: string
  onUpdate: (data: any) => void
}

export default class RepackInputTable extends React.PureComponent<RepackInputTableprops> {
  onUpdate = (e: any, type: string) => {
    const { value } = e.target
    let dataSource = cloneDeep(this.props.dataSource[0])
    dataSource[type] = value
    this.props.onUpdate(dataSource)
  }

  calcQuantity = (e: any) => {
    const { value } = e.target
    let dataSource = cloneDeep(this.props.dataSource[0])
    dataSource['quantity'] = value

    const { grossWeight } = this.props.dataSource[0]

    if (grossWeight) {
      const quantity = parseFloat(value)
      if (!isNaN(quantity) && quantity !== null) {
        dataSource['netWeight'] = quantity * grossWeight;
      }
    }

    this.props.onUpdate(dataSource)
  }

  formatValues = (type: string) => {
    let dataSource = cloneDeep(this.props.dataSource[0])
    if (dataSource[type] !== '') {
      dataSource[type] = parseFloat(dataSource[type])
      this.props.onUpdate(dataSource)
    }
  }

  render() {
    const { dataSource, logisticUnit } = this.props
    console.log('input data source', dataSource)
    const columns = [
      {
        title: 'Product',
        dataIndex: 'variety',
        render: (variety: string, record: any) => {
          if (variety) {
            return variety
          }
        }
      },
      {
        title: 'Input qty',
        dataIndex: 'quantity',
        width: 166,
        render: (quantity: number, record: any) => {
          const availableToSell = getAvailableToSell(record)
          const warning = availableToSell < quantity
          const textInput = <ThemeInput className={warning ? "warning" : ""} style={{ width: 60, marginRight: 5}} value={quantity} onChange={this.calcQuantity} />
          return (
            <Flex className="v-center">
              {warning ? (
                <Tooltip title="User cannot enter Input qty greater than the current available to sell quantity for the input item lot">{textInput}</Tooltip>
              ) : textInput}
              {record.uom}
            </Flex>
          )
        }
      },
      {
        title: 'Net weight in',
        dataIndex: 'netWeight',
        width: 150,
        render: (weight: number, record: any) => {
          return (
            <Flex className="v-center">
              <ThemeInput style={{ width: 80, marginRight: 5}} value={weight} onChange={(e: any) => this.onUpdate(e, 'netWeight')} />
              {logisticUnit}
            </Flex>
          )
        }
      },
    ];

    return (
      <>
        <div className="table-header">{'Input'}</div>
        <Table dataSource={dataSource} columns={columns} pagination={false}/>
      </>
    )
  }
}
