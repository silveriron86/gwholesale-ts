import * as React from "react";
import { Table, Icon } from 'antd';
import { AddAutocomplete } from "~/modules/customers/nav-sales/order-detail/tabs/add-autocomplete";
import {ThemeInput, Flex} from "~/modules/customers/customers.style";
import {cloneDeep} from "lodash";
import {Icon as IconSVG} from "~/components";
import UomSelect from "~/modules/customers/nav-sales/order-detail/components/uom-select";

interface RepackOutputTableprops {
  logisticUnit: string
  dataSource: any[]
  onUpdate: Function
  getItemLot: Function
  itemLots: any[]
  getItemLotLoading: boolean
  isOneTimeRepack: boolean
  isCreating: boolean
}

export default class RepackOutputTable extends React.PureComponent<RepackOutputTableprops> {
  componentWillReceiveProps(nextProps: RepackOutputTableprops) {
    if (this.props.getItemLotLoading !== nextProps.getItemLotLoading && nextProps.getItemLotLoading === false) {
      let defaultLot = null;
      if (nextProps.itemLots.length > 0) {
        const available = nextProps.itemLots.find((v: any) => {
          const disabled = v.lotAvailableQty > 0 && v.lotAvailableQty < nextProps.dataSource[0].quantity
          return !disabled;
        })

        if (available) {
          defaultLot = available.lotId
        }
      }
      this.onUpdate(defaultLot, 'lot', 0)
    }
  }

  onAddItem = (record: any) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource.push({
      ...record,
      netWeight: 0,
      uom: record.overrideUOM ? record.overrideUOM : record.inventoryUOM,
      repackType: 1,
    })
    this.props.getItemLot(record.itemId);
    this.props.onUpdate(dataSource)
  }

  onUpdate = (value: any, type: string, index: number) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource[index][type] = value
    this.props.onUpdate(dataSource)
  }

  calcQuantity = (value: any, index: number) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource[index]['quantity'] = value

    const { defaultGrossWeight } = this.props.dataSource[index]
    if (defaultGrossWeight) {
      const quantity = parseFloat(value)
      if (!isNaN(quantity) && quantity !== null) {
        dataSource[index]['netWeight'] = quantity * defaultGrossWeight;
      }
    }

    this.props.onUpdate(dataSource)
  }

  deleteRow = (index: number) => {
    let dataSource = cloneDeep(this.props.dataSource)
    dataSource.splice(index, 1)
    this.props.onUpdate(dataSource)
  }

  onChangeUom = (orderItemId: string, value: any, index: number) => {
    this.onUpdate(value, 'uom', index)
  }

  render() {
    const { dataSource, logisticUnit, selectedItem, isOneTimeRepack, isCreating } = this.props
    const columns = [
      {
        title: 'Product(s)',
        dataIndex: 'variety',
        render: (variety: string, record: any) => {
          if (variety) {
            return variety
          }
          
          return (
            <AddAutocomplete {...this.props} onAddToRepack={this.onAddItem}/>
          )
        }
      },
      {
        title: 'Output qty',
        dataIndex: 'quantity',
        width: 166,
        render: (quantity: number, record: any, index: number) => {
          if (!record.variety) {
            return ''
          }
          if (isOneTimeRepack || (!isCreating && record.variety)) {
            if (record.variety === 'Total') {
              return `${quantity} UNITS`
            }
            return (
              <Flex className="v-center">
                <ThemeInput style={{ width: 60}} value={quantity} onChange={(e: any) => this.calcQuantity(e.target.value, index)} />
                <div style={{ minWidth: 60, position: 'absolute', marginLeft: 65}}>
                  <UomSelect
                    orderItem={{...selectedItem, UOM: record.uom}}
                    type={2}
                    handlerChangeUom={(orderItemId: string, value: any) => this.onChangeUom(orderItemId, value, index)}
                  />
                </div>
              </Flex>
            )
          } else {
              return ''
          }
        }          
      },
      {
        title: 'Net weight out',
        dataIndex: 'netWeight',
        width: 150,
        render: (netWeight: number, record: any, index: number) => {
          if (!record.variety) {
            return ''
          }
         
          if (isOneTimeRepack || (!isCreating && record.variety)) {
            if (record.variety === 'Total') {
              return `${netWeight} ${logisticUnit}`
            }
            return (
              <Flex className="v-center">
                <ThemeInput style={{ width: 80, marginRight: 5}} value={netWeight} onChange={(e: any) => this.onUpdate(e.target.value, 'netWeight', index)}/> {logisticUnit}
                <div className="trash" onClick={() => this.deleteRow(index)}>
                  <IconSVG
                    type="trash"
                    viewBox="0 0 24 24"
                    width="16"
                    height="16"
                    style={{ margin: '0 4px 2px -2px' }}
                  />
                </div>
              </Flex>
            )
          } else {
            return (
              <div className="icon" style={{textAlign: 'right'}} onClick={() => this.deleteRow(index)}>
                <IconSVG
                  type="trash"
                  viewBox="0 0 24 24"
                  width="16"
                  height="16"
                />
              </div>
            )
          }
        }
      },
    ];

    const data = [
      ...dataSource,
      {
        variety: '',
      }
    ]

    if ((isOneTimeRepack || !isCreating) && dataSource.length > 0) {
      let quantity = 0
      let netWeight = 0
      dataSource.forEach((row: any) => {
        quantity += row.quantity ? parseFloat(row.quantity) : 0
        netWeight += row.netWeight ? parseFloat(row.netWeight) : 0
      })
      data.push({
        variety: 'Total',
        quantity,
        netWeight
      })
    }

    return (
      <>
        <div className="table-header">{'Output'}</div>
        <Table dataSource={data} columns={columns} pagination={false}/>
      </>
    )
  }
}
