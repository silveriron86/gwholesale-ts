import * as React from 'react'
import { Col, Button } from 'antd'
import { Flex, ThemeButton, ThemeModal, ThemeIconButton, ThemeSpin, ThemeInput } from '~/modules/customers/customers.style'
import { RepackModalWrapper, RepackQuantityModalRow } from '../repack.styles'
import { Item, noPaddingFooter, ValueLabel } from '~/modules/customers/nav-sales/styles';
import ReceivingPalletize from '../../../receiving/modals/pallets';
import { ClassNames } from '@emotion/core';
import { baseQtyToRatioQty, generealPadString, ratioQtyToBaseQty, ratioQtyToInventoryQty } from '~/common/utils';
import { NewLabelModal } from '../../../_style';
import NewPrintPalletLabel from '../../../receiving/newPrintLabel/NewPrintPalletLabel';
import NewPrintWeightLabel from '../../../receiving/newPrintLabel/NewPrintWeightLabel';
import { OrderService } from '~/modules/orders/order.service';
type RepackQuantityModalProps = {
  visible: boolean
  savingQuantities: boolean
  selectedRepackDetail: any
  selectedRepackItem: any
  onClose: (status?: boolean)=> void
  getPallets: (orderItemId: number)=> any
  getWeightSheets: (palletId: number) => any
  updatePalletDetail: (payload: any) => any
  updateAllPalletDetail: (payload: any[]) => any
  handlerOrderItemsRedux: (paylaod: any) => any
  updateOrderItemWithoutRefresh: (paylaod: any) => any
  updateQuantites: (data: any) => void
  sellerSetting: any
  pallets: any[]
  weightSheet: any
  vendors: any[]
}

type RepackQuantityModalStates = {
  visiblePalletizeModal: boolean
  visibleWeightModal: boolean
  numberOfPallets: number
  unitOfPallets: number
  unitWeight: number
  palletDataSource: any[]
  unitItems: any[]
}

class RepackQuantityModal extends React.PureComponent<RepackQuantityModalProps, RepackQuantityModalStates> {
  constructor(props: RepackQuantityModalProps) {
    super(props)
    this.state = {
      visiblePalletizeModal: false,
      visibleWeightModal: false,
      numberOfPallets: 0,
      unitOfPallets: 0,
      unitWeight: 0,
      palletDataSource: [],
      unitItems: []
    }
  }

  componentWillReceiveProps(nextProps: RepackQuantityModalProps) {
    const _this = this
    const wholesaleOrderItem = nextProps.selectedRepackDetail?.wholesaleOrderItem
    if(wholesaleOrderItem && nextProps.visible && !this.props.visible) {
      OrderService.instance.getPallets(wholesaleOrderItem.wholesaleOrderItemId).subscribe({
        next(res: any) {
          if(res.body.data?.length) {
            const pallets = res.body.data
            const palletId = pallets[0].id
            const unitOfPallets = pallets[0].palletUnits
            OrderService.instance.getWeightSheets(palletId).subscribe({
              next(weightRes: any) {
                if(weightRes.body.data) {
                  const weights = weightRes.body.data
                  let unitWeight = 0
                  if(weights.unitsInfo) {
                    const unitInfo = JSON.parse(weights.unitsInfo)
                    const keys = Object.keys(unitInfo ?? {})
                    if(keys.length) {
                      unitWeight = +unitInfo[keys[0]]
                    }
                  }
                  
                  _this.setState({
                    numberOfPallets: wholesaleOrderItem.palletQty,
                    unitOfPallets,
                    unitWeight
                  })
                }
              }
            })
          }
        }
      })
    }
  }

  handleSaveQuantities = (value: number) => {
    const wholesaleOrderItem = this.props.selectedRepackDetail?.wholesaleOrderItem
    const { selectedRepackItem } = this.props
    const { numberOfPallets, unitOfPallets, unitWeight } = this.state
    this.props.updateQuantites({
      wholesaleOrderItemId: wholesaleOrderItem?.wholesaleOrderItemId,
      repackItemId: selectedRepackItem?.id,
      numberOfPallets,
      unitOfPallets,
      unitWeight
    })
  }

  openModal = (type: string) => {
    const stateObj = {...this.state}
    stateObj[type] = true
    this.setState({...stateObj})
  }

  onCloseNumberOfPalletModal = () => {
    this.setState({
      visiblePalletizeModal: false,
      visibleWeightModal: false
    })
  }

  onChangeValues = (type: string, value: number) => {
    if(!value || isNaN(value)) return
    const stateObj = {...this.state}
    stateObj[type] = value
    this.setState({...stateObj})
  }

  render() {
    const { visible, savingQuantities, selectedRepackDetail, sellerSetting, onClose } = this.props
    const { 
      visiblePalletizeModal, 
      visibleWeightModal, 
      palletDataSource, 
      unitItems, 
      unitOfPallets, 
      numberOfPallets, 
      unitWeight } = this.state
    return (
      <>
        <ThemeModal
          title={`Enter repack`}
          visible={visible}
          onCancel={(e) => onClose()}
          okText={'Save'}
          className='dark'
          footer={[
            <Flex>
              <ThemeButton type="primary" onClick={this.handleSaveQuantities}>
                Save Update
              </ThemeButton>
              <ThemeIconButton className="no-border dark" onClick={onClose} style={{marginLeft: 24}}>Cancel</ThemeIconButton>
            </Flex>,
          ]}
          width={600}
        >
          <RepackModalWrapper>
            <ThemeSpin spinning={savingQuantities}>
              <RepackQuantityModalRow>
                <Item>
                  <ValueLabel className="bold">Lemon</ValueLabel>
                </Item>
              </RepackQuantityModalRow>
              <RepackQuantityModalRow>
                <div>Number of pallets</div>
                <Col span={12}>
                  <ThemeInput value={numberOfPallets} onChange={(e: any)=>this.onChangeValues('numberOfPallets', e.target.value)}/>
                </Col>
                <Col span={5} style={{marginLeft: 12}}></Col>
                <Col span={6}>
                  <Button style={{width: 150}} onClick={()=>this.openModal('visiblePalletizeModal')}>Print pallet label</Button>
                </Col>
              </RepackQuantityModalRow>
              <RepackQuantityModalRow>
                <div>Units of pallets</div>
                <Col span={12}>
                  <ThemeInput value={unitOfPallets} onChange={(e: any)=>this.onChangeValues('unitOfPallets', e.target.value)}/> 
                </Col>
                  <Col span={5} style={{marginLeft: 12}}>
                    <ValueLabel className="medium">{selectedRepackDetail?.item?.inventoryUOM}</ValueLabel>
                  </Col>
                <Col span={6}>
                  <Button style={{width: 150}} onClick={()=>this.openModal('visibleWeightModal')}>Print unit label</Button>
                </Col>
              </RepackQuantityModalRow>
              <RepackQuantityModalRow>
                <div>Unit weight</div>
                <Col span={12}>
                  <ThemeInput value={unitWeight} onChange={(e: any)=>this.onChangeValues('unitWeight', e.target.value)}/>
                </Col>      
                <Col span={5} style={{marginLeft: 12}}>
                  <ValueLabel className="medium">{sellerSetting?.company?.logisticUnit === 'IMPERIAL' ? 'lbs': 'KG'}</ValueLabel>
                </Col>        
              </RepackQuantityModalRow>
            </ThemeSpin>
          </RepackModalWrapper>
        </ThemeModal>
        {this.state.visiblePalletizeModal &&
        <NewLabelModal
          style={{ width: '703px', height: '930px', padding: '0px' }}
          width={703}
          footer={null}
          visible={visiblePalletizeModal}
          onCancel={this.onCloseNumberOfPalletModal}
        >
          <NewPrintPalletLabel 
            selectedUnit={'Pallet 6X4'}
            dataSource={palletDataSource} 
            sellerSetting={sellerSetting} 
            props={{...this.props, palletizeItem: selectedRepackDetail?.wholesaleOrderItem}}/>
        </NewLabelModal>         
        }
       {visibleWeightModal &&
        <NewLabelModal
          style={{ width: '703px', height: '930px', padding: '0px' }}
          width={703}
          footer={null}
          visible={visibleWeightModal}
          onCancel={this.onCloseNumberOfPalletModal}
        >
           <NewPrintWeightLabel
             items={unitItems}
             palletId={undefined}
             firstPallet={palletDataSource[0]}
             dataSource={palletDataSource}
             sellerSetting={sellerSetting}
             props={{...this.props, weightItem: selectedRepackDetail?.wholesaleOrderItem}}
           />
        </NewLabelModal>
       }
        
      </>
    )
  }
}

export default RepackQuantityModal
