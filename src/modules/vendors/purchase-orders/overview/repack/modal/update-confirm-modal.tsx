import React, { useState } from 'react'
import { Flex, ThemeButton, ThemeCheckbox, ThemeModal, ThemeTable } from '~/modules/customers/customers.style'
import { RepackConfirmModalWrapper } from '../repack.styles'
interface IProps {
  visible: boolean
  dataSource: any[]
  updateSavingDataList: any
  onClose: () => void
  onSave: () => void
}
export const ConfirmRepackUpdateModal = (props: IProps) => {
  const { visible, dataSource, updateSavingDataList, onSave, onClose } = props
  const [confirmChecked, setConfirmChecked] = useState(false)
  const repackItems = Object.values(updateSavingDataList?.repackData ?? {})
  const updatePalletData = Object.values(updateSavingDataList?.pallets ?? {})
  let updateQtyDetails: any[] = repackItems.map((item: any)=> {
    return item.data?.dataList?.filter((el: any)=>el.repackType === 1).map((el: any)=> ({
      wholesaleOrderItemId: el.wholesaleOrderItemId,
      quantity: +el.quantity,
    }))
  })
  const orderItemIdList = updateQtyDetails.map(el=>el.wholesaleOrderItemId)
  updatePalletData.forEach((el: any)=>{
    if(orderItemIdList.indexOf(el.wholesaleOrderItemId) == -1) {
      const qty = el.numberOfPallets * el.unitOfPallets
      updateQtyDetails.push({wholesaleOrderItemId: el.wholesaleOrderItemId, quantity: qty})
    }
  })
  const columns: any[] = [
    {
      title: 'Lot #',
      dataIndex: 'lotId',
      align: 'cener'
    },
    {
      title: 'Item',
      dataIndex: 'variety',
      align: 'cener',
      render: (variety: string, record: any) => {
        return record.wholesaleItem?.variety
      }
    },
    {
      title: 'Untis Received (Original/New)',
      dataIndex: 'receivedQty',
      align: 'cener',
      render: (receivedQty: number, record: any) => {
        const qtyInfo = updateQtyDetails?.find((el: any)=>el.wholesaleOrderItemId=record.wholesaleOrderItemId)
        return `${receivedQty}/${receivedQty + qtyInfo?.quantity}`
      }
    },
    {
      title: 'Available to sell (Original/New)',
      dataIndex: 'lotAvailableQty',
      align: 'cener',
      render: (lotAvailableQty: number, record: any) => {
        const qtyInfo = updateQtyDetails?.find((el: any)=>el.wholesaleOrderItemId=record.wholesaleOrderItemId)
        return `${lotAvailableQty}/${lotAvailableQty + qtyInfo?.quantity}`
      }
    },
    {
      title: 'Units on hand (Original/New)',
      dataIndex: 'onHandQty',
      align: 'cener',
      render: (onHandQty: number, record: any) => {
        const qtyInfo = updateQtyDetails?.find((el: any)=>el.wholesaleOrderItemId=record.wholesaleOrderItemId)
        return `${onHandQty}/${onHandQty + qtyInfo?.quantity}`
      }
    },
  ]
  return (
    <ThemeModal
        title={`Update Units Received`}
        visible={visible}
        onCancel={onClose}
        okText={'Save'}
        className='dark'
        footer={[
          <Flex>
            <ThemeButton type="primary" onClick={onSave} disabled={!confirmChecked}>
              Save and Update inventory
            </ThemeButton>
            <ThemeButton onClick={onClose} style={{marginLeft: 24}}>Cancel</ThemeButton>
          </Flex>,
        ]}
       width={1200}
      >
        <RepackConfirmModalWrapper>
          <div className='description'>Units from this lot have already been added to sales orders. Please confirm you want to modify the units received.</div>
          <ThemeTable
            pagination={false}
            columns={columns}
            dataSource={dataSource}
          />
          <div className='description'>Pallets and unit weights may have already been generated for this lot, please make the necessary modifications after submitting this change.</div>
          <ThemeCheckbox checked={confirmChecked} onChange={(e: any)=>setConfirmChecked(e.target.checked)}>I understand</ThemeCheckbox>
        </RepackConfirmModalWrapper>
        
      </ThemeModal>
  )
}
