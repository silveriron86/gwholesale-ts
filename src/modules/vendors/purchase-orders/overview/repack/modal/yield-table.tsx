import * as React from "react";
import { Table } from 'antd';

interface RepackYieldTableprops {
  logisticUnit: string
  input: any[]
  output: any[]
}

export default class RepackYieldTable extends React.PureComponent<RepackYieldTableprops> {
  getValue = (v: any) => {
    return v ? parseFloat(v) : 0
  }

  render() {
    const { logisticUnit, input, output } = this.props

    let quantity = 0
    let netWeight = 0

    output.forEach((row: any) => {
      quantity += this.getValue(row.quantity)
      netWeight += this.getValue(row.netWeight)
    })
    input.forEach((row: any) => {
      quantity -= this.getValue(row.quantity)
      netWeight -= this.getValue(row.netWeight)
    })

    console.log(input, output, quantity)

    const dataSource = [{
      key: '1',
      name: 'Yield loss',
      quantity,
      netWeight,
    }]

    const columns = [
      {
        title: '',
        dataIndex: 'name',
      },
      {
        title: 'Units',
        dataIndex: 'quantity',
        width: 166,
        render: (quantity: number, record: any) => {
          return `${quantity} UNITS`
        }
      },
      {
        title: 'Weight',
        dataIndex: 'netWeight',
        width: 150,
        render: (netWeight: number, record: any) => {
          return `${netWeight} ${logisticUnit}`
        }
      },
    ];

    return (
      <>
        <div className="table-header">Yield</div>
        <Table dataSource={dataSource} columns={columns} pagination={false}/>
      </>
    )
  }
}
