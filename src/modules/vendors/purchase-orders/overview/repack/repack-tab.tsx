import * as React from 'react'
import { Table, Icon, Tooltip } from 'antd'
import { Theme } from '~/common'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Icon as IconSvg } from '~/components/icon/'
import { formatNumber, getOrderPrefix, mathRoundFun, numberWithCommas, responseHandler } from '~/common/utils'
import { RepackTabWrapper, RepackModalWrapper, RepackDownloadButton } from './repack.styles'
import { Flex, ThemeButton, ThemeOutlineButton } from '~/modules/customers/customers.style'
import moment from 'moment'
import _ from 'lodash'
import { OrderItem } from '~/schema'
import RepackQuantityModal from './modal/quantity-modal'
import EnterRepackModal from './modal/enter-repack-modal'
import { OrderService } from '~/modules/orders/order.service'
import { ConfirmRepackUpdateModal } from './modal/update-confirm-modal'

type RepackProps = OrdersDispatchProps &
  OrdersStateProps &
  {
    theme: Theme,
  }

export default class RepackTab extends React.PureComponent<RepackProps> {
  state = {
    dataSource: [],
    visibleQuantityModal: false,
    repackModalVisible: false,
    selectedRepackItem: null,
    selectedRepackInputOrderItem: null,
    selectedRepackDetail: null,
    updateSavingDataList: null,
    visibleConfirmUpdateModal: false,
    udpatedQuantities: null
  }

  componentDidMount() {
    if(this.props.repack) {
      this.initializeDataSource(this.props.repack)
    }
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.repack && JSON.stringify(nextProps.repack) != JSON.stringify(this.props.repack)) {
      this.initializeDataSource(nextProps.repack)
    }
  }

  initializeDataSource = (data: any) => {
    let dataSource: any[] = []
    this.setState({dataSource: data.repackItems})
  }

  onChangeQuanty = (selectedRepackDetail: any, selectedRepackItem: any) => {
    this.setState({
      visibleQuantityModal: true,
      selectedRepackDetail,
      selectedRepackItem
    })
  }

  onCloseRepackQuantityModal = (status: boolean = false) => {
    this.setState({
      visibleQuantityModal: status,
      selectedRepackDetail: null
    })
  }

  renderTable = (repackItem: any) => {
    const {inputRepack, outRepack, totalRepack} = this.getRepackConentData(repackItem)
    
    const inputColumns = [
      {
        title: 'Input product',
        dataIndex: 'variety',
      },
      {
        title: 'Input qty',
        dataIndex: 'quantity',
        width: 180,
        render: (qty: number, record: any) => {
          return `${qty} ${record.uom}`
        }
      },
      {
        title: 'Cost per unit',
        dataIndex: 'cost',
        width: 180,
        render: (cost: number, record: any) => {
          return `$${formatNumber(cost, 2)}/${record.uom}`
        }
      },
      {
        title: 'Total input cost',
        dataIndex: 'quantity',
        key: 'total',
        width: 180,
        render: (quantity: number, record: any) => {
          const cost = formatNumber(Math.abs(quantity) * record.cost, 2)
          return `${quantity < 0 ? '-' : ''}$${numberWithCommas(cost)}`
        }
      },
      {
        title: 'Net weight in',
        dataIndex: 'netWeight',
        width: 180,
        render: (weight: number, record: any) => {
          return `${numberWithCommas(weight?.toString())} lbs`
        }
      },
    ];

    const outputColumns = [
      {
        title: 'Output product',
        dataIndex: 'variety',
      },
      {
        title: 'Output qty',
        dataIndex: 'quantity',
        width: 180,
        render: (qty: any, record: any) => {
          if (repackItem.isOneTimeRepack || record.isTotalRow) {
            return record.isTotalRow ? qty : `${qty} ${record.uom}`
          } 
          
          // const dataList = editRepackQtys?.data?.dataList ?? []
          // const orderItem = dataList.find(el=>el.wholesaleOrderItemId === record.wholesaleOrderItem.wholesaleOrderItemId)
          // if(editRepackQtys && orderItem) {
          //   qty = orderItem.quantity ?? 0
          // } else if(qtys && qtys?.wholesaleOrderItemId === record.wholesaleOrderItem.wholesaleOrderItemId) {
          //   qty = qtys.numberOfPallets * qtys.unitOfPallets
          // }
          
          return (
            <div>
              <span>{record.isTotalRow ? qty : `${qty} ${record.uom}`}</span>
              <span className="icon">
                <Icon 
                  onClick={() => this.onChangeQuanty(record, repackItem)}
                  type="plus-circle"
                  theme="filled"
                  viewBox="0 0 20 20"
                  style={{marginLeft: 12}}
                />
              </span>
            </div>
          )
        }
      },
      {
        title: (
          <Flex className='v-center'>
            <span>Est. cost per unit</span>
            <Tooltip title="Estimated cost based on inputs. You can change the actual cost in the PO.">
              <Icon type="info-circle" className="info-icon" style={{ marginLeft: 5, marginTop: -1 }} />
            </Tooltip>
          </Flex>
        ),
        dataIndex: 'cost',
        width: 180,
        render: (cost: number, record: any) => {
          return record.isTotalRow ? '' : `$${formatNumber(cost, 2)}/${record.uom}`
        }
      },
      {
        title: 'Total output cost',
        dataIndex: 'quantity',
        key: 'total',
        width: 180,
        render: (quantity: number, record: any) => {
          let qty = quantity
          
          // const dataList = editRepackQtys?.data?.dataList ?? []
          // const orderItem = dataList.find(el=>el.wholesaleOrderItemId === record.wholesaleOrderItem.wholesaleOrderItemId)
          // if(editRepackQtys && orderItem) {
          //   qty = orderItem.quantity ?? 0
          // } else if(qtys && qtys?.wholesaleOrderItemId === record.wholesaleOrderItem.wholesaleOrderItemId) {
          //   qty = qtys.numberOfPallets * qtys.unitOfPallets
          // }
          
          const cost = formatNumber(Math.abs(qty) * record.cost, 2)
          return record.isTotalRow ? record.totalCost : `${qty < 0 ? '-' : ''}$${numberWithCommas(cost)}`
        }
      },
      {
        title: 'Net weight out',
        dataIndex: 'netWeight',
        width: 180,
        render: (weight: number, record: any) => {
          // const dataList = editRepackQtys?.data?.dataList ?? []
          // const orderItem = dataList.find(el=>el.wholesaleOrderItemId === record.wholesaleOrderItem.wholesaleOrderItemId)
          // if(editRepackQtys && orderItem) {
          //   weight = orderItem.netWeight ?? 0
          // } else if(qtys && qtys?.wholesaleOrderItemId === record.wholesaleOrderItem.wholesaleOrderItemId) {
          //   weight = qtys.numberOfPallets * qtys.unitOfPallets * qtys.unitWeight
          // }
          return `${numberWithCommas(weight?.toString())} lbs`
        }
      },
    ];

    const totalColumns = [
      {
        title: 'Yield',
        key: 'name',
        render: () => 'Yield gain/loss'
      },
      {
        title: 'By units',
        dataIndex: 'quantity',
        width: 180,
        render: (quantity: number, record: any) => {
          return (
            <>
              <span>{quantity} unit</span>
              <div className="percent">{mathRoundFun(quantity / inputRepack.quantity * 100, 2)}%</div>
            </>
          )
        }
      },
      {
        title: 'Cost per unit',
        dataIndex: 'cost',
        width: 180,
        render: (cost: number) => {
          return (
            <>
              <span>${`${formatNumber(cost, 2)}/unit`}</span>
              <div className="percent"></div>
            </>
          )
        }
      },
      {
        title: 'Total cost',
        dataIndex: 'yieldLossQty',
        key: 'total',
        width: 180,
        render: (yieldLossQty: number, record: any) => {
          const lossQty = formatNumber(Math.abs(yieldLossQty), 2)
          return (
            <>
              <span>{yieldLossQty < 0 ? '-' : ''}${numberWithCommas(lossQty)}</span>
              <div className="percent"></div>
            </>
          )
        }
      },
      {
        title: 'By weight',
        dataIndex: 'netWeight',
        width: 180,
        render: (weight: number, record: any) => {
          return (
            <>
              <span>{numberWithCommas(weight?.toString())} lbs</span>
              <div className="percent">{weight == 0 || inputRepack.netWeight == 0 ? 0 : mathRoundFun(Math.abs(weight) / inputRepack.netWeight * 100, 2)}%</div>
            </>
          )
        }
      },
    ];

    let outRepackData = outRepack
    if (outRepack.length > 1) {
      const totalCost = _.sumBy(outRepack, (o: any) => {
        return o.quantity * o.cost
      })
      const totalRow = {
        isTotalRow: true,
        variety: 'Total',
        quantity: _.sumBy(outRepack, 'quantity') + ' UNITS',
        totalCost: `${totalCost < 0 ? '-' : ''}$${formatNumber(totalCost, 2)}`,
        netWeight: _.sumBy(outRepack, 'netWeight')
      }
      outRepackData = [...outRepackData, totalRow]
    }
    return (
      <>
        <Table dataSource={[inputRepack]} columns={inputColumns} pagination={false}/>
        <Table className={outRepack.length > 1 ? 'with-total-row' : ''} dataSource={outRepackData} columns={outputColumns} pagination={false} style={{marginTop: 20}} />

        <div className="total-table">
          <Table dataSource={[totalRepack]} columns={totalColumns} pagination={false} />
        </div>
      </>
    )
  }

  onDeleteRepackItem = (repackItemId: number) => {
    this.props.deleteRepackItem(repackItemId);
  }

  renderDownloadTable = () => {
    let result: any = []

    this.state.dataSource.forEach((el: any, index: number) => {
      const {inputRepack, outRepack, totalRepack} = this.getRepackConentData(el)

      const repackTitle = (
        <tr key={`download-title-${el.id}`}>
          <td>Repack {index + 1} {el.repackReferenceNo ? `(${el.repackReferenceNo})` : ''}</td>
          <td colSpan={4}></td>
        </tr>
      )
      const repackDate = (
        <tr key={`download-date-${el.id}`}>
          <td>Repack date: {moment(el.repackDate).format('MM/DD/YYYY')}</td>
          <td colSpan={4}></td>
        </tr>
      )
      const yieldInfo = (
        <tr key={`download-yield-info-${el.id}`}>
          <td>Yield loss paid for by: {el.yieldLossPaidType == 0 ? 'Vendor (shipper)' : 'Wholesaler/broker'}</td>
          <td colSpan={4}></td>
        </tr>
      )
      const inputHeader = (
        <tr key={`download-input-header-${el.id}`}>
          <td>Input Product</td>
          <td>Input Qty</td>
          <td>Cost per unit</td>
          <td>Total input cost</td>
          <td>New Weight in</td>
        </tr>
      )
      const inputRepackContent = (
        <tr key={`download-repack-content-${el.id}`}>
          <td>{inputRepack.variety}</td>
          <td>{inputRepack.quantity} {inputRepack.uom}</td>
          <td>{`$${formatNumber(inputRepack.cost, 2)}/${inputRepack.uom}`}</td>
          <td>{`${inputRepack.quantity < 0 ? '-' : ''}$${formatNumber(Math.abs(inputRepack.quantity) * inputRepack.cost, 2)}`}</td>
          <td>{inputRepack.netWeight} lbs</td>
        </tr>
      )
      const outHeader = (
        <tr key={`download-out-header-${el.id}`}>
          <td>Output Product</td>
          <td>Output Qty</td>
          <td>Est. cost per unit</td>
          <td>Total output cost</td>
          <td>New Weight out</td>
        </tr>
      )
      let totalCost = 0
      let outRepackContents = outRepack.map((o: any, i: number)=> {
        totalCost += o.quantity * o.cost
        return (
          <tr key={`download-out-content-${el.id}-${i}`}>
            <td>{o.variety}</td>
            <td>{o.quantity} {o.uom}</td>
            <td>{`$${formatNumber(o.cost, 2)}/${o.uom}`}</td>
            <td>{`${o.quantity < 0 ? '-' : ''}$${formatNumber(Math.abs(o.quantity) * o.cost, 2)}`}</td>
            <td>{o.netWeight} lbs</td>
          </tr>
        )
      })

      if(outRepack.length > 1) {
        const totalRow = {
          isTotalRow: true,
          variety: 'Total',
          quantity: _.sumBy(outRepack, 'quantity') + ' UNITS',
          totalCost: `${totalCost < 0 ? '-' : ''}$${formatNumber(totalCost, 2)}`,
          netWeight: _.sumBy(outRepack, 'netWeight')
        }
        outRepackContents = [...outRepackContents,
        (
          <tr key={`download-out-total-${el.id}`}>
             <td>{totalRow.variety}</td>
            <td>{totalRow.quantity}</td>
            <td></td>
            <td>{totalRow.totalCost}</td>
            <td>{totalRow.netWeight} lbs</td>
          </tr>
        )]
      }
      const totalHeader = (
        <tr key={`download-total-header-${el.id}`}>
          <td></td>
          <td>By units</td>
          <td>Cost per unit</td>
          <td>Yield loss cost</td>
          <td>By weight</td>
        </tr>
      )
      const totalContent = (
        <>
          <tr key={`download-total-content-${el.id}-1`}>
            <td></td>
            <td>{totalRepack.quantity} unit</td>
            <td>{`$${formatNumber(totalRepack.cost, 2)}/unit`}</td>
            <td>{totalRepack.yieldLossQty < 0 ? '-' : ''}${formatNumber(Math.abs(totalRepack.yieldLossQty), 2)}</td>
            <td>{totalRepack.netWeight} lbs</td>
          </tr>
          <tr key={`download-total-content-${el.id}-2`}>
            <td></td>
            <td>{mathRoundFun(totalRepack.quantity / inputRepack.quantity * 100, 2)}%</td>
            <td></td>
            <td></td>
            <td>{totalRepack.netWeight == 0 || inputRepack.netWeight == 0 ? 0 : mathRoundFun(Math.abs(totalRepack.netWeight) / inputRepack.netWeight * 100, 2)}%</td>
          </tr>
          <tr key={`download-total-content-${el.id}-3`}><td colSpan={5}></td></tr>
        </>
      )

      result = [...result, repackTitle, repackDate, yieldInfo, inputHeader, inputRepackContent, outHeader, ...outRepackContents, totalHeader, totalContent]
    });
    return result
  }

  onClickDownload = () => {
    const { dataSource } = this.state
    const XLSX = window.XLSX
    let wb = XLSX.utils.table_to_book(document.getElementById('repack-table'))
    let wscols = [
      { wpx: 300 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols
    let totalRows: number = 0
    dataSource.forEach((el: any, index: number) => {
      const {inputRepack, outRepack, totalRepack} = this.getRepackConentData(el)

      wb.Sheets.Sheet1['A' + (totalRows + 2)] = { v: moment(el.repackDate).format('MM/DD/YYYY')}
      wb.Sheets.Sheet1['D' + (totalRows + 5)] = { v: `${inputRepack.quantity < 0 ? '-' : ''}$${formatNumber(Math.abs(inputRepack.quantity) * inputRepack.cost, 2)}`}
      let totalCost = 0
      const totalRowCount = outRepack.length > 1 ? 1 : 0
      outRepack.forEach((o: any, i:number) => {
        totalCost += o.quantity * o.cost
        wb.Sheets.Sheet1['D' + (totalRows + 7 + i)] = {v: `${o.quantity < 0 ? '-' : ''}$${formatNumber(Math.abs(o.quantity) * o.cost, 2)}`}
      });
      if(outRepack.length > 1) {
        wb.Sheets.Sheet1['D' + (totalRows + 6 + outRepack.length + 1)] = {v: `${totalCost < 0 ? '-' : ''}$${formatNumber(totalCost, 2)}`}
      }
      wb.Sheets.Sheet1['B' + (totalRows + (6 + outRepack.length) + 3) + totalRowCount] = {v: `${mathRoundFun(Math.abs(totalRepack.quantity) / inputRepack.quantity * 100, 2)}%`}
      wb.Sheets.Sheet1['E' + (totalRows + (6 + outRepack.length) + 3) + totalRowCount] ={v: `${totalRepack.netWeight == 0 || inputRepack.netWeight == 0 ? 0 : mathRoundFun(Math.abs(totalRepack.netWeight) / inputRepack.netWeight * 100, 2)}%`}

      totalRows += el.repackDetails.length + 3 + 3 + 3 + totalRowCount // 3: repack info fields, 3: table headers, 3: total contents including empty row
    });
    const title = `PO ${getOrderPrefix(this.props.sellerSetting, 'purchase')}${this.props.currentOrder?.wholesaleOrderId}-Repack Report.xlsx`
    XLSX.writeFile(wb, title)
  }

  getRepackConentData = (repackItem: any) => {
    let inputRepack: any = repackItem.repackDetails?.find((el: any)=>el.repackType === 0)
    const repackInputCost = inputRepack?.wholesaleOrderItem.cost
    inputRepack = {
      ...inputRepack,
      cost: repackInputCost,
      variety: `${inputRepack?.item.variety}` + (inputRepack?.item.sku ? `(${inputRepack?.item.sku})` : '')
    }
    const outRepack = repackItem.repackDetails.filter((el: any)=>el.repackType === 1).map((el: any) => {
      return {
        ...el,
        cost: el.wholesaleOrderItem.cost,
        variety: `${el.item.variety}` + (el.item.sku ? `(${el.item.sku})` : '')
      }
    })

    let totalRepack: any = {
        quantity: _.sumBy(outRepack, 'quantity') - inputRepack.quantity,
        cost: repackInputCost,
        netWeight: _.sumBy(outRepack, 'netWeight') - inputRepack.netWeight,
      }
    totalRepack = {...totalRepack, yieldLossQty: totalRepack.quantity * totalRepack.cost}
    return {inputRepack, outRepack, totalRepack}
  }

  onOpenEditRepackModal = (selectedRepackItem: any) => {
    const orderItem = selectedRepackItem?.repackDetails?.find((el: any)=>el.repackType == 0)?.wholesaleOrderItem ?? {}
    this.setState({
      repackModalVisible: true,
      selectedRepackItem,
      selectedRepackInputOrderItem: {...orderItem, status: orderItem?.wholesaleOrderItemStatus},
    })
  }

  onCloseRepackModal = () => {
    this.setState({
      repackModalVisible: false,
    })
  }

  onUpdatePO = () => {
    const { selectedRepackItem } = this.state
    const _this = this
    const outRepacks = selectedRepackItem?.repackDetails?.filter((el: any)=>el.repackType === 1)
    const orderItems = outRepacks.map((el: any)=>el.wholesaleOrderItem)
    const lotIds = orderItems?.map((el: any)=>el.lotId)
    OrderService.instance.getSoldItemsById({dataList: lotIds}).subscribe({
      next(res: any) {
        const data = responseHandler(res, false).body.data
        if(data?.length) {
          _this.setState({visibleConfirmUpdateModal: true})
        } else {
          _this.updatePO()
        }
      }
    })
  }

  updatePO = () => {
    const { dataSource, udpatedQuantities } = this.state
    const repackData = dataSource.map((el: any)=>({
      ...el,
      repackDate: moment(el.repackDate).format('MM/DD/YYYY')
    }))
    const pallets = Object.values(udpatedQuantities ?? {})
    this.props.updateRepack({repackData, pallets})
    this.setState({
      visibleConfirmUpdateModal: false,
      selectedRepackItem: null,
      updateSavingDataList: null,
      selectedRepackDetail: null,
      udpatedQuantities: null
    })
  }

  updateSavingData = (data: any) => {
    const { selectedRepackItem } = this.state
    let curSavingList = this.state.updateSavingDataList ?? {}
    
    const dataList = data?.data?.dataList.map((el: any) => ({
        wholesaleOrderItemId: el.wholesaleOrderItemId,
        quantity: +el.quantity,
        netWeight: +el.netWeight
    }))
    
    this.updateTableDataSource(data.repackItemId, dataList)

    if(selectedRepackItem) {
      this.setState({
        updateSavingDataList: 
          {
            ...curSavingList,
            [selectedRepackItem.id]: data
          }
      })
    }
  }

  updateQuantites = (data: any) => {
    const qtys = this.state.udpatedQuantities ?? {}
    const tableData = {
      wholesaleOrderItemId: data.wholesaleOrderItemId,
      quantity: data.numberOfPallets * data.unitOfPallets,
      netWeight: data.numberOfPallets * data.unitOfPallets * data.unitWeight,
    }
    this.updateTableDataSource(data.repackItemId, [tableData], true)
    
    this.setState({
      visibleQuantityModal: false,
      udpatedQuantities: {
        ...qtys,
        [data.repackItemId]: data
      }
    })
  }

  updateTableDataSource = (repackItemId: number, data: any[], fromQuantityModal: boolean = false) => {
    let repackFoundIndex = 0;
    const repackItem: any = this.state.dataSource.find((el: any, index: number)=>{
      if(el.id == repackItemId) {
        repackFoundIndex = index;
        return true
      }
    })
    
    const orderItemIds = data.map(el=>el.wholesaleOrderItemId)
    repackItem.repackDetails = repackItem?.repackDetails.map((el: any, index: number)=>{
      if(orderItemIds.indexOf(el.wholesaleOrderItem.wholesaleOrderItemId) > -1) {
       const tableData = data.find(d=>d.wholesaleOrderItemId == el.wholesaleOrderItem.wholesaleOrderItemId)
       return {
         ...el,
         quantity: fromQuantityModal ? el.quantity + tableData?.quantity : tableData?.quantity,
         netWeight: fromQuantityModal ? el.netWeight + tableData?.netWeight : tableData?.netWeight
       }
      } else {
        return el
      }
    })
    
    const stateDataSourceObj = [...this.state.dataSource]
    stateDataSourceObj.splice(repackFoundIndex, 1, repackItem)
    this.setState({
      dataSource: stateDataSourceObj
    })
  }

  getSavableOrderItems = () => {
    const { updateSavingDataList, udpatedQuantities, dataSource } = this.state
    let updateDataKeys = Object.keys(updateSavingDataList ? updateSavingDataList : {})
    let updatedQuantityKeys = Object.keys(udpatedQuantities ? udpatedQuantities : {})
    updatedQuantityKeys.forEach(key => {
      if(updateDataKeys.indexOf(key) === -1){
        updateDataKeys.push(key)
      }
    });

    let savableOutputOrderItems: any[] = []
    dataSource.forEach(repackItem=> {
      if(updateDataKeys.indexOf(repackItem.id.toString()) > -1) {
        const { outRepack } = this.getRepackConentData(repackItem)
        const orderItems = outRepack.map(el=>{
          return el.wholesaleOrderItem
        })
        savableOutputOrderItems = [...savableOutputOrderItems, ...orderItems]
      }
      
    })
  }

  render() {
    const { 
      dataSource, 
      visibleQuantityModal, 
      selectedRepackDetail, 
      selectedRepackInputOrderItem,
      selectedRepackItem,
      updateSavingDataList,
      visibleConfirmUpdateModal,
      udpatedQuantities
     } = this.state
    const selectedOutRepackDetails: any[] = selectedRepackItem?.repackDetails?.filter((el: any)=>el.repackType === 1)
    let selectedOutOrderItems: any[] = []
    if(selectedOutRepackDetails?.length) {
      selectedOutOrderItems = selectedOutRepackDetails.map(el=>el.wholesaleOrderItem)
    }
    const savableOrderItems = this.getSavableOrderItems()
    let totalInputUnits = 0;
    let totalInputWeight = 0;
    let totalOutputUnits = 0;
    let totalOutputWeight = 0;
    let totalUnits = 0;
    let totalWeights = 0;
    if (dataSource.length > 0) {
      dataSource.forEach((el: any) => {
        const { inputRepack, outRepack, totalRepack } = this.getRepackConentData(el)

        totalInputUnits += inputRepack.quantity;
        totalInputWeight += inputRepack.netWeight;

        totalOutputUnits += _.sumBy(outRepack, 'quantity');
        totalOutputWeight += _.sumBy(outRepack, 'netWeight');

        totalUnits += totalRepack.quantity;
        totalWeights += totalRepack.netWeight;
      })
    }
    
    return (
      <RepackTabWrapper>
        {dataSource.length > 0 && (
          <>
            <RepackDownloadButton>
              <div>
                <ThemeOutlineButton onClick={this.onClickDownload}>
                  <Icon type="save" theme="filled" />
                  Download
                </ThemeOutlineButton>
              </div>
              <div style={{marginTop: 12}}>
                <ThemeButton style={{width: '100%'}} disabled={!updateSavingDataList && !udpatedQuantities} onClick={this.onUpdatePO}>Update PO</ThemeButton>
              </div>
            </RepackDownloadButton>
            <div className='summary'>
              <div className="table-header">Repack Summary</div>
              <table>
                <thead>
                  <tr>
                    <th></th>
                    <th width="30%">Units</th>
                    <th width="30%">Weight</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Total input</td>
                    <td>{formatNumber(totalInputUnits, 0)} units</td>
                    <td>{formatNumber(totalInputWeight, 0)} lbs</td>
                  </tr>
                  <tr>
                    <td>Total output</td>
                    <td>{formatNumber(totalOutputUnits, 0)} ({mathRoundFun(totalOutputUnits / totalInputUnits * 100, 2)}%)</td>
                    <td>{formatNumber(totalOutputWeight, 0)} ({mathRoundFun(totalOutputWeight / totalInputWeight * 100, 2)}%)</td>
                  </tr>
                  <tr>
                    <td>Yield gain/loss</td>
                    <td>{formatNumber(totalUnits, 0)} ({mathRoundFun(totalUnits / totalInputUnits * 100, 2)}%)</td>
                    <td>{formatNumber(totalWeights, 0)} ({mathRoundFun(totalWeights / totalInputWeight * 100, 2)}%)</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </>
        )}
        {dataSource.map((el: any, index) => {
          return (
            <RepackModalWrapper className="in-tab" key={`table-${index}`}>
              
                <Flex className="v-center">
                  <div className="table-header">Repack {index + 1} {el.isOneTimeRepack ? '(One-Time Repack)' : '(Continuous Repack)'}
                  </div>
                  {!el.isOneTimeRepack &&
                  <div className="repack-action-icon">
                    <IconSvg type="edit" viewBox="0 0 18 18" width={19} height={19} onClick={() => this.onOpenEditRepackModal(el)} />
                  </div>
                  }
                  <div className="repack-action-icon">
                    <Tooltip title="Delete this repack record. Inventory updates made by this repack operation will not be reverted.">
                      <IconSvg type="trash" viewBox="0 0 32 32" width={24} height={24} onClick={()=> this.onDeleteRepackItem(el.id)}/>
                    </Tooltip>
                  </div>
                </Flex>

              <div className="sub-header">
                <div>Repack date: {moment(el.repackDate).format('MM/DD/YYYY')}</div>
                <div>Yield loss paid for by: {el.yieldLossPaidType == 0 ? 'Vendor (shipper)' : 'Wholesaler/broker'}</div>
              </div>
              {this.renderTable(el)}
            </RepackModalWrapper>
          )
        })}
        <RepackQuantityModal
          visible={visibleQuantityModal}
          savingQuantities={false}
          selectedRepackItem={selectedRepackItem}
          selectedRepackDetail={selectedRepackDetail}
          onClose={this.onCloseRepackQuantityModal}
          updateQuantites={this.updateQuantites}
          {...this.props}
        />
      
        <EnterRepackModal
          isCreating={false}
          selectedItem={selectedRepackInputOrderItem}
          selectedRepackItem={selectedRepackItem}
          sellerSetting={this.props.sellerSetting}
          visible={this.state.repackModalVisible}
          updateSavingData={this.updateSavingData}
          onClose={this.onCloseRepackModal}
        />
    
        <ConfirmRepackUpdateModal
          visible={visibleConfirmUpdateModal}
          dataSource={selectedOutOrderItems}
          updateSavingDataList={{repackData: updateSavingDataList, pallets: udpatedQuantities}}
          onClose={()=>this.setState({visibleConfirmUpdateModal: false})}
          onSave={this.updatePO}
        />
        <table id="repack-table" style={{display: 'none'}}>
          <tbody>
            {this.renderDownloadTable()}
          </tbody>
        </table>
      </RepackTabWrapper>
    )
  }
}
