import styled from '@emotion/styled'
import { Row } from 'antd'
import { gray01, topLightGrey } from '~/common'

export const FloatingMenu = styled('div')((props: any) => ({
  height: 28,
  padding: '2px !important',
  width: 36,
  textAlign: 'center',
  backgroundColor: 'white',
  border: '1px solid grey',
  boxShadow: '0px 1px 3px 1px rgba(0,0,0,0.65);',
  position: 'absolute',
  right: 42,
  borderRadius: 4,
  left: 'calc(50% - 18px)',
  top: -4,
  '&:hover': {
    boxShadow: '0px 1px 1px 1px rgba(0,0,0,0.45);',
    backgroundColor: topLightGrey,
  },
}))

export const RepackModalWrapper = styled('div')((props: any) => ({
  margin: '-15px 0px',
  padding: 5,
  label: {
    marginBottom: 3,
    fontWeight: 'normal !important' as 'normal',
    color: 'rgba(0, 0, 0, 0.65) !important'
  },
  '.table-header': {
    fontSize: 15,
    fontWeight: 500,
    marginTop: 20,
    marginBottom: 5,
    color: '#3c4244',
  },
  '.ant-table-wrapper': {
    '.ant-table-content': {
      '.ant-table-body': {
        'table': {
          borderTop: '3px solid #ddd',
          borderRadius: 0
        },
        '.ant-table-thead': {
          '& > tr > th': {
            padding: '10px 16px'
          },
          '.ant-table-column-title': {
            fontSize: 14,
            color: '#515a5c !important',
          }
        },
        '.ant-table-tbody > tr > td': {
          backgroundColor: 'white !important',
          padding: '0px 16px',
          height: 44,
          '.icon': {
            svg: {
              path: {
                fill: props.theme.theme
              }
            }
          },
          '.trash': {
            position: 'absolute',
            right: 5,
            cursor: 'pointer',
            svg: {
              path: {
                fill: props.theme.theme
              }
            }
          },
          '.percent': {
            fontSize: 12,
            color: '#888',
            height: 18,
          }
        }
      }
    }
  },
  '.total-table': {
    marginTop: 20,
    '.ant-table-wrapper': {
      '.ant-table-content': {
        '.ant-table-body': {
          '.ant-table-tbody > tr > td': {
            height: 60
          }
        }
      }
    }
  },
  '&.in-tab': {
    '.table-header': {
      position: 'relative',
      marginTop: 30,
      fontWeight: 'bold',
      color: '#666',
    },
    '.sub-header': {
      margin: '8px 0 18px',
      fontSize: 13,
      color: '#333',
      lineHeight: '23px'
    },
    '.repack-action-icon': {
      marginTop: 26,
      cursor: 'pointer',
      marginLeft: 20,
      'path': {
        fill: props.theme.theme
      }
    },
    '.repack-download': {
      position: 'absolute',
      right: 0
    }
  }
}))

export const RepackDownloadButton = styled('div')((props: any) => ({
  position: 'absolute',
  right: 0
}))

export const RepackTabWrapper = styled('div')((props: any) => ({
  position: 'relative',
  padding: '5px 0px 50px 20px',
  color: gray01,
  maxWidth: 1100,
  '.with-total-row': {
    'tr:last-child': {
      td: {
        borderBottom: 'none'
      }
    }
  },
  '.summary': {
    borderBottom: '1px solid #ccc',
    paddingBottom: 20,
    marginBottom: 10,
    '.table-header': {
      fontSize: 18,
      fontWeight: 600,
    },
    table: {
      minWidth: 500
    }
  }
}))

export const RepackQuantityModalRow = styled(Row)((props: any) => ({
  margin: 12
}))

export const RepackConfirmModalWrapper = styled('div')((props: any) => ({
  padding: 12,
  '.description': {
    margin: '12px 0'
  },
  
}))