import React from 'react'
import {
  ThemeOutlineButton,
  FlexCenter,
  ThemeIcon,
  ThemeSelect,
  ThemeCheckbox,
  ThemeInputNumber,
  ThemeInput,
  ThemeButton,
  ThemeModal,
} from '~/modules/customers/customers.style'
import { Icon as IconSVG } from '~/components'
import { of } from 'rxjs'
import OverviewModal from './overview-modal'
import { OrderItem } from '~/schema'
import _, { cloneDeep } from 'lodash'
import EditableTable from '~/components/Table/editable-table'
import { Popconfirm, Tooltip, Icon, Popover, Select, Menu, Dropdown } from 'antd'
import {
  formatNumber,
  isArrayEqual,
  ratioQtyToBaseQty,
  baseQtyToRatioQty,
  ratioPriceToBasePrice,
  basePriceToRatioPrice,
  formatItemDescription,
  getDefaultUnit,
  getAllowedEndpoint,
  addVendorProductCode,
  judgeConstantRatio,
  isFloat,
} from '~/common/utils'
import TypeEditWrapper from '~/modules/settings/tabs/Product/TypeEditWrapper'
import jQuery from 'jquery'
import NotesModal from '~/modules/customers/sales/cart/modals/notes-modal'
import { noteGreyIcon, noteIcon } from '~/modules/customers/sales/_style'
import UomSelect from '~/modules/customers/nav-sales/order-detail/components/uom-select'
import { DialogSubContainer, FloatMenuItem, noPaddingFooter, SelectWrapper } from '~/modules/customers/nav-sales/styles'
import ItemHistoryModal from './item-history-modal'
import ItemCostModal from './item-cost-modal'
import AddNewBrandModal from '~/modules/orders/components/add-new-brand-modal'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, responseHandler } from '~/common/utils'
import { ClassNames } from '@emotion/core'
import LotSpecificationModal from './lot-specification-modal'
import EnterRepackModal from './repack/modal/enter-repack-modal'
import { FloatingMenu } from './repack/repack.styles'
import { ClickParam } from 'antd/lib/menu'
import SearchFooter from './components/search-footer'

interface TableViewProps {
  currentProducts?: any[]
  orderId: string
  searchStr: string
  isLocked: Boolean
  items: OrderItem[]
  handleSave: Function
  handleRemove: Function
  getCompanyProductAllTypes: Function
  setCompanyProductType: Function
  deleteProductType: Function
  updateProductType: Function
  companyProductTypes: any
  updatePOItemUom: Function
  handlerUpdatePOUOMRedux: Function
  itemHistory: any[]
  getPurchaseOrderItemHistory: Function
  sellerSetting: any
  orderStatus: string
  newModalVisible: boolean
  updateProduct: Function
  itemCost: any[]
  getPurchaseOrderItemCost: Function
  updateSalesPrice: Function
  printable: any
  updateOrderQuantityLoading: any
  onSearch: Function
  onSelect: Function
  tableLoading: boolean
  addItemId: boolean
  changeAddItem: () => void
}

export class TableView extends React.PureComponent<TableViewProps> {
  state: any
  lotSpecModalRef = React.createRef<any>()
  constructor(props: TableViewProps) {
    super(props)
    this.state = {
      dataSource: this.initDataWithSalesPrice(cloneDeep(props.items)),
      overviewModalVisible: false,
      initNotes: _.map(props.items, 'note'),
      modifyNotes: _.map(props.items, 'note'),
      itemHistoryModal: false,
      itemCostModal: false,
      selectedItem: null,
      addItemClicked: false,
      openAddNewBrandModal: false,
      currentSuppliers: [],
      oldBrands: [],
      addingBrandItemId: null,
      brandingItemName: '',
      visibleCOOModal: false,
      oldCost: null,
      wholesaleOrderItemId: null,
      newCost: null,
      visibleLotSpecificationModal: false,
      repackOrderItem: null,
      repackModalVisible: false,
      addItemId: false,
    }
  }

  componentDidUpdate(prevProps: TableViewProps) {
    if (this.state.addItemClicked && prevProps.items.length == this.props.items.length - 1) {
      const rows = jQuery('.purchase-order-overview-table tbody .ant-table-row')
      if (rows.length > 0) {
        const editableEls = jQuery(rows[rows.length - 1]).find('.tab-able')
        if (editableEls.length > 0) {
          setTimeout(() => {
            jQuery(editableEls[0]).trigger('click')
            this.setState({ addItemClicked: false })
          }, 50)
        }
      }
    }
  }

  componentWillReceiveProps(nextProps: TableViewProps) {
    if (this.props.items != nextProps.items) {
      this.setState({
        dataSource: this.initDataWithSalesPrice(cloneDeep(nextProps.items)),
      })
    }
    if (this.props.newModalVisible === true && nextProps.newModalVisible === false) {
      this.setState({ addItemClicked: true })
    }

    if (nextProps.addItemId !== null) {
      this.setState({
        addItemId: nextProps.addItemId,
      })
    }
  }

  initDataWithSalesPrice = (data: Array<OrderItem>) => {
    data.forEach((el) => {
      const markup = (el.margin ? el.margin : 0) / 100
      const pricingUOM = el.pricingUOM ? el.pricingUOM : el.baseUOM
      el.salesPrice = formatNumber(el.freight + el.cost / (1 - markup), 2)
      el.cost = basePriceToRatioPrice(pricingUOM, el.cost, el)
      el.originQuantity = el.quantity
      el.quantity = baseQtyToRatioQty(
        el.overrideUOM ? el.overrideUOM : el.UOM,
        el.quantity,
        el,
      )
      el.qtyConfirmed = baseQtyToRatioQty(
        el.overrideUOM ? el.overrideUOM : el.UOM,
        el.qtyConfirmed,
        el,
      )
      el.receivedQty = baseQtyToRatioQty(
        el.overrideUOM ? el.overrideUOM : el.UOM,
        el.receivedQty,
        el,
      )
      el.pricingUOM = pricingUOM
    })
    return data
  }

  handleSave = (row: OrderItem, id: any) => {
    this.props.updateOrderQuantityLoading.fetching = true
    let newFreight = Number(row.freight)
    let cost = Number(row.cost)
    let salesPrice = Number(row.salesPrice)
    let margin = Number(row.margin)
    // const newData = [...this.props.items]
    const newData = cloneDeep(this.state.dataSource)
    const index = newData.findIndex((item) => row.wholesaleOrderItemId == item.wholesaleOrderItemId)
    const item = newData[index]
    // let newFreight = Number(item.freight)
    // let cost = Number(item.cost)
    // let salesPrice = Number(item.salesPrice)
    // let margin = Number(item.margin)
    const markup = (item.margin ? item.margin : 0) / 100
    const itemSalesPrice = Number(Number(item.freight + item.cost * (1 + markup)).toFixed(2))
    if (cost != 0) {
      if (
        (!isNaN(newFreight) && item.freight != newFreight) ||
        (!isNaN(margin) && item.margin != margin) ||
        (!isNaN(cost) && item.cost != cost)
      ) {
        row.salesPrice = formatNumber(newFreight + cost * (1 + margin / 100), 2)
      } else if (!isNaN(salesPrice) && itemSalesPrice != salesPrice) {
        let diff: number = ((((salesPrice - newFreight) / cost) * 100) / 100).toFixed(4)
        let newMargin = salesPrice > 0 && salesPrice - newFreight > cost ? ((diff - 1.0) * 100).toFixed(2) : 0
        row.margin = newMargin
        row.salesPrice = salesPrice
      }
    } else {
      delete row.margin
      delete row.salesPrice
    }

    let status = row.status
    if (id == 'qtyConfirmed' && (status == 'NEW' || status == 'PLACED') && !_.isEmpty(row.qtyConfirmed)) {
      status = 'CONFIRMED'
    }
    const pricingUom = row.pricingUOM ? row.pricingUOM : row.inventoryUOM
    const uom = row.overrideUOM ? row.overrideUOM : row.UOM
    row.quantity = row.quantity ? row.quantity : 0
    row.qtyConfirmed = row.qtyConfirmed ? row.qtyConfirmed : 0
    row.receivedQty = row.receivedQty ? row.receivedQty : 0
    let cost = ratioPriceToBasePrice(pricingUom, row.cost, row, 12)
    let quantity = ratioQtyToBaseQty(uom, row.quantity, row, 12)
    let qtyConfirmed = ratioQtyToBaseQty(uom, row.qtyConfirmed, row, 12)
    let receivedQty = ratioQtyToBaseQty(uom, row.receivedQty, row, 12)
    let orderWeight = row.orderWeight

    // No reload table in case of UNITS ORDERED or UNITS CONFIRMED
    if (id != 'lotSpec') {
      newData[index][id] = row[id]
    } else {
      newData[index]['modifiers'] = row['modifiers']
      newData[index]['extraOrigin'] = row['extraOrigin']
      newData[index]['grossVolume'] = row['grossVolume']
      newData[index]['grossWeight'] = row['grossWeight']
    }
    this.setState({ dataSource: newData })
    // const noRefresh = id === 'qtyConfirmed' || id === 'quantity'

    this.props.handleSave(
      {
        ...row,
        cost: cost,
        quantity: quantity,
        qtyConfirmed,
        receivedQty,
        status,
        orderWeight,
      },
      id === 'lotSpec' ? false : true,
    )
  }

  onDeleteRow = (id: number) => {
    // const dataSource = [...this.state.dataSource]
    // this.setState({ dataSource: dataSource.filter((item) => item.wholesaleOrderItemId !== id) })
    this.props.handleRemove(id)
  }

  handleSelectProduct = (record: OrderItem, item: any) => {
    record.itemId = item.itemId
    // this.props.handleSave(record)
  }

  toggleOverviewModal = () => {
    this.setState({
      overviewModalVisible: !this.state.overviewModalVisible,
    })
  }

  setNewNote(value: string, index: number) {
    let { modifyNotes } = this.state
    modifyNotes[index] = value
    this.setState({ modifyNotes })
  }

  onNotesVisbleChange = (record: any, index: number, visible: boolean) => {
    let { initNotes, modifyNotes } = this.state
    if (visible === false) {
      if (initNotes[index] != modifyNotes[index]) {
        initNotes[index] = modifyNotes[index]
        const uom = record.overrideUOM ? record.overrideUOM : record.inventoryUOM
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        let cost = ratioPriceToBasePrice(pricingUOM, record.cost, record, 12)
        let quantity = baseQtyToRatioQty(
          record.inventoryUOM,
          ratioQtyToBaseQty(uom, record.quantity, record, 12),
          record,
          12,
        )
        let qtyConfirmed = baseQtyToRatioQty(
          record.inventoryUOM,
          ratioQtyToBaseQty(uom, record.qtyConfirmed, record, 12),
          record,
          12,
        )
        let receivedQty = baseQtyToRatioQty(
          record.inventoryUOM,
          ratioQtyToBaseQty(uom, record.receivedQty, record, 12),
          record,
          12,
        )
        this.props.handleSave({
          ...record,
          cost: cost,
          quantity: quantity,
          qtyConfirmed,
          receivedQty,
          note: modifyNotes[index],
        })
        this.setState({ initNotes })
      }
    }
  }

  onTableDropdownChange = (record: any, type: string, value: any) => {
    const row = { ...record }
    const uom = record.overrideUOM ? record.overrideUOM : record.inventoryUOM
    const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
    let cost = ratioPriceToBasePrice(pricingUOM, record.cost, record, 12)
    let quantity = baseQtyToRatioQty(
      record.inventoryUOM,
      ratioQtyToBaseQty(uom, record.quantity, record, 12),
      record,
      12,
    )

    let qtyConfirmed = baseQtyToRatioQty(
      record.inventoryUOM,
      ratioQtyToBaseQty(uom, record.qtyConfirmed, record, 12),
      record,
      12,
    )
    let receivedQty = baseQtyToRatioQty(
      record.inventoryUOM,
      ratioQtyToBaseQty(uom, record.receivedQty, record, 12),
      record,
      12,
    )
    if (type == 'extraOrigin') {
      const extraCOO = this.props.companyProductTypes ? this.props.companyProductTypes.extraCOO : []
      const found = extraCOO.find((el) => el.id == value)
      if (found) {
        row[type] = found.name
      } else {
        return
      }
    } else {
      row[type] = value
    }
    row['cost'] = cost
    row['quantity'] = quantity
    row['qtyConfirmed'] = qtyConfirmed
    row['receivedQty'] = receivedQty
    // const newData = [...this.props.items]
    // const index = newData.findIndex((item) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    // const item = newData[index]
    // newData.splice(index, 1, {
    //   ...item,
    //   ...row,
    // })

    // this.setState({ dataSource: newData })
    this.props.handleSave(row)
  }

  onCheckPas = (record: any, e: any) => {
    const uom = record.overrideUOM ? record.overrideUOM : record.inventoryUOM
    const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
    let cost = ratioPriceToBasePrice(pricingUOM, record.cost, record, 12)
    let quantity = baseQtyToRatioQty(
      record.inventoryUOM,
      ratioQtyToBaseQty(uom, record.quantity, record, 12),
      record,
      12,
    )

    let qtyConfirmed = baseQtyToRatioQty(
      record.inventoryUOM,
      ratioQtyToBaseQty(uom, record.qtyConfirmed, record, 12),
      record,
      12,
    )
    let receivedQty = baseQtyToRatioQty(
      record.inventoryUOM,
      ratioQtyToBaseQty(uom, record.receivedQty, record, 12),
      record,
      12,
    )

    this.props.handleSave({
      ...record,
      pas: e.target.checked,
      cost: cost,
      record,
      quantity: quantity,
      qtyConfirmed,
      receivedQty,
    })
  }

  onChangeUom = (orderItemId: number, value: any) => {
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    let row: OrderItem | null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el['UOM'] = value
        el['overrideUOM'] = value
        row = el
        return
      }
    })
    this.setState({
      dataSource: cloneData,
    })

    let data = {
      id: orderItemId,
      UOM: value,
      cost: row ? ratioPriceToBasePrice(row.pricingUOM, row.cost, row, 12) : 0,
      pricingUOM: row ? row.pricingUOM : '',
    }

    // const reduxData = {}
    this.props.handlerUpdatePOUOMRedux({
      ...data,
      quantity: ratioQtyToBaseQty(value, row?.quantity, row, 12),
      qtyConfirmed: ratioQtyToBaseQty(value, row?.qtyConfirmed, row, 12),
      receivedQty: ratioQtyToBaseQty(value, row?.receivedQty, row, 12)
    })
    this.props.updatePOItemUom(data)
  }

  onChangeCost = (orderItemId: number, value: any) => {
    if (!_.isNumber(value)) {
      return
    }
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    let oldCost = null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        oldCost = el['cost']
        el['cost'] = value
        return
      }
    })
    if (this.state.oldCost == null) {
      this.setState({
        oldCost: oldCost,
        wholesaleOrderItemId: orderItemId,
      })
    }
    this.setState({
      dataSource: cloneData,
    })
  }

  openItemCostModal = (orderItemId: number, record: any, value: any) => {
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    const inputCost = parseFloat(value.substring(1))
    let row: OrderItem = null

    if (inputCost === record['cost']) {
      return
    }

    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el['cost'] = parseFloat(value.substring(1))
        row = el
        return
      }
    })

    this.setState(
      {
        dataSource: cloneData,
      },
      () => {
        if (!record.hasTransaction) {
          //no transaction
          this.updateOrderItemCost(record, null)
        }
      },
    )

    const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM

    this.setState({
      selectedItem: record,
      newCost: ratioPriceToBasePrice(pricingUOM, row?.cost, row, 12),
      itemCostModal: Boolean(record.hasTransaction),
    })
  }

  updateOrderItemCost = (record: any, data: any) => {
    const { dataSource } = this.state
    const { orderId } = this.props
    const _this = this
    let cloneData = cloneDeep(dataSource)
    let row: OrderItem | null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == record.wholesaleOrderItemId) {
        row = el
        return
      }
    })

    if (data != null && data.length != 0) {
      //update new price
      OrderService.instance.updateSalesPrice(orderId, data).subscribe({
        next(res: any) {
          of(responseHandler(res, false).body.data)
        },
        error(err) {
          checkError(err)
        },
        complete() {
          _this.props.updatePOItemUom({
            id: record.wholesaleOrderItemId,
            UOM: row ? row.overrideUOM : '',
            cost: row ? ratioPriceToBasePrice(row.pricingUOM, row.cost, row, 12) : 0,
            pricingUOM: row ? row.pricingUOM : '',
          })
        },
      })
    } else {
      this.props.updatePOItemUom({
        id: record.wholesaleOrderItemId,
        UOM: row ? row.overrideUOM : '',
        cost: row ? ratioPriceToBasePrice(row.pricingUOM, row.cost, row, 12) : 0,
        pricingUOM: row ? row.pricingUOM : '',
      })
    }

    this.setState({
      oldCost: null,
      newCost: null,
      selectedItem: null,
      itemCostModal: false,
    })
  }

  onChangePricingUom = (orderItemId: number, value: string) => {
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    let row: OrderItem | null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el['pricingUOM'] = value
        row = el
        return
      }
    })

    this.props.updatePOItemUom({
      id: orderItemId,
      UOM: row ? row.overrideUOM : '',
      cost: row ? ratioPriceToBasePrice(value, row.cost, row, 12) : 0,
      pricingUOM: value,
    })
  }

  openItemHistoryModal = (orderItem: any) => {
    this.setState({
      selectedItem: orderItem,
      itemHistoryModal: true,
    })
  }

  closeItemHistoryModal = () => {
    this.setState({
      selectedItem: null,
      itemHistoryModal: false,
    })
  }

  closeItemCostModal = () => {
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == this.state.wholesaleOrderItemId) {
        el['cost'] = this.state.oldCost
        return
      }
    })

    this.setState({
      dataSource: cloneData,
      oldCost: null,
      selectedItem: null,
      newCost: null,
      itemCostModal: false,
    })
  }

  openAddNewBrandModal = (record: any) => {
    if (record == -1) {
      this.setState({
        openAddNewBrandModal: false,
        currentSuppliers: [],
        addingBrandItemId: null,
        oldBrands: [],
        brandingItemName: '',
      })
    } else if (record) {
      const itemId = record.itemId
      const suppliers = record.suppliers
      const arrSuppliers = suppliers
        ? suppliers.split(',').map((el) => {
            return el.trim()
          })
        : []
      this.setState({
        openAddNewBrandModal: true,
        currentSuppliers: arrSuppliers,
        oldBrands: arrSuppliers,
        addingBrandItemId: itemId,
        brandingItemName: record.variety,
      })
    }
  }

  openAddNewCOOModal = () => {
    this.setState(
      {
        visibleCOOModal: !this.state.visibleCOOModal,
      },
      () => {
        if (!this.state.visibleCOOModal) {
          this.props.getCompanyProductAllTypes()
        }
      },
    )
  }

  updateItemSupplier = (data: string[]) => {
    const { dataSource, addingBrandItemId, selectedItem } = this.state
    const index = dataSource.findIndex((s: any) => s.wholesaleOrderItemId === selectedItem.wholesaleOrderItemId)
    const diff = data.filter(
      (s: string) => typeof dataSource[index].suppliers === 'undefined' || !dataSource[index].suppliers.includes(s),
    )
    dataSource.forEach((el: any) => {
      if (el.itemId == addingBrandItemId) {
        el.suppliers = data.join(', ')
      }
    })

    let item = JSON.parse(
      JSON.stringify({
        ...selectedItem,
        suppliers: dataSource[index].suppliers,
      }),
    )

    if (diff.length > 0) {
      item.modifiers = diff[0]
    }

    this.setState({
      selectedItem: item,
      dataSource,
      openAddNewBrandModal: false,
    })
  }

  onChangeBrandList = (currentSuppliers: string[]) => {
    this.setState({ currentSuppliers })
  }

  changeQty = (value: number, orderItemId: number, field: string) => {
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    let row: OrderItem | null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el[field] = value

        if (field == 'qtyConfirmed' && (el.status == 'NEW' || el.status == 'PLACED') && el.qtyConfirmed !== '') {
          el.status = 'CONFIRMED'
        }
        if (field == 'qtyConfirmed' && el.qtyConfirmed == '') {
          el.qtyConfirmed = 0
        }
        row = el
        return
      }
    })

    this.setState({
      dataSource: cloneData,
    })
  }

  blurChangeQty = (value: number, orderItemId: number, field: string) => {
    const { dataSource } = this.state
    let cloneData = cloneDeep(dataSource)
    let row: OrderItem | null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        row = el
        return
      }
    })
    // if (row && row[field] != value) {
    this.handleSave(row, field)
    // }
  }

  onShowLotSpecificationModal = (record: any) => {
    this.setState({
      selectedItem: this.state.visibleLotSpecificationModal ? null : record,
      visibleLotSpecificationModal: !this.state.visibleLotSpecificationModal,
    })
  }

  handleSaveLotInfo = () => {
    if (this.state.visibleLotSpecificationModal) {
      this.lotSpecModalRef.current.saveLotSpecInfo()
    }
    this.setState({ visibleLotSpecificationModal: false })
  }

  toggleRepackModal = (param: ClickParam, record: any) => {
    if (param?.key === '0') {
      this.setState({
        repackOrderItem: record,
        repackModalVisible: !this.state.repackModalVisible,
      })
    }
  }

  onCloseRepackModal = () => {
    this.setState({
      repackOrderItem: null,
      repackModalVisible: false,
    })
  }

  renderFloatMenuItems = (record: any): any => {
    return (
      <Menu onClick={(param) => this.toggleRepackModal(param, record)}>
        <Menu.Item key={0}>
          <FloatMenuItem className="black">Repack lot</FloatMenuItem>
        </Menu.Item>
      </Menu>
    )
  }

  render() {
    const { orderId, isLocked, searchStr, items, orderStatus, sellerSetting, currentProducts, printable } = this.props
    const {
      overviewModalVisible,
      visibleLotSpecificationModal,
      selectedItem,
      dataSource,
      repackModalVisible,
      repackOrderItem,
    } = this.state
    const dataSourceTemp = cloneDeep(dataSource)
    const extraCOO = this.props.companyProductTypes ? this.props.companyProductTypes.extraCOO : []
    let filteredItems: any[] = dataSourceTemp.filter((row: any) => {
      return (
        (row.variety && row.variety.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) ||
        (row.SKU && row.SKU.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) ||
        (row.UOM && row.UOM.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0)
      )
    })
    filteredItems = addVendorProductCode(filteredItems, currentProducts)
    const columns = [
      {
        title: '#',
        key: 'id',
        className: 'th-center',
        align: 'center',
        width: 30,
        render: (id: number, record: OrderItem, index: number) => {
          return record.qtyConfirmed !== null && record.quantity !== record.qtyConfirmed ? (
            <Tooltip
              title="Units Ordered does not match Units Confirmed"
              placement="right"
              overlayClassName="error-tooltip"
            >
              <FlexCenter>
                <Icon type="warning" style={{ fontSize: 25, color: '#FFCC00' }} theme="filled" />
              </FlexCenter>
            </Tooltip>
          ) : (
            index + 1
          )
        },
      },
      {
        title: 'LOT #',
        dataIndex: 'lotId',
        className: 'th-left',
        key: 'orderId',
        width: 130,
        // render: (_text: string, record: OrderItem, index: number) => `${orderId}${index+1}`
      },
      {
        title: 'VENDOR CODE',
        dataIndex: 'vendorProductCode',
        className: 'th-left',
        key: 'vendorProductCode',
        width: 145,
      },
      {
        title: 'DESCRIPTION',
        dataIndex: 'variety',
        className: 'th-left',
        key: 'variety',
        // minWidth: 260,
        render: (variety: string, record: any, index: number) => {
          const accountType = localStorage.getItem('accountType') || ''
          return (
            <a
              href={`#/product/${record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`}
              className="product-name"
              style={{ minWidth: 240, display: 'block' }}
            >
              {formatItemDescription(variety, record.SKU, sellerSetting)}
            </a>
          )
        },
      },
      // {
      //   title: 'BRAND',
      //   className: 'th-left',
      //   dataIndex: 'modifiers',
      //   key: 'modifiers',
      //   width: 120,
      //   render: (modifiers: any, record: any) => {
      //     const currentSuppliers = record.suppliers ? record.suppliers.split(', ').map((el: string) => el.trim()) : []
      //     if (!isLocked && orderStatus != 'CANCEL') {
      //       return (
      //         <SelectWrapper className="select-container-parent">
      //           <ThemeSelect
      //             style={{ width: 'fit-content', minWidth: '100%' }}
      //             onChange={(value: any) => this.onTableDropdownChange(record, 'modifiers', value)}
      //             value={modifiers}
      //             showSearch
      //             filterOption={(input: any, option: any) => {
      //               if (option.props.children) {
      //                 return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      //               } else {
      //                 return true
      //               }
      //             }}
      //             dropdownMatchSelectWidth={false}
      //             dropdownRender={(el: any) => {
      //               return (
      //                 <div>
      //                   {el}
      //                   <Divider style={{ margin: '4px 0' }} />
      //                   <div
      //                     style={{ padding: '4px 8px', cursor: 'pointer' }}
      //                     onMouseDown={(e) => e.preventDefault()}
      //                     onClick={(e) => this.openAddNewBrandModal(record)}
      //                   >
      //                     <Icon type="plus" /> Add New
      //                   </div>
      //                 </div>
      //               )
      //             }}
      //           >
      //             <Select.Option value="" />
      //             {currentSuppliers.map((el: any, index: number) => {
      //               return (
      //                 <Select.Option key={`${el}_${index}`} value={el} className="select-option">
      //                   {el}
      //                 </Select.Option>
      //               )
      //             })}
      //           </ThemeSelect>
      //         </SelectWrapper>
      //       )
      //     }
      //     return modifiers ? modifiers : ''
      //   },
      // },
      // {
      //   title: 'ORIGIN',
      //   className: 'th-left',
      //   dataIndex: 'extraOrigin',
      //   key: 'extraOrigin',
      //   width: 120,
      //   render: (extraOrigin: any, record: any) => {
      //     if (!isLocked && orderStatus != 'CANCEL') {
      //       return (
      //         <SelectWrapper className="select-container-parent">
      //           <ThemeSelect
      //             value={extraOrigin}
      //             onChange={(value: any) => this.onTableDropdownChange(record, 'extraOrigin', value)}
      //             style={{ width: 'fit-content', minWidth: '100%' }}
      //             showSearch
      //             filterOption={(input: any, option: any) => {
      //               if (option.props.children) {
      //                 return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
      //               } else {
      //                 return true
      //               }
      //             }}
      //             dropdownMatchSelectWidth={false}
      //             dropdownRender={(el: any) => {
      //               return (
      //                 <div>
      //                   {el}
      //                   <Divider style={{ margin: '4px 0' }} />
      //                   <div
      //                     style={{ padding: '4px 8px', cursor: 'pointer' }}
      //                     onMouseDown={(e) => e.preventDefault()}
      //                     onClick={(e) => this.openAddNewCOOModal(record)}
      //                   >
      //                     <Icon type="plus" /> Add New
      //                   </div>
      //                 </div>
      //               )
      //             }}
      //           >
      //             <Select.Option value="" />
      //             {extraCOO.length &&
      //               extraCOO.map((p, i) => {
      //                 return (
      //                   <Select.Option value={p.id} key={`${p.name}_${i}`}>
      //                     {p.name}
      //                   </Select.Option>
      //                 )
      //               })}
      //           </ThemeSelect>
      //         </SelectWrapper>
      //       )
      //     }
      //     return extraOrigin ? extraOrigin : ''
      //   },
      // },
      {
        title: 'LOT SPECS',
        dataIndex: 'lot_specification',
        className: 'th-left',
        key: 'lot_specification',
        width: 200,
        render: (value: any, record: any) => {
          const lotSpec = `${_.get(record, 'modifiers', '')}${record.modifiers ? ',' : ''}
          ${_.get(record, 'extraOrigin', '')}${record.extraOrigin ? ',' : ''}
          ${(isFloat(record.grossWeight) ? formatNumber(record.grossWeight, 2) : record.grossWeight) || 0}
          ${_.get(record, 'grossWeightUnit', getDefaultUnit('WEIGHT', sellerSetting))},

          ${(isFloat(record.grossVolume) ? formatNumber(record.grossVolume, 2) : record.grossVolume) || 0}
          ${_.get(record, 'grossVolumeUnit', getDefaultUnit('VOLUME', sellerSetting))}`
          if (isLocked || orderStatus == 'CANCEL') {
            return lotSpec
          } else {
            return (
              <div className="product-name" style={{ textDecoration: 'none' }}>
                {lotSpec}
                <Icon
                  style={{ marginLeft: 8 }}
                  type="setting"
                  onClick={(evt: any) => this.onShowLotSpecificationModal(record)}
                />
              </div>
            )
          }
        },
      },
      {
        title: 'UNITS ORDERED',
        dataIndex: 'quantity',
        className: 'th-left',
        key: 'quantity',
        width: 70,
        render: (value: number, record: any, index: number) => {
          if (isLocked || orderStatus == 'CANCEL') {
            return isFloat(value) ? formatNumber(value, 2) : value
          } else {
            return (
              <ThemeInput
                id={`UNITSORDERED${index}`}
                value={value}
                style={{ marginRight: 4, width: 70, textAlign: 'right' }}
                onChange={(evt: any) => {
                  this.changeQty(evt.target.value.match(/^\d*(\.?\d{0,2})/g)[0], record.wholesaleOrderItemId, 'quantity')
                  this.setState({ addItemId: false })
                  this.props.changeAddItem()
                }}
                onBlur={(evt: any) => this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'quantity')}
              />
            )
          }
        },
      },
      // {
      //   title: 'INVENTORY UOM',
      //   dataIndex: 'inventoryUOM',
      //   key: 'inventoryUOM',
      //   // editable: true,
      //   width: 140,
      //   edit_width: 140,
      // },
      {
        title: 'UOM (UNITS ORDERED)',
        className: 'th-left',
        dataIndex: 'inventoryUOM',
        key: 'inventoryUOM',
        // editable: true,
        width: 140,
        render: (value: any, record: any) => {
          if (!isLocked && orderStatus != 'CANCEL') {
            record.UOM = record.overrideUOM ? record.overrideUOM : record.UOM
            const disabled = record.hasTransaction > 0 ? true : false
            return (
              <SelectWrapper>
                <UomSelect
                  handlerChangeUom={this.onChangeUom.bind(this)}
                  orderItem={record}
                  type={2}
                  disabled={disabled}
                />
              </SelectWrapper>
            )
          }
          return record.overrideUOM ? record.overrideUOM : value
        },
      },
      {
        title: 'COST',
        dataIndex: 'cost',
        className: 'th-left',
        key: 'cost',
        edit_width: 80,
        width: 115,
        render: (value: any, record: any) => {
          let cost = record.cost
          if (!isLocked && orderStatus != 'CANCEL') {
            return (
              <ThemeInputNumber
                value={cost}
                // onChange={this.onChangeCost.bind(this, record.wholesaleOrderItemId)}
                onBlur={(e) => this.openItemCostModal(record.wholesaleOrderItemId, record, e.target.value)}
                formatter={(value) => `${value ? '$' + value.match(/^\d*(\.?\d{0,2})/g)[0] : ''}`}
                disabled={!isLocked && orderStatus != 'CANCEL' ? false : true}
              />
            )
          }
          return `$${formatNumber(cost, 2)}`
        },
      },
      // {
      //   title: 'PRICING UOM',
      //   dataIndex: 'baseUOM',
      //   key: 'baseUOM',
      //   width: 115,
      // },
      {
        title: 'UOM (COST)',
        dataIndex: 'baseUOM',
        className: 'th-left',
        key: 'baseUOM',
        width: 115,
        render: (value: any, record: any, index: number) => {
          let comp: any = null
          if (!isLocked && orderStatus != 'CANCEL') {
            comp = (
              <ThemeSelect
                value={record.pricingUOM}
                suffixIcon={<Icon type="caret-down" />}
                onChange={(val: any) => this.onChangePricingUom(record.wholesaleOrderItemId, val, index)}
                style={{ minWidth: 78 }}
                disabled={!isLocked && orderStatus != 'CANCEL' ? false : true}
              >
                {/* {record.baseUOM != record.inventoryUOM && (
                  <Select.Option value={record.baseUOM}>{record.baseUOM}</Select.Option>
                )} */}
                <Select.Option value={record.inventoryUOM}>{record.inventoryUOM}</Select.Option>
                {record.wholesaleProductUomList != null && record.wholesaleProductUomList.length > 0
                  ? record.wholesaleProductUomList.map((uom) => {
                      if (!uom.deletedAt && uom.useForPurchasing) {
                        return (
                          <Select.Option key={uom.name} value={uom.name} title={uom.name}>
                            {uom.name}
                          </Select.Option>
                        )
                      }
                    })
                  : ''}
              </ThemeSelect>
            )
          } else {
            comp = <span>{record.pricingUOM}</span>
          }
          return (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {comp}
              <Tooltip title="View Cost History">
                <ThemeIcon
                  onClick={() => this.openItemHistoryModal(record)}
                  type="bar-chart"
                  style={{ fontSize: 20, marginLeft: 8 }}
                />
              </Tooltip>
            </div>
          )
        },
      },
      {
        title: <Tooltip title="Priced after sale">PAS</Tooltip>,
        dataIndex: 'pas',
        className: 'th-left',
        width: 50,
        render: (pas: boolean, record: any) => {
          return (
            <ThemeCheckbox
              className="pas"
              checked={pas}
              onChange={(e) => this.onCheckPas(record, e)}
              disabled={isLocked || orderStatus == 'CANCEL'}
            />
          )
        },
      },
      {
        title: 'EXTENDED COST',
        key: 'extendedCost',
        className: 'th-right',
        width: 50,
        align: 'right',
        render: (record: any) => {
          const constantRatio = judgeConstantRatio(record)
          const baseCost = ratioPriceToBasePrice(record.pricingUOM, record.cost, record, 12)
          // const baseQuantity = ratioQtyToBaseQty(record.overrideUOM || record.UOM, record.quantity, record, 12)
          if (!constantRatio) {
            return <span>${formatNumber(Math.round(baseCost * record.orderWeight * 100) / 100, 2)}</span>
          } else {
            return <span>${formatNumber(Math.round(baseCost * record.originQuantity * 100) / 100, 2)}</span>
          }
        },
      },
      // {
      //   title: 'FREIGHT COST/UNIT',
      //   dataIndex: 'freight',
      //   key: 'freight',
      //   editable: true,
      //   edit_width: 80,
      //   width: 105,
      // },
      // {
      //   title: 'SALES MARKUP %',
      //   dataIndex: 'margin',
      //   key: 'margin',
      //   editable: true,
      //   width: 85,
      //   edit_width: 60,
      //   render: (value: number, record: any) => {
      //     let element: any = <></>
      //     if(Number(record.cost) != 0) {
      //       if(value != null)
      //       element = `${value}%`
      //     } else {
      //       element = <Tooltip
      //                   title="Not available until cost/unit is provided"
      //                   placement="top"
      //                   overlayClassName="error-tooltip"
      //                 >
      //                 <span>{value == null ? '' : `${value}%`}</span>
      //               </Tooltip>

      //     }
      //     return element
      //   }
      // },
      // {
      //   title: 'SALES PRICE',
      //   dataIndex: 'salesPrice',
      //   key: 'sale_price',
      //   editable: true,
      //   edit_width: 85,
      //   width: 120,
      //   render: (value: number, record: any) => {
      //     let element: any = value
      //     if(Number(record.cost) == 0) {
      //       element = <Tooltip
      //                   title="Not available until cost/unit is provided"
      //                   placement="top"
      //                   overlayClassName="error-tooltip"
      //                 >
      //                 <span>{value}</span>
      //               </Tooltip>

      //     }
      //     return element
      //   }
      // },
      {
        title: 'UNITS CONFIRMED',
        dataIndex: 'qtyConfirmed',
        className: 'th-left',
        key: 'qtyConfirmed',
        width: 70,
        render: (value: number, record: any) => {
          // if ((record.status == 'NEW' || record.status == 'PLACED') && record.qtyConfirmed == 0) {
          //   return ''
          // } else {
          //   return value
          // }
          if (
            (record.status == 'NEW' || record.status == 'PLACED' || record.status == 'RECEIVED') &&
            record.qtyConfirmed == 0
          ) {
            value = ''
          }
          if (isLocked || orderStatus == 'CANCEL') {
            return isFloat(value) ? formatNumber(value, 2) : value
          } else {
            if (record.hasTransaction > 0) {
              return (
                <Tooltip
                  placement="top"
                  title={
                    'This lot has been included in one or more sales orders. The confirmed quantity cannot be modified.'
                  }
                >
                  <div>
                    <ThemeInput
                      value={value}
                      style={{ marginRight: 4, width: 70, textAlign: 'right', pointerEvents: 'none' }}
                      onChange={(evt: any) => {
                        this.changeQty(evt.target.value.match(/^\d*(\.?\d{0,2})/g)[0], record.wholesaleOrderItemId, 'qtyConfirmed')
                      }}
                      onBlur={(evt: any) =>
                        this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'qtyConfirmed')
                      }
                      disabled={true}
                    />
                  </div>
                </Tooltip>
              )
            } else {
              return (
                <ThemeInput
                  value={value}
                  style={{ marginRight: 4, width: 70, textAlign: 'right' }}
                  onChange={(evt: any) => {
                    this.changeQty(evt.target.value.match(/^\d*(\.?\d{0,2})/g)[0], record.wholesaleOrderItemId, 'qtyConfirmed')
                  }}
                  onBlur={(evt: any) =>
                    this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'qtyConfirmed')
                  }
                />
              )
            }
          }
        },
      },
      // {
      //   title: 'CARRIER',
      //   dataIndex: 'carrier',
      //   key: 'carrier',
      //   editable: true,
      //   width: 95,
      //   edit_width: 70,
      // },
      {
        title: 'NOTES',
        dataIndex: 'note',
        key: 'note',
        width: 80,
        render: (notes: string, record: OrderItem, index: number) => (
          <Popover
            placement="right"
            content={
              <NotesModal
                isLocked={isLocked}
                record={record}
                setNewNote={this.setNewNote.bind(this)}
                index={index}
                orderStatus={orderStatus}
              />
            }
            trigger="click"
            onVisibleChange={this.onNotesVisbleChange.bind(this, record, index)}
          >
            <FlexCenter>
              {
                <ThemeIcon
                  type="file-text"
                  style={record.note ? noteIcon : noteGreyIcon}
                  // style={record.permanent_note || record.temporary_note ? noteIcon : noteGreyIcon}
                />
              }
            </FlexCenter>
          </Popover>
        ),
      },
      {
        title: 'STATUS',
        dataIndex: 'status',
        key: 'status',
        width: 110,
        render: (status: string, record: OrderItem) => {
          return (
            <div className="relative">
              {status}
              {(isLocked || orderStatus == 'CANCEL') ? null : (
                <Dropdown overlay={this.renderFloatMenuItems(record)} placement="bottomRight" trigger={['click']}>
                  <FloatingMenu className="floating-menu-body">
                    <IconSVG type="ellipsis" viewBox="0 0 16 4" width={16} height={8} />
                  </FloatingMenu>
                </Dropdown>
              )}
            </div>
          )
        },
      },
      {
        title: '',
        key: 'delete',
        dataIndex: 'delete',
        render: (status: string, record: any, index: any) => {
          if (isLocked || record.hasTransaction > 0 || orderStatus == 'CANCEL') {
            return
          }

          return (
            <Popconfirm
              title="Permanently delete this one related record?"
              okText="Delete"
              onConfirm={this.onDeleteRow.bind(this, record.wholesaleOrderItemId)}
            >
              <FlexCenter style={{ cursor: 'pointer' }}>
                <ThemeIcon type="close" style={{ fontWeight: 'bold' }} />
              </FlexCenter>
            </Popconfirm>
          )
        },
      },
    ]

    if (this.state.addItemId == true) {
      const ele = document.getElementById(`UNITSORDERED${filteredItems.length - 1}`)
      if (ele) {
        ele.focus()

        // this.setState({ addItemId: false })
        //this.props.changeAddItem()
      }
    }

    let allProps = { ...this.props }
    allProps['TypeEditWrapper'] = TypeEditWrapper

    return (
      <div className="purchase-order-overview-table">
        {/* <ConfigProvider renderEmpty={() => null}> */}
        <EditableTable
          columns={columns}
          dataSource={filteredItems}
          handleSave={this.handleSave}
          handleSelectProduct={this.handleSelectProduct}
          rowKey="wholesaleOrderItemId"
          tableLoading={this.props.tableLoading}
          allProps={allProps}
          footer={() => {
            if (isLocked || orderStatus == 'CANCEL') {
              return null
            }
            return (
              <SearchFooter
                onSearch={this.props.onSearch}
                onSelect={this.props.onSelect}
                currentProducts={this.props.currentProducts}
                handleAddOrderItem={this.props.handleAddOrderItem}
              />
            )
          }}
        />
        {/* </ConfigProvider> */}
        <EnterRepackModal
          isCreating={true}
          sellerSetting={sellerSetting}
          selectedItem={repackOrderItem}
          visible={repackModalVisible}
          onClose={this.onCloseRepackModal}
        />

        <OverviewModal
          orderId={orderId}
          items={filteredItems}
          showModal={overviewModalVisible}
          onCloseRequest={this.toggleOverviewModal}
        />
        <ItemHistoryModal
          selectedItem={this.state.selectedItem}
          showModal={this.state.itemHistoryModal}
          onClose={this.closeItemHistoryModal}
          getPurchaseOrderItemHistory={this.props.getPurchaseOrderItemHistory}
          itemHistory={this.props.itemHistory}
          sellerSetting={this.props.sellerSetting}
        />
        <ItemCostModal
          newCost={this.state.newCost}
          selectedItem={this.state.selectedItem}
          getPurchaseOrderItemCost={this.props.getPurchaseOrderItemCost}
          showModal={this.state.itemCostModal}
          onClose={this.closeItemCostModal}
          onSave={this.updateOrderItemCost}
          itemCost={this.props.itemCost}
        />
        {this.state.addingBrandItemId && (
          <AddNewBrandModal
            visible={this.state.openAddNewBrandModal}
            itemId={this.state.addingBrandItemId}
            itemName={this.state.brandingItemName}
            oldBrands={this.state.oldBrands}
            // currentSuppliers={this.state.currentSuppliers}
            companyProductTypes={this.props.companyProductTypes}
            onVisibleChange={this.openAddNewBrandModal}
            onChangeBrandList={this.onChangeBrandList}
            updateDatasource={this.updateItemSupplier}
            updateProduct={this.props.updateProduct}
            setCompanyProductTypes={this.props.setCompanyProductType}
          />
        )}
        <ThemeModal
          title={`Edit Value List "Origin"`}
          visible={this.state.visibleCOOModal}
          onCancel={this.openAddNewCOOModal}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="extraCOO" title="Origin" buttonTitle="Add Origin" />
        </ThemeModal>
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              title={`Lot Specification`}
              visible={visibleLotSpecificationModal}
              onCancel={this.onShowLotSpecificationModal}
              okText={'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={this.handleSaveLotInfo}>
                    Save
                  </ThemeButton>
                </DialogSubContainer>
              }
              width={600}
            >
              <LotSpecificationModal
                ref={this.lotSpecModalRef}
                handleSave={this.handleSave}
                record={selectedItem}
                extraCOO={extraCOO}
                openAddNewCOOModal={this.openAddNewCOOModal}
                openAddNewBrandModal={this.openAddNewBrandModal}
              />
            </ThemeModal>
          )}
        </ClassNames>
      </div>
    )
  }
}

export default TableView
