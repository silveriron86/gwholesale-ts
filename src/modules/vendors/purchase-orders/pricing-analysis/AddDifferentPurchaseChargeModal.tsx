import React, { useEffect, useState } from 'react'
import { Col, Row, Input, Tooltip, Icon, Button } from 'antd'
import { ThemeModal, ThemeTable } from '~/modules/customers/customers.style'
import { OrderService } from '~/modules/orders/order.service'
import { checkError, formatNumber, responseHandler } from '~/common/utils'
import { of } from 'rxjs'
import styled from '@emotion/styled'
import _ from 'lodash'

const { Search } = Input

interface Iprops {
  onCancel: () => void
  handleAddCharge: (items: any[]) => void
  orderId: number
  selectedKeys: any
}

const AddDifferentPurchaseChargeModal: React.FC<Iprops> = ({ onCancel, handleAddCharge, orderId, selectedKeys }) => {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState({})
  const [total, setTotal] = useState(0)
  const [selectedRows, setSelectedRows] = useState<any[]>(selectedKeys)
  const [expandedRowKeys, setExpandedRowKeys] = useState<any[]>([])
  const [query, setQuery] = useState({
    orderId,
    currentPage: 0,
    pageSize: 10,
    searchKey: '',
    greaterThenZero: 1,
  })

  const fetchData = () => {
    setLoading(true)
    OrderService.instance.getAllextraPOList(query).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        const concatData = res.body.data.dataList.map((v) => {
          if (_.some(selectedRows, ['wholesaleOrderId', v.wholesaleOrderId])) {
            return { ...v, price: _.find(selectedRows, ['wholesaleOrderId', v.wholesaleOrderId]).price }
          }
          return v
        })
        setData(_.keyBy(concatData, 'wholesaleOrderId'))

        setTotal(res.body.data.total)
      },
      error(err) {
        checkError(err)
      },
      complete() {
        setLoading(false)
      },
    })

    // OrderService.instance.getAllExtraCharge(query).subscribe({
    //   next(res: any) {
    //     of(responseHandler(res, false).body.data)
    //     setData({
    //       ..._.keyBy(res.body.data.data, 'wholesaleOrderId'),
    //       ..._.keyBy(
    //         _.intersectionBy(selectedRows, res.body.data.data, 'wholesaleOrderId'),
    //         'wholesaleOrderId',
    //       ),
    //     })
    //     setTotal(res.body.data.total)
    //   },
    //   error(err) {
    //     checkError(err)
    //   },
    //   complete() {
    //     setLoading(false)
    //   },
    // })
  }

  useEffect(() => {
    fetchData()
  }, [query])

  const handleChangeAmount = (item: any, price: any) => {
    if (_.toNumber(price) > item.totalCost) {
      setExpandedRowKeys([...expandedRowKeys, item.wholesaleOrderId])
    } else {
      setExpandedRowKeys(expandedRowKeys.filter((k) => k !== item.wholesaleOrderId))
    }
    setData({ ...data, [item.wholesaleOrderId]: { ...data[item.wholesaleOrderId], price } })
    if (selectedRows.some((s) => s.wholesaleOrderId === item.wholesaleOrderId)) {
      setSelectedRows(
        selectedRows.map((v) => {
          if (v.wholesaleOrderId === item.wholesaleOrderId) {
            return { ...v, chargeFrom: item.wholesaleOrderId, price }
          }
          return v
        }),
      )
    }
  }

  const handleSave = () => {
    handleAddCharge(selectedRows)
    onCancel()
  }

  const columns = [
    {
      title: 'ORDER NO.',
      align: 'left',
      dataIndex: 'wholesaleOrderId',
    },
    {
      title: 'VENDOR',
      align: 'left',
      dataIndex: 'vendorName',
    },
    {
      title: 'DELIVERY DATE',
      align: 'left',
      dataIndex: 'deliveryDate',
    },
    {
      title: 'STATUS',
      align: 'left',
      dataIndex: 'isLocked',
      render: (t: any) => <Status isLocked={t}>{t ? 'Closed' : 'Open'}</Status>,
    },
    {
      title: 'CHARGE TOTAL',
      align: 'right',
      dataIndex: 'totalCost',
      render: (t: number) => `$${formatNumber(t, 2)}`,
    },
    {
      title: (
        <div>
          AMOUNT TO ADD
          <Tooltip placement="top" title="You can choose to allocate a portion of this charge, or all of it">
            <Icon type="info-circle" style={{ marginLeft: 10, color: 'rgb(86 131 96)' }} />
          </Tooltip>
        </div>
      ),
      align: 'right',
      dataIndex: 'price',
      render: (t: string | number | readonly string[] | undefined, r: { totalCost: string | undefined }) => {
        return (
          <Input
            value={t}
            placeholder={r.totalCost}
            prefix={'$'}
            style={{ textAlign: 'right', width: 100 }}
            onChange={(e) => handleChangeAmount(r, e.target.value.replace(/^\D*(\d*(?:\.\d{0,2})?).*$/g, '$1'))}
          />
        )
      },
    },
  ]

  const rowSelection = {
    onSelect: (record: any, selected: boolean) => {
      if (selected) {
        setSelectedRows([...selectedRows, { ...record, price: record.totalCost, chargeFrom: record.wholesaleOrderId }])
        setData({
          ...data,
          [record.wholesaleOrderId]: {
            ...data[record.wholesaleOrderId],
            price: record.totalCost,
          },
        })
      } else {
        setSelectedRows(selectedRows.filter((v) => v.wholesaleOrderId !== record.wholesaleOrderId))
        setData({ ...data, [record.wholesaleOrderId]: { ...data[record.wholesaleOrderId], price: '' } })
      }
    },
    onSelectAll: (selected: any, selectedRow: any, changeRows: _.List<any> | null | undefined) => {
      if (selected) {
        setSelectedRows(_.uniqBy([...selectedRows, ...selectedRow], 'wholesaleOrderId'))
      } else {
        setSelectedRows(_.xorBy(selectedRows, changeRows, 'wholesaleOrderId'))
      }
    },
    selectedRowKeys: selectedRows.map((v: any) => v.wholesaleOrderId),
  }

  return (
    <Modal
      title="Add charge from different purchase"
      width={1080}
      visible
      onCancel={onCancel}
      footer={[
        <span onClick={onCancel} style={{ cursor: 'pointer' }} key="1">
          Back
        </span>,
        <Button type="primary" disabled={!!expandedRowKeys.length} onClick={handleSave} key="2">
          Add charge
        </Button>,
      ]}
    >
      <Row type="flex" justify="space-between" align="middle">
        <Col>Select at least one charge from your other purchases</Col>
        <Col>
          <Search
            placeholder="Search purchases..."
            onSearch={(searchKey) => setQuery({ ...query, searchKey, currentPage: 0 })}
            style={{ width: 200 }}
          />
        </Col>
      </Row>
      <Table
        dataSource={_.reverse(Object.values(data))}
        loading={loading}
        columns={columns}
        bordered={false}
        expandedRowKeys={expandedRowKeys}
        expandIconColumnIndex={-1}
        expandIconAsCell={false}
        rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}
        expandedRowRender={(record: any) => (
          <p style={{ margin: 0, textAlign: 'right', color: '#ff0000', fontWeight: 500 }}>
            <Icon type="warning" theme="filled" style={{ marginRight: '5px' }} />
            Amount to add must be under or equal to ${record.totalCost}
          </p>
        )}
        pagination={{
          total,
          current: query.currentPage + 1,
          pageSize: query.pageSize,
          onChange: (current) => setQuery({ ...query, currentPage: current - 1 }),
        }}
        rowKey="wholesaleOrderId"
      />
    </Modal>
  )
}

const Status = styled.div<{ isLocked: boolean }>`
  border: 1px solid #ccc;
  border-radius: 15px;
  text-align: center;
  position: relative;
  padding: 4px;
  /* ::after {
    position: absolute;
    content: '';
    left: 8px;
    top: 12px;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    background: ${(props) => (props.isLocked ? '#1c6e31' : '#82898a')};
  } */
`
const Modal = styled(ThemeModal)`
  .ant-table-selection {
    display: none;
  }
  .ant-modal-footer {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 14px 22px;
    background: #f7f7f7;
    color: #418553;
    border-top: 1px solid #d7d7d7;
  }
`

const Table = styled(ThemeTable)`
  .ant-table-row-expand-icon-cell {
    padding: 0;
    width: 0;
  }
  .ant-table-pagination {
    width: 100%;
    text-align: center;
    float: none;
    margin: 34px 0 10px 0;
  }
  .ant-table-thead > tr > th {
    border-bottom: 3px solid #d7d7d7;
    padding: 16px 9px;
  }
  .ant-table-tbody > tr > td {
    background: #fff !important;
    border-bottom: 1px solid #d7d7d7;
  }
`

export default AddDifferentPurchaseChargeModal
