import React, { FC, useEffect, useState } from 'react'
import styled from '@emotion/styled'
import { ThemeModal, ThemeTable } from '~/modules/customers/customers.style'
import { Icon, Input, Radio, Spin, Tooltip } from 'antd'
import { OrderService } from '~/modules/orders/order.service'
import { of } from 'rxjs'
import { checkError, formatDate, formatNumber, getCostPre, responseHandler } from '~/common/utils'
import AddDifferentPurchaseChargeModal from './AddDifferentPurchaseChargeModal'
import _ from 'lodash'
import { Link } from 'react-router-dom'
import { ALLOCATION_TYPE } from '~/schema'

interface IProps {
  onCancel: () => void
  orderId: any
  currentOrder: any
  currentOrderItems: any
  updatePOItemAllocate: any
  oneOffItems: any
  relatedBills: any
  getAlloationData: any
}

const AllocateExtraCharges: FC<IProps> = ({
  onCancel,
  orderId,
  currentOrder,
  currentOrderItems,
  updatePOItemAllocate,
  oneOffItems,
  getAlloationData,
  relatedBills,
}) => {
  const [allocationMethod, setAllocationMethod] = useState(currentOrder.allocateType)
  const [loading, setLoading] = useState(false)
  const [saveLoading, setSaveLoading] = useState(false)
  const [currentExtraCharge, setCurrentExtraCharge] = useState<any[]>([])
  const [selectedRows, setSelectedRows] = useState<any[]>([])
  const [totalChargesPrice, setTotalChargePrice] = useState(0)
  const [totalAllocation, setTotalAllocation] = useState(0)
  const [totalReceivedQty, setTotalReceivedQty] = useState(0)
  const [totalPalletUnits, setPalletUnits] = useState(0)

  const [orderItems, setOrderItems] = useState(currentOrderItems.map((oi: any) => {
    return {
      ...oi,
      totalAllocated: oi.totalAllocateCharge
    }
  }))
  const [isHasPallets, setIsHasPallets] = useState(false)

  const [addDifferentPurchaseChargeModalVisible, setAddDifferentPurchaseChargeModalVisible] = useState(false)

  const fetchData = () => {
    setLoading(true)

    OrderService.instance.getPOAllocate(orderId).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        if (!res.body.data || !res.body.data.length) {
          OrderService.instance.getOrderOneOffItemsById(orderId).subscribe({
            next(resp: any) {
              of(responseHandler(resp, false).body.data)
              setCurrentExtraCharge(
                resp.body.data.map((v: any) => {
                  return ({
                    price: (Number(v.price || 0) + Number(v.freight || 0)) * v.quantity,
                    wholesaleOrderId: orderId,
                    wholesaleOrderItemId: v.wholesaleOrderItemId,
                    itemName: v.itemName || v.vendorName,
                    vendorName: v.vendorName,
                    description: v.chargeDesc,
                  })
                }),
              )
            },
            error(err) {
              checkError(err)
            },
            complete() {
              setLoading(false)
            },
          })
        } else {
          const selectRows: any = []
          const currentExtracharges: any = []

          res.body.data.forEach((v: { isChecked: any; chargeFrom: any; wholesaleOrderItemId: any }) => {
            // For charges from another PO, they are assumed as checked
            if (v.chargeFrom) {
            // if (v.isChecked) {
              selectRows.push({
                ...v,
                wholesaleOrderId: v.chargeFrom,
                wholesaleOrderItemId: v.chargeFrom || v.wholesaleOrderItemId,
              })
            // }
            }
            currentExtracharges.push({
              ...v,
              wholesaleOrderId: v.chargeFrom,
              wholesaleOrderItemId: v.chargeFrom || v.wholesaleOrderItemId,
            })
          })
          const oneOffItemData = _.xorBy(oneOffItems, currentExtracharges, 'wholesaleOrderItemId').filter((v) => v.status)
          const relatedBillData = _.xorBy(relatedBills, currentExtracharges, 'wholesaleOrderItemId').filter((v) => v.status)
          const data = [...oneOffItemData,...relatedBillData]
          const lastExtraChargesIndex = _.findLastIndex(currentExtracharges, (v) => !v.chargeFrom)

          currentExtracharges.splice(
            lastExtraChargesIndex + 1,
            0,
            data.map((v: any) => ({
              price: (Number(v.price || 0) + Number(v.freight || 0)) * v.quantity,
              wholesaleOrderId: orderId,
              wholesaleOrderItemId: v.wholesaleOrderItemId,
              itemName: v.itemName || v.vendorName,
              vendorName: v.vendorName,
            })),
          )

          setSelectedRows(selectRows)
          setCurrentExtraCharge(_.flatten(currentExtracharges))
          setLoading(false)
        }

      },
      error(err) {
        checkError(err)
      },
    })
  }

  const fetchPallet = () => {
    OrderService.instance.getPOPallets(orderId).subscribe({
      next(resp: any) {
        of(responseHandler(resp, false).body.data)
        setIsHasPallets(resp.body.data.length > 0)
        setPalletUnits(_.get(resp, 'body.data', []).length)

        const palletGroupByOrder = _.groupBy(resp.body.data, 'wholesaleOrderItemId')
        setOrderItems(
          orderItems.map((item: { totalAllocateCharge: any; wholesaleOrderItemId: any }) => ({
            ...item,
            totalAllocateCharge: currentOrder.allocateType === ALLOCATION_TYPE.MANUALLY ? item.totalAllocateCharge : '',
            palletUnits: _.get(palletGroupByOrder, `${item.wholesaleOrderItemId}`, []).length,
          })),
        )
      },
    })
  }

  useEffect(() => {
    fetchPallet()
  }, [])

  useEffect(() => {
    setTotalReceivedQty(currentOrderItems.reduce((n: any, p: { status: string, receivedQty: number, qtyConfirmed: number }) => {
      const getUnit = () => {
        if (p.status === 'PLACED') {
          return 0
        } else if (p.status === 'CONFIRMED') {
          return p.qtyConfirmed
        } else if (p.status === 'RECEIVED') {
          return p.receivedQty
        }
      }
      return n + getUnit()
    }, 0))
    setOrderItems(currentOrderItems)
  }, [currentOrderItems])

  useEffect(() => {
    setTotalChargePrice(selectedRows.reduce((p: number, n: any) => p + n.price, 0))
  }, [selectedRows])

  useEffect(() => {
    fetchData()
  }, [])

  useEffect(() => {
    setTotalAllocation(
      orderItems.reduce((n: any, p: any) => _.add(n, _.toNumber(_.get(p, 'totalAllocateCharge', 0))), 0),
    )
  }, [orderItems])

  const handleChangeAllocation = (record: { wholesaleOrderItemId: any }, value: string) => {
    setOrderItems(
      orderItems.map((v: { wholesaleOrderItemId: any }) => {
        if (v.wholesaleOrderItemId === record.wholesaleOrderItemId) {
          return { ...v, totalAllocateCharge: value }
        }
        return v
      }),
    )
  }

  const columns = [
    {
      title: 'COST',
      align: 'left',
      dataIndex: 'itemName',
      render: (t: any, r: { chargeFrom: any; vendorName: any; deliveryDate: any, description: string,relatedBillVendorName: string }) => {
        if (r.chargeFrom) {
          return  (
            <span
              style={{ color: '#1C6E31', cursor: 'pointer' }}
              onClick={() => setAddDifferentPurchaseChargeModalVisible(true)}>
              {`${r.chargeFrom}: ${r.vendorName} (${formatDate(r.deliveryDate)})`}
            </span>
          )
        }
        if(r.description) {
          return `${t} (${r.description})`
        }
        if(r.relatedBillVendorName){
          return `${r.relatedBillVendorName}`
        }
        return t
      },
    },
    {
      title: 'AMOUNT',
      align: 'left',
      dataIndex: 'price',
      render: (t: any) => `$${formatNumber(t, 2)}`,
    },
  ]

  const costPreColumns = [
    {
      title: 'ITEM',
      align: 'left',
      dataIndex: 'variety',
    },
    {
      title: 'ALLOCATION',
      align: 'left',
      key: 'allocation',
      dataIndex: 'totalAllocateCharge',
      className: allocationMethod === ALLOCATION_TYPE.MANUALLY ? '' : 'display',
      render: (t: string | number | readonly string[] | undefined, r: any) => (
        <Input
          value={t}
          prefix={'$'}
          style={{ textAlign: 'right', width: 100 }}
          onChange={(e) => handleChangeAllocation(r, e.target.value.replace(/^\D*(\d*(?:\.\d{0,2})?).*$/g, '$1'))}
        />
      ),
    },
    {
      align: 'left',
      title: 'UNITS',
      dataIndex: 'status',
      render: (status: string, r: any) => {
        return getUnits(r)
      },
    },
    {
      align: 'right',
      title: 'ALLOCATED COSTS/UNIT',
      render: (r: { receivedQty: number; palletUnits: number; totalAllocateCharge: any }) =>
        `$${formatNumber(getCostPre(r, allocationMethod, totalChargesPrice, totalReceivedQty, totalPalletUnits), 2)}`,
    },
    {
      align: 'right',
      title: 'TOTAL ALLOCATED COST',
      dataIndex: 'totalAllocated',
      key: 'totalAllocated',
      render: (totalAllocated: number, r: any) => {
        return `$${formatNumber(_.multiply(getCostPre(r, allocationMethod, totalChargesPrice, totalReceivedQty, totalPalletUnits), getUnits(r)), 2)}`
      }
    }
  ]

  const getUnits = (r: any) => {
    const { status } = r
    
    if (status === 'PLACED') {
      // 0 if item is in Placed Status
      return 0
    } else if (status === 'CONFIRMED') {
      // Confirmed Qty if item is in Confirmed status
      return r.qtyConfirmed
    } else if (status === 'RECEIVED') {
      // Received Qty if item is in Received status
      return r.receivedQty
    }
    
    if (allocationMethod === ALLOCATION_TYPE.BILLABLE || allocationMethod === ALLOCATION_TYPE.MANUALLY) {
      return r.receivedQty
    } else if (allocationMethod === ALLOCATION_TYPE.PALLETS) {
      return r.palletUnits
    }

    return 0
  }

  const radioStyle = {
    display: 'block',
    height: '34px',
    lineHeight: '30px',
  }

  const rowSelection = {
    onSelect: (record: any, selected: boolean) => {
      if (selected) {
        setSelectedRows([...selectedRows, record])
      } else {
        setSelectedRows(selectedRows.filter((v) => v.wholesaleOrderItemId !== record.wholesaleOrderItemId))
      }
    },
    selectedRowKeys: selectedRows.map((v: any) => v.wholesaleOrderItemId),
  }

  const handleAddCharge = (charges: any[]) => {
    const formatCharges = charges.map((v: any) => ({
      ...v,
      wholesaleOrderItemId: v.wholesaleOrderId,
      chargeFrom: v.wholesaleOrderId,
      price: _.toNumber(v.price || 0),
      itemName: `${v.wholesaleOrderId}: ${v.vendorName} (${v.deliveryDate})`,
    }))

    setSelectedRows(_.reverse(_.uniqBy(_.reverse([...selectedRows, ...formatCharges]), 'wholesaleOrderItemId')))
    setCurrentExtraCharge(
      _.reverse(_.uniqBy(_.reverse([...currentExtraCharge, ...formatCharges]), 'wholesaleOrderItemId')),
    )
  }

  const getTotalAllocateCharge = (item: { receivedQty: any; palletUnits: any; totalAllocateCharge: any }) => {
    if (allocationMethod === ALLOCATION_TYPE.BILLABLE) {
      return _.multiply(getUnits(item), getCostPre(item, allocationMethod, totalChargesPrice, totalReceivedQty, totalPalletUnits))
    } else if (allocationMethod === ALLOCATION_TYPE.PALLETS) {
      return _.multiply(getCostPre(item, allocationMethod, totalChargesPrice, totalReceivedQty, totalPalletUnits), getUnits(item))
    } else {
      return item.totalAllocateCharge
    }
  }

  const handleChangeAllocationMethod = (value: any) => {
    setAllocationMethod(value)
  }

  // const getCostPre = (item: { receivedQty: number; palletUnits: number; totalAllocateCharge: any }) => {
  //   if (allocationMethod === ALLOCATION_TYPE.BILLABLE) {
  //     if (!item.receivedQty) return 0
  //     return _.floor(_.divide(totalChargesPrice, totalReceivedQty), 12)
  //   } else if (allocationMethod === ALLOCATION_TYPE.PALLETS) {
  //     if (!item.palletUnits) return 0
  //     return _.floor(_.divide(totalChargesPrice, totalPalletUnits), 12)
  //   } else if (allocationMethod === ALLOCATION_TYPE.MANUALLY) {
  //     if (!item.totalAllocateCharge || !item.receivedQty) return 0
  //     return _.floor(_.divide(item.totalAllocateCharge, item.receivedQty), 12)
  //   }
  //   return 0
  // }

  const handleSave = () => {
    setSaveLoading(true)
    const wholesaleOrderItemList = orderItems.map((v: any) => ({
      wholesaleOrderItemId: v.wholesaleOrderItemId,
      totalAllocateCharge: getTotalAllocateCharge(v),
      perAllocateCost: getCostPre(v, allocationMethod, totalChargesPrice, totalReceivedQty, totalPalletUnits),
      updated: true,
    }))
    const data = {
      allocateType: allocationMethod,
      wholesaleAllocateChargeList: currentExtraCharge.map((v: any) => ({
        wholesaleOrderItemId: v.chargeFrom ? undefined : v.wholesaleOrderItemId,
        price: v.price,
        id: v.id || undefined,
        isChecked: selectedRows.some((s) => s.wholesaleOrderItemId === v.wholesaleOrderItemId) ? 1 : 0,
        chargeFrom: v.chargeFrom ? v.chargeFrom : undefined,
      })),
      wholesaleOrderItemList,
    }
    OrderService.instance.addPOAllocateExtraCharges({ data, orderId }).subscribe({
      next(res: any) {
        of(responseHandler(res, false).body.data)
        onCancel()
        updatePOItemAllocate({ wholesaleOrderItemList, allocateType: allocationMethod })
        getAlloationData(orderId)
      },
      error(err) {
        checkError(err)
      },
      complete() {
        setSaveLoading(false)
      },
    })
  }

  return (
    <AllocateExtraChargesModal
      visible
      onCancel={onCancel}
      onOk={handleSave}
      confirmLoading={saveLoading}
      title="Cost allocation settings"
      okButtonProps={{ disabled: _.gt(totalAllocation, totalChargesPrice) }}
      okText="Save allocation"
      width={800}
    >
      <Spin spinning={loading}>
        <AllocationMethodSection>
          <div className="label">Allocation method</div>
          <Radio.Group
            className="radiogroup"
            value={allocationMethod}
            onChange={(e) => handleChangeAllocationMethod(e.target.value)}
          >
            <Radio style={radioStyle} value={ALLOCATION_TYPE.BILLABLE}>
              Auto-allocate equally based on billable units
            </Radio>
            <Radio style={radioStyle} value={ALLOCATION_TYPE.PALLETS} disabled={!isHasPallets}>
              {isHasPallets ? (
                'Auto-allocate equally across pallets, then across billable units'
              ) : (
                <Tooltip placement="bottom" title="You must set pallet count in Receiving tab">
                  Auto-allocate equally across pallets, then across billable units
                </Tooltip>
              )}
            </Radio>
            <ManuallyRadio style={radioStyle} value={ALLOCATION_TYPE.MANUALLY}>
              Manually allocate
            </ManuallyRadio>
          </Radio.Group>
        </AllocationMethodSection>
        <ExtraChargesSection>
          {/* <div className="label">Select costs to allocate</div> */}
          <ThemeTable
            columns={columns}
            dataSource={currentExtraCharge}
            bordered={false}
            rowSelection={rowSelection}
            pagination={false}
            rowKey="wholesaleOrderItemId"
            rowClassName={(record, index) => {
              if (record.chargeFrom) {
                // hide checkbox
                return 'hide-checkbox'
              }
              return ''
            }}
          />
          {/* <p className="addCharge" onClick={() => setAddDifferentPurchaseChargeModalVisible(true)}>
            <Icon type="plus" />
            Add charge from different Purchase Order
          </p> */}
          <div className="totalCharge" style={{marginTop: 15}}>
            <p>Total costs to allocate</p>
            <span>${formatNumber(totalChargesPrice, 2)}</span>
          </div>
        </ExtraChargesSection>
        <AllocationPriceSection>
          <ThemeTable
            columns={costPreColumns}
            dataSource={orderItems}
            bordered={false}
            pagination={false}
            rowKey="wholesaleOrderItemId"
          />
        </AllocationPriceSection>
        {allocationMethod === ALLOCATION_TYPE.MANUALLY && (
          <TotalAllocated isWarning={_.gt(totalAllocation, totalChargesPrice)}>
            {_.gt(totalAllocation, totalChargesPrice) && (
              <Icon type="warning" theme="filled" style={{ marginRight: '5px' }} />
            )}
            <span>
              ${totalAllocation} of ${totalChargesPrice} allocated
            </span>
          </TotalAllocated>
        )}
      </Spin>
      {addDifferentPurchaseChargeModalVisible && (
        <AddDifferentPurchaseChargeModal
          onCancel={() => setAddDifferentPurchaseChargeModalVisible(false)}
          handleAddCharge={handleAddCharge}
          orderId={orderId}
          selectedKeys={selectedRows.filter((v) => v.chargeFrom).map((o) => ({ ...o, wholesaleOrderId: o.chargeFrom }))}
        />
      )}
    </AllocateExtraChargesModal>
  )
}

const AllocateExtraChargesModal = styled(ThemeModal)`
  .ant-modal-body {
    padding: 24px 0 0;
  }
  .ant-modal-footer {
    background: #f7f7f7;
    border-top: 1px solid #dbdbdb;
  }
`

const AllocationMethodSection = styled.div`
  padding: 0 24px;
  border-bottom: 1px solid rgb(216, 219, 219);
  .label {
    margin-bottom: 10px;
  }
  .ant-radio-group {
    margin-left: 20px;
  }
  .radiogroup {
    margin-bottom: 40px;
  }
`

const ExtraChargesSection = styled(AllocationMethodSection)`
  padding-top: 30px;
  padding-bottom: 20px;
  .ant-table-selection {
    display: none;
  }
  .addCharge {
    i {
      margin-right: 5px;
    }
    color: rgb(82, 189, 127);
    margin-top: 10px;
    cursor: pointer;
  }
  .totalCharge {
    text-align: right;
    p {
      margin-bottom: 0;
    }
    span {
      font-size: 30px;
    }
  }
  .ant-table-tbody {
    & > tr > td {
      padding-left: 16px !important;
      background: #fff !important;
    }
  }
`
const AllocationPriceSection = styled.div`
  background: rgb(247, 247, 247);
  padding-bottom: 40px;
  .display {
    display: none;
  }
  .ant-table-thead {
    & > tr > th {
      background: rgb(247, 247, 247) !important;
    }
  }
  .ant-table-tbody {
    & > tr > td {
      border-bottom: 1px solid #dbdbdb;
      padding-left: 16px !important;
    }
  }
  table {
    padding: 0 20px;
    background: rgb(247, 247, 247);
  }
`

const TotalAllocated = styled.div<{ isWarning: boolean }>`
  text-align: right;
  padding: 0px 20px 40px;
  background: #f7f7f7;
  color: ${(props) => (props.isWarning ? '#1c6e31' : '#595959')};
`
const ManuallyRadio = styled(Radio)`
  ::after {
    position: absolute;
    content: 'Manuall allocate charges among different items';
    left: 25px;
    top: 20px;
    color: rgb(136, 142, 143);
  }
`

export default AllocateExtraCharges
