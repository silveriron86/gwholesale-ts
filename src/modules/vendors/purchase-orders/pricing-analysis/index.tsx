import * as React from 'react'
import { Theme } from '~/common'
import { Icon as IconSvg } from '~/components/icon/'
import { ThemeButton, ThemeTable } from '~/modules/customers/customers.style'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { Flex, Unit } from '~/modules/customers/nav-sales/styles'
import { Icon, Tooltip, Popconfirm } from 'antd'

import { basePriceToRatioPrice, formatItemDescription, getAllowedEndpoint, priceToPrice } from '~/common/utils'
import { PricingAnalysisHeader, PurchaseOrderPricing } from '../_style'
import AllocateExtraCharges from './AllcateExtraChargesModal'
import { bignumber, formatBn } from '~/common/mathjs'
import InputNumber, { formatPercent, format$, parse$, parsePercent } from '~/components/Input/EnhanceInputNumber'
import styled from '@emotion/styled'

type Props = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
  }

const formatPrice = (orderItems: any) => {
  // 基于pricingUOM计算出下面属性的值
  return orderItems.map((orderItem: any) => {
    const pricingUOM = orderItem.pricingUOM
    const originalItemPrice = orderItem.itemPrice
    const originalItemCost = orderItem.itemCost
    const originalMargin =
      originalItemPrice === 0
        ? bignumber(0).toFixed(2)
        : bignumber(originalItemPrice)
            .sub(originalItemCost)
            .div(originalItemPrice)
            .mul(100)
            .toFixed(2)

    const cost = basePriceToRatioPrice(pricingUOM, orderItem.cost, orderItem, 12)
    const itemPrice = basePriceToRatioPrice(pricingUOM, orderItem.itemPrice, orderItem, 12)
    const itemCost = basePriceToRatioPrice(pricingUOM, orderItem.itemCost, orderItem, 12)
    const fromUOM = orderItem.overrideUOM || orderItem.inventoryUOM
    const perAllocateCost = priceToPrice(orderItem.perAllocateCost, fromUOM, pricingUOM, orderItem, 12)
    return {
      ...orderItem,
      cost,
      itemPrice,
      itemCost,
      perAllocateCost,
      originalItemPrice,
      originalItemCost,
      originalMargin,
    }
  })
}

type StateType = {
  changing: string | null
  changingValue: string
  currentOrderItems: any[]
  _preOrderItems: any[]
  allocateModalVisible: boolean
}

export default class PurchasePricingAnalysis extends React.PureComponent<Props, StateType> {
  state = {
    _preOrderItems: this.props.orderItems,
    currentOrderItems: formatPrice([...this.props.orderItems]),
    allocateModalVisible: false,
    changing: null,
    changingValue: '',
  }

  static getDerivedStateFromProps(nextProps: any, preState: any) {
    if (preState._preOrderItems === nextProps.orderItems) {
      return null
    }
    return { currentOrderItems: formatPrice(nextProps.orderItems), _preOrderItems: nextProps.orderItems }
  }

  componentDidMount() {
    const orderId = this.props.match.params.orderId
    if (orderId) {
      this.props.getOrderItemsById(orderId)
    }
    window.addEventListener('click', this.resetChanging)
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.resetChanging)
  }

  resetChanging = () => {
    this.setState({
      changing: null,
      changingValue: '',
    })
  }

  showWarn = (item: any) => (item.itemPrice || item.itemPrice === 0) && item.itemPrice < item.cost

  calMarkup = (orderItem: any) => {
    const { itemPrice, cost, perAllocateCost } = orderItem
    return bignumber(itemPrice)
      .sub(cost)
      .sub(perAllocateCost)
  }

  calMargin = (orderItem: any) => {
    if (orderItem.itemPrice == 0) return bignumber(0)
    return this.calMarkup(orderItem)
      .div(orderItem.itemPrice)
      .mul(100)
  }

  changePrice = (wholesaleOrderItemId: number, price: string) => {
    const { currentOrderItems } = this.state

    this.setState({
      currentOrderItems: currentOrderItems.map((el: any) => {
        if (el.wholesaleOrderItemId == wholesaleOrderItemId) {
          return { ...el, itemPrice: price, updated: true }
        }
        return el
      }),
    })
  }

  changeCost = (wholesaleOrderItemId: number, cost: string) => {
    const { currentOrderItems } = this.state

    this.setState({
      currentOrderItems: currentOrderItems.map((el: any) => {
        if (el.wholesaleOrderItemId == wholesaleOrderItemId) {
          return { ...el, itemCost: cost, updated: true }
        }
        return el
      }),
    })
  }

  changeMarkup = (wholesaleOrderItemId: number, markup: string) => {
    const { currentOrderItems } = this.state

    this.setState({
      changing: wholesaleOrderItemId + '-markup',
      changingValue: markup,
      currentOrderItems: currentOrderItems.map((el: any) => {
        if (el.wholesaleOrderItemId == wholesaleOrderItemId) {
          return {
            ...el,
            itemPrice: bignumber(markup)
              .add(el.cost)
              .add(el.perAllocateCost)
              .toNumber(),
            updated: true,
          }
        }
        return el
      }),
    })
  }

  changeMargin = (wholesaleOrderItemId: number, margin: string) => {
    const { currentOrderItems } = this.state
    if (+margin >= 100) {
      return
    }

    this.setState({
      changing: wholesaleOrderItemId + '-margin',
      changingValue: margin,
      currentOrderItems: currentOrderItems.map((el: any) => {
        if (el.wholesaleOrderItemId == wholesaleOrderItemId) {
          return {
            ...el,
            itemPrice: bignumber(el.cost)
              .add(el.perAllocateCost)
              .div(1 - +margin / 100)
              .toNumber(),
            updated: true,
          }
        }
        return el
      }),
    })
  }

  autoFillDefaultCosts = () => {
    const { currentOrderItems } = this.state

    this.setState({
      currentOrderItems: currentOrderItems.map((el: any) => {
        const newCost = bignumber(el.cost).add(el.perAllocateCost)
        const updated = !newCost.equals(el.itemCost)
        return { ...el, itemCost: newCost.toNumber(), updated }
      }),
    })
  }

  autoFillMargin = () => {
    const { currentOrderItems } = this.state

    currentOrderItems.forEach((el: any) => {
      const { originalMargin, wholesaleOrderItemId } = el
      const margin = this.calMargin(el).toDecimalPlaces(2)
      if (!margin.equals(originalMargin)) {
        this.changeMargin(wholesaleOrderItemId, originalMargin)
      }
    })
  }

  sharedColumns: any[] = [
    {
      title: '#',
      key: 'id',
      className: 'th-center',
      align: 'center',
      width: 30,
      render: (id: number, record: any, index: number) => {
        return index + 1
      },
    },
    {
      title: 'Description',
      dataIndex: 'variety',
      width: 300,
      align: 'left',
      render: (variety: string, record: any) => {
        const accountType = localStorage.getItem('accountType') || ''
        return (
          <a
            href={`#/product/${record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`}
            className="product-name"
            style={{ minWidth: 240, display: 'block' }}
          >
            {formatItemDescription(variety, record.SKU, this.props.sellerSetting)}
          </a>
        )
      },
    },
    {
      title: 'Item Cost In This PO',
      dataIndex: 'cost',
      width: 200,
      align: 'left',
      render: (value: number, record: any) => {
        return `${formatBn(value, { $: true })}/${record.pricingUOM}`
      },
    },
    {
      title: 'Allocated costs',
      dataIndex: 'perAllocateCost',
      key: 'allocated-cost',
      width: 200,
      align: 'left',
      render: (value: number, record: any) => {
        return (
          <ExtraChargePrice onClick={() => this.setState({ allocateModalVisible: true })}>
            {formatBn(record.perAllocateCost, { $: true })}
          </ExtraChargePrice>
        )
      },
    },
    {
      title: 'Full lot cost',
      dataIndex: 'perAllocateCost',
      key: 'full-lot-cost',
      className: 'border-right',
      width: 150,
      align: 'left',
      render: (value: number, record: any) => {
        const fullCost = bignumber(record.cost).add(record.perAllocateCost)
        const fullCostStr = formatBn(fullCost, { $: true })
        return `${fullCostStr}/${record.pricingUOM}`
      },
    },
  ]

  priceColumns: any[] = [
    {
      title: 'Set new default price',
      dataIndex: 'itemPrice',
      width: 250,
      className: 'pl50',
      align: 'left',
      render: (price: number, record: any) => {
        const warn = this.showWarn(record)
        const tooltipText = warn ? `Warning: Price less than cost` : ''
        return (
          <Flex className="v-center">
            <Tooltip placement="top" title={tooltipText}>
              <InputNumber
                warn={warn}
                place2
                value={price}
                style={{ width: 80 }}
                onChange={(val) => this.changePrice(record.wholesaleOrderItemId, val)}
                formatter={format$}
                parser={parse$}
              />
            </Tooltip>
            <Unit style={{ flex: 1, minWidth: 100, textAlign: 'left' }}>/{record.pricingUOM}</Unit>
          </Flex>
        )
      },
    },
    {
      title: 'Markup',
      dataIndex: 'markup',
      width: 100,
      align: 'left',
      render: (value: number, record: any) => {
        const { changing, changingValue } = this.state
        const { wholesaleOrderItemId } = record
        const markup = changing === wholesaleOrderItemId + '-markup' ? changingValue : this.calMarkup(record).toFixed()

        const warn = this.showWarn(record)
        const tooltipText = warn ? `Warning: Price less than cost` : ''
        return (
          <Flex className="v-center">
            <Tooltip placement="top" title={tooltipText}>
              <InputNumber
                warn={warn}
                place2
                value={markup}
                style={{ width: 80 }}
                onChange={(val) => this.changeMarkup(wholesaleOrderItemId, val)}
                onBlur={() => this.setState({ changing: null, changingValue: '' })}
                formatter={format$}
                parser={parse$}
              />
            </Tooltip>
          </Flex>
        )
      },
    },
    {
      title: (
        <Flex className="v-center">
          <span style={{ marginRight: 5 }}>margin</span>
          <Popconfirm
            title="Auto-fill margin values to equal current margin between default price and default cost."
            okText="Auto-fill margin"
            onConfirm={this.autoFillMargin}
          >
            <IconSvg
              type="duplicate"
              viewBox={void 0}
              style={{ width: 24, height: 24, marginRight: 4, fill: 'transparent', cursor: 'pointer' }}
            />
          </Popconfirm>
        </Flex>
      ),
      dataIndex: 'margin',
      align: 'left',
      render: (value: number, record: any) => {
        const { changing, changingValue } = this.state
        const { wholesaleOrderItemId } = record
        const margin = changing === wholesaleOrderItemId + '-margin' ? changingValue : this.calMargin(record).toFixed()

        const warn = this.showWarn(record)
        const tooltipText = warn ? `Warning: Price less than cost` : ''
        return (
          <StyledMarginDiv>
            <Tooltip placement="top" title={tooltipText}>
              <InputNumber
                max={99.99}
                warn={warn}
                place2
                value={margin}
                style={{ width: 80 }}
                onChange={(val) => this.changeMargin(wholesaleOrderItemId, val)}
                onBlur={() => this.setState({ changing: null, changingValue: '' })}
                formatter={formatPercent}
                parser={parsePercent}
              />
            </Tooltip>
            <StyledMargin>Current: {formatBn(record.originalMargin)}%</StyledMargin>
          </StyledMarginDiv>
        )
      },
    },
  ]

  costColumns: any[] = [
    {
      title: (
        <Flex className="v-center">
          <span style={{ marginRight: 5 }}>Set the default cost</span>
          <Popconfirm
            title="Auto-fill item default costs to match item full lot costs."
            okText="Auto-fill costs"
            onConfirm={this.autoFillDefaultCosts}
          >
            <IconSvg
              type="duplicate"
              viewBox={void 0}
              style={{ width: 24, height: 24, marginRight: 4, fill: 'transparent', cursor: 'pointer' }}
            />
          </Popconfirm>
        </Flex>
      ),
      dataIndex: 'itemCost',
      className: 'pl50',
      width: 250,
      align: 'left',
      render: (val: number, record: any) => {
        return (
          <Flex className="v-center">
            <InputNumber
              place2
              value={val}
              style={{ width: 80 }}
              onChange={(val) => this.changeCost(record.wholesaleOrderItemId, val)}
              formatter={format$}
              parser={parse$}
            />
            <Unit style={{ flex: 1, minWidth: 100, textAlign: 'left' }}>/{record.pricingUOM}</Unit>
          </Flex>
        )
      },
    },
    {
      title: 'Difference',
      dataIndex: 'diff',
      width: 150,
      align: 'left',
      render: (value: number, record: any) => {
        const diff = bignumber(record.itemCost)
          .sub(record.cost)
          .sub(record.perAllocateCost)
          .toFixed(2)
        return `${diff}/${record.pricingUOM}`
      },
    },
    {
      title: '',
      dataIndex: '',
    },
  ]

  getDataToUpdate = () => {
    const { currentOrderItems } = this.state
    const data: any = []
    currentOrderItems.forEach((orderItem: any) => {
      if (orderItem.updated) {
        data.push({
          itemId: orderItem.itemId,
          price: orderItem.itemPrice,
          defaultSellingPricingUOM: orderItem.pricingUOM,
          defaultPurchasingCostUOM: orderItem.pricingUOM,
          cost: orderItem.itemCost,
        })
      }
    })
    return data
  }

  savePriceAndCost = () => {
    const data = this.getDataToUpdate()
    this.props.updateItemsPrice(data)
  }

  render() {
    const { oneOffItems, relatedBills } = this.props
    const { orderId } = this.props.match.params
    const { currentOrderItems, allocateModalVisible } = this.state
    return (
      <PurchaseOrderPricing>
        <StyledDiv>
          <PricingAnalysisHeader>
            <div>
              Update item default price
              <Tooltip title="This edits the item default price value that is set in the Product page for the item.">
                <Icon className="icon-info" type="info-circle" />
              </Tooltip>
            </div>
            {/*<ThemeButton onClick={() => this.setState({ allocateModalVisible: true })}>Allocate costs</ThemeButton>*/}
          </PricingAnalysisHeader>
          <ThemeTable
            columns={[...this.sharedColumns, ...this.priceColumns]}
            dataSource={currentOrderItems}
            rowKey="wholesaleOrderItemId"
            pagination={false}
          />

          {allocateModalVisible && (
            <AllocateExtraCharges
              onCancel={() => this.setState({ allocateModalVisible: false })}
              orderId={orderId}
              currentOrder={this.props.currentOrder}
              currentOrderItems={this.state.currentOrderItems}
              updatePOItemAllocate={(v: any) => {
                this.props.updatePOItemAllocate(v)
                this.props.getPurchaseOrderProfitability(orderId)
                this.props.getOrderItemsById(orderId)
                this.props.getAllocationPallets(orderId)
              }}
              oneOffItems={oneOffItems}
              relatedBills={relatedBills}
              getAlloationData={this.props.getAlloationData}
            />
          )}
        </StyledDiv>
        <div style={{ padding: '16px 40px 0 25px' }}>
          <PricingAnalysisHeader>
            <div>
              Update item default cost
              <Tooltip title="This edits the item default cost value that is set in the Product page for the item.">
                <Icon className="icon-info" type="info-circle" />
              </Tooltip>
            </div>
          </PricingAnalysisHeader>
          <ThemeTable
            columns={[...this.sharedColumns, ...this.costColumns]}
            dataSource={currentOrderItems}
            rowKey="wholesaleOrderItemId"
            pagination={false}
          />
          <ThemeButton onClick={this.savePriceAndCost} className="mt10" disabled={ this.getDataToUpdate().length === 0 }>
            Save
          </ThemeButton>
        </div>
      </PurchaseOrderPricing>
    )
  }
}

const ExtraChargePrice = styled.span`
  padding: 6px 8px;
  border-radius: 3px;
  box-shadow: 0px 1px 3px #ccc;
  color: rgb(28, 110, 49);
  cursor: pointer;
`

const StyledDiv = styled.div`
  padding: 16px 40px 0 25px;
  .ant-table-tbody > tr > td {
    padding: 20px 0 20px !important;
  }
`

const StyledMarginDiv = styled.div`
  position: relative;
  width: 130px;
`

const StyledMargin = styled.div`
  height: 18px;
  line-height: 18px;
  position: absolute;
  bottom: -20px;
  font-size: 10px;
  color: #333;
`
