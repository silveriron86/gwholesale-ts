import * as React from 'react'
import _ from 'lodash'
import { Theme, gray01 } from '~/common'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import {
  formatNumber,
  getCostPre,
  getOrderPrefix,
  mathRoundFun,
  round,
  ratioPriceToBasePrice,
  ratioQtyToInventoryQty
} from '~/common/utils'

import { FlexWrap, InsightBlock } from '~/modules/reports/reports.style'
import { Icon, Tooltip } from 'antd'
import { ProfitabilityTable, StyledDiv } from '../_style'
import ProfitabilityItem from './profitability-item'
import { ALLOCATION_TYPE, OrderItem } from '~/schema'
import { ThemeButton, ThemeOutlineButton } from '~/modules/customers/customers.style'
import AllocateExtraCharges from '../pricing-analysis/AllcateExtraChargesModal'
import LiquidationModal from './liquidation-modal'
import moment from 'moment'

type Props = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
  }

export default class PurchaseProfitability extends React.PureComponent<Props> {
  itemRef: any = React.createRef<any>()
  priceUnitTotal: any[] = [], // define this variable to set custom configuartion to excel columns
  state = {
    data: {
      salesOrderItems: [],
      itemAdjustments: [],
    },
    allocationPallets: [],
    totalInfo: {},
    liquidationVisible: false,
    allocateModalVisible: false
  }

  updateTotalInfo = (data: any) => {
    let { totalInfo } = this.state
    if (!data.orderItemId) return
    totalInfo[data.orderItemId] = totalInfo[data.orderItemId] ? { ...totalInfo[data.orderItemId], ...data } : data
    this.setState({ totalInfo })
  }

  componentDidMount() {
    const orderId = this.props.match.params.orderId
    if (orderId) {
      this.props.getPurchaseOrderProfitability(orderId)
      this.props.getOrderItemsById(orderId)
      this.props.getAlloationData(orderId)
      this.props.getAllocationPallets(orderId)
    }
    if (Array.isArray(this.props.allocationPalletData)) {
      this.formatAllocationPalletData(this.props.allocationPalletData, this.props.orderItems)
    }
    this.setState({ data: this.props.profitability })
  }

  componentWillReceiveProps(nextProps: any) {
    if (JSON.stringify(this.props.profitability) != JSON.stringify(nextProps.profitability)) {
      this.setState({ data: nextProps.profitability })
    }

    if (JSON.stringify(this.props.allocationPalletData) != JSON.stringify(nextProps.allocationPalletData) ||
      (this.props.allocationPalletData === null && Array.isArray(nextProps.allocationPalletData)) ||
      JSON.stringify(this.props.orderItems) != JSON.stringify(nextProps.orderItems) ) {
        this.formatAllocationPalletData(nextProps.allocationPalletData, nextProps.orderItems)
    }
  }

  formatAllocationPalletData = (data: any[], orderItems: any[]) => {
    const palletGroupByOrder = _.groupBy(data, 'wholesaleOrderItemId')
    const { currentOrder } = this.props
    if (!currentOrder) return []

    const allocationPallets: any[] = orderItems.map((item: any) => ({
      ...item,
      totalAllocateCharge: currentOrder.allocateType === ALLOCATION_TYPE.MANUALLY ? item.totalAllocateCharge : '',
      palletUnits: _.reduce(
        _.get(palletGroupByOrder, `${item.wholesaleOrderItemId}`, []),
        (sum, n) => sum + n.palletUnits,
        0,
      ),
    }))
    this.setState({ allocationPallets: JSON.parse(JSON.stringify(allocationPallets)) })
  }

  renderProfitabiltyDetail = () => {
    const { orderItems, currentOrder, allocationData } = this.props
    const { data, allocationPallets } = this.state
    let result: any[] = []
    const totalReceivedQty = orderItems.reduce((n: any, p: { receivedQty: any }) => n + p.receivedQty, 0)
    const totalConfirmedQty = orderItems.reduce((n: any, p: { qtyConfirmed: any }) => n + p.qtyConfirmed, 0)
    const totalQuantity = orderItems.reduce((n: any, p: { quantity: any }) => n + p.quantity, 0)
    const totalChargesPrice = allocationData.reduce((p: number, n: any) => p + n.price, 0)
    const totalPalletUnits = allocationPallets.reduce((n: any, p: { palletUnits: any }) => n + p.palletUnits, 0)
    orderItems.forEach(el => {
      const palletItem = allocationPallets.find((o: any) => o.wholesaleOrderItemId == el.wholesaleOrderItemId)
      result.push(<ProfitabilityItem
        key={el.wholesaleOrderItemId}
        orderItem={el}
        data={data}
        currentOrder={currentOrder}
        allocationData={allocationData}
        palletItem={palletItem}
        totalConfirmedQty={totalConfirmedQty}
        totalReceivedQty={currentOrder?.wholesaleOrderStatus == 'RECEIVED' ? totalReceivedQty : totalQuantity}
        totalChargesPrice={totalChargesPrice}
        totalPalletUnits={totalPalletUnits}
        updateTotalInfo={this.updateTotalInfo}
      />)
    });
    return result
  }

  getTotalValues = () => {
    const { totalInfo } = this.state
    let headerValues: any = {
      totalSold: 0,
      totalRemaining: 0,
      totalReceived: 0,
      totalCost: 0,
      pureTotalCost: 0,
      allocationTotalCost: 0,
      totalSalesPrice: 0,
      percentageSold: 0,
      profit: 0,
      margin: 0
    }
    const ids = Object.keys(totalInfo)
    if (!ids.length) return headerValues
    ids.forEach(id => {
      const info = totalInfo[id]
      headerValues.totalSold += info.totalSold
      headerValues.totalReceived += info.totalReceived
      headerValues.totalCost += info.subTotalCost
      headerValues.pureTotalCost += info.pureTotalCost
      headerValues.allocationTotalCost += info.allocationTotalCost
      if(!isNaN(info.totalSales)) {
        headerValues.totalSalesPrice += info.totalSales
      }
    });
    headerValues.totalRemaining = headerValues.totalReceived - headerValues.totalSold
    headerValues.percentageSold = headerValues.totalReceived ? (headerValues.totalSold / headerValues.totalReceived *100) : 0
    headerValues.profit = headerValues.totalSalesPrice - headerValues.totalCost
    headerValues.margin = headerValues.totalSalesPrice ? headerValues.profit / headerValues.totalSalesPrice * 100 : 0
    return headerValues
  }

  renderDownloadTable = () => {
    const { orderItems } = this.props
    const { data } = this.state
    let trs: any[] = []
    let priceUnitTotal: any = []
    orderItems.forEach(el => {
      trs.push(
        <tr key={el.wholesaleOrderItemId}>
          <td colSpan={7}>{el.variety}{el.SKU ? `(${el.SKU})` : ''}</td>
        </tr>
      )
      priceUnitTotal.push({
        p: 'header',
        t: 'header'
      })
      const salesData = this.makeSalesDownloadData(el, data);
      if(salesData.data.length) {
        trs.push(
        <tr key={`sales-info-header-${el.wholesaleOrderItemId}`}>
          <td>SALES</td>
          <td>Status</td>
          <td>Sell Date</td>
          <td>Order no.</td>
          <td>Qty</td>
          <td>Price/Unit</td>
          <td>Total Sales</td>
        </tr>)
        priceUnitTotal.push({
          p: 'header',
          t: 'header'
        })
        salesData.data.forEach((sale, index) => {
          trs.push(
          <tr key={`sales-info-data-${el.wholesaleOrderItemId}-${index}`}>
            <td>{sale['companyName']}</td>
            <td>{sale['wholesaleOrderStatus']}</td>
            <td>{sale['deliveryDate'] ? moment(sale['deliveryDate']).format('MM/DD/YYYY') : ''}</td>
            <td>{sale['wholesaleOrderId']}</td>
            <td>{`${sale['catchWeightQty']} ${sale['uom']}`}</td>
            <td></td>
            <td></td>
          </tr>
          )
          priceUnitTotal.push({
            p: `${formatNumber(sale['salePrice'], 2)}/${sale['uom']}`,
            t: `${sale['totalSales'] < 0 ? '-' : ''}$${formatNumber(Math.abs(sale['totalSales']), 2)}`
          })
        });
      }

      const costData = this.makeCostDownloadData(el, salesData.totalCatchWeight)
      if(costData.data.length) {
        trs.push(
        <tr key={`cost-info-header-${el.wholesaleOrderItemId}`}>
          <td>COSTS</td>
          <td></td>
          <td>Purchase Date</td>
          <td>Order no.</td>
          <td>Qty</td>
          <td>Cost/Unit</td>
          <td>Total Costs</td>
        </tr>)
        priceUnitTotal.push({
          p: 'header',
          t: 'header'
        })
        costData.data.forEach((cost, index) => {
          trs.push(
          <tr key={`cost-info-data-${el.wholesaleOrderItemId}-${index}`}>
            <td>{cost['type']}</td>
            <td></td>
            <td>{cost['deliveryDate'] ? moment(cost['deliveryDate']).format('MM/DD/YYYY') : ''}</td>
            <td>{cost['wholesaleOrderId']}</td>
            <td>{cost['totalQty'] ? `${cost['totalQty']} ${cost['uom']}` : ''}</td>
            <td></td>
            <td></td>
          </tr>
          )
          priceUnitTotal.push({
            p: `$${formatNumber(mathRoundFun(cost['cost'], 3), 2)}/${cost.uom}`,
            t: cost['cost'] * cost['totalQty'] ? `$${formatNumber(cost['cost'] * cost['totalQty'], 2)}` : ''
          })
        });
      }

      const statisticData = this.makeStatisticDownloadData(el, salesData.lotLevelSubTotalPrice, costData.lotLevelSubTotalCost);
      statisticData.forEach((statistic, index) => {
        trs.push(
          <tr key={`statistic-data-${el.wholesaleOrderItemId}-${index}`}>
            <td colSpan={5}>{statistic['type']}</td>
            <td colSpan={2}></td>
          </tr>
        )
        priceUnitTotal.push({
          p: statistic['cost'],
          t: ''
        })
      })
    });
    this.priceUnitTotal = priceUnitTotal
    return trs;
  }

  makeSalesDownloadData = (orderItem: OrderItem, data: any) => {
    let dataSource: any[] = []
    let salesChildItems = data ? data.salesOrderItems.filter((o: any) => o.lotId == orderItem.lotId) : []
    const costingUOM = orderItem.inventoryUOM
    const adjustmentItems: any[] = data ? data.itemAdjustments.filter(
      (o: any) => o.lotId == orderItem.lotId && o.adjustmentType == 1
    ).map((o: any) => {
      const uom = o.overrideUOM ? o.overrideUOM : o.inventoryUOM
      let salePrice = o.salePrice
      const found = data.salesOrderItems.find((so: any) => so.wholesaleOrderItemId === o.wholesaleOrderItemId)
      if(found) {
        salePrice = found.salePrice
      }
      return {
        ...o,
        uom,
        companyName: `Return: ${o.companyName}`,
        catchWeightQty: costingUOM == uom ? -o.catchWeightQty : -ratioQtyToInventoryQty(uom, o.catchWeightQty, o.wholesaleItem),
        salePrice: costingUOM == uom ? salePrice : ratioPriceToBasePrice(uom, salePrice, o.wholesaleItem),
        returned: costingUOM == uom ? o.catchWeightQty : ratioQtyToInventoryQty(uom, o.catchWeightQty, o.wholesaleItem),
      }
    }) : []
    salesChildItems = salesChildItems.map((el: any) => {
      const uom = el.overrideUOM ? el.overrideUOM : el.inventoryUOM
      const subCatchWeight = costingUOM == uom ? el.catchWeightQty : ratioQtyToInventoryQty(uom, el.catchWeightQty, el.wholesaleItem)
      const catchWeightQty = el.wholesaleOrderStatus == 'SHIPPED' ? subCatchWeight : el.orderQty
      return {
        ...el,
        uom,
        companyName: el.wholesaleClient ? el.wholesaleClient.clientCompany.companyName : '',
        deliveryDate: moment(el.deliveryDate).format('MM/DD/YYYY'),
        catchWeightQty: costingUOM == uom ? catchWeightQty : ratioQtyToInventoryQty(uom, catchWeightQty, el.wholesaleItem),
        salePrice: costingUOM == uom ? el.salePrice : ratioPriceToBasePrice(uom, el.salePrice, el.wholesaleItem)
      }
    })

    dataSource = [...salesChildItems, ...adjustmentItems]

    const totalCatchWeight = _.sumBy(dataSource, 'catchWeightQty')
    const totalSalePrice = _.sumBy(dataSource, function (o) {
      return o.catchWeightQty * o.salePrice
    })

    let totalSales = 0
    dataSource = dataSource.map((record: any) => {
      const rowTotal = record.salePrice * record.catchWeightQty;
      totalSales += rowTotal
      return {
        ...record,
        totalSales: rowTotal
      }
    })

    const total = {
      companyName: 'Total sales',
      catchWeightQty: totalCatchWeight,
      salePrice: totalCatchWeight != 0 ? totalSalePrice / totalCatchWeight : 0,
      totalCost: totalSalePrice,
      uom: costingUOM,
      totalSales,
    }
    return {
      data: dataSource.length ? [...dataSource, { ...total }] : [],
      lotLevelSubTotalPrice: total.salePrice,
      totalCatchWeight
    }
  }

  makeCostDownloadData = (orderItem: OrderItem, totalCatchWeight: number) => {
    const { orderItems, currentOrder, allocationData } = this.props
    const { allocationPallets } = this.state
    const palletItem = allocationPallets.find((o: any) => o.wholesaleOrderItemId == orderItem.wholesaleOrderItemId)
    const totalReceivedQty = orderItems.reduce((n: any, p: { receivedQty: any }) => n + p.receivedQty, 0)
    const totalConfirmedQty = orderItems.reduce((n: any, p: { qtyConfirmed: any }) => n + p.qtyConfirmed, 0)
    const totalChargesPrice = allocationData.reduce((p: number, n: any) => p + n.price, 0)
    const totalPalletUnits = allocationPallets.reduce((n: any, p: { palletUnits: any }) => n + p.palletUnits, 0)
    const uom = orderItem.inventoryUOM
    if (!currentOrder) return {data: [], lotLevelSubTotalCost: 0}

    const itemCost = {
      type: 'Item cost',
      deliveryDate: moment(orderItem.deliveryDate).format('MM/DD/YYYY'),
      wholesaleOrderId: currentOrder ? currentOrder.wholesaleOrderId : '',
      totalQty: totalCatchWeight,
      cost: orderItem.cost,
      uom
    }

    let allocations: any[] = []
    const qty = orderItem.status == 'RECEIVED' ? totalReceivedQty : totalConfirmedQty;
    allocationData.forEach((v: any) => {
      if (v.isChecked) {
        const allocationCost = palletItem && qty ? getCostPre(palletItem, currentOrder.allocateType, totalChargesPrice, qty, totalPalletUnits) : 0
        const itemName = v.itemName ? `${v.itemName}${!v.chargeDesc ? '' : `(${v.chargeDesc})`}` : ''
        allocations.push({
          ...v,
          type: v.relatedBillVendorName || v.vendorName || itemName,
          cost: totalChargesPrice && v.price ? _.divide(v.price, totalChargesPrice) * allocationCost : 0,
          wholesaleOrderId: v.chargeFrom ? v.chargeFrom : currentOrder?.wholesaleOrderId,
          totalQty: totalCatchWeight,
          deliveryDate: v.deliveryDate ? v.deliveryDate : currentOrder.deliveryDate,
          uom
        })
      }
    })
    const totalCost = _.sumBy([itemCost, ...allocations], 'cost')

    const total = {
      type: 'Total costs',
      totalQty: totalCatchWeight,
      cost: totalCost,
      totalCost: totalCost * totalCatchWeight,
      uom
    }

    return {
      data: [itemCost, ...allocations, total],
      lotLevelSubTotalCost: total.cost
    }
  }

  makeStatisticDownloadData = (orderItem: OrderItem, lotLevelSubTotalPrice: number, lotLevelSubTotalCost: number) => {
    const uom = orderItem.inventoryUOM
    const profit = lotLevelSubTotalPrice - lotLevelSubTotalCost
    return [
      {
        type: 'Profit (unit-level)',
        cost: `${profit < 0 ? '-' : ''}$${formatNumber(Math.abs(profit), 2)}/${uom}`,
        dataType: 'statistic',
        deliveryDate: '',
        totalQty: '',
        totalCost: ''
      },
      {
        type: 'Margin (unit-level)',
        cost: `${lotLevelSubTotalPrice == 0 ? '0.0' : mathRoundFun((profit / lotLevelSubTotalPrice) * 100, 1)}%`,
        dataType: 'statistic',
        deliveryDate: '',
        totalQty: '',
        totalCost: ''
      }
    ]
  }

  onDownloadReport = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('profitability-report'))
    var wscols = [
      { wpx: 300 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 },
      { wpx: 100 }
    ]

    wb.Sheets.Sheet1['!cols'] = wscols

    this.priceUnitTotal.forEach((data: any, index: number)=> {
      if(data.p == 'header') return
      if(data.p) {
        wb.Sheets.Sheet1['F' + (index + 1)] = {v: data.p}
      }
      if(data.t) {
        wb.Sheets.Sheet1['G' + (index + 1)] = {v: data.t}
      }
    })
    const today = moment().format('MM-DD-YYYY')
    const title = `PO-${getOrderPrefix(this.props.sellerSetting, 'purchase')}-${this.props.currentOrder?.wholesaleOrderId}-profitability-report-${today}.xlsx`
    XLSX.writeFile(wb, title)
  }

  render() {
    const headerValues = this.getTotalValues()
    const insightStyle = { paddingBottom: 16}
    const { orderId } = this.props.match.params

    const { liquidationVisible, allocateModalVisible } = this.state
    // let
    return (
      <div style={{ padding: '16px 40px 0 25px' }}>
        <div>
          <FlexWrap style={{ marginBottom: 48}}>
            <InsightBlock style={{...insightStyle, marginLeft: 0}}>
              <div className="label">
                Units sold
                <Tooltip title="Here is to use base uom as the unit of quantity">
                  <Icon type="info-circle" style={{ marginLeft: '8px', marginTop: '-3px' }} />
                </Tooltip>
              </div>
              <div className="value">
                {round(headerValues.totalSold)}/{round(headerValues['totalReceived'])}
              </div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Percentage sold</div>
              <div className="value">{formatNumber(headerValues.percentageSold, 2)}%</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total sales</div>
              <div className="value">${formatNumber(headerValues.totalSalesPrice, 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Product cost</div>
              <div className="value">${formatNumber(headerValues.pureTotalCost, 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Allocated costs</div>
              <div className="value">${formatNumber(headerValues.allocationTotalCost, 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total costs</div>
              <div className="value">${formatNumber(headerValues['totalCost'], 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total profit</div>
              <div className="value">{headerValues['profit'] < 0 ? '-$':'$'}{formatNumber(Math.abs(headerValues['profit']), 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total margin</div>
              <div className="value">{formatNumber(headerValues['margin'], 2)}%</div>
            </InsightBlock>
            <StyledDiv>
              <ThemeButton onClick={()=>{ this.setState({liquidationVisible: true}) }}>
                Calculate liquidation
              </ThemeButton>
              <ThemeOutlineButton onClick={this.onDownloadReport}>
                Download report
              </ThemeOutlineButton>
            </StyledDiv>

          </FlexWrap>
        </div>
        <ProfitabilityTable>
          {this.renderProfitabiltyDetail()}
        </ProfitabilityTable>

        {allocateModalVisible && (
          <AllocateExtraCharges
            onCancel={() => this.setState({ allocateModalVisible: false })}
            orderId={orderId}
            currentOrder={this.props.currentOrder}
            currentOrderItems={this.props.orderItems}
            updatePOItemAllocate={(v: any) => {
              this.props.updatePOItemAllocate(v)
              this.props.getPurchaseOrderProfitability(orderId)
              this.props.getOrderItemsById(orderId)
              this.props.getAllocationPallets(orderId)
            }}
            oneOffItems={this.props.oneOffItems}
            relatedBills={this.props.relatedBills}
            getAlloationData={this.props.getAlloationData}
          />
        )}

        {
          liquidationVisible && <LiquidationModal
          headerValues={headerValues}
          oneOffItems={this.props.oneOffItems}
          orderItems={this.props.orderItems}
          currentOrder={this.props.currentOrder}
          createOffItem={this.props.createOffItem}
          getOrderItemsById={this.props.getOrderItemsById}
          onCancel={() => { this.setState({liquidationVisible:false}) }}
          visible={liquidationVisible}
          allocationData={this.props.allocationData}
          sellerSetting={this.props.sellerSetting}
        />
        }

        <table id='profitability-report' style={{ display: 'none' }}>
          <tbody>
            {this.renderDownloadTable()}
          </tbody>
        </table>
      </div>
    )
  }
}
