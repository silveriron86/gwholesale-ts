import React, { useState, useReducer, useMemo } from 'react'
import { Modal, Radio, Input, Select, Descriptions, notification } from 'antd'
import styled from '@emotion/styled'
import LiquidationTable from './liquidation-table'
import { Icon as IconSvg } from '~/components/icon/'
import { useParams } from 'react-router'
import { v4 as uuidv4 } from 'uuid'
import { OrderService } from '../../../orders/order.service'
import moment from 'moment'
import { formatNumber, getOrderPrefix } from '~/common/utils'
import { ThemeIconButton } from '~/modules/customers/customers.style'

enum Extra {
  wholesale = 'wholesale',
  ship = 'ship',
}

const { Item: DItem } = Descriptions

type StateType = {
  feeCost: number
  feePercent: string | number
  feeUnit: string | number
  feeType: string | number
  extraChargeType: string | number
  liquidationMethod: string | number
}

export default function LiquidationModal(props: any) {
  const {
    visible,
    onCancel,
    currentOrder,
    orderItems,
    headerValues,
    createOffItem,
    getOrderItemsById,
    allocationData,
    sellerSetting
  } = props

  const { totalSalesPrice, totalSold, pureTotalCost } = headerValues
  const { orderId } = useParams<{ orderId: any }>()

  const sortedOrderItems = useMemo(() => {
    return [...orderItems].sort((a: any, b: any) => {
      if (a.variety < b.variety) {
        return -1
      } else if (a.variety > b.variety) {
        return 1
      }
      return 0
    })
  }, [orderItems])
  const allZero = sortedOrderItems.every(item => !item.cost || item.cost ==='0' )

  const [values, setValues]: [StateType, any] = useReducer(
    (state: StateType, action: any) => {
      return { ...state, ...action }
    },
    {
      feeCost: totalSold == 0 ? 0.0 : (pureTotalCost / totalSold).toFixed(4),
      feePercent: '',
      feeUnit: '',
      feeType: '1',
      extraChargeType: '1',
      liquidationMethod: '1',
    },
  )

  const [extraMap, setExtraMap] = useReducer((state: any, action: any) => {
    return { ...state, ...action }
  }, {})

  const oneOffItemsSort = useMemo(() => {
    let allocationDataFmt = allocationData.filter((item: any) => item.isChecked)
    allocationDataFmt = allocationDataFmt.map((item: any) => ({
      itemName: item.relatedBillVendorName || item.vendorName || item.itemName ,
      total: item.price,
      id: item.id,
    }))

    return [...allocationDataFmt]
  }, [allocationData])

  const { extraChargeType } = values
  const { wholeCharges, shipCharges } = useMemo(() => {
    let wholeCharges: any = []
    let shipCharges: any = []

    const allCharges = oneOffItemsSort.map((item: any) => ({
      key: uuidv4(),
      name: <span style={{ paddingLeft: 20 }}>{item.itemName}</span>,
      total: item.total,
      hidePercent: true,
      id: item.id,
    }))

    if (extraChargeType === '1') {
      wholeCharges = allCharges
    } else if (extraChargeType === '2') {
      shipCharges = allCharges
    } else if (extraChargeType === '3') {
      Object.keys(extraMap).forEach((key) => {
        const offItem = allCharges.find((item: any) => item.id == key)
        const val = extraMap[key]
        if (val === Extra.wholesale) {
          wholeCharges.push(offItem)
        } else if (val === Extra.ship) {
          shipCharges.push(offItem)
        }
      })
    }

    return { wholeCharges, shipCharges }
  }, [oneOffItemsSort, extraChargeType, extraMap])

  const extraShipTotal = shipCharges.reduce((pre: any, cur: any) => (pre += cur.total), 0)
  const extraWholeTotal = wholeCharges.reduce((pre: any, cur: any) => (pre += cur.total), 0)

  const { feeType, feeCost, feePercent, feeUnit } = values
  let wholesalerFee: number = 0
  let netLiquidation: number = 0
  if (feeType === '1') {
    wholesalerFee = totalSalesPrice - pureTotalCost
    netLiquidation = feeCost * totalSold - extraShipTotal
  } else if (feeType === '2') {
    wholesalerFee = (+feePercent * totalSalesPrice) / 100
    netLiquidation = totalSalesPrice - wholesalerFee - extraShipTotal
  } else if (feeType === '3') {
    wholesalerFee = +feeUnit * totalSold
    netLiquidation = totalSalesPrice - wholesalerFee - extraShipTotal
  } else {
    console.error('unexpected feeType')
  }

  const adjust = netLiquidation - pureTotalCost
  const factor = pureTotalCost == 0 ? 0 : netLiquidation / pureTotalCost
  const newAvgCost = totalSold == 0 ? 0.00 : netLiquidation / totalSold

  const addNewOffItem = () => {
    const data = {
      constantRatio: true,
      cost: 0,
      picked: 0,
      price: adjust.toFixed(2),
      quantity: 1,
      status: 'SHIPPED',
      chargeDesc: 'Liquidation adjustment',
      itemName: 'Liquidation adjustment',
      orderId,
      uom: 'each',
    }
    createOffItem(data)
  }

  const onOk = () => {
    if (values.liquidationMethod === '2') {
      addNewOffItem()
      onCancel()
      notification.success({
        message: 'Liquidation successfully processed',
        duration: 5
      })
    } else if (values.liquidationMethod === '1') {
      // ajust cost
      OrderService.instance
        .adjustCost(orderId, {
          orderItemList: sortedOrderItems.map((item) => ({
            wholesaleOrderItemId: item.wholesaleOrderItemId,
            cost: allZero ? newAvgCost.toFixed(2) : (item.cost * factor).toFixed(2),
          })),
        })
        .subscribe({
          next(res: any) {
            if (res.statusCodeValue === 200) {
              getOrderItemsById(orderId)
              notification.success({
                message: res.body.message,
                duration: 5
              })
              onCancel()
            } else {
              notification.error({ message: 'Liquidation process failed' })
            }
          },
          error(err) {
            notification.error({ message: 'Liquidation process failed' })
          },
          complete() {},
        })
    } else {
      notification.warn({ message: 'Please select Liquidation method' })
    }
  }

  const onClickDownload = () => {
    const XLSX = window.XLSX
    var wb = XLSX.utils.table_to_book(document.getElementById('liquidation-report-download'), {raw: true})
    var wscols = [
      { wpx: 100 },
      { wpx: 80 },
      { wpx: 100 },
      { wpx: 150 },
      { wpx: 100 },
      { wpx: 80 },
    ]
    wb.Sheets.Sheet1['!cols'] = wscols
    const today = moment().format('MM-DD-YYYY')
    const title = `PO-${getOrderPrefix(sellerSetting, 'purchase')}${currentOrder?.wholesaleOrderId}-profitability-report-${today}.xlsx`
    XLSX.writeFile(wb, title)
  }

  return (
    <StyledModal
      onOk={onOk}
      onCancel={onCancel}
      width="90%"
      visible={visible}
      title="Liquidation"
      okText="Process liquidation"
    >
      <StyledWrap>
        <div className="left">
          <div className="title">Liquidation settings</div>
          <div className="left-label first-label">Wholesaler/broker fee structure</div>
          <StyledRadioGroup value={values.feeType} onChange={(e) => setValues({ feeType: e.target.value })}>
            <Radio value="1">
              Purchase cost
              {values.feeType === '1' && (
                <Input
                  value={values.feeCost}
                  onChange={(e) => setValues({ feeCost: e.target.value })}
                  onBlur={(e) => setValues({ feeCost: (+e.target.value).toFixed(2) })}
                  type="number"
                  prefix="$"
                  style={{ width: 130, marginLeft: 10 }}
                />
              )}
            </Radio>
            <Radio value="2">
              % of sales
              {values.feeType === '2' && (
                <Input
                  value={values.feePercent}
                  onChange={(e) => setValues({ feePercent: e.target.value })}
                  type="number"
                  suffix="%"
                  style={{ width: 110, marginLeft: 10 }}
                />
              )}
            </Radio>
            <Radio value="3">
              Commissions per unit
              {values.feeType === '3' && (
                <Input
                  value={values.feeUnit}
                  onChange={(e) => setValues({ feeUnit: e.target.value })}
                  onBlur={(e) => setValues({ feeUnit: (+e.target.value).toFixed(2) })}
                  type="number"
                  prefix="$"
                  style={{ width: 110, marginLeft: 10 }}
                />
              )}
            </Radio>
          </StyledRadioGroup>
          <div className="left-label">Allocated costs</div>
          <Select
            value={values.extraChargeType}
            onChange={(val: any) => setValues({ extraChargeType: val })}
            style={{ width: 248 }}
          >
            <Select.Option value="1">Wholesaler/broker pays</Select.Option>
            <Select.Option value="2">Vendor (shipper) pays</Select.Option>
            <Select.Option value="3">Custom split payment</Select.Option>
          </Select>
          {values.extraChargeType === '3' && (
            <table className="left-table">
              <tbody>
                <tr>
                  <th></th>
                  <th>
                    <span style={{ marginRight: 10 }}>Wh/Broker</span>
                    <span>Ship</span>
                  </th>
                </tr>
                {oneOffItemsSort.map((off: any) => {
                  const { id, itemName } = off
                  return (
                    <tr key={id}>
                      <td style={{ textAlign: 'left' }}>{itemName}</td>
                      <td>
                        <Radio.Group
                          value={extraMap[id]}
                          onChange={(e) => {
                            setExtraMap({ [id]: e.target.value })
                          }}
                        >
                          <Radio value={Extra.wholesale} />
                          <Radio value={Extra.ship} />
                        </Radio.Group>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          )}

          <div className="left-label">Liquidation method</div>
          <Select
            value={values.liquidationMethod}
            onChange={(v: any) => setValues({ liquidationMethod: v })}
            style={{ width: 248 }}
          >
            <Select.Option value="1">Adjust purchase cost(s)</Select.Option>
            <Select.Option value="2">Post adjustment as extra charge</Select.Option>
          </Select>

          {sortedOrderItems.length <= 1 && (
            <>
              <div className="left-cost first-cost ft16">
                <div className="cost-label">Original purchase cost</div>
                <div className="cost-value">${formatNumber(pureTotalCost / totalSold, 2)}/unit</div>
              </div>
              <div className="left-cost ft16">
                <div style={{ fontWeight: 'bold' }} className="cost-label">
                  New purchase cost
                </div>
                <div className="cost-value">${formatNumber(newAvgCost, 2)}/unit</div>
              </div>
            </>
          )}
          {sortedOrderItems.length > 1 && (
            <>
              <div className="left-cost first-cost ft16">
                <div className="cost-label">Original avg. cost</div>
                <div className="cost-value">${formatNumber(pureTotalCost / totalSold, 2)}/unit</div>
              </div>
              {sortedOrderItems.map((item: any) => (
                <div className="left-cost" key={item.wholesaleOrderItemId}>
                  <div className="cost-label">{item.variety}</div>
                  <div className="cost-value">${formatNumber(item.cost, 2)}/unit</div>
                </div>
              ))}
              <div className="left-cost ft16" style={{ marginTop: 10 }}>
                <div style={{ fontWeight: 'bold' }} className="cost-label">
                  New avg. cost
                </div>
                <div className="cost-value">${formatNumber(newAvgCost, 2)}/unit</div>
              </div>
              {sortedOrderItems.map((item: any) => (
                <div className="left-cost" key={item.wholesaleOrderItemId}>
                  <div className="cost-label">{item.variety}</div>
                  <div className="cost-value">${allZero ? formatNumber(newAvgCost, 2) : formatNumber(item.cost * factor, 2)}/unit</div>
                </div>
              ))}
            </>
          )}
        </div>

        <div className="right">
          <StyledTitle>
            Liquidation report
            <ThemeIconButton onClick={onClickDownload}>
              <IconSvg viewBox="0 0 36 36" width={25} height={25} type="unit_weight_sheet" />
            </ThemeIconButton>
          </StyledTitle>
          <div style={{ padding: '20px 30px' }}>
            <StyledDescriptions colon={false} column={1}>
              <DItem label="Vendor (shipper)">{currentOrder?.wholesaleClient?.clientCompany?.companyName}</DItem>
              <DItem label="Lot">
                {sortedOrderItems.map((item: any) => (
                  <div key={item.lotId}>{item.lotId}</div>
                ))}
              </DItem>
              <DItem label="Delivery date">
                {currentOrder?.deliveryDate ? moment(currentOrder.deliveryDate).format('MM/DD/YYYY') : ''}
              </DItem>
            </StyledDescriptions>
            <div style={{ fontWeight: 'bold', padding: '15px 0' }}>Sales summary</div>
            <SDescriptions colon={false} column={6} layout="vertical">
              <DItem label="Units sold">{formatNumber(headerValues.totalSold, 0)}</DItem>
              <DItem label="Units received">{formatNumber(headerValues.totalReceived, 0)}</DItem>
              <DItem label="Units remaining">
                {formatNumber(headerValues.totalReceived - headerValues.totalSold, 0)}
              </DItem>
            </SDescriptions>
            <SDescriptions colon={false} column={6} layout="vertical">
              <DItem label="Total sales">${formatNumber(headerValues.totalSalesPrice, 2)}</DItem>
              <DItem label="Product cost">${formatNumber(headerValues.pureTotalCost, 2)}</DItem>
              <DItem label="Allocated costs">${formatNumber(headerValues.allocationTotalCost, 2)}</DItem>
              <DItem label="Total costs">${formatNumber(headerValues.totalCost, 2)}</DItem>
              <DItem label="Profit">${formatNumber(headerValues.profit, 2)}</DItem>
              <DItem label="Total margin">{formatNumber(headerValues.margin, 1)}%</DItem>
            </SDescriptions>
            <LiquidationTable
              headerValues={headerValues}
              wholesalerFee={wholesalerFee}
              wholeCharges={wholeCharges}
              shipCharges={shipCharges}
              extraWholeTotal={extraWholeTotal}
              extraShipTotal={extraShipTotal}
              netLiquidation={netLiquidation}
              adjust={adjust}
            />
          </div>
          
          <table id="liquidation-report-download" className="uk-report-table table table-striped" style={{ display: 'none' }}>
            <tbody>
              <tr></tr>
              <tr></tr>
              <tr>
                <td colSpan={6}>Liquidation Report</td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td>Vendor (shipper):</td>
                <td>{currentOrder?.wholesaleClient?.clientCompany?.companyName}</td>
              </tr>
              <tr>
                <td>Lot:</td>
                <td>{sortedOrderItems[0]?.lotId}</td>
              </tr>
              {
                sortedOrderItems.slice(1)?.map(item=> (
                  <tr>
                    <td></td>
                    <td>{item.lotId}</td>
                  </tr>
                ))
              }
              <tr>
                <td>Delivery Date:</td>
                <td>{currentOrder?.deliveryDate ? moment(currentOrder.deliveryDate).format('MM/DD/YYYY') : ''}</td>
              </tr>
              <tr></tr>
              <tr>
                <td colSpan={6}>Summary Totals</td>
              </tr>
              <tr></tr>  
              <tr>
                <td>Units sold</td>
                <td>{`${formatNumber(headerValues.totalSold, 0)}`}</td>
                <td></td>
                <td>Total sales</td>
                <td>{`$${formatNumber(headerValues.totalSalesPrice, 2)}`}</td>
              </tr>
              <tr>
                <td>Units received</td>
                <td>{`${formatNumber(headerValues.totalReceived, 0)}`}</td>
                <td></td>
                <td>Product cost</td>
                <td>{`$${formatNumber(headerValues.pureTotalCost, 2)}`}</td>
              </tr>
              <tr>
                <td>Units remaining</td>
                <td>{`${formatNumber(headerValues.totalReceived - headerValues.totalSold, 0)}`}</td>
                <td></td>
                <td>Allocated costs</td>
                <td>{`$${formatNumber(headerValues.allocationTotalCost, 2)}`}</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total costs</td>
                <td>{`$${formatNumber(headerValues.totalCost, 2)}`}</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Profit</td>
                <td>{`$${formatNumber(headerValues.profit, 2)}`}</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total margin</td>
                <td>{`${formatNumber(headerValues.margin, 1)}%`}</td>
              </tr>
              <tr></tr>
              <tr></tr>
              <tr></tr>
              <tr>
                <td>Wholesaler/broker fee structure:</td>
                <td></td>
                <td></td>
                <td>Original avg.cost</td>
                <td>{`$ ${formatNumber(pureTotalCost / totalSold, 2)}`}</td>
                <td>per unit</td>
              </tr>
              <tr>
                <td>{values.feeType == '1' ? 'Purchase cost$' : values.feeType == '2' ? '% of sales' : 'Commissions per unit'}</td>
                <td>{values.feeType == '1' ? `$ ${values.feeCost}`: values.feeType == '2' ? `${values.feePercent}%` : `$${values.feeUnit}`}</td>
                <td></td>
                <td>{sortedOrderItems[0]?.variety}</td>
                <td>{`$ ${formatNumber(sortedOrderItems[0]?.cost, 2)}`}</td>
                <td>per unit</td>
              </tr>
              {
                sortedOrderItems.slice(1).map((item: any, index: number)=> (
                  <tr key={index}>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{item.variety}</td>
                    <td>{`$ ${formatNumber(item.cost, 2)}`}</td>
                    <td>per unit</td>
                  </tr>
                ))
              }
              <tr></tr>
              <tr>
                <td>Allocated costs:</td>
                <td></td>
                <td></td>
                <td>New avg.cost</td>
                <td>{`$ ${formatNumber(newAvgCost, 2)}`}</td>
                <td>per unit</td>
              </tr>
              <tr>
                <td>{values.extraChargeType == '1' ? 'Wholesaler/broker pays' : values.extraChargeType == '2' ? 'Vendor (shipper) pays' : 'Custom split payment' }</td>
                <td></td>
                <td></td>
                <td>{sortedOrderItems[0]?.variety}</td>
                <td>{`$ ${allZero ? formatNumber(newAvgCost, 2) : formatNumber(sortedOrderItems[0]?.cost * factor, 2)}`}</td>
                <td>per unit</td>
              </tr>
              {sortedOrderItems.slice(1).map((item: any, index: number) => (
                <tr key={index}>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>{item.variety}</td>
                  <td>{`$ ${allZero ? formatNumber(newAvgCost, 2) : formatNumber(item.cost * factor, 2)}`}</td>
                  <td>per unit</td>
                </tr>
              ))}
              <tr>
                <td>Liquidation method:</td>
              </tr>
              <tr>
                <td>{values.liquidationMethod == '1' ? 'Adjust purchase cost(s)' : 'Post adjustment as extra charge'}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </StyledWrap>
    </StyledModal>
  )
}

const SDescriptions = styled(Descriptions)`
  .ant-descriptions-row > th,
  .ant-descriptions-row > td {
    padding-bottom: 4px;
  }
  .ant-descriptions-item-label {
    color: rgba(0, 0, 0, 0.65);
  }
  .ant-descriptions-item-content {
    font-size: 16px;
    color: rgba(0, 0, 0, 0.85);
  }
  & + & {
    margin-top: 8px;
    padding-bottom: 12px;
    border-bottom: 1px solid #d8d8d8;
  }
`

const StyledDescriptions = styled(Descriptions)`
  border-bottom: 1px solid #d8d8d8;
  .ant-descriptions-item-label {
    width: 150px;
    text-align: left;
  }
  .ant-descriptions-item-content {
    width: 200px;
    text-align: right;
  }
`

const StyledModal = styled(Modal)`
  .ant-modal-body {
    padding: 0;
  }
`

const StyledRadioGroup = styled(Radio.Group)`
  .ant-radio-wrapper {
    height: 42px;
    line-height: 42px;
    display: block;
  }
`

const StyledWrap = styled.div`
  display: flex;
  font-weight: normal;

  .ft16 {
    font-size: 16px;
  }

  .title {
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    color: #373d3f;
    padding-bottom: 12px;
  }

  .left-table {
    margin-top: 8px;
    background: #fafafa;
    padding: 8px 10px;
    display: block;
    th {
      font-weight: normal;
    }
    td {
      text-align: center;
      padding-bottom: 8px;
      .ant-radio-wrapper:first-child {
        margin: 0 50px 0 20px;
      }
    }
  }

  .left {
    flex: 0 0 365px;
    padding: 15px 20px;
    .left-label {
      margin-top: 36px;
      &.first-label {
        margin-top: 20px;
      }
    }

    .left-cost {
      display: flex;
      padding: 5px 0;
      justify-content: space-between;
      &.first-cost {
        margin-top: 36px;
      }
    }
  }

  .right {
    flex: 1 1 943px;
    background: #fafafa;
  }
`

const StyledTitle = styled.div`
  padding: 15px 20px;
  margin-left: 10px;
  border-bottom: 1px solid #d8dbdb;
  display: flex;
  justify-content: space-between;
  align-items: center;

  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #373d3f;
  padding-bottom: 12px;
`
