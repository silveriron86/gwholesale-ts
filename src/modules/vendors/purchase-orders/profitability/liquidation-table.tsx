import React, { useMemo } from 'react'
import styled from '@emotion/styled'
import { Table } from 'antd'
import { v4 as uuidv4 } from 'uuid'
import { formatNumber } from '~/common/utils'

type PropsType = {
  headerValues: any
  wholesalerFee: any
  wholeCharges: any
  shipCharges: any
  extraShipTotal: any
  extraWholeTotal: any
  netLiquidation: any
  adjust: any
}

const emptyRowFn = (num: number) => {
  const res = []
  while (num > 0) {
    --num
    res.push({
      key: uuidv4(),
      hideName: true,
      hideTotal: true,
      hidePerUnit: true,
      hidePercent: true,
    })
  }
  return res
}

const columns1 = [
  {
    title: '',
    dataIndex: 'name',
    render: (val: any, row: any) => (row.hideName ? <span>&nbsp;</span> : <div style={{ minWidth: 166 }}>{val}</div>),
  },
  {
    title: 'Total',
    dataIndex: 'total',
    render: (val: number, row: any) => {
      if (row.hideTotal) {
        return null
      }
      if (val < 0) {
        return `-$${formatNumber(Math.abs(val), 2)}`
      }
      return `$${formatNumber(val, 2)}`
    },
  },
  {
    title: 'Per unit',
    dataIndex: 'perUnit',
    render: (val: any, row: any) => {
      if (row.hidePerUnit) {
        return null
      }
      if (val < 0) {
        return `-$${formatNumber(Math.abs(val), 2)}`
      }
      return `$${formatNumber(val, 2)}`
    },
  },
  {
    title: '% of sales',
    dataIndex: 'percent',
    render: (val: any, row: any) => (row.hidePercent ? null : val),
  },
]

export default function LiquidationTable(props: PropsType) {
  const {
    headerValues,
    wholesalerFee,
    wholeCharges,
    shipCharges,
    extraShipTotal,
    extraWholeTotal,
    netLiquidation,
    adjust,
  } = props

  const { totalSalesPrice, totalSold, pureTotalCost } = headerValues

  const { wRows, sRows } = useMemo(() => {
    const wRows = [...wholeCharges]
    const sRows = [...shipCharges]
    const wl = wRows.length
    const sl = sRows.length
    if (wl > sl) {
      sRows.push(...emptyRowFn(wl - sl))
    } else if (wl < sl) {
      wRows.push(...emptyRowFn(sl - wl))
    }
    return { wRows, sRows }
  }, [wholeCharges, shipCharges])

  // prettier-ignore
  const perUnitFn = (val:number) => (totalSold == 0 ? 0.00 : (val / totalSold))
  const percentFn = (val: number) => (totalSalesPrice == 0 ? '0.0%' : ((val * 100) / totalSalesPrice).toFixed(1) + '%')
  const format = (arr: any[]) =>
    arr.map((item) => ({
      ...item,
      perUnit: perUnitFn(item.total),
      percent: percentFn(item.total),
    }))

  const vendorData = format([
    {
      key: uuidv4(),
      name: 'Total sales',
      total: totalSalesPrice,
    },
    {
      key: uuidv4(),
      name: 'Original purchase cost',
      total: pureTotalCost,
    },
    {
      key: uuidv4(),
      name: 'Wholesaler/broker fees',
      total: wholesalerFee,
    },
    {
      key: uuidv4(),
      name: 'Allocated costs',
      total: extraShipTotal,
    },
    ...sRows,
    {
      key: uuidv4(),
      name: 'Net liquidation to shipper',
      total: netLiquidation,
    },
    {
      key: uuidv4(),
      name: 'Adjustment',
      total: adjust,
    },
  ])

  const wholesalerData = format([
    ...emptyRowFn(2),
    {
      key: uuidv4(),
      name: 'Wholesaler/broker fees',
      total: wholesalerFee,
    },
    {
      key: uuidv4(),
      name: 'Allocated costs',
      total: extraWholeTotal,
    },
    ...wRows,
    {
      key: uuidv4(),
      name: 'Net profit/commission',
      total: wholesalerFee - extraWholeTotal,
    },
  ])

  return (
    <StyledDiv>
      <div>
        <StyledTitle>Vendor (shipper)</StyledTitle>
        <StyledTable size="middle" pagination={false} columns={columns1} dataSource={vendorData} rowKey="key" />
      </div>
      <div>
        <StyledTitle>Wholesaler/broker</StyledTitle>
        <StyledTable size="middle" pagination={false} columns={columns1} dataSource={wholesalerData} rowKey="key" />
      </div>
    </StyledDiv>
  )
}

const StyledTable = styled(Table)`
  .ant-table-row > td {
    color: #373d3f !important;
  }
  td,
  th {
    border: none !important;
  }
`

const StyledDiv = styled.div`
  padding-bottom: 15px;
  display: flex;
  justify-content: space-between;
  > div {
    flex: 0 1 48%;
  }
`

const StyledTitle = styled.div`
  font-weight: bold;
  padding: 15px 0;
`
