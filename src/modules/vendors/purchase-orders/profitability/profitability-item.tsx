import * as React from 'react'
import _ from 'lodash'
import moment from 'moment'
import { Divider, Icon, Tooltip } from 'antd'
import { ThemeTable } from '~/modules/customers/customers.style'
import {
  formatNumber,
  ratioQtyToInventoryQty,
  ratioPriceToBasePrice,
  mathRoundFun,
  round,
  getCostPre
} from '~/common/utils'
import { ProfitabilityItemWrapper } from '../_style'
import { OrderDetail, OrderItem } from '~/schema'
import { FlexWrap, InsightBlock } from '~/modules/reports/reports.style'
import { Link } from 'react-router-dom'
type Props = {
  orderItem: OrderItem
  data: any
  currentOrder: OrderDetail | null,
  allocationData: any[]
  palletItem: any
  totalConfirmedQty: number
  totalReceivedQty: number
  totalChargesPrice: number
  totalPalletUnits: number
  updateTotalInfo: Function
}

export default class ProfitabilityItem extends React.PureComponent<Props> {
  state = {
    totalCatchWeight: 0,
    costCatchWeight: 0,
    totalSalePrice: 0,
    totalCost: 0,
    totalRemaining: 0,
    totalReceived: 0,
    totalCostPerUnit: 0,
    subTotalCost: 0,
    pureTotalCost: 0,
    allocationTotalCost: 0,
    returned: 0,
    sold: 0,
    adjusted: 0,
    lotLevelSubTotalPrice: 0,
    lotLevelSubTotalCost: 0
  }

  columns: any[] = [
    {
      title: 'SALES',
      dataIndex: 'companyName',
      width: '30%',
      align: 'left',
    },
    {
      title: 'Status',
      dataIndex: 'wholesaleOrderStatus',
      width: 100,
      align: 'left'
    },
    {
      title: 'Sell Date',
      dataIndex: 'deliveryDate',
      width: 200,
      align: 'left',
      render: (deliveryDate: number) => {
        if (deliveryDate) {
          return moment(deliveryDate).format('MM/DD/YYYY')
        }
        return ''
      },
    },
    {
      title: 'Order no.',
      dataIndex: 'wholesaleOrderId',
      align: 'left',
      width: 200,
      render: (orderId: any) => {
        return (
          <Link to={`/sales-order/${orderId}`}>
            <span>{orderId}</span>
          </Link>
        )
      }
    },
    {
      title: 'Qty',
      dataIndex: 'catchWeightQty',
      align: 'left',
      // width: 200,
      width: '10%',
      render: (catchWeightQty: number, record: any) => {
        return `${round(catchWeightQty)} ${record.uom}`
      },
    },
    {
      title: 'Price/Unit',
      dataIndex: 'salePrice',
      align: 'right',
      width: 200,
      render: (salePrice: number, record: any) => {
        return `$${formatNumber(salePrice, 2)}/${record.uom}`
      },
    },
    {
      title: 'Total Sales',
      dataIndex: 'totalSales',
      align: 'right',
      render: (totalSales: number) => {
        return `${totalSales < 0 ? '-' : ''}$${formatNumber(Math.abs(totalSales), 2)}`
      },
    }
  ]

  costColumns: any[] = [
    {
      title: 'COSTS',
      dataIndex: 'type',
      width: '30%',
      align: 'left',
    },
    {
      title: '',
      dataIndex: 'k',
      width: 100,
      align: 'left',
    },
    {
      title: 'Purchase Date',
      dataIndex: 'deliveryDate',
      width: 200,
      align: 'left',
      render: (deliveryDate: number) => {
        if (deliveryDate) {
          return moment(deliveryDate).format('MM/DD/YYYY')
        }
        return ''
      },
    },
    {
      title: 'Order no.',
      dataIndex: 'wholesaleOrderId',
      align: 'left',
      width: 200,
      render: (orderId: any) => {
        return (
          <Link to={`/order/${orderId}/purchase-cart`}>
            <span>{orderId}</span>
          </Link>
        )
      }
    },
    {
      title: 'Qty',
      dataIndex: 'totalQty',
      align: 'left',
      // width: 200,
      width: '10%',
      render: (totalQty: number, record: any) => {
        return totalQty ? `${round(totalQty)} ${record.uom}` : ''
      },
    },
    {
      title: 'Cost/Unit',
      dataIndex: 'cost',
      align: 'right',
      width: 200,
      render: (cost: any, record: any) => {
        if (record.dataType == 'statistic') {
          return cost
        }
        const value = mathRoundFun(cost, 3)
        return `$${formatNumber(value, 2)}/${record.uom}`
      },
    },
    {
      title: 'Total Costs',
      dataIndex: 'totalCost',
      align: 'right',
      render: (totalCost: number, record: any) => {
        if (record.dataType == 'statistic') return ''
        const total = record.cost * record.totalQty
        return total ? `$${formatNumber(total, 2)}` : ''
      },
    }
  ]

  formatDataSource = () => {
    const { orderItem, data } = this.props
    let dataSource: any[] = []
    let salesChildItems = data ? data.salesOrderItems.filter((o: any) => o.lotId == orderItem.lotId) : []
    const costingUOM = /*orderItem.overrideUOM ? orderItem.overrideUOM : */orderItem.inventoryUOM
    const adjustmentItems: any[] = data ? data.itemAdjustments.filter(
      (o: any) => o.lotId == orderItem.lotId && o.adjustmentType == 1
    ).map((o: any) => {
      const uom = o.overrideUOM ? o.overrideUOM : o.inventoryUOM
      let salePrice = o.salePrice
      const found = data.salesOrderItems.find(so => so.wholesaleOrderItemId === o.wholesaleOrderItemId)
      if(found) {
        salePrice = found.salePrice
      }
      return {
        ...o,
        uom,
        companyName: `Return: ${o.companyName}`,
        catchWeightQty: costingUOM == uom ? -o.catchWeightQty : -ratioQtyToInventoryQty(uom, o.catchWeightQty, o.wholesaleItem),
        salePrice: costingUOM == uom ? salePrice : ratioPriceToBasePrice(uom, salePrice, o.wholesaleItem),
        returned: costingUOM == uom ? o.catchWeightQty : ratioQtyToInventoryQty(uom, o.catchWeightQty, o.wholesaleItem),
      }
    }) : []
    const productAdjusted: any[] = data ? data.itemAdjustments.filter(
      (o: any) => o.lotId == orderItem.lotId && o.adjustmentType == 2
    ) : []
    salesChildItems = salesChildItems.map((el: any) => {
      const uom = el.overrideUOM ? el.overrideUOM : el.inventoryUOM
      const subCatchWeight = el.catchWeightQty
      const catchWeightQty = el.wholesaleOrderStatus == 'SHIPPED' ? subCatchWeight : el.orderQty
      return {
        ...el,
        uom,
        companyName: el.wholesaleClient ? el.wholesaleClient.clientCompany.companyName : '',
        deliveryDate: moment(el.deliveryDate).format('MM/DD/YYYY'),
        catchWeightQty: el.catchWeightQty,
        salePrice: costingUOM == uom ? el.salePrice : ratioPriceToBasePrice(uom, el.salePrice, el.wholesaleItem)
      }
    })
    const sold = _.sumBy(salesChildItems, 'catchWeightQty')
    dataSource = [...salesChildItems, ...adjustmentItems]
    const returned = _.sumBy(adjustmentItems, function (o) {
      return o.returnInInventory ? o.returned : 0
    })
    const totalCatchWeight = _.sumBy(dataSource, 'catchWeightQty')
    const costCatchWeight = _.sumBy(dataSource, (o) => o.returnInInventory===false? 0:o.catchWeightQty)
    const totalSalePrice = _.sumBy(dataSource, function (o) {
      return o.catchWeightQty * o.salePrice
    })
    const productAdjustTotal = _.sumBy(productAdjusted, 'catchWeightQty')

    let totalSales = 0
    dataSource = dataSource.map((record: any) => {
      const rowTotal = record.salePrice.toFixed(2) * record.catchWeightQty.toFixed(2);
      totalSales += rowTotal
      return {
        ...record,
        totalSales: rowTotal
      }
    })

    const total = {
      companyName: 'Total sales',
      catchWeightQty: totalCatchWeight,
      salePrice: totalCatchWeight != 0 ? totalSalePrice / totalCatchWeight : 0,
      totalCost: totalSalePrice,
      uom: costingUOM,
      totalSales,
    }
    this.props.updateTotalInfo({
      orderItemId: orderItem.wholesaleOrderItemId,
      totalSales,
      totalSold: sold,
      totalReceived: orderItem.receivedQty
    })

    console.log('productAdjustTotal', productAdjustTotal)
    this.setState({
      totalRemaining: orderItem.receivedQty - sold + returned + productAdjustTotal,
      totalSalePrice: totalSalePrice,
      lotLevelSubTotalPrice: total.salePrice,
      totalReceived: orderItem.receivedQty,
      adjusted: productAdjustTotal,
      totalCatchWeight,
      costCatchWeight,
      returned,
      sold
    })
    return dataSource.length ? [...dataSource, { ...total }] : []
  }

  formatCostDataSource = () => {
    const {
      currentOrder,
      orderItem,
      allocationData,
      palletItem,
      totalChargesPrice,
      totalPalletUnits,
      totalReceivedQty,
      totalConfirmedQty
    } = this.props
    const { costCatchWeight } = this.state
    if (!currentOrder) return
    const uom = /*orderItem.overrideUOM ? orderItem.overrideUOM : */orderItem.inventoryUOM
    const itemCost = {
      type: 'Item cost',
      deliveryDate: moment(orderItem.deliveryDate).format('MM/DD/YYYY'),
      wholesaleOrderId: currentOrder ? currentOrder.wholesaleOrderId : '',
      totalQty: costCatchWeight,
      cost: orderItem.cost,
      uom
    }
    console.log('allocation data', allocationData, palletItem, totalChargesPrice)
    let allocations: any[] = []
    const qty = orderItem.status == 'RECEIVED' ? totalReceivedQty : totalConfirmedQty;
    allocationData.forEach((v: any) => {
      if (v.isChecked) {
        const allocationCost = palletItem && qty ? getCostPre(palletItem, currentOrder.allocateType, totalChargesPrice, qty, totalPalletUnits) : 0
        const itemName = v.itemName ? `${v.itemName}${!v.chargeDesc ? '' : `(${v.chargeDesc})`}` : ''
        console.log('allco cost', allocationCost)
        allocations.push({
          ...v,
          type: v.relatedBillVendorName || v.vendorName || itemName,
          cost: totalChargesPrice && v.price ? _.divide(v.price, totalChargesPrice) * allocationCost : 0,
          wholesaleOrderId: v.chargeFrom ? v.chargeFrom : currentOrder?.wholesaleOrderId,
          totalQty: costCatchWeight,
          deliveryDate: v.deliveryDate ? v.deliveryDate : currentOrder.deliveryDate,
          uom
        })
      }
    })
    const totalCost = _.sumBy([itemCost, ...allocations], 'cost')
    const allocationTotalCost = _.sumBy(allocations, 'cost')

    const total = {
      type: 'Total costs',
      totalQty: costCatchWeight,
      cost: totalCost,
      totalCost: totalCost * costCatchWeight,
      uom
    }
    this.props.updateTotalInfo({
      orderItemId: orderItem.wholesaleOrderItemId,
      subTotalCost: total.totalCost,
      pureTotalCost: orderItem.cost * costCatchWeight,
      allocationTotalCost: allocationTotalCost * costCatchWeight,
    })
    this.setState({
      totalCostPerUnit: totalCost,
      subTotalCost: total.totalCost,
      pureTotalCost: orderItem.cost * costCatchWeight,
      allocationTotalCost: allocationTotalCost * costCatchWeight,
      lotLevelSubTotalCost: total.cost,
      totalCost: totalCost * orderItem.receivedQty
    })
    console.log([itemCost, ...allocations, total])
    return [itemCost, ...allocations, total]
  }

  getStatistic = () => {
    const { orderItem } = this.props
    const { lotLevelSubTotalPrice, lotLevelSubTotalCost } = this.state
    const uom = /*orderItem.overrideUOM ? orderItem.overrideUOM : */orderItem.inventoryUOM
    const profit = lotLevelSubTotalPrice - lotLevelSubTotalCost
    const total = [
      {
        type: 'Profit (unit-level)',
        cost: `${profit < 0 ? '-' : ''}$${formatNumber(Math.abs(profit), 2)}/${uom}`,
        dataType: 'statistic',
        deliveryDate: '',
        totalQty: '',
        totalCost: ''
      },
      {
        type: 'Margin (unit-level)',
        cost: `${lotLevelSubTotalPrice == 0 ? '0.0' : mathRoundFun((profit / lotLevelSubTotalPrice) * 100, 1)}%`,
        dataType: 'statistic',
        deliveryDate: '',
        totalQty: '',
        totalCost: ''
      }
    ]
    return total
  }

  render() {
    const { orderItem } = this.props
    const {
      totalCatchWeight,
      totalSalePrice,
      totalCost,
      totalRemaining,
      totalReceived,
      subTotalCost,
      pureTotalCost,
      allocationTotalCost,
      adjusted,
      returned,
      sold
    } = this.state
    const dataSource = this.formatDataSource()
    const costDataSource = this.formatCostDataSource()
    const statistic = this.getStatistic()
    const insightStyle = {paddingBottom: 16}

    return (
      <ProfitabilityItemWrapper>
        <Divider style={{ height: 3 }} />
        <div style={{ paddingBottom: 48 }}>
          <div className="item-title"><Link to={`/product/${orderItem.itemId}/specifications`}>{orderItem.variety}</Link> (Lot {orderItem.lotId})</div>
          <FlexWrap style={{ marginBottom: 48 }}>
            <InsightBlock style={{...insightStyle, marginLeft: 0}}>
              <div className="label">
                Units sold
                <Tooltip
                  title={
                    <div>
                      <div>{round(totalReceived)} units received</div>
                      <div>{round(sold)} units sold</div>
                      <div>{round(returned)} units returned</div>
                      <div>{round(adjusted)} units adjusted</div>
                    </div>
                  }>
                  <Icon type="info-circle" style={{ marginLeft: '8px', marginTop: '-3px' }} />
                </Tooltip>
              </div>
              <div className="value">
                {round(totalReceived - totalRemaining)}/{round(totalReceived)}
              </div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Percentage sold</div>
              <div className="value">{totalReceived ? formatNumber((sold) / totalReceived * 100, 2) : 0}%</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total sales</div>
              <div className="value">${formatNumber(totalSalePrice, 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Product cost</div>
              <div className="value">${formatNumber(pureTotalCost, 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Allocated costs</div>
              <div className="value">${formatNumber(allocationTotalCost, 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total costs</div>
              <div className="value">${formatNumber(subTotalCost, 2)}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total profit</div>
              <div className="value"> {totalSalePrice - subTotalCost < 0 ? `-$${formatNumber(Math.abs(totalSalePrice - subTotalCost), 2)}` : `$${formatNumber(totalSalePrice - subTotalCost, 2)}`}</div>
            </InsightBlock>
            <InsightBlock style={insightStyle}>
              <div className="label">Total margin</div>
              <div className="value">{totalSalePrice ? formatNumber((totalSalePrice - subTotalCost) / totalSalePrice * 100, 2) : 0}%</div>
            </InsightBlock>
          </FlexWrap>
          <div style={{width: '80%'}}>
            <ThemeTable
              columns={this.columns}
              dataSource={dataSource}
              pagination={false}
              bordered={false}
              rowKey={"wholesaleOrderItemId"}
            />
            <Divider />
            <ThemeTable
              columns={this.costColumns}
              dataSource={costDataSource}
              pagination={false}
              bordered={false}
              rowKey={"wholesaleOrderItemId"}
            />
            <Divider />
            <ThemeTable
              className="statistic"
              showHeader={false}
              columns={this.costColumns}
              dataSource={statistic}
              pagination={false}
              bordered={false}
              rowKey={"type"}
            />
          </div>
        </div>
      </ProfitabilityItemWrapper>
    )
  }
}
