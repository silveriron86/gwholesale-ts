import React from 'react'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import PageLayout from '~/components/PageLayout'
import { PurchaseNavHeader } from '../_components/nav-header'
import { CustomersInfoBody, layoutStyle } from '~/modules/customers/components/customers-detail/customers-detail.style'
import {
  CustomerDetailsTitle as DetailsTitle,
  CustomerDetailsWrapper as DetailsWrapper,
} from '~/modules/customers/customers.style'
import { Layout } from 'antd'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { connect } from 'redux-epics-decorator'
import { OrderItem } from '~/schema'
import { ReceivingContainer } from './receiving-container'
import moment from 'moment'

type ReceivingProps = OrdersDispatchProps &
  OrdersStateProps & /*RouteComponentProps<{ orderId: string }> & */ {
    theme: Theme
  }

export default class Receiving extends React.PureComponent<ReceivingProps> {
  state: any
  constructor(props: ReceivingProps) {
    super(props)
    this.state = {
      orderItemId: -1,
    }
  }

  componentDidMount() {
    // const orderId = this.props.match.params.orderId
    // this.props.getOrderDetail(orderId)
    // this.props.getOrderItemsById(orderId)
    this.props.getSellerSetting()
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.state.orderItemId > -1) {
      if (this.props.orderItems.length && nextProps.orderItems.length) {
        const oldItem = this.props.orderItems.find((el) => el.wholesaleOrderItemId == this.state.orderItemId)
        const newItem = nextProps.orderItems.find((el) => el.wholesaleOrderItemId == this.state.orderItemId)
        if (oldItem && newItem && oldItem.palletQty != newItem.palletQty) {
          this.props.getPallets(this.state.orderItemId)
        }
      }
    }
  }

  handleSave = (item: OrderItem) => {
    this.props.handlerOrderItemsRedux(item)
    this.props.updateOrderItemWithoutRefresh(item)
    // console.log(item)
    // if(parseInt(item.receivedQty, 10) > 0) {
    //   // When a unit confirm is enter, order item should move to confirmed status.
    //   const {currentOrder} = this.props
    //   if(currentOrder) {
    //     setTimeout(() => {
    //       this.props.updateOrderInfo({
    //         wholesaleOrderId: currentOrder.wholesaleOrderId,
    //         deliveryDate: moment(currentOrder.deliveryDate).format('MM/DD/YYYY'),
    //         wholesaleCustomerClientId: currentOrder.wholesaleClient.clientId,
    //         status: 'ARRIVED',
    //       })
    //     }, 600)
    //   }
    // }
  }

  handleSaveOrderInfo = (orderInfo: any) => {
    this.props.updateOrderLocked(orderInfo)
  }

  onShowRow = (id: number) => {
    this.setState({
      orderItemId: id,
    })
  }

  getSheets = (id) => {
    console.log('call weight sehet', id)
    this.props.getWeightSheets(id)
  }

  render() {
    const { currentOrder, orderItems, orderItem } = this.props
    const { orderItemId } = this.state
    const orderId = this.props.match.params.orderId

    if (!currentOrder) {
      return <PageLayout currentTopMenu={'menu-Purchasing-Purchase Orders'} />
    }

    return (
      <div style={{ padding: '15px 40px 0 25px', overflowX: 'auto' }}>
        {/* <DetailsTitle>Receiving</DetailsTitle> */}
        <ReceivingContainer
          orderId={orderId}
          orderItemId={orderItemId}
          orderItems={orderItems}
          handleSave={this.handleSave}
          handleShow={this.onShowRow}
          handleSaveOrderInfo={this.handleSaveOrderInfo}
          currentOrder={currentOrder}
          getPallets={this.props.getPallets}
          pallets={this.props.pallets}
          updatePalletDetail={this.props.updatePalletDetail}
          vendors={this.props.vendors}
          updateAllPalletDetail={this.props.updateAllPalletDetail}
          sellerSetting={this.props.sellerSetting}
          getWeightSheets={this.getSheets}
          totalPalletsInfo={this.props.totalPalletsInfo}
          getTotalUnitsByAllPallets={this.props.getTotalUnitsByAllPallets}
          weightSheet={this.props.weightSheet}
          saveWeightSheet={this.props.saveWeightSheet}
          getOrderItemByOrderItemId={this.props.getOrderItemByOrderItemId}
          updatePOIReceived={this.props.updatePOIReceived}
          orderItem={orderItem}
          companyProductTypes={this.props.companyProductTypes}
          getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
          updateProduct={this.props.updateProduct}
          setCompanyProductType={this.props.setCompanyProductType}
          setOrderItemsReceived={this.props.setOrderItemsReceived}
          updatingReceived={this.props.updatingReceived}
        />
      </div>
    )
  }
}

// const mapStateToProps = (state: GlobalState) => state.orders
// export default withTheme(connect(OrdersModule)(mapStateToProps)(Receiving))
