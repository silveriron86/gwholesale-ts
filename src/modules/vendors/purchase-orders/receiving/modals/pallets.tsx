import { DatePicker, Icon, InputNumber, Select, Modal } from 'antd'
import * as React from 'react'
import moment, { Moment } from 'moment'
import {
  DialogSubContainer,
  FlexDiv,
  DialogBodyDiv,
  Description,
  ValueLabel,
  ItemHMargin4,
} from '~/modules/customers/nav-sales/styles'
import { Item, ItemValue, NewLabelModal, PalletTableWrapper } from './../../_style'
import { Flex, ThemeButton, ThemeOutlineButton, ThemeIconButton, ThemeTable } from '~/modules/customers/customers.style'
import { Pallet } from '~/schema'
import jQuery from 'jquery'
import { CustomButton } from '~/components'
import PrintLabels from '../print-labels'
import { fullButton } from '~/modules/customers/sales/_style'
import { printWindow } from '~/common/utils'
import PrintLotLabels from '../print-lot-label'
import NewPrintLabel from '../newPrintLabel/NewPrintLotLabel'
import NewPrintLotLabel from '../newPrintLabel/NewPrintLotLabel'
import NewPrintPalletLabel from '../newPrintLabel/NewPrintPalletLabel'

type ReceivingPalletizeProps = {
  palletizeItem: any
  visible: boolean
  getPallets: Function
  pallets: any[]
  onSave: Function
  updatePalletDetail: Function
  vendors: any[]
  updateAllPalletDetail: Function
  onClose: Function
  sellerSetting: any
}

class ReceivingPalletize extends React.PureComponent<ReceivingPalletizeProps> {
  state = {
    dataSource: [],
    allBestByDate: '',
    printLabelsReviewShow: false,
    printLotLabelPreview: false,
    printTestLabelsReviewShow: false, // test new label state
    printLabelsRef: null,
  }
  changeIds: number[] = []
  columns: any[] = [
    {
      title: 'PALLET #',
      dataIndex: 'displayNumber',
    },
    {
      title: 'UNITS/PALLET',
      dataIndex: 'palletUnits',
      render: (units: any, record: any, index: number) => {
        const val = units ? parseInt(units.toString()) : 0
        return (
          <Flex>
            <InputNumber
              min={0}
              step={1}
              precision={0}
              style={{ width: 60 }}
              value={val}
              onChange={(value) => this.onChangeUnits(value, record)}
              onBlur={(evt) => this.onPalletUnitsChange(evt, record)}
              onFocus={(evt) => this.onFocusToPalletUnit(evt)}
            />
            {index === 0 && (
              <ThemeIconButton
                className="no-border black"
                type="link"
                style={{ marginLeft: 10 }}
                onClick={() => this.copyUnits(val)}
              >
                <Icon type="copy" style={ItemHMargin4} className="copy-units" />
              </ThemeIconButton>
            )}
          </Flex>
        )
      },
    },
    {
      title: 'COMPANY NAME',
      dataIndex: 'companyName',
      width: '30%',
      render: (companyName: string, record: any) => {
        // return (
        //   <ThemeSelect
        //     style={{ width: '90%' }}
        //     showSearch
        //     filterOption={(input: any, option: any) =>
        //       option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        //     }
        //     onChange={(value: string, opt: any) => this.handleSelectCompany(value, record)}
        //     value={companyName}
        //   >
        //     {this.renderCompanyNames()}
        //   </ThemeSelect>
        // )
        return companyName
      },
    },
    {
      title: 'BEST BY DATE',
      dataIndex: 'bestByDate',
      width: '25%',
      render: (bestByDate: any, record: any) => {
        return (
          <DatePicker
            value={moment(bestByDate)}
            onChange={(date, dateStr) => this.changeBestByDate(date, dateStr, record)}
          />
        )
      },
    },
  ]
  componentDidMount() {
    const { palletizeItem } = this.props
    if (palletizeItem && palletizeItem.wholesaleOrderItemId) {
      this.props.getPallets(this.props.palletizeItem.wholesaleOrderItemId)
    }
    if (this.props.pallets && this.props.pallets.length) {
      this.setState({ dataSource: this.props.pallets })
    }
  }

  componentWillReceiveProps(nextProps: ReceivingPalletizeProps) {
    if (JSON.stringify(this.props.pallets) != JSON.stringify(nextProps.pallets)) {
      this.setState({ dataSource: nextProps.pallets })
    }
    if (nextProps.visible) {
      this.changeIds = []
    }
  }

  copyUnits = (units: number) => {
    let dataSource = JSON.parse(JSON.stringify(this.state.dataSource))
    this.changeIds = []
    dataSource.forEach((row: any) => {
      row.palletUnits = units
      this.changeIds.push(row.id)
    })
    this.setState({
      dataSource,
    })
  }

  onBestByDateChange = (date: Moment | null, dateString: string) => {
    const newData = [...this.state.dataSource] as Pallet[]
    newData.forEach((el: any) => {
      el.bestByDate = moment(date).format('MM/DD/YYYY')
    })
    this.setState(
      {
        allBestByDate: date,
        dataSource: newData,
      },
      () => {
        this.changeIds = newData.map((el) => {
          return el.id
        })
      },
    )
  }

  onChangeUnits = (value: any, record: any) => {
    record.palletUnits = value
    this.changePalletDetail(record)
  }

  onPalletUnitsChange = (evt: any, record: any) => {
    const qty = evt.target.value
    if (record.palletUnits != qty) {
      record.palletUnits = qty
      this.changePalletDetail(record)
    }
  }

  onFocusToPalletUnit = (evt: any) => {
    jQuery(evt.target).trigger('select')
  }

  onChangePalletQty = (evt: any) => {
    const qty = evt.target.value
    this.AddOrRemovePallet(qty)
  }

  AddOrRemovePallet = (qty: number) => {
    if (this.props.palletizeItem.palletQty != qty) {
      const row = { ...this.props.palletizeItem, palletQty: qty }
      this.props.onSave(row, 'palletQty')
      setTimeout(() => {
        this.props.getPallets(this.props.palletizeItem.wholesaleOrderItemId)
      }, 1000)
    }
  }

  changePalletDetail = (row: Pallet) => {
    const newData = [...this.state.dataSource] as Pallet[]
    const index = newData.findIndex((item: Pallet) => row.id === item.id)
    const item = newData[index]
    if (this.changeIds.indexOf(row.id) == -1) {
      this.changeIds.push(row.id)
    }
    newData.splice(index, 1, {
      ...item,
      ...row,
    })

    this.setState({ dataSource: newData })
    // this.props.updatePalletDetail({
    //   orderItemId: this.props.palletizeItem.wholesaleOrderItemId,
    //   palletId: row.id,
    //   palletUnits: row.palletUnits,
    //   companyName: row.companyName,
    //   bestByDate: moment(row.bestByDate).format('MM/DD/YYYY'),
    // })
  }

  changeBestByDate = (m: Moment | null, str: string, row: any) => {
    row.bestByDate = moment(str).format('MM/DD/YYYY')
    this.changePalletDetail(row)
  }

  renderCompanyNames = () => {
    const { vendors } = this.props
    if (vendors.length > 0) {
      return vendors.map((row) => {
        return (
          <Select.Option key={`vendor-${row.clientId}`} value={row.clientCompany.companyName}>
            {row.clientCompany.companyName}
          </Select.Option>
        )
      })
    } else {
      return []
    }
  }

  handleSelectCompany = (_value: string, row: any) => {
    if (row.companyName != _value) {
      row.companyName = _value
      this.changePalletDetail(row)
    }
  }

  onSavePalletDetails = () => {
    if (this.changeIds.length) {
      const changedData = this.state.dataSource
        .filter((el: any) => this.changeIds.indexOf(el.id) > -1)
        .map((el: any) => {
          return {
            ...el,
            bestByDate: moment(el.bestByDate).format('MM/DD/YYYY'),
          }
        })
      this.changeIds = []
      this.props.updateAllPalletDetail({ data: changedData })
    }
    this.props.onClose()
  }

  openPrintModal = () => {
    this.setState({
      printLabelsReviewShow: true,
    })
  }

  openLotPrintModal = () => {
    this.setState({
      printLotLabelPreview: true,
    })
  }

  openPrintTestModal = () => {
    this.setState({
      printTestLabelsReviewShow: true,
    })
  }

  closePrintModal = () => {
    this.setState({
      printLabelsReviewShow: false,
      printLotLabelPreview: false,
      printTestLabelsReviewShow: false,
    })
  }

  printContent: any = () => {
    return this.state.printLabelsRef
  }

  render() {
    const { palletizeItem, sellerSetting } = this.props

    return palletizeItem ? (
      <>
        <DialogSubContainer className="bordered">
          <FlexDiv className="space-between">
            <div style={{ maxWidth: '40%' }}>
              <Item>ITEM</Item>
              <ItemValue>{palletizeItem.variety}</ItemValue>
            </div>
            <div>
              <Item># OF PALLETS</Item>
              <InputNumber value={this.state.dataSource.length} onBlur={(e) => this.onChangePalletQty(e)} />
            </div>
            <div>
              <Item>BEST BY DATE</Item>
              <DatePicker
                value={this.state.allBestByDate ? moment(this.state.allBestByDate) : ''}
                onChange={this.onBestByDateChange}
              />
            </div>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv style={{ padding: '16px 8px' }}>
          <PalletTableWrapper>
            <ThemeTable
              className="pallets-table"
              columns={this.columns}
              dataSource={this.state.dataSource}
              scroll={{ y: 400 }}
              pagination={false}
            />
            <ThemeOutlineButton
              style={{ marginTop: 16 }}
              onClick={() => this.AddOrRemovePallet(this.state.dataSource.length + 1)}
            >
              ADD PALLET
            </ThemeOutlineButton>
          </PalletTableWrapper>
        </DialogBodyDiv>
        <DialogSubContainer style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <ThemeButton type="primary" onClick={this.onSavePalletDetails}>
              Save
            </ThemeButton>
            <ThemeOutlineButton style={{ marginLeft: 12 }} onClick={this.props.onClose}>
              CANCEL
            </ThemeOutlineButton>
          </div>
          <div>
            <ThemeButton shape="round" onClick={this.openLotPrintModal} style={{ marginRight: 12 }}>
              <Icon type="printer" />
              Print Lot Labels
            </ThemeButton>
            <ThemeButton shape="round" onClick={this.openPrintModal}>
              <Icon type="printer" />
              Print Pallet Labels
            </ThemeButton>
          </div>
        </DialogSubContainer>

        {/*<Modal width={688} footer={null} visible={this.state.printLabelsReviewShow} onCancel={this.closePrintModal}>
          <div id={'PrintLabelsModal1'} style={{ padding: 20 }}>
            <PrintLabels
              type="pallet"
              dataSource={this.state.dataSource}
              ref={(el) => {
                this.setState({ printLabelsRef: el })
              }}
              sellerSetting={sellerSetting}
            />
          </div>
          <CustomButton
            style={fullButton}
            onClick={() => printWindow('PrintLabelsModal1', this.printContent.bind(this))}
          >
            <Icon type="printer" theme="filled" />
            Print Labels
          </CustomButton>
            </Modal>*/}

        {/*<Modal width={688} footer={null} visible={this.state.printLotLabelPreview} onCancel={this.closePrintModal}>
          <div id={'PrintLabelsModal2'} style={{ padding: 20 }}>
            <PrintLotLabels
              dataSource={this.state.dataSource}
              ref={(el) => {
                this.setState({ printLabelsRef: el })
              }}
              sellerSetting={sellerSetting}
            />
          </div>
          <CustomButton
            style={fullButton}
            onClick={() => printWindow('PrintLabelsModal2', this.printContent.bind(this))}
          >
            <Icon type="printer" theme="filled" />
            Print Labels
          </CustomButton>
            </Modal>*/}

        <NewLabelModal
          style={{ width: '703px', height: '930px', padding: '0px' }}
          width={703}
          footer={null}
          visible={this.state.printLotLabelPreview}
          onCancel={this.closePrintModal}
        >
          <NewPrintLotLabel dataSource={this.state.dataSource} sellerSetting={sellerSetting} props={this.props} />
        </NewLabelModal>
        <NewLabelModal
          style={{ width: '703px', height: '930px', padding: '0px' }}
          width={703}
          footer={null}
          visible={this.state.printLabelsReviewShow}
          onCancel={this.closePrintModal}
        >
          <NewPrintPalletLabel dataSource={this.state.dataSource} sellerSetting={sellerSetting} props={this.props} />
        </NewLabelModal>
      </>
    ) : (
      ''
    )
  }
}

export default ReceivingPalletize
