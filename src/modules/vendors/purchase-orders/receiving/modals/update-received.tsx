import { DatePicker, Icon, InputNumber, Select, Modal } from 'antd'
import { ThemeCheckbox, ThemeTable } from '~/modules/customers/customers.style'
import { RedSpan } from '~/modules/customers/customers.style'

import * as React from 'react'

type UpdateReceivedProps = {
  orderItem: any
  newReceivedQty: string
  newOrderWeight: string
  understandCheckBox: boolean
  changeUnderstandCheckBoxStatus: Function
  isEditOrderWeight: boolean
}

class UpdateReceivedModalBody extends React.PureComponent<UpdateReceivedProps> {
  state = {}

  columns: any[] = [
    {
      title: 'Lot #',
      dataIndex: 'lotId',
      align: 'center',
      width: 130,
    },
    {
      title: 'Item',
      dataIndex: 'variety',
      align: 'center',
      width: 220,
      render: (variety: any, record: any) => {
        return record.wholesaleItem.variety
      },
    },
    {
      title: `${this.props.isEditOrderWeight ? 'Units Net Weight (Original/New)' : 'Units Received (Original/New)'}`,
      dataIndex: 'receivedQty',
      align: 'center',
      width: 220,
      render: (receivedQty: number, record: any) => {
        if (this.props.isEditOrderWeight) {
          if (this.props.newOrderWeight != null) {
            return `${record.orderWeight}/${this.props.newOrderWeight}`
          }
          return receivedQty
        } else {
          if (this.props.newReceivedQty != null) {
            return `${receivedQty}/${this.props.newReceivedQty}`
          }
          return receivedQty
        }
      },
    },
    {
      title: 'Available to sell (Original/New)',
      dataIndex: 'lotAvailableQty',
      align: 'center',
      width: 220,
      render: (lotAvailableQty: number, record: any) => {
        if ((this.props.isEditOrderWeight ? this.props.newOrderWeight : this.props.newReceivedQty) != null) {
          let newValue = 0
          // console.log(record)
          if (
            record.wholesaleOrderItemStatus == 'CONFIRMED' &&
            record.qtyConfirmed != record.lotAvailableQty &&
            record.receivedQty == 0
          ) {
            newValue = parseFloat(this.props.newReceivedQty || this.props.newOrderWeight) - (record.qtyConfirmed - lotAvailableQty)
          } else {
            if (record.receivedQty != this.props.newReceivedQty || this.props.newOrderWeight){
              newValue = parseFloat(this.props.newReceivedQty || this.props.newOrderWeight)
            } else {
              newValue = parseFloat(this.props.newReceivedQty || this.props.newOrderWeight) - (record.receivedQty - lotAvailableQty)
            }
          }
          if (newValue < 0) {
            return (
              <>
                {lotAvailableQty}/<RedSpan>{newValue}</RedSpan>
              </>
            )
          } else {
            return `${lotAvailableQty}/${newValue}`
          }
        }
        return lotAvailableQty
      },
    },
    {
      title: 'Units on hand (Original/New)',
      dataIndex: 'onHandQty',
      align: 'center',
      width: 220,
      render: (onHandQty: number, record: any) => {
        if ((this.props.isEditOrderWeight ? this.props.newOrderWeight : this.props.newReceivedQty) != null) {
          const newValue = parseFloat(this.props.newReceivedQty || this.props.newOrderWeight) - ((this.props.isEditOrderWeight ? record.orderWeight : record.receivedQty) - onHandQty)
          if (newValue < 0) {
            return (
              <>
                {onHandQty}/<RedSpan>{newValue}</RedSpan>
              </>
            )
          } else {
            return `${onHandQty}/${newValue}`
          }
        }
        return onHandQty
      },
    },
  ]
  componentDidMount() {}

  render() {
    const { orderItem } = this.props
    console.log(this.props.newOrderWeight)

    return (
      <>
        {!this.props.isEditOrderWeight ? <p style={{fontWeight: 'normal'}}>
          Units from this lot have already been added to sales orders. Please confirm you want to modify the units received.
          </p> :
          <p style={{fontWeight: 'normal'}}>
            Units from this lot have already been added to sales orders. Please confirm you want to modify the units received.
          </p>
        }
        <ThemeTable pagination={false} columns={this.columns} dataSource={orderItem} rowKey="wholesaleOrderItemId" />
        <p className="mt20 ml20" style={{fontWeight: 'normal', marginBottom: 0}}>
          Pallets and unit weights may have already been generated for this lot, please make the necessary modifications
          after submitting this change.
          <ThemeCheckbox style={{marginTop: 10, display: 'block'}} onChange={this.props.changeUnderstandCheckBoxStatus} checked={this.props.understandCheckBox}>
            I understand
          </ThemeCheckbox>
        </p>
      </>
    )
  }
}

export default UpdateReceivedModalBody
