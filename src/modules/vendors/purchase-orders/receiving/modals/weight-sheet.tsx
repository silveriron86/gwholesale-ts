import { Icon, Modal, Select, Tooltip } from 'antd'
import { cloneDeep } from 'lodash'
import * as React from 'react'
import {
  InputLabel,
  ThemeButton,
  ThemeInput,
  ThemeOutlineButton,
  ThemeSelect,
  ThemeSwitch,
} from '~/modules/customers/customers.style'
import { NumericInput } from '~/modules/customers/nav-sales/order-detail/modals/picked-quanitty'
import {
  DialogSubContainer,
  FlexDiv,
  DialogBodyDiv,
  Description,
  ValueLabel,
  Flex,
  Item,
  ItemHMargin4,
} from '~/modules/customers/nav-sales/styles'
import { Item as LabelItem, ItemValue, NewLabelModal } from './../../_style'
import jQuery from 'jquery'
import { formatNumber, printWindow } from '~/common/utils'
import PrintLabels from '../print-labels'
import { CustomButton } from '~/components'
import { fullButton } from '~/modules/customers/sales/_style'
import NewPrintWeightLabel from '../newPrintLabel/NewPrintWeightLabel'

type ReceivingWeightSheetProps = {
  orderId: string
  weightItem: any
  visible: boolean
  onClose: Function
  getPallets: Function
  pallets: any[]
  getWeightSheets: Function
  weightSheet: any
  saveWeightSheet: Function
  totalPalletsInfo: any[]
  getTotalUnitsByAllPallets: Function
}

class ReceivingWeightSheet extends React.PureComponent<ReceivingWeightSheetProps> {
  constructor(props: ReceivingWeightSheetProps) {
    super(props)
    let parsedFailedIndexes = []
    let parsedFailedMessages = []
    const parsedFailed = localStorage.getItem(
      `RECEIVING_PARSING_FAILED_${props.weightItem.wholesaleOrderItemId.toString()}`,
    )
    const messages = localStorage.getItem(
      `RECEIVING_PARSING_FAILED_MESSAGES_${props.weightItem.wholesaleOrderItemId.toString()}`,
    )
    if (parsedFailed) {
      parsedFailedIndexes = JSON.parse(parsedFailed)
    }
    if (messages) {
      parsedFailedMessages = JSON.parse(messages)
    }

    this.state = {
      newValue: '',
      items: [],
      loaded: false,
      firstOpen: false,
      ids: [],
      palletId: this.props.pallets.length ? this.props.pallets[0].id : '',
      scanMode: localStorage.getItem('WEIGHT_SHEET_SCAN_MODE') === 'true',
      parsedFailedIndexes, //  Index of the catch weight that parse is failed in scann mode
      parsedFailedMessages,
      printLabelsReviewShow: false,
      parseSuccessIndexes: [],
      printLabelsRef: null,
      dataSource: [],
      currentParsingIndex: -1,
      cachedItems: [],
      cachedWeightSheet: [],
    }
  }

  componentDidMount() {
    if (this.props.weightItem) {
      this.props.getPallets(this.props.weightItem.wholesaleOrderItemId)
      this.props.getTotalUnitsByAllPallets(this.props.weightItem.wholesaleOrderItemId)
    }
    if (this.state.palletId) {
      this.props.getWeightSheets(this.state.palletId)
    }
    if (this.props.weightSheet) {
      const items = this.formatUnitsItems(this.props.weightSheet)
      let cachedItems = cloneDeep(this.state.cachedItems)
      cachedItems[this.state.palletId] = cloneDeep(items[0])
      this.setState({
        items: items[0],
        cachedItems: cachedItems,
        parseSuccessIndexes: items[1],
      })
      this.setFocusOnInput()
    }

    if (this.props.pallets && this.props.pallets.length) {
      this.setState({ dataSource: this.props.pallets })
    }

    setTimeout(() => {
      jQuery('.quantity-item input')
        .first()
        .trigger('select')
    }, 100)
  }

  componentWillReceiveProps(nextProps: ReceivingWeightSheetProps, nextState: any) {
    if (JSON.stringify(this.props.pallets) != JSON.stringify(nextProps.pallets)) {
      this.setState({ dataSource: nextProps.pallets })
      if (nextProps.pallets.length > 0) {
        this.setState({ palletId: nextProps.pallets[0].id }, () => {
          this.props.getWeightSheets(this.state.palletId)
        })
      } else {
        this.setState({ palletId: '' })
      }
    }
    if (JSON.stringify(this.props.weightSheet) != JSON.stringify(nextProps.weightSheet)) {
      if (!Array.isArray(this.state.cachedItems[this.state.palletId])) {
        const items = this.formatUnitsItems(nextProps.weightSheet)
        let cachedItems = cloneDeep(this.state.cachedItems)
        let cachedWeightSheet = cloneDeep(this.state.cachedWeightSheet)
        cachedItems[this.state.palletId] = cloneDeep(items[0])
        cachedWeightSheet[this.state.palletId] = cloneDeep(nextProps.weightSheet)
        this.setState({
          items: items[0],
          cachedItems,
          cachedWeightSheet,
          parseSuccessIndexes: items[1],
        })
      }
      this.setFocusOnInput()
    }

    if (this.state.palletId && this.state.palletId != '') {
      const items = this.formatUnitsItems(this.props.weightSheet)
      let cachedItems = cloneDeep(this.state.cachedItems)
      cachedItems[this.state.palletId] = cloneDeep(items[0])
      this.setState({
        items: items[0],
        cachedItems: cachedItems,
        parseSuccessIndexes: items[1],
      })
    }
  }

  formatUnitsItems = (weightSheet: any) => {
    let result: string[] = []
    let successIndexes: number[] = []
    if (weightSheet && weightSheet.unitsInfo) {
      const unitsInfo = JSON.parse(weightSheet.unitsInfo)
      const keys = Object.keys(unitsInfo)

      keys.forEach((el: any, index: number) => {
        result.push(unitsInfo[el])
        if (unitsInfo[el]) {
          successIndexes.push(index)
        }
      })
    }
    return [result, successIndexes]
  }

  onPalletChange = (palletId: any) => {
    const { cachedItems } = this.state
    this.setState(
      {
        palletId,
        items: Array.isArray(cachedItems[palletId]) ? cachedItems[palletId] : [],
      },
      () => {
        this.props.getWeightSheets(this.state.palletId)
      },
    )
  }

  handleAdd = () => {
    const { items } = this.state
    let updateItems = [...items]
    // this.props.resetLoading()
    updateItems.push('')

    let cachedItems = cloneDeep(this.state.cachedItems)
    cachedItems[this.state.palletId] = cloneDeep(updateItems)

    this.setState({ items: updateItems, cachedItems }, () => {
      this.setFocusOnInput(false)
    })
  }

  deleteUnit = (index: number) => {
    const updateItems = [...this.state.items]
    updateItems.splice(index, 1)

    let parsedFailedIndexes = [...this.state.parsedFailedIndexes]
    const found = parsedFailedIndexes.indexOf(index)
    if (found >= 0) {
      parsedFailedIndexes.splice(found, 1)
    }
    let parsedFailedMessages = [...this.state.parsedFailedMessages]
    const foundMessageIndex = parsedFailedMessages.findIndex((el) => el.index == index)
    if (foundMessageIndex >= 0) {
      parsedFailedMessages.splice(foundMessageIndex, 1)
    }
    if (parsedFailedIndexes.length > 0) {
      parsedFailedIndexes.forEach((failedIndex, i) => {
        if (failedIndex > index) {
          parsedFailedIndexes[i] = failedIndex - 1
        }
      })
    }
    if (parsedFailedMessages.length > 0) {
      parsedFailedMessages.forEach((el, i) => {
        if (parseInt(el.index) > index) {
          el.index = parseInt(el.index) - 1
        }
      })
    }

    let cachedItems = cloneDeep(this.state.cachedItems)
    cachedItems[this.state.palletId] = cloneDeep(updateItems)

    this.setState({
      items: updateItems,
      cachedItems,
      parsedFailedIndexes,
      parsedFailedMessages,
    })
  }

  onChangeUnitWeight = (index: number, value: string) => {
    const { items } = this.state
    let newItems: string[] = [...items]
    newItems[index] = value
    const parseSuccessIndexes = [...this.state.parseSuccessIndexes]
    const findIndex = parseSuccessIndexes.indexOf(index)
    if (findIndex > -1) {
      parseSuccessIndexes.splice(findIndex, 1)
    }

    let cachedItems = cloneDeep(this.state.cachedItems)
    cachedItems[this.state.palletId] = cloneDeep(newItems)

    this.setState({
      items: newItems,
      cachedItems,
      currentParsingIndex: index,
      parseSuccessIndexes,
    })
  }

  renderUnitsField = () => {
    const { palletId, scanMode, parsedFailedIndexes, parsedFailedMessages, cachedItems } = this.state
    const { weightSheet, weightItem } = this.props
    let result: any[] = []
    const items = cachedItems[palletId]

    if (Array.isArray(items) && this.state.palletId) {
      items.forEach((el: any, index: number) => {
        const warning = parsedFailedIndexes.indexOf(index) >= 0
        const warningMessage = parsedFailedMessages.find((el) => el.index == index)
        result.push(
          <Item className="quantity-item" key={index}>
            <Flex className="v-center space-between">
              {warning && scanMode ? (
                <InputLabel className="warning">
                  {index + 1} {warningMessage.message}
                </InputLabel>
              ) : (
                <InputLabel>{index + 1}</InputLabel>
              )}
              <Icon type="close-circle" className="close-quantity" onClick={this.deleteUnit.bind(this, index)} />
            </Flex>
            <div>
              {scanMode ? (
                <ThemeInput
                  className={warning ? 'warning' : ''}
                  value={el}
                  onChange={(e: any) => this.onChangeUnitWeight(index, e.target.value)}
                />
              ) : (
                <NumericInput
                  className={warning ? 'warning' : ''}
                  value={el}
                  onChange={this.onChangeUnitWeight.bind(this, index)}
                />
              )}
              {index == 0 && items.length > 1 && (
                <Tooltip placement="top" title={'Autopaste value here to all remaining cells'}>
                  <Icon
                    type="copy"
                    style={{ ...ItemHMargin4, cursor: 'pointer' }}
                    className="copy-order-qty"
                    onClick={this.fillEmptyValues}
                  />
                </Tooltip>
              )}
            </div>
          </Item>,
        )
      })
    }

    return result
  }

  fillEmptyValues = () => {
    const { items, parsedFailedIndexes, parseSuccessIndexes, parsedFailedMessages } = this.state
    if (!items[0]) {
      return
    }
    const firstValue = items[0]
    let clonedData: string[] = []
    items.forEach((el: string, index: number) => {
      if (!el || !el.trim() || (el && !el.replace(' ', ''))) {
        clonedData.push(firstValue)
        const foundIndex = parsedFailedIndexes.indexOf(index)
        if (foundIndex >= 0) {
          parsedFailedIndexes.splice(foundIndex, 1)
        }
        const foundMessageIndex = parsedFailedMessages.findIndex((el) => el.index == index)
        if (foundMessageIndex >= 0) {
          parsedFailedMessages.splice(foundMessageIndex, 1)
        }
        const successIndex = parseSuccessIndexes.indexOf(index)
        if (successIndex == -1) {
          parseSuccessIndexes.push(index)
        }
      } else {
        clonedData.push(el)
      }
    })

    let cachedItems = cloneDeep(this.state.cachedItems)
    cachedItems[this.state.palletId] = cloneDeep(clonedData)

    this.setState({
      items: clonedData,
      cachedItems,
      parsedFailedIndexes,
      parseSuccessIndexes,
      parsedFailedMessages,
    })
  }

  initEventHandler = () => {
    const _this = this
    jQuery('.quantity-item input').unbind()

    // Move focus to next field by carriage return
    jQuery('.quantity-item input')
      .unbind()
      .bind('click', (e: any) => {
        // When user clicks on field, full contents of field should be highlighted, so when user starts typing, full content of field is replaced
        jQuery(e.target).select()
      })
      .bind('keypress', (e: any) => {
        if (e.keyCode === 13 || e.keyCode === 9) {
          this.handleParsing(e)
        }
      })
      .bind('focus', (e: any) => {
        jQuery(e.target).trigger('select')
      })
      .bind('blur', (e: any) => {
        this.handleParsing(e)
      })
  }

  handleParsing = (e: any) => {
    // Only that textbox is parsed when press enter key or tab
    const currentEl = jQuery(e.currentTarget)
      .parent()
      .parent()
    const { weightItem } = this.props
    let supplierSkus = weightItem.supplierSkus ? weightItem.supplierSkus.split(',') : []
    supplierSkus = supplierSkus.map((el) => {
      return el.replaceAll(' ', '').trim()
    })
    const { items, scanMode, parsedFailedIndexes, parseSuccessIndexes } = this.state
    let index = jQuery('.units-wrapper .quantity-item').index(currentEl[0])

    let isFailed: boolean = false
    let message: string = ''
    let unitValue = ''
    const unitWeight = items[index]
    if (scanMode && unitWeight && parseSuccessIndexes.indexOf(index) == -1) {
      //first 2 digits
      //GS 128 parsing algorithm
      const prefix = unitWeight.substr(0, 2)
      if (prefix != '01' && prefix != '02') {
        isFailed = true
        message = 'SKU not detected!'
      }

      if (unitWeight.length > 17) {
        //17th digit
        const supplierSku = unitWeight.substr(10, 5)
        if (supplierSkus.indexOf(supplierSku) == -1) {
          isFailed = true
          message = 'Wrong SKU!'
        }
        const flag = unitWeight[16]
        let suffix = ''
        if (flag != '1') {
          suffix = unitWeight.substr(16)
        } else if (unitWeight.length > 24) {
          suffix = unitWeight.substr(24)
        }
        if (!suffix || (suffix.indexOf('320') != 0 && suffix.indexOf('310') != 0)) {
          isFailed = true
          message = 'Wrong SKU!'
        }

        if (suffix.length > 3) {
          const floatPoint = parseInt(suffix[3])
          if (isNaN(floatPoint)) {
            isFailed = true
            message = 'Wrong SKU!'
          }

          const realValueStr = suffix.substr(4, 6)
          const strValue =
            realValueStr.substr(0, realValueStr.length - floatPoint) + '.' + realValueStr.substr(-floatPoint)
          if (suffix.indexOf('320') == 0) {
            unitValue = formatNumber(parseFloat(strValue), 2)
          } else if (suffix.indexOf('310') == 0) {
            unitValue = formatNumber(parseFloat(strValue) * 2.20483, 2)
          }
        }
      }

      let newItems: string[] = cloneDeep(items)
      let parsedFailedIndexes = [...this.state.parsedFailedIndexes]
      let messages = [...this.state.parsedFailedMessages]
      newItems[index] = isNaN(parseFloat(unitValue)) ? '' : unitValue
      if (parseSuccessIndexes.indexOf(index) < 0) {
        parseSuccessIndexes.push(index)
      }
      if (isFailed === true) {
        // When not able to parse, it should stay in the field, but clear the text value
        if (parsedFailedIndexes.indexOf(index) < 0) {
          parsedFailedIndexes.push(index)
          messages.push({ index: index, message: message })
        }

        let cachedItems = cloneDeep(this.state.cachedItems)
        cachedItems[this.state.palletId] = cloneDeep(newItems)

        this.setState({
          items: newItems,
          cachedItems,
          parsedFailedIndexes,
          parseSuccessIndexes,
          parsedFailedMessages: messages,
        })
        if (index + 1 === jQuery('.units-wrapper .quantity-item').length - 1) {
          index = -1
        }
        jQuery(jQuery('.units-wrapper .quantity-item')[index + 1])
          .find('input')
          .focus()
      } else {
        const foundIndex = parsedFailedIndexes.indexOf(index)
        if (foundIndex >= 0) {
          parsedFailedIndexes.splice(foundIndex, 1)
        }
        const foundMessageIndex = messages.findIndex((el) => el.index == index)
        if (foundMessageIndex >= 0) {
          messages.splice(foundMessageIndex, 1)
        }

        let cachedItems = cloneDeep(this.state.cachedItems)
        cachedItems[this.state.palletId] = cloneDeep(newItems)

        this.setState({
          items: newItems,
          cachedItems,
          parsedFailedIndexes,
          parseSuccessIndexes,
          parsedFaildMessages: messages,
        })

        if (index + 1 === jQuery('.units-wrapper .quantity-item').length - 1) {
          index = -1
        }
        jQuery(jQuery('.units-wrapper .quantity-item')[index + 1])
          .find('input')
          .focus()
      }
    }
    this.setState({ currentParsingIndex: -1 })
  }

  setFocusOnInput = (isFirst: boolean = true) => {
    setTimeout(() => {
      const fields = jQuery('.quantity-item').find('.ant-input, .ant-btn')
      if (fields.length) {
        if (isFirst) {
          jQuery(fields[0]).trigger('focus')
        } else {
          jQuery('.quantity-item .ant-input')
            .last()
            .trigger('focus')
        }
      }
      this.initEventHandler()
    }, 500)
  }

  onChangeScanMode = (scanMode: boolean) => {
    localStorage.setItem('WEIGHT_SHEET_SCAN_MODE', scanMode.toString())
    this.setState(
      {
        scanMode,
      },
      () => {
        setTimeout(() => {
          this.initEventHandler()
        }, 10)
      },
    )
  }

  onClose = () => {
    /*
    
    move this functions to componentWillUnmount() to make sure values can't disappear after refresh

    this.setState({
      items: [],
      cachedItems: [],
      cachedWeightSheet: [],
    })*/
  }

  componentWillUnmount() {
    this.setState({
      items: [],
      cachedItems: [],
      cachedWeightSheet: [],
    })
  }

  onSaveWeightSheet = () => {
    const { cachedItems, cachedWeightSheet, parsedFailedIndexes, parsedFailedMessages } = this.state
    const { weightItem } = this.props

    cachedItems.forEach((items: any, palletId: number) => {
      if (typeof items !== 'undefined' && palletId == this.state.palletId) {
        const weightSheet = cachedWeightSheet[palletId]
        let data = weightSheet ? { ...weightSheet } : {}

        let unitsInfo = {}
        items.forEach((el: any, index: number) => {
          unitsInfo['unit' + (index + 1)] = el
        })
        data.palletId = palletId
        data.id = this.props.weightSheet ? (this.props.weightSheet.id ? this.props.weightSheet.id : '') : ''
        data.unitsInfo = JSON.stringify(unitsInfo)
        data.unitsCount = items.length
        this.props.saveWeightSheet(data)
      }
    })

    if (parsedFailedIndexes.length > 0) {
      localStorage.setItem(
        `RECEIVING_PARSING_FAILED_${weightItem.wholesaleOrderItemId.toString()}`,
        JSON.stringify(parsedFailedIndexes),
      )
      localStorage.setItem(
        `RECEIVING_PARSING_FAILED_MESSAGES_${weightItem.wholesaleOrderItemId.toString()}`,
        JSON.stringify(parsedFailedMessages),
      )
    } else {
      localStorage.removeItem(`RECEIVING_PARSING_FAILED_${weightItem.wholesaleOrderItemId.toString()}`)
      localStorage.removeItem(`RECEIVING_PARSING_FAILED_MESSAGES_${weightItem.wholesaleOrderItemId.toString()}`)
    }

    this.props.onClose()
  }

  getTotal = () => {
    const { items, currentParsingIndex } = this.state
    let total = 0
    items.forEach((el, index) => {
      if (currentParsingIndex != index) {
        total += !isNaN(parseFloat(el)) ? parseFloat(el) : 0
      }
    })
    return formatNumber(total, 2)
  }

  openPrintModal = () => {
    this.setState({
      printLabelsReviewShow: true,
    })
  }

  closePrintModal = () => {
    this.setState({
      printLabelsReviewShow: false,
    })
  }

  printContent: any = () => {
    return this.state.printLabelsRef
  }

  render() {
    const { weightItem, pallets, weightSheet, sellerSetting, totalPalletsInfo } = this.props
    const { items, scanMode, palletId } = this.state
    const total = this.getTotal()
    let totalBillableQuantity = 0
    totalPalletsInfo.forEach((element) => {
      totalBillableQuantity += element.total
    })
    if (!this.state.palletId) {
      total = 0
    }
    return weightItem ? (
      <>
        <DialogSubContainer className="bordered">
          <FlexDiv className="space-between">
            <div style={{ maxWidth: '40%' }}>
              <LabelItem>ITEM</LabelItem>
              <ItemValue>{weightItem.variety}</ItemValue>
            </div>
            <div>
              <LabelItem>TOTAL BILLABLE QTY</LabelItem>
              <ItemValue>{totalBillableQuantity ? formatNumber(totalBillableQuantity, 2) : 0.0}</ItemValue>
            </div>
            <div style={{ textAlign: 'center' }}>
              <LabelItem>SCANNER INPUT</LabelItem>
              <ThemeSwitch checked={scanMode} onChange={this.onChangeScanMode} />
            </div>
          </FlexDiv>
        </DialogSubContainer>
        <DialogBodyDiv>
          <FlexDiv className="space-between">
            <div>
              <LabelItem>Total Quantity This Pallet:</LabelItem>
              <ItemValue>{total != '0' ? total : '0.00'} LBs</ItemValue>
            </div>
            <div>
              <LabelItem>Pallet #</LabelItem>
              <ThemeSelect
                value={palletId}
                style={{ minWidth: 120, width: 'fit-content' }}
                dropdownStyle={{ maxHeight: 200 }}
                dropdownClassName={'weight-sheet-dropdown-renderer'}
                onChange={this.onPalletChange}
              >
                {pallets
                  ? pallets.map((el) => {
                      return (
                        <Select.Option key={el.id} value={el.id}>
                          {el.displayNumber}
                        </Select.Option>
                      )
                    })
                  : ''}
              </ThemeSelect>
            </div>
            <div>
              <LabelItem>Pallet Units:</LabelItem>
              <ItemValue>{weightSheet ? weightSheet.unitsCount : 0}</ItemValue>
            </div>
          </FlexDiv>
          <Flex
            className="units-wrapper"
            style={{ flexWrap: 'wrap', alignItems: 'flex-end', minHeight: 64, maxHeight: 450, overflowY: 'auto' }}
          >
            {this.renderUnitsField()}
            <LabelItem className="quantity-item ">
              <ThemeOutlineButton style={{ marginTop: 12 }} onClick={this.handleAdd}>
                Add CASE
              </ThemeOutlineButton>
            </LabelItem>
          </Flex>
        </DialogBodyDiv>
        <DialogSubContainer style={{ display: 'flex', justifyContent: 'space-between' }}>
          <div>
            <ThemeButton type="primary" onClick={this.onSaveWeightSheet}>
              Save
            </ThemeButton>
            <ThemeOutlineButton style={{ marginLeft: 12 }} onClick={this.props.onClose}>
              CANCEL
            </ThemeOutlineButton>
          </div>
          <div>
            <ThemeButton shape="round" onClick={this.openPrintModal}>
              <Icon type="printer" />
              Print Unit Labels
            </ThemeButton>
          </div>
        </DialogSubContainer>

        <NewLabelModal
          style={{ width: '703px', height: '930px', padding: '0px' }}
          width={703}
          footer={null}
          visible={this.state.printLabelsReviewShow}
          onCancel={this.closePrintModal}
        >
          <NewPrintWeightLabel
            items={items}
            palletId={palletId}
            dataSource={this.state.dataSource}
            sellerSetting={sellerSetting}
            props={this.props}
          />
        </NewLabelModal>
      </>
    ) : (
      ''
    )
  }
}

export default ReceivingWeightSheet
