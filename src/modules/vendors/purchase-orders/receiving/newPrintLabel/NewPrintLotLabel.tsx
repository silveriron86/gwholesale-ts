import { Modal } from 'antd'
import React, { useEffect, useState } from 'react'
import {
  labelSetting,
  label43Obj,
  label251Obj,
  label252Obj,
  label43ObjSetting,
  label251ObjSetting,
  label252ObjSetting,
  LotLabelObj,
} from '~/modules/product/components/Label/components/type/type'
import LotLabel from './printSetting/LotLabel'
import LotLabelCanvasPrint from './printCanvas/LotLabelCanvasPrint'

interface IProps {
  dataSource: any
  sellerSetting: any
  props: any
}

const NewPrintLotLabel = ({ dataSource, sellerSetting, props }: IProps): JSX.Element => {
  const customCompanyPrefix =
    sellerSetting &&
    sellerSetting.company &&
    typeof sellerSetting.company.customCompanyPrefix !== 'undefined' &&
    sellerSetting.company.customCompanyPrefix !== null
      ? sellerSetting.company.customCompanyPrefix
      : '000'

  const barcodeString = `0111WSW${customCompanyPrefix}${
    dataSource.length > 0 && dataSource[0].wholesaleOrderItem.wholesaleItem.labelSerial
      ? dataSource[0].wholesaleOrderItem.wholesaleItem.labelSerial
      : '00000'
  }010${dataSource.length > 0 ? dataSource[0].displayNumber : ''}`
  const upcCodeString = `012226203370`

  const [labelSetting, setLabelSetting] = useState<labelSetting>({
    x1A: true, // should get from backend
    x1B: true, // should get from backend
    productName: dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.wholesaleItem.variety : '',
    lot: props.palletizeItem.lotId,
    poNum: props.orderId,
    isPoNum: true, // should get from backend
    isOrigin: true, // should get from backend
    isCompanyInfo: true, // should get from backend
    origin: dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.wholesaleItem.origin : '', //each item should have an origin -- wholesaleItem
    companyInfo: 'companyInfo', // should get from backend
    count: dataSource.length > 0 ? dataSource[0].palletUnits : '',
    qrCode: barcodeString,
    logo: sellerSetting.imagePath ? `${process.env.AWS_PUBLIC}/${sellerSetting.imagePath}` : '', // not sure if it is  sellerSetting.imagePath
    isLogo: true,
    barcode1: barcodeString,
    barcode2: upcCodeString,
    sellByDate: dataSource.length > 0 ? dataSource[0].bestByDate : '',
    sellByDateShow: true, // should get from backend
    note: 'notes', // should get from backend
    sku: dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.wholesaleItem.sku : '',
    constantRatio: false,
  })

  const [unit, setUnit] = useState<string>('4x3')
  const [showModal, setShowModal] = useState<boolean>(false)

  const [label43ObjSetting, setLabel43ObjSetting] = useState<label43ObjSetting>({
    isPoNum: labelSetting.isPoNum,
    isOrigin: labelSetting.isOrigin,
    companyInfoString: labelSetting.companyInfo,
    isLogo: labelSetting.isLogo,
    isCompanyInfo: labelSetting.isCompanyInfo,
    sku: labelSetting.sku,
  })

  const [label251ObjSetting, setLabel251ObjSetting] = useState<label251ObjSetting>({
    productName: labelSetting.productName,
    sellByDateShow: labelSetting.sellByDateShow,
    sellByDate: labelSetting.sellByDate,
    companyInfoString: labelSetting.companyInfo,
    upcCode: labelSetting.barcode2,
  })

  const [label252ObjSetting, setLabel252ObjSetting] = useState<label252ObjSetting>({
    productName: labelSetting.productName,
    note: labelSetting.note,
    companyInfoString: labelSetting.companyInfo,
    sellByDateShow: labelSetting.sellByDateShow,
    sellByDate: labelSetting.sellByDate,
  })

  const [show2X1A, set2X1A] = useState<boolean>(false)
  const [show2X1B, set2X1B] = useState<boolean>(false)

  const [update4X3, setUpdate4X3] = useState<boolean>(false)
  const [update2X1B, setUpdate2X1B] = useState<boolean>(false)
  const [update2X1A, setUpdate2X1A] = useState<boolean>(false)

  const [update4X3Id, setUpdate4X3Id] = useState<number>(0)
  const [update2X1BId, setUpdate2X1BId] = useState<number>(0)
  const [update2X1AId, setUpdate2X1AId] = useState<number>(0)

  const [showConfirm4X3mModal, setShowConfirm4X3Modal] = useState<boolean>(false)
  const [showConfirm2X1AModal, setShowConfirm2X1AModal] = useState<boolean>(false)
  const [showConfirm2XABModal, setShowConfirm2XABModal] = useState<boolean>(false)

  useEffect(() => {
    ;(async function anyNameFunction() {
      await getLabelsAsync()
    })()
  }, [])

  useEffect(() => {
    //console.log(label43ObjSetting)
  }, [label43ObjSetting, label251ObjSetting, label252ObjSetting])

  useEffect(() => {
    //console.log(sellerSetting)
  }, [])

  const getLabelsAsync = () => {
    /*const http = new Http()
    const newService = new ProductService(http)
    newService.getLabelList().subscribe({
      next(res: any) {
        const labelList = res.body.data
        const userId = localStorage.getItem('id')
        //get 4X3
        const propLabel4X3 = labelList.find(
          (label: any) => label.createdBy.userId == userId && label.name == 'unit 4X3',
        )
        if (propLabel4X3) {
          const label4X3 = Object.assign({}, label43ObjSetting)
          ;(label4X3.companyInfoString = propLabel4X3.companyInformation ? propLabel4X3.companyInformation : ''),
            (label4X3.isCompanyInfo = propLabel4X3.companyInformationEnabled),
            (label4X3.isLogo = propLabel4X3.logoEnabled),
            (label4X3.isOrigin = propLabel4X3.originUsaEnabled),
            (label4X3.isPoNum = propLabel4X3.poNumberEnabled)
          setLabel43ObjSetting(label4X3)
          setUpdate4X3(true)
          setUpdate4X3Id(propLabel4X3.id)
        }
        //get 2X1A
        const propLabel2X1A = labelList.find(
          (label: any) => label.createdBy.userId == userId && label.name == 'unit 2X1A',
        )
        if (propLabel2X1A) {
          const label2X1A = Object.assign({}, label251ObjSetting)
          ;(label2X1A.companyInfoString = propLabel2X1A.companyInformation ? propLabel2X1A.companyInformation : ''),
            (label2X1A.productName = propLabel2X1A.editedProductName ? propLabel2X1A.editedProductName : ''),
            (label2X1A.sellByDateShow = propLabel2X1A.sellByDateEnabled)
          setLabel251ObjSetting(label2X1A)
          set2X1A(propLabel2X1A.isActive)
          setUpdate2X1A(true)
          setUpdate2X1AId(propLabel2X1A.id)
        }
        //get 2X1B
        const propLabel2X1B = labelList.find(
          (label: any) => label.createdBy.userId == userId && label.name == 'unit 2X1B',
        )
        if (propLabel2X1B) {
          const label2X1B = Object.assign({}, label252ObjSetting)
          ;(label2X1B.companyInfoString = propLabel2X1B.companyInformation ? propLabel2X1B.companyInformation : ''),
            (label2X1B.productName = propLabel2X1B.editedProductName ? propLabel2X1B.editedProductName : ''),
            (label2X1B.note = propLabel2X1B.notes ? propLabel2X1B.notes : ''),
            (label2X1B.sellByDateShow = propLabel2X1B.sellByDateEnabled)
          setLabel252ObjSetting(label2X1B)
          set2X1B(propLabel2X1B.isActive)
          setUpdate2X1B(true)
          setUpdate2X1BId(propLabel2X1B.id)
        }
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })*/
  }

  const getLabel4X3 = (): LotLabelObj => {
    return {
      productName: labelSetting.productName,
      lot: labelSetting.lot,
      poNum: labelSetting.poNum,
      isPoNum: label43ObjSetting.isPoNum,
      origin: labelSetting.origin,
      isOrigin: label43ObjSetting.isOrigin,
      companyInfo: label43ObjSetting.companyInfoString,
      isCompanyInfo: label43ObjSetting.isCompanyInfo,
      barCode: labelSetting.barcode1,
      qrCode: labelSetting.qrCode,
      logo: labelSetting.logo,
      isLogo: label43ObjSetting.isLogo,
      sku: label43ObjSetting.sku,
      constantRatio: labelSetting.constantRatio !== undefined ? labelSetting.constantRatio : false,
    }
  }

  const getLabel251A = (): label251Obj => {
    return {
      x1A: labelSetting.x1A,
      productName: label251ObjSetting.productName,
      lot: labelSetting.lot,
      origin: labelSetting.origin,
      sellByDateShow: label251ObjSetting.sellByDateShow,
      sellByDate: labelSetting.sellByDate,
      barcode1: labelSetting.barcode1,
      barcode2: labelSetting.barcode2,
      companyInfo: label251ObjSetting.companyInfoString,
    }
  }

  const getLabel251B = (): label252Obj => {
    return {
      x1B: labelSetting.x1B,
      productName: label252ObjSetting.productName,
      lot: labelSetting.lot,
      origin: labelSetting.origin,
      barcode1: labelSetting.barcode1,
      note: label252ObjSetting.note,
      companyInfo: label252ObjSetting.companyInfoString,
      sellByDateShow: label252ObjSetting.sellByDateShow,
      sellByDate: labelSetting.sellByDate,
    }
  }
  //update functions
  /*const update4X3Fn = () => {
    const obj = {
      id: update4X3Id,
      companyInformationEnabled: label43ObjSetting.isCompanyInfo,
      logoEnabled: label43ObjSetting.isLogo,
      originUsaEnabled: label43ObjSetting.isOrigin,
      poNumberEnabled: label43ObjSetting.isPoNum,
      companyInformation: label43ObjSetting.companyInfoString,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm4X3Modal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const update2X1AFn = () => {
    const obj = {
      id: update2X1AId,
      editedProductName: label251ObjSetting.productName,
      sellByDateEnabled: label251ObjSetting.sellByDateShow,
      companyInformation: label251ObjSetting.companyInfoString,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm2X1AModal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const update2X1BFn = () => {
    const obj = {
      id: update2X1BId,
      editedProductName: label252ObjSetting.productName,
      notes: label252ObjSetting.note,
      companyInformation: label252ObjSetting.companyInfoString,
      sellByDateEnabled: label252ObjSetting.sellByDateShow,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm2XABModal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }*/

  const getRightPart = () => {
    switch (unit) {
      case '4x3':
        return (
          <div>
            <LotLabel
              labelObj={getLabel4X3()}
              passSetting={(obj) => setLabel43ObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update4X3}
              updateAsyncFn={() => setShowConfirm4X3Modal(true)}
              UOM="CASE"
            />
          </div>
        )
      /*case '2X1 A':
        return (
          <div>
            <Label25X1A
              labelObj={getLabel251A()}
              passSetting={(obj) => setLabel251ObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update2X1A}
              updateAsyncFn={() => setShowConfirm2X1AModal(true)}
            />
          </div>
        )
      case '2X1 B':
        return (
          <div>
            <Label25X1B
              labelObj={getLabel251B()}
              passSetting={(obj) => setLabel252ObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update2X1B}
              updateAsyncFn={() => setShowConfirm2XABModal(true)}
            />
          </div>
        )*/
    }
  }

  const getMutipleCanvas = () => {
    switch (unit) {
      case '4x3':
        const lotLabel: LotLabelObj[] = []
        let uom = ''
        dataSource.forEach((item: any, index: number) => {
          let constantRatio = true
          const uomList = item.wholesaleOrderItem.wholesaleItem.wholesaleProductUomList
          const matched = uomList.find((u: any) => u.name === item.wholesaleOrderItem.pricingUOM)
          if (matched) {
            constantRatio = matched.constantRatio
          }
          const obj: LotLabelObj = {
            productName: item.wholesaleOrderItem.wholesaleItem.variety,
            lot: labelSetting.lot,
            poNum: labelSetting.poNum,
            isPoNum: label43ObjSetting.isPoNum,
            origin: dataSource.length > 0 ? dataSource[index].wholesaleOrderItem.wholesaleItem.origin : '',
            isOrigin: label43ObjSetting.isOrigin,
            companyInfo: label43ObjSetting.companyInfoString,
            isCompanyInfo: labelSetting.isCompanyInfo,
            barCode: `0111WSW${customCompanyPrefix}${
              item.wholesaleOrderItem.wholesaleItem.labelSerial
                ? item.wholesaleOrderItem.wholesaleItem.labelSerial
                : '00000'
            }010${item.displayNumber}`,
            qrCode: labelSetting.qrCode,
            logo: labelSetting.logo,
            isLogo: label43ObjSetting.isLogo,
            sku: item.wholesaleOrderItem.wholesaleItem.sku,
            constantRatio: constantRatio,
          }
          lotLabel.push(obj)
          uom = item.wholesaleOrderItem
            ? item.wholesaleOrderItem.wholesaleItem
              ? item.wholesaleOrderItem.wholesaleItem.defaultPurchaseUnitUOM
              : ''
            : ''
        })
        return <LotLabelCanvasPrint labelObjs={lotLabel} setting={label43ObjSetting} UOM={uom ? uom : ''} />
      /*case '2X1 A':
        const labels2x1: label251Obj[] = []
        dataSource.forEach((item: any, index: number) => {
          const obj: label251Obj = {
            x1A: labelSetting.x1A,
            productName: label252ObjSetting.productName,
            lot: labelSetting.lot,
            origin: labelSetting.origin,
            sellByDate: item.bestByDate,
            sellByDateShow: true,
            barcode1: `0111WSW${customCompanyPrefix}${
              item.wholesaleOrderItem.wholesaleItem.labelSerial
                ? item.wholesaleOrderItem.wholesaleItem.labelSerial
                : '00000'
            }010${props.pallets[index].id}`,
            barcode2: item.wholesaleOrderItem.wholesaleItem.upc
              ? item.wholesaleOrderItem.wholesaleItem.upc.length == 12
                ? item.wholesaleOrderItem.wholesaleItem.upc
                : ''
              : '',
            companyInfo: label43ObjSetting.companyInfoString,
          }
          labels2x1.push(obj)
        })
        return <LabelCanvasPrint1 label251Obj={labels2x1} setting={label251ObjSetting} />
      case '2X1 B':
        const labels2x1B: label252Obj[] = []
        dataSource.forEach((item: any, index: number) => {
          const obj: label252Obj = {
            x1B: labelSetting.x1B,
            productName: label252ObjSetting.productName,
            lot: labelSetting.lot,
            origin: labelSetting.origin,
            barcode1: `0111WSW${customCompanyPrefix}${
              item.wholesaleOrderItem.wholesaleItem.labelSerial
                ? item.wholesaleOrderItem.wholesaleItem.labelSerial
                : '00000'
            }010${props.pallets[index].id}`,
            note: labelSetting.note,
            companyInfo: label252ObjSetting.companyInfoString,
            sellByDate: item.bestByDate,
            sellByDateShow: true,
          }
          labels2x1B.push(obj)
        })
        return <LabelCanvasPrint2 label252Obj={labels2x1B} setting={label252ObjSetting} />*/
    }
  }

  return (
    <div style={{ padding: '0px' }}>
      <div style={{ height: '54px', borderBottom: '1px solid #D8DBDB' }}>
        <p style={{ marginLeft: '24px', paddingTop: '16px', fontSize: '18px' }}>{`Labels - ${
          unit == '2X1 A' ? '2.5X1 A' : unit == '2X1 B' ? '2.5X1 B' : unit
        }`}</p>
      </div>
      <div style={{ display: 'flex', backgroundColor: 'white' }}>
        <div
          style={{
            width: '160px',
            display: 'inline',
            textAlign: 'left',
            position: 'relative',
          }}
        >
          <p
            style={{
              paddingLeft: '24px',
              cursor: 'pointer',
              height: '36px',
              lineHeight: '36px',
              marginBottom: '0px',
              background: `${unit == '4x3' ? '#F2F9F3' : 'white'}`,
            }}
            onClick={() => setUnit('4x3')}
          >
            Lot label
          </p>
          {show2X1A ? (
            <p
              style={{
                paddingLeft: '24px',
                cursor: 'pointer',
                height: '36px',
                lineHeight: '36px',
                marginBottom: '0px',
                background: `${unit == '2X1 A' ? '#F2F9F3' : 'white'}`,
              }}
              onClick={() => setUnit('2X1 A')}
            >
              UNIT 2.5X1 A
            </p>
          ) : (
            <></>
          )}
          {show2X1B ? (
            <p
              onClick={() => setUnit('2X1 B')}
              style={{
                paddingLeft: '24px',
                cursor: 'pointer',
                height: '36px',
                lineHeight: '36px',
                marginBottom: '0px',
                background: `${unit == '2X1 B' ? '#F2F9F3' : 'white'}`,
              }}
            >
              UNIT 2.5X1 B
            </p>
          ) : (
            <></>
          )}
          <div
            style={{
              height: '67px',
              width: '149px',
              background: '#e8e8e8',
              position: 'absolute',
              bottom: '0px',
            }}
          >
            <p style={{ color: '#e8e8e8' }}>s</p>
          </div>
        </div>
        {getRightPart()}
        <Modal
          style={{ padding: '0px' }}
          width={703}
          footer={null}
          visible={showModal}
          onCancel={() => {
            setShowModal(false)
          }}
        >
          <div>{getMutipleCanvas()}</div>
        </Modal>
        {/*<NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm4X3mModal}
          onCancel={() => {
            setShowConfirm4X3Modal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update4X3Fn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm4X3Modal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>
        <NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm2X1AModal}
          onCancel={() => {
            setShowConfirm2X1AModal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update2X1AFn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm2X1AModal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>
        <NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm2XABModal}
          onCancel={() => {
            setShowConfirm2XABModal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update2X1BFn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm2XABModal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>*/}
      </div>
    </div>
  )
}

export default NewPrintLotLabel
