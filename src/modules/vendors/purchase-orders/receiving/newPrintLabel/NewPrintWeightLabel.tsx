import { Button, Modal } from 'antd'
import React, { FC, useRef, useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import { Http } from '~/common/http'
import { printWindow } from '~/common/utils'
import { ThemeButton } from '~/modules/customers/customers.style'
import {
  labelSetting,
  label43Obj,
  label251Obj,
  label252Obj,
  label43ObjSetting,
  label251ObjSetting,
  label252ObjSetting,
  label251AQRObj,
  label251BQRObj,
  pallet4X6ObjSetting,
  pallet4X6Obj,
} from '~/modules/product/components/Label/components/type/type'
import { ProductService } from '~/modules/product/product.service'
import { NewLabelModal } from '../../_style'
import Label25X1A from './printSetting/Label25X1A'
import Label25X1B from './printSetting/Label25X1B'
import Label4X3 from './printSetting/Label4X3'
import LabelCanvasPrint from './printCanvas/LabelCanvasPrint'
import LabelCanvasPrint1 from './printCanvas/LabelCanvasPrint1'
import LabelCanvasPrint2 from './printCanvas/LabelCanvasPrint2'
import Label25X1BQR from './printSetting/Label25X1BQR'
import LabelCanvasPrint2X51QR from './printCanvas/LabelCanvasPrint2X51QR'
import LabelCanvasPrint2X52QR from './printCanvas/LabelCanvasPrint2X52QR'
import Label25X1AQR from './printSetting/Label25X1AQR'
import Label6X4Pallet from './printSetting/Label6X4Pallet'
import LabelCanvasPrint6X4Pallet from './printCanvas/LabelCanvasPrint6X4Pallet'

interface IProps {
  dataSource: any
  sellerSetting: any
  props: any
  items: string[]
  palletId: any
  firstPallet?: any
}

const NewPrintWeightLabel = ({ dataSource, sellerSetting, props, items, palletId, firstPallet }: IProps): JSX.Element => {
  const getItems = () => {
    const found = dataSource.find((item: any) => item.id == palletId)
    const newItems: any[] = []
    if (found && items?.length) {
      items?.forEach((unit) => {
        if (found) {
          newItems.push({
            ...found,
            unit,
          })
        } else if(firstPallet) {
          newItems.push({
            ...firstPallet,
            unit,
          })
        }
      })
    } 
    return newItems
  }

  const padString = (num: number) => {
    const str = num.toString()
    const pad = '000000'
    return pad.substring(0, pad.length - str.length) + str
  }
  const getWeightBarcode = (item: any) => {
    if (item) {
      const customCompanyPrefix =
        sellerSetting &&
        sellerSetting.company &&
        typeof sellerSetting.company.customCompanyPrefix !== 'undefined' &&
        sellerSetting.company.customCompanyPrefix !== null
          ? sellerSetting.company.customCompanyPrefix
          : '000'
      const labelSerial = item.wholesaleOrderItem.wholesaleItem.labelSerial
        ? item.wholesaleOrderItem.wholesaleItem.labelSerial
        : '00000'
      const currentPalletId = item.displayNumber.substring(0, 20)
      const itemUnit = item.unit === '' || item.unit === null || typeof item.unit === 'undefined' ? 0 : item.unit
      const weight = parseFloat(itemUnit)
        .toFixed(2)
        .replace('.', '')
      const newWeight = padString(parseFloat(weight))
      return `0111WSW${customCompanyPrefix}${labelSerial}03202${newWeight}10${currentPalletId}`
    } else {
      return `0111WSWEmpty`
    }
  }

  const barcodeString = getWeightBarcode(getItems()[0])
  const upcCodeString =
    dataSource.length > 0
      ? dataSource[0].wholesaleOrderItem.wholesaleItem.upc
        ? parseInt(dataSource[0].wholesaleOrderItem.wholesaleItem.upc).toString().length <= 12
          ? parseInt(dataSource[0].wholesaleOrderItem.wholesaleItem.upc).toString()
          : ''
        : ''
      : ''

  const [labelSetting, setLabelSetting] = useState<labelSetting>({
    x1A: true, // should get from backend
    x1B: true, // should get from backend
    productName: dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.wholesaleItem.variety : '',
    lot: props.weightItem.lotId,
    poNum: props.weightItem.lotId ? props.orderId : '',
    isPoNum: true, // should get from backend
    isOrigin: true, // should get from backend
    isCompanyInfo: true, // should get from backend
    origin: dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.extraOrigin : '', //each item should have an origin -- wholesaleItem
    companyInfo: 'companyInfo', // should get from backend
    count: items ? (items[0] ? items[0] : '0') : '0',
    qrCode: barcodeString,
    logo: sellerSetting && sellerSetting.imagePath ? `${process.env.AWS_PUBLIC}/${sellerSetting.imagePath}` : '', // not sure if it is  sellerSetting.imagePath
    isLogo: true,
    barcode1: barcodeString,
    barcode2: upcCodeString,
    sellByDate: dataSource.length > 0 ? dataSource[0].bestByDate : '',
    sellByDateShow: true, // should get from backend
    note: 'notes', // should get from backend
    sku: dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.wholesaleItem.sku : '',
  })

  const [pallet4X6, setPallet4X6] = useState<pallet4X6Obj>({
    lot: props.weightItem.lotId,
    poNum: props.weightItem.lotId ? props.orderId : '',
    field: dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.modifiers : '',
    comm: props.weightItem.category,
    origin:
      dataSource.length > 0
        ? dataSource[0].wholesaleOrderItem.wholesaleItem.origin
          ? dataSource[0].wholesaleOrderItem.wholesaleItem.origin
          : ''
        : '',
    sku:
      dataSource.length > 0
        ? dataSource[0].wholesaleOrderItem.wholesaleItem.sku
          ? dataSource[0].wholesaleOrderItem.wholesaleItem.sku
          : ''
        : '',
    style:
      dataSource.length > 0
        ? dataSource[0].wholesaleOrderItem.wholesaleItem.packing
          ? dataSource[0].wholesaleOrderItem.wholesaleItem.packing
          : ''
        : '',
    size:
      dataSource.length > 0
        ? dataSource[0].wholesaleOrderItem.wholesaleItem.size
          ? dataSource[0].wholesaleOrderItem.wholesaleItem.size
          : ''
        : '',
    quantity: props.weightItem.quantity,
    description:
      dataSource.length > 0
        ? dataSource[0].wholesaleOrderItem.wholesaleItem.variety
          ? dataSource[0].wholesaleOrderItem.wholesaleItem.variety
          : ''
        : '',
    qrCode: barcodeString,
  })

  const [unit, setUnit] = useState<string>('4x3')
  const [showModal, setShowModal] = useState<boolean>(false)

  const [label43ObjSetting, setLabel43ObjSetting] = useState<label43ObjSetting>({
    isPoNum: labelSetting.isPoNum,
    isOrigin: labelSetting.isOrigin,
    companyInfoString: labelSetting.companyInfo,
    isLogo: labelSetting.isLogo,
    isCompanyInfo: labelSetting.isCompanyInfo,
    sku: labelSetting.sku,
  })

  const [label251ObjSetting, setLabel251ObjSetting] = useState<label251ObjSetting>({
    productName: labelSetting.productName,
    sellByDateShow: labelSetting.sellByDateShow,
    sellByDate: labelSetting.sellByDate,
    companyInfoString: labelSetting.companyInfo,
    upcCode: labelSetting.barcode2,
  })

  const [label251AQRObjSetting, setLabel251AQRObjSetting] = useState<label251ObjSetting>({
    productName: labelSetting.productName,
    sellByDateShow: labelSetting.sellByDateShow,
    sellByDate: labelSetting.sellByDate,
    companyInfoString: labelSetting.companyInfo,
    upcCode: labelSetting.barcode2,
  })

  const [label252ObjSetting, setLabel252ObjSetting] = useState<label252ObjSetting>({
    productName: labelSetting.productName,
    note: labelSetting.note,
    companyInfoString: labelSetting.companyInfo,
    sellByDateShow: labelSetting.sellByDateShow,
    sellByDate: labelSetting.sellByDate,
  })

  const [label251BQRObjSetting, setLabel251BQRObjSetting] = useState<label252ObjSetting>({
    productName: labelSetting.productName,
    note: labelSetting.note,
    companyInfoString: labelSetting.companyInfo,
    sellByDateShow: labelSetting.sellByDateShow,
    sellByDate: labelSetting.sellByDate,
  })

  const [pallet4X6Setting, setPallet4X6Setting] = useState<pallet4X6ObjSetting>({
    lot: pallet4X6.lot,
    poNum: pallet4X6.poNum,
    field: pallet4X6.field,
    comm: pallet4X6.comm,
    origin: pallet4X6.origin,
    sku: pallet4X6.sku,
    style: pallet4X6.style,
    size: pallet4X6.size,
    quantity: pallet4X6.quantity,
    description: pallet4X6.description,
    qrCode: pallet4X6.qrCode,
  })

  const [show2X1A, set2X1A] = useState<boolean>(false)
  const [show2X1B, set2X1B] = useState<boolean>(false)
  const [show2X1AQR, set2X1AQR] = useState<boolean>(false)
  const [show2X1BQR, set2X1BQR] = useState<boolean>(false)

  const [update4X3, setUpdate4X3] = useState<boolean>(false)
  const [update2X1B, setUpdate2X1B] = useState<boolean>(false)
  const [update2X1A, setUpdate2X1A] = useState<boolean>(false)
  const [update2X1BQR, setUpdate2X1BQR] = useState<boolean>(false)
  const [update2X1AQR, setUpdate2X1AQR] = useState<boolean>(false)

  const [update4X3Id, setUpdate4X3Id] = useState<number>(0)
  const [update2X1BId, setUpdate2X1BId] = useState<number>(0)
  const [update2X1AId, setUpdate2X1AId] = useState<number>(0)
  const [update2X1BQRId, setUpdate2X1BQRId] = useState<number>(0)
  const [update2X1AQRId, setUpdate2X1AQRId] = useState<number>(0)

  const [showConfirm4X3mModal, setShowConfirm4X3Modal] = useState<boolean>(false)
  const [showConfirm2X1AModal, setShowConfirm2X1AModal] = useState<boolean>(false)
  const [showConfirm2XABModal, setShowConfirm2XABModal] = useState<boolean>(false)
  const [showConfirm2X1AQRModal, setShowConfirm2X1AQRModal] = useState<boolean>(false)
  const [showConfirm2X1BQRModal, setShowConfirm2X1BQRModal] = useState<boolean>(false)

  useEffect(() => {
    ;(async function anyNameFunction() {
      await getLabelsAsync()
    })()
  }, [])

  useEffect(() => {
    //console.log(label252ObjSetting)
  }, [
    label43ObjSetting,
    label251ObjSetting,
    label252ObjSetting,
    label251AQRObjSetting,
    label251BQRObjSetting,
    pallet4X6Setting,
  ])

  useEffect(() => {
    // console.log(labelSetting)
    //console.log(sellerSetting)
    //console.log(dataSource)
    //console.log(props.weightItem)
    //console.log(props)
  }, [])

  const getLabelsAsync = () => {
    const companyId = props.sellerSetting.company.companyId
    const http = new Http()
    const newService = new ProductService(http)
    newService.getLabelList().subscribe({
      next(res: any) {
        const labelList = res.body.data
        //get 4X3
        const propLabel4X3 = labelList.find(
          (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 4X3',
        )
        if (propLabel4X3) {
          const label4X3 = Object.assign({}, label43ObjSetting)
          ;(label4X3.companyInfoString = propLabel4X3.companyInformation ? propLabel4X3.companyInformation : ''),
            (label4X3.isCompanyInfo = propLabel4X3.companyInformationEnabled),
            (label4X3.isLogo = propLabel4X3.logoEnabled),
            (label4X3.isOrigin = propLabel4X3.originUsaEnabled),
            (label4X3.isPoNum = propLabel4X3.poNumberEnabled)
          setLabel43ObjSetting(label4X3)
          setUpdate4X3(true)
          setUpdate4X3Id(propLabel4X3.id)
        }
        //get 2X1A
        const propLabel2X1A = labelList.find(
          (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1A',
        )
        if (propLabel2X1A) {
          const label2X1A = Object.assign({}, label251ObjSetting)
          ;(label2X1A.companyInfoString = propLabel2X1A.companyInformation ? propLabel2X1A.companyInformation : ''),
            (label2X1A.productName = propLabel2X1A.editedProductName ? propLabel2X1A.editedProductName : ''),
            (label2X1A.sellByDateShow = propLabel2X1A.sellByDateEnabled)
          setLabel251ObjSetting(label2X1A)
          set2X1A(propLabel2X1A.isActive)
          setUpdate2X1A(true)
          setUpdate2X1AId(propLabel2X1A.id)
        }
        //get 2X1A QR
        const propLabel2X1AQR = labelList.find(
          (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1AQR',
        )
        if (propLabel2X1AQR) {
          const label2X1AQR = Object.assign({}, label251AQRObjSetting)
          ;(label2X1AQR.companyInfoString = propLabel2X1AQR.companyInformation
            ? propLabel2X1AQR.companyInformation
            : ''),
            (label2X1AQR.productName = propLabel2X1AQR.editedProductName ? propLabel2X1AQR.editedProductName : ''),
            (label2X1AQR.sellByDateShow = propLabel2X1AQR.sellByDateEnabled)
          setLabel251AQRObjSetting(label2X1AQR)
          set2X1AQR(propLabel2X1AQR.isActive)
          setUpdate2X1AQR(true)
          setUpdate2X1AQRId(propLabel2X1AQR.id)
        }
        //get 2X1B
        const propLabel2X1B = labelList.find(
          (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1B',
        )
        if (propLabel2X1B) {
          const label2X1B = Object.assign({}, label252ObjSetting)
          ;(label2X1B.companyInfoString = propLabel2X1B.companyInformation ? propLabel2X1B.companyInformation : ''),
            (label2X1B.productName = propLabel2X1B.editedProductName ? propLabel2X1B.editedProductName : ''),
            (label2X1B.note = propLabel2X1B.notes ? propLabel2X1B.notes : ''),
            (label2X1B.sellByDateShow = propLabel2X1B.sellByDateEnabled)
          setLabel252ObjSetting(label2X1B)
          console.log(propLabel2X1B)
          set2X1B(propLabel2X1B.isActive)
          setUpdate2X1B(true)
          setUpdate2X1BId(propLabel2X1B.id)
        }
        //get 2X1B QR
        const propLabel2X1BQR = labelList.find(
          (label: any) => label.wholesaleCompany.companyId.toString() == companyId && label.name == 'unit 2X1BQR',
        )
        if (propLabel2X1BQR) {
          const label2X1BQR = Object.assign({}, label251BQRObjSetting)
          ;(label2X1BQR.companyInfoString = propLabel2X1BQR.companyInformation
            ? propLabel2X1BQR.companyInformation
            : ''),
            (label2X1BQR.productName = propLabel2X1BQR.editedProductName ? propLabel2X1BQR.editedProductName : ''),
            (label2X1BQR.note = propLabel2X1BQR.notes ? propLabel2X1BQR.notes : ''),
            (label2X1BQR.sellByDateShow = propLabel2X1BQR.sellByDateEnabled)
          setLabel251BQRObjSetting(label2X1BQR)
          set2X1BQR(propLabel2X1BQR.isActive)
          setUpdate2X1BQR(true)
          setUpdate2X1BQRId(propLabel2X1BQR.id)
        }
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const getLabel4X3 = (): label43Obj => {
    return {
      productName: labelSetting.productName,
      lot: labelSetting.lot,
      poNum: labelSetting.poNum,
      isPoNum: label43ObjSetting.isPoNum,
      origin: labelSetting.origin,
      isOrigin: label43ObjSetting.isOrigin,
      companyInfo: label43ObjSetting.companyInfoString,
      isCompanyInfo: label43ObjSetting.isCompanyInfo,
      count: labelSetting.count,
      barCode: labelSetting.barcode1,
      qrCode: labelSetting.qrCode,
      logo: labelSetting.logo,
      isLogo: label43ObjSetting.isLogo,
      sku: label43ObjSetting.sku,
    }
  }

  const getLabel251A = (): label251Obj => {
    return {
      x1A: labelSetting.x1A,
      productName: label251ObjSetting.productName,
      lot: labelSetting.lot,
      origin: labelSetting.origin,
      sellByDateShow: label251ObjSetting.sellByDateShow,
      sellByDate: labelSetting.sellByDate,
      barcode1: labelSetting.barcode1,
      barcode2: labelSetting.barcode2,
      companyInfo: label251ObjSetting.companyInfoString,
      itemName: labelSetting.productName,
    }
  }

  const getLabel251AQR = (): label251AQRObj => {
    return {
      x1A: labelSetting.x1A,
      productName: label251AQRObjSetting.productName,
      lot: labelSetting.lot,
      origin: labelSetting.origin,
      sellByDateShow: label251AQRObjSetting.sellByDateShow,
      sellByDate: labelSetting.sellByDate,
      barcode1: labelSetting.barcode1,
      barcode2: labelSetting.barcode2,
      companyInfo: label251AQRObjSetting.companyInfoString,
      itemName: labelSetting.productName,
    }
  }

  const getLabel251B = (): label252Obj => {
    return {
      x1B: labelSetting.x1B,
      productName: label252ObjSetting.productName,
      lot: labelSetting.lot,
      origin: labelSetting.origin,
      barcode1: labelSetting.barcode1,
      note: label252ObjSetting.note,
      companyInfo: label252ObjSetting.companyInfoString,
      sellByDateShow: label252ObjSetting.sellByDateShow,
      sellByDate: labelSetting.sellByDate,
      itemName: labelSetting.productName,
    }
  }

  const getLabel251BQR = (): label251BQRObj => {
    return {
      x1B: labelSetting.x1B,
      productName: label251BQRObjSetting.productName,
      lot: labelSetting.lot,
      origin: labelSetting.origin,
      barcode1: labelSetting.barcode1,
      note: label251BQRObjSetting.note,
      companyInfo: label251BQRObjSetting.companyInfoString,
      sellByDateShow: label251BQRObjSetting.sellByDateShow,
      sellByDate: labelSetting.sellByDate,
      itemName: labelSetting.productName,
    }
  }

  const getPallet4X6Label = (): pallet4X6Obj => {
    return {
      lot: pallet4X6Setting.lot,
      poNum: pallet4X6Setting.poNum,
      field: pallet4X6Setting.field,
      comm: pallet4X6Setting.comm,
      origin: pallet4X6Setting.origin,
      sku: pallet4X6Setting.sku,
      style: pallet4X6Setting.style,
      size: pallet4X6Setting.size,
      quantity: pallet4X6Setting.quantity,
      description: pallet4X6Setting.description,
      qrCode: pallet4X6Setting.qrCode,
    }
  }

  const update4X3Fn = () => {
    const obj = {
      id: update4X3Id,
      companyInformationEnabled: label43ObjSetting.isCompanyInfo,
      logoEnabled: label43ObjSetting.isLogo,
      originUsaEnabled: label43ObjSetting.isOrigin,
      poNumberEnabled: label43ObjSetting.isPoNum,
      companyInformation: label43ObjSetting.companyInfoString,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm4X3Modal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const update2X1AFn = () => {
    const obj = {
      id: update2X1AId,
      editedProductName: 'Product Name',
      sellByDateEnabled: label251ObjSetting.sellByDateShow,
      companyInformation: label251ObjSetting.companyInfoString,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm2X1AModal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const update2X1AQRFn = () => {
    const obj = {
      id: update2X1AQRId,
      editedProductName: 'Product Name',
      sellByDateEnabled: label251AQRObjSetting.sellByDateShow,
      companyInformation: label251AQRObjSetting.companyInfoString,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm2X1AQRModal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const update2X1BFn = () => {
    const obj = {
      id: update2X1BId,
      editedProductName: 'Product Name',
      notes: label252ObjSetting.note,
      companyInformation: label252ObjSetting.companyInfoString,
      sellByDateEnabled: label252ObjSetting.sellByDateShow,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm2XABModal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const update2X1BQRFn = () => {
    const obj = {
      id: update2X1BQRId,
      editedProductName: 'Product Name',
      notes: label251BQRObjSetting.note,
      companyInformation: label251BQRObjSetting.companyInfoString,
      sellByDateEnabled: label251BQRObjSetting.sellByDateShow,
    }
    const http = new Http()
    const newService = new ProductService(http)
    newService.updateLabel(obj).subscribe({
      next(res: any) {
        setShowConfirm2X1BQRModal(false)
      },
      error(err: any) {
        //
      },
      complete() {
        //setStage(0)
      },
    })
  }

  const getRightPart = () => {
    switch (unit) {
      case '4x3':
        return (
          <div>
            <Label4X3
              labelObj={getLabel4X3()}
              passSetting={(obj) => setLabel43ObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update4X3}
              updateAsyncFn={() => setShowConfirm4X3Modal(true)}
              UOM={dataSource.length > 0 ? dataSource[0].wholesaleOrderItem.pricingUOM : ''}
            />
          </div>
        )
      case '2X1 A':
        return (
          <div>
            <Label25X1A
              labelObj={getLabel251A()}
              passSetting={(obj) => setLabel251ObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update2X1A}
              updateAsyncFn={() => setShowConfirm2X1AModal(true)}
            />
          </div>
        )
      case '2X1 A QR':
        return (
          <div>
            <Label25X1AQR
              labelObj={getLabel251AQR()}
              passSetting={(obj) => setLabel251AQRObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update2X1AQR}
              updateAsyncFn={() => setShowConfirm2X1AQRModal(true)}
            />
          </div>
        )
      case '2X1 B':
        return (
          <div>
            <Label25X1B
              labelObj={getLabel251B()}
              passSetting={(obj) => setLabel252ObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update2X1B}
              updateAsyncFn={() => setShowConfirm2XABModal(true)}
            />
          </div>
        )
      case '2X1 B QR':
        return (
          <div>
            <Label25X1BQR
              labelObj={getLabel251BQR()}
              passSetting={(obj) => setLabel251BQRObjSetting(obj)}
              print={() => setShowModal(true)}
              update={update2X1BQR}
              updateAsyncFn={() => setShowConfirm2X1BQRModal(true)}
            />
          </div>
        )
      case 'Pallet 6X4':
        return (
          <div>
            <Label6X4Pallet
              labelObj={getPallet4X6Label()}
              passSetting={(obj) => setPallet4X6Setting(obj)}
              print={() => setShowModal(true)}
            />
          </div>
        )
    }
  }

  const getMutipleCanvas = () => {
    switch (unit) {
      case '4x3':
        const labels43: label43Obj[] = []
        let uom = ''
        getItems().forEach((item: any, index: number) => {
          const obj: label43Obj = {
            productName: item.wholesaleOrderItem.wholesaleItem.variety,
            lot: labelSetting.lot,
            poNum: labelSetting.poNum,
            isPoNum: label43ObjSetting.isPoNum,
            origin: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.extraOrigin
                ? item.wholesaleOrderItem.extraOrigin
                : ''
              : '',
            isOrigin: label43ObjSetting.isOrigin,
            companyInfo: label43ObjSetting.companyInfoString,
            isCompanyInfo: labelSetting.isCompanyInfo,
            count: item.unit,
            barCode: getWeightBarcode(item),
            qrCode: getWeightBarcode(item),
            logo: labelSetting.logo,
            isLogo: label43ObjSetting.isLogo,
            sku: item.wholesaleOrderItem.wholesaleItem.sku,
          }
          labels43.push(obj)
          uom = item.wholesaleOrderItem.pricingUOM
        })
        return <LabelCanvasPrint labelObjs={labels43} setting={label43ObjSetting} UOM={uom} />
      case '2X1 A':
        const labels2x1: label251Obj[] = []
        getItems().forEach((item: any, index: number) => {
          const obj: label251Obj = {
            x1A: labelSetting.x1A,
            productName:
              label251ObjSetting.productName == 'Product Name'
                ? item.wholesaleOrderItem
                  ? item.wholesaleOrderItem.wholesaleItem
                    ? item.wholesaleOrderItem.wholesaleItem.variety
                      ? item.wholesaleOrderItem.wholesaleItem.variety
                      : ''
                    : ''
                  : ''
                : label251ObjSetting.productName,
            lot: labelSetting.lot,
            origin: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.extraOrigin
                ? item.wholesaleOrderItem.extraOrigin
                : ''
              : '',
            sellByDate:
              label251ObjSetting.sellByDate.toString().indexOf('-') !== -1
                ? label251ObjSetting.sellByDate.replace('-', '/').replace('-', '/')
                : label251ObjSetting.sellByDate,
            sellByDateShow: label251ObjSetting.sellByDateShow,
            barcode1: getWeightBarcode(item),
            barcode2: label251ObjSetting.upcCode
              ? label251ObjSetting.upcCode.toString().length <= 12
                ? label251ObjSetting.upcCode.toString()
                : ''
              : '',
            companyInfo: label251ObjSetting.companyInfoString,
          }
          labels2x1.push(obj)
        })
        return <LabelCanvasPrint1 label251Obj={labels2x1} setting={label251ObjSetting} />
      case '2X1 A QR':
        const labels2x1AQR: label251AQRObj[] = []
        getItems().forEach((item: any, index: number) => {
          const obj: label251AQRObj = {
            x1A: labelSetting.x1A,
            productName:
              label251AQRObjSetting.productName == 'Product Name'
                ? item.wholesaleOrderItem
                  ? item.wholesaleOrderItem.wholesaleItem
                    ? item.wholesaleOrderItem.wholesaleItem.variety
                      ? item.wholesaleOrderItem.wholesaleItem.variety
                      : ''
                    : ''
                  : ''
                : label251AQRObjSetting.productName,
            lot: labelSetting.lot,
            origin: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.extraOrigin
                ? item.wholesaleOrderItem.extraOrigin
                : ''
              : '',
            sellByDate:
              label251AQRObjSetting.sellByDate.toString().indexOf('-') !== -1
                ? label251AQRObjSetting.sellByDate.replace('-', '/').replace('-', '/')
                : label251AQRObjSetting.sellByDate,
            sellByDateShow: label251AQRObjSetting.sellByDateShow,
            barcode1: getWeightBarcode(item),
            barcode2: label251AQRObjSetting.upcCode
              ? label251AQRObjSetting.upcCode.toString().length <= 12
                ? label251AQRObjSetting.upcCode.toString()
                : ''
              : '',
            companyInfo: label251AQRObjSetting.companyInfoString,
          }
          labels2x1AQR.push(obj)
        })
        return <LabelCanvasPrint2X51QR label251Obj={labels2x1AQR} setting={label251AQRObjSetting} />
      case '2X1 B':
        const labels2x1B: label252Obj[] = []
        getItems().forEach((item: any, index: number) => {
          const obj: label252Obj = {
            x1B: labelSetting.x1B,
            productName:
              label252ObjSetting.productName == 'Product Name'
                ? item.wholesaleOrderItem
                  ? item.wholesaleOrderItem.wholesaleItem
                    ? item.wholesaleOrderItem.wholesaleItem.variety
                      ? item.wholesaleOrderItem.wholesaleItem.variety
                      : ''
                    : ''
                  : ''
                : label252ObjSetting.productName,
            lot: labelSetting.lot,
            origin: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.extraOrigin
                ? item.wholesaleOrderItem.extraOrigin
                : ''
              : '',
            barcode1: getWeightBarcode(item),
            note: labelSetting.note,
            companyInfo: label252ObjSetting.companyInfoString,
            sellByDate:
              label252ObjSetting.sellByDate.toString().indexOf('-') !== -1
                ? label252ObjSetting.sellByDate.replace('-', '/').replace('-', '/')
                : label252ObjSetting.sellByDate,
            sellByDateShow: label252ObjSetting.sellByDateShow,
          }
          labels2x1B.push(obj)
        })
        return <LabelCanvasPrint2 label252Obj={labels2x1B} setting={label252ObjSetting} />
      case '2X1 B QR':
        const labels2x1BQR: label251BQRObj[] = []
        getItems().forEach((item: any, index: number) => {
          const obj: label251BQRObj = {
            x1B: labelSetting.x1B,
            productName:
              label251BQRObjSetting.productName == 'Product Name'
                ? item.wholesaleOrderItem
                  ? item.wholesaleOrderItem.wholesaleItem
                    ? item.wholesaleOrderItem.wholesaleItem.variety
                      ? item.wholesaleOrderItem.wholesaleItem.variety
                      : ''
                    : ''
                  : ''
                : label251BQRObjSetting.productName,
            lot: labelSetting.lot,
            origin: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.extraOrigin
                ? item.wholesaleOrderItem.extraOrigin
                : ''
              : '',
            barcode1: getWeightBarcode(item),
            note: labelSetting.note,
            companyInfo: label251BQRObjSetting.companyInfoString,
            sellByDate:
              label251BQRObjSetting.sellByDate.toString().indexOf('-') !== -1
                ? label251BQRObjSetting.sellByDate.replace('-', '/').replace('-', '/')
                : label251BQRObjSetting.sellByDate,
            sellByDateShow: label251BQRObjSetting.sellByDateShow,
          }
          labels2x1BQR.push(obj)
        })
        return <LabelCanvasPrint2X52QR label252Obj={labels2x1BQR} setting={label251BQRObjSetting} />
      case 'Pallet 6X4':
        const labels6X4Pallet: pallet4X6Obj[] = []
        getItems().forEach((item: any, index: number) => {
          const obj: pallet4X6Obj = {
            lot: props.weightItem.lotId,
            poNum: props.weightItem.lotId ? props.orderId : '',
            field: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.modifiers
                ? item.wholesaleOrderItem.modifiers
                : ''
              : '',
            comm: props.weightItem.category,
            origin: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.wholesaleItem.origin
                ? item.wholesaleOrderItem.wholesaleItem.origin
                : ''
              : '',
            sku: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.wholesaleItem.sku
                ? item.wholesaleOrderItem.wholesaleItem.sku
                : ''
              : '',
            style: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.wholesaleItem.packing
                ? item.wholesaleOrderItem.wholesaleItem.packing.toString()
                : ''
              : '',
            size: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.wholesaleItem.size
                ? item.wholesaleOrderItem.wholesaleItem.size
                : ''
              : '',
            quantity: item.unit,
            description: item.wholesaleOrderItem
              ? item.wholesaleOrderItem.wholesaleItem.variety
                ? item.wholesaleOrderItem.wholesaleItem.variety
                : ''
              : '',
            qrCode: barcodeString,
          }
          labels6X4Pallet.push(obj)
        })
        return <LabelCanvasPrint6X4Pallet label6X4Obj={labels6X4Pallet} setting={pallet4X6Setting} />
    }
  }

  return (
    <div style={{ padding: '0px' }}>
      <div style={{ height: '54px', borderBottom: '1px solid #D8DBDB' }}>
        <p style={{ marginLeft: '24px', paddingTop: '16px', fontSize: '18px' }}>{`Labels - ${
          unit == '2X1 A' ? '2.5X1 A' : unit == '2X1 B' ? '2.5X1 B' : unit
        }`}</p>
      </div>
      <div style={{ display: 'flex', backgroundColor: 'white' }}>
        <div
          style={{
            width: '160px',
            display: 'inline',
            textAlign: 'left',
            position: 'relative',
          }}
        >
          <p
            style={{
              paddingLeft: '24px',
              cursor: 'pointer',
              height: '36px',
              lineHeight: '36px',
              marginBottom: '0px',
              background: `${unit == '4x3' ? '#F2F9F3' : 'white'}`,
            }}
            onClick={() => setUnit('4x3')}
          >
            UNIT 4x3
          </p>
          {show2X1A ? (
            <p
              style={{
                paddingLeft: '24px',
                cursor: 'pointer',
                height: '36px',
                lineHeight: '36px',
                marginBottom: '0px',
                background: `${unit == '2X1 A' ? '#F2F9F3' : 'white'}`,
              }}
              onClick={() => setUnit('2X1 A')}
            >
              UNIT 2.5X1 A
            </p>
          ) : (
            <></>
          )}
          {show2X1B ? (
            <p
              onClick={() => setUnit('2X1 B')}
              style={{
                paddingLeft: '24px',
                cursor: 'pointer',
                height: '36px',
                lineHeight: '36px',
                marginBottom: '0px',
                background: `${unit == '2X1 B' ? '#F2F9F3' : 'white'}`,
              }}
            >
              UNIT 2.5X1 B
            </p>
          ) : (
            <></>
          )}
          {show2X1AQR ? (
            <p
              style={{
                paddingLeft: '24px',
                cursor: 'pointer',
                height: '36px',
                lineHeight: '36px',
                marginBottom: '0px',
                background: `${unit == '2X1 A QR' ? '#F2F9F3' : 'white'}`,
              }}
              onClick={() => setUnit('2X1 A QR')}
            >
              UNIT 2.5X1 A QR
            </p>
          ) : (
            <></>
          )}
          {show2X1BQR ? (
            <p
              onClick={() => setUnit('2X1 B QR')}
              style={{
                paddingLeft: '24px',
                cursor: 'pointer',
                height: '36px',
                lineHeight: '36px',
                marginBottom: '0px',
                background: `${unit == '2X1 B QR' ? '#F2F9F3' : 'white'}`,
              }}
            >
              UNIT 2.5X1 B QR
            </p>
          ) : (
            <></>
          )}

          <div
            style={{
              height: '67px',
              width: '149px',
              background: '#e8e8e8',
              position: 'absolute',
              bottom: '0px',
            }}
          >
            <p style={{ color: '#e8e8e8' }}>s</p>
          </div>
        </div>
        {getRightPart()}
        <Modal
          style={{ padding: '0px' }}
          width={703}
          footer={null}
          visible={showModal}
          onCancel={() => {
            setShowModal(false)
          }}
        >
          <div>{getMutipleCanvas()}</div>
        </Modal>
        <NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm4X3mModal}
          onCancel={() => {
            setShowConfirm4X3Modal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update4X3Fn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm4X3Modal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>
        <NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm2X1AModal}
          onCancel={() => {
            setShowConfirm2X1AModal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update2X1AFn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm2X1AModal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>
        <NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm2X1AQRModal}
          onCancel={() => {
            setShowConfirm2X1AQRModal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update2X1AQRFn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm2X1AQRModal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>
        <NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm2XABModal}
          onCancel={() => {
            setShowConfirm2XABModal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update2X1BFn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm2XABModal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>
        <NewLabelModal
          width={703}
          footer={null}
          visible={showConfirm2X1BQRModal}
          onCancel={() => {
            setShowConfirm2X1BQRModal(false)
          }}
        >
          <div style={{ height: '114px', textAlign: 'center' }}>
            <p style={{ height: '114px', paddingTop: '47px', fontSize: '15px' }}>
              Are you sure you want to overwrite this label template?
            </p>
          </div>
          <div
            style={{
              paddingLeft: '33px',
              paddingTop: '14px',
              paddingBottom: '14px',
              textAlign: 'left',
              display: 'felx',
              backgroundColor: '#e8e8e8',
              width: '100%',
              position: 'relative',
              borderTop: '1px solid #F7F7F7',
              height: '68px',
            }}
          >
            <ThemeButton
              style={{ right: '-3px', height: '40px' }}
              onClick={() => {
                update2X1BQRFn()
              }}
            >
              Overwrite Template
            </ThemeButton>
            <Button
              style={{
                right: '-40px',
                height: '40px',
                color: 'green',
                backgroundColor: '#e8e8e8',
                border: '1px #e8e8e8 solid',
              }}
              onClick={() => {
                setShowConfirm2X1BQRModal(false)
              }}
            >
              Cancel
            </Button>
          </div>
        </NewLabelModal>
      </div>
    </div>
  )
}

export default NewPrintWeightLabel
