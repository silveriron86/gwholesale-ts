import React, { FC, useRef, useEffect, useState } from 'react'
import { fabric } from 'fabric'
import QRCode from 'react-qr-code'
import JsBarcode from 'jsbarcode'
import { useImmer } from 'use-immer'
import { label43Obj, label43ObjSetting } from '~/modules/product/components/Label/components/type/type'
import { Flex, ThemeButton } from '~/modules/customers/customers.style'
import { Input, Progress, Spin } from 'antd'
import { PalletLabelWrapper } from '../../../_style'
import Barcode from 'react-barcode'
import { printWindow } from '~/common/utils'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  labelObjs: label43Obj[]
  setting: label43ObjSetting
  UOM: string
}

const LabelCanvasPrint = ({ labelObjs, setting, UOM }: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false)
  const [elements, setElements] = useState<any>(null)

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setElements(getHtmlElements())
      setLoading(false)
    }, 3000)
  }, [labelObjs, setting])

  useEffect(() => {
    //console.log(loading)
  }, [loading])

  const printCanvas = () => {
    printWindow('PrintLabelsModal')
  }

  const getHtmlElements = () => {
    return labelObjs.map((item, index) => {
      return (
        <div key={`row-${index}`} style={{ height: '283px', width: '401px' }}>
          <div style={{ textAlign: 'center', justifyContent: 'center' }}>
            <Barcode value={item.barCode} displayValue={true} fontSize={14} width={1} height={40} />
          </div>
          <div>
            <p style={{ fontSize: '20px', marginBottom: '0px', height: '40px', lineHeight: '20px', color: 'black' }}>
              {item.productName}
            </p>
            <Flex style={{ display: 'flex' }}>
              <p
                style={{
                  fontSize: '56px',
                  width: '320px',
                  marginBottom: '0px',
                  fontWeight: 'bold',
                  height: '56px',
                  lineHeight: '56px',
                  color: 'black',
                }}
              >
                {item.sku.substring(0, 8)}
              </p>
              <div style={{ marginTop: '10px' }}>
                <QRCode value={item.barCode} size={65} />
              </div>
            </Flex>
            <div style={{ display: 'flex' }}>
              <div style={{ width: '320px' }}>
                <p
                  style={{ fontSize: '16px', marginBottom: '2px', height: '16px', lineHeight: '16px', color: 'black' }}
                >{`Lot:${item.lot}`}</p>
                {setting.isLogo == true ? (
                  <img src={item.logo} style={{ height: '50px', width: '150px', marginTop: '2px' }} />
                ) : (
                  <div style={{ display: 'inline' }}>
                    <p
                      style={{
                        fontSize: '16px',
                        marginBottom: '2px',
                        height: '16px',
                        lineHeight: '16px',
                        color: 'black',
                      }}
                    >
                      {setting.isPoNum ? `PO:${item.poNum}` : ''}
                    </p>
                    <p
                      style={{
                        fontSize: '16px',
                        marginBottom: '2px',
                        height: '16px',
                        lineHeight: '16px',
                        color: 'black',
                      }}
                    >
                      {setting.isOrigin ? `Origin:${item.origin == null ? '' : item.origin}` : ''}
                    </p>
                    <p
                      style={{
                        fontSize: '16px',
                        marginBottom: '0px',
                        height: '16px',
                        lineHeight: '16px',
                        color: 'black',
                      }}
                    >
                      {setting.companyInfoString && setting.isCompanyInfo ? setting.companyInfoString : ''}
                    </p>
                  </div>
                )}
              </div>
              <div style={{ display: 'flex', position: 'relative' }}>
                <p
                  style={{
                    fontSize: '20px',
                    marginBottom: '2px',
                    height: '20px',
                    lineHeight: '20px',
                    right: '-65px',
                    top: '30px',
                    position: 'absolute',
                    fontWeight: 'bold',
                    color: 'black',
                  }}
                >
                  {item.count.indexOf('.') != -1 ? `${item.count}` : `${item.count ? `${item.count}.00` : '0'}`}
                </p>
                <p
                  style={{
                    fontSize: '20px',
                    marginBottom: '0px',
                    height: '20px',
                    lineHeight: '20px',
                    right: '-65px',
                    top: '50px',
                    position: 'absolute',
                    color: 'black',
                  }}
                >
                  {UOM}
                </p>
              </div>
            </div>
          </div>
          <div style={{ pageBreakAfter: 'always' }} />
        </div>
      )
    })
  }

  return (
    <div style={{ height: 'auto', padding: '20px' }}>
      <Spin style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == true ? 'inline' : 'none'}` }} />
      <ThemeButton
        style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == false ? 'inline' : 'none'}` }}
        onClick={() => {
          printCanvas()
        }}
      >
        Print
      </ThemeButton>
      <div id="PrintLabelsModal" style={{ display: 'none' }}>
        {elements ? elements : ''}
      </div>
    </div>
  )
}

export default LabelCanvasPrint

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
