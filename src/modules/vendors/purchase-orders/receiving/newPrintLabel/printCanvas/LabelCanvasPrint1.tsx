import React, { FC, useRef, useEffect, useState } from 'react'
import { fabric } from 'fabric'
import QRCode from 'qrcode-svg'
import JsBarcode from 'jsbarcode'
import { useImmer } from 'use-immer'
import { Flex, ThemeButton } from '~/modules/customers/customers.style'
import { label251Obj, label251ObjSetting } from '~/modules/product/components/Label/components/type/type'
import { Spin } from 'antd'
import { printWindow } from '~/common/utils'
import Barcode from 'react-barcode'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  label251Obj: label251Obj[]
  setting: label251ObjSetting
}

const LabelCanvasPrint1 = ({ label251Obj, setting }: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false)
  const [elements, setElements] = useState<any>(null)

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setElements(getHtmlElements())
      setLoading(false)
    }, 1000)
  }, [label251Obj, setting])

  useEffect(() => {
    if (loading == false) {
      JsBarcode('.upc-code').init()
    }
  }, [loading])

  const getTime = (time: string) => {
    return time.toString().indexOf('/') == -1 ? timeFn(parseInt(time)) : time.replace('/', '-').replace('/', '-')
  }

  const timeFn = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const printCanvas = () => {
    printWindow('PrintLabels25X1')
  }

  const textToSvgBarcode2 = (text: string) => {
    const svg = document.createElement('svg')
    JsBarcode(svg, text, {
      format: 'UPC',
      width: 1.7,
      height: 50,
      fontSize: 15,
    })

    return svg.outerHTML
  }

  const upcExist = (code: string) => {
    try {
      const barcodeStr = textToSvgBarcode2(code)
      return true
    } catch {
      return false
    }
  }

  const getHtmlElements = () => {
    return label251Obj.map((item, index) => {
      return (
        <div key={`row-${index}`} style={{ height: '100px', width: '250px' }}>
          <div style={{ display: 'flex', height: '30px' }}>
            <Barcode value={item.barcode1} displayValue={true} fontSize={8} width={1} height={16} />
          </div>
          <div style={{ display: 'flex', height: '30px' }}>
            <div style={{ display: 'inline', marginTop: '10px', width: '75px', marginLeft: '2px' }}>
              <p
                style={{
                  fontSize: '7px',
                  marginBottom: '2px',
                  height: '5px',
                  lineHeight: '5px',
                  color: 'black',
                  fontFamily: 'Arial Narrow',
                }}
              >
                {`LOT:${item.lot}`}
              </p>
              <p
                style={{
                  fontSize: '7px',
                  marginBottom: '2px',
                  height: '5px',
                  lineHeight: '6px',
                  color: 'black',
                  fontFamily: 'Arial Narrow',
                }}
              >
                {`ORIGIN:${item.origin == null ? '' : item.origin.substring(0, 12)}`}
              </p>
              <p
                style={{
                  fontSize: '7px',
                  marginBottom: '0px',
                  height: '5px',
                  lineHeight: '6px',
                  color: 'black',
                  fontFamily: 'Arial Narrow',
                }}
              >
                {setting.sellByDateShow ? `SELL BY: ${getTime(item.sellByDate)}` : ''}
              </p>
            </div>
            <p
              style={{
                fontSize: '9px',
                paddingTop: '0px',
                marginBottom: '0px',
                height: '20px',
                lineHeight: '9px',
                textAlign: 'left',
                color: 'black',
                fontWeight: 'bold',
                marginTop: '9px',
                width: '175px',
              }}
            >
              {item.productName}
            </p>
          </div>
          <div style={{ display: 'flex', height: '30px' }}>
            <div style={{ width: '55%' }}>
              {upcExist(item.barcode2) ? (
                <svg
                  className="upc-code"
                  jsbarcode-format="upc"
                  jsbarcode-value={item.barcode2}
                  jsbarcode-textmargin="0"
                  jsbarcode-fontsize="8px"
                  jsbarcode-width="1"
                  jsbarcode-height="10"
                ></svg>
              ) : (
                <p style={{ color: 'white' }}></p>
              )}
            </div>
            <p
              style={{
                fontSize: '8px',
                marginLeft: '2px',
                marginBottom: '0px',
                height: '8px',
                lineHeight: '8px',
                marginTop: '10px',
                color: 'black',
                fontWeight: 'bold',
              }}
            >
              {item.companyInfo}
            </p>
          </div>
          <div style={{ pageBreakAfter: 'always' }} />
        </div>
      )
    })
  }

  return (
    <div style={{ height: 'auto', padding: '20px' }}>
      <Spin style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == true ? 'inline' : 'none'}` }} />
      <ThemeButton
        style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == false ? 'inline' : 'none'}` }}
        onClick={() => {
          printCanvas()
        }}
      >
        Print
      </ThemeButton>
      <div id="PrintLabels25X1" style={{ display: 'none' }}>
        {elements ? elements : ''}
      </div>
    </div>
  )
}

export default LabelCanvasPrint1

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
