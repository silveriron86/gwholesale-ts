import React, { FC, useRef, useEffect, useState } from 'react'
import { fabric } from 'fabric'
import JsBarcode from 'jsbarcode'
import { useImmer } from 'use-immer'
import { label252Obj, label252ObjSetting } from '~/modules/product/components/Label/components/type/type'
import { Flex, ThemeButton } from '~/modules/customers/customers.style'
import { Spin } from 'antd'
import { printWindow } from '~/common/utils'
import Barcode from 'react-barcode'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  label252Obj: label252Obj[]
  setting: label252ObjSetting
}

const LabelCanvasPrint2 = ({ label252Obj, setting }: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false)
  const [elements, setElements] = useState<any>(null)

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setElements(getHtmlElements())
      setLoading(false)
    }, 3000)
  }, [label252Obj, setting])

  useEffect(() => {
    //console.log(loading)
  }, [loading])

  const getTime = (time: string) => {
    return time.toString().indexOf('/') == -1 ? timeFn(parseInt(time)) : time.replace('/', '-').replace('/', '-')
  }

  const timeFn = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const printCanvas = () => {
    printWindow('PrintLabels25X1')
  }

  const getHtmlElements = () => {
    return label252Obj.map((item, index) => {
      return (
        <div key={`row-${index}`} style={{ height: '100px', width: '250px' }}>
          <div style={{ display: 'flex', height: '30px' }}>
            <Barcode value={item.barcode1} displayValue={true} fontSize={8} width={1} height={16} />
          </div>
          <div style={{ display: 'flex', height: '30px' }}>
            <div style={{ display: 'inline', marginTop: '10px', width: '75px', marginLeft: '2px' }}>
              <p
                style={{
                  fontSize: '7px',
                  marginBottom: '2px',
                  height: '5px',
                  lineHeight: '5px',
                  color: 'black',
                  fontFamily: 'Arial Narrow',
                }}
              >
                {`LOT:${item.lot}`}
              </p>
              <p
                style={{
                  fontSize: '7px',
                  marginBottom: '2px',
                  height: '5px',
                  lineHeight: '6px',
                  color: 'black',
                  fontFamily: 'Arial Narrow',
                }}
              >
                {`ORIGIN:${item.origin == null ? '' : item.origin.substring(0, 12)}`}
              </p>
              <p
                style={{
                  fontSize: '7px',
                  marginBottom: '0px',
                  height: '5px',
                  lineHeight: '6px',
                  color: 'black',
                  fontFamily: 'Arial Narrow',
                }}
              >
                {setting.sellByDateShow ? `SELL BY: ${getTime(item.sellByDate)}` : ''}
              </p>
            </div>
            <p
              style={{
                fontSize: '9px',
                paddingTop: '0px',
                marginBottom: '0px',
                height: '20px',
                lineHeight: '9px',
                textAlign: 'left',
                color: 'black',
                fontWeight: 'bold',
                marginTop: '9px',
                width: '175px',
              }}
            >
              {item.productName}
            </p>
          </div>
          <div style={{ textAlign: 'center' }}>
            <p style={{ marginBottom: '2px', marginTop: '2px', color: 'black', fontSize: '8px', fontWeight: 'bold' }}>
              {setting.note}
            </p>
            <p
              style={{
                color: 'black',
                fontSize: '8px',
                fontWeight: 'bold',
              }}
            >
              {setting.companyInfoString}
            </p>
          </div>
          <div style={{ pageBreakAfter: 'always' }} />
        </div>
      )
    })
  }

  return (
    <div style={{ height: 'auto', padding: '20px' }}>
      <Spin style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == true ? 'inline' : 'none'}` }} />
      <ThemeButton
        style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == false ? 'inline' : 'none'}` }}
        onClick={() => {
          printCanvas()
        }}
      >
        Print
      </ThemeButton>
      <div id="PrintLabels25X1" style={{ display: 'none' }}>
        {elements ? elements : ''}
      </div>
    </div>
  )
}

export default LabelCanvasPrint2
