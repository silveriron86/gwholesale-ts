import React, { useEffect, useState } from 'react'
import { fabric } from 'fabric'
import JsBarcode from 'jsbarcode'
import { ThemeButton } from '~/modules/customers/customers.style'
import { label251AQRObj, label251ObjSetting } from '~/modules/product/components/Label/components/type/type'
import { Spin } from 'antd'
import { printWindow } from '~/common/utils'
import QRCode from 'react-qr-code'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  label251Obj: label251AQRObj[]
  setting: label251ObjSetting
}

const LabelCanvasPrint2X51QR = ({ label251Obj, setting }: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false)
  const [elements, setElements] = useState<any>(null)

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setElements(getHtmlElements())
      setLoading(false)
    }, 1000)
  }, [label251Obj, setting])

  useEffect(() => {
    if (loading == false) {
      JsBarcode('.upc-code').init()
    }
  }, [loading])

  const getTime = (time: string) => {
    return time.toString().indexOf('/') == -1 ? timeFn(parseInt(time)) : time.replace('/', '-').replace('/', '-')
  }

  const timeFn = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  const printCanvas = () => {
    printWindow('PrintLabels25X1')
  }

  const textToSvgBarcode2 = (text: string) => {
    const svg = document.createElement('svg')
    JsBarcode(svg, text, {
      format: 'UPC',
      width: 1.7,
      height: 50,
      fontSize: 15,
    })

    return svg.outerHTML
  }

  const upcExist = (code: string) => {
    try {
      const barcodeStr = textToSvgBarcode2(code)
      return true
    } catch {
      return false
    }
  }

  const getHtmlElements = () => {
    return label251Obj.map((item, index) => {
      return (
        <div key={`row-${index}`} style={{ height: '100px', width: '250px' }}>
          <div style={{ display: 'flex', height: '60px', paddingTop: '10px' }}>
            <div style={{ width: '195px', display: 'inline', paddingLeft: '8px' }}>
              <div style={{ height: '30px' }}>
                <p
                  style={{
                    fontSize: '9px',
                    marginBottom: '0px',
                    height: '20px',
                    lineHeight: '9px',
                    textAlign: 'left',
                    color: 'black',
                    fontWeight: 'bold',
                    width: '100%',
                  }}
                >
                  {item.productName}
                </p>
              </div>
              <div style={{ height: '30px' }}>
                <p
                  style={{
                    fontSize: '7px',
                    marginBottom: '2px',
                    height: '5px',
                    lineHeight: '5px',
                    color: 'black',
                    fontFamily: 'Arial Narrow',
                  }}
                >
                  {`LOT:${item.lot}`}
                </p>
                <p
                  style={{
                    fontSize: '7px',
                    marginBottom: '2px',
                    height: '5px',
                    lineHeight: '6px',
                    color: 'black',
                    fontFamily: 'Arial Narrow',
                  }}
                >
                  {`ORIGIN:${item.origin == null ? '' : item.origin.substring(0, 12)}`}
                </p>
                <p
                  style={{
                    fontSize: '7px',
                    marginBottom: '0px',
                    height: '5px',
                    lineHeight: '6px',
                    color: 'black',
                    fontFamily: 'Arial Narrow',
                  }}
                >
                  {setting.sellByDateShow ? `SELL BY: ${getTime(item.sellByDate)}` : ''}
                </p>
              </div>
            </div>
            <div style={{ marginRight: '10px' }}>
              <QRCode value={item.barcode1} size={50} />
            </div>
          </div>
          <div style={{ display: 'flex', height: '40px' }}>
            <div style={{ width: '55%' }}>
              {upcExist(item.barcode2) ? (
                <svg
                  className="upc-code"
                  jsbarcode-format="upc"
                  jsbarcode-value={item.barcode2}
                  jsbarcode-textmargin="0"
                  jsbarcode-fontsize="8px"
                  jsbarcode-width="1"
                  jsbarcode-height="10"
                ></svg>
              ) : (
                <p style={{ color: 'white' }}></p>
              )}
            </div>
            <div style={{ width: '45%' }}>
              <p
                style={{
                  fontSize: '8px',
                  marginLeft: '2px',
                  marginTop: '8px',
                  marginBottom: '0px',
                  height: '24px',
                  lineHeight: '8px',
                  color: 'black',
                  fontWeight: 'bold',
                  wordWrap: 'break-word',
                  overflow: 'hidden',
                }}
              >
                {item.companyInfo}
              </p>
            </div>
          </div>
        </div>
      )
    })
  }

  return (
    <div style={{ height: 'auto', padding: '20px' }}>
      <Spin style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == true ? 'inline' : 'none'}` }} />
      <ThemeButton
        style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == false ? 'inline' : 'none'}` }}
        onClick={() => {
          printCanvas()
        }}
      >
        Print
      </ThemeButton>
      <div id="PrintLabels25X1" style={{ display: 'none' }}>
        {elements ? elements : ''}
      </div>
    </div>
  )
}

export default LabelCanvasPrint2X51QR

const toObjectFn = (target: any) => {
  return (function(toObject) {
    return function() {
      return fabric.util.object.extend(toObject.call(target), {
        id: target.id,
      })
    }
  })(target.toObject)
}

const addId = (target: any, id: number) => {
  target.id = id
  target.toObject = toObjectFn(target)
}
