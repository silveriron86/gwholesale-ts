import React, { useEffect, useState } from 'react'
import {
  label252Obj,
  label252ObjSetting,
  pallet4X6Obj,
  pallet4X6ObjSetting,
} from '~/modules/product/components/Label/components/type/type'
import { ThemeButton } from '~/modules/customers/customers.style'
import { Spin } from 'antd'
import { printWindow } from '~/common/utils'
import QRCode from 'react-qr-code'

interface IOptions {
  id: number
  key: Symbol
  label: string
  type: string
  url?: string
}

interface IProps {
  label6X4Obj: pallet4X6Obj[]
  setting: pallet4X6ObjSetting
}

const LabelCanvasPrint6X4Pallet = ({ label6X4Obj, setting }: IProps): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false)
  const [elements, setElements] = useState<any>(null)

  useEffect(() => {
    setLoading(true)
    setTimeout(() => {
      setElements(getHtmlElements())
      setLoading(false)
    }, 3000)
  }, [label6X4Obj, setting])

  useEffect(() => {
    //console.log(loading)
  }, [loading])

  const printCanvas = () => {
    printWindow('PrintLabels6X4')
  }

  const getHtmlElements = () => {
    return label6X4Obj.map((item, index) => {
      return (
        <div key={`row-${index}`} style={{ height: '385px', width: '580px' }}>
          <div style={{ width: '568px', margin: '2px', height: '280px', border: 'solid black 1px' }}>
            <div style={{ width: '568px', height: '65px', borderBottom: 'solid black 1px', display: 'flex' }}>
              <div style={{ width: '196px', borderRight: 'solid black 1px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  Lot #
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  {item.lot}
                </p>
              </div>
              <div style={{ width: '150px', borderRight: 'solid black 1px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  PO #
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  {item.poNum}
                </p>
              </div>
              <div style={{ width: '222px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  Field
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    fontWeight: 'bold',
                    marginBottom: '0px',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  {item.field}
                </p>
              </div>
            </div>
            <div style={{ width: '568px', height: '65px', borderBottom: 'solid black 1px', display: 'flex' }}>
              <div style={{ width: '196px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  Comm
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  {item.comm}
                </p>
              </div>
              <div style={{ width: '150px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  Origin
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  {item.origin}
                </p>
              </div>
              <div style={{ width: '222px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    height: '20px',
                    fontFamily: 'Arial',
                  }}
                >
                  SKU
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    lineHeight: '22px',
                    fontFamily: 'Arial',
                    overflow: 'hidden',
                  }}
                >
                  {item.sku}
                </p>
              </div>
            </div>
            <div style={{ width: '568px', height: '65px', borderBottom: 'solid black 1px', display: 'flex' }}>
              <div style={{ width: '196px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  Style
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  {item.style}
                </p>
              </div>
              <div style={{ width: '150px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  Size
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    lineHeight: '22px',
                    fontFamily: 'Arial',
                    overflow: 'hidden',
                  }}
                >
                  {item.size}
                </p>
              </div>
              <div style={{ width: '222px' }}>
                <p
                  style={{
                    fontSize: '16px',
                    marginLeft: '5px',
                    marginBottom: '0px',
                    fontWeight: 'normal',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  Quantity
                </p>
                <p
                  style={{
                    fontSize: '26px',
                    marginLeft: '10px',
                    marginBottom: '0px',
                    fontWeight: 'bold',
                    color: 'black',
                    fontFamily: 'Arial',
                  }}
                >
                  {item.quantity}
                </p>
              </div>
            </div>
            <div style={{ width: '568px', height: '100px' }}>
              <p
                style={{
                  fontSize: '16px',
                  marginLeft: '5px',
                  marginBottom: '0px',
                  fontWeight: 'normal',
                  color: 'black',
                  fontFamily: 'Arial',
                  height: '20px',
                }}
              >
                Description
              </p>
              <p
                style={{
                  fontSize: '32px',
                  marginLeft: '10px',
                  marginBottom: '0px',
                  fontWeight: 'bold',
                  color: 'black',
                  fontFamily: 'Arial',
                  lineHeight: '32px',
                  height: '64px',
                  overflow: 'hidden',
                }}
              >
                {item.description}
              </p>
            </div>
          </div>
          <div style={{ height: '90px', display: 'flex' }}>
            <div style={{ width: '33%' }}>
              <p
                style={{
                  fontSize: '17px',
                  marginLeft: '10px',
                  marginBottom: '0px',
                  fontWeight: 'bold',
                  lineHeight: '17px',
                  height: '35px',
                  color: 'black',
                  wordBreak: 'break-word',
                  overflow: 'hidden',
                  fontFamily: 'Arial',
                  width: '99%',
                }}
              >
                {item.description}
              </p>
              <div style={{ display: 'flex', height: '50px' }}>
                <div style={{ width: '130px' }}>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      fontWeight: 'bold',
                      marginBottom: '0px',
                      color: 'black',
                      fontFamily: 'Arial',
                      height: '15px',
                    }}
                  >
                    {item.field}
                  </p>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      fontWeight: 'bold',
                      marginBottom: '0px',
                      color: 'black',
                      fontFamily: 'Arial',
                      height: '15px',
                    }}
                  >
                    {item.lot}
                  </p>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      marginBottom: '0px',
                      fontWeight: 'bold',
                      color: 'black',
                      fontFamily: 'Arial',
                      height: '15px',
                    }}
                  >
                    {item.quantity}
                  </p>
                </div>
                <div style={{ marginTop: '2px' }}>
                  <QRCode value={item.qrCode} size={50} />
                </div>
              </div>
            </div>
            <div style={{ width: '33%' }}>
              <p
                style={{
                  fontSize: '17px',
                  marginLeft: '10px',
                  marginBottom: '0px',
                  fontWeight: 'bold',
                  lineHeight: '17px',
                  height: '35px',
                  color: 'black',
                  wordBreak: 'break-word',
                  overflow: 'hidden',
                  width: '99%',
                  fontFamily: 'Arial',
                }}
              >
                {item.description}
              </p>
              <div style={{ display: 'flex', height: '50px' }}>
                <div style={{ width: '150px' }}>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      fontWeight: 'bold',
                      marginBottom: '0px',
                      color: 'black',
                      height: '15px',
                      fontFamily: 'Arial',
                    }}
                  >
                    {item.field}
                  </p>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      fontWeight: 'bold',
                      marginBottom: '0px',
                      color: 'black',
                      fontFamily: 'Arial',
                      height: '15px',
                    }}
                  >
                    {item.lot}
                  </p>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      marginBottom: '0px',
                      fontWeight: 'bold',
                      color: 'black',
                      height: '15px',
                      fontFamily: 'Arial',
                    }}
                  >
                    {item.quantity}
                  </p>
                </div>
                <div style={{ marginTop: '2px' }}>
                  <QRCode value={item.qrCode} size={50} />
                </div>
              </div>
            </div>
            <div style={{ width: '33%' }}>
              <p
                style={{
                  fontSize: '17px',
                  marginLeft: '10px',
                  marginBottom: '0px',
                  fontWeight: 'bold',
                  lineHeight: '17px',
                  height: '35px',
                  color: 'black',
                  wordBreak: 'break-word',
                  overflow: 'hidden',
                  fontFamily: 'Arial',
                  width: '99%',
                }}
              >
                {item.description}
              </p>
              <div style={{ display: 'flex', height: '50px' }}>
                <div style={{ width: '130px' }}>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      fontWeight: 'bold',
                      marginBottom: '0px',
                      color: 'black',
                      fontFamily: 'Arial',
                      height: '15px',
                    }}
                  >
                    {item.field}
                  </p>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      fontWeight: 'bold',
                      marginBottom: '0px',
                      color: 'black',
                      fontFamily: 'Arial',
                      height: '15px',
                    }}
                  >
                    {item.lot}
                  </p>
                  <p
                    style={{
                      fontSize: '18px',
                      marginLeft: '10px',
                      marginBottom: '0px',
                      fontWeight: 'bold',
                      color: 'black',
                      fontFamily: 'Arial',
                      height: '15px',
                    }}
                  >
                    {item.quantity}
                  </p>
                </div>
                <div style={{ marginTop: '2px' }}>
                  <QRCode value={item.qrCode} size={50} />
                </div>
              </div>
            </div>
          </div>
          <div style={{ pageBreakAfter: 'always' }} />
        </div>
      )
    })
  }

  return (
    <div style={{ height: 'auto', padding: '20px' }}>
      <Spin style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == true ? 'inline' : 'none'}` }} />
      <ThemeButton
        style={{ marginTop: '20px', marginLeft: '20px', display: `${loading == false ? 'inline' : 'none'}` }}
        onClick={() => {
          printCanvas()
        }}
      >
        Print
      </ThemeButton>
      <div id="PrintLabels6X4" style={{ display: 'none' }}>
        {elements ? elements : ''}
      </div>
    </div>
  )
}

export default LabelCanvasPrint6X4Pallet
