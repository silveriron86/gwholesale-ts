import { Button, Checkbox, DatePicker, Input, Radio } from 'antd'
import moment from 'moment'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas1 from '~/modules/product/components/Label/components/LabelCanvas1'
import { label251Obj, label251ObjSetting } from '~/modules/product/components/Label/components/type/type'

interface IProps {
  labelObj: label251Obj
  passSetting: (obj: label251ObjSetting) => void
  print: () => void
  update: boolean
  updateAsyncFn: () => void
}

const Label25X1A = ({ labelObj, passSetting, print, update, updateAsyncFn }: IProps): JSX.Element => {
  const [name, setProductName] = useState(labelObj.productName)
  const [sellByDate, setSellByDate] = useState(labelObj.sellByDate)
  const [sellByDatShow, setSellByDatShow] = useState(labelObj.sellByDateShow)
  const [companyInformation, setCompanyInformation] = useState(labelObj.companyInfo)
  const [itemName, setItemName] = useState(labelObj.itemName ? labelObj.itemName : '')
  const [upcCode, setUpcCode] = useState(labelObj.barcode2)

  const lot = labelObj.lot
  const origin = labelObj.origin
  const bar_code1 = labelObj.barcode1

  useEffect(() => {
    const obj: label251ObjSetting = {
      productName: name,
      sellByDateShow: sellByDatShow,
      sellByDate: sellByDate,
      companyInfoString: companyInformation,
      upcCode: upcCode,
    }
    passSetting(obj)
  }, [name, sellByDate, sellByDatShow, companyInformation, upcCode])

  useEffect(() => {
    setProductName(labelObj.productName)
    setSellByDate(labelObj.sellByDate)
    setSellByDatShow(labelObj.sellByDateShow)
    setCompanyInformation(labelObj.companyInfo)
    setItemName(labelObj.itemName ? labelObj.itemName : '')
    setUpcCode(labelObj.barcode2)
  }, [update])

  const getLabelObj = (): label251Obj => {
    return {
      x1A: labelObj.x1A,
      productName: name == 'Product Name' ? itemName : name,
      lot: lot,
      origin: origin,
      sellByDate: sellByDatShow ? sellByDate : '',
      sellByDateShow: sellByDatShow,
      barcode1: bar_code1,
      barcode2: upcCode,
      companyInfo: companyInformation,
    }
  }

  const setDate = (e: any) => {
    const date = new Date(e)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    setSellByDate(`${month}-${day}-${year}`)
  }

  const time = (time = +new Date()) => {
    var date = new Date(time)
    const year = date.getFullYear()
    const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
    const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    return `${month}-${day}-${year}`
  }

  return (
    <div
      style={{
        borderLeft: '1px solid #D8DBDB',
      }}
    >
      <div style={{ width: '460px', marginLeft: '33px' }}>
        <div style={{ paddingTop: '23px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            Product Name
          </p>
          <Input
            style={{ marginTop: '4px', width: '95%' }}
            value={name == 'Product Name' ? itemName : name}
            onChange={(e) => setProductName(e.target.value)}
          />
        </div>
        <div style={{ marginTop: '0px', display: 'flex' }}>
          <div style={{ width: '25%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
                marginTop: '28px',
              }}
            >
              Lot #
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{lot}</p>
          </div>
          <div style={{ width: '25%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
                marginTop: '28px',
              }}
            >
              Origin
            </p>
            <p style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'normal', marginTop: '9px' }}>{origin}</p>
          </div>
          <div style={{ width: '50%', display: 'flex' }}>
            <Checkbox
              style={{ marginTop: '50px', marginRight: '10px' }}
              onChange={(e) => setSellByDatShow(e.target.checked)}
              checked={sellByDatShow}
            />
            <div>
              <p
                style={{
                  fontSize: '14px',
                  lineHeight: '19px',
                  marginBottom: '0px',
                  fontWeight: 'normal',
                  marginTop: '28px',
                }}
              >
                Sell By Date
              </p>
              <DatePicker
                defaultValue={moment(time(parseInt(sellByDate)), 'MM-DD-YYYY')}
                onChange={(e) => setDate(e)}
              />
            </div>
          </div>
        </div>
        <div style={{ marginTop: '21px' }}>
          <p
            style={{
              fontSize: '15px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
              marginTop: '28px',
            }}
          >
            Barcodes
          </p>
          <p
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'bold', marginTop: '3px', marginBottom: '0px' }}
          >
            Code-128 <span style={{ fontWeight: 'normal' }}>{bar_code1}</span>
          </p>
          <div style={{ display: 'flex' }}>
            <p
              style={{
                width: '60px',
                fontSize: '15px',
                lineHeight: '36px',
                fontWeight: 'bold',
                marginTop: '0px',
                marginBottom: '0px',
              }}
            >
              UPC-A
            </p>
            <Input
              style={{ marginTop: '4px', width: '80%' }}
              value={upcCode}
              onChange={(e) => setUpcCode(e.target.value)}
            />
          </div>
        </div>
        <div style={{ display: 'inline' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
              marginTop: '28px',
            }}
          >
            Company Information
          </p>
          <Input
            style={{ marginTop: '4px', width: '80%' }}
            value={companyInformation}
            onChange={(e) => setCompanyInformation(e.target.value)}
          />
        </div>
      </div>
      <div style={{ height: '345px' }}>
        <p
          style={{
            marginLeft: '33px',
            fontSize: '15px',
            lineHeight: '19px',
            marginBottom: '0px',
            fontWeight: 'normal',
            marginTop: '28px',
          }}
        >
          Preview
        </p>
        <LabelCanvas1 label251Obj={getLabelObj()} />
      </div>
      <div
        style={{
          paddingLeft: '33px',
          paddingTop: '14px',
          paddingBottom: '14px',
          textAlign: 'left',
          display: 'felx',
          backgroundColor: '#e8e8e8',
          width: '554px',
          position: 'relative',
          borderTop: '1px solid #F7F7F7',
          height: '68px',
        }}
      >
        <ThemeButton
          style={{ right: '-223px', borderRadius: '40px', height: '40px' }}
          onClick={() => {
            updateAsyncFn()
          }}
        >
          Update Template
        </ThemeButton>
        <ThemeButton
          style={{ right: '-245px', borderRadius: '40px', height: '40px' }}
          onClick={() => {
            print()
          }}
        >
          Print Label
        </ThemeButton>
      </div>
    </div>
  )
}

export default Label25X1A
