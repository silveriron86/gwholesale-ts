import { Input } from 'antd'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas64BPallet from '~/modules/product/components/Label/components/LabelCanvas64BPallet'
import { pallet4X6Obj, pallet4X6ObjSetting } from '~/modules/product/components/Label/components/type/type'

interface IProps {
  labelObj: pallet4X6Obj
  passSetting: (obj: pallet4X6ObjSetting) => void
  print: () => void
}

const Label6X4A2Pallet = ({ labelObj, passSetting, print }: IProps): JSX.Element => {
  const [lot, setLot] = useState(labelObj.lot)
  const [poNum, setPoNum] = useState(labelObj.poNum)
  const [field, setField] = useState(labelObj.field)
  const [comm, setComm] = useState(labelObj.comm)
  const [origin, setOrigin] = useState(labelObj.origin)
  const [sku, setSku] = useState(labelObj.sku)
  const [style, setStyle] = useState(labelObj.style)
  const [size, setSize] = useState(labelObj.size)
  const [quantity, setQuantity] = useState(labelObj.quantity)
  const [description, setDescription] = useState(labelObj.description)
  const [qrCode, setQrCode] = useState(labelObj.qrCode)

  useEffect(() => {
    setLot(labelObj.lot)
    setPoNum(labelObj.poNum)
    setField(labelObj.field)
    setComm(labelObj.comm)
    setOrigin(labelObj.origin)
    setSku(labelObj.sku)
    setStyle(labelObj.style)
    setSize(labelObj.size)
    setQuantity(labelObj.quantity)
    setDescription(labelObj.description)
    setQrCode(labelObj.qrCode)
  }, [labelObj])

  useEffect(() => {
    const obj: pallet4X6ObjSetting = {
      lot: lot,
      poNum: poNum,
      field: field,
      comm: comm,
      origin: origin,
      sku: sku,
      style: style,
      size: size,
      quantity: quantity,
      description: description,
      qrCode: qrCode,
    }
    passSetting(obj)
  }, [lot, poNum, field, comm, origin, sku, style, size, quantity, description, qrCode])

  const getLabelObj = (): pallet4X6Obj => {
    return {
      lot: lot,
      poNum: poNum,
      field: field,
      comm: comm,
      origin: origin,
      sku: sku,
      style: style,
      size: size,
      quantity: quantity,
      description: description,
      qrCode: qrCode,
    }
  }

  return (
    <div
      style={{
        borderLeft: '1px solid #D8DBDB',
      }}
    >
      <div style={{ width: '460px', marginLeft: '33px' }}>
        <div style={{ display: 'flex' }}>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              Lot
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={lot}
              onChange={(e) => setLot(e.target.value)}
            />
          </div>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              poNum
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={poNum}
              onChange={(e) => setPoNum(e.target.value)}
            />
          </div>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              field
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={field}
              onChange={(e) => setField(e.target.value)}
            />
          </div>
        </div>
        <div style={{ display: 'flex' }}>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              comm
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={comm}
              onChange={(e) => setComm(e.target.value)}
            />
          </div>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '18px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              origin
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={origin}
              onChange={(e) => setOrigin(e.target.value)}
            />
          </div>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              sku
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={sku}
              onChange={(e) => setSku(e.target.value)}
            />
          </div>
        </div>
        <div style={{ display: 'flex' }}>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              style
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={style}
              onChange={(e) => setStyle(e.target.value)}
            />
          </div>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              size
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={size}
              onChange={(e) => setSize(e.target.value)}
            />
          </div>
          <div style={{ width: '33%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              quantity
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={quantity}
              onChange={(e) => setQuantity(e.target.value)}
            />
          </div>
        </div>
        <div style={{ display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              description
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                fontWeight: 'normal',
                lineHeight: '25.2px',
                marginBottom: '0px',
                color: '#4A5355',
              }}
            >
              QR code
            </p>
            <Input
              disabled={true}
              style={{ marginTop: '4px', width: '80%' }}
              value={qrCode}
              onChange={(e) => setQrCode(e.target.value)}
            />
          </div>
        </div>
      </div>
      <div style={{ height: '400px', marginLeft: '3px' }}>
        <p
          style={{
            fontSize: '15px',
            lineHeight: '19px',
            marginBottom: '0px',
            fontWeight: 'normal',
            marginTop: '28px',
          }}
        >
          Preview
        </p>
        <LabelCanvas64BPallet labelObj={getLabelObj()} radioNum={0.7} />
      </div>
      <div
        style={{
          paddingLeft: '33px',
          paddingTop: '14px',
          paddingBottom: '14px',
          textAlign: 'left',
          display: 'felx',
          backgroundColor: '#e8e8e8',
          width: '554px',
          position: 'relative',
          borderTop: '1px solid #F7F7F7',
          height: '68px',
        }}
      >
        <ThemeButton
          style={{ right: '-335px', borderRadius: '40px', height: '40px' }}
          onClick={() => {
            print()
          }}
        >
          Print Label
        </ThemeButton>
      </div>
    </div>
  )
}

export default Label6X4A2Pallet
