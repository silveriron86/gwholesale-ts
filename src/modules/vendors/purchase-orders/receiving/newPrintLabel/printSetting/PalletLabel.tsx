import { Button, Checkbox, Input, Radio } from 'antd'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { ThemeButton } from '~/modules/customers/customers.style'
import LabelCanvas from '~/modules/product/components/Label/components/LabelCanvas'
import LotLabelCanvas from '~/modules/product/components/Label/components/LotLabelCanvas'
import PalletLabelCanvas from '~/modules/product/components/Label/components/PalletLabelCanvas'
import {
  label43Obj,
  label43ObjSetting,
  LotLabelObj,
  PalletLabelObj,
} from '~/modules/product/components/Label/components/type/type'

interface IProps {
  labelObj: PalletLabelObj
  passSetting: (obj: label43ObjSetting) => void
  print: () => void
  update: boolean
  updateAsyncFn: () => void
  UOM: string
}

const PalletLabel = ({ labelObj, passSetting, print, update, updateAsyncFn, UOM }: IProps): JSX.Element => {
  const [active, setActive] = useState('Yes')
  const [name, setProductName] = useState(labelObj.productName)
  const [subTitle, setSubTitle] = useState(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
  const [poNum, setPoNum] = useState(labelObj.isPoNum)
  const [Origin, setOrigin] = useState(labelObj.isOrigin)
  const [companyInfo, setCompanyInfo] = useState(labelObj.isCompanyInfo)
  const [companyInfoString, setCompanyInfoString] = useState<string>('Puebio Fruit, ASD')

  const lot = labelObj.lot
  const origin = labelObj.origin
  const poNumber = labelObj.poNum
  const barCode = labelObj.barCode
  const qrCode = labelObj.qrCode

  useEffect(() => {
    const obj: label43ObjSetting = {
      isLogo: subTitle == 'Logo' ? true : false,
      isPoNum: poNum,
      isOrigin: Origin,
      companyInfoString: companyInfoString,
      isCompanyInfo: companyInfo,
      sku: labelObj.sku,
    }
    passSetting(obj)
  }, [subTitle, poNum, Origin, companyInfoString, companyInfo])

  useEffect(() => {
    setProductName(labelObj.productName)
    setSubTitle(labelObj.isLogo ? 'Logo' : 'AdditionalInfo')
    setPoNum(labelObj.isPoNum)
    setOrigin(labelObj.isOrigin)
    setCompanyInfo(labelObj.isCompanyInfo)
    setCompanyInfoString(labelObj.companyInfo)
  }, [update])

  const getLabelObj = (): PalletLabelObj => {
    return {
      productName: name,
      lot: lot,
      poNum: poNumber ? poNumber.toString() : '',
      isPoNum: poNum,
      origin: origin ? origin.toString() : '',
      isOrigin: Origin,
      companyInfo: companyInfo ? companyInfoString.toString() : '',
      isCompanyInfo: companyInfo,
      count: labelObj.count,
      barCode: barCode,
      qrCode: qrCode,
      logo: labelObj.logo,
      isLogo: subTitle == 'Logo' ? true : false,
      sku: labelObj.sku,
      palletId: labelObj.palletId,
      constantRatio: labelObj.constantRatio,
    }
  }

  return (
    <div
      style={{
        borderLeft: '1px solid #D8DBDB',
      }}
    >
      <div style={{ width: '460px', marginLeft: '33px' }}>
        {/*<div style={{ paddingTop: '23px' }}>
          <p
            style={{
              fontSize: '14px',
              lineHeight: '19px',
              marginBottom: '0px',
              fontWeight: 'normal',
            }}
          >
            Product Name
          </p>
          <p
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'bold', marginTop: '9px', marginBottom: '0px' }}
          >
            {name}
          </p>
        </div>
        <div style={{ marginTop: '29px', display: 'flex' }}>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
                color: '#4A5355',
              }}
            >
              Lot #
            </p>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '21px',
                marginTop: '9px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              {lot}
            </p>
          </div>
          <div style={{ width: '50%' }}>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '19px',
                marginBottom: '0px',
                fontWeight: 'normal',
                color: '#4A5355',
              }}
            >
              Origin
            </p>
            <p
              style={{
                fontSize: '14px',
                lineHeight: '21px',
                marginTop: '9px',
                marginBottom: '0px',
                fontWeight: 'normal',
              }}
            >
              {origin}
            </p>
          </div>
        </div>
        <div style={{ marginTop: '21px' }}>
          <p style={{ fontSize: '14px', lineHeight: '19px', marginBottom: '0px', fontWeight: 'normal' }}>Barcodes</p>
          <p
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'bold', marginTop: '3px', marginBottom: '0px' }}
          >
            Code-128 <span style={{ fontWeight: 'normal' }}>{barCode}</span>
          </p>
          <p
            style={{ fontSize: '15px', lineHeight: '21px', fontWeight: 'bold', marginTop: '3px', marginBottom: '0px' }}
          >
            QR <span style={{ fontWeight: 'normal' }}>{qrCode}</span>
          </p>
        </div>
        <div style={{ display: 'inline' }}>
          <div style={{ marginTop: '26px' }}>
            <Radio
              style={{ fontWeight: 'normal' }}
              checked={subTitle == 'Logo' ? true : false}
              onClick={() => {
                setSubTitle('Logo')
              }}
            >
              Logo
            </Radio>
          </div>
          <div style={{ marginTop: '16px' }}>
            <Radio
              style={{ fontWeight: 'normal' }}
              checked={subTitle !== 'Logo' ? true : false}
              onClick={() => {
                setSubTitle('AdditionalInfo')
              }}
            >
              Additional Information
            </Radio>
          </div>
        </div>
        <div style={{ display: subTitle == 'Logo' ? 'none' : 'inline' }}>
          <div style={{ marginTop: '9px', marginLeft: '21px' }}>
            <Checkbox
              style={{ fontWeight: 'normal' }}
              onChange={(e) => {
                setPoNum(e.target.checked)
              }}
              checked={poNum}
            >
              PO Number
              {poNumber == '' && poNumber == null ? (
                ''
              ) : (
                <span style={{ fontWeight: 'normal', color: '#C2C2C2' }}>{` ${poNumber}`}</span>
              )}
            </Checkbox>
          </div>
          <div style={{ marginTop: '7px', marginLeft: '21px' }}>
            <Checkbox
              style={{ fontWeight: 'normal' }}
              onChange={(e) => {
                setOrigin(e.target.checked)
              }}
              checked={Origin}
            >
              Origin
              {origin == '' && origin == null ? (
                ''
              ) : (
                <span style={{ fontWeight: 'normal', color: '#C2C2C2' }}>{` ${origin}`}</span>
              )}
            </Checkbox>
          </div>
          <div style={{ marginTop: '7px', marginLeft: '21px' }}>
            <Checkbox
              style={{ fontWeight: 'normal' }}
              onChange={(e) => {
                setCompanyInfo(e.target.checked)
              }}
              checked={companyInfo}
            >
              Company Information
            </Checkbox>
            <Input
              style={{ marginTop: '4px', marginLeft: '22px', width: '80%' }}
              value={typeof companyInfoString == 'string' ? companyInfoString : ''}
              onChange={(e) => {
                setCompanyInfoString(e.target.value)
              }}
            />
          </div>
        </div> */}
      </div>
      <div style={{ height: '345px' }}>
        <p
          style={{
            marginLeft: '33px',
            fontSize: '15px',
            lineHeight: '19px',
            marginBottom: '0px',
            fontWeight: 'normal',
            marginTop: '28px',
          }}
        >
          Preview
        </p>
        <PalletLabelCanvas labelObj={getLabelObj()} UOM={UOM} />
      </div>
      <div
        style={{
          paddingLeft: '33px',
          paddingTop: '14px',
          paddingBottom: '14px',
          textAlign: 'left',
          display: 'felx',
          backgroundColor: '#e8e8e8',
          width: '554px',
          position: 'relative',
          borderTop: '1px solid #F7F7F7',
          height: '68px',
        }}
      >
        {/*<ThemeButton
          style={{ right: '-223px', borderRadius: '40px', height: '40px' }}
          onClick={() => {
            updateAsyncFn()
          }}
        >
          Update Template
        </ThemeButton>*/}
        <ThemeButton
          style={{ right: '-245px', borderRadius: '40px', height: '40px' }}
          onClick={() => {
            print()
          }}
        >
          Print Label
        </ThemeButton>
      </div>
    </div>
  )
}

export default PalletLabel
