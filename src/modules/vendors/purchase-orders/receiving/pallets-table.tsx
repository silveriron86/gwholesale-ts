import React from 'react'
import { connect } from 'redux-epics-decorator'
import EditableTable from '~/components/Table/editable-table'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { WrapperTitle } from '~/modules/customers/accounts/addresses/addresses.style'
import { PalletsTableWrapper, PalltesTableFooter, GreyTable, CompanyNameAC } from '../_style'
import { ThemeButton } from '~/modules/customers/customers.style'
import { Col, Icon, Modal, Row, DatePicker } from 'antd'
import { printWindow } from '~/common/utils'
import { fullButton } from '~/modules/customers/sales/_style'
import { CustomButton } from '~/components'
import { PrintLabels } from '~/modules/vendors/purchase-orders/receiving/print-labels'
import moment, { Moment } from 'moment'
import { Pallet, OrderItem } from '~/schema'

type PalletsTableProps = OrdersDispatchProps &
  OrdersStateProps & {
    orderItemId: number
    selected: OrderItem | null
  }

export class PalletsTable extends React.PureComponent<PalletsTableProps> {
  selectedRecord: any = null
  state = {
    dataSource: [],
    printLabelsReviewShow: false,
    companyName: [],
    companyNames: [],
    printLabelsRef: null,
  }

  componentDidMount() {
    this._getPallets(this.props.orderItemId)
  }

  componentWillReceiveProps(nextProps: PalletsTableProps) {
    if (
      nextProps.orderItemId > 0 &&
      // (this.props.selected === null || nextProps.selected.palletQty !== this.props.selected.palletQty)
      (this.props.selected === null || this.props.orderItemId != nextProps.orderItemId)
    ) {
      this._getPallets(nextProps.orderItemId)
    }

    // tslint:disable-next-line:prefer-const
    let pallets = [...nextProps.pallets]
    // tslint:disable-next-line:prefer-const
    let companyName = [] as string[]
    pallets.map((pallet: Pallet, index: number) => {
      companyName.push(pallet.companyName)
      return this._formatPallet(pallet)
    })

    this.setState({
      dataSource: pallets,
      companyName: companyName,
    })
  }

  _formatPallet = (pallet: any) => {
    if (pallet.bestByDate) {
      pallet.bestByDate = moment(pallet.bestByDate).format('M/DD/YYYY')
    }
    return pallet
  }

  _getPallets = (orderItemId: number) => {
    if (orderItemId >= 0) {
      this.props.getPallets(orderItemId)
    }
  }

  handleSave = (row: Pallet) => {
    const newData = [...this.state.dataSource] as Pallet[]
    const index = newData.findIndex((item: Pallet) => row.id === item.id)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
    this.props.updatePalletDetail({
      orderItemId: this.props.orderItemId,
      palletId: row.id,
      palletUnits: row.palletUnits,
      companyName: row.companyName,
      bestByDate: row.bestByDate,
    })
  }

  handleChangeCompany = (index: number, value: any) => {
    // tslint:disable-next-line:prefer-const
    let companyName = [...this.state.companyName] as string[]
    companyName[index] = value
    this.setState({
      companyName: companyName,
    })
  }

  handleSelectCompany = (index: number, _value: any, row: any) => {
    // tslint:disable-next-line:prefer-const
    let companyName = [...this.state.companyName] as string[]
    companyName[index] = row.props.children
    this.setState(
      {
        companyName: companyName,
      },
      () => {
        const dataSource = [...this.state.dataSource] as any[]
        // tslint:disable-next-line:prefer-const
        let selectedRow: any = { ...dataSource[index] }
        selectedRow.companyName = companyName[index]
        this.handleSave(selectedRow)
      },
    )
  }

  onSearch = (searchStr: string) => {
    const { vendors } = this.props
    // tslint:disable-next-line:prefer-const
    let companyNames: any[] = []
    if (vendors.length > 0 && searchStr) {
      vendors.filter((row) => {
        if (row.clientCompany && row.clientCompany.companyName.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) {
          companyNames.push({
            value: row.clientId,
            text: row.clientCompany.companyName,
          })
        }
      })
    }
    this.setState({
      companyNames: companyNames,
    })
  }

  changeBestByDate = (m: Moment, str: string) => {
    if (this.selectedRecord) {
      let row: Pallet = this.selectedRecord
      row.bestByDate = moment(str).format('MM/DD/YYYY')
      this.handleSave(row)
    }
  }

  render() {
    const { orderItemId } = this.props
    const { dataSource, companyName, companyNames } = this.state
    const dateFormat = 'YYYY/MM/DD'
    const { sellerSetting } = this.props
    const printCompanyName = sellerSetting && sellerSetting.userSetting ? sellerSetting.userSetting.companyName : ''
    const printDeliverListTrigger = () => {
      return (
        <CustomButton style={fullButton}>
          <Icon type="printer" theme="filled" />
          Print Labels
        </CustomButton>
      )
    }

    const printContent: any = () => {
      return this.state.printLabelsRef
    }

    const openPrintModal = () => {
      this.setState({
        printLabelsReviewShow: true,
      })
    }

    const closePrintModal = () => {
      this.setState({
        printLabelsReviewShow: false,
      })
    }

    const defaultColumns = [
      {
        title: 'PALLET #',
        dataIndex: 'displayNumber',
      },
      {
        title: 'PALLET UNITS',
        dataIndex: 'palletUnits',
      },
      {
        title: 'COMPANY NAME',
        dataIndex: 'companyName',
        width: 150,
      },
      {
        title: 'BEST BY DATE',
        dataIndex: 'bestByDate',
        width: 100,
      },
    ]
    const editableColumns = [
      {
        title: 'PALLET #',
        dataIndex: 'displayNumber',
        align: 'center',
        width: 80,
      },
      {
        title: 'PALLET UNITS',
        dataIndex: 'palletUnits',
        editable: true,
        edit_width: 55,
        width: 80,
      },
      {
        title: 'COMPANY NAME',
        dataIndex: 'companyName',
        edit_width: 55,
        editable: true,
        width: 80,
        render: (value: string, record: any, index: number) => {
          return (
            <CompanyNameAC
              value={companyName[index]}
              dataSource={companyNames}
              onSearch={this.onSearch}
              onChange={this.handleChangeCompany.bind(this, index)}
              onSelect={this.handleSelectCompany.bind(this, index)}
            />
          )
        },
      },
      {
        title: 'BEST BY DATE',
        dataIndex: 'bestByDate',
        // editable: true,
        edit_width: 65,
        width: 90,
        render: (value: number, record: any) => {
          // return value ? moment(value).format('M/DD/YY') : ''

          return (
            <DatePicker
              allowClear={false}
              placeholder=""
              defaultValue={value ? moment(value) : undefined}
              format={dateFormat}
              onChange={this.changeBestByDate}
              onOpenChange={(open) => {
                if (!open) this.selectedRecord = record
              }}
            />
          )
        },
      },
    ]

    return (
      <PalletsTableWrapper active={orderItemId >= 0}>
        <div style={{ backgroundColor: 'white' }}>
          <WrapperTitle disabled={orderItemId === -1}>Pallets</WrapperTitle>
        </div>
        {orderItemId < 0 ? (
          <GreyTable columns={defaultColumns} dataSource={[]} key="id" />
        ) : (
            <EditableTable columns={editableColumns} dataSource={dataSource} handleSave={this.handleSave} />
          )}
        <PalltesTableFooter>
          <Row gutter={24}>
            <Col style={{ textAlign: 'center' }} span={24}>
              <ThemeButton shape="round" disabled={orderItemId === -1} onClick={openPrintModal.bind(this)}>
                <Icon type="printer" />
                Preview/Print Labels
              </ThemeButton>
            </Col>
          </Row>
        </PalltesTableFooter>
        <Modal
          width={1080}
          footer={null}
          visible={this.state.printLabelsReviewShow}
          onCancel={closePrintModal.bind(this)}
        >
          <div id={'PrintLabelsModal'}>
            <PrintLabels
              dataSource={dataSource}
              ref={(el) => {
                this.setState({ printLabelsRef: el })
              }}
              company={printCompanyName}
            />
          </div>
          <CustomButton style={fullButton} onClick={() => printWindow('PrintLabelsModal', printContent.bind(this))}>
            <Icon type="printer" theme="filled" />
            Print Labels
          </CustomButton>
        </Modal>
      </PalletsTableWrapper>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default connect(OrdersModule)(mapStateToProps)(PalletsTable)
