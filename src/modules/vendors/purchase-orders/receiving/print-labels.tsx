import React from 'react'
// import { OrderDetail, OrderItem } from '~/schema'
// import { Row, Col } from 'antd'
import Barcode from 'react-barcode'
import QRCode from 'react-qr-code'

// import { textCenter, palletPrintCol } from '~/modules/customers/customers.style'
// import List from 'antd/lib/list'
// import moment from 'moment'
import { PalletLabelWrapper } from '../_style'
import { Flex, Flex1 } from '~/modules/customers/customers.style'
import { cloneDeep, forEach } from 'lodash'

interface PrintLabelsProps {
  dataSource: Array<any>
  sellerSetting: any
  type: string
  items?: any[]
  palletId?: number
  total?: number
}

export class PrintLabels extends React.PureComponent<PrintLabelsProps> {
  padString = (num: number) => {
    const str = num.toString()
    const pad = '000000'
    return pad.substring(0, pad.length - str.length) + str
  }

  render() {
    const { sellerSetting, type, total, items, palletId } = this.props
    const isUnitLabel = type === 'unit'
    const _fontSize = (val: number) => {
      const FONT_SCALE = 1
      return val * FONT_SCALE
    }
    const source = this.props.dataSource
    const companyLogo = sellerSetting && sellerSetting.imagePath ? `${process.env.AWS_PUBLIC}/${sellerSetting.imagePath}` : ''
    const customCompanyPrefix =
      sellerSetting &&
      sellerSetting.company &&
      typeof sellerSetting.company.customCompanyPrefix !== 'undefined' &&
      sellerSetting.company.customCompanyPrefix !== null
        ? sellerSetting.company.customCompanyPrefix
        : '000'
    const lastRowHeight = 40

    if (source.length == 0) return []
    let result: any[] = []

    let rows = []
    if (isUnitLabel) {
      const found = source.find((item) => item.id == palletId)
      if (found && items && items.length) {
        items?.forEach((unit) => {
          rows.push({
            ...found,
            unit,
          })
        })
      }
    } else {
      rows = cloneDeep(source)
    }

    rows.forEach((item, index) => {
      // current pallet # field[variable length pallet id up to 20 characters]
      const currentPalletId = item.displayNumber.substring(0, 20)
      const labelSerial = item.wholesaleOrderItem.wholesaleItem.labelSerial
        ? item.wholesaleOrderItem.wholesaleItem.labelSerial
        : '00000'

      let constantRatio = true
      let barCodeValue = `0111WSW${customCompanyPrefix}${labelSerial}010${currentPalletId}`
      const itemUnit = item.unit === '' || item.unit === null || typeof item.unit === 'undefined' ? 0 : item.unit
      const palletUnits =
        item.palletUnits === '' || item.palletUnits === null || typeof item.palletUnits === 'undefined'
          ? 0
          : item.palletUnits
      if (isUnitLabel) {
        let weight = parseFloat(itemUnit).toFixed(2)
        weight = weight.replace('.', '')
        weight = this.padString(parseFloat(weight))
        barCodeValue = `0111WSW${customCompanyPrefix}${labelSerial}03202${weight}10${currentPalletId}`
      } else {
        const uomList = item.wholesaleOrderItem.wholesaleItem.wholesaleProductUomList
        const matched = uomList.find((u) => u.name === item.wholesaleOrderItem.pricingUOM)
        if (matched) {
          constantRatio = matched.constantRatio
        }
      }

      let itemName = item.wholesaleOrderItem.wholesaleItem.variety
      result.push(
        <div key={`row-${index}`}>
          <PalletLabelWrapper className="print-block" style={{ paddingTop: 5 }}>
            {isUnitLabel ? (
              <div style={{ height: 6 }}></div>
            ) : (
              <div
                className="text-center"
                style={{ fontSize: _fontSize(17), lineHeight: `${_fontSize('17')}px`, marginBottom: 0 }}
              >
                PALLET:{item.displayNumber}
              </div>
            )}
            <Flex className="h-center" style={{ height: 70, marginBottom: _fontSize(0) }}>
              <Barcode
                value={barCodeValue}
                displayValue={true}
                width={isUnitLabel ? 1 : 2}
                height={isUnitLabel ? 40 : 50}
                fontSize={14}
                margin={0}
                marginTop={0}
              />
            </Flex>
            <div style={{ display: 'flex' }}>
              <div style={{ width: '520px' }}>
                <div style={{ height: '30px' }}>
                  <p
                    style={{
                      width: '320px',
                      flex: 1,
                      fontSize: _fontSize(17),
                      lineHeight: `${_fontSize(15)}px`,
                      overflow: 'hidden',
                      paddingRight: 10,
                      marginTop: 0,
                      height: '30px',
                      wordWrap: 'break-word',
                      marginBottom: '0px',
                    }}
                  >
                    {itemName}
                  </p>
                </div>
                <div
                  style={{
                    fontSize: _fontSize(56),
                    lineHeight: `${_fontSize(55)}px`,
                    fontWeight: 700,
                    overflow: 'hidden',
                    height: _fontSize(55),
                    marginTop: 0,
                    marginRight: 0,
                    marginBottom: 0,
                  }}
                >
                  {item.wholesaleOrderItem.wholesaleItem.sku
                    ? item.wholesaleOrderItem.wholesaleItem.sku.toString().substring(0, 8)
                    : ''}
                </div>
              </div>
              <div>
                <QRCode value={barCodeValue} size={80} />
              </div>
            </div>
            <div style={{ fontSize: _fontSize(14) }}>
              Lot: {item.wholesaleOrderItem && item.wholesaleOrderItem.lotId ? item.wholesaleOrderItem.lotId : ''}
            </div>
            <Flex className="v-center">
              <Flex1>
                {companyLogo !== '' && <img src={companyLogo} style={{ maxHeight: lastRowHeight, maxWidth: 170 }} />}
              </Flex1>
              {isUnitLabel ? (
                <div style={{ width: 150, textAlign: 'right' }}>
                  <div style={{ fontSize: _fontSize(26), lineHeight: `${_fontSize(20)}px`, fontWeight: 700 }}>
                    {parseFloat(itemUnit).toFixed(2)}
                  </div>
                  <div
                    style={{ fontSize: _fontSize(18), lineHeight: `${_fontSize(18)}px`, fontWeight: 300, marginTop: 3 }}
                  >
                    {item.wholesaleOrderItem.pricingUOM}
                  </div>
                </div>
              ) : (
                <div style={{ width: 150, textAlign: 'right' }}>
                  <div style={{ fontSize: _fontSize(26), lineHeight: `${_fontSize(20)}px`, fontWeight: 700 }}>
                    {`${palletUnits} ${
                      item.wholesaleOrderItem.overrideUOM
                        ? item.wholesaleOrderItem.overrideUOM
                        : item.wholesaleOrderItem.uom
                    }`}
                  </div>
                  <div style={{ fontSize: _fontSize(14), lineHeight: `${_fontSize(14)}px`, marginTop: 3 }}>
                    {constantRatio === true ? 'FIXED' : 'VARIABLE'}
                  </div>
                </div>
              )}
            </Flex>
          </PalletLabelWrapper>
          {index != rows.length - 1 && <hr />}
          <div style={{ pageBreakAfter: 'always' }} />
        </div>,
      )
    })
    return result
  }
}

export default PrintLabels
