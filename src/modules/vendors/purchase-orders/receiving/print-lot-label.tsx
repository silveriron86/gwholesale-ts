import React from 'react'
// import { OrderDetail, OrderItem } from '~/schema'
// import { Row, Col } from 'antd'
import Barcode from 'react-barcode'
import QRCode from 'react-qr-code'

// import { textCenter, palletPrintCol } from '~/modules/customers/customers.style'
// import List from 'antd/lib/list'
// import moment from 'moment'
import { PalletLabelWrapper } from '../_style'
import { Flex, Flex1 } from '~/modules/customers/customers.style'
import { cloneDeep } from 'lodash'

interface PrintLotLabelsProps {
  dataSource: Array<any>
  sellerSetting: any
  items?: any[]
  palletId?: number
  total?: number
}

export class PrintLotLabels extends React.PureComponent<PrintLotLabelsProps> {
  padString = (num: number) => {
    const str = num.toString()
    const pad = '000000'
    return pad.substring(0, pad.length - str.length) + str
  }

  render() {
    const { sellerSetting } = this.props
    const _fontSize = (val: number) => {
      const FONT_SCALE = 1
      return val * FONT_SCALE
    }
    const source = this.props.dataSource
    let companyLogo = sellerSetting && sellerSetting.imagePath ?`${process.env.AWS_PUBLIC}/${sellerSetting.imagePath}` : ''
    const useLogoInLotLabelPrint =
      sellerSetting && typeof sellerSetting.company.useLogoInLotLabelPrint !== 'undefined'
        ? sellerSetting.company.useLogoInLotLabelPrint
        : true
    if (companyLogo) {
      companyLogo = companyLogo.replace('http://staging', 'http://wholesaleware')
    }
    const customCompanyPrefix =
      sellerSetting &&
      sellerSetting.company &&
      typeof sellerSetting.company.customCompanyPrefix !== 'undefined' &&
      sellerSetting.company.customCompanyPrefix !== null
        ? sellerSetting.company.customCompanyPrefix
        : '000'
    const lastRowHeight = 40

    if (source.length == 0) return []
    let result: any[] = []

    let rows = cloneDeep(source)

    rows.forEach((item, index) => {
      // current pallet # field[variable length pallet id up to 20 characters]
      const currentPalletId = item.displayNumber.substring(0, 20)
      const labelSerial = item.wholesaleOrderItem.wholesaleItem.labelSerial
        ? item.wholesaleOrderItem.wholesaleItem.labelSerial
        : '00000'

      let constantRatio = true
      let barCodeValue = `0111WSW${customCompanyPrefix}${labelSerial}010${currentPalletId}`
      const uomList = item.wholesaleOrderItem.wholesaleItem.wholesaleProductUomList
      const matched = uomList.find((u) => u.name === item.wholesaleOrderItem.pricingUOM)
      if (matched) {
        constantRatio = matched.constantRatio
      }

      let itemName = item.wholesaleOrderItem.wholesaleItem.variety
      result.push(
        <div key={`row-${index}`}>
          <PalletLabelWrapper className="print-block" style={{ paddingTop: 5 }}>
            <Flex className="h-center" style={{ height: 70, marginBottom: _fontSize(0) }}>
              <Barcode
                value={barCodeValue}
                displayValue={true}
                width={2}
                height={50}
                fontSize={14}
                margin={0}
                marginTop={0}
              />
            </Flex>
            <Flex className="space-between" style={{ marginTop: -5 }}>
              <div>
                <div style={{ height: '30px' }}>
                  <p
                    style={{
                      width: '320px',
                      flex: 1,
                      fontSize: _fontSize(17),
                      lineHeight: `${_fontSize(15)}px`,
                      overflow: 'hidden',
                      paddingRight: 10,
                      marginTop: 0,
                      height: '30px',
                      wordWrap: 'break-word',
                      marginBottom: '0px',
                    }}
                  >
                    {itemName}
                  </p>
                </div>
                <div>
                  {item.wholesaleOrderItem &&
                    item.wholesaleOrderItem.wholesaleItem &&
                    item.wholesaleOrderItem.wholesaleItem.sku && (
                      <div
                        style={{
                          fontSize: _fontSize(18),
                          lineHeight: `${_fontSize(26)}px`,
                          overflow: 'hidden',
                          marginTop: 0,
                          marginBottom: 3,
                        }}
                      >
                        SKU: {item.wholesaleOrderItem.wholesaleItem.sku}
                      </div>
                    )}
                </div>
              </div>
              <div>
                <QRCode value={barCodeValue} size={80} />
              </div>
            </Flex>
            <div className="clearfix" />
            <Flex className="v-center" style={{ marginTop: '8px' }}>
              <Flex1>
                {useLogoInLotLabelPrint ? (
                  <>
                    {companyLogo !== '' && (
                      <img src={companyLogo} style={{ maxHeight: lastRowHeight, maxWidth: 170 }} />
                    )}
                  </>
                ) : (
                  <div style={{ fontSize: _fontSize(35), fontWeight: 700, lineHeight: `${_fontSize(35)}px` }}>
                    {sellerSetting ? sellerSetting.company.purchasePrefix : ''}
                  </div>
                )}
              </Flex1>
              <div style={{ width: 150, textAlign: 'right' }}>
                <div style={{ fontSize: _fontSize(26), lineHeight: `${_fontSize(20)}px`, fontWeight: 700 }}>
                  {item.wholesaleOrderItem.overrideUOM
                    ? item.wholesaleOrderItem.overrideUOM
                    : item.wholesaleOrderItem.inventoryUOM}
                </div>
                <div style={{ fontSize: _fontSize(14), lineHeight: `${_fontSize(14)}px`, marginTop: 3 }}>
                  {constantRatio === true ? 'FIXED' : 'VARIABLE'}
                </div>
              </div>
            </Flex>
            {item.wholesaleOrderItem && item.wholesaleOrderItem.lotId && (
              <div
                style={{
                  fontSize: _fontSize(48),
                  fontWeight: 700,
                  lineHeight: `${_fontSize(40)}px`,
                  overflow: 'hidden',
                  marginTop: 8,
                  marginBottom: 3,
                  textAlign: 'center',
                }}
              >
                {item.wholesaleOrderItem.lotId}
              </div>
            )}
          </PalletLabelWrapper>
          {index != rows.length - 1 && <hr />}
          <div style={{ pageBreakAfter: 'always' }} />
        </div>,
      )
    })
    return result
  }
}

export default PrintLotLabels
