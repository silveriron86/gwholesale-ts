import React from 'react'
import { withTheme } from 'emotion-theming'
import { OrderItem } from '~/schema'
import ReceivingTable from './receiving-table'
import PalletsTable from './pallets-table'
import { Col, Row, Switch } from 'antd'
import { WrapperTitle } from '~/modules/customers/accounts/addresses/addresses.style'
import { ReceivingTableWrapper, LockText, PurchaseSearchWrapper } from '../_style'
import { ThemeSwitch } from '~/modules/customers/customers.style'
import SearchInput from '~/components/Table/search-input'

interface ReceivingContainerProps {
  orderId: string
  orderItems: OrderItem[]
  orderItem: OrderItem
  updateReceivedModalLoading: boolean
  orderItemId: number
  currentOrder: any
  handleSave: Function
  handleShow: Function
  handleSaveOrderInfo: Function
  getPallets: Function
  pallets: any[]
  updatePalletDetail: Function
  vendors: any[]
  updateAllPalletDetail: Function
  getWeightSheets: Function
  weightSheet: any
  saveWeightSheet: Function
  totalPalletsInfo: any[]
  getTotalUnitsByAllPallets: Function
  getOrderItemByOrderItemId: Function
  updatePOIReceived: Function
  companyProductTypes: any
  getCompanyProductAllTypes: Function
  updateProduct: Function
  setCompanyProductType: Function
}

export class ReceivingContainer extends React.PureComponent<ReceivingContainerProps> {
  state = {
    index: -1,
    isLocked: this.props.currentOrder.isLocked,
    searchStr: '',
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.currentOrder.isLocked != this.props.currentOrder.isLocked) {
      this.setState({
        isLocked: nextProps.currentOrder.isLocked,
      })
    }
  }

  onShow = (index: number, itemId: number) => {
    this.setState({
      index: index,
    })
    this.props.handleShow(itemId)
  }

  onChangeSwitch = (checked: Boolean) => {
    this.setState({
      isLocked: checked,
    })
    this.props.handleSaveOrderInfo({
      wholesaleOrderId: this.props.orderId,
      isLocked: checked,
    })
  }

  onSearch = (text: string) => {
    this.setState({
      searchStr: text,
    })
  }

  render() {
    const { orderId, orderItemId, orderItems, handleSave, currentOrder } = this.props
    const { index, isLocked, searchStr } = this.state
    const filteredItems =
      orderItems.length > 0
        ? orderItems.filter((row) => {
          return (
            (row.variety && row.variety.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) ||
            (row.SKU && row.SKU.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0) ||
            (row.UOM && row.UOM.toLowerCase().indexOf(searchStr.toLowerCase()) >= 0)
          )
        })
        : []
    return (
      <div style={{ minWidth: 1300 }}>
        {/* <Row>
          <Col md={24}> */}
        <WrapperTitle>Receiving</WrapperTitle>
        <div style={{ textAlign: 'right', padding: '24px 0 12px' }}>
          <PurchaseSearchWrapper className="inline-block">
            <SearchInput
              handleSearch={this.onSearch}
              placeholder="Search"
              className="purchase-item-search header-last-tab purchase-cart-actions add-item-modal-input"
            />
          </PurchaseSearchWrapper>
        </div>
        <ReceivingTableWrapper>
          <ReceivingTable
            orderId={orderId}
            isLocked={isLocked}
            orderItems={filteredItems}
            handleSave={handleSave}
            handleShow={this.onShow}
            getPallets={this.props.getPallets}
            pallets={this.props.pallets}
            updatePalletDetail={this.props.updatePalletDetail}
            vendors={this.props.vendors}
            updateAllPalletDetail={this.props.updateAllPalletDetail}
            sellerSetting={this.props.sellerSetting}
            getWeightSheets={this.props.getWeightSheets}
            weightSheet={this.props.weightSheet}
            saveWeightSheet={this.props.saveWeightSheet}
            totalPalletsInfo={this.props.totalPalletsInfo}
            getTotalUnitsByAllPallets={this.props.getTotalUnitsByAllPallets}
            getOrderItemByOrderItemId={this.props.getOrderItemByOrderItemId}
            orderItem={this.props.orderItem}
            updateReceivedModalLoading={this.props.updateReceivedModalLoading}
            updatePOIReceived={this.props.updatePOIReceived}
            companyProductTypes={this.props.companyProductTypes}
            getCompanyProductAllTypes={this.props.getCompanyProductAllTypes}
            updateProduct={this.props.updateProduct}
            setCompanyProductType={this.props.setCompanyProductType}
            setOrderItemsReceived={this.props.setOrderItemsReceived}
            updatingReceived={this.props.updatingReceived}
          />
        </ReceivingTableWrapper>
        {/* </Col> */}
        {/* <Col md={7}>
            <PalletsTable
              selected={orderItems.length > 0 && index >= 0 ? orderItems[index] : null}
              orderItemId={orderItemId}
            />
          </Col> */}
        {/* </Row> */}
        {/* <Row style={{ marginTop: '40px' }}>
          <Col md={24}>
            <ThemeSwitch checked={isLocked} onChange={this.onChangeSwitch} />
            <LockText>Lock/Finalize Order</LockText>
          </Col>
        </Row> */}
      </div>
    )
  }
}

export default withTheme(ReceivingContainer)
