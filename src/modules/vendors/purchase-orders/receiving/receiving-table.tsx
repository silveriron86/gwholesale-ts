import React from 'react'
import { ClassNames } from '@emotion/core'
import { noteGreyIcon, noteIcon } from '~/modules/customers/sales/_style'
import { Flex } from '~/modules/inventory/components/inventory-header.style'
import {
  FlexCenter,
  ThemeIcon,
  ThemeModal,
  ThemeInput,
  ThemeButton,
  ThemeOutlineButton,
  ThemeSpin,
} from '~/modules/customers/customers.style'
import { Popover, Button, Tooltip, Icon } from 'antd'
import { OrderItem } from '~/schema'
import NotesModal from '~/modules/customers/sales/cart/modals/notes-modal'
import { Icon as IconSvg } from '~/components'
import EditableTable from '~/components/Table/editable-table'
import _ from 'lodash'
import {
  ratioQtyToBaseQty,
  baseQtyToRatioQty,
  ratioQtyToInventoryQty,
  inventoryQtyToRatioQty,
  formatItemDescription,
  ratioPriceToBasePrice,
  getDefaultUnit,
  getAllowedEndpoint,
  judgeConstantRatio
} from '~/common/utils'
import { DialogSubContainer, NoBorderButton, noPaddingFooter } from '~/modules/customers/nav-sales/styles'
import ReceivingPalletize from './modals/pallets'
import ReceivingWeightSheet from './modals/weight-sheet'
import UpdateReceivedModalBody from './modals/update-received'
import LotSpecificationModal from '../overview/lot-specification-modal'
import AddNewBrandModal from '~/modules/orders/components/add-new-brand-modal'
import TypesEditor from '~/modules/settings/tabs/Product/TypesEditor'
import { gray01 } from "~/common"
import styled from '@emotion/styled'

interface ReceivingTableProps {
  orderId: string
  orderItems: OrderItem[]
  orderItem: OrderItem
  handleSave: Function
  handleShow: Function
  isLocked: Boolean
  updateReceivedModalLoading: Boolean
  getPallets: Function
  pallets: any[]
  updatePalletDetail: Function
  vendors: any[]
  updateAllPalletDetail: Function
  getWeightSheets: Function
  weightSheet: any
  saveWeightSheet: Function
  orderStatus: string
  totalPalletsInfo: any[]
  getTotalUnitsByAllPallets: Function
  getOrderItemByOrderItemId: Function
  updatePOIReceived: Function
  companyProductTypes: any
  getCompanyProductAllTypes: Function
  updateProduct: Function
  setCompanyProductType: Function
  setOrderItemsReceived: Function
}

export class ReceivingTable extends React.PureComponent<ReceivingTableProps> {
  state: any
  lotSpecModalRef = React.createRef<any>()
  weightSheetRef = React.createRef<any>()
  constructor(props: ReceivingTableProps) {
    super(props)
    this.state = {
      dataSource: this._formatItems(_.cloneDeep(props.orderItems)),
      initNotes: _.map(props.orderItems, 'note'),
      modifyNotes: _.map(props.orderItems, 'note'),
      visiblePalletizeModal: false,
      palletizeItem: null,
      visibleWeightSheetModal: false,
      updateRecievedQtyModal: false,
      weightItem: null,
      previousDataSource: this._formatItems(_.cloneDeep(props.orderItems)),
      newReceivedQty: null,
      understandCheckBox: false,
      visibleLotSpecificationModal: false,
      selectedItem: null,
      openAddNewBrandModal: false,
      currentSuppliers: [],
      oldBrands: [],
      addingBrandItemId: null,
      brandingItemName: '',
      visibleCOOModal: false,
      confirmReceivedModal: false,
      updating: false,
      isEditOrderWeight: false
    }
  }

  componentWillReceiveProps(nextProps: ReceivingTableProps) {
    if (JSON.stringify(nextProps.orderItems) !== JSON.stringify(this.props.orderItems)) {
      this.setState({
        dataSource: this._formatItems(_.cloneDeep(nextProps.orderItems)),
      })
    }

    if (nextProps.updatingReceived !== this.props.updatingReceived && nextProps.updatingReceived === false) {
      this.setState({
        updating: false
      })
    }
  }

  _formatItems = (orderItems: any[]) => {
    if (orderItems.length === 0) {
      return []
    }

    const items = [...orderItems]
    items.map((item) => {
      item.temperature = item.temperature === 'null' || item.temperature === null ? '' : item.temperature
      item.receivedQty =
        item.receivedQty !== ''
          ? baseQtyToRatioQty(
            item.overrideUOM ? item.overrideUOM : item.UOM,
            item.receivedQty,
            item,
          )
          : 0
      item.qtyConfirmed = baseQtyToRatioQty(
        item.overrideUOM ? item.overrideUOM : item.UOM,
        item.qtyConfirmed,
        item,
      )
      item.quantity = baseQtyToRatioQty(
        item.overrideUOM ? item.overrideUOM : item.UOM,
        item.quantity,
        item,
      )
      item.orderWeight =
        item.orderWeight !== ''
          ? baseQtyToRatioQty(
            item.pricingUOM ? item.pricingUOM : item.UOM,
            item.orderWeight,
            item,
          )
          : 0
    })
    return items
  }

  onNotesVisbleChange = (record: any, index: number, visible: boolean) => {
    let { initNotes, modifyNotes } = this.state
    if (visible === false) {
      if (initNotes[index] != modifyNotes[index]) {
        initNotes[index] = modifyNotes[index]
        const uom = record.overrideUOM ? record.overrideUOM : record.inventoryUOM
        const pricingUOM = record.pricingUOM ? record.pricingUOM : record.inventoryUOM
        let cost = ratioPriceToBasePrice(pricingUOM, record.cost, record, 12)
        let quantity = baseQtyToRatioQty(
          record.inventoryUOM,
          ratioQtyToBaseQty(uom, record.quantity, record, 12),
          record,
          12,
        )
        let receivedQty = baseQtyToRatioQty(
          record.inventoryUOM,
          ratioQtyToBaseQty(uom, record.receivedQty, record, 12),
          record,
          12,
        )
        delete record.qtyConfirmed
        this.props.handleSave({
          ...record,
          cost: cost,
          quantity: quantity,
          receivedQty,
          note: modifyNotes[index],
        })
        this.setState({ initNotes })
      }
    }
  }
  setNewNote(value: string, index: number) {
    let { modifyNotes } = this.state
    modifyNotes[index] = value
    this.setState({ modifyNotes })
  }

  toggleConfirmReceivedModal = () => {
    this.setState({
      confirmReceivedModal: !this.state.confirmReceivedModal
    })
  }

  setToConfirmedQuantities = () => {
    this._setToQuantities('confirmed_qty')
  }

  setToOrderedQuantities = () => {
    this._setToQuantities('order_qty')
  }

  _setToQuantities = (qtyType) => {
    this.setState({
      updating: true
    }, () => {
      const { orderId } = this.props
      this.props.setOrderItemsReceived({
        orderId,
        qtyType
      })
      this.toggleConfirmReceivedModal();
    })
  }

  onSave = (row: OrderItem, id: any) => {
    const newData = [...this.state.dataSource]
    const index = newData.findIndex((item) => row.lotId === item.lotId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })
    this.setState({ dataSource: newData })
    const uom = row.overrideUOM ? row.overrideUOM : row.UOM
    const pricingUOM = row.pricingUOM ? row.pricingUOM : row.inventoryUOM
    // let status = row.status
    // if (id == 'receivedQty' && status == 'CONFIRMED' && row.receivedQty !== '') {
    //   status = 'RECEIVED'
    // }
    // if (id == 'receivedQty' && row.receivedQty == '') {
    //   row.receivedQty = 0
    // }
    let receivedQty = row.receivedQty
      ? baseQtyToRatioQty(row.inventoryUOM, ratioQtyToBaseQty(uom, row.receivedQty, row, 12), row, 12)
      : 0

    // let quantity = row.quantity
    let quantity = baseQtyToRatioQty(row.inventoryUOM, ratioQtyToBaseQty(uom, row.quantity, row, 12), row, 12)
    let orderWeight = row.orderWeight
    ? baseQtyToRatioQty(row.inventoryUOM, ratioQtyToBaseQty(pricingUOM, row.orderWeight, row, 12), row, 12)
    : 0

    //delete qtyConfirmed field according to `move to save method`
    delete row.qtyConfirmed
    this.props.handleSave({
      ...row,
      receivedQty,
      quantity,
      // status,
      orderWeight,
    })
  }

  toggleReceivingPalletize = (record: any) => {
    this.setState({
      visiblePalletizeModal: !this.state.visiblePalletizeModal,
      palletizeItem: record,
    })
  }

  toggleWeightSheetModal = (record: any) => {
    this.setState({
      visibleWeightSheetModal: !this.state.visibleWeightSheetModal,
      weightItem: record,
    }, () => {
      if(this.weightSheetRef.current) {
        this.weightSheetRef.current.onClose()
      }
    })
  }

  onCloseModal = () => {
    this.setState({
      visibleWeightSheetModal: false,
      visiblePalletizeModal: false,
      palletizeItem: null,
      weightItem: null,
    })
  }

  changeQty = (value: number, orderItemId: number, field: string) => {
    console.log(value, orderItemId, field)
    const { dataSource } = this.state
    let cloneData = _.cloneDeep(dataSource)
    let row: OrderItem | null
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItemId) {
        el[field] = value
        if (!value) {
          el[field] = 0
        }
        // if (field == 'receivedQty' && el.status == 'CONFIRMED' && el.receivedQty !== '') {
        //   el.status = 'RECEIVED'
        // }
        // if (field == 'receivedQty' && el.receivedQty == '') {
        //   el.receivedQty = 0
        // }

        el['isUpdated'] = true
        row = el
        return
      }
    })
    this.setState({
      dataSource: cloneData,
      previousDataSource: dataSource,
    })
  }

  blurChangeQty = (value: number | string, orderItemId: number, field: string, record: OrderItem) => {
    console.log(judgeConstantRatio(record))
    const { dataSource, previousDataSource } = this.state
    let cloneData = _.cloneDeep(dataSource)
    // const index = previousDataSource.findIndex((item: any) => orderItemId === item.wholesaleOrderItemId)
    const cloneIndex = cloneData.findIndex((item: any) => orderItemId === item.wholesaleOrderItemId)
    let row = cloneData[cloneIndex]
    if (!cloneData[cloneIndex].isUpdated) {
      return
    }
    if (field == 'receivedQty' && judgeConstantRatio(record) && cloneData[cloneIndex].hasTransaction > 0) {
      const uom = row.overrideUOM ? row.overrideUOM : row.UOM

      let toBaseReceivedQty = row.receivedQty
      ? baseQtyToRatioQty(row.inventoryUOM, ratioQtyToBaseQty(uom, row.receivedQty, row, 12), row, 12)
        : 0
      this.props.getOrderItemByOrderItemId(cloneData[cloneIndex].wholesaleOrderItemId)
      this.setState({
        updateRecievedQtyModal: true,
        newReceivedQty: value,
        isEditOrderWeight: false,
        toBaseReceivedQty
      })

      return
    } else if (field == 'orderWeight' && !judgeConstantRatio(record) && record.pricingUOM === record.inventoryUOM && cloneData[cloneIndex].hasTransaction > 0) {
      const uom = row.pricingUOM ? row.pricingUOM : row.UOM

      let newOrderWeight = row.orderWeight
      ? baseQtyToRatioQty(row.inventoryUOM, ratioQtyToBaseQty(uom, row.orderWeight, row, 12), row, 12)
        : 0
      this.props.getOrderItemByOrderItemId(cloneData[cloneIndex].wholesaleOrderItemId)

      return this.setState({
        updateRecievedQtyModal: true,
        isEditOrderWeight: true,
        newOrderWeight
      })
    } else {
      cloneData[cloneIndex].isUpdated = false
      this.setState({
        dataSource: cloneData,
      })
    }
    if (field == 'receivedQty' && value == 0) {
      cloneData[cloneIndex]['receivedQty'] = 0
      cloneData[cloneIndex]['status'] = 'RECEIVED'
    }
    if (field == 'receivedQty' && cloneData[cloneIndex].receivedQty !== 0) {
      cloneData[cloneIndex]['status'] = 'RECEIVED'
    }
    this.onSave(row, field)
  }

  updateReceived = () => {
    const { orderItem } = this.props
    const { isEditOrderWeight, toBaseReceivedQty, dataSource, newOrderWeight } = this.state
    let cloneData = _.cloneDeep(dataSource)
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItem.wholesaleOrderItemId) {
        el['isUpdated'] = false
        return
      }
    })
    this.setState({
      updateRecievedQtyModal: false,
      understandCheckBox: false,
      rstandCheckBox: false,
      dataSource: cloneData,
    })
    this.props.updatePOIReceived({
      wholesaleOrderItemId: orderItem.wholesaleOrderItemId,
      data: {
        type: isEditOrderWeight ? 2 : 1,
        value: isEditOrderWeight ? newOrderWeight : toBaseReceivedQty,
      },
    })
  }

  cancelUpdateModal = () => {
    const { orderItem } = this.props
    const { dataSource } = this.state
    let cloneData = _.cloneDeep(dataSource)
    cloneData.forEach((el: OrderItem) => {
      if (el.wholesaleOrderItemId == orderItem.wholesaleOrderItemId) {
        el.receivedQty = orderItem.receivedQty * orderItem.ratio
        el.orderWeight = orderItem.receivedQty * orderItem.ratio
        el['isUpdated'] = false
        return
      }
    })
    this.setState({
      updateRecievedQtyModal: false,
      newReceivedQty: null,
      dataSource: this._formatItems(_.cloneDeep(this.props.orderItems)),
      understandCheckBox: false,
    })
  }

  changeUnderstandCheckBoxStatus = (e: any) => {
    this.setState({
      understandCheckBox: !!e.target.checked,
    })
  }

  onShowLotSpecificationModal = (record: any) => {
    this.setState({
      selectedItem: this.state.visibleLotSpecificationModal ? null : record,
      visibleLotSpecificationModal: !this.state.visibleLotSpecificationModal,
    })
  }

  handleSaveLotInfo = () => {
    if (this.state.visibleLotSpecificationModal) {
      this.lotSpecModalRef.current.saveLotSpecInfo()
    }
    this.setState({ visibleLotSpecificationModal: false })
  }

  openAddNewBrandModal = (record: any) => {
    if (record == -1) {
      this.setState({
        openAddNewBrandModal: false,
        currentSuppliers: [],
        addingBrandItemId: null,
        oldBrands: [],
        brandingItemName: '',
      })
    } else if (record) {
      const itemId = record.itemId
      const suppliers = record.suppliers
      const arrSuppliers = suppliers
        ? suppliers.split(',').map((el) => {
          return el.trim()
        })
        : []
      this.setState({
        openAddNewBrandModal: true,
        currentSuppliers: arrSuppliers,
        oldBrands: arrSuppliers,
        addingBrandItemId: itemId,
        brandingItemName: record.variety,
      })
    }
  }

  openAddNewCOOModal = () => {
    this.setState(
      {
        visibleCOOModal: !this.state.visibleCOOModal,
      },
      () => {
        if (!this.state.visibleCOOModal) {
          this.props.getCompanyProductAllTypes()
        }
      },
    )
  }

  onChangeBrandList = (currentSuppliers: string[]) => {
    this.setState({ currentSuppliers })
  }

  updateItemSupplier = (data: string[]) => {
    const { dataSource, addingBrandItemId, selectedItem } = this.state
    dataSource.forEach((el: any) => {
      if (el.itemId == addingBrandItemId) {
        el.suppliers = data.join(', ')
      }
    })
    const index = dataSource.findIndex((s: any) => s.wholesaleOrderItemId === selectedItem.wholesaleOrderItemId)
    this.setState({
      selectedItem: JSON.parse(JSON.stringify({
        ...selectedItem,
        suppliers: dataSource[index].suppliers
      })),
      dataSource,
      openAddNewBrandModal: false,
    })
  }

  render() {
    const { orderId, isLocked, sellerSetting, orderStatus, orderItem } = this.props
    const {
      confirmReceivedModal,
      dataSource,
      updateRecievedQtyModal,
      newReceivedQty,
      understandCheckBox,
      selectedItem,
      visibleLotSpecificationModal } = this.state
    const newOnhandQty =
      newReceivedQty != null && orderItem != null
        ? parseFloat(newReceivedQty) - (orderItem.receivedQty - orderItem.onHandQty)
        : 0
    const newAvailableQty =
      newReceivedQty != null && orderItem != null
        ? parseFloat(newReceivedQty) - (orderItem.receivedQty - orderItem.lotAvailableQty)
        : 0
    const extraCOO = this.props.companyProductTypes ? this.props.companyProductTypes.extraCOO : []
    // move to save method:
    // const newDataSource = dataSource.map((row: any) => {
    //   delete row.qtyConfirmed
    //   return { ...row }
    // })
    const columns: Array<any> = [
      {
        title: '#',
        key: 'index',
        align: 'center',
        width: 50,
        render: (_text: string, record: OrderItem, index: number) => {
          return index + 1
        },
      },
      {
        title: 'LOT #',
        dataIndex: 'lotId',
        key: 'lotId',
        width: 110,
        // render: (_text: string, record: OrderItem, index: number) => `${orderId}${index+1}`
      },
      {
        title: 'DESCRIPTION',
        dataIndex: 'variety',
        width: 160,
        render: (variety: string, record: any, index: number) => {
          const accountType = localStorage.getItem("accountType") || ''
          return (
            <a href={`#/product/${record.itemId}/${getAllowedEndpoint('product', accountType, 'specifications')}`} className="product-name">
              {formatItemDescription(variety, record.SKU, sellerSetting)}
            </a>
          )
        },
      },
      {
        title: 'SKU',
        dataIndex: 'SKU',
        width: 150,
      },
      {
        title: (<span>UNITS ORDERED / <br/>CONFIRMED</span>),
        dataIndex: 'quantity',
        align: 'center',
        render: (quantity: any, record: any) => {
          return (
            <>
              {quantity}/{record.qtyConfirmed}
            </>
          )
        },
      },
      {
        title: (
          <Flex>
            <span>UNITS<br/>RECEIVED</span>
            <Icon type="copy" className="copy-order-qty" style={{ marginTop: 20, marginLeft: 4 }} onClick={this.toggleConfirmReceivedModal} />
          </Flex>
        ),
        dataIndex: 'receivedQty',
        align: 'center',
        // edit_width: 80,
        // width: 110,
        render: (value: number, record: OrderItem) => {
          if (record.status == 'CONFIRMED' && value === 0) {
            value = ''
          }
          if (isLocked || orderStatus == 'CANCEL') {
            return value
          } else {
            // if (record.hasTransaction > 0) {
            //   if (record.status != 'RECEIVED') {
            //     return (
            //       <ThemeInput
            //         value={value}
            //         style={{ marginRight: 4, width: 60, textAlign: 'right' }}
            //         onChange={(evt: any) => {
            //           this.changeQty(evt.target.value, record.wholesaleOrderItemId, 'receivedQty')
            //         }}
            //         onBlur={(evt: any) =>
            //           this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'receivedQty')
            //         }
            //       />
            //     )
            //   } else {
            //     return (
            //       <Tooltip
            //         placement="top"
            //         title={
            //           'This lot has been included in one or more sales orders. The received quantity cannot be modified.'
            //         }
            //       >
            //         <div>
            //           <ThemeInput
            //             value={value}
            //             style={{ marginRight: 4, width: 60, textAlign: 'right', pointerEvents: 'none' }}
            //             onChange={(evt: any) => {
            //               this.changeQty(evt.target.value, record.wholesaleOrderItemId, 'receivedQty')
            //             }}
            //             onBlur={(evt: any) =>
            //               this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'receivedQty')
            //             }
            //             disabled={true}
            //           />
            //         </div>
            //       </Tooltip>
            //     )
            //   }
            // } else {
            return (
              <ThemeInput
                value={value}
                style={{ marginRight: 15, width: 60, textAlign: 'right' }}
                onChange={(evt: any) => {
                  this.changeQty(evt.target.value.match(/^\d*(\.?\d{0,2})/g)[0], record.wholesaleOrderItemId, 'receivedQty')
                }}
                onBlur={(evt: any) => this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'receivedQty', record)}
              />
            )
            // }
          }
        },
      },
      {
        title: 'UOM',
        dataIndex: 'UOM',
        // width: 80,
        render: (value: string, record: OrderItem) => {
          return record.overrideUOM ? record.overrideUOM : record.UOM
        },
      },
      {
        title: 'RECEIVED NET WEIGHT',
        dataIndex: 'orderWeight',
        width: 80,
        render: (value: number, record: OrderItem) => {
          const companyWeightUom = sellerSetting ? sellerSetting.company ? sellerSetting.company.weightUOM : '' : ''
          const weightUOM = (judgeConstantRatio(record) && record.pricingUOM !== record.inventoryUOM) ? companyWeightUom: record.pricingUOM
          if (isLocked) {
          return <>{value}/{weightUOM}</>
          } else {
            return (
              <NetWeight
                value={value}
                className={(!value && !judgeConstantRatio(record) && record.pricingUOM === record.inventoryUOM && record.status === 'RECEIVED') ? 'warningInput' : ''}
                style={{ marginRight: 4, width: 120, textAlign: 'right' }}
                onChange={(evt: any) => {
                  this.changeQty(evt.target.value, record.wholesaleOrderItemId, 'orderWeight')
                }}
                onBlur={(evt: any) => this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'orderWeight', record)}
                addonAfter={weightUOM}
              />
            )
          }
        },
      },
      {
        title: 'NOTES',
        dataIndex: 'notes',
        key: 'note',
        // width: 150,
        render: (notes: string, record: OrderItem, index: number) => (
          <Popover
            placement="right"
            content={
              <NotesModal
                isLocked={isLocked}
                record={record}
                setNewNote={this.setNewNote.bind(this)}
                index={index}
                orderStatus={orderStatus}
              />
            }
            trigger="click"
            onVisibleChange={this.onNotesVisbleChange.bind(this, record, index)}
          >
            <FlexCenter>{<ThemeIcon type="file-text" style={record.note ? noteIcon : noteGreyIcon} />}</FlexCenter>
          </Popover>
        ),
      },
      {
        title: 'LOT SPECIFICATION',
        dataIndex: 'lot_specification',
        className: 'th-left',
        key: 'lot_specification',
        width: 250,
        render: (value: any, record: any) => {
          const lotSpec =
            `${_.get(record, 'modifiers', '')}${record.modifiers ? ',' : ''}
          ${_.get(record, 'extraOrigin', '')}${record.extraOrigin ? ',' : ''}
          ${record.grossWeight ? record.grossWeight : record.defaultGrossWeight ? record.defaultGrossWeight : 0}
          ${_.get(record, 'grossWeightUnit', getDefaultUnit('WEIGHT', sellerSetting))},
          ${record.grossVolume ? record.grossVolume : record.defaultGrossVolume ? record.defaultGrossVolume : 0}
          ${_.get(record, 'grossVolumeUnit', getDefaultUnit('VOLUME', sellerSetting))}`
          if (isLocked || orderStatus == 'CANCEL') {
            return lotSpec
          } else {
            return (
              <div className="product-name" style={{ textDecoration: 'none' }}>
                {lotSpec}
                <Icon style={{ marginLeft: 8 }} type="setting" onClick={(evt: any) => this.onShowLotSpecificationModal(record)} />
              </div>
            )
          }
        }
      },
      {
        title: 'TEMP',
        dataIndex: 'temperature',
        width: 70,
        render: (value: number, record: OrderItem) => {
          if (isLocked) {
            return value
          } else {
            return (
              <ThemeInput
                value={value}
                style={{ marginRight: 4, width: 60, textAlign: 'right' }}
                onChange={(evt: any) => {
                  this.changeQty(evt.target.value, record.wholesaleOrderItemId, 'temperature')
                }}
                onBlur={(evt: any) => this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'temperature', record)}
              />
            )
          }
        },
      },
      {
        title: '# OF PALLETS',
        dataIndex: 'palletQty',
        width: 70,
        render: (value: number, record: OrderItem) => {
          if (isLocked) {
            return value
          } else {
            return (
              <ThemeInput
                value={value}
                style={{ marginRight: 4, width: 60, textAlign: 'right' }}
                onChange={(evt: any) => {
                  this.changeQty(evt.target.value, record.wholesaleOrderItemId, 'palletQty')
                }}
                onBlur={(evt: any) => this.blurChangeQty(evt.target.value, record.wholesaleOrderItemId, 'palletQty', record)}
              />
            )
          }
        },
      },
      {
        title: 'LOT / PALLET LABELS',
        dataIndex: 'x',
        align: 'center',
        render: (x: any, record: any) => {
          return (
            <ThemeButton onClick={() => this.toggleReceivingPalletize(record)}>
              <Icon type="caret-right" />
            </ThemeButton>
          )
        },
      },
      {
        title: 'UNIT WEIGHT SHEET',
        dataIndex: 'y',
        align: 'center',
        render: (y: any, record: any) => {
          return (
            <Button style={{ padding: 0 }} onClick={() => this.toggleWeightSheetModal(record)}>
              <IconSvg type="unit_weight_sheet" viewBox="0 0 36 36" width={30} height={30} />
            </Button>
          )
        },
      },
      // {
      //   title: 'SHOW PALLETS',
      //   key: 'show',
      //   // width: 90,
      //   align: 'center',
      //   render: (_text: string, record: OrderItem, index: number) =>
      //     this.props.index === index ? (
      //       <SelectedArrow type="caret-left" />
      //     ) : (
      //       <ThemeButton shape="circle" onClick={this.props.handleShow.bind(this, index, record.wholesaleOrderItemId)}>
      //         <Icon type="caret-right" />
      //       </ThemeButton>
      //     ),
      // },
    ]

    if (sellerSetting && sellerSetting.company && sellerSetting.company.locationMethod == 1) {
      columns.push(
        {
          title: 'LOCATION',
          dataIndex: 'itemLocation',
          align: 'center'
        }
      )
    }

    return (
      <>
        <ThemeSpin spinning={this.state.updating}>
          <EditableTable columns={columns} dataSource={dataSource} handleSave={this.onSave} rowKey="lotId" />
        </ThemeSpin>
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              visible={this.state.visiblePalletizeModal}
              title={`Receiving - Palletize`}
              onCancel={() => this.toggleReceivingPalletize(null)}
              okText={'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              width={640}
              footer={null}
            >
              {this.state.visiblePalletizeModal && (
                <ReceivingPalletize
                  palletizeItem={this.state.palletizeItem}
                  visible={this.state.visiblePalletizeModal}
                  getPallets={this.props.getPallets}
                  pallets={this.props.pallets}
                  onSave={this.onSave}
                  updatePalletDetail={this.props.updatePalletDetail}
                  vendors={this.props.vendors}
                  updateAllPalletDetail={this.props.updateAllPalletDetail}
                  onClose={this.onCloseModal}
                  sellerSetting={sellerSetting}
                />
              )}
            </ThemeModal>
          )}
        </ClassNames>
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              visible={this.state.visibleWeightSheetModal}
              title={`Receiving Weight Sheet`}
              onCancel={() => this.toggleWeightSheetModal(null)}
              okText={'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0, color: gray01 }}
              className={`${cx(css(noPaddingFooter))}`}
              footer={null}
              width={680}
            >
              {this.state.visibleWeightSheetModal && (
                <ReceivingWeightSheet
                  orderId={orderId}
                  weightItem={this.state.weightItem}
                  visible={this.state.visibleWeightSheetModal}
                  onClose={this.onCloseModal}
                  getPallets={this.props.getPallets}
                  pallets={this.props.pallets}
                  getWeightSheets={this.props.getWeightSheets}
                  weightSheet={this.props.weightSheet}
                  saveWeightSheet={this.props.saveWeightSheet}
                  sellerSetting={sellerSetting}
                  totalPalletsInfo={this.props.totalPalletsInfo}
                  getTotalUnitsByAllPallets={this.props.getTotalUnitsByAllPallets}
                  ref={this.weightSheetRef}
                />
              )}
            </ThemeModal>
          )}
        </ClassNames>
        {updateRecievedQtyModal && (
          <ThemeModal
            // visible={this.state.updateRecievedQtyModal}
            visible={updateRecievedQtyModal}
            loading={this.props.updateReceivedModalLoading}
            title={this.state.isEditOrderWeight ? 'Update Net Weight' : `Update Units Received`}
            onCancel={this.cancelUpdateModal}
            width={1200}
            footer={
              <Flex>
                {(newOnhandQty < 0 || newAvailableQty < 0) && (
                  <Tooltip title={'WARNING: This lot will have negative value(s) if changes are submitted!'}>
                    <ThemeButton key="submit" onClick={this.updateReceived} disabled={!understandCheckBox}>
                      Save and Update inventory
                    </ThemeButton>
                  </Tooltip>
                )}

                {newOnhandQty >= 0 && newAvailableQty >= 0 && (
                  <ThemeButton key="submit" onClick={this.updateReceived} disabled={!understandCheckBox}>
                    {this.state.isEditOrderWeight ? 'Save and Update Net Weight' : `Save and Update inventory`}
                  </ThemeButton>
                )}
                <ThemeOutlineButton key="back" onClick={this.cancelUpdateModal}>
                  Cancel
                </ThemeOutlineButton>
              </Flex>
            }
          >
            <UpdateReceivedModalBody
              orderItem={orderItem ? [orderItem] : []}
              newReceivedQty={this.state.toBaseReceivedQty}
              newOrderWeight={this.state.newOrderWeight}
              understandCheckBox={understandCheckBox}
              changeUnderstandCheckBoxStatus={this.changeUnderstandCheckBoxStatus}
              isEditOrderWeight={this.state.isEditOrderWeight}
            ></UpdateReceivedModalBody>
          </ThemeModal>
        )}
        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              title={`Lot Specification`}
              visible={visibleLotSpecificationModal}
              onCancel={this.onShowLotSpecificationModal}
              okText={'Save'}
              cancelText={'Cancel'}
              bodyStyle={{ padding: 0 }}
              className={`${cx(css(noPaddingFooter))}`}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={this.handleSaveLotInfo}>
                    Save
                  </ThemeButton>
                </DialogSubContainer>
              }
              width={600}
            >
              <LotSpecificationModal
                ref={this.lotSpecModalRef}
                handleSave={this.props.handleSave}
                record={selectedItem}
                extraCOO={extraCOO}
                openAddNewCOOModal={this.openAddNewCOOModal}
                openAddNewBrandModal={this.openAddNewBrandModal}
              />
            </ThemeModal>
          )}
        </ClassNames>
        {this.state.addingBrandItemId && (
          <AddNewBrandModal
            visible={this.state.openAddNewBrandModal}
            itemId={this.state.addingBrandItemId}
            itemName={this.state.brandingItemName}
            oldBrands={this.state.oldBrands}
            companyProductTypes={this.props.companyProductTypes}
            onVisibleChange={this.openAddNewBrandModal}
            onChangeBrandList={this.onChangeBrandList}
            updateDatasource={this.updateItemSupplier}
            updateProduct={this.props.updateProduct}
            setCompanyProductTypes={this.props.setCompanyProductType}
          />
        )}
        <ThemeModal
          title={`Edit Value List "Origin"`}
          visible={this.state.visibleCOOModal}
          onCancel={this.openAddNewCOOModal}
          cancelText="Close"
          okButtonProps={{ style: { display: 'none' } }}
        >
          <TypesEditor isModal={true} field="extraCOO" title="Origin" buttonTitle="Add Origin" />
        </ThemeModal>

        <ClassNames>
          {({ css, cx }) => (
            <ThemeModal
              title='Confirm received quantities'
              visible={confirmReceivedModal}
              onCancel={this.toggleConfirmReceivedModal}
              className={`${cx(css(noPaddingFooter))}`}
              footer={
                <DialogSubContainer>
                  <ThemeButton type="primary" onClick={this.setToConfirmedQuantities}>
                    Set to confirmed quantities
                  </ThemeButton>
                  <ThemeOutlineButton onClick={this.setToOrderedQuantities}>Set to ordered quantities</ThemeOutlineButton>
                </DialogSubContainer>
              }
              width={610}
            >
              <p>You have not completed receiving for your items.</p>
              <div>We will automatically set the received quantities with either the ordered or confirmed quantities</div>
            </ThemeModal>
          )}
        </ClassNames>
      </>
    )
  }
}

const NetWeight = styled(ThemeInput)`
  &.warningInput {
    .ant-input {
      outline: none;
      background-color: #FEEAE9;
      border: 1px solid #EB5757;
      box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.25);
    }
  }
`

export default ReceivingTable
