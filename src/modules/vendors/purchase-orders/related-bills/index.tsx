import * as React from 'react'
import { CACHED_NS_LINKED, CACHED_QBO_LINKED, Theme } from '~/common'
import { textCenter, ThemeIcon, ThemeOutlineButton, ThemeSelect } from '~/modules/customers/customers.style'
import { OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { OneOffItemTableWrapper } from '~/modules/customers/accounts/addresses/addresses.style'
import { Icon, Popconfirm, Select, Tooltip, DatePicker } from 'antd'
import { OrderItem } from '~/schema'
import { formatNumber } from '~/common/utils'
import { Flex } from '~/modules/customers/nav-sales/styles'
import jQuery from 'jquery'
import EditableTable from '~/components/Table/editable-table'
import { connect } from 'react-redux'
import moment from 'moment'
import _ from 'lodash'
import { QBOImage, NSImage } from '~/modules/orders/components/orders-table.style'
import qboImage from '../../../../modules/cashSales/components/qbo.png'
import nsImage from '../../../../modules/cashSales/components/netsuite.svg'
import AllocateExtraCharges from '../pricing-analysis/AllcateExtraChargesModal'

type RelatedBillsProps = OrdersDispatchProps &
  OrdersStateProps & {
    theme: Theme
  }

class PurchaseOrderRelatedBills extends React.PureComponent<RelatedBillsProps> {
  state = {
    pageSize: 30,
    currentPage: 1,
    relatedBills: [],
    addItemClicked: false,
    allocationSettingModalVisible: false,
  }

  componentDidMount() {
    this.setOffItemsState(this.props)
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.relatedBills !== nextProps.relatedBills) {
      this.setOffItemsState(nextProps)
    }
  }

  componentDidUpdate(prevProps: any) {
    if (
      this.state.addItemClicked &&
      prevProps.relatedBills.length >= 0 &&
      prevProps.relatedBills.length === this.props.relatedBills.length - 1
    ) {
      const rows = jQuery('.one-off-table .ant-table-row')
      const allEls = jQuery('.one-off-table .ant-table-row .tab-able')
      const that = this
      if (rows.length > 0) {
        setTimeout(() => {
          const editableEls = jQuery(rows[rows.length - 1]).find('.tab-able')
          if (editableEls.length > 0) {
            setTimeout(() => {
              jQuery(editableEls[0]).trigger('click')
              const index = allEls.index(editableEls[0])
              that.setState({ addItemClicked: false })
              window.localStorage.setItem('CLICKED-INDEX', `${index}`)
            }, 50)
          }
        }, 200)
      }
    }
  }
  handleSave = (row: OrderItem, field?: string) => {
    this._saveOffItem(row, field)
  }

  _saveOffItem(row: OrderItem, field?: string) {
    const newData = [...this.state.relatedBills]
    const index = newData.findIndex((item: any) => row.wholesaleOrderItemId === item.wholesaleOrderItemId)
    const item = newData[index]
    newData.splice(index, 1, {
      ...item,
      ...row,
    })

    this.setState({ dataSource: newData })
    row.quantity = row.quantity ? row.quantity : 0
    row.picked = row.picked ? row.picked : 0
    row.uom = row.UOM
    row.billDate = row.billDate ? moment(row.billDate).format('MM/DD/YYYY') : row.billDate

    // console.log(row)
    this.onUpsetOffItem(row)
  }

  onUpsetOffItem = (data: any) => {
    // update
    if (parseInt(data['wholesaleOrderItemId']) > 0) {
      this.setState({
        relatedBills: this.state.relatedBills.map((v) => {
          if (v.wholesaleOrderItemId === data.wholesaleOrderItemId) return data
          return v
        }),
      })
      this.props.updateOffItem(data)
    } else {
      this.props.createOffItem(data)
    }
  }

  onDeleteRow = (id: number) => {
    const dataSource = [...this.state.relatedBills]
    this.setState({ dataSource: dataSource.filter((item) => item.wholesaleOrderItemId !== id) })
    this.props.removeOffItem({ wholesaleOrderItemId: id })
  }

  setOffItemsState = (props: any) => {
    let relatedBills = props.relatedBills.filter((obj: any) => {
      if (!obj.isInventory) {
        obj.uom = obj.UOM
        obj.variety = obj.itemName
        return true
      } else {
        return false
      }
    })
    this.setState({
      relatedBills,
    })
  }

  addRelatedBill = (clientId: number) => {
    console.log('extra charge type = ', clientId)
    // const itemList: OrderItem[] = lodash.cloneDeep(this.state.relatedBills)
    const newData = {
      wholesaleOrderItemId: 0,
      constantRatio: true,
      cost: 0,
      picked: 0,
      price: 0,
      quantity: 0,
      status: 'SHIPPED',
      itemName: name ?? '',
      uom: 'each',
      orderId: this.props.match.params.orderId,
      wholesaleClientId: clientId,
    }
    // itemList.push(newData)
    this.setState({
      addItemClicked: true,
      // relatedBills: itemList,
    })

    this.props.createOffItem(newData)
  }

  updateBillDate = (row: any, dateMoment: any, dateString: string) => {
    row.billDate = dateString
    this.handleSave(row, 'billDate')
  }

  changePage = (page: number) => {
    this.setState({ currentPage: page })
  }

  toggleAllocationSettings = () => {
    this.setState({
      allocationSettingModalVisible: !this.state.allocationSettingModalVisible
    })
  }

  render() {
    const { currentOrder, simplifyVendors, orderItems, oneOffItems } = this.props
    const { pageSize, currentPage, relatedBills, allocationSettingModalVisible } = this.state
    const editable = currentOrder && !currentOrder.isLocked && currentOrder.wholesaleOrderStatus != 'CANCEL'
    const isNS = localStorage.getItem(CACHED_NS_LINKED) != 'null'
    const isQBO = localStorage.getItem(CACHED_QBO_LINKED) != 'null'
    let extraChargeColumn: Array<any> = [
      {
        title: 'Vendor',
        dataIndex: 'vendorName',
        key: 'vendorName',
        align: 'center',
        width: '15%',
        sorter: (a: any, b: any) => a.vendorName.localeCompare(b.vendorName),
        render: (value: string) => {
          return typeof value !== 'undefined' && value.trim() !== '' ? (
            `${value}`
          ) : (
            <div style={{ height: 20, width: '100%' }} />
          )
        },
      },
      {
        title: 'Description',
        dataIndex: 'chargeDesc',
        key: 'chargeDesc',
        align: 'center',
        editable,
        sorter: (a: any, b: any) => a.chargeDesc.localeCompare(b.chargeDesc),
        render: (value: string) => {
          return typeof value !== 'undefined' && value != null && value.trim() !== '' ? (
            `${value}`
          ) : (
            <div style={{ height: 20, width: '100%' }} />
          )
        },
      },
      {
        title: 'Bill Date',
        dataIndex: 'billDate',
        key: 'billDate',
        align: 'center',
        type: 'DatePicker',
        width: 200,
        sorter: (a: any, b: any) => a.billDate.localeCompare(b.billDate),
        render: (value: string, record: any) => {
          let date = value ? moment(value) : moment()
          return (
            <DatePicker
              allowClear={false}
              onChange={this.updateBillDate.bind(this, record)}
              defaultValue={date}
              disabled={!editable}
              placeholder="MM/DD/YYYY"
              format="MM/DD/YYYY"
              suffixIcon={<ThemeIcon type="calendar" viewBox="0 0 24 24" width={24} height={24} />}
            />
          )
        },
      },
      {
        title: 'Bill Number',
        dataIndex: 'billNumber',
        key: 'billNumber',
        align: 'center',
        width: '25%',
        editable,
        sorter: (a: any, b: any) => a.billNumber.localeCompare(b.billNumber),
        render: (value: string) => {
          return typeof value !== 'undefined' && value != null && value.trim() !== '' ? (
            `${value}`
          ) : (
            <div style={{ height: 20, width: '100%' }} />
          )
        },
      },
    ]
    if (localStorage.getItem(CACHED_QBO_LINKED) !== 'null')
      extraChargeColumn.push(
        {
          title: 'QBO',
          dataIndex: 'relatedQboId',
          key: 'relatedQboId',
          width: 50,
          sorter: true,
          className: 'no-pad-td',
          render: (relatedQboId: string, record: any) => {
            console.log(currentOrder)
            if (typeof relatedQboId !== 'undefined' && relatedQboId !== null) {
              return <center><a href={`https://qbo.intuit.com/app/bill?txnId=${relatedQboId}`} target="_blank"><QBOImage src={qboImage} /></a></center>
            } else {
              return ''
            }
          },
        }
      )

    if (localStorage.getItem(CACHED_NS_LINKED) !== 'null')
      extraChargeColumn.push({
        title: 'NS',
        dataIndex: 'nsId',
        key: 'nsId',
        width: 50,
        sorter: true,
        render: (text: string, record: any) => {
          const nsRealmId = localStorage.getItem(CACHED_NS_LINKED)
          if (text) {
            const act = 'cashsale.nl'
            return (
              <a
                href={`https://${nsRealmId}.app.netsuite.com/app/accounting/transactions/${act}?id=${text}`}
                target="_blank"
              >
                <NSImage src={nsImage} />
              </a>
            )
          } else {
            return ''
          }
        },
      })

    extraChargeColumn.push({
      title: 'Quantity',
      dataIndex: 'quantity',
      align: 'center',
      key: 'unit',
      editable,
      width: 150,
      type: 'number',
      sorter: (a: any, b: any) => a.quantity - b.quantity,
    })
    extraChargeColumn.push({
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      align: 'center',
      editable,
      width: 190,
      sorter: (a: any, b: any) => a.price - b.price,
      render: (value: string, record: any) => {
        return (
          <div style={textCenter}>
            ${value}/{record.UOM}
          </div>
        )
      },
    })
    extraChargeColumn.push({
      title: 'Total Price',
      dataIndex: 'item_total',
      align: 'center',
      key: 'item_total',
      sorter: (a: any, b: any) => a.item_total - b.item_total,
      render: (data: number, record: any) => {
        const extended = Number(record.price ? record.price : 0) + Number(record.freight ? record.freight : 0)
        let total = 0
        total = extended * record.quantity
        const prefix = total >= 0 ? '' : '-'
        return (
          <div style={textCenter}>
            {prefix}${formatNumber(Math.abs(total), 2)}
          </div>
        )
      },
    })
    extraChargeColumn.push({
      title: '',
      key: 'delete',
      render: (_text: any, record: any, index: any) => {
        return editable ? (
          <div>
            <Popconfirm
              title="Permanently delete this one related record?"
              okText="Delete"
              onConfirm={this.onDeleteRow.bind(this, record.wholesaleOrderItemId)}
            >
              <ThemeIcon type="close" style={{ marginLeft: '10px' }} />
            </Popconfirm>
          </div>
        ) : null
      },
    })

    const vendors = simplifyVendors.sort((a: any, b: any) => a.clientCompanyName.localeCompare(b.clientCompanyName))
    const footer = editable ? (
      <div>
        <div style={{ width: '15%', borderRight: '1px solid #D8DBDB', textAlign: 'center', padding: 12 }}>
          <ThemeSelect
            className="select-vendor"
            placeholder="Select vendor"
            onChange={this.addRelatedBill}
            style={{ width: '100%' }}
            showSearch
            optionFilterProp="children"
            filterOption={(input: string, option: any) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {vendors.sort((a: any, b: any) => a.clientCompanyName.localeCompare(b.clientCompanyName)).map((vendor: any) => (
              <Select.Option key={`extra-charge-type${vendor.clientId}`} value={vendor.clientId}>
                {vendor.clientCompanyName}
              </Select.Option>
            ))}
          </ThemeSelect>
        </div>
      </div>
    ) : null

    return (
      <div style={{ padding: '0 40px 0 25px' }} className="one-off-table">
        <div className="tab-header" style={{paddingRight: 0}}>
          <Flex className="v-center space-between" style={{width: '100%'}}>
            <h3 style={{marginBottom: 0}}>
              Related Bills{' '}
              <Tooltip
                title={`Each Related Bills row will create/update a separate Vendor Bill in ${
                  isNS ? 'Netsuite' : 'QBO'
                } upon syncing.  Related Bills will not appear on ${_.get(
                  this.props,
                  'currentOrder.wholesaleClient.clientCompany.companyName',
                  '',
                )}’ PO or Bill.`}
              >
                <ThemeIcon type="info-circle" style={{ marginLeft: '8px', marginTop: '-3px' }} />
              </Tooltip>
            </h3>
            <ThemeOutlineButton onClick={this.toggleAllocationSettings}>Allocation settings</ThemeOutlineButton>
          </Flex>
        </div>
        <OneOffItemTableWrapper>
          <EditableTable
            className={'nested-head min-height-placeholder extra-charge'}
            columns={extraChargeColumn}
            dataSource={relatedBills}
            handleSave={this.handleSave}
            rowKey="wholesaleOrderItemId"
            pagination={{ pageSize: pageSize, current: currentPage, onChange: this.changePage, defaultCurrent: 1 }}
            footer={() => footer}
            allProps={this.props}
          />
        </OneOffItemTableWrapper>

        {allocationSettingModalVisible && (
          <AllocateExtraCharges
            onCancel={this.toggleAllocationSettings}
            orderId={currentOrder.wholesaleOrderId}
            currentOrder={currentOrder}
            currentOrderItems={orderItems}
            updatePOItemAllocate={(v: any) => {
              this.props.updatePOItemAllocate(v)
              this.props.getPurchaseOrderProfitability(currentOrder.wholesaleOrderId)
              this.props.getOrderItemsById(currentOrder.wholesaleOrderId)
              this.props.getAllocationPallets(currentOrder.wholesaleOrderId)
            }}
            oneOffItems={oneOffItems}
            relatedBills={relatedBills}
            getAlloationData={this.props.getAlloationData}
          />
        )}
      </div>
    )
  }
}

export default connect(({ orders }) => ({ sellerSetting: orders.sellerSetting }))(PurchaseOrderRelatedBills)
