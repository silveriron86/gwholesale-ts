import * as React from 'react'
import { RouteComponentProps } from 'react-router'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'

import PageLayout from '~/components/PageLayout'
import { PurcahseHeader } from '../_components/header'
import { CustomersInfoBody, layoutStyle } from '~/modules/customers/components/customers-detail/customers-detail.style'
import { CustomerDetailsTitle as DetailsTitle, CustomerDetailsWrapper as DetailsWrapper } from '~/modules/customers/customers.style'
import { Layout, Row, Col } from 'antd'
import OrdersTable from './orders-table'
import { GlobalState } from '~/store/reducer'
import { OrdersModule, OrdersStateProps, OrdersDispatchProps } from '~/modules/orders/orders.module'
import { connect } from 'redux-epics-decorator'

type RelatedOrdersProps = OrdersDispatchProps &
  OrdersStateProps &
  RouteComponentProps<{ orderId: string }> & {
    theme: Theme
  }

export class RelatedOrders extends React.PureComponent<RelatedOrdersProps> {
  state: any
  constructor(props: RelatedOrdersProps) {
    super(props)
  }

  editItem = () => { }

  componentDidMount() {
    const orderId = this.props.match.params.orderId
    this.props.getOrderDetail(orderId)
    if (this.props.currentOrder) {
      const vendorId = this.props.currentOrder.wholesaleClient.clientId
      this.props.getRelatedOrders(vendorId)
    }
  }

  componentWillReceiveProps = (nextProps: RelatedOrdersProps) => {
    if ((!this.props.currentOrder && nextProps.currentOrder && nextProps.currentOrder.wholesaleClient.clientId)) {
      const vendorId = nextProps.currentOrder.wholesaleClient.clientId
      console.log(console.log('related orders', vendorId))
      if (vendorId) {
        this.props.getRelatedOrders(vendorId)
      }
    }
  }

  render() {
    const { currentOrder, relatedOrders } = this.props
    if (!currentOrder) {
      return <PageLayout currentTopMenu={'menu-Purchasing-Purchase Orders'} />
    }

    return (
      <PageLayout currentTopMenu={'menu-Purchasing-Purchase Orders'}>
        <PurcahseHeader order={currentOrder} />
        <CustomersInfoBody>
          <DetailsWrapper>
            <div style={{ padding: '0 10px' }}>
              <Row>
                <Col md={24}>
                  <DetailsTitle>Related Orders</DetailsTitle>
                  <OrdersTable items={relatedOrders ? relatedOrders.slice(0, 10) : []} />
                </Col>
              </Row>
            </div>
          </DetailsWrapper>
        </CustomersInfoBody>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => state.orders
export default withTheme(connect(OrdersModule)(mapStateToProps)(RelatedOrders))
