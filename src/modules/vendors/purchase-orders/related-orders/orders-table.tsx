import React from 'react'
import { ml0, ThemeTable, ThemeIcon } from '~/modules/customers/customers.style'
import { PureWrapper as Wrapper, WrapperTitle } from '~/modules/customers/accounts/addresses/addresses.style'
import { ColumnProps } from 'antd/es/table'
import { Link } from 'react-router-dom'
import { TempOrder } from '~/schema'
import { formatDate } from '~/common/utils'

interface OrdersTableProps {
  items: TempOrder[]
}

export class OrdersTable extends React.PureComponent<OrdersTableProps> {
  render() {
    const { items } = this.props

    const columns: ColumnProps<any>[] = [
      {
        title: 'QUOTED / SCHEDULED DELIVERY DATE',
        dataIndex: 'deliveryDate',
        width: 200,
        render: (value: string) => formatDate(value),
      },
      {
        title: 'PO #',
        dataIndex: 'wholesaleOrderId',
        width: 80,
      },
      {
        title: 'VENDOR',
        dataIndex: 'fullName',
        width: 600,
        render: (fullName: string, order: any) => {
          return (
            <Link to={`/order/${order.wholesaleOrderId}/purchase-cart`} style={{ marginLeft: 5 }}>
              <ThemeIcon type="link" theme="filled" /> {fullName}
            </Link>
          )
        },
      },
      {
        title: 'UNITS RECEIVED',
        dataIndex: 'receivedQty',
        width: 150,
      },
      {
        title: 'UNITS CONFIRMED',
        dataIndex: 'qtyConfirmed',
        width: 150,
      },
    ]
    return (
      <Wrapper style={ml0}>
        <WrapperTitle>Related Orders</WrapperTitle>
        <ThemeTable
          pagination={{ hideOnSinglePage: true, pageSize: 12 }}
          columns={columns}
          dataSource={items}
          rowKey="wholesaleOrderId"
        />
      </Wrapper>
    )
  }
}

export default OrdersTable
