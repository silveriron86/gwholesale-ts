import React from 'react'
import { 
  Label
 } from './../requisition.style'
import { Row, Col, Radio } from 'antd'

interface RequisitionOptionProps {
    title: string
    value: boolean
}

export class RequisitionOption extends React.PureComponent<RequisitionOptionProps> {
  constructor(props: RequisitionOptionProps) {
    super(props)
    
  }
  componentDidMount() {

  }

  onChange = () => {

  }

  render() {
    const { title, value } = this.props
    return (
     <Row justify='space-between' type='flex'>
       <Col span={6}>
        <Label>{title}</Label>
      </Col>
      <Col span={14}>
      <Radio.Group onChange={this.onChange} value={value} style={{minWidth: 140}}>
        <Radio value={1}>YES</Radio>
        <Radio value={0}>NO</Radio>
      </Radio.Group>
      </Col>
     </Row>
    )
  }
}

export default RequisitionOption
