import React from 'react'
import { 
  Label, StockWrapper, PartialTitle
 } from './../requisition.style'
import { Row, Col, Radio, DatePicker, Table } from 'antd'
import { Icon } from '~/components'
import { DateEditorWrapper, DateTitle } from '~/modules/orders/components/orders-header.style'
import moment from 'moment'

interface PricingProps {

}

export class Pricing extends React.PureComponent<PricingProps> {
  column: ({ title: string; dataIndex: string; key: string; width: number } | { title: string; dataIndex: string; key: string; width?: undefined })[]
  
  constructor(props: PricingProps) {
    super(props)

    this.column = [
      {
        title: 'TIMESTAMP',
        dataIndex: 'timestamp',
        key: 'timestamp',
        width: 300
      },
      {
        title: 'GROUP',
        dataIndex: 'group',
        key: 'group',
      },
      {
        title: 'LEVEL',
        dataIndex: 'level',
        key: 'level',
      },
      {
        title: 'PRICE',
        dataIndex: 'price',
        key: 'price',
      },
      {
        title: 'COST',
        dataIndex: 'cost',
        key: 'cost',
      },
      {
        title: 'ACOUNT',
        dataIndex: 'account',
        key: 'account',
      }
    ]
    
  }
  componentDidMount() {

  }
  
  init = () => {
    let data = []
    for (let i = 0; i < 6; i++) {
      let a = {
        timestamp: '4/15/2020, 2:00:25PM',
        group: 'D',
        level: '--',
        price: '$5.75',
        cost: '$4.89',
        account: 'Round N Round Pot',
        
      } 
      data.push(a)
    }
    return data
  }

  render() {
    
    return (
      <div>
        <Table 
          columns={this.column}
          dataSource={this.init()}
          rowKey="pricing-id"
        />
      </div>
    )
  }
}

export default Pricing
