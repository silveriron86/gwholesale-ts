import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { OrderTable } from '~/modules/orders/components'
import { VendorsModule, VendorsStateProps, VendorsDispatchProps } from '../../vendors.module'
import { AuthUser } from '~/schema'
import { RouteProps } from 'react-router'

type IProps = VendorsDispatchProps &
  VendorsStateProps &
  RouteProps & {
    currentUser?: AuthUser
  }

export class PurchaseOrdersTab extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props)
  }
  render() {
    return (
      <div>
        <OrderTable
          history={this.props.history}
          orders={this.props.orders}
          tableMini={false}
          currentUser={this.props.currentUser}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export default connect(VendorsModule)(mapStateToProps)(PurchaseOrdersTab)
