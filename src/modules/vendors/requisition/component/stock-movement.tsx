import React from 'react'
import {
  Label, StockWrapper, PartialTitle
 } from './../requisition.style'
import { Row, Col, Radio, DatePicker, Table } from 'antd'
import { Icon } from '~/components'
import { DateEditorWrapper, DateTitle } from '~/modules/orders/components/orders-header.style'
import moment from 'moment'

interface StockMovementProps {

}

export class StockMovement extends React.PureComponent<StockMovementProps> {
  salesColumn: ({ title: string; dataIndex: string; key: string; width?: undefined } | { title: string; dataIndex: string; key: string; width: number })[]
  purchaseColumn: ({ title: string; dataIndex: string; key: string; width?: undefined } | { title: string; dataIndex: string; key: string; width: number })[]

  constructor(props: StockMovementProps) {
    super(props)

    this.salesColumn = [
      {
        title: '#',
        dataIndex: 'index',
        key: 'index',
      },
      {
        title: 'SHIP DATE',
        dataIndex: 'ship_date',
        key: 'ship_date',
      },
      {
        title: 'ACCOUNT',
        dataIndex: 'account',
        key: 'account',
        width: 200
      },
      {
        title: 'UNITS SHIPPED',
        dataIndex: 'units_shipped',
        key: 'units_shipped',
      },
      {
        title: 'UOM',
        dataIndex: 'uom',
        key: 'uom',
      }
    ]

    this.purchaseColumn = [
      {
        title: '#',
        dataIndex: 'index',
        key: 'index',
      },
      {
        title: 'SCHEDULED DELIVERY DATE',
        dataIndex: 'sch_delivery_date',
        key: 'sch_delivery_date',
      },
      {
        title: 'VENDOR',
        dataIndex: 'vendor',
        key: 'vendor',
        width: 200
      },
      {
        title: 'UNITS CONFIRMED',
        dataIndex: 'units_confirmed',
        key: 'units_confirmed',
      },
      {
        title: 'UNITS_RECEIVED',
        dataIndex: 'units_received',
        key: 'units_received',
      },
      {
        title: 'UOM',
        dataIndex: 'uom',
        key: 'uom',
      },
      {
        title: 'RECEIVED TIMESTAMP',
        dataIndex: 'received_timestamp',
        key: 'received_timestamp',
        width: 200
      }
    ]

  }
  componentDidMount() {

  }

  onChange = () => {

  }

  onDateChange = () => {

  }

  makeSalesData = () => {
    let data = []
    for (let i = 0; i < 6; i++) {
      let a = {
        index: i + 1,
        ship_date: '4/15/2020',
        account: 'Round N Round Pot',
        units_shipped: 5,
        uom: 'Case'

      }
      data.push(a)
    }
    return data
  }

  makePurcaseData = () => {
    let data = []
    for (let i = 0; i < 6; i++) {
      let a = {
        index: i + 1,
        sch_delivery_date: '4/15/2020',
        vendor: 'D&T Foods, Inc',
        units_confirmed: 90,
        units_received: 90,
        uom: 'Case',
        received_timestamp: '4/15/2020, 8:31PM',

      }
      data.push(a)
    }
    return data
  }

  renderDateEditor = () => {
    const calendarIcon = <Icon type="calendar" viewBox="0 0 24 24" width={24} height={24} />
    const dateFormat = 'MM/DD/YYYY'
    // const { dates } = this.props;
    const dates = [moment(1585699200000), moment(1588291200000)]
    const { RangePicker } = DatePicker

    return (
      <DateEditorWrapper className="orders-delivery-date">
        <RangePicker
          defaultValue={[dates[0], dates[1]]}
          format={dateFormat}
          suffixIcon={calendarIcon}
          onChange={this.onDateChange}
          allowClear={false}
        />
        <DateTitle>RECORD DATE</DateTitle>
      </DateEditorWrapper>
    )
  }

  render() {

    return (
      <StockWrapper>
        <Row>
          <Col span={6}>
            {this.renderDateEditor()}
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <PartialTitle>Sales</PartialTitle>
            <Table
              columns={this.salesColumn}
              dataSource={this.makeSalesData()}
              rowKey="sales-id"
            />
          </Col>
          <Col span={1}></Col>
          <Col span={15}>
          <PartialTitle>Purchases</PartialTitle>
            <Table
              columns={this.purchaseColumn}
              dataSource={this.makePurcaseData()}
              rowKey="sales-id"
            />
          </Col>
        </Row>
      </StockWrapper>
    )
  }
}

export default StockMovement
