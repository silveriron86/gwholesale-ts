import React from 'react'
import { 
  ListItemWrapper,
  ProductName, 
  InputLabel, 
  ProductBody, 
  SummaryValue,
  SummaryTitle,
  ColumnWrapper,
  Label,
  Value,
  RowWrapper
 } from './../requisition.style'
import { Row, Col } from 'antd'
import { ThemeButton } from '~/modules/customers/customers.style'
import RequisitionOption from '../component/option'

interface ListItemProps {
    productIndex: number
    data: any
    showDetail: Function 
}

export class ListItem extends React.PureComponent<ListItemProps> {
  constructor(props: ListItemProps) {
    super(props)
    
  }
  componentDidMount() {

  }

  onClickDetail = () => {
    const { productIndex } = this.props
    this.props.showDetail(productIndex)
  }

  render() {
    const {data} = this.props

    return (
      <ListItemWrapper>
        <Row type='flex' justify='space-between'>
          <Col>
            <InputLabel>PRODUCT NAME</InputLabel>
            <ProductName>{data.name}</ProductName>
          </Col>
          <Col>
            <ThemeButton shape="round" onClick={this.onClickDetail}>More Details</ThemeButton>
          </Col>
        </Row>
        <Row>
          <ProductBody>
            <Row>
              <Col md={6} xs={12}>
                <SummaryValue>{data.units_on_hand}</SummaryValue>
                <SummaryTitle>ON HAND</SummaryTitle>
              </Col>
              <Col md={6} xs={12}>
                <SummaryValue>{data.ordered}</SummaryValue>
                <SummaryTitle>ORDERED</SummaryTitle>
              </Col>
              <Col md={6} xs={12}>
                <SummaryValue>{data.qtyConfirmed}</SummaryValue>
                <SummaryTitle>CONFIRMED</SummaryTitle>
              </Col>
              <Col md={6} xs={12}>
                <SummaryValue>{data.weeks_til_empty}</SummaryValue>
                <SummaryTitle>WEEKS TIL EMPTY</SummaryTitle>
              </Col>
            </Row>
            <hr />
            <Row>
              <ColumnWrapper md={6} xs={12}>
                <Row>
                  <RequisitionOption title={'SELL'} value={data.sell}/>  
                </Row>
                <Row>
                  <RequisitionOption title={'PURCHASE'} value={data.purchase}/>  
                </Row>
                <Row>
                  <RequisitionOption title={'MANUFACTURE'} value={data.manufacture}/>  
                </Row>
                <Row>
                  <RequisitionOption title={'PUSH'} value={data.push}/>  
                </Row>
              </ColumnWrapper>
              <ColumnWrapper md={6} xs={12}>
                <RowWrapper>
                  <Label>PROCESSED</Label>
                  <Value>{data.processed}</Value>  
                </RowWrapper>
                <RowWrapper>
                  <Label>PRODUCT SKU</Label>
                  <Value>{data.sku}</Value>  
                </RowWrapper>
                <RowWrapper>
                  <Label>CATEGORY</Label>
                  <Value>{data.category}</Value>  
                </RowWrapper>
                <RowWrapper>
                  <Label>OUTBOUND UNITS/<br />SUB PER DAY</Label>
                  <Value>{1.2}</Value>  
                </RowWrapper>
              </ColumnWrapper>
              <ColumnWrapper md={6} xs={12}>
                <RowWrapper>
                  <Label>SUB UOM</Label>
                  <Value>{'Piece'}</Value>  
                </RowWrapper>
                <RowWrapper>
                  <Label>REORDER LEVEL</Label>
                  <Value>{5}</Value>  
                </RowWrapper>
                <RowWrapper>
                  <Label>ORDERED COST</Label>
                  <Value>{'$2.03'}</Value>  
                </RowWrapper>
                <RowWrapper>
                  <Label>RECEIVED COST</Label>
                  <Value>{'$2.03'}</Value>  
                </RowWrapper>
              </ColumnWrapper>
              <ColumnWrapper md={6} xs={12}>
                <RowWrapper>
                  <Label>PRICE LIST COST</Label>
                  <Value>{'$1.72'}</Value>  
                </RowWrapper>
                <RowWrapper>
                  <Label>REORDER DIFFERENCE</Label>
                  <Value>{5}</Value>  
                </RowWrapper>
              </ColumnWrapper>
            </Row>
          </ProductBody>
        </Row>
      </ListItemWrapper>
    )
  }
}

export default ListItem
