import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { VendorsModule, VendorsDispatchProps } from '../../vendors.module'
import { RequisitionListWrapper } from './../requisition.style'
import { ListItem } from './listitem'

interface RequisitionListProps {
  data: any
  showDetail: Function
}

export class RequisitionList extends React.PureComponent<RequisitionListProps> {
  constructor(props: any) {
    super(props)
    
  }
  componentDidMount() {

  }

  renderRequisitions = () => {
    const { data } = this.props 
    let arr = []
    for (let i = 0; i < data.length; i++) {
      arr.push(<ListItem key={i} productIndex={i} data={data[i]} showDetail={this.props.showDetail}/>)
    }
    return arr
  }

  render() {
    return (
      <RequisitionListWrapper>
        {this.renderRequisitions()}
      </RequisitionListWrapper>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export default connect(VendorsModule)(mapStateToProps)(RequisitionList)
