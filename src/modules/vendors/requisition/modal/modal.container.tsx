import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { ModalContainer, InputLabel, ModalProductName, CarouselWrapper, ProductBody, RowWrapper, Label, Value } from '../requisition.style'
import { Row, Col, Carousel, Checkbox } from 'antd'
import OverviewSection from '~/modules/customers/components/overview-section'
import ModalTabs from './tab'

interface RequisitionModalProps { 
  data: any
}

export class RequisitionModal extends React.PureComponent<RequisitionModalProps> {
  state: any
  requisitionDetail: any
  constructor(props: any) {
    super(props)

    this.state = {
      standardWight: true
    }
    
    //fake data
    this.requisitionDetail = {
      carousels: [
        'https://img.favpng.com/24/25/18/beefsteak-raw-meat-png-favpng-SXNB2dmjLFe0QjA6hCFZ5h36C.jpg',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSb7H0SLR8PlbfFPCLAs9kyWb9kZoJy_Xnlvo1ovCh5fGWKzNSw&usqp=CAU',
        'https://pluspng.com/img-png/meat-png-beef-meat-800.png',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQQXRxWNV8cTQtURTjmkjqTfa52fWmcXMkLlpdwpOKsqh5xyDAj&usqp=CAU'
      ]
    }
  }
  componentDidMount() {

  }

  onSwitchingView = (param: string)=> {
    this.setState({viewType: param})
  }

  onChangeStandardWeight = () => {
    this.setState({standardWight: !this.state.standardWight})
  }

  renderCarouselImages = (data: string[]) => {
    if(data.length == 0) return
    let result = []
    for (let i = 0; i < data.length; i++) {
      const el = data[i];
      result.push(
      <div key={i}>
        <img height={'250px'} src={el} />
      </div>)
    }
    return result
  }

  render() {
    const { data } = this.props
    return (
      <ModalContainer>
        <Row type='flex' justify='space-between'>
          <Col span={8}>
            <InputLabel>PRODUCT NAME</InputLabel>
            <ModalProductName>{data.name}</ModalProductName>
          </Col>
          <Col span={8}>
            <Row>
              <Col span={12}>
                <OverviewSection
                    label="ON HAND"
                    value={data.units_on_hand}
                  />
              </Col>
              <Col span={12}>
                <OverviewSection
                    label="ORDERED"
                    value={data.ordered}
                  />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col span={14}>
            <CarouselWrapper>
              <Carousel 
                effect='fade'
                dots={true}
                dotPosition='bottom'
                autoplay
                autoplaySpeed={3000}>
                {this.renderCarouselImages(this.requisitionDetail.carousels)}
              </Carousel>
            </CarouselWrapper>
          </Col>
          <Col span={10}>
            <ProductBody style={{marginTop: 15, marginBottom: 40}}>
              <h3 style={{fontWeight: 'bold'}}>Specifications</h3>
              <RowWrapper>
                <Label>Category</Label>
                <Value>{data.category}</Value>  
              </RowWrapper>
              <RowWrapper>
                <Label>Family</Label>
                <Value>--</Value>  
              </RowWrapper>
              <RowWrapper>
                <Label>Brand</Label>
                <Value>--</Value>  
              </RowWrapper>
              <RowWrapper>
                <Label>Temperature</Label>
                <Value>--</Value>  
              </RowWrapper>
              <RowWrapper>
                <Label>Standard Weight</Label>
                <Value><Checkbox checked={this.state.standardWight} onChange={this.onChangeStandardWeight}/></Value>  
              </RowWrapper>
              <RowWrapper>
                <Label>Pieces / Unit</Label>
                <Value>--</Value>  
              </RowWrapper>
              <RowWrapper>
                <Label>Weight / Unit</Label>
                <Value>33.07</Value>  
              </RowWrapper>
              <RowWrapper>
                <Label>Unit(s) of Measure</Label>
                <Value>Case</Value>
              </RowWrapper>
            </ProductBody>
          </Col>
        </Row>
        <Row>
          <ModalTabs />
        </Row>
      </ModalContainer>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export default RequisitionModal
