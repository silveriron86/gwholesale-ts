import * as React from 'react'
import { connect } from 'redux-epics-decorator'
import { withTheme } from 'emotion-theming'
import { Theme } from '~/common'
import { Tabs } from 'antd'
import { SettingsTabs } from '~/modules/settings/settings.style'
import StockMovement from '../component/stock-movement'
import { PurcaseOrdersTab } from '../component/purchase-orders'
import Pricing from '../component/pricing'

const { TabPane } = Tabs

class ModalTabs extends React.PureComponent {
  state: any
  constructor(props: any) {
    super(props)
    this.state = {
      tabKey: '1',
    }
  }

  componentDidMount() {
    
  }  

  onChangeTab = (key: string) => {
    this.setState({
      tabKey: key,
    })
  }

  render() {
    return (
      <SettingsTabs onChange={this.onChangeTab} type="card">
        <TabPane tab="Stock Movement" key="1">
          <StockMovement />
        </TabPane>
        <TabPane tab="Purchase Orders" key="2">
         <PurcaseOrdersTab />
        </TabPane>
        <TabPane tab="Pricing" key="3">
          <Pricing />
        </TabPane>
      </SettingsTabs>
    )
  }
}

export default ModalTabs