import React from 'react'
import { connect } from 'redux-epics-decorator'
import { GlobalState } from '~/store/reducer'
import { VendorsModule, VendorsStateProps, VendorsDispatchProps } from '../vendors.module'
import PageLayout from '~/components/PageLayout'
import { RequisitionWrapper, RequisitionContent } from './requisition.style'
import RequisitionTable from './tableview/tableview.container'
import { RequisitionList } from './listview/listivew.container'
import { Row, Col, Modal } from 'antd'
import SearchInput from '~/components/Table/search-input'
import RequisitionModal from './modal/modal.container'
import { ViewSwitcherEl } from '~/components/elements/view-selector'

export class RequisitionContainer extends React.PureComponent {
  state: any
  constructor(props: any) {
    super(props)

    this.state = {
      viewType: 'table',
      showModal: false,
      selectedProductIndex: null
    }
  }
  componentDidMount() {

  }

  onSwitchingView = (param: string) => {
    this.setState({ viewType: param })
  }

  onSearch = () => {

  }

  onShowDetail = (index: number) => {
    console.log(index)
    this.setState({
      showModal: true,
      selectedProductIndex: index
    })
  }

  onCloseModal = () => {
    this.setState({
      showModal: false,
      selectedProductIndex: null
    })
  }

  getRequisitionData = () => {
    let data = []
    for (let i = 0; i < 10; i++) {
      let a = {
        id: 'data_' + i,
        name: 'Beef C/S Flank Fresh, Circle-T',
        sell: true,
        purchase: true,
        manufacture: false,
        push: true,
        processed: 'N',
        sku: 1025,
        category: 'Beef Cow',
        units_on_hand: '10 Cases',
        qtyConfirmed: 0,
        weeks_til_empty: 10,
        ordered: 6
      }

      data.push(a)
    }
    return data
  }

  render() {
    const { viewType, showModal } = this.state
    // fake data
    const data = this.getRequisitionData()

    const { selectedProductIndex } = this.state

    return (
      <PageLayout noSubMenu={true} currentTopMenu={'menu-Purchasing-Requisition Catalog'}>
        <RequisitionWrapper>
          <Row type='flex' justify='space-between'>
            <Col span={8}>
              <ViewSwitcherEl viewType={viewType} onClick={this.onSwitchingView} />
            </Col>
            <Col span={4}>
              <SearchInput handleSearch={this.onSearch} placeholder="Search" />
            </Col>
          </Row>
          <RequisitionContent>
            {viewType == 'table' ?
              <RequisitionTable showDetail={this.onShowDetail} data={data} /> :
              <RequisitionList data={data} showDetail={this.onShowDetail} />}
          </RequisitionContent>
        </RequisitionWrapper>
        <Modal width='95%' footer={null} visible={showModal} onCancel={this.onCloseModal}>
          {selectedProductIndex !== null && selectedProductIndex < data.length && <RequisitionModal data={data[selectedProductIndex]} />}
        </Modal>
      </PageLayout>
    )
  }
}

const mapStateToProps = (state: GlobalState) => {
  return { ...state.orders, currentUser: state.currentUser }
}

export default connect(VendorsModule)(mapStateToProps)(RequisitionContainer)
