import styled from '@emotion/styled'
import { lightGrey, topLightGrey, darkGrey, mediumGrey, darkGreen } from '~/common'
import { Col, Row } from 'antd'

export const RequisitionWrapper = styled('div')({
  padding: '20px 20px 20px 60px'
})

export const RequisitionContent = styled('div')({
  marginTop: 40,
  '& .ant-table .ant-table-thead': {
    boxShadow: '0px 10px 16px -7px #DDD !important'
  }
})

export const RequisitionListWrapper = styled('div')({
  marginTop: 40
})

export const ListItemWrapper = styled('div')({
  marginTop: 10
})

export const InputLabel = styled('div')({
  color: mediumGrey,
  fontSize: 12,
  lineHeight: '12px',
  fontWeight: 'bold',
  letterSpacing: '0.05em',
  textAlign: 'left',
  padding: '10px 0',
})

export const ProductName = styled('div')({
  fontSize: '18px',
  letterSpacing: '0.05em',
  color: darkGrey,
  marginBottom: '10px',
  fontWeight: 'bold'
})

export const ProductBody = styled('div')((props)=>({
  backgroundColor: props.theme.lighter,
  padding: 20,
  borderRadius: 6
}))

export const SummaryValue = styled('div')((props)=>({
  color: props.theme.light,
  fontSize: '22px',
  marginBottom: 8,
  textAlign: 'left'
}))

export const SummaryTitle = styled('div')((props)=>({
  color: props.theme.darkGrey,
  fontSize: '14px',
  textAlign: 'left',
  marginBottom: 20
}))

export const Label = styled('div')({
  textAlign: 'left',
  padding: '2px 0',
  width: '70%',
  float: 'left'
})

export const Value = styled('div')({
  textAlign: 'left',
  padding: '4px 0',
  fontWeight: 'bold',
  color: 'black'
})

export const RowWrapper = styled('div')({
  // display: 'flex',
  // justifyContent: 'space-between'
})

export const ColumnWrapper = styled(Col)({
  padding: '10px 20px',
})

export const ModalContainer = styled('div')({
  padding: 20
})

export const ModalProductName = styled('div')((props) =>({
  fontSize: '18px',
  letterSpacing: '0.05em',
  color: props.theme.dark,
  marginBottom: '10px',
  fontWeight: 'bold'
}))

export const CarouselWrapper = styled('div')((props) =>({
  position: 'relative',
  '& .slick-dots': {
    bottom: '-15px !important',
    width: '50%',  //should be calculated when image is ready  
    '& li': {
      width: 14,
      height: 14,
      borderRadius: 7,
      background: topLightGrey,
      margin: '0 8px'
    },
    '& li.slick-active': {
      background: props.theme.main,

      '& button': {
        background: 'transparent'
      }
    }
  }
}))

export const StockWrapper = styled('div')((props)=>({
  '& .ant-table thead': {
    maxHeight: '68px !important',
    '& th': {
      background: props.theme.lighter
    }
  }
}))

export const PartialTitle = styled('div')((props)=>({
  background: props.theme.light,
  textAlign: 'center',
  fontWeight: 'bold',
  color: props.theme.dark,
  marginTop: 20,
  padding: 10,
  fontSize: 16
}))
