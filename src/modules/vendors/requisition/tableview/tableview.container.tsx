import React from 'react'
import { OrderItem } from '~/schema'
import { Table, Select, Popover } from 'antd'
import { ThemeTable } from '~/modules/customers/customers.style'
import { formatNumber } from '~/common/utils'

const { Option } = Select

interface RequisitionTableProps {
  data: any
  showDetail: Function
}

class RequisitionTable extends React.PureComponent<RequisitionTableProps> {
  state: any
  mutiSortTable = React.createRef<any>()
  columns: ({ title: string; key: string; dataIndex?: undefined; render?: undefined } | { title: string; dataIndex: string; key: string; render: (sell: boolean, record: any, index: any) => JSX.Element } | { ... })[]
  constructor(props: RequisitionTableProps) {
    super(props)
    this.state = {

    }

    this.columns = [
        {
          title: 'PRODUCT NAME',
          dataIndex: 'name',
          key: 'name',
          width: 400,
          sorter: (a: any, b: any) => {
            const sortIndex = this.mutiSortTable.current!.state.sortIndex.find((meta: any) => meta.dataIndex === 'name')
            if (sortIndex && sortIndex.sortOrder === 'descend') return this.onSortString('name', b, a)
            return this.onSortString('email', a, b)
          },
        },
        {
          title: 'SELL',
          dataIndex: 'sell',
          key: 'sell',
          width: 100,
          render: (sell: boolean, record: any, index: any) => {
            return (
              <Select value={sell ? 1 : 0} style={{ width: '100%' }}>
                <Option value={0}>NO</Option>
                <Option value={1}>YES</Option>
              </Select>)
          },
        },
        {
          title: 'PURCHASE',
          dataIndex: 'purchase',
          key: 'purchase',
          width: 100,
          render: (purchase: boolean, record: any, index: any) => {
            return (
              <Select value={purchase ? 1 : 0} style={{ width: '100%' }}>
                <Option value={0}>NO</Option>
                <Option value={1}>YES</Option>
              </Select>)
          },
        },
        {
          title: 'MANUFACTURE',
          dataIndex: 'manufacture',
          key: 'manufacture',
          width: 100,
          render: (manufacture: boolean, record: any, index: any) => {
            return (
              <Select value={manufacture ? 1 : 0} style={{ width: '100%' }}>
                <Option value={0}>NO</Option>
                <Option value={1}>YES</Option>
              </Select>)
          },
        },
        {
          title: 'MANUFACTURE',
          dataIndex: 'push',
          key: 'push',
          width: 100,
          render: (push: boolean, record: any, index: any) => {
            return (
              <Select value={push ? 1 : 0} style={{ width: '100%' }}>
                <Option value={0}>NO</Option>
                <Option value={1}>YES</Option>
              </Select>)
          },
        },
        {
          title: 'PROCESSED',
          dataIndex: 'processed',
          key: 'processed',
        },
        {
          title: 'PRODUCT SKU',
          dataIndex: 'sku',
          key: 'sku',
        },
        {
          title: 'CATEGORY',
          dataIndex: 'category',
          key: 'category',
        },
        {
          title: 'ON HAND',
          dataIndex: 'units_on_hand',
          key: 'units_on_hand',
        },
        {
          title: 'CONFIRMED',
          dataIndex: 'qtyConfirmed',
          key: 'qtyConfirmed',
        },
        {
          title: <Popover content="Number of weeks until inventory is estimated to hit 0 at current weekly demand rate.">
                WEEKS TIL EMPTY
              </Popover >,
          dataIndex: 'weeks_til_empty',
          key: 'weeks_til_empty',
          sorter: (a: any, b: any) => {
            const sortIndex = this.mutiSortTable.current!.state.sortIndex.find(
              (meta: any) => meta.dataIndex === 'weeks_til_empty',
            )
            if (sortIndex && sortIndex.sortOrder === 'descend') return b.weeks_til_empty - a.weeks_til_empty
            return a.weeks_til_empty - b.weeks_til_empty
          },
          render:(value: number) => {
            return formatNumber(value / 7, 2)
          }
        },
        {
          title: 'ORDERED',
          dataIndex: 'ordered',
          key: 'ordered',
        },
      ]
  }

  onSortString = (key: string, a: any, b: any) => {
    const stringA = a[key] ? a[key] : ''
    const stringB = b[key] ? b[key] : ''
    return stringA.localeCompare(stringB)
  }

  onRow = (record: any, index: number) => {
    const that = this
    return {
      onClick: (_event: any) => {
        _event.preventDefault()
        that.props.showDetail(index)
      }
    }
  }

  render() {
    const { data } = this.props
    return (
      <>
        <ThemeTable
          ref={this.mutiSortTable}
          columns={this.columns}
          dataSource={data}
          onRow={this.onRow}
          rowKey="id"
        />
      </>
    )
  }
}

export default RequisitionTable
