export interface ReceivingModel {
  id: number
  description: string
  sku: string
  units_ordered: number
  uom: string
  notes: string
  units_received: number
  net_weight: number
  temp: number
  pallets: number
}

export interface PalletModel {
  id: number
  units: number
  company_name: string
  date: string
}

export interface OrderModel {
  id: number
  delivery_date: string
  po: number
  vendor: string
  units_received?: number
  units_confirmed?: number
}

export interface DocumentModel {
  id: number
  date: string
  account: number
  typ: string
  note?: string
}
