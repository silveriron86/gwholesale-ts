import { Module, EffectModule, ModuleDispatchProps, Effect, StateObservable, DefineAction, Reducer } from 'redux-epics-decorator'
import { Observable, of, from } from 'rxjs'
import { switchMap, map, takeUntil, endWith, catchError, mergeMap, startWith } from 'rxjs/operators'
import { Action } from 'redux-actions'
import { push, goBack } from 'connected-react-router'
import { format } from 'date-fns'

import { VendorService } from './vendors.service'
import { GlobalState } from '~/store/reducer'
import {
  Customer,
  TempCustomer,
  TempOrder,
  CustomerStatus,
  CustomerType,
  MainAddress,
  Document,
  PriceSheet,
  MainContact,
  Address,
  Order,
  WholesaleRoute,
  BriefClient,
  SaleItem,
  PriceSheetItem,
  PriceSheetItemNew,
} from '~/schema'
// import { Customer, CustomerStatus, CustomerType, Address } from '~/schema'
import { MessageType } from '~/components'
import { checkError, responseHandler } from '~/common/utils'
import { DeliveryService } from '../delivery/delivery.service'
import { OrderService } from '../orders/order.service'
import { CustomerService } from '../customers/customers.service'
import { SettingService } from '../setting/setting.service'
import { PriceSheetService } from '../pricesheet/pricesheet.service'

export interface CustomerPriceSheet {
  id: number
  clientId: number
  updatedAt: string
  name: string
  assigned: number
  items: number
}

export interface VendorsStateProps {
  items: TempCustomer[] | null // null means loading
  vendor: TempCustomer
  importVendors: TempCustomer[]
  orders: Order[]
  vendorOrders: TempOrder[]
  priceSheets: CustomerPriceSheet[]
  sellerPriceSheets: PriceSheet[]
  loading: boolean
  message: string
  description: string
  type: MessageType | null
  showModalImportVendor: boolean
  contacts: MainContact[]
  addresses: MainAddress[]
  documents: Document[]
  documentsLoading: boolean
  routes: WholesaleRoute[]
  newItemId: number
  newVendorId: number
  vendorTotal: number
  vendorList: TempCustomer[]
  hasMore: Boolean
  searchParam: string
  loadingVendor: Boolean
  addingVendor: Boolean
  sortType: string
  orderTotal: number
  documentSearchProps: any
  documentsTotal: number
  POReportParams: any
  POReports: any
  sellerSetting: any
  companyProductTypes: any
  briefVendors: BriefClient[] | null
  saleItems: SaleItem[]
  productListId: number | null
  productList: PriceSheetItemNew[]
  productListLoading: boolean
  loadingPaymentAddresses: boolean
  loadingDeliveryAddresses: boolean
}

@Module('vendors')
export class VendorsModule extends EffectModule<VendorsStateProps> {
  defaultState = {
    items: null,
    vendor: {} as TempCustomer,
    importVendors: [],
    orders: [],
    vendorOrders: [],
    priceSheets: [] as CustomerPriceSheet[],
    sellerPriceSheets: [] as PriceSheet[],
    loading: true,
    message: '',
    description: '',
    type: null,
    showModalImportVendor: false,
    contacts: [],
    addresses: [],
    documents: [],
    documentsLoading: false,
    routes: [],
    newItemId: -1,
    newVendorId: -1,
    vendorTotal: 0,
    vendorList: [],
    hasMore: false,
    searchParam: '',
    loadingVendor: true,
    sortType: '',
    orderTotal: 0,
    documentSearchProps: {},
    documentsTotal: 0,
    POReportParams: {},
    POReports: {},
    sellerSetting: null,
    loadingVendors: true,
    addingVendor: false,
    companyProductTypes: null,
    briefVendors: null,
    saleItems: [],
    productListId: null,
    productList: [],
    productListLoading: true,
    loadingPaymentAddresses: false,
    loadingDeliveryAddresses: false,
  }

  @DefineAction() dispose$!: Observable<void>

  constructor(
    private vendor: VendorService,
    private delivery: DeliveryService,
    private order: OrderService,
    private customer: CustomerService,
    private setting: SettingService,
    private priceSheet: PriceSheetService,
  ) {
    super()
  }

  @Reducer()
  resetDocuments(state:VendorsStateProps){
    return {...state, documents:[], documentsLoading: false, documentsTotal:0}
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return {
        ...state,
        message: '',
        description: '',
        type: null,
        loadingCustomers: true,
        loadingVendors: true,
        loading: true,
      }
    },
  })
  resetLoading(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return { ...state, addingVendor: true, newCustomerId: -1 }
    },
  })
  startAdding(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return { ...state, addingVendor: false }
    },
  })
  endAdding(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return { ...state, newItemId: -1 }
    },
  })
  resetItemId(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: VendorsStateProps, { payload }: Action<any>) => {
      return { ...state, documentSearchProps: payload }
    },
  })
  setDocumentSearchProps(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: VendorsStateProps, { payload }: Action<any>) => {
      return { ...state, POReportParams: payload }
    },
  })
  setPOReportsParams(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, loadingPaymentAddresses: true}
    },
  })
  startLoadingPaymentAddress(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: TempCustomersStateProps) => {
      return {...state, loadingDeliveryAddresses: true}
    },
  })
  startLoadingDeliveryAddress(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<{ [key: string]: string }>) => {
      const newValue = action.payload
      return {
        ...state,
        vendor: {
          ...state.vendor,
          ...newValue,
        },
        // message: 'Vendor Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateCurrentCustomer(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.vendor.updateVendor(state$.value.vendors.vendor.clientId!, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getVendor)(state$.value.vendors.vendor.clientId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect()
  goBack(action$: Observable<void>) {
    return action$.pipe(map(() => goBack()))
  }

  @Effect()
  goto(action$: Observable<string>) {
    return action$.pipe(map((path) => push(path)))
  }

  @Effect({
    done: (state: VendorsStateProps, { payload }: Action<any>) => {
      return { ...state, importCustomers: payload, loadingCustomers: false, loading: false }
    },
  })
  getQBCustomers(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getAllVendors().pipe(
          switchMap((data1) =>
            this.vendor.getImportVendors().pipe(
              switchMap((data2) => of(this.compareImportCustomer(data1, data2))),
              takeUntil(this.dispose$),
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      window.location.reload()
      return {
        ...state,
        loading: false,
      }
    },
  })
  syncQBOVendors(action$: Observable<any>) {
    return action$.pipe(
      switchMap((vendorIds: any) => from(vendorIds)),
      mergeMap((vendorId: any) => {
        return this.customer.syncQBOClient(vendorId)
      }, 8),
      switchMap((data) => of(responseHandler(data, true).body.data)),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<Customer[]>) => {
      return {
        ...state,
        items: action.payload,
        loadingVendors: false,
      }
    },
  })
  getAllCustomersForUser(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getAllVendors().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          switchMap((data: any) => of(this.formatCustomers(data))),
          map(this.createAction('done')),
          takeUntil(this.dispose$),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<Customer[]>) => {
      return {
        ...state,
        briefVendors: action.payload?.sort((a: any, b: any) => {
          return a.clientCompanyName.localeCompare(b.clientCompanyName)
        }),
        loadingVendors: false,
      }
    },
  })
  getBriefVendors(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getBriefVendors().pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          switchMap((data: any) => of(this.formatBriefClient(data))),
          map(this.createAction('done')),
          takeUntil(this.dispose$),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      // let vendorList = state.vendorList
      // if(state.searchParam != action.payload.searchParam){
      //   vendorList = [];
      // }

      // if(state.sortType != action.payload.sortType){
      //   vendorList = [];
      // }
      return {
        ...state,
        searchParam: action.payload.searchParam,
        vendorList: [],
        loadingVendor: true,
        hasMore: false,
      }
    },
  })
  setSearchParam(action$: Observable<any>) {
    return action$.pipe(map(this.createAction('done'), takeUntil(this.dispose$)))
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      const vendorList = state.vendorList.concat(action.payload.dataList)
      return {
        ...state,
        vendorList: vendorList,
        vendorTotal: action.payload.total,
        vendor: {},
        vendorOrders: [],
        contacts: [],
        addresses: [],
        documents: [],
        hasMore: vendorList.length < action.payload.total ? true : false,
        loadingVendor: false,
        newVendorId: -1,
      }
    },
  })
  getAllVendorsByCondition(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) =>
        this.vendor.getAllVendorsByCondition(data).pipe(
          switchMap((data) => of(responseHandler(data, false).body.data)),
          // switchMap((data: any) => of(this.formatCustomers(data))),
          map(this.createAction('done')),
          takeUntil(this.dispose$),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<Customer>) => {
      return {
        ...state,
        vendor: action.payload,
      }
    },
  })
  getVendor(action$: Observable<string>) {
    return action$.pipe(
      switchMap((vendorId) => this.vendor.getVendorInfo(vendorId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      switchMap((data) => of(this.formatCustomer(data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, { payload }: Action<any>) => {
      return { ...state, currentCompanyUsers: payload.userList }
    },
  })
  getCompanyUsers(action$: Observable<any>) {
    return action$.pipe(
      switchMap(() => this.vendor.getCompanyUsers()),
      map(this.createAction('done'), takeUntil(this.dispose$)),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      return {
        ...state,
        vendorOrders: action.payload.dataList,
        orderTotal: action.payload.total,
        loading: false,
        newItemId: -1,
        newVendorId: -1,
      }
    },
  })
  getVendorOrder(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) =>
        this.vendor
          .getVendorOrdersByVendorId(
            parseInt(data.id, 10),
            data.from,
            data.to,
            data.page,
            data.pageSize,
            data.queryParam,
            data.orderByFilter,
            data.direction,
          )
          .pipe(
            switchMap((data: any) => of(responseHandler(data, false).body.data)),
            map((data) => {
              const total = data.total
              const orderList = data.dataList
              const newList = this.formatTempOrder(orderList)
              return { total, dataList: newList }
            }),
          ),
      ),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      let newOrders = state.vendorOrders
      let newItemId = action.payload && action.payload.length > 0 ? action.payload[0].wholesaleOrderId : -1
      if (action.payload && action.payload.length > 0) {
        newOrders.unshift(...action.payload)
      }
      return {
        ...state,
        // message: 'Created Purchase Order Successfully',
        // type: MessageType.SUCCESS,
        vendorOrders: newOrders,
        newItemId: newItemId,
      }
    },
    error_message: (state: VendorsStateProps, action: Action<any>) => {
      return {
        ...state,
        // type: MessageType.ERROR,
        // error: action.payload,
      }
    },
  })
  createEmptyOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.createOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) =>
            of(this.formatTempOrder([responseHandler(data, true).body.data])).pipe(
              map(this.createAction('done')),
              // endWith(this.createActionFrom(this.getVendorOrder)(state$.value.vendors.vendor.clientId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<TempOrder[]>) => {
      return {
        ...state,
        orders: action.payload,
      }
    },
  })
  getVendorOrders(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    // const processData = (data: TempOrder[]) => {
    //   const sortedData = sortBy(data, 'id').reverse()
    //   return sortedData
    // }
    return action$.pipe(
      switchMap(() =>
        this.vendor.getVendorOrders(state$.value.currentUser.userId).pipe(
          map((data) => data.map((order) => this.formatOrder(order))),
          map(this.createAction('done')),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<PriceSheet[]>) => {
      return {
        ...state,
        sellerPriceSheets: action.payload,
      }
    },
  })
  getVendorSellerPriceSheet(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.vendor.getSellerPriceSheet(state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) =>
            of(responseHandler(data, false).body.data).pipe(
              map(this.createAction('done')),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<CustomerPriceSheet[]>) => {
      return {
        ...state,
        priceSheets: action.payload,
      }
    },
  })
  getVendorPriceSheet(action$: Observable<string>) {
    return action$.pipe(
      switchMap((customerId) => this.vendor.getVendorPriceSheet(customerId)),
      switchMap((data: any) => of(responseHandler(data, false).body.data)),
      switchMap((data: any) => of(this.formatPriceSheet(data))),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      return {
        ...state,
        // message: 'Created Vendor Successfully',
        // type: MessageType.SUCCESS,
        newVendorId: action.payload.clientId,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        addingCustomers: false,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createVendor(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.vendor.createVendor(data).pipe(
          switchMap((data: any) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.syncCompany)(data.body.data.companyId)),
              // endWith(this.createActionFrom(this.getBriefVendors)()),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  syncCompany(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customerId) => this.customer.syncCompany(customerId)),
      map(this.createAction('done')),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  syncContactUser(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((contactId) => this.customer.syncContactUser(contactId)),
      map(this.createAction('done')),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return {
        ...state,
        showModalImportCustomer: false,
        message: 'Customers Imported Successfully',
        type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingCustomers: false,
        loading: false,
        message: payload,
        type: MessageType.ERROR,
        hasError: true,
      }
    },
  })
  createVendors(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((customers: any) => from(customers)),
      mergeMap((customer: any) => {
        var newCustomer = { company: customer.company, mainContact: customer }
        return this.vendor.createVendor(newCustomer)
      }),
      map(this.createAction('done')),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return {
        ...state,
        message: 'Assign PriceSheet Successful',
        type: MessageType.SUCCESS,
      }
    },
  })
  assignPriceSheet(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((priceSheetId) =>
        this.vendor.assignPriceSheet(priceSheetId, +state$.value.vendors.vendor.clientId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getVendorPriceSheet)(state$.value.vendors.vendor.clientId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return { ...state, message: '', description: '', type: null }
    },
  })
  resetNotif(action$: Observable<void>) {
    return action$.pipe(map(this.createAction('done')))
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return {
        ...state,
        message: 'Unassign PriceSheet Successful',
        type: MessageType.SUCCESS,
      }
    },
  })
  unAssignPriceSheet(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((priceSheetClientId) =>
        this.vendor.unAssignPriceSheet(priceSheetClientId).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getVendorPriceSheet)(state$.value.vendors.vendor.clientId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<Customer>) => {
      return {
        ...state,
        contacts: action.payload,
      }
    },
  })
  getVendorContacts(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((vendorId) => this.vendor.getContacts(vendorId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      // switchMap((data) => of(this.formatCustomers(data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return {
        ...state,
        // message: 'Create a contact successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createVendorContact(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: MainContact) =>
        this.vendor.createContact(state$.value.vendors.vendor.clientId!, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getVendorContacts)(state$.value.vendors.vendor.clientId!)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        // message: 'Set Main Contact',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  setVendorMainContact(action$: Observable<number>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((contactId: number) =>
        this.vendor.setMainContact(state$.value.vendors.vendor.clientId!, contactId).pipe(
          switchMap((data) =>
            of(responseHandler(data).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.syncContactUser)(data.body.data.mainContact.user.userId)),
              endWith(this.createActionFrom(this.getVendorContacts)(state$.value.vendors.vendor.clientId!)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        // message: 'Contact Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateVendorContact(action$: Observable<MainContact>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: MainContact) =>
        this.vendor.updateContact(state$.value.vendors.vendor.clientId!, data.contactId, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getVendorContacts)(state$.value.vendors.vendor.clientId!)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<Customer>) => {
      return {
        ...state,
        addresses: action.payload,
        loadingPaymentAddresses: false,
        loadingDeliveryAddresses: false,
      }
    },
  })
  getVendorAddresses(action$: Observable<string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((vendorId) => this.vendor.getAddresses(vendorId)),
      switchMap((data) => of(responseHandler(data, false).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return {
        ...state,
        // message: 'Create an address successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingPaymentAddresses: false,
        loadingDeliveryAddresses: false,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createVendorAddress(action$: Observable<MainAddress>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: MainAddress) =>
        this.vendor.createAddress(state$.value.vendors.vendor.clientId!, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getVendorAddresses)(state$.value.vendors.vendor.clientId!)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        // message: 'Address Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        loadingPaymentAddresses: false,
        loadingDeliveryAddresses: false,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateVendorAddress(action$: Observable<MainAddress>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: MainAddress) =>
        this.vendor.updateAddress(state$.value.vendors.vendor.clientId!, data.wholesaleAddressId, data).pipe(
          switchMap((data) =>
            of(responseHandler(data, true).body.data).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getVendorAddresses)(state$.value.vendors.vendor.clientId!)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  @Effect({
    before: state => {
      return {...state, documentsLoading:true}
    },
    done: (state: VendorsStateProps, action: Action<Customer>) => {
      const { payload } = action
      return {
        ...state,
        documentsLoading:false,
        documents: typeof payload === 'string' ? [] : payload && payload.dataList ? payload.dataList : [],
        documentsTotal: typeof payload === 'string' ? 0 : payload && payload.total ? payload.total : 0,
      }
    },
  })
  getDocuments(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data) => this.vendor.getDocuments(data).pipe(
        switchMap((data) => of(responseHandler(data, false).body.data)),
        map(this.createAction('done'), takeUntil(this.dispose$)),
        startWith(this.createAction('before')(true)),
        catchError((error) => of(checkError(error))),
      )),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return {
        ...state,
        // message: 'Create a document successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  createDocument(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.vendor.createDocument(state$.value.vendors.vendor.clientId!, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getDocuments)({
              ...state$.value.vendors.documentSearchProps,
              vendorId: state$.value.vendors.vendor.clientId.toString(),
            }),
          ),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<{ [key: string]: string }>) => {
      return {
        ...state,
        // message: 'Docuement Updated Successfully',
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, { payload }: Action<any>) => {
      return {
        ...state,
        // message: payload,
        // type: MessageType.ERROR,
        // hasError: true,
      }
    },
  })
  updateDocument(action$: Observable<Document>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: Document) =>
        this.vendor.updateDocument(state$.value.vendors.vendor.clientId!, data.id, data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(
            this.createActionFrom(this.getDocuments)({
              ...state$.value.vendors.documentSearchProps,
              vendorId: state$.value.vendors.vendor.clientId.toString(),
            }),
          ),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, { payload }: Action<WholesaleRoute[]>) => {
      return {
        ...state,
        routes: payload,
      }
    },
  })
  getAllRoutes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.delivery.getAllRoutes()),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, { payload }: Action<WholesaleRoute[]>) => {
      return {
        ...state,
        POReports: payload && payload.length > 0 ? payload[0] : [],
      }
    },
  })
  getPurchaseOrderStatistics(action$: Observable<any>) {
    return action$.pipe(
      switchMap((data) => this.vendor.getPurchaseOrderStatistics(data)),
      switchMap((data) => of(responseHandler(data).body.data)),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<any>) => {
      return {
        ...state,
        sellerSetting: action.payload.userSetting,
      }
    },
  })
  getSellerSetting(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.setting
          .getUserSetting(state$.value.currentUser.userId ? state$.value.currentUser.userId : '0')
          .pipe(map(this.createAction('done'))),
      ),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    done: (state: any, action: Action<any>) => {
      return {
        ...state,
        companyProductTypes: action.payload,
      }
    },
  })
  getCompanyProductAllTypes(action$: Observable<void>) {
    return action$.pipe(
      switchMap(() => this.setting.getCompanyProductAllTypes()),
      switchMap((data) => of(this.formatProductTypes(responseHandler(data).body.data))),
      map(this.createAction('done'), takeUntil(this.dispose$)),
      catchError((error) => of(checkError(error))),
    )
  }

  @Effect({
    before: (state: VendorsStateProps) => ({ ...state, loading: true }),
    done: (state: VendorsStateProps, action: Action<any>) => {
      let newOrders = state.vendorOrders
      if (action.payload && action.payload.length > 0) newOrders.unshift(...action.payload)
      console.log('action.payload', action.payload)
      return {
        ...state,
        vendorOrders: newOrders,
        newItemId: action.payload[0].wholesaleOrderId,
        message: 'DUPLICATED_SUCCESS',
        loading: false
        // type: MessageType.SUCCESS,
      }
    },
    error_message: (state: VendorsStateProps, action: Action<any>) => {
      return {
        ...state,
      }
    },
  })
  duplicateOrder(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: any) =>
        this.order.duplicatePurchaseOrder(data, state$.value.currentUser ? state$.value.currentUser.userId! : '1').pipe(
          switchMap((data) => of(responseHandler(data).body.data)),
          switchMap((data) =>
            of(this.formatTempOrder([data])).pipe(
              map(this.createAction('done')),
              startWith(this.createAction('before')()),
              // endWith(this.createActionFrom(this.getOrder)(state$.value.customers.customer.clientId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
        ),
      ),
    )
  }

  formatProductTypes = (datas: any[]) => {
    let result = {
      unitOfMeasure: [],
      returnReason: [],
      carrier: [],
      modifier: [],
      costStructure: [],
      modeOfTransportation: [],
      suppliers: [],
      freightTypes: [],
      shippingTerms: [],
      paymentTerms: [],
      paymentTermsFixedTypes: [],
      extraCOO: [],
      extraCharges: [],
      customerTypes: [],
      vendorTypes: [],
      reasonType: []
    }

    // UNIT_OF_MEASURE, RETURN_REASON, CARRIER,MODIFIER,COST_STRUCTURE,MODE_OF_TRANSPORTATION
    datas.map((type: any) => {
      switch (type.type) {
        case 'UNIT_OF_MEASURE':
          result.unitOfMeasure.push(type)
          break
        case 'RETURN_REASON':
          result.returnReason.push(type)
          break
        case 'CARRIER':
          result.carrier.push(type)
          break
        case 'MODIFIER':
          result.modifier.push(type)
          break
        case 'COST_STRUCTURE':
          result.costStructure.push(type)
          break
        case 'MODE_OF_TRANSPORTATION':
          result.modeOfTransportation.push(type)
          break
        case 'SUPPLIERS':
          result.suppliers.push(type)
          break
        case 'FREIGHT':
          result.freightTypes.push(type)
          break
        case 'SHIPPING_TERM':
          result.shippingTerms.push(type)
          break
        case 'FINANCIAL_TERM':
          result.paymentTerms.push(type)
          break
        case 'FINANCIAL_TERM_FIXED_DAYS':
          result.paymentTermsFixedTypes.push(type)
          break
        case 'EXTRA_COO':
          result.extraCOO.push(type)
          break
        case 'EXTRA_CHARGE':
          result.extraCharges.push(type)
          break
        case 'CUSTOMER_BUSINESS_TYPE':
          result.customerTypes.push(type)
          break
        case 'VENDOR_BUSINESS_TYPE':
          result.vendorTypes.push(type)
          break
        case 'REASON_TYPE':
          result.reasonType.push(type)
          break
      }
    })
    return result
  }

  formatCustomer(data: any): TempCustomer {
    return data
    // const status = data.status ? data.status.toLowerCase() : CustomerStatus.INACTIVE
    // const type = data.type ? data.type.toLowerCase() : CustomerType.INDIVIDUAL

    // return {
    //   type: type,
    //   id: data.customerId,
    //   name: data.fullName,
    //   company: data.company,
    //   phone: data.phone,
    //   address: data.address as Address,
    //   email: data.emailAddress,
    //   status,
    //   qboId: data.qboId,
    // } as Customer
  }

  formatCustomers(datas: any[]): TempCustomer[] {
    // TODO
    // 'address' always to be null
    return datas.map((data) => {
      // const status = data.status ? data.status.toLowerCase() : CustomerStatus.INACTIVE
      // const type = data.type ? data.type.toLowerCase() : CustomerType.INDIVIDUAL
      return data
    })
  }

  formatBriefClient(datas: any[]): TempCustomer[] {
    return datas.map((data) => {
      if (data.mainBillingAddressId) {
        return {
          clientId: data.clientId,
          clientCompanyName: data.clientCompanyName,
          wholesaleCompanyId: data.wholesaleCompanyId,
          clientCompanyId: data.clientCompanyId,
          type: data.type,
          businessType: data.businessType,
          mainContactName: data.mainContactName,
          mainContactEmail: data.mainContactEmail,
          mobilePhone: data.mobilePhone,
          lastQBOUpdate: data.lastQBOUpdate,
          status: data.status,
          mainBillingAddress: {
            address: {
              street1: data.street1,
              street2: data.street2,
              zipcode: data.zipcode,
              city: data.city,
              country: data.country,
              state: data.state,
            },
          },
        }
      } else {
        return data
      }
    })
  }

  compareImportCustomer(data1: Customer[], data2: any): Customer[] {
    const vendorList = {}
    const vendorList2 = {}
    const importCustomerList = []
    for (const vendor of data1) {
      if (vendor.qboId != null) vendorList[vendor.qboId] = vendor
    }
    for (const vendor of data2.customerList) {
      const status = vendor.status ? CustomerStatus.ACTIVE : CustomerStatus.INACTIVE
      if (vendorList2[vendor.id] === 1) {
        continue
      }
      vendorList2[vendor.id] = 1
      const tempCustomer: Customer = {
        type: CustomerType.INDIVIDUAL,
        id: '',
        company: vendor.company,
        name: '',
        phone: '',
        email: '',
        address: {} as Address,
        status: status,
        qboId: vendor.id,
      }
      if (vendor.company == null || vendor.company.length === 0) tempCustomer.company = vendor.fullyQualifiedName
      if (vendor.primaryEmailAddr != null && vendor.primaryEmailAddr.address != null)
        tempCustomer.email = vendor.primaryEmailAddr.address
      if (vendor.primaryPhone != null && vendor.primaryPhone.freeFormNumber != null)
        tempCustomer.phone = vendor.primaryPhone.freeFormNumber
      if (vendor.givenName != null) tempCustomer.name += vendor.givenName
      if (vendor.familyName != null) {
        if (tempCustomer.name.length > 0) tempCustomer.name += ' '
        tempCustomer.name += vendor.familyName
      }
      if (tempCustomer.name == null || tempCustomer.name.length === 0) {
        tempCustomer.name = vendor.displayName
      }
      if (vendorList[vendor.id] == null) {
        importCustomerList.push(tempCustomer)
      }
    }

    return importCustomerList.map((data) => ({
      type: data.type,
      id: '',
      company: data.company,
      name: data.name,
      phone: data.phone,
      email: data.email,
      address: data.address,
      status: data.status,
      qboId: data.qboId,
    }))
  }

  formatTempOrder(datas: any[]): TempOrder[] {
    return datas.map((data) => {
      data.createdDate = format(data.createdDate, 'MM/D/YY')
      data.deliveryDate = format(data.deliveryDate, 'MM/D/YY')
      data.updatedDate = format(data.updatedDate, 'MM/D/YY')
      if (data.wholesaleOrderStatus != null) data.status = data.wholesaleOrderStatus
      return data
    })
  }

  formatOrder(data: any): Order {
    return {
      id: data.wholesaleOrderId,
      totalCost: data.totalCost,
      totalPrice: data.totalPrice,
      status: data.status,
      deliveryDate: +new Date(data.deliveryDate),
      updatedDate: +new Date(data.updatedDate),
      priceSheetId: data.priceSheetId,
      qboId: data.qboId,
      customer: {
        id: data.wholesaleCustomerClientId,
        name: data.fullName,
      },
      linkToken: data.linkToken,
      firstName: data.firstName,
      lastName: data.lastName,
      company: {
        id: data.wholesaleClient.clientCompany.id,
        name: data.wholesaleClient.clientCompany.companyName,
      },
    }
  }

  formatPriceSheet(datas: any[]): CustomerPriceSheet[] {
    // TODO MOCK_DATA
    // data need 'assigned' & 'items'
    return datas.map((data) => ({
      id: data.priceSheetId,
      clientId: data.priceSheetClientId,
      updatedAt: format(data.updatedDate, 'MM/D/YY'),
      name: data.name,
      assigned: 0,
      items: 0,
    }))
  }

  @Effect({
    done: (state: VendorsStateProps, action: Action<SaleItem[]>) => {
      return {
        ...state,
        saleItems: action.payload,
      }
    },
  })
  getSaleItems(action$: Observable<void>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(() =>
        this.priceSheet
          .getAllItems(state$.value.currentUser.company ? state$.value.currentUser.company : 'GRUBMARKET')
          .pipe(
            switchMap((data) => of(responseHandler(data, false).body.data)),
            map(this.createAction('done')),
            catchError((error) => of(checkError(error))),
          ),
      ),
    )
  }

  @Effect({
    before: (state: VendorsStateProps) => ({ ...state, productList: [], productListId: null }),
    done: (state: VendorsStateProps, action: Action<number>) => {
      return {
        ...state,
        productListId: action.payload,
      }
    },
  })
  getProductListId(action$: Observable<number | string>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((vendorId) => {
        return this.vendor.getProductListByVendorId(vendorId).pipe(
          switchMap((data: any) =>
            of(responseHandler(data, false).body.data.priceSheetId).pipe(
              map(this.createAction('done')),
              endWith(this.createActionFrom(this.getProductList)(data.body.data.priceSheetId)),
              catchError((error) => of(checkError(error))),
            ),
          ),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        )
      }),
    )
  }

  @Effect({
    before: (state: VendorsStateProps) => ({ ...state, productListLoading: true }),
    done: (state: VendorsStateProps, action: Action<SaleItem[]>) => ({
      ...state,
      productList: action.payload,
      productListLoading: false,
    }),
  })
  getProductList(action$: Observable<any>) {
    return action$.pipe(
      switchMap((priceSheetId) =>
        this.priceSheet.getPriceSheetItems(priceSheetId).pipe(
          switchMap((data) => {
            return of(responseHandler(data, false).body.data)
          }),
          map(this.createAction('done')),
          startWith(this.createAction('before')()),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return { ...state, message: 'Updated Product List Successfully', type: MessageType.SUCCESS }
    },
  })
  assignPriceSheetItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: ParamType) =>
        this.priceSheet.assignPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getProductList)(data.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect({
    done: (state: VendorsStateProps) => {
      return { ...state, message: 'Updated Product List Successfully', type: MessageType.SUCCESS }
    },
  })
  addPriceSheetItems(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap((data: ParamType) =>
        this.priceSheet.addPriceSheetItems(data).pipe(
          switchMap((data) => of(responseHandler(data, true).body.data)),
          map(this.createAction('done')),
          endWith(this.createActionFrom(this.getProductList)(data.priceSheetId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
    )
  }

  @Effect()
  updatePirceSheetItem(action$: Observable<any>, state$: StateObservable<GlobalState>) {
    return action$.pipe(
      switchMap(({ callback, priceSheetItemId, ...rest }: any) =>
        this.vendor.updatePirceSheetItem(priceSheetItemId, rest).pipe(
          switchMap((res) => {
            callback(res)
            return of(responseHandler(res, true))
          }),
          map(this.createAction('done')),
          // endWith(this.createActionFrom(this.getProductList)(state$.value.vendors.productListId)),
          catchError((error) => of(checkError(error))),
        ),
      ),
      catchError((error) => of(checkError(error))),
    )
  }
}

type ParamType = {
  priceSheetId: number
}

export type VendorsDispatchProps = ModuleDispatchProps<VendorsModule>
