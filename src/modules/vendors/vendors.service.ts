import { Http } from '~/common'
import { Injectable } from 'redux-epics-decorator'
import { TempCustomer as Vendor } from '~/schema'

@Injectable()
export class VendorService {
  constructor(private readonly http: Http) {}

  getAllVendors() {
    return this.http.get<any[]>(`/vendor/list`)
  }

  getBriefVendors() {
    return this.http.get<any[]>(`/vendor/brief-vendors`)
  }

  getAllVendorsByCondition(data: any) {
    let queryParam = escape(data.queryParam)
    return this.http.get<any>(
      `/vendor/lists?queryParam=${queryParam}&page=${data.page}&pageSize=${data.pageSize}&sortType=${data.sortType}&active=${data.active}`,
    )
  }

  getVendorInfo(vendorId: string) {
    return this.http.get<Vendor>(`/vendor/get/${vendorId}`)
  }

  getCompanyUsers() {
    return this.http.get<any[]>(`/session/getCompanyUsers`)
  }

  getImportVendors() {
    // return this.http.get<any[]>(`importVendors`)
    return this.http.get<any[]>(`/vendor/getQBOvendors`)
  }

  getImportNSVendors() {
    // return this.http.get<any[]>(`importVendors`)
    return this.http.get<any[]>(`/netsuite/vendor`)
  }

  createVendor(data: any) {
    return this.http.post(`/vendor/create`, {
      body: JSON.stringify(data),
    })
  }

  updateVendor(vendorId: number, data: any) {
    return this.http.post(`/vendor/update/${vendorId}`, {
      body: JSON.stringify(data),
    })
  }

  getVendorOrdersByVendorId(
    vendorId: number,
    fromDate: string,
    toDate: string,
    page: number,
    pageSize: number,
    queryParam: string,
    orderBy: string,
    direction: string,
  ) {
    let url = `/account/client/${vendorId}/orders?from=${fromDate}&to=${toDate}`
    if (page != null && pageSize != null) {
      url += `&page=${page}&pageSize=${pageSize}`
    }
    if (queryParam) {
      url += `&queryParam=${queryParam}`
    }
    if (orderBy != '') url += `&orderBy=${orderBy}&direction=${direction}`
    return this.http.get<any[]>(url)
  }

  getVendorOrders(userId: string) {
    return this.http.get<any[]>(`/account/user/${userId}/vendor/orders/list`)
  }

  getSellerPriceSheet(userId: string) {
    return this.http.get(`/pricesheet/userCompany/${userId}`)
  }

  getVendorPriceSheet(vendorId: string) {
    return this.http.get(`/account/user/${vendorId}/pricesheets/list`)
  }

  assignPriceSheet(priceSheetId: number, clientId: number) {
    return this.http.post(`/pricesheet/${priceSheetId}/assign/customer/${clientId}`)
  }

  unAssignPriceSheet(priceSheetClientId: number) {
    return this.http.post(`/pricesheet/unassign/pricesheetclient/${priceSheetClientId}`)
  }

  getContacts(vendorId: string) {
    return this.http.get(`/vendor/getContacts/${vendorId}`)
  }
  createContact(vendorId: number, data: any) {
    return this.http.post(`/vendor/createContact/${vendorId.toString()}`, {
      body: JSON.stringify(data),
    })
  }
  updateContact(vendorId: number, contactId: number, data: any) {
    return this.http.put(`/vendor/updateContact/${vendorId.toString()}/${contactId.toString()}`, {
      body: JSON.stringify(data),
    })
  }
  setMainContact(vendorId: number, contactId: number) {
    return this.http.post(`/vendor/${vendorId.toString()}/setMainContact/${contactId.toString()}`)
  }

  getAddresses(vendorId: string) {
    return this.http.get(`/vendor/getAddress/${vendorId}`)
  }
  createAddress(vendorId: number, data: any) {
    return this.http.post(`/vendor/createAddress/${vendorId.toString()}`, {
      body: JSON.stringify(data),
    })
  }
  updateAddress(vendorId: number, addressId: number, data: any) {
    return this.http.post(`/vendor/updateAddress/${vendorId.toString()}/${addressId.toString()}`, {
      body: JSON.stringify(data),
    })
  }

  getDocuments(data: any) {
    const {vendorId, ...rest} = data
    return this.http.get(
      `/vendor/user/getDocuments/${vendorId}`,{ query: rest }
    )
  }
  createDocument(vendorId: number, data: any) {
    return this.http.post(`/vendor/user/createDocument/${vendorId.toString()}`, {
      body: JSON.stringify(data),
    })
  }
  updateDocument(vendorId: number, id: number, data: any) {
    return this.http.put(`/vendor/user/updateDocument/${vendorId.toString()}/${id.toString()}`, {
      body: JSON.stringify(data),
    })
  }

  getPurchaseOrderStatistics(data: any) {
    return this.http.get(
      `/vendor/order/statistics?from=${data.from}&to=${data.to}&search=${data.search}&vendorId=${data.id}&type=${data.type}`,
    )
  }

  getProductListByVendorId(vendorId: number | string) {
    return this.http.get(`/vendor/get-product-list-sheet/${vendorId}`)
  }

  updatePirceSheetItem(priceSheetItemId: number, data: any) {
    return this.http.post<any>(`/pricesheet/${priceSheetItemId}/pricesheetitem/update?isCustomField=true`, {
      body: JSON.stringify(data),
    })
  }

  getVendorProducts(vendorId: number | string) {
    return this.http.get(`/pricesheet/get-vendor-products/${vendorId}`)
  }
}
