import { ActionsObservable, ofType } from 'redux-epics-decorator'
import { flatMap, map, startWith } from 'rxjs/operators'
import { Action, createAction } from 'redux-actions'
import { of } from 'rxjs'

import { GlobalState } from './store/reducer'
import { AuthUser, UserRole } from './schema'
import { CACHED_COMPANY, CACHED_ACCOUNT_TYPE, CACHED_USER_ID, CACHED_FULLNAME, CACHED_ACCESSTOKEN } from './common'

export const USER_LOGIN = createAction<void>('USER_LOGIN')
const USER_LOGIN_SUCCESS = createAction<AuthUser>('USER_LOGIN_SUCCESS')

export const USER_LOGOUT_SUCCESS = createAction<void>('USER_LOGOUT_SUCCESS')

const GLOBAL_LOADING = createAction<void>('loading')

export const globalDataEpic = (action$: ActionsObservable<Action<void>>) => {
  return action$.pipe(
    ofType(`${USER_LOGIN}`),
    startWith(GLOBAL_LOADING), // dont know this for what?
    flatMap(() => {
      return of({
        userId: localStorage.getItem(CACHED_USER_ID)!,
        accessToken: localStorage.getItem(CACHED_ACCESSTOKEN)!,
        // session: localStorage.getItem(CACHED_AUTH_TOKEN),
        accountType: localStorage.getItem(CACHED_ACCOUNT_TYPE) as UserRole,
        // account: localStorage.getItem(CACHED_ACCOUNT_TYPE)!,
        company: localStorage.getItem(CACHED_COMPANY)!,
        fullName: localStorage.getItem(CACHED_FULLNAME)!,
      })
    }),
    map(USER_LOGIN_SUCCESS),
  )
}

export const globalDataReducer = (state: GlobalState | null = null, action: Action<AuthUser>) => {
  if (action.type === `${USER_LOGIN_SUCCESS}`) {
    return {
      ...action.payload,
      isLogined: Boolean(action.payload!.accessToken),
    }
  }

  if (action.type === `${USER_LOGOUT_SUCCESS}`) {
    return {
      ...action.payload,
      isLogined: false,
    }
  }

  return state
}

export interface RootDispatchProps {
  fetchGlobalData: typeof USER_LOGIN
}
