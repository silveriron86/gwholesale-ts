import { CategorySetting, ThemeSetting } from './modules/setting'
//import { CustomerDetailContainer } from './modules/customers/components/customer-detail'
import { Inventory, ItemStock } from './modules/inventory'
import { OrderDetail, Orders } from './modules/orders'
import { PriceSheetDetail, PriceSheets } from './modules/pricesheet'

import AccountDetails from './modules/customers/accounts/account-details'
import { AccountProfileContainer } from './modules/account'
import Addresses from './modules/customers/accounts/addresses'
import AuditLog from './modules/manufacturing/work-orders/audit-log/audit-log'
import { CashSales } from './modules/cashSales'
import CashSalesDetailContainer from './modules/cashSales/nav-sales/order-detail/order-detail-container'
import Chat from './modules/customers/accounts/chat'
import Contacts from './modules/customers/accounts/contacts'
import CreditMemo from './modules/customers/sales/credit-memo'
import { CustomerPriceSheetsContainer } from './modules/customers/accounts/price-sheets'
import CustomerProductsPage from './modules/customers/accounts/products'
import { Customers } from './modules/customers'
import { CustomersDetailContainer } from './modules/customers/components/customers-detail'
import { DefaultPricingDetail } from './modules/pricing/pricing-detail/default-pricing-detail'
import { DeliveryContainer } from './modules/delivery/delivery.container'
import { DeliveryRoutingDetail } from './modules/delivery/routings/route.container'
import { DeliveryRoutings } from './modules/delivery/routings/routings.container'
import Documents from './modules/customers/accounts/documents'
import DriverReports from './modules/delivery/drivers'
import FinancialDetails from './modules/customers/accounts/financial-details'
import { Intelligence } from './modules/intelligence'
import { Label } from './modules/product/components/label.container'
import LocationPage from './modules/location'
import { NewLabel } from './modules/product/components/Label/label.container'
import { NewOrderContainer } from './modules/orders/order-new.container'
import { NewOrderPriceSheetContainer } from './modules/orders/order-pricesheet-new.container'
import { NewOrderStockContainer } from './modules/orders/order-stock-new.container'
import { NewTokenOrderContainer } from './modules/orders/order-token-new.container'
import OrderDetails from './modules/vendors/purchase-orders/order-details'
import OrderPrint from './modules/orders/order-print'
import Overview from './modules/manufacturing/work-orders/overview'
import PalletHistory from './modules/location/history'
import { PriceContainer } from './modules/pricing/pricing.container'
import PriceSheetPrintView from '~/modules/pricesheet/pricesheet-print.container'
import { PricingDetail } from './modules/pricing/pricing-detail/pricing-detail.container'
import ProcessingList from './modules/manufacturing/processing-list'
import ProcessingResource from './modules/manufacturing/processing-resource/index.container'
import { ProductContainer } from './modules/product'
import ProductList from './modules/vendors/accounts/product-list'
import PurchaseDocuments from './modules/vendors/purchase-orders/documents'
import PurchaseOrderReports from './modules/vendors/purchase-order-report'
import PurchaseOverview from './modules/vendors/purchase-orders/overview'
import Receiving from './modules/vendors/purchase-orders/receiving'
import RelatedOrders from './modules/vendors/purchase-orders/related-orders'
import ReportDetail from './modules/reports/components/report-detail'
import ReportsContainer from './modules/reports/reports.container'
import { RequisitionContainer } from './modules/vendors/requisition/requisition.container'
import { Restock } from './modules/restock/restock.container'
import { RouteProps } from 'react-router'
import SalesCart from './modules/customers/sales/cart'
import SalesCheckout from './modules/customers/sales/checkout'

import {PointOfSale} from './modules/pointOfSale/pointOfSale.container'
import SalesDocuments from './modules/customers/sales/documents'
import SalesMargins from './modules/customers/sales/margins'
import SalesOrderDetailContainer from './modules/customers/nav-sales/order-detail/order-detail-container'
/**
 * nav-2.5 components
 */
import SalesOrderList from './modules/customers/nav-sales/order-list/sales-order-list'
import SalesOrderReport from './modules/customers/report'
import SalesSettings from './modules/customers/sales/settings'
// import PurchaseOrders from './modules/vendors/purchase-orders/orders.container'
import Settings from './modules/settings/settings.container'
import Special from './modules/manufacturing/work-orders/special'
import { Statistics } from './modules/statistics/statistics.container'
import Templates from './modules/manufacturing/work-orders/templates'
import { UserRole } from './schema'
import VendorAccountDetails from './modules/vendors/accounts/account-details'
import VendorAddresses from './modules/vendors/accounts/addresses'
import VendorContacts from './modules/vendors/accounts/contacts'
import VendorCreditMemo from './modules/vendors/purchase-orders/credit-memo'
import { VendorDetailContainer } from './modules/vendors/accounts/vendor-detail'
import VendorDocuments from './modules/vendors/accounts/documents'
import VendorFinancialDetail from './modules/vendors/accounts/financial-detail'
import { Vendors } from './modules/vendors'
import WorkOrderDetails from './modules/manufacturing/work-orders/order-details'
import WorkOrdersList from './modules/manufacturing/work-orders/work-orders-list.container'

interface RouteConfig extends RouteProps {
  path: string
  needRoles?: UserRole[]
  showInHeader?: boolean
  headerName?: string
  title?: string
}

export const roleFilter = (currentRole: UserRole) => (config: RouteConfig) => {
  if (Array.isArray(config.needRoles) && config.needRoles.length) {
    // role matched in needRoles
    return config.needRoles.indexOf(currentRole) > -1
  }
  // no needRoles
  return true
}

const routeConfig: RouteConfig[] = [
  // Routing for Customers module
  {
    path: '/customers',
    component: Customers,
    showInHeader: true,
    headerName: 'Customers',
    title: 'Customers',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/orders',
    showInHeader: true,
    component: CustomersDetailContainer,
    headerName: 'Customer Accounts',
    title: 'Customer Orders',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/products',
    component: CustomerProductsPage,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Products',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/price-sheets',
    component: CustomerPriceSheetsContainer,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Price Sheets',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/price/:id',
    component: PricingDetail,
    showInHeader: true,
    headerName: 'Price Sheet Detail',
    title: 'Price Sheet Detail',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/account',
    component: AccountDetails,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Account Details',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/addresses',
    component: Addresses,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Addresses',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/contacts',
    component: Contacts,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Contacts',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/financial-details',
    component: FinancialDetails,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Financial Details',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/documents',
    component: Documents,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Documents',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/chat',
    component: Chat,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Chat',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/customer/:customerId/settings',
    component: SalesSettings,
    showInHeader: true,
    headerName: 'Customer Accounts',
    title: 'Customer Settings',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },

  // Routing for Sales Order module
  {
    path: '/sales-orders',
    component: Orders,
    showInHeader: true,
    headerName: 'Sales Orders',
    title: 'Customer Settings',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.CUSTOMER,
      UserRole.SALES,
      UserRole.WAREHOUSE,
      UserRole.SELLER_RESTRICTED,
    ],
  },
  {
    path: '/sales-order-reports',
    component: SalesOrderReport,
    showInHeader: true,
    headerName: 'Sales Order Reports',
    title: 'Sales Order',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.CUSTOMER, UserRole.SALES, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/sales-order/:orderId',
    component: SalesOrderDetailContainer,
    showInHeader: true,
    headerName: 'Sales Order',
    title: 'Sales Order Cart',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED],
  },
  // {
  //   path: '/order/:orderId/sales-cart',
  //   component: SalesCart,
  //   showInHeader: true,
  //   headerName: 'Sales Order',
  //   title: 'Sales Order Cart',
  //   needRoles: [UserRole.SALES],
  // },
  {
    path: '/order/:orderId/sales-documents',
    component: SalesDocuments,
    showInHeader: true,
    headerName: 'Sales Order',
    title: 'Sales Documents',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES],
  },
  {
    path: '/order/:orderId/sales-checkout',
    component: SalesCheckout,
    showInHeader: true,
    headerName: 'Sales Order',
    title: 'Sales Order Checkout',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES],
  },
  {
    path: '/order/:orderId/credit-memo',
    component: CreditMemo,
    showInHeader: true,
    headerName: 'Sales Order',
    title: 'Sales Order Credit Memo',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES],
  },
  {
    path: '/order/:orderId/purchase-credit-memo',
    component: VendorCreditMemo,
    showInHeader: true,
    headerName: 'Purchase Order',
    title: 'Purchase Order Credit Memo',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES],
  },
  ,
  {
    path: '/order/:orderId/sales-margins',
    component: SalesMargins,
    showInHeader: true,
    headerName: 'Sales Order',
    title: 'Sales Order Margins',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES],
  },
  {
    path: '/order/:orderId/sales-settings',
    component: SalesSettings,
    showInHeader: true,
    headerName: 'Sales Order',
    title: 'Sales Order Settings',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES],
  },

  {
    path: '/cash-sales',
    component: CashSales,
    showInHeader: true,
    headerName: 'Cash Sales',
    title: 'Cash Sales',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.CUSTOMER, UserRole.SALES, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/cash-sales/:orderId',
    component: CashSalesDetailContainer,
    showInHeader: true,
    headerName: 'Cash Sales',
    title: 'Cash Sales Cart',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.SELLER_RESTRICTED],
  },
  // Routing for point of sale
  {
    path: '/POS',
    component: PointOfSale,
    showInHeader: true,
    headerName: 'Sales Receipt',
    title: 'Sales Receipt',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.SELLER_RESTRICTED],
  },

  // Routing for Vendor module
  {
    path: '/vendors',
    component: Vendors,
    showInHeader: true,
    headerName: 'Vendors',
    title: 'Vendors',
    needRoles: [UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/orders',
    showInHeader: true,
    component: VendorDetailContainer,
    headerName: 'Vendor Accounts',
    title: 'Vendor Orders',
    needRoles: [UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/account',
    component: VendorAccountDetails,
    showInHeader: true,
    headerName: 'Vendor Accounts',
    title: 'Vendor Account Details',
    needRoles: [UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/products',
    component: ProductList,
    showInHeader: true,
    headerName: 'Vendor Products',
    title: 'Vendor Products',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/addresses',
    component: VendorAddresses,
    showInHeader: true,
    headerName: 'Vendor Accounts',
    title: 'Vendor Addresses',
    needRoles: [UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/financial-details',
    component: VendorFinancialDetail,
    showInHeader: true,
    headerName: 'Vendor Accounts',
    title: 'Financial Detail',
    needRoles: [UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/contacts',
    component: VendorContacts,
    showInHeader: true,
    headerName: 'Vendor Accounts',
    title: 'Vendor Contacts',
    needRoles: [UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/documents',
    component: VendorDocuments,
    showInHeader: true,
    headerName: 'Vendor Accounts',
    title: 'Vendor Documents',
    needRoles: [UserRole.BUYER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/vendor/:vendorId/settings',
    component: SalesSettings,
    showInHeader: true,
    headerName: 'Vendor Accounts',
    title: 'Vendor Settings',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/purchase-orders',
    component: Orders,
    showInHeader: true,
    headerName: 'Purchase Orders',
    title: 'Purchase Orders',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.CUSTOMER,
      UserRole.SALES,
      UserRole.BUYER,
      UserRole.WAREHOUSE,
    ],
  },
  {
    path: '/requisition',
    component: RequisitionContainer,
    showInHeader: true,
    headerName: 'Requisition Catalog',
    title: 'Requisition Catalog',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.CUSTOMER],
  },
  {
    path: '/purchase-order-reports',
    component: PurchaseOrderReports,
    showInHeader: true,
    headerName: 'Purchase Order Reports',
    title: 'Purchase Order Reports',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.CUSTOMER],
  },
  {
    path: '/order/:orderId/purchase-cart',
    component: PurchaseOverview,
    showInHeader: true,
    headerName: 'Purchase Order',
    title: 'Purchase Order Cart',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.BUYER, UserRole.WAREHOUSE],
  },
  {
    path: '/order/:orderId/purchase-details',
    component: OrderDetails,
    showInHeader: true,
    headerName: 'Purchase Order',
    title: 'Purchase Order Details',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.BUYER],
  },
  {
    path: '/order/:orderId/purchase-receiving',
    component: Receiving,
    showInHeader: true,
    headerName: 'Purchase Order',
    title: 'Purchase Order Receiving',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.BUYER],
  },
  {
    path: '/order/:orderId/purchase-related',
    component: RelatedOrders,
    showInHeader: true,
    headerName: 'Purchase Order',
    title: 'Purchase Related Orders',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.BUYER],
  },
  {
    path: '/order/:orderId/purchase-documents',
    component: PurchaseDocuments,
    showInHeader: true,
    headerName: 'Purchase Order',
    title: 'Purchase Documents',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SALES, UserRole.BUYER],
  },

  //routes for daily delivery module
  {
    path: '/delivery/deliveries',
    showInHeader: true,
    component: DeliveryContainer,
    headerName: 'Fulfillment',
    title: 'Fulfillment',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.CUSTOMER,
      UserRole.SALES,
      UserRole.WAREHOUSE,
      UserRole.SELLER_RESTRICTED,
    ],
  },

  {
    path: '/delivery/routes',
    component: DeliveryRoutings,
    showInHeader: true,
    headerName: 'Routes',
    title: 'Delivery Routes',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED],
  },

  {
    path: '/delivery/route/:routeId',
    component: DeliveryRoutingDetail,
    showInHeader: true,
    headerName: 'Routes',
    title: 'Delivery Routes',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/delivery/driver-reports',
    component: DriverReports,
    showInHeader: true,
    headerName: 'Driver Reports',
    title: 'Driver Reports',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.WAREHOUSE, UserRole.SELLER_RESTRICTED],
  },

  // Routing for Products module
  {
    path: '/inventory',
    component: Inventory,
    showInHeader: true,
    headerName: 'Products',
    title: 'Products',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.OPERATOR,
      UserRole.SALES,
      UserRole.BUYER,
      UserRole.WAREHOUSE,
    ],
  },
  {
    path: '/inventory/item/stock/:id',
    component: ItemStock,
    headerName: 'Products',
    title: 'Product Details',
    needRoles: [UserRole.OPERATOR, UserRole.ADMIN, UserRole.SUPERADMIN],
  },

  // orders related pages
  {
    path: '/orders',
    component: Orders,
    showInHeader: true,
    headerName: 'Orders',
    title: 'Orders',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/order/pricesheet/:id/customer',
    showInHeader: true,
    component: NewOrderPriceSheetContainer,
    headerName: 'Orders',
    title: 'Order PriceSheet',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/order/pricesheet/:id/customer/:customerId/new',
    component: NewOrderPriceSheetContainer,
    headerName: 'Orders',
    title: 'Order PriceSheet',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/neworder',
    component: NewOrderContainer,
    headerName: 'Orders',
    title: 'New Order',
    showInHeader: true,
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/order/:orderId/token',
    headerName: 'New Order',
    showInHeader: true,
    title: 'Order',
    component: NewTokenOrderContainer,
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/order/:orderId/:success?',
    component: OrderDetail,
    headerName: 'Orders',
    title: 'Order Success',
    showInHeader: true,
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/order/:orderId/success',
    component: OrderDetail,
    headerName: 'Order Confirmation',
    title: 'Order Success',
    showInHeader: true,
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/order/:orderId/error',
    component: OrderDetail,
    headerName: 'Orders',
    title: 'Order Error',
    showInHeader: true,
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/orders/:orderId/print',
    component: OrderPrint,
    headerName: 'Orders',
    title: 'Order Print',
    showInHeader: true,
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/order/item/stock/',
    component: NewOrderStockContainer,
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },

  // pricesheets related pages
  {
    path: '/pricesheets',
    component: PriceSheets,
    showInHeader: true,
    headerName: 'Price Sheets',
    title: 'Price Sheets',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/pricesheet/:id',
    showInHeader: true,
    component: PriceSheetDetail,
    headerName: 'Pricesheets',
    title: 'Pricesheet',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/pricesheet/:id/print',
    component: PriceSheetPrintView,
    headerName: 'Pricesheets',
    title: 'Pricesheet Print',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/myaccount/profile',
    headerName: 'My Account',
    title: 'My Account Profile',
    component: AccountProfileContainer,
    needRoles: [
      UserRole.CUSTOMER,
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.SALES,
      UserRole.BUYER,
      UserRole.WAREHOUSE,
    ],
  },
  {
    path: '/myaccount/categories',
    headerName: 'My Account',
    title: 'My Account Categories',
    component: CategorySetting,
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/myaccount/setting',
    headerName: 'My Account',
    title: 'My Company Setting',
    component: ThemeSetting,
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },

  // others
  {
    path: '/intelligence',
    component: Intelligence,
    showInHeader: true,
    headerName: 'Intelligence',
    title: 'Intelligence',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/product/:id',
    component: ProductContainer,
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.SALES,
      UserRole.BUYER,
      UserRole.WAREHOUSE,
      UserRole.SELLER_RESTRICTED,
    ],
  },
  {
    path: '/product/:id/description',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Description',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.SALES,
      UserRole.BUYER,
      UserRole.WAREHOUSE,
      UserRole.SELLER_RESTRICTED,
    ],
  },
  {
    path: '/product/:id/details',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Details',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.SALES,
      UserRole.BUYER,
      UserRole.WAREHOUSE,
      UserRole.SELLER_RESTRICTED,
    ],
  },
  {
    path: '/product/:id/activity',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Activity',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/product/:id/lots',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Lots',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/product/:id/logistics',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Logistics',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/product/:id/adjustment',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Adjustment',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/product/:id/pricing',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Pricing',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/product/:id/gallery',
    showInHeader: true,
    exact: false,
    headerName: 'Product',
    title: 'Product Gallery',
    needRoles: [
      UserRole.ADMIN,
      UserRole.SUPERADMIN,
      UserRole.SALES,
      UserRole.BUYER,
      UserRole.WAREHOUSE,
      UserRole.SELLER_RESTRICTED,
    ],
  },
  {
    path: '/prices',
    component: PriceContainer,
    showInHeader: true,
    exact: false,
    headerName: 'Price Sheets',
    title: 'Price Sheets',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/pricing/:id/default-pricing/:customerId',
    component: DefaultPricingDetail,
    showInHeader: true,
    exact: false,
    headerName: '',
    title: '',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/price/:id',
    component: PricingDetail,
    showInHeader: true,
    headerName: 'Price Sheet Detail',
    title: 'Price Sheet Detail',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/settings',
    component: Settings,
    showInHeader: true,
    headerName: 'Driver-Route Assignment',
    title: 'Settings',
    needRoles: [UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/work-orders',
    component: WorkOrdersList,
    showInHeader: true,
    headerName: 'Work Orders',
    title: 'Work Orders',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/manufacturing/:orderId/wo-overview',
    component: Overview,
    showInHeader: true,
    headerName: 'Work Orders',
    title: 'Work Orders Overview',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/manufacturing/:orderId/wo-details',
    component: WorkOrderDetails,
    showInHeader: true,
    headerName: 'Work Orders',
    title: 'Work Order Details',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/manufacturing/:orderId/audit-log',
    component: AuditLog,
    showInHeader: true,
    headerName: 'Work Orders',
    title: 'Audit Log',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/manufacturing-special',
    component: Special,
    showInHeader: true,
    headerName: 'Special Customer Requests',
    title: 'Work Orders Special Customer Requests',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/manufacturing/:orderId/wo-templates',
    component: Templates,
    showInHeader: true,
    headerName: 'Work Orders',
    title: 'Work Orders Templates',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/manufacturing-pl',
    component: ProcessingList,
    showInHeader: true,
    headerName: 'Processing List',
    title: 'Processing List',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/processing-resource',
    component: ProcessingResource,
    showInHeader: true,
    headerName: 'Processing Resource Matrix',
    title: 'Processing Resource Matrix',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/location',
    component: LocationPage,
    showInHeader: true,
    headerName: 'Warehouse Sync',
    title: 'Warehouse',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.WAREHOUSE],
  },
  {
    path: '/pallet-history',
    component: PalletHistory,
    showInHeader: true,
    headerName: 'Pallet History',
    title: 'Pallet History',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.WAREHOUSE],
  },
  {
    path: '/reports',
    component: ReportsContainer,
    showInHeader: true,
    headerName: 'Reports',
    title: 'Reports',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/reports/:filterId',
    component: ReportDetail,
    showInHeader: true,
    headerName: 'Reports',
    title: 'Reports',
    needRoles: [UserRole.CUSTOMER, UserRole.SALES, UserRole.ADMIN, UserRole.SUPERADMIN, UserRole.SELLER_RESTRICTED],
  },
  {
    path: '/label',
    component: NewLabel,
    showInHeader: true,
    headerName: 'Label',
    title: 'Label',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/restock',
    component: Restock,
    showInHeader: true,
    headerName: 'Restock',
    title: 'Restock',
    needRoles: [UserRole.CUSTOMER, UserRole.ADMIN, UserRole.SUPERADMIN],
  },
  {
    path: '/statistics',
    component: Statistics,
    showInHeader: true,
    headerName: 'Statistics',
    title: 'Statistics',
    needRoles: [UserRole.SUPERADMIN],
  },
]

export default routeConfig
