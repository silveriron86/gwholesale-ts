import React, { useEffect } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { RouteComponentProps, withRouter } from 'react-router'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Global } from '@emotion/core'
import { ThemeProvider } from 'emotion-theming'
import lodash from 'lodash'
import { useEffectModule } from 'redux-epics-decorator'

import { Header } from '~/components'
import { themes } from '~/common'
import { CACHED_AUTH_TOKEN, CACHED_ACCESSTOKEN } from '~/common'

import { AccountLoginContainer } from '~/modules/account/account-login.container'
import { AccountPasswordContainer } from '~/modules/account/password-reset.container'
import { SettingModule } from '~/modules/setting'
import AccountLogout from '~/modules/account/account-logout.container'

import { RootDispatchProps } from './root.module'
import { GlobalState } from './store/reducer'
import { globalStyle } from './global.style'

import routeConfig, { roleFilter } from './routeConfig'
import { AuthUser, UserRole } from './schema'
import { NewTokenOrderContainer } from './modules/orders/order-token-new.container'
import { OrderDetail } from './modules/orders/order-detail.container'
import { HomeProductContainer } from './modules/homepage/home-product.container'

import { PricingComponent } from './modules/homepage/pricing.container'
import { HomePageComponent } from './modules/homepage/homepage.container'
import OrderPrint from './modules/orders/order-print'
import Error404 from './components/error/404'
import { ProductDetails, ProductActivity, ProductLots, ProductGallery } from './modules/product/components'
import { TermsComponent } from './modules/homepage/terms.container'

export function RouteWithTitle({ title, ...props }) {
  if (props.computedMatch.path === '/product/:id') {
    let lastParam = props.location.pathname.replace(props.computedMatch.url, '')
    if (lastParam === '/details') {
      title = 'Product Details'
    } else if (lastParam === '/activity') {
      title = 'Product Activity'
    } else if (lastParam === '/lots') {
      title = 'Product Lots'
    } else if (lastParam === '/gallery') {
      title = 'Product Gallery'
    }
  }
  return (
    <>
      <Helmet>
        <title>{title ? `WholesaleWare - ${title}` : 'WholesaleWare'}</title>
      </Helmet>
      <Route {...props} />
    </>
  )
}

type RoutesProps = RouteComponentProps & {
  isLogined?: boolean
  currentUser: AuthUser
  theme: string
  logo: string
} & RootDispatchProps

const Application: React.SFC<RoutesProps> = (props) => {
  const [, settingAction] = useEffectModule(SettingModule, () => ({}))

  useEffect(() => {
    // get userInfo/logo/theme after logined
    let accessToken = localStorage.getItem(CACHED_ACCESSTOKEN)
    if (props?.currentUser?.isLogined == true && accessToken) {
      settingAction.getLogo()
      settingAction.getTheme()
    }
    return settingAction.dispose$ as any
  }, [])

  const availableRouteConfig = routeConfig.filter(
    roleFilter(props.currentUser ? props.currentUser.accountType : UserRole.CUSTOMER),
  )
  const headerConfig = availableRouteConfig.filter((n) => n.showInHeader)
  const isLogined = props.currentUser ? props.currentUser.isLogined : false
  const accountType = props.currentUser ? props.currentUser.accountType : UserRole.CUSTOMER
  const location = props.location

  //Hack for when is logined and in home/landing page blank
  // why add localStorage.getItem(CACHED_AUTH_TOKEN) ?
  // when session expired, props login status not cleared so quickly,
  // so if you don't add this, The browser will redirect again
  if (isLogined && localStorage.getItem(CACHED_AUTH_TOKEN) && location.pathname == '/home/landing') {
    var pathName = '/customers'
    switch (accountType) {
      case UserRole.SALES: {
        pathName = availableRouteConfig[0].path
        break
      }
      case UserRole.CUSTOMER: {
        pathName = '/sales-orders'
        break
      }
      default: {
        pathName = '/sales-orders'
      }
    }
    props.history.push(pathName)
  }
  // index route should be different with different user role
  // index route should be first in availableRouteConfig
  return (
    <React.Fragment>
      <Header
        hide={!isLogined}
        tabConfig={(location.pathname === '/home/terms') ? null : headerConfig}
        logo={props.logo}
        location={location}
        currentUser={props.currentUser}
      />
      {isLogined ? (
        <Switch>
          {accountType === UserRole.SALES ? (
            <Redirect exact from="/" to={availableRouteConfig[0].path} />
          ) : accountType === UserRole.CUSTOMER ? (
            <Redirect exact from="/" to="/sales-orders" />
          ) : (
            <Redirect exact from="/" to="/sales-orders" />
          )}
          <Route exact path="/myaccount/logout" component={AccountLogout} />
          {availableRouteConfig.map((config) => {
            const routeProps = lodash.omit(config, 'showInHeader', 'needRoles')
            return (
              <RouteWithTitle
                title={config.title}
                exact
                key={routeProps.path}
                {...routeProps}
                currentUser={props.currentUser}
              />
            )
          })}
          <Route path="/login/:success?" component={AccountLoginContainer} />
          <Route path="/home/landing" component={HomePageComponent} />
          <Route path="" component={Error404} />
        </Switch>
      ) : (
        <Switch>
          <RouteWithTitle exact path="/product" title="Product" component={HomeProductContainer} />
          <RouteWithTitle exact path="/signin" title="SignIn" component={AccountLoginContainer} />
          <Route path="/login/:success?" component={AccountLoginContainer} />
          <Route path="/setPassword/:token" component={AccountPasswordContainer} />
          <Route path="/order/:orderId/token" component={NewTokenOrderContainer} />
          <Route path="/order/:orderId/success" component={OrderDetail} />
          <Route path="/orders/:orderId/print" component={OrderPrint} />
          <Route exact path="/" component={HomePageComponent} />
          <RouteWithTitle path="/pricing" title="Pricing" component={PricingComponent} />
          <RouteWithTitle path="/home/terms" title="Terms" component={TermsComponent} />
          <Redirect to="/" />
        </Switch>
      )}
    </React.Fragment>
  )
}

const Container: React.SFC<RoutesProps> = (props) => {
  return (
    <ThemeProvider theme={themes[props.theme]}>
      <Global styles={globalStyle} />
      <Application {...props} key={props.location.pathname} />
    </ThemeProvider>
  )
}

export const Routes = withRouter(
  connect((state: GlobalState) => ({
    currentUser: state.currentUser,
    theme: state.setting.themeKey,
    logo: state.setting.logo,
  }))(Container),
)
