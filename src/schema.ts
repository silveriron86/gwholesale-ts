// import { weekdays } from "moment";

export interface Order {
  id: string
  totalCost: number
  totalPrice: number
  status: string
  deliveryDate: number
  updatedDate: number
  createdDate: number
  priceSheetId: number
  customer: {
    id: number
    name: string
  }
  qboId: number
  nsId: number
  linkToken: string
  firstName: string
  lastName: string
  company: {
    id: number
    name: string
    type: string
  }
  printDate: number
  printVersion: number
  //for work order
  wholesaleOrderId: number
  wholesaleOrderStatus: string
  relateOrderId: number
  isWorkOrder: number
  requestedTime: string
  name: string
  revisedTime: string
  startDate: string
  endDate: string
  completionDate: string
  manufactureNote: string
  isTemplate: number
  workerList: User[]
  orderItemList: OrderItem[]
  orderItems: OrderItem[]
  station: Station
  wholesaleStationSchedule: StationSchedule[]
  childOrders: any[]
  reference: string
  wholesaleClient: TempCustomer
  lastQboUpdate: string
  seller: User
  totalTax: number
  totalCostPlusTax: number
}

export interface OrderDetail {
  orderItems: OrderItem[]
  createdDate: number
  orderDate: number
  deliveryDate: number //In the PO: this is just scheduled delivery date
  updatedDate: number
  quotedDate: number
  qboId: string
  totalPrice: number
  totalCost: number
  wholesaleOrderId: string
  reference: string
  wholesaleOrderStatus: string
  wholesaleCustomerClient: any
  user: Seller
  seller: Seller
  priceSheetId: number
  wholesaleClient: TempCustomer
  linkToken: string
  linkExpireDate: number
  shippingAddress: MainAddress
  billingAddress: MainAddress
  deliveryInstruction: string
  deliveryPhone: string
  pickerNote: string
  customerNote: string
  invoiceDueDate: number
  shippingCost: number
  defaultDriver: AccountUser
  defaultRoute: WholesaleRoute
  overrideRoute: WholesaleRoute
  vendorSalesRep: string
  deliveryTime: number
  printDate: number
  printVersion: number
  parentOrder: OrderDetail
  pickup: boolean
  freightType: string
  financialTerms: string
  isLocked: boolean
  pickupAddress: string
  warehousePickupTime: string
  pickupContactName: string
  pickupPhoneNumber: string
  pickupReferenceNo: string
  carrier: string
  originAddress: string
  carrierReferenceNo: string
  deliveryContact: string
  cashSale: boolean
  nsId: string
  numberOfPallets: number
  customOrderNo: string
  fulfillmentType: number
  shippingDate: string //This field is actual delivery date in PO
  carrierPhoneNo: string
  driverName: string
  driverLicense: string
  billDate: number
  billNumber: string
  wholesaleClientId: number
  lastQboUpdate: number
  lastNsUpdate: number
  allocateType: number
  totalTax: number
}

export interface OrderItem {
  uom: any
  wholesaleItem: any
  unitsAcutal: any
  freight: number
  origin: string
  packing: string
  provider: string
  variety: string
  price: number
  organic: boolean
  cost: number
  quantity: number
  onHandQty: number
  margin: number
  weight: number
  label: string
  itemId: number
  size: string
  grade: string
  category: string
  SKU: string
  UOM: string
  baseUOM: string
  inventoryUOM: string
  status: string
  createdDate: number
  deliveryDate: number
  wholesaleOrderItemId: number
  vendorItem: VendorItem
  wholesaleItemId?: string
  note: string
  picked: number
  lotId: string
  qtyConfirmed: number
  orderWeight: number
  receivedQty: number
  palletQty: number
  constantRatio: boolean
  itemName: string
  useForSelling: boolean
  useForPurchasing: boolean
  isInvenotry: boolean
  displayOrder: number
  pricingUOM: string
  pas: boolean
  tax: number
  taxRate: number

  //for work order
  weightType: string
  type: number
  unitsActual: number
  weightActual: number
  relateOrderItemId: number
  workOrderStatus: string | null
  modifiers: string
  suppliers: string
  overrideUOM: string

  pricingLogic: string
  pricingGroup: string
  wholesaleProductUomList: WholesaleProductUom[]
  //for credit memo
  returnUom: string
  supplierSkus: string
  editedItemName: string
  grossWeight: number
  grossVolume: number
  defaultGrossWeight: number
  defaultGrossVolume: number
  grossWeightUnit: string
  grossVolumeUnit: string
  lotAssignmentMethod: number
  enableLotOverflow: boolean
  displayOrderProduct: number
  taxEnabled: boolean
}

export interface WholesaleProductUom {
  id: number
  name: string
  ratio: number
  type: number
  accessToken: String
  constantRatio: number
  createdAt: string
  updatedAt: string
  deletedAt: string
  useForInventory: boolean
  useForSelling: boolean
  useForPurchasing: boolean
  priceFactor: number
  costFactor: number
  nsId: string
}

// interface for login response
export interface AuthUser {
  userId: string
  tokenId: string
  accountType: UserRole
  company: string
  accessToken: String

  message?: string
  redirectLink?: string
  customer?: string

  // extend from local
  isLogined?: boolean

  //expected field
  fullName: string
}

export interface AccountUser {
  id: number
  userId?: string
  emailAddress: string
  firstName: string
  lastName: string
  phone: string
  status: string
  addressId: string
  company: string
  password: string
  imagePath: string
  role: string
}

export interface Address {
  department: string
  addressId: number
  street1: string
  street2: string
  city: string
  state: string
  country: string
  zipcode: string
  createdDate: number
  updatedDate: number
  longitude: number
  latitude: number
  name: string
  whoelsaleAddressId: number
  nsId: string
}

export interface Chat {
  id: number
  user: AccountUser
  client: TempCustomer
  content: string
  type: number
  createdDate: number
  updatedDate: number
}

export interface Document {
  id: number
  wholesaleClient: TempCustomer
  type: string
  title: string
  notes: string
  url: string
  createdDate: number
  updatedDate: number
}

export interface Vendor {
  vendorId: number
  name: string
  emailAddress: string
  status: string
  address: Address
}

export interface VendorItem {
  vendorItemId: number
  vendor: Vendor
  saleItem: SaleItem
  unitName: string
  unitCost: number
  quantityPerUnit: number
  quantity: number
}

export enum CustomerStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
  DELETE = 'delete',
}

export enum CustomerType {
  RESTAURANT = 'RESTAURANT',
  WHOLESALER = 'WHOLESALER',
  GROCERY = 'GROCERY',
  INDIVIDUAL = 'INDIVIDUAL',
  FARMER = 'FARMER',
  DISTRIBUTOR = 'DISTRIBUTOR',
}

export interface Customer {
  type: CustomerType
  id: string
  company: string
  name: string
  phone: string
  email: string
  address: Address
  status: CustomerStatus
  qboId: string
  seller?: AccountUser
  user?: AccountUser
  fullName?: string
  customerId?: number
}

export interface Company {
  companyId: number
  companyName: string
  logoUrl: string
  phone: string
  fax: string
  email: string
  website: string
  bestByDate: number
  address: Address
  targetFulfillmentDate: string
  cutOffTime: string
  createdDate: number
  updatedDate: number
  salesPrefix: string
  purchasePrefix: string
  isDisablePickingStep: boolean
  isEnableCashSales: boolean
  isEnableDebitMemo: boolean
  isAutomatically: boolean
  defaultCostType: number
  customShippedLabel: string
  itemDescriptionFormat: string
  hideMenuLabel: boolean
  hideMenuRequisitionCat: boolean
  visibleCustomOrderNo: boolean
  purchaseTerms: string
  showPriceSheetSize: boolean
  showPriceSheetWeight: boolean
  showPriceSheetPacking: boolean
  showPriceSheetBrand: boolean
  showPriceSheetOrigin: boolean
  duplicatedOrderPriceStrategy: number
  warehousePickEnabled: boolean
  warehouseReceivingEnabled: boolean
  logisticUnit: string
  locationMethod: number
  defaultCreditMemoItemIds: string // credit memo item ids that is joined by ;
  defaultLotAssignMethod: number
  defaultOverflowEnabled: boolean
}

export interface WholesaleCompanySetting {
  id: number
  companyId: number
  name: string
  type: string
}

export interface Seller {
  userId: number
  emailAddress: string
  secondaryEmail?: string
  firstName: string
  lastName: string
  phone: string
  secondaryPhone?: string
  oldPassword?: string
  newPassword?: string
  retypePassword?: string
  status: string
  imagePath?: string
  createdDate: number
  updatedDate: number
  isAdmin: number
  invoiceEnabled: boolean
  isTemp: number
  myRegion: number
  landingPage: boolean
  vipUser: boolean
  noMoreOpsError: boolean
  goldVip: boolean
  customerNote?: string
  address?: string
  referralUser?: string
  salesRepUser?: string
  stripeCustomerId?: number
  company: string
  application: string
}

export interface DeliveryRoute {}
export interface DeliveryRoute {}

export interface MainAddress {
  wholesaleAddressId: number
  wholesaleCompany: Company
  address: Address
  phone: string
  createdDate: number
  updatedDate: number
  addressType: string
  deliveryWindows: string
  deliveryRoute: string
  isMain: boolean
  wholesaleRouteDetail: WholesaleRouteDetail
}

export interface MainContact {
  contactId?: number
  id?: number
  user: number | null
  wholesaleCompany: Company
  name: string
  title: string
  fax: string
  salutation: string
  phone: string
  mobilePhone: string
  email: string
  notes: string
  clientId: number
  createdDate: number
  updatedDate: number
  userRole: string | null
  isMain: boolean
  wholesaleClient?: {
    clientId: number
  }
}

export interface TempCustomer {
  clientId: number
  status: string
  mainAddress: MainAddress
  wholesaleCompany: Company
  clientCompany: Company
  website: string
  fax: string
  mobilePhone: string
  alternativePhone: string
  email: string
  notes: string
  mainContact: MainContact
  createdDate: number
  updatedDate: number
  qboId: string
  nsId: string
  seller: Seller
  accountant: Seller
  manager: Seller
  purchaser: Seller
  openBalance: number
  overdue: number
  creditTerm: string
  creditLimit: number
  type: string
  dba: string
  openUpdatedAt: number
  overdueUpdatedAt: number
  lastQBOUpdate: number
  creditRating: string
  servicesProvided: string
  creditMonthlyInterestRate: number
  businessType: string
  defaultLogic: string
  defaultGroup: string
  paymentTerm: string
  displayPriceSheetPrice: number
  abbreviation: string
  margin: number
  sharedClientId: number
  costMethod: number
  taxable: boolean
  autoSyncProducts: boolean
}

export interface BriefClient {
  clientId: number
  status: string
  wholesaleCompanyId: number
  clientCompanyId: number
  clientCompanyName: string
  type: string
  businessType: CustomerType
  mainContactName: string
  mainContactEmail: string
  mainBillingAddress: MainAddress
  mobilePhone: string
}

export interface QBOAccount {
  type: CustomerType
  company: string
  name: string
  phone: string
  email: string
  address: Address
  status: string
  qboId: string
  billingAddress: Address
  shippingAddress: Address
  mobilePhone: string
  fax: string
}

export interface NSAccount {
  entity: string
  type: CustomerType
  company: string
  name: string
  phone: string
  email: string
  address: Address
  status: string
  nsId: string
  billingAddress: Address
  shippingAddress: Address
  mobilePhone: string
  fax: string
}

export interface NSOrderImport {
  orderId: string
  client: string
}

export interface TempOrder {
  createdDate: number
  totalPrice: number
  wholesaleOrderId: number
  fullName: string
  wholesaleCustomerClientId: number
  updatedDate: number
  deliveryDate: number
  linkToken: string
  totalCost: number
  qboId: number
  status: string
}

export interface User {
  userId: string
  qboRealmId: string
  qboAccessToken: string
  qboRefreshToken: string
  qboEmail: string
  lastUpdate: string
  company: string
  logoURL: string
  colorTheme: string
  firstName?: string
  lastName?: string
}

export interface CustomerPriceSheet {
  priceSheetId: string
  firstName: string
  lastName: string
  company: string
  emailAddress: string
  phone: string
  customerId: string
  name: string
  priceSheetClientId: string
  linkToken: string
}

export interface PriceSheet {
  priceSheetId: string
  createdDate: string
  updatedDate: string
  name: string
  user: User
  companyShared: boolean
  owner: boolean
  lastOrder: Order
  itemCount: number
  assignedCount: number
  assignedClients: TempCustomer[]
  isDefault: boolean
  isShared: boolean
  isClientList: boolean
}

export interface PriceSheetItem {
  priceSheetItemId: string
  availableQuantity: number
  cost: number
  margin: number
  markup: number
  salePrice: number
  freight: number
  origin: string
  stockEnabled: boolean
  weight: string
  label: string
  packing: string
  itemId: string
  isListed: boolean
  createdDate: number
  size: string
  provider: string
  variety: string
  grade: string
  wholesaleCategory: SaleCategory
  category: string
  SKU: string
  organic: boolean
  orderQuantity?: number
  isAnchor?: boolean
  priceSheetName: string
  priceSheetId: string
  wholesaleItem?: SaleItem
  modifier: string
  note: string
}

export interface TokenPriceSheet {
  wholesaleCustomerClient: any
  priceSheet: PriceSheet
  priceSheetItemList: PriceSheetItem[]
  sellerSetting: any
}

export interface PriceSheetItemNew {
  priceSheetItemId: string
  priceSheet: PriceSheet
  freight: number
  margin: number
  salePrice: number
  wholesaleItem: SaleItem
  createdDate: string
  updatedDate: string
}

export interface SaleItem {
  class: any
  itemId: string
  weight: number
  cost: number
  quantity: number
  provider: string
  variety: string
  size: string
  origin: string
  packing: string
  grade: string
  label: string
  warehouse: string
  SKU: string
  wholesaleCategory: SaleCategory
  qboId: string
  nsId: string
  nsCategoryId: number
  baseUOM: string
  inventoryUOM: string
  stock: number
  stockEnabled: boolean
  organic: boolean
  isListed: boolean
  createdDate: string
  updatedDate: string
  lastQBOUpdate: string
  isAnchor?: boolean
  wholesaleItemId: string
  orderQuantity?: number
  wholesaleCategoryId?: string
  price: number
  isOverridePrice: boolean
  cover?: string
  //add saleprice in saleitem
  salePrice?: number
  margin?: number
  freight?: number
  status: boolean
  marginA: number
  markupA: number
  lowA: number
  highA: number
  marginB: number
  markupB: number
  lowB: number
  highB: number
  marginC: number
  markupC: number
  lowC: number
  highC: number
  marginD: number
  markupD: number
  lowD: number
  highD: number
  marginE: number
  markupE: number
  lowE: number
  highE: number
  suppliers: string
  wholesaleProductUomList: WholesaleProductUom[]
}

export interface PriceSheetClient {
  customerId: string
  fullName: string
  emailAddress: string
  linkToken: string
  priceSheetClientId: string
}

export interface SaleCategoryBase {
  wholesaleCategoryId: string
  name: string
}

export interface SaleCategory extends SaleCategoryBase {
  warehouse: string
  wholesaleSection: SaleSection
  isDefault: boolean
}

export interface SaleSection {
  wholesaleSectionId: string
  name: string
  warehouse: string
  qboId: string
  categories?: SaleCategory[]
  isDefault: boolean
}

export enum UserRole {
  CUSTOMER = 'customer',
  SALES = 'seller',
  OPERATOR = 'operator',
  BUYER = 'buyer',
  ADMIN = 'admin',
  SUPERADMIN = 'superadmin',
  WAREHOUSE = 'warehouse',
  SELLER_RESTRICTED='seller_restricted'
}

export interface WholesaleRoute {
  id: number
  routeName: string
  activeDays: string
  activeTime: string
  areas: string
  assigned: boolean
  company: Company
}

export interface WholesaleRouteDetail {
  id: number
  route: WholesaleRoute
  wholesaleClient: TempCustomer
  address: WholesaleAddress
  availableAddresses: Address[]
}

export interface WholesaleAddress {
  wholesaleAddressId: number
  wholesaleCompany: Company
  address: Address
  phone: string
  createdDate: string
  updatedDate: string
  department: string
  addressType: AddressType
  deliveryWindows: string
  isMain: boolean
  deletedAt: string
}

export interface WholesaleDelivery {
  driver: AccountUser
  routes: WholesaleRoute[]
  completedOrders: number
  totalOrders: number
  totalUnits: number
  totalPicks: number
  orders: OrderDTO[]
}

export interface WorkOrder {
  id: string
  name: string
  isTemplate: number
  status: number
  notes: string
  revisedTime: number
  completionTime: number
  startTime: number
  endTime: number
  requestedTime: number
  processor: User[]
}

export interface WorkOrderItem {
  id: string
  wholesaleItem: SaleItem
  type: number
  planQty: number
  actualQty: number
  actualWeight: number
  description: string
  instructions: string
  uom: string
  weightType: string
}

export interface factory {
  id: string
  name: string
  company: Company
}

export interface station {
  id: string
  name: string
  type: string
  factoryId: string
}

export interface WorkOrderItemSchedule {
  id: string
  type: number
  stationId: string
  workOrderItemId: string
}

export interface WorkOrderAssign {
  id: string
  user: User
  workOrderId: string
}

export interface OrderDTO {
  customerId: number
  wholesaleOrderId: number
  customer: string
  versionNumber: number
  sumOfUnits: number
  status: string
  linkToken: string
  deliveryDestination: string
  defaultDriverId: number
  defaultRouteId: number
  overrideDriverId: number
  overrideRouteId: number
  hasWorkOrder: boolean
}

export interface WholesalePayload {
  id: number
  driver: AccountUser
  deliveryWeekOfDay: string
  wholesaleRoute: WholesaleRoute
  scheduleOrder: string
}

export interface Vendor {
  clientId: number
  status: string
  mainAddress: MainAddress
  wholesaleCompany: Company
  clientCompany: Company
  website: string
  fax: string
  mobilePhone: string
  notes: string
  mainContact: MainContact
  createdDate: number
  updatedDate: number
  qboI: string
  seller: Seller
  openBalance: number
  overdue: number
  creditTerm: string
  creditLimit: number
  type: string
  openUpdatedAt: string
  overdueUpdatedAt?: string
}

export interface DateRange {
  from?: string
  to?: string
}

export interface Pallet {
  id: number
  palletUnits: number
  companyName: string
  bestByDate: number
}

export enum AddressType {
  BILLING = 'BILLING',
  SHIPPING = 'SHIPPING',
}

export const WEEKDAYS: string[] = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']

export interface WholesaleLabel {
  id: number
  name: string
  content: string | null
  createdAt: string
  wholesaleCompany: Company
  createdBy: AuthUser
  additionalInformationEnabled: boolean
  companyInformation: string | null
  companyInformationEnabled: boolean
  editedProductName: null | string
  isActive: boolean
  labelType: number
  logoEnabled: boolean
  notes: null | string
  originUsaEnabled: boolean
  poNumberEnabled: boolean
  sellByDateEnabled: boolean
}

// Manufacturing
export interface StationDetail {
  id: number
  wholesaleStation: Station
  user: User
  createdAt: string
  updatedAt: string
  deletedAt: string
}

export interface Station {
  id: number
  name: string
  wholesaleCompany: Company
  createdAt: string
  updatedAt: string
  deletedAt: string
  wholesaleStationSettingList: StationSetting[]
  WholesaleStationDetailList: StationDetail[]
}

export interface StationSetting {
  id: number
  wholesaleStation: Station
  wholesaleManufactureType: ManufactureType
  displayOrder: number
  createdAt: string
  updatedAt: string
  deletedAt: string
}

export interface StationSchedule {
  id: number
  wholesaleOrder: Order
  wholesaleStationDetail: StationDetail
  wholesaleStationSetting: StationSetting
  wholesaleStation: Station
  createdAt: string
  updatedAt: string
  deletedAt: string
}

export interface ManufactureType {
  id: number
  name: string
  createdAt: number
  updatedAt: number
  deletedAt: number
  wholesaleStationSetting: StationSetting[]
}

export interface WholesaleSalesOrderPalletLabel {
  id: number
  palletNumber: string
  palletUnits: number
  wholesaleOrder: OrderDetail
  productIds: string
  displayNumber: number
}

export enum AvailableStatus {
  REQUEST,
  RECEIVED,
  WIP,
  COMPLETE,
  APPROVED,
}

export enum ALLOCATION_TYPE {
  BILLABLE = 1,
  PALLETS,
  MANUALLY,
}
