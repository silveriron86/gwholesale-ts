import { combineModuleEpics, combineEpics } from 'redux-epics-decorator'

import { CustomersModule } from '~/modules/customers'
import { VendorsModule } from '~/modules/vendors'
import { InventoryModule } from '~/modules/inventory'
import { ProductModule } from '~/modules/product'
import { PriceSheetModule } from '~/modules/pricesheet'
import { PricingModule } from '~/modules/pricing'
import { OrdersModule } from '~/modules/orders'
import { AccountModule } from '~/modules/account'
import { SettingModule } from '~/modules/setting'
import { HomePageModule } from '~/modules/homepage'
import { globalDataEpic } from '../root.module'
import { IntelligenceModule } from '~/modules/intelligence'
import { DeliveryModule } from '~/modules/delivery'
import { SettingsModule } from '~/modules/settings';
import { ManufacturingModule } from '~/modules/manufacturing';
import { LocationModule } from '~/modules/location/location.module'
import { ReportsModule } from '~/modules/reports'
import {RestockModule} from "~/modules/restock/restock.module";
import { StatisticsModule } from '~/modules/statistics/statistics.module'

export const rootEpic = combineEpics(
  globalDataEpic,
  combineModuleEpics(
    CustomersModule,
    VendorsModule,
    InventoryModule,
    ProductModule,
    OrdersModule,
    PriceSheetModule,
    AccountModule,
    SettingModule,
    HomePageModule,
    IntelligenceModule,
    DeliveryModule,
    SettingsModule,
    ManufacturingModule,
    PricingModule,
    LocationModule,
    ReportsModule,
    RestockModule,
    StatisticsModule
  ),
)
