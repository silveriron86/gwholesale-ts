import { createHashHistory } from 'history'

let basename = '/'

if (!__DEV__) {
  basename = '/'
}

export const history = createHashHistory({
  basename: basename,
})
