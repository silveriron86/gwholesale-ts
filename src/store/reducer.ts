import { combineReducers } from 'redux'
import { combineModuleReducers } from 'redux-epics-decorator'
import { RouterState, connectRouter } from 'connected-react-router'

import { TempCustomersStateProps, CustomersModule } from '~/modules/customers'
import { InventoryStateProps, InventoryModule } from '~/modules/inventory'
import { OrdersStateProps, OrdersModule } from '~/modules/orders'
import { PriceSheetStateProps, PriceSheetModule } from '~/modules/pricesheet'
import { PricingStateProps, PricingModule } from '~/modules/pricing'
import { IntelligenceStateProps, IntelligenceModule } from '~/modules/intelligence'
import { AccountStateProps, AccountModule } from '~/modules/account'
import { SettingStateProps, SettingModule } from '~/modules/setting'
import { HomePageStateProps, HomePageModule } from '~/modules/homepage'
import { DailyDeliveryProps, DeliveryModule } from '~/modules/delivery'
import { VendorsStateProps, VendorsModule } from '~/modules/vendors'
import { ManufacturingStateProps, ManufacturingModule } from '~/modules/manufacturing'
import { SettingsProps, SettingsModule } from '~/modules/settings'
import { ProductModule } from '~/modules/product'
import { globalDataReducer } from '../root.module'
import { history } from './history'
import { AuthUser } from '~/schema'
import { LocationModule, LocationStateProps } from '~/modules/location/location.module'
import { ReportsModule, ReportsStateProps } from '~/modules/reports'
import { RestockModule, RestockStateProps } from '~/modules/restock/restock.module'
import { StatisticsModule, StatisticsStateProps } from '~/modules/statistics/statistics.module'
import { PointOfSaleModule, PointOfSaleStateProps } from '~/modules/pointOfSale/pointOfSale.module'

export interface GlobalState {
  vendors: VendorsStateProps
  router: RouterState
  product: any
  customers: TempCustomersStateProps
  priceSheet: PriceSheetStateProps
  pricing: PricingStateProps
  inventory: InventoryStateProps
  intelligence: IntelligenceStateProps
  orders: OrdersStateProps
  account: AccountStateProps
  currentUser: AuthUser
  setting: SettingStateProps
  homepage: HomePageStateProps
  delivery: DailyDeliveryProps
  settings: SettingsProps
  manufacturing: ManufacturingStateProps
  location: LocationStateProps
  reports: ReportsStateProps
  restock: RestockStateProps
  statistics: StatisticsStateProps
  pointOfSale: PointOfSaleStateProps
}

export const rootReducer = (state: any, action: any) => {
  if (action.type === 'USER_LOGOUT_SUCCESS') {
    const { router } = state
    state = { router }
  }
  return appReducer(state, action)
}

export const appReducer = combineReducers({
  ...combineModuleReducers({
    priceSheet: PriceSheetModule,
    pricing: PricingModule,
    customers: CustomersModule,
    vendors: VendorsModule,
    inventory: InventoryModule,
    intelligence: IntelligenceModule,
    orders: OrdersModule,
    account: AccountModule,
    setting: SettingModule,
    homepage: HomePageModule,
    product: ProductModule,
    delivery: DeliveryModule,
    settings: SettingsModule,
    manufacturing: ManufacturingModule,
    location: LocationModule,
    reports: ReportsModule,
    restock: RestockModule,
    statistics: StatisticsModule,
    pointOfSale: PointOfSaleModule,
  }),
  currentUser: globalDataReducer,
  router: connectRouter(history),
})
