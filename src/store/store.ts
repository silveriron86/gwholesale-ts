import { createStore, compose, applyMiddleware, Store } from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import { routerMiddleware } from 'connected-react-router'
import { identity } from 'lodash'
import { catchError, takeUntil } from 'rxjs/operators'
import { empty, Subject } from 'rxjs'

import { rootReducer, GlobalState } from './reducer'
import { rootEpic } from './epics'
import { history } from './history'

export const configureStore = () => {
  const epicMiddleware = createEpicMiddleware()

  if (__DEV__) {
    // tslint:disable-next-line:prefer-const no-var-keyword
    var hot$ = new Subject()
  }

  const store: Store<GlobalState> = createStore(
    rootReducer,
    compose(
      applyMiddleware(routerMiddleware(history), epicMiddleware),
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION__({ name: 'grub' }) : identity,
    ),
  ) as any

  const runEpic = (epic: Function) => (...args: any[]) => {
    const action$ = epic(...args).pipe(
      catchError((e) => {
        console.error(e)
        return empty()
      }),
    )
    if (__DEV__) {
      return action$.pipe(takeUntil(hot$))
    }
    return action$
  }

  epicMiddleware.run(runEpic(rootEpic))

  if (__DEV__ && module.hot) {
    module.hot.accept('./reducer', () => {
      store.replaceReducer(require('./reducer').rootReducer)
    })
    module.hot.accept('./epics', () => {
      hot$.next()
      const nextRootEpic = require('./epics').rootEpic
      epicMiddleware.run(runEpic(nextRootEpic))
    })
  }

  return { store, history }
}
