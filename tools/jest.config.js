module.exports = {
  browser: false,
  verbose: true,
  rootDir: process.cwd(),
  roots: [`<rootDir>/src`],
  setupTestFrameworkScriptFile: '<rootDir>/tools/test-setup.js',
  moduleDirectories: ['<rootDir>/node_modules', '<rootDir>/src'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
  moduleNameMapper: {
    '~/(.*)$': `<rootDir>/src/$1`,
    '\\.(svg|png|jpg)': '<rootDir>/__mocks__/assets-mock.js'
  },
  transform: {
    '^.+\\.(t|j)sx?$': '<rootDir>/tools/preprocessor.js'
  },
  testRegex: '/__test__/.*\\.spec\\.(j|t)sx?$',
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.{ts,tsx}',
    '!**/*.d.ts',
    '!**/*.style.{ts,tsx}',
    '!**/node_modules/**',
    '!**/dist/**',
    '!**/tools/**'
  ],
  coverageReporters: ['lcov', 'html']
}
