const ts = require('typescript')
const path = require('path')

const tsConfig = require(path.join(process.cwd(), 'tsconfig.json'))

module.exports = {
  process (src, path) {
    if (path.endsWith('.ts') || path.endsWith('.tsx')) {
      return ts.transpile(
        src,
        Object.assign(tsConfig.compilerOptions, {
          module: 'commonjs',
          esModuleInterop: true
        }),
        path,
        []
      )
    }
    return src
  }
}
