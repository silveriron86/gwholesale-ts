const { configure } = require('enzyme')
const Adapter = require('enzyme-adapter-react-16')

require('reflect-metadata')

global.__DEV__ = true
global.__AUTHORIZATION__ = ''

configure({ adapter: new Adapter() })

process.on('unhandledRejection', (reason) => {
  console.error(reason)
})
