const lessToJs = require('less-vars-to-js')
const fs = require('fs')
const path = require('path')

const themeLess = fs.readFileSync(path.join(__dirname, '/theme.less'), 'utf8')

const themeObj = lessToJs(themeLess, {
  resolveVariables: true,
  stripPrefix: true
})

module.exports = themeObj
