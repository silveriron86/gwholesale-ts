const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CircularDependencyPlugin = require('circular-dependency-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const HashOutput = require('webpack-plugin-hash-output')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')
const shell = require('shelljs')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const common = require('./webpack.common')
const themeConfig = require('./theme')

const COMMIT_SHA = shell.exec('git rev-parse --short HEAD').stdout.trim()
const PKG_VERSION = require('../package.json').version

module.exports = merge(common, {
  mode: 'production',

  output: {
    filename: `[name].[chunkhash:8].js`,
    publicPath: '/',
    chunkFilename: `[name].[chunkhash:8].js`
  },

  performance: {
    hints: 'warning'
  },

  devtool: 'hidden-nosources-source-map',

  optimization: {
    minimize: true,
    minimizer: [
      {
        apply: (compiler) => {
          new TerserPlugin({
            cache: true,
            parallel: true,
            sourceMap: true,
            extractComments: true,
            terserOptions: {
              compress: false
            }
          }).apply(compiler)
        }
      }
    ],
    runtimeChunk: true,
    splitChunks: {
      chunks: 'all',
      maxAsyncRequests: 20,
      maxInitialRequests: 20,
      cacheGroups: {
        vendors: {
          name: 'vendors',
          test: (module) => module.nameForCondition && /[\\/]node_modules[\\/]/.test(module.nameForCondition())
        },
        styles: {
          name: 'styles',
          test: (module) =>
            module.nameForCondition && /\.css$/.test(module.nameForCondition()) && !/^javascript/.test(module.type),
          chunks: 'all',
          minSize: 0,
          minChunks: 1,
          reuseExistingChunk: true,
          priority: 100
        }
      }
    }
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              modules: false,
              import: true,
              importLoaders: 2
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: path.join(__dirname, 'postcss.config.js')
              }
            }
          }
        ]
      },
      {
        test: /\.less/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              modules: false,
              import: true,
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: path.join(__dirname, 'postcss.config.js')
              }
            }
          },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: themeConfig,
              sourceMap: false
            }
          }
        ]
      }
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: `[name].[chunkhash:8].css`
    }),

    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"',
      __DEV__: false,
      __COMMIT_SHA__: `"${COMMIT_SHA}"`,
      __PKG_VERSION__: `"${PKG_VERSION}"`,

      //STAGING
      'process.env.WSW2_HOST': '"http://wholesaleware-dev-wsw2.us-east-2.elasticbeanstalk.com"',
      'process.env.GATEWAY_HOST': '"http://wholesaleware-dev-gateway.us-east-2.elasticbeanstalk.com"',
      'process.env.CLIENT_ID': '"wsw"',
      'process.env.CLIENT_SECRET': '"oauth2"',
      'process.env.AWS_PRIVATE': '"http://dbjln6thcszd9.cloudfront.net"',
      'process.env.AWS_PUBLIC': '"http://d1f7aeckfftyhw.cloudfront.net"',

    }),

    new CircularDependencyPlugin({
      exclude: /node_modules/,
      failOnError: true
    }),

    new HashOutput(),

    new webpack.HashedModuleIdsPlugin(),

    new MomentLocalesPlugin({
      localesToKeep: ['en']
    }),

    new HtmlWebpackPlugin({
      commitSHA: COMMIT_SHA,
      version: PKG_VERSION,
      PUBLIC_URL: './',
      template: path.join(process.cwd(), 'public', 'index.html'),
      filename: 'index.html',
      BUILD_PATH: '',
      timestamp: new Date().getTime()
    }),

    new CopyWebpackPlugin([
      { from: 'public/base.css', to: './' }
    ]),

    ...(!process.env.ANALYZER ? [] : [new BundleAnalyzerPlugin()])
  ]
})
