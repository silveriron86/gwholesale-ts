const os = require('os')
const { join } = require('path')
const HappyPack = require('happypack')
const svgoConfig = require('./svgo.config')
const IgnoreNotFoundExportPlugin = require('./ignore-not-found-export-plugin')

const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length - 1 })

/**
 * @type {import("webpack").Configuration}
 */
module.exports = {
  context: process.cwd(),

  entry: './src/app.tsx',

  output: {
    path: join(process.cwd(), 'dist'),
    chunkFilename: '[name].[hash].js'
  },

  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    modules: ['node_modules'],
    alias: {
      '~': join(process.cwd(), 'src')
    }
  },

  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        loader: 'happypack/loader?id=sourcemap',
        enforce: 'pre',
        include: [join(process.cwd(), 'src')]
      },
      {
        test: /\.tsx?$/,
        use: 'happypack/loader?id=ts',
        exclude: /node_modules/
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              svgoConfig
            }
          },
          {
            loader: 'svgo-loader',
            options: svgoConfig
          }
        ]
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        }
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: "file-loader"
      },
    ]
  },

  plugins: [
    new HappyPack({
      id: 'ts',
      loaders: [
        {
          loader: 'ts-loader',
          options: {
            compiler: join(process.cwd(), 'tools', 'ts.js'),
            transpileOnly: true,
            happyPackMode: true,
            configFile: join(process.cwd(), 'tsconfig.json')
          }
        }
      ],
      threadPool: happyThreadPool
    }),

    new HappyPack({
      id: 'sourcemap',
      loaders: ['source-map-loader'],
      threadPool: happyThreadPool
    }),

    new IgnoreNotFoundExportPlugin()
  ],
  node: {fs: 'empty'},
}
