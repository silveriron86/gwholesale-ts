const { join } = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const common = require('./webpack.common')
const themeConfig = require('./theme')

module.exports = merge(common, {
  mode: 'development',

  devtool: 'cheap-module-source-map',

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: false,
            },
          },
        ],
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: false,
            },
          },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: themeConfig,
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },

  devServer: {
    port: 8081,
    publicPath: common.output.publicPath,
    historyApiFallback: true,
    hot: true,
    proxy: {
      // '/api': {
      //   target: process.env.MOCK ? 'http://127.0.0.1:7002' : 'http://wangq.nat100.top/grubmarket',
      //   // target: process.env.MOCK ? 'http://127.0.0.1:7002' : 'http://localhost:8080/grubmarket',
      //   changeOrigin: true,
      // },
      // '/oauth': {
      //   // target: 'http://localhost:9000/wsw-auth',
      //   target: 'http://wsw-dev-gateway.us-east-2.elasticbeanstalk.com/wsw-auth',
      //   changeOrigin: true,
      // },
      // '/user/password': {
      //   // target: 'http://localhost:9000/wsw-hr/api',
      //   target: 'http://wsw-dev-gateway.us-east-2.elasticbeanstalk.com/wsw-hr/api',
      //   changeOrigin: true,
      // },
      // '/auth/reset-password': {
      //   // target: 'http://localhost:9000/wsw-hr/api',
      //   target: 'http://wsw-dev-gateway.us-east-2.elasticbeanstalk.com/wsw-hr/api',
      //   changeOrigin: true,
      // },
    },
  },

  optimization: {
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false,
    minimize: false,
  },

  plugins: [
    new webpack.NamedModulesPlugin(),

    new webpack.HotModuleReplacementPlugin(),

    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"',
      __DEV__: true,

      // __AUTHORIZATION__: '"7mYgcZXT1OcjjIWOf5UCisraFzexytTC"'
      __AUTHORIZATION__: '"7mYgcZXT1OcjjIWOf5UCisraFzexytTC"',
      'process.env.WSW2_HOST': '"http://wsw-dev-wsw2.us-east-2.elasticbeanstalk.com"',
      'process.env.GATEWAY_HOST': '"http://wsw-dev-gateway.us-east-2.elasticbeanstalk.com"',
      'process.env.CLIENT_ID': '"wsw"',
      'process.env.CLIENT_SECRET': '"oauth2"',
      'process.env.AWS_PRIVATE': '"http://d1i5d4tau1jjns.cloudfront.net"',
      'process.env.AWS_PUBLIC': '"http://dh99yd9ydsyt1.cloudfront.net"',
    }),

    new HtmlWebpackPlugin({
      template: join(process.cwd(), 'public', 'index.html'),
      filename: 'index.html',
      PUBLIC_URL: './',
      BUILD_PATH: '/public',
      timestamp: new Date().getTime(),
    }),

    new ForkTsCheckerWebpackPlugin({
      tsconfig: join(process.cwd(), 'tsconfig.json'),
    }),
  ],
})
