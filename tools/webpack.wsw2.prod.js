const { join } = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const common = require('./webpack.common')
const themeConfig = require('./theme')

module.exports = merge(common, {
  mode: 'development',

  devtool: 'cheap-module-source-map',

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: false,
            },
          },
        ],
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: false,
            },
          },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: themeConfig,
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },

  devServer: {
    port: 8081,
    publicPath: common.output.publicPath,
    historyApiFallback: true,
    hot: true,
    proxy: {
      // '/api': {
      //   target: process.env.MOCK ? 'http://127.0.0.1:7002' : 'http://wholesaleware-prod-wsw2.us-west-1.elasticbeanstalk.com',
      //   // target: process.env.MOCK ? 'http://127.0.0.1:7002' : 'http://localhost:8080/grubmarket',
      //   changeOrigin: true,
      // },
      // '/oauth': {
      //   // target: 'http://localhost:9000/wsw-auth',
      //   target: 'https://aut.wholesaleware.com/wsw-auth',
      //   changeOrigin: true,
      // },
      // '/user/password': {
      //   // target: 'http://localhost:9000/wsw-hr/api',
      //   target: 'https://aut.wholesaleware.com/wsw-hr/api',
      //   changeOrigin: true,
      // },
      // '/auth/reset-password': {
      //   // target: 'http://localhost:9000/wsw-hr/api',
      //   target: 'https://aut.wholesaleware.com/wsw-hr/api',
      //   changeOrigin: true,
      // },
    },
  },

  optimization: {
    removeAvailableModules: false,
    removeEmptyChunks: false,
    splitChunks: false,
    minimize: false,
  },

  plugins: [
    new webpack.NamedModulesPlugin(),

    new webpack.HotModuleReplacementPlugin(),

    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"',
      __DEV__: true,

      // __AUTHORIZATION__: '"7mYgcZXT1OcjjIWOf5UCisraFzexytTC"'
      __AUTHORIZATION__: '"7mYgcZXT1OcjjIWOf5UCisraFzexytTC"',
      'process.env.WSW2_HOST': '"http://wholesaleware-prod-wsw2.us-west-1.elasticbeanstalk.com"',
      'process.env.GATEWAY_HOST': '"https://aut.wholesaleware.com"',
      'process.env.CLIENT_ID': '"wsw"',
      'process.env.CLIENT_SECRET': '"oauth2"',
      'process.env.AWS_PRIVATE': '"https://d1wkky6g2l0f4m.cloudfront.net"',
      'process.env.AWS_PUBLIC': '"https://d2pa4uyr6dz0nu.cloudfront.net"',
    }),

    new HtmlWebpackPlugin({
      template: join(process.cwd(), 'public', 'index.html'),
      filename: 'index.html',
      PUBLIC_URL: './',
      BUILD_PATH: '/public',
      timestamp: new Date().getTime(),
    }),

    new ForkTsCheckerWebpackPlugin({
      tsconfig: join(process.cwd(), 'tsconfig.json'),
    }),
  ],
})
