
  
# Wroking with docker   
 - [docker install](https://docs.docker.com/install/)    
     
 - [docker compose install](https://docs.docker.com/compose/install/)    
     
 - [kitematic](https://kitematic.com/)  
  
    
## mysql    
    
   
 docker network create grubmarket   
 docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -d --network grubmarket -p 3306:3306 mysql:5.7.29   
open kitematic just like    
mysql is working ~     
you will set mysql user / set privilege as usual    
![KNGXqr](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/KNGXqr.png)    
    
## nacos   
new database ( naocos )in mysql    
use naocos.sql  Initialize database befor run docker container    
@  /**/wholesaleware/docker/nacos/nacos.sql    
    
![80MDxg](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/80MDxg.png)    
    
user mysql.env config nacos server    
  
find env file url like:    
  
@ /**/wholesaleware/docker/nacos/mysql.env    
    
  
   
  
 PREFER_HOST_NAME=hostname MODE=standalone SPRING_DATASOURCE_PLATFORM=mysql DB.NUM=1 MYSQL_MASTER_SERVICE_HOST=mysql MYSQL_MASTER_SERVICE_DB_NAME=nacos MYSQL_MASTER_SERVICE_PORT=3306 MYSQL_MASTER_SERVICE_USER=root MYSQL_MASTER_SERVICE_PASSWORD=root  
check the env file is match your docker env    
after check  will create nacos server    
    
 cd  **/wholesaleware/docker/nacos/       
  
 docker run -d --name nacos --env-file=mysql.env --network grubmarket -p 8848:8848 nacos/nacos-server:1.1.4   
  
nacos docs [https://nacos.io/en-us/](https://nacos.io/en-us/)  
  
### dev server  
  
  
[http://54.184.92.172:8848/nacos/](http://54.184.92.172:8848/nacos/)  
account: nacos    
password: grubmarket  
  
**if u guys have local databases or nacos, run and set with bootstrap-local.yaml file**  
  # IDE  
    
 - eclipse  
 - IntelliJ IDEA  
  
   
  
  ## plugin  
  
  [install  Lombok](https://projectlombok.org/)  
  
  
# run server  
  
set Active profiles   
dev or local  
![jnTjfb](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/jnTjfb.png)  
  
**if u guys have local databases or nacos, run and set with bootstrap-local.yaml file**  
  # Server type   
 We have two server types at present .      
      
      
 - Auth server      
 - User auth and management      
 - Manage communication permissions between applications      
          
 - Resource server      
 - Micro service application      
 - Focus on your own business system or business module      
 - Just as file uploading modules and message centers can be independent modules      
 - A business system can also be a resource server      
 - swagger has been integrated into it（[http://*****:****/swagger-ui.html](http://localhost:9000/swagger-ui.html)）      
      
# Boot auth server      
 - boot server before Initialize database （use auth.sql）      
 - The auth server should have an independent DB      
       
![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229203702.png)      
         
 - Table oauth_client_details      
 - business system will config CLIENT_ID,CLIENT_SECRET,SCOPE,TYPE at application.yml      
 - ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229203832.png)      
       
 - Table: oauth_user      
 - field:SOURCE should be the ID of the user table in the business system      
 - when the user of the business system passes the auth server that will get user information      
          
![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229203949.png)      
 - Auth server config :      
      
![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204132.png)      
      
# Boot resource server ( wholesale server ) - boot server before Initialize database （use wholesale.sql）      
 - The wholesale server should have an independent DB      
       
 ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204339.png)      
      
 - project config (application.yml)      
      
![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204502.png)      
      
# Use Postman to access wholesale server's restful API      
 - download Postman software([https://www.postman.com/](https://www.postman.com/))      
 - We should call the auth server to get tokens      
 - Check token      
 - Refresh token      
 - Use token to access  resource server API      
 - At resource server get user info from auth server      
      
## Use postman get token      
 - full 2 tabs (Authorization and Body) input to get token      
      
**Authorization tab** ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204652.png)      
      
**body tab** ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204715.png)      
      
**We get the token to access the wholesale server**      
 ## Check token      
      
 **full 2 tabs (Authorization and Body) input to check token**      
 ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204751.png)      
      
## Refresh token      
      
 **full 2 tabs (Authorization and Body) input to refresh token**      
 ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204814.png)      
      
## 5. Use token to access  resource server API ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204848.png)      
 ## At resource server get user info from auth server ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229204957.png)      
 # Swagger      
 ## Swagger token access      
 - maybe swagger ui url: [http://localhost:9000/swagger-ui.html](http://localhost:9000/swagger-ui.html)      
       
 ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229205048.png)      
      
 ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229205131.png)      
      
## Swagger Annotations [https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations](https://github.com/swagger-api/swagger-core/wiki/Swagger-2.X---Annotations) [https://github.com/swagger-api/swagger-core/wiki/Annotations](https://github.com/swagger-api/swagger-core/wiki/Annotations)      
      
# Utils      
 ## Check  Role use  RoleUtils
 
 ### e.g. 

![fhC3Pm](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/fhC3Pm.png)

 - User role have been redesigned. If users of company A can only see the data of company A, then there must be a record in the role table to describe the relationship between them.
- type Driver  that we think this permission is only related to order ，so source set order id
 - No other permissions have been designed yet, but we can think that if it is a roleType:USER,roleCategoryType:EMPLOYEE then the source should store the company id 
 - we have access field at ROLE table that used to control read and write permissions

### TABLE: ROLE
![NsVzGS](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/NsVzGS.png)

### AccessRole:
![DQEtXP](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/DQEtXP.png)
 ### RoleCategoryType:
 ![sf2mSO](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/sf2mSO.png)
 ### RoleType
 ![YbIUxC](https://raw.githubusercontent.com/fuzhentao/bed/pics/uPic/YbIUxC.png)
      
the user has permission or not the to access the API on the basis of this record      
![](https://raw.githubusercontent.com/fuzhentao/bed/pics/20200229210345.png)

***So far we have only a few APIs in roleUtils, and we will continue to develop in the future to support more business module***

***e.g.*** 

 - A company has multiple warehouses, but not all company users can
   access all the warehouses
 - So here we need to use the 
 roleType.**USER**  &&  roleCatagoryType.**WAREHOUSE**
 to set a record to distinguish the
roleType.**USER**  && roleCategoryType.**EMPLOYEE**

      
## In the future      
 1. Maybe later we send an email to the driver.      
 2. The driver clicks url with the token && redirect_uri      
 3. go the web page that JavaScript will put token request header and call rest API(redirect_uri)       
 4. the api check user permission if pass ,return data to web      
 5. web display the order information that the driver has permission      
       
      
# logger I've made system level log at all controller ![](https://raw.githubusercontent.com/fuzhentao/bed/pics/1582982844776.jpg)      
      
you also use annotaion @wholesaleLogger at server.impl      
      
![](https://raw.githubusercontent.com/fuzhentao/bed/pics/1582983439749.jpg)      
      
      
      
# Others      
 - REST Resource Naming Guide      
 - Bean validation      
 - JPA annotations      
 - Infinite Recursion with Jackson JSON and Hibernate JPA issue      
 - coming soon..      
      
## REST Resource Naming Guide [https://restfulapi.net/resource-naming/](https://restfulapi.net/resource-naming/)      
 ## Bean validation [https://reflectoring.io/bean-validation-with-spring-boot/](https://reflectoring.io/bean-validation-with-spring-boot/)      
 ## JPA annotations [https://dzone.com/articles/all-jpa-annotations-mapping-annotations](https://dzone.com/articles/all-jpa-annotations-mapping-annotations)      
 ## Infinite Recursion with Jackson JSON and Hibernate JPA issue [https://stackoverflow.com/questions/3325387/infinite-recursion-with-jackson-json-and-hibernate-jpa-issue](https://stackoverflow.com/questions/3325387/infinite-recursion-with-jackson-json-and-hibernate-jpa-issue)